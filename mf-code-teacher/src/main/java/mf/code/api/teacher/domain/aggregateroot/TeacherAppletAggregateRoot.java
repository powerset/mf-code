package mf.code.api.teacher.domain.aggregateroot;

import lombok.Data;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.MoneyFormatUtil;
import mf.code.teacher.repo.po.Teacher;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * mf.code.api.teacher.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午5:02
 */
@Data
public class TeacherAppletAggregateRoot {
	// 平台已累计发放分红(真实)
	private BigDecimal bonus;
	// 店铺banner
	private List<Map> bannerList;
	// 大咖列表
	private List<Map> bigNameList;
	// 进入前10的真实用户信息
	private List<Teacher> trueTeachers;
	// 10名排行榜机器人
	private List<Map> falseTeachers;
	// 个人累计收益排行榜
	private Set<ZSetOperations.TypedTuple<String>> incomeRange;
	// 军团武力均值
	private int averageValueOfForce;

	public Map<String, Object> getHomePageAppletInfo() {
		BigDecimal falseBonus = getFalseBonus();

		Map<String, Object> appletInfo = new HashMap<>();
		appletInfo.put("rankingList", queryTeacherIncomeRangeByTopSize(3));
		appletInfo.put("bonus", MoneyFormatUtil.yuanToWanyuan(this.bonus.add(falseBonus)));
		appletInfo.put("bannerList", this.bannerList);
		appletInfo.put("bigNameList", this.bigNameList);
		return appletInfo;
	}

	public Map<String, Object> queryRankingListTop10() {
		Map<String, Object> rankingList = new HashMap<>();
		rankingList.put("rankingList", queryTeacherIncomeRangeByTopSize(10));
		return rankingList;
	}

	private BigDecimal getFalseBonus() {
		BigDecimal falseBonus = BigDecimal.ZERO;
		if (CollectionUtils.isEmpty(this.falseTeachers)) {
			return falseBonus;
		}

		for (Map falseMap : falseTeachers) {
			falseBonus = falseBonus.add(new BigDecimal(falseMap.get("totalIncome").toString()));
		}

		return falseBonus;
	}

	private Object queryTeacherIncomeRangeByTopSize(int size) {
		List<Map> rankingList = new ArrayList<>();

		Map<String, Map> falseTeacherMap = newFalseTeacherMap();
		Map<String, Map> trueTeacherMap = newTrueTeacherMap();

		int rank = 1;
		for (ZSetOperations.TypedTuple<String> next : incomeRange) {
			if (rank > size) {
				break;
			}
			Map map = null;
			if (trueTeacherMap.containsKey(next.getValue())) {
				map = trueTeacherMap.get(next.getValue());

			}
			if (falseTeacherMap.containsKey(next.getValue())) {
				map = falseTeacherMap.get(next.getValue());
			}
			rankingList.add(map);

			rank++;
		}

		return rankingList;
	}

	private Map<String, Map> newFalseTeacherMap() {
		Map<String, Map> falseTeacherMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(falseTeachers)) {
			for (Map falseTeacher : falseTeachers) {
				falseTeacher.put("totalIncome", MoneyFormatUtil.yuanToWanyuan(new BigDecimal(falseTeacher.get("totalIncome").toString())));
				falseTeacherMap.put(falseTeacher.get("tid").toString(), falseTeacher);
			}
		}
		return falseTeacherMap;
	}

	private Map<String, Map> newTrueTeacherMap() {
		Map<String, Map> trueTeacherMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(trueTeachers)) {
			for (Teacher teacher: trueTeachers) {
				Map<String, Object>  incomeListMap = new HashMap<>();
				incomeListMap.put("tid", teacher.getId());
				incomeListMap.put("avatarUrl", teacher.getAvatarUrl());
				incomeListMap.put("roleName", teacher.getRoleName());
				incomeListMap.put("enterTime", DateUtil.dateToString(teacher.getAuditTime(), DateUtil.POINT_DATE_FORMAT));
				incomeListMap.put("totalIncome", MoneyFormatUtil.yuanToWanyuan(teacher.getTotalIncome()));
				trueTeacherMap.put(teacher.getId().toString(), incomeListMap);
			}
		}
		return trueTeacherMap;
	}

	public void setBonus(String bonus) {
		if (StringUtils.isBlank(bonus)) {
			this.bonus = BigDecimal.ZERO;
			return;
		}
		this.bonus = new BigDecimal(bonus).setScale(2, RoundingMode.DOWN);
	}
}
