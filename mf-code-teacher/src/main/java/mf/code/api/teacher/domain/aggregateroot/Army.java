package mf.code.api.teacher.domain.aggregateroot;

import lombok.Data;
import mf.code.api.teacher.domain.valueobject.ArmyConfiguration;
import mf.code.api.teacher.domain.valueobject.ArmyFunds;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * 军团
 *
 * mf.code.api.teacher.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-08 下午5:07
 */
@Data
public class Army {
	private Long tid;
	private ArmyFunds armyFunds;
	private ArmyConfiguration armyConfiguration;

	public Army(ArmyFunds armyFunds, ArmyConfiguration armyConfiguration) {
		this.armyFunds = armyFunds;
		this.armyConfiguration = armyConfiguration;
	}

	/**
	 * 军队估值
	 */
	public BigDecimal assessmentValueOfArmy() {
		// 军团估值 = 自己商家总粉丝数 * 40 + 下级讲师商家总粉丝数 * 6

		Integer soldierNum = this.armyConfiguration.getSoldierNum();
		BigDecimal fansValue = new BigDecimal(40);
		BigDecimal myArmyValue = new BigDecimal(soldierNum).multiply(fansValue);

		Integer friendlySoldierNum = this.armyConfiguration.getFriendlySoldierNum();
		BigDecimal friendlyFansValue = new BigDecimal(6);
		BigDecimal friendlyArmyValue = new BigDecimal(friendlySoldierNum).multiply(friendlyFansValue);

		return myArmyValue.add(friendlyArmyValue).setScale(0, RoundingMode.DOWN);
	}

	public int getValueOfForce() {
		// 武力值 = （将领数*0.2+士兵数*0.35+友军数*0.1+友军将领*0.1+友军士兵数*0.25）* 100
		int generalNum = this.armyConfiguration.getGeneralNum();
		int soldierNum = this.armyConfiguration.getSoldierNum();
		int friendNum = this.armyConfiguration.getFriendNum();
		int friendlyGeneralNum = this.armyConfiguration.getFriendlyGeneralNum();
		int friendlySoldierNum = this.armyConfiguration.getFriendlySoldierNum();

		return (int)((generalNum*0.2 + soldierNum*0.35 + friendNum*0.1 + friendlyGeneralNum*0.1 + friendlySoldierNum*0.25) * 100);
	}

	public Map<String, Object> getArmyPage() {
		Map<String, Object> resultVO = new HashMap<>();
		// 返回军饷库信息
		resultVO.put("totalIncome", this.armyFunds.getTotalIncome());
		resultVO.put("armyValue", assessmentValueOfArmy());
		resultVO.put("recruitGeneralIncome", this.armyFunds.getRecruitGeneralIncome());
		resultVO.put("soldierIncome", this.armyFunds.getSoldierIncome());
		resultVO.put("friendlyIncome", this.armyFunds.getFriendlyRecruitGeneralIncome());
		resultVO.put("promotionBouns", this.armyFunds.getPromotionBouns());
		resultVO.put("quarterBouns", this.armyFunds.getQuarterBouns());
		resultVO.put("yearBouns", this.armyFunds.getYearBouns());
		// 返回军团配置
		resultVO.put("forceValue", getValueOfForce());
		resultVO.put("generalNum", this.armyConfiguration.getGeneralNum());
		resultVO.put("soldierNum", this.armyConfiguration.getSoldierNum());
		resultVO.put("friendNum", this.armyConfiguration.getFriendNum());
		resultVO.put("friendlyGeneralNum", this.armyConfiguration.getFriendlyGeneralNum());
		resultVO.put("friendlySoldierNum", this.armyConfiguration.getFriendlySoldierNum());
		return resultVO;
	}
}
