package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.goods.repo.po.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ActivityDefMapper extends BaseMapper<ActivityDef> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(ActivityDef record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ActivityDef record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ActivityDef selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ActivityDef record);

    /**
     * 退库存
     * @param map
     * @return
     */
    int addStock(Map map);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ActivityDef record);

    /**
     * 查询活动定义列表
     *
     * @param params
     * @return
     */
    List<ActivityDef> selectByParams(Map<String, Object> params);

    /**
     * 总数
     *
     * @param params
     * @return
     */
    Integer countForPageList(Map<String, Object> params);

    /**
     * 微信回调后改库存
     * @param  m
     * @return
     */
    Integer updateStockAfterCallback(Map m);

    /**
     * 微信回调后改库存
     *
     * @param id
     * @return
     */
    Integer updateDeposit(@Param(value = "id") Long id,
                          @Param(value = "amount") BigDecimal amount,
                          @Param(value = "num") Integer num,
                          @Param(value = "uTime") Date uTime);

    List<ActivityDef> listPageByParams(Map param);

    /**
     * 查询符合参数条件的总记录数
     *
     * @param param 查询条件
     * @return Integer 总记录数
     */
    Integer countByParams(Map<String, Object> param);

    /**
     * 增加保证金余额 和 库存余额
     *
     * @param id
     * @param depositActual
     * @param stockActual
     * @return
     */
    Integer updateDepositDefAndStockDef(@Param(value = "id") Long id,
                                        @Param(value = "depositActual") BigDecimal depositActual,
                                        @Param(value = "stockActual") Integer stockActual,
                                        @Param(value = "uTime") Date uTime);

    /**
     * 更新红包任务红包库存
     *
     * @param id     def主键
     * @param num    更新红包数量（包含正负号）
     * @param amount 更新红包金额（包含正负号）
     * @param uTime  更新时间
     * @return
     */
    Integer updateRedPackStock(@Param(value = "id") Long id,
                               @Param(value = "num") Integer num,
                               @Param(value = "amount") BigDecimal amount,
                               @Param(value = "uTime") Date uTime);

    int subtractTaskRpNumByPrimaryKey(Long activityDefId, @Param(value = "uTime") Date uTime);

    List<ActivityDef> selectByMerchantIdShopId(Map m);

    /**
     * 如果是新人有礼物 , 也就是助力活动 , 也就是赠品活动
     * @param m
     * @return
     */
    List<ActivityDef> selectByMerchantIdShopId4Newbie(Map m);

    ActivityDef findByGoodsId(Long goodsId);

    List<ActivityDef> findListByShopId(Long shopId);

    List<ActivityDef> selectDefByShopId4Summary(Map<String, Object> param);
    ActivityDef selectOpenRPDefByShopId(Long shopId);

    List<Goods> selectGoodsByDef(Map m);

    List<Goods> selectGoodsByTask(Map m);

    Integer reduceShopManagerDeposit(@Param("id") Long id, @Param("stock") int stock, @Param("uTime") Date uTime);
}
