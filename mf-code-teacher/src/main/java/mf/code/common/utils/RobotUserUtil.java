package mf.code.common.utils;

import org.apache.commons.lang.math.NumberUtils;

import java.util.*;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月14日 14:19
 */
public class RobotUserUtil {
    private static List<String> robot_user_name = new ArrayList<String>();

    static {
//        robot_user_name.add("脸美逼遭罪@");
//        robot_user_name.add("承诺i");
//        robot_user_name.add("站在千里之外");
//        robot_user_name.add("泪°");
//        robot_user_name.add("ぬ 兲使と翼");
//        robot_user_name.add("島是海哭碎菂訫°");
//        robot_user_name.add("叫我小透明");
//        robot_user_name.add("别同情我");
//        robot_user_name.add("库存已久");
//        robot_user_name.add("路人甲i 路人");
//        robot_user_name.add("旁观者");
//        robot_user_name.add("你听我说");
//        robot_user_name.add("不惯与生人识");
//        robot_user_name.add("遥歌");
//        robot_user_name.add("没有梦想何必远方");
//        robot_user_name.add("何必虚情假意说我最珍贵i");
//        robot_user_name.add("西瓜熟了夏天走了他丢了");
//        robot_user_name.add("一片霓裳");
//        robot_user_name.add("太爱闹i");
//        robot_user_name.add("想你的瞬间");
//        robot_user_name.add("睫毛溺了水");
//        robot_user_name.add("cardiac(心脏");
//        robot_user_name.add("可爱的不像话");
//        robot_user_name.add("一个人　　Always°");
//        robot_user_name.add("|▍Me-未成年");
//        robot_user_name.add("我的华丽转身要践踏着你们");
//        robot_user_name.add("北里新妆");
//        robot_user_name.add("女汉子般的坚强i");
//        robot_user_name.add("ゞ_早已忘记心痛的感觉つ");
        robot_user_name.add("果酱");
//        robot_user_name.add("夜辰");
    }

    private static final String avatarUrl_front = "https://asset.wxyundian.com/data-test3/forbid_robot_user/";// robot1.jpg

    public static Map<String, Object> getRobotUserAvatarUrlAndNickFromOss() {
        Map<String, Object> map = new HashMap<>();
        int urlRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(1, 30));
        String name = "robot" + 1 + ".jpg";
        map.put("url", avatarUrl_front + name);
        int nameRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(0, 30));
        map.put("name", robot_user_name.get(0));
        return map;
    }

    public static String getRandom(int min, int max) {
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return String.valueOf(s);
    }

    private static List<String> assist_user_name = new ArrayList<String>();

    static {
        assist_user_name.add("不交电费瞎发啥光i");
        assist_user_name.add("承诺i");
        assist_user_name.add("站在千里之外");
        assist_user_name.add("泪°");
        assist_user_name.add("ぬ 兲使と翼");
        assist_user_name.add("島是海哭碎菂訫°");
        assist_user_name.add("叫我小透明");
        assist_user_name.add("别同情我");
        assist_user_name.add("库存已久");
        assist_user_name.add("路人甲i 路人");
        assist_user_name.add("旁观者");
        assist_user_name.add("你听我说");
        assist_user_name.add("不惯与生人识");
        assist_user_name.add("遥歌");
        assist_user_name.add("没有梦想何必远方");
        assist_user_name.add("何必虚情假意说我最珍贵i");
        assist_user_name.add("西瓜熟了夏天走了他丢了");
        assist_user_name.add("一片霓裳");
        assist_user_name.add("太爱闹i");
        assist_user_name.add("想你的瞬间");
        assist_user_name.add("睫毛溺了水");
        assist_user_name.add("cardiac(心脏");
        assist_user_name.add("可爱的不像话");
        assist_user_name.add("一个人　　Always°");
        assist_user_name.add("|▍Me-未成年");
        assist_user_name.add("我的华丽转身要践踏着你们");
        assist_user_name.add("北里新妆");
        assist_user_name.add("┈┾冷瞳づ");
        assist_user_name.add("ゞ_早已忘记心痛的感觉つ");
        assist_user_name.add("果酱");
        assist_user_name.add("夜辰");
    }

    public static Map<String, Object> getAssistUserAvatarUrlAndNickFromOss() {
        Map<String, Object> map = new HashMap<>();
        int urlRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(1, 30));
        String name = "robot" + urlRandomNum + ".jpg";
        map.put("avatarUrl", avatarUrl_front + name);
        int nameRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(0, 30));
        map.put("nickName", assist_user_name.get(nameRandomNum));
        return map;
    }

    /******************集客师*********************/
    private static List<String> teacher_role_name = new ArrayList<String>();

    static {
        teacher_role_name.add("Tom老师");
        teacher_role_name.add("James老师");
        teacher_role_name.add("Parker老师");
        teacher_role_name.add("Carvin老师");
        teacher_role_name.add("丫丫老师");
        teacher_role_name.add("Bob老师");
        teacher_role_name.add("Mic老师");
        teacher_role_name.add("Jack老师");
        teacher_role_name.add("青山老师");
        teacher_role_name.add("Seven老师");
        teacher_role_name.add("魔风老师");
        teacher_role_name.add("飞叶老师");
        teacher_role_name.add("无尘老师");
        teacher_role_name.add("马丁老师");
        teacher_role_name.add("song老师");
        teacher_role_name.add("廖老师");
        teacher_role_name.add("Johnny老师");
        teacher_role_name.add("林老师");
        teacher_role_name.add("曼妮老师");
        teacher_role_name.add("梦晴老师");
        teacher_role_name.add("小艾老师");
        teacher_role_name.add("于菲老师");
        teacher_role_name.add("小樱老师");
        teacher_role_name.add("小若老师");
        teacher_role_name.add("笑笑老师");
        teacher_role_name.add("小夕老师");
        teacher_role_name.add("丹丹老师");
        teacher_role_name.add("kerry老师");
        teacher_role_name.add("Fyy老师");
        teacher_role_name.add("百川老师");
        teacher_role_name.add("曲成老师");
    }


    public static Map<String, Object> getTeacher() {
        Map<String, Object> map = new HashMap<>();
        int nameRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(0, 30));
        int urlRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(1, 30));
        String name = "robot" + urlRandomNum + ".jpg";
        map.put("avatarUrl", avatarUrl_front + name);
        map.put("roleName", teacher_role_name.get(nameRandomNum));
        String random = RobotUserUtil.getRandom(1, 2);
        map.put("type", random);
        return map;
    }

    /***
     * 获取集客师假数据
     * @param limit
     * @return
     */
    public static List<Map<String, Object>> getRobotTeacher(int limit) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            while (true) {
                Map<String, Object> map = RobotUserUtil.getTeacher();
                int frequency = Collections.frequency(list, map);
                //每次获取的时候对象重复次数不能超过2次
                if (frequency <= 2) {
                    list.add(map);
                    break;
                }
            }
        }
        return list;
    }
}
