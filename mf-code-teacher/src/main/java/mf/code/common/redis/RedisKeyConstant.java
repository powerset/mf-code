package mf.code.common.redis;

/**
 * mf.code.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-13 上午10:38
 */
public class RedisKeyConstant {
	/**
	 * 指导一个商家导入至少1商品
	 */
	public static final String TEACHER_TRAINING_IOP = "teacher:training:iop:";// <tid>

	/**
	 * 指导一个商家创建至少3个活动
	 */
	public static final String TEACHER_TRAINING_CTA = "teacher:training:cta:";// <tid>

	/**
	 * 协助一个商家提升粉丝数
	 */
	public static final String TEACHER_TRAINING_PF = "teacher:training:pf:"; // <num>:<tid>
	/**
	 * redis 收入弹框防重加锁
	 */
	public static final String TEACHER_INCOME_DIALOG_FORBID_REPEAT = "teacher:income:dialog:forbidrepeat:";//<tid>

	/**
	 * 集客师成功招募将领 增加收益弹窗 40%
	 */
	public static final String TEACHER_INCOME_DIALOG40 = "teacher:income:dialog40:";//<tid>
	/**
	 * 推荐的集客师 成功招募将领 增加收益弹窗 5%
	 */
	public static final String TEACHER_INCOME_DIALOG5 = "teacher:income:dialog5:";//<tid>

	/**
	 * 各军团武力值
	 */
	public static final String TEACHER_ARMY_FORCE = "teacher:army:force";

	/**
	 * 数据库 teacher数据缓存
	 */
	public static final String TEACHER_CACHE = "teacher:cache:id:";//<tid>
	/**
	 * 数据库 teacherInvite缓存
	 */
	public static final String TEACHER_INVITE_CACHE = "teacher:invite:cache:id:";//<tid>
	//申请结果
	public static final String TEACHER_APPLY_RESULT = "teacher:apply:result:";//<tid>

	public static final String TEACHER_RECOMMAND_COUNT_LIST = "teacher:recommand:count:list:";//<tid>
	/**
	 * 集客师平台累计发放分红
	 */
	public static final String TEACHER_PLATFORM_ACCUMULATED_BONUS = "teacher:platform:accumulated:bonus";

	/**
	 * 个人累计收益排行榜(真实数据) key, roleName, scroe
	 */
	public static final String TEACHER_ACCUMULATED_INCOME_LIST = "teacher:accumulated:income:list";
	/**
	 * 个人累计收益排行榜(带有假数据)
	 */
	public static final String TEACHER_ACCUMULATED_INCOME_LIST_WITH_FALSE = "teacher:accumulated:income:list:with:false";
	/**
	 * 集客师 商户注册用短信验证码键
	 */
	public final static String TEACHER_REGISTER = "teacher:register:";

	/**
	 * 集客师今日信息
	 * String todayInfo = stringRedisTemplate.opsForValue().get(RedisKeyConstant.TEACHER_TODY_INFO + teacherAggregateRoot.getId());
	 * if (StringUtils.isNotBlank(todayInfo)) {
	 * 		JSONObject jsonObject = JSON.parseObject(todayInfo);
	 * 		jsonObject.getBigDecimal("todayIncome");
	 * 		jsonObject.getInteger("todayMerchants");
	 * }
	 */
	public final static String TEACHER_TODY_INFO = "teacher:tody:info:";//<tid>
	/**
	 * 数据字典键
	 * key--> common:dict:<type>:<key>
	 * value--> 1
	 */
	public static final String COMMON_DICT = "common:dict:";
	/**
	 *
	 */
	public static final String ERROR_EMAILSENDER = "log:email:send:";//<title>

	public static String USER_LOGIN_FORBID_REPEAT = "user:login:forbid:repeat:";//<openId>

	/**
	 * 数据库 用户数据缓存
	 */
	public static final String MF_CODE_USER_CACHE = "mf:code:user:id:";//<userId>

	/**
	 * 首页用户发起(走订单)活动的reids
	 */
	public static final String HOMEPAGE_ACTIVITY_SHOP = "activity:activity:homepage:shop:";//<shopid>
	/**
	 * 首页用户发起(走订单)活动的reids
	 */
	public static final String HOMEPAGE_TURNTABLE_SHOP = "activity:activity:homepage:shop:turntable:";//<shopid>:<uid>
}
