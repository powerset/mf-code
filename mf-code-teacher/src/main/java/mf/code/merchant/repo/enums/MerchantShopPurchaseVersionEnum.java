//package mf.code.merchant.repo.enums;
//
///**
// * mf.code.merchant.repo.enums
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年02月25日 14:42
// */
//public enum MerchantShopPurchaseVersionEnum {
//    //0测试版(给测试用的)1试用版 2公开版 3私有版
//    VERSION_TEST(0, "测试版"),
//    VERSION_TRYOUT(1, "试用版"),
//    VERSION_PUBLIC(2, "公有版"),
//    VERSION_PRIVATE(3, "私有版"),
//    ;
//
//    private int code;
//    private String message;
//
//    MerchantShopPurchaseVersionEnum(int code, String message) {
//        this.code = code;
//        this.message = message;
//    }
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//
//    public static MerchantShopPurchaseVersionEnum findByCode(Integer code) {
//        for (MerchantShopPurchaseVersionEnum typeEnum : MerchantShopPurchaseVersionEnum.values()) {
//            if (typeEnum.getCode() == code) {
//                return typeEnum;
//            }
//        }
//        return null;
//    }
//
//
//    public static String findByDesc(Integer code) {
//        for (MerchantShopPurchaseVersionEnum typeEnum : MerchantShopPurchaseVersionEnum.values()) {
//            if (typeEnum.getCode() == code) {
//                return typeEnum.getMessage();
//            }
//        }
//        return "";
//    }
//}
