package mf.code.comment.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * comment
 */
public class Comment implements Serializable {
    /**
     * 主键id
     */
    private Long id;

    /**
     *
     */
    private Long parentId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 订单编号 本系统的订单编号
     */
    private String orderNo;

    /**
     * 谁评论就填睡的id，若role=1则userId为用户id,若role=2则userId=merchantId
     */
    private Long userId;

    /**
     * 商品的parentid
     */
    private Long goodsParentId;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 商品skuid
     */
    private Long skuId;

    /**
     * -1初始值,0买家已评论，1审核通过，2不通过
     */
    private Integer status;

    /**
     * 审核不通过的理由
     */
    private String reason;

    /**
     * 评论信息
     */
    private String contents;

    /**
     * 评论上传的图片信息，json字符串 ["url1","url2"]
     */
    private String pics;

    /**
     * 总评分
     */
    private BigDecimal totalStars;

    /**
     * 评论等级详情，json格式 {"desStar":2,"logisticsStar":3,"serviceStar":4}
     */
    private String commentStarsDetail;

    /**
     * 提交评价时间
     */
    private Date submitTime;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 是否删除，0未删除，1已删除，默认是0
     */
    private Integer del;

    /**
     * 1用户，2商户，3平台
     */
    private Integer role;

    /**
     * comment
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * @return id 主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键id
     * @param id 主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return parent_id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     *
     * @param parentId
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 订单id
     * @return order_id 订单id
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * 订单id
     * @param orderId 订单id
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 订单编号 本系统的订单编号
     * @return order_no 订单编号 本系统的订单编号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 订单编号 本系统的订单编号
     * @param orderNo 订单编号 本系统的订单编号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 谁评论就填睡的id，若role=1则userId为用户id,若role=2则userId=merchantId
     * @return user_id 谁评论就填睡的id，若role=1则userId为用户id,若role=2则userId=merchantId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 谁评论就填睡的id，若role=1则userId为用户id,若role=2则userId=merchantId
     * @param userId 谁评论就填睡的id，若role=1则userId为用户id,若role=2则userId=merchantId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 商品的parentid
     * @return goods_parent_id 商品的parentid
     */
    public Long getGoodsParentId() {
        return goodsParentId;
    }

    /**
     * 商品的parentid
     * @param goodsParentId 商品的parentid
     */
    public void setGoodsParentId(Long goodsParentId) {
        this.goodsParentId = goodsParentId;
    }

    /**
     * 商品id
     * @return goods_id 商品id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商品id
     * @param goodsId 商品id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商品skuid
     * @return sku_id 商品skuid
     */
    public Long getSkuId() {
        return skuId;
    }

    /**
     * 商品skuid
     * @param skuId 商品skuid
     */
    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    /**
     * -1初始值,0买家已评论，1审核通过，2不通过
     * @return status -1初始值,0买家已评论，1审核通过，2不通过
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * -1初始值,0买家已评论，1审核通过，2不通过
     * @param status -1初始值,0买家已评论，1审核通过，2不通过
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 审核不通过的理由
     * @return reason 审核不通过的理由
     */
    public String getReason() {
        return reason;
    }

    /**
     * 审核不通过的理由
     * @param reason 审核不通过的理由
     */
    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    /**
     * 评论信息
     * @return contents 评论信息
     */
    public String getContents() {
        return contents;
    }

    /**
     * 评论信息
     * @param contents 评论信息
     */
    public void setContents(String contents) {
        this.contents = contents == null ? null : contents.trim();
    }

    /**
     * 评论上传的图片信息，json字符串 ["url1","url2"]
     * @return pics 评论上传的图片信息，json字符串 ["url1","url2"]
     */
    public String getPics() {
        return pics;
    }

    /**
     * 评论上传的图片信息，json字符串 ["url1","url2"]
     * @param pics 评论上传的图片信息，json字符串 ["url1","url2"]
     */
    public void setPics(String pics) {
        this.pics = pics == null ? null : pics.trim();
    }

    /**
     * 总评分
     * @return total_stars 总评分
     */
    public BigDecimal getTotalStars() {
        return totalStars;
    }

    /**
     * 总评分
     * @param totalStars 总评分
     */
    public void setTotalStars(BigDecimal totalStars) {
        this.totalStars = totalStars;
    }

    /**
     * 评论等级详情，json格式 {"desStar":2,"logisticsStar":3,"serviceStar":4}
     * @return comment_stars_detail 评论等级详情，json格式 {"desStar":2,"logisticsStar":3,"serviceStar":4}
     */
    public String getCommentStarsDetail() {
        return commentStarsDetail;
    }

    /**
     * 评论等级详情，json格式 {"desStar":2,"logisticsStar":3,"serviceStar":4}
     * @param commentStarsDetail 评论等级详情，json格式 {"desStar":2,"logisticsStar":3,"serviceStar":4}
     */
    public void setCommentStarsDetail(String commentStarsDetail) {
        this.commentStarsDetail = commentStarsDetail == null ? null : commentStarsDetail.trim();
    }

    /**
     * 提交评价时间
     * @return submit_time 提交评价时间
     */
    public Date getSubmitTime() {
        return submitTime;
    }

    /**
     * 提交评价时间
     * @param submitTime 提交评价时间
     */
    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 是否删除，0未删除，1已删除，默认是0
     * @return del 是否删除，0未删除，1已删除，默认是0
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 是否删除，0未删除，1已删除，默认是0
     * @param del 是否删除，0未删除，1已删除，默认是0
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 1用户，2商户，3平台
     * @return role 1用户，2商户，3平台
     */
    public Integer getRole() {
        return role;
    }

    /**
     * 1用户，2商户，3平台
     * @param role 1用户，2商户，3平台
     */
    public void setRole(Integer role) {
        this.role = role;
    }

    /**
     *
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", orderId=").append(orderId);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", userId=").append(userId);
        sb.append(", goodsParentId=").append(goodsParentId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", skuId=").append(skuId);
        sb.append(", status=").append(status);
        sb.append(", reason=").append(reason);
        sb.append(", contents=").append(contents);
        sb.append(", pics=").append(pics);
        sb.append(", totalStars=").append(totalStars);
        sb.append(", commentStarsDetail=").append(commentStarsDetail);
        sb.append(", submitTime=").append(submitTime);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", role=").append(role);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
