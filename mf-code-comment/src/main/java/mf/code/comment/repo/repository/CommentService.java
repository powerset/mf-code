package mf.code.comment.repo.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.comment.repo.po.Comment;

/**
 * mf.code.comment.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-01 22:05
 */
public interface CommentService extends IService<Comment> {
}
