package mf.code.comment.repo.dto;/**
 * create by qc on 2019/8/20 0020
 */

import lombok.Data;

/**
 * 吐槽实体
 *
 * @author gbf
 * 2019/8/20 0020、09:43
 */
@Data
public class ComplainDto {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 内容
     */
    private String content;
    /**
     * ddd:抖带带  dxp:买家版
     */
    private String project;
}
