package mf.code.comment.repo.dto;

import lombok.Data;

@Data
public class CommentReqDTO {

    /**
     * 商户id
     */
    private Long merchantId;
    /**
     * 评论内容
     */
    private String contents;
    /**
     * 评论id
     */
    private Long commentId;
    /**
     * 店铺id
     */
    private Long shopId;
}
