package mf.code.comment.repo.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.repo.dao.CommentMapper;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.repo.repository.CommentService;
import org.springframework.stereotype.Service;

/**
 * mf.code.comment.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-01 22:05
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
}
