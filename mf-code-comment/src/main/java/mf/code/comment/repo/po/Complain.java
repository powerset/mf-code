package mf.code.comment.repo.po;/**
 * create by qc on 2019/8/21 0021
 */

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * @author gbf
 * 2019/8/21 0021、11:28
 */
@Data
@ToString
@NoArgsConstructor
@Document(value = "dy_complain")
public class Complain {
    @Id
    @Field("_id")
    private String id;
    /**
     * 用户ID
     */
    @Field(value = "uid")
    private String uid;

    /**
     * ddd:抖带带  dxp:买家版
     */
    private String project;

    /**
     * 电话
     */
    @Field(value = "phone")
    private String phone;

    /**
     * 内容
     */
    @Field(value = "content")
    private String content;

    /**
     * 创建时间
     */
    @Field(value = "ctime")
    private Date ctime;

    /**
     * 时间戳
     */
    @Field(value = "current")
    private Long current;

    /**
     * 版本号
     */
    @Field(value = "version")
    private String version;
}
