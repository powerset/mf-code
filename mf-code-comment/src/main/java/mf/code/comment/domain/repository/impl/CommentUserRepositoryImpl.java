package mf.code.comment.domain.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.api.feignclient.OrderAppService;
import mf.code.comment.api.feignclient.UserAppService;
import mf.code.comment.domain.aggregateroot.CommentUser;
import mf.code.comment.domain.repository.CommentUserRepository;
import mf.code.comment.dto.OrderCommentDTO;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.repo.repository.CommentService;
import mf.code.common.DelEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.dto.UserResp;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.comment.domain.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 14:24
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CommentUserRepositoryImpl implements CommentUserRepository {
    private final CommentService commentService;
    private final UserAppService userAppService;
    private final OrderAppService orderAppService;

    /**
     * 通过用户id 获取评价用户的信息
     *
     * @param userId
     * @return 完成 userId， userInfo 信息的装配
     */
    @Override
    public CommentUser findByUserId(Long userId) {
        UserResp userResp = userAppService.queryUser(userId);
        if (userResp == null) {
            log.error("服务异常, userId = {}", userId);
            return null;
        }
        CommentUser commentUser = new CommentUser();
        commentUser.setUserId(userResp.getId());
        commentUser.setUserInfo(userResp);
        return commentUser;
    }

    /**
     * 通过订单id 增装订单评价信息
     *
     * @param commentUser
     * @param orderId
     *
     * 完成 commentList 信息的装配
     */
    @Override
    public void addOrderComment(CommentUser commentUser, Long orderId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Comment::getOrderId, orderId)
                .eq(Comment::getDel, DelEnum.NO.getCode());
        wrapper.orderByAsc("ctime");
        List<Comment> comments = commentService.list(wrapper);
        commentUser.setCommentList(comments);
    }

    /**
     * 增装 待评价商品信息
     *
     * @param commentUser
     * @param merchantId
     * @param shopId
     * @param offset
     * @param size
     */
    @Override
    public void addPendingGoodsPage(CommentUser commentUser, Long merchantId, Long shopId, Long offset, Long size) {
        AppletMybatisPageDto<Map> pendingGoodsPage = new AppletMybatisPageDto<>(size, offset);

        Long userId = commentUser.getUserId();
        SimpleResponse goodsOrderByPage = this.orderAppService.getGoodsOrderByPage(merchantId, shopId, userId, 4, 100, 0);
        if (goodsOrderByPage.getCode() != 0) {
            log.error("服务异常，simpleResponse.getCode() != 0");
            commentUser.setPendingGoodsPage(pendingGoodsPage);
            return;
        }

        Map appletMybatisPageDto = (Map) goodsOrderByPage.getData();
        List content = (List) appletMybatisPageDto.get("content");
        if (CollectionUtils.isEmpty(content)) {
            commentUser.setPendingGoodsPage(pendingGoodsPage);
            return;
        }

        List<Long> orderIdList = new ArrayList<>();
        Map<Long, Map> goodsOrderRespMap = new HashMap<>();
        for (Object obj : content) {
            Map goodsOrderMap = (Map) obj;
            Long orderId = Long.valueOf(goodsOrderMap.get("orderId").toString());
            orderIdList.add(orderId);
            goodsOrderRespMap.put(orderId, goodsOrderMap);
        }

        Page<Comment> page = new Page<>(offset / size + 1, size);
        page.setAsc("ctime");
        // 查询条件 userId; parentId = 0 ，status == -1
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Comment::getOrderId, orderIdList)
                .eq(Comment::getParentId, 0L)
                .eq(Comment::getStatus, CommentStatusEnum.INIT.getCode())
                .eq(Comment::getDel, DelEnum.NO.getCode());
        IPage<Comment> commentIPage = commentService.page(page, wrapper);
        if (commentIPage == null) {
            log.error("数据库异常");
            commentUser.setPendingGoodsPage(pendingGoodsPage);
            return;
        }
        long total = commentIPage.getTotal();
        pendingGoodsPage.setTotal(total);
        if (offset + size < total) {
            pendingGoodsPage.setPullDown(true);
        }

        List<Map> goodsOrderRespList = new ArrayList<>();
        List<Comment> records = commentIPage.getRecords();
        for (Comment comment : records) {
            Map goodsOrderMap = goodsOrderRespMap.get(comment.getOrderId());
            goodsOrderMap.put("commentStatus", 0);
            goodsOrderRespList.add(goodsOrderMap);
        }
        pendingGoodsPage.setContent(goodsOrderRespList);

        commentUser.setPendingGoodsPage(pendingGoodsPage);
    }

    /**
     * 保存 用户提交的评价信息
     *
     * @param commentUser
     * @return
     */
    @Transactional
    @Override
    public boolean saveComment(CommentUser commentUser) {
        List<Comment> commentList = commentUser.getCommentList();
        if (CollectionUtils.isEmpty(commentList)) {
            log.info("没有要保存的记录");
            return true;
        }
        List<Comment> saveCommentList = new ArrayList<>();
        List<Comment> updateCommentList = new ArrayList<>();
        for (Comment comment : commentList) {
            if (comment.getId() == null) {
                saveCommentList.add(comment);
            } else {
                updateCommentList.add(comment);
            }
        }
        if (!CollectionUtils.isEmpty(saveCommentList)) {
            boolean save = commentService.saveBatch(saveCommentList);
            if (!save) {
                log.error("数据库存储异常");
                throw new RuntimeException();
            }
        }
        if (!CollectionUtils.isEmpty(updateCommentList)) {
            boolean update = commentService.updateBatchById(updateCommentList);
            if (!update) {
                log.error("数据库存储异常");
                throw new RuntimeException();
            }
        }
        return true;
    }

    /**
     * 根据评价状态和店铺id查询评论信息
     * @param statusList
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    @Override
    public List<OrderCommentDTO> getCommentListByStatus(List<Integer> statusList, Long shopId, int page, int size) {
        if (page<=0){
            page=1;
        }
        if (size<=0){
            size=10;
        }
        QueryWrapper<Comment> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().in(Comment::getStatus,statusList)
                .eq(Comment::getShopId,shopId)
                .eq(Comment::getDel,DelEnum.NO.getCode());
        Page<Comment> pages = new Page<>(page, size);

        IPage<Comment> commentIPage = commentService.page(pages, queryWrapper);

        if (null == commentIPage){
            return null;
        }

        List<OrderCommentDTO> orderCommentDTOList = new ArrayList<>();

        List<Comment> records = commentIPage.getRecords();

        if (null ==records ){
            return null;
        }

        BeanUtils.copyProperties(records,orderCommentDTOList);

        return orderCommentDTOList;
    }
}
