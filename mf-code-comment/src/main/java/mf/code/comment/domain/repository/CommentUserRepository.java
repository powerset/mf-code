package mf.code.comment.domain.repository;

import mf.code.comment.domain.aggregateroot.CommentUser;
import mf.code.comment.dto.OrderCommentDTO;

import java.util.List;
import mf.code.comment.repo.po.Comment;

/**
 * mf.code.comment.domain.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 14:23
 */
public interface CommentUserRepository {
    /**
     * 通过用户id 获取评价用户的信息
     *
     * @param userId
     * @return
     *
     * 完成 userId， userInfo 信息的装配
     */
    CommentUser findByUserId(Long userId);

    /**
     * 通过订单id 增装订单评价信息
     *
     * @param commentUser
     * @param orderId
     *
     * 完成 commentList 信息的装配
     */
    void addOrderComment(CommentUser commentUser, Long orderId);

    /**
     * 增装 待评价商品信息
     * @param commentUser
     */
    void addPendingGoodsPage(CommentUser commentUser, Long merchantId, Long shopId, Long offset, Long size);

    /**
     * 保存 用户提交的评价信息
     * @param commentUser
     * @return
     */
    boolean saveComment(CommentUser commentUser);

    /**
     * 分页查询-- 根据状态查询评论列表信息
     * @param statusList
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    List<OrderCommentDTO> getCommentListByStatus(List<Integer> statusList, Long shopId, int page, int size);
}
