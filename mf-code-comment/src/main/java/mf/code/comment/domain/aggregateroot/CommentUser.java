package mf.code.comment.domain.aggregateroot;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import mf.code.comment.CommentRoleEnum;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.model.request.CommentRequest;
import mf.code.comment.repo.po.Comment;
import mf.code.common.DelEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.utils.DateUtil;
import mf.code.user.dto.UserResp;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.comment.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 11:07
 */
@Data
public class CommentUser {
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户信息
     */
    private UserResp userInfo;
    /**
     * orderId
     */
    private Long orderId;
    /**
     * 订单评价情况 -- 通过唯一标识orderId
     * tips:
     * 首评论 parentId为0, status 包含 -1初始值,0买家已评论，1审核通过，2不通过，按ctime顺序
     * 平台审核 -- 无新增记录， 只更新 首评论状态status
     */
    private List<Comment> commentList;
    /**
     * 分页处理 待评价商品信息 -- 主要用于小程序 待评价订单列表 信息展示
     * 用户在店铺的 所有待评价历史记录基础上 获取待评价记录的商品信息
     *
     * 通过已获取的 pendingCommentPage 关联外键 获取 pendingGoodsPage
     */
    private AppletMybatisPageDto pendingGoodsPage;

    /**
     * 提交评价记录
     *
     * @param commentRequest
     */
    public void submitComment(CommentRequest commentRequest) {
        String contents = commentRequest.getContents();
        String pics = commentRequest.getPics();
        List<String> picList = new ArrayList<>();
        if (!StringUtils.isEmpty(pics)) {
            String[] split = pics.split(",");
            Collections.addAll(picList, split);
        }

        BigDecimal desStar = new BigDecimal(commentRequest.getDesStar());
        BigDecimal logisticsStar = new BigDecimal(commentRequest.getLogisticsStar());
        BigDecimal serviceStar = new BigDecimal(commentRequest.getServiceStar());
        Map<String, Object> starMap = new HashMap<>();
        starMap.put("desStar", desStar);
        starMap.put("logisticsStar", logisticsStar);
        starMap.put("serviceStar", serviceStar);
        // 计算综合评分
        BigDecimal comprehensiveScore = this.calculateComprehensiveScore(desStar, logisticsStar, serviceStar);

        Date now = new Date();
        if (StringUtils.isEmpty(contents)) {
            if (comprehensiveScore.compareTo(new BigDecimal("5")) == 0) {
                contents = "该用户觉得商品很好，给出了5星好评";
            } else {
                contents = "该用户觉得商品较好";
            }
        }

        if (CollectionUtils.isEmpty(this.commentList)) {
            this.commentList = new ArrayList<>();
            Comment comment = new Comment();
            comment.setParentId(0L);
            comment.setMerchantId(Long.valueOf(commentRequest.getMerchantId()));
            comment.setShopId(Long.valueOf(commentRequest.getShopId()));
            comment.setOrderId(Long.valueOf(commentRequest.getOrderId()));
            comment.setOrderNo(commentRequest.getOrderNO());
            comment.setUserId(Long.valueOf(commentRequest.getUserId()));
            comment.setGoodsParentId(commentRequest.getProductParentId());
            comment.setGoodsId(Long.valueOf(commentRequest.getGoodsId()));
            comment.setSkuId(Long.valueOf(commentRequest.getSkuId()));
            comment.setStatus(CommentStatusEnum.COMMENT.getCode());
            comment.setContents(contents);
            if (!CollectionUtils.isEmpty(picList)) {
                comment.setPics(JSON.toJSONString(picList));
            }
            comment.setTotalStars(comprehensiveScore);
            comment.setCommentStarsDetail(JSON.toJSONString(starMap));
            comment.setSubmitTime(now);
            comment.setRole(CommentRoleEnum.USER.getCode());
            comment.setUtime(now);
            comment.setCtime(now);
            comment.setDel(DelEnum.NO.getCode());
            this.commentList.add(comment);
        } else {
            List<Comment> comments = new ArrayList<>();
            Comment comment = new Comment();
            for (Comment comm : this.commentList) {
                if (comm.getParentId() == 0) {
                    comment = comm;
                    break;
                }
            }
            comment.setStatus(CommentStatusEnum.COMMENT.getCode());
            comment.setContents(contents);
            if (!CollectionUtils.isEmpty(picList)) {
                comment.setPics(JSON.toJSONString(picList));
            }
            comment.setTotalStars(comprehensiveScore);
            comment.setCommentStarsDetail(JSON.toJSONString(starMap));
            comment.setSubmitTime(now);
            comment.setRole(CommentRoleEnum.USER.getCode());
            comment.setUtime(now);
            comments.add(comment);
            this.commentList = comments;
        }
    }

    /**
     * 计算综合评分
     * 综合星星评价=商品描述星星*50%+物流服务*25%+服务态度*25%
     * 不足一颗星时，小数位：(0, 0.5] = 0.5 (0.5, 1) = 1
     *
     * @param desStar 描述相符
     * @param logisticsStar 物流服务
     * @param serviceStar 态度服务
     * @return
     */
    private BigDecimal calculateComprehensiveScore(BigDecimal desStar, BigDecimal logisticsStar, BigDecimal serviceStar) {

        BigDecimal percent = new BigDecimal("100");

        BigDecimal desScore = desStar.multiply(new BigDecimal("50"));
        BigDecimal logisticsScore = logisticsStar.multiply(new BigDecimal("25"));
        BigDecimal serviceScore = serviceStar.multiply(new BigDecimal("25"));

        BigDecimal comprehensiveScore = (desScore.add(logisticsScore).add(serviceScore))
                .divide(percent, 2, BigDecimal.ROUND_CEILING);

        // 处理小数点部分
        String[] split = comprehensiveScore.toString().split("\\.");
        if (split.length < 2) {
            return comprehensiveScore;
        } else {
            String point = split[1];
            int pointScore = Integer.parseInt(point);
            if (pointScore > 0 && pointScore <= 50) {
                return comprehensiveScore.setScale(0, BigDecimal.ROUND_DOWN).add(new BigDecimal("0.5"));
            } else {
                return comprehensiveScore.setScale(0, BigDecimal.ROUND_CEILING);
            }
        }
    }

    /**
     * 查看 订单评价详情
     * @return
     */
    public List<Map<String, Object>> queryOrderCommentInfo() {

        if (CollectionUtils.isEmpty(this.commentList)) {
            // 订单已取消，或评价订单未生成
            return null;
        }
        if (!this.checkHaveCommented()) {
            // 用户未评价
            return null;
        }
        List<Map<String, Object>> orderCommentList = new ArrayList<>();
        for (Comment comment: this.commentList) {
            Map<String, Object> orderComment = new HashMap<>();
            orderComment.put("orderId", comment.getOrderId());
            orderComment.put("orderNO", comment.getOrderNo());
            orderComment.put("submitTime", DateUtil.dateToString(comment.getSubmitTime(), DateUtil.POINT_DATE_FORMAT_THREE));
            orderComment.put("userId", userId);
            orderComment.put("totalStars", comment.getTotalStars());
            orderComment.put("contents", comment.getContents());
            orderComment.put("reason", comment.getReason());
            orderComment.put("id", comment.getId());
            orderComment.put("parentId", comment.getParentId());
            orderComment.put("pics", comment.getPics() == null ? new ArrayList<>() : JSON.parseArray(comment.getPics()));
            orderComment.put("role", comment.getRole());
            orderComment.put("status", comment.getStatus());
            orderCommentList.add(orderComment);
        }
        return orderCommentList;
    }

    /**
     * 校验是否可评价
     * @return true 用户已评价, false 可提交/用户未评价
     */
    public boolean checkHaveCommented() {
        if (CollectionUtils.isEmpty(this.commentList)) {
            return false;
        }
        for (Comment comment : this.commentList) {
            if (comment.getParentId() == 0 && comment.getStatus() == CommentStatusEnum.INIT.getCode()) {
                // 用户未评价
                return false;
            }
        }
        return true;
    }
}
