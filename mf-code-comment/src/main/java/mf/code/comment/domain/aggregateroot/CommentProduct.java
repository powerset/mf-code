package mf.code.comment.domain.aggregateroot;

import lombok.Data;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.goods.dto.ProductEntity;

/**
 * mf.code.comment.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 14:17
 */
@Data
public class CommentProduct {
    /**
     * 商品id
     */
    private Long productId;
    /**
     * 商品信息
     */
    private ProductEntity productInfo;
    /**
     * 分页处理 商品评价历史记录
     */
    private AppletMybatisPageDto productCommentPage;
}
