package mf.code.comment.domain.repository;

import mf.code.comment.repo.po.Complain;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 抖带带提建议
 * create by qc on 2019/8/21 0021
 */
public interface ComplainRepository extends MongoRepository<Complain, String> {
}
