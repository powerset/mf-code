package mf.code.comment.domain.repository.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.api.feignclient.GoodsAppService;
import mf.code.comment.api.feignclient.UserAppService;
import mf.code.comment.domain.aggregateroot.CommentProduct;
import mf.code.comment.domain.repository.CommentProductRepository;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.repo.repository.CommentService;
import mf.code.common.DelEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.ProductEntity;
import mf.code.user.dto.UserResp;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.comment.domain.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 22:13
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CommentProductRepositoryImpl implements CommentProductRepository {
    private final GoodsAppService goodsAppService;
    private final CommentService commentService;
    private final UserAppService userAppService;

    /**
     * 通过商品id 获取 评价商品信息
     *
     * @param productId
     * @return 完成 productId， ProductInfo 信息的装配
     */
    @Override
    public CommentProduct findByProductId(Long productId) {
        List<Long> productIdList = new ArrayList<>();
        productIdList.add(productId);
        List<ProductEntity> productEntities = goodsAppService.listByIds(productIdList);
        if (CollectionUtils.isEmpty(productEntities)) {
            log.error("商品服务异常, productId = {}", productId);
            return null;
        }
        ProductEntity productEntity = productEntities.get(0);
        CommentProduct commentProduct = new CommentProduct();
        commentProduct.setProductId(productEntity.getId());
        commentProduct.setProductInfo(productEntity);
        return commentProduct;
    }

    /**
     * 增装 商品评价信息
     *
     * @param commentProduct
     * @param offset
     * @param size           完成 productCommentPage 信息的装配
     */
    @Override
    public void addProductCommentPage(CommentProduct commentProduct, Long offset, Long size, boolean goodsParentId) {
        Long productId = commentProduct.getProductId();
        // 获取商品的所有评价
        Page<Comment> page = new Page<>(offset / size + 1, size);
        page.setDesc("submit_time");

        // 获取用户评价
        QueryWrapper<Comment> parentWrapper = new QueryWrapper<>();
        parentWrapper.lambda()
                .eq(Comment::getParentId, 0L)
                .eq(Comment::getGoodsId, productId)
                .eq(Comment::getStatus, CommentStatusEnum.PASS.getCode())
                .eq(Comment::getDel, DelEnum.NO.getCode());
        IPage<Comment> commentIPage = commentService.page(page, parentWrapper);
        if(!goodsParentId){
            parentWrapper.and(params -> params.in("goods_id", productId));
        }else {
            parentWrapper.and(params -> params.in("goods_parent_id", productId));
        }

        AppletMybatisPageDto<Map<String, Object>> productCommentPage = new AppletMybatisPageDto<>(size, offset);
        if (commentIPage == null || CollectionUtils.isEmpty(commentIPage.getRecords())) {
            // 没有评价信息
            commentProduct.setProductCommentPage(productCommentPage);
            return;
        }
        long total = commentIPage.getTotal();
        productCommentPage.setTotal(total);

        // 按orderId进行分组，组内以submit_time进行顺序
        List<Long> parentIdList = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        for (Comment comment : commentIPage.getRecords()) {
            parentIdList.add(comment.getId());
            userIds.add(comment.getUserId());
        }
        if (CollectionUtils.isEmpty(parentIdList) || CollectionUtils.isEmpty(userIds)) {
            log.error("comment 数据异常");
            return;
        }
        // 获取 评论回复
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Comment::getParentId, parentIdList)
                .eq(Comment::getStatus, CommentStatusEnum.PASS.getCode())
                .eq(Comment::getDel, DelEnum.NO.getCode());
        wrapper.orderByAsc("submit_time");
        List<Comment> commentList = commentService.list(wrapper);
        Map<Long, List<Comment>> orderCommentListMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(commentList)) {
            for (Comment comment : commentList) {
                Long orderId = comment.getOrderId();
                List<Comment> subCommentList = orderCommentListMap.get(orderId);
                if (CollectionUtils.isEmpty(subCommentList)) {
                    subCommentList = new ArrayList<>();
                }
                subCommentList.add(comment);
                orderCommentListMap.put(comment.getOrderId(), subCommentList);
            }
        }
        // 获取用户信息
        List<UserResp> userResps = userAppService.listByIds(userIds);
        Map<Long, UserResp> userRespMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userResps)) {
            for (UserResp userResp : userResps) {
                userRespMap.put(userResp.getId(), userResp);
            }
        }
        // 处理页面返回
        List<Map<String, Object>> commentMapList = new ArrayList<>();
        for (Comment comment : commentIPage.getRecords()) {
            Map<String, Object> commentMap = new HashMap<>();
            Long orderId = comment.getOrderId();
            Long userId = comment.getUserId();
            UserResp userResp = userRespMap.get(userId);
            if (userResp == null) {
                continue;
            }
            commentMap.put("avatarUrl", userResp.getAvatarUrl());
            commentMap.put("nickName", userResp.getNickName());
            commentMap.put("totalStars", comment.getTotalStars());
            commentMap.put("contents", comment.getContents());
            commentMap.put("submitTime", DateUtil.dateToString(comment.getSubmitTime(), DateUtil.FORMAT_TWO));
            commentMap.put("pics", comment.getPics() == null ? new ArrayList<>() : JSON.parseArray(comment.getPics()));
            commentMap.put("role", comment.getRole());
            List<Comment> comments = orderCommentListMap.get(orderId);
            if (!CollectionUtils.isEmpty(comments)) {
                List<Map<String, Object>> answerList = new ArrayList<>();
                for (Comment answer : comments) {
                    Map<String, Object> answerMap = new HashMap<>();
                    answerMap.put("role", answer.getRole());
                    answerMap.put("contents", answer.getContents());
                    answerList.add(answerMap);
                }
                commentMap.put("answerList", answerList);
            }
            commentMapList.add(commentMap);
        }
        productCommentPage.setContent(commentMapList);

        if (offset + size < total) {
            productCommentPage.setPullDown(true);
        }
        commentProduct.setProductCommentPage(productCommentPage);
    }
}
