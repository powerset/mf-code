package mf.code.comment.domain.repository;

import mf.code.comment.domain.aggregateroot.CommentProduct;

/**
 * mf.code.comment.domain.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 22:13
 */
public interface CommentProductRepository {
    /**
     * 通过商品id 获取 评价商品信息
     *
     * @param productId
     * @return 完成 productId， ProductInfo 信息的装配
     */
    CommentProduct findByProductId(Long productId);

    /**
     * 增装 商品评价信息
     *
     * @param commentProduct
     * @param offset
     * @param size           完成 productCommentPage 信息的装配
     */
    void addProductCommentPage(CommentProduct commentProduct, Long offset, Long size, boolean goodsParentId);
}
