package mf.code.comment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.api.feignclient.GoodsAppService;
import mf.code.comment.api.feignclient.OrderAppService;
import mf.code.comment.repo.dao.CommentMapper;
import mf.code.comment.repo.dto.CommentDTO;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.service.PlateCommentApiService;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.ProductEntity;
import mf.code.order.dto.OrderResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * des：平台--评论service
 * author:yunshan
 * time:2019/07/30
 */
@Service
@Slf4j
public class PlateCommentApiServiceImpl implements PlateCommentApiService {

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private GoodsAppService goodsAppService;

    @Autowired
    private OrderAppService orderAppService;

    /**
     * 已审核
     */
    private static final int CHECKED = 2;

    /**
     * 待审核
     */
    private static final int UNCHECKED = 1;

    /**
     * 已审核--全部包含审核通过和不通过
     */
    private static final int IS_CHEHCK_PASS_ALL = 0;

    /**
     * 已审核--通过
     */
    private static final int IS_CHECK_PASS_YES = 1;

    /**
     * 已审核--未通过
     */
    private static final int IS_CHECK_PASS_NO = 2;

    /**
     * 审核通过1
     */
    private static final int IS_PASS_YES = 1;

    /**
     * 审核不通过2
     */
    private static final int IS_PASS_NO = 2;


    /**
     * 平台--根据查询条件获取评论列表
     *
     * @param type 1待审核，2已审核 ，默认是待审核
     * @param status
     * @param startTime
     * @param endTime
     * @param goodsId
     * @param orderNo
     * @param page
     * @param size
     * @return
     */
    @Override
    public SimpleResponse getCheckList(Integer type, Integer status, String startTime, String endTime, Long goodsId, String orderNo, Long page, Long size) {

        //初始化数据
        int count = 0;
        List<CommentDTO> commentList = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("commentList", commentList);
        map.put("count", count);
        map.put("page", page);
        map.put("size", size);
        map.put("unCheckEdCount",0);

        //检验分页参数
        if (page <= 0) {
            page = 1L;
        }
        if (size <= 0) {
            size = 10L;
        }

        //封装查询状态值
        List<Integer> statusList = new ArrayList<>();
        //待审核
        if (type == UNCHECKED) {
            statusList.add(CommentStatusEnum.COMMENT.getCode());
        } else if (type == CHECKED) {
            //已审核
            //已审核--全部包含审核通过和未通过
            if (status == IS_CHEHCK_PASS_ALL) {
                statusList.add(CommentStatusEnum.FAIL.getCode());
                statusList.add(CommentStatusEnum.PASS.getCode());
            } else if (status == IS_CHECK_PASS_YES) {
                //已审核--通过
                statusList.add(CommentStatusEnum.PASS.getCode());
            } else if (status == IS_CHECK_PASS_NO) {
                //已审核--未通过
                statusList.add(CommentStatusEnum.FAIL.getCode());
            } else {
                return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_VALID, map);
            }

        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_VALID, map);
        }

        //封装查询条件
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
            Date begin = DateUtil.stringtoDate(startTime, DateUtil.FORMAT_ONE);
            Date end = DateUtil.stringtoDate(endTime, DateUtil.FORMAT_ONE);
            if (begin.after(end)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_VALID, map);
            }

            queryWrapper.lambda().between(Comment::getSubmitTime, begin, end);
        }

        if (null != goodsId) {
            queryWrapper.lambda().eq(Comment::getGoodsId, goodsId);
        }


        if (StringUtils.isNotBlank(orderNo)) {
            queryWrapper.lambda().eq(Comment::getOrderNo, orderNo);
        }

        queryWrapper.lambda().in(Comment::getStatus, statusList);
        queryWrapper.lambda().eq(Comment::getParentId, 0L);
        queryWrapper.lambda().eq(Comment::getDel, DelEnum.NO.getCode());

        //初始化分页信息
        Page<Comment> pages = new Page<>(page, size);
        pages.setDesc("submit_time");

        IPage<Comment> commentIPage = commentMapper.selectPage(pages, queryWrapper);

        //获取分页信息
        List<Comment> cmList = commentIPage.getRecords();
        if (CollectionUtils.isEmpty(cmList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS,map);
        }

        List<CommentDTO> cList = new ArrayList<>();
        for (Comment c : cmList) {

            CommentDTO dto = new CommentDTO();
            Long pId = c.getGoodsId();
            List<Long> pidList = new ArrayList<>();
            pidList.add(pId);

            List<ProductEntity> productEntityList = goodsAppService.listByIds(pidList);
            if (CollectionUtils.isEmpty(productEntityList)) {
                continue;
            }

            String goodsName = productEntityList.get(0).getProductTitle();
            String goodsPic = productEntityList.get(0).getMainPics();

            OrderResp orderResp = orderAppService.queryOrder(c.getOrderId());
            if(null== orderResp){
                continue;
            }

            //复制bean
            BeanUtils.copyProperties(c, dto);

            dto.setGoodsPic(goodsPic);
            dto.setGoodsName(goodsName);
            dto.setOrderNo(orderResp.getOrderNo());

            cList.add(dto);
        }

        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Comment::getParentId, 0L)
                .eq(Comment::getStatus, CommentStatusEnum.COMMENT.getCode())
                .eq(Comment::getDel, DelEnum.NO.getCode());
        Integer unCheckEdCount = commentMapper.selectCount(wrapper);

        map.put("commentList", cList);
        map.put("count", commentIPage.getTotal());
        map.put("unCheckEdCount",unCheckEdCount);


        return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
    }


    /**
     * 根据查询条件获取评论列表的总条数
     *
     * @param type
     * @param status
     * @param startTime
     * @param endTime
     * @param goodsId
     * @param orderNo
     * @return
     */
    private int getCheckListNum(Integer type, Integer status, String startTime, String endTime, Long goodsId, Long orderNo) {
        //封装查询状态值
        List<Integer> statusList = new ArrayList<>();
        //待审核
        if (type == UNCHECKED) {
            statusList.add(CommentStatusEnum.COMMENT.getCode());
        }
        else if (type == CHECKED) {
            //已审核
            //已审核--全部包含审核通过和未通过
            if (status == IS_CHEHCK_PASS_ALL) {
                statusList.add(CommentStatusEnum.FAIL.getCode());
                statusList.add(CommentStatusEnum.PASS.getCode());
            } else if (status == IS_CHECK_PASS_YES) {
                //已审核--通过
                statusList.add(CommentStatusEnum.PASS.getCode());
            } else if (status == IS_CHECK_PASS_NO) {
                //已审核--未通过
                statusList.add(CommentStatusEnum.FAIL.getCode());
            } else {
                return 0;
            }

        } else {
            return 0;
        }

        //封装查询条件
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
            Date begin = DateUtil.stringtoDate(startTime, DateUtil.POINT_DATE_FORMAT_TWO);
            Date end = DateUtil.stringtoDate(endTime, DateUtil.POINT_DATE_FORMAT_TWO);
            if (begin.after(end)) {
                return 0;
            }
            queryWrapper.lambda().between(Comment::getCtime, startTime, endTime);
        }

        if (null != goodsId) {
            queryWrapper.lambda().eq(Comment::getGoodsId, goodsId);
        }

        if (null != orderNo) {
            OrderResp orderResp = orderAppService.queryOrderByOrderNo(orderNo);
            if(null== orderResp){
                return 0;
            }
            queryWrapper.lambda().eq(Comment::getOrderId, orderResp.getId());
        }

        queryWrapper.lambda().in(Comment::getStatus, statusList)
                .eq(Comment::getDel, DelEnum.NO.getCode());

        //过滤有查询不到产品信息的数据，保证总数的正确性。
        List<Comment> commentList = commentMapper.selectList(queryWrapper);
        List<Comment> cList = new ArrayList<>();
        for (Comment c : commentList) {
            Long productId = c.getGoodsId();
            List<Long> pidList = new ArrayList<>();
            pidList.add(productId);

            List<ProductEntity> productEntityList = goodsAppService.listByIds(pidList);
            if (CollectionUtils.isEmpty(productEntityList)) {
                continue;
            }

            OrderResp order = orderAppService.queryOrder(c.getOrderId());
            if (null == order) {
                continue;
            }
            cList.add(c);
        }

        if (CollectionUtils.isEmpty(cList)) {
            return 0;
        }

        return cList.size();
    }

    /**
     * 是否审核通过
     *
     * @param type 1通过，2不通过
     * @param id
     * @param reason
     * @return
     */
    @Override
    public SimpleResponse isPass(Integer type, Long id, String reason) {

        //审核不通过，需要填写理由
        if (type == IS_PASS_NO && StringUtils.isEmpty(reason)) {

            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
        }

        //审核不通过，填写理由不能超过30字
        if (type == IS_PASS_NO && !StringUtils.isEmpty(reason) && reason.length() > 30) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_LENGTH);
        }

        Comment comment = commentMapper.selectById(id);
        if (null == comment) {
            log.debug(">>>>>>>>>>>>根据评论id获取评论信息失败，id=" + id);
            return new SimpleResponse(ApiStatusEnum.ERROR_DB);
        }

        comment.setUtime(new Date());

        if (type == IS_PASS_YES) {
            comment.setStatus(CommentStatusEnum.PASS.getCode());
        } else if (type == IS_PASS_NO) {
            comment.setStatus(CommentStatusEnum.FAIL.getCode());
            comment.setReason(reason);
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
        }

        int count= commentMapper.updateById(comment);
        if (count <=0){
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        return new SimpleResponse();
    }


}
