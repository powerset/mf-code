package mf.code.comment.service;

import mf.code.common.simpleresp.SimpleResponse;

public interface PlateCommentApiService {

    /**
     * 平台--根据查询条件获取审核列表
     * @param type
     * @param status
     * @param startTime
     * @param endTime
     * @param goodsId
     * @param orderNo
     * @param page
     * @param size
     * @return
     */
    SimpleResponse getCheckList(Integer type, Integer status, String startTime, String endTime, Long goodsId, String orderNo, Long page, Long size);


    /**
     * 评论是否通过
     * @param type
     * @param id
     * @param reason
     * @return
     */
    SimpleResponse isPass(Integer type, Long id, String reason);
}
