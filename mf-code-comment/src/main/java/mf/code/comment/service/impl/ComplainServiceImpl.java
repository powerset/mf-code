package mf.code.comment.service.impl;/**
 * create by qc on 2019/8/21 0021
 */

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.domain.repository.ComplainRepository;
import mf.code.comment.repo.dto.ComplainDto;
import mf.code.comment.repo.po.Complain;
import mf.code.comment.service.ComplainService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author gbf
 * 2019/8/21 0021、09:12
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ComplainServiceImpl implements ComplainService {
    private final ComplainRepository complainRepository;

    /**
     * 上报意见
     *
     * @param complainDto
     * @return
     */
    @Override
    public SimpleResponse report(ComplainDto complainDto) {
        Complain complain = new Complain();
        complain.setUid(complainDto.getUserId());
        complain.setContent(complainDto.getContent());
        complain.setPhone(complainDto.getPhone());
        complain.setVersion("v1.0");
        Date now = new Date();
        complain.setCtime(now);
        complain.setCurrent(now.getTime());
        complain.setProject(complainDto.getProject());
        complain = complainRepository.insert(complain);
        if (StringUtils.isBlank(complain.getId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }
}
