package mf.code.comment.service;

import mf.code.comment.model.request.CommentRequest;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.comment.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 16:34
 */
public interface AppletCommentService {
    /**
     * 待评价  商品列表 页
     * @param userId
     * @param merchantId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse queryPendingGoodsPage(Long userId, Long merchantId, Long shopId, Long offset, Long size);

    /**
     * 查询 某订单的评价详情
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse queryOrderCommentInfo(Long userId, Long orderId);

    /**
     * 提交评价
     * @param commentRequest
     * @return
     */
    SimpleResponse submitComment(CommentRequest commentRequest);

    /**
     * 获取 商品评价 页
     * @param goodsId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse queryProductCommentPage(Long goodsId, Long shopId, Long offset, Long size);
}
