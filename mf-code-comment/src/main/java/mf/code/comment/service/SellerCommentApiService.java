package mf.code.comment.service;

import mf.code.common.simpleresp.SimpleResponse;

public interface SellerCommentApiService {
    /**
     * 商户端-根据订单查询评价信息
     *
     * @param orderId
     * @return
     */
    SimpleResponse checkComments(Long orderId, Long shopId);

    /**
     * 商家回复
     *
     * @param merchantId
     * @param contents
     * @param commentId
     * @param shopId
     * @return
     */
    SimpleResponse sellerReplyComment(Long merchantId, String contents, Long commentId, Long shopId);
}
