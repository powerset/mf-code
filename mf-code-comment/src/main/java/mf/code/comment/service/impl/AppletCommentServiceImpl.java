package mf.code.comment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.api.feignclient.GoodsAppService;
import mf.code.comment.domain.aggregateroot.CommentProduct;
import mf.code.comment.domain.aggregateroot.CommentUser;
import mf.code.comment.domain.repository.CommentProductRepository;
import mf.code.comment.domain.repository.CommentUserRepository;
import mf.code.comment.model.request.CommentRequest;
import mf.code.comment.service.AppletCommentService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * mf.code.comment.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 16:34
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AppletCommentServiceImpl implements AppletCommentService {
    private final CommentUserRepository commentUserRepository;
    private final CommentProductRepository commentProductRepository;
    private final GoodsAppService goodsAppService;

    /**
     * 待评价  商品列表 页
     *
     * @param userId
     * @param merchantId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryPendingGoodsPage(Long userId, Long merchantId, Long shopId, Long offset, Long size) {
        CommentUser commentUser = commentUserRepository.findByUserId(userId);
        if (commentUser == null) {
            log.error("获取用户信息异常, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候再试");
        }
        commentUserRepository.addPendingGoodsPage(commentUser, merchantId, shopId, offset, size);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, commentUser.getPendingGoodsPage());
    }

    /**
     * 查询 某订单的评价详情
     *
     * @param userId
     * @param orderId
     * @return
     */
    @Override
    public SimpleResponse queryOrderCommentInfo(Long userId, Long orderId) {
        CommentUser commentUser = commentUserRepository.findByUserId(userId);
        if (commentUser == null) {
            log.error("获取用户信息异常, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候再试");
        }
        commentUserRepository.addOrderComment(commentUser, orderId);

        List<Map<String, Object>> orderCommentInfo = commentUser.queryOrderCommentInfo();
        if (CollectionUtils.isEmpty(orderCommentInfo)) {
            log.error("此订单没有评价信息");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "用户未评价");
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS, orderCommentInfo);
    }

    /**
     * 提交评价
     *
     * @param commentRequest
     * @return
     */
    @Override
    public SimpleResponse submitComment(CommentRequest commentRequest) {
        Long userId = Long.valueOf(commentRequest.getUserId());
        Long orderId = Long.valueOf(commentRequest.getOrderId());
        CommentUser commentUser = commentUserRepository.findByUserId(userId);
        if (commentUser == null) {
            log.error("获取用户信息异常, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候再试");
        }
        commentUserRepository.addOrderComment(commentUser, orderId);
        // 校验是否已评价
        if (commentUser.checkHaveCommented()) {
            log.error("用户已评价, userId = {}, orderId = {}", userId, orderId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "已评价, 请勿重复提交");
        }
        if (CollectionUtils.isEmpty(commentUser.getCommentList())) {
            // 获取商品信息
            List<Long> productIdList = new ArrayList<>();
            productIdList.add(Long.valueOf(commentRequest.getGoodsId()));
            List<ProductEntity> productEntities = goodsAppService.listByIds(productIdList);
            if (CollectionUtils.isEmpty(productEntities)) {
                log.error("获取商品信息异常, goodsId = {}", commentRequest.getGoodsId());
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "网络异常，请稍候再试");
            }
            commentRequest.setProductParentId(productEntities.get(0).getParentId());
        }
        commentUser.submitComment(commentRequest);
        boolean save = commentUserRepository.saveComment(commentUser);
        if (!save) {
            log.error("数据库异常, 保存失败, commentUser = {}", commentUser);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "网络异常，请稍候再试");
        }
        return new SimpleResponse();
    }

    /**
     * 获取 商品评价 页
     *
     * @param goodsId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryProductCommentPage(Long goodsId, Long shopId, Long offset, Long size) {
        CommentProduct commentProduct = commentProductRepository.findByProductId(goodsId);
        commentProduct.setProductId(commentProduct.getProductInfo().getParentId());
        if (commentProduct == null) {
            log.error("获取商品信息异常, goodsId = {}", goodsId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候再试");
        }
        commentProductRepository.addProductCommentPage(commentProduct, offset, size, true);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, commentProduct.getProductCommentPage());
    }
}
