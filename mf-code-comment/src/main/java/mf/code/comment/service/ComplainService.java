package mf.code.comment.service;

import mf.code.comment.repo.dto.ComplainDto;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * create by qc on 2019/8/21 0021
 */
public interface ComplainService {
    /**
     * 上报意见
     *
     * @param complainDto
     * @return
     */
    SimpleResponse report(ComplainDto complainDto);
}
