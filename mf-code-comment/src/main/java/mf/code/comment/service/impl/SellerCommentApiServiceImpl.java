package mf.code.comment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentRoleEnum;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.CommentStepEnum;
import mf.code.comment.api.feignclient.OrderAppService;
import mf.code.comment.repo.dao.CommentMapper;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.service.SellerCommentApiService;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.OrderResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
public class SellerCommentApiServiceImpl implements SellerCommentApiService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private OrderAppService orderAppService;

    /**
     * 根据订单查询评论信息
     *
     * @param orderId
     * @return
     */
    @Override
    public SimpleResponse checkComments(Long orderId, Long shopId) {
        List<Comment> clist = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("commentList", clist);
        if (null == orderId) {
            log.debug(">>>>>>>>>>> checkComments 缺少必要参数");
            return new SimpleResponse(ApiStatusEnum.ERROR_DB, map);
        }
        //根据orderId查询评论表
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Comment::getOrderId, orderId)
                .eq(Comment::getDel, DelEnum.NO.getCode())
                .orderByAsc(Comment::getSubmitTime);

        clist = commentMapper.selectList(queryWrapper);

        if (CollectionUtils.isEmpty(clist)) {
            log.debug(">>>>>>>>>>> checkComments 根据orderId获取评论信息为空，orderId=" + orderId);
            map.put("commentStep", CommentStepEnum.NOT_FINISH.getCode());
            return new SimpleResponse(ApiStatusEnum.ERROR_DB, map);
        }

        OrderResp orderResp = orderAppService.queryOrder(orderId);
        if (orderResp == null) {
            map.put("commentStep", CommentStepEnum.NOT_FINISH.getCode());
            return new SimpleResponse(ApiStatusEnum.ERROR_DB, map);
        }

        //根据评论条数判断回复状态
        if (clist.size() > 1) {
            //商户已回复
            map.put("commentList", clist);
            map.put("commentStep", CommentStepEnum.SELLER_REPLY.getCode());
        } else if (clist.size() == 1) {
            Comment comment = clist.get(0);
            //直销商户-回复评价
            if (shopId.equals(orderResp.getGoodsShopId())) {
                //获取回复状态值
                int status = comment.getStatus();
                if (status == CommentStatusEnum.PASS.getCode()) {
                    //买家已经评论，并且审核通过
                    map.put("commentList", clist);
                    map.put("commentStep", CommentStepEnum.USER_REPLY.getCode());

                } else if (status == CommentStatusEnum.INIT.getCode() || status == CommentStatusEnum.COMMENT.getCode()) {
                    //用户未评论
                    map.put("commentList", clist);
                    map.put("commentStep", CommentStepEnum.USER_UNREPLY.getCode());

                } else if (status == CommentStatusEnum.FAIL.getCode()) {
                    //禁止评论
                    map.put("commentList", clist);
                    map.put("commentStep", CommentStepEnum.FORBID_COMMENT.getCode());
                }
            } else {
                //非直销商户-不可回复评价
                map.put("commentStep", CommentStepEnum.NOT_FINISH.getCode());
                return new SimpleResponse(ApiStatusEnum.ERROR_DB, map);
            }
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
    }

    /**
     * 商家端回复评论
     *
     * @param merchantId
     * @param contents
     * @param commentId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse sellerReplyComment(Long merchantId, String contents, Long commentId, Long shopId) {
        if (null == merchantId || null == commentId || null == contents || null == shopId) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
        }

        //商家端的回复不能超过30字
        if (contents.length() > 30) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_LENGTH);
        }

        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Comment::getId, commentId)
                .eq(Comment::getDel, DelEnum.NO.getCode());

        Comment c = commentMapper.selectOne(queryWrapper);

        if (null == c) {
            log.debug(">>>>获取用户品论信息失败,commentId=" + commentId);
            return new SimpleResponse(ApiStatusEnum.ERROR_DB);
        }

        OrderResp orderResp = orderAppService.queryOrder(c.getOrderId());
        if (orderResp == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "获取订单信息异常，请重试");
        }
        if (!shopId.equals(orderResp.getGoodsShopId())) {
            log.debug(">>>>本店铺下的评论才能回复,commentId=" + commentId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO26);
        }

        //需要修改的数据
        c.setParentId(commentId);
        c.setUtime(new Date());
        c.setContents(contents);
        c.setUserId(merchantId);
        c.setRole(CommentRoleEnum.SELLER.getCode());
        c.setTotalStars(new BigDecimal(0));
        c.setCommentStarsDetail("0");
        c.setReason(null);
        c.setPics(null);
        c.setDel(0);
        c.setSubmitTime(new Date());

        int count = commentMapper.insert(c);
        if (count <= 0) {
            log.debug(">>>>插入商家评论信息失败,commentId=" + commentId);
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }
}
