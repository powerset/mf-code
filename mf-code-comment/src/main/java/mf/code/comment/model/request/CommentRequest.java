package mf.code.comment.model.request;

import lombok.Data;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.comment.model.request
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 17:35
 */
@Data
public class CommentRequest {
    /**
     * 店铺id
     */
    private String shopId;
    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 订单id
     */
    private String orderId;
    /**
     * 订单编号
     */
    private String orderNO;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品skuid
     */
    private String skuId;
    /**
     * 图片信息url， 以,号分割
     */
    private String pics;
    /**
     * 评分--描述相符 最多5星
     */
    private String desStar;
    /**
     * 评分--物流态度 最多5星
     */
    private String logisticsStar;
    /**
     * 评分--服务评分 最多5星
     */
    private String serviceStar;
    /**
     * 评论内容 < 200 字
     */
    private String contents;

    /**
     * 非前端传入，而是服务内因传输需要，所添加的字段
     */
    private Long productParentId;

    /**
     * 参数校验
     * @return
     */
    public boolean checkParams() {
        if (StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(merchantId) || !RegexUtils.StringIsNumber(merchantId)
                || StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(orderId) || !RegexUtils.StringIsNumber(orderId)
                || StringUtils.isBlank(orderNO)
                || StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)
                || StringUtils.isBlank(skuId) || !RegexUtils.StringIsNumber(skuId)
                || StringUtils.isBlank(desStar) || !RegexUtils.StringIsNumber(desStar)
                || StringUtils.isBlank(logisticsStar) || !RegexUtils.StringIsNumber(logisticsStar)
                || StringUtils.isBlank(serviceStar) || !RegexUtils.StringIsNumber(serviceStar)
                || StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)) {
            return false;
        }
        if (StringUtils.isNotBlank(contents) && contents.length() > 200) {
            return false;
        }
        return pics.split(",").length <= 6;
    }
}
