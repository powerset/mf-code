package mf.code.comment.common.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.internal.connection.ServerAddressHelper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

/**
 * 带身份验证
 *
 * @author gbf
 * 2019/3/21 0021、17:02
 */
@Data
@Configuration
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${spring.data.mongodb.database}")
    private String database;
    @Value("${spring.data.mongodb.uri}")
    private String uri;

    @Override
    protected String getDatabaseName() {
        return database;
    }

    @Bean
    @Override
    public MongoDbFactory mongoDbFactory() {
        return new SimpleMongoDbFactory(mongoClient(), getDatabaseName());
    }

    @Bean
    @Override
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoDbFactory(), null);
    }

    @Bean
    @Override
    public MongoClient mongoClient() {
        return new MongoClient(new MongoClientURI(uri));
//        if ("prod".equals(active) ) {
//        } else {
//            return new MongoClient(ServerAddressHelper.createServerAddress(host, port),
//                    MongoCredential.createCredential(username, database, password.toCharArray()),
//                    MongoClientOptions.builder().build());
//        }
    }

}
