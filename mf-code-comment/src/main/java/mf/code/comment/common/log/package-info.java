/**
 * <pre>
 * MyLogSuffixDir
 * 定制 日志文件路径 与 server.port环境变量 有关联关系
 *   例如
 *     java命令行参数为
 *       -Dserver.port=8001
 *     日志文件路径为
 *       /home/code/logs/mf-code-one-8001/all/all-%d{yyyy-MM-dd}.log
 *       /home/code/logs/mf-code-one-8001/error/error-%d{yyyy-MM-dd}.log
 *
 * MyLogBaseDir
 * 定制 日志文件路径 与 开发环境 有关联关系
 *   例如
 *     macOS操作系统
 *     日志文件路径为
 *       /Users/{用户名}/logs/home/code/logs/mf-code-one-8001/all/all-%d{yyyy-MM-dd}.log
 *       /Users/{用户名}/logs/home/code/logs/mf-code-one-8001/error/error-%d{yyyy-MM-dd}.log
 * </pre>
 */
package mf.code.comment.common.log;
