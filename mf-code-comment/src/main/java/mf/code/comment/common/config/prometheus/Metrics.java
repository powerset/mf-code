package mf.code.comment.common.config.prometheus;

import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;

/**
 * mf.code.user.common.config.prometheus
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-26 15:24
 */
public class Metrics {
	public static final Counter counter = Counter.build()
			.name("api_request_total")
			.labelNames("path", "method")
			.help("http累计请求量")
			.register();

	public static final Gauge gauge = Gauge.build()
			.name("api_inprogress_requests")
			.labelNames("path", "method")
			.help("正在处理的http请求数")
			.register();

	// 默认的buckets范围为{.005, .01, .025, .05, .075, .1, .25, .5, .75, 1, 2.5, 5, 7.5, 10}
	public static final Histogram histogram = Histogram.build()
			.name("api_requests_latency_seconds_histogram")
			.labelNames("path", "method")
			.help("http请求所消耗时间")
			.register();
}
