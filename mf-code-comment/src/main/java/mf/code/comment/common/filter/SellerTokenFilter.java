package mf.code.comment.common.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * mf.code.common.filter
 * Description:
 *
 * @author: gel
 * @date: 2018-11-10 14:23
 */
@Slf4j
@Component
public class SellerTokenFilter extends HttpServlet implements Filter {
    private static final long serialVersionUID = 1L;

    private static String uidName;
    private static String debugMode;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        uidName = filterConfig.getInitParameter("uidName");
        debugMode = filterConfig.getInitParameter("debugMode");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        Object contentType = httpServletRequest.getContentType();
        if(contentType != null && !StringUtils.containsIgnoreCase(String.valueOf(contentType),"application/json")){
            chain.doFilter(request, response);
            return;
        }
        BodyReaderRequestWrapper requestWrapper = new BodyReaderRequestWrapper((HttpServletRequest)request);
        Object attribute = requestWrapper.getSession().getAttribute(uidName);
        if(attribute != null){
            requestWrapper.addParameter(uidName, attribute.toString());
        }
        // debug模式
        if(debugMode != null && debugMode.equals("1")) {
            Object header = requestWrapper.getHeader("debugToken");
            if (header != null) {
                requestWrapper.addParameter(uidName, header.toString());
            }
        }
        chain.doFilter(requestWrapper, response);
    }
}

