package mf.code.comment.common.redis;

/**
 * mf.code.user.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 00:13
 */
public class RedisKeyConstant {

	/**
	 * 提交防重的key
	 */
	public static final String SUBMIT_FORBID_REPEAT = "jkmf:comment:forbidrepeat:submit:";// <uid>:<oid>
}
