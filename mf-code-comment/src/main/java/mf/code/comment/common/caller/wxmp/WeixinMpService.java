package mf.code.comment.common.caller.wxmp;

import mf.code.comment.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.comment.common.caller.wxmp.vo.WxsendCustomerMsgVo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Map;

/**
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月25日 11:42
 */
public interface WeixinMpService {

    String getAccessToken(String appid, String appSecret);

    /**
     * 生成二维码并上传oss
     *
     * @param
     * @param sence 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
     * @param width
     * @return
     */
    String getWXACodeUnlimit(String appid, String appSecret, String sence, String path, Integer width, boolean isHyaline);

    /***
     * 生成圆形的二维码bufferImg
     * @param sence
     * @param path
     * @param width
     * @return
     */
    BufferedImage getRoundnessWXACodeUnlimit(String appid, String appSecret, String sence, String path, Integer width);

    /***
     * 发送模板消息
     * @param vo 发送模板
     * @return
     */
    Map<String, Object> sendTemplateMessage(String appid, String appSecret, WxSendMsgVo vo);

    /***
     * 把媒体文件上传到微信服务器。目前仅支持图片。用于发送客服消息或被动回复用户消息。
     * @return 媒体文件上传后，获取标识，3天内有效。
     */
    Map<String, Object> uploadTempMedia(File file, String type);

    /***
     * 发送客服消息给用户
     * @param vo 客服消息模板
     * @return
     */
    Map<String, Object> sendCustomerMessage(String appid, String appSecret, WxsendCustomerMsgVo vo);

    Map<String, Object> sendMessage(String url, String params);
}
