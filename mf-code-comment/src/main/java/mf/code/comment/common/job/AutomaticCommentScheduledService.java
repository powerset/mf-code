package mf.code.comment.common.job;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentRoleEnum;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.api.feignclient.OrderAppService;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.repo.repository.CommentService;
import mf.code.common.DelEnum;
import mf.code.common.utils.DateUtil;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderBizStatusEnum;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.comment.common.job
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-05 08:48
 */
@Component
@Slf4j
// @Profile(value = {"test2", "prod"})
public class AutomaticCommentScheduledService {
    @Autowired
    private CommentService commentService;
    @Autowired
    private OrderAppService orderAppService;

    @Scheduled(fixedDelay = 60000)
    public void comment7DaysAgo() {
        for (;;) {
            Page<Comment> page = new Page<>(1, 15);
            page.setAsc("ctime");

            QueryWrapper<Comment> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(Comment::getParentId, 0L)
                    .eq(Comment::getStatus, CommentStatusEnum.INIT.getCode())
                    .lt(Comment::getCtime, DateUtils.addDays(new Date(), -7))
                    .eq(Comment::getRole, CommentRoleEnum.USER.getCode())
                    .eq(Comment::getDel, DelEnum.NO.getCode());
            IPage<Comment> commentIPage = commentService.page(page, wrapper);
            if (commentIPage == null || CollectionUtils.isEmpty(commentIPage.getRecords())) {
                log.info("已无需要自动评价的订单");
                break;
            }
            List<Long> orderIdList = new ArrayList<>();
            for (Comment comment : commentIPage.getRecords()) {
                orderIdList.add(comment.getOrderId());
            }
            List<OrderResp> orderRespList = orderAppService.listByIds(orderIdList);
            if (CollectionUtils.isEmpty(orderRespList)) {
                log.error("order服务异常");
                break;
            }
            Map<Long, OrderResp> orderRespMap = new HashMap<>();
            for (OrderResp orderResp : orderRespList) {
                orderRespMap.put(orderResp.getId(), orderResp);
            }
            Date now = new Date();
            List<Comment> updateCommentList = new ArrayList<>();
            for (Comment comment : commentIPage.getRecords()) {
                OrderResp orderResp = orderRespMap.get(comment.getOrderId());
                if (orderResp == null || orderResp.getBizStatus() == OrderBizStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode()) {
                    // 删除逻辑
                    comment.setDel(DelEnum.YES.getCode());
                    comment.setUtime(now);
                    updateCommentList.add(comment);
                } else {
                    Map<String, Object> starMap = new HashMap<>();
                    starMap.put("desStar", 5);
                    starMap.put("logisticsStar", 5);
                    starMap.put("serviceStar", 5);
                    // 计算综合评分
                    BigDecimal comprehensiveScore = new BigDecimal("5").setScale(1, BigDecimal.ROUND_DOWN);
                    comment.setContents("该用户觉得商品很好，给出了5星好评");
                    comment.setStatus(CommentStatusEnum.PASS.getCode());
                    comment.setTotalStars(comprehensiveScore);
                    comment.setCommentStarsDetail(JSON.toJSONString(starMap));
                    comment.setSubmitTime(now);
                    comment.setUtime(now);
                    updateCommentList.add(comment);
                }
            }
            commentService.updateBatchById(updateCommentList);
        }
    }
}
