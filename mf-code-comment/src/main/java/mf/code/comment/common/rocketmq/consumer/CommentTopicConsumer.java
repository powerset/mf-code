package mf.code.comment.common.rocketmq.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentRoleEnum;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.api.feignclient.GoodsAppService;
import mf.code.comment.domain.aggregateroot.CommentUser;
import mf.code.comment.domain.repository.CommentUserRepository;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.repo.repository.CommentService;
import mf.code.common.DelEnum;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.exception.RocketMQException;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.dto.ProductEntity;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.common.rocketmq.consumer
 * Description:
 *
 * @author gel
 * @date 2019-05-06 19:04
 */
@Service
@Slf4j
@RocketMQMessageListener(topic = "comment", consumerGroup = "comment-consumer")
public class CommentTopicConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    @Autowired
    private CommentUserRepository commentUserRepository;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private CommentService commentService;

    @Override
    public void onMessage(MessageExt message) {
        String bizValue = new String(message.getBody(), Charset.forName("UTF-8"));
        String msgId = message.getMsgId();
        String tags = message.getTags();
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.COMMENT_INIT.getTag(), tags)) {
            commentInit(msgId, bizValue);
        }
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        // set consumer consume message from now
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

    private void commentInit(String msgId, String bizValue) {
        log.info("订单评价消息消费成功" + bizValue);
        JSONObject jsonObject = JSON.parseObject(bizValue);
        String shopId = jsonObject.getString("shopId");
        String merchantId = jsonObject.getString("merchantId");
        String userId = jsonObject.getString("userId");
        String orderId = jsonObject.getString("orderId");
        String orderNO = jsonObject.getString("orderNO");
        String goodsId = jsonObject.getString("goodsId");
        String skuId = jsonObject.getString("skuId");

        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(orderId) || !RegexUtils.StringIsNumber(orderId)
                || StringUtils.isBlank(orderNO)
                || StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)
                || StringUtils.isBlank(skuId) || !RegexUtils.StringIsNumber(skuId)
                || StringUtils.isBlank(merchantId) || !RegexUtils.StringIsNumber(merchantId)) {
            log.error("参数 异常");
            return;
        }

        CommentUser commentUser = commentUserRepository.findByUserId(Long.valueOf(userId));
        if (commentUser == null) {
            log.error("获取用户信息异常, 为排除服务异常，延迟消费, userId = {}", userId);
            throw new RocketMQException();
        }
        commentUserRepository.addOrderComment(commentUser, Long.valueOf(orderId));
        if (!CollectionUtils.isEmpty(commentUser.getCommentList())) {
            log.error("已有此订单评价记录");
            return;
        }

        // 查询商品的有效性
        List<Long> goodsIds = new ArrayList<>();
        goodsIds.add(Long.valueOf(goodsId));
        List<ProductEntity> productEntities = goodsAppService.listByIds(goodsIds);
        if (CollectionUtils.isEmpty(productEntities)) {
            log.error("获取商品信息异常, 为排除服务异常，延迟消费, goodsId = {}", goodsIds);
            throw new RocketMQException();
        }
        ProductEntity productEntity = productEntities.get(0);

        Date now = new Date();

        Comment comment = new Comment();
        comment.setParentId(0L);
        comment.setMerchantId(Long.valueOf(merchantId));
        comment.setShopId(Long.valueOf(shopId));
        comment.setOrderId(Long.valueOf(orderId));
        comment.setOrderNo(orderNO);
        comment.setUserId(Long.valueOf(userId));
        comment.setGoodsParentId(productEntity.getParentId());
        comment.setGoodsId(productEntity.getId());
        comment.setSkuId(Long.valueOf(skuId));
        comment.setStatus(CommentStatusEnum.INIT.getCode());
        comment.setCtime(now);
        comment.setUtime(now);
        comment.setDel(DelEnum.NO.getCode());
        comment.setRole(CommentRoleEnum.USER.getCode());

        boolean save = commentService.save(comment);
        if (!save) {
            log.error("数据库异常，延迟消费");
            throw new RocketMQException();
        }
    }




}
