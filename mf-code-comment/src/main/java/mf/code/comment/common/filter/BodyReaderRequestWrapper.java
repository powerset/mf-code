package mf.code.comment.common.filter;

import com.alibaba.fastjson.JSONObject;
import mf.code.common.utils.JsonParseUtil;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * mf.code.common.filter
 * Description:
 *
 * @author: gel
 * @date: 2018-11-09 21:45
 */
public class BodyReaderRequestWrapper extends HttpServletRequestWrapper {

    private byte[] body;
    private Map<String, String[]> params = new HashMap<>();

    public BodyReaderRequestWrapper(HttpServletRequest request) throws IOException {
        // 将request交给父类，以便于调用对应方法的时候，将其输出
        super(request);
        String bodySting = HttpHelper.getBodyString(request);
        this.body = bodySting.getBytes(Charset.forName("UTF-8"));
        // post方式中需要从流中读取数据
        if (StringUtils.equalsIgnoreCase(request.getMethod(), "POST")) {
            Map<String, String[]> stringObjectMap = JsonParseUtil.parseToMap(bodySting);
            if (stringObjectMap != null) {
                this.params.putAll(stringObjectMap);
            }
        }
        //将参数表，赋予给当前的Map以便于持有request中的参数
        this.params.putAll(request.getParameterMap());
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body);

        return new ServletInputStream() {

            @Override
            public int read() throws IOException {
                return byteArrayInputStream.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }
        };
    }

    @Override
    public String getParameter(String name) {//重写getParameter，代表参数从当前类中的map获取
        String[] values = params.get(name);
        if (values == null || values.length == 0) {
            return null;
        }
        return values[0];
    }

    @Override
    public String[] getParameterValues(String name) {//同上
        return params.get(name);
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return new Enumeration<String>() {
            private Iterator<String> iterator = params.keySet().iterator();

            @Override
            public boolean hasMoreElements() {
                return iterator.hasNext();
            }

            @Override
            public String nextElement() {
                return iterator.next();
            }
        };
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return params;
    }

    /**
     * 增加多个参数
     *
     * @param otherParams
     */
    public void addAllParameters(Map<String, Object> otherParams) {
        for (Map.Entry<String, Object> entry : otherParams.entrySet()) {
            addParameter(entry.getKey(), entry.getValue());
        }
    }

    /**
     * 增加参数
     *
     * @param name
     * @param value
     */
    public void addParameter(String name, Object value) {
        if (value != null) {
            if (value instanceof String[]) {
                params.put(name, (String[]) value);
            } else if (value instanceof String) {
                params.put(name, new String[]{(String) value});
            } else {
                params.put(name, new String[]{String.valueOf(value)});
            }
            // body回填
            Set<Map.Entry<String, String[]>> entrySet = this.params.entrySet();
            JSONObject jsonObject = new JSONObject();
            try {
                for (Map.Entry<String, String[]> entry : entrySet) {
                    jsonObject.put(entry.getKey(), entry.getValue()[0]);
                }
                body = jsonObject.toJSONString().getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }
}
