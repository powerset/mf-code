package mf.code.comment.common.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="tmplatemessage")
@Data
@Component
public class WxmpProperty {
    // 商城分销推送模板
    private String orderCashBackMsgTmpId;

    private String formIdRedisKey;


    public String getRedisKey(Long uid){
        return this.formIdRedisKey.replace("A",uid.toString());
    }
}
