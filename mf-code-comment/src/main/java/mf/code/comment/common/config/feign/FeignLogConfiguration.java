package mf.code.comment.common.config.feign;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mf.code.user.common.config.feign
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-18 16:59
 */
@Configuration
public class FeignLogConfiguration {

	@Bean
	Logger.Level feignLoggerLevel() {
		return Logger.Level.BASIC;
	}
}
