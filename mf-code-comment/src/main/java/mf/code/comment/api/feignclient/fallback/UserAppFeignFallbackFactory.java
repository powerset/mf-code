package mf.code.comment.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.api.feignclient.UserAppService;
import mf.code.user.dto.UserResp;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Slf4j
@Component
public class UserAppFeignFallbackFactory implements FallbackFactory<UserAppService> {
    @Override
    public UserAppService create(Throwable throwable) {
        return new UserAppService() {
            @Override
            public UserResp queryUser(Long userId) {
                return null;
            }

            @Override
            public List<UserResp> listByIds(List<Long> userIds) {
                return null;
            }

        };
    }
}
