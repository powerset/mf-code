package mf.code.comment.api.applet;/**
 * create by qc on 2019/8/20 0020
 */

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.repo.dto.ComplainDto;
import mf.code.comment.service.ComplainService;
import mf.code.common.simpleresp.SimpleResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * 提建议
 *
 * @author gbf
 * 2019/8/20 0020、09:47
 */
@RestController
@RequestMapping("/api/comment/applet/complain/v1")
@RequiredArgsConstructor
@Slf4j
public class AppletComplainApi {
    private final ComplainService complainService;

    /**
     * 吐槽接口
     *
     * @param complainDto
     * @return
     * @url /api/comment/applet/complain/v1/complain
     */
    @PostMapping("/complain")
    public SimpleResponse complain(@RequestBody ComplainDto complainDto, HttpServletRequest request) {
        if (StringUtils.isBlank(complainDto.getUserId())) {
            return new SimpleResponse(1, "参数错误:用户id不能为空");
        }
        if (StringUtils.isBlank(complainDto.getContent())) {
            return new SimpleResponse(1, "参数错误:建议内容不能为空");
        }
        String project = request.getHeader("project");
        complainDto.setProject(project);
        return complainService.report(complainDto);
    }
}
