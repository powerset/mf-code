package mf.code.comment.api.applet;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.common.redis.RedisForbidRepeat;
import mf.code.comment.common.redis.RedisKeyConstant;
import mf.code.comment.model.request.CommentRequest;
import mf.code.comment.repo.dto.ComplainDto;
import mf.code.comment.service.AppletCommentService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.comment.api.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 15:31
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/comment/applet")
public class AppletCommentApi {
    private final AppletCommentService appletCommentService;
    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 查询 待评价  商品列表 页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/queryPendingGoodsPage")
    public SimpleResponse queryPendingGoodsPage(@RequestParam("userId") String userId,
                                                @RequestParam("merchantId") String merchantId,
                                                @RequestParam("shopId") String shopId,
                                                @RequestParam(value = "offset", defaultValue = "0") String offset,
                                                @RequestParam(value = "size", defaultValue = "10") String size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(merchantId) || !RegexUtils.StringIsNumber(merchantId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(offset) || !RegexUtils.StringIsNumber(offset)
                || StringUtils.isBlank(size) || !RegexUtils.StringIsNumber(size)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return appletCommentService.queryPendingGoodsPage(Long.valueOf(userId), Long.valueOf(merchantId), Long.valueOf(shopId), Long.valueOf(offset), Long.valueOf(size));
    }

    /**
     * 查询 某订单的评价详情
     *
     * @param userId
     * @param orderId
     * @return
     */
    @GetMapping("/queryOrderCommentInfo")
    public SimpleResponse queryOrderCommentInfo(@RequestParam("userId") String userId,
                                                @RequestParam("orderId") String orderId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(orderId) || !RegexUtils.StringIsNumber(orderId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return appletCommentService.queryOrderCommentInfo(Long.valueOf(userId), Long.valueOf(orderId));
    }

    /**
     * 提交评价
     *
     * @param commentRequest
     * @return
     */
    @PostMapping("/submitComment")
    public SimpleResponse submitComment(@RequestBody CommentRequest commentRequest) {
        if (!commentRequest.checkParams()) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        // 提交防重
        RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.SUBMIT_FORBID_REPEAT + commentRequest.getUserId() + ":" + commentRequest.getOrderId());
        SimpleResponse simpleResponse = appletCommentService.submitComment(commentRequest);
        stringRedisTemplate.delete(RedisKeyConstant.SUBMIT_FORBID_REPEAT + commentRequest.getUserId() + ":" + commentRequest.getOrderId());
        return simpleResponse;
    }

    /**
     * 获取 商品评价 页
     *
     * @param goodsId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/queryProductCommentPage")
    public SimpleResponse queryProductCommentPage(@RequestParam("goodsId") String goodsId,
                                                  @RequestParam("shopId") String shopId,
                                                  @RequestParam(value = "offset", defaultValue = "0") String offset,
                                                  @RequestParam(value = "size", defaultValue = "10") String size) {
        if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(offset) || !RegexUtils.StringIsNumber(offset)
                || StringUtils.isBlank(size) || !RegexUtils.StringIsNumber(size)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return appletCommentService.queryProductCommentPage(Long.valueOf(goodsId), Long.valueOf(shopId), Long.valueOf(offset), Long.valueOf(size));
    }


}
