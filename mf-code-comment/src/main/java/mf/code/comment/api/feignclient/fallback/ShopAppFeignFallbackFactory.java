package mf.code.comment.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.comment.api.feignclient.ShopAppService;
import mf.code.merchant.dto.MerchantDTO;
import org.springframework.stereotype.Component;

/**
 * mf.code.web.api.feignclient.fallback
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-27 14:31
 */
@Component
public class ShopAppFeignFallbackFactory implements FallbackFactory<ShopAppService> {

    @Override
    public ShopAppService create(Throwable throwable) {
        return new ShopAppService() {

            @Override
            public MerchantDTO queryMerchantById(Long merchantId) {
                return null;
            }
        };
    }
}
