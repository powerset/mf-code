package mf.code.comment.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.comment.api.feignclient.DistributionAppService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class DistributionAppFeignFallbackFactory implements FallbackFactory<DistributionAppService> {
    @Override
    public DistributionAppService create(Throwable throwable) {
        return new DistributionAppService() {
            @Override
            public Map<Long, BigDecimal> queryOrderHistoryRebate(List<Long> orderIds) {
                return new HashMap<>();
            }

        };
    }
}
