package mf.code.comment.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.comment.api.feignclient.GoodsAppService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GoodsAppFeignFallbackFactory  implements FallbackFactory<GoodsAppService> {

    @Override
    public GoodsAppService create(Throwable throwable) {
         return new GoodsAppService() {

             /**
              * 通过主键 批量获取商品信息
              *
              * @param goodsIds
              * @return
              */
            @Override
            public List<ProductEntity> listByIds(List<Long> goodsIds) {
                return null;
            }

             @Override
             public List<ProductEntity> queryProductDetailBySkuIds(List<Long> skuIdList) {
                 return null;
             }
         };
    }




}
