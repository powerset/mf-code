package mf.code.comment.api.feignclient;

import mf.code.comment.api.feignclient.fallback.OrderAppFeignFallbackFactory;
import mf.code.comment.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.OrderResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "mf-code-order", fallbackFactory = OrderAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OrderAppService {
    /**
     * 通过orderid 获取订单信息
     *
     * @param orderId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrder")
    OrderResp queryOrder(@RequestParam(name = "orderId") Long orderId);

    /**
     * 通过orderNo 获取订单信息
     * @param orderNo
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderByOrderNo")
    OrderResp queryOrderByOrderNo(@RequestParam(name = "orderNo") Long orderNo);

    /**
     * 通过订单id 获取订单信息
     *
     * @param orderIds
     * @return
     */
    @GetMapping("/feignapi/order/applet/v7/listByIds")
    List<OrderResp> listByIds(@RequestParam("orderIds") List<Long> orderIds);


    /***
     * 获取订单分页信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 0：全部 1待付款 2 待发货 3待收货 4 交易成功 5 退款/售后
     * @param limit
     * @param offset
     * @return
     */
    @GetMapping("/feignclient/order/applet/getGoodsOrderByPage")
    SimpleResponse getGoodsOrderByPage(@RequestParam("merchantId") Long merchantId,
                                       @RequestParam("shopId") Long shopId,
                                       @RequestParam(name = "userId") Long userId,
                                       @RequestParam(name = "type", defaultValue = "1", required = false) int type,
                                       @RequestParam(name = "limit", required = false, defaultValue = "10") int limit,
                                       @RequestParam(name = "offset", required = false, defaultValue = "0") int offset);
}
