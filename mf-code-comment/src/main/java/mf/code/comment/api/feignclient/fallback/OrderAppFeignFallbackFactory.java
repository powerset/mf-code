package mf.code.comment.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.comment.api.feignclient.OrderAppService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.OrderResp;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public OrderResp queryOrder(Long orderId) {
                return null;
            }

            @Override
            public OrderResp queryOrderByOrderNo(Long orderNo) {
                return null;
            }

            @Override
            public List<OrderResp> listByIds(List<Long> orderIds) {
                return null;
            }

            @Override
            public SimpleResponse getGoodsOrderByPage(Long merchantId, Long shopId, Long userId, int type, int limit, int offset) {
                return null;
            }
        };

    }
}
