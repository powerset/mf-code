package mf.code.comment.api.platform;

import lombok.extern.slf4j.Slf4j;
import mf.code.comment.service.PlateCommentApiService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/comment/platform/auditing")
public class PlatformCommentApi {

    @Autowired
    private PlateCommentApiService plateCommentApiService;

    /**
     * 平台--审核评论列表
     *
     * @param type      1待审核，2已审核 ，默认是待审核
     * @param startTime
     * @param orderNo
     * @param goodsId
     * @param page
     * @param size
     * @param status    0全部，1审核通过，2审核不通过
     * @return
     */
    @GetMapping(value = "/getCheckListByStatus")
    public SimpleResponse getCheckListByStatus(@RequestParam(value = "type", defaultValue = "1") Integer type,
                                       @RequestParam(value = "orderNo", required = false) String orderNo,
                                       @RequestParam(value = "startTime", required = false) String startTime,
                                       @RequestParam(value = "endTime", required = false) String endTime,
                                       @RequestParam(value = "goodsId", required = false) Long goodsId,
                                       @RequestParam(value = "page", defaultValue = "1") Long page,
                                       @RequestParam(value = "size", defaultValue = "10") Long size,
                                       @RequestParam(value = "status", defaultValue = "0") Integer status) {

        return plateCommentApiService.getCheckList(type, status, startTime, endTime, goodsId, orderNo, page, size);
    }


    /**
     * 评论审核是否通过
     *
     * @param type   1通过，2不通过
     * @param id
     * @param reason
     * @return
     */
    @GetMapping(value = "/isPass")
    public SimpleResponse isPass(@RequestParam(value = "type") Integer type,
                                 @RequestParam(value = "id") Long id,
                                 @RequestParam(value = "reason", required = false) String reason) {
        //校验参数
        if (null == id || type == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
        }

        return plateCommentApiService.isPass(type,id,reason);

    }


}
