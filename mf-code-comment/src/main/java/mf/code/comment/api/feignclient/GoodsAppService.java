package mf.code.comment.api.feignclient;

import mf.code.comment.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import mf.code.comment.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "mf-code-goods", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface GoodsAppService {

    /**
     * 通过主键 批量获取商品信息
     *
     * @param goodsIds
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v8/listByIds")
    List<ProductEntity> listByIds(@RequestParam("goodsIds") List<Long> goodsIds);

    /**
     * 通过商品skuId 查询商品详情，全部字段对应db
     */
    @GetMapping("/feignapi/goods/applet/v5/queryProductDetailBySkuIds")
    List<ProductEntity> queryProductDetailBySkuIds(@RequestParam("skuIdList") List<Long> skuIdList);

}
