package mf.code.comment.api.seller;

import lombok.extern.slf4j.Slf4j;
import mf.code.comment.repo.dto.CommentReqDTO;
import mf.code.comment.service.SellerCommentApiService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.comment.api.seller
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-01 08:53
 */
@RestController
@RequestMapping("/api/comment/seller/v1/")
@Slf4j
public class SellerCommentApi {

    @Autowired
    private SellerCommentApiService sellerCommentApiService;

    /**
     * 根据订单id获取评论内容
     * @param orderId
     * @return
     */
    @GetMapping("checkComments")
    public SimpleResponse checkComments(@RequestParam("orderId") Long orderId,
                                        @RequestParam("shopId") Long shopId) {

        return sellerCommentApiService.checkComments(orderId, shopId);
    }

    /**
     * 商家回复评论
     * @return
     */
    @PostMapping("sellerReplyComment")
    public SimpleResponse sellerReplyComment(@RequestBody CommentReqDTO commentReqDTO
                                            ){

        if (null== commentReqDTO){
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1);
        }

        return sellerCommentApiService.sellerReplyComment(commentReqDTO.getMerchantId(),commentReqDTO.getContents(),commentReqDTO.getCommentId(),commentReqDTO.getShopId());
    }

}
