package mf.code.comment.api.feignclient;

import mf.code.comment.api.feignclient.fallback.DistributionAppFeignFallbackFactory;
import mf.code.comment.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-08 19:11
 */
@FeignClient(value = "mf-code-distribution", fallbackFactory = DistributionAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface DistributionAppService {

    @GetMapping("/feignapi/distribution/applet/v5/queryOrderHistoryRebate")
    Map<Long, BigDecimal> queryOrderHistoryRebate(@RequestParam("orderIds") List<Long> orderIds);
}
