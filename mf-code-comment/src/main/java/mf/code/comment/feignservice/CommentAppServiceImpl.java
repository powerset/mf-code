package mf.code.comment.feignservice;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.domain.aggregateroot.CommentProduct;
import mf.code.comment.domain.aggregateroot.CommentUser;
import mf.code.comment.domain.repository.CommentProductRepository;
import mf.code.comment.domain.repository.CommentUserRepository;
import mf.code.comment.dto.OrderCommentDTO;
import mf.code.comment.repo.po.Comment;
import mf.code.comment.repo.repository.CommentService;
import mf.code.common.DelEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.comment.feignservice
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 14:59
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class CommentAppServiceImpl {
    private final CommentUserRepository commentUserRepository;
    private final CommentService commentService;
    private final CommentProductRepository commentProductRepository;

    /**
     * 批量 获取 订单评价的状态
     *
     * @param orderIds
     * @return 0 未评价， 1已评价
     */
    @GetMapping("/feignapi/comment/applet/listOrderCommentStatus")
    public Map<Long, String> listOrderCommentStatus(@RequestParam("orderIds") List<Long> orderIds) {
        if (CollectionUtils.isEmpty(orderIds)) {
            log.error("参数异常, orderIds = {}", orderIds);
            return null;
        }
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Comment::getParentId, 0L)
                .in(Comment::getOrderId, orderIds)
                .eq(Comment::getDel, DelEnum.NO.getCode());
        List<Comment> commentList = commentService.list(wrapper);
        Map<Long, Comment> commentMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(commentList)) {
            for (Comment comment : commentList) {
                commentMap.put(comment.getOrderId(), comment);
            }
        }
        Map<Long, String> orderCommentStatus = new HashMap<>();
        for (Long orderId : orderIds) {
            Comment comment = commentMap.get(orderId);
            if (comment == null || comment.getStatus() == CommentStatusEnum.INIT.getCode()) {
                orderCommentStatus.put(orderId, "0");
            } else {
                orderCommentStatus.put(orderId, "1");
            }
        }
        return orderCommentStatus;
    }

    /**
     * 获取 商品 最新的一条 评价记录
     *
     * @param goodsId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/comment/applet/queryProductCommentPage")
    public Map queryProductCommentPage(@RequestParam("goodsId") Long goodsId,
                                       @RequestParam("shopId") Long shopId,
                                       @RequestParam(name = "goodsParentId", defaultValue = "false") boolean goodsParentId) {
        if (goodsId == null || goodsId < 1
                || shopId == null || shopId < 1) {
            log.error("参数异常");
            return null;
        }
        CommentProduct commentProduct = commentProductRepository.findByProductId(goodsId);
        if (commentProduct == null) {
            log.error("获取商品信息异常, goodsId = {}", goodsId);
            return null;
        }
        commentProductRepository.addProductCommentPage(commentProduct, 0L, 1L, goodsParentId);
        AppletMybatisPageDto productCommentPage = commentProduct.getProductCommentPage();
        List content = productCommentPage.getContent();
        if (CollectionUtils.isEmpty(content)) {
            return null;
        }
        Map map = (Map) content.get(0);
        map.put("num", productCommentPage.getTotal());
        return map;
    }

    /**
     * 我的订单中待评价数量
     *
     * @return
     */
    @GetMapping("/feignapi/comment/applet/getUnCommentNum")
    public int getUnCommentNum(@RequestParam("userId") Long userId,
                               @RequestParam("merchantId") Long merchantId,
                               @RequestParam("shopId") Long shopId) {
        if (userId == null || userId < 1
                || merchantId == null || merchantId < 1
                || shopId == null || shopId < 1) {
            log.error("我的订单中待评价数量 参数异常");
            return 0;
        }
        CommentUser commentUser = commentUserRepository.findByUserId(userId);
        if (commentUser == null) {
            log.error("服务异常， userId = {}", userId);
            return 0;
        }
        commentUserRepository.addPendingGoodsPage(commentUser, merchantId, shopId, 0L, 1L);

        return (int) commentUser.getPendingGoodsPage().getTotal();
    }


    /**
     * 根据shopId和订单id分页查询评论信息
     *
     * @param orderIdList
     * @param shopId
     * @param statusList  -1初始值（未评论）,0买家已评论，1审核通过，2不通过
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/feignapi/comment/seller/getCommentListByShopAndOrderIds")
    public List<OrderCommentDTO> getCommentListByShopAndOrderIds(@RequestParam(value = "orderIdList", required = false) List<Long> orderIdList, @RequestParam(value = "statusList", required = false) List<Integer> statusList, @RequestParam(value = "shopId", required = false) Long shopId, @RequestParam("page") int page, @RequestParam("size") int size) {

        log.info(">>>>>>>>>>>>>>>>>>>>>435  enter into getCommentListByShopAndOrderIds ");
        AppletMybatisPageDto<OrderCommentDTO> pendingGoodsPage = getCommentInfoList(orderIdList, statusList, shopId, page, size);

        if (null == pendingGoodsPage || pendingGoodsPage.getContent() == null) {
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>234599have find pendingGoodsPage.getContent() is null ");
            return null;
        }

        log.info(">>>>>>>>>>>>>8999 pendingGoodsPage.getContent() =" + JSON.toJSONString(pendingGoodsPage.getContent()));
        return pendingGoodsPage.getContent();
//        log.info(">>>>>>>>>>>>>6879 list ="+JSON.toJSONString(getCommentList( orderIdList,  statusList, shopId,  page,  size)));
//        return getCommentList( orderIdList,  statusList, shopId,  page,  size);
    }


    /**
     * 获取评论信息总数
     *
     * @param orderIdList 分页查询-- 根据状态查询评论列表信息
     * @param statusList
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/comment/seller/getCountOfCommentListNum")
    public Long getCountOfCommentListNum(@RequestParam(value = "orderIdList", required = false) List<Long> orderIdList, @RequestParam(value = "statusList", required = false) List<Integer> statusList,
                                         @RequestParam(value = "shopId", required = false) Long shopId) {

        AppletMybatisPageDto<OrderCommentDTO> pendingGoodsPage = getCommentInfoList(orderIdList, statusList, shopId, 1, 500);

        if (null == pendingGoodsPage) {
            return 0L;
        }
        return pendingGoodsPage.getTotal();
    }

    /**
     * 获取评论信息
     *
     * @param orderIdList
     * @param commentStatus
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    private AppletMybatisPageDto<OrderCommentDTO> getCommentInfoList(List<Long> orderIdList, List<Integer> commentStatus, Long shopId, int page, int size) {

        log.info(">>>>>>>>>>>>>>> info have enter into getCommentInfoList shopId=" + shopId + ",");
        AppletMybatisPageDto<OrderCommentDTO> pendingGoodsPage = new AppletMybatisPageDto<>(size, page);

        pendingGoodsPage.setContent(new ArrayList<>());
        pendingGoodsPage.setLimit(size);
        pendingGoodsPage.setOffset(page);
        pendingGoodsPage.setPullDown(false);

        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        if (!CollectionUtils.isEmpty(orderIdList)) {
            queryWrapper.lambda().in(Comment::getOrderId, orderIdList);
        }
        if (!CollectionUtils.isEmpty(commentStatus)) {
            queryWrapper.lambda().in(Comment::getStatus, commentStatus);
        }

        if (null != shopId) {
            queryWrapper.lambda().eq(Comment::getShopId, shopId);
        }
        queryWrapper.lambda().eq(Comment::getDel, DelEnum.NO.getCode());

        Page<Comment> pages = new Page<>(page, size);
        IPage<Comment> commentIPage = commentService.page(pages, queryWrapper);
        if (commentIPage == null) {
            log.error("数据库异常");
            return pendingGoodsPage;
        }

        List<Comment> commentList = commentIPage.getRecords();

        List<OrderCommentDTO> orderCommentDTOList = new ArrayList<>();

        if (CollectionUtils.isEmpty(commentList)) {
            return pendingGoodsPage;
        }

        log.info(">>>>>>>>>>>>55 have find commentList=" + JSON.toJSONString(commentList));

        //BeanUtils.copyProperties(commentList,orderCommentDTOList);
        for (Comment c : commentList) {

            OrderCommentDTO orderCommentDTO = new OrderCommentDTO();
            BeanUtils.copyProperties(c, orderCommentDTO);

            orderCommentDTOList.add(orderCommentDTO);
        }


        long total = commentIPage.getTotal();
        pendingGoodsPage.setTotal(total);

        if (page * size - 1 <= total) {
            pendingGoodsPage.setPullDown(true);
        }

        log.info(">>>>>>>>>>>> 121have find orderCommentDTOList=" + JSON.toJSONString(orderCommentDTOList));
        pendingGoodsPage.setContent(orderCommentDTOList);

        return pendingGoodsPage;
    }


}


