package mf.code.comment;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MfCodeCommentApplicationTests {

    public static void main(String[] args) {
        SpringApplication.run(MfCodeCommentApplicationTests.class, args);
    }

    @Test
    public void contextLoads() {
    }

}
