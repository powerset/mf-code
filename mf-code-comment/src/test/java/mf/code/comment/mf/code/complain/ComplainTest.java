package mf.code.comment.mf.code.complain;/**
 * create by qc on 2019/8/21 0021
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.comment.MfCodeCommentApplication;
import mf.code.comment.api.applet.AppletComplainApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author gbf
 * 2019/8/21 0021、13:56
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MfCodeCommentApplication.class)
@Slf4j
public class ComplainTest {
    @Autowired
    private AppletComplainApi appletComplainApi;

    @Test
    public void apiTest() {

    }

}
