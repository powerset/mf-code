package mf.code.goodsAppService;

import lombok.extern.slf4j.Slf4j;
import mf.code.goods.api.applet.feignservice.GoodsAppServiceImpl;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.GoodsSkuDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * mf.code.goodsAppService
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月10日 19:02
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductSkuFeignTest {
    @Autowired
    private GoodsAppServiceImpl goodsAppService;

    @Test
    public void queryProductSku() {
        Long merchantId = 8L;
        Long shopId = 8L;
        Long skuId = 16L;
        Long userId = 36L;
        Long goodsId = 1L;
        GoodsSkuDTO goodsSkuDTO = this.goodsAppService.queryProductSku(merchantId, shopId, goodsId, skuId, userId);
        log.info("info:{}", goodsSkuDTO);
    }

    @Test
    public void homePageGoodsInfoTest(){

        Map<String, Object> map = goodsAppService.homePageGoodsInfo(114L, "36", ProductShowTypeEnum.NEWBIE.getCode(), 1L, 10L);
        System.out.println(map);
    }
}
