package mf.code;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MfCodeGoodsApplicationTests {

    public static void main(String[] args) {
        SpringApplication.run(MfCodeGoodsApplicationTests.class, args);
    }

    @Test
    public void contextLoads() {
    }

}
