package mf.code.goods.group;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.goods.api.applet.feignservice.GoodsAppServiceImpl;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.goods.repo.dao.ProductGroupMapMapper;
import mf.code.goods.repo.po.ProductGroupMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * mf.code.goods.group
 * Description:
 *
 * @author gel
 * @date 2019-05-17 12:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductGroupTest {
    @Autowired
    private ProductGroupMapMapper productGroupMapMapper;

    @Test
    public void selectDistinctGroupIdListByParentProductId() {
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getParentProductId, 623)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        queryWrapper.select("group_id");
        List<Object> groupIdList = productGroupMapMapper.selectObjs(queryWrapper);
        Set<Long> groupIdResultSet = new HashSet<>();
        for (Object object : groupIdList) {
            groupIdResultSet.add(Long.valueOf(object.toString()));
        }
    }
}
