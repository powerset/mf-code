package mf.code.goods.api.applet;/**
 * create by qc on 2019/4/10 0010
 */

import mf.code.goods.api.applet.feignservice.GoodsAppServiceImpl;
import mf.code.goods.api.applet.v10.ActivityProductApi;
import mf.code.goods.service.SellerGoodsV8Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author gbf
 * 2019/4/10 0010、14:10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppletActivityGoodsApiTest {
    @Autowired
    private ActivityProductApi activityProductApi;
    @Autowired
    private SellerGoodsV8Service sellerGoodsV8Service;
    @Autowired
    private GoodsAppServiceImpl goodsAppService;


    @Test
    public void activityProductMs() {
        sellerGoodsV8Service.pickProductList(0, 4, 8L, "", 3L, 3);
    }

    @Test
    public void getPlatformDistRateCommission() {
        // parameter= {"merchantId":["260"],"shopId":["155"],"goodsId":["139"],"skuId":["2070"]} Exception= java.lang.NullPointerException
        String platformDistRateCommission = goodsAppService.getPlatformDistRateCommission(260L, 155L, 2070L);
    }
}
