package mf.code;

import mf.code.goods.common.redis.RedisKeyConstant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisScanTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    private String NEW_KEY = "NEW_KEY:list";

    @Test
    public void getScan() {
        int count = 1;
        Cursor<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().scan(
                RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, ScanOptions.NONE);

        Long size = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST);
        System.out.println(size);

        while (typedTuples.hasNext()) {
            ZSetOperations.TypedTuple<String> item = typedTuples.next();

            if (item.getScore() >= 0) {
                System.out.println(item.getValue() + ":row=" + count++);
                stringRedisTemplate.opsForZSet().add(NEW_KEY, item.getValue(), item.getScore());
            }
        }

        //该店铺已经分销过的产品
        List<Long> llist = new ArrayList<>();
        llist.add(273L);
        llist.add(765L);


        Cursor<ZSetOperations.TypedTuple<String>> newTypedTuples = stringRedisTemplate.opsForZSet().scan(
                NEW_KEY, ScanOptions.NONE);

        Long num = stringRedisTemplate.opsForZSet().size(NEW_KEY);
        System.out.println(">>>>>>>>>>>>>>num" + num);
        while (newTypedTuples.hasNext()) {
            ZSetOperations.TypedTuple<String> newitem = newTypedTuples.next();

            for(Long l : llist){
                if (newitem.getValue().equals(l.toString())){
                    stringRedisTemplate.opsForZSet().remove(NEW_KEY,l.toString());
                }

            }
            System.out.println("key=" + newitem.getValue() + ",score=" + newitem.getScore());
        }

    }
}
