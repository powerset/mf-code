package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

public interface PlateFromDistributionGoodsApiService {
    /**
     * 上传图片--平台分销--推荐
     *
     * @param image
     * @param key
     * @param type
     * @return
     */
    SimpleResponse uploadPicture(MultipartFile image, String key, String type);

    /**
     * 获取新类目的信息
     *
     * @param plateFromId
     * @return
     */
    SimpleResponse getNewSpecesInfo(String plateFromId);

    /**
     * 根据新的类目ID批量修改类目信息
     *
     * @param specesId
     * @param ratio
     * @return
     */
    SimpleResponse updateSpecesInfo(String specesId, BigDecimal ratio);

    /**
     * 保存图片地址
     * @param mainPicsList
     * @return
     */

    SimpleResponse saveUploadPics(String mainPicsList, String key, String type);

    /**
     * 获取已经上传的banner图
     * @return
     */

    SimpleResponse showHaveUploadPics(String key, String type);
}
