package mf.code.goods.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.goods.api.seller.dto.PlateDisProductListResultDTO;
import mf.code.goods.common.redis.RedisForbidRepeat;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.dto.CategoryDistributionDTO;
import mf.code.goods.service.AsyncProductService;
import mf.code.goods.service.CategoryGroupMappingService;
import mf.code.goods.service.SellerPlateGoodsDistributionApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-07-01 17:37
 */
@Slf4j
@Service
public class AsyncProductServiceImpl implements AsyncProductService {

    private static final long REDIS_EXPIRE_MINUTES = 1L;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CategoryGroupMappingService categoryGroupMappingService;
    @Autowired
    private SellerPlateGoodsDistributionApiService sellerPlateGoodsDistributionApiService;

    /**
     * 创建店铺商品零级类目缓存
     *
     * @param shopId
     */
    @Async
    @Override
    public void createProductCategoryZeroRedisForShop(Long shopId) {
        String redisKey = RedisKeyConstant.PRODUCT_CATEGORY_ZERO_SHOP + shopId;
        Set<String> members = stringRedisTemplate.opsForSet().members(redisKey);
        if (!CollectionUtils.isEmpty(members)) {
            return;
        }
        String redisLock = RedisKeyConstant.PRODUCT_CATEGORY_ZERO_LOCK_SHOP + shopId;
        boolean nx = RedisForbidRepeat.NX(stringRedisTemplate, redisLock, 10, TimeUnit.SECONDS);
        if (!nx) {
            log.info("创建店铺商品零级类目缓存,被锁定。shopId:" + shopId);
            return;
        }
        log.info("开始生成店铺零级类目缓存，店铺ID: " + shopId + "时间：" + DateUtil.getNow());
        // 生成店铺零级类目缓存
        List<CategoryDistributionDTO> categoryDistributionDTOS = categoryGroupMappingService.queryAllZeroLevel();
        if (CollectionUtils.isEmpty(categoryDistributionDTOS)) {
            RedisForbidRepeat.delKey(stringRedisTemplate, redisLock);
            return;
        }
        for (CategoryDistributionDTO categoryDistributionDTO : categoryDistributionDTOS) {
            PlateDisProductListResultDTO resultDTO = sellerPlateGoodsDistributionApiService.getPalteDistributionListResult(
                    1, 0, shopId.toString(), categoryDistributionDTO.getSid(), null);
            if (resultDTO.getCount() == 0) {
                continue;
            }
            stringRedisTemplate.opsForSet().add(redisKey, categoryDistributionDTO.getSid());
        }
        stringRedisTemplate.expire(redisKey, REDIS_EXPIRE_MINUTES, TimeUnit.MINUTES);
        log.info("结束生成店铺零级类目缓存，店铺ID: " + shopId + "时间：" + DateUtil.getNow());
        RedisForbidRepeat.delKey(stringRedisTemplate, redisLock);
    }
}
