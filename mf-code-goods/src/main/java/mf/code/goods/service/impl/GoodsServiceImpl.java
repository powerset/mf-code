package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.AddOrReduceEnum;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.goods.api.feignclient.ShopAppService;
import mf.code.goods.api.seller.dto.*;
import mf.code.goods.common.caller.aliyunoss.OssCaller;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.common.redis.RedisForbidRepeat;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.*;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.dao.SellerGoodsDistributionMapper;
import mf.code.goods.repo.po.*;
import mf.code.goods.repo.repostory.*;
import mf.code.goods.repo.service.CategoryTaobaoService;
import mf.code.goods.repo.vo.ProductSkuSpecValVo;
import mf.code.goods.service.*;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.shop.constants.AppTypeEnum;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.shop.dto.ShopDetailForPlatformDTO;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.AnnotatedType;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static mf.code.common.utils.FileUtil.checkFileType;

/**
 * @author gbf
 * 2019/4/3 0003、09:09
 */
@Service
@Slf4j
public class GoodsServiceImpl extends ServiceImpl<ProductMapper, Product> implements GoodsService {
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private SellerGoodsDistributionMapper sellerGoodsDistributionMapper;
    @Autowired
    private CategoryTaobaoRepository categoryTaobaoRepository;
    @Autowired
    private CategoryTaobaoService categoryTaobaoService;
    @Autowired
    private ProductGroupMapRepository productGroupMapRepository;
    @Autowired
    private ProductGroupRepository productGroupRepository;
    @Autowired
    private ProductDistributionAuditLogRepository productDistributionAuditLogRepository;
    @Autowired
    private SellerProductRepository sellerProductRepostory;
    @Autowired
    private SellerSkuRepository sellerSkuRepostory;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private GoodsSkuService goodsSkuService;
    @Autowired
    private GoodsSkuAboutService goodsSkuAboutService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ProductSkuSpecService productSkuSpecService;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private AsyncProductService asyncProductService;

    private final String SELLER_RECALL = "seller/goods/";
    private final String MIN_COMMISSION_PRICE = "0.25";


    /**
     * 上传图片
     *
     * @param image  .
     * @param shopId .
     * @return .
     */
    @Override
    public SimpleResponse uploadPic(MultipartFile image, Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();

        long PIC_MAX_SIZE = 1024L * 5 * 1024;
        if (image.getSize() > PIC_MAX_SIZE) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "上传图片大小过大");
        }
        String picName = shopId + "_" + System.currentTimeMillis() + RandomStrUtil.randomStr(3);
        String bucketName = SELLER_RECALL + shopId + "/";
        String imgUrl = this.uploadObject2OSS(image, bucketName, picName);
        Map<String, Object> map = new HashMap<>();
        map.put("imgUrl", imgUrl);
        simpleResponse.setData(map);
        return simpleResponse;
    }


    /**
     * 修改商品
     *
     * @param productDto 商品dto
     * @return SimpleResponse
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse update(ProductDTO productDto) {
        Date now = new Date();
        Product product = this.editProduct(productDto);
        if (product == null) {
            return new SimpleResponse(15, "商品的状态有误,该商品不是可修改状态");
        }
        SimpleResponse error = this.checkoutProductBelongError(product, productDto.getMerchantId(), productDto.getShopId());
        if (error != null) {
            return error;
        }

        int saveProductResult = sellerProductRepostory.updateBySelective(product);
        if (saveProductResult == 0) {
            log.error("插入product表失败,merchantId={},shopId={},当前时间是={}", productDto.getMerchantId(), productDto.getShopId(), now);
            return new SimpleResponse(14, "插入产品错误");
        }
        this.handleUpdateSku(now, productDto, product);
        /*******add 修改product价格相关的属性begin************/
        ProductSkuInfoDTO dto = productSkuMapper.getProductSkuPriceInfo(productDto.getProductId());
        int count = sellerGoodsDistributionMapper.updateProductPriceInfoById(dto);
        if (count < 1) {
            log.error("修改product中的价格相关的属性错误,productId={}", product.getId());
            Assert.isTrue(false, 202, "新增sku发生错误");
        }
        /*******add 修改product价格相关的属性end**************/

        return new SimpleResponse(0, "success");
    }

    /**
     * 删除
     *
     * @param productId 产品id
     * @return .
     */
    @Override
    public SimpleResponse del(long productId, Integer appType) {
        AppTypeEnum appTypeEnum = AppTypeEnum.JKMF;
        if (appType == AppTypeEnum.DDD.getCode())
            appTypeEnum = AppTypeEnum.DDD;
        return this.del(productId, appTypeEnum);
    }

    /**
     * 删除
     *
     * @param productId 产品id
     * @return .
     */
    @Override
    public SimpleResponse del(long productId, AppTypeEnum appTypeEnum) {
        Product product = sellerProductRepostory.selectById(productId);
        if (product == null) {
            return new SimpleResponse(30, "无此商品");
        }
        // 删除商品必须是下架状态
        if (appTypeEnum == AppTypeEnum.JKMF && product.getStatus() == ProductConstant.Status.ON_SALE) {
            return new SimpleResponse(10, "不能删除上架状态的商品");
        }
        product.setDel(1);
        product.setUtime(new Date());
        int delResult = sellerProductRepostory.updateBySelective(product);
        if (delResult != 1) {
            log.error("删除商品失败,productid={}", productId);
            return new SimpleResponse(11, "删除失败");
        }
        return new SimpleResponse(0, "success");
    }

    /**
     * 平台运营审核
     *
     * @param productId         产品id
     * @param checkStatus
     * @param reason            原因
     * @param checkReasonPicStr
     * @return .
     */
    @Override
    public SimpleResponse audit(long productId, Integer checkStatus, String reason, String checkReasonPicStr) {
        Product product = sellerProductRepostory.selectById(productId);
        if (product == null) {
            return new SimpleResponse(30, "无此商品");
        }
        if (product.getStatus() == ProductConstant.Status.ON_SALE) {
            return new SimpleResponse(10, "该商品已经上架,无需审核");
        }
        if (product.getDel() == ProductConstant.DelStatus.HAS_DELETED) {
            return new SimpleResponse(11, "该商品已经被删除");
        }
        if (product.getCheckStatus() == ProductConstant.CheckStatus.PASS) {
            return new SimpleResponse(12, "该商品已经审核通过");
        }
        if (product.getCheckStatus() == ProductConstant.CheckStatus.NOT_COMMIT) {
            return new SimpleResponse(13, "该商品未提交审核,无法审核此商品");
        }
        if (ProductConstant.CheckStatus.NOT_PASS == checkStatus) {
            if (StringUtils.isBlank(reason)) {
                return new SimpleResponse(14, "如审核不通过,务必填写原因");
            }
        }
        Date now = new Date();
        product.setCheckStatus(checkStatus);
        product.setCheckReason(reason);
        product.setCheckReasonPic(checkReasonPicStr);
        product.setCheckTime(now);
        product.setUtime(now);
        // 如果是审核通过 , 就将product.status=1 ,否则保持原有 status=0
        if (ProductConstant.CheckStatus.PASS == checkStatus) {
            product.setPutawayTime(now);
            product.setStatus(ProductConstant.Status.ON_SALE);
        }
        int updateResult = sellerProductRepostory.updateBySelective(product);
        if (updateResult != 1) {
            log.error("审核操作失败,productId={}", productId);
            return new SimpleResponse(20, "审核操作失败");
        }
        return new SimpleResponse(0, "success");
    }


    /**
     * 商品详情
     * <pre>
     *     商品名称
     *     主图
     *     详情图
     *     佣金=>计算最大最小比例,再进行计算
     *     售价=>计算最大最小值
     *     总销量 sum(sku.stock_del-stock)
     *     总库存 sum(sku.stock)
     *     计算销售属性
     * </pre>
     *
     * @param productId 产品id
     * @return .
     */
    @Override
    public SimpleResponse detail(long productId) {
        Product product = sellerProductRepostory.selectById(productId);
        CategoryTaobao categoryTaobao = categoryTaobaoRepository.selectBySid(product.getProductSpecs().toString(), 1);
        if (product == null) {
            return new SimpleResponse(30, "无此商品");
        }
        BigDecimal platSupportRatio = product.getPlatSupportRatio();
        if (platSupportRatio == null) {
            platSupportRatio = new BigDecimal(0);
        }
        List<ProductSku> skuList = productSkuMapper.findByProductId(productId);

        //售价
        BigDecimal priceMin = new BigDecimal(999999999);
        BigDecimal priceMax = new BigDecimal(0);
        //淘宝价
        BigDecimal priceTbMin = new BigDecimal(999999999);
        BigDecimal priceTbMax = new BigDecimal(0);

        Integer stockTotal = 0;
        Integer stockDefTotal = 0;
        String mainPicsStr = product.getMainPics();
        List<String> list = JSON.parseArray(mainPicsStr, String.class);
        List<Map<String, Object>> skuResultMapList = new ArrayList<>();

        Map<Long, String> specNameMap = goodsSkuAboutService.getSkuSalePropMap();
        //计算销售属性给前端展示
        Map<String, List<ProductSkuSpecValVo>> tempMap = new HashMap<>();
        for (ProductSku sku : skuList) {
            BigDecimal sellingPrice = sku.getSkuSellingPrice();
            priceMin = sellingPrice.compareTo(priceMin) < 0 ? sellingPrice : priceMin;
            priceMax = sellingPrice.compareTo(priceMax) > 0 ? sellingPrice : priceMax;

            //淘宝价
            BigDecimal skuThirdPrice = sku.getSkuThirdPrice();
            priceTbMin = skuThirdPrice.compareTo(priceTbMin) < 0 ? skuThirdPrice : priceTbMin;
            priceTbMax = skuThirdPrice.compareTo(priceTbMax) > 0 ? skuThirdPrice : priceTbMax;

            stockDefTotal += sku.getStockDef();
            stockTotal += sku.getStock();
            String skuPic = sku.getSkuPic();

            Map<String, Object> skuMap = new HashMap<>();
            skuMap.put("skuId", sku.getId());
            skuMap.put("stock", sku.getStock());
            //这里的快照,是设计初期的名称歧义
            skuMap.put("snapshoot", sku.getNotShow());
            skuMap.put("skuSellingPrice", sku.getSkuSellingPrice());
            skuMap.put("skuThirdPrice", sku.getSkuThirdPrice());
            skuMap.put("skuPic", skuPic == null ? (list != null && list.size() > 0 ? list.get(0) : null) : skuPic);
            String specSortStr = sku.getSpecSort();
            //存储商品属性
            List<ProductSkuSpec> productSkuSpecList = productSkuSpecService.parseProductSkuSpecs(specSortStr, specNameMap);

            skuMap.put("specs", productSkuSpecList);
            skuResultMapList.add(skuMap);

            for (ProductSkuSpec e : productSkuSpecList) {
                List<ProductSkuSpecValVo> valVoList = null;
                String key = e.getParentId();
                if (tempMap.containsKey(e.getParentId())) {
                    valVoList = tempMap.get(key);
                } else {
                    valVoList = new ArrayList<>();
                }
                ProductSkuSpecValVo vo = new ProductSkuSpecValVo();
                vo.setSpecValId(Long.parseLong(e.getSpecId()));
                vo.setSpecValVal(e.getSpecName());
                valVoList.add(vo);
                tempMap.put(key, valVoList);
            }
        }
        List<Map> propResultList = new ArrayList<>();
        Set<String> keySet = tempMap.keySet();
        for (String key : keySet) {
            Map<String, Object> propResultMap = new HashMap<>();
            propResultMap.put("specNameId", key);
            propResultMap.put("specNameVal", specNameMap.get(key));
            propResultMap.put("list", tempMap.get(key));
            propResultList.add(propResultMap);
        }


        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("productId", product.getId());
        resultMap.put("saleProps", propResultList);
        resultMap.put("title", product.getProductTitle());
        //图片
        resultMap.put("mainPic", mainPicsStr);
        resultMap.put("detailPic", product.getDetailPics());
        //佣金
        resultMap.put("commissionMin", priceMin.multiply(platSupportRatio).divide(new BigDecimal(100)));
        resultMap.put("commissionMax", priceMax.multiply(platSupportRatio).divide(new BigDecimal(100)));
        //售价
        resultMap.put("priceMin", priceMin);
        resultMap.put("priceMax", priceMax);
        //淘宝价
        resultMap.put("skuThirdPriceMin", priceTbMin);
        resultMap.put("skuThirdPriceMax", priceTbMax);
        //商品类别
        resultMap.put("category", categoryTaobao == null ? "" : categoryTaobao.getSid());
        resultMap.put("categoryName", categoryTaobao == null ? "" : categoryTaobao.getName());
        //销量
        resultMap.put("stock", stockTotal);
        resultMap.put("saleVolume", stockDefTotal - stockTotal);
        //状态
        resultMap.put("status", product.getStatus());
        // sku 列表
        resultMap.put("skuList", skuResultMapList);
        resultMap.put("initSales", product.getInitSales());
        resultMap.put("numIid", product.getNumIid());
        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 审核列表
     *
     * @param conditionMap
     * @param pageCurrent
     * @param pageSize
     * @return
     */
    @Override
    public SimpleResponse checkListByCondition(Map<String, Object> conditionMap, int pageCurrent, int pageSize) {
        conditionMap.put("pageBegin", (pageCurrent - 1) * pageSize);
        conditionMap.put("pageSize", pageSize);
        int pagingCountTotal = sellerProductRepostory.countByConditionsForPlat(conditionMap);
        List<Map> resultList = sellerProductRepostory.findByConditionsForPlat(conditionMap);
        for (Map m : resultList) {
            Object productId = m.get("productId");
            Assert.isTrue(productId != null, 90, "产品id为空,不存在此商品");
            String productIdStr = productId.toString();
            //提交审核时间
            Object commitTime = m.get("commitTime");
            if (commitTime != null) {
                Date date = (Date) commitTime;
                String dateStr = DateUtil.dateToString(date, DateUtil.FORMAT_TWO);
                m.put("commitTime", dateStr);
            } else {
                log.error("获取提交审核时间有误,该商品id={}", productIdStr);
            }
            //审核时间
            Object checkTime = m.get("checkTime");
            if (checkTime != null) {
                Date date = (Date) checkTime;
                String dateStr = DateUtil.dateToString(date, DateUtil.FORMAT_TWO);
                m.put("checkTime", dateStr);
            } else {
                m.put("checkTime", "");
                log.error("获取审核时间有误,该商品id={}", productIdStr);
            }
            // 图片
            Object productPic = m.get("productPic");
            Assert.isTrue(productPic != null, 91, "获取到产品列表,但产品id=?的主图为空".replace("?", productIdStr));
            m.put("productPic", this.getFirstPicFromMainPics(productPic));
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", pagingCountTotal);
        resultMap.put("list", resultList);
        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 商品审核-详情
     *
     * @param productId 产品id
     * @return
     */
    @Override
    public SimpleResponse platCheckProductDetail(Long productId) {
        Product product = sellerProductRepostory.selectById(productId);
        CategoryTaobao categoryTaobao = categoryTaobaoRepository.selectBySid(product.getProductSpecs().toString(), 1);
        if (product == null) {
            return new SimpleResponse(30, "无此商品");
        }
        List<ProductSku> skuList = productSkuMapper.findByProductId(product.getParentId());

        BigDecimal priceMin = new BigDecimal(999999999);
        BigDecimal priceMax = new BigDecimal(0);
        BigDecimal priceTbMin = new BigDecimal(999999999);
        BigDecimal priceTbMax = new BigDecimal(0);

        Integer stockTotal = 0;
        String mainPicsStr = product.getMainPics();
        List<String> list = JSON.parseArray(mainPicsStr, String.class);
        List<Map<String, Object>> skuResultMapList = new ArrayList<>();

        for (ProductSku sku : skuList) {
            //售价
            BigDecimal sellingPrice = sku.getSkuSellingPrice();
            priceMin = sellingPrice.compareTo(priceMin) < 0 ? sellingPrice : priceMin;
            priceMax = sellingPrice.compareTo(priceMax) > 0 ? sellingPrice : priceMax;
            //淘宝价
            BigDecimal skuThirdPrice = sku.getSkuThirdPrice();
            priceTbMin = skuThirdPrice.compareTo(priceTbMin) < 0 ? skuThirdPrice : priceTbMin;
            priceTbMax = skuThirdPrice.compareTo(priceTbMax) > 0 ? skuThirdPrice : priceTbMax;

            stockTotal += sku.getStock();
            String skuPic = sku.getSkuPic();

            Map<String, Object> skuMap = new HashMap<>();
            String specSortStr = sku.getSpecSort();
            ProductSkuSpec spec = new ProductSkuSpec();
            //存储商品属性
            List<ProductSkuSpec> productSkuSpecList = productSkuSpecService.parseProductSkuSpecs(specSortStr, goodsSkuAboutService.getSkuSalePropMap());
            skuMap.put("specs", productSkuSpecList);
            //库存
            skuMap.put("stock", sku.getStock());
            //售价
            skuMap.put("skuSellingPrice", sku.getSkuSellingPrice());
            //淘宝价
            skuMap.put("skuThirdPrice", sku.getSkuThirdPrice());
            //图片
            skuMap.put("skuPic", skuPic == null ? (list != null && list.size() > 0 ? list.get(0) : null) : skuPic);

            skuResultMapList.add(skuMap);
        }
        Map<String, Object> resultMap = new HashMap<>();
        //商品id
        resultMap.put("productId", product.getId());
        //标题
        resultMap.put("title", product.getProductTitle());
        //商品类别
        resultMap.put("category", categoryTaobao == null ? "" : categoryTaobao.getSid());
        resultMap.put("categoryName", categoryTaobao == null ? "" : categoryTaobao.getName());
        //售价
        resultMap.put("priceMin", priceMin);
        resultMap.put("priceMax", priceMax);
        //淘宝价
        resultMap.put("skuThirdPriceMin", priceTbMin);
        resultMap.put("skuThirdPriceMax", priceTbMax);
        //库存
        resultMap.put("stock", stockTotal);
        //主图
        resultMap.put("mainPic", mainPicsStr);
        //详图
        resultMap.put("detailPic", product.getDetailPics());
        //提交时间
        Date utime = product.getSubmitTime();
        resultMap.put("commitTime", utime == null ? "--" : DateUtil.dateToString(utime, DateUtil.FORMAT_TWO));
        //审核时间
        Date checkTime = product.getCheckTime();
        resultMap.put("checkTime", checkTime == null ? "--" : DateUtil.dateToString(checkTime, DateUtil.FORMAT_TWO));
        //审核状态
        resultMap.put("checkStatus", product.getCheckStatus());
        //理由
        resultMap.put("chackReason", product.getCheckReason());
        //审核不通过图片
        resultMap.put("refundImg", product.getCheckReasonPic());
        //商品状态
        resultMap.put("status", product.getStatus());

        // sku 列表
        resultMap.put("skuList", skuResultMapList);
        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 不通过理由
     *
     * @param productId
     * @return
     */
    @Override
    public SimpleResponse getCheckReason(Long productId) {
        Product product = sellerProductRepostory.selectById(productId);
        if (product == null) {
            return new SimpleResponse(4, "无此商品");
        }
        if (product.getStatus() == 1) {
            return new SimpleResponse(1, "状态错误:此商品已经上架");
        }
        if (product.getCheckStatus() == 1) {
            return new SimpleResponse(2, "状态错误:此商品已审核成功");
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("checkReason", product.getCheckReason());
        resultMap.put("checkReasonPic", product.getCheckReasonPic());
        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 分销商-设置比例
     */
    @Override
    public SimpleResponse setDistributionByDistributor(Long merchantId, Long shopId, Long productId, BigDecimal selfSupportRatio, Integer statInMarketWannaOnSale) {
        Product product = sellerProductRepostory.selectById(productId);
        if (product == null) {
            return new SimpleResponse(10, "无此商品,或此商品状态有误");
        }
        if (product.getDel() != ProductConstant.DelStatus.NOT_DELETED) {
            return new SimpleResponse(14, "商品已被删除");
        }
        //已经分销了此产品
        if (!product.getId().equals(product.getParentId())) {
            //并且此产品在上架状态
            if (product.getStatus() == ProductConstant.Status.ON_SALE) {
                return new SimpleResponse(16, "已上架商品不能修改分销比例");
            }
        }
        //如果相等=>从分销市场来的,要分销的产品
        BigDecimal platSupportRatio = product.getPlatSupportRatio();
        if (platSupportRatio == null) {
            return new SimpleResponse(17, "数据错误,平台分销佣金为空");
        }
        List<ProductSku> skuByProductId = productSkuMapper.findByProductId(product.getParentId());
        BigDecimal min = this.findPriceMinFromSku(skuByProductId);
        BigDecimal shareRatio = selfSupportRatio;
        PlatformDistRateAndPriceDTO platformDistRate = getPlatformDistRate(shopId.toString(), product.getProductSpecs(), min, platSupportRatio);
        BigDecimal priceAfterDist = platformDistRate.getPriceAfterDist();
        BigDecimal minCommission = priceAfterDist.multiply(shareRatio).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
        if (minCommission.compareTo(new BigDecimal(MIN_COMMISSION_PRICE)) < 0) {
            return new SimpleResponse(18, "需保证推广者佣金大于" + MIN_COMMISSION_PRICE + "，请重新设置分销比例");
        }

        //如果是自己的产品,
        if (merchantId.equals(product.getMerchantId()) && shopId.equals(product.getShopId())) {
            //店铺分销
            product.setSelfSupportRatio(selfSupportRatio);
            //仓库中
            product.setStatus(0);
            product.setUtime(new Date());
            int updateResult = sellerProductRepostory.update(product);
            if (updateResult != 1) {
                return new SimpleResponse(16, "设置分销比例失败");
            }
        } else {

            Map<String, Object> map = new HashMap<>();
            map.put("productId", productId);
            map.put("shopId", shopId);
            Long num = sellerGoodsDistributionMapper.getProductIdEqlParentIdNum(map);

            if (num != 0) {
                return new SimpleResponse();
            }
        }
        return new SimpleResponse(0, "success");
    }

    /***
     * 获取该店铺的商品分组
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse selectProductGroup(Long merchantId, Long shopId) {
        // TODO 进入首页是会调用这个接口，直接调用异步查询平台商城类目接口
        asyncProductService.createProductCategoryZeroRedisForShop(shopId);
        List<ProductGroup> productGroups = productGroupRepository.selectByShopIdForApplet(shopId);
        ProductGroupListDTO dto = new ProductGroupListDTO();
        dto.setGroups(new ArrayList<>());
        SimpleResponse simpleResponse = new SimpleResponse();
        //初始化一条默认全部的组返回前端
        ProductGroupDTO productGroupDTO = new ProductGroupDTO();
        productGroupDTO.setId(0L);
        productGroupDTO.setName("推荐");
        dto.getGroups().add(productGroupDTO);

        simpleResponse.setData(dto);
        if (CollectionUtils.isEmpty(productGroups)) {
            return simpleResponse;
        }
        for (ProductGroup productGroup : productGroups) {
            ProductGroupDTO productGroupDTO1 = new ProductGroupDTO();
            BeanUtils.copyProperties(productGroup, productGroupDTO1);
            dto.getGroups().add(productGroupDTO1);
        }
        simpleResponse.setData(dto);
        return simpleResponse;
    }

    /**
     * 汇总 所有商品-奖品信息,列表优化
     * 条件：审核通过上架，未进入平台分销，设置淘宝numiid，并且不是新人专区商品
     *
     * @param shopId
     * @return
     */
    @Override
    public int countAwardListForListOptimize(Long shopId) {

        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .eq(Product::getCheckStatus, ProductConstant.CheckStatus.PASS)
                .eq(Product::getDel, DelEnum.NO.getCode())
                .isNull(Product::getPlatSupportRatio)
                .isNotNull(Product::getNumIid)
                .ne(Product::getShowType, ProductShowTypeEnum.NEWBIE.getCode());
        return sellerProductRepostory.selectCount(queryWrapper);
    }

    /**
     * 根据店铺id 按id倒叙 分页查询 所有商品-奖品信息 按分组查询
     * 条件：审核通过上架，未进入平台分销，设置淘宝numiid，并且不是新人专区商品
     */
    @Override
    public List<Product> pageAwardListForListOptimize(Long shopId, int pageNum, int pageSize) {
        IPage<Product> page = new Page<>(pageNum, pageSize);
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .eq(Product::getCheckStatus, ProductConstant.CheckStatus.PASS)
                .eq(Product::getDel, DelEnum.NO.getCode())
                .isNull(Product::getPlatSupportRatio)
                .isNotNull(Product::getNumIid)
                .ne(Product::getShowType, ProductShowTypeEnum.NEWBIE.getCode());
        IPage<Product> iPage = sellerProductRepostory.selectPage(page, queryWrapper);
        return iPage.getRecords();
    }

    /**
     * 根据查询条件获取列表,20190604列表优化
     *
     * @param reqDTO
     */
    @Override
    public SimpleResponse listByConditionForListOptimize(SellerProductListReqDTO reqDTO) {
        Map<String, Object> resultMap = new HashMap<>();
        Long productIdFromReq = reqDTO.getProductId();
        if (productIdFromReq != null) {
            Product product = sellerProductRepostory.selectById(Long.valueOf(productIdFromReq.toString()));
            if (product == null || !product.getShopId().equals(reqDTO.getShopId())) {
                resultMap.put("total", 0);
                resultMap.put("list", new ArrayList<>());
                return new SimpleResponse();
            }
        }
        IPage<Product> productIPage = sellerProductRepostory.pageByParams(reqDTO);
        List<Product> productList = productIPage.getRecords();
        long recordTotal = productIPage.getTotal();
        if (CollectionUtils.isEmpty(productList)) {
            resultMap.put("total", 0);
            resultMap.put("list", new ArrayList<>());
            return new SimpleResponse();
        }

        List<Long> parentIdList = new ArrayList<>();
        Set<String> categorySidList = new HashSet<>();
        for (Product product : productList) {
            parentIdList.add(product.getParentId());
            categorySidList.add(product.getProductSpecs().toString());
        }
        // 查询类目
        List<CategoryTaobao> categoryTaobaos = categoryTaobaoRepository.selectBySidList(new ArrayList<>(categorySidList), 1);
        Map<String, CategoryTaobao> categoryTaobaoMap = new HashMap<>();
        for (CategoryTaobao categoryTaobao : categoryTaobaos) {
            categoryTaobaoMap.put(categoryTaobao.getSid(), categoryTaobao);
        }
        // 查询sku列表
        List<ProductSku> productSkuList = sellerSkuRepostory.selectByProductIdList(parentIdList);
        List<SellerProductListResultDTO> resultList = new ArrayList<>();
        for (Product product : productList) {
            SellerProductListResultDTO resultDTO = new SellerProductListResultDTO();
            resultDTO.setCategory(product.getProductSpecs());
            CategoryTaobao categoryTaobao = categoryTaobaoMap.get(product.getProductSpecs().toString());
            if (categoryTaobao != null) {
                resultDTO.setCategoryName(categoryTaobao.getName());
            }
            resultDTO.setSelfSupportRadio(product.getSelfSupportRatio());
            resultDTO.setShopDistributionCommissionRadio(product.getSelfSupportRatio());
            resultDTO.setPlatSupportRadio(product.getPlatSupportRatio());
            resultDTO.setPlatDistributionCommissionRadio(product.getPlatSupportRatio());

            resultDTO.setPriceMin(product.getProductPriceMin());
            resultDTO.setPriceMax(product.getProductPriceMax());
            resultDTO.setProductTitle(product.getProductTitle());
            resultDTO.setProductPic(getFirstPicFromMainPics(product.getMainPics()));
            resultDTO.setShowType(product.getShowType());

            // 前端产品状态 1:在售 2:审核中 3 仓库中 -1:供应商下架
            Integer status = product.getStatus();
            Integer checkStatus = product.getCheckStatus();
            String checkReason = product.getCheckReason();
            Long productId = product.getId();
            Long parentId = product.getParentId();
            resultDTO.setProductId(productId);
            resultDTO.setParentId(parentId);
            resultDTO.setStatus(status);
            /** 计算佣金 begin */
            DistributeInfoEntity productDistributeInfo = new DistributeInfoEntity();
            productDistributeInfo.setProductSpec(product.getProductSpecs());
            productDistributeInfo.setPrice(product.getProductPriceMin());
            productDistributeInfo.setSelfSupportRadio(product.getSelfSupportRatio());
            productDistributeInfo.setPlatSupportRadio(product.getPlatSupportRatio());
            ProductDistributCommissionEntity distrIncodeByProduct = distributionAppService.getDistrIncodeByProduct(productDistributeInfo);
            if (distrIncodeByProduct == null) {
                log.error("调用分销服务异常,返回为null");
                // 这是原计算方案. 如果服务调用失败, 用原算法顶上
                resultDTO.setCommissionNum(BigDecimal.ZERO);
            } else {
                resultDTO.setCommissionNum(distrIncodeByProduct.getPlatDistributCommission());
            }
            /** 计算佣金 end */
            resultDTO.setCheckStatus(checkStatus);
            resultDTO.setCheckReason(checkReason);
            resultDTO.setStatusFromShow(getStatusFromShow(productId, parentId, status, checkStatus, checkReason));

            // 分销渠道 1 店铺分销、2 店铺+平台、3 平台分销、4 无
            Object platSupportRadio = product.getPlatSupportRatio();
            Object selfSupportRadio = product.getSelfSupportRatio();
            Integer distMode = this.getDistributionMode(selfSupportRadio, platSupportRadio, productId, parentId);
            resultDTO.setDistributionMode(distMode);
            resultDTO.setDistributionTotal(0);
            // 自营商品才能获取推广量
            if (StringUtils.equalsIgnoreCase(productId.toString(), parentId.toString())) {
                resultDTO.setDistributionTotal(this.getPopularizeOrderCount(parentId, reqDTO.getShopId()));
            }

            // 如果是分销广场商品则需要隐藏平台分销属性
            if (!parentId.equals(productId)) {
                resultDTO.setPlatDistributionCommissionRadio(null);
            }
            ProductDistributionAuditLog auditLog = productDistributionAuditLogRepository.findByProductIdLimitOne(Long.parseLong(parentId.toString()));
            Map<String, Integer> stringIntegerMap = setOperateMap(product.getShowType(), productId, parentId, platSupportRadio, selfSupportRadio, auditLog);
            SellerProductOperateDTO operateDTO = new SellerProductOperateDTO();
            operateDTO.setSetDistributionByDistributor(stringIntegerMap.get("setDistributionByDistributor"));
            operateDTO.setSetDistributionByProvider(stringIntegerMap.get("setDistributionByProvider"));
            operateDTO.setSetDistributionByProviderAgain(stringIntegerMap.get("setDistributionByProviderAgain"));
            operateDTO.setStopDistributionByProvider(stringIntegerMap.get("stopDistributionByProvider"));
            resultDTO.setOperate(operateDTO);
            // 只有自营才有平台分销状态
            if (StringUtils.equals(productId.toString(), parentId.toString())) {
                // 如果有审核记录，并且当前终止分销，即 审核记录中为审核通过，但是product中无平台分销和店铺分销设置。则不设置分销状态字段auditStatus
                if (auditLog != null) {
                    resultDTO.setAuditStatus(auditLog.getStatus());
                    if (auditLog.getStatus() == ProductConstant.Distribution.Audit.Status.PASS
                            && platSupportRadio == null && selfSupportRadio == null) {
                        resultDTO.setAuditStatus(null);
                    }
                    if (auditLog.getStatus() == ProductConstant.Distribution.Audit.Status.NOT_PASS) {
                        resultDTO.setCheckReason(auditLog.getAuditReason());
                        resultDTO.setCheckReasonPic(StringUtils.isNotBlank(auditLog.getAuditPics()) ? JSONArray.parseArray(auditLog.getAuditPics(), String.class) : new ArrayList<>());
                    }
                }
            }
            resultDTO.setOrigin(product.getFromFlag());
            // 销量
            Integer stockDef = 0;
            Integer stock = 0;
            for (ProductSku productSku : productSkuList) {
                if (productSku.getProductId().equals(product.getParentId())) {
                    stockDef += productSku.getStockDef();
                    stock += productSku.getStock();
                }
            }
            resultDTO.setSalesVolume(stockDef - stock);
            resultDTO.setStockTotal(stock);
            resultList.add(resultDTO);

            // 抖带带-售罄状态
            if (reqDTO.getOnSale() == ProductConstant.Status.DDD_ON_SALE && stock == 0) {
                resultDTO.setStatus(ProductConstant.Status.SELL_OUT);
            }
        }

        resultMap.put("total", recordTotal);
        resultMap.put("list", resultList);
        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 根据查询条件获取商户后台审核列表,20190604列表优化
     *
     * @param conditionMap
     * @param pageCurrent
     * @param pageSize
     */
    @Override
    public SimpleResponse checkListByConditionForListOptimize(Map<String, Object> conditionMap, int pageCurrent, int pageSize) {
        /**
         *         conditionMap.put("checkResult", checkResult);// 0:全部 1:审核通过 2:审核不通过
         *         conditionMap.put("type", type);//1:待审核 2:已审核
         */
        /*
         * 查询是否有店铺
         */
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        if (conditionMap.get("productId") != null && StringUtils.isNotBlank(conditionMap.get("productId").toString())) {
            Long productId = Long.valueOf(conditionMap.get("productId").toString());
            queryWrapper.lambda().eq(Product::getId, productId);
        } else {
            if (conditionMap.get("shopName") != null && StringUtils.isNotBlank(conditionMap.get("shopName").toString())) {
                List<Long> shopIdListByShopName = shopAppService.getShopIdListByShopName(conditionMap.get("shopName").toString());
                if (!CollectionUtils.isEmpty(shopIdListByShopName)) {
                    queryWrapper.lambda().in(Product::getShopId, shopIdListByShopName);
                }
            }
            if (conditionMap.get("phone") != null && StringUtils.isNotBlank(conditionMap.get("phone").toString())) {
                Long merchantIdByPhone = shopAppService.getMerchantIdByPhone(conditionMap.get("phone").toString());
                queryWrapper.lambda().eq(Product::getMerchantId, merchantIdByPhone);
            }
            if (conditionMap.get("timeBegin") != null && StringUtils.isNotBlank(conditionMap.get("timeBegin").toString())
                    && conditionMap.get("timeEnd") != null && StringUtils.isNotBlank(conditionMap.get("timeEnd").toString())) {
                queryWrapper.lambda().between(Product::getSubmitTime, conditionMap.get("timeBegin"), conditionMap.get("timeEnd"));
            }
        }
        int type = Integer.valueOf(conditionMap.get("type").toString());
        if (type == 1) {
            queryWrapper.lambda().eq(Product::getCheckStatus, ProductConstant.CheckStatus.CHECKING);
            queryWrapper.lambda().orderByAsc(Product::getSubmitTime);
        }
        if (type == 2) {

            if (conditionMap.get("checkResult") == null && StringUtils.isNotBlank(conditionMap.get("checkResult").toString())) {
                queryWrapper.lambda().ne(Product::getCheckStatus, ProductConstant.CheckStatus.CHECKING);
            } else {
                int checkResult = Integer.valueOf(conditionMap.get("checkResult").toString());
                if (checkResult == 1) {
                    queryWrapper.lambda().eq(Product::getCheckStatus, ProductConstant.CheckStatus.PASS);
                }
                if (checkResult == 2) {
                    queryWrapper.lambda().eq(Product::getCheckStatus, ProductConstant.CheckStatus.NOT_PASS);
                }
            }
            queryWrapper.lambda().orderByDesc(Product::getCheckTime);
        }

        Page<Product> page = new Page<>(pageCurrent, pageSize);
        IPage<Product> productIPage = sellerProductRepostory.selectPage(page, queryWrapper);
        long pagingCountTotal = productIPage.getTotal();
        List<Product> productList = productIPage.getRecords();
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (CollectionUtils.isEmpty(productList)) {
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("total", pagingCountTotal);
            resultMap.put("list", resultList);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultMap);
        }
        Set<Long> parentIdList = new HashSet<>();
        Set<Long> shopIdList = new HashSet<>();
        Set<String> categorySidList = new HashSet<>();
        for (Product product : productList) {
            parentIdList.add(product.getParentId());
            shopIdList.add(product.getShopId());
            categorySidList.add(product.getProductSpecs().toString());
        }
        List<ProductSku> productSkuList = sellerSkuRepostory.selectByProductIdList(new ArrayList<>(parentIdList));
        List<ShopDetailForPlatformDTO> shopListByShopIdList = shopAppService.getShopListByShopIdList(new ArrayList<>(shopIdList));
        List<CategoryTaobao> categoryTaobaoList = categoryTaobaoRepository.selectBySidList(new ArrayList<>(categorySidList), 1);
        for (Product product : productList) {
            Map<String, Object> dtoMap = new HashMap<>();
            dtoMap.put("productId", product.getId());
            dtoMap.put("productPic", this.getFirstPicFromMainPics(product.getMainPics()));
            dtoMap.put("productTitle", product.getProductTitle());
            dtoMap.put("priceMin", product.getProductPriceMin());
            dtoMap.put("priceMax", product.getProductPriceMax());
            dtoMap.put("checkStatus", product.getCheckStatus());
            dtoMap.put("checkReason", product.getCheckReason());
            if (product.getCheckTime() != null) {
                dtoMap.put("checkTime", DateUtil.dateToString(product.getCheckTime(), DateUtil.FORMAT_TWO));
            }
            dtoMap.put("commitTime", "");
            if (product.getSubmitTime() != null) {
                dtoMap.put("commitTime", DateUtil.dateToString(product.getSubmitTime(), DateUtil.FORMAT_TWO));
            }
            dtoMap.put("status", product.getStatus());
            for (ShopDetailForPlatformDTO platformDTO : shopListByShopIdList) {
                if (product.getShopId().equals(platformDTO.getId())) {
                    dtoMap.put("shopName", platformDTO.getShopName());
                    dtoMap.put("phone", platformDTO.getMerchantPhone());
                    break;
                }
            }
            for (CategoryTaobao categoryTaobao : categoryTaobaoList) {
                if (StringUtils.equals(product.getProductSpecs().toString(), categoryTaobao.getSid())) {
                    dtoMap.put("categoryName", categoryTaobao.getName());
                    break;
                }
            }
            // 销量
            Integer stock = 0;
            for (ProductSku productSku : productSkuList) {
                if (productSku.getProductId().equals(product.getParentId())) {
                    stock += productSku.getStock();
                }
            }
            dtoMap.put("stockTotal", stock);
            resultList.add(dtoMap);
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", pagingCountTotal);
        resultMap.put("list", resultList);
        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 根据id获取商品
     *
     * @param productId
     * @return
     */
    @Override
    public Product findById(Long productId) {
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Product::getId, productId);
        return goodsService.getOne(queryWrapper);
    }

    /**
     * 根据sid查询父节点
     *
     * @param productSpecs
     * @return
     */
    @Override
    public String getParentSpecBySpec(Long productSpecs) {
        QueryWrapper<CategoryTaobao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(CategoryTaobao::getSid, productSpecs);
        CategoryTaobao categoryTaobao = categoryTaobaoService.getOne(queryWrapper);
        return categoryTaobao.getParentId();
    }

    /**
     * 删除商品后,增加在选货列表中增加此商品
     *
     * @param productId
     */
    @Override
    public void addZsetCacheByProductId(Long productId) {

    }

    private BigDecimal findPriceMinFromSku(List<ProductSku> list) {
        BigDecimal min = new BigDecimal(9999999);
        for (ProductSku sku : list) {
            BigDecimal skuSellingPrice = sku.getSkuSellingPrice();
            if (min.compareTo(skuSellingPrice) > 0) {
                min = skuSellingPrice;
            }
        }
        return min;
    }

    /**
     * 根据产品id查询产品,并根据dto修改可能变更内容
     *
     * @param dto .
     * @return .
     */
    private Product editProduct(ProductDTO dto) {
        Date now = new Date();
        Product product = sellerProductRepostory.selectById(dto.getProductId());
        if (product == null) {
            log.error("product is null");
            return null;
        }
        log.info("<<<<<<<<<<<<<<<商品参数修改前数据 :{}", product);
        if (product.getCheckStatus() == 0) {
            return null;
        }
        Integer putaway = dto.getPutaway();
        if (putaway == 1) {
            Integer productStatus = product.getStatus();
            //如果未上架,就提交上架审核
            if (productStatus != ProductConstant.Status.ON_SALE) {
                //提交时间
                product.setSubmitTime(now);
                //审核中状态
                product.setCheckStatus(ProductConstant.CheckStatus.CHECKING);
            }
        } else {
            // status=0 and check_status=-1 (check_status只能是-1,如果是check_status=0,这时是不能修改的)
            product.setCheckStatus(ProductConstant.CheckStatus.NOT_COMMIT);
        }
        product.setProductTitle(dto.getProductTitle());
        product.setProductSpecs(dto.getProductSpecs());
        product.setMainPics(dto.getMainPics());
        product.setDetailPics(dto.getDetailPics());
        product.setUtime(now);
        if (dto.getInitSales() != null) {
            Assert.isTrue(dto.getInitSales() >= 0, 11, "初始销量请设置为正整数");
            product.setInitSales(dto.getInitSales());
        }
        if (dto.getNumIid() != null && NumberValidationUtil.isNumeric(dto.getNumIid())) {
            product.setNumIid(dto.getNumIid());
        }
        log.info("<<<<<<<<<<<<<<<商品参数修改后数据 :{}", product);
        return product;

    }

    /**
     * 创建商品
     *
     * @param productDTO .
     * @return .
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse create(ProductDTO productDTO) {
        Date now = new Date();
        Product product = this.getProductFromDto(productDTO, now);

        int saveProductResult = sellerProductRepostory.create(product);
        if (saveProductResult != 1) {
            log.error("插入product表失败,merchantId={},shopId={},当前时间是={}", productDTO.getMerchantId(), productDTO.getShopId(), now);
            return new SimpleResponse(10, "插入产品错误");
        }
        // 补齐parent_id
        product.setParentId(product.getId());
        //设置商品的属性：0，无属性，1：自营，2：分销
        product.setFromFlag(ProductFromFlagEnum.SELF.getCode());

        int updateProductResult = sellerProductRepostory.updateBySelective(product);
        if (updateProductResult != 1) {
            // 使用事务回滚
            Assert.isTrue(false, -1, "插入product记录后,update parent_id错误,回滚");
        }
        //插入sku
        this.handlerCreateSku(productDTO, now, product);
        return new SimpleResponse(0, "success");
    }

    /**
     * 处理创建
     *
     * @param productDTO
     * @param now
     * @param product
     */
    private void handlerCreateSku(ProductDTO productDTO, Date now, Product product) {
        List<ProductSkuDTO> skuDtoList = productDTO.getProductSkuDTOList();
        for (ProductSkuDTO skuDto : skuDtoList) {
            ProductSku skuBean = new ProductSku();
            skuBean.setCtime(now);
            skuBean.setMerchantId(product.getMerchantId());
            skuBean.setShopId(product.getShopId());
            //商品id
            skuBean.setProductId(product.getId());
            //库存
            Integer stock = skuDto.getStock();
            if (stock < 0) {
                Assert.isTrue(false, 100, "sku中库存错误");
            }
            skuBean.setStockDef(stock);
            skuBean.setStock(stock);

            skuBean = this.getSkuBeanCommon(skuDto, skuBean, now);

            int insertSkuResult = productSkuMapper.insertSelective(skuBean);
            if (insertSkuResult != 1) {
                //如果sku插入失败,就标记product为删除状态
                log.error("插入sku错误,productId={}", product.getId());
                Assert.isTrue(false, 202, "新增sku发生错误");
            }
        }

        /***add 修改product中的价格相关的属性 begin******/
        ProductSkuInfoDTO dto = productSkuMapper.getProductSkuPriceInfo(product.getId());
        int count = sellerGoodsDistributionMapper.updateProductPriceInfoById(dto);
        if (count < 1) {
            log.error("修改product中的价格相关的属性错误,productId={}", product.getId());
            Assert.isTrue(false, 202, "新增sku发生错误");
        }
        /***add 修改product中的价格相关的属性 end********/
    }

    /**
     * 创建/修改(这是修改整个商品的业务,单独修改库存/价格的不实用此方法) sku处理
     *
     * @param now        当前时间
     * @param productDTO 产品dto
     * @param product    产品对象
     */
    private void handleUpdateSku(Date now, ProductDTO productDTO, Product product) {
        //将参数中sku转为  bean
        List<ProductSkuDTO> skuDtoList = productDTO.getProductSkuDTOList();
        for (ProductSkuDTO skuDto : skuDtoList) {
            ProductSku skuById = productSkuMapper.selectByPrimaryKey(skuDto.getSkuId());
            //如果是修改的sku
            if (skuById != null) {
                BigDecimal skuSellingPriceDB = skuById.getSkuSellingPrice();
                BigDecimal skuSellingPriceFront = skuDto.getSkuSellingPrice();
                //如果价格修改了
                if (skuSellingPriceDB.compareTo(skuSellingPriceFront) != 0) {
                    //走创建快照业务
                    goodsSkuService.updatePriceAndStock(skuById, skuDto, now, product, skuSellingPriceFront);
                    /***修改商品的价格时，分销商品的佣金也会被修改 begin***/
                    //获取佣金
                    if (null != product) {
                        BigDecimal platSupportRatios = product.getPlatSupportRatio();
                        if (platSupportRatios != null) {
                            List<ProductSku> skuByProductId = productSkuMapper.findByProductId(product.getId());
                            BigDecimal min = this.findPriceMinFromSku(skuByProductId);
                            log.info(">>>>>>>>>>>>>>>>have find  min price =" + min);
                            PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(product.getShopId().toString(), product.getProductSpecs(), min, platSupportRatios);
                            //供应商的抽佣比例（去除平台固定抽佣比例）
                            BigDecimal platSupportRatio = platformDistRate.getPlatSupportRatio();
                            BigDecimal commissionNum = min.multiply(platSupportRatio).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
                            //根据productID 修改product信息
                            Map<String, Object> params = new HashMap<>();
                            params.put("productId", product.getId());
                            params.put("commissionNum", commissionNum);
                            int count = sellerProductRepostory.updateByProductId(params);
                            if (1 != count) {
                                continue;
                            }
                        }
                    }
                    /***修改商品的价格时，分销商品的佣金也会被修改 end***/

                } else {
                    //直接修改
                    this.updateSku(skuById, skuDto, now, product);
                }
            } else {
                //此sku为新加的
                this.handlerCreateSku(productDTO, now, product);
            }
        }
    }


    /**
     * 修改sku
     *
     * @param skuById
     * @param skuDto
     * @param now
     * @param product
     */
    private void updateSku(ProductSku skuById, ProductSkuDTO skuDto, Date now, Product product) {
        // 原库存
        Integer stockOld = skuById.getStock();
        // 商户修改的库存
        Integer stockNew = skuDto.getStock();
        if (stockNew < 0) {
            Assert.isTrue(false, 105, "库存错误,库存为正整数");
        }
        skuById.setStock(stockNew);
        skuById.setStockDef(this.calcStockDefForUpdate(skuById.getStockDef(), stockNew, stockOld));
        skuById = this.getSkuBeanCommon(skuDto, skuById, now);

        String redisKey = "mf.code.product:sku:updateStock:SKU_ID" + skuById.getId();
        if (RedisForbidRepeat.NX(stringRedisTemplate, redisKey, 30, TimeUnit.SECONDS)) {
            int updateSkuResult = productSkuMapper.updateByPrimaryKeySelective(skuById);
            if (updateSkuResult != 1) {
                //如果sku插入失败,就标记product为删除状态
                log.error("修改sku错误,productId={},SKU_ID={}", product.getId(), skuById.getId());
                Assert.isTrue(false, 201, "修改sku发生错误");
            }
            stringRedisTemplate.delete(redisKey);
        } else {
            // TODO: 2019/4/10 0010 可以细化  => 将没用问题的sku修改成功,有锁的修改不成功
            log.info("修改库存并发锁生效");
            Assert.isTrue(false, 11, "网络繁忙,请稍后操作");
        }
    }

    /**
     * 计算库存定义的新值
     *
     * @param stockDefOld
     * @param stockNew
     * @param stockOld
     * @return
     */
    public Integer calcStockDefForUpdate(int stockDefOld, int stockNew, int stockOld) {
        return stockDefOld + (stockNew - stockOld);
    }

    /**
     * 获取一个sku的bean 创建和修改通用
     *
     * @param skuDto
     * @param productSkuByDto
     * @param now
     * @return
     */
    private ProductSku getSkuBeanCommon(ProductSkuDTO skuDto, ProductSku productSkuByDto, Date now) {
        //售价
        BigDecimal skuSellingPrice = skuDto.getSkuSellingPrice();
        //图片
        String skuPic = skuDto.getSkuPic();
        productSkuByDto.setSkuPic(skuPic);
        if (skuSellingPrice.compareTo(BigDecimal.ZERO) < 0) {
            Assert.isTrue(false, 101, "sku中销售价有误");
        }
        productSkuByDto.setSkuSellingPrice(skuSellingPrice);
        //淘宝价
        BigDecimal skuThirdPrice = skuDto.getSkuThirdPrice();
        if (skuThirdPrice.compareTo(BigDecimal.ZERO) < 0) {
            Assert.isTrue(false, 102, "sku中淘宝售价有误");
        }
        productSkuByDto.setSkuThirdPrice(skuThirdPrice);
        //快照
        productSkuByDto.setSnapshoot(0);
        // 是否显示.
        productSkuByDto.setNotShow(skuDto.getSnapshoot());

        // 生成sku顺序
        // 生成 指标(横向)顺序
        productSkuByDto.setUtime(now);
        /**
         * <pre>
         *     商品的属性的横向排序值 : 类目id1:类目值1,类目id2:类目值2
         * </pre>
         */
        String propsJson = skuDto.getPropsJson();
        productSkuByDto.setPropsJson(propsJson);
        List<ProductSkuSalePropDTO> propsList = skuDto.parsePropListFromString(skuDto.getPropsJson());
        if (propsList == null) {
            //失败处理
            Assert.isTrue(false, 201, "销售属性不能为空");
        }
        productSkuByDto.setSpecSort(this.getSpecSort(propsList));
        /**
         * <pre>
         *     sku排序值: 类目id1类目id2类目id3
         * </pre>
         */
        String skuSort = null;
        Integer placeholder = 1000000000;
        NumberFormat nf = NumberFormat.getInstance();
        for (ProductSkuSalePropDTO propDto : propsList) {
            String specIdStr = propDto.getProductSpecsId().toString();
            Long categoryId = propDto.getCategoryId();
            if (specIdStr.length() < 9) {
                specIdStr = categoryId + nf.format(placeholder / (Math.pow(10, specIdStr.length()))) + specIdStr;
            }
            skuSort = skuSort == null ? specIdStr : skuSort + specIdStr;
        }

        productSkuByDto.setSkuSort(skuSort);
        return productSkuByDto;
    }

    /**
     * 生成商品的属性的横向排序值
     *
     * @param propsList 销售属性数组
     * @return 排序值
     */
    private String getSpecSort(List<ProductSkuSalePropDTO> propsList) {
        String specSort = null;
        for (ProductSkuSalePropDTO props : propsList) {
            Long categoryId = props.getCategoryId();
            Long productSpecsId = props.getProductSpecsId();
            specSort = specSort == null ? "" : specSort + ",";
            specSort = specSort + categoryId + ":" + productSpecsId;
        }
        return specSort;
    }

    /**
     * 在dto中获取Product对象
     *
     * @param dto 产品DTO
     * @param now 当前时间
     * @return 产品对象
     */
    private Product getProductFromDto(ProductDTO dto, Date now) {
        Product product = new Product();
        // 商户 店铺
        Long shopId = dto.getShopId();
        Long merchantIdByShopId = shopAppService.getMerchantIdByShopId(shopId);
        product.setMerchantId(merchantIdByShopId);
        dto.setMerchantId(merchantIdByShopId);
        product.setShopId(shopId);
        // 标题
        product.setProductTitle(dto.getProductTitle());
        // 类目
        product.setProductSpecs(dto.getProductSpecs());
        // 图片
        product.setMainPics(dto.getMainPics());
        product.setDetailPics(dto.getDetailPics());
        // 审核状态 如果点上架按钮: check_status=0 如果点仓库按钮 check_status=-1
        Integer putaway = dto.getPutaway();
        if (putaway == 1) {
            product.setCheckStatus(ProductConstant.CheckStatus.CHECKING);
            product.setSubmitTime(now);
        } else {
            product.setCheckStatus(ProductConstant.CheckStatus.NOT_COMMIT);
        }
        product.setStatus(ProductConstant.Status.NOT_ON_SALE);
        // 初始销量
        if (dto.getInitSales() != null) {
            Assert.isTrue(dto.getInitSales() >= 0, 11, "初始销量请设置为正整数");
            product.setInitSales(dto.getInitSales());
        }
        if (dto.getNumIid() != null && NumberValidationUtil.isNumeric(dto.getNumIid())) {
            product.setNumIid(dto.getNumIid());
        }
        product.setDel(0);
        // 时间
        product.setCtime(now);
        product.setUtime(now);
        return product;
    }

    /**
     * 根据查询条件获取列表
     *
     * @return .
     */
    @Override
    public SimpleResponse listByCondition(SellerProductListReqDTO reqDTO) {

        Map<String, Object> resultMap = new HashMap<>();
        Long productIdFromReq = reqDTO.getProductId();
        if (productIdFromReq != null) {
            Product product = sellerProductRepostory.selectById(Long.valueOf(productIdFromReq.toString()));
            if (product == null || !product.getShopId().equals(reqDTO.getShopId())) {
                resultMap.put("total", 0);
                resultMap.put("list", new ArrayList<>());
                return new SimpleResponse();
            }
        }

        int recordTotal = sellerProductRepostory.countByConditions(BeanMapUtil.beanToMap(reqDTO));
        List<Map<String, Object>> resultList = sellerProductRepostory.findByConditions(BeanMapUtil.beanToMap(reqDTO));

        List<Long> productIdList = new ArrayList<>();

        for (Map<String, Object> m : resultList) {
            Object productId = m.get("productId");
            Assert.isTrue(productId != null, 90, "产品id为空,不存在此商品");
            // 前端产品状态 1:在售 2:审核中 3 仓库中
            Object statusObj = m.get("status");
            Object checkStatusObj = m.get("checkStatus");
            Object checkReason = m.get("checkReason");
            Object parentId = m.get("parentId");
            m.put("statusFromShow", this.getStatusFromShow(productId, m.get("parentId"), statusObj, checkStatusObj, checkReason));

            /*
             * 分销渠道 1 店铺分销、2 店铺+平台、3 平台分销、4 无
             * if(IFNULL(p.self_support_ratio,-1)=-1,0,1) isSelfSupport,
             * if(IFNULL(p.plat_support_ratio,-1)=-1,0,1) isPlatSupport,
             */
            Object platSupportRadio = m.get("platSupportRadio");
            Object selfSupportRadio = m.get("selfSupportRadio");
            Integer distMode = this.getDistributionMode(selfSupportRadio, platSupportRadio, productId, parentId);
            m.put("distributionMode", distMode);
            // 推广量
            productIdList.add(Long.parseLong(parentId.toString()));
            m.put("distributionTotal", 0);
            // 自营商品才能获取推广量
            if (StringUtils.equalsIgnoreCase(productId.toString(), parentId.toString())) {
                m.put("distributionTotal", this.getPopularizeOrderCount(parentId, reqDTO.getShopId()));
            }

            Object productPic = m.get("productPic");
            Assert.isTrue(productPic != null, 91, "获取到产品列表,但产品id=?的主图为空".replace("?", productId.toString()));
            m.put("productPic", this.getFirstPicFromMainPics(productPic));

            // 如果是分销广场商品则需要隐藏平台分销属性
            if (!Long.valueOf(parentId.toString()).equals(Long.valueOf(productId.toString()))) {
                m.put("platDistributionCommissionRadio", null);
            }
            ProductDistributionAuditLog auditLog = productDistributionAuditLogRepository.findByProductIdLimitOne(Long.parseLong(parentId.toString()));
            Integer showType = m.get("showType") == null ? 0 : new Integer(m.get("showType").toString());
            Map<String, Integer> stringIntegerMap = this.setOperateMap(showType, Long.parseLong(productId.toString()), Long.parseLong(parentId.toString()), platSupportRadio, selfSupportRadio, auditLog);
            m.put("operate", stringIntegerMap);
            // 只有自营才有平台分销状态
            if (StringUtils.equals(productId.toString(), parentId.toString())) {
                // 如果有审核记录，并且当前终止分销，即 审核记录中为审核通过，但是product中无平台分销和店铺分销设置。则不设置分销状态字段auditStatus
                if (auditLog != null) {
                    m.put("auditStatus", auditLog.getStatus());
                    if (auditLog.getStatus() == ProductConstant.Distribution.Audit.Status.PASS
                            && platSupportRadio == null && selfSupportRadio == null) {
                        m.remove("auditStatus");
                    }
                    if (auditLog.getStatus() == ProductConstant.Distribution.Audit.Status.NOT_PASS) {
                        m.put("checkReason", auditLog.getAuditReason());
                        m.put("checkReasonPic", StringUtils.isNotBlank(auditLog.getAuditPics()) ? JSONArray.parseArray(auditLog.getAuditPics(), String.class) : new ArrayList<>());
                    }
                }
            }
        }

        resultMap.put("total", recordTotal);
        resultMap.put("list", resultList);
        return new SimpleResponse(0, "success", resultMap);
    }

    private Map<String, Integer> setOperateMap(Integer showType, Long id, Long parentId, Object platSupportRadio, Object selfSupportRadio, ProductDistributionAuditLog auditLog) {
        Map<String, Integer> operateMap = new HashMap<>();
        //来源-自营创建
        if (id.equals(parentId)) {
            // 有分销审核，并且审核状态不是通过， 只有建立推广
            if (auditLog != null && auditLog.getStatus() != ProductConstant.Distribution.Audit.Status.PASS) {
                //供应商-建立推广
                operateMap.put("setDistributionByProvider", 1);
                //供应商-停止
                operateMap.put("stopDistributionByProvider", 0);
                //供应商-分销设置
                operateMap.put("setDistributionByProviderAgain", 0);
                // 如果设置里有自营分销，并且平台分销不通过审核，则需要提供修改和终止功能
                if (selfSupportRadio != null) {
                    //供应商-建立推广
                    operateMap.put("setDistributionByProvider", 0);
                    //供应商-停止
                    operateMap.put("stopDistributionByProvider", 1);
                    //供应商-分销设置
                    operateMap.put("setDistributionByProviderAgain", 1);
                }
            } else {
                //平台分销比例都为空
                if (platSupportRadio == null && selfSupportRadio == null) {
                    //供应商-建立推广
                    operateMap.put("setDistributionByProvider", 1);
                    //供应商-停止
                    operateMap.put("stopDistributionByProvider", 0);
                    //供应商-分销设置
                    operateMap.put("setDistributionByProviderAgain", 0);
                } else if (platSupportRadio != null && selfSupportRadio != null) {
                    //供应商-建立推广
                    operateMap.put("setDistributionByProvider", 0);
                    //供应商-停止
                    operateMap.put("stopDistributionByProvider", 1);
                    //供应商-分销设置
                    operateMap.put("setDistributionByProviderAgain", 0);
                } else {
                    //供应商-建立推广
                    operateMap.put("setDistributionByProvider", 0);
                    //供应商-停止
                    operateMap.put("stopDistributionByProvider", 1);
                    //供应商-分销设置
                    operateMap.put("setDistributionByProviderAgain", 1);
                }
            }
            //分销商按钮
            operateMap.put("setDistributionByDistributor", 0);
            if (showType == ProductShowTypeEnum.NEWBIE.getCode()) {
                operateMap.put("setDistributionByProvider", 0);
            }
        } else {//来源-分销市场
            //供应商-设置
            operateMap.put("setDistributionByProvider", 0);
            //供应商-停止
            operateMap.put("stopDistributionByProvider", 0);
            //供应商-分销设置
            operateMap.put("setDistributionByProviderAgain", 0);
            //分销商-设置
            if (selfSupportRadio == null) {
                operateMap.put("setDistributionByDistributor", 1);
            } else {
                operateMap.put("setDistributionByDistributor", 0);
            }
        }
        return operateMap;
    }

    /**
     * 一个产品的分销订单数量 -- 先不管分销业务
     *
     * @param productIdObj
     * @param shopIdObj
     * @return
     */
    private Integer getPopularizeOrderCount(Object productIdObj, Object shopIdObj) {
        Assert.isTrue(productIdObj != null, -1, "product服务调用order服务前,参数校验异常,产品id:productIdObj=null");
        Assert.isTrue(shopIdObj != null, -1, "product服务调用order服务前,参数校验异常,店铺id:shopIdObj=null");
        String productId = productIdObj.toString();
        String shopId = shopIdObj.toString();
        Assert.isLong(productId, -2, "product服务调用order服务前,参数校验异常,产品id:productIdObj不能转为Long类型");
        Assert.isLong(shopId, -2, "product服务调用order服务前,参数校验异常,产品id:shopIdObj不能转为Long类型");
        int popularizeOrderCount = 0;
        try {
            List<ProductSku> productSkuList = sellerSkuRepostory.selectByProductId(Long.valueOf(productId));
            if (CollectionUtils.isEmpty(productSkuList)) {
                return popularizeOrderCount;
            }
            List<Long> skuList = new ArrayList<>();
            for (ProductSku productSku : productSkuList) {
                skuList.add(productSku.getId());
            }
            popularizeOrderCount = orderAppService.summaryOrderByShopIdAndProductId(Long.valueOf(shopId), skuList);
        } catch (RuntimeException e) {
            log.error("produc服务调用order服务的orderAppService.summaryOrderByShopIdAndProductId(shopId,productId)异常");
            log.error(e.getMessage());
        }
        return popularizeOrderCount;
    }

    /**
     * 获取主图第一张
     *
     * @param pics
     * @return
     */
    private String getFirstPicFromMainPics(Object pics) {
        if (pics == null) {
            return null;
        }
        String mainPicStr = pics.toString();
        List<String> list = JSON.parseArray(mainPicStr, String.class);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }


    /**
     * 计算商品上架/审核中/仓库中/审核失败状态
     *
     * @param statusObj
     * @param checkStatusObj
     * @param checkReason
     * @return
     */
    private Integer getStatusFromShow(Object productId, Object parentId, Object statusObj, Object checkStatusObj, Object checkReason) {
        int status = statusObj == null ? 0 : Integer.parseInt(statusObj.toString());
        int checkStatus = checkStatusObj == null ? -1 : Integer.parseInt(checkStatusObj.toString());
        int statusFromShow = 0;
        // 暂时添加判断如果状态为-1，则当前商品为供应商下架，只能删除操作
        if (status == ProductConstant.Status.PLAT_OFF_SHELF) {
            return ProductConstant.StatusFrontShow.PLAT_OFF_SHELF;
        }
        //自营创建
        if (StringUtils.equalsIgnoreCase(productId.toString(), parentId.toString())) {
            if (status == ProductConstant.Status.NOT_ON_SALE && checkStatus == ProductConstant.CheckStatus.NOT_COMMIT) {
                // status=0 check_status=-1 => 仓库中
                if (checkReason == null || "".equals(checkReason.toString())) {
                    statusFromShow = ProductConstant.StatusFrontShow.IN_STORE;
                } else {
                    statusFromShow = ProductConstant.StatusFrontShow.CHECK_FAILD;
                }
            } else if (status == ProductConstant.Status.NOT_ON_SALE && checkStatus == ProductConstant.CheckStatus.CHECKING) {
                // status=0 check_status=0 => 仓库 待审核
                statusFromShow = ProductConstant.StatusFrontShow.CHECKING;
            } else if (status == ProductConstant.Status.ON_SALE && checkStatus == ProductConstant.CheckStatus.PASS) {
                // status=1 check_status=1 => 上架
                statusFromShow = ProductConstant.StatusFrontShow.ON_SALE;
            }
        } else {
            //分销市场
            if (status == ProductConstant.Status.NOT_ON_SALE) {
                statusFromShow = ProductConstant.StatusFrontShow.IN_STORE;
            } else {
                statusFromShow = ProductConstant.StatusFrontShow.ON_SALE;
            }
        }
        return statusFromShow;
    }

    /**
     * 计算分销模式
     *
     * @param selfSupportRadio
     * @param platSupportRadio
     * @return
     */
    private Integer getDistributionMode(Object selfSupportRadio, Object platSupportRadio, Object productId, Object parentId) {
        /*
         *              * if(IFNULL(p.self_support_ratio,-1)=-1,0,1) isSelfSupport,
         *              * if(IFNULL(p.plat_support_ratio,-1)=-1,0,1) isPlatSupport,
         */
        int distributionMode = 0;
        if (selfSupportRadio == null && platSupportRadio == null) {
            distributionMode = ProductConstant.DistributionMode.NONE;
        } else if (selfSupportRadio == null && platSupportRadio != null) {
            distributionMode = ProductConstant.DistributionMode.PLAT;
        } else if (selfSupportRadio != null && platSupportRadio == null) {
            distributionMode = ProductConstant.DistributionMode.SHOP;
        } else if (selfSupportRadio != null && platSupportRadio != null) {
            distributionMode = ProductConstant.DistributionMode.SHOP_AND_PLAT;
            // 含泪修改，这里还需要根据是否为分销市场商品，显示店铺分销
            if (!productId.toString().equals(parentId.toString())) {
                distributionMode = ProductConstant.DistributionMode.SHOP;
            }
        }
        return distributionMode;
    }

    /**
     * 上架
     *
     * @param productId  .
     * @param merchantId .
     * @param shopId     .
     * @return .
     */
    @Override
    public SimpleResponse upShelf(Long productId, Long merchantId, Long shopId) {
        Product product = sellerProductRepostory.selectById(productId);
        SimpleResponse error = this.checkoutProductBelongError(product, merchantId, shopId);
        if (error != null) {
            return error;
        }
        if (product.getStatus() == ProductConstant.Status.ON_SALE) {
            return new SimpleResponse(13, "此商品已上架");
        }
        Date now = new Date();
        product.setUtime(now);
        if (product.getCheckStatus() != null && product.getCheckStatus() == ProductConstant.CheckStatus.PASS) {
            product.setStatus(ProductConstant.Status.ON_SALE);
        } else {
            product.setCheckStatus(ProductConstant.CheckStatus.CHECKING);
            product.setSubmitTime(now);
            //这时候还未上架 , 待审核通过才上架
            Integer productStatus = product.getStatus();
            if (productStatus == 1) {
                //上架的商品依然可以修改,前端并无其他按钮来标识这个商品是已经上架的商品的修改,所以这里保留原有的产品上架状态status=1
                product.setStatus(ProductConstant.Status.ON_SALE);
            } else {
                //如果商品未上架,status=0
                product.setStatus(ProductConstant.Status.NOT_ON_SALE);
            }
        }
        int putawayResult = sellerProductRepostory.updateBySelective(product);
        if (putawayResult == 1) {
            return new SimpleResponse(0, "success");
        }
        log.error("修改上架状态失败,shopId={},productId={}", shopId, productId);
        return new SimpleResponse(7, "修改商家状态失败");
    }

    /**
     * 校验该商品属于此商户和店铺
     *
     * @param product    产品
     * @param merchantId 商户
     * @param shopId     店铺
     * @return SimpleResponse
     */
    private SimpleResponse checkoutProductBelongError(Product product, Long merchantId, Long shopId) {
        if (product == null) {
            return new SimpleResponse(10, "无此商品,或此商品状态有误");
        }

        if (!product.getShopId().equals(shopId)) {
            return new SimpleResponse(12, "此商品并不属于此店铺");
        }
        return null;
    }

    /**
     * 下架
     *
     * @param productId  .
     * @param merchantId .
     * @param shopId     .
     * @return .
     */
    @Override
    public SimpleResponse downshelf(Long productId, Long merchantId, Long shopId) {
        Product product = sellerProductRepostory.selectById(productId);
        SimpleResponse error = this.checkoutProductBelongError(product, merchantId, shopId);
        if (error != null) {
            return error;
        }
        if (product.getStatus() != ProductConstant.Status.ON_SALE) {
            return new SimpleResponse(13, "此商品未上架,因此不能下架");
        }
        product.setUtime(new Date());
        product.setCheckStatus(ProductConstant.CheckStatus.NOT_COMMIT);
        product.setStatus(ProductConstant.Status.NOT_ON_SALE);
        product.setCheckReason(null);
        product.setCheckTime(null);
        product.setSubmitTime(null);
        product.setPutawayTime(null);
        //佣金比例置空
        product.setSelfSupportRatio(null);
        product.setPlatSupportRatio(null);

        /**设置佣金begin**/
        product.setCommissionNum(null);
        /**设置佣金end**/

        // 如果是分销商操作分销商品下架则直接设置状态为供应商下架，方便操作，但是也造成不能再次上架
        if (!product.getId().equals(product.getParentId())) {
            product.setStatus(ProductConstant.Status.PLAT_OFF_SHELF);
        }
        /**add 当商品是自营产品时，且具有分销属性，下架需要去掉缓存begin***/
        if (product.getFromFlag() == ProductFromFlagEnum.SELF.getCode()) {
            this.stringRedisTemplate.opsForZSet().remove(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, product.getId().toString());
        }
        /**add 当商品是自营产品时，且具有分销属性，下架需要去掉缓存begin***/
        // 方便平台商城列表查询
        if (product.getFromFlag() == ProductFromFlagEnum.PLAT.getCode()) {
            product.setFromFlag(ProductFromFlagEnum.MALL.getCode());
        }
        //这里的更新语句不是selective,全量更新
        int putawayResult = sellerProductRepostory.update(product);
        if (putawayResult == 0) {
            log.error("修改下架状态失败,shopId={},productId={}", shopId, productId);
            return new SimpleResponse(14, "下架失败");
        }
        sellerProductRepostory.downshilfProductByParentId(productId);

        // TODO 如果是分销商下架，只需要更新自己的商品；否则需要按照原始商品id更新（是否可能造成in数据过多）
        if (!product.getId().equals(product.getParentId())) {
            List<Long> groupIdList = productGroupMapRepository.selectGroupIdListByProductId(product.getId());
            if (CollectionUtils.isEmpty(groupIdList)) {
                return new SimpleResponse();
            }
            // 有分组关联，扣除商品数量
            Integer integer1 = productGroupMapRepository.updateDelByProductId(product.getId());
            if (integer1 != 0) {
                productGroupRepository.updateGoodsNumByIdList(groupIdList, AddOrReduceEnum.REDUCE);
            }
        } else {
            List<Long> groupIdList = productGroupMapRepository.selectGroupIdListByParentProductId(product.getParentId());
            if (CollectionUtils.isEmpty(groupIdList)) {
                return new SimpleResponse();
            }
            // 有分组关联，扣除商品数量
            Integer integer1 = productGroupMapRepository.updateDelByParentProductId(product.getParentId());
            if (integer1 != 0) {
                productGroupRepository.updateGoodsNumByIdList(groupIdList, AddOrReduceEnum.REDUCE);
            }
        }
        return new SimpleResponse(0, "success");
    }

    /**
     * 终止分销
     *
     * @param productId  .
     * @param merchantId .
     * @param shopId     .
     * @return .
     */
    @Override
    public SimpleResponse diststop(Long productId, Long merchantId, Long shopId) {
        /*
         * <pre>
         *     产品需求:
         *      自营分销 : 商品分销状态失效,粉丝无法继续推广
         *      平台分销 : 从分销商店铺下架  =>如果是平台分销商品,下架所有parent_id为此商品id的商品
         * </pre>
         */
        Product product = sellerProductRepostory.selectById(productId);
        if (product == null) {
            return new SimpleResponse(10, "无此商品,或此商品状态有误");
        }
        if (!product.getMerchantId().equals(merchantId)) {
            return new SimpleResponse(11, "此商品并不属于此商户");
        }
        if (!product.getShopId().equals(shopId)) {
            return new SimpleResponse(12, "此商品并不属于此店铺");
        }
        BigDecimal selfSupportRatio = product.getSelfSupportRatio();
        BigDecimal platSupportRatio = product.getPlatSupportRatio();
        if (selfSupportRatio == null && platSupportRatio == null) {
            return new SimpleResponse(13, "此商品无分销属性");
        }
        // 自营分销
        if (selfSupportRatio != null && selfSupportRatio.compareTo(BigDecimal.ZERO) > 0) {
            product.setSelfSupportRatio(null);
        }
        // 平台分销
        boolean isPlatSupport = false;
        if (platSupportRatio != null && platSupportRatio.compareTo(BigDecimal.ZERO) > 0) {
            isPlatSupport = true;
            product.setPlatSupportRatio(null);
            /**设置佣金begin**/
            product.setCommissionNum(null);
            /**设置佣金end**/
        }
        product.setUtime(new Date());
        // 全量更新
        int diststopResult = sellerProductRepostory.update(product);
        if (diststopResult != 1) {
            log.error("终止分销操作失败,shopId={},productId={}", shopId, productId);
            new SimpleResponse(15, "终止分销失败");
        }
        log.info("终止分销设置成功,shopId={},productId={}", shopId, productId);
        if (isPlatSupport) {
            //如果是平台分销商品,下架所有parent_id为此商品id的商品
            Map<String, Object> conditionMap = new HashMap<>();
            conditionMap.put("parentId", productId);
            conditionMap.put("now", new Date());
            sellerProductRepostory.downshilf(conditionMap);
        }

        /*****add 当分销市场优化开关打开时，终止推广需要将含有分销属性的缓存清理掉begin***********/
        if (product.getFromFlag() == 1) {
            this.stringRedisTemplate.opsForZSet().remove(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, product.getId().toString());
        }
        /*****add 当分销市场优化开关打开时，终止推广需要将含有分销属性的缓存清理掉end***********/

        // TODO 终止分销，需要更新原始商品id所关联的所有分组，并且数目减1（同样可能面临in条件过长的问题）
        List<Long> groupIdList = productGroupMapRepository.selectGroupIdListByParentProductIdWithoutSelf(product.getParentId());
        if (CollectionUtils.isEmpty(groupIdList)) {
            return new SimpleResponse();
        }
        // 有分组关联，扣除商品数量
        Integer integer1 = productGroupMapRepository.updateDelByParentProductIdWithoutSelf(product.getParentId());
        if (integer1 != 0) {
            productGroupRepository.updateGoodsNumByIdList(groupIdList, AddOrReduceEnum.REDUCE);
        }


        return new SimpleResponse(0, "success");
    }

    /**
     * 将分销市场商品加入仓库
     *
     * @param productId  .
     * @param merchantId .
     * @param shopId     .
     * @return .
     */
    @Override
    public SimpleResponse addstore(String productId, String merchantId, String shopId) {
        Product parentProduct = sellerProductRepostory.selectById(Long.parseLong(productId));
        if (parentProduct == null) {
            return new SimpleResponse(30, "无此商品");
        }
        SimpleResponse simpleResponse = this.productAddstoreError(parentProduct);
        if (simpleResponse != null) {
            return simpleResponse;
        }
        //创建新商品 , 加入仓库
        Product newProduct = this.createProductFromParentProduct4Addstore(parentProduct, merchantId, shopId);
        if (sellerProductRepostory.create(newProduct) == 0) {
            log.error("分销市场选择商品加入仓库失败,shopId={},parentProductId={}", shopId, parentProduct.getId());
            return new SimpleResponse(21, "加入仓库时,创建新商品失败");
        }

        return new SimpleResponse(0, "success");
    }

    /**
     * 复制父商品加入仓库,创建新商品
     *
     * @param parentProduct .
     * @param merchantId    .
     * @param shopId        .
     * @return .
     */
    private Product createProductFromParentProduct4Addstore(Product parentProduct, String merchantId, String shopId) {
        Product newProduct = new Product();
        Date now = new Date();
        newProduct.setMerchantId(Long.parseLong(merchantId));
        newProduct.setShopId(Long.parseLong(shopId));
        newProduct.setParentId(parentProduct.getId());
        newProduct.setMainPics(parentProduct.getMainPics());
        newProduct.setDetailPics(parentProduct.getDetailPics());
        newProduct.setThirdPriceMin(parentProduct.getThirdPriceMin());
        newProduct.setThirdPriceMax(parentProduct.getThirdPriceMax());
        newProduct.setSort(parentProduct.getSort());
        newProduct.setStatus(0);
        newProduct.setPutawayTime(null);
        newProduct.setSelfSupportRatio(null);
        newProduct.setPlatSupportRatio(parentProduct.getPlatSupportRatio());
        newProduct.setCheckStatus(1);
        newProduct.setCtime(now);
        newProduct.setUtime(now);
        newProduct.setDel(0);
        return newProduct;
    }

    private Product createProductFromParentProduct4Upshelf(Product parentProduct, String merchantId, String shopId, BigDecimal selfSupportRadio) {
        Product newProduct = this.createProductFromParentProduct4Addstore(parentProduct, merchantId, shopId);
        newProduct.setSelfSupportRatio(selfSupportRadio);
        newProduct.setStatus(1);
        newProduct.setPutawayTime(parentProduct.getCtime());
        return newProduct;
    }


    private SimpleResponse productAddstoreError(Product product) {
        if (product == null) {
            return new SimpleResponse(10, "此商品不存在");
        }
        if (product.getStatus() != 1) {
            return new SimpleResponse(11, "此商品为非上架状态");
        }
        if (product.getPlatSupportRatio() == null) {
            return new SimpleResponse(12, "此商品没有平台分销属性");
        }
        if (product.getDel() == 1) {
            return new SimpleResponse(13, "此商品已被删除");
        }
        if (product.getCheckStatus() != 1) {
            return new SimpleResponse(14, "此商品未审核通过");
        }
        return null;
    }


    /**
     * 将分销市场商品直接上架
     *
     * @param productId                    .
     * @param merchantId                   .
     * @param shopId                       .
     * @param selfDistributionRadioDecimal .
     * @return .
     */
    @Override
    public SimpleResponse distUpshelf(String productId, String merchantId, String shopId, BigDecimal selfDistributionRadioDecimal) {
        /*
         * <pre>
         *     以乙商家上架 甲商家 A 店铺的 a 商品 例:
         *     复制 a商品的基本信息 , 创建 b 商品.
         *     1  b.pareng_id=a.id
         *     2  b.plat_support_radio = a.plat_support_radio
         *     3  b.self_support_radio = 乙商家自己设置
         * </pre>
         */
        Product parentProduct = sellerProductRepostory.selectById(Long.parseLong(productId));
        if (parentProduct == null) {
            return new SimpleResponse(30, "无此商品");
        }
        SimpleResponse simpleResponse = this.productAddstoreError(parentProduct);
        if (simpleResponse != null) {
            return simpleResponse;
        }
        //创建新商品 , 加入仓库
        Product newProduct = this.createProductFromParentProduct4Upshelf(parentProduct, merchantId, shopId, selfDistributionRadioDecimal);
        if (sellerProductRepostory.create(newProduct) == 0) {
            log.error("分销市场选择商品上架失败,shopId={},parentProductId={}", shopId, parentProduct.getId());
            return new SimpleResponse(21, "加入仓库时,创建新商品失败");
        }

        return new SimpleResponse(0, "success");
    }

    /**
     * 商品-创建商品-获取类目
     *
     * @param merchantIdStr
     * @param shopIdStr
     * @return
     */
    @Override
    public SimpleResponse getSpecs(Long merchantIdStr, Long shopIdStr) {
        List<CategoryTaobao> categoryTaobaoList = categoryTaobaoRepository.selectByLevel(1);
        List<Map<String, Object>> respMapList = new ArrayList<>();
        for (CategoryTaobao categoryTaobao : categoryTaobaoList) {
            Map<String, Object> respMap = new HashMap<>();
            respMap.put("sid", categoryTaobao.getSid());
            respMap.put("name", categoryTaobao.getName());
            respMapList.add(respMap);
        }
        return new SimpleResponse(0, "success", respMapList);
    }

    /**
     * 获取修改前置信息
     *
     * @param merchantId .
     * @param shopId     .
     * @param productId  .
     * @return .
     */
    @Override
    public SimpleResponse getBeforedistInfo(String merchantId, String shopId, String productId) {
        Map<String, Object> conditionsMap = new HashMap<>();
        conditionsMap.put("merchantId", merchantId);
        conditionsMap.put("shopId", shopId);
        conditionsMap.put("productId", productId);
        Product product = sellerProductRepostory.selectById(Long.valueOf(productId));
        List<ProductSku> productSkus = sellerSkuRepostory.selectByProductId(product.getParentId());
        if (CollectionUtils.isEmpty(productSkus)) {
            return new SimpleResponse(11, "商品数据不完整");
        }
        Product productParent = sellerProductRepostory.selectById(product.getParentId());
        BigDecimal platSupportRadio = productParent.getPlatSupportRatio();
        if (platSupportRadio == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "分销设置丢失");
        }
        BigDecimal priceMin = new BigDecimal("100000");
        BigDecimal priceMax = BigDecimal.ZERO;
        for (ProductSku productSku : productSkus) {
            if (productSku.getSkuSellingPrice().compareTo(priceMax) > 0) {
                priceMax = productSku.getSkuSellingPrice();
            }
            if (productSku.getSkuSellingPrice().compareTo(priceMin) < 0) {
                priceMin = productSku.getSkuSellingPrice();
            }
        }
        PlatformDistRateAndPriceDTO minDto = getPlatformDistRate(shopId, product.getProductSpecs(), priceMin, platSupportRadio);
        PlatformDistRateAndPriceDTO maxDto = getPlatformDistRate(shopId, product.getProductSpecs(), priceMax, platSupportRadio);
        BigDecimal commissionMin = minDto.getPriceAfterDist();
        BigDecimal commissionMax = maxDto.getPriceAfterDist();
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("productSellingPrice", "￥" + priceMin + "-" + priceMax);
        responseMap.put("priceMax", priceMax);
        responseMap.put("priceMin", priceMin);
        responseMap.put("distributionCommissionRadio", minDto.getPlatSupportRatio());
        responseMap.put("commissionTotal", "￥" + commissionMin + "-" + commissionMax);
        responseMap.put("commissionMax", commissionMax);
        responseMap.put("commissionMin", commissionMin);
        return new SimpleResponse(0, "success", responseMap);
    }

    /**
     * 获取供应商商品经过平台分佣之后的售价和比例
     *
     * @param shopId          店铺id
     * @param productSpecs    商品类目
     * @param productPrice    商品售价
     * @param platSupportRate 供应商分销比例
     * @return
     */
    @Override
    public PlatformDistRateAndPriceDTO getPlatformDistRate(String shopId, Long productSpecs, BigDecimal productPrice, BigDecimal platSupportRate) {
        ProductDistributionDTO productDistributionDTO = new ProductDistributionDTO();
        productDistributionDTO.setShopId(Long.valueOf(shopId));
        productDistributionDTO.setProductSpecs(productSpecs);
        String resultJson = distributionAppService.queryOrderDistributionProperty(productDistributionDTO);
        BigDecimal platformDistRate = BigDecimal.TEN;
        if (StringUtils.isNotBlank(resultJson)) {
            JSONObject jsonObject = JSONObject.parseObject(resultJson);
            // 平台抽佣默认为10%
            Object goodsCategoryRate = jsonObject.get("goodsCategoryRate");
            if (goodsCategoryRate != null && NumberValidationUtil.isDecimal(goodsCategoryRate.toString())) {
                platformDistRate = new BigDecimal(goodsCategoryRate.toString());
            }
        }
        BigDecimal platSupportRatio = platSupportRate.multiply(new BigDecimal("100").subtract(platformDistRate)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
        BigDecimal priceAfterDist = productPrice.multiply(platSupportRatio).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
        PlatformDistRateAndPriceDTO dto = new PlatformDistRateAndPriceDTO();
        dto.setPlatformDistRate(platformDistRate);
        dto.setPlatSupportRatio(platSupportRatio);
        dto.setPriceAfterDist(priceAfterDist);
        return dto;
    }

    /**
     * 设置商铺分销推广佣金比例
     *
     * @param merchantId                   .
     * @param shopId                       .
     * @param productId                    .
     * @param selfDistributionRadioDecimal 店铺分佣比例
     * @param platDistributionRadioDecimal 平台分佣比例
     * @return .
     */
    @Override
    @Transactional
    public SimpleResponse setDistributionRadio(Long merchantId, Long shopId, Long productId, BigDecimal selfDistributionRadioDecimal, BigDecimal platDistributionRadioDecimal) {
        Product product = sellerProductRepostory.selectById(productId);
        if (product == null) {
            return new SimpleResponse(10, "无此商品,或此商品状态有误");
        }
        if (!product.getMerchantId().equals(merchantId)) {
            return new SimpleResponse(11, "此商品并不属于此商户");
        }
        if (!product.getShopId().equals(shopId)) {
            return new SimpleResponse(12, "此商品并不属于此店铺");
        }
        if (product.getStatus() != ProductConstant.Status.ON_SALE) {
            return new SimpleResponse(13, "商品必须为上架状态");
        }
        // 已加入新手专区不能建立分销推广
        if (product.getShowType() == ProductShowTypeEnum.NEWBIE.getCode()) {
            return new SimpleResponse(13, "已加入新手专区不能建立推广");
        }
        if (product.getDel() != ProductConstant.DelStatus.NOT_DELETED) {
            return new SimpleResponse(14, "商品已被删除");
        }
        if (!product.getId().equals(product.getParentId())) {
            return new SimpleResponse(15, "此商品不是供应商自营商品");
        }
        BigDecimal selfSupportRatioDB = product.getSelfSupportRatio();
        if (selfSupportRatioDB != null
                && (selfDistributionRadioDecimal.compareTo(selfSupportRatioDB) != 0)) {
            return new SimpleResponse(16, "不能修改店铺分销比例");
        }
        BigDecimal platSupportRatioDB = product.getPlatSupportRatio();
        if (platSupportRatioDB != null
                && (platDistributionRadioDecimal.compareTo(platSupportRatioDB) != 0)) {
            return new SimpleResponse(16, "不能修改平台分销比例");
        }
        // 获取店铺信息
        MerchantShopDTO merchantShopDTO = shopAppService.findShopByShopId(shopId);
        if (merchantShopDTO != null) {
            // 判断店铺版本
            if (MerchantShopPurchaseVersionEnum.CLEAN.getCode().equals(merchantShopDTO.getPurchaseVersion()) ||
                    MerchantShopPurchaseVersionEnum.WEIGHTING.getCode().equals(merchantShopDTO.getPurchaseVersion())) {
                log.info("--------------------- 尾货清仓版-建立分销-置空自营分销比例. ");
                selfDistributionRadioDecimal = null;
            }
        }

        List<ProductSku> skuByProductId = productSkuMapper.findByProductId(product.getParentId());
        BigDecimal min = this.findPriceMinFromSku(skuByProductId);
        // 计算佣金金额最小值不得小于0.25
        if (platDistributionRadioDecimal != null) {
            PlatformDistRateAndPriceDTO platformDistRateDTO = getPlatformDistRate(shopId.toString(), product.getProductSpecs(), min, platDistributionRadioDecimal);
            BigDecimal priceAfterDist = platformDistRateDTO.getPriceAfterDist();
            if (priceAfterDist.compareTo(new BigDecimal(MIN_COMMISSION_PRICE)) < 0) {
                return new SimpleResponse(18, "需保证推广者佣金大于" + MIN_COMMISSION_PRICE + "，请重新设置分销比例");
            }
        }
        if (selfDistributionRadioDecimal != null) {
            BigDecimal priceAfterDist = min.multiply(selfDistributionRadioDecimal).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
            if (priceAfterDist.compareTo(new BigDecimal(MIN_COMMISSION_PRICE)) < 0) {
                return new SimpleResponse(18, "需保证推广者佣金大于" + MIN_COMMISSION_PRICE + "，请重新设置分销比例");
            }
        }
        Date now = new Date();
        if (platDistributionRadioDecimal != null) {
            // 如果数据库没有供应商分销设置，说明没有提交审核或者审核失败；如果供应商分销这是不为空，并且供应商分销设置与入参不相等，说明供应商需要修改并审核
            if (platSupportRatioDB == null || (platDistributionRadioDecimal.compareTo(platSupportRatioDB) != 0)) {
                ProductDistributionAuditLog auditLog = new ProductDistributionAuditLog();
                //商户
                auditLog.setMerchantId(product.getMerchantId());
                //店铺
                auditLog.setShopId(product.getShopId());
                //产品id
                auditLog.setProductId(product.getId());
                //分销比例
                auditLog.setPlatSupportRatio(platDistributionRadioDecimal);
                //状态
                auditLog.setStatus(ProductConstant.Distribution.Audit.Status.WAITING_FOR_AUDIT);
                //作废之前的分销比例
                productDistributionAuditLogRepository.abandonAllDistributionByProduct(productId);
                //插入新的分销比例
                productDistributionAuditLogRepository.createSelective(auditLog);
            }
        }

        product.setSelfSupportRatio(selfDistributionRadioDecimal);
        //需要审核平台分销比例
        product.setPlatSupportRatio(null);

        /**设置佣金begin**/
        product.setCommissionNum(null);
        /**设置佣金end**/
        product.setUtime(now);
        int setRadioResult = sellerProductRepostory.updateBySelective(product);
        Assert.isTrue(setRadioResult == 1, 7, "商家设置佣金比例发生异常");
        return new SimpleResponse(0, "success");
    }


    /**
     * 上传oss返回oss地址
     *
     * @param image      .
     * @param bucketName .
     * @param picName    .
     * @return .
     */
    private String uploadObject2OSS(MultipartFile image, String bucketName, String picName) {
        String ext = FilenameUtils.getExtension(image.getOriginalFilename());
        String[] imageType = {"gif", "png", "jpg", "jpeg", "bmp"};
        if (!checkFileType(imageType, ext)) {
            log.error("文件格式格式错误");
        }
        String fileName = picName + "." + ext;
        String bucket = bucketName + fileName;
        String imgUrl = "";
        try {
            imgUrl = ossCaller.uploadObject2OSSInputstream(image.getInputStream(), bucket);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgUrl;
    }
}
