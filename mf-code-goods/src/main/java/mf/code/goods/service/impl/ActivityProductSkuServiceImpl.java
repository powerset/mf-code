package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSONObject;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.feignapi.dto.ActivityProductDTO;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.service.ActivityProductSkuService;
import mf.code.goods.service.GoodsSkuAboutService;
import mf.code.goods.service.GoodsUpdateService;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.goods.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月17日 16:47
 */
@Service
public class ActivityProductSkuServiceImpl implements ActivityProductSkuService {
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private GoodsSkuAboutService goodsSkuAboutService;
    @Autowired
    private GoodsUpdateService goodsUpdateService;

    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /***
     * 活动-商品-详情页
     *
     * @param merchantId    商户编号
     * @param shopId    店铺编号
     * @param userId    用户编号
     * @param skuId sku编号
     * @return
     */
    @Override
    public SimpleResponse getDiscountGoodsDetail(Long merchantId, Long shopId, Long userId, Long skuId) {
        ActivityProductDTO resp = new ActivityProductDTO();

        ProductSku productSku = productSkuMapper.selectByPrimaryKey(skuId);
        if (productSku == null || productSku.getDel() == DelEnum.YES.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品已下架~");
        }
        Product product = productMapper.selectByPrimaryKey(productSku.getProductId());
        if (product == null || (product.getDel() == DelEnum.YES.getCode())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品已下架~");
        }
        // 为了商品查询的方便,这里直接生成一条分销记录
        if (!product.getShopId().equals(shopId)) {
            product = goodsUpdateService.getProductForPlatMall(merchantId, shopId, product.getId(), AppTypeEnum.JKMF);
        }
        //商品详情长图
        if (StringUtils.isNotBlank(product.getDetailPics())) {
            List<String> list = JSONObject.parseArray(product.getDetailPics(), String.class);
            List<String> pics = new ArrayList<>();
            for (String string : list) {
                pics.add(string + compressionRatioPic);
                resp.setPicDetail(pics);
            }
        }

        //针对商品设置的初始销量是否有值
        int initSales = 0;
        if (StringUtils.isNotBlank(productSku.getExtra())) {
            Map<String, Object> jsonMap = JSONObject.parseObject(productSku.getExtra(), Map.class);
            if (jsonMap != null) {
                if (jsonMap.get("init_sales") != null) {
                    initSales = NumberUtils.toInt(jsonMap.get("init_sales").toString());
                }
            }
        }

        //是否是分销商品
        ProductDistributionTypeEnum distributionTypeEnum = ProductDistributionTypeEnum.findByParams(product.getId(), product.getParentId(), product.getSelfSupportRatio(), product.getPlatSupportRatio());
        resp.setGoodsType(distributionTypeEnum.getCode());
        //获取该商品sku信息
        resp.setSkus(getSkuInfo(productSku));

        resp.setParentId(product.getParentId());
        resp.setProductId(product.getId());
        resp.setPrice(productSku.getSkuSellingPrice());
        resp.setThirdPrice(productSku.getSkuThirdPrice());
        resp.setSalesVolume((productSku.getStockDef() - productSku.getStock()) + initSales + "");
        resp.setStock(productSku.getStock());
        List<String> mainPics = JSONObject.parseArray(product.getMainPics(), String.class);
        List<String> pics = new ArrayList<>();
        for (String string : mainPics) {
            pics.add(string + compressionRatioPic);
            resp.setGoodPics(pics);
        }
        resp.setSkuId(productSku.getId());
        resp.setProductTitle(product.getProductTitle());
        if (ProductConstant.ProductSku.NotShow.FLASH_SALE == productSku.getNotShow()) {
            resp.setShowType(ProductConstant.ProductSku.NotShow.FLASH_SALE);
            List<String> effectTime = Arrays.asList(productSku.getEffectTime().split("_"));
            if (effectTime.size() > 1) {
                Date end = new Date(NumberUtils.toLong(effectTime.get(1)));
                if (System.currentTimeMillis() > end.getTime()) {
                    resp.setCutoffTime(0L);
                } else {
                    long cutoffTime = DateUtil.timeSubStamp(new Date(), end);
                    resp.setCutoffTime(cutoffTime);
                }
            }
        } else if (ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK == productSku.getNotShow()) {
            resp.setShowType(ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK);
            resp.setPrice(new BigDecimal("0.00"));
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    private List<Map> getSkuInfo(ProductSku productSku) {
        List<Long> catIds = new ArrayList<>();
        List<String> specsValues = new ArrayList<>();
        String[] split = productSku.getSpecSort().split(",");
        for (int i = 0; i < split.length; i++) {
            String[] split1 = split[i].split(":");
            //获取类目
            if (catIds.indexOf(NumberUtils.toLong(split1[0])) == -1) {
                catIds.add(NumberUtils.toLong(split1[0]));
            }
            //获取类目下的属性
            if (specsValues.indexOf(split1[1]) == -1) {
                specsValues.add(split1[1]);
            }
        }
        return goodsSkuAboutService.getSkuItemInfo(catIds, specsValues);
    }
}
