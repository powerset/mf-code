package mf.code.goods.service.impl;/**
 * create by qc on 2019/4/20 0020
 */

import mf.code.common.exception.ArgumentException;
import mf.code.common.utils.Assert;
import mf.code.goods.api.seller.dto.ProductSkuSpec;
import mf.code.goods.repo.dao.ProductSpecsMapper;
import mf.code.goods.repo.po.ProductSpecs;
import mf.code.goods.service.ProductSkuSpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author gbf
 * 2019/4/20 0020、18:28
 */
@Service
public class ProductSkuSpecServiceImpl implements ProductSkuSpecService {
    @Autowired
    private ProductSpecsMapper productSpecsMapper;

    @Override
    public List<ProductSkuSpec> parseProductSkuSpecs(String specsStr, Map<Long, String> dic) {
        List<ProductSkuSpec> productSkuSpecList = new ArrayList<>();
        String[] specArr = specsStr.split(",");
        for (String spec : specArr) {
            ProductSkuSpec productSkuSpec = this.parseProductSkuSpec(spec, dic);
            productSkuSpecList.add(productSkuSpec);
        }
        return productSkuSpecList;
    }

    @Override
    public ProductSkuSpec parseProductSkuSpec(String specStr, Map<Long, String> dic) {
        String[] strArr = specStr.split(":");
        if (strArr.length < 2) {
            throw new ArgumentException(1, "解析product的销售属性参数异常,specStr=" + specStr);
        }
        ProductSkuSpec productSkuSpec = new ProductSkuSpec();
        String parentId = strArr[0];
        String specId = strArr[1];
        Assert.isInteger(parentId, -1, "解析销售属性值错误");
        Assert.isInteger(specId, -1, "解析销售属性值错误");

        productSkuSpec.setParentId(parentId);
        productSkuSpec.setSpecId(specId);
        productSkuSpec.setParentName(dic.get(Long.parseLong(parentId)));
        ProductSpecs productSpecs = productSpecsMapper.selectByPrimaryKey(Long.parseLong(specId));
        if(productSpecs!=null){
            productSkuSpec.setSpecName(productSpecs.getSpecsValue());
        }
        return productSkuSpec;
    }
}
