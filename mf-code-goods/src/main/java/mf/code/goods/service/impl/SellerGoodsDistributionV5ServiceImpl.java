package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.netflix.discovery.converters.Auto;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.goods.api.seller.dto.*;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.repo.dao.*;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductDistributionAuditLog;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.vo.ProductSkuSpecValVo;
import mf.code.goods.service.*;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
public class SellerGoodsDistributionV5ServiceImpl implements SellerGoodsDistributionV5Service {

    @Autowired
    private SellerGoodsDistributionMapper sellerGoodsDistributionMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private CategoryTaobaoMapper categoryTaobaoMapper;
    @Autowired
    private GoodsSkuAboutService goodsSkuAboutService;
    @Autowired
    private ProductSkuSpecService productSkuSpecService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private ProductDistributionAuditLogMapper productDistributionAuditLogMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MasterFixDataService masterFixDataService;

    /**
     * 含有分销属性商品销量排行榜
     */
    public static final String PRODUCT_SALESVOLUME_RANKING_LIST = "jkmf:product:salesvolume:ranking:list";

    @Override
    public DisProductListResultDTO getDisProductListResult(Map<String, Object> params, String shopid) {
        log.info(">>>>>>>>>>>>>>>>>>>>>hava enter into getDisProductListResult");
        //根据查询条件获取结果集
        //获取product和product_Sku中的属性
        List<DisProductListInfoDTO> productList = sellerGoodsDistributionMapper.getProductAndProductSkuInfo(params);
        if (null == productList) {
            log.info("have find productList is null and params=" + JSON.toJSONString(params));
            return null;
        }
        List<DisProductListInfoDTO> productListNum = sellerGoodsDistributionMapper.getProductAndProductSkuNum(params);
        if (null == productListNum) {
            log.info("have find productListNum is null and params=" + JSON.toJSONString(params));
            return null;
        }
        //获取总条数
        int count = productListNum.size();
        List<DisProductListInfoDTO> disProductList = new ArrayList<>();
        //获取需要的属性
        for (DisProductListInfoDTO info : productList) {
            String title = info.getTitle();
            String mainPic = info.getMainPic();
            BigDecimal salePrice = info.getSalePrice();
            BigDecimal commissionPre = info.getCommissionPre();

            if (null == commissionPre) {
                commissionPre = new BigDecimal("0");
            }

            Long shopId = info.getShopId();
            //是否为自己店铺标识
            String selfShopFlag = "0";
            if (null != shopid) {
                Long sid = Long.valueOf(shopid);
                if (0 == Long.compare(sid, shopId)) {
                    selfShopFlag = "1";
                }
            }
            //如果是自己的店铺，则上架和加仓都显示
            int stackFlag = 0;
            String joinStoreFlag = "1";
            if ("1".equals(selfShopFlag)) {
                stackFlag = 1;
                joinStoreFlag = "0";
            }

            //计算佣金
            Long productSpecs = info.getProductSpecs();
            PlatformDistRateAndPriceDTO minDto = goodsService.getPlatformDistRate(shopId.toString(), productSpecs, salePrice, commissionPre);
            BigDecimal commission = info.getCommission();

            BigDecimal thirdPrice = info.getThirdPrice();
            Long productId = info.getProductId();
            log.info(">>>>>>>>>>>>have find productId=" + productId + "and thirdPrice=" + thirdPrice);
            //获取平台需要抽佣比例
            BigDecimal platformDistRate = minDto.getPlatformDistRate();
            BigDecimal cpre = commissionPre.multiply(new BigDecimal("100").subtract(platformDistRate)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);

            DisProductListInfoDTO dinfo = new DisProductListInfoDTO();
            dinfo.setTitle(title);
            dinfo.setMainPic(mainPic);
            dinfo.setSalePrice(salePrice);
            dinfo.setCommissionPre(cpre);
            dinfo.setCommission(commission);
            dinfo.setStackFlag(stackFlag);
            dinfo.setJoinStoreFlag(joinStoreFlag);
            dinfo.setThirdPrice(thirdPrice);
            dinfo.setSelfShopFlag(selfShopFlag);
            dinfo.setProductId(productId);
            dinfo.setShopId(shopId);
            dinfo.setMerchantId(info.getMerchantId());

            disProductList.add(dinfo);
        }

        DisProductListResultDTO dto = new DisProductListResultDTO();
        dto.setDisProductList(disProductList);
        dto.setCount(count);
        return dto;
    }

    @Override
    public DisProductListResultDTO getDisProductListResultBySwitch(Map<String, Object> params, String shopid, String salesVolume, int num, int size) {

        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>have enter into getDisProductListResultBySwitch");
        List<DisProductListInfoDTO> disProductList = new ArrayList<>();
        int count = 0;
        if (StringUtils.isNotBlank(salesVolume)) {
            //获取含有分销属性产品的信息
            List<DisProductListInfoDTO> productList = sellerGoodsDistributionMapper.getDisProductInfo(params);
            /**新增对id和商品名称同时查*/
            if (params != null && params.get("productName") != null) {
                Object productNameOrId = params.get("productName");
                if (productNameOrId != null) {
                    String pid = productNameOrId.toString();
                    try {
                        Long pidInt = Long.parseLong(pid);
                        params.put("productIdFromNet", pidInt);
                        params.put("productName",null);
                        List<DisProductListInfoDTO> disProductInfo = sellerGoodsDistributionMapper.getDisProductInfo(params);
                        productList.add(disProductInfo.get(0));

                    }catch (Exception e){
                        log.info(e.toString());
                    }
                }
            }
            /**新增对id和商品名称同时查*/
            if (null == productList || productList.size() <= 0) {
                log.info("have find getDisProductListResultBySwitch productList is null and params=" + JSON.toJSONString(params));
                return null;
            }

            /**   `_` 这个查询与sellerGoodsDistributionMapper.getDisProductInfo(params)看上去是一个查询 by qc */
            //获取含有分销属性产品信息的数量
            List<DisProductListInfoDTO> productListNum = sellerGoodsDistributionMapper.getDisProductInfoNum(params);
            if (null == productListNum || productListNum.size() <= 0) {
                log.info("have find getDisProductListResultBySwitch productListNum is null and params=" + JSON.toJSONString(params));
                return null;
            }

            //获取总条数
            count = productListNum.size();

            //获取需要的属性
            disProductList = getDisProductListInfoDTOList(productList, shopid);
        } else {
            //总条数
            Long nums = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST);
            count = new Long(nums).intValue();

            long pageNum = new Integer(num).longValue();
            long pageSize = new Integer(size).longValue();
            disProductList = this.orderByType(salesVolume, pageNum, pageSize, shopid);
            if (null == disProductList) {
                return null;
            }
        }
        params.put("pageNum",null);
        List<DisProductListInfoDTO> disProductInfoForAll = sellerGoodsDistributionMapper.getDisProductInfo(params);
        DisProductListResultDTO dto = new DisProductListResultDTO();
        dto.setDisProductList(disProductList);
        dto.setCount(disProductInfoForAll.size());
        return dto;
    }

    /**
     * 根据排序类型排序
     *
     * @param salesVolume
     * @param pageNum
     * @param pageSize
     * @param shopid
     * @return
     */
    private List<DisProductListInfoDTO> orderByType(String salesVolume, long pageNum, long pageSize, String shopid) {
        if (pageNum <= 0) {
            pageNum = 1;
        }
        //排序前的list
        List<DisProductListInfoDTO> disProductList = new ArrayList<>();
        //排序后的list
        List<DisProductListInfoDTO> disList = new ArrayList<>();
        Long sizeNum = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST);
        //当缓存为空时，需要重新放入缓存
        if (null == sizeNum || sizeNum < 1) {
            //补偿措施
            masterFixDataService.batchAddCacheByProductId();
        }
        //按照销量正序
        if ("0".equals(salesVolume)) {
            //分页获取含有分销属性的商品信息
            Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().rangeWithScores(
                    RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, (pageNum - 1) * pageSize, pageNum * pageSize - 1);
            if (CollectionUtils.isEmpty(typedTuples)) {
                return null;
            }

            List<Long> productIds = new ArrayList<>();
            for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
                String value = typedTuple.getValue();
                if (StringUtils.isNotBlank(value)) {
                    productIds.add(Long.valueOf(value));
                }
            }

            disProductList = queryByIds(productIds, shopid, salesVolume);

            //重新排序
            for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
                String value = typedTuple.getValue();
                if (StringUtils.isNotBlank(value)) {
                    for (DisProductListInfoDTO dto : disProductList) {
                        if (Long.valueOf(value).equals(dto.getProductId())) {
                            disList.add(dto);
                        }
                    }
                }
            }
        } else {
            //分页获取含有分销属性的商品信息
            Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                    RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, (pageNum - 1) * pageSize, pageNum * pageSize - 1);
            if (CollectionUtils.isEmpty(typedTuples)) {
                return null;
            }

            List<Long> productIds = new ArrayList<>();
            for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
                String value = typedTuple.getValue();
                if (StringUtils.isNotBlank(value)) {
                    productIds.add(Long.valueOf(value));
                }
            }

            disProductList = queryByIds(productIds, shopid, salesVolume);

            //重新排序
            for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
                String value = typedTuple.getValue();
                if (StringUtils.isNotBlank(value)) {
                    for (DisProductListInfoDTO dto : disProductList) {
                        if (Long.valueOf(value).equals(dto.getProductId())) {
                            disList.add(dto);
                        }
                    }
                }
            }
        }
        return disList;
    }

    /**
     * 批量获取含有分销属性的产品信息
     *
     * @param productIds
     * @param shopid
     * @return
     */
    private List<DisProductListInfoDTO> queryByIds(List<Long> productIds, String shopid, String salesVolume) {
        Map map = new HashMap();
        map.put("productIds", productIds);
        map.put("salesVolume", salesVolume);
        List<DisProductListInfoDTO> dtoList = sellerGoodsDistributionMapper.queryByIds(map);
        if (null == dtoList) {
            return null;
        }

        List<DisProductListInfoDTO> resultList = getDisProductListInfoDTOList(dtoList, shopid);
        return resultList;
    }

    /**
     * 提取含有分销属性商品的信息
     *
     * @param productList
     * @param shopid
     * @return
     */
    private List<DisProductListInfoDTO> getDisProductListInfoDTOList(List<DisProductListInfoDTO> productList, String shopid) {
        List<DisProductListInfoDTO> disProductList = new ArrayList<>();
        //获取需要的属性
        for (DisProductListInfoDTO info : productList) {
            String title = info.getTitle();
            String mainPic = info.getMainPic();
            BigDecimal salePrice = info.getMinSellPrice();
            BigDecimal commissionPre = info.getCommissionPre();

            if (null == commissionPre) {
                commissionPre = new BigDecimal("0");
            }

            Long shopId = info.getShopId();
            //是否为自己店铺标识
            String selfShopFlag = "0";
            if (null != shopid) {
                Long sid = Long.valueOf(shopid);
                if (0 == Long.compare(sid, shopId)) {
                    selfShopFlag = "1";
                }
            }
            //如果是自己的店铺，则上架和加仓都显示
            int stackFlag = 0;
            String joinStoreFlag = "1";
            if ("1".equals(selfShopFlag)) {
                stackFlag = 1;
                joinStoreFlag = "0";
            }

            //计算佣金
            Long productSpecs = info.getProductSpecs();
            PlatformDistRateAndPriceDTO minDto = goodsService.getPlatformDistRate(shopId.toString(), productSpecs, salePrice, commissionPre);
            BigDecimal commission = info.getCommission();

            BigDecimal thirdPrice = info.getMinTaobaoPrice();
            Long productId = info.getProductId();
            log.info(">>>>>>>>>>>>have find productId=" + productId + "and thirdPrice=" + thirdPrice);
            //获取平台需要抽佣比例
            BigDecimal platformDistRate = minDto.getPlatformDistRate();
            BigDecimal cpre = commissionPre.multiply(new BigDecimal("100").subtract(platformDistRate)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);

            DisProductListInfoDTO dinfo = new DisProductListInfoDTO();
            dinfo.setTitle(title);
            dinfo.setMainPic(mainPic);
            dinfo.setSalePrice(salePrice);
            dinfo.setCommissionPre(cpre);
            dinfo.setCommission(commission);
            dinfo.setStackFlag(stackFlag);
            dinfo.setJoinStoreFlag(joinStoreFlag);
            dinfo.setThirdPrice(thirdPrice);
            dinfo.setSelfShopFlag(selfShopFlag);
            dinfo.setProductId(productId);
            dinfo.setShopId(shopId);
            dinfo.setMerchantId(info.getMerchantId());

            disProductList.add(dinfo);
        }

        return disProductList;
    }

    @Override
    public SimpleResponse detail(long productId, Long shopId) {
        Product product = productMapper.selectByPrimaryKey(productId);
        CategoryTaobao categoryTaobao = categoryTaobaoMapper.selectByPrimaryKey(product.getProductSpecs());
        if (product == null) {
            return new SimpleResponse(30, "无此商品");
        }
        BigDecimal platSupportRatio = product.getPlatSupportRatio();
        if (platSupportRatio == null) {
            platSupportRatio = new BigDecimal(0);
        }
        List<ProductSku> skuList = productSkuMapper.findByProductId(productId);

        //售价
        BigDecimal priceMin = new BigDecimal(999999999);
        BigDecimal priceMax = new BigDecimal(0);
        //淘宝价
        BigDecimal priceTbMin = new BigDecimal(999999999);
        BigDecimal priceTbMax = new BigDecimal(0);

        Integer stockTotal = 0;
        Integer stockDefTotal = 0;
        String mainPicsStr = product.getMainPics();
        List<String> list = JSON.parseArray(mainPicsStr, String.class);
        List<Map<String, Object>> skuResultMapList = new ArrayList<>();

        Map<Long, String> specNameMap = goodsSkuAboutService.getSkuSalePropMap();
        //计算销售属性给前端展示
        Map<String, List<ProductSkuSpecValVo>> tempMap = new HashMap<>();
        for (ProductSku sku : skuList) {
            BigDecimal sellingPrice = sku.getSkuSellingPrice();
            priceMin = sellingPrice.compareTo(priceMin) < 0 ? sellingPrice : priceMin;
            priceMax = sellingPrice.compareTo(priceMax) > 0 ? sellingPrice : priceMax;


            //淘宝价
            BigDecimal skuThirdPrice = sku.getSkuThirdPrice();
            priceTbMin = skuThirdPrice.compareTo(priceTbMin) < 0 ? skuThirdPrice : priceTbMin;
            priceTbMax = skuThirdPrice.compareTo(priceTbMax) > 0 ? skuThirdPrice : priceTbMax;

            stockDefTotal += sku.getStockDef();
            stockTotal += sku.getStock();
            String skuPic = sku.getSkuPic();

            Map<String, Object> skuMap = new HashMap<>();
            skuMap.put("skuId", sku.getId());
            skuMap.put("stock", sku.getStock());
            //这里的快照,是设计初期的名称歧义
            skuMap.put("snapshoot", sku.getNotShow());
            skuMap.put("skuSellingPrice", sku.getSkuSellingPrice());
            skuMap.put("sellingPrice", sku.getSkuSellingPrice());
            skuMap.put("skuThirdPrice", sku.getSkuThirdPrice());
            skuMap.put("skuPic", skuPic == null ? (list != null && list.size() > 0 ? list.get(0) : null) : skuPic);
            String specSortStr = sku.getSpecSort();
            //存储商品属性
            List<ProductSkuSpec> productSkuSpecList = productSkuSpecService.parseProductSkuSpecs(specSortStr, specNameMap);

            skuMap.put("specs", productSkuSpecList);
            skuResultMapList.add(skuMap);

            for (ProductSkuSpec e : productSkuSpecList) {
                List<ProductSkuSpecValVo> valVoList = null;
                String key = e.getParentId();
                if (tempMap.containsKey(e.getParentId())) {
                    valVoList = tempMap.get(key);
                } else {
                    valVoList = new ArrayList<>();
                }
                ProductSkuSpecValVo vo = new ProductSkuSpecValVo();
                vo.setSpecValId(Long.parseLong(e.getSpecId()));
                vo.setSpecValVal(e.getSpecName());
                valVoList.add(vo);
                tempMap.put(key, valVoList);
            }
        }
        List<Map> propResultList = new ArrayList<>();
        Set<String> keySet = tempMap.keySet();
        for (String key : keySet) {
            Map<String, Object> propResultMap = new HashMap<>();
            propResultMap.put("specNameId", key);
            propResultMap.put("specNameVal", specNameMap.get(key));
            propResultMap.put("list", tempMap.get(key));
            propResultList.add(propResultMap);
        }


        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("productId", product.getId());
        resultMap.put("saleProps", propResultList);
        resultMap.put("title", product.getProductTitle());
        //图片
        resultMap.put("mainPic", mainPicsStr);
        resultMap.put("detailPic", product.getDetailPics());

        PlatformDistRateAndPriceDTO maxDto = goodsService.getPlatformDistRate(shopId.toString(), product.getProductSpecs(), priceMax, platSupportRatio);
        PlatformDistRateAndPriceDTO minDto = goodsService.getPlatformDistRate(shopId.toString(), product.getProductSpecs(), priceMin, platSupportRatio);

        log.info("have find commissionPre=" + platSupportRatio + ",commissionMin=" + minDto.getPriceAfterDist() + ",commissionMax=" + maxDto.getPriceAfterDist());
        //佣金
        resultMap.put("commissionMin", minDto.getPriceAfterDist());
        resultMap.put("commissionMax", maxDto.getPriceAfterDist());
//        resultMap.put("commissionMin", priceMin.multiply(platSupportRatio).divide(new BigDecimal(100)));
//        resultMap.put("commissionMax", priceMax.multiply(platSupportRatio).divide(new BigDecimal(100)));


        //售价
        resultMap.put("priceMin", priceMin);
        resultMap.put("priceMax", priceMax);
        //淘宝价
        resultMap.put("skuThirdPriceMin", priceTbMin);
        resultMap.put("skuThirdPriceMax", priceTbMax);
        //商品类别
        resultMap.put("category", categoryTaobao == null ? "" : categoryTaobao.getSid());
        resultMap.put("categoryName", categoryTaobao == null ? "" : categoryTaobao.getName());
        //销量
        resultMap.put("stock", stockTotal);

        /*******modify by yunshan 修改商品的销量算法begin***********/
        resultMap.put("saleVolume", stockDefTotal - stockTotal);
//        //获取缓存中的销量
//        Double saleVolume = stringRedisTemplate.opsForZSet().score(PRODUCT_SALESVOLUME_RANKING_LIST,product.getParentId().toString());
//        if (saleVolume==null){
//            saleVolume=0D;
//        }
//        int num = saleVolume.intValue();
//        resultMap.put("saleVolume", num);
//        /*******modify by yunshan 修改商品的销量算法end***********/


        //是否为自己店铺标识
        String selfShopFlag = "0";
        if (null != shopId) {
            Long sid = Long.valueOf(shopId);
            if (0 == Long.compare(product.getShopId(), sid)) {
                selfShopFlag = "1";
            }
        }
        resultMap.put("selfShopFlag", selfShopFlag);

        //上架标识1:上架,0:未上架
        int stackFlag = 0;
        //入库标识（1，未入库，0：已入库）
        String joinStoreFlag = "1";
        if ("1".equals(selfShopFlag)) {
            stackFlag = 1;
            joinStoreFlag = "0";
            resultMap.put("joinStoreFlag", joinStoreFlag);
            resultMap.put("status", stackFlag);
        } else {
            resultMap.put("joinStoreFlag", joinStoreFlag);
            resultMap.put("status", stackFlag);
        }

        // sku 列表
        resultMap.put("skuList", skuResultMapList);
        return new SimpleResponse(0, "success", resultMap);
    }

    @Override
    public SimpleResponse upShelf(Long productId, Long merchantId, Long shopId, String commissionPre, Integer appType) {
        if (null == productId || null == merchantId || null == shopId || null == commissionPre) {
            return new SimpleResponse(2, "必填参数为空");
        }
        Product product = productMapper.selectByPrimaryKey(productId);
        if (null == product) {
            log.info("have not find  upShelf productid from store and productId=" + productId + ",shopId=" + shopId);
            return new SimpleResponse(18, "获取上架商品信息失败");
        }

        //判断抽佣后的的佣金是否大于0.25
        BigDecimal platSupportRatio = product.getPlatSupportRatio();
        if (platSupportRatio == null) {
            return new SimpleResponse(17, "数据错误,平台分销佣金为空");
        }
        List<ProductSku> skuByProductId = productSkuMapper.findByProductId(product.getParentId());
        BigDecimal min = this.findPriceMinFromSku(skuByProductId);
        BigDecimal shareRatio = new BigDecimal(100).subtract(new BigDecimal(commissionPre));
        PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(shopId.toString(), product.getProductSpecs(), min, platSupportRatio);
        BigDecimal priceAfterDist = platformDistRate.getPriceAfterDist();
        BigDecimal minCommission = priceAfterDist.multiply(shareRatio).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);

        log.info(">>>>>>>>>>>>>>>>>> have find minCommission=" + minCommission + ",productId=" + productId + ",shopId=" + shopId + ",min=" + min);
        String minCommissonF = "0.25";
        if (minCommission.compareTo(new BigDecimal(minCommissonF)) < 0) {
            return new SimpleResponse(18, "需保证推广者佣金大于" + minCommissonF + "，请重新设置分销比例");
        }

        //上架需要判断是否已经加入仓库
        Map<String, Object> map = new HashMap<>();
        map.put("productId", productId);
        map.put("shopId", shopId);
        Long num = sellerGoodsDistributionMapper.getProductIdEqlParentIdNum(map);

        //需要操作的数据
        int flag = 1;//上架

        log.info(">>>>>>>>have find productId=" + productId + ",num=" + num);
        //没有加入仓库则需要先加入仓库
        if (num == 0) {
            Product newPro = getProductInfo(product, merchantId, shopId, flag, new BigDecimal(commissionPre), productId);
            log.info(">>>>>>>>prepare to insert into store !!! ");
            int count = productMapper.insertSelective(newPro);
            if (1 != count) {
                return new SimpleResponse(10, "插入分销商品失败");
            }
            log.info("^^^^^^^^^^^^^^^^^^^^^^^抖带带上架-开始 appType={} 更新抖带带分销商品的 本店铺分销比例, self_support_radio=0", appType);
            // 如果是抖带带,重写佣金比例为0
            if (AppTypeEnum.DDD.getCode() == appType) {
                newPro.setSelfSupportRatio(new BigDecimal(0));
                productMapper.updateByPrimaryKeySelective(newPro);
                log.info("^^^^^^^^^^^^^^^^^^^^^^^抖带带上架-执行成功 更新抖带带分销商品的 本店铺分销比例, self_support_radio=0");
            }
            return new SimpleResponse(0, "success");
        }
        //获取已加入仓库中的数据根据productID作为parentID 和shopID 查询
        Map<String, Object> mp = new HashMap<>();
        mp.put("parentId", productId);
        mp.put("shopId", shopId);

        log.info(">>>>>>>>>have find productId=" + productId + ",shopId=" + shopId);
        Product pt = productMapper.selectByPidAndShopId(mp);

        if (null == pt) {
            log.info("have not find productid from store and productId=" + productId + ",shopId=" + shopId);
            return new SimpleResponse(18, "获取上架商品信息失败");
        }

        //当产品属于上架中1的时候，不能修改佣金比例
        if (pt.getStatus() >= 1 && pt.getFromFlag() != ProductFromFlagEnum.MALL.getCode()) {
            log.info("have  find productid is status=1 from store and productId=" + productId + ",shopId=" + shopId);
            return new SimpleResponse(19, "上架中的商品不能修改佣金比例");
        }

        //已经加入仓库则修改上架状态为1,佣金比例
        Product p = getProduct(pt, new BigDecimal(commissionPre), product.getPlatSupportRatio());
        if (AppTypeEnum.DDD.getCode() == appType) {
            p.setSelfSupportRatio(BigDecimal.ZERO);
            log.info("^^^^^^^^^^^^^^^^^^^^^^^抖带带上架-执行成功 更新抖带带分销商品的 本店铺分销比例, self_support_radio=0");
        }
        int count = productMapper.updateByPrimaryKeySelective(p);
        if (1 != count) {
            return new SimpleResponse(17, "分销商品上架失败");
        }
        log.info("end upShelf and find  id=" + p.getId());
        return new SimpleResponse(0, "success");
    }

    private Product getProduct(Product pt, BigDecimal commissionPre, BigDecimal platSupportRatio) {
        pt.setStatus(1);
        if (null != commissionPre) {
            BigDecimal selfSupportRatio = new BigDecimal(100).subtract(commissionPre);
            pt.setSelfSupportRatio(selfSupportRatio);
        }
        pt.setCheckStatus(1);
        pt.setPlatSupportRatio(platSupportRatio);
        pt.setUtime(new Date());
        pt.setPutawayTime(new Date());
        //设置商品属性：2，分销
        pt.setFromFlag(2);

        /******分销的商品上架的时需要修改佣金 begin**/
        //获取佣金
        List<ProductSku> skuByProductId = productSkuMapper.findByProductId(pt.getParentId());
        BigDecimal min = this.findPriceMinFromSku(skuByProductId);
        BigDecimal platSupportRatios = pt.getPlatSupportRatio();
        if (platSupportRatios == null) {
            platSupportRatios = new BigDecimal(0);
        }
        PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(pt.getShopId().toString(), pt.getProductSpecs(), min, platSupportRatios);
        //供应商的抽佣比例（去除平台固定抽佣比例）
        BigDecimal platSupportRatioNum = platformDistRate.getPlatSupportRatio();
        BigDecimal commissionNum = min.multiply(platSupportRatioNum).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
        pt.setCommissionNum(commissionNum);
        /******分销的商品上架的时需要修改佣金 end**/
        return pt;
    }

    private BigDecimal findPriceMinFromSku(List<ProductSku> list) {
        BigDecimal min = new BigDecimal(9999999);
        for (ProductSku sku : list) {
            BigDecimal skuSellingPrice = sku.getSkuSellingPrice();
            if (min.compareTo(skuSellingPrice) > 0) {
                min = skuSellingPrice;
            }
        }
        return min;
    }


    @Override
    public SimpleResponse storage(Long productId, Long merchantId, Long shopId) {
        if (null == productId || null == merchantId || null == shopId) {
            return new SimpleResponse(2, "必填参数为空");
        }

        log.info(">>>>>>>>>have find storage productId=" + productId + ",merchantId=" + merchantId + ",shopId=" + shopId);
        Product product = productMapper.selectByPrimaryKey(productId);
        if (null == product) {
            log.info(">>>>>>>>>have find storage is already been store productId=" + productId + ",merchantId=" + merchantId + ",shopId=" + shopId);
            return new SimpleResponse(12, "未发现该商品");
        }
        //根据parentID 查看该商品是否已经入库
        Map<String, Object> params = new HashMap<>();
        params.put("productId", productId);
        params.put("shopId", shopId);
        Product pro = productMapper.selectByPidAndShopIdExist(params);

        // 特殊处理，为了区分平台商城强制设置的商品和用户自己认领的分销商品，所以采用id是否为空判断是否应该存储
        product.setId(null);
        if (null != pro) {
            if (pro.getFromFlag() == ProductFromFlagEnum.MALL.getCode()) {
                product = pro;
            } else {
                return new SimpleResponse(15, "该商品已经被分销");
            }
        }

        //加入仓库标识
        int flag = 0;
        Product newPro = getProductInfo(product, merchantId, shopId, flag, null, productId);
        int count;
        if (newPro.getId() == null) {
            count = productMapper.insertSelective(newPro);
        } else {
            count = productMapper.updateByPrimaryKeySelective(newPro);
        }
        if (1 != count) {
            return new SimpleResponse(10, "插入分销商品失败");
        }
        return new SimpleResponse();
    }

    @Override
    public SimpleResponse batchUpdateProductInfo(Long productId) {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>have enter into batchUpdateProductInfo");
        //获取指定范围的数据
        Map<String, Object> param = new HashMap<>();
        param.put("productId", productId);

        List<DisProductListInfoDTO> productListNum = null;
        productListNum = sellerGoodsDistributionMapper.findProductAndProductSkuOptimize(param);
        if (null == productListNum) {
            log.info("have find productListNum is null and params=" + JSON.toJSONString(productId));
            return null;
        }

        List<DisProductListInfoDTO> disProductListInfoDTOSNum = new ArrayList<>();
        //过滤log中审核状态不为1 的数据
        for (DisProductListInfoDTO dto : productListNum) {
            long pid = dto.getProductId();
            ProductDistributionAuditLog plogInfo = productDistributionAuditLogMapper.findByProductIdAndStatus(pid);
            if (null != plogInfo) {
                disProductListInfoDTOSNum.add(dto);
            }
        }

        if (null == disProductListInfoDTOSNum) {
            log.info("have find productListNum is null and params=" + JSON.toJSONString(productId));
            return new SimpleResponse(1, "fail");
        }

        for (DisProductListInfoDTO info : disProductListInfoDTOSNum) {
            BigDecimal platSupportRatios = info.getCommissionPre();
            if (null == platSupportRatios) {
                platSupportRatios = new BigDecimal(0);
            }

            //获取佣金
//            List<ProductSku> skuByProductId = productSkuMapper.findByProductId(info.getProductId());
//            BigDecimal min = this.findPriceMinFromSku(skuByProductId);
//            PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(info.getShopId().toString(), info.getProductSpecs(), min, platSupportRatios);
//
//            //供应商的抽佣比例（去除平台的固定抽佣比例后）
//            BigDecimal platSupportRatio = platformDistRate.getPlatSupportRatio();
            //获取平台抽佣后的佣金 ：售价*平台佣比*(100-固定抽佣比)=分销商和用户的佣金
            BigDecimal commissionNum = null;
            commissionNum = info.getMinSellPrice().multiply(platSupportRatios).multiply(new BigDecimal(90)).divide(new BigDecimal("10000"), 2, BigDecimal.ROUND_DOWN);

            //根据productID 修改product信息
            Map<String, Object> params = new HashMap<>();
            params.put("productId", info.getProductId());
            params.put("commissionNum", commissionNum);
            int count = productMapper.updateByProductId(params);
            if (1 != count) {
                continue;
            }
        }

        return new SimpleResponse(0, "success");
    }

    @Override
    public SimpleResponse batchUpdateProductPrice(Long productId) {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>have enter into batchUpdateProductPrice");
        //获取指定范围的数据
        Map<String, Object> params = new HashMap<>();
        params.put("productId", productId);

        List<ProductSkuInfoDTO> skuInfo = sellerGoodsDistributionMapper.getProductSkuInfo(params);
        if (CollectionUtils.isEmpty(skuInfo)) {
            return new SimpleResponse(-1, "fail");
        }
        //总条数
        int num = skuInfo.size();

        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>have find num=" + num);
        for (int i = 0; i < num; i++) {
            ProductSkuInfoDTO info = skuInfo.get(i);
            int count = sellerGoodsDistributionMapper.updateProductPriceInfoById(info);
            if (count <= 0) {
                log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>have find update product info fail  and id =" + info.getProductId());
            }
        }

        return new SimpleResponse(0, "success");
    }

    @Override
    public SimpleResponse batchAddCacheByProductId() {
        log.info(">>>>>>>>>>>>hava enter into batchAddCacheByProductId ");
        Long sizeNum = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST);
        if (null != sizeNum && sizeNum > 0) {
            return new SimpleResponse(12, "不能重复添加缓存");
        }
        masterFixDataService.batchAddCacheByProductId();
        return new SimpleResponse(0, "success");
    }

    @Override
    public SimpleResponse batchUpdateOrderSalesNumByProductId(Long productId) {
//        List<DisProductListInfoDTO> productList = sellerGoodsDistributionMapper.batchUpdateOrderSalesNumByProductId(productId);
//        if (null == productList) {
//            return new SimpleResponse(-1, "未发现该商品");
//        }
//
//        //总条数
//        int num = productList.size();
//        //分批条数
//        int size = 200;
//        //批数
//        int no = 0;
//        if (num % size == 0) {
//            no = num / size;
//        } else {
//            no = num / size + 1;
//        }
//
//        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>have find batchAddCacheByProductId no =" + no + ",num=" + num);
//
//        for (int i = 0; i <= no; i++) {
//            for (int k = i * size; k < (i + 1) * size; k++) {
//                if(k>=num){
//                    break;
//                }
//                DisProductListInfoDTO dto = productList.get(k);
//                Long pid = dto.getProductId();
//                //获取该产品的销量
//            }
//        }
        return new SimpleResponse(0, "success");

    }

    /**
     * 将含有分销属性的商品加入缓存
     * <p>
     * !!! 未使用
     *
     * @param dto
     */
    private void addCatch(DisProductListInfoDTO dto) {
        if (null == dto) {
            return;
        }
        //获取已售件数
        Long pid = dto.getProductId();
        int count = orderAppService.countGoodsNumByOrder(pid);
        stringRedisTemplate.opsForZSet().add(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, pid.toString(), count);
    }


    private Product getProductInfo(Product product, Long merchantId, Long shopId, int flag, BigDecimal platSupportRatio, Long productId) {
        log.info("have enter into getProductInfo and shopId=" + shopId + ",productId=" + productId);
        //重新生成一条商品信息入库
        product.setParentId(productId);
        product.setMerchantId(merchantId);
        product.setShopId(shopId);
        if (flag <= 0) {
            product.setStatus(0);
        } else {
            product.setStatus(1);
            product.setPutawayTime(new Date());
        }
        product.setSelfSupportRatio(null);
        // platSupportRatio 为商户在分销市场设置的商户抽佣比例。（注意：非供应商分佣）
        if (null != platSupportRatio) {
            BigDecimal selfSupportRatio = new BigDecimal(100).subtract(platSupportRatio);
            product.setSelfSupportRatio(selfSupportRatio);
        }

        //商品属性：2，分销
        product.setFromFlag(2);

        //获取佣金
        List<ProductSku> skuByProductId = productSkuMapper.findByProductId(product.getId());
        BigDecimal min = this.findPriceMinFromSku(skuByProductId);
        BigDecimal platSupportRatios = product.getPlatSupportRatio();
        if (platSupportRatios == null) {
            platSupportRatios = new BigDecimal(0);
        }
        PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(shopId.toString(), product.getProductSpecs(), min, platSupportRatios);
        BigDecimal priceAfterDist = platformDistRate.getPriceAfterDist();
        product.setCommissionNum(priceAfterDist);

        product.setCtime(new Date());
        return product;
    }

}
