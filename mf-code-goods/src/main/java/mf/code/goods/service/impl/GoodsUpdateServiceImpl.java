package mf.code.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.AddOrReduceEnum;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.feignclient.ShopAppService;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.CategoryDistributionDTO;
import mf.code.goods.dto.ProductUpdateReqDTO;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductDistributionAuditLog;
import mf.code.goods.repo.repostory.ProductDistributionAuditLogRepository;
import mf.code.goods.repo.repostory.ProductGroupMapRepository;
import mf.code.goods.repo.repostory.ProductGroupRepository;
import mf.code.goods.repo.repostory.SellerProductRepository;
import mf.code.goods.service.CategoryGroupMappingService;
import mf.code.goods.service.GoodsUpdateService;
import mf.code.shop.constants.AppTypeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-15 15:43
 */
@Slf4j
@Service
public class GoodsUpdateServiceImpl implements GoodsUpdateService {

    @Autowired
    private SellerProductRepository sellerProductRepository;
    @Autowired
    private ProductDistributionAuditLogRepository productDistributionAuditLogRepository;
    @Autowired
    private ProductGroupRepository productGroupRepository;
    @Autowired
    private ProductGroupMapRepository productGroupMapRepository;
    @Autowired
    private CategoryGroupMappingService categoryGroupMappingService;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ShopAppService shopAppService;

    /**
     * @param dto 入参
     * @return simpleResponse
     */
    @Override
    public SimpleResponse newbieProduct(ProductUpdateReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Product product = sellerProductRepository.selectById(dto.getProductId());
        if (product == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            simpleResponse.setMessage("查无商品信息");
            return simpleResponse;
        }
        // 加入新手专区
        if (dto.getShowType() == ProductShowTypeEnum.NEWBIE.getCode()) {
            return joinNewbieProduct(product);
        } else {
            return exitNewbieProduct(product);
        }
    }

    /**
     * 检查平台商城商品的创建流程
     *
     * @param merchantId  商户id
     * @param shopId      店铺id
     * @param goodsId     商品id
     * @param appTypeEnum
     * @return product
     */
    @Override
    public Product getProductForPlatMall(Long merchantId, Long shopId, Long goodsId, AppTypeEnum appTypeEnum) {
        Product product = sellerProductRepository.selectById(goodsId);
        if (product == null) {
            return null;
        }
        // 商户自己的商品，或者主动从分销市场分销的商品。1，传入商品id和店铺是匹配的；2，传入商品id是原始的，店铺为分销的店铺
        if (product.getShopId().equals(shopId)) {
            return product;
        }
        // 如果不是自家商品,并且还没有从分销市场分销商品，则需要创建一个商品
        if (product.getStatus() != ProductConstant.Status.ON_SALE
                || product.getCheckStatus() != ProductConstant.CheckStatus.PASS
                || product.getDel() != DelEnum.NO.getCode()
                || product.getPlatSupportRatio() == null) {
            return null;
        }
        // 如果merchantId为空
        if (merchantId == null) {
            Long merchantIdByShopId = shopAppService.getMerchantIdByShopId(shopId);
            if (merchantIdByShopId != null) {
                merchantId = merchantIdByShopId;
            }
        }
        // 再查询自家店铺是否创建过该分销商品
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.lambda()
                .eq(Product::getMerchantId, merchantId)
                .eq(Product::getShopId, shopId)
                .eq(Product::getParentId, goodsId)
        ;
        if (appTypeEnum == AppTypeEnum.DDD) {
            productQueryWrapper.and(params -> params.eq("del", DelEnum.NO.getCode()));
        }
        Product originProduct = productMapper.selectOne(productQueryWrapper);
        if (originProduct != null) {
            // 第二种情况，传入商品id是原始的，店铺为分销的店铺。直接返回该商品
            if (appTypeEnum == AppTypeEnum.DDD || appTypeEnum == AppTypeEnum.DXP) {
                if (originProduct.getFromFlag() == ProductFromFlagEnum.PLAT.getCode()) {
                    return originProduct;
                }
            } else {
                if (originProduct.getFromFlag() != ProductFromFlagEnum.PLAT.getCode()) {
                    return originProduct;
                }
            }

            // 异常情况快速失败
            if (originProduct.getFromFlag() != ProductFromFlagEnum.MALL.getCode()) {
                return null;
            }
            // 进入此步骤，则说明product为供应商商品。需要对分销商商品主动补回数据
            if (product.getStatus() != ProductConstant.Status.ON_SALE
                    || product.getDel() != DelEnum.NO.getCode()) {
                // 供应商商品下架或删除，快熟失败
                return null;
            }
            originProduct.setStatus(product.getStatus());
            originProduct.setPlatSupportRatio(product.getPlatSupportRatio());
            originProduct.setCheckStatus(product.getCheckStatus());
            originProduct.setCheckTime(product.getCheckTime());
            originProduct.setSubmitTime(product.getSubmitTime());
            originProduct.setPutawayTime(product.getPutawayTime());
            originProduct.setCommissionNum(product.getCommissionNum());
            CategoryDistributionDTO categoryDTO = categoryGroupMappingService.queryZeroLevelByFirst(product.getProductSpecs().toString());
            BigDecimal distributionRatio = new BigDecimal("90");
            if (categoryDTO == null || categoryDTO.getDistributionRatio() == null) {
                log.error("数据库查询不到数据，请确保商品类目数据无误");
            } else {
                if (categoryDTO.getDistributionRatio().intValue() >= 0 && categoryDTO.getDistributionRatio().intValue() <= 100) {
                    distributionRatio = new BigDecimal("100").subtract(categoryDTO.getDistributionRatio());
                }
            }
            originProduct.setSelfSupportRatio(distributionRatio);
            sellerProductRepository.updateBySelective(originProduct);
            return originProduct;
        }
        // 如果传入店铺与所属店铺不匹配，并且没有被创建过，则可能是从平台商城进入，需要给店铺一个默认。
        Product platMallProduct = new Product();
        BeanUtils.copyProperties(product, platMallProduct);
        platMallProduct.setId(null);
        platMallProduct.setOrderSales(0);
        platMallProduct.setMerchantId(merchantId);
        platMallProduct.setShopId(shopId);
        platMallProduct.setFromFlag(ProductFromFlagEnum.MALL.getCode());

        CategoryDistributionDTO categoryDTO = categoryGroupMappingService.queryZeroLevelByFirst(product.getProductSpecs().toString());
        BigDecimal distributionRatio = new BigDecimal("90");
        if (categoryDTO == null || categoryDTO.getDistributionRatio() == null) {
            log.error("数据库查询不到数据，请确保商品类目数据无误");
        } else {
            if (categoryDTO.getDistributionRatio().intValue() >= 0 && categoryDTO.getDistributionRatio().intValue() <= 100) {
                distributionRatio = new BigDecimal("100").subtract(categoryDTO.getDistributionRatio());
            }
        }
        platMallProduct.setSelfSupportRatio(distributionRatio);
        platMallProduct.setCtime(new Date());
        platMallProduct.setUtime(new Date());
        int count = productMapper.insertSelective(platMallProduct);
        if (count > 0) {
            product = platMallProduct;
        }
        return product;
    }

    /**
     * 退出新手专区
     *
     * @param product 商品
     * @return simpleResponse
     */
    private SimpleResponse exitNewbieProduct(Product product) {
        // 退出新手专区功能有逻辑上的悖论，当前价格不能修改，但是又要保证用户只能购买一次新手专区商品。所以直接屏蔽退出新手专区入口
        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "暂不支持退出新手专区");
//        if (product.getShowType() == ProductShowTypeEnum.NONE.getCode()) {
//            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
//            simpleResponse.setMessage("已取消新手专区，请勿重复操作");
//            return simpleResponse;
//        }
//        product.setShowType(ProductShowTypeEnum.NONE.getCode());
//        product.setUtime(new Date());
//        Integer integer = sellerProductRepostory.updateBySelective(product);
//        if (integer == 0) {
//            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
//            simpleResponse.setMessage("取消新手专区失败");
//            return simpleResponse;
//        }
//        return simpleResponse;
    }

    /**
     * 加入新手专区
     *
     * @param product 商品
     * @return simpleResponse
     */
    private SimpleResponse joinNewbieProduct(Product product) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (product.getShowType() == ProductShowTypeEnum.NEWBIE.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("已加入新手专区，请勿重复操作");
            return simpleResponse;
        }
        if (!product.getId().equals(product.getParentId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            simpleResponse.setMessage("自营商品才能加入新手专区");
            return simpleResponse;
        }
        if (product.getStatus() != ProductConstant.Status.ON_SALE) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("上架商品才能加入新手专区");
            return simpleResponse;
        }
        // 有分销属性，不能加入新手专区
        if (product.getPlatSupportRatio() != null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO15);
            simpleResponse.setMessage("自营商品且无分销才能加入新手专区");
            return simpleResponse;
        } else {
            ProductDistributionAuditLog auditLog = productDistributionAuditLogRepository.findByProductIdLimitOne(product.getId());
            // 如果有分销申请直接取消
            if (auditLog != null) {
                productDistributionAuditLogRepository.abandonAllDistributionByProduct(product.getId());
            }
        }
        product.setShowType(ProductShowTypeEnum.NEWBIE.getCode());
        product.setUtime(new Date());
        Integer integer = sellerProductRepository.updateBySelective(product);
        if (integer == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO16);
            simpleResponse.setMessage("加入新手专区失败");
            return simpleResponse;
        }
        // 加入新人专区，需要移除分组
        List<Long> groupIdList = productGroupMapRepository.selectGroupIdListByProductId(product.getId());
        if (CollectionUtils.isEmpty(groupIdList)) {
            return simpleResponse;
        }
        // 有分组关联，删除扣除商品数量
        Integer integer1 = productGroupMapRepository.updateDelByProductId(product.getId());
        if (integer1 != 0) {
            productGroupRepository.updateGoodsNumByIdList(groupIdList, AddOrReduceEnum.REDUCE);
        }
        return simpleResponse;
    }
}
