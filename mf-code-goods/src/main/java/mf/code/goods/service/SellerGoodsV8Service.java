package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.seller.dto.PlateDisProductListResultDTO;
import mf.code.goods.api.seller.dto.SpecesAndBannerDTO;
import mf.code.goods.repo.po.Product;
import mf.code.shop.constants.AppTypeEnum;

import java.util.List;

/**
 * create by qc on 2019/8/19 0019
 */
public interface SellerGoodsV8Service {
    /**
     * 抖带带选货列表
     *
     * @param offset    偏移量
     * @param limit     每页数量
     * @param shopId    店铺
     * @param speceType 商品类目
     * @param userId    用户
     * @param sort      排序 1今日上新 2热销 3高佣
     * @return PlateDisProductListResultDTO
     */
    PlateDisProductListResultDTO pickProductList(Integer offset, Integer limit, Long shopId, String speceType, Long userId, Integer sort);

    /**
     * 通过父类目查询商品列表
     *
     * @param specesId
     * @return
     */
    List<Product> findProductBySpecId(String specesId);

    /**
     * 查本店全部分销商品列表
     *
     * @param shopId
     * @return
     */
    List<Product> findDistrProductList(Long shopId);

    /**
     * 处理抖带带的类目
     *
     *
     * @param dtoList
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse handlDDDSpec(List<SpecesAndBannerDTO> dtoList, Long shopId, Long userId);

    /***
     * 获取商品轮播详细
     *
     *
     * @param shopId
     * @param userId
     * @param appTypeEnum
     * @return
     */
    SimpleResponse getBannerList(Long shopId, Long userId, AppTypeEnum appTypeEnum);
}
