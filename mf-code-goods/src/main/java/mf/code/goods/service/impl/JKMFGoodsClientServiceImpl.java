package mf.code.goods.service.impl;/**
 * create by qc on 2019/9/21 0021
 */

import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.api.feignclient.ShopAppService;
import mf.code.goods.api.seller.dto.DisProductListInfoDTO;
import mf.code.goods.api.seller.dto.DisProductListResultDTO;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dto.GoodsListPageDTO;
import mf.code.goods.repo.po.NetPlatformGoodsPo;
import mf.code.goods.repo.vo.NetPlatformGoodsListVo;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.JKMFGoodsClientService;
import mf.code.goods.service.SellerGoodsDistributionV5Service;
import mf.code.shop.dto.ShopDetailForPlatformDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author gbf
 * 2019/9/21 0021、11:49
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class JKMFGoodsClientServiceImpl implements JKMFGoodsClientService {
    private final SellerGoodsDistributionV5Service sellerGoodsDistributionV5Service;
    private final StringRedisTemplate stringRedisTemplate;
    private final GoodsService goodsService;
    private final ProductMapper productMapper;
    /***服务间*/
    private final ShopAppService shopAppService;

    /**
     * 商品列表
     * <p>
     * 维度类型 1:供应商 2:商品
     * 标签类型 1:爆款 2:特价 3:尾货 4:全部
     * 查询关键字
     * 商品类目
     * 排序 1:综合排序(销量) 2:上新时间
     * 当前页
     * 每页数
     *
     * @return json
     */
    @Override
    public String goodsList(GoodsListPageDTO goodsListPageDTO) {
        Integer sort = goodsListPageDTO.getSort();

        // 如果有查询条件,就不走缓存
        if (StringUtils.isNotBlank(goodsListPageDTO.getKeyWord())) {
            sort = 2;
        }
        if (StringUtils.isNotBlank(goodsListPageDTO.getSpec())) {
            sort = 2;
        }

        /* bornFlag 他的代码中此标识为字符串,但只判断是否空进行上新排序
         * bornFlag =空 销量排序使用了缓存
         * bornFlag!=空 直接在数据库中order by */
        String bornFlag = sort == 1 ? "" : "isNotBlank";

        Map<String, Object> sqlParams = new HashMap<>();
        // 类目
        sqlParams.put("kinds", goodsListPageDTO.getSpec());
        // 查询关键字
        sqlParams.put("productName", goodsListPageDTO.getKeyWord());
        // 排序 上新
        sqlParams.put("bornFlag", bornFlag);
        // 分页
        sqlParams.put("pageNum", (goodsListPageDTO.getPageCurrent() - 1) * 10);
        sqlParams.put("pageSize", goodsListPageDTO.getPageSize());
        sqlParams.put("labType", goodsListPageDTO.getLabType());

        DisProductListResultDTO disProductListResultBySwitchWrap = sellerGoodsDistributionV5Service.getDisProductListResultBySwitch(sqlParams, "0", bornFlag, goodsListPageDTO.getPageCurrent(), goodsListPageDTO.getPageSize());

        /** 获取列表 */
        List<DisProductListInfoDTO> disProductList = (disProductListResultBySwitchWrap == null || disProductListResultBySwitchWrap.getDisProductList() == null) ? new ArrayList<>() :
                disProductListResultBySwitchWrap.getDisProductList();

        /** 包装返回数据 */
        List<NetPlatformGoodsListVo> netPlatformGoodsListVoList = new ArrayList<>();
        List<Long> shopIdList = new ArrayList<>();
        for (DisProductListInfoDTO dto : disProductList) {
            NetPlatformGoodsListVo vo = new NetPlatformGoodsListVo();
            vo.setProductId(dto.getProductId());
            vo.setMainPic(dto.getMainPic());
            vo.setPrice(dto.getSalePrice());
            vo.setFkPrice(dto.getThirdPrice());
            vo.setProductTitle(dto.getTitle());

            SimpleResponse detailResp = goodsService.detail(dto.getProductId());

            Integer saleVol = 0;
            Integer stock = -1;
            if (detailResp != null && detailResp.getData() != null) {
                Map data = (Map) detailResp.getData();
                stock = (Integer) data.get("stock");
                saleVol = (Integer) data.get("saleVolume");
            }

            if (goodsListPageDTO.getLabType() == 1) {
                // 爆款 历史销量超过10个的宝贝
                if (saleVol <= 10) {
                    continue;
                }
            }
            if (goodsListPageDTO.getLabType() == 2) {
                // 特价 价格处在39元之下的宝贝
                if (vo.getPrice().compareTo(new BigDecimal(39)) > 0) {
                    continue;
                }
            }
            if (goodsListPageDTO.getLabType() == 3) {
                // 尾货 总库存≤100的宝贝
                if (stock > 100) {
                    continue;
                }
            }


            // 热销 销量 >10
            vo.setPop(saleVol > 10 ? 1 : 0);
            // 清仓 库存 <=100
            vo.setClean((stock != -1 && stock <= 100) ? 1 : 0);
            // 限时特价 价格小于39
            vo.setLimit(dto.getSalePrice().compareTo(this.getPopGoodsPricePropInRedis()) < 0 ? 1 : 0);

            vo.setShopId(dto.getShopId());
            shopIdList.add(dto.getShopId());
            netPlatformGoodsListVoList.add(vo);
        }
        // 批量获取店铺信息
        List<ShopDetailForPlatformDTO> shopListByShopIdList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(shopIdList)) {
            shopListByShopIdList = shopAppService.getShopListByShopIdList(shopIdList);
        }
        for (NetPlatformGoodsListVo vo : netPlatformGoodsListVoList) {
            for (ShopDetailForPlatformDTO shopDto : shopListByShopIdList) {
                if (shopDto.getId().equals(vo.getShopId())) {
                    vo.setShopName(shopDto.getShopName());
                    vo.setShopLogo(shopDto.getPicPath());
                }
            }
        }
        Date now = new Date();
        sqlParams.put("begin", DateUtil.begin(now));
        sqlParams.put("end", DateUtil.end(now));
        List<NetPlatformGoodsPo> netPlatformGoodsList = productMapper.findNetPlatformGoodsList(sqlParams);
        sqlParams.put("begin", null);
        sqlParams.put("end", null);
        List<NetPlatformGoodsPo> netPlatformGoodsListAll = productMapper.findNetPlatformGoodsList(sqlParams);
        Set<Long> shopSet = new HashSet<>();
        for (NetPlatformGoodsPo po : netPlatformGoodsListAll) {
            shopSet.add(po.getShopId());
        }
        // 爆款
        Integer popToday = 0;
        // 限时特价
        Integer limitToday = 0;
        // 清仓
        Integer cleanToday = 0;
        // 今日全部
        Integer allToday = CollectionUtils.isEmpty(netPlatformGoodsList) ? 0 : netPlatformGoodsList.size();
        for (NetPlatformGoodsPo po : netPlatformGoodsList) {
            if (po.getSaleVol() > 10) {
                popToday++;
            }
            if (po.getPrice().compareTo(this.getPopGoodsPricePropInRedis()) < 0) {
                limitToday++;
            }
            if (po.getStock() < 100) {
                cleanToday++;
            }
        }
        // 分销市场全部商品
        int allGoods = CollectionUtils.isEmpty(netPlatformGoodsListAll) ? 0 : netPlatformGoodsListAll.size();
        Map<String, Integer> number = new HashMap<>();
        number.put("provider", shopSet.size());
        number.put("goods", allGoods);
        number.put("pop", popToday);
        number.put("limit", limitToday);
        number.put("clean", cleanToday);
        number.put("allToday", netPlatformGoodsList.size());
        number.put("all", disProductListResultBySwitchWrap.getCount());

        Map<String, Object> resultDataMap = new HashMap<>();
        resultDataMap.put("number", number);
        resultDataMap.put("list", netPlatformGoodsListVoList);
        resultDataMap.put("refreshTime", this.getRefreshTime());
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 全网通调用集客魔方商品列表返回结果:{}", resultDataMap);
        try {
            return new String(JSON.toJSONString(resultDataMap).getBytes("utf-8"), "ISO-8859-1");
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * 刷新时间
     *
     * @return
     */
    private String getRefreshTime() {
        String redisKey = "net:plt:goods:rt";
        String redisVal = stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isNotBlank(redisVal)) {
            return redisVal;
        }
        String nowStr = DateUtil.dateToString(new Date(), DateUtil.FORMAT_ONE);
        stringRedisTemplate.opsForValue().set(redisKey, nowStr);
        stringRedisTemplate.expire(redisKey, 2, TimeUnit.HOURS);
        return nowStr;
    }

    /**
     * 获取redis中配置的限时特价的价格
     *
     * @return
     */
    public BigDecimal getPopGoodsPricePropInRedis() {
        String redisKey = "net:plt:goods:pop:price";
        String priceProp = stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isNotBlank(priceProp)) {
            try {
                return new BigDecimal(priceProp);
            } catch (Exception e) {
                log.error(e.toString());
                return new BigDecimal(39);
            }
        } else {
            stringRedisTemplate.opsForValue().set(redisKey, "39");
            return new BigDecimal(39);
        }
    }


    /**
     * 获取商品详情
     *
     * @param productId 商品id
     * @return
     */
    @Override
    public String detail(Long productId) {
        SimpleResponse simpleResponse = sellerGoodsDistributionV5Service.detail(productId, 0L);
        if (simpleResponse == null) {
            return null;
        }
        Object data = simpleResponse.getData();
        if (data == null) {
            return null;
        }
        Map dataMap;
        try {
            dataMap = (Map) data;
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
        BigDecimal priceMin = (BigDecimal) dataMap.get("priceMin");
        Integer saleVolume = (Integer) dataMap.get("saleVolume");
        Integer stock = (Integer) dataMap.get("stock");
        Integer pop = 0;
        Integer clean = 0;
        Integer limit = 0;
        if (saleVolume > 10) {
            pop = 1;
        }
        if (stock < 100) {
            clean = 1;
        }
        if (priceMin.compareTo(new BigDecimal(39)) < 0) {
            limit = 1;
        }
        dataMap.put("pop", pop);
        dataMap.put("clean", clean);
        dataMap.put("limit", limit);
        try {
            return new String(JSON.toJSONString(dataMap).getBytes("utf-8"), "ISO-8859-1");
        } catch (Exception e) {
            log.info("转码报错");
            return null;
        }
    }

    /**
     * 获取商品类目
     */
    @Override
    public String getSpecs() {
        SimpleResponse simpleResponse = goodsService.getSpecs(0L, 0L);
        if (simpleResponse == null || simpleResponse.getData() == null) {
            return null;
        }
        List data = (List<Map<String, Object>>) simpleResponse.getData();
        try {
            return new String(JSON.toJSONString(data).getBytes("utf-8"), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
