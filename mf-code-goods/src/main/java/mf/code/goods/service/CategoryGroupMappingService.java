package mf.code.goods.service;

import mf.code.goods.dto.CategoryDistributionDTO;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.goods.service
 * Description: 一级淘宝类目与类目分组之间的映射关系
 *
 * @author gel
 * @date 2019-06-24 17:42
 */
public interface CategoryGroupMappingService {


    /**
     * 根据0级新类目sid查询1级类目
     *
     * @param parentSid
     * @return
     */
    List<String> queryFirstLevelByParentSid(String parentSid);

    /**
     * 查询所有零级类目属性
     *
     * @return
     */
    List<CategoryDistributionDTO> queryAllZeroLevel();

    /**
     * 通过一级类目sid查询零级类目
     *
     * @return
     */
    CategoryDistributionDTO queryZeroLevelByFirst(String oneSid);

    /**
     * 修改零级类目抽佣比例和顺序
     *
     * @return
     */
    CategoryDistributionDTO updateZeroLevelRatioAndSort(String zeroSid, BigDecimal ratio, Integer sort);
}
