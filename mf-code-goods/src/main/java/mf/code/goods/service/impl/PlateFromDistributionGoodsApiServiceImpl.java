package mf.code.goods.service.impl;

import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RandomStrUtil;
import mf.code.goods.common.caller.aliyunoss.OssCaller;
import mf.code.goods.dto.CategoryDistributionDTO;
import mf.code.goods.repo.dao.CategoryTaobaoMapper;
import mf.code.goods.repo.dao.CommonDictMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.po.CommonDict;
import mf.code.goods.service.CategoryGroupMappingService;
import mf.code.goods.service.CommonDictService;
import mf.code.goods.service.PlateFromDistributionGoodsApiService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import static mf.code.common.utils.FileUtil.checkFileType;

@Service
@Slf4j
public class PlateFromDistributionGoodsApiServiceImpl implements PlateFromDistributionGoodsApiService {
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private CategoryTaobaoMapper categoryTaobaoMapper;
    @Autowired
    private CategoryGroupMappingService categoryGroupMappingService;

    private  String KEY = "platformDistribution";

    @Override
    public SimpleResponse uploadPicture(MultipartFile image, String key, String type) {

        long PIC_MAX_SIZE = 1024L  * 1024;
        if (image.getSize() > PIC_MAX_SIZE) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "上传图片大小过大");
        }
        String picName = System.currentTimeMillis() + RandomStrUtil.randomStr(3);
        String bucketName = KEY + "/";

        String ext = FilenameUtils.getExtension(image.getOriginalFilename());
        String[] imageType = {"gif", "png", "jpg", "jpeg", "bmp"};
        if (!checkFileType(imageType, ext)) {
            log.error("文件格式格式错误");
        }
        String fileName = picName + "." + ext;
        String bucket = bucketName + fileName;
        String imgUrl = "";
        try {
            imgUrl = ossCaller.uploadObject2OSSInputstream(image.getInputStream(), bucket);
        } catch (IOException e) {
            e.printStackTrace();
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("imgUrl", imgUrl);
        return new SimpleResponse(ApiStatusEnum.SUCCESS,map);
    }

    @Override
    public SimpleResponse getNewSpecesInfo(String plateFromId) {
        List<CategoryDistributionDTO> list = categoryGroupMappingService.queryAllZeroLevel();
//        List<CategoryTaobao> categoryTaobaos = categoryTaobaoMapper.selectNewSpeceTypeInfo(null);
        if (CollectionUtils.isEmpty(list)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_SYSTEM);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, list);
    }

    @Override
    public SimpleResponse updateSpecesInfo(String specesId, BigDecimal ratio) {

        List<CategoryTaobao> categoryTaobaos = categoryTaobaoMapper.selectNewSpeceTypeInfoByparentId(specesId);
        if (CollectionUtils.isEmpty(categoryTaobaos)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_SYSTEM);
        }

        Map map = new HashMap<>();
        map.put("specesId", specesId);
        map.put("ratio", ratio);
        int count = categoryTaobaoMapper.updateSpecesInfo(map);

        if (count <= 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_SYSTEM);
        }

        return new SimpleResponse();
    }

    @Override
    public SimpleResponse saveUploadPics(String mainPicsList, String key, String type) {
        if (StringUtils.isEmpty(type)){
            type = KEY;
        }
        if(StringUtils.isEmpty(key)){
            key="1000000000";
        }
        //查看是否已经保存过该分类的图片集,已经存在则修改，没有则新增
        CommonDict commonDict  = commonDictService.selectByTypeKey(type,key);

        if (null==commonDict){
            commonDict = new CommonDict();
            commonDict.setValue(mainPicsList);
            commonDict.setKey(key);
            commonDict.setType(type);
            commonDict.setUtime(new Date());
            commonDict.setCtime(new Date());
            commonDict.setValue1("");
            commonDict.setValue2("");
            commonDict.setParentId(0L);
            commonDict.setRemark("");
            commonDict.setSort(999);
            int count = commonDictService.create(commonDict);
            if (count<=0){
                return new SimpleResponse(ApiStatusEnum.ERROR);
            }
            return new SimpleResponse();
        }
        else{
            commonDict.setValue(mainPicsList);
            commonDict.setCtime(new Date());

            int num = commonDictService.update(commonDict);

            if (num<=0){
                return new SimpleResponse(ApiStatusEnum.ERROR);
            }
            return new SimpleResponse();
        }
    }

    @Override
    public SimpleResponse showHaveUploadPics(String key, String type) {
        if(StringUtils.isEmpty(key)){
            key="1000000000";
        }
        if (StringUtils.isEmpty(type)){
            type = KEY;
        }
        CommonDict commonDict  = commonDictService.selectByTypeKey(type,key);

        if (null==commonDict ){
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        String pics = commonDict.getValue();
        if (null ==pics ){
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        List<String> minPicsList = new ArrayList<>();
        try{
            minPicsList= JSON.parseArray(pics, String.class);
        }catch (RuntimeException e){
            log.error("have find showHaveUploadPics minPicsList to json error",e);
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        Map map = new HashMap();
        map.put("minPicsList",minPicsList);

        return new SimpleResponse(ApiStatusEnum.SUCCESS,map);
    }


}
