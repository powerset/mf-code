package mf.code.goods.service;/**
 * create by qc on 2019/4/9 0009
 */

import mf.code.common.simpleresp.SimpleResponse;

/**
 * @author gbf
 * 2019/4/9 0009、14:53
 */
public interface SellerGoodsAssistService {
    /**
     * 列表头部的统计数据 仓库中数量/在售中数量
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse count(Long merchantId, Long shopId);

    /**
     * 该库存前置查询
     * @param merchantId
     * @param shopId
     * @param productId
     * @return
     */
    SimpleResponse updateStockBefore(Long merchantId, long shopId, long productId);

    /**
     * 改价前置查询
     * @param merchantId
     * @param shopId
     * @param productId
     * @return
     */
    SimpleResponse updatePriceBefore(Long merchantId, Long shopId, Long productId);

    /**
     * 获取商品的规格类目
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse productSpecs(Long merchantId, Long shopId);

    /**
     * 上报规格类目
     */
    SimpleResponse postSpecValue(Long merchantId, Long shopId, Long parentId, String specName);

    /**
     * 审核不通过原因
     * @param productId
     * @return
     */
    SimpleResponse getCheckReason(Long productId);
}
