package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.scheduling.annotation.Async;

/**
 * mf.code.goods.service
 * Description:
 *
 * @author gel
 * @date 2019-06-15 16:27
 */
public interface MasterFixDataService {

    @Async
    SimpleResponse batchAddCacheByProductId(Integer sort);

    @Async
    SimpleResponse batchAddCacheByProductId();


    /**
     * 无用的方法
     * @param productId
     * @param num
     */
    @Async
    void addCacheByProductId(Long productId, int num);
}
