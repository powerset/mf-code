package mf.code.goods.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.domain.aggregateroot.PromotionProduct;
import mf.code.goods.repo.repostory.PromotionProductRepository;
import mf.code.goods.service.PlatformProductPromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 13:03
 */
@Slf4j
@Service
public class PlatformProductPromotionServiceImpl implements PlatformProductPromotionService {
	@Autowired
	private PromotionProductRepository promotionProductRepository;

	/**
	 * 本周特推 -- 加入特推管理
	 *
	 * @param productId
	 * @return
	 */
	@Override
	public SimpleResponse addHighlyRecommend(Long productId) {
		PromotionProduct promotionProduct = promotionProductRepository.findById(productId);
		if (promotionProduct == null) {
			log.error("商品不存在, productId = {}", productId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "商品不存在");
		}
		// 校验产品是否符合推广活动要求 -- 本次迭代暂不考虑


		return null;
	}
}
