package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.applet.ProductPlateGoodsDistributionApi;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.api.feignclient.OneAppService;
import mf.code.goods.api.seller.dto.DisProductListInfoDTO;
import mf.code.goods.api.seller.dto.PlateDisProductListResultDTO;
import mf.code.goods.api.seller.dto.SpecesAndBannerDTO;
import mf.code.goods.common.redis.RedisForbidRepeat;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.common.tlab.AppTypeRequestContext;
import mf.code.goods.dto.CategoryDistributionDTO;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.goods.repo.dao.SellerGoodsDistributionMapper;
import mf.code.goods.repo.dao.SellerPlateGoodsDistributionMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.po.CommonDict;
import mf.code.goods.repo.repostory.CategoryTaobaoRepository;
import mf.code.goods.service.*;
import mf.code.one.dto.ApolloLHYXDTO;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class SellerPlateGoodsDistributionApiServiceImpl implements SellerPlateGoodsDistributionApiService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SellerPlateGoodsDistributionMapper sellerPlateGoodsDistributionMapper;
    @Autowired
    private SellerGoodsDistributionMapper sellerGoodsDistributionMapper;
    @Autowired
    private MasterFixDataService masterFixDataService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private CategoryGroupMappingService categoryGroupMappingService;
    @Autowired
    private CategoryTaobaoRepository categoryTaobaoRepository;
    @Autowired
    private OneAppService oneAppService;

    //复制zset1中的数据
    public static final String NEW_KEY = "jkmf:plate:distribution:salesvolume:list";

    private static final String KEY_IS_NOT_EXIT = NEW_KEY + ":" + "isnotfind";

    //获取本店铺根据类目查询的商品缓存key zset3
    public static final String NEW_KEY_SPECS = NEW_KEY + ":" + "specs";

    //根据类目获取该类目下的产品信息 zset2
    private static final String NEW_KEY_SID = NEW_KEY + ":" + "sid";

    private static final String TYPE = "platformDistribution";

    //缓存失效时间是一小时
    private static final Long TIME_OUT = 10L;

    @Value("${lhyx.yrTitle}")
    private String yrTitle;
    @Value("${lhyx.hdTitle}")
    private String hdTitle;


    @Override
    public PlateDisProductListResultDTO getPalteDistributionListResult(Integer limit, Integer offset, String shopId, String speceType, Long userId, Integer sort) {
        //获取含有分销属性的数据--缓存中有则从缓存中获取，缓存中没有则调接口获取
        Long sizeNum = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST);
        List<ProductDistributionDTO> pdisProductList = new ArrayList<>();

        //总条数
        int count;
        //从缓存中获取数据,为空则从新加载
        if (null == sizeNum || sizeNum == 0) {
            masterFixDataService.batchAddCacheByProductId(sort);
        } else {
            pdisProductList = getPlateDistributionProductList(shopId, limit, offset, speceType, userId, sort);
        }

        Long nums = stringRedisTemplate.opsForZSet().size(NEW_KEY_SPECS + ":" + shopId + ":" + speceType);
        if (null == nums) {
            nums = 0L;
        }
        count = nums.intValue();

        PlateDisProductListResultDTO dto = new PlateDisProductListResultDTO();
        dto.setProductDistributionDTO(pdisProductList);
        dto.setDisProductList(new ArrayList<>());
        dto.setCount(count);
        return dto;
    }

    @Override
    public PlateDisProductListResultDTO getPalteDistributionListResult(Integer limit, Integer offset, String shopId, String speceType, Long userId) {
        return this.getPalteDistributionListResult(limit, offset, shopId, speceType, userId, 2);
    }

    @Override
    public SimpleResponse batchUpdateSpecsInfo() {
        return null;
    }

    @Override
    public List<SpecesAndBannerDTO> getBannerAndSpece(Long shopId, Long userId) {

        List<CategoryDistributionDTO> list = new ArrayList<>();
        //缓存中获取类目类型
        String redisKey = RedisKeyConstant.PRODUCT_CATEGORY_ZERO_SHOP + shopId;
        Set<String> members = stringRedisTemplate.opsForSet().members(redisKey);
        // 如果没有的话则需要返回全部类目
        if (CollectionUtils.isEmpty(members)) {
            list = categoryGroupMappingService.queryAllZeroLevel();
        } else {
            for (String sid : members) {
                CategoryTaobao categoryTaobao = categoryTaobaoRepository.selectBySid(sid, 0);
                CategoryDistributionDTO categoryDistributionDTO = new CategoryDistributionDTO();
                categoryDistributionDTO.setSort(categoryTaobao.getSort());
                categoryDistributionDTO.setSid(categoryTaobao.getSid());
                categoryDistributionDTO.setName(categoryTaobao.getName());
                categoryDistributionDTO.setLevel(categoryTaobao.getLevel());
                categoryDistributionDTO.setDistributionRatio(categoryTaobao.getDistributionRatio());
                list.add(categoryDistributionDTO);
            }
        }

        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        List<SpecesAndBannerDTO> sdtoList = new ArrayList<>();

        // 添加联合营销活动类目,判断是否开启改类目
        ApolloLHYXDTO apolloLHYXDTO = oneAppService.getUnionConf();
        if (apolloLHYXDTO != null) {
            Integer status = apolloLHYXDTO.getStatus();
            SpecesAndBannerDTO sdto = new SpecesAndBannerDTO();
            sdto.setSpecesId("0");
            if (!CollectionUtils.isEmpty(apolloLHYXDTO.getBanner())) {
                List<String> urlList = new ArrayList<>();
                for (ApolloLHYXDTO.Banner banner : apolloLHYXDTO.getBanner()) {
                    urlList.add(banner.getUrl());
                }
                sdto.setBannerUrl(JSON.toJSONString(urlList));
            }
            if (status != null) {
                //  0：预热活动未开始 1：预热活动进行中 2：预热活动已结束 3：正式活动进行中 4：正式活动已结束
                if (status == 1) {
                    // 添加会场类目
                    sdto.setSpecesName(yrTitle);
                    sdtoList.add(sdto);
                } else if (status == 3) {
                    // 添加会场类目
                    sdto.setSpecesName(hdTitle);
                    sdtoList.add(sdto);
                }
            }
        }

        // 添加推荐的sid
        String sidDefault = "1000000000";
        CommonDict comDict = commonDictService.selectByTypeKey(TYPE, sidDefault);

        if (null != comDict) {
            SpecesAndBannerDTO sdto = new SpecesAndBannerDTO();

            sdto.setBannerUrl(comDict.getValue());
            sdto.setSpecesId(comDict.getKey());
            sdto.setSpecesName("推荐");
            sdtoList.add(sdto);
        }

        for (CategoryDistributionDTO dto : list) {
            String sid = dto.getSid();

            //获取banner信息
            CommonDict com = commonDictService.selectByTypeKey(TYPE, sid);
            String bannerUrl = null;
            if (null != com) {
                bannerUrl = com.getValue();
            }

            SpecesAndBannerDTO sdto = new SpecesAndBannerDTO();
            sdto.setBannerUrl(bannerUrl);
            sdto.setSpecesId(sid);
            sdto.setSpecesName(dto.getName());

            sdtoList.add(sdto);
        }

        return sdtoList;
    }


    /**
     * 获取本店铺含有分销属性的商品ID且此商品没有在本店铺分销过
     *
     * @return
     */
    private List<ProductDistributionDTO> getPlateDistributionProductList(String shopId, int offset, int limit, String spacesId, Long userId, Integer sort) {

        // 如果zset3存在并且能查询商品，直接返回
        List<ProductDistributionDTO> disList = getList(shopId, spacesId, limit, offset, userId);
        if (!CollectionUtils.isEmpty(disList)) {
            return disList;
        }
        //分布式缓存，加入防重锁
        String redisRepeatKey = NEW_KEY_SID + ":lock:" + shopId + ":" + spacesId;
        if (!RedisForbidRepeat.NX(stringRedisTemplate, redisRepeatKey, 3, TimeUnit.SECONDS)) {
            log.error("<<<<<<<<该缓存还在使用中:" + redisRepeatKey);
            return disList;
        }

        try {
            //新增缓存：zset1,从老缓存中复制数据到zset1 --- 将原列表zset复制
            copyCacheFromOldZset(shopId);


            //获取本店铺分销过的商品信息
            List<DisProductListInfoDTO> list = new ArrayList<>();
            /** 抖带带业务 如果分销过 , 就不展示*/
            if (AppTypeEnum.DDD == AppTypeRequestContext.getAppType()) {
                // 1.抖带带没用下架就是删除. 2.如果供应商终止推广,抖带带的分销商品会变为-1. 3.排除已经分销但下架的状态.
                list = sellerPlateGoodsDistributionMapper.gethaveDistributionGoodsInfo4DDD(shopId);
            } else {
                // 集客魔方逻辑
                list = sellerPlateGoodsDistributionMapper.gethaveDistributionGoodsInfo(shopId);
            }
            log.info("qc-test------->获取到的appType={}", AppTypeRequestContext.getAppType());
            /** 抖带带业务 */
            //缓存zset1，在缓存中存放不包含本店铺分销过的商品信息  --- 从原zset中删除已经分销的商品
            removeHaveDistributionGoods(list, shopId);

            //根据店铺，商品ID，类目ID获取商品信息
            Map<String, Object> map = new HashMap<>();
//            map.put("shopId", shopId);//此处夜辰感觉无意义，如果你觉得有，请跟我讨论
            //根据传过来的新类目ID获取老类目ID，specesId=0表示查询全部
            List<String> sidList = null;
            if (!StringUtils.equals("1000000000", spacesId)) {
                sidList = categoryGroupMappingService.queryFirstLevelByParentSid(spacesId);
                map.put("sidList", sidList);
            }

            //根据类目获取分销产品列表（已经去除在本店铺分销过的产品）
            List<DisProductListInfoDTO> listByPidAndSid = sellerPlateGoodsDistributionMapper.queryByProductidAndSid(map);

            //新增缓存新增缓存zset2，缓存中存放根据类目筛选后的商品信息   --- zset2->根据类目查询的商品列表,value=商品id,分数=0.
            addCacheByProductId(listByPidAndSid, shopId, spacesId);

            //获取zset1和zset2的交集后的结果zset3  ---- 合并zset,获得传说中的zset3 , 作废zset2
            getCacheFromZset1AndZset2Intersection(shopId, spacesId, sort);

            //迭代缓存zset2，获取缓存中商品的详情，并根据销量排序  ---------------这里应该为: 迭代缓存zset3;zset2已经被删除
            disList = getList(shopId, spacesId, limit, offset, userId);
        } finally {
            //删除防重rediskey
            stringRedisTemplate.delete(redisRepeatKey);
        }

        return disList;
    }

    /**
     * 获取zset1和zset2的交集后的结果zset3
     *
     * @param shopId
     * @param spacesId
     * @param sort     销量排序 sort==null:集客魔方接口 sort有值抖带带
     */
    private void getCacheFromZset1AndZset2Intersection(String shopId, String spacesId, Integer sort) {

        String zset1 = NEW_KEY + ":" + shopId;
        String zset2 = NEW_KEY_SID + ":" + shopId + ":" + spacesId;
        String zset3 = NEW_KEY_SPECS + ":" + shopId + ":" + spacesId;


        Long sizeNum = stringRedisTemplate.opsForZSet().size(zset3);
        if (null != sizeNum && sizeNum > 0) {
            return;
        }
        stringRedisTemplate.opsForZSet().intersectAndStore(zset1, zset2, zset3);
        stringRedisTemplate.expire(zset3, TIME_OUT, TimeUnit.MINUTES);
        //当生成zset3时，zset2作为临时缓存就失去了作用，需要删除
        stringRedisTemplate.delete(zset2);
        //测试专用：缓存失效时间为1分钟
//        stringRedisTemplate.expire(zset3, 3, TimeUnit.MINUTES);


    }

    /**
     * 获取平台分销列表
     *
     * @param shopId
     * @param spacesId
     * @param limit
     * @param offset
     * @return
     */
    private List<ProductDistributionDTO> getList(String shopId, String spacesId, int offset, int limit, Long userId) {

        //排序后的list
        List<ProductDistributionDTO> disList = new ArrayList<>();

        int start = limit;
        int end = limit + offset - 1;

        //从新增缓存zset3中获取商品的详细信息
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                NEW_KEY_SPECS + ":" + shopId + ":" + spacesId, start, end);
        if (CollectionUtils.isEmpty(typedTuples)) {
            return disList;
        }

        List<Long> productIds = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            if (StringUtils.isNotBlank(value)) {
                productIds.add(Long.valueOf(value));
            }
        }
        //排序前的list
        List<ProductDistributionDTO> disProductList = queryByIds(productIds, shopId, userId, spacesId);

        if (CollectionUtils.isEmpty(disProductList)) {
            return disList;
        }

        //重新排序
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            if (StringUtils.isNotBlank(value)) {
                for (ProductDistributionDTO dto : disProductList) {
                    if (Long.valueOf(value).equals(dto.getProductId())) {
                        disList.add(dto);
                    }
                }
            }
        }

        return disList;
    }


    /**
     * 新增缓存：zset1,从就缓存中复制数据到zset1
     */
    private void copyCacheFromOldZset(String shopId) {
        //当缓存不为空的时候才会复制或者新增
        Long sizeNum = stringRedisTemplate.opsForZSet().size(NEW_KEY + ":" + shopId);
        if (null != sizeNum && sizeNum > 0) {
            return;
        }
        stringRedisTemplate.opsForZSet().unionAndStore(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, KEY_IS_NOT_EXIT, NEW_KEY + ":" + shopId);
        stringRedisTemplate.expire(NEW_KEY + ":" + shopId, TIME_OUT, TimeUnit.MINUTES);

    }

    /**
     * 新增缓存--缓存中存放根据类目ID获取的商品信息
     *
     * @param listByPidAndSid
     * @param shopId
     * @param spacesId
     */
    private void addCacheByProductId(List<DisProductListInfoDTO> listByPidAndSid, String shopId, String spacesId) {

        //当缓存不为空的时候才会复制或者新增
        Long sizeNum = stringRedisTemplate.opsForZSet().size(NEW_KEY_SID + ":" + shopId + ":" + spacesId);
        if (null != sizeNum && sizeNum > 0) {
            return;
        }

        for (DisProductListInfoDTO dto : listByPidAndSid) {
            Long productId = dto.getParentId();
            stringRedisTemplate.opsForZSet().add(NEW_KEY_SID + ":" + shopId + ":" + spacesId, productId.toString(), 0);
        }

        stringRedisTemplate.expire(NEW_KEY_SID + ":" + shopId + ":" + spacesId, TIME_OUT, TimeUnit.MINUTES);
        //测试专用：缓存失效时间为1分钟
//        stringRedisTemplate.expire(NEW_KEY_SID + ":" + shopId + ":" + spacesId, 3, TimeUnit.MINUTES);

    }


    /**
     * 去除产品中已经被本店铺分销过的产品，并将数据放入缓存
     *
     * @param list
     */
    public void removeHaveDistributionGoods(List<DisProductListInfoDTO> list, String shopId) {

        //如果本店铺没有分销过含有分销属性的商品，则不做任何操作
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        Long sizeNum = stringRedisTemplate.opsForZSet().size(NEW_KEY + ":" + shopId);
        if (null == sizeNum || sizeNum < 1) {
            return;
        }

        for (DisProductListInfoDTO dto : list) {
            Long productId = dto.getParentId();
            stringRedisTemplate.opsForZSet().remove(NEW_KEY + ":" + shopId, productId.toString());
        }

        stringRedisTemplate.expire(NEW_KEY + ":" + shopId, TIME_OUT, TimeUnit.MINUTES);
//                        //测试专用：缓存失效时间是1分钟
//                        stringRedisTemplate.expire(NEW_KEY + ":" + shopId, 1, TimeUnit.MINUTES);

    }


    /**
     * 批量获取含有分销属性的产品信息
     *
     * @param productIds
     * @param shopid
     * @return
     */
    public List<ProductDistributionDTO> queryByIds(List<Long> productIds, String shopid, Long userId, String spacesId) {
        Map<String, Object> map = new HashMap<>();
        map.put("productIds", productIds);

        log.info(">>>>>>>>>>>>have find productIds {}", productIds);
        List<DisProductListInfoDTO> dtoList = sellerGoodsDistributionMapper.queryByIds(map);
        if (CollectionUtils.isEmpty(dtoList)) {
            log.error(">>>>>>>>>>>>>>>>have find dtoList is null");
            return null;
        }

        List<ProductDistributionDTO> resultList = getDisProductListInfoDTO(dtoList, shopid, userId, spacesId);
        return resultList;
    }


    /**
     * 提取含有分销属性商品的信息
     *
     * @param productList
     * @param shopid
     * @param userId
     * @param spacesId
     * @return
     */
    public List<ProductDistributionDTO> getDisProductListInfoDTO(List<DisProductListInfoDTO> productList, String shopid, Long userId, String spacesId) {
        // 获取销售商品
        List<ProductDistributionDTO> productDistributionDTOList = new ArrayList<>();
        for (DisProductListInfoDTO info : productList) {
            ProductDistributionDTO productDistributionDTO = new ProductDistributionDTO();
            // TODO 这里不能将productId直接拿去查询。因为可能会对分销数量造成影响
            productDistributionDTO.setProductId(info.getProductId() + 1);
            productDistributionDTO.setParentId(info.getParentId());
            productDistributionDTO.setShopId(info.getShopId());
            productDistributionDTO.setPrice(info.getMinSellPrice());
            productDistributionDTO.setPlatSupportRatio(info.getCommissionPre());
            productDistributionDTO.setStatus(info.getStatus());
            productDistributionDTO.setStatus(info.getStackFlag());
            // 查询零级类目的店铺分销比例
            CategoryDistributionDTO categoryDTO = categoryGroupMappingService.queryZeroLevelByFirst(info.getProductSpecs().toString());
            productDistributionDTO.setSelfSupportRatio(new BigDecimal("100").subtract(categoryDTO.getDistributionRatio()));
            productDistributionDTO.setProductSpecs(info.getProductSpecs());
            productDistributionDTO.setMainPics(info.getMainPic());
            productDistributionDTO.setProductTitle(info.getTitle());
            productDistributionDTO.setThirdPrice(info.getMinTaobaoPrice());
            productDistributionDTO.setMerchantId(info.getMerchantId());

            /** 分销佣金重新计算 begin */
            DistributeInfoEntity productDistributeInfo = new DistributeInfoEntity();
            productDistributeInfo.setProductSpec(productDistributionDTO.getProductSpecs());
            productDistributeInfo.setPrice(productDistributionDTO.getPrice());
            productDistributeInfo.setSelfSupportRadio(productDistributionDTO.getSelfSupportRatio());
            productDistributeInfo.setPlatSupportRadio(productDistributionDTO.getPlatSupportRatio());
            // 获取平台分销佣金和比例信息
            ProductDistributCommissionEntity distrIncodeByProduct = distributionAppService.getDistrIncodeByProduct(productDistributeInfo);
            if (distrIncodeByProduct == null) {
                log.error("调用分销服务异常,返回为null");
                // 这是原计算方案. 如果服务调用失败, 用原算法顶上
                productDistributionDTO.setCommissionNum(info.getCommission());
            } else {
                productDistributionDTO.setCommissionNum(distrIncodeByProduct.getPlatDistributCommission());
            }
            /** 分销佣金重新计算 end */

            String key = NEW_KEY_SPECS + ":" + shopid + ":" + spacesId;
            Double salseNum = stringRedisTemplate.opsForZSet().score(key, info.getParentId().toString());
            if (null == salseNum) {
                salseNum = 0D;
            }
            if (salseNum < 0) {
                salseNum = 0D;
            }
            int num = salseNum.intValue();
            productDistributionDTO.setSalesVolume(num + "");

            productDistributionDTOList.add(productDistributionDTO);
        }

        AppletUserProductRebateDTO appletUserProductRebateDTO = new AppletUserProductRebateDTO();
        appletUserProductRebateDTO.setShopId(Long.valueOf(shopid));
        appletUserProductRebateDTO.setUserId(userId);
        appletUserProductRebateDTO.setProductDistributionDTOList(productDistributionDTOList);


        // 获取 对此用户 的商品返利金额
        AppletUserProductRebateDTO productRebate = distributionAppService.queryProductRebateByUserIdShopId(appletUserProductRebateDTO);
        List<ProductDistributionDTO> setValInterate = productRebate.getProductDistributionDTOList();
        if (!CollectionUtils.isEmpty(setValInterate)) {
            for (int i = 0; i < setValInterate.size(); i++) {
                setValInterate.get(i).setStatus(appletUserProductRebateDTO.getProductDistributionDTOList().get(i).getStatus());
                setValInterate.get(i).setCommissionNum(appletUserProductRebateDTO.getProductDistributionDTOList().get(i).getCommissionNum());
            }
        }
        if (null == productRebate) {
            log.info(">>>>>>>>>>getRebate productRebate is null ");
            return null;
        }
        List<ProductDistributionDTO> dto = productRebate.getProductDistributionDTOList();

        if (CollectionUtils.isEmpty(dto)) {
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>> getRebate dto is null");
            return null;
        }
        for (ProductDistributionDTO productDistributionDTO : dto) {
            productDistributionDTO.setProductId(productDistributionDTO.getProductId() - 1);
        }
        return dto;
    }
}
