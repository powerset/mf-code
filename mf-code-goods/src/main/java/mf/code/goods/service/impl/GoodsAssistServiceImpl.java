package mf.code.goods.service.impl;/**
 * create by qc on 2019/4/9 0009
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.seller.dto.ProductSkuSpec;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.dao.ProductSpecsMapper;
import mf.code.goods.repo.dao.ProductSpecsNameMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.po.ProductSpecs;
import mf.code.goods.repo.po.ProductSpecsName;
import mf.code.goods.service.GoodsSkuAboutService;
import mf.code.goods.service.ProductSkuSpecService;
import mf.code.goods.service.SellerGoodsAssistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gbf
 * 2019/4/9 0009、14:54
 */
@Service
@Slf4j
public class GoodsAssistServiceImpl implements SellerGoodsAssistService {

    /**
     * 修改库存
     */
    private static final int IS_UPDATE_STOCK = 1;
    /**
     * 修改价格
     */
    private static final int IS_UPDATE_PRICE = 2;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private ProductSpecsMapper productSpecsMapper;
    @Autowired
    private GoodsSkuAboutService goodsSkuAboutService;
    @Autowired
    private ProductSpecsNameMapper productSpecsNameMapper;
    @Autowired
    private ProductSkuSpecService productSkuSpecService;

    /**
     * 列表头部的统计数据 仓库中数量/在售中数量
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse count(Long merchantId, Long shopId) {
        int countOnsale = productMapper.countOnsale(shopId);
        int countInStore = productMapper.countInStore(shopId);
        Map<String, Integer> resultMap = new HashMap<>();
        resultMap.put("countOnsale", countOnsale);
        resultMap.put("countInStore", countInStore);

        return new SimpleResponse(0, "success", resultMap);
    }

    /**
     * 该库存前置查询
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @param productId  商品id
     * @return .
     */
    @Override
    public SimpleResponse updateStockBefore(Long merchantId, long shopId, long productId) {
        SimpleResponse simpleResponse = this.productError(merchantId, shopId, productId);
        if (simpleResponse != null) {
            return simpleResponse;
        }
        List<Map<String, Object>> productSkuList = this.getProductSkuList(productId, IS_UPDATE_STOCK);

        return new SimpleResponse(0, "success", productSkuList);
    }


    private List<Map<String, Object>> getProductSkuList(Long productId, int updateStockOrPrice) {
        List<ProductSku> skuList = productSkuMapper.findByProductId(productId);
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (ProductSku sku : skuList) {
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("productId", productId);
            resultMap.put("skuId", sku.getId());
            if (updateStockOrPrice == IS_UPDATE_PRICE) {
                resultMap.put("price", sku.getSkuSellingPrice());
            }
            if (updateStockOrPrice == IS_UPDATE_STOCK) {
                resultMap.put("stock", sku.getStock());
            }
            String specSortStr = sku.getSpecSort();
            List<ProductSkuSpec> productSkuSpecList = productSkuSpecService.parseProductSkuSpecs(specSortStr, goodsSkuAboutService.getSkuSalePropMap());
            resultMap.put("specs", productSkuSpecList);
            resultList.add(resultMap);
        }
        return resultList;
    }


    /**
     * 校验商品是否合法
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @param productId  商品id
     * @return .
     */
    private SimpleResponse productError(Long merchantId, Long shopId, Long productId) {

        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null) {
            return new SimpleResponse(12, "该商品不存在");
        }
        if (!merchantId.equals(product.getMerchantId())) {
            return new SimpleResponse(10, "该商品不属于此商户");
        }
        if (!shopId.equals(product.getShopId())) {
            return new SimpleResponse(11, "该商品不属于此店铺");
        }
        return null;
    }

    /**
     * 该价前置查询
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @param productId  商品id
     * @return .
     */
    @Override
    public SimpleResponse updatePriceBefore(Long merchantId, Long shopId, Long productId) {
        SimpleResponse simpleResponse = this.productError(merchantId, shopId, productId);
        if (simpleResponse != null) {
            return simpleResponse;
        }
        List<Map<String, Object>> productSkuList = this.getProductSkuList(productId, IS_UPDATE_PRICE);
        return new SimpleResponse(0, "success", productSkuList);
    }

    /**
     * 获取商品的规格类目
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse productSpecs(Long merchantId, Long shopId) {
        Map<String, Long> conditionMap = new HashMap<>();
        conditionMap.put("merchantId", null);
        List<Map> specs = productSpecsNameMapper.findAll();
        return new SimpleResponse(0, "success", specs);
    }

    /**
     * 上报规格类目
     */
    @Override
    public SimpleResponse postSpecValue(Long merchantId, Long shopId, Long parentId, String specName) {
        ProductSpecsName productSpecsName = productSpecsNameMapper.selectByPrimaryKey(parentId);
        ProductSpecs productSpecsNew = new ProductSpecs();
        productSpecsNew.setParentName(productSpecsName.getName());
        productSpecsNew.setMerchantId(merchantId);
        productSpecsNew.setSpecsValue(specName);
        productSpecsNew.setSpecsNameId(parentId);
        int result = productSpecsMapper.insertSelective(productSpecsNew);
        if (result == 1) {
            Map<String, Long> m = new HashMap<>();
            m.put("id", productSpecsNew.getId());
            return new SimpleResponse(0, "success", m);
        }
        return new SimpleResponse(2, "执行插入数据,数据库返回结果是0");
    }

    /**
     * 审核不通过原因
     *
     * @param productId
     * @return
     */
    @Override
    public SimpleResponse getCheckReason(Long productId) {
        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null) {
            return new SimpleResponse(1, "商品不存在");
        }
        if (product.getStatus() == ProductConstant.Status.ON_SALE) {
            return new SimpleResponse(2, "该商品已上架");
        }
        if (product.getCheckStatus() == ProductConstant.CheckStatus.CHECKING) {
            return new SimpleResponse(3, "商品正在审核中,尚未有新的审核意见");
        }
        String checkReason = product.getCheckReason();
        if (checkReason == null) {
            return new SimpleResponse(4, "该商品未通过审核,但没有给出审核意见");
        }
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("checkReason", checkReason);
        resultMap.put("checkReasonPics",product.getCheckReasonPic());
        return new SimpleResponse(0, "success", resultMap);
    }
}
