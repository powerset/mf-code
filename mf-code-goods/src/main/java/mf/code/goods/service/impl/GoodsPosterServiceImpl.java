package mf.code.goods.service.impl;

import mf.code.common.WeixinMpConstants;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.common.caller.wxmp.WeixinMpService;
import mf.code.goods.common.caller.wxmp.WxpayProperty;
import mf.code.goods.service.GoodsPosterService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.goods.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月16日 13:58
 */
@Service
public class GoodsPosterServiceImpl implements GoodsPosterService {
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private WeixinMpService weixinMpService;

    @Override
    public SimpleResponse sharePoster(Long merchantID, Long shopID, Long goodsId, Long userID, Long fromID) {
        SimpleResponse d = new SimpleResponse();
        if (merchantID == null || merchantID == 0 || shopID == null || shopID == 0 || userID == null || userID == 0 || fromID == null || fromID == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }

        //分享商品
        int scene = WeixinMpConstants.SCENE_GOODSDETAIL_POSTER;
        if (goodsId == 0) {
            //分享邀请
            scene = WeixinMpConstants.SCENE_USER_FENXIAO_POSTER;
        }

        Object[] objects = new Object[]{merchantID, shopID, scene, goodsId, userID, fromID};
        String wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), this.getStrArray(objects, ","), WeixinMpConstants.WXCODESHOPPOSTERPATH + "/" + shopID + "/", 430, false);
        if (StringUtils.isBlank(wxaCodeUnlimit)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "生成小程序码失败");
        }
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put("appletCode", wxaCodeUnlimit);

        d.setData(resp);
        return d;
    }

    private String getStrArray(Object[] array, String separator) {
        if (array == null || array.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(array[0]);
        for (int i = 1; i < array.length; i++) {
            sb.append(separator).append(array[i]);
        }
        return sb.toString();
    }
}
