package mf.code.goods.service;/**
 * create by qc on 2019/9/21 0021
 */

import mf.code.goods.repo.dto.GoodsListPageDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author gbf
 * 2019/9/21 0021、11:48
 */
public interface JKMFGoodsClientService {
    /**
     * 商品列表
     * <p>
     * 维度类型 1:供应商 2:商品
     * 标签类型 1:爆款 2:特价 3:尾货 4:全部
     * 查询关键字
     * 商品类目
     * 排序 1:综合排序(销量) 2:上新时间
     * 当前页
     * 每页数
     *
     * @return json
     */
    String goodsList(@RequestBody GoodsListPageDTO goodsListPageDTO);

    /**
     * 获取商品详情
     *
     * @param productId 商品id
     * @return
     */
    String detail(@RequestParam("productId") Long productId);


    /**
     * 获取商品类目
     */
    String getSpecs();

}
