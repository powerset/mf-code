package mf.code.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.seller.dto.PlatformDistRateAndPriceDTO;
import mf.code.goods.api.seller.dto.SellerProductListReqDTO;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.repo.po.Product;
import mf.code.shop.constants.AppTypeEnum;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author gbf
 * 2019/4/3 0003、09:09
 */
public interface GoodsService extends IService<Product> {
    /**
     * 创建
     *
     * @param dto
     * @return
     */
    SimpleResponse create(ProductDTO dto);

    /**
     * 根据查询条件获取列表
     */
    @Deprecated
    SimpleResponse listByCondition(SellerProductListReqDTO reqDTO);


    /**
     * 上架
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse upShelf(Long productId, Long merchantId, Long shopId);

    /**
     * 获取修改前置信息
     *
     * @param merchantId
     * @param shopId
     * @param productId
     * @return
     */
    SimpleResponse getBeforedistInfo(String merchantId, String shopId, String productId);

    /**
     * 获取供应商商品经过平台分佣之后的售价和比例
     *
     * @param shopId
     * @param productSpecs
     * @param productPrice
     * @param platSupportRate
     * @return
     */
    PlatformDistRateAndPriceDTO getPlatformDistRate(String shopId, Long productSpecs, BigDecimal productPrice, BigDecimal platSupportRate);

    /**
     * 设置商铺分销推广佣金比例
     *
     * @param merchantId
     * @param shopId
     * @param productId
     * @param selfDistributionRadioDecimal
     * @param platDistributionRadioDecimal
     * @return
     */
    SimpleResponse setDistributionRadio(Long merchantId, Long shopId, Long productId, BigDecimal selfDistributionRadioDecimal, BigDecimal platDistributionRadioDecimal);

    /**
     * 下架
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse downshelf(Long productId, Long merchantId, Long shopId);

    /**
     * 终止分销
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse diststop(Long productId, Long merchantId, Long shopId);

    /**
     * 添加店铺
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse addstore(String productId, String merchantId, String shopId);

    /**
     * 分销市场直接上架
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @param selfDistributionRadioDecimal
     * @return
     */
    SimpleResponse distUpshelf(String productId, String merchantId, String shopId, BigDecimal selfDistributionRadioDecimal);

    /**
     * 类目下拉表
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse getSpecs(Long merchantId, Long shopId);

    /**
     * 上传主图
     *
     * @param image
     * @return
     */
    SimpleResponse uploadPic(MultipartFile image, Long shopId);

    /**
     * 修改
     *
     * @param dto
     * @return
     */
    SimpleResponse update(ProductDTO dto);

    /**
     * 标记删除
     *
     * @param productId
     * @param appType
     * @return
     */
    SimpleResponse del(long productId, Integer appType);

    /**
     * 抖带带-删除商品
     * @param productId
     * @param appTypeEnum
     * @return
     */
    SimpleResponse del(long productId, AppTypeEnum appTypeEnum);

    /**
     * 审核
     *
     * @param productId
     * @param checkStatus
     * @param reason
     * @param checkReasonPicStr
     * @return
     */
    SimpleResponse audit(long productId, Integer checkStatus, String reason, String checkReasonPicStr);

    /**
     * 详情
     *
     * @param productId
     * @return
     */
    SimpleResponse detail(long productId);

    /**
     * 商品审核列表
     *
     * @param conditionMap
     * @param pageCurrent
     * @param pageSize
     * @return
     */
    @Deprecated
    SimpleResponse checkListByCondition(Map<String, Object> conditionMap, int pageCurrent, int pageSize);

    /**
     * 平台审核-商品详情
     *
     * @param productId 产品id
     * @return SimpleResponse
     */
    SimpleResponse platCheckProductDetail(Long productId);

    /**
     * 审核查看不通过理由
     *
     * @param productId
     * @return
     */
    SimpleResponse getCheckReason(Long productId);

    /**
     * 分销商设置分销比例
     *
     * @param merchantId
     * @param shopId
     * @param productId
     * @param selfSupportRatio
     * @return
     */
    SimpleResponse setDistributionByDistributor(Long merchantId, Long shopId, Long productId, BigDecimal selfSupportRatio, Integer statInMarketWannaOnSale);

    /***
     * 获取该店铺的商品分组
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse selectProductGroup(Long merchantId, Long shopId);

    /**
     * 汇总 所有商品-奖品信息,列表优化
     * 条件：审核通过上架，未进入平台分销，设置淘宝numiid，并且不是新人专区商品
     *
     * @param shopId
     * @return
     */
    int countAwardListForListOptimize(Long shopId);

    /**
     * 根据店铺id 按id倒叙 分页查询 所有商品-奖品信息 按分组查询
     * 条件：审核通过上架，未进入平台分销，设置淘宝numiid，并且不是新人专区商品
     */
    List<Product> pageAwardListForListOptimize(Long shopId, int page, int size);

    /**
     * 根据查询条件获取列表,20190604列表优化
     */
    SimpleResponse listByConditionForListOptimize(SellerProductListReqDTO reqDTO);

    /**
     * 根据查询条件获取商户后台审核列表,20190604列表优化
     */
    SimpleResponse checkListByConditionForListOptimize(Map<String, Object> conditionMap, int pageCurrent, int pageSize);

    /**
     * 根据id查询
     * @param productId
     * @return
     */
    Product findById(Long productId);
    /**
     * 根据sid查询父节点
     * @param productSpecs
     * @return
     */
    String getParentSpecBySpec(Long productSpecs);

    /**
     * 删除商品后,增加在选货列表中增加此商品
     * @param productId
     */
    void addZsetCacheByProductId(Long productId);
}
