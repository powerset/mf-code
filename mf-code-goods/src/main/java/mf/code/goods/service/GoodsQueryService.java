package mf.code.goods.service;

import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.goods.dto.ProductSingleResultDTO;

import java.util.List;

/**
 * mf.code.goods.service
 * Description:
 *
 * @author gel
 * @date 2019-05-16 16:47
 */
public interface GoodsQueryService {


    /**
     * 查询商品基本信息
     *
     * @param shopId
     * @param productId
     * @return
     */
    ProductSingleResultDTO selectSingleProduct(Long shopId, Long productId);

    /**
     * 根据商品skuId查询秒杀商品
     * 如果存在当前时间段的秒杀sku则直接返回，否则需要创建
     *
     *
     */
    List<GoodsSkuDTO> queryOrCreateSkuForFlashSale();
}
