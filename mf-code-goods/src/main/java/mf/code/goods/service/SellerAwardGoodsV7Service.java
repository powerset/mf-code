package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.goods.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年05月27日 16:02
 */
public interface SellerAwardGoodsV7Service {

    /**
     * 获取奖品集合,优化查询
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse listForListOptimize(Long merchantId, Long shopId, int pageNum, int pageSize);
}
