package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.goods.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 13:02
 */
public interface PlatformProductPromotionService {

	/**
	 * 本周特推 -- 加入特推管理
	 *
	 * @param productId
	 * @return
	 */
	SimpleResponse addHighlyRecommend(Long productId);
}
