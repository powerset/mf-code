package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.ProductSku;

import java.util.Map;

/**
 * create by qc on 2019/4/18 0018
 */
public interface GoodsSkuLogService {
    /**
     * c端写日志
     * @param productSku
     * @param stockNumber
     * @param isConsume
     * @param type
     * @param orderId
     */
    void insertLogForClient(ProductSku productSku, int stockNumber, boolean isConsume, int type, Long orderId);

    /**
     * 商户写日志
     * @param productSku
     * @param stockNumber
     */
    void insertLogForMerchant(ProductSku productSku, int stockNumber);

    /**
     * 查询前n条日志
     * @param conditionMap
     * @return
     */
    SimpleResponse topLogs(Map<String, Object> conditionMap);
}
