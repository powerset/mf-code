package mf.code.goods.service;

import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.shop.constants.AppTypeEnum;

import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月08日 17:55
 */
public interface GoodsSkuAboutService {

    /***
     * 获取sku类目信息
     * @param catIds
     * @param specsValues
     * @return
     */
    List<Map> getSkuItemInfo(List<Long> catIds, List<String> specsValues);

    /***
     * 获取sku信息列表
     * @param productSkus
     * @param appTypeEnum
     * @return
     */
    List<Map> getSkuInfo(Product product, List<ProductSku> productSkus, ProductDistributionDTO distributionDTO, AppTypeEnum appTypeEnum);

    /***
     * 类目下属性值
     * @param specsValues
     * @return
     */
    List<Map> getSkuSpecs(List<Long> specsValues);

    /**
     * 获取销售属性的Map
     * @return
     */
    Map<Long, String> getSkuSalePropMap();
}
