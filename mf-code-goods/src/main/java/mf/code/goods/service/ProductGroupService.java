package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductGroupListResultDTO;
import mf.code.goods.dto.ProductGroupReqDTO;
import mf.code.goods.dto.ProductSingleResultDTO;

import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.service
 * Description:
 *
 * @author gel
 * @date 2019-05-16 15:51
 */
public interface ProductGroupService {

    /**
     * 查询分组列表
     *
     * @param shopId
     * @return
     */
    List<ProductGroupListResultDTO> list(Long shopId);

    /**
     * 创建分组
     *
     * @param dto
     * @return
     */
    Integer createGroup(ProductGroupReqDTO dto);

    /**
     * 添加商品到分组
     *
     * @param dto
     * @return
     */
    Integer addGroupGoods(ProductGroupReqDTO dto);

    /**
     * 查询分组内的商品
     *
     * @param shopId
     * @param groupId
     * @param pageNum
     * @param pageSize
     * @return
     */
    Map<String, Object> queryGroupGoods(Long shopId, Long groupId, Long pageNum, Long pageSize);

    /**
     * 删除分组内的商品
     *
     * @param dto
     * @return
     */
    Integer delGroupGoods(ProductGroupReqDTO dto);

    /**
     * 更新分组信息
     *
     * @param dto
     * @return
     */
    Integer updateSelective(ProductGroupReqDTO dto);

    /**
     * 删除分组
     *
     * @param dto
     * @return
     */
    Integer delGroup(ProductGroupReqDTO dto);

    /***
     * 通过分组id查询商品信息列表
     * @param merchantId
     * @param shopId
     * @param productGroupId 商品分组编号
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse getProductsByGroupId(Long merchantId, Long shopId, Long userId, Long productGroupId, Long offset, Long size);

}
