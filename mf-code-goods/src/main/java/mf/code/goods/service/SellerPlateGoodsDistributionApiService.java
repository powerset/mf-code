package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.seller.dto.PlateDisProductListResultDTO;
import mf.code.goods.api.seller.dto.SpecesAndBannerDTO;

import java.util.List;

/**
 * 分销--平台
 */
public interface SellerPlateGoodsDistributionApiService {
    /**
     * 获取分销列表--平台 排序
     *
     * @param limit
     * @param offset
     * @param shopId
     * @param speceType
     * @param sort
     * @return
     */
    PlateDisProductListResultDTO getPalteDistributionListResult(Integer limit, Integer offset, String shopId, String speceType, Long userId, Integer sort);

    /**
     * 获取分销列表--平台
     *
     * @param limit
     * @param offset
     * @param shopId
     * @param speceType
     * @return
     */
    PlateDisProductListResultDTO getPalteDistributionListResult(Integer limit, Integer offset, String shopId, String speceType, Long userId);

    /**
     * 批量修改类目中的信息
     *
     * @return
     */
    SimpleResponse batchUpdateSpecsInfo();

    /**
     * 获取banner和类目信息
     *
     * @param shopId
     * @param userId
     * @return
     */
    List<SpecesAndBannerDTO> getBannerAndSpece(Long shopId, Long userId);


}
