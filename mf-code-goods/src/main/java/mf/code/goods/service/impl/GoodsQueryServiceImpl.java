package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSONArray;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.Assert;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.goods.dto.ProductSingleResultDTO;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.repostory.SellerProductRepository;
import mf.code.goods.repo.repostory.SellerSkuRepository;
import mf.code.goods.service.GoodsQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-16 16:47
 */
@Service
public class GoodsQueryServiceImpl implements GoodsQueryService {

    @Autowired
    private SellerProductRepository sellerProductRepostory;
    @Autowired
    private SellerSkuRepository sellerSkuRepostory;

    /**
     * 查询商品基本信息
     *
     * @param shopId
     * @param productId
     * @return
     */
    @Override
    public ProductSingleResultDTO selectSingleProduct(Long shopId, Long productId) {
        Product product = sellerProductRepostory.selectById(productId);
        Assert.notNull(product, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "不存在该商品");
        Assert.isTrue(product.getShopId().equals(shopId), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "不存在该商品");
        List<ProductSku> productSkus = sellerSkuRepostory.selectByProductId(product.getParentId());
        Assert.isTrue(!CollectionUtils.isEmpty(productSkus), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "不存在该商品");
        BigDecimal min = new BigDecimal("1000000");
        BigDecimal max = BigDecimal.ZERO;
        for (ProductSku productSku : productSkus) {
            if (productSku.getSkuSellingPrice().compareTo(min) < 0) {
                min = productSku.getSkuSellingPrice();
            }
            if (productSku.getSkuSellingPrice().compareTo(max) > 0) {
                max = productSku.getSkuSellingPrice();
            }
        }
        ProductSingleResultDTO resultDTO = new ProductSingleResultDTO();
        resultDTO.setProductId(productId.toString());
        resultDTO.setProductMax(max.toString());
        resultDTO.setProductMin(min.toString());
        resultDTO.setProductTitle(product.getProductTitle());
        List<String> mainPicList = JSONArray.parseArray(product.getMainPics(), String.class);
        if (!CollectionUtils.isEmpty(mainPicList)) {
            resultDTO.setProductPic(mainPicList.get(0));
        }
        return resultDTO;
    }

    /**
     * 根据商品skuId查询秒杀商品
     * 如果存在当前时间段的秒杀sku则直接返回，否则需要创建
     * 1、根据入参的时间和sku列表（包含skuId，售价，初始销量，库存，活动时间）
     *    查询该时间段的not_show为2的old_sku_id为入参skuId的productSku
     * 2、如果有则直接拼接参数返回
     * 3、如果没有则需要分布式锁，创建该秒杀sku，返回列表
     */
    @Override
    public List<GoodsSkuDTO> queryOrCreateSkuForFlashSale() {
        List<GoodsSkuDTO> goodsSkuDTOList = new ArrayList<>();
        GoodsSkuDTO goodsSkuDTO = new GoodsSkuDTO();

        goodsSkuDTOList.add(goodsSkuDTO);
        return goodsSkuDTOList;
    }
}
