package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.seller.dto.DisProductListResultDTO;

import java.util.Map;

/**
 * 分销
 */
public interface SellerGoodsDistributionV5Service {
    /**
     * 获取分销列表
     * @param params
     * @param shopid
     * @return
     */
    DisProductListResultDTO getDisProductListResult(Map<String, Object> params,String shopid);


    /**
     * 获取分销列表--通过开关控制
     * @param params
     * @param shopid
     * @return
     */
    DisProductListResultDTO getDisProductListResultBySwitch(Map<String, Object> params,String shopid,String salesVolume,int num,int size);

    /**
     * 获取商品详情
     * @param productId
     * @param shopId
     * @return
     */
    SimpleResponse detail(long productId,Long shopId);

    /**
     * 分销商品上架
     * @param productId
     * @param merchantId
     * @param shopId
     * @param commissionPre
     * @param appType
     * @return
     */
    SimpleResponse upShelf(Long productId, Long merchantId, Long shopId, String commissionPre, Integer appType);

    /**
     * 分销商品加入仓库
     * @param productId
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse storage(Long productId,Long merchantId, Long shopId);

    /**
     * 根据productID 批量修改product信息
     * @param productId
     * @return
     */
    SimpleResponse batchUpdateProductInfo(Long productId);

    /**
     * 根据productID 批量修改product的价格相关信息
     * @param productId
     * @return
     */
    SimpleResponse batchUpdateProductPrice(Long productId);

    /**
     * 根据productID 批量添加销量缓存
     * @return
     */
    SimpleResponse batchAddCacheByProductId();


    /**
     * 批量修改product中的销量信息
     * @param productId
     * @return
     */




    SimpleResponse batchUpdateOrderSalesNumByProductId(Long productId);
}
