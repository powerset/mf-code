package mf.code.goods.service.impl;/**
 * create by qc on 2019/8/19 0019
 */

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.api.seller.dto.DisProductListInfoDTO;
import mf.code.goods.api.seller.dto.PlateDisProductListResultDTO;
import mf.code.goods.api.seller.dto.SpecesAndBannerDTO;
import mf.code.goods.common.apollo.ApolloPropertyService;
import mf.code.goods.common.apollo.DouyinProperty;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.common.constant.ProductStatusEnum;
import mf.code.goods.common.constant.RedisKey;
import mf.code.goods.common.constant.SpeceTypeEnum;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.goods.repo.dao.SellerPlateGoodsDistributionMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSVRanking;
import mf.code.goods.repo.repostory.CategoryTaobaoRepository;
import mf.code.goods.repo.repostory.SellerProductRepository;
import mf.code.goods.repo.service.ProductService;
import mf.code.goods.service.SellerGoodsV8Service;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static mf.code.goods.service.impl.SellerPlateGoodsDistributionApiServiceImpl.NEW_KEY_SPECS;

/**
 * @author gbf
 * 2019/8/19 0019、10:28
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class SellerGoodsV8ServiceImpl implements SellerGoodsV8Service {
    private final ProductService productService;
    private final SellerProductRepository sellerProductRepository;
    private final StringRedisTemplate stringRedisTemplate;
    private final CategoryTaobaoRepository categoryTaobaoRepository;
    private final SellerPlateGoodsDistributionApiServiceImpl sellerPlateGoodsDistributionApiService;
    private final ApolloPropertyService apolloPropertyService;
    private final DistributionAppService distributionAppService;
    private final SellerPlateGoodsDistributionMapper sellerPlateGoodsDistributionMapper;

    /**
     * @param shopId    店铺
     * @param userId    用户
     * @param speceType 商品类目 类目，默认是1000000000，查询全部，1000000001：百货食品，1000000002：服装鞋包，1000000003：美妆饰品，1000000004：手机数码，
     *                  1000000005：母婴用品，1000000006：生活服务，1000000007：家用电器，1000000008：家居建材，1000000009：运动户外，1000000010：文化玩乐，
     *                  1000000011：其他商品，1000000012：汽配摩托，1000000013：游戏话费
     * @param sort      排序 1今日上新 2热销 3高佣
     * @param offset    偏移量
     * @param limit     每页数量
     * @return
     * @see mf.code.goods.api.seller.dto.PlateDisProductListResultDTO
     */
    @Override
    public PlateDisProductListResultDTO pickProductList(Integer offset, Integer limit, Long shopId, String speceType, Long userId, Integer sort) {
        // 1.查本店分销的商品.条件:shop_id=? from_flag=2分销 del=0
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getFromFlag, ProductFromFlagEnum.PLAT.getCode())
                .eq(Product::getDel, DelEnum.NO.getCode());
        List<Product> shopDistributeList = productService.list(queryWrapper);

        // 2.查有销量的商品 zset
        String redisKeyRoot = RedisKey.PLAT_GOODS_RANK + ":";
        Date beginDate = DateUtil.stringtoDate(RedisKey.REDIS_ZSET_DAY_BEGIN, DateUtil.FORMAT_FOUR);
        String redisKeyBegin = redisKeyRoot + RedisKey.REDIS_ZSET_DAY_BEGIN;
        List<String> redisKeyList = new ArrayList<>();
        for (int i = 1; i < DateUtil.dayDiff(beginDate, new Date()) + 1; i++) {
            Date tempDate = DateUtil.addDay(beginDate, i);
            String redisKeyDay = DateUtil.dateToString(tempDate, DateUtil.FORMAT_FOUR);
            redisKeyList.add(redisKeyRoot + redisKeyDay);
        }
        // zset并集
        stringRedisTemplate.opsForZSet().unionAndStore(redisKeyBegin, redisKeyList, RedisKey.PLAT_GOODS_RANK);

        // 查询redis合并后的zset集合
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeByScoreWithScores(RedisKey.PLAT_GOODS_RANK, 0, -1);
        // 销售榜数组
        List<ProductSVRanking> productSVRankingList = new ArrayList<>();
        List<String> productIdList = new ArrayList<>();
        for (ZSetOperations.TypedTuple zSetOperations : typedTuples) {
            String goodsId = zSetOperations.getValue().toString();
            Double score = zSetOperations.getScore();
            productSVRankingList.add(new ProductSVRanking(goodsId, score));
            productIdList.add(goodsId);
        }

        if (SpeceTypeEnum.DEFALT.getCode().toString().equals(speceType)) {
            // 类目为默认 , 按照sort排序

        } else {
            // 按照类目筛选 , 排序规则:销量倒序
            Map param = new HashMap();
            param.put("specParentId", speceType);
            param.put("productIdList", productIdList);
            List<Product> products = sellerProductRepository.selectProductListByParentSpece(param);

        }

        return null;
    }

    /**
     * 通过父类目查询商品列表
     *
     * @param specesId
     * @return
     */
    @Override
    public List<Product> findProductBySpecId(String specesId) {
        Map map = new HashMap();
        map.put("specParentId", specesId);
        return sellerProductRepository.selectProductListByParentSpece(map);
    }

    /**
     * 查本店全部分销商品列表
     *
     * @param shopId
     * @return
     */
    @Override
    public List<Product> findDistrProductList(Long shopId) {
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Product::getShopId, shopId)
                .eq(Product::getFromFlag, ProductFromFlagEnum.PLAT.getCode())
                .eq(Product::getStatus, ProductStatusEnum.ONLINE.getCode())
                .isNotNull(Product::getPlatSupportRatio)
                .eq(Product::getDel, DelEnum.NO);
        return productService.list(queryWrapper);
    }

    /**
     * 处理抖带带的类目
     *
     * @param dtoList
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse handlDDDSpec(List<SpecesAndBannerDTO> dtoList, Long shopId, Long userId) {
        // 抖带带 类目
        String redisKeySpecInShop = RedisKeyConstant.DDD_BANNER + ":" + shopId;
        // 有缓存, 返回
        String json = stringRedisTemplate.opsForValue().get(redisKeySpecInShop);
        if (!StringUtils.isEmpty(json)) {
            List<SpecesAndBannerDTO> specesAndBannerDTOS = JSON.parseArray(json, SpecesAndBannerDTO.class);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, specesAndBannerDTOS);
        }

        List<SpecesAndBannerDTO> removeSpecList = new ArrayList<>();
        for (SpecesAndBannerDTO dto : dtoList) {
            String specesId = dto.getSpecesId();
            if (SpeceTypeEnum.DEFALT.getCode().toString().equals(specesId)) {
                continue;
            }
            // 查该类目的商品
            List<Product> products = this.findProductBySpecId(specesId);
            if (products == null || products.size() == 0) {
                removeSpecList.add(dto);
            }
        }
        // 第一次抛弃 无数据的类目. 无他 只为减少后面的迭代次数
        for (SpecesAndBannerDTO dto : removeSpecList) {
            dtoList.remove(dto);
        }

        removeSpecList = new ArrayList<>();

        // 查本店铺的分销列表
        List<Product> distrProductListInMyShop = this.findDistrProductList(shopId);
        // 计算 本店铺 在选货列表中有商品的类目
        for (SpecesAndBannerDTO specDto : dtoList) {
            if (SpeceTypeEnum.DEFALT.getCode().toString().equals(specDto.getSpecesId())) {
                continue;
            }
            // 此类目下的 全部 子类目
            List<CategoryTaobao> categoryTaobaos = categoryTaobaoRepository.selectNextLevelByParentSid(specDto.getSpecesId(), 1);
            List<String> specArr = new ArrayList<>();
            for (CategoryTaobao categoryTaobao : categoryTaobaos) {
                specArr.add(categoryTaobao.getSid());
            }
            specArr.add(specDto.getSpecesId());
            // 查此类目 及其子类目   在商城展示 的全部商品
            QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(Product::getStatus, ProductStatusEnum.ONLINE.getCode())
                    .eq(Product::getFromFlag, ProductFromFlagEnum.SELF.getCode())
                    .eq(Product::getDel, DelEnum.NO.getCode())
                    .isNotNull(Product::getPlatSupportRatio)
                    .in(Product::getProductSpecs, specArr);
            List<Product> mallProductList = productService.list(queryWrapper);
            if (mallProductList.size() == 0) {
                removeSpecList.add(specDto);
                continue;
            }
            // 剔除商品数组
            List<Product> rmDistrProductList = new ArrayList<>();
            for (Product productDistr : distrProductListInMyShop) {
                for (Product productMall : mallProductList) {
                    if (productDistr.getParentId().equals(productMall.getId())) {
                        rmDistrProductList.add(productDistr);
                    }
                }
            }
            // 从商城商品列表中剔除 删除数组的数据
            for (Product product : rmDistrProductList) {
                mallProductList.remove(product);
            }
            // 商城列表数组为0 删除类目
            if (mallProductList.size() == 0) {
                removeSpecList.add(specDto);
            }
        }
        // 删除本店铺的类目
        for (SpecesAndBannerDTO dto : removeSpecList) {
            dtoList.remove(dto);
        }

        stringRedisTemplate.opsForValue().set(redisKeySpecInShop, JSON.toJSONString(dtoList));
        stringRedisTemplate.expire(redisKeySpecInShop, 180, TimeUnit.SECONDS);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, dtoList);
    }

    @Override
    public SimpleResponse getBannerList(Long shopId, Long userId, AppTypeEnum appTypeEnum) {
        DouyinProperty apolloDouyinProperty = apolloPropertyService.getApolloDouyinProperty();

        List<ProductDistributionDTO> productDistributionDTOS = new ArrayList<>();
        if (apolloDouyinProperty == null || apolloDouyinProperty.getDouyin() == null || apolloDouyinProperty.getDouyin().getGids() == null) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, productDistributionDTOS);
        }
        List<DouyinProperty.Gids> gids = apolloDouyinProperty.getDouyin().getGids();

        List<Long> pids = new ArrayList<>();
        for (DouyinProperty.Gids gid : gids) {
            pids.add(NumberUtils.toLong(gid.getGid()));
        }
        log.info("<<<<<<<<productIds apollo:{}", pids);
        if (CollectionUtils.isEmpty(pids)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, productDistributionDTOS);
        }

        //去除已上架的
        Map<Long, DisProductListInfoDTO> exitMap = new HashMap<>();
        if (shopId != null && shopId > 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("shopId", shopId);
            //根据类目获取分销产品列表（已经去除在本店铺分销过的产品）
            List<DisProductListInfoDTO> listByPidAndSid = sellerPlateGoodsDistributionMapper.queryByProductidAndSid(map);
            if (!CollectionUtils.isEmpty(listByPidAndSid)) {
                for (DisProductListInfoDTO disProductListInfoDTO : listByPidAndSid) {
                    exitMap.put(disProductListInfoDTO.getParentId(), disProductListInfoDTO);
                }
                log.info("<<<<<<<<exitMap:{}", exitMap);
            }
        }

        productDistributionDTOS = sellerPlateGoodsDistributionApiService.queryByIds(pids, String.valueOf(0), userId, "1000000000");
        if (CollectionUtils.isEmpty(productDistributionDTOS)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, productDistributionDTOS);
        }
        for (ProductDistributionDTO productDistributionDTO : productDistributionDTOS) {
            DisProductListInfoDTO disProductListInfoDTO = exitMap.get(productDistributionDTO.getProductId());
            if (disProductListInfoDTO != null) {
                productDistributionDTO.setOnShop(true);
            }
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, productDistributionDTOS);
    }
}
