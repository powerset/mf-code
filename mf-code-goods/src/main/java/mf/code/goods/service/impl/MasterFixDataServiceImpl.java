package mf.code.goods.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.goods.api.seller.dto.DisProductListInfoDTO;
import mf.code.goods.common.constant.ProductSortEnum;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.SellerGoodsDistributionMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.service.MasterFixDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-06-15 16:27
 */
@Slf4j
@Service
public class MasterFixDataServiceImpl implements MasterFixDataService {
    @Autowired
    private SellerGoodsDistributionMapper sellerGoodsDistributionMapper;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ProductMapper productMapper;
    private static Lock lock = new ReentrantLock();

    /**
     * 商城商品列表默认销量排序
     * @return
     */
    @Async
    @Override
    public SimpleResponse batchAddCacheByProductId() {
        return this.batchAddCacheByProductId(ProductSortEnum.SALE.getCode());
    }

    /**
     * 抖带带选货列表 查询默认销量排序
     * @param sort
     * @return
     */
    @Async
    @Override
    public SimpleResponse batchAddCacheByProductId(Integer sort) {
        lock.lock();
        try {
            List<DisProductListInfoDTO> productList = sellerGoodsDistributionMapper.batchAddCacheByProductIdAll(sort);
            if (null == productList) {
                return new SimpleResponse(-1, "未发现该商品");
            }

            //总条数
            int num = productList.size();
            //分批条数
            int size = 200;
            //批数
            int no = 0;
            if (num % size == 0) {
                no = num / size;
            } else {
                no = num / size + 1;
            }

            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>have find batchAddCacheByProductId no =" + no + ",num=" + num);

            for (int i = 0; i <= no; i++) {
                for (int k = i * size; k < (i + 1) * size; k++) {
                    if (k >= num) {
                        break;
                    }
                    DisProductListInfoDTO dto = productList.get(k);
                    addCache(dto);
                }
            }
        } catch (Exception e) {
            return new SimpleResponse(-1, "fail");
        } finally {
            lock.unlock();
        }
        return new SimpleResponse(0, "success");
    }

    /**
     * 将含有分销属性的商品加入缓存
     *
     * @param dto
     */
    private void addCache(DisProductListInfoDTO dto) {
        if (null == dto) {
            return;
        }
        //获取已售件数
        Long pid = dto.getProductId();
        //int count = orderAppService.countGoodsNumByOrder(pid);
        long count = sellerGoodsDistributionMapper.getStockNum(pid);

        stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, pid.toString(), count);
    }

    /**
     * 无用的方法
     * @param productId
     * @param num
     */
    @Async
    @Override
    public void addCacheByProductId(Long productId, int num) {
        Product product = this.productMapper.selectByPrimaryKey(productId);
        if (null == product) {
            return;
        }

        Long parendId = product.getParentId();
        stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, parendId.toString(), num);
    }
}
