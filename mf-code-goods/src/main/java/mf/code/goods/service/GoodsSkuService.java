package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductSkuDTO;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.shop.constants.AppTypeEnum;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.goods.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 20:44
 */
public interface GoodsSkuService {

    /***
     * 查询商品详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param goodsId
     * @param appTypeEnum
     * @return
     */
    SimpleResponse getGoodsDetail(Long merchantId,
                                  Long shopId,
                                  Long userId,
                                  Long goodsId, AppTypeEnum appTypeEnum);

    /**
     * 修改sku价格
     *
     * @param skuId
     * @param sellingPrice
     * @return
     */
    SimpleResponse updatePrice(Long skuId, BigDecimal sellingPrice);


    /**
     * 商户-更新sku 库存
     *
     * @param skuId
     * @param stock
     * @return
     */
    SimpleResponse updateStock(Long skuId, int stock);

    /**
     * 库存消费
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @return SimpleResponse
     */
    SimpleResponse consumeStock(Long productSkuId, int stockNumber, Long orderId);

    /**
     * 还库存
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @return SimpleResponse
     */
    SimpleResponse giveBackStock(Long productSkuId, int stockNumber, Long orderId);

    /**
     * 修改sku价格和库存等信息 , 修改价格生成快照,其他修改信息记录在新sku中
     *
     * @param skuById
     * @param skuDto
     * @param now
     * @param product
     * @param skuSellingPriceFront
     */
    SimpleResponse updatePriceAndStock(ProductSku skuById, ProductSkuDTO skuDto, Date now, Product product, BigDecimal skuSellingPriceFront);
}
