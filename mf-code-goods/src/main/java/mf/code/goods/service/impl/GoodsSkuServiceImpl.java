package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.NumberValidationUtil;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.*;
import mf.code.goods.api.seller.dto.PlatformDistRateAndPriceDTO;
import mf.code.goods.api.seller.dto.ProductSkuInfoDTO;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.common.constant.RedisKey;
import mf.code.goods.common.exception.BusinessException;
import mf.code.goods.common.redis.RedisForbidRepeat;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.domain.valueobject.ProductSkuRoot;
import mf.code.goods.dto.GoodProductSkuDTO;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.goods.dto.ProductSkuDTO;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.dao.SellerGoodsDistributionMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.GoodsSkuLogService;
import mf.code.goods.service.GoodsSkuService;
import mf.code.goods.service.GoodsUpdateService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.UserCouponDTO;
import mf.code.shop.constants.AppTypeEnum;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.goods.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 20:44
 */
@Service
@Slf4j
public class GoodsSkuServiceImpl implements GoodsSkuService {
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private mf.code.goods.service.GoodsSkuAboutService GoodsSkuAboutService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GoodsSkuLogService goodsSkuLogService;
    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SellerGoodsDistributionMapper sellerGoodsDistributionMapper;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private GoodsUpdateService goodsUpdateService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private CommentAppService commentAppService;

    @Override
    public SimpleResponse getGoodsDetail(Long merchantId, Long shopId, Long userId, Long goodsId, AppTypeEnum appTypeEnum) {

        Product product = this.productMapper.selectByPrimaryKey(goodsId);
        if (product == null || (product.getDel() == DelEnum.YES.getCode())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品不存在..");
        }
        // 为了商品查询的方便,这里直接生成一条分销记录
        if (!product.getShopId().equals(shopId)) {
            product = goodsUpdateService.getProductForPlatMall(merchantId, shopId, goodsId, appTypeEnum);
        }
        GoodProductSkuDTO resp = new GoodProductSkuDTO();
        resp.setStatus(product.getStatus());
        if (product.getStatus() != 1) {
            //商品下架标识
            resp.setStatus(0);
        }
        resp.setGoodsId(product.getId());
        resp.setTitle(product.getProductTitle());
        //商品主图
        if (StringUtils.isNotBlank(product.getMainPics())) {
            List<String> mainPics = JSONObject.parseArray(product.getMainPics(), String.class);
            List<String> pics = new ArrayList<>();
            for (String string : mainPics) {
                pics.add(string + compressionRatioPic);
            }
            resp.setGoodsPics(pics);
        }
        //商品详情长图
        if (StringUtils.isNotBlank(product.getDetailPics())) {
            resp.setPicDetail(JSONObject.parseArray(product.getDetailPics(), String.class));
        }

        //是否是分销商品
        ProductDistributionTypeEnum distributionTypeEnum = ProductDistributionTypeEnum.findByParams(product.getId(), product.getParentId(), product.getSelfSupportRatio(), product.getPlatSupportRatio());
        resp.setGoodsType(distributionTypeEnum.getCode());

        //获取sku信息 not_show 0显示 1不显示 snapshoot 0非快照 1快照
        QueryWrapper<ProductSku> skuQueryWrapper = new QueryWrapper<>();
        skuQueryWrapper.lambda()
                .eq(ProductSku::getProductId, product.getParentId())
                .eq(ProductSku::getSnapshoot, ProductConstant.ProductSku.Snapshoot.NOT_SNAPSHOOT)
                .eq(ProductSku::getNotShow, ProductConstant.ProductSku.NotShow.SHOW)
                .eq(ProductSku::getDel, DelEnum.NO.getCode())
                .orderByDesc(ProductSku::getSkuSort)
        ;
        List<ProductSku> productSkus = this.productSkuMapper.selectList(skuQueryWrapper);
        if (CollectionUtils.isEmpty(productSkus)) {
            SimpleResponse simpleResponse = new SimpleResponse();
            simpleResponse.setData(resp);
            return simpleResponse;
        }
        //商品价格
        BigDecimal minPrice = productSkus.get(0).getSkuSellingPrice();
        BigDecimal maxPrice = productSkus.get(0).getSkuSellingPrice();
        BigDecimal originalMinPrice = productSkus.get(0).getSkuThirdPrice();
        BigDecimal originalMaxPrice = productSkus.get(0).getSkuThirdPrice();
        List<Long> catIds = new ArrayList<>();
        List<String> specsValues = new ArrayList<>();
        int stock = 0;
        int stockDef = 0;
        for (ProductSku productSku : productSkus) {
            String[] split = productSku.getSpecSort().split(",");
            for (int i = 0; i < split.length; i++) {
                String[] split1 = split[i].split(":");
                //获取类目
                if (catIds.indexOf(NumberUtils.toLong(split1[0])) == -1) {
                    catIds.add(NumberUtils.toLong(split1[0]));
                }
                //获取类目下的属性
                if (specsValues.indexOf(split1[1]) == -1) {
                    specsValues.add(split1[1]);
                }
            }
            if (minPrice.compareTo(productSku.getSkuSellingPrice()) > 0) {
                minPrice = productSku.getSkuSellingPrice();
            }
            if (maxPrice.compareTo(productSku.getSkuSellingPrice()) < 0) {
                maxPrice = productSku.getSkuSellingPrice();
            }
            if (originalMinPrice.compareTo(productSku.getSkuThirdPrice()) > 0) {
                originalMinPrice = productSku.getSkuThirdPrice();
            }
            if (originalMaxPrice.compareTo(productSku.getSkuThirdPrice()) < 0) {
                originalMaxPrice = productSku.getSkuThirdPrice();
            }
            stock = productSku.getStock() + stock;
            stockDef = productSku.getStockDef() + stockDef;
        }

        //获取sku类目信息
        resp.setItems(this.GoodsSkuAboutService.getSkuItemInfo(catIds, specsValues));

        int totalSale = 0;
        // 平台商城直接取用分销市场销量缓存。否则采用之前的查询订单逻辑
        if (!product.getShopId().equals(shopId) || product.getFromFlag() == ProductFromFlagEnum.MALL.getCode()) {
            Double score = stringRedisTemplate.opsForZSet().score(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, product.getParentId().toString());
            if (score != null && score > 0D) {
                totalSale = score.intValue();
            }
        } else {
            if (appTypeEnum == AppTypeEnum.JKMF) {
                //已售件数
                int sale = this.orderAppService.countGoodsNumByOrder(goodsId);
                totalSale = product.getInitSales() + sale;
            } else {
                totalSale = stockDef - stock;
            }
        }

        resp.setSaleQuantity(NumberValidationUtil.convertNumberForString(totalSale));
        //获取catProSpecs = this.productSpecsMapper.selectList(specsQueryWrapper);商品价格,某宝价区间 product_sku: 价格遍历
        resp.setPrice(minPrice + "-" + maxPrice);
        resp.setMinPrice(minPrice.setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setOriginalPrice(originalMinPrice + "-" + originalMaxPrice);
        resp.setMinOriginalPrice(originalMinPrice.setScale(2, BigDecimal.ROUND_DOWN).toString());

        //客服微信号
        Map<String, Object> shopInfoByLogin = this.shopAppService.queryShopInfoByLogin(String.valueOf(shopId));
        if (shopInfoByLogin != null && shopInfoByLogin.get("serviceWx") != null) {
            resp.setServiceWx((shopInfoByLogin.get("serviceWx").toString()));
        }

        //获取该用户该店铺本月收益信息(查询 在此店铺内， 用户本月消费和收入的概况)
        if (userId != null) {
            UserDistributionInfoDTO incomeOverviewTomonthDTO = this.distributionAppService.queryDistributionInfo(String.valueOf(userId), String.valueOf(shopId));
            if (incomeOverviewTomonthDTO != null) {
                resp.setProfit(incomeOverviewTomonthDTO);
            }
        }


        //返现金额:平台抽佣比例，店铺分销比例，goodsId,parentId,价格(price),spendingAmountTomonth（本月的用户消费）
        ProductDistributionDTO distributionDTO = new ProductDistributionDTO();
        distributionDTO.setParentId(product.getParentId());
        distributionDTO.setProductId(product.getId());
        distributionDTO.setSelfSupportRatio(product.getSelfSupportRatio());
        distributionDTO.setPlatSupportRatio(product.getPlatSupportRatio());
        distributionDTO.setShopId(product.getShopId());
        distributionDTO.setUserId(userId);
        resp.setCashback("0.00");
        if (minPrice.compareTo(BigDecimal.ZERO) > 0 && userId != null) {
            distributionDTO.setPrice(minPrice);
            String rebateCommission = distributionAppService.getRebateCommission(distributionDTO);
            resp.setCashback(rebateCommission);
        }

        //获取sku属性列表
        resp.setSkus(this.GoodsSkuAboutService.getSkuInfo(product, productSkus, distributionDTO, appTypeEnum));
        //商品库存
        resp.setStock(stock);

        List<UserCouponDTO> userCouponDTOS = oneAppService.findUserMerchantShopCouponByProductId(userId, product.getId());
        if (!CollectionUtils.isEmpty(userCouponDTOS)) {
            if (userCouponDTOS.get(0).getStatus() != UserCouponStatusEnum.USED.getCode()) {
                Map map = new HashMap();
                map.put("id", userCouponDTOS.get(0).getId());
                map.put("amount", userCouponDTOS.get(0).getAmount());
                resp.setDiscountInfo(map);
            }
        }
        Map<String, Object> map = oneAppService.queryProductActivityInfo(product.getId());
        if (!CollectionUtils.isEmpty(map)) {
            resp.setActivityInfo(map);
        }

        // 2.6迭代 新增 商品评价功能
        if (product.getFromFlag() == ProductFromFlagEnum.PLAT.getCode()) {
            Map commentInfo = commentAppService.queryProductCommentPage(product.getParentId(), shopId, true);
            resp.setCommentInfo(commentInfo);
        } else {
            Map commentInfo = commentAppService.queryProductCommentPage(goodsId, shopId, false);
            resp.setCommentInfo(commentInfo);
        }
        // 新增返回信息 -> 佣金
        if(appTypeEnum == AppTypeEnum.DDD){
            if (product.getFromFlag() != ProductFromFlagEnum.PLAT.getCode()) {
                //此时该用户只能上架
                resp.setStatus(0);
            }

            DistributeInfoEntity distributeInfoEntity = new DistributeInfoEntity();
            distributeInfoEntity.setPrice(product.getProductPriceMin());
            distributeInfoEntity.setProductSpec(product.getProductSpecs());
            distributeInfoEntity.setSelfSupportRadio(product.getSelfSupportRatio());
            distributeInfoEntity.setPlatSupportRadio(product.getPlatSupportRatio());
            ProductDistributCommissionEntity distrIncodeByProduct = distributionAppService.getDistrIncodeByProduct(distributeInfoEntity);
            resp.setCommissionNum(distrIncodeByProduct.getPlatDistributCommission());
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        if (userId != null) {
            asyncBizLogForViewGoods(merchantId, shopId, userId, product);
        }
        return simpleResponse;
    }

    /**
     * 埋点
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param product
     */
    private void asyncBizLogForViewGoods(Long merchantId, Long shopId, Long userId, Product product) {
        FanActionForMQ fanActionForMQ = new FanActionForMQ();
        fanActionForMQ.setMid(merchantId);
        fanActionForMQ.setSid(shopId);
        fanActionForMQ.setUid(userId);
        fanActionForMQ.setEvent(FanActionEventEnum.VIEW_GOODS.getCode());
        fanActionForMQ.setEventId(product.getId());
        fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
        rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
                JSON.toJSONString(fanActionForMQ), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("浏览商品埋点发送成功");
                    }

                    @Override
                    public void onException(Throwable e) {
                        log.info("浏览商品埋点发送失败");
                    }
                });
    }

    /**
     * 修改价格
     *
     * @param skuId        sku的id
     * @param sellingPrice 售价
     * @return
     */
    @Override
    @Transactional
    public SimpleResponse updatePrice(Long skuId, BigDecimal sellingPrice) {
        //生成快照
        Date now = new Date();
        ProductSku productSku = productSkuMapper.selectByPrimaryKey(skuId);
        if (productSku == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此sku");
        }
        SimpleResponse updateSnapshootError = this.updateSnapshootError(productSku, skuId, now);
        if (updateSnapshootError != null) {
            return updateSnapshootError;
        }
        SimpleResponse createNewSkuError = this.createNewSkuError(productSku, skuId, sellingPrice, now);
        if (createNewSkuError != null) {
            return createNewSkuError;
        }

        /****价格修改后需要重新计算佣金**/
        Map<String, Object> params = new HashMap<>();
        params.put("productId", productSku.getProductId());

        Product pt = productMapper.selectByPrimaryKey(productSku.getProductId());
        BigDecimal platSupportRatios = pt.getPlatSupportRatio();
        //判断分销属性是否为空
        if (null != platSupportRatios) {
            List<ProductSku> skuByProductId = productSkuMapper.findByProductId(productSku.getProductId());
            BigDecimal min = this.findPriceMinFromSku(skuByProductId);
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>have find selesPrice min =" + min);
            PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(productSku.getShopId().toString(), pt.getProductSpecs(), min, platSupportRatios);
            //供应商的抽佣比例（去除平台固定抽佣比例）
            BigDecimal platSupportRatio = platformDistRate.getPlatSupportRatio();
            BigDecimal commissionNum = min.multiply(platSupportRatio).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
            params.put("commissionNum", commissionNum);
            int count = productMapper.updateByProductId(params);

            log.info("have find commissionNum=" + commissionNum + ",priceAfterDist=" + platformDistRate.getPriceAfterDist());
            if (count < 1) {
                return new SimpleResponse(1, "fail");
            }

        }

        /***价格修改后需要重新计算佣金end***/

        /*******add 修改product价格相关的属性begin************/
        ProductSkuInfoDTO dto = productSkuMapper.getProductSkuPriceInfo(productSku.getProductId());
        int count = sellerGoodsDistributionMapper.updateProductPriceInfoById(dto);
        if (count < 1) {
            log.error("修改product中的价格相关的属性错误,productId={}", productSku.getProductId());
            Assert.isTrue(false, 202, "修改价格相关属性错误");
        }
        /*******add 修改product价格相关的属性end**************/

        return new SimpleResponse(0, "success");
    }

    private BigDecimal findPriceMinFromSku(List<ProductSku> list) {
        BigDecimal min = new BigDecimal(9999999);
        for (ProductSku sku : list) {
            BigDecimal skuSellingPrice = sku.getSkuSellingPrice();
            if (min.compareTo(skuSellingPrice) > 0) {
                min = skuSellingPrice;
            }
        }
        return min;
    }

    /**
     * 创建新sku
     *
     * @param productSku
     * @param skuId
     * @param sellingPrice
     * @param now
     * @return
     */
    private SimpleResponse createNewSkuError(ProductSku productSku, Long skuId, BigDecimal sellingPrice, Date now) {
        //创建新sku，新建不能使用原有的数据对象
        ProductSku newProductSku = new ProductSku();
        BeanUtils.copyProperties(productSku, newProductSku);
        newProductSku.setId(null);
        newProductSku.setSkuSellingPrice(sellingPrice);
        newProductSku.setSnapshoot(ProductConstant.ProductSku.Snapshoot.NOT_SNAPSHOOT);
        newProductSku.setCtime(now);
        newProductSku.setOldSkuId(skuId);
        int insertResult = productSkuMapper.insertSelective(newProductSku);
        if (insertResult != 1) {
            // 若使用回滚需要抛出异常
            log.error("修改商品SKU价格业务,在将SKU置为快照成功,新增SKU记录失败,回滚,SKU_ID={}", skuId);
            throw new BusinessException(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "修改价格失败");
        }
        return null;
    }

    /**
     * 生成快照
     *
     * @param productSku
     * @param skuId      id 写的太蠢,有时间改掉
     * @param now
     * @return
     */
    private SimpleResponse updateSnapshootError(ProductSku productSku, Long skuId, Date now) {
        productSku.setId(skuId);
        productSku.setSnapshoot(ProductConstant.ProductSku.Snapshoot.IS_SNAPSHOOT);
        productSku.setUtime(now);
        int updateResult = productSkuMapper.updateByPrimaryKeySelective(productSku);
        if (updateResult == 0) {
            log.error("修改商品SKU价格业务,在将SKU置为快照失败,SKU_ID={}", skuId);
            return new SimpleResponse(1, "修改价格失败");
        }
        return null;
    }

    /**
     * 商户修改价格和sku的库存,等信息
     */
    @Override
    @Transactional
    public SimpleResponse updatePriceAndStock(ProductSku skuById, ProductSkuDTO skuDto, Date now, Product product, BigDecimal skuSellingPriceFront) {
        //快照
        skuById.setSnapshoot(ProductConstant.ProductSku.Snapshoot.IS_SNAPSHOOT);
        //修改时间
        skuById.setUtime(now);
        SimpleResponse updateSnapshootError = this.updateSnapshootError(skuById, skuById.getId(), now);
        if (updateSnapshootError != null) {
            return updateSnapshootError;
        }

        //图片
        skuById.setSkuPic(skuDto.getSkuPic());
        //定义的库存stock_def= stock_def + (商户设置的新库存 - sku的原有库存)
        skuById.setStockDef(skuById.getStockDef() + (skuDto.getStock() - skuById.getStock()));
        //库存
        skuById.setStock(skuDto.getStock());
        //淘宝价
        skuById.setSkuThirdPrice(skuDto.getSkuThirdPrice());
        //是否展示
        skuById.setNotShow(skuDto.getNotShow());
        SimpleResponse createNewSkuError = this.createNewSkuError(skuById, skuById.getId(), skuDto.getSkuSellingPrice(), now);
        if (createNewSkuError != null) {
            return createNewSkuError;
        }

        return new SimpleResponse(0, "success");
    }

    /**
     * 商家修改库存
     *
     * @param skuId    商品sku的id
     * @param stockNew
     * @return
     */
    @Override
    @Transactional
    public SimpleResponse updateStock(Long skuId, int stockNew) {
        String redisKey = RedisKey.Product.Sku.SKU_ID + skuId;
        if (!RedisForbidRepeat.NX(stringRedisTemplate, redisKey, 5, TimeUnit.SECONDS)) {
            return new SimpleResponse(11, "网络繁忙,请稍后操作");
        }
        ProductSku skuById = productSkuMapper.selectByPrimaryKey(skuId);
        if (skuById == null) {
            return new SimpleResponse(12, "该商品不存在");
        }
        if (skuById.getDel() == 1) {
            return new SimpleResponse(13, "已被标记删除 ");
        }
        SimpleResponse simpleResponse = this.updateStockHandler(stockNew, skuById);
        stringRedisTemplate.delete(redisKey);

        return simpleResponse;
    }

    private SimpleResponse stockError(int stockNumber) {
        if (stockNumber < 0) {
            return new SimpleResponse(901, "数量需大于零");
        }
        return null;
    }

    /**
     * 消费库存
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @return SimpleResponse
     */
    @Override
    @Transactional
    public SimpleResponse consumeStock(Long productSkuId, int stockNumber, Long orderId) {
        SimpleResponse simpleResponse = this.stockError(stockNumber);
        if (simpleResponse != null) {
            return simpleResponse;
        }
        return this.stockHandler(productSkuId, stockNumber, true, orderId);
    }

    /**
     * 还库存
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @return SimpleResponse
     */
    @Override
    @Transactional
    public SimpleResponse giveBackStock(Long productSkuId, int stockNumber, Long orderId) {
        SimpleResponse simpleResponse = this.stockError(stockNumber);
        if (simpleResponse != null) {
            return simpleResponse;
        }
        return this.stockHandler(productSkuId, stockNumber, false, orderId);
    }


    /**
     * c端的处理库存
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @param isConsume    true:消耗 false:还库存
     * @return SimpleResponse
     */
    private SimpleResponse stockHandler(Long productSkuId, int stockNumber, Boolean isConsume, Long orderId) {
        // 处理快照问题
        productSkuId = this.skuSnapshootHanlder(productSkuId);
        if (productSkuId == null) {
            return new SimpleResponse(904, "此商品不存在或不合法");
        }
        //
        String redisKey = RedisKey.Product.Sku.SKU_ID + productSkuId;
        if (!RedisForbidRepeat.NX(stringRedisTemplate, redisKey, 5, TimeUnit.SECONDS)) {
            return new SimpleResponse(903, "此sku库存资源被占用,请稍后再试");
        }
        Map<String, Object> conditionMap = new HashMap<>();
        conditionMap.put("skuId", productSkuId);
        conditionMap.put("stockNumber", isConsume ? stockNumber : (-1 * stockNumber));
        int updateStockResult = productSkuMapper.updateStockAtom(conditionMap);
        if (updateStockResult != 1) {
            ProductSku productSku = productSkuMapper.selectByPrimaryKey(productSkuId);
            throw new BusinessException(902, "修改库存失败,库存剩余为:" + productSku.getStock());
        }
        RedisForbidRepeat.delKey(stringRedisTemplate, redisKey);

        ProductSku productSku = productSkuMapper.selectByPrimaryKey(productSkuId);

        Integer stock = productSku.getStock();
        Integer stockDef = productSku.getStockDef();
        if (stock > stockDef) {
            throw new BusinessException(904, "动态库存大于定义库存.");
        }
        //插入log表
        goodsSkuLogService.insertLogForClient(productSku, stockNumber, isConsume, isConsume ? 2 : 3, orderId);

        /******add 换库存时需要修改已有库存的值begin ********/
        try {
            Product product = this.productMapper.selectByPrimaryKey(productSku.getProductId());
            if (null == product) {
                throw new BusinessException(904, "获取产品信息失败");
            }
            //判断商品是否具有分销属性
            if (null != product.getPlatSupportRatio() &&
                    1 == product.getCheckStatus() &&
                    1 == product.getStatus()) {
                //库存数
                int num = 0;
                if (isConsume) {
                    num = stockNumber;
                } else {
                    num = -1 * stockNumber;
                }
                log.info(">>>>>>>>>>>>>>> have find num=" + num);
                stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, product.getParentId().toString(), num);
            }

        } catch (Exception e) {
            throw new BusinessException(904, "修改库存信息失败");
        }
        /******add 换库存时需要修改已有库存的值end ********/

        return new SimpleResponse(0, "success");
    }

    /**
     * 查询最新的商品skuId，但由于sku中字段含义变化，所以此处的逻辑变更：
     * 1、查询当前sku
     * 2、判断当前sku为普通商品sku，not_show为0或者1，则按照not_show为1或者0来查询
     * 3、如果当前为特殊商品sku，如秒杀，活动sku，则直接使用当前的sku
     *
     * @param productSkuId
     * @return
     */
    private Long skuSnapshootHanlder(Long productSkuId) {
        ProductSku currentSku = productSkuMapper.selectByPrimaryKey(productSkuId);
        if (currentSku == null) {
            throw new BusinessException(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "查询sku错误，数据错误。");
        }
        Integer notShow = currentSku.getNotShow();
        // 如果当前为特殊商品sku，如秒杀，活动sku，则直接返回当前的sku
        if (notShow == ProductConstant.ProductSku.NotShow.FLASH_SALE
                || notShow == ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK) {
            return productSkuId;
        }
        Long receiveStockSkuId = productSkuId;
        while (true) {
            ProductSku productSku = productSkuMapper.selectProductSkuBySkuOldId(receiveStockSkuId);
            //如果根据old_sku_id中查不到sku,就说明此sku不是快照
            if (productSku == null) {
                return receiveStockSkuId;
            } else {
                receiveStockSkuId = productSku.getId();
            }
        }
    }

    /**
     * 商家库存处理逻辑
     *
     * @param stockNew
     * @param skuById
     * @return
     */
    private SimpleResponse updateStockHandler(int stockNew, ProductSku skuById) {
        Long skuId = skuById.getId();
        Integer stockDef = skuById.getStockDef();
        if (stockDef == null) {
            Assert.isTrue(false, 11, "product.stock_def=null,此场景应只出现在直接在数据库造的数据");
        }
        Map<String, Object> conditionMap = new HashMap<>();
        conditionMap.put("skuId", skuId);
        conditionMap.put("stockNew", stockNew);
        conditionMap.put("utime", new Date());

        int updateResult = productSkuMapper.updateStockAtomMerchant(conditionMap);
        if (updateResult != 1) {
            log.error("修改商品库存失败,SKU_ID={}", skuById.getId());
            return new SimpleResponse(10, "修改库存失败");
        }
        ProductSku beanForStockLog = new ProductSkuRoot().beanForStockLog(skuById.getMerchantId(), skuById.getShopId(), skuById.getProductId(), skuById.getId());
        goodsSkuLogService.insertLogForMerchant(beanForStockLog, (stockNew - skuById.getStock()));
        return new SimpleResponse(0, "success");
    }
}
