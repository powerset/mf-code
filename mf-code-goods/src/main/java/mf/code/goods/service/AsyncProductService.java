package mf.code.goods.service;

/**
 * mf.code.goods.service
 * Description:
 *
 * @author gel
 * @date 2019-07-01 17:37
 */
public interface AsyncProductService {

    /**
     * 创建店铺商品零级类目缓存
     *
     * @param shopId
     */
    void createProductCategoryZeroRedisForShop(Long shopId);
}
