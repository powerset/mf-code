package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.goods.api.feignclient.ShopAppService;
import mf.code.goods.api.seller.dto.PlatformDistRateAndPriceDTO;
import mf.code.goods.api.seller.dto.ProductSkuSpec;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.constant.DistributionAuditStatusEnum;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.dto.PlatformDistAuditReqDTO;
import mf.code.goods.dto.PlatformDistListReqDTO;
import mf.code.goods.dto.PlatformDistListResultDTO;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductDistributionAuditLog;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.repostory.CategoryTaobaoRepository;
import mf.code.goods.repo.repostory.ProductDistributionAuditLogRepository;
import mf.code.goods.repo.repostory.SellerProductRepository;
import mf.code.goods.repo.repostory.SellerSkuRepository;
import mf.code.goods.repo.vo.ProductSkuSpecValVo;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.GoodsSkuAboutService;
import mf.code.goods.service.PlatformDistAuditService;
import mf.code.goods.service.ProductSkuSpecService;
import mf.code.shop.dto.ShopDetailForPlatformDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-07 19:36
 */
@Service
@Slf4j
public class PlatformDistAuditServiceImpl implements PlatformDistAuditService {
    @Autowired
    private ProductDistributionAuditLogRepository productDistributionAuditLogRepository;
    @Autowired
    private SellerProductRepository sellerProductRepostory;
    @Autowired
    private CategoryTaobaoRepository categoryTaobaoRepository;
    @Autowired
    private SellerSkuRepository sellerSkuRepostory;
    @Autowired
    private GoodsSkuAboutService goodsSkuAboutService;
    @Autowired
    private ProductSkuSpecService productSkuSpecService;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 平台运营-商品审核列表
     *
     * @param reqDTO
     */
    @Override
    public SimpleResponse checkList(PlatformDistListReqDTO reqDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", 0);
        resultMap.put("list", new ArrayList<>());
        QueryWrapper<ProductDistributionAuditLog> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(reqDTO.getMerchant()) && RegexUtils.isMobileSimple(reqDTO.getMerchant())) {
            Long merchantIdByPhone = shopAppService.getMerchantIdByPhone(reqDTO.getMerchant());
            queryWrapper.lambda().eq(ProductDistributionAuditLog::getMerchantId, merchantIdByPhone);
        }
        if (StringUtils.isNotBlank(reqDTO.getShopName())) {
            List<Long> shopIdList = shopAppService.getShopIdListByShopName(reqDTO.getShopName());
            if (CollectionUtils.isEmpty(shopIdList)) {
                return simpleResponse;
            }
            queryWrapper.lambda().in(ProductDistributionAuditLog::getShopId, shopIdList);
        }
        if (StringUtils.isNotBlank(reqDTO.getSubmitTimeBegin()) && StringUtils.isNotBlank(reqDTO.getSubmitTimeEnd())) {
            Date begin = DateUtil.stringtoDate(reqDTO.getSubmitTimeBegin(), DateUtil.LONG_DATE_FORMAT);
            Date end = DateUtil.stringtoDate(reqDTO.getSubmitTimeEnd(), DateUtil.LONG_DATE_FORMAT);
            if (begin.after(end)) {
                return simpleResponse;
            }
            queryWrapper.lambda().between(ProductDistributionAuditLog::getCtime, reqDTO.getSubmitTimeBegin(),
                    DateUtil.dateToString(DateUtil.addDay(end, 1), DateUtil.LONG_DATE_FORMAT));
        }
        Set<Integer> statusSet = new HashSet<>();
        // 类型 1:未审核 2:已审核
        if (reqDTO.getType() != null) {
            // 未审核
            if (reqDTO.getType() == 1) {
                statusSet.add(DistributionAuditStatusEnum.INIT.getCode());
            }
            // 已审核
            if (reqDTO.getType() == 2) {
                // 审核状态0:全部 1:审核通过 2:审核不通过
                if (reqDTO.getCheckResult() != null) {
                    if (reqDTO.getCheckResult() == 1) {
                        statusSet.add(DistributionAuditStatusEnum.SUCCESS.getCode());
                    } else if (reqDTO.getCheckResult() == 2) {
                        statusSet.add(DistributionAuditStatusEnum.FAIL.getCode());
                    } else {
                        statusSet.addAll(Arrays.asList(DistributionAuditStatusEnum.SUCCESS.getCode(), DistributionAuditStatusEnum.FAIL.getCode()));
                    }
                } else {
                    statusSet.addAll(Arrays.asList(DistributionAuditStatusEnum.SUCCESS.getCode(), DistributionAuditStatusEnum.FAIL.getCode()));
                }
            }
        }
        queryWrapper.lambda().in(ProductDistributionAuditLog::getStatus, statusSet);
        if (reqDTO.getSize() > 100) {
            reqDTO.setSize(100);
        }
        Page<ProductDistributionAuditLog> page = new Page<>(reqDTO.getPage(), reqDTO.getSize());
        IPage<ProductDistributionAuditLog> productDistributionAuditLogIPage = productDistributionAuditLogRepository.selectPage(page, queryWrapper);
        List<ProductDistributionAuditLog> auditLogList = productDistributionAuditLogIPage.getRecords();
        resultMap.put("total", productDistributionAuditLogIPage.getTotal());
        resultMap.put("list", new ArrayList<>());
        if (CollectionUtils.isEmpty(auditLogList)) {
            simpleResponse.setData(resultMap);
            return simpleResponse;
        }
        // 查询商品，及店铺id
        Set<Long> productIdSet = new HashSet<>();
        Set<Long> shopIdSet = new HashSet<>();
        for (ProductDistributionAuditLog auditLog : auditLogList) {
            productIdSet.add(auditLog.getProductId());
            shopIdSet.add(auditLog.getShopId());
        }
        // 查询商品信息
        List<Product> productList = sellerProductRepostory.selectBatchIds(new ArrayList<>(productIdSet));
        List<ShopDetailForPlatformDTO> shopDetailList = shopAppService.getShopListByShopIdList(new ArrayList<>(shopIdSet));
        Map<Long, Product> productMap = new HashMap<>();
        Map<Long, ShopDetailForPlatformDTO> shopDetailMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(productList)) {
            for (Product product : productList) {
                productMap.put(product.getId(), product);
            }
        }
        if (!CollectionUtils.isEmpty(shopDetailList)) {
            for (ShopDetailForPlatformDTO dto : shopDetailList) {
                shopDetailMap.put(dto.getId(), dto);
            }
        }
        // 查询销量
        Map<Long, Integer> countByProductIdList = orderAppService.countGoodsNumByProductIds(new ArrayList<>(productIdSet));
        List<PlatformDistListResultDTO> resultDTOList = new ArrayList<>();
        for (ProductDistributionAuditLog auditLog : auditLogList) {
            PlatformDistListResultDTO resultDTO = new PlatformDistListResultDTO();
            resultDTO.setId(auditLog.getId());
            resultDTO.setMerchantId(auditLog.getMerchantId());
            resultDTO.setCheckStatus(auditLog.getStatus().toString());
            resultDTO.setCheckTime(auditLog.getAuditTime() == null ? "" : DateUtil.dateToString(auditLog.getAuditTime(), DateUtil.FORMAT_ONE));
            resultDTO.setCommitTime(DateUtil.dateToString(auditLog.getCtime(), DateUtil.FORMAT_ONE));
            resultDTO.setProductId(auditLog.getProductId());
            resultDTO.setProportion(auditLog.getPlatSupportRatio().toString());
            ShopDetailForPlatformDTO shopDetailForPlatformDTO = shopDetailMap.get(auditLog.getShopId());
            if (shopDetailForPlatformDTO != null) {
                String addressText = shopDetailForPlatformDTO.getAddressText();
                addressText = addressText == null ? "" : addressText.replace("_", "");
                resultDTO.setAddress(addressText + shopDetailForPlatformDTO.getAddressDetail());
                resultDTO.setPhone(shopDetailForPlatformDTO.getPhone());
                resultDTO.setShopId(shopDetailForPlatformDTO.getId());
                resultDTO.setShopName(shopDetailForPlatformDTO.getShopName());
                resultDTO.setShopowner(shopDetailForPlatformDTO.getUsername());
                resultDTO.setMerchant(shopDetailForPlatformDTO.getMerchantPhone());
            }
            Product product = productMap.get(auditLog.getProductId());
            if (product != null) {
                // 查询商品销量最小金额
                List<ProductSku> productSkus = sellerSkuRepostory.selectByProductId(auditLog.getProductId());
                if (!CollectionUtils.isEmpty(productSkus)) {
                    BigDecimal min = new BigDecimal("100000");
                    BigDecimal max = BigDecimal.ZERO;
                    Integer stock = 0;
                    for (ProductSku productSku : productSkus) {
                        if (productSku.getSkuSellingPrice().compareTo(min) < 0) {
                            min = productSku.getSkuSellingPrice();
                        }
                        if (productSku.getSkuSellingPrice().compareTo(max) > 0) {
                            max = productSku.getSkuSellingPrice();
                        }
                        stock += productSku.getStock();
                    }
                    resultDTO.setPriceMax(max.toString());
                    resultDTO.setPriceMin(min.toString());
                    resultDTO.setSurplusStock(stock.toString());
                }
                JSONArray jsonArray = JSONArray.parseArray(product.getMainPics());
                if (jsonArray != null) {
                    resultDTO.setProductPic(jsonArray.get(0).toString());
                }
                resultDTO.setProductName(product.getProductTitle());
            }
            if (!CollectionUtils.isEmpty(countByProductIdList)) {
                Integer count = countByProductIdList.get(auditLog.getProductId());
                if (count != null) {
                    resultDTO.setSalesVolume(count.toString());
                }
            }
            resultDTOList.add(resultDTO);
        }
        resultMap.put("list", resultDTOList);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 详情
     *
     * @param productId
     * @return
     */
    @Override
    public SimpleResponse goodsDetail(Long productId, Long auditId) {
        Product product = sellerProductRepostory.selectById(productId);
        CategoryTaobao categoryTaobao = categoryTaobaoRepository.selectBySid(product.getProductSpecs().toString(), 1);
        if (product == null) {
            return new SimpleResponse(30, "无此商品");
        }
        BigDecimal platSupportRatio = product.getPlatSupportRatio();
        if (platSupportRatio == null) {
            platSupportRatio = new BigDecimal(0);
        }
        List<ProductSku> skuList = sellerSkuRepostory.selectByProductId(productId);

        //售价
        BigDecimal priceMin = new BigDecimal(999999999);
        BigDecimal priceMax = new BigDecimal(0);
        //淘宝价
        BigDecimal priceTbMin = new BigDecimal(999999999);
        BigDecimal priceTbMax = new BigDecimal(0);

        Integer stockTotal = 0;
        Integer stockDefTotal = 0;
        String mainPicsStr = product.getMainPics();
        List<String> list = JSON.parseArray(mainPicsStr, String.class);
        List<Map<String, Object>> skuResultMapList = new ArrayList<>();

        Map<Long, String> specNameMap = goodsSkuAboutService.getSkuSalePropMap();
        //计算销售属性给前端展示
        Map<String, List<ProductSkuSpecValVo>> tempMap = new HashMap<>();
        for (ProductSku sku : skuList) {
            BigDecimal sellingPrice = sku.getSkuSellingPrice();
            priceMin = sellingPrice.compareTo(priceMin) < 0 ? sellingPrice : priceMin;
            priceMax = sellingPrice.compareTo(priceMax) > 0 ? sellingPrice : priceMax;

            //淘宝价
            BigDecimal skuThirdPrice = sku.getSkuThirdPrice();
            priceTbMin = skuThirdPrice.compareTo(priceTbMin) < 0 ? skuThirdPrice : priceTbMin;
            priceTbMax = skuThirdPrice.compareTo(priceTbMax) > 0 ? skuThirdPrice : priceTbMax;

            stockDefTotal += sku.getStockDef();
            stockTotal += sku.getStock();
            String skuPic = sku.getSkuPic();

            Map<String, Object> skuMap = new HashMap<>();
            skuMap.put("skuId", sku.getId());
            skuMap.put("stock", sku.getStock());
            //这里的快照,是设计初期的名称歧义
            skuMap.put("snapshoot", sku.getNotShow());
            skuMap.put("skuSellingPrice", sku.getSkuSellingPrice());
            skuMap.put("skuThirdPrice", sku.getSkuThirdPrice());
            skuMap.put("skuPic", skuPic == null ? (list != null && list.size() > 0 ? list.get(0) : null) : skuPic);
            String specSortStr = sku.getSpecSort();
            //存储商品属性
            List<ProductSkuSpec> productSkuSpecList = productSkuSpecService.parseProductSkuSpecs(specSortStr, specNameMap);

            skuMap.put("specs", productSkuSpecList);
            skuResultMapList.add(skuMap);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("productId", product.getId());
        resultMap.put("title", product.getProductTitle());
        //图片
        resultMap.put("mainPic", list);
        resultMap.put("detailPic", product.getDetailPics());
        //佣金
        resultMap.put("commissionMin", priceMin.multiply(platSupportRatio).divide(new BigDecimal(100)));
        resultMap.put("commissionMax", priceMax.multiply(platSupportRatio).divide(new BigDecimal(100)));
        //售价
        resultMap.put("priceMin", priceMin);
        resultMap.put("priceMax", priceMax);
        //淘宝价
        resultMap.put("skuThirdPriceMin", priceTbMin);
        resultMap.put("skuThirdPriceMax", priceTbMax);
        //商品类别
        resultMap.put("category", categoryTaobao == null ? "" : categoryTaobao.getName());
        resultMap.put("categoryName", categoryTaobao == null ? "" : categoryTaobao.getName());
        //销量
        resultMap.put("stock", stockTotal);
        resultMap.put("saleVolume", stockDefTotal - stockTotal);
        //状态
        resultMap.put("status", product.getStatus());
        // sku 列表
        resultMap.put("skuList", skuResultMapList);
        ProductDistributionAuditLog auditLog = productDistributionAuditLogRepository.selectById(auditId);
        if (auditLog != null) {
            resultMap.put("checkStatus", auditLog.getStatus());
            resultMap.put("checkTime", auditLog.getAuditTime() == null ? "" : DateUtil.dateToString(auditLog.getAuditTime(), DateUtil.FORMAT_ONE));
            resultMap.put("commitTime", DateUtil.dateToString(auditLog.getCtime(), DateUtil.FORMAT_ONE));
            resultMap.put("proportion", auditLog.getPlatSupportRatio());
        }
        List<ShopDetailForPlatformDTO> shopDetailList = shopAppService.getShopListByShopIdList(Arrays.asList(product.getShopId()));
        if (!CollectionUtils.isEmpty(shopDetailList)) {
            ShopDetailForPlatformDTO shopDetailForPlatformDTO = shopDetailList.get(0);
            shopDetailForPlatformDTO.setMerchant(shopDetailForPlatformDTO.getMerchantPhone());
            String addressText = shopDetailForPlatformDTO.getAddressText();
            addressText = addressText == null ? "" : addressText.replace("_", "");
            shopDetailForPlatformDTO.setAddress(addressText + shopDetailForPlatformDTO.getAddressDetail());
            shopDetailForPlatformDTO.setShopowner(shopDetailForPlatformDTO.getUsername());
            resultMap.put("merchantInfo", shopDetailForPlatformDTO);
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 审核查看不通过理由
     *
     * @param productId
     * @return
     */
    @Override
    public SimpleResponse getCheckReason(Long productId, Long auditId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ProductDistributionAuditLog auditLog = productDistributionAuditLogRepository.selectById(auditId);
        if (auditLog == null || !auditLog.getProductId().equals(productId)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("审核记录不存在");
            return simpleResponse;
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("checkReason", auditLog.getAuditReason());
        if (StringUtils.isNotBlank(auditLog.getAuditPics())) {
            List<String> picList = JSONArray.parseArray(auditLog.getAuditPics(), String.class);
            resultMap.put("checkReasonPic", picList);
        }
        resultMap.put("checkTime", auditLog.getAuditTime() == null ? "" : DateUtil.dateToString(auditLog.getAuditTime(), DateUtil.FORMAT_ONE));
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 审核
     *
     * @param reqDTO
     * @param checkReasonPicList
     * @return
     */
    @Override
    public SimpleResponse audit(PlatformDistAuditReqDTO reqDTO, List<String> checkReasonPicList) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (reqDTO.getAuditId() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("参数错误");
            return simpleResponse;
        }
        ProductDistributionAuditLog auditLog = productDistributionAuditLogRepository.selectById(reqDTO.getAuditId());
        if (auditLog == null) {
            log.info(">>>>>>>>>>>>>>>>>have find id not find and productid ="+reqDTO.getProductId());
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("审核记录不存在");
            return simpleResponse;
        }
        if (auditLog.getStatus() != DistributionAuditStatusEnum.INIT.getCode()) {
            log.info(">>>>>>>>>>>>have find id have been checked and productid ="+reqDTO.getProductId());
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("审核记录已更改，请刷新列表页面重新提交");
            return simpleResponse;
        }
        String jsonString = JSONArray.toJSONString(checkReasonPicList);
        if (StringUtils.isNotBlank(jsonString)) {
            auditLog.setAuditPics(jsonString);
        }
        auditLog.setAuditReason(reqDTO.getCheckReason());
        auditLog.setAuditTime(new Date());
        auditLog.setStatus(reqDTO.getCheckStatus());
        auditLog.setUtime(new Date());
        Integer count = productDistributionAuditLogRepository.updateSelective(auditLog);
        if (count < 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("审核分销属性失败");
            return simpleResponse;
        }
        if (reqDTO.getCheckStatus() == ProductConstant.Distribution.Audit.Status.PASS) {
            Product product = sellerProductRepostory.selectById(auditLog.getProductId());
            product.setPlatSupportRatio(auditLog.getPlatSupportRatio());

            /****add 分销商品审核通过后需要加入缓存，缓存中的销量为0 begin*******/
            log.info(">>>>>>>>>>>>have find reqDTO.getProductId()="+reqDTO.getProductId());
            stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, reqDTO.getProductId().toString(), 0);
            /****add 分销商品审核通过后需要加入缓存，缓存中的销量为0 end*********/

            /******平台佣金比例审核通过后需要修改佣金 begin**/
            //是否具有分销属性
            BigDecimal platSupportRatios = product.getPlatSupportRatio();
            if (platSupportRatios != null) {
                //获取佣金
                List<ProductSku> skuByProductId = productSkuMapper.findByProductId(reqDTO.getProductId());
                BigDecimal min = this.findPriceMinFromSku(skuByProductId);
                log.info(">>>>>>>>>>>>>>>>>>>>>>>hvae find min1= "+min+",getProductId="+reqDTO.getProductId()+",productid="+product.getId());
                PlatformDistRateAndPriceDTO platformDistRate = goodsService.getPlatformDistRate(product.getShopId().toString(), product.getProductSpecs(), min, platSupportRatios);
                BigDecimal priceAfterDist = platformDistRate.getPriceAfterDist();
                product.setCommissionNum(priceAfterDist);
            }
            /******平台佣金比例审核通过后需要修改佣金 end**/

            Integer productCount = sellerProductRepostory.updateBySelective(product);
            if (productCount < 1) {
                simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                simpleResponse.setMessage("审核分销属性失败");
                return simpleResponse;
            }

        }
        return simpleResponse;
    }

    private BigDecimal findPriceMinFromSku(List<ProductSku> list) {
        BigDecimal min = new BigDecimal(9999999);
        for (ProductSku sku : list) {
            BigDecimal skuSellingPrice = sku.getSkuSellingPrice();
            if (min.compareTo(skuSellingPrice) > 0) {
                min = skuSellingPrice;
            }
        }
        return min;
    }
}
