package mf.code.goods.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.goods.service.SellerMarketService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-09-19 11:03 上午
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SellerMarketServiceImpl implements SellerMarketService {
    private final StringRedisTemplate stringRedisTemplate;

    @Override
    public void list() {
        // redis销量排行榜: stringRedisTemplate.opsForZSet().reverseRange(RedisKeyConstant.PRODUCT_SALESVOLUME_RANKING_LIST, 0, -1); -- (0, -1)可处理分页

        // 一级筛选：爆款主推、限时特价、尾货专区、全部商品
        // 二级排序：综合排序、上新排序

        // 爆款主推 -- redis销量排行榜（分页）-- 商品id集合查库       ||  -- 附加其他标签 -- 附加二级排序 -- 附加其他展示信息
        // 限时特价 -- 数据库（查询条件、分页）                      || -- 附加其他标签 -- 附加二级排序 -- 附加其他展示信息
        // 尾货专区 -- 数据库（查询条件、分页）                      || -- 附加其他标签 -- 附加二级排序 -- 附加其他展示信息
        // 全部商品 -- redis销量排行榜（分页）-- 商品id集合查库       ||  -- 附加其他标签 -- 附加二级排序 -- 附加其他展示信息
        // 商品名称或商品id筛选 -- 数据库（查询条件）                 || -- 附加其他标签 -- 附加二级排序 -- 附加其他展示信息
        // （# 问题：无前置标签筛选）商品类目 -- 数据库（查询条件、分页）|| -- 附加其他标签 -- 附加二级排序 -- 附加其他展示信息

        // 标签的今日更新商品数 字段处理 -- 标签的所有商品数查询 -- 入各自标签的redisKey（HshMap<"2019-09-18", 30>）今日-昨日 负数显示0

        // 更新时间 -- redis (获取商品列表页时校验，2小时重置一次)
    }
}
