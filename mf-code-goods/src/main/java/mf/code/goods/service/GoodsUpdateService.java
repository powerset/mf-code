package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductUpdateReqDTO;
import mf.code.goods.repo.po.Product;
import mf.code.shop.constants.AppTypeEnum;

/**
 * @author gel
 */
public interface GoodsUpdateService {

    /**
     * 加入新手专区
     *
     * @param dto
     * @return
     */
    SimpleResponse newbieProduct(ProductUpdateReqDTO dto);

    /**
     * 检查平台商城商品的创建流程
     *
     * @param merchantId
     * @param shopId
     * @param goodsId
     * @param appTypeEnum
     * @return
     */
    Product getProductForPlatMall(Long merchantId, Long shopId, Long goodsId, AppTypeEnum appTypeEnum);
}
