package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.NumberValidationUtil;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.common.constant.ProductGroupTypeEnum;
import mf.code.goods.common.constant.ProductStatusEnum;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.ProductGroupListResultDTO;
import mf.code.goods.dto.ProductGroupReqDTO;
import mf.code.goods.dto.ProductSingleResultDTO;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductGroup;
import mf.code.goods.repo.po.ProductGroupMap;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.repostory.*;
import mf.code.goods.service.ProductGroupService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-16 15:51
 */
@Slf4j
@Service
public class ProductGroupServiceImpl implements ProductGroupService {
    @Autowired
    private ProductGroupRepository productGroupRepository;
    @Autowired
    private ProductGroupMapRepository productGroupMapRepository;
    @Autowired
    private SellerProductRepository sellerProductRepostory;
    @Autowired
    private SellerSkuRepository sellerSkuRepostory;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private AppletProductRepository appletProductRepository;


    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /**
     * 查询分组列表
     *
     * @param shopId
     * @return
     */
    @Override
    public List<ProductGroupListResultDTO> list(Long shopId) {
        List<ProductGroup> productGroups = productGroupRepository.selectByShopIdForSeller(shopId);
        if (CollectionUtils.isEmpty(productGroups)) {
            return new ArrayList<>();
        }
        List<ProductGroupListResultDTO> resultDTOList = new ArrayList<>();
        for (ProductGroup productGroup : productGroups) {
            ProductGroupListResultDTO resultDTO = new ProductGroupListResultDTO();
            resultDTO.setGroupId(productGroup.getId().toString());
            resultDTO.setGroupName(productGroup.getName());
            resultDTO.setGroupOrder(productGroup.getSort().toString());
            resultDTO.setGroupGoodNum(productGroup.getGoodsNum().toString());
            resultDTOList.add(resultDTO);
        }
        return resultDTOList;
    }

    /**
     * 创建分组
     *
     * @param dto
     * @return
     */
    @Override
    public Integer createGroup(ProductGroupReqDTO dto) {
        Assert.notNull(dto.getMerchantId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请重新登录");
        Assert.notNull(dto.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请选择店铺");
        Assert.isBlank(dto.getGroupName(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "请填写分组名称");
        List<ProductGroup> productGroups = productGroupRepository.selectByShopIdForSeller(dto.getShopId());
        if (!CollectionUtils.isEmpty(productGroups)) {
            Assert.isTrue(productGroups.size() < 10, ApiStatusEnum.ERROR_BUS_NO14.getCode(), "只可添加十个商品分组");
            for (ProductGroup productGroup : productGroups) {
                // 校验分组名称重复
                Assert.isTrue(!StringUtils.equalsIgnoreCase(productGroup.getName(), dto.getGroupName()),
                        ApiStatusEnum.ERROR_BUS_NO15.getCode(), "分组名称重复，请更换");
            }
        }
        ProductGroup productGroup = new ProductGroup();
        productGroup.setMerchantId(dto.getMerchantId());
        productGroup.setShopId(dto.getShopId());
        productGroup.setName(dto.getGroupName());
        productGroup.setDel(DelEnum.NO.getCode());
        productGroup.setType(ProductGroupTypeEnum.PRODUCT.getCode());
        productGroup.setSort(0);
        productGroup.setGoodsNum(0);
        Date now = new Date();
        productGroup.setCtime(now);
        productGroup.setUtime(now);
        return productGroupRepository.createSelective(productGroup);
    }

    /**
     * 添加商品到分组
     *
     * @param dto
     * @return
     */
    @Override
    public Integer addGroupGoods(ProductGroupReqDTO dto) {
        Assert.notNull(dto.getMerchantId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请重新登录");
        Assert.notNull(dto.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请选择店铺");
        Assert.notNull(dto.getGroupId(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "请选择待添加商品的分组");
        Assert.notNull(dto.getProductId(), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "请选择要添加的商品");
        ProductGroup productGroup = productGroupRepository.selectById(dto.getGroupId());
        Assert.notNull(productGroup, ApiStatusEnum.ERROR_BUS_NO15.getCode(), "不存在该分组信息");
        Assert.isTrue(productGroup.getShopId().equals(dto.getShopId()), ApiStatusEnum.ERROR_BUS_NO16.getCode(), "不存在该分组信息");
        Product product = sellerProductRepostory.selectById(dto.getProductId());
        Assert.notNull(product, ApiStatusEnum.ERROR_BUS_NO17.getCode(), "不存在该商品");
        Assert.isTrue(product.getShopId().equals(dto.getShopId()), ApiStatusEnum.ERROR_BUS_NO18.getCode(), "不存在该商品");
        Assert.isTrue(product.getStatus() == ProductConstant.Status.ON_SALE, ApiStatusEnum.ERROR_BUS_NO19.getCode(), "上架商品才能保存分组");
        Assert.isTrue(product.getShowType() == ProductShowTypeEnum.NONE.getCode(),
                ApiStatusEnum.ERROR_BUS_NO12.getCode(), "新手专区商品不能加入分组中");
        List<Long> longs = productGroupMapRepository.selectGroupIdListByProductId(dto.getProductId());
        if (!CollectionUtils.isEmpty(longs)) {
            for (Long groupId : longs) {
                if (!groupId.equals(dto.getGroupId())) {
                    continue;
                }
                Assert.isTrue(CollectionUtils.isEmpty(longs), ApiStatusEnum.ERROR_BUS_NO20.getCode(), "您已添加过该商品");
            }
        }
        ProductGroupMap productGroupMap = new ProductGroupMap();
        productGroupMap.setGroupId(productGroup.getId());
        productGroupMap.setProductId(product.getId());
        productGroupMap.setParentProductId(product.getParentId());
        Date now = new Date();
        productGroupMap.setCtime(now);
        productGroupMap.setUtime(now);
        productGroupMap.setDel(DelEnum.NO.getCode());
        Integer count = productGroupMapRepository.createOrUpdateSelective(productGroupMap);
        if (count == 0) {
            return 0;
        }
        // 加入分组需要更新数字加1
        productGroup.setGoodsNum(productGroup.getGoodsNum() + 1);
        return productGroupRepository.updateSelectiveById(productGroup);
    }

    /**
     * 查询分组内的商品
     *
     * @param shopId
     * @param groupId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Map<String, Object> queryGroupGoods(Long shopId, Long groupId, Long pageNum, Long pageSize) {
        Assert.notNull(shopId, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请选择店铺");
        Assert.notNull(groupId, ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请选择分组");
        ProductGroup productGroup = productGroupRepository.selectById(groupId);
        Assert.isTrue(shopId.equals(productGroup.getShopId()), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "您无权操作该分组");
        Map<String, Object> resultMap = new HashMap<>();
        IPage<ProductGroupMap> page = productGroupMapRepository.selectByGroupId(groupId, pageNum, pageSize);
        List<ProductGroupMap> groupMapList = page.getRecords();
        if (CollectionUtils.isEmpty(groupMapList)) {
            resultMap.put("list", new ArrayList<>());
            resultMap.put("total", 0);
            return resultMap;
        }
        List<ProductSingleResultDTO> resultDTOS = new ArrayList<>();
        List<Long> productIdList = new ArrayList<>();
        List<Long> parentIdList = new ArrayList<>();
        for (ProductGroupMap productGroupMap : groupMapList) {
            productIdList.add(productGroupMap.getProductId());
            parentIdList.add(productGroupMap.getParentProductId());
        }
        List<Product> productList = sellerProductRepostory.selectBatchIds(productIdList);
        List<ProductSku> productSkuList = sellerSkuRepostory.selectByProductIdList(parentIdList);
        for (Product product : productList) {
            ProductSingleResultDTO resultDTO = new ProductSingleResultDTO();
            resultDTO.setProductTitle(product.getProductTitle());
            List<String> mainPicList = JSONArray.parseArray(product.getMainPics(), String.class);
            if (!CollectionUtils.isEmpty(mainPicList)) {
                resultDTO.setProductPic(mainPicList.get(0));
            }
            resultDTO.setProductId(product.getId().toString());
            BigDecimal min = new BigDecimal("1000000");
            BigDecimal max = BigDecimal.ZERO;
            for (ProductSku productSku : productSkuList) {
                if (!productSku.getProductId().equals(product.getParentId())) {
                    continue;
                }
                if (productSku.getSkuSellingPrice().compareTo(min) < 0) {
                    min = productSku.getSkuSellingPrice();
                }
                if (productSku.getSkuSellingPrice().compareTo(max) > 0) {
                    max = productSku.getSkuSellingPrice();
                }
            }
            resultDTO.setProductMin(min.toString());
            resultDTO.setProductMax(max.toString());
            resultDTOS.add(resultDTO);
        }
        resultMap.put("list", resultDTOS);
        resultMap.put("total", page.getTotal());
        return resultMap;
    }

    /**
     * 删除分组内的商品
     *
     * @param dto
     * @return
     */
    @Override
    public Integer delGroupGoods(ProductGroupReqDTO dto) {
        Assert.notNull(dto.getMerchantId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请重新登录");
        Assert.notNull(dto.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请选择店铺");
        Long groupId = dto.getGroupId();
        Assert.notNull(groupId, ApiStatusEnum.ERROR_BUS_NO13.getCode(), "请选择待更新的商品分组");
        ProductGroup productGroup = productGroupRepository.selectById(groupId);
        Assert.isTrue(dto.getShopId().equals(productGroup.getShopId()), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "您无权操作该分组");
        Assert.notNull(dto.getProductId(), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "请选择待移除商品");
        Product product = sellerProductRepostory.selectById(dto.getProductId());
        Assert.notNull(product, ApiStatusEnum.ERROR_BUS_NO15.getCode(), "该商品不存在");
        Assert.isTrue(dto.getProductId().equals(product.getId()), ApiStatusEnum.ERROR_BUS_NO16.getCode(), "该商品不存在");
        Integer count = productGroupMapRepository.updateDelByGroupIdAndProductId(groupId, dto.getProductId());
        if (count == 0) {
            return 0;
        }
        // 移除分组需要更新数字减1
        productGroup.setGoodsNum(productGroup.getGoodsNum() - 1);
        if (productGroup.getGoodsNum() < 0) {
            productGroup.setGoodsNum(0);
        }
        return productGroupRepository.updateSelectiveById(productGroup);
    }

    /**
     * 更新分组信息
     *
     * @param dto
     * @return
     */
    @Override
    public Integer updateSelective(ProductGroupReqDTO dto) {
        Assert.notNull(dto.getMerchantId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请重新登录");
        Assert.notNull(dto.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请选择店铺");
        Long groupId = dto.getGroupId();
        Assert.notNull(groupId, ApiStatusEnum.ERROR_BUS_NO13.getCode(), "请选择待更新的商品分组");
        ProductGroup productGroup = productGroupRepository.selectById(groupId);
        Assert.isTrue(dto.getShopId().equals(productGroup.getShopId()), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "您无权操作该分组");
        ProductGroup productGroupUpdate = new ProductGroup();
        BeanUtils.copyProperties(productGroup, productGroupUpdate);
        if (dto.getGroupOrder() != null) {
            Assert.isTrue(dto.getGroupOrder() > 0 && dto.getGroupOrder() < 100,
                    ApiStatusEnum.ERROR_BUS_NO15.getCode(), "请输入正确的排列序号");
            productGroupUpdate.setSort(dto.getGroupOrder());
        }
        if (StringUtils.isNotBlank(dto.getGroupName())) {
            Assert.isTrue(dto.getGroupName().length() <= 6, ApiStatusEnum.ERROR_BUS_NO16.getCode(), "分组名不多于6个汉字");
            List<ProductGroup> productGroups = productGroupRepository.selectByShopIdNameDel(dto.getShopId(), dto.getGroupName());
            Assert.isTrue(CollectionUtils.isEmpty(productGroups), ApiStatusEnum.ERROR_BUS_NO17.getCode(), "请更换不重复的分组名称");
            productGroupUpdate.setName(dto.getGroupName());
        }
        productGroupUpdate.setUtime(new Date());
        return productGroupRepository.updateSelectiveById(productGroupUpdate);
    }

    /**
     * 删除分组
     *
     * @param dto
     * @return
     */
    @Override
    public Integer delGroup(ProductGroupReqDTO dto) {
        Assert.notNull(dto.getMerchantId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请重新登录");
        Assert.notNull(dto.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请选择店铺");
        Long groupId = dto.getGroupId();
        Assert.notNull(groupId, ApiStatusEnum.ERROR_BUS_NO13.getCode(), "请选择待删除商品的分组");
        ProductGroup productGroup = productGroupRepository.selectById(groupId);
        Assert.isTrue(dto.getShopId().equals(productGroup.getShopId()), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "您无权操作该分组");
        Integer count = productGroupRepository.updateDelById(groupId);
        if (count == 0) {
            return 0;
        }
        return productGroupMapRepository.updateDelByGroupId(groupId);
    }

    /***
     * 通过分组id查询商品信息列表
     * @param merchantId
     * @param shopId
     * @param productGroupId 商品分组编号
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse getProductsByGroupId(Long merchantId, Long shopId, Long userId, Long productGroupId, Long offset, Long size) {
        if (shopId == null || shopId <= 0) {
            return null;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("showType", ProductShowTypeEnum.NONE.getCode());
        if (productGroupId > 0) {
            param.put("groupId", productGroupId);
        }
        param.put("offset", offset);
        param.put("size", size);
        List<ProductDistributionDTO> productDistributionDTOList = null;

        if (productGroupId == 0) {
            //查找推荐
            productDistributionDTOList = appletProductRepository.pageWithSalesVolumeDescForListOptimize(param);
        } else {
            //分组
            productDistributionDTOList = appletProductRepository.pageWithSalesVolumeDescByGroupIdForListOptimize(param);
        }
        if (productDistributionDTOList == null) {
            productDistributionDTOList = new ArrayList<>();
        }

        AppletMybatisPageDto<ProductDistributionDTO> distributionProductPage = new AppletMybatisPageDto<>(size, offset);
        if (CollectionUtils.isEmpty(productDistributionDTOList)) {
            log.error("获取商品列表失败");
            Map<String, Object> homePage = new HashMap<>();
            homePage.put("goodsRecommend", distributionProductPage);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, homePage);
        }

        Integer total = 0;
        if (productGroupId == 0) {
            QueryWrapper<Product> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(Product::getShopId, shopId)
                    .eq(Product::getStatus, ProductStatusEnum.ONLINE.getCode())
                    .eq(Product::getDel, DelEnum.NO.getCode())
                    .eq(Product::getShowType, ProductShowTypeEnum.NONE.getCode())
            ;
            total = productMapper.selectCount(wrapper);
        } else {
            QueryWrapper<ProductGroupMap> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ProductGroupMap::getGroupId, productGroupId)
                    .eq(ProductGroupMap::getDel, DelEnum.NO.getCode())
            ;
            total = productGroupMapRepository.countByGroupId(wrapper);
        }

        // 获取 对此用户 的商品返利金额
        AppletUserProductRebateDTO appletUserProductRebateDTO = new AppletUserProductRebateDTO();
        appletUserProductRebateDTO.setShopId(shopId);
        appletUserProductRebateDTO.setOffset(offset);
        appletUserProductRebateDTO.setSize(size);
        appletUserProductRebateDTO.setTotal(total);
        appletUserProductRebateDTO.setProductDistributionDTOList(productDistributionDTOList);
        appletUserProductRebateDTO.setUserId(userId);

        if (userId != null) {
            AppletUserProductRebateDTO productRebate = distributionAppService.queryProductRebateByUserIdShopId(appletUserProductRebateDTO);
            if (productRebate == null) {
                log.error("获取用户的商品返利金额 异常, userId = {}, shopId = {}", userId, shopId);
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "商品正在飞奔出现...请重试");
            }
            appletUserProductRebateDTO = productRebate;
        }



        distributionProductPage.setContent(appletUserProductRebateDTO.getProductDistributionDTOList());
        if (offset + size < appletUserProductRebateDTO.getTotal()) {
            distributionProductPage.setPullDown(true);
        }

        if (!CollectionUtils.isEmpty(appletUserProductRebateDTO.getProductDistributionDTOList())) {
            for (ProductDistributionDTO productDistributionDTO : appletUserProductRebateDTO.getProductDistributionDTOList()) {
                JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                if (mainPics.size() == 0) {
                    continue;
                }
                productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);

                String salesVolume = productDistributionDTO.getSalesVolume();
                int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                String totalSaleStr = NumberValidationUtil.convertNumberForString(totalSale);
                productDistributionDTO.setSalesVolume(totalSaleStr);
            }
        }

        Map<String, Object> homePage = new HashMap<>();
        homePage.put("goodsRecommend", distributionProductPage);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, homePage);
    }
}
