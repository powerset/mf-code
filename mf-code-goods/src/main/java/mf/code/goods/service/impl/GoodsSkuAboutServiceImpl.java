package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.common.DelEnum;
import mf.code.common.utils.Assert;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.goods.repo.dao.ProductSpecsMapper;
import mf.code.goods.repo.dao.ProductSpecsNameMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.po.ProductSpecs;
import mf.code.goods.repo.po.ProductSpecsName;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.GoodsSkuAboutService;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月08日 17:55
 */
@Service
public class GoodsSkuAboutServiceImpl implements GoodsSkuAboutService {
    @Autowired
    private ProductSpecsMapper productSpecsMapper;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private ProductSpecsNameMapper productSpecsNameMapper;
    @Autowired
    private GoodsService goodsService;

    @Override
    public List<Map> getSkuItemInfo(List<Long> catIds, List<String> specsValues) {
        List<Map> list = new ArrayList<>();
        if (CollectionUtils.isEmpty(catIds) || CollectionUtils.isEmpty(specsValues)) {
            return new ArrayList<>();
        }

        //商品sku类目
        QueryWrapper<ProductSpecsName> specsNameQueryWrapper = new QueryWrapper<>();
        specsNameQueryWrapper.lambda()
                .in(ProductSpecsName::getId, catIds)
        ;
        List<ProductSpecsName> catProSpecs = this.productSpecsNameMapper.selectList(specsNameQueryWrapper);

        //商品sku下的属性
        QueryWrapper<ProductSpecs> specsQueryWrapper = new QueryWrapper<>();
        specsQueryWrapper.lambda()
                .eq(ProductSpecs::getDel, DelEnum.NO.getCode())
                .in(ProductSpecs::getId, specsValues)
        ;
        List<ProductSpecs> specsProSpecs = this.productSpecsMapper.selectList(specsQueryWrapper);
        if (CollectionUtils.isEmpty(specsProSpecs) || CollectionUtils.isEmpty(catProSpecs)) {
            return new ArrayList<>();
        }

        Map<Long, ProductSpecsName> catIdSortMap = new HashMap<>();
        for (ProductSpecsName productSpecsName : catProSpecs) {
            catIdSortMap.put(productSpecsName.getId(), productSpecsName);
        }
        List<ProductSpecsName> catIdSortList = new ArrayList<>();
        for (Long catId : catIds) {
            ProductSpecsName productSpecsName = catIdSortMap.get(catId);
            if (productSpecsName != null) {
                catIdSortList.add(productSpecsName);
            }
        }

        Map<Long, List<ProductSpecs>> specsMap = new HashMap<>();
        for (ProductSpecs productSpecs : specsProSpecs) {
            List<ProductSpecs> specs = specsMap.get(productSpecs.getSpecsNameId());
            if (CollectionUtils.isEmpty(specs)) {
                specs = new ArrayList<>();
            }
            specs.add(productSpecs);
            specsMap.put(productSpecs.getSpecsNameId(), specs);
        }

        for (ProductSpecsName productSpecsNames : catIdSortList) {
            Map<String, Object> itemMap = new HashMap<>();
            itemMap.put("id", productSpecsNames.getId());
            itemMap.put("name", productSpecsNames.getName());
            itemMap.put("details", new ArrayList<>());
            List<ProductSpecs> specs = specsMap.get(productSpecsNames.getId());
            if (!CollectionUtils.isEmpty(specs)) {
                List<Map> list1 = new ArrayList<>();
                for (ProductSpecs productSpecs1 : specs) {
                    Map<String, Object> specMap = new HashMap<>();
                    specMap.put("id", productSpecs1.getId());
                    specMap.put("name", productSpecs1.getSpecsValue());
                    list1.add(specMap);
                }
                itemMap.put("details", list1);
            }
            list.add(itemMap);
        }
        return list;
    }

    @Override
    public List<Map> getSkuInfo(Product product, List<ProductSku> productSkus, ProductDistributionDTO distributionDTO, AppTypeEnum appTypeEnum) {
        List<Map> list = new ArrayList<>();
        AppletUserProductRebateDTO appletUserProductRebateDTO = new AppletUserProductRebateDTO();
        appletUserProductRebateDTO.setProductDistributionDTOList(new ArrayList<>());
        appletUserProductRebateDTO.setUserId(distributionDTO.getUserId());
        appletUserProductRebateDTO.setShopId(distributionDTO.getShopId());

        List<DistributeInfoEntity> distributeInfoEntities = new ArrayList<>();
        for (ProductSku productSku : productSkus) {
            ProductDistributionDTO distributionDTO1 = new ProductDistributionDTO();
            BeanUtils.copyProperties(distributionDTO, distributionDTO1);
            distributionDTO1.setProductId(productSku.getId());
            distributionDTO1.setPrice(productSku.getSkuSellingPrice());
            appletUserProductRebateDTO.getProductDistributionDTOList().add(distributionDTO1);

            DistributeInfoEntity distributeInfoEntity = new DistributeInfoEntity();
            distributeInfoEntity.setPrice(productSku.getSkuSellingPrice());
            distributeInfoEntity.setProductSpec(product.getProductSpecs());
            distributeInfoEntity.setSelfSupportRadio(product.getSelfSupportRatio());
            distributeInfoEntity.setPlatSupportRadio(product.getPlatSupportRatio());
            distributeInfoEntities.add(distributeInfoEntity);
        }
        List<ProductDistributCommissionEntity> distrIncodeByProductBatch = new ArrayList<>();
        if (AppTypeEnum.DDD == appTypeEnum) {
            distrIncodeByProductBatch = distributionAppService.getDistrIncodeByProductBatch(distributeInfoEntities);
        }
        Map<Long, BigDecimal> skuMapRebate = new HashMap<>();
        if (!CollectionUtils.isEmpty(appletUserProductRebateDTO.getProductDistributionDTOList()) && distributionDTO.getUserId() != null) {
            appletUserProductRebateDTO = this.distributionAppService.queryProductRebateByUserIdShopId(appletUserProductRebateDTO);
        }
        if (appletUserProductRebateDTO != null && !CollectionUtils.isEmpty(appletUserProductRebateDTO.getProductDistributionDTOList())) {
            for (ProductDistributionDTO productDistributionDTO : appletUserProductRebateDTO.getProductDistributionDTOList()) {
                skuMapRebate.put(productDistributionDTO.getProductId(), productDistributionDTO.getRebate());
            }
        }

        for (int j = 0; j < productSkus.size(); j++) {
            ProductSku productSku = productSkus.get(j);
            String mapKey = "";
            //1:3,2:5
            String[] split = productSku.getSpecSort().split(",");
            for (int i = 0; i < split.length; i++) {
                String[] split1 = split[i].split(":");
                //sku类目下的属性拼接
                if (StringUtils.isNotBlank(mapKey)) {
                    mapKey = mapKey + "-" + split1[1];
                } else {
                    mapKey = mapKey + split1[1];
                }
            }
            Map<String, Map> skuMap = new HashMap<>();
            Map skuInfo = new HashMap();
            skuInfo.put("price", productSku.getSkuSellingPrice());
            skuInfo.put("originalPrice", productSku.getSkuSellingPrice());
            skuInfo.put("stock", productSku.getStock());
            if (StringUtils.isNotBlank(productSku.getSkuPic())) {
                skuInfo.put("goodsPic", productSku.getSkuPic());
            } else {
                //取商品主图的第一张
                List<String> strPics = JSONObject.parseArray(product.getMainPics(), String.class);
                skuInfo.put("goodsPic", strPics.get(0));
            }
            skuInfo.put("skuId", productSku.getId());
            //0 显示(默认显示)/ 1 不显示
            skuInfo.put("notShow", productSku.getNotShow());


            skuInfo.put("cashback", BigDecimal.ZERO);
            BigDecimal bigDecimal = skuMapRebate.get(productSku.getId());
            if (bigDecimal != null) {
                skuInfo.put("cashback", bigDecimal);
            }

            skuInfo.put("commissionNum", CollectionUtils.isEmpty(distrIncodeByProductBatch) || distrIncodeByProductBatch.get(j) == null || distrIncodeByProductBatch.get(j).getPlatDistributCommission() == null ?
                    BigDecimal.ZERO : distrIncodeByProductBatch.get(j).getPlatDistributCommission());

            skuMap.put(mapKey, skuInfo);
            list.add(skuMap);
        }
        return list;
    }

    /***
     * 类目下属性值
     * @param specsValues
     * @return
     */
    @Override
    public List<Map> getSkuSpecs(List<Long> specsValues) {
        QueryWrapper<ProductSpecs> specsQueryWrapper = new QueryWrapper<>();
        specsQueryWrapper = new QueryWrapper<>();
        specsQueryWrapper.lambda()
                .eq(ProductSpecs::getDel, DelEnum.NO.getCode())
                .in(ProductSpecs::getId, specsValues)
        ;
        //商品sku类目
        List<ProductSpecs> specsProSpecs = this.productSpecsMapper.selectList(specsQueryWrapper);
        if (CollectionUtils.isEmpty(specsProSpecs)) {
            return new ArrayList<>();
        }
        List<Map> list = new ArrayList<>();
        for (ProductSpecs productSpecs : specsProSpecs) {
            Map<String, Object> specMap = new HashMap<>();
            specMap.put("id", productSpecs.getId());
            specMap.put("name", productSpecs.getSpecsValue());
            list.add(specMap);
        }
        return list;
    }

    @Override
    public Map<Long, String> getSkuSalePropMap() {
        List<Map<String, Object>> specDic = productSpecsMapper.findAll();
        Map<Long, String> mapDic = new HashMap<>();
        for (Map<String, Object> m : specDic) {
            Object specId = m.get("specId");
            Object specsValue = m.get("specsValue");
            if (specId == null || specsValue == null) {
                Assert.isTrue(false, -1, "解析销售属性异常");
            }
            Assert.isInteger(specId.toString(), -1, "解析销售属性异常");
            mapDic.put(Long.parseLong(specId.toString()), specsValue.toString());
        }
        return mapDic;
    }
}
