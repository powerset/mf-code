package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.goods.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月17日 16:47
 */
public interface ActivityProductSkuService {

    /***
     * 活动-商品-详情页
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param skuId
     * @return
     */
    SimpleResponse getDiscountGoodsDetail(Long merchantId,
                                          Long shopId,
                                          Long userId,
                                          Long skuId);
}
