package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.dto.CategoryDistributionDTO;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.repostory.CategoryTaobaoRepository;
import mf.code.goods.service.CategoryGroupMappingService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-06-24 17:43
 */
@Slf4j
@Service
public class CategoryGroupMappingServiceImpl implements CategoryGroupMappingService {

    @Autowired
    private CategoryTaobaoRepository categoryTaobaoRepository;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 根据0级新类目sid查询1级类目
     *
     * @param parentSid
     * @return
     */
    @Override
    public List<String> queryFirstLevelByParentSid(String parentSid) {

        List<String> categoryOneList = new ArrayList<>();
        String redisKey = RedisKeyConstant.PRODUCT_CATEGORY_ONEBYFIRSTLEVEL + parentSid;
        // 优先读缓存
        String list = stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isNotBlank(list)) {
            List<CategoryDistributionDTO> categoryDistributionDTOS = JSON.parseArray(list, CategoryDistributionDTO.class);
            if (CollectionUtils.isEmpty(categoryDistributionDTOS)) {
                return categoryOneList;
            }
            for (CategoryDistributionDTO categoryDistributionDTO : categoryDistributionDTOS) {
                categoryOneList.add(categoryDistributionDTO.getSid());
            }
            return categoryOneList;
        }
        // 查询并设置缓存
        List<CategoryTaobao> categoryTaobaos = categoryTaobaoRepository.selectNextLevelByParentSid(parentSid, 1);
        // 如果数据库无法查询到数据，依然存redis10秒
        if (CollectionUtils.isEmpty(categoryTaobaos)) {
            log.error("当前类目：" + parentSid + "，数据库中并不存在");
            stringRedisTemplate.opsForValue().set(redisKey, "[]", 10, TimeUnit.MINUTES);
            return categoryOneList;
        }
        List<CategoryDistributionDTO> categoryDistributionDTOList = new ArrayList<>();
        for (CategoryTaobao categoryTaobao : categoryTaobaos) {
            CategoryDistributionDTO categoryDistributionDTO = new CategoryDistributionDTO();
            categoryDistributionDTO.setLevel(categoryTaobao.getLevel());
            categoryDistributionDTO.setName(categoryTaobao.getName());
            categoryDistributionDTO.setSid(categoryTaobao.getSid());
            categoryDistributionDTO.setParentId(categoryDistributionDTO.getParentId());
            categoryDistributionDTOList.add(categoryDistributionDTO);
            categoryOneList.add(categoryTaobao.getSid());
        }
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(categoryDistributionDTOList), 10, TimeUnit.MINUTES);
        return categoryOneList;

    }

    /**
     * 查询所有类目id
     *
     * @return
     */
    @Override
    public List<CategoryDistributionDTO> queryAllZeroLevel() {
        List<CategoryDistributionDTO> categoryDistributionDTOList = new ArrayList<>();
        String list = stringRedisTemplate.opsForValue().get(RedisKeyConstant.PRODUCT_CATEGORY_ZERO);
        if (StringUtils.isNotBlank(list)) {
            categoryDistributionDTOList = JSON.parseArray(list, CategoryDistributionDTO.class);
            return categoryDistributionDTOList;
        }
        // 查询并设置缓存
        List<CategoryTaobao> categoryTaobaos = categoryTaobaoRepository.selectByLevel(0);
        if (CollectionUtils.isEmpty(categoryTaobaos)) {
            // 这里应该并没有错位，否则是部署流程问题
            return categoryDistributionDTOList;
        }
        for (CategoryTaobao categoryTaobao : categoryTaobaos) {
            CategoryDistributionDTO categoryDistributionDTO = new CategoryDistributionDTO();
            categoryDistributionDTO.setSort(categoryTaobao.getSort());
            categoryDistributionDTO.setSid(categoryTaobao.getSid());
            categoryDistributionDTO.setParentId(categoryTaobao.getParentId());
            categoryDistributionDTO.setName(categoryTaobao.getName());
            categoryDistributionDTO.setLevel(categoryTaobao.getLevel());
            categoryDistributionDTO.setDistributionRatio(categoryTaobao.getDistributionRatio());
            categoryDistributionDTOList.add(categoryDistributionDTO);
        }
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.PRODUCT_CATEGORY_ZERO, JSON.toJSONString(categoryDistributionDTOList), 10, TimeUnit.MINUTES);
        return categoryDistributionDTOList;
    }

    /**
     * 通过一级类目sid查询零级类目
     *
     * @param oneSid
     * @return
     */
    @Override
    public CategoryDistributionDTO queryZeroLevelByFirst(String oneSid) {
        CategoryTaobao oneCategory = categoryTaobaoRepository.selectBySid(oneSid, 1);
        if (oneCategory == null) {
            return null;
        }
        CategoryTaobao categoryTaobao = categoryTaobaoRepository.selectBySid(oneCategory.getParentId(), 0);
        CategoryDistributionDTO categoryDistributionDTO = new CategoryDistributionDTO();
        BeanUtils.copyProperties(categoryTaobao, categoryDistributionDTO);
        return categoryDistributionDTO;
    }

    /**
     * 修改零级类目抽佣比例和顺序
     *
     * @param zeroSid
     * @param ratio
     * @param sort
     * @return
     */
    @Override
    public CategoryDistributionDTO updateZeroLevelRatioAndSort(String zeroSid, BigDecimal ratio, Integer sort) {
        if (StringUtils.isBlank(zeroSid) || ratio == null) {
            return null;
        }
        // 查询零级类目
        CategoryTaobao categoryTaobao = categoryTaobaoRepository.selectBySid(zeroSid, 0);
        if (categoryTaobao == null) {
            return null;
        }
        if (ratio.intValue() < 0 || ratio.intValue() > 100) {
            return null;
        }
        categoryTaobao.setDistributionRatio(ratio);
        if (sort != null) {
            categoryTaobao.setSort(sort);
        }
        categoryTaobao.setUtime(new Date());
        Integer count = categoryTaobaoRepository.updateById(categoryTaobao);
        if (count != 0) {
            // 如果更新成功，则删除缓存
            stringRedisTemplate.delete(RedisKeyConstant.PRODUCT_CATEGORY_ZERO);
        }
        CategoryDistributionDTO categoryDistributionDTO = new CategoryDistributionDTO();
        BeanUtils.copyProperties(categoryTaobao, categoryDistributionDTO);
        return categoryDistributionDTO;
    }
}
