package mf.code.goods.service.impl;/**
 * create by qc on 2019/4/18 0018
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.repo.dao.ProductStockLogMapper;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.po.ProductStockLog;
import mf.code.goods.service.GoodsSkuLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 库存日志操作记录
 *
 * @author gbf
 * 2019/4/18 0018、16:42
 */
@Service
@Slf4j
public class GoodsSkuLogServiceImpl implements GoodsSkuLogService {
    @Autowired
    private ProductStockLogMapper productStockLogMapper;

    /**
     * c端写操作记录
     *
     * @param productSku  sku对象
     * @param stockNumber 修改库存的数量
     * @param isConsume   true:消费库存 false:腿库存
     * @param type        类型 类型 1:商户修改库存 2:消费库存 3:退换库存
     * @param orderId     订单id
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertLogForClient(ProductSku productSku, int stockNumber, boolean isConsume, int type, Long orderId) {
        ProductStockLog stockLog = this.setDataFromProductSku(productSku);
        //数量
        stockLog.setStockNumber(isConsume ? (-1 * stockNumber) : stockNumber);
        //类型 类型 1:商户修改库存 2:消费库存 3:退换库存'
        stockLog.setType(type);
        //订单号
        stockLog.setOrderId(orderId);
        //修改时间
        stockLog.setCtime(new Date());
        this.insert(stockLog);
    }

    /**
     * 商户写库存操作记录
     *
     * @param productSku
     * @param stockNumber
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertLogForMerchant(ProductSku productSku, int stockNumber) {
        ProductStockLog stockLog = this.setDataFromProductSku(productSku);
        stockLog.setOrderId(null);
        stockLog.setStockNumber(stockNumber);
        stockLog.setType(ProductConstant.ProductSku.Stock.ProductSkuStockLog.MERCHANT);
        stockLog.setCtime(new Date());
        this.insert(stockLog);
    }

    /**
     * 库存变更前20条
     *
     * @param conditionMap
     * @return
     */
    @Override
    public SimpleResponse topLogs(Map<String, Object> conditionMap) {
        List<ProductStockLog> top20 = productStockLogMapper.findTop20(conditionMap);
        return new SimpleResponse(0, "success", top20);
    }

    /**
     * 创建bean
     *
     * @param productSku
     * @return
     */
    private ProductStockLog setDataFromProductSku(ProductSku productSku) {
        ProductStockLog productStockLog = new ProductStockLog();
        //商户
        productStockLog.setMerchantId(productSku.getMerchantId());
        //店铺
        productStockLog.setShopId(productSku.getShopId());
        //商品
        productStockLog.setProductId(productSku.getProductId());
        //sku
        productStockLog.setSkuId(productSku.getId());
        return productStockLog;
    }

    /**
     * 写操作
     *
     * @param stockLog
     */
    private void insert(ProductStockLog stockLog) {
        int result = productStockLogMapper.insert(stockLog);
        if (result != 1) {
            log.error("写库存操作记录失败");
        }
    }
}
