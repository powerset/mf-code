package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.PlatformDistAuditReqDTO;
import mf.code.goods.dto.PlatformDistListReqDTO;

import java.util.List;

/**
 * mf.code.goods.service
 * Description:
 *
 * @author gel
 * @date 2019-05-07 19:36
 */
public interface PlatformDistAuditService {

    /**
     * 平台运营-商品审核列表
     */
    SimpleResponse checkList(PlatformDistListReqDTO reqDTO);

    /**
     * 详情
     *
     * @param productId
     * @param auditId
     * @return
     */
    SimpleResponse goodsDetail(Long productId, Long auditId);

    /**
     * 审核查看不通过理由
     *
     * @param productId
     * @param auditId
     * @return
     */
    SimpleResponse getCheckReason(Long productId, Long auditId);

    /**
     * 审核
     *
     * @param reqDTO
     * @param checkReasonPicList
     * @return
     */
    SimpleResponse audit(PlatformDistAuditReqDTO reqDTO, List<String> checkReasonPicList);
}
