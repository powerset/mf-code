package mf.code.goods.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.goods.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月16日 13:58
 */
public interface GoodsPosterService {
    SimpleResponse sharePoster(Long merchantID, Long shopID, Long goodsId, Long userID, Long fromID);
}
