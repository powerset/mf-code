package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import mf.code.common.dto.MybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.SellerAwardGoodsDTO;
import mf.code.goods.repo.po.Product;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.SellerAwardGoodsV7Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年05月27日 16:02
 */
@Service
public class SellerAwardGoodsV7ServiceImpl implements SellerAwardGoodsV7Service {
    @Autowired
    private GoodsService goodsService;

    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /***
     * 获取奖品集合,优化查询
     * @param merchantId
     * @param shopId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public SimpleResponse listForListOptimize(Long merchantId, Long shopId, int pageNum, int pageSize) {
        Assert.notNull(merchantId, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        Assert.notNull(shopId, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");

        MybatisPageDto<SellerAwardGoodsDTO> awardGoodsMybatisPage = new MybatisPageDto<>();
        int total = goodsService.countAwardListForListOptimize(shopId);
        // 判断总条数是否满足查询条件，是否有查询列表的必要
        if (total - (pageNum - 1) * pageSize < 0) {
            SimpleResponse simpleResponse = new SimpleResponse();
            awardGoodsMybatisPage.from(pageNum, pageSize, total);
            simpleResponse.setData(awardGoodsMybatisPage);
            return simpleResponse;
        }
        List<Product> productList = goodsService.pageAwardListForListOptimize(shopId, pageNum, pageSize);
        if (CollectionUtils.isEmpty(productList)) {
            SimpleResponse simpleResponse = new SimpleResponse();
            awardGoodsMybatisPage.from(pageNum, pageSize, total);
            simpleResponse.setData(awardGoodsMybatisPage);
            return simpleResponse;
        }

        awardGoodsMybatisPage.setContent(new ArrayList<>());
        for (Product product : productList) {
            SellerAwardGoodsDTO sellerAwardGoodsDTO = new SellerAwardGoodsDTO();
            sellerAwardGoodsDTO.setDisplayGoodsName(product.getProductTitle());
            sellerAwardGoodsDTO.setId(product.getId());
            sellerAwardGoodsDTO.setParentId(product.getParentId());
            sellerAwardGoodsDTO.setDisplayMaxPrice(product.getProductPriceMax().toString());
            sellerAwardGoodsDTO.setDisplayMinPrice(product.getProductPriceMin().toString());
            sellerAwardGoodsDTO.setDisplayPrice(product.getProductPriceMin() + "-" + product.getProductPriceMax());
            JSONArray mainPics = JSON.parseArray(product.getMainPics());
            if (CollectionUtils.isEmpty(mainPics)) {
                sellerAwardGoodsDTO.setDisplayBanner("");
            } else {
                sellerAwardGoodsDTO.setDisplayBanner(mainPics.getString(0) + compressionRatioPic);
            }
            awardGoodsMybatisPage.getContent().add(sellerAwardGoodsDTO);
        }
        awardGoodsMybatisPage.from(pageNum, pageSize, total);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(awardGoodsMybatisPage);
        return simpleResponse;
    }
}
