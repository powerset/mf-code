package mf.code.goods.service;

import mf.code.goods.api.seller.dto.ProductSkuSpec;

import java.util.List;
import java.util.Map;

/**
 * create by qc on 2019/4/20 0020
 */
public interface ProductSkuSpecService {

    List<ProductSkuSpec> parseProductSkuSpecs(String specsStr, Map<Long, String> dic);

    ProductSkuSpec parseProductSkuSpec(String specStr, Map<Long, String> dic);
}
