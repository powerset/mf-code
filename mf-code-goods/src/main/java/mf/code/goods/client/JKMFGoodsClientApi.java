package mf.code.goods.client;/**
 * create by qc on 2019/9/21 0021
 */

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.goods.repo.dto.GoodsListPageDTO;
import mf.code.goods.service.JKMFGoodsClientService;
import org.springframework.web.bind.annotation.*;

/**
 * 为全网通的商品页面提供服务
 *
 * @author gbf
 * 2019/9/21 0021、11:26
 */
@RestController
// 请求路径的格式不能动,这是在st工程中统一的,v1就是在这里,你没看错,我也没写错.
@RequestMapping("/feignapi/v1/goods")
@Slf4j
@RequiredArgsConstructor
public class JKMFGoodsClientApi {
    private final JKMFGoodsClientService jkmfGoodsClientService;

    /**
     * 商品列表
     * <p>
     * 维度类型 1:供应商 2:商品
     * 标签类型 1:爆款 2:特价 3:尾货 4:全部
     * 查询关键字
     * 商品类目
     * 排序 1:综合排序(销量) 2:上新时间
     * 当前页
     * 每页数
     *
     * @return json
     */
    @PostMapping("goodsList")
    public String goodsList(@RequestBody GoodsListPageDTO goodsListPageDTO) {
        return jkmfGoodsClientService.goodsList(goodsListPageDTO);
    }

    /**
     * 获取商品详情
     *
     * @param productId 商品id
     * @return
     */
    @GetMapping("detail")
    public String detail(@RequestParam("productId") Long productId) {
        return jkmfGoodsClientService.detail(productId);
    }


    /**
     * 获取商品类目
     */
    @GetMapping("getSpecs")
    public String getSpecs() {
        return jkmfGoodsClientService.getSpecs();
    }

}
