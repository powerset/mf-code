package mf.code.goods.domain.aggregateroot;

import lombok.Data;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductPromotion;
import mf.code.goods.repo.po.ProductSku;

import java.util.List;
import java.util.Map;

/**
 * 推广活动相关的产品根
 *
 * mf.code.goods.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 10:31
 */
@Data
public class PromotionProduct {
	/**
	 * 产品id
	 */
	private Long id;
	/**
	 * 产品详情
	 */
	private Product productInfo;
	/**
	 * 产品sku Map<productId, List<productSku>> -- 非快照，一个产品可有多个sku
	 */
	private Map<Long, List<ProductSku>> productSkuMap;
	/**
	 * 产品活动 Map<productId, List<productSku>> -- 一个产品可参与不同的产品活动
	 */
	private Map<Long, List<ProductPromotion>> productPromotionMap;
}
