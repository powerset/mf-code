package mf.code.goods.domain.valueobject;/**
 * create by qc on 2019/4/19 0019
 */

import mf.code.goods.repo.po.ProductSku;

/**
 * @author gbf
 * 2019/4/19 0019、17:55
 */
public class ProductSkuRoot {

    public ProductSku beanForStockLog(Long merchantId, Long shopId, Long productId, Long skuId) {
        ProductSku sku = new ProductSku();
        sku.setId(skuId);
        sku.setMerchantId(merchantId);
        sku.setShopId(shopId);
        sku.setProductId(productId);
        return sku;
    }
}
