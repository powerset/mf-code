package mf.code.goods.api.seller.dto;/**
 * create by qc on 2019/4/9 0009
 */

import lombok.Data;

@Data
public class ProductSkuSpec {
    private String parentId;
    private String specId;
    private String parentName;
    private String specName;
}
