package mf.code.goods.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.goods.api.feignclient.CommentAppService;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CommentAppFeignFallbackFactory implements FallbackFactory<CommentAppService> {
    @Override
    public CommentAppService create(Throwable throwable) {
        return new CommentAppService(){
            @Override
            public Map queryProductCommentPage(Long goodsId, Long shopId, boolean goodsParentId) {
                return null;
            }
        };
    }
}
