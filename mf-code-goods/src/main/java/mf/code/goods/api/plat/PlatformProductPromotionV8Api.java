package mf.code.goods.api.plat;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.service.PlatformProductPromotionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 运营平台 商品推广活动类 api
 * 如：本周特推、6.18大促 etc.
 *
 * mf.code.goods.api.plat
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 12:50
 */
@Slf4j
@RestController
@RequestMapping("/api/goods/platform/v8/promotion")
public class PlatformProductPromotionV8Api {
	@Autowired
	private PlatformProductPromotionService platformProductPromotionService;

	/**
	 * 本周特推 -- 加入特推管理
	 *
	 * @param productId
	 * @return
	 */
	@PostMapping("/addHighlyRecommend")
	public SimpleResponse addHighlyRecommend(@RequestParam("productId") String productId) {
		if (StringUtils.isNotBlank(productId) || !RegexUtils.StringIsNumber(productId)) {
			log.error("参数异常, productId = {}", productId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
		}

		return platformProductPromotionService.addHighlyRecommend(Long.valueOf(productId));
	}

}
