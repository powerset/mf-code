package mf.code.goods.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.order.dto.OrderResp;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@Component
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public int countGoodsNumByOrder(Long goodsId) {
                return 0;
            }

            @Override
            public OrderResp queryOrder(Long orderId) {
                return null;
            }

            @Override
            public int summaryOrderByShopIdAndProductId(Long shopId, List<Long> skuIdList) {
                return 0;
            }

            @Override
            public List<OrderResp> queryOrderSettledByUserIdShopId(Long userId, Long shopId) {
                return null;
            }

            @Override
            public Map<String, OrderResp> queryOrderRecordThisMonth(Long userId, Long shopId) {
                return null;
            }

            @Override
            public Map<Long, Integer> countGoodsNumByProductIds(List<Long> productIds) {
                return null;
            }

            @Override
            public Integer queryOrderNumByProductIds(Long shopId, Long userId, List<Long> productIds) {
                return null;
            }
        };
    }
}
