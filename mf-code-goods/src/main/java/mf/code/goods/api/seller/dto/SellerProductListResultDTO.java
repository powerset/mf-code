package mf.code.goods.api.seller.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.goods.api.seller.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-12 10:28
 */
@Data
public class SellerProductListResultDTO {

    /**
     * 商品id
     */
    private Long productId;
    /**
     * 商品来源id
     */
    private Long parentId;
    /**
     * 商品主图
     */
    private String productPic;
    /**
     * 商品标题
     */
    private String productTitle;
    /**
     * 售价最小值
     */
    private BigDecimal priceMin;
    /**
     * 售价最大值
     */
    private BigDecimal priceMax;
    /**
     * 类目id
     */
    private Long category;

    /**
     * 类目名称
     */
    private String categoryName;
    /**
     * 上架状态
     */
    private Integer status;
    /**
     * 审核状态
     */
    private Integer checkStatus;
    /**
     * 审核不通过原因
     */
    private String checkReason;
    /**
     * 审核不通过图片
     */
    private List<String> checkReasonPic;
    /**
     * 剩余库存
     */
    private Integer stockTotal;

    /**
     * 销量
     */
    private Integer salesVolume;


    /**
     * 推广量
     */
    private Integer distributionTotal;

    /**
     * 分销方式
     */
    private Integer distributionMode;

    /**
     * 是否自营 1:是 0:否
     */
    private Integer isSelfSupport;

    /**
     * 是否平台分销 1:是 0:否
     */
    private Integer isPlatSupport;

    /**
     * 店铺分销比例
     */
    private BigDecimal shopDistributionCommissionRadio;

    /**
     * 平台分销比例
     */
    private BigDecimal platDistributionCommissionRadio;

    /**
     * 店铺分销比例
     */
    private BigDecimal selfSupportRadio;

    /**
     * 平台分销比例
     */
    private BigDecimal platSupportRadio;

    /**
     * 平台分销状态（0：审核中，1：分销中，-1：审核不通过）
     */
    private Integer auditStatus;

    /**
     * 来源 1:自营 2:平台
     */
    private Integer origin;

    /**
     * -1:供应商下架 1:在售 2:审核中 3 仓库中4:不通过
     */
    private Integer statusFromShow;
    /**
     * 显示类型：0无；1新手专区
     */
    private Integer showType;

    /**
     * 操作按钮控制
     */
    private SellerProductOperateDTO operate;

    /**
     * 佣金
     */
    private BigDecimal commissionNum;
}
