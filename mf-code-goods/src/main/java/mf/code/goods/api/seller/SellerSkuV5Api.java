package mf.code.goods.api.seller;/**
 * create by qc on 2019/4/9 0009
 */

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductSkuDTO;
import mf.code.goods.service.GoodsSkuLogService;
import mf.code.goods.service.GoodsSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gbf
 * 2019/4/9 0009、18:13
 */
@RestController
@RequestMapping("/api/goods/seller/v5/goods/sku")
public class SellerSkuV5Api {
    @Autowired
    private GoodsSkuService goodsSkuService;
    @Autowired
    private GoodsSkuLogService goodsSkuLogService;

    /**
     * 改价
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "updateprice")
    public SimpleResponse updatePrice(@RequestBody ProductSkuDTO dto) {
        Long skuId = dto.getSkuId();
        BigDecimal sellingPrice = dto.getSkuSellingPrice();
        if (sellingPrice == null || sellingPrice.compareTo(BigDecimal.ZERO) < 0) {
            return new SimpleResponse(3, "售价不能小于0");
        }
        return goodsSkuService.updatePrice(skuId, sellingPrice);
    }

    /**
     * 商户该库存
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "updatestock")
    public SimpleResponse updateStock(@RequestBody ProductSkuDTO dto) {
        Integer skuStock = dto.getStock();
        if (skuStock < 0) {
            return new SimpleResponse(3, "库存数不能小于0");
        }
        return goodsSkuService.updateStock(dto.getSkuId(), skuStock);
    }

    /**
     * 操作记录
     *
     * @param shopId
     * @param productId
     * @param skuId
     * @return
     */
    @GetMapping(value = "log")
    public SimpleResponse log(@RequestParam(value = "shopId") Long shopId,
                              @RequestParam(value = "productId") Long productId, @RequestParam(value = "skuId") Long skuId) {
        Map<String, Object> conditionMap = new HashMap<>();
        conditionMap.put("shopId", shopId);
        conditionMap.put("productId", productId);
        conditionMap.put("skuId", skuId);
        conditionMap.put("topNum", 20);
        return goodsSkuLogService.topLogs(conditionMap);
    }

}
