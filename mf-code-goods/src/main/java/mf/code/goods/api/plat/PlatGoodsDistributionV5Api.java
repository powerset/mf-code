package mf.code.goods.api.plat;/**
 * create by qc on 2019/5/6 0006
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.constant.DistributionAuditStatusEnum;
import mf.code.goods.dto.PlatformDistAuditReqDTO;
import mf.code.goods.dto.PlatformDistListReqDTO;
import mf.code.goods.service.PlatformDistAuditService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 分销平台接口
 *
 * @author gel
 * 2019年5月7日 17:51:25
 */
@RestController
@RequestMapping("/api/goods/platform/v5/goods/plat/distribution")
@Slf4j
public class PlatGoodsDistributionV5Api {
    @Autowired
    private PlatformDistAuditService platformDistAuditService;

    /**
     * 平台运营-商品审核列表
     *
     * @param merchant        商家账号
     * @param shopName        店铺名称
     * @param submitTimeBegin 开始时间
     * @param submitTimeEnd   结束时间
     * @param checkResult     审核状态0:全部 1:审核通过 2:审核不通过
     * @param type            类型 1:未审核 2:已审核
     * @param pageCurrent     当前页
     * @param pageSize        每页数据条数
     * @return
     */
    @GetMapping(value = "checkList")
    public SimpleResponse checkList(@RequestParam(value = "merchant", required = false) String merchant,
                                    @RequestParam(value = "shopName", required = false) String shopName,
                                    @RequestParam(value = "submitTimeBegin", required = false) String submitTimeBegin,
                                    @RequestParam(value = "submitTimeEnd", required = false) String submitTimeEnd,
                                    @RequestParam(value = "checkResult", required = false) Integer checkResult,
                                    @RequestParam(value = "type", required = false) Integer type,
                                    @RequestParam(value = "pageCurrent", defaultValue = "1") Integer pageCurrent,
                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        PlatformDistListReqDTO reqDTO = new PlatformDistListReqDTO();
        reqDTO.setMerchant(merchant);
        reqDTO.setShopName(shopName);
        reqDTO.setSubmitTimeBegin(submitTimeBegin);
        reqDTO.setSubmitTimeEnd(submitTimeEnd);
        reqDTO.setCheckResult(checkResult);
        reqDTO.setType(type);
        reqDTO.setPage(pageCurrent);
        reqDTO.setSize(pageSize);
        return platformDistAuditService.checkList(reqDTO);
    }

    /**
     * 平台运营-商品详情
     *
     * @param productId
     * @return
     */
    @GetMapping(value = "goodsInfo")
    public SimpleResponse goodsInfo(@RequestParam(value = "productId") Long productId,
                                    @RequestParam(value = "auditId") Long auditId) {
        return platformDistAuditService.goodsDetail(productId, auditId);
    }

    /**
     * 平台运营-审核
     *
     * @param reqDTO
     * @return
     */
    @PostMapping(value = "check")
    public SimpleResponse check(@RequestBody PlatformDistAuditReqDTO reqDTO) {
        Integer checkStatus = reqDTO.getCheckStatus();
        if (1 == checkStatus) {
            reqDTO.setCheckStatus(DistributionAuditStatusEnum.SUCCESS.getCode());
        }else{
            reqDTO.setCheckStatus(DistributionAuditStatusEnum.FAIL.getCode());
        }
        String checkReasonPicsStr = reqDTO.getCheckReasonPicsStr();
        List<String> checkReasonPicList = null;
        if (StringUtils.isNotBlank(checkReasonPicsStr)) {
            String[] picArr = checkReasonPicsStr.split(",");
            checkReasonPicList = Arrays.asList(picArr);
            if (checkReasonPicList.size() > 3) {
                return new SimpleResponse(1, "审核图片不能多于3张");
            }
        }
        return platformDistAuditService.audit(reqDTO, checkReasonPicList);
    }

    /**
     * 平台运营-审核不通过原因
     *
     * @param productId
     * @return
     */
    @GetMapping(value = "reason")
    public SimpleResponse reason(@RequestParam(value = "productId") Long productId,
                                 @RequestParam(value = "auditId") Long auditId) {
        return platformDistAuditService.getCheckReason(productId, auditId);
    }

}
