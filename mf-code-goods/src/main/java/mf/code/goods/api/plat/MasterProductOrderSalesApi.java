package mf.code.goods.api.plat;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.service.SellerGoodsDistributionV5Service;
import mf.code.goods.service.SellerPlateGoodsDistributionApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * mf.code.goods.api.plat
 * Description: 修复数据api
 *
 * @author gel
 * @date 2019-06-14 16:40
 */
@Slf4j
@RestController
@RequestMapping("/api/goods/platform/product/")
public class MasterProductOrderSalesApi {
    /**
     * 不要学习这里的代码，该处只是为了方便修改数据设计的
     */
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private SellerGoodsDistributionV5Service sellerGoodsDistributionV5Service;
    @Autowired
    private SellerPlateGoodsDistributionApiService sellerPlateGoodsDistributionApiService;

    /**
     * 修复订单销量字段数据
     *
     * @return .
     */
    @GetMapping(value = "orderSales")
    public SimpleResponse orderSales() {
        SimpleResponse simpleResponse = new SimpleResponse();
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Product::getId);
        Page<Product> page = new Page<>(1, 1);
        IPage<Product> productIPage = productMapper.selectPage(page, queryWrapper);
        List<Product> productList = productIPage.getRecords();
        if (CollectionUtils.isEmpty(productList)) {
            simpleResponse.setData("没有商品");
            return simpleResponse;
        }
        int productCount = 0;
        long maxId = productList.get(0).getId();
        for (long i = 1; i < maxId; i++) {
            int count = orderAppService.countGoodsNumByOrder(i);
            if (count > 0) {
                Product productForUpdate = new Product();
                productForUpdate.setId(i);
                productForUpdate.setOrderSales(count);
                try {
                    int updateCount = productMapper.updateByPrimaryKeySelective(productForUpdate);
                    if (updateCount == 1) {
                        productCount++;
                    }
                } catch (Exception e) {
                    log.error("更新异常", e);
                }
            }
        }
        simpleResponse.setData("完成操作数量：" + productCount);
        return simpleResponse;
    }

    /**
     * 批量修改product中的价格相关的信息
     *
     * @param productId
     * @return
     */
    @PostMapping(value = "market/batchUpdateProductPrice")
    public SimpleResponse batchUpdateProductPrice(@RequestParam("productId") Long productId) {
        return sellerGoodsDistributionV5Service.batchUpdateProductPrice(productId);
    }

    /***
     * 根据productID 批量添加销量缓存
     * @return
     */
    @PostMapping(value = "market/batchAddCacheByProductId")
    public SimpleResponse batchAddCacheByProductId(){
        return sellerGoodsDistributionV5Service.batchAddCacheByProductId();
    }

    /**
     * 批量修改product中具有分销属性的的信息
     * @param productId
     * @return
     */
    @PostMapping(value = "market/batchUpdateProductInfo")
    public SimpleResponse batchUpdateProductInfo(@RequestParam("productId") Long productId){
        return sellerGoodsDistributionV5Service.batchUpdateProductInfo(productId);
    }



    /**
     * 批量修改类目表中的新类目
     * @return
     */


    @PostMapping(value = "plate/batchUpdateSpecsInfo")
    public SimpleResponse batchUpdateSpecsInfo(){
        return sellerPlateGoodsDistributionApiService.batchUpdateSpecsInfo();
    }
}
