package mf.code.goods.api.feignclient;

import mf.code.goods.api.feignclient.fallback.CommentAppFeignFallbackFactory;
import mf.code.goods.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.api.feignclient
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-02 14:58
 */
@FeignClient(value = "mf-code-comment", fallbackFactory = CommentAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface CommentAppService {

    /**
     * 获取 商品 最新的一条 评价记录
     *
     * @param goodsId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/comment/applet/queryProductCommentPage")
    Map queryProductCommentPage(@RequestParam("goodsId") Long goodsId,
                                @RequestParam("shopId") Long shopId,
                                @RequestParam(name = "goodsParentId", defaultValue = "false") boolean goodsParentId);
}
