package mf.code.goods.api.seller;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.service.SellerAwardGoodsV7Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.goods.api.seller
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年05月27日 15:50
 */
@RestController
@Slf4j
@RequestMapping("/api/goods/seller/v7/goods")
public class SellerAwardGoodsV7Api {

    @Autowired
    private SellerAwardGoodsV7Service sellerAwardGoodsV7Service;

    /***
     * 获取奖品集合,注意与商品管理的列表 没有关系，没有关系，没有关系
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping("list")
    public SimpleResponse list(@RequestParam("merchantId") Long merchantId,
                               @RequestParam("shopId") Long shopId,
                               @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return sellerAwardGoodsV7Service.listForListOptimize(merchantId, shopId, pageNum, pageSize);
    }
}
