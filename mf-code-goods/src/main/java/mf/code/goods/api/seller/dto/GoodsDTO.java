package mf.code.goods.api.seller.dto;/**
 * create by qc on 2019/4/2 0002
 */

import com.alibaba.fastjson.JSON;
import lombok.Data;
import mf.code.goods.dto.ProductSkuDTO;
import mf.code.goods.dto.ProductSkuSalePropDTO;
import mf.code.goods.repo.po.ProductSaleProps;
import mf.code.goods.repo.po.ProductSku;

import java.util.List;
import java.util.Map;

/**
 * @author gbf
 * 2019/4/2 0002、19:04
 */
@Data
public class GoodsDTO {
    private String productId;
    //产品类目
    private String productSpecs;
    private String merchantId;
    private String shopId;
    //产品标题
    private String productTitle;
    //主图
    private String mainPics;
    //产品库存
    private String productTotalStock;
    //第三方 产品价格 最低价
    private String thirdPriceMin;
    //第三方 产品价格 最高价
    private String thirdPriceMax;
    //详情图片
    private String detailPics;
    //sku json格式
    private String sku;

    private List<Map> skuMapList;
    // sku数组
    private List<ProductSku> productSkusList;
    //分销设置
    private String selfDistribution;
    private String platDistribution;
    private String selfDistributionRadio;
    private String platDistributionRadio;

    //是否上架
    private String putaway;

    //审核不通过理由
    private String reason;
    /**
     * 销售属性 json格式 木子要求变更为如下结构
     * <pre>
     *     saleProp:[
     *        {
     *          id:1,
     *          values:[]
     *        }
     *     ]
     * </pre>
     */
    private String saleProp;
    // 销售属性数组
    private List<ProductSaleProps> productSalePropsList;


    /**
     * 解析sku的字符串为sku对象数组
     *
     * @return
     */
    public List<ProductSkuDTO> getProductSkusList() {
        return JSON.parseArray(sku, ProductSkuDTO.class);
    }

    /**
     * 解析销售属性字符串为 对象数组
     *
     * @return
     */
    public List<ProductSaleProps> getProductSalePropsList() {
        return JSON.parseArray(saleProp, ProductSaleProps.class);
    }

    /**
     * 从sku中解析销售属性(此时:sku的saleProp属性为字符串) 解析为数组对象
     *
     * @param saleProp .
     * @return .
     */
    public List<ProductSkuSalePropDTO> getProductSalePropsListFromSku(String saleProp) {

        List<ProductSkuSalePropDTO> productSalePropsList = null;
        try {
            return JSON.parseArray(saleProp, ProductSkuSalePropDTO.class);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return null;
        }
    }

}