package mf.code.goods.api.seller.dto;

import lombok.Data;

/**
 * mf.code.goods.api.seller.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-12 10:27
 */
@Data
public class SellerProductListReqDTO {

    /**
     * 商户id
     */
    private Long merchantId;
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 商品id
     */
    private Long productId;
    /**
     * 是否在售，1:在售 2:仓库中
     */
    private Integer onSale;
    /**
     * 商品标题
     */
    private String productTitle;
    /**
     * 分销方式 0：全部 1：店铺分销 2：店铺+平台分销 3：平台分销 4：无
     */
    private Integer distributionMode;
    /**
     * 来源 0：全部 1：自营商品 2：分销商品
     */
    private Integer origin;
    /**
     * 库存状态 0:全部(默认) 1:预警
     */
    @Deprecated
    private Integer stockStat;
    /**
     * 销量最小值 库存
     */
    private Integer salesVolumeMin;
    /**
     * 销量最大值 库存
     */
    private Integer salesVolumeMax;
    /**
     * 时间开始
     */
    @Deprecated
    private String timeBegin;
    /**
     * 时间结束
     */
    @Deprecated
    private String timeEnd;

    /**
     * 页码
     */
    private Integer pageCurrent;
    /**
     * 开始条数
     */
    private Integer pageBegin;
    private Integer pageSize;

}
