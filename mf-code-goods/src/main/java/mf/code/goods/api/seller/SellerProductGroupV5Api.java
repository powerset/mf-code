package mf.code.goods.api.seller;

import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductGroupListResultDTO;
import mf.code.goods.dto.ProductGroupReqDTO;
import mf.code.goods.dto.ProductSingleResultDTO;
import mf.code.goods.service.ProductGroupService;
import mf.code.goods.service.GoodsQueryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.api.seller
 * Description:
 *
 * @author gel
 * @date 2019-05-16 15:53
 */
@RestController
@RequestMapping("/api/goods/seller/v5/goods/group")
public class SellerProductGroupV5Api {

    @Autowired
    private ProductGroupService productGroupService;
    @Autowired
    private GoodsQueryService goodsQueryService;

    /**
     * 商品分组列表
     *
     * @param shopId
     * @return
     */
    @GetMapping(value = "list")
    public SimpleResponse list(@RequestParam(value = "shopId") Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        List<ProductGroupListResultDTO> resultDTOList = productGroupService.list(shopId);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("list", resultDTOList);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 商品分组-添加分组
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "addgroup")
    public SimpleResponse addGroup(@RequestBody ProductGroupReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (dto.getShopId() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请重新登录选择店铺");
            return simpleResponse;
        }
        if (StringUtils.isBlank(dto.getGroupName())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("分组名称不得为空");
            return simpleResponse;
        }
        if (dto.getGroupName().length() > 6) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("分组名称不得超过六个字");
            return simpleResponse;
        }
        Integer count = productGroupService.createGroup(dto);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("创建分组失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 商品分组--单个id查询商品
     *
     * @param merchantId
     * @param shopId
     * @param productId
     * @return
     */
    @GetMapping(value = "querysinglegood")
    public SimpleResponse querySingleGood(@RequestParam(value = "merchantId") Long merchantId,
                                          @RequestParam(value = "shopId") Long shopId,
                                          @RequestParam(value = "productId") Long productId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ProductSingleResultDTO productSingleResultDTO = goodsQueryService.selectSingleProduct(shopId, productId);
        simpleResponse.setData(productSingleResultDTO);
        return simpleResponse;
    }


    /**
     * 商品分组-添加商品到分组
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "addgroupgood")
    public SimpleResponse addGroupGoods(@RequestBody ProductGroupReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Integer count = productGroupService.addGroupGoods(dto);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("添加商品到分组失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 商品分组--分组下的所有商品
     *
     * @param merchantId
     * @param shopId
     * @param groupId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "querygroupgood")
    public SimpleResponse queryGroupGoods(@RequestParam(value = "merchantId") Long merchantId,
                                          @RequestParam(value = "shopId") Long shopId,
                                          @RequestParam(value = "groupId") Long groupId,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Long pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Long pageSize) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultMap = productGroupService.queryGroupGoods(shopId, groupId, pageNum, pageSize);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }


    /**
     * 商品分组-删除该分组的下商品
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "delgroupgood")
    public SimpleResponse delGroupGoods(@RequestBody ProductGroupReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (dto.getGroupId() == null || dto.getProductId() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("请选择带删除的商品");
            return simpleResponse;
        }
        Integer count = productGroupService.delGroupGoods(dto);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("删除该分组的下商品失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 商品分组--修改分组名称
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "updategroupName")
    public SimpleResponse updateGroupName(@RequestBody ProductGroupReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (StringUtils.isBlank(dto.getGroupName()) || dto.getGroupName().length() > 6) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("请输入合适的分组名称");
            return simpleResponse;
        }
        Integer count = productGroupService.updateSelective(dto);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("修改分组名称失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 商品分组--修改分组排序
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "updategroupOrder")
    public SimpleResponse updateGroupOrder(@RequestBody ProductGroupReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (dto.getShopId() == null || dto.getGroupId() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请选择待修改分组");
            return simpleResponse;
        }
        if (dto.getGroupOrder() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("请输入排列序号");
            return simpleResponse;
        }
        Integer count = productGroupService.updateSelective(dto);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("修改分组排序失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 商品分组-删除该分组
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "delgroup")
    public SimpleResponse delGroup(@RequestBody ProductGroupReqDTO dto) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (dto.getShopId() == null || dto.getGroupId() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请选择待删除分组");
            return simpleResponse;
        }
        productGroupService.delGroup(dto);
        return simpleResponse;
    }

}
