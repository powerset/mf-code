package mf.code.goods.api.applet.v10;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.service.ActivityProductSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.goods.api.applet.v10
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月17日 15:33
 */
@RestController
@RequestMapping("/api/goods/applet/v10")
@Slf4j
public class ActivityProductApi {
    @Autowired
    private ActivityProductSkuService activityProductSkuService;

    /***
     * 查询优惠 商品详情-单一sku
     * @param merchantId
     * @param shopId
     * @param userId
     * @param skuId
     * @return
     */
    @GetMapping("/getDiscountGoodsDetail")
    public SimpleResponse getDiscountGoodsDetail(@RequestParam("merchantId") Long merchantId,
                                                 @RequestParam("shopId") Long shopId,
                                                 @RequestParam("userId") Long userId,
                                                 @RequestParam("skuId") Long skuId) {
        if (merchantId == null || merchantId == 0 || shopId == null || merchantId == 0
                || userId == null || userId == 0 || skuId == null || skuId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "数据异常，请传入正确的数据");
        }
        return this.activityProductSkuService.getDiscountGoodsDetail(merchantId, shopId, userId, skuId);
    }
}
