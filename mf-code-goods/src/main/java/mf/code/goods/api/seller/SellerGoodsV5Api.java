package mf.code.goods.api.seller;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.annotation.Upload;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.goods.api.seller.dto.GoodsDTO;
import mf.code.goods.api.seller.dto.SellerProductListReqDTO;
import mf.code.goods.common.redis.RedisForbidRepeat;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.dto.ProductSkuDTO;
import mf.code.goods.dto.ProductUpdateReqDTO;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.GoodsUpdateService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author gbf
 * 2019/4/2 0002、18:03
 */
@RestController
@Slf4j
@RequestMapping(value = {"/api/goods/seller/v5/goods", "/api/goods/platform/v5/goods", "/api/goods/applet/v5/goods"})
public class SellerGoodsV5Api {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsUpdateService goodsUpdateService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 获取商品类目下拉表
     *
     * @param merchantId .api/goods/seller/v5/goods/distribution
     * @param shopId
     * @return .
     */
    @GetMapping(value = "getSpecs")
    public SimpleResponse getSpecs(@RequestParam(value = "merchantId", required = false) Long merchantId,
                                   @RequestParam(value = "shopId", required = false) Long shopId) {
        return goodsService.getSpecs(merchantId, shopId);
    }

    /**
     * 图片上传
     *
     * @param image  图片对象
     * @param shopId 店铺id
     * @return .
     */
    @Upload
    @PostMapping(value = "uploadPic")
    public SimpleResponse uploadPic(@RequestParam(value = "image") MultipartFile image,
                                    @RequestParam(name = "shopId") Long shopId) {
        return goodsService.uploadPic(image, shopId);
    }

    /**
     * 创建商品
     *
     * @param dto dto
     * @return SimpleResponse
     */
    @PostMapping(value = "")
    public SimpleResponse create(@RequestBody ProductDTO dto) {
        Integer putaway = dto.getPutaway();
        Assert.isRightNumberSection(putaway, 0, 1, 2002, "putaway只能是1或0");

        SimpleResponse response = this.forbidRepeat(dto.getMerchantId(), dto.getShopId(), dto.hashCode());
        if (response != null) {
            return response;
        }

        this.assertProductDto(dto);
        this.assertSkuDto(dto);
        return goodsService.create(dto);
    }

    /**
     * 防重
     *
     * @param merchantId
     * @param shopId
     * @param hashCode
     * @return
     */
    private SimpleResponse forbidRepeat(Long merchantId, Long shopId, int hashCode) {
        String redisKey = "mf:code:goods:createOrUpdateGoods:" + merchantId + ":" + shopId + ":" + hashCode;
        if (!RedisForbidRepeat.NX(stringRedisTemplate, redisKey, 20, TimeUnit.SECONDS)) {
            return new SimpleResponse(1, "请勿重复提交");
        }
        return null;
    }


    /**
     * 修改商品
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "update")
    public SimpleResponse update(@RequestBody ProductDTO dto) {
        // 修改商品,其实修改的是sku,商品的属性都不能修改,因为修改商品需要平台审核.已经核实思宏
        SimpleResponse response = this.forbidRepeat(dto.getMerchantId(), dto.getShopId(), dto.hashCode());
        if (response != null) {
            return response;
        }
        this.assertProductDto(dto);
        this.assertSkuDto(dto);
        return goodsService.update(dto);
    }

    /**
     * 创建产品断言
     * <pre>
     *     错误码 :
     *     1 商户id
     *     2 店铺id
     *     3 产品类目
     *     4 产品标题
     *     5 产品主图
     *     6 商品详情图片
     *     61 商品详情图片json
     * </pre>
     *
     * @param dto .
     */
    private void assertProductDto(ProductDTO dto) {
        //商品类目,即:category_taobao对应字段
        Long productSpecs = dto.getProductSpecs();
        Assert.isTrue(productSpecs != null, 3, "商品类目必填");
        //标题
        String productTitle = dto.getProductTitle();
        Assert.hasText(productTitle, 4, "商品标题必填");
        Assert.isTrue(productTitle.length() <= 26 && productTitle.length() >= 2, 4, "商品标题字数需在2到26之间");
        //商品主图断言
        String mainPicsStr = dto.getMainPics();
        List<String> mainPics = dto.parsePicsList(mainPicsStr);
        if (CollectionUtils.isEmpty(mainPics)) {
            Assert.isTrue(false, 51, "商品主图json数组错误");
        }
        Assert.isTrue(mainPics.size() >= 1, 52, "商品主图最少为一张");
        Assert.isTrue((mainPics.size() <= 5), 52, "商品主图最多为5张");
        dto.setMainPics(JSON.toJSONString(mainPics));
        //商品详情图断言
        String detailPicsStr = dto.getDetailPics();
        List<String> detailPics = dto.parsePicsList(detailPicsStr);
        if (CollectionUtils.isEmpty(detailPics)) {
            Assert.isTrue(false, 6, "商品详情图片不能为空");
        }
        Assert.isTrue(detailPics.size() >= 1, 52, "商品详情图最少为一张");
        Assert.isTrue((detailPics.size() <= 15), 52, "商品详情图最多为15张");
        dto.setMainPics(JSON.toJSONString(detailPics));

    }

    /**
     * 修改的sku参数断言
     * 7 商品sku
     * 71 商品sku的json
     * 72 售价是空
     * 73 售价不是数字
     * 8 淘宝价最高最低区间不符
     * 82 淘宝价
     * 83 淘宝价不是数字
     * 91 库存
     * 92 库存不是数字
     *
     * @param dto
     */
    private void assertSkuDto(ProductDTO dto) {
        // sku
        String skuString = dto.getSku();
        if (skuString == null) {
            Assert.isTrue(false, 70, "商品sku解析失败");
        }
        List<ProductSkuDTO> productSkuDTOList = dto.parseSkuListFromString();
        if (productSkuDTOList == null) {
            Assert.isTrue(false, 7, "商品sku解析失败");
        }
        if (productSkuDTOList.size() == 0) {
            Assert.isTrue(false, 71, "商品sku解析失败");
        }

        dto.setProductSkuDTOList(productSkuDTOList);

        for (ProductSkuDTO sku : productSkuDTOList) {
            BigDecimal sellingPrice = sku.getSkuSellingPrice();
            BigDecimal thirdPrice = sku.getSkuThirdPrice();
            Integer stock = sku.getStock();

            this.priceError(sellingPrice, 70, "售价错误.售价为>=0的可含小数的数字");
            this.priceError(thirdPrice, 80, "淘宝价错误,淘宝价为>=0的可含小数的数字");

            if (stock == null) {
                Assert.isTrue(false, 92, "库存有误");
            } else {
                if (stock < 0) {
                    Assert.isTrue(false, 94, "库存有误,库存不能小于0");
                }
            }
            //这里的快照,只标识是否展示,而非快照概念
            Integer snapshoot = sku.getSnapshoot();
            Assert.isTrue(snapshoot != null, 101, "商品快照参数错误缺失");
            Assert.isInteger(snapshoot.toString(), 102, "商品快照参数错误,该参数只能为1或0");
            Assert.isRightNumberSection(snapshoot, 0, 1, 103, "商品快照参数错误,该参数只能为1或0");
        }
    }

    /**
     * 校验价格类型
     *
     * @param price
     * @param errorCode
     * @param errorMessage
     */
    private void priceError(BigDecimal price, int errorCode, String errorMessage) {
        if (price == null) {
            Assert.isTrue(false, errorCode, errorMessage);
        } else {
            if (price.compareTo(BigDecimal.ZERO) < 0) {
                Assert.isTrue(false, errorCode, errorMessage);
            }
        }
    }

    /**
     * 商品管理列表页
     *
     * @param shopId           店铺
     * @param productId        产品id 精确查询
     * @param productTitle     产品标题
     * @param distributionMode 分销方式
     * @param origin           来源
     * @param stockStat        库存状态
     * @param salesVolumeMin   销量
     * @param salesVolumeMax   销量
     * @param timeBegin        开始时间
     * @param timeEnd          结束时间
     * @return
     */
    @GetMapping(value = "list")
    public SimpleResponse find(@RequestParam(value = "merchantId") Long merchantId,
                               @RequestParam(value = "shopId") Long shopId,
                               @RequestParam(value = "productId", required = false) Long productId, // 产品id
                               @RequestParam(value = "productTitle", required = false) String productTitle, //产品标题
                               @RequestParam(value = "distributionMode", required = false) Integer distributionMode, //分销方式
                               @RequestParam(value = "origin", required = false) Integer origin, //来源
                               @RequestParam(value = "stockStat", required = false) Integer stockStat, //库存状态
                               @RequestParam(value = "salesVolumeMin", required = false) Integer salesVolumeMin, //销量
                               @RequestParam(value = "salesVolumeMax", required = false) Integer salesVolumeMax, //销量
                               @RequestParam(value = "timeBegin", required = false) String timeBegin, //时间
                               @RequestParam(value = "timeEnd", required = false) String timeEnd,
                               @RequestParam(value = "status", required = false) Integer status, //上架状态 1:商家 0:仓库
                               @RequestParam(value = "pageCurrent", defaultValue = "1") Integer pageCurrent,
                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                               @RequestParam(value = "onSale", defaultValue = "1") Integer onSale) { // 1:在售 2:仓库中

        Assert.numberBiggerThanZeroNullable(salesVolumeMax, 40, "销量最低值必须>=0");
        Assert.numberBiggerThanZeroNullable(salesVolumeMin, 41, "销量最高值必须>=0");

        if (salesVolumeMax != null && salesVolumeMin != null) {
            if (salesVolumeMax < salesVolumeMin) {
                Assert.isTrue(false, 43, "销量最大值<最小值");
            }
        }

        // 库存状态
        if (stockStat != null) {
            Assert.isRightNumberSection(stockStat, 0, 1, 52, "库存状态有误");
        }
        if (distributionMode != null) {
            Assert.isRightNumberSection(distributionMode, 0, 4, 54, "分销渠道有误");
        }
        //产品来源
        if (origin != null) {
            Assert.isRightNumberSection(origin, 0, 4, 55, "产品来源有误");
        }

        Date beginDate = null;
        Date endDate = null;
        boolean notBlankBegin = StringUtils.isNotBlank(timeBegin);
        boolean notBlankEnd = StringUtils.isNotBlank(timeEnd);
        if (notBlankBegin) {
            Assert.canConvertDate(timeBegin, DateUtil.LONG_DATE_FORMAT, 4, "开始时间错误");
            beginDate = DateUtil.stringtoDate(timeBegin, DateUtil.LONG_DATE_FORMAT);
            timeBegin = DateUtil.dateToString(beginDate, DateUtil.FORMAT_ONE);
        }
        if (notBlankEnd) {
            Assert.canConvertDate(timeEnd, DateUtil.LONG_DATE_FORMAT, 5, "结束时间错误");
            endDate = DateUtil.getDateBeforeZero(DateUtil.stringtoDate(timeEnd, DateUtil.LONG_DATE_FORMAT));
            timeEnd = DateUtil.dateToString(endDate, DateUtil.FORMAT_ONE);
        }
        if (notBlankBegin && notBlankEnd) {
            Assert.isRightDateArgument(timeBegin, timeEnd, DateUtil.LONG_DATE_FORMAT, 6, "开始时间在结束之间之后");
        }
        //条件map
        SellerProductListReqDTO reqDTO = new SellerProductListReqDTO();
        reqDTO.setOnSale(onSale);
        reqDTO.setMerchantId(merchantId);
        reqDTO.setShopId(shopId);
        reqDTO.setProductId(productId);
        reqDTO.setProductTitle(productTitle);
        reqDTO.setDistributionMode(distributionMode);
        reqDTO.setOrigin(origin);
        reqDTO.setStockStat(stockStat);
        reqDTO.setSalesVolumeMin(salesVolumeMin);
        reqDTO.setSalesVolumeMax(salesVolumeMax);
        reqDTO.setTimeBegin(timeBegin);
        reqDTO.setTimeEnd(timeEnd);
        reqDTO.setPageCurrent(pageCurrent);
        reqDTO.setPageBegin((pageCurrent - 1) * pageSize);
        reqDTO.setPageSize(pageSize);
        return goodsService.listByConditionForListOptimize(reqDTO);
    }

    /**
     * 上架
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "upshelf")
    public SimpleResponse upshelf(@RequestBody ProductDTO dto) {
        // 断言 id
        /**
         * <pre>
         *     校验商品: 1 是否存在
         *               2 状态是否是在待上架状态
         * </pre>getSpecs
         */
        Long productId = dto.getProductId();
        Long merchantId = dto.getMerchantId();
        Long shopId = dto.getShopId();
        return goodsService.upShelf(productId, merchantId, shopId);
    }

    /**
     * 下架
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "downshelf")
    public SimpleResponse downshelf(@RequestBody ProductDTO dto) {
        Long productId = dto.getProductId();
        Long merchantId = dto.getMerchantId();
        Long shopId = dto.getShopId();

        return goodsService.downshelf(productId, merchantId, shopId);
    }


    /**
     * 将分销市场商品加入仓库
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "dist/addstore")
    public SimpleResponse distAddstore(@RequestBody GoodsDTO dto) {
        String productId = dto.getProductId();
        String merchantId = dto.getMerchantId();
        String shopId = dto.getShopId();
        Assert.isNumber(productId, 1, "产品id错误");
        Assert.isNumber(merchantId, 2, "商户id错误");
        Assert.isNumber(shopId, 3, "店铺id错误");
        return goodsService.addstore(productId, merchantId, shopId);
    }

    /**
     * 将分销市场商品直接上架
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "dist/upshelf")
    public SimpleResponse distUpshelf(@RequestBody GoodsDTO dto) {

        String productId = dto.getProductId();
        String merchantId = dto.getMerchantId();
        String shopId = dto.getShopId();
        String selfDistributionRadio = dto.getSelfDistributionRadio();
        Assert.isNumber(productId, 1, "产品id错误");
        Assert.isNumber(merchantId, 2, "商户id错误");
        Assert.isNumber(shopId, 3, "店铺id错误");
        Assert.isNumber(selfDistributionRadio, 4, "店铺佣金比例有误");
        BigDecimal selfDistributionRadioDecimal = new BigDecimal(selfDistributionRadio);
        if (selfDistributionRadioDecimal.compareTo(new BigDecimal(1)) <= 0) {
            new SimpleResponse(5, "店铺佣金比例必须在1%到100%之间");
        }
        if (selfDistributionRadioDecimal.compareTo(new BigDecimal(100)) >= 0) {
            new SimpleResponse(5, "店铺佣金比例必须在1%到100%之间");
        }
        return goodsService.distUpshelf(productId, merchantId, shopId, selfDistributionRadioDecimal);
    }

    /**
     * 删除
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "del")
    public SimpleResponse del(@RequestBody ProductDTO dto) {
        Long productId = dto.getProductId();
        SimpleResponse del = goodsService.del(productId, dto.getAppType());
        if (del.getCode().equals(ApiStatusEnum.SUCCESS.getCode())) {
            // 删除成功后更新缓存
            goodsService.addZsetCacheByProductId(dto.getProductId());
        }
        return del;
    }

    /**
     * 商品详情
     *
     * @return
     */
    @GetMapping(value = "detail")
    public SimpleResponse detail(@RequestParam("productId") Long productId) {
        return goodsService.detail(productId);
    }

    /**
     * 加入新手专区
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "newbieProduct")
    public SimpleResponse newbieProduct(@RequestBody ProductUpdateReqDTO dto) {
        Assert.notNull(dto.getShopId(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "店铺编号错误，请重新登录");
        Assert.notNull(dto.getProductId(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请选择商品加入新手专区");
        return goodsUpdateService.newbieProduct(dto);
    }
}
