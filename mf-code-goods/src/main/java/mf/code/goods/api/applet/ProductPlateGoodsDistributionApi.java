package mf.code.goods.api.applet;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.api.seller.dto.PlateDisProductListResultDTO;
import mf.code.goods.api.seller.dto.SpecesAndBannerDTO;
import mf.code.goods.common.constant.ProductSortEnum;
import mf.code.goods.common.constant.SpeceTypeEnum;
import mf.code.goods.common.tlab.AppTypeRequestContext;
import mf.code.goods.repo.service.CategoryTaobaoService;
import mf.code.goods.service.SellerGoodsV8Service;
import mf.code.goods.service.SellerPlateGoodsDistributionApiService;
import mf.code.shop.constants.AppTypeEnum;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/goods/applet/v1/goods/plate")
@RequiredArgsConstructor
@Slf4j
public class ProductPlateGoodsDistributionApi {
    private final SellerPlateGoodsDistributionApiService sellerPlateGoodsDistributionApiService;
    private final SellerGoodsV8Service sellerGoodsV8Service;
    private final StringRedisTemplate stringRedisTemplate;
    private final CategoryTaobaoService categoryTaobaoService;
    //复制zset1中的数据
    private static final String NEW_KEY = "jkmf:plate:distribution:salesvolume:list";
    //获取本店铺根据类目查询的商品缓存key zset3
    private static final String NEW_KEY_SPECS = NEW_KEY + ":" + "specs";

    /**
     * 获取平台分销产品信息
     *
     * @param shopId
     * @param userId
     * @param speceType
     * @param offset
     * @param limit
     * @param sort      抖带带推荐排序 1今日上新 2热销 3高佣
     * @return
     */
    @GetMapping(value = "/distribution/list")
    public SimpleResponse getPalteDistributionList(@RequestParam(value = "shopId", defaultValue = "0") String shopId,
                                                   @RequestParam(value = "userId", defaultValue = "0") Long userId,
                                                   @RequestParam(value = "speceType", defaultValue = "1000000000") String speceType,
                                                   @RequestParam(value = "offset", defaultValue = "0") Integer offset,
                                                   @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                   @RequestParam(value = "sort", required = false) Integer sort,
                                                   HttpServletRequest request) {
        String project = request.getHeader("project");
        if ("ddd".equals(project)) {
            AppTypeRequestContext.setAppType(AppTypeEnum.DDD);
        }

        //必传参数校验
//        if (StringUtils.isEmpty(shopId)) {
//            log.debug("enter into ProductPlateGoodsDistributionApi.getPalteDistributionList and find shopId is null");
//            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
//        }

        limit = limit < 0 ? 10 : limit;

        //获取结果集
        PlateDisProductListResultDTO resultDTO = null;
        if (sort == null || ProductSortEnum.SALE.getCode() == sort) {
            resultDTO = sellerPlateGoodsDistributionApiService.getPalteDistributionListResult(offset, limit, shopId, speceType, userId, sort);
        }

        if (null == resultDTO) {
            PlateDisProductListResultDTO resultfail = new PlateDisProductListResultDTO();
            resultfail.setCount(0);
            resultfail.setOffset(offset);
            resultfail.setLimit(limit);
            resultfail.setIsPullDown(false);
            resultfail.setDisProductList(new ArrayList<>());
            resultfail.setProductDistributionDTO(new ArrayList<>());
            log.debug("enter into ProductPlateGoodsDistributionApi.getPalteDistributionList and find resultDTO is null");
            return new SimpleResponse(ApiStatusEnum.ERROR_SYSTEM, resultfail);
        }

        Long nums = stringRedisTemplate.opsForZSet().size(NEW_KEY_SPECS + ":" + shopId + ":" + speceType);
        if (null == nums) {
            nums = 0L;
        }
        int count = nums.intValue();
        int end = offset + limit - 1;
        //是否可以继续分页
        boolean isPullDown = true;

        if (end >= count) {
            isPullDown = false;
        }

        resultDTO.setLimit(limit);
        resultDTO.setOffset(offset);
        resultDTO.setIsPullDown(isPullDown);

        log.debug("out of ProductPlateGoodsDistributionApi.getPalteDistributionList success!");
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultDTO);
    }

    /**
     * 获取平台分销产品信息
     * by gbf 20190819
     *
     * @param shopId
     * @param userId
     * @param speceType
     * @param offset
     * @param limit
     * @param sort      抖带带推荐排序 1今日上新 2热销 3高佣
     * @return
     */
    @GetMapping(value = "/distribution/list/ddd")
    public SimpleResponse getPalteDistributionListDDD(@RequestParam(value = "shopId") Long shopId,
                                                      @RequestParam(value = "userId") Long userId,
                                                      @RequestParam(value = "speceType", defaultValue = "1000000000") String speceType,
                                                      @RequestParam(value = "offset", defaultValue = "0") Integer offset,
                                                      @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                      @RequestParam(value = "sort") Integer sort) {

        // 参数校验
        if (offset < 0 || limit < 1) {
            return new SimpleResponse(1, "分页参数错误");
        }
        if (sort < 1 || sort > 3) {
            return new SimpleResponse(1, "排序参数错误");
        }
        Assert.isNumber(speceType, 1, "类目参数错误");
        int speceTypeInt = Integer.parseInt(speceType);
        if (speceTypeInt < SpeceTypeEnum.DEFALT.getCode() || speceTypeInt > SpeceTypeEnum.GAMES.getCode()) {
            return new SimpleResponse(1, "参数类目错误");
        }

        //获取结果集
        PlateDisProductListResultDTO resultDTO = sellerGoodsV8Service.pickProductList(offset, limit, shopId, speceType, userId, sort);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultDTO);
    }

    /**
     * 获取banner和类目信息
     *
     * @param shopId // 未登录, 入参为0
     * @param userId // 未登录, 入参为0
     * @return
     */
    @GetMapping(value = "distribution/getSpecesAndBanners")
    public SimpleResponse getSpecesAndBanners(@RequestParam(value = "shopId", required = false, defaultValue = "0") Long shopId,
                                              @RequestParam(value = "userId", required = false, defaultValue = "0") Long userId,
                                              @RequestParam(value = "appType", required = false, defaultValue = "0") Integer appType) {
        // 获取全平台类目
        List<SpecesAndBannerDTO> dtoList = sellerPlateGoodsDistributionApiService.getBannerAndSpece(shopId, userId);
        // 集客魔方 直接返回
        if (appType == AppTypeEnum.JKMF.getCode()) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, dtoList);
        }
        return sellerGoodsV8Service.handlDDDSpec(dtoList, shopId, userId);
    }

    /***
     * 获取商品轮播详细
     *
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("distribution/getBannerList")
    public SimpleResponse getBannerList(@RequestParam(value = "shopId", defaultValue = "0") Long shopId,
                                        @RequestParam(value = "userId", defaultValue = "0") Long userId,
                                        HttpServletRequest request) {
        AppTypeEnum appTypeEnum = AppTypeEnum.JKMF;
        String project = request.getHeader("project");
        if ("ddd".equals(project)) {
            appTypeEnum = AppTypeEnum.DDD;
        }
        if ("dxp".equals(project)) {
            appTypeEnum = AppTypeEnum.DXP;
        }

        return sellerGoodsV8Service.getBannerList(shopId, userId, appTypeEnum);
    }
}
