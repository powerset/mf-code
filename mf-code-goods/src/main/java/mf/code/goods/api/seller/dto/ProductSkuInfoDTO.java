package mf.code.goods.api.seller.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * sku信息
 */
@Data
public class ProductSkuInfoDTO {

    //商品的最低售价
    private BigDecimal minSellPrice;
    //商品的最高售价
    private BigDecimal maxSellPrice;
    //商品的最低淘宝售价
    private BigDecimal minTaobaoPrice;
    //商品的最高淘宝售价
    private BigDecimal maxTaobaoPrice;
    //商品的ID
    private Long productId;
}
