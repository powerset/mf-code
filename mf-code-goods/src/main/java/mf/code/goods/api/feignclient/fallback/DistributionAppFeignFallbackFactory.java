package mf.code.goods.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class DistributionAppFeignFallbackFactory implements FallbackFactory<DistributionAppService> {
    @Override
    public DistributionAppService create(Throwable throwable) {
        return new DistributionAppService() {
            @Override
            public UserDistributionInfoDTO queryDistributionInfo(String userId, String shopId) {
                return null;
            }

            @Override
            public String getRebateCommission(ProductDistributionDTO productDTO) {
                return null;
            }

            @Override
            public SimpleResponse distributionCommission(Long userId, Long shopId, Long orderId) {
                return null;
            }

            @Override
            public Map<Long, String> queryUserRebateByProduct(List<String> distributionDTOLists) {
                return null;
            }

            /**
             * 获取订单分销属性
             *
             * @param product
             */
            @Override
            public String queryOrderDistributionProperty(ProductDistributionDTO product) {
                return null;
            }

            @Override
            public AppletUserProductRebateDTO queryProductRebateByUserIdShopId(AppletUserProductRebateDTO appletUserProductRebate) {
                return null;
            }

            @Override
            public ProductDistributCommissionEntity getDistrIncodeByProduct(DistributeInfoEntity productDTO) {
                return null;
            }

            @Override
            public List<ProductDistributCommissionEntity> getDistrIncodeByProductBatch(List<DistributeInfoEntity> productDTOList) {
                return null;
            }


        };
    }
}
