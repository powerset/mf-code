package mf.code.goods.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.fallback.DistributionAppFeignFallbackFactory;
import mf.code.goods.common.config.feign.FeignLogConfiguration;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-08 19:11
 */
@FeignClient(value = "mf-code-distribution", fallbackFactory = DistributionAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface DistributionAppService {
    /**
     * 查询 在此店铺内， 用户本月消费和收入的概况
     *
     * @param userId
     * @param shopId
     * @return statusCode = 0: 用户本月 预估收益，消费条件，消费金额，达标差额
     * statusCode = 1: 本月累计收益，最新团员贡献佣金
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryDistributionInfo")
    UserDistributionInfoDTO queryDistributionInfo(@RequestParam("userId") String userId,
                                                  @RequestParam("shopId") String shopId);

    /**
     * 获取 用户返利金额
     *
     * @param productDTO
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/getRebateCommission")
    String getRebateCommission(@RequestBody ProductDistributionDTO productDTO);

    /**
     * 支付成功 处理用户返利和分佣
     *
     * @param userId
     * @param shopId
     * @param orderId
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/distributionCommission")
    SimpleResponse distributionCommission(@RequestParam("userId") Long userId,
                                          @RequestParam("shopId") Long shopId,
                                          @RequestParam("orderId") Long orderId);

    /**
     * 获取用户商品返利
     *
     * @param distributionDTOLists
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryUserRebateByProduct")
    Map<Long, String> queryUserRebateByProduct(@RequestParam("distributionDTOLists") List<String> distributionDTOLists);

    /**
     * 获取订单分销属性
     */
    @PostMapping("/feignapi/distribution/applet/v5/queryOrderDistributionProperty")
    String queryOrderDistributionProperty(@RequestBody ProductDistributionDTO product);


    @PostMapping("/feignapi/distribution/applet/v5/queryProductRebateByUserIdShopId")
    AppletUserProductRebateDTO queryProductRebateByUserIdShopId(@RequestBody AppletUserProductRebateDTO appletUserProductRebate);

    /**
     * 获取商品的分销佣金
     *
     * @param productDTO
     * @return BigDecimal
     */
    @PostMapping("/feignapi/distribution/applet/v5/getDistrIncodeByProduct")
    ProductDistributCommissionEntity getDistrIncodeByProduct(@RequestBody DistributeInfoEntity productDTO);

    /**
     * 批量获取商品的分销佣金
     *
     * @param productDTOList
     * @return List<ProductDistributCommissionEntity>
     */
    @PostMapping("/feignapi/distribution/applet/v5/getDistrIncodeByProduct/batch")
    List<ProductDistributCommissionEntity> getDistrIncodeByProductBatch(@RequestBody List<DistributeInfoEntity> productDTOList);
}
