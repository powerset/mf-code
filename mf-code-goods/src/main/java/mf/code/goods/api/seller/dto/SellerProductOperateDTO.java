package mf.code.goods.api.seller.dto;

import lombok.Data;

/**
 * mf.code.goods.api.seller.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-12 11:40
 */
@Data
public class SellerProductOperateDTO {
    /**
     * 分销商 -分销设置 1:展示 0:不展示
     */
    private Integer setDistributionByDistributor;

    /**
     * 供应商 设置分销(建立推广) 1:展示 0:不展示
     */
    private Integer setDistributionByProvider;

    /**
     * 供应商 停止推广给 1:展示 0:不展示
     */
    private Integer stopDistributionByProvider;

    /**
     * 供应商 设置分销(继续设置) 1:展示 0:不展示
     */
    private Integer setDistributionByProviderAgain;
}
