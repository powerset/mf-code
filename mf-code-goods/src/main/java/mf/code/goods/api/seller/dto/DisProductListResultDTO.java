package mf.code.goods.api.seller.dto;

import lombok.Data;

import java.util.List;

/**
 * 分销商品信息
 */
@Data
public class DisProductListResultDTO {
    //分销产品信息集合
    private List<DisProductListInfoDTO> disProductList;
    //当前页
    private Integer pageNum;
    //每页条数
    private Integer pageSize;
    //总条数
    private int count;
}
