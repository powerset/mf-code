package mf.code.goods.api.seller.dto;

import lombok.Data;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;

import java.util.List;

@Data
public class PlateDisProductListResultDTO {
    //分销产品信息集合
    private List<DisProductListInfoDTO> disProductList;
    //当前页
    private Integer limit;
    //每页条数
    private Integer offset;
    //总条数
    private int count;


    //是否可以分页
    private Boolean isPullDown;

    //平台分销产品信息集
    private List<ProductDistributionDTO> productDistributionDTO;

    //获取banner图信息
    private List<SpecesAndBannerDTO> speceDto;
}
