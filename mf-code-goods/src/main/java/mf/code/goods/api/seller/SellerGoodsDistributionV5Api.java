package mf.code.goods.api.seller;/**
 * create by qc on 2019/5/6 0006
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.api.feignclient.ShopAppService;
import mf.code.goods.api.seller.dto.DisProductListResultDTO;
import mf.code.goods.common.constant.SpeceTypeEnum;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.repo.po.Product;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.SellerGoodsDistributionV5Service;
import mf.code.goods.service.impl.SellerPlateGoodsDistributionApiServiceImpl;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gbf
 * 2019/5/6 0006、20:49
 */
@RestController
@RequestMapping("/api/goods/seller/v5/goods/distribution")
@Slf4j
public class SellerGoodsDistributionV5Api {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private SellerGoodsDistributionV5Service sellerGoodsDistributionV5Service;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 分销市场--列表
     *
     * @param shopId
     * @param kinds
     * @param productName
     * @param bornFlag
     * @param commission
     * @param salesVolume
     * @param commissionPre
     * @param salesPrriceStr
     * @param salesPrriceEnd
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping(value = "market/list")
    public SimpleResponse list(@RequestParam(value = "shopId", required = false) String shopId,
                               @RequestParam(value = "kinds", required = false) String kinds,
                               @RequestParam(value = "productName", required = false) String productName,
                               @RequestParam(value = "bornFlag", required = false) String bornFlag,
                               @RequestParam(value = "commission", required = false) String commission,
                               @RequestParam(value = "salesVolume", required = false) String salesVolume,
                               @RequestParam(value = "commissionPre", required = false) String commissionPre,
                               @RequestParam(value = "salesPrriceStr", required = false) String salesPrriceStr,
                               @RequestParam(value = "salesPrriceEnd", required = false) String salesPrriceEnd,
                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

        //封装请求参数
        Map<String, Object> params = new HashMap<>();
        params.put("kinds", kinds);
        params.put("productName", productName);
        params.put("commission", commission);
        params.put("salesVolume", salesVolume);
        params.put("commissionPre", commissionPre);
        params.put("salesPrriceStr", salesPrriceStr);
        params.put("salesPrriceEnd", salesPrriceEnd);

        params.put("pageSize", pageSize <= 0 ? 10 : pageSize);

        if (StringUtils.isBlank(kinds) && StringUtils.isBlank(productName) &&
                StringUtils.isBlank(commission) && StringUtils.isBlank(salesVolume) &&
                StringUtils.isBlank(commissionPre) && StringUtils.isBlank(salesPrriceEnd) &&
                StringUtils.isBlank(salesPrriceStr) && StringUtils.isBlank(bornFlag)) {
            bornFlag = "1";
            params.put("bornFlag", bornFlag);
        }
        if (!StringUtils.isBlank(bornFlag)) {
            params.put("bornFlag", bornFlag);
        }

        int num = 0;
        if (pageNum <= 0) {
            num = 1;
        } else {
            num = (pageNum - 1) * pageSize;
        }
        params.put("pageNum", num);
        log.debug(JSON.toJSONString(params));

        //获取结果集
        DisProductListResultDTO resultDTO = sellerGoodsDistributionV5Service.getDisProductListResultBySwitch(params, shopId, salesVolume, pageNum, pageSize);

        if (null == resultDTO) {
            DisProductListResultDTO resultfail = new DisProductListResultDTO();
            resultfail.setCount(0);
            resultfail.setPageSize(pageSize);
            resultfail.setPageNum(pageNum);
            resultfail.setDisProductList(new ArrayList<>());
            log.debug("enter into SellerGoodsDistributionV5Api.list and find resultDTO is null");
            return new SimpleResponse(0, "success", resultfail);
        }
        resultDTO.setPageNum(pageNum);
        resultDTO.setPageSize(pageSize);

        log.debug("out of SellerGoodsDistributionV5Api.list success!");
        return new SimpleResponse(0, "success", resultDTO);
    }


    /**
     * 获取分销商品的详情
     *
     * @param productId
     * @return
     */
    @GetMapping(value = "market/detail")
    public SimpleResponse detail(@RequestParam("productId") Long productId,
                                 @RequestParam("shopId") Long shopId) {
        return sellerGoodsDistributionV5Service.detail(productId, shopId);
    }

    /**
     * 分销市场--上架
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @param commissionPre
     * @return
     */
    @PostMapping(value = "market/putaway")
    public SimpleResponse putaway(@RequestParam("productId") Long productId,
                                  @RequestParam("merchantId") Long merchantId,
                                  @RequestParam("shopId") Long shopId,
                                  @RequestParam("commissionPre") String commissionPre,
                                  @RequestParam(value = "appType", required = false, defaultValue = "0") Integer appType) {
        SimpleResponse simpleResponse = sellerGoodsDistributionV5Service.upShelf(productId, merchantId, shopId, commissionPre, appType);
        if (AppTypeEnum.JKMF.getCode() == appType) {
            return simpleResponse;
        }
        if (ApiStatusEnum.SUCCESS.getCode() == simpleResponse.getCode()) {
            Product product = goodsService.findById(productId);
            String parentSpecBySpec = goodsService.getParentSpecBySpec(product.getProductSpecs());
            stringRedisTemplate.opsForZSet().remove(SellerPlateGoodsDistributionApiServiceImpl.NEW_KEY + ":" + shopId, productId.toString());
            stringRedisTemplate.delete(SellerPlateGoodsDistributionApiServiceImpl.NEW_KEY_SPECS + ":" + shopId + ":" + parentSpecBySpec);
            stringRedisTemplate.delete(SellerPlateGoodsDistributionApiServiceImpl.NEW_KEY_SPECS + ":" + shopId + ":" + SpeceTypeEnum.DEFALT.getCode());
        }
        return simpleResponse;
    }

    /**
     * 分销市场--加入仓库
     *
     * @param productId
     * @param merchantId
     * @param shopId
     * @return
     */
    @PostMapping(value = "market/storage")
    public SimpleResponse storage(@RequestParam("productId") Long productId,
                                  @RequestParam("merchantId") Long merchantId,
                                  @RequestParam("shopId") Long shopId) {
        return sellerGoodsDistributionV5Service.storage(productId, merchantId, shopId);
    }

    //########################################################################################
    // 供应商
    //########################################################################################

    /**
     * 供应商-建立推广分销
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "provider/setDistribution")
    public SimpleResponse setDistributionByProvider(@RequestBody ProductDTO dto) {
        Long merchantId = dto.getMerchantId();
        Long shopId = dto.getShopId();
        Long productId = dto.getProductId();
        BigDecimal selfDistributionRadio = dto.getSelfSupportRatio();
        BigDecimal platDistributionRadio = dto.getPlatSupportRatio();
        SimpleResponse simpleResponse;
        if (selfDistributionRadio == null && platDistributionRadio == null) {
            return new SimpleResponse(1, "平台分销佣金和店铺分销佣金,不能都是空");
        }
        if (selfDistributionRadio != null) {
            simpleResponse = this.auditRadio(selfDistributionRadio, "请设置正确的分销佣金比例（1%≤佣金比例＜100%）");
            if (simpleResponse != null) {
                return simpleResponse;
            }
        }
        if (platDistributionRadio != null) {
            simpleResponse = this.auditRadio(platDistributionRadio, "请设置正确的分销佣金比例（1%≤佣金比例＜100%）");
            if (simpleResponse != null) {
                return simpleResponse;
            }
        }

        return goodsService.setDistributionRadio(merchantId, shopId, productId, selfDistributionRadio, platDistributionRadio);
    }


    /**
     * 供应商-终止分销
     * <pre>
     *     自营分销属性置空
     *     平台分销属性置空
     *     但是: 该商品还在售   与下架的区别
     * </pre>
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "provider/stopDistribution")
    public SimpleResponse stopDistributionByProvider(@RequestBody ProductDTO dto) {
        Long productId = dto.getProductId();
        Long shopId = dto.getShopId();
        Long merchantId = shopAppService.getMerchantIdByShopId(shopId);
        if (merchantId == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "服务调用异常，请稍后重试");
        }
        return goodsService.diststop(productId, merchantId, shopId);
    }


    //########################################################################################
    // 分销商
    //########################################################################################

    /**
     * 自营商品-点击建立推广-展示页面的  数据获取
     *
     * @param merchantId .
     * @param shopId     .
     * @param productId  .
     * @return .
     */
    @GetMapping(value = "getInfoBeforeSetDistri")
    public SimpleResponse beforedist(@RequestParam("merchantId") String merchantId,
                                     @RequestParam("shopId") String shopId,
                                     @RequestParam(value = "productId") String productId) {
        Assert.isNumber(merchantId, 1, "商户id错误");
        Assert.isNumber(shopId, 2, "店铺id错误");
        Assert.isNumber(productId, 3, "产品错误");
        Assert.isTrue(Long.valueOf(productId) != 0, 4, "产品ID传输错误");
        return goodsService.getBeforedistInfo(merchantId, shopId, productId);
    }

    /**
     * 分销商-设置分销比例
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "distributor/setDistribution")
    public SimpleResponse setDistributionByDistributor(@RequestBody ProductDTO dto) {
        Long merchantId = dto.getMerchantId();
        Long shopId = dto.getShopId();
        Long productId = dto.getProductId();
        BigDecimal selfSupportRatio = dto.getSelfSupportRatio();
        SimpleResponse simpleResponse = this.auditRadio(selfSupportRatio, "请设置正确的分销佣金比例（1%≤佣金比例＜100%）");
        if (simpleResponse != null) {
            return simpleResponse;
        }
        Integer statInMarketWannaOnSale = dto.getStatInMarketWannaOnSale();//1:上架 2:仓库
        if (statInMarketWannaOnSale == null) {
            return new SimpleResponse(2, "参数:getStatInMarketWannaOnSale缺失,1:上架2:仓库");
        }
        // 这个接口应该只有分销商设置抽佣才会调用，所以这里需要把用户输入的抽佣比例替换成分销比例
        selfSupportRatio = new BigDecimal("100").subtract(selfSupportRatio);
        return goodsService.setDistributionByDistributor(merchantId, shopId, productId, selfSupportRatio, statInMarketWannaOnSale);
    }


    /**
     * 校验佣金比例
     *
     * @param radio    佣金比例
     * @param errorMsg 错误信息
     * @return .
     */
    private SimpleResponse auditRadio(BigDecimal radio, String errorMsg) {
        if (radio != null) {
            if (radio.compareTo(new BigDecimal(1)) < 0) {
                return new SimpleResponse(1, errorMsg);
            }
            if (radio.compareTo(new BigDecimal(100)) >= 0) {
                return new SimpleResponse(1, errorMsg);
            }
        }
        return null;
    }

}
