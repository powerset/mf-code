package mf.code.goods.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.service.GoodsPosterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.goods.api.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月16日 13:56
 */
@RestController
@RequestMapping("/api/goods/applet/v5")
@Slf4j
public class GoodsPosterApi {
    @Autowired
    private GoodsPosterService goodsPosterService;

    @GetMapping(path = "/sharePoster")
    public SimpleResponse sharePoster(@RequestParam(name = "merchantId") Long merchantID,
                                      @RequestParam(name = "shopId") Long shopID,
                                      @RequestParam(name = "goodsId", defaultValue = "0", required = false) Long goodsId,
                                      @RequestParam(name = "from") Long fromID,
                                      @RequestParam(name = "userId") Long userID) {
        return this.goodsPosterService.sharePoster(merchantID, shopID, goodsId, userID, fromID);
    }

}
