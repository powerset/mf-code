package mf.code.goods.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.goods.api.feignclient.UserAppService;
import mf.code.user.dto.UserResp;
import org.springframework.stereotype.Component;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class UserAppFeignFallbackFactory implements FallbackFactory<UserAppService> {
    @Override
    public UserAppService create(Throwable throwable) {
        return new UserAppService() {
            @Override
            public UserResp queryUser(Long userId) {
                return null;
            }

        };
    }
}
