package mf.code.goods.api.plat;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.annotation.Upload;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.CategoryDistributionDTO;
import mf.code.goods.dto.PlatBannerPicDTO;
import mf.code.goods.service.CategoryGroupMappingService;
import mf.code.goods.service.PlateFromDistributionGoodsApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/goods/platform/v1/category")
@Slf4j
public class PlateFromDistributionGoodsApi {
    @Autowired
    PlateFromDistributionGoodsApiService plateFromDistributionGoodsApiService;
    @Autowired
    private CategoryGroupMappingService categoryGroupMappingService;

    /**
     * 上传推荐图片
     */

    @Upload
    @PostMapping(value = "uploadPicture")
    public SimpleResponse uploadPicture(@RequestParam(value = "image") MultipartFile image,
                                        @RequestParam(value = "key", required = false) String key,
                                        @RequestParam(value = "type", required = false) String type) {

        SimpleResponse response = plateFromDistributionGoodsApiService.uploadPicture(image, key, type);

        return response;
    }

    /**
     * 保存上传的图片--平台分销
     *
     * @return
     */
    @PostMapping(value = "saveUploadPics")
    public SimpleResponse saveUploadPics(@RequestBody PlatBannerPicDTO pic){
        if(null==pic){
            return  new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
        }

        List<String> minPicsList = new ArrayList<>();
        try{
            minPicsList= JSON.parseArray(pic.getMinPics(), String.class);
        }catch (RuntimeException e){
            log.error("have find saveUploadPics getMinPics to json error",e);
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

//        //图片小于1时不保存
//        if(CollectionUtils.isEmpty(minPicsList) || minPicsList.size()<1){
//            return  new SimpleResponse(ApiStatusEnum.ERROR.getCode(),"图片不能少于1张");
//        }

        String mainPicsList = pic.getMinPics();
        String key = pic.getSid();
        String type = pic.getType();

        SimpleResponse response = plateFromDistributionGoodsApiService.saveUploadPics(mainPicsList,key,type);

        return response;
    }

    @GetMapping(value = "showHaveUploadPics")
    public SimpleResponse showHaveUploadPics(@RequestParam(value = "platformId", required = false) String plateFromId,@RequestParam(value = "key", required = false) String key,
                                             @RequestParam(value = "type", required = false) String type){
        SimpleResponse response = plateFromDistributionGoodsApiService.showHaveUploadPics(key,type);
        return response;
    }


    /**
     * 获取新类目信息
     *
     * @param plateFromId
     * @return
     */
    @GetMapping(value = "getNewSpecesInfo")
    public SimpleResponse getNewSpecesInfo(@RequestParam(value = "platformId", required = false) String plateFromId) {
        return plateFromDistributionGoodsApiService.getNewSpecesInfo(plateFromId);
    }

    /**
     * 根据类目ID修改类目表信息
     *
     * @param specesId
     * @return
     */

    @PostMapping(value = "updateSpecesInfo")
    public SimpleResponse updateSpecesInfo(@RequestParam(name = "specesId") String specesId,
                                           @RequestParam(value = "ratio") BigDecimal ratio) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (ratio == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请输入抽佣比例");
            return simpleResponse;
        }
        if (ratio.intValue() > 100 || ratio.intValue() < 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("抽佣比例填写不合法");
            return simpleResponse;
        }
        CategoryDistributionDTO categoryDTO = categoryGroupMappingService.updateZeroLevelRatioAndSort(specesId, ratio, null);
        if(categoryDTO == null){
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("保存抽佣比例失败");
            return simpleResponse;
        }
        simpleResponse.setData(categoryDTO);
        return simpleResponse;
    }


}
