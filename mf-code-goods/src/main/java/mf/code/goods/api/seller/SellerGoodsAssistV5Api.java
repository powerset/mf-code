package mf.code.goods.api.seller;/**
 * create by qc on 2019/4/9 0009
 */

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.service.SellerGoodsAssistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author gbf
 * 2019/4/9 0009、14:48
 */
@RestController
@RequestMapping("/api/goods/seller/v5/goods/assist")
public class SellerGoodsAssistV5Api {
    @Autowired
    private SellerGoodsAssistService sellerGoodsAssistService;


    /**
     * 列表头部的统计数据 仓库中数量/在售中数量
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping(value = "count")
    public SimpleResponse count(@RequestParam(value = "merchantId") Long merchantId,
                                @RequestParam(value = "shopId") Long shopId) {
        return sellerGoodsAssistService.count(merchantId, shopId);
    }

    /**
     * 更新库存-前置查询
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @param productId  产品id
     * @return .
     */
    @GetMapping(value = "updatestock/before")
    public SimpleResponse updateStockBefore(@RequestParam(value = "merchantId") Long merchantId,
                                            @RequestParam(value = "shopId") Long shopId,
                                            @RequestParam(value = "productId") Long productId) {
        return sellerGoodsAssistService.updateStockBefore(merchantId,shopId,productId);
    }

    /**
     * 更新价格-前置查询
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @param productId  产品id
     * @return .
     */
    @GetMapping(value = "updateprice/before")
    public SimpleResponse updatePriceBefore(@RequestParam(value = "merchantId") Long merchantId,
                                            @RequestParam(value = "shopId") Long shopId,
                                            @RequestParam(value = "productId") Long productId) {
        return sellerGoodsAssistService.updatePriceBefore(merchantId,shopId,productId);
    }

    /**
     *获取规格名
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return .
     */
    @GetMapping(value = "productSpecs")
    public SimpleResponse productSpecs(@RequestParam(value = "merchantId") Long merchantId,
                                       @RequestParam(value = "shopId") Long shopId) {
        return sellerGoodsAssistService.productSpecs(merchantId,shopId);
    }

    /**
     * 上报规格值
     * @param merchantId
     * @param shopId
     * @param parentId
     * @param specName
     * @return
     */
    @PostMapping(value = "postSpecValue")
    public SimpleResponse postSpecValue(@RequestParam(value = "merchantId") Long merchantId,
                                        @RequestParam(value = "shopId") Long shopId,
                                        @RequestParam(value = "parentId") Long parentId,
                                        @RequestParam(value = "specName") String specName){
        Assert.hasText(specName,1,"规格值不能为空");
        return sellerGoodsAssistService.postSpecValue(merchantId,shopId,parentId,specName);
    }

    /**
     * 审核原因-查看-商户  注意:平台有不同接口
     */
    @GetMapping(value = "getCheckReason")
    public SimpleResponse getCheckReason(@RequestParam(value = "productId")Long productId){
        return sellerGoodsAssistService.getCheckReason(productId);
    }


}
