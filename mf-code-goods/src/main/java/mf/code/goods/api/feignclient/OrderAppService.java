package mf.code.goods.api.feignclient;

import mf.code.goods.api.feignclient.fallback.OrderAppFeignFallbackFactory;
import mf.code.goods.common.config.feign.FeignLogConfiguration;
import mf.code.order.dto.OrderResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@FeignClient(name = "mf-code-order", fallbackFactory = OrderAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OrderAppService {

    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByOrder")
    int countGoodsNumByOrder(@RequestParam(name = "goodsId") Long goodsId);

    /**
     * 通过orderid 获取订单信息
     *
     * @param orderId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrder")
    OrderResp queryOrder(@RequestParam(name = "orderId") Long orderId);

    /**
     * 根据店铺id,产品id统计订单数
     *
     * @param shopId
     * @param skuIdList
     * @return int统计数量
     */
    @GetMapping("/feignapi/order/applet/v5/summaryOrderByShopIdAndProductId")
    int summaryOrderByShopIdAndProductId(@RequestParam(name = "shopId") Long shopId,
                                         @RequestParam(name = "skuIdList") List<Long> skuIdList);

    /**
     * 获取 此用户在此店铺的 所有 可结算订单 -- 进行结算处理
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderSettledByUserIdShopId")
    List<OrderResp> queryOrderSettledByUserIdShopId(@RequestParam("userId") Long userId,
                                                    @RequestParam("shopId") Long shopId);


    /**
     * 查询 用户 在此店铺得 本月的消费记录
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderRecordThisMonth")
    Map<String, OrderResp> queryOrderRecordThisMonth(@RequestParam("userId") Long userId,
                                                     @RequestParam("shopId") Long shopId);

    /**
     * 通过商品id 获取商品销量
     *
     * @param productIds
     * @return Map<productId   ,       salesVolume>
     */
    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByProductIds")
    Map<Long, Integer> countGoodsNumByProductIds(@RequestParam("productIds") List<Long> productIds);

    /***
     * 获取某商品集合的订单数
     * @param shopId
     * @param userId
     * @param productIds
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderNumByProductIds")
    Integer queryOrderNumByProductIds(@RequestParam("shopId") Long shopId,
                                      @RequestParam("userId") Long userId,
                                      @RequestParam("productIds") List<Long> productIds);
}
