package mf.code.goods.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.goods.api.feignclient.ShopAppService;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.dto.MerchantOrderReq;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.shop.dto.ShopDetail;
import mf.code.shop.dto.ShopDetailForPlatformDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class ShopAppFeignFallbackFactory implements FallbackFactory<ShopAppService> {
    @Override
    public ShopAppService create(Throwable throwable) {
        return new ShopAppService() {
            @Override
            public Map<String, Object> queryShopInfoByLogin(String shopId) {
                return new HashMap<>(1);
            }

            @Override
            public Long getMerchantIdByShopId(Long shopId) {
                return null;
            }

            @Override
            public List<Long> selectAllIds() {
                return new ArrayList<>();
            }

            @Override
            public Integer updateMerchantBalance(MerchantOrderReq merchantOrderReq) {
                return null;
            }

            /**
             * 根据商家帐号即手机号获取商户id
             *
             * @param phone
             * @return
             */
            @Override
            public Long getMerchantIdByPhone(String phone) {
                return null;
            }

            /**
             * 根据店铺名称
             *
             * @param shopName
             * @return
             */
            @Override
            public List<Long> getShopIdListByShopName(String shopName) {
                return new ArrayList<>();
            }

            /**
             * 根据店铺id查询店铺详情
             *
             * @param shopIdList
             * @return
             */
            @Override
            public List<ShopDetailForPlatformDTO> getShopListByShopIdList(List<Long> shopIdList) {
                return new ArrayList<>();
            }

            @Override
            public MerchantDTO queryMerchantById(Long merchantId) {
                return null;
            }

            @Override
            public MerchantShopDTO findShopByShopId(Long shopId) {
                return null;
            }
        };
    }
}
