package mf.code.goods.api.applet.feignservice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.NumberValidationUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.common.utils.RobotUserUtil;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.api.feignclient.DistributionAppService;
import mf.code.goods.api.feignclient.OrderAppService;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.common.redis.RedisForbidRepeat;
import mf.code.goods.common.redis.RedisKeyConstant;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.*;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.po.ProductSpecs;
import mf.code.goods.repo.po.ProductSpecsName;
import mf.code.goods.repo.repostory.AppletProductRepository;
import mf.code.goods.repo.repostory.SellerProductRepository;
import mf.code.goods.repo.repostory.SellerProductSpecsRepository;
import mf.code.goods.repo.repostory.SellerSkuRepository;
import mf.code.goods.repo.service.ProductService;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.GoodsSkuAboutService;
import mf.code.goods.service.GoodsSkuService;
import mf.code.goods.service.GoodsUpdateService;
import mf.code.one.dto.MsInfoDTO;
import mf.code.shop.constants.AppTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.goods.api.applet.feignservice
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 14:42
 */
@Slf4j
@RestController
public class GoodsAppServiceImpl {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private GoodsSkuAboutService goodsSkuAboutService;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private SellerSkuRepository sellerSkuRepostory;
    @Autowired
    private SellerProductRepository sellerProductRepostory;
    @Autowired
    private GoodsSkuService goodsSkuService;
    @Autowired
    private SellerProductSpecsRepository sellerProductSpecsRepository;
    @Autowired
    private AppletProductRepository appletProductRepository;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private GoodsUpdateService goodsUpdateService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GoodsService goodsService;

    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /**
     * 根据传入skuId，查询或者创建新的sku，平台免单抽奖专用
     */
    @GetMapping("/feignapi/goods/applet/v8/createSkuForPlatformOrderBack")
    public ProductSkuEntity createSkuForPlatformOrderBack(@RequestParam("productSkuId") Long productSkuId,
                                                          @RequestParam("stockDef") Integer stockDef) {
        if (productSkuId == null || productSkuId < 1) {
            log.error("参数异常");
            return null;
        }
        ProductSku productSku = sellerSkuRepostory.getById(productSkuId);
        if (productSku == null) {
            log.error("查无此sku");
            return null;
        }
        ProductSku newProductSku = new ProductSku();
        BeanUtils.copyProperties(productSku, newProductSku);
        newProductSku.setNotShow(ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK);
        newProductSku.setOldSkuId(productSku.getId());
        newProductSku.setStockDef(stockDef);
        newProductSku.setStock(stockDef);
        newProductSku.setUtime(new Date());
        newProductSku.setCtime(new Date());
        int i = sellerSkuRepostory.insertSelective(newProductSku);
        if (i == 0) {
            log.error("数据库存储失败");
            return null;
        }
        ProductSkuEntity productSkuEntity = new ProductSkuEntity();
        BeanUtils.copyProperties(newProductSku, productSkuEntity);
        return productSkuEntity;
    }


    /**
     * 通过主键 批量获取商品信息
     *
     * @param goodsIds
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v8/listByIds")
    public List<ProductEntity> listByIds(@RequestParam("goodsIds") List<Long> goodsIds) {
        if (CollectionUtils.isEmpty(goodsIds)) {
            log.error("参数异常");
            return null;
        }
        Collection<Product> products = productService.listByIds(goodsIds);
        if (CollectionUtils.isEmpty(products)) {
            return null;
        }
        List<ProductEntity> goodProductDTOList = new ArrayList<>();
        for (Product product : products) {
            ProductEntity goodProductDTO = new ProductEntity();
            BeanUtils.copyProperties(product, goodProductDTO);
            goodProductDTOList.add(goodProductDTO);
        }
        return goodProductDTOList;
    }

    /***
     * 随机查询一个在售的sku
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/queryRandomSkuOnsale")
    public GoodsSkuDTO queryRandomSkuOnsale() {
        log.info("<<<<<<<<随机查询一个在售开始");
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.lambda()
                .eq(Product::getStatus, 1)
                .eq(Product::getCheckStatus, 1)
                .eq(Product::getShowType, ProductShowTypeEnum.NONE.getCode())
                .eq(Product::getDel, DelEnum.NO.getCode())
        ;
        int count = productService.count(productQueryWrapper);
        log.info("<<<<<<<<查询到商品的总数 count:{}", count);
        if (count == 0) {
            return null;
        }
        int size = 30;
        //计算总页码
        int pageSum = count % size == 0 ? count / size : (int) Math.floor(count / size) + 1;
        //随机取页码 区间[m,n]
        int randomPage = NumberUtils.toInt(RobotUserUtil.getRandom(1, pageSum));
        Page<Product> productPage = new Page<Product>(randomPage, size);
        IPage<Product> iPage = productService.page(productPage, productQueryWrapper);
        List<Product> products = iPage.getRecords();
        log.info("<<<<<<<<查询到所需的商品集合 product_size:{}", products.size());
        if (CollectionUtils.isEmpty(products)) {
            return null;
        }
        //随机取一个商品
        int randomProduct = NumberUtils.toInt(RobotUserUtil.getRandom(0, products.size()));
        Product product = products.get(randomProduct);
        log.info("<<<<<<<<查询到所需的商品 product:{}", product);
        if (product == null) {
            log.error("<<<<<<<<随机取一个商品异常：products_size: {}, randomProduct:{} ", products.size(), randomProduct);
            return null;
        }

        //获取sku信息 not_show 0显示 1不显示 snapshoot 0非快照 1快照
        QueryWrapper<ProductSku> skuQueryWrapper = new QueryWrapper<>();
        skuQueryWrapper.lambda()
                .eq(ProductSku::getProductId, product.getParentId())
                .eq(ProductSku::getSnapshoot, 0)
                .eq(ProductSku::getDel, DelEnum.NO.getCode())
        ;
        List<ProductSku> productSkus = this.productSkuMapper.selectList(skuQueryWrapper);
        if (CollectionUtils.isEmpty(productSkus)) {
            log.info("<<<<<<<<该商品下未创建sku：productId:{} ", product.getId());
            return null;
        }
        //随机一个sku
        int randomSku = NumberUtils.toInt(RobotUserUtil.getRandom(0, productSkus.size()));
        ProductSku productSku = productSkus.get(randomSku);
        log.info("<<<<<<<<查询到所需的商品 sku信息 productSku:{}", productSku);
        if (productSku == null) {
            log.error("<<<<<<<<随机取一个商品异常：productSku_size: {}, randomSku:{} ", productSkus.size(), randomSku);
            return null;
        }

        List<Long> specsValues = new ArrayList<>();
        String[] split = productSku.getSpecSort().split(",");
        for (int i = 0; i < split.length; i++) {
            String[] split1 = split[i].split(":");
            //获取类目下的属性
            if (RegexUtils.StringIsNumber(split1[1])) {
                specsValues.add(NumberUtils.toLong(split1[1]));
            }
        }

        GoodsSkuDTO resp = new GoodsSkuDTO();
        resp.setSkus(this.goodsSkuAboutService.getSkuSpecs(specsValues));
        resp.setCashback("0.00");
        resp.setPrice(productSku.getSkuSellingPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setOriginalPrice(productSku.getSkuSellingPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setTitle(product.getProductTitle());
        if (StringUtils.isNotBlank(productSku.getSkuPic())) {
            resp.setPic(productSku.getSkuPic());
        } else {
            //取商品主图的第一张
            List<String> strPics = JSONObject.parseArray(product.getMainPics(), String.class);
            resp.setPic(strPics.get(0));
        }
        //库存数
        resp.setStock(productSku.getStock());
        resp.setShopId(product.getShopId());
        resp.setMerchantId(product.getMerchantId());
        resp.setGoodsId(product.getId());
        resp.setSkuId(productSku.getId());
        log.info("<<<<<<<<skuId:{}, skuInfo:{}", productSku.getId(), resp);
        return resp;
    }

    @GetMapping("/feignapi/goods/applet/v5/queryProductSkuList")
    public Map<Long, GoodsSkuDTO> queryProductSkuList(@RequestParam("merchantId") Long merchantId,
                                                      @RequestParam("shopId") Long shopId,
                                                      @RequestParam("goodsId") List<Long> goodsIds) {
        log.info("<<<<<<<<开始查询goodsIds编号 {} 的信息 ", goodsIds);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.lambda()
                .eq(Product::getMerchantId, merchantId)
                .eq(Product::getShopId, shopId)
                .in(Product::getId, goodsIds)
        ;

        List<Product> products = this.productMapper.selectList(productQueryWrapper);
        if (CollectionUtils.isEmpty(products)) {
            log.error("<<<<<<<< 商品集合不存在", goodsIds);
            return null;
        }

        List<Long> parentProductIds = new ArrayList<>();
        Map<Long, Product> parentProductMap = new HashMap<>();
        for (Product product : products) {
            parentProductMap.put(product.getParentId(), product);
            if (parentProductIds.indexOf(product.getParentId()) == -1) {
                parentProductIds.add(product.getParentId());
            }
        }

        QueryWrapper<ProductSku> skuQueryWrapper = new QueryWrapper<>();
        skuQueryWrapper.lambda()
                .in(ProductSku::getProductId, parentProductIds)
        ;
        List<ProductSku> productSkus = productSkuMapper.selectList(skuQueryWrapper);
        if (CollectionUtils.isEmpty(productSkus)) {
            log.error("<<<<<<<< 商品sku集合不存在", goodsIds);
            return null;
        }

        Map<Long, GoodsSkuDTO> resps = new HashMap<>();
        for (ProductSku productSku : productSkus) {
            Product product = parentProductMap.get(productSku.getProductId());
            List<Long> specsValues = new ArrayList<>();
            String[] split = productSku.getSpecSort().split(",");
            for (int i = 0; i < split.length; i++) {
                String[] split1 = split[i].split(":");
                //获取类目下的属性
                if (RegexUtils.StringIsNumber(split1[1])) {
                    specsValues.add(NumberUtils.toLong(split1[1]));
                }
            }
            GoodsSkuDTO resp = new GoodsSkuDTO();
            resp.setSkus(this.goodsSkuAboutService.getSkuSpecs(specsValues));
            resp.setPrice(productSku.getSkuSellingPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.setOriginalPrice(productSku.getSkuSellingPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.setTitle(product != null ? product.getProductTitle() : "");
            if (StringUtils.isNotBlank(productSku.getSkuPic())) {
                resp.setPic(productSku.getSkuPic());
            } else {
                //取商品主图的第一张
                List<String> strPics = JSONObject.parseArray(product.getMainPics(), String.class);
                resp.setPic(strPics.get(0));
            }
            //库存数
            resp.setStock(productSku.getStock());
            resp.setShopId(shopId);
            resps.put(productSku.getId(), resp);
            log.info("<<<<<<<<skuId:{}, skuInfo:{}", productSku.getId(), resp);
        }
        return resps;
    }

    @GetMapping("/feignapi/goods/applet/v5/queryProductSku")
    public GoodsSkuDTO queryProductSku(@RequestParam("merchantId") Long merchantId,
                                       @RequestParam("shopId") Long shopId,
                                       @RequestParam("goodsId") Long goodsId,
                                       @RequestParam("skuId") Long skuId,
                                       @RequestParam("userId") Long userId) {
        log.info("<<<<<<<<开始查询sku编号 {} 的信息 ", skuId);
        QueryWrapper<ProductSku> skuQueryWrapper = new QueryWrapper<>();
        skuQueryWrapper.lambda()
                .eq(ProductSku::getId, skuId)
        ;
        ProductSku productSku = this.productSkuMapper.selectOne(skuQueryWrapper);
        if (productSku == null) {
            log.error("<<<<<<<< skuid{} 的sku不存在", skuId);
            return null;
        }
        Product product = goodsUpdateService.getProductForPlatMall(merchantId, shopId, goodsId, AppTypeEnum.JKMF);
        if (!goodsId.equals(product.getId())) {
            log.error("<<<<<<<< goodsId{} 平台商城商品创建，结果goodsId{}", goodsId, product.getId());
        }
        //获取该用户该店铺本月收益信息(查询 在此店铺内， 用户本月消费和收入的概况)
        ProductDistributionDTO distributionDTO = new ProductDistributionDTO();
        distributionDTO.setParentId(product.getParentId());
        distributionDTO.setProductId(productSku.getId());
        distributionDTO.setSelfSupportRatio(product.getSelfSupportRatio());
        distributionDTO.setPlatSupportRatio(product.getPlatSupportRatio());
        distributionDTO.setPrice(productSku.getSkuSellingPrice());
        distributionDTO.setShopId(shopId);
        distributionDTO.setUserId(userId);
        String rebateCommission = distributionAppService.getRebateCommission(distributionDTO);

        List<Long> specsValues = new ArrayList<>();
        String[] split = productSku.getSpecSort().split(",");
        for (int i = 0; i < split.length; i++) {
            String[] split1 = split[i].split(":");
            //获取类目下的属性
            if (RegexUtils.StringIsNumber(split1[1])) {
                specsValues.add(NumberUtils.toLong(split1[1]));
            }
        }
        GoodsSkuDTO resp = new GoodsSkuDTO();
        resp.setSkus(this.goodsSkuAboutService.getSkuSpecs(specsValues));
        resp.setNotShow(productSku.getNotShow());
        resp.setCashback(rebateCommission);
        resp.setPrice(productSku.getSkuSellingPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setOriginalPrice(productSku.getSkuSellingPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setTitle(product.getProductTitle());
        if (StringUtils.isNotBlank(productSku.getSkuPic())) {
            resp.setPic(productSku.getSkuPic());
        } else {
            //取商品主图的第一张
            List<String> strPics = JSONObject.parseArray(product.getMainPics(), String.class);
            resp.setPic(strPics.get(0));
        }
        //库存数
        resp.setStock(productSku.getStock());
        resp.setShopId(shopId);
        log.info("<<<<<<<<skuId:{}, skuInfo:{}", skuId, resp);
        return resp;
    }

    @GetMapping("/feignapi/goods/applet/v5/queryProduct")
    public GoodProductDTO queryProduct(@RequestParam("merchantId") Long merchantId,
                                       @RequestParam("shopId") Long shopId,
                                       @RequestParam("goodsId") Long goodsId) {
        Product product = goodsUpdateService.getProductForPlatMall(merchantId, shopId, goodsId, AppTypeEnum.JKMF);
        if (product == null) {
            return null;
        }
        // 组装参数
        GoodProductDTO resp = new GoodProductDTO();
        BeanUtils.copyProperties(product, resp);
        resp.setMerchantId(merchantId);
        resp.setShopId(shopId);
        resp.setSelfSupportRatio(product.getSelfSupportRatio());
        resp.setPlatSupportRatio(product.getPlatSupportRatio());
        resp.setGoodsId(product.getId());
        resp.setTitle(product.getProductTitle());
        resp.setPrice(product.getProductPriceMin().toString());
        resp.setOriginalPrice(product.getThirdPriceMin().toString());
        resp.setSaleQuantity(product.getOrderSales() + product.getInitSales());
        resp.setCommissionNum(product.getCommissionNum());
        if (StringUtils.isNotBlank(product.getMainPics())) {
            resp.setGoodsPics(JSONObject.parseArray(product.getMainPics(), String.class));
        }
        if (StringUtils.isNotBlank(product.getDetailPics())) {
            resp.setPicDetail(JSONObject.parseArray(product.getDetailPics(), String.class));
        }
        if (product.getParentId() != null && product.getParentId() > 0) {
            Product parentProduct = this.productMapper.selectByPrimaryKey(product.getParentId());
            if (parentProduct != null) {
                resp.setParentId(parentProduct.getId());
                resp.setParentMerchantId(parentProduct.getMerchantId());
                resp.setParentShopId(parentProduct.getShopId());
            }
        } else {
            resp.setParentId(product.getId());
            resp.setParentMerchantId(product.getMerchantId());
            resp.setParentShopId(product.getShopId());
        }
        return resp;
    }

    /***
     * 获取店铺的商品编号集合
     * @param shopId
     * @param type 0:普通商品 1：新人专享商品 ProductShowTypeEnum枚举里
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/getProductIds")
    public List<Long> getProductIds(@RequestParam("shopId") Long shopId,
                                    @RequestParam(value = "type", defaultValue = "0") Integer type) {
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("showType", type);
        List<Long> productIds = productMapper.queryProductIds(param);
        if (CollectionUtils.isEmpty(productIds)) {
            return null;
        }
        return productIds;
    }

    /**
     * 按销量从大到小 分页展示 所有可售商品
     *
     * @param shopId
     * @param type   0:普通商品 1：新人专享商品 ProductShowTypeEnum枚举里
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/pageGoodsSaleWithSalesVolumeDesc")
    public AppletUserProductRebateDTO pageGoodsSaleWithSalesVolumeDesc(@RequestParam("shopId") Long shopId,
                                                                       @RequestParam(value = "type", defaultValue = "0") Integer type,
                                                                       @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                                                       @RequestParam(value = "size", defaultValue = "16") Long size) {
        if (shopId == null || shopId <= 0) {
            return null;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("showType", type);
        param.put("offset", offset);
        param.put("size", size);
        List<ProductDistributionDTO> productDistributionDTOList = null;
        productDistributionDTOList = appletProductRepository.pageWithSalesVolumeDescForListOptimize(param);
        if (productDistributionDTOList == null) {
            productDistributionDTOList = new ArrayList<>();
        }

        QueryWrapper<Product> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .in(Product::getFromFlag, Arrays.asList(ProductFromFlagEnum.SELF.getCode(), ProductFromFlagEnum.PLAT.getCode()))
                .eq(Product::getDel, DelEnum.NO.getCode())
                .eq(Product::getShowType, type)
        ;
        Integer total = productMapper.selectCount(wrapper);

        AppletUserProductRebateDTO appletUserProductRebateDTO = new AppletUserProductRebateDTO();
        appletUserProductRebateDTO.setShopId(shopId);
        appletUserProductRebateDTO.setOffset(offset);
        appletUserProductRebateDTO.setSize(size);
        appletUserProductRebateDTO.setTotal(total);

        appletUserProductRebateDTO.setProductDistributionDTOList(productDistributionDTOList);

        return appletUserProductRebateDTO;
    }

    /**
     * 获取 商品分销属性
     *
     * @param goodsId
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/queryGoodsDistribution")
    public ProductDistributionDTO queryGoodsDistribution(@RequestParam("goodsId") String goodsId) {
        if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)) {
            log.error("参数异常");
            return null;
        }
        Product product = productMapper.selectById(goodsId);
        if (product == null) {
            log.error("无法获取商品信息 productId = {}", goodsId);
            return null;
        }

        ProductDistributionDTO distributionDTO = new ProductDistributionDTO();
        distributionDTO.setMerchantId(product.getMerchantId());
        distributionDTO.setPrice(BigDecimal.ZERO);
        distributionDTO.setShopId(product.getShopId());
        distributionDTO.setProductId(product.getId());
        distributionDTO.setParentId(product.getParentId());
        distributionDTO.setSelfSupportRatio(product.getSelfSupportRatio());
        distributionDTO.setPlatSupportRatio(product.getPlatSupportRatio());

        return distributionDTO;
    }

    /**
     * 商户根据skuId列表查询商品信息
     *
     * @param skuIdList
     * @return
     */
    @GetMapping("/feignapi/goods/seller/v5/queryProductSkuMapBySkuIdList")
    public SimpleResponse queryProductSkuMapBySkuIdList(@RequestParam("skuIdList") List<Long> skuIdList) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (CollectionUtils.isEmpty(skuIdList)) {
            return simpleResponse;
        }
        List<ProductSku> productSkuList = sellerSkuRepostory.selectBatchIds(skuIdList);
        if (CollectionUtils.isEmpty(productSkuList)) {
            return simpleResponse;
        }
        Set<Long> productIdSet = new HashSet<>();
        Set<Long> specsIdSet = new HashSet<>();
        Set<Long> specsNameIdSet = new HashSet<>();
        for (ProductSku productSku : productSkuList) {
            productIdSet.add(productSku.getProductId());
            // 查询规格
            String[] split = productSku.getSpecSort().split(",");
            for (int i = 0; i < split.length; i++) {
                String[] split1 = split[i].split(":");
                // 类目
                specsNameIdSet.add(NumberUtils.toLong(split1[0]));
                //获取类目下的属性
                if (RegexUtils.StringIsNumber(split1[1])) {
                    specsIdSet.add(NumberUtils.toLong(split1[1]));
                }
            }
        }
        List<Product> productList = sellerProductRepostory.selectBatchIds(new ArrayList<>(productIdSet));
        if (CollectionUtils.isEmpty(productList)) {
            return simpleResponse;
        }
        List<ProductSpecs> productSpecsList = sellerProductSpecsRepository.selectSpecsListByIdForSeller(new ArrayList<>(specsIdSet));
        if (CollectionUtils.isEmpty(productSpecsList)) {
            return simpleResponse;
        }
        List<ProductSpecsName> productSpecsNameList = sellerProductSpecsRepository.selectSpecsNameListByIdForSeller(new ArrayList<>(specsNameIdSet));
        if (CollectionUtils.isEmpty(productSpecsNameList)) {
            return simpleResponse;
        }
        // 组合成map-->key:productId;value:SellerProductAndSkuResultDTO
        Map<String, SellerProductAndSkuResultDTO> resultMap = new HashMap<>();
        Map<Long, Product> productMap = new HashMap<>();
        Map<Long, ProductSpecs> productSpecsMap = new HashMap<>();
        Map<Long, ProductSpecsName> productSpecsNameMap = new HashMap<>();
        // 方便后面遍历
        for (Product product : productList) {
            productMap.put(product.getId(), product);
        }
        for (ProductSpecs productSpecs : productSpecsList) {
            productSpecsMap.put(productSpecs.getId(), productSpecs);
        }
        for (ProductSpecsName productSpecsName : productSpecsNameList) {
            productSpecsNameMap.put(productSpecsName.getId(), productSpecsName);
        }
        for (ProductSku productSku : productSkuList) {
            SellerProductAndSkuResultDTO resultDTO = new SellerProductAndSkuResultDTO();
            resultDTO.setSkuId(productSku.getId());
            resultDTO.setNotShow(productSku.getNotShow());
            resultDTO.setProductId(productSku.getProductId());
            resultDTO.setProductPrice(productSku.getSkuSellingPrice());
            // 查询规格
            StringBuilder specsSortDesc = new StringBuilder();
            String[] split = productSku.getSpecSort().split(",");
            for (int i = 0; i < split.length; i++) {
                String[] split1 = split[i].split(":");
                // 类目
                ProductSpecsName productSpecsName = productSpecsNameMap.get(NumberUtils.toLong(split1[0]));
                ProductSpecs productSpecs = productSpecsMap.get(NumberUtils.toLong(split1[1]));
                specsSortDesc.append(productSpecsName.getName()).append(" : ").append(productSpecs.getSpecsValue()).append("  ");
            }
            resultDTO.setProductSpecs(productSku.getSpecSort());
            resultDTO.setProductSpecsDesc(specsSortDesc.toString());
            resultDTO.setProductSrc("自营商品");
            Product product = productMap.get(productSku.getProductId());
            if (!product.getId().equals(product.getParentId())) {
                resultDTO.setProductSrc("分销商品");
            }
            resultDTO.setParentProductId(product.getParentId());
            resultDTO.setProductSrcMainPics(product.getMainPics());
            resultDTO.setProductTitle(product.getProductTitle());
            resultMap.put(productSku.getId().toString(), resultDTO);
        }
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 消费库存
     *
     * @param productSkuId
     * @param stockNumber
     * @return
     */
    @GetMapping("/feignapi/goods/seller/v5/consumeStock")
    public SimpleResponse consumeStock(@RequestParam("productSkuId") Long productSkuId,
                                       @RequestParam("stockNumber") int stockNumber,
                                       @RequestParam("orderId") Long orderId) {
        return goodsSkuService.consumeStock(productSkuId, stockNumber, orderId);
    }

    /**
     * 还库存
     *
     * @param productSkuId
     * @param stockNumber
     * @return
     */
    @GetMapping("/feignapi/goods/seller/v5/giveBackStock")
    public SimpleResponse giveBackStock(@RequestParam("productSkuId") Long productSkuId,
                                        @RequestParam("stockNumber") int stockNumber,
                                        @RequestParam("orderId") Long orderId) {
        return goodsSkuService.giveBackStock(productSkuId, stockNumber, orderId);
    }

    /**
     * 根据商品id查询商品详情，全部字段对应db
     *
     * @param productIdList
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/queryProductDetailList")
    public List<ProductEntity> queryProductDetail(@RequestParam("productIdList") List<Long> productIdList) {
        if (CollectionUtils.isEmpty(productIdList)) {
            log.error("参数异常");
            return null;
        }
        List<Product> productList = this.sellerProductRepostory.selectBatchIds(productIdList);
        if (CollectionUtils.isEmpty(productList)) {
            return null;
        }
        List<ProductEntity> productEntityList = new ArrayList<>();
        for (Product product : productList) {
            ProductEntity productEntity = new ProductEntity();
            BeanUtils.copyProperties(product, productEntity);
            List<ProductSku> productSkuList = sellerSkuRepostory.selectByProductId(product.getParentId());
            if (CollectionUtils.isEmpty(productSkuList)) {
                return null;
            }
            List<ProductSkuEntity> productSkuEntityList = new ArrayList<>();
            for (ProductSku productSku : productSkuList) {
                ProductSkuEntity productSkuEntity = new ProductSkuEntity();
                BeanUtils.copyProperties(productSku, productSkuEntity);
                productSkuEntityList.add(productSkuEntity);
            }
            productEntity.setProductSkuList(productSkuEntityList);
            productEntityList.add(productEntity);
        }
        return productEntityList;
    }

    /**
     * 通过商品skuId 查询商品详情，全部字段对应db
     */
    @GetMapping("/feignapi/goods/applet/v5/queryProductDetailBySkuIds")
    public List<ProductEntity> queryProductDetailBySkuIds(@RequestParam("skuIdList") List<Long> skuIdList) {
        if (CollectionUtils.isEmpty(skuIdList)) {
            log.error("参数异常");
            return null;
        }
        Collection<ProductSku> productSkuList = this.sellerSkuRepostory.listByIds(skuIdList);
        if (CollectionUtils.isEmpty(productSkuList)) {
            log.error("通过skuId 未查询到信息 skuIds = {}", skuIdList);
            return null;
        }
        Set<Long> productIdList = new HashSet<>();
        for (ProductSku productSku : productSkuList) {
            productIdList.add(productSku.getProductId());
        }
        if (CollectionUtils.isEmpty(productIdList)) {
            log.error("商品id列表为空，productIdList = {}", productIdList);
            return null;
        }
        List<Product> productList = this.sellerProductRepostory.selectBatchIds(new ArrayList<>(productIdList));
        if (CollectionUtils.isEmpty(productList)) {
            log.error("通过productIdList 未查询到信息，productIdList = {}", productIdList);
            return null;
        }
        Map<Long, Product> productMap = new HashMap<>();
        for (Product product : productList) {
            productMap.put(product.getId(), product);
        }
        List<ProductEntity> productEntityList = new ArrayList<>();

        for (ProductSku productSku : productSkuList) {
            Product product = productMap.get(productSku.getProductId());
            if (product == null) {
                continue;
            }
            ProductEntity productEntity = new ProductEntity();
            BeanUtils.copyProperties(product, productEntity);

            productEntity.setSkuId(productSku.getId());

            ProductSkuEntity productSkuEntity = new ProductSkuEntity();
            BeanUtils.copyProperties(productSku, productSkuEntity);

            List<ProductSkuEntity> productSkuEntityList = new ArrayList<>();
            productSkuEntityList.add(productSkuEntity);
            productEntity.setProductSkuList(productSkuEntityList);

            productEntityList.add(productEntity);

        }
        return productEntityList;
    }

    /**
     * 查询该店铺下的所有商品
     *
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/feignapi/goods/one/v5/querySelfProductByShopId")
    public List<Product> querySelfProductByShopId(@RequestParam("shopId") Long shopId,
                                                  @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                  @RequestParam(value = "size", defaultValue = "10") Integer size) {
        //必要参数判空
        if (null == shopId) {
            return null;
        }
        //查询商品信息
        QueryWrapper<Product> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .eq(Product::getCheckStatus, ProductConstant.CheckStatus.PASS)
                .eq(Product::getFromFlag, ProductFromFlagEnum.SELF.getCode())
                .eq(Product::getDel, DelEnum.NO.getCode());

        Page<Product> pages = new Page<>(page, size);
        pages.setDesc("id");

        IPage<Product> product = productMapper.selectPage(pages, wrapper);
        if (null == product) {
            return null;
        }
        return product.getRecords();
    }

    /**
     * 获取该店铺下的商品数量
     *
     * @param shopId
     * @param merchantId
     * @return
     */
    @GetMapping("/feignapi/goods/one/v5/queryProductCountByShopId")
    public int queryProductCountByShopId(@RequestParam("shopId") Long shopId,
                                         @RequestParam("merchantId") Long merchantId) {
        //查询商品信息
        QueryWrapper<Product> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .eq(Product::getCheckStatus, ProductConstant.CheckStatus.PASS)
                .eq(Product::getFromFlag, ProductFromFlagEnum.SELF.getCode())
                .eq(Product::getDel, DelEnum.NO.getCode());

        return productMapper.selectCount(wrapper);
    }

    /**
     * 获取首页商品信息，包括：是否购买过新人专享、获取新人专享销售商品、获取销售商品
     *
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v8/homePageGoodsInfo")
    public Map<String, Object> homePageGoodsInfo(@RequestParam("shopId") Long shopId,
                                                 @RequestParam(value = "userId", required = false) String userId,
                                                 @RequestParam(value = "type", defaultValue = "0") Integer type,
                                                 @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                                 @RequestParam(value = "size", defaultValue = "16") Long size) {
        if (shopId < 1) {
            return null;
        }

        //是否购买过新人专享
        boolean newManRecommandImpression = true;
        List<Long> productIds = getProductIds(shopId, type);
        if (!CollectionUtils.isEmpty(productIds) && StringUtils.isNotBlank(userId) && RegexUtils.StringIsNumber(userId)) {
            //查询新人专享是否下过单
            Integer num = orderAppService.queryOrderNumByProductIds(shopId, Long.valueOf(userId), productIds);
            if (num != null && num > 0) {
                newManRecommandImpression = false;
            }
        }
        // 获取新人专享销售商品
        AppletUserProductRebateDTO newMansProductRebateDTO = null;
        if (newManRecommandImpression) {
            newMansProductRebateDTO = pageGoodsSaleWithSalesVolumeDesc(shopId, ProductShowTypeEnum.NEWBIE.getCode(), 0L, size);
            if (newMansProductRebateDTO == null) {
                log.error("获取商品列表失败");
            }

            if (!CollectionUtils.isEmpty(newMansProductRebateDTO.getProductDistributionDTOList()) && newMansProductRebateDTO.getProductDistributionDTOList().size() > 3) {
                newMansProductRebateDTO.setProductDistributionDTOList(newMansProductRebateDTO.getProductDistributionDTOList().subList(0, 3));
            }


            if (!CollectionUtils.isEmpty(newMansProductRebateDTO.getProductDistributionDTOList())) {
                for (ProductDistributionDTO productDistributionDTO : newMansProductRebateDTO.getProductDistributionDTOList()) {
                    JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                    if (mainPics.size() == 0) {
                        continue;
                    }
                    productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);

                    String salesVolume = productDistributionDTO.getSalesVolume();
                    int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                    productDistributionDTO.setSalesVolume(NumberValidationUtil.convertNumberForString(totalSale));
                }
            }
        }

        // 获取销售商品
        AppletUserProductRebateDTO appletUserProductRebateDTO = pageGoodsSaleWithSalesVolumeDesc(shopId, ProductShowTypeEnum.NONE.getCode(), offset, size);
        if (appletUserProductRebateDTO == null) {
            log.error("获取商品列表失败");
            return null;
        }

        if (!CollectionUtils.isEmpty(appletUserProductRebateDTO.getProductDistributionDTOList())) {
            for (ProductDistributionDTO productDistributionDTO : appletUserProductRebateDTO.getProductDistributionDTOList()) {
                JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                if (mainPics.size() == 0) {
                    continue;
                }
                productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);

                String salesVolume = productDistributionDTO.getSalesVolume();
                int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                productDistributionDTO.setSalesVolume(NumberValidationUtil.convertNumberForString(totalSale));
            }
        }

        Map<String, Object> result = new HashMap<>(2);
        result.put("appletUserProductRebateDTO", appletUserProductRebateDTO);
        result.put("newMansGoodsRecommend", newManRecommandImpression ? newMansProductRebateDTO : null);
        result.put("newManRecommandImpression", newManRecommandImpression);
        return result;
    }

    /**
     * 根据传入sku信息，查询或者创建新的sku，秒杀专用
     *
     * @return Map
     */
    @PostMapping("/feignapi/goods/applet/sku/queryOrCreateForFlashSale")
    public List<ProductDistributionDTO> queryOrCreateForFlashSale(@RequestBody MsInfoDTO msInfoDTO) {
        if (msInfoDTO == null) {
            return new ArrayList<>();
        }
        // skuid,价格,秒杀库存，秒初始量
        List<String> skuStrList = msInfoDTO.getSkus();
        if (CollectionUtils.isEmpty(skuStrList)) {
            return new ArrayList<>();
        }
        List<ProductDistributionDTO> productDistributionDTOList = new ArrayList<>();
        for (String skuStr : skuStrList) {
            String[] skuInfo = skuStr.split(",");
            if (skuInfo.length == 0) {
                continue;
            }
            Long skuId = Long.valueOf(skuInfo[0]);
            ProductSku returnProductSku;
            List<ProductSku> productSkuList = sellerSkuRepostory.selectByOldSkuIdAndEffectTime(skuId, msInfoDTO.getTime() + "_" + msInfoDTO.getEndTime());
            if (CollectionUtils.isEmpty(productSkuList)) {
                ProductSku productSku = sellerSkuRepostory.selectBySkuId(skuId);
                if (productSku == null) {
                    continue;
                }
                // 需要创建
                String redisLockKey = RedisKeyConstant.PRODUCT_SKU_FLASH_SALE_CREATE_LOCK + productSku.getId() + ":" + msInfoDTO.getTime();
                boolean nx = RedisForbidRepeat.NX(stringRedisTemplate, redisLockKey);
                if (!nx) {
                    continue;
                }
                returnProductSku = new ProductSku();
                BeanUtils.copyProperties(productSku, returnProductSku);
                returnProductSku.setId(null);
                returnProductSku.setSkuSellingPrice(new BigDecimal(skuInfo[1]));
                returnProductSku.setSkuCostPrice(BigDecimal.ZERO);
                returnProductSku.setStockDef(Integer.valueOf(skuInfo[2]));
                returnProductSku.setStock(Integer.valueOf(skuInfo[2]));
                returnProductSku.setOldSkuId(productSku.getId());
                returnProductSku.setSnapshoot(ProductConstant.ProductSku.Snapshoot.NOT_SNAPSHOOT);
                returnProductSku.setNotShow(ProductConstant.ProductSku.NotShow.FLASH_SALE);
                returnProductSku.setEffectTime(msInfoDTO.getTime() + "_" + msInfoDTO.getEndTime());
                // 初始销量
                Map<String, Object> extra = new HashMap<>();
                extra.put("init_sales", Integer.valueOf(skuInfo[3]));
                returnProductSku.setExtra(JSON.toJSONString(extra));
                returnProductSku.setCtime(new Date());
                returnProductSku.setUtime(new Date());
                returnProductSku.setDel(DelEnum.NO.getCode());
                int i = sellerSkuRepostory.insertSelective(returnProductSku);
                RedisForbidRepeat.delKey(stringRedisTemplate, redisLockKey);
                if (i == 0) {
                    continue;
                }
            } else {
                if (productSkuList.size() > 1) {
                    log.error("存在两条相同时间的秒杀sku，存在逻辑上的问题");
                }
                returnProductSku = productSkuList.get(0);
            }

            ProductDistributionDTO distributionDTO = new ProductDistributionDTO();
            distributionDTO.setUserId(msInfoDTO.getUserId());
            distributionDTO.setShopId(msInfoDTO.getShopId());

            Product productForPlatMall = goodsUpdateService.getProductForPlatMall(null, msInfoDTO.getShopId(),
                    returnProductSku.getProductId(), AppTypeEnum.JKMF);
            if (productForPlatMall == null) {
                continue;
            }
            distributionDTO.setParentId(productForPlatMall.getParentId());
            distributionDTO.setProductId(productForPlatMall.getId());
            distributionDTO.setSelfSupportRatio(productForPlatMall.getSelfSupportRatio());
            distributionDTO.setPlatSupportRatio(productForPlatMall.getPlatSupportRatio());
            distributionDTO.setPrice(returnProductSku.getSkuSellingPrice());
            distributionDTO.setThirdPrice(returnProductSku.getSkuThirdPrice());
            distributionDTO.setSalesVolume((returnProductSku.getStockDef() - returnProductSku.getStock()) + "");
            distributionDTO.setStock(returnProductSku.getStock());
            distributionDTO.setStockDef(returnProductSku.getStockDef());
            distributionDTO.setMainPics(JSONArray.parseArray(productForPlatMall.getMainPics(), String.class).get(0) + compressionRatioPic);
            distributionDTO.setSkuId(returnProductSku.getId());
            String extraJson = returnProductSku.getExtra();
            JSONObject jsonObject = JSON.parseObject(extraJson);
            if (jsonObject != null) {
                distributionDTO.setInitSale(jsonObject.getIntValue("init_sales"));
            }
            distributionDTO.setPutawayTime(productForPlatMall.getPutawayTime());
            distributionDTO.setProductSpecs(productForPlatMall.getProductSpecs());
            distributionDTO.setProductTitle(productForPlatMall.getProductTitle());
            productDistributionDTOList.add(distributionDTO);
        }
        return productDistributionDTOList;
    }

    /**
     * 通过商品id查询sku
     *
     * @param productIdList
     * @return
     */
    @GetMapping("/feignapi/goods/applet/sku/querySkuListByProductIds")
    public List<ProductSku> querySkuListByProductIds(@RequestParam("productIdList") List<Long> productIdList) {
        if (CollectionUtils.isEmpty(productIdList)) {
            return Lists.newArrayList();
        }

        List<ProductSku> productSkuEntities = productSkuMapper.selectList(
                new QueryWrapper<ProductSku>()
                        .lambda()
                        .in(ProductSku::getProductId, productIdList)
                        .orderByAsc(ProductSku::getSkuSellingPrice)
        );
        return productSkuEntities;
    }


    /***
     * 获取供应商商品经过平台分佣之后的售价和比例
     */
    @GetMapping("/feignapi/goods/applet/sku/getPlatformDistRateCommission")
    public String getPlatformDistRateCommission(@RequestParam("merchantId") Long merchantId,
                                                @RequestParam("shopId") Long shopId,
                                                @RequestParam("skuId") Long skuId) {
        log.info("<<<<<<<<开始查询sku编号 {} 的信息 ", skuId);
        QueryWrapper<ProductSku> skuQueryWrapper = new QueryWrapper<>();
        skuQueryWrapper.lambda()
                .eq(ProductSku::getId, skuId)
        ;
        //sku
        ProductSku productSku = this.productSkuMapper.selectOne(skuQueryWrapper);
        if (productSku == null) {
            log.error("<<<<<<<< skuid{} 的sku不存在", skuId);
            return null;
        }
        //sku直系商品
        Product product1 = goodsService.getById(productSku.getProductId());
        log.info("<<<<<<<<pro info:{}", product1);
        if (product1 == null || product1.getStatus() == -1) {
            return BigDecimal.ZERO.toString();
        }
        //获取供应商商品信息
        Product product = goodsService.getById(product1.getParentId());
        log.info("<<<<<<<<pro parent info:{}", product);
        DistributeInfoEntity distributeInfoEntity = new DistributeInfoEntity();
        distributeInfoEntity.setPrice(productSku.getSkuSellingPrice());
        distributeInfoEntity.setProductSpec(product.getProductSpecs());
        distributeInfoEntity.setSelfSupportRadio(product.getSelfSupportRatio());
        distributeInfoEntity.setPlatSupportRadio(product.getPlatSupportRatio());
        ProductDistributCommissionEntity distrIncodeByProduct = distributionAppService.getDistrIncodeByProduct(distributeInfoEntity);

        return distrIncodeByProduct.getPlatDistributCommission().toString();
    }


}
