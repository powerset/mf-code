package mf.code.goods.api.seller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.service.SellerMarketService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.goods.api.seller
 * Description:
 *
 * @author: 百川
 * @date: 2019-09-19 10:48 上午
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/goods/seller/v8/goods/market")
@Slf4j
public class SellerMarketController {
    private final SellerMarketService sellerMarketService;

    /**
     * 平台方选货市场--列表
     */
    @GetMapping(value = "list")
    public SimpleResponse list() {
        sellerMarketService.list();
        return null;
    }
}
