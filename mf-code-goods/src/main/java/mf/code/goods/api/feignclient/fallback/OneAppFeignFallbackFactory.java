package mf.code.goods.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.goods.api.feignclient.OneAppService;
import mf.code.one.dto.ApolloLHYXDTO;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.one.dto.UserCouponDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.api.feignclient.fallback
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:07
 */
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable cause) {
        return new OneAppService() {
            /**
             * 获取联合营销配置信息
             * 返回值result内容
             * status：返回状态码 0：预热活动未开始 1：预热活动进行中 2：预热活动已结束 3：正式活动进行中 4：正式活动已结束
             * time: 距离下一个状态剩余毫秒数
             * count：累计免单商品数
             * money：累计免单商品总价值
             * banner：banner列表，list类型
             * url：banner地址
             * gid：跳转商品id
             */
            @Override
            public ApolloLHYXDTO getUnionConf() {
                return null;
            }

            @Override
            public List<MerchantShopCouponDTO> findMerchantShopCouponByProductId(Long productId) {
                return null;
            }

            @Override
            public Map<String, Object> queryProductActivityInfo(Long productId) {
                return null;
            }

            @Override
            public List<UserCouponDTO> findUserMerchantShopCouponByProductId(Long userId, Long productId) {
                return null;
            }
        };
    }
}
