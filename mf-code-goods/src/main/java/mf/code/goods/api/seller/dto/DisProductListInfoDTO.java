package mf.code.goods.api.seller.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 分销产品列表信息
 */
@Data
public class DisProductListInfoDTO {
    //产品名
    private String title ;

    //商品主图URL
    private String mainPic;

    //售价
    private BigDecimal salePrice ;

    //佣金比例
    private BigDecimal commissionPre;

    private BigDecimal selfCommissionPre;

    //佣金
    private BigDecimal commission ;

    //上架标识1:上架,0:未上架
    private int stackFlag;

    //入库标识（1，未入库，0：已入库）
    private String joinStoreFlag ;

    //原价
    private BigDecimal thirdPrice;

    //是否为自己商铺（0：否，1：是）
    private String selfShopFlag ;

    //产品ID
    private Long productId;

    //店铺ID
    private Long shopId;

    private Long merchantId;

    //类目
    private Long productSpecs;

    private BigDecimal minSellPrice;
    private BigDecimal maxSellPrice;
    private BigDecimal minTaobaoPrice;
    private BigDecimal maxTaobaoPrice;

    private Long parentId;

    //自购省返利
    private BigDecimal rebate;

    private Double salseNum;
    private BigDecimal commission_num;
    private Integer status;

}
