package mf.code.goods.api.applet;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.service.GoodsService;
import mf.code.goods.service.GoodsSkuService;
import mf.code.goods.service.ProductGroupService;
import mf.code.shop.constants.AppTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.goods.api.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月17日 14:39
 */
@RestController
@RequestMapping("/api/goods/applet/v5")
@Slf4j
public class ProductApi {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProductGroupService productGroupService;
    @Autowired
    private GoodsSkuService goodsSkuService;

    /***
     * 获取该店铺的商品分组
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping("/getProductGroup")
    public SimpleResponse getProductGroup(@RequestParam("merchantId") Long merchantId,
                                          @RequestParam("shopId") Long shopId) {
        Assert.isTrue(merchantId != null && merchantId > 0 && shopId != null && shopId > 0, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        return goodsService.selectProductGroup(merchantId, shopId);
    }


    /***
     * 通过分组id查询商品信息列表
     * @param merchantId
     * @param shopId
     * @param userId
     * @param productGroupId 商品分组编号
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/getProductsByGroupId")
    public SimpleResponse getProductsByGroupId(@RequestParam("merchantId") Long merchantId,
                                               @RequestParam("shopId") Long shopId,
                                               @RequestParam(value = "userId", required = false) Long userId,
                                               @RequestParam(value = "productGroupId", defaultValue = "0") Long productGroupId,
                                               @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                               @RequestParam(value = "size", defaultValue = "10") Long size) {
        boolean checkValid = merchantId != null && merchantId > 0 && shopId != null && shopId > 0;
        Assert.isTrue(checkValid, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        SimpleResponse simpleResponse = productGroupService.getProductsByGroupId(merchantId, shopId, userId, productGroupId, offset, size);
        log.info("<<<<<<<商品分组列表返回：{}", JSONObject.toJSONString(simpleResponse));
        return simpleResponse;
    }


    /***
     * 查询商品详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param goodsId
     * @return
     */
    @GetMapping("/getGoodsDetail")
    public SimpleResponse getGoodsDetail(@RequestParam("merchantId") Long merchantId,
                                         @RequestParam("shopId") Long shopId,
                                         @RequestParam(value = "userId", required = false) Long userId,
                                         @RequestParam("goodsId") Long goodsId, HttpServletRequest request) {
        AppTypeEnum appTypeEnum = AppTypeEnum.JKMF;
        String project = request.getHeader("project");
        if ("ddd".equals(project)) {
            appTypeEnum = AppTypeEnum.DDD;
        }
        if ("dxp".equals(project)) {
            appTypeEnum = AppTypeEnum.DXP;
        }
        return this.goodsSkuService.getGoodsDetail(merchantId, shopId, userId, goodsId, appTypeEnum);
    }
}
