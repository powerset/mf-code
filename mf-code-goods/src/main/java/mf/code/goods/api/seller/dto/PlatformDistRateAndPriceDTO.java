package mf.code.goods.api.seller.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.goods.api.seller.dto
 * Description: 平台抽佣之后的比例和售价
 *
 * @author gel
 * @date 2019-05-13 17:27
 */
@Data
public class PlatformDistRateAndPriceDTO {
    /**
     * 平台抽佣比例
     */
    private BigDecimal platformDistRate;
    /**
     * 供应商抽佣比例
     */
    private BigDecimal platSupportRatio;
    /**
     * 供应商商品售价
     */
    private BigDecimal priceAfterDist;
}
