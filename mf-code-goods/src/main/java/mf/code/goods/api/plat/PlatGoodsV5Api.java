package mf.code.goods.api.plat;/**
 * create by qc on 2019/4/9 0009
 */

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.service.GoodsService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gbf
 * 2019/4/9 0009、20:29
 */
@RestController
@RequestMapping("/api/goods/platform/v5/goods/plat")
public class PlatGoodsV5Api {
    @Autowired
    private GoodsService goodsService;

    /**
     * 审核列表
     *
     * @param productId       .
     * @param phone           .
     * @param shopName        .
     * @param submitTimeBegin .
     * @param submitTimeEnd   .
     * @return .
     */
    @GetMapping(value = "list/check")
    public SimpleResponse list(@RequestParam(value = "productId", required = false) Long productId,
                               @RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "shopName", required = false) String shopName,
                               @RequestParam(value = "submitTimeBegin", required = false) String submitTimeBegin,
                               @RequestParam(value = "submitTimeEnd", required = false) String submitTimeEnd,
                               @RequestParam(value = "type", defaultValue = "1") Integer type, //1:待审核 2:已审核
                               @RequestParam(value = "checkResult", required = false) Integer checkResult, // 0:全部 1:审核通过 2:审核不通过
                               @RequestParam(value = "pageCurrent", defaultValue = "1") int pageCurrent,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize
    ) {
        if (StringUtils.isNotBlank(phone)) {
            Assert.isInteger(phone, 1, "手机错误");
            Assert.isTrue(phone.length() == 11, 2, "手机错误");
        }
        Assert.isRightNumberSection(type, 1, 2, 3, "审核状态type值只能为1和2");
        boolean notBlankBegin = StringUtils.isNotBlank(submitTimeBegin);
        boolean notBlankEnd = StringUtils.isNotBlank(submitTimeEnd);
        Date beginDate = null;
        Date endDate = null;
        if (notBlankBegin) {
            Assert.canConvertDate(submitTimeBegin, DateUtil.LONG_DATE_FORMAT, 4, "开始时间错误");
            beginDate = DateUtil.stringtoDate(submitTimeBegin, DateUtil.LONG_DATE_FORMAT);
            submitTimeBegin = DateUtil.dateToString(beginDate, DateUtil.FORMAT_ONE);
        }
        if (notBlankEnd) {
            Assert.canConvertDate(submitTimeEnd, DateUtil.LONG_DATE_FORMAT, 5, "结束时间错误");
            endDate = DateUtil.getDateBeforeZero(DateUtil.stringtoDate(submitTimeEnd, DateUtil.LONG_DATE_FORMAT));
            submitTimeEnd = DateUtil.dateToString(endDate, DateUtil.FORMAT_ONE);
        }
        if (notBlankBegin && notBlankEnd) {
            Assert.isRightDateArgument(submitTimeBegin, submitTimeEnd, DateUtil.LONG_DATE_FORMAT, 6, "开始时间在结束之间之后");
        }
        if (checkResult != null) {
            Assert.isRightNumberSection(checkResult, 0, 2, 6, "审核结果参数错误,只能为:0,1,2");
        }
        Map<String, Object> conditionMap = new HashMap<>();
        conditionMap.put("productId", productId);
        conditionMap.put("phone", phone);
        conditionMap.put("shopName", shopName);
        conditionMap.put("timeBegin", submitTimeBegin);
        conditionMap.put("timeEnd", submitTimeEnd);
        conditionMap.put("checkResult", checkResult);
        conditionMap.put("type", type);
        return goodsService.checkListByConditionForListOptimize(conditionMap, pageCurrent, pageSize);
    }

    /**
     * 审核商品-详情
     *
     * @param productId .
     * @return .
     */
    @GetMapping(value = "detail")
    public SimpleResponse detail(@RequestParam(value = "productId", required = false) Long productId) {
        return goodsService.platCheckProductDetail(productId);
    }

    /**
     * 审核
     *
     * @param dto .
     * @return .
     */
    @PostMapping(value = "audit")
    public SimpleResponse audit(@RequestBody ProductDTO dto) {
        Long productId = dto.getProductId();
        Integer checkStatus = dto.getCheckStatus();
        if (2 == checkStatus) {
            checkStatus = -1;
        }
        String checkReason = dto.getCheckReason();
        List<String> checkReasonPicList = dto.parseCheckReasonPicsStr();
        if (checkReasonPicList != null && checkReasonPicList.size() > 3) {
            return new SimpleResponse(1, "审核图片不能多于3张");
        }
        return goodsService.audit(productId, checkStatus, checkReason, dto.getCheckReasonPicsStr());
    }

    /**
     * 查看审核不通过理由-平台
     *
     * @param productId
     * @return
     */
    @GetMapping(value = "getCheckReason")
    public SimpleResponse getCheckReason(@RequestParam(value = "productId") Long productId) {
        return goodsService.getCheckReason(productId);
    }


}
