package mf.code.goods.api.feignclient;

import mf.code.goods.api.feignclient.fallback.OneAppFeignFallbackFactory;
import mf.code.goods.common.config.feign.FeignLogConfiguration;
import mf.code.one.dto.ApolloLHYXDTO;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.one.dto.UserCouponDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * mf-code-one服务间调用feignClient
 *
 * @author gel
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {

    /**
     * 获取联合营销配置信息
     * 返回值result内容
     * status：返回状态码 0：预热活动未开始 1：预热活动进行中 2：预热活动已结束 3：正式活动进行中 4：正式活动已结束
     * time: 距离下一个状态剩余毫秒数
     * count：累计免单商品数
     * money：累计免单商品总价值
     * banner：banner列表，list类型
     * url：banner地址
     * gid：跳转商品id
     */
    @GetMapping("/feignapi/one/applet/getUnionConf")
    ApolloLHYXDTO getUnionConf();


    /**
     * 根据商品id查询优惠卷
     *
     * @param productId
     * @return
     */
    @GetMapping("/feignapi/one/applet/findMerchantShopCouponByProductId")
    List<MerchantShopCouponDTO> findMerchantShopCouponByProductId(@RequestParam("productId") Long productId);

    /***
     * 查询商品是否作用于活动信息
     * @param productId
     * @return
     */
    @GetMapping("/feignapi/one/applet/queryProductActivityInfo")
    Map<String, Object> queryProductActivityInfo(@RequestParam(value = "productId") Long productId);

    /**
     * 通过用户id和商品id查询优惠劵
     *
     * @param userId
     * @param productId
     * @return
     */
    @GetMapping("/feignapi/one/applet/findUserMerchantShopCouponByProductId")
    List<UserCouponDTO> findUserMerchantShopCouponByProductId(@RequestParam("userId") Long userId, @RequestParam("productId") Long productId);
}
