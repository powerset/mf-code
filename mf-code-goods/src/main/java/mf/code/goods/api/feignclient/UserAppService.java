package mf.code.goods.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.feignclient.fallback.UserAppFeignFallbackFactory;
import mf.code.goods.common.config.feign.FeignLogConfiguration;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * mf.code.user.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 19:38
 */
@FeignClient(name = "mf-code-user",fallbackFactory = UserAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface UserAppService {

    /***
     * 获取用户信息
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryUser")
    UserResp queryUser(@RequestParam("userId") Long userId);

}
