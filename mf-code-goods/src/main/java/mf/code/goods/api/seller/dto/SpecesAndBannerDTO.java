package mf.code.goods.api.seller.dto;

import lombok.Data;

/**
 * banner图和类目ID
 */
@Data
public class SpecesAndBannerDTO {
    /**
     * 新类目名称
     */
    private String specesName;

    /**
     * 新类目ID
     */
    private String specesId;

    /**
     * banner 图地址
     */
    private String bannerUrl;

}
