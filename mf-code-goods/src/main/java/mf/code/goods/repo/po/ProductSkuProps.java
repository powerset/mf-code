package mf.code.goods.repo.po;/**
 * create by qc on 2019/4/4 0004
 */

import lombok.Data;

/**
 * @author gbf
 * 2019/4/4 0004、15:30
 */
@Data
public class ProductSkuProps implements Comparable<ProductSkuProps> {
    private String productSpecsId;
    private String specsValue;
    private String parentId;
    private String parentName;

    @Override

    public int compareTo(ProductSkuProps prop) {
        return Integer.parseInt(this.productSpecsId) - Integer.parseInt(prop.productSpecsId);
    }
}
