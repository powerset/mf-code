package mf.code.goods.repo.dao;

import mf.code.goods.api.seller.dto.DisProductListInfoDTO;
import mf.code.goods.api.seller.dto.ProductSkuInfoDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SellerGoodsDistributionMapper {

    Long getProductIdEqlParentIdNum(Map<String, Object> map);

    List<DisProductListInfoDTO> findProductAndProductSku(Map<String, Object> map);

    List<DisProductListInfoDTO> findProductAndProductSkuOptimize(Map<String, Object> map);

    List<DisProductListInfoDTO> getProductAndProductSkuInfo(Map<String, Object> params);

    List<DisProductListInfoDTO> getProductAndProductSkuNum(Map<String, Object> params);

    /*****二次优化 begin*******/
    int updateProductPriceInfoById(ProductSkuInfoDTO info);

    List<ProductSkuInfoDTO> getProductSkuInfo(Map<String, Object> params);

    //获取含有分销属性的商品
    List<DisProductListInfoDTO> getDisProductInfo(Map<String, Object> params);

    //获取含有分销属性的商品的数量
    List<DisProductListInfoDTO> getDisProductInfoNum(Map<String, Object> params);

    List<DisProductListInfoDTO> batchAddCacheByProductId(Long productId);

    //批量获取商品信息
    List<DisProductListInfoDTO> queryByIds(Map<String, Object> params);

    //批量修改product中的销量信息
    List<DisProductListInfoDTO> batchUpdateOrderSalesNumByProductId(Long productId);

    /**
     * 增加排序
     * @param sort
     * @return
     */
    List<DisProductListInfoDTO> batchAddCacheByProductIdAll(Integer sort);

    //根据productID  获取销量
    Long getStockNum(@Param("productId") Long productId);
    /*****二次优化 end*******/
}
