package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.repo.po.ProductSpecs;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductSpecsMapper extends BaseMapper<ProductSpecs> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ProductSpecs record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductSpecs record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductSpecs selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductSpecs record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductSpecs record);

    /**
     * 获取规格
     * @param map
     * @return
     */
    List<ProductSpecs> findSpecsByMerchantId(Map map);

    /**
     * 获取全部规格
     * @return
     */
    List<Map<String,Object>> findAll();

}