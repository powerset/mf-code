package mf.code.goods.repo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.goods.repo.dao.ProductPromotionMapper;
import mf.code.goods.repo.po.ProductPromotion;
import mf.code.goods.repo.service.ProductPromotionService;

/**
 * mf.code.goods.repo.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 09:03
 */
public class ProductPromotionServiceImpl extends ServiceImpl<ProductPromotionMapper, ProductPromotion> implements ProductPromotionService {
}
