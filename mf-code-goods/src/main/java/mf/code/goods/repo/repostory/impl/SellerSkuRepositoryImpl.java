package mf.code.goods.repo.repostory.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.common.DelEnum;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.repostory.SellerSkuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author gbf
 * 2019/4/3 0003、09:13
 */
@Repository
public class SellerSkuRepositoryImpl extends ServiceImpl<ProductSkuMapper, ProductSku> implements SellerSkuRepository {

    @Autowired
    private ProductSkuMapper productSkuMapper;

    /**
     * 创建新的sku
     *
     * @param productSku
     * @return
     */
    @Override
    public int insertSelective(ProductSku productSku) {
        return productSkuMapper.insertSelective(productSku);
    }

    /**
     * 根据skuId列表查询sku列表
     *
     * @param skuIdList
     * @return
     */
    @Override
    public List<ProductSku> selectBatchIds(List<Long> skuIdList) {
        if (CollectionUtils.isEmpty(skuIdList)) {
            return new ArrayList<>();
        }
        if (skuIdList.size() > 100) {
            skuIdList = skuIdList.subList(0, 100);
        }
        return productSkuMapper.selectBatchIds(skuIdList);
    }

    /**
     * 根据商品id，查询sku列表
     *
     * @param productId
     * @return
     */
    @Override
    public List<ProductSku> selectByProductId(Long productId) {
        return productSkuMapper.findByProductId(productId);
    }

    /**
     * 根据商品id列表，查询sku列表
     *
     * @param productIdList
     * @return
     */
    @Override
    public List<ProductSku> selectByProductIdList(List<Long> productIdList) {
        if (CollectionUtils.isEmpty(productIdList)) {
            return new ArrayList<>();
        }
        // TODO 这里可能要埋坑
        if (productIdList.size() > 500) {
            productIdList = productIdList.subList(0, 500);
        }
        QueryWrapper<ProductSku> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .in(ProductSku::getProductId, productIdList)
                .eq(ProductSku::getSnapshoot, ProductConstant.ProductSku.Snapshoot.NOT_SNAPSHOOT)
                .in(ProductSku::getNotShow, Arrays.asList(ProductConstant.ProductSku.NotShow.SHOW,
                        ProductConstant.ProductSku.NotShow.NOT_SHOW))
                .eq(ProductSku::getDel, DelEnum.NO.getCode());
        return productSkuMapper.selectList(queryWrapper);
    }

    /**
     * 根据skuId列表，查询sku
     *
     * @param skuId
     * @return
     */
    @Override
    public ProductSku selectBySkuId(Long skuId) {
        return productSkuMapper.selectByPrimaryKey(skuId);
    }

    /**
     * 根据skuId列表，查询sku，秒杀专用哟
     *
     * @param skuId      原始skuId
     * @param effectTime
     * @return 应该是一条当前时间生成的数据
     */
    @Override
    public List<ProductSku> selectByOldSkuIdAndEffectTime(Long skuId, String effectTime) {
        if (StringUtils.isEmpty(effectTime)) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductSku> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductSku::getOldSkuId, skuId)
                .eq(ProductSku::getSnapshoot, ProductConstant.ProductSku.Snapshoot.NOT_SNAPSHOOT)
                .eq(ProductSku::getNotShow, ProductConstant.ProductSku.NotShow.FLASH_SALE)
                .eq(ProductSku::getEffectTime, effectTime)
                .eq(ProductSku::getDel, DelEnum.NO.getCode())
                ;
        return productSkuMapper.selectList(queryWrapper);
    }
}
