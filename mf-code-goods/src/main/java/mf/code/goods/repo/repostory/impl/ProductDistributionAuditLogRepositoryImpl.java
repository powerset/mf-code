package mf.code.goods.repo.repostory.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.goods.repo.dao.ProductDistributionAuditLogMapper;
import mf.code.goods.repo.po.ProductDistributionAuditLog;
import mf.code.goods.repo.repostory.ProductDistributionAuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-08 9:35
 */
@Service
public class ProductDistributionAuditLogRepositoryImpl implements ProductDistributionAuditLogRepository {
    @Autowired
    private ProductDistributionAuditLogMapper productDistributionAuditLogMapper;

    /**
     * 创建分销审核表
     *
     * @param productDistributionAuditLog
     * @return
     */
    @Override
    public Integer createSelective(ProductDistributionAuditLog productDistributionAuditLog) {
        if (productDistributionAuditLog == null) {
            return 0;
        }
        productDistributionAuditLog.setCtime(new Date());
        productDistributionAuditLog.setUtime(new Date());
        return productDistributionAuditLogMapper.insertSelective(productDistributionAuditLog);
    }

    /**
     * 更新分销审核表
     *
     * @param productDistributionAuditLog
     * @return
     */
    @Override
    public Integer updateSelective(ProductDistributionAuditLog productDistributionAuditLog) {
        if (productDistributionAuditLog == null) {
            return 0;
        }
        productDistributionAuditLog.setUtime(new Date());
        return productDistributionAuditLogMapper.updateByPrimaryKeySelective(productDistributionAuditLog);
    }

    /**
     * 根据productId查询分销审核表
     *
     * @param productId
     * @return
     */
    @Override
    public List<ProductDistributionAuditLog> findByProductId(Long productId) {
        if (productId <= 0) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductDistributionAuditLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ProductDistributionAuditLog::getProductId, productId);
        return productDistributionAuditLogMapper.selectList(queryWrapper);
    }

    /**
     * 查询最新的一次分销设置记录
     *
     * @param productId
     * @return
     */
    @Override
    public ProductDistributionAuditLog findByProductIdLimitOne(Long productId) {
        if (productId == null) {
            return null;
        }
        return productDistributionAuditLogMapper.findByProductIdLimitOne(productId);
    }

    /**
     * 作废分销设置
     *
     * @param productId
     */
    @Override
    public void abandonAllDistributionByProduct(Long productId) {
        productDistributionAuditLogMapper.abandonAllDistributionByProduct(productId);
    }


    /**
     * 分页查询分销审核记录
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<ProductDistributionAuditLog> selectPage(Page<ProductDistributionAuditLog> page, Wrapper<ProductDistributionAuditLog> queryWrapper) {
        return productDistributionAuditLogMapper.selectPage(page, queryWrapper);
    }

    /**
     * 根据主键id查询记录
     *
     * @param id
     * @return
     */
    @Override
    public ProductDistributionAuditLog selectById(Long id) {
        return productDistributionAuditLogMapper.selectByPrimaryKey(id);
    }
}
