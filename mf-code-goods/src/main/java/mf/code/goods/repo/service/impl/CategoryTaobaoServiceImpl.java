package mf.code.goods.repo.service.impl;/**
 * create by qc on 2019/8/22 0022
 */

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.goods.repo.dao.CategoryTaobaoMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.service.CategoryTaobaoService;
import org.springframework.stereotype.Service;

/**
 * @author gbf
 * 2019/8/22 0022、15:33
 */
@Service
public class CategoryTaobaoServiceImpl extends ServiceImpl<CategoryTaobaoMapper, CategoryTaobao> implements CategoryTaobaoService {
}
