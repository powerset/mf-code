package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.repo.po.ProductGroupMap;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductGroupMapMapper extends BaseMapper<ProductGroupMap> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(ProductGroupMap record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductGroupMap record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductGroupMap selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductGroupMap record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductGroupMap record);
}
