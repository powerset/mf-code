package mf.code.goods.repo.dao;

import mf.code.goods.repo.po.ProductStockLog;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductStockLogMapper {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(ProductStockLog record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductStockLog record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductStockLog selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductStockLog record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductStockLog record);

    /**
     * 库存操作记录20条
     * @param map
     * @return
     */
    List<ProductStockLog> findTop20(Map map);
}