package mf.code.goods.repo.vo;/**
 * create by qc on 2019/4/18 0018
 */

import lombok.Data;

import java.util.List;

/**
 * @author gbf
 * 2019/4/18 0018、11:39
 */
@Data
public class ProductSkuSpecNameVo {
    private Long specNameId;
    private String specNameVal;
    private List<ProductSkuSpecValVo> specValList;

    @Override
    public String toString() {
        return "ProductSkuSpecNameVo{" +
                "specNameId=" + specNameId +
                ", specNameVal='" + specNameVal + '\'' +
                ", specValList=" + specValList +
                '}';
    }
}
