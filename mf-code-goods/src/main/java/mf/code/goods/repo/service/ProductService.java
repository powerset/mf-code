package mf.code.goods.repo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.goods.repo.po.Product;

/**
 * mf.code.goods.repo.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-30 17:45
 */
public interface ProductService extends IService<Product> {
}
