package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CategoryTaobaoMapper extends BaseMapper<CategoryTaobao> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(CategoryTaobao record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(CategoryTaobao record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    CategoryTaobao selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(CategoryTaobao record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(CategoryTaobao record);

    List<CategoryTaobao> selectAll();

    /**
     * 获取新类目信息
     * @return
     */
    List<CategoryTaobao> selectNewSpeceTypeInfo(@Param("specesId") String specesId);

    int updateSpecesInfo(Map map);

    List<CategoryTaobao> selectNewSpeceTypeInfoByparentId(@Param("specesId")String specesId);
}
