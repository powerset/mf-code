package mf.code.goods.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * common_dict
 * 数据字典表，jkmfv1.0创建
 */
public class CommonDict implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 类型（字符串）
     */
    private String type;

    /**
     * 字段名
     */
    private String key;

    /**
     * 字段意义
     */
    private String value;

    /**
     * 字段意义1
     */
    private String value1;

    /**
     * 字段意义2
     */
    private String value2;

    /**
     * 字段意义3
     */
    private String value3;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 备注名
     */
    private String remark;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * common_dict
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 类型（字符串）
     * @return type 类型（字符串）
     */
    public String getType() {
        return type;
    }

    /**
     * 类型（字符串）
     * @param type 类型（字符串）
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 字段名
     * @return key 字段名
     */
    public String getKey() {
        return key;
    }

    /**
     * 字段名
     * @param key 字段名
     */
    public void setKey(String key) {
        this.key = key == null ? null : key.trim();
    }

    /**
     * 字段意义
     * @return value 字段意义
     */
    public String getValue() {
        return value;
    }

    /**
     * 字段意义
     * @param value 字段意义
     */
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    /**
     * 字段意义1
     * @return value1 字段意义1
     */
    public String getValue1() {
        return value1;
    }

    /**
     * 字段意义1
     * @param value1 字段意义1
     */
    public void setValue1(String value1) {
        this.value1 = value1 == null ? null : value1.trim();
    }

    /**
     * 字段意义2
     * @return value2 字段意义2
     */
    public String getValue2() {
        return value2;
    }

    /**
     * 字段意义2
     * @param value2 字段意义2
     */
    public void setValue2(String value2) {
        this.value2 = value2 == null ? null : value2.trim();
    }

    /**
     * 字段意义3
     * @return value3 字段意义3
     */
    public String getValue3() {
        return value3;
    }

    /**
     * 字段意义3
     * @param value3 字段意义3
     */
    public void setValue3(String value3) {
        this.value3 = value3 == null ? null : value3.trim();
    }

    /**
     * 父级id
     * @return parent_id 父级id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 父级id
     * @param parentId 父级id
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 备注名
     * @return remark 备注名
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注名
     * @param remark 备注名
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 排序字段
     * @return sort 排序字段
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 排序字段
     * @param sort 排序字段
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", type=").append(type);
        sb.append(", key=").append(key);
        sb.append(", value=").append(value);
        sb.append(", value1=").append(value1);
        sb.append(", value2=").append(value2);
        sb.append(", value3=").append(value3);
        sb.append(", parentId=").append(parentId);
        sb.append(", remark=").append(remark);
        sb.append(", sort=").append(sort);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}