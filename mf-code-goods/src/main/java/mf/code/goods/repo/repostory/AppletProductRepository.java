package mf.code.goods.repo.repostory;

import mf.code.distribution.feignapi.dto.ProductDistributionDTO;

import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author gel
 * @date 2019-06-13 14:41
 */
public interface AppletProductRepository {

    /**
     * 销量排行列表优化
     *
     * @param param
     * @return
     */
    List<ProductDistributionDTO> pageWithSalesVolumeDescForListOptimize(Map<String, Object> param);

    /**
     * 销量排行列表优化
     *
     * @param param
     * @return
     */
    List<ProductDistributionDTO> pageWithSalesVolumeDescByGroupIdForListOptimize(Map<String, Object> param);
}
