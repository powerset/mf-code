package mf.code.goods.repo.repostory.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.common.DelEnum;
import mf.code.goods.repo.dao.ProductGroupMapMapper;
import mf.code.goods.repo.po.ProductGroupMap;
import mf.code.goods.repo.repostory.ProductGroupMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-16 14:35
 */
@Service
public class ProductGroupMapRepositoryImpl implements ProductGroupMapRepository {

    @Autowired
    private ProductGroupMapMapper productGroupMapMapper;

    /**
     * 创建商品分组
     *
     * @param productGroupMap
     * @return
     */
    @Override
    public Integer createOrUpdateSelective(ProductGroupMap productGroupMap) {
        if (productGroupMap == null) {
            return 0;
        }
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getProductId, productGroupMap.getProductId())
                .eq(ProductGroupMap::getGroupId, productGroupMap.getGroupId());
        List<ProductGroupMap> productGroupMaps = productGroupMapMapper.selectList(queryWrapper);
        Date now = new Date();
        if (CollectionUtils.isEmpty(productGroupMaps)) {
            productGroupMap.setCtime(now);
            productGroupMap.setUtime(now);
            productGroupMap.setDel(DelEnum.NO.getCode());
            return productGroupMapMapper.insertSelective(productGroupMap);
        } else {
            ProductGroupMap productGroupMapUpdate = productGroupMaps.get(0);
            if (productGroupMapUpdate.getDel() == DelEnum.NO.getCode()) {
                // 未删除不需要更新
                return 0;
            }
            productGroupMapUpdate.setUtime(now);
            productGroupMapUpdate.setDel(DelEnum.NO.getCode());
            return productGroupMapMapper.updateByPrimaryKeySelective(productGroupMapUpdate);
        }
    }

    /**
     * 根据商品分组id查询商品分组映射列表
     *
     * @param groupId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public IPage<ProductGroupMap> selectByGroupId(Long groupId, Long pageNum, Long pageSize) {
        IPage<ProductGroupMap> page = new Page<>(pageNum, pageSize);
        if (groupId == null) {
            return page;
        }
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getGroupId, groupId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        return productGroupMapMapper.selectPage(page, queryWrapper);
    }

    /**
     * 根据商品分组id查询商品id列表
     *
     * @param groupId
     * @return
     */
    @Override
    public List<Long> selectProductIdListByGroupId(Long groupId) {
        if (groupId == null) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(ProductGroupMap::getProductId)
                .eq(ProductGroupMap::getGroupId, groupId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        List<ProductGroupMap> productGroupMaps = productGroupMapMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(productGroupMaps)) {
            return new ArrayList<>();
        }
        Set<Long> productIdSet = new HashSet<>();
        for (ProductGroupMap productGroupMap : productGroupMaps) {
            productIdSet.add(productGroupMap.getProductId());
        }
        return new ArrayList<>(productIdSet);
    }

    /**
     * 根据商品id查询分组id列表
     *
     * @param productId
     * @return
     */
    @Override
    public List<Long> selectGroupIdListByProductId(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getProductId, productId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        List<ProductGroupMap> groupIdList = productGroupMapMapper.selectList(queryWrapper);
        Set<Long> groupIdResultSet = new HashSet<>();
        for (ProductGroupMap groupMap : groupIdList) {
            groupIdResultSet.add(groupMap.getGroupId());
        }
        return new ArrayList<>(groupIdResultSet);
    }

    /**
     * 根据原始商品id查询分组id列表
     *
     * @param parentProductId
     * @return
     */
    @Override
    public List<Long> selectGroupIdListByParentProductId(Long parentProductId) {
        if (parentProductId == null) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getParentProductId, parentProductId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        List<ProductGroupMap> groupIdList = productGroupMapMapper.selectList(queryWrapper);
        Set<Long> groupIdResultSet = new HashSet<>();
        for (ProductGroupMap groupMap : groupIdList) {
            groupIdResultSet.add(groupMap.getGroupId());
        }
        return new ArrayList<>(groupIdResultSet);
    }

    /**
     * 根据原始商品id查询分组id列表,排除自身的分组
     *
     * @param productId
     * @return
     */
    @Override
    public List<Long> selectGroupIdListByParentProductIdWithoutSelf(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getParentProductId, productId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        List<ProductGroupMap> groupIdList = productGroupMapMapper.selectList(queryWrapper);
        Set<Long> groupIdResultSet = new HashSet<>();
        for (ProductGroupMap groupMap : groupIdList) {
            if (groupMap.getProductId().equals(productId)) {
                continue;
            }
            groupIdResultSet.add(groupMap.getGroupId());
        }
        return new ArrayList<>(groupIdResultSet);
    }

    /**
     * 根据分组id删除
     *
     * @param groupId
     * @return
     */
    @Override
    public Integer updateDelByGroupId(Long groupId) {
        ProductGroupMap productGroupMap = new ProductGroupMap();
        productGroupMap.setDel(DelEnum.YES.getCode());
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getGroupId, groupId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        return productGroupMapMapper.update(productGroupMap, queryWrapper);
    }

    /**
     * 根据商品id删除
     *
     * @param productId
     * @return
     */
    @Override
    public Integer updateDelByProductId(Long productId) {
        ProductGroupMap productGroupMap = new ProductGroupMap();
        productGroupMap.setDel(DelEnum.YES.getCode());
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getProductId, productId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        return productGroupMapMapper.update(productGroupMap, queryWrapper);
    }

    /**
     * 根据分组id商品id删除
     *
     * @param groupId
     * @param productId
     * @return
     */
    @Override
    public Integer updateDelByGroupIdAndProductId(Long groupId, Long productId) {
        ProductGroupMap productGroupMap = new ProductGroupMap();
        productGroupMap.setDel(DelEnum.YES.getCode());
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getProductId, productId)
                .eq(ProductGroupMap::getGroupId, groupId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        return productGroupMapMapper.update(productGroupMap, queryWrapper);
    }

    /**
     * 根据原始商品id删除
     *
     * @param parentProductId
     * @return
     */
    @Override
    public Integer updateDelByParentProductId(Long parentProductId) {
        ProductGroupMap productGroupMap = new ProductGroupMap();
        productGroupMap.setDel(DelEnum.YES.getCode());
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getParentProductId, parentProductId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        return productGroupMapMapper.update(productGroupMap, queryWrapper);
    }

    /**
     * 根据原始商品id删除,排除自营的商品
     *
     * @param parentProductId
     * @return
     */
    @Override
    public Integer updateDelByParentProductIdWithoutSelf(Long parentProductId) {
        ProductGroupMap productGroupMap = new ProductGroupMap();
        productGroupMap.setDel(DelEnum.YES.getCode());
        QueryWrapper<ProductGroupMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroupMap::getParentProductId, parentProductId)
                .ne(ProductGroupMap::getProductId, parentProductId)
                .eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        return productGroupMapMapper.update(productGroupMap, queryWrapper);
    }

    /***
     * 根据分组汇总总数目
     * @param wrapper
     * @return
     */
    @Override
    public Integer countByGroupId(QueryWrapper<ProductGroupMap> wrapper) {
        return productGroupMapMapper.selectCount(wrapper);
    }
}
