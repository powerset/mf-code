package mf.code.goods.repo.repostory.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.common.AddOrReduceEnum;
import mf.code.common.DelEnum;
import mf.code.goods.common.constant.ProductGroupTypeEnum;
import mf.code.goods.repo.dao.ProductGroupMapper;
import mf.code.goods.repo.po.ProductGroup;
import mf.code.goods.repo.repostory.ProductGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-16 14:35
 */
@Service
public class ProductGroupRepositoryImpl implements ProductGroupRepository {

    @Autowired
    private ProductGroupMapper productGroupMapper;

    /**
     * 创建商品分组
     *
     * @param productGroup
     * @return
     */
    @Override
    public Integer createSelective(ProductGroup productGroup) {
        if (productGroup == null) {
            return 0;
        }
        Date now = new Date();
        productGroup.setCtime(now);
        productGroup.setUtime(now);
        productGroup.setDel(DelEnum.NO.getCode());
        return productGroupMapper.insertSelective(productGroup);
    }

    /**
     * 根据分组id更新记录
     *
     * @param productGroup
     * @return
     */
    @Override
    public Integer updateSelectiveById(ProductGroup productGroup) {
        if (productGroup == null) {
            return 0;
        }
        if (productGroup.getId() == null) {
            return 0;
        }
        productGroup.setUtime(new Date());
        return productGroupMapper.updateByPrimaryKeySelective(productGroup);
    }

    /**
     * 根据主键查询分组
     *
     * @param groupId
     * @return
     */
    @Override
    public ProductGroup selectById(Long groupId) {
        if (groupId == null) {
            return null;
        }
        return productGroupMapper.selectByPrimaryKey(groupId);
    }

    /**
     * 根据店铺查询分组列表，商户端用
     *
     * @param shopId
     * @return
     */
    @Override
    public List<ProductGroup> selectByShopIdForSeller(Long shopId) {
        if (shopId == null) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroup::getShopId, shopId)
                .eq(ProductGroup::getType, ProductGroupTypeEnum.PRODUCT.getCode())
                .eq(ProductGroup::getDel, DelEnum.NO.getCode());
        return productGroupMapper.selectList(queryWrapper);
    }

    /**
     * 根据店铺查询分组列表，小程序端使用。未删除，关联商品数量大于0，order字段正序排列
     *
     * @param shopId
     * @return
     */
    @Override
    public List<ProductGroup> selectByShopIdForApplet(Long shopId) {
        if (shopId == null) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroup::getShopId, shopId)
                .eq(ProductGroup::getType, ProductGroupTypeEnum.PRODUCT.getCode())
                .eq(ProductGroup::getDel, DelEnum.NO.getCode())
                .gt(ProductGroup::getGoodsNum, 0)
                .orderByAsc(ProductGroup::getSort);
        return productGroupMapper.selectList(queryWrapper);
    }

    /**
     * 校验分组名称不重复
     *
     * @param shopId
     * @param name
     * @return
     */
    @Override
    public List<ProductGroup> selectByShopIdNameDel(Long shopId, String name) {
        QueryWrapper<ProductGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductGroup::getShopId, shopId)
                .eq(ProductGroup::getName, name)
                .eq(ProductGroup::getType, ProductGroupTypeEnum.PRODUCT.getCode())
                .eq(ProductGroup::getDel, DelEnum.NO.getCode());
        return productGroupMapper.selectList(queryWrapper);
    }

    /**
     * 根据主键删除分组
     *
     * @param groupId
     * @return
     */
    @Override
    public Integer updateDelById(Long groupId) {
        if (groupId == null) {
            return 0;
        }
        ProductGroup group = new ProductGroup();
        group.setId(groupId);
        group.setUtime(new Date());
        group.setDel(DelEnum.YES.getCode());
        return productGroupMapper.updateByPrimaryKeySelective(group);
    }

    /**
     * 根据主键列表更新商品数量
     *
     * @param groupIdList
     * @param addOrReduceEnum
     * @return
     */
    @Override
    public Integer updateGoodsNumByIdList(List<Long> groupIdList, AddOrReduceEnum addOrReduceEnum) {
        if (CollectionUtils.isEmpty(groupIdList)) {
            return 0;
        }
        // TODO 简单实现
        if (addOrReduceEnum == AddOrReduceEnum.ADD) {
            return productGroupMapper.updateAddGoodsNumByPrimaryKeyList(groupIdList);
        } else {
            return productGroupMapper.updateReduceGoodsNumByPrimaryKeyList(groupIdList);
        }
    }

}
