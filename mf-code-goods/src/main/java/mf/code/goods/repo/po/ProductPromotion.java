package mf.code.goods.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * product_promotion
 * 商品促销活动表
 */
public class ProductPromotion implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 供应商商户id（所属商户）
     */
    private Long merchantId;

    /**
     * 店铺id（所属店铺）
     */
    private Long shopId;

    /**
     * 促销活动类型：1.本周特推
     */
    private Integer type;

    /**
     * 活动状态 0:待发布(默认值) 1:进行中 2:已停止
     */
    private Integer status;

    /**
     * 推广产品描述
     */
    private String productDetail;

    /**
     * 推广图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    private String promotionPics;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 标记删除 0 :正常 1:删除
     */
    private Integer del;

    /**
     * product_promotion
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 产品id
     * @return product_id 产品id
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 产品id
     * @param productId 产品id
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * 供应商商户id（所属商户）
     * @return merchant_id 供应商商户id（所属商户）
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 供应商商户id（所属商户）
     * @param merchantId 供应商商户id（所属商户）
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id（所属店铺）
     * @return shop_id 店铺id（所属店铺）
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id（所属店铺）
     * @param shopId 店铺id（所属店铺）
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 促销活动类型：1.本周特推
     * @return type 促销活动类型：1.本周特推
     */
    public Integer getType() {
        return type;
    }

    /**
     * 促销活动类型：1.本周特推
     * @param type 促销活动类型：1.本周特推
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 活动状态 0:待发布(默认值) 1:进行中 2:已停止
     * @return status 活动状态 0:待发布(默认值) 1:进行中 2:已停止
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 活动状态 0:待发布(默认值) 1:进行中 2:已停止
     * @param status 活动状态 0:待发布(默认值) 1:进行中 2:已停止
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 推广产品描述
     * @return product_detail 推广产品描述
     */
    public String getProductDetail() {
        return productDetail;
    }

    /**
     * 推广产品描述
     * @param productDetail 推广产品描述
     */
    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail == null ? null : productDetail.trim();
    }

    /**
     * 推广图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     * @return promotion_pics 推广图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    public String getPromotionPics() {
        return promotionPics;
    }

    /**
     * 推广图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     * @param promotionPics 推广图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    public void setPromotionPics(String promotionPics) {
        this.promotionPics = promotionPics == null ? null : promotionPics.trim();
    }

    /**
     * 活动开始时间
     * @return start_time 活动开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 活动开始时间
     * @param startTime 活动开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 活动结束时间
     * @return end_time 活动结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 活动结束时间
     * @param endTime 活动结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 标记删除 0 :正常 1:删除
     * @return del 标记删除 0 :正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 标记删除 0 :正常 1:删除
     * @param del 标记删除 0 :正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append(", productDetail=").append(productDetail);
        sb.append(", promotionPics=").append(promotionPics);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}