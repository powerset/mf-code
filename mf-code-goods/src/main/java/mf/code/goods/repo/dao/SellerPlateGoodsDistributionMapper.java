package mf.code.goods.repo.dao;

import mf.code.goods.api.seller.dto.DisProductListInfoDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SellerPlateGoodsDistributionMapper {
    List<DisProductListInfoDTO> gethaveDistributionGoodsInfo(@Param("shopId") String shopId);

    List<DisProductListInfoDTO> queryByProductidAndSid(Map<String, Object> map);

    /**
     * 抖带带 查询本店铺分销的商品
     * @param shopId
     * @return
     */
    List<DisProductListInfoDTO> gethaveDistributionGoodsInfo4DDD(String shopId);
}
