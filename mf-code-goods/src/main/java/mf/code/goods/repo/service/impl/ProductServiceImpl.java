package mf.code.goods.repo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * mf.code.goods.repo.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-30 17:45
 */
@Slf4j
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {


}
