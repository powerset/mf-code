package mf.code.goods.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * product_stock_log
 */
public class ProductStockLog implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private Long merchantId;

    /**
     * 
     */
    private Long shopId;

    /**
     * 类型 1:商户修改库存 2:消费库存 3:退换库存
     */
    private Integer type;

    /**
     * 订单号 冗余 当前不用 0:商户的操作
     */
    private Long orderId;

    /**
     * 
     */
    private Long productId;

    /**
     * 
     */
    private Long skuId;

    /**
     * 库存数 如果是消费库/减少库存存 为负数. 如果是还库存/增加库存,为正数
     */
    private Integer stockNumber;

    /**
     * 
     */
    private Date ctime;

    /**
     * product_stock_log
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return merchant_id 
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 
     * @param merchantId 
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 
     * @return shop_id 
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 
     * @param shopId 
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 类型 1:商户修改库存 2:消费库存 3:退换库存
     * @return type 类型 1:商户修改库存 2:消费库存 3:退换库存
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型 1:商户修改库存 2:消费库存 3:退换库存
     * @param type 类型 1:商户修改库存 2:消费库存 3:退换库存
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 订单号 冗余 当前不用 0:商户的操作
     * @return order_id 订单号 冗余 当前不用 0:商户的操作
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * 订单号 冗余 当前不用 0:商户的操作
     * @param orderId 订单号 冗余 当前不用 0:商户的操作
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 
     * @return product_id 
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 
     * @param productId 
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * 
     * @return sku_id 
     */
    public Long getSkuId() {
        return skuId;
    }

    /**
     * 
     * @param skuId 
     */
    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    /**
     * 库存数 如果是消费库/减少库存存 为负数. 如果是还库存/增加库存,为正数
     * @return stock_number 库存数 如果是消费库/减少库存存 为负数. 如果是还库存/增加库存,为正数
     */
    public Integer getStockNumber() {
        return stockNumber;
    }

    /**
     * 库存数 如果是消费库/减少库存存 为负数. 如果是还库存/增加库存,为正数
     * @param stockNumber 库存数 如果是消费库/减少库存存 为负数. 如果是还库存/增加库存,为正数
     */
    public void setStockNumber(Integer stockNumber) {
        this.stockNumber = stockNumber;
    }

    /**
     * 
     * @return ctime 
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 
     * @param ctime 
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", type=").append(type);
        sb.append(", orderId=").append(orderId);
        sb.append(", productId=").append(productId);
        sb.append(", skuId=").append(skuId);
        sb.append(", stockNumber=").append(stockNumber);
        sb.append(", ctime=").append(ctime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}