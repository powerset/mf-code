package mf.code.goods.repo.repostory.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.repo.dao.ProductGroupMapMapper;
import mf.code.goods.repo.dao.ProductGroupMapper;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.po.ProductGroup;
import mf.code.goods.repo.po.ProductGroupMap;
import mf.code.goods.repo.po.ProductSku;
import mf.code.goods.repo.repostory.AppletProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author gel
 * @date 2019-06-13 14:42
 */
@Slf4j
@Service
public class AppletProductRepositoryImpl implements AppletProductRepository {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private ProductGroupMapper productGroupMapper;
    @Autowired
    private ProductGroupMapMapper productGroupMapMapper;

    /**
     * 销量排行列表优化
     * 条件：店铺（入参） AND 显示类型（入参） AND 上架状态 AND 未删除 AND sku展示
     *
     * @param param
     * @return
     */
    @Override
    public List<ProductDistributionDTO> pageWithSalesVolumeDescForListOptimize(Map<String, Object> param) {
        if (CollectionUtils.isEmpty(param)) {
            return new ArrayList<>();
        }
        if (param.get("shopId") == null) {
            return new ArrayList<>();
        }
        Long shopId = Long.valueOf(param.get("shopId").toString());
        long offset = Long.valueOf(param.get("offset").toString());
        long size = Long.valueOf(param.get("size").toString());

        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .in(Product::getFromFlag, Arrays.asList(ProductFromFlagEnum.SELF.getCode(), ProductFromFlagEnum.PLAT.getCode()))
                .eq(Product::getDel, DelEnum.NO.getCode());
        if (param.get("showType") != null) {
            queryWrapper.lambda().eq(Product::getShowType, param.get("showType"));
        }
        // 排序
        queryWrapper.lambda().orderByDesc(Product::getOrderSales, Product::getPutawayTime);
        Page<Product> page = new Page<>(offset / size + 1, size);
        IPage<Product> iPage = productMapper.selectPage(page, queryWrapper);
        List<Product> productList = iPage.getRecords();
        if (CollectionUtils.isEmpty(productList)) {
            return new ArrayList<>();
        }
        return getProductDistributionListForFrontEnd(productList);
    }

    /**
     * 组合小程序端商品查询返回结果
     *
     * @param productList
     * @return
     */
    private List<ProductDistributionDTO> getProductDistributionListForFrontEnd(List<Product> productList) {
        List<Long> parentIds = new ArrayList<>();
        for (Product product : productList) {
            parentIds.add(product.getParentId());
        }
        QueryWrapper<ProductSku> skuQueryWrapper = new QueryWrapper<>();
        skuQueryWrapper.lambda()
                .in(ProductSku::getProductId, parentIds)
                .eq(ProductSku::getSnapshoot, 0)
                .eq(ProductSku::getNotShow, 0)
                .eq(ProductSku::getDel, DelEnum.NO.getCode());
        List<ProductSku> productSkuList = productSkuMapper.selectList(skuQueryWrapper);
        List<ProductDistributionDTO> productDistributionDTOList = new ArrayList<>();
        for (Product product : productList) {
            ProductDistributionDTO distributionDTO = new ProductDistributionDTO();
            BeanUtils.copyProperties(product, distributionDTO);
            distributionDTO.setProductId(product.getId());
            distributionDTO.setSalesVolume(product.getOrderSales().toString());
            distributionDTO.setPrice(product.getProductPriceMin());
            distributionDTO.setThirdPrice(product.getThirdPriceMin());
            // 销量
            Integer stock = 0;
            for (ProductSku productSku : productSkuList) {
                if (productSku.getProductId().equals(product.getParentId())) {
                    stock += productSku.getStock();
                }
            }
            distributionDTO.setStock(stock);
            distributionDTO.setInitSale(product.getInitSales());
            productDistributionDTOList.add(distributionDTO);
        }
        return productDistributionDTOList;
    }

    /**
     * 销量排行列表优化
     * <p>
     * 条件：1、分组id（分组表），查询分组内的商品id列表
     * 2、店铺（入参） AND 显示类型（入参） AND 上架状态 AND 未删除 AND sku展示 AND 分组内商品ID列表
     *
     * @param param
     * @return
     */
    @Override
    public List<ProductDistributionDTO> pageWithSalesVolumeDescByGroupIdForListOptimize(Map<String, Object> param) {
        if (CollectionUtils.isEmpty(param)) {
            return new ArrayList<>();
        }
        if (param.get("shopId") == null) {
            return new ArrayList<>();
        }
        Long shopId = Long.valueOf(param.get("shopId").toString());
        long offset = Long.valueOf(param.get("offset").toString());
        long size = Long.valueOf(param.get("size").toString());
        Long productGroupId = Long.valueOf(param.get("groupId").toString());

        ProductGroup productGroup = productGroupMapper.selectByPrimaryKey(productGroupId);
        if (productGroup == null || productGroup.getDel() == DelEnum.YES.getCode()) {
            return new ArrayList<>();
        }
        QueryWrapper<ProductGroupMap> groupMapQueryWrapper = new QueryWrapper<>();
        groupMapQueryWrapper.lambda().eq(ProductGroupMap::getGroupId, productGroupId).eq(ProductGroupMap::getDel, DelEnum.NO.getCode());
        List<ProductGroupMap> groupMapList = productGroupMapMapper.selectList(groupMapQueryWrapper);
        SortedSet<Long> productIdSet = new TreeSet<>();
        for (ProductGroupMap productGroupMap : groupMapList) {
            // 判断超过1000个id则返回，保证程序正确执行
            if (productIdSet.size() >= 1000) {
                log.error("商品分组查询列表，单个分组商品数超过1000，分组id" + productGroupId);
                break;
            }
            productIdSet.add(productGroupMap.getProductId());
        }
        if (productIdSet.size() == 0) {
            return new ArrayList<>();
        }
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Product::getShopId, shopId)
                .eq(Product::getStatus, ProductConstant.Status.ON_SALE)
                .in(Product::getId, productIdSet)
                .eq(Product::getDel, DelEnum.NO.getCode());
        if (param.get("showType") != null) {
            queryWrapper.lambda().eq(Product::getShowType, param.get("showType"));
        }
        // 排序
        queryWrapper.lambda().orderByDesc(Product::getOrderSales, Product::getPutawayTime);
        Page<Product> page = new Page<>(offset / size + 1, size);
        IPage<Product> iPage = productMapper.selectPage(page, queryWrapper);
        List<Product> productList = iPage.getRecords();
        if (CollectionUtils.isEmpty(productList)) {
            return new ArrayList<>();
        }
        return getProductDistributionListForFrontEnd(productList);
    }
}
