package mf.code.goods.repo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.goods.repo.po.ProductPromotion;

/**
 * mf.code.goods.repo.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 09:02
 */
public interface ProductPromotionService extends IService<ProductPromotion> {
}
