package mf.code.goods.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * product_group
 * 商品分组表，记录商户端创建修改分组信息。
 */
public class ProductGroup implements Serializable {
    /**
     * 商品分组id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 分组类型（1：商品分组）
     */
    private Integer type;

    /**
     * 分组名
     */
    private String name;

    /**
     * 分组排序商家编辑，默认为0。正序排列
     */
    private Integer sort;

    /**
     * 冗余商品数量，方便查询
     */
    private Integer goodsNum;

    /**
     * 删除标识
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * product_group
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商品分组id
     * @return id 商品分组id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商品分组id
     * @param id 商品分组id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 分组类型（1：商品分组）
     * @return type 分组类型（1：商品分组）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 分组类型（1：商品分组）
     * @param type 分组类型（1：商品分组）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 分组名
     * @return name 分组名
     */
    public String getName() {
        return name;
    }

    /**
     * 分组名
     * @param name 分组名
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 分组排序商家编辑，默认为0。正序排列
     * @return sort 分组排序商家编辑，默认为0。正序排列
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 分组排序商家编辑，默认为0。正序排列
     * @param sort 分组排序商家编辑，默认为0。正序排列
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 冗余商品数量，方便查询
     * @return goods_num 冗余商品数量，方便查询
     */
    public Integer getGoodsNum() {
        return goodsNum;
    }

    /**
     * 冗余商品数量，方便查询
     * @param goodsNum 冗余商品数量，方便查询
     */
    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    /**
     * 删除标识
     * @return del 删除标识
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识
     * @param del 删除标识
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", type=").append(type);
        sb.append(", name=").append(name);
        sb.append(", sort=").append(sort);
        sb.append(", goodsNum=").append(goodsNum);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}