package mf.code.goods.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * product_specs
 */
public class ProductSpecs implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 规格值 (颜色:红/蓝/...   尺寸:170/175/...)
     */
    private String specsValue;

    /**
     * 规格名称的id fk product_specs_name.id
     */
    private Long specsNameId;

    /**
     * 父节点主键. 例:
id   parent    specs_value
1       0             颜色
2       1              红
3       1              白
4       0             尺寸
5       4               大
     */
    private Long parentId;

    /**
     * 对parent_id的冗余
     */
    private String parentName;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 标记删除 0:正常 1:删除
     */
    private Integer del;

    /**
     * product_specs
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 规格值 (颜色:红/蓝/...   尺寸:170/175/...)
     * @return specs_value 规格值 (颜色:红/蓝/...   尺寸:170/175/...)
     */
    public String getSpecsValue() {
        return specsValue;
    }

    /**
     * 规格值 (颜色:红/蓝/...   尺寸:170/175/...)
     * @param specsValue 规格值 (颜色:红/蓝/...   尺寸:170/175/...)
     */
    public void setSpecsValue(String specsValue) {
        this.specsValue = specsValue == null ? null : specsValue.trim();
    }

    /**
     * 规格名称的id fk product_specs_name.id
     * @return specs_name_id 规格名称的id fk product_specs_name.id
     */
    public Long getSpecsNameId() {
        return specsNameId;
    }

    /**
     * 规格名称的id fk product_specs_name.id
     * @param specsNameId 规格名称的id fk product_specs_name.id
     */
    public void setSpecsNameId(Long specsNameId) {
        this.specsNameId = specsNameId;
    }

    /**
     * 父节点主键. 例: id   parent    specs_value 1       0             颜色 2       1              红 3       1              白 4       0             尺寸 5       4               大
     * @return parent_id 父节点主键. 例: id   parent    specs_value 1       0             颜色 2       1              红 3       1              白 4       0             尺寸 5       4               大
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 父节点主键. 例: id   parent    specs_value 1       0             颜色 2       1              红 3       1              白 4       0             尺寸 5       4               大
     * @param parentId 父节点主键. 例: id   parent    specs_value 1       0             颜色 2       1              红 3       1              白 4       0             尺寸 5       4               大
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 对parent_id的冗余
     * @return parent_name 对parent_id的冗余
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * 对parent_id的冗余
     * @param parentName 对parent_id的冗余
     */
    public void setParentName(String parentName) {
        this.parentName = parentName == null ? null : parentName.trim();
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 标记删除 0:正常 1:删除
     * @return del 标记删除 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 标记删除 0:正常 1:删除
     * @param del 标记删除 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", specsValue=").append(specsValue);
        sb.append(", specsNameId=").append(specsNameId);
        sb.append(", parentId=").append(parentId);
        sb.append(", parentName=").append(parentName);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}