package mf.code.goods.repo.repostory.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.goods.domain.aggregateroot.PromotionProduct;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.dao.ProductPromotionMapper;
import mf.code.goods.repo.dao.ProductSkuMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.repostory.PromotionProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 11:50
 */
@Slf4j
@Service
public class PromotionProductRepositoryImpl implements PromotionProductRepository {
	@Autowired
	private ProductMapper productMapper;
	@Autowired
	private ProductSkuMapper productSkuMapper;
	@Autowired
	private ProductPromotionMapper productPromotionMapper;

	/**
	 * 根据商品id 查询
	 *
	 * @param productId
	 * @return
	 */
	@Override
	public PromotionProduct findById(Long productId) {
		Product productInfo = productMapper.selectById(productId);
		if (productInfo == null) {
			log.error("商品不存在, productId = {}", productId);
			return null;
		}
		// 装配
		return assemblyPromotionProduct(productInfo);
	}

	private PromotionProduct assemblyPromotionProduct(Product productInfo) {
		PromotionProduct promotionProduct = new PromotionProduct();
		promotionProduct.setId(productInfo.getId());
		promotionProduct.setProductInfo(productInfo);
		return promotionProduct;
	}
}
