package mf.code.goods.repo.repostory;

import mf.code.goods.domain.aggregateroot.PromotionProduct;

/**
 * 推广活动相关的产品根 仓储
 * <p>
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 11:48
 */
public interface PromotionProductRepository {

	/**
	 * 根据商品id 查询
	 *
	 * @param productId
	 * @return
	 */
	PromotionProduct findById(Long productId);
}
