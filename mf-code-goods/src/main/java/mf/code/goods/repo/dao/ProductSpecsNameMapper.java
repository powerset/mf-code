package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.repo.po.ProductSpecsName;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductSpecsNameMapper extends BaseMapper<ProductSpecsName> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ProductSpecsName record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductSpecsName record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductSpecsName selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductSpecsName record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductSpecsName record);

    /**
     * 获取规格名称列表
     * @return
     */
    List<Map> findAll();

}