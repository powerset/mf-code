package mf.code.goods.repo.repostory;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.goods.dto.PlatformDistListReqDTO;
import mf.code.goods.repo.po.ProductDistributionAuditLog;

import java.util.List;

/**
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author gel
 * @date 2019-05-07 20:52
 */
public interface ProductDistributionAuditLogRepository {

    /**
     * 创建分销审核表
     *
     * @param productDistributionAuditLog
     * @return
     */
    Integer createSelective(ProductDistributionAuditLog productDistributionAuditLog);

    /**
     * 更新分销审核表
     *
     * @param productDistributionAuditLog
     * @return
     */
    Integer updateSelective(ProductDistributionAuditLog productDistributionAuditLog);

    /**
     * 根据productId查询分销审核表
     *
     * @param productId
     * @return
     */
    List<ProductDistributionAuditLog> findByProductId(Long productId);

    /**
     * 根据productId查询最近一条审核记录
     *
     * @param productId
     * @return
     */
    ProductDistributionAuditLog findByProductIdLimitOne(Long productId);

    /**
     * 作废之前所有分销设置
     *
     * @param productId
     */
    void abandonAllDistributionByProduct(Long productId);

    /**
     * 分页查询分销审核记录
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<ProductDistributionAuditLog> selectPage(Page<ProductDistributionAuditLog> page, Wrapper<ProductDistributionAuditLog> queryWrapper);

    /**
     * 根据主键id查询记录
     *
     * @param id
     * @return
     */
    ProductDistributionAuditLog selectById(Long id);
}
