package mf.code.goods.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * product_sku
 */
public class ProductSku implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 冗余 赞不写 如需商品名称,在product中获取
     */
    private String skuName;

    /**
     * sku图片 可以为空, 如果为空,获取商品主图的第一张
     */
    private String skuPic;

    /**
     * 售价
     */
    private BigDecimal skuSellingPrice;

    /**
     * 淘宝价
     */
    private BigDecimal skuThirdPrice;

    /**
     * 成本价格
     */
    private BigDecimal skuCostPrice;

    /**
     * 定义库存
     */
    private Integer stockDef;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 0 显示(默认显示)/ 1 不显示
     */
    private Integer notShow;

    /**
     * 是否在售 1:快照 0:非快照(默认)
     */
    private Integer snapshoot;

    /**
     * 计算生成的sku的展示顺序, 如果有2个属性就组成xxxyyy的数字,以此保证用户新增了一个属性的值,依然有序. 
例: 
原为:
红 1  此sort值为: 100101
红 2  此sort值为: 100102
黑 1  此sort值为: 200101
黑 2  此sort值为: 200102
改为:
红 1  此sort值为: 100101
红 2  此sort值为: 100102
红 3  此sort值为: 100103
黑 1  此sort值为: 200101
     */
    private String skuSort;

    /**
     * 类目横向排序值 例:
如果product_spec
颜色id=1
红色id=2
黑色id=3
尺寸=10
1cm的id =11
2cm的id =12
红 1cm : 此值为  1:2,10:11
     */
    private String specSort;

    /**
     * 冗余 辅助校验
     */
    private String propsJson;

    /**
     * 生成快照后,会生成新的skuId,再次保留生成快照前的skuId
     */
    private Long oldSkuId;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 标记删除 0:正常 1:删除
     */
    private Integer del;

    /**
     * 生效时间，可以作为秒杀sku的区分
     */
    private String effectTime;

    /**
     * sku额外字段（json对象：初始销量init_sales）
     */
    private String extra;

    /**
     * product_sku
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 产品id
     * @return product_id 产品id
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 产品id
     * @param productId 产品id
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 冗余 赞不写 如需商品名称,在product中获取
     * @return sku_name 冗余 赞不写 如需商品名称,在product中获取
     */
    public String getSkuName() {
        return skuName;
    }

    /**
     * 冗余 赞不写 如需商品名称,在product中获取
     * @param skuName 冗余 赞不写 如需商品名称,在product中获取
     */
    public void setSkuName(String skuName) {
        this.skuName = skuName == null ? null : skuName.trim();
    }

    /**
     * sku图片 可以为空, 如果为空,获取商品主图的第一张
     * @return sku_pic sku图片 可以为空, 如果为空,获取商品主图的第一张
     */
    public String getSkuPic() {
        return skuPic;
    }

    /**
     * sku图片 可以为空, 如果为空,获取商品主图的第一张
     * @param skuPic sku图片 可以为空, 如果为空,获取商品主图的第一张
     */
    public void setSkuPic(String skuPic) {
        this.skuPic = skuPic == null ? null : skuPic.trim();
    }

    /**
     * 售价
     * @return sku_selling_price 售价
     */
    public BigDecimal getSkuSellingPrice() {
        return skuSellingPrice;
    }

    /**
     * 售价
     * @param skuSellingPrice 售价
     */
    public void setSkuSellingPrice(BigDecimal skuSellingPrice) {
        this.skuSellingPrice = skuSellingPrice;
    }

    /**
     * 淘宝价
     * @return sku_third_price 淘宝价
     */
    public BigDecimal getSkuThirdPrice() {
        return skuThirdPrice;
    }

    /**
     * 淘宝价
     * @param skuThirdPrice 淘宝价
     */
    public void setSkuThirdPrice(BigDecimal skuThirdPrice) {
        this.skuThirdPrice = skuThirdPrice;
    }

    /**
     * 成本价格
     * @return sku_cost_price 成本价格
     */
    public BigDecimal getSkuCostPrice() {
        return skuCostPrice;
    }

    /**
     * 成本价格
     * @param skuCostPrice 成本价格
     */
    public void setSkuCostPrice(BigDecimal skuCostPrice) {
        this.skuCostPrice = skuCostPrice;
    }

    /**
     * 定义库存
     * @return stock_def 定义库存
     */
    public Integer getStockDef() {
        return stockDef;
    }

    /**
     * 定义库存
     * @param stockDef 定义库存
     */
    public void setStockDef(Integer stockDef) {
        this.stockDef = stockDef;
    }

    /**
     * 库存
     * @return stock 库存
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * 库存
     * @param stock 库存
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 0 显示(默认显示)/ 1 不显示
     * @return not_show 0 显示(默认显示)/ 1 不显示
     */
    public Integer getNotShow() {
        return notShow;
    }

    /**
     * 0 显示(默认显示)/ 1 不显示
     * @param notShow 0 显示(默认显示)/ 1 不显示
     */
    public void setNotShow(Integer notShow) {
        this.notShow = notShow;
    }

    /**
     * 是否在售 1:快照 0:非快照(默认)
     * @return snapshoot 是否在售 1:快照 0:非快照(默认)
     */
    public Integer getSnapshoot() {
        return snapshoot;
    }

    /**
     * 是否在售 1:快照 0:非快照(默认)
     * @param snapshoot 是否在售 1:快照 0:非快照(默认)
     */
    public void setSnapshoot(Integer snapshoot) {
        this.snapshoot = snapshoot;
    }

    /**
     * 计算生成的sku的展示顺序, 如果有2个属性就组成xxxyyy的数字,以此保证用户新增了一个属性的值,依然有序.  例:  原为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 黑 1  此sort值为: 200101 黑 2  此sort值为: 200102 改为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 红 3  此sort值为: 100103 黑 1  此sort值为: 200101
     * @return sku_sort 计算生成的sku的展示顺序, 如果有2个属性就组成xxxyyy的数字,以此保证用户新增了一个属性的值,依然有序.  例:  原为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 黑 1  此sort值为: 200101 黑 2  此sort值为: 200102 改为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 红 3  此sort值为: 100103 黑 1  此sort值为: 200101
     */
    public String getSkuSort() {
        return skuSort;
    }

    /**
     * 计算生成的sku的展示顺序, 如果有2个属性就组成xxxyyy的数字,以此保证用户新增了一个属性的值,依然有序.  例:  原为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 黑 1  此sort值为: 200101 黑 2  此sort值为: 200102 改为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 红 3  此sort值为: 100103 黑 1  此sort值为: 200101
     * @param skuSort 计算生成的sku的展示顺序, 如果有2个属性就组成xxxyyy的数字,以此保证用户新增了一个属性的值,依然有序.  例:  原为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 黑 1  此sort值为: 200101 黑 2  此sort值为: 200102 改为: 红 1  此sort值为: 100101 红 2  此sort值为: 100102 红 3  此sort值为: 100103 黑 1  此sort值为: 200101
     */
    public void setSkuSort(String skuSort) {
        this.skuSort = skuSort == null ? null : skuSort.trim();
    }

    /**
     * 类目横向排序值 例: 如果product_spec 颜色id=1 红色id=2 黑色id=3 尺寸=10 1cm的id =11 2cm的id =12 红 1cm : 此值为  1:2,10:11
     * @return spec_sort 类目横向排序值 例: 如果product_spec 颜色id=1 红色id=2 黑色id=3 尺寸=10 1cm的id =11 2cm的id =12 红 1cm : 此值为  1:2,10:11
     */
    public String getSpecSort() {
        return specSort;
    }

    /**
     * 类目横向排序值 例: 如果product_spec 颜色id=1 红色id=2 黑色id=3 尺寸=10 1cm的id =11 2cm的id =12 红 1cm : 此值为  1:2,10:11
     * @param specSort 类目横向排序值 例: 如果product_spec 颜色id=1 红色id=2 黑色id=3 尺寸=10 1cm的id =11 2cm的id =12 红 1cm : 此值为  1:2,10:11
     */
    public void setSpecSort(String specSort) {
        this.specSort = specSort == null ? null : specSort.trim();
    }

    /**
     * 冗余 辅助校验
     * @return props_json 冗余 辅助校验
     */
    public String getPropsJson() {
        return propsJson;
    }

    /**
     * 冗余 辅助校验
     * @param propsJson 冗余 辅助校验
     */
    public void setPropsJson(String propsJson) {
        this.propsJson = propsJson == null ? null : propsJson.trim();
    }

    /**
     * 生成快照后,会生成新的skuId,再次保留生成快照前的skuId
     * @return old_sku_id 生成快照后,会生成新的skuId,再次保留生成快照前的skuId
     */
    public Long getOldSkuId() {
        return oldSkuId;
    }

    /**
     * 生成快照后,会生成新的skuId,再次保留生成快照前的skuId
     * @param oldSkuId 生成快照后,会生成新的skuId,再次保留生成快照前的skuId
     */
    public void setOldSkuId(Long oldSkuId) {
        this.oldSkuId = oldSkuId;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 标记删除 0:正常 1:删除
     * @return del 标记删除 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 标记删除 0:正常 1:删除
     * @param del 标记删除 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 生效时间，可以作为秒杀sku的区分
     * @return effect_time 生效时间，可以作为秒杀sku的区分
     */
    public String getEffectTime() {
        return effectTime;
    }

    /**
     * 生效时间，可以作为秒杀sku的区分
     * @param effectTime 生效时间，可以作为秒杀sku的区分
     */
    public void setEffectTime(String effectTime) {
        this.effectTime = effectTime == null ? null : effectTime.trim();
    }

    /**
     * sku额外字段（json对象：初始销量init_sales）
     * @return extra sku额外字段（json对象：初始销量init_sales）
     */
    public String getExtra() {
        return extra;
    }

    /**
     * sku额外字段（json对象：初始销量init_sales）
     * @param extra sku额外字段（json对象：初始销量init_sales）
     */
    public void setExtra(String extra) {
        this.extra = extra == null ? null : extra.trim();
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", skuName=").append(skuName);
        sb.append(", skuPic=").append(skuPic);
        sb.append(", skuSellingPrice=").append(skuSellingPrice);
        sb.append(", skuThirdPrice=").append(skuThirdPrice);
        sb.append(", skuCostPrice=").append(skuCostPrice);
        sb.append(", stockDef=").append(stockDef);
        sb.append(", stock=").append(stock);
        sb.append(", notShow=").append(notShow);
        sb.append(", snapshoot=").append(snapshoot);
        sb.append(", skuSort=").append(skuSort);
        sb.append(", specSort=").append(specSort);
        sb.append(", propsJson=").append(propsJson);
        sb.append(", oldSkuId=").append(oldSkuId);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", effectTime=").append(effectTime);
        sb.append(", extra=").append(extra);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}