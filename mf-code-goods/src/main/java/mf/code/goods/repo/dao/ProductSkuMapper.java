package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.api.seller.dto.ProductSkuInfoDTO;
import mf.code.goods.repo.po.ProductSku;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductSkuMapper extends BaseMapper<ProductSku> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ProductSku record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductSku record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductSku selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductSku record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductSku record);

    /**
     * 根据商品id查询sku
     * @param productId
     * @return
     */
    List<ProductSku> findByProductId(Long productId);

    /**
     * c端库存变更(原子)
     * @param conditionMap
     * @return
     */
    int updateStockAtom(Map<String, Object> conditionMap);

    /**
     * 商户端库存变更(原子)
     * @param conditionMap
     * @return
     */
    int updateStockAtomMerchant(Map<String, Object> conditionMap);

    ProductSku selectProductSkuBySkuOldId(Long oldSkuId);

    /**
     * 获取sku中的价格相关信息
     * @param productId
     * @return
     */
    ProductSkuInfoDTO getProductSkuPriceInfo(@Param("productId") Long productId);
}
