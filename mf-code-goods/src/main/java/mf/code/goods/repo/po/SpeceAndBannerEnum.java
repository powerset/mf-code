package mf.code.goods.repo.po;

/**
 * 品台分销类目
 */
public enum SpeceAndBannerEnum {

    SPECE_ZERO("0","推荐"),
    SPECE_ONE("1","百货食品"),
    SPECE_TWO("2","服装鞋包"),
    SPECE_THREE("3","美妆饰品"),
    SPECE_FOUR("4","手机数码"),
    SPECE_FIVE("5","母婴用品"),
    SPECE_SIX("6","生活服务"),
    SPECE_SEVEN("7","家用电器"),
    SPECE_EIGHT("8","家居建材"),
    SPECE_NINE("9","运动户外"),
    SPECE_TEN("10","文化玩乐"),
    SPECE_ELEVEN("11","其他商品"),
    SPECE_TWE("12","汽配摩托");




    SpeceAndBannerEnum(String speceId,String speceName){
        this.speceId=speceId;
        this.speceName = speceName;
    }

    /**
     * 类目ID
     */
    private String speceId ;

    /**
     * 类目名
     */
    private String speceName;

    public String getSpeceId() {
        return speceId;
    }

    public void setSpeceId(String speceId) {
        this.speceId = speceId;
    }

    public String getSpeceName() {
        return speceName;
    }

    public void setSpeceName(String speceName) {
        this.speceName = speceName;
    }
}
