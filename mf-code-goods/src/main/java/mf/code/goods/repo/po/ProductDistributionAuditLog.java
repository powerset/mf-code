package mf.code.goods.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * product_distribution_audit_log
 */
public class ProductDistributionAuditLog implements Serializable {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 平台分销佣金比例,审核通过后,将此值写入product表
     */
    private BigDecimal platSupportRatio;

    /**
     * 审核状态 0:未审核 1:审核通过 2:审核不通过 3:作废
     */
    private Integer status;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 审核不通过原因
     */
    private String auditReason;

    /**
     * 审核图片
     */
    private String auditPics;

    /**
     * 审核人
     */
    private Long operatorId;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * product_distribution_audit_log
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * @return id 主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键id
     * @param id 主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 产品id
     * @return product_id 产品id
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 产品id
     * @param productId 产品id
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * 平台分销佣金比例,审核通过后,将此值写入product表
     * @return plat_support_ratio 平台分销佣金比例,审核通过后,将此值写入product表
     */
    public BigDecimal getPlatSupportRatio() {
        return platSupportRatio;
    }

    /**
     * 平台分销佣金比例,审核通过后,将此值写入product表
     * @param platSupportRatio 平台分销佣金比例,审核通过后,将此值写入product表
     */
    public void setPlatSupportRatio(BigDecimal platSupportRatio) {
        this.platSupportRatio = platSupportRatio;
    }

    /**
     * 审核状态 0:未审核 1:审核通过 2:审核不通过 3:作废
     * @return status 审核状态 0:未审核 1:审核通过 2:审核不通过 3:作废
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 审核状态 0:未审核 1:审核通过 2:审核不通过 3:作废
     * @param status 审核状态 0:未审核 1:审核通过 2:审核不通过 3:作废
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 审核时间
     * @return audit_time 审核时间
     */
    public Date getAuditTime() {
        return auditTime;
    }

    /**
     * 审核时间
     * @param auditTime 审核时间
     */
    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    /**
     * 审核不通过原因
     * @return audit_reason 审核不通过原因
     */
    public String getAuditReason() {
        return auditReason;
    }

    /**
     * 审核不通过原因
     * @param auditReason 审核不通过原因
     */
    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason == null ? null : auditReason.trim();
    }

    /**
     * 审核图片
     * @return audit_pics 审核图片
     */
    public String getAuditPics() {
        return auditPics;
    }

    /**
     * 审核图片
     * @param auditPics 审核图片
     */
    public void setAuditPics(String auditPics) {
        this.auditPics = auditPics == null ? null : auditPics.trim();
    }

    /**
     * 审核人
     * @return operator_id 审核人
     */
    public Long getOperatorId() {
        return operatorId;
    }

    /**
     * 审核人
     * @param operatorId 审核人
     */
    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", productId=").append(productId);
        sb.append(", platSupportRatio=").append(platSupportRatio);
        sb.append(", status=").append(status);
        sb.append(", auditTime=").append(auditTime);
        sb.append(", auditReason=").append(auditReason);
        sb.append(", auditPics=").append(auditPics);
        sb.append(", operatorId=").append(operatorId);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}