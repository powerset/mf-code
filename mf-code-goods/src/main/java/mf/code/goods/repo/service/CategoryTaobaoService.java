package mf.code.goods.repo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.goods.repo.po.CategoryTaobao;

/**
 * create by qc on 2019/8/22 0022
 */
public interface CategoryTaobaoService extends IService<CategoryTaobao> {
}
