package mf.code.goods.repo.vo;/**
 * create by qc on 2019/4/18 0018
 */

/**
 * @author gbf
 * 2019/4/18 0018、13:37
 */
public class ProductSkuSpecValVo{
    private Long specValId;
    private String specValVal;

    public Long getSpecValId() {
        return specValId;
    }

    public ProductSkuSpecValVo setSpecValId(Long specValId) {
        this.specValId = specValId;
        return this;
    }

    public String getSpecValVal() {
        return specValVal;
    }

    public ProductSkuSpecValVo setSpecValVal(String specValVal) {
        this.specValVal = specValVal;
        return this;
    }

    @Override
    public String toString() {
        return "ProductSkuSpecValVo{" +
                "specValId=" + specValId +
                ", specValVal='" + specValVal + '\'' +
                '}';
    }
}