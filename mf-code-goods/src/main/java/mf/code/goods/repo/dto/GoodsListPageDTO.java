package mf.code.goods.repo.dto;/**
 * create by qc on 2019/9/21 0021
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 查询商品列表页面dto
 * <p>
 * 维度类型 1:供应商 2:商品
 * 标签类型 1:爆款 2:特价 3:尾货 4:全部
 * 查询关键字
 * 商品类目
 * 排序 1:综合排序(销量) 2:上新时间
 * 当前页
 * 每页数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsListPageDTO {
    private Integer dimType;
    private Integer labType;
    private String keyWord;
    private String spec;
    private Integer sort;
    private Integer pageCurrent;
    private Integer pageSize;
}
