package mf.code.goods.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * product_group_map
 * 商品分组映射表，记录商品与分组的映射关系
 */
public class ProductGroupMap implements Serializable {
    /**
     * 商品分组映射id
     */
    private Long id;

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 来源商品id
     */
    private Long parentProductId;

    /**
     * 分组id
     */
    private Long groupId;

    /**
     * 删除标识
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * product_group_map
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商品分组映射id
     * @return id 商品分组映射id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商品分组映射id
     * @param id 商品分组映射id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商品id
     * @return product_id 商品id
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 商品id
     * @param productId 商品id
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * 来源商品id
     * @return parent_product_id 来源商品id
     */
    public Long getParentProductId() {
        return parentProductId;
    }

    /**
     * 来源商品id
     * @param parentProductId 来源商品id
     */
    public void setParentProductId(Long parentProductId) {
        this.parentProductId = parentProductId;
    }

    /**
     * 分组id
     * @return group_id 分组id
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * 分组id
     * @param groupId 分组id
     */
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    /**
     * 删除标识
     * @return del 删除标识
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识
     * @param del 删除标识
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", parentProductId=").append(parentProductId);
        sb.append(", groupId=").append(groupId);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}