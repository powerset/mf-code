package mf.code.goods.repo.repostory.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.goods.repo.dao.CategoryTaobaoMapper;
import mf.code.goods.repo.po.CategoryTaobao;
import mf.code.goods.repo.repostory.CategoryTaobaoRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-08 19:55
 */
@Service
public class CategoryTaobaoRepositoryImpl implements CategoryTaobaoRepository {

    @Autowired
    private CategoryTaobaoMapper categoryTaobaoMapper;

    @Override
    public CategoryTaobao selectBySid(String categorySid, Integer level) {
        QueryWrapper<CategoryTaobao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(CategoryTaobao::getSid, categorySid).eq(CategoryTaobao::getLevel, level).eq(CategoryTaobao::getType, 1);
        return categoryTaobaoMapper.selectOne(queryWrapper);
    }

    /**
     * 查询品类全部
     *
     * @param level
     * @return
     */
    @Override
    public List<CategoryTaobao> selectByLevel(Integer level) {
        QueryWrapper<CategoryTaobao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CategoryTaobao::getLevel, level)
                .eq(CategoryTaobao::getType, 1)
                .orderByAsc(CategoryTaobao::getSort);
        return categoryTaobaoMapper.selectList(queryWrapper);
    }

    /**
     * 根据sid列表查询品类数据
     *
     * @param sidList
     * @param level
     * @return
     */
    @Override
    public List<CategoryTaobao> selectBySidList(List<String> sidList, Integer level) {
        if (CollectionUtils.isEmpty(sidList)) {
            return new ArrayList<>();
        }
        QueryWrapper<CategoryTaobao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CategoryTaobao::getLevel, level)
                .in(CategoryTaobao::getSid, sidList)
                .eq(CategoryTaobao::getType, 1);
        return categoryTaobaoMapper.selectList(queryWrapper);
    }

    /**
     * 根据0级新类目sid查询1级类目
     *
     * @param parentSid
     * @return
     */
    @Override
    public List<CategoryTaobao> selectNextLevelByParentSid(String parentSid, Integer level) {
        if (StringUtils.isBlank(parentSid)) {
            return new ArrayList<>();
        }
        QueryWrapper<CategoryTaobao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CategoryTaobao::getType, 1)
                .eq(CategoryTaobao::getLevel, level)
                .eq(CategoryTaobao::getParentId, parentSid);
        return categoryTaobaoMapper.selectList(queryWrapper);
    }

    /**
     * 根据id更新
     *
     * @param categoryTaobao
     * @return
     */
    @Override
    public Integer updateById(CategoryTaobao categoryTaobao) {
        if (categoryTaobao == null || categoryTaobao.getId() == null) {
            return 0;
        }
        categoryTaobao.setUtime(new Date());
        return categoryTaobaoMapper.updateByPrimaryKeySelective(categoryTaobao);
    }
}
