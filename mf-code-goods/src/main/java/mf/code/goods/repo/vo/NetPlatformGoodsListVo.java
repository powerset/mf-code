package mf.code.goods.repo.vo;/**
 * create by qc on 2019/9/21 0021
 */

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author gbf
 * 2019/9/21 0021、14:51
 */
@Data
@Slf4j
public class NetPlatformGoodsListVo {
    /**
     * 商品id
     */
    private Long productId;
    /**
     * 主图
     */
    private String mainPic;
    /**
     * 售价
     */
    private BigDecimal price;

    /**
     * 划线价
     */
    private BigDecimal fkPrice;
    /**
     * 商品名称
     */
    private String productTitle;
    /**
     * 标签 爆款
     */
    private Integer pop;
    /**
     * 标签 清仓
     */
    private Integer clean;
    /**
     * 标签 显示特价
     */
    private Integer limit;
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 店铺logo
     */
    private String shopLogo;
    /**
     * 店铺名称
     */
    private String shopName;

}
