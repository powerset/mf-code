package mf.code.goods.repo.repostory.impl;/**
 * create by qc on 2019/4/2 0002
 */

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.common.DelEnum;
import mf.code.goods.api.seller.dto.SellerProductListReqDTO;
import mf.code.goods.common.constant.ProductFromFlagEnum;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.repo.dao.ProductMapper;
import mf.code.goods.repo.po.Product;
import mf.code.goods.repo.repostory.SellerProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author gbf
 * 2019/4/2 0002、20:19
 */
@Repository
public class SellerProductRepositoryImpl implements SellerProductRepository {

    @Autowired
    private ProductMapper productMapper;

    /**
     * 根据商品父类目,查询其直接子类目的商品列表
     */
    @Override
    public List<Product> selectProductListByParentSpece(Map param) {
        return productMapper.selectProductListByParentSpece(param);
    }

    /**
     * 创建商品
     *
     * @param product product
     * @return SimpleResponse
     */
    @Override
    public Integer create(Product product) {
        return productMapper.insertSelective(product);
    }

    /**
     * 根据主键列表查询商品信息
     *
     * @param idList
     * @return
     */
    @Override
    public List<Product> selectBatchIds(List<Long> idList) {
        if (CollectionUtils.isEmpty(idList)) {
            return new ArrayList<>();
        }
        if (idList.size() > 100) {
            idList = idList.subList(0, 100);
        }
        return productMapper.selectBatchIds(idList);
    }

    /**
     * 根据主键查询商品
     *
     * @param id
     * @return
     */
    @Override
    public Product selectById(Long id) {
        return productMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据主键查询商品
     *
     * @param product
     * @return
     */
    @Override
    public Integer updateBySelective(Product product) {
        if (product == null) {
            return 0;
        }
        product.setUtime(new Date());
        return productMapper.updateByPrimaryKeySelective(product);
    }

    /**
     * 根据条件分页查询
     *
     * @param reqDTO
     * @return
     */
    @Override
    public IPage<Product> pageByParams(SellerProductListReqDTO reqDTO) {
        Page<Product> ipage = new Page<>(reqDTO.getPageCurrent(), reqDTO.getPageSize());
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Product::getShopId, reqDTO.getShopId());
        if (reqDTO.getProductId() != null && reqDTO.getProductId() > 0) {
            queryWrapper.lambda().eq(Product::getId, reqDTO.getProductId());
        }
        // 抖带带扩展 onSale=10 已上架商品 包含供应商下架.
        if (reqDTO.getOnSale() == ProductConstant.Status.DDD_ON_SALE) {
            queryWrapper.lambda().ne(Product::getStatus, ProductConstant.Status.NOT_ON_SALE);
        } else {
            // 在售
            if (reqDTO.getOnSale() == ProductConstant.Status.ON_SALE) {
                queryWrapper.lambda().eq(Product::getStatus, ProductConstant.Status.ON_SALE);
            } else {
                queryWrapper.lambda().ne(Product::getStatus, ProductConstant.Status.ON_SALE);
            }
        }

        if (!StringUtils.isEmpty(reqDTO.getProductTitle())) {
            queryWrapper.lambda().like(Product::getProductTitle, reqDTO.getProductTitle());
        }
        if (reqDTO.getOrigin() != null && reqDTO.getOrigin() != 0) {
            queryWrapper.lambda().eq(Product::getFromFlag, reqDTO.getOrigin());
        } else {
            queryWrapper.lambda().in(Product::getFromFlag, Arrays.asList(ProductFromFlagEnum.SELF.getCode(), ProductFromFlagEnum.PLAT.getCode()));
        }
        if (reqDTO.getDistributionMode() != null) {
            if (reqDTO.getDistributionMode() == 1) {
                queryWrapper.lambda().isNotNull(Product::getSelfSupportRatio).isNull(Product::getPlatSupportRatio);
            }
            if (reqDTO.getDistributionMode() == 2) {
                queryWrapper.lambda().isNotNull(Product::getSelfSupportRatio).isNotNull(Product::getPlatSupportRatio);
            }
            if (reqDTO.getDistributionMode() == 3) {
                queryWrapper.lambda().isNull(Product::getSelfSupportRatio).isNotNull(Product::getPlatSupportRatio);
            }
            if (reqDTO.getDistributionMode() == 4) {
                queryWrapper.lambda().isNull(Product::getSelfSupportRatio).isNull(Product::getPlatSupportRatio);
            }
        }
        queryWrapper.lambda().

                eq(Product::getDel, DelEnum.NO.getCode());

        queryWrapper.lambda().

                orderByDesc(Product::getPutawayTime);
        // TODO 库存销售数量
        return productMapper.selectPage(ipage, queryWrapper);
    }

    /**
     * 更新订单销量字段，通过id条件
     *
     * @param id
     * @param number
     * @return
     */
    @Override
    public Integer updateOrderSalesById(Long id, Integer number) {
        return productMapper.updateOrderSalesById(id, number);
    }

    /**
     * 汇总数量
     *
     * @param conditionMap
     * @return
     */
    @Deprecated
    @Override
    public int countByConditionsForPlat(Map<String, Object> conditionMap) {
        return productMapper.findByConditionsForPlatPaging(conditionMap);
    }

    /**
     * 查询列表
     *
     * @param conditionMap
     * @return
     */
    @Override
    public List<Map> findByConditionsForPlat(Map<String, Object> conditionMap) {
        return productMapper.findByConditionsForPlat(conditionMap);
    }

    /**
     * 拼接条件统计数量
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public int selectCount(QueryWrapper<Product> queryWrapper) {
        return productMapper.selectCount(queryWrapper);
    }

    /**
     * 拼接mybatis-plus分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<Product> selectPage(IPage<Product> page, QueryWrapper<Product> queryWrapper) {
        return productMapper.selectPage(page, queryWrapper);
    }

    /**
     * 根据商品id，更新 佣金
     *
     * @param params
     * @return
     */
    @Override
    public int updateByProductId(Map<String, Object> params) {
        return productMapper.updateByProductId(params);
    }

    /**
     * 根据条件查询商品
     *
     * @param beanToMap
     * @return
     */
    @Deprecated
    @Override
    public int countByConditions(Map<String, Object> beanToMap) {
        return productMapper.findByConditionsPaging(beanToMap);
    }

    /**
     * 根据条件查询
     *
     * @param beanToMap
     * @return
     */
    @Deprecated
    @Override
    public List<Map<String, Object>> findByConditions(Map<String, Object> beanToMap) {
        return productMapper.findByConditions(beanToMap);
    }

    /**
     * 全量更新
     *
     * @param product
     * @return
     */
    @Override
    public int update(Product product) {
        return productMapper.updateByPrimaryKey(product);
    }

    /**
     * 根据父商品id下架商品
     *
     * @param productId
     */
    @Override
    public void downshilfProductByParentId(Long productId) {
        productMapper.downshilfProductByParentId(productId);
    }

    /**
     * 下架商品
     *
     * @param conditionMap
     */
    @Override
    public int downshilf(Map<String, Object> conditionMap) {
        return productMapper.downshilf(conditionMap);
    }

}
