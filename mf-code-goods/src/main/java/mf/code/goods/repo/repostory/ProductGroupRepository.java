package mf.code.goods.repo.repostory;

import mf.code.common.AddOrReduceEnum;
import mf.code.goods.repo.po.ProductGroup;

import java.util.List;

/**
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author gel
 * @date 2019-05-16 14:34
 */
public interface ProductGroupRepository {

    /**
     * 创建商品分组
     *
     * @param productGroup
     * @return
     */
    Integer createSelective(ProductGroup productGroup);

    /**
     * 根据分组id更新记录
     *
     * @param productGroup
     * @return
     */
    Integer updateSelectiveById(ProductGroup productGroup);

    /**
     * 根据主键查询分组
     *
     * @param groupId
     * @return
     */
    ProductGroup selectById(Long groupId);

    /**
     * 根据店铺查询分组列表，商户端用
     *
     * @param shopId
     * @return
     */
    List<ProductGroup> selectByShopIdForSeller(Long shopId);

    /**
     * 根据店铺查询分组列表，小程序端使用
     *
     * @param shopId
     * @return
     */
    List<ProductGroup> selectByShopIdForApplet(Long shopId);

    /**
     * 校验分组名称不重复
     *
     * @param shopId
     * @return
     */
    List<ProductGroup> selectByShopIdNameDel(Long shopId, String name);

    /**
     * 根据主键删除分组
     *
     * @param groupId
     * @return
     */
    Integer updateDelById(Long groupId);

    /**
     * 根据主键列表更新商品数量
     *
     * @param groupIdList
     * @return
     */
    Integer updateGoodsNumByIdList(List<Long> groupIdList, AddOrReduceEnum addOrReduceEnum);
}
