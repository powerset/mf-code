package mf.code.goods.repo.repostory;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.goods.repo.po.CategoryTaobao;

import java.util.List;

/**
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author gel
 * @date 2019-05-08 19:55
 */
public interface CategoryTaobaoRepository {

    /**
     * 根据sid查询品类
     *
     * @param categorySid
     * @param level
     * @return
     */
    CategoryTaobao selectBySid(String categorySid, Integer level);

    /**
     * 查询品类全部
     *
     * @param level
     * @return
     */
    List<CategoryTaobao> selectByLevel(Integer level);

    /**
     * 查询品类全部
     *
     * @param level
     * @return
     */
    List<CategoryTaobao> selectBySidList(List<String> sidList, Integer level);

    /**
     * 根据上级类目sid查询下级类目
     *
     * @param parentSid
     * @return
     */
    List<CategoryTaobao> selectNextLevelByParentSid(String parentSid, Integer level);

    /**
     * 根据id更新
     *
     * @param categoryTaobao
     * @return
     */
    Integer updateById(CategoryTaobao categoryTaobao);
}
