package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.repo.po.ProductGroup;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductGroupMapper extends BaseMapper<ProductGroup> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(ProductGroup record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductGroup record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductGroup selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductGroup record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductGroup record);

    /**
     * 根据主键列表减少商品数量-1
     *
     * @param groupIdList
     */
    int updateReduceGoodsNumByPrimaryKeyList(@Param(value = "groupIdList") List<Long> groupIdList);

    /**
     * 根据主键列表增加商品数量+1
     *
     * @param groupIdList
     */
    int updateAddGoodsNumByPrimaryKeyList(@Param(value = "groupIdList") List<Long> groupIdList);
}
