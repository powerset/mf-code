package mf.code.goods.repo.repostory;

import mf.code.goods.repo.po.ProductSpecs;
import mf.code.goods.repo.po.ProductSpecsName;

import java.util.List;

/**
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author gel
 * @date 2019-04-19 17:20
 */
public interface SellerProductSpecsRepository {

    /**
     * 通过规格值id列表查询
     *
     * @param specsList
     * @return
     */
    List<ProductSpecs> selectSpecsListByIdForSeller(List<Long> specsList);

    /**
     * 通过规格名id列表查询
     *
     * @param specsNameList
     * @return
     */
    List<ProductSpecsName> selectSpecsNameListByIdForSeller(List<Long> specsNameList);
}
