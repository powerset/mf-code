package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.SellerAwardGoodsDTO;
import mf.code.goods.repo.po.NetPlatformGoodsPo;
import mf.code.goods.repo.po.Product;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductMapper extends BaseMapper<Product> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record a
     */
    int insert(Product record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(Product record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    Product selectByPrimaryKey(Long id);

    Product selectByPidAndShopId(Map<String, Object> mp);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Product record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Product record);

    /**
     * 根据店铺id 按销量从大到小 分页查询 所有可售商品
     *
     * @param param
     * @return
     */
    @Deprecated
    List<ProductDistributionDTO> pageGoodsSaleWithSalesVolumeDesc(Map param);

    /**
     * 根据店铺id 按销量从大到小 分页查询 所有可售商品 按分组查询
     *
     * @param param
     * @return
     */
    @Deprecated
    List<ProductDistributionDTO> pageGoodsSaleWithSalesVolumeDescByGroupId(Map param);

    /**
     * 商品管理列表
     *
     * @param conditionMap 条件
     * @return List<Map>
     */
    @Deprecated
    List<Map<String,Object>> findByConditions(Map conditionMap);
    /**
     * 商品管理列表
     *
     * @param conditionMap 条件
     * @return List<Map>
     */
    @Deprecated
    int findByConditionsPaging(Map conditionMap);
    /**
     * 平台-商品管理列表
     *
     * @param conditionMap 条件
     * @return List<Map>
     */
    @Deprecated
    List<Map> findByConditionsForPlat(Map conditionMap);
    /**
     * 平台-商品管理列表
     *
     * @param conditionMap 条件
     * @return List<Map>
     */
    @Deprecated
    int findByConditionsForPlatPaging(Map conditionMap);

    /**
     * 下架此商品在所有店铺的分销
     *
     * @param conditionMap .
     * @return .
     */
    int downshilf(Map conditionMap);

    /**
     * @param parseLong
     * @return
     */
    Long countProductByParentId(long parseLong);

    /**
     * 统计在售商品数量
     *
     * @param shopId
     * @return
     */
    int countOnsale(long shopId);

    /**
     * 统计仓库中数量
     *
     * @param shopId
     * @return
     */
    int countInStore(long shopId);

    /**
     * 下架所有分销商品
     * @param productId
     */
    void downshilfProductByParentId(Long productId);

    /***
     * 查询分销商是否已经在分销中
     * @param params
     * @return
     */
    Product selectByPidAndShopIdExist(Map<String, Object> params);

    List<Long> queryProductIds(Map<String, Object> param);

    /**
     * 根据productID修改佣金
     * @param params
     * @return
     */
    int updateByProductId(Map<String, Object> params);

    /**
     * 汇总 所有商品-奖品信息
     *
     * @param param
     * @return
     */
    @Deprecated
    int pageAwardGoodsTotal(Map param);

    /**
     * 根据店铺id 按id倒叙 分页查询 所有商品-奖品信息 按分组查询
     *
     * @param param
     * @return
     */
    @Deprecated
    List<SellerAwardGoodsDTO> pageAwardGoodsOrderByIdDesc(Map param);

    /**
     * 更新订单销量字段，通过id条件
     *
     * @param id
     * @param number
     * @return
     */
    Integer updateOrderSalesById(@Param(value = "id") Long id, @Param(value = "number") Integer number);
    /**
     * 根据商品父类目,查询其直接子类目的商品列表
     */
    List<Product> selectProductListByParentSpece(Map param);

    /**
     * 查询全网通所有商品
     */
    List<NetPlatformGoodsPo> findNetPlatformGoodsList(Map param);
}
