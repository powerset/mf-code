package mf.code.goods.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.goods.repo.po.ProductDistributionAuditLog;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDistributionAuditLogMapper extends BaseMapper<ProductDistributionAuditLog> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(ProductDistributionAuditLog record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductDistributionAuditLog record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ProductDistributionAuditLog selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductDistributionAuditLog record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductDistributionAuditLog record);

    /**
     * 查询最新的一次分销设置记录
     *
     * @param productId
     * @return
     */
    ProductDistributionAuditLog findByProductIdLimitOne(Long productId);

    /**
     * 未审核记录作废设置
     *
     * @param productId
     */
    void abandonAllDistributionByProduct(Long productId);

    /**
     * 根据产品ID和审核状态获取审核信息
     * @param pid
     * @return
     */
    ProductDistributionAuditLog findByProductIdAndStatus(long pid);
}
