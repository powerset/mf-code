package mf.code.goods.repo.repostory;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.goods.repo.po.ProductGroupMap;

import java.util.List;

/**
 * mf.code.goods.repo.repostory
 * Description:
 *
 * @author gel
 * @date 2019-05-16 14:35
 */
public interface ProductGroupMapRepository {

    /**
     * 创建商品分组
     *
     * @param productGroupMap
     * @return
     */
    Integer createOrUpdateSelective(ProductGroupMap productGroupMap);

    /**
     * 根据商品分组id查询商品分组映射列表
     *
     * @param groupId
     * @return
     */
    IPage<ProductGroupMap> selectByGroupId(Long groupId, Long pageNum, Long pageSize);

    /**
     * 根据商品分组id查询商品id列表，小程序端使用
     *
     * @param groupId
     * @return
     */
    List<Long> selectProductIdListByGroupId(Long groupId);

    /**
     * 根据商品id查询分组id列表
     *
     * @param productId
     * @return
     */
    List<Long> selectGroupIdListByProductId(Long productId);

    /**
     * 根据原始商品id查询分组id列表
     *
     * @param productId
     * @return
     */
    List<Long> selectGroupIdListByParentProductId(Long productId);

    /**
     * 根据原始商品id查询分组id列表,排除自身的分组
     *
     * @param productId
     * @return
     */
    List<Long> selectGroupIdListByParentProductIdWithoutSelf(Long productId);

    /**
     * 根据分组id删除
     *
     * @param groupId
     * @return
     */
    Integer updateDelByGroupId(Long groupId);

    /**
     * 根据商品id删除
     *
     * @param productId
     * @return
     */
    Integer updateDelByProductId(Long productId);

    /**
     * 根据分组id商品id删除
     *
     * @param groupId
     * @param productId
     * @return
     */
    Integer updateDelByGroupIdAndProductId(Long groupId, Long productId);

    /**
     * 根据原始商品id删除
     *
     * @param parentProductId
     * @return
     */
    Integer updateDelByParentProductId(Long parentProductId);

    /**
     * 根据原始商品id删除,排除自营的商品
     *
     * @param parentProductId
     * @return
     */
    Integer updateDelByParentProductIdWithoutSelf(Long parentProductId);

    /***
     * 根据分组汇总总数目
     * @param wrapper
     * @return
     */
    Integer countByGroupId(QueryWrapper<ProductGroupMap> wrapper);



}
