package mf.code.goods.repo.repostory.impl;

import mf.code.goods.repo.dao.ProductSpecsMapper;
import mf.code.goods.repo.dao.ProductSpecsNameMapper;
import mf.code.goods.repo.po.ProductSpecs;
import mf.code.goods.repo.po.ProductSpecsName;
import mf.code.goods.repo.repostory.SellerProductSpecsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * mf.code.goods.repo.repostory.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-19 17:20
 */
@Service
public class SellerProductSpecsRepositoryImpl implements SellerProductSpecsRepository {
    @Autowired
    private ProductSpecsMapper productSpecsMapper;
    @Autowired
    private ProductSpecsNameMapper productSpecsNameMapper;

    /**
     * 通过规格值id列表查询
     *
     * @param specsList
     * @return
     */
    @Override
    public List<ProductSpecs> selectSpecsListByIdForSeller(List<Long> specsList) {
        return productSpecsMapper.selectBatchIds(specsList);
    }

    /**
     * 通过规格名id列表查询
     *
     * @param specsNameList
     * @return
     */
    @Override
    public List<ProductSpecsName> selectSpecsNameListByIdForSeller(List<Long> specsNameList) {
        return productSpecsNameMapper.selectBatchIds(specsNameList);
    }
}
