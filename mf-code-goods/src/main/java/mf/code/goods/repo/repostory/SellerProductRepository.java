package mf.code.goods.repo.repostory;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.api.seller.dto.SellerProductListReqDTO;
import mf.code.goods.repo.po.Product;

import java.util.List;
import java.util.Map;

/**
 * @author qc
 */
public interface SellerProductRepository {
    /**
     * 根据商品父类目,查询其直接子类目的商品列表
     */
    List<Product> selectProductListByParentSpece(Map param);
    /**
     * 创建产品
     *
     * @param product product
     * @return SimpleResponse
     */
    Integer create(Product product);

    /**
     * 根据主键列表查询商品信息
     *
     * @param idList
     * @return
     */
    List<Product> selectBatchIds(List<Long> idList);

    /**
     * 根据主键查询商品
     *
     * @param id
     * @return
     */
    Product selectById(Long id);

    /**
     * 根据主键更新商品
     *
     * @param product
     * @return
     */
    Integer updateBySelective(Product product);

    /**
     * 根据条件分页查询
     *
     * @param reqDTO
     * @return
     */
    IPage<Product> pageByParams(SellerProductListReqDTO reqDTO);

    /**
     * 更新订单销量字段，通过id条件
     *
     * @param id
     * @param number
     * @return
     */
    Integer updateOrderSalesById(Long id, Integer number);

    /**
     * 统计数量
     *
     * @param conditionMap
     * @return
     */
    int countByConditionsForPlat(Map<String, Object> conditionMap);

    /**
     * 查询列表
     *
     * @param conditionMap
     * @return
     */
    List<Map> findByConditionsForPlat(Map<String, Object> conditionMap);

    /**
     * 拼接条件统计数量
     *
     * @param queryWrapper
     * @return
     */
    int selectCount(QueryWrapper<Product> queryWrapper);

    /**
     * 拼接mybatis-plus分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<Product> selectPage(IPage<Product> page, QueryWrapper<Product> queryWrapper);

    /**
     * 根据商品id，更新 佣金
     *
     * @param params
     * @return
     */
    int updateByProductId(Map<String, Object> params);

    /**
     * 根据条件查询商品
     *
     * @param beanToMap
     * @return
     */
    int countByConditions(Map<String, Object> beanToMap);

    /**
     * 根据条件查询
     *
     * @param beanToMap
     * @return
     */
    List<Map<String, Object>> findByConditions(Map<String, Object> beanToMap);

    /**
     * 全量更新
     *
     * @param product
     * @return
     */
    int update(Product product);

    /**
     * 根据父商品id下架商品
     *
     * @param productId
     */
    void downshilfProductByParentId(Long productId);

    /**
     * 下架商品
     *
     * @param conditionMap
     */
    int downshilf(Map<String, Object> conditionMap);
}
