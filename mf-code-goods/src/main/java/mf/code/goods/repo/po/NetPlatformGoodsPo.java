package mf.code.goods.repo.po;/**
 * create by qc on 2019/9/21 0021
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * 全网通商品
 *
 * @author gbf
 * 2019/9/21 0021、16:45
 */
@Data
public class NetPlatformGoodsPo {
    /**
     * count商品数
     */
    private Long productId;
    /**
     * count店铺数
     */
    private Long shopId;
    /**
     * count特价商品 需要单独查询
     */
    private BigDecimal price;
    /**
     * count爆款 销量>10
     */
    private Integer saleVol;

    /**
     * count尾货 库存<100
     */
    private Integer stock;

}
