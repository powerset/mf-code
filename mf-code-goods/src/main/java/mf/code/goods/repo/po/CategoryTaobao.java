package mf.code.goods.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * category_taobao
 * 品类-淘宝，品牌-淘宝TODO
 */
public class CategoryTaobao implements Serializable {
    /**
     * 品类主键ID
     */
    private Long id;

    /**
     * 拼音简写
     */
    private String spell;

    /**
     * 名称
     */
    private String name;

    /**
     * 类目ID
     */
    private String sid;

    /**
     * 上级类目ID
     */
    private String parentId;

    /**
     * 类型1:类目2品牌
     */
    private Integer type;

    /**
     * 类目层级
     */
    private Integer level;

    /**
     * 类目佣金比例
     */
    private BigDecimal distributionRatio;

    /**
     * 分组排序运营编辑，默认为0。正序排列
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * category_taobao
     */
    private static final long serialVersionUID = 1L;

    /**
     * 品类主键ID
     * @return id 品类主键ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 品类主键ID
     * @param id 品类主键ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 拼音简写
     * @return spell 拼音简写
     */
    public String getSpell() {
        return spell;
    }

    /**
     * 拼音简写
     * @param spell 拼音简写
     */
    public void setSpell(String spell) {
        this.spell = spell == null ? null : spell.trim();
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 类目ID
     * @return sid 类目ID
     */
    public String getSid() {
        return sid;
    }

    /**
     * 类目ID
     * @param sid 类目ID
     */
    public void setSid(String sid) {
        this.sid = sid == null ? null : sid.trim();
    }

    /**
     * 上级类目ID
     * @return parent_id 上级类目ID
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 上级类目ID
     * @param parentId 上级类目ID
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    /**
     * 类型1:类目2品牌
     * @return type 类型1:类目2品牌
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型1:类目2品牌
     * @param type 类型1:类目2品牌
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 类目层级
     * @return level 类目层级
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 类目层级
     * @param level 类目层级
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * 类目佣金比例
     * @return distribution_ratio 类目佣金比例
     */
    public BigDecimal getDistributionRatio() {
        return distributionRatio;
    }

    /**
     * 类目佣金比例
     * @param distributionRatio 类目佣金比例
     */
    public void setDistributionRatio(BigDecimal distributionRatio) {
        this.distributionRatio = distributionRatio;
    }

    /**
     * 分组排序运营编辑，默认为0。正序排列
     * @return sort 分组排序运营编辑，默认为0。正序排列
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 分组排序运营编辑，默认为0。正序排列
     * @param sort 分组排序运营编辑，默认为0。正序排列
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", spell=").append(spell);
        sb.append(", name=").append(name);
        sb.append(", sid=").append(sid);
        sb.append(", parentId=").append(parentId);
        sb.append(", type=").append(type);
        sb.append(", level=").append(level);
        sb.append(", distributionRatio=").append(distributionRatio);
        sb.append(", sort=").append(sort);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}