package mf.code.goods.repo.repostory;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.goods.repo.po.ProductSku;

import java.util.List;

/**
 * create by qc on 2019/4/3 0003
 */
public interface SellerSkuRepository extends IService<ProductSku> {

    /**
     * 创建新的sku
     *
     * @param productSku
     * @return
     */
    int insertSelective(ProductSku productSku);

    /**
     * 根据skuId列表查询sku列表
     *
     * @param skuIdList
     * @return
     */
    List<ProductSku> selectBatchIds(List<Long> skuIdList);

    /**
     * 根据商品id，查询sku列表
     *
     * @param productId
     * @return
     */
    List<ProductSku> selectByProductId(Long productId);

    /**
     * 根据商品id列表，查询sku列表
     *
     * @param productId
     * @return
     */
    List<ProductSku> selectByProductIdList(List<Long> productId);

    /**
     * 根据skuId列表，查询sku
     *
     * @param skuId
     * @return
     */
    ProductSku selectBySkuId(Long skuId);

    /**
     * 根据skuId列表，查询sku，秒杀专用哟
     *
     * @param skuId 原始skuId
     * @return 应该是一条当前时间生成的数据
     */
    List<ProductSku> selectByOldSkuIdAndEffectTime(Long skuId, String effectTime);
}
