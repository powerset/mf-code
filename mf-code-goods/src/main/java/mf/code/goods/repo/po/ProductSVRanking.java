package mf.code.goods.repo.po;/**
 * create by qc on 2019/8/19 0019
 */

import lombok.Data;

/**
 * @author gbf
 * 2019/8/19 0019、14:32
 */
@Data
public class ProductSVRanking {
    /**
     * 商品id
     */
    private String productId;
    /**
     * 商品在zset中的分数
     */
    private Double score;

    public ProductSVRanking(String productId, Double score) {
        this.productId = productId;
        this.score = score;
    }
}
