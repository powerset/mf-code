package mf.code.goods.common.filter;

import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * mf.code.common.filter
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-29 下午7:20
 */
public class AppletTokenFilter extends HttpServlet implements Filter {
    private static final long serialVersionUID = 1L;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        Object contentType = httpServletRequest.getContentType();
        if(contentType != null && !StringUtils.containsIgnoreCase(String.valueOf(contentType),"application/json")){
            chain.doFilter(request, response);
            return;
        }
        BodyReaderRequestWrapper requestWrapper = new BodyReaderRequestWrapper((HttpServletRequest)request);
        chain.doFilter(requestWrapper, response);
    }
}

