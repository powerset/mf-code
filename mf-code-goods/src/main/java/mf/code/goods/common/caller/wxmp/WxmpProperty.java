package mf.code.goods.common.caller.wxmp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="tmplatemessage")
@Data
@Component
public class WxmpProperty {
    //抽奖结果通知-用户自发活动(温馨提示、活动名称、注意事项)
    private String userAwardResultMsgTmpId;
    //抽奖结果通知-计划类(温馨提示、活动名称、注意事项、发起商户)
    private String planAwardResultMsgTmpId;
    //审核结果通知(温馨提示、审核内容、后续步骤)
    private String auditingResultMsgTmpId;
    //订单进度提醒(温馨提示、订单详情、备注)
    private String orderProessMsgTmpId;
    //订单取消通知(温馨提示、取消原因、备注)
    private String ordercancelMsgTmpId;
    //订单状态变动通知(订单状态、订单内容、备注)
    private String orderStatusChangeMsgTmpId;
    //拼团失败通知(温馨提示、活动名称、失败原因)
    private String assembleFailMsgTmpId;
    //拼团成功通知(温馨提示、活动名称、失败原因)
    private String assembleSuccessMsgTmpId;

    private String formIdRedisKey;

    private String teacherFormIdRedisKey;


    /*************集客师模板*****************/
    //收益到账通知	温馨提示、收益来源、金额
    private String teacherIncomeReachMsgTmpId;
    //审核结果通知	温馨提示、审核内容、后续步骤
    private String teacherAuditingResultMsgTmpId;


    public String getRedisKey(Long uid){
        return this.formIdRedisKey.replace("A",uid.toString());
    }

    public String getTeacherRedisKey(Long tid){
        return this.teacherFormIdRedisKey.replace("A",tid.toString());
    }
}
