package mf.code.goods.common.apollo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * mf.code.common.apollo
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月15日 19:35
 */
@Data
public class DouyinProperty {

    private Douyin douyin;

    @Data
    public static class Douyin {
        @JsonProperty("gids")
        private List<Gids> gids;
    }

    @Data
    public static class Gids {
        @JsonProperty("gid")
        private String gid;
    }
}
