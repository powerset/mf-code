package mf.code.goods.common.constant;

/**
 * mf.code.goods.common.constant
 * Description:
 *
 * @author gel
 * @date 2019-06-24 20:42
 */
public enum ProductGroupTypeEnum {
    /**
     * 1：商品分组；
     */
    PRODUCT(1, "商品分组"),
    /**
     * 2：类目分组
     */
    CATEGORY(2, "类目分组"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ProductGroupTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
