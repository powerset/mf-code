package mf.code.goods.common.constant;

/**
 * mf.code.goods.common.constant
 * Description: 来源标识（0：无，1：自营，2:分销。判断条件是id是否等于parent_id,等于是自营，不等于是分销）
 *
 * @author gel
 * @date 2019-06-26 14:27
 */
public enum ProductFromFlagEnum {

    /**
     * 0：无；
     */
    NONE(0, "无"),
    /**
     * 1：自营
     */
    SELF(1, "自营"),
    /**
     * 2：分销
     */
    PLAT(2, "分销"),
    /**
     * 3：平台商城强制设置
     */
    MALL(3, "平台商城"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ProductFromFlagEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
