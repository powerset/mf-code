package mf.code.goods.common.constant;

/**
 * create by qc on 2019/8/19 0019
 */
public enum SpeceTypeEnum {
    DEFALT(1000000000, "默认"),
    FOOD(1000000001, "百货食品"),
    CLOTHES(1000000002, "服装鞋包"),
    ACCESSORIES(1000000003, "美妆饰品"),
    PHONE(1000000004, "手机数码"),
    CHILDREN(1000000005, "母婴用品"),
    LIFE(1000000006, "生活服务"),
    HOUSEHOLD(1000000007, "家用电器"),
    BUILDING(1000000008, "家居建材"),
    SPORT(1000000009, "运动户外"),
    PLAY(1000000010, "文化玩乐"),
    OTHER(1000000011, "其他商品"),
    MOTOR(1000000012, "汽配摩托"),
    GAMES(1000000013, "游戏话费"),
    ;


    private Integer code;
    private String desc;

    SpeceTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
