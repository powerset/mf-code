package mf.code.goods.common.rocketmq.consumer;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.goods.dto.ProductRankSalesVolumeDTO;
import mf.code.goods.repo.repostory.SellerProductRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

/**
 * mf.code.goods.common.rocketmq.consumer
 * Description:
 *
 * @author gel
 * @date 2019-06-13 12:04
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "product_rank", consumerGroup = "product_rank-consumer")
public class ProductRankTopicConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {

    @Autowired
    private SellerProductRepository sellerProductRepostory;

    @Override
    public void onMessage(MessageExt message) {
        String bizVale = new String(message.getBody(), Charset.forName("UTF-8"));
        String msgId = message.getMsgId();
        String tags = message.getTags();
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.PRODUCT_RANK_SALES_VOLUME.getTag(), tags)) {
            addProductInfoIntoCatch(msgId, bizVale);
        }
    }


    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }


    private void addProductInfoIntoCatch(String msgId, String bizVale) {
        ProductRankSalesVolumeDTO volumeDTO = JSON.parseObject(bizVale, ProductRankSalesVolumeDTO.class);
        Long productId = volumeDTO.getGoodsId();
        Integer num = volumeDTO.getNumber();
        sellerProductRepostory.updateOrderSalesById(productId, num);
    }
}
