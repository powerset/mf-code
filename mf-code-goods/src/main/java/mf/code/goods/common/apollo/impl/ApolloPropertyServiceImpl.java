package mf.code.goods.common.apollo.impl;

import com.alibaba.fastjson.JSONObject;
import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.core.enums.ConfigFileFormat;
import com.ctrip.framework.apollo.internals.YmlConfigFile;
import mf.code.goods.common.apollo.ApolloPropertyService;
import mf.code.goods.common.apollo.DouyinProperty;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

/**
 * mf.code.goods.common.apollo.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月26日 19:12
 */
@Service
public class ApolloPropertyServiceImpl implements ApolloPropertyService {
    @Override
    public DouyinProperty getApolloDouyinProperty() {
        YmlConfigFile configFile = (YmlConfigFile) ConfigService.getConfigFile("application-apollo", ConfigFileFormat.YML);
        if (configFile == null) {
            return new DouyinProperty();
        }
        Yaml yaml = new Yaml();
        Object load = yaml.load(configFile.getContent());
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(load));
        DouyinProperty douyinProperty = JSONObject.toJavaObject(jsonObject, DouyinProperty.class);
        return douyinProperty;
    }
}
