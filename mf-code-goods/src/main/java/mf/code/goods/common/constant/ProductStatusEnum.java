package mf.code.goods.common.constant;

/**
 * mf.code.goods.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 15:18
 */
public enum  ProductStatusEnum {
	/**
	 * 下架
	 */
	OFFLINE(-1, "下架"),
	/**
	 * 下架
	 */
	NOSALE(0, "未上架"),
	/**
	 * 上架
	 */
	ONLINE(1, "上架"),
	/**
	 * 预售
	 */
	PRESALE(2, "预售"),
	;

	/**
	 * code
	 */
	private int code;
	/**
	 * 描述
	 */
	private String desc;

	ProductStatusEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
