package mf.code.goods.common.constant;

/**
 * create by qc on 2019/8/15 0015
 */
public enum ProductSortEnum {

    NEW(1, "上新排序"),
    SALE(2, "销售排序"),
    COMMISSION(3, "高佣排序");

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ProductSortEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

}
