package mf.code.goods.common.redis;

/**
 * mf.code.user.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 00:13
 */
public class RedisKeyConstant {

    /**
     * 数据字典键
     * key--> common:dict:<type>:<key>
     * value--> 1
     */
    public static final String COMMON_DICT = "common:dict:";

    /**
     * 含有分销属性商品销量排行榜
     */
    public static final String PRODUCT_SALESVOLUME_RANKING_LIST = "jkmf:product:salesvolume:ranking:list"; // 商品id - score 销量

    /**
     * 零级新类目缓存
     * key--> jkmf:product:category:zero
     * value--> 列表json字符串
     */
    public static final String PRODUCT_CATEGORY_ZERO = "jkmf:product:category:zero";

    /**
     * 零级新类目下关联的类目
     * key--> jkmf:product:category:one:<sid>
     * value--> 列表json字符串
     */
    public static final String PRODUCT_CATEGORY_ONEBYFIRSTLEVEL = "jkmf:product:category:one:";

    /**
     * 店铺包含零级类目
     * key--> jkmf:product:category:zero:<shop>
     * value--> 列表id
     */
    public static final String PRODUCT_CATEGORY_ZERO_SHOP = "jkmf:product:category:zero:";

    /**
     * 店铺包含零级类目redis锁
     * key--> jkmf:product:category:zero:lock:<shop>
     * value--> 1
     */
    public static final String PRODUCT_CATEGORY_ZERO_LOCK_SHOP = "jkmf:product:category:zero:lock:";

    /**
     * 店铺包含零级类目redis锁
     * key--> jkmf:product:category:zero:lock:<shop>
     * value--> 1
     */
    public static final String PRODUCT_SKU_FLASH_SALE_CREATE_LOCK = "jkmf:product:sku:flashsale:create:lock:";

    /**
     * 抖带带tab列表
     */
    public static final String DDD_BANNER = "common:dict:banner:list:dy";

}
