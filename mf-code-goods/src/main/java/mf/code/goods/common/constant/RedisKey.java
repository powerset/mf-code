package mf.code.goods.common.constant;/**
 * create by qc on 2019/4/17 0017
 */

/**
 * @author gbf
 * 2019/4/17 0017、15:16
 */
public class RedisKey {
    private static String REDIS_KEY_ROOT = "mf:code:";
    /**
     * 商品销售排行榜开始日期
     */
    public static final String REDIS_ZSET_DAY_BEGIN = "20190808";
    /**
     * 平台商品排行榜
     */
    public static final String PLAT_GOODS_RANK = "statis:rt:goods:pay:rank:plat";

    public static class Product {
        /**
         * 商品业务
         */
        private static String PRODUCT = REDIS_KEY_ROOT + "product:";

        /**
         * sku业务
         */
        public static class Sku {
            private static String SKU = PRODUCT + "sku:";
            /**
             * 修改库存业务
             */
            public static final String UPDATE_STOCK_BIZ = SKU + "updateStock:skuId:";//<SKU_ID>;
            /**
             * 修改库存业务
             */
            public static final String SKU_ID = SKU + "updateStock:skuId:";//<SKU_ID>;
        }

        /**
         * 分销
         */
        public static class Distribution {
            /**
             * 商品佣金
             */
            public static final String MIN_COMMISSION = PRODUCT + "commission:";
        }
    }
}
