package mf.code.goods.common.apollo;

/**
 * mf.code.common.apollo
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月15日 20:23
 */
public interface ApolloPropertyService {

    DouyinProperty getApolloDouyinProperty();
}
