package mf.code.goods.common.tlab;/**
 * create by qc on 2019/8/27 0027
 */

import mf.code.shop.constants.AppTypeEnum;

/**
 * 本地线程内获取应用类型
 *
 * @author gbf
 * 2019/8/27 0027、11:44
 */

public class AppTypeRequestContext {
    /**
     * ThreadLocal
     */
    private static final ThreadLocal<AppTypeEnum> APP_TYPE_ENUM_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * @param appType AppTypeEnum
     */
    public static void setAppType(AppTypeEnum appType) {
        APP_TYPE_ENUM_THREAD_LOCAL.set(appType);
    }

    /**
     * @return AppTypeEnum
     */
    public static AppTypeEnum getAppType() {
        return APP_TYPE_ENUM_THREAD_LOCAL.get();
    }
}
