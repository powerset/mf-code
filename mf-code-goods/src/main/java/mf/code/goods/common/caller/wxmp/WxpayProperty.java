package mf.code.goods.common.caller.wxmp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "wxpay")
@Data
@Component
public class WxpayProperty {
    private String payRedPackageUrl; //红包Url
    private String payTransfersUrl; //企业付款Url

    private String payUnifiedorderUrl;//统一下单url
    private String payUnifiedNotifyUrl;//统一下单回调地址
    private String payRefundNotifyUrl;//退款回调地址

    private String sellerPayUnifiedNotifyUrl; //商户统一下单的回调地址
    private String sellerPayRefundNotifyUrl; //商户退款下单的回调地址

    private String payQueryOrderUrl;//查询订单地址
    private String payRefundOrderUrl;//退款地址
    private String payRefundQueryOrderUrl;//查询退款地址

    private String mchAppId; //商户appid
    private String mchId;  //商户号
    private String mchSecretKey; //商户平台设置的密钥key
    private String pubAppId;  // 公众号appid
    private String pubAppSecret; //公众号appSecret

    //企业付款的一些字段
    private String desc; //描述
    //红包的一些字段
    private String wishing;  // 祝福语
    private String mchSendName; //商户名称
    private String actName; //活动名称
    private String remark;  // 备注
    private String orderName; //订单名称
    private String certPath; //商户证书

    /**
     * 获取用户信息url
     */
    private String userInfoUrl;
    private String accessTokenUrl;

    /***
     * 集客魔方的小程序
     */
    private String mfName;
    private String mfAppId;
    private String mfAppSecret;

    /***
     * 集客师的小程序
     */
    private String teacherName;
    private String teacherAppId;
    private String teacherSecret;
}
