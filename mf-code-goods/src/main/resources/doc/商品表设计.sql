--商户（目前线上有部分数据没有记录，TODO 订购版本需要从shop表同步）

-- 第三方店铺（挂钩到商户下）(TODO 线上的淘宝店铺待添加处理)
CREATE TABLE `merchant_third_shop` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '第三方店铺id',
  `merchant_id` bigint(20) unsigned NOT NULL COMMENT '商户id',
  `src_type` tinyint(4) NOT NULL COMMENT '店铺来源类型 1淘宝 2京东',
  `nick` varchar(128) NOT NULL COMMENT '卖家昵称',
  `shop_name` varchar(32) DEFAULT NULL COMMENT '商铺名',
  `shop_url` varchar(256) DEFAULT NULL COMMENT '商铺url',
  `third_service` int(11) DEFAULT 1 COMMENT '第三方数据服务(1:喜销宝)',
  `third_deadline` datetime DEFAULT NULL COMMENT '第三方数据服务（喜销宝）截止时间',
  `third_pic_path` varchar(1024) DEFAULT NULL COMMENT '第三方数据服务（喜销宝）来源原始店标例如：淘宝用"http://logo.taobao.com/shop-logo"来拼接成绝对路径',
  `pic_path` varchar(1024) DEFAULT NULL COMMENT '小时光OSS店标地址。返回相对路径',
  `third_req` varchar(1024) DEFAULT NULL COMMENT '第三方数据服务（喜销宝）服务接口请求数据',
  `third_res` text COMMENT '第三方数据服务（喜销宝）服务接口响应数据',
  `del` int(11) DEFAULT NULL COMMENT '删除标识',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `INDEX_MERCHANT_ID` (`merchant_id`),
  KEY `INDEX_NICK` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='第三方店铺（挂钩到商户下）';

-- 店铺添加字段
alter table `merchant_shop` add `custom_shop_name` varchar(128) default '' COMMENT '自定义商铺名' AFTER `shop_name`;
alter table `merchant_shop` add `custom_pic_path` varchar(255) default '' COMMENT '自定义店标地址' AFTER `pic_path`;
alter table `merchant_shop` add `username` varchar(128) default '' COMMENT '店长姓名' AFTER `phone`;
alter table `merchant_shop` add `address_code` varchar(128) default '' COMMENT '省市区编码(下划线分割)' AFTER `pic_path`;
alter table `merchant_shop` add `address_text` varchar(128) default '' COMMENT '省市区(下划线分割)' AFTER `pic_path`;
alter table `merchant_shop` add `address_detail` varchar(128) default '' COMMENT '地址详情' AFTER `pic_path`;
alter table `merchant_shop` add `distStatus` tinyint(4) DEFAULT 0 COMMENT '分销资格审核(-1:不通过，0:待审核，1：通过)' AFTER `category_id`;
alter table `merchant_shop` add `openStatus` tinyint(4) DEFAULT 0 COMMENT '开店审核(-1:不通过，0:待审核，1：通过)' AFTER `category_id`;
alter table `merchant_shop` add `distReason` varchar(128) DEFAULT '' COMMENT '分销资格审核原因' AFTER `distStatus`;
alter table `merchant_shop` add `openReason` varchar(128) DEFAULT '' COMMENT '开店审核审核原因' AFTER `openStatus`;


-- product
CREATE TABLE `product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级商品id（从哪个商品派生的）',
  `merchant_id` bigint(20) NOT NULL COMMENT '商户id（所属商户）',
  `shop_id` bigint(20) NOT NULL COMMENT '店铺id（所属店铺）',
  `product_title` varchar(255) NOT NULL COMMENT '商品标题',
  `product_specs` bigint(20) NOT NULL COMMENT '商品类目',
  `product_detail` text COMMENT '商品描述',
  `main_pics` text COMMENT '商品图（主图级轮播图） 存json',
  `detail_pics` text COMMENT '详情图（n+图片）',
  `product_price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `status` int(11) DEFAULT NULL COMMENT '状态（-1:下架,1:上架,2:预售,0:未上架）',
  `check_status` int(11) DEFAULT NULL COMMENT '审核状态',
  `check_reason` varchar(2000) DEFAULT NULL COMMENT '审核原因',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序',
  `putaway_time` datetime DEFAULT NULL COMMENT '上架时间',
  `self_support_ratio` double DEFAULT NULL COMMENT '自营分销佣金比例',
  `plat_support_ratio` double DEFAULT NULL COMMENT '平台分销佣金比例',
  `ctime` datetime DEFAULT NULL COMMENT '创建时间',
  `utime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- product_sku
CREATE TABLE `product_sku` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL COMMENT '产品id',
  `merchant_id` bigint(20) NOT NULL COMMENT '商户id',
  `shop_id` bigint(20) NOT NULL COMMENT '店铺id',
  `sku_name` varchar(255) DEFAULT NULL COMMENT 'sku名称',
  `sku_pics` varchar(255) DEFAULT NULL COMMENT 'sku图片',
  `sku_selling_price` decimal(10,0) NOT NULL COMMENT '售价',
  `sku_cose_price` decimal(10,0) NOT NULL COMMENT '成本价格',
  `stock_def` int(11) DEFAULT NULL COMMENT '定义库存',
  `stock` int(11) NOT NULL COMMENT '库存',
  `on_sale` int(11) NOT NULL COMMENT '是否在售 1:在售 2:快照',
  `sort` varchar(255) DEFAULT NULL COMMENT '排序值',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- product_specs
CREATE TABLE `product_specs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) NOT NULL COMMENT '类目id',
  `specs_value` varchar(255) NOT NULL COMMENT '类目值',
  `merchant_id` bigint(20) DEFAULT NULL COMMENT '商户id',
  `ctime` datetime DEFAULT NULL COMMENT '创建时间',
  `utime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
