package mf.code.distribution.junit;

import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.common.rocketmq.consumer.impl.BizTypeTopicConsumerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.distribution.junit
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 12:45
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class FunctionTest {
	@Autowired
	private BizTypeTopicConsumerService bizTypeTopicConsumerService;


	@Test
	public void shopPolicyTest1() {
		bizTypeTopicConsumerService.saveBizLog("", "{\"current\":1562843946620,\"event\":\"3\",\"eventId\":1826,\"extra\":{\"productId\":198,\"transactionRate\":2,\"vipShopRate\":50,\"parentId\":198,\"goodsCategoryRate\":10},\"mid\":150,\"sid\":110,\"uid\":325}");
	}


}
