package mf.code.distribution.junit;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.repo.mongo.po.FanAction;
import mf.code.distribution.repo.mongo.repository.FanActionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * mf.code.distribution.junit
 * Description:
 *
 * @author gel
 * @date 2019-07-09 18:57
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MongodbTest {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private FanActionRepository fanActionRepository;
    @Test
    public void querySomething(){
        //根据shopId查询过去一周的访问信息
        Criteria criteria = Criteria
                .where("sid").is(8L)
                .and("event").is("5");

        Aggregation agg = Aggregation.newAggregation(FanAction.class,
                // 查询条件
                Aggregation.match(criteria),
                Aggregation.sort(new Sort(Sort.Direction.DESC, "current")),
                // 组装返回哪些字段，注意这里project的字段必须包含查询条件，分组排序字段
                Aggregation.project("sid", "mid", "event", "uid", "current", "eventType"),
                // 分组，求总数
                Aggregation.group("uid")
                        .first("uid").as("uid")
                        .first("mid").as("mid")
                        .first("sid").as("sid")
                        .first("current").as("current")
                        ,
                Aggregation.skip(0L),
                Aggregation.limit(5)
        );
        AggregationResults<FanAction> fanActionList = mongoTemplate.aggregate(agg, "fan_action", FanAction.class);
        System.out.println(fanActionList.getMappedResults());
    }

    @Test
    public void querySomethinga(){
        //根据shopId查询过去一周的访问信息
        fanActionRepository.sumTaxForTeamManagerSevenDay(110L, Arrays.asList(325L), Arrays.asList("3"));
    }
    @Test
    public void querySomethingb(){
        //根据shopId查询过去一周的访问信息
        FanAction fanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(110L, 325L, "3", 1827L);
        System.out.println(JSON.toJSONString(fanAction));
    }
}
