#!/usr/bin/env bash
echo "------------------- kill java progress start --------------"
port="8003"
aliasname="mf-code-distribution"

echo "当前服务端口"${aliasname} ${port}
echo "试验性杀进程,如有报错请勿惊慌"

ps aux|grep ${port}|grep -v grep|awk '{print $2}'|xargs kill -9
echo "------------------- kill java progress end --------------"

echo "******************** deploy start ********************"

nohup java -Dserver.port=${port} -jar /home/code/git-data/mf-code-parent-${port}/${aliasname}/target/${aliasname}-1.0-SNAPSHOT.jar --spring.profiles.active=prod --eureka.instance.ip-address=129.211.139.95> /home/code/git-data/mf-code-parent-${port}/${aliasname}/nohup.out 2>&1 &
# nohup java -Dserver.port=${port} -jar /home/code/git-data/mf-code-parent-${port}/${aliasname}/target/${aliasname}-1.0-SNAPSHOT.jar --spring.profiles.active=prod --eureka.instance.ip-address=129.211.142.25> /home/code/git-data/mf-code-parent-${port}/${aliasname}/nohup.out 2>&1 &

echo "******************** deploy end ********************"
