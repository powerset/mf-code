package mf.code.distribution.repo.mongo.repository.operator.impl;

import mf.code.common.utils.DateUtil;
import mf.code.distribution.dto.FanActionCountDTO;
import mf.code.distribution.repo.mongo.po.FanAction;
import mf.code.distribution.repo.mongo.repository.operator.FanActionOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.distribution.repo.mongo.repository.operator.impl
 * Description:
 *
 * @author gel
 * @date 2019-07-05 11:25
 */
public class FanActionOperatorImpl implements FanActionOperator {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 表名
     */
    private static final String COLLECTION_NAME = "fan_action";

    /**
     * 通过埋点事件类型、店铺、团队列表统计人数
     *
     * @param shopId
     * @param teamUidList
     * @param eventList
     * @return
     */
    @Override
    public long countUidForTeamManagerSevenDay(Long shopId, List<Long> teamUidList, List<String> eventList) {
        Criteria criteria = new Criteria();
        criteria.and("sid").is(shopId)
                .and("event").in(eventList)
                .and("uid").in(teamUidList)
                .and("current").gte(getTimeMillisSevenDaysBefore()).lt(DateUtil.getCurrentMillis());
        TypedAggregation<FanAction> agg = Aggregation.newAggregation(FanAction.class,
                // 组装返回哪些字段，注意这里project的字段必须包含查询条件，分组排序字段
                Aggregation.project("sid", "event", "uid", "current"),
                // 查询条件
                Aggregation.match(criteria),
                // 分组，求总数
                Aggregation.group("uid")
        );
        // 这里采用string是因为这里分组只是为了能查询条数。可使用其他包含返回字段的数据类型
        AggregationResults<String> aggregate = mongoTemplate.aggregate(agg, COLLECTION_NAME, String.class);
        if (CollectionUtils.isEmpty(aggregate.getMappedResults())) {
            return 0L;
        }
        return aggregate.getMappedResults().size();
    }

    /**
     * 通过埋点事件类型、店铺、团队列表统计埋点事件数
     *
     * @param shopId
     * @param teamUidList
     * @param eventList
     * @return
     */
    @Override
    public long countEventIdForTeamManagerSevenDay(Long shopId, List<Long> teamUidList, List<String> eventList) {
        Criteria criteria = new Criteria();
        criteria.and("sid").is(shopId)
                .and("event").in(eventList)
                .and("uid").in(teamUidList)
                .and("current").gte(getTimeMillisSevenDaysBefore()).lt(DateUtil.getCurrentMillis());

        return mongoTemplate.count(new Query(criteria), COLLECTION_NAME);
    }

    /**
     * 查询某个粉丝全期的粉丝明细
     *
     * @param shopId
     * @param userId
     * @param typeList
     * @param offset
     * @param limit
     * @return
     */
    @Override
    public List<FanAction> pageListForTeamManagerAll(Long shopId, Long userId, List<String> typeList, Long offset, Long limit) {
        Criteria criteria = new Criteria();
        criteria.and("sid").is(shopId)
                .and("uid").is(userId)
                .and("event").in(typeList);

        Query query = new Query(criteria);
        query.with(new Sort(Sort.Direction.DESC, "current"));
        query.skip(offset).limit(limit.intValue());

        List<FanAction> fanActionsList = mongoTemplate.find(query, FanAction.class, COLLECTION_NAME);

        if (CollectionUtils.isEmpty(fanActionsList)) {
            return null;
        }
        return fanActionsList;
    }

    /**
     * 获取团队管理统一分页查询
     *
     * @param shopId
     * @param userIdList
     * @param typeList
     * @param offset
     * @param limit
     * @return
     */
    @Override
    public List<FanAction> pageListForTeamManagerSevenDay(Long shopId, List<Long> userIdList, List<String> typeList, Long offset, int limit) {
        if (offset <= 0) {
            offset = 0L;
        }
        if (limit <= 0) {
            limit = 4;
        }

        //根据shopId查询过去一周的访问信息
        Criteria criteria = Criteria
                .where("sid").is(shopId)
                .and("event").in(typeList)
                .and("uid").in(userIdList)
                .and("current").gte(getTimeMillisSevenDaysBefore()).lte(DateUtil.getCurrentMillis());

        Query query = new Query(criteria);
        query.with(new Sort(Sort.Direction.DESC, "current"));
        query.skip(offset).limit(limit);

        List<FanAction> fanActionsList = mongoTemplate.find(query, FanAction.class, COLLECTION_NAME);

        if (CollectionUtils.isEmpty(fanActionsList)) {
            return null;
        }
        return fanActionsList;
    }

    /**
     * 汇总团队成员七日缴税额度(订单，或者任务)
     *
     * @param shopId
     * @param teamUidList
     * @param eventList
     * @return
     */
    @Override
    public BigDecimal sumTaxForTeamManagerSevenDay(Long shopId, List<Long> teamUidList, List<String> eventList) {
        Criteria criteria = new Criteria();
        criteria.and("sid").is(shopId)
                .and("event").in(eventList)
                .and("uid").in(teamUidList)
                .and("current").gte(getTimeMillisSevenDaysBefore()).lt(DateUtil.getCurrentMillis());
        Aggregation agg = Aggregation.newAggregation(FanAction.class,
                // 查询条件
                Aggregation.match(criteria),
                // 组装返回哪些字段，注意这里project的字段必须包含查询条件，分组排序字段
                Aggregation.project("sid", "event", "uid", "current", "eventType", "amount", "tax", "extra", "ctime"),
                // 分组，求总数
                Aggregation.group("sid").sum("tax").as("sum")
        );

        AggregationResults<FanActionCountDTO> aggregate = mongoTemplate.aggregate(agg, COLLECTION_NAME, FanActionCountDTO.class);
        if (CollectionUtils.isEmpty(aggregate.getMappedResults())) {
            return BigDecimal.ZERO;
        }
        FanActionCountDTO fanActionCountDTO = aggregate.getMappedResults().get(0);
        if (fanActionCountDTO.getSum() == null) {
            return BigDecimal.ZERO;
        }
        return fanActionCountDTO.getSum();
    }

    /**
     * 通过店铺id，用户id, 埋点事件，埋点事件id查询一条埋点记录
     *
     * @param shopId
     * @param uid
     * @param event
     * @param eventId
     * @return
     */
    @Override
    public FanAction queryBySidAndUidAndEventAndEventid(Long shopId, Long uid, String event, Long eventId) {
        Criteria criteria = new Criteria();
        criteria.and("sid").is(shopId)
                .and("uid").is(uid)
                .and("event").is(event)
                .and("eventid").is(eventId);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, FanAction.class, COLLECTION_NAME);
    }

    @Override
    public long countForFansActiveAll(Long shopId, Long userId, List<String> typeList) {
        //根据shopId查询过去一周的访问信息
        Criteria criteria = Criteria
                .where("sid").is(shopId)
                .and("event").in(typeList)
                .and("uid").is(userId);
        return mongoTemplate.count(new Query(criteria), FanAction.class, COLLECTION_NAME);
    }

    @Override
    public List<FanAction> pageListUidListGroupByUid(Long shopId, List<Long> userIdList, List<String> typeList, Long offset, Long limit) {

        //根据shopId查询过去一周的访问信息
        Criteria criteria = Criteria
                .where("sid").is(shopId)
                .and("event").in(typeList)
                .and("uid").in(userIdList)
                .and("current").gte(getTimeMillisSevenDaysBefore()).lte(DateUtil.getCurrentMillis());

        if (offset <= 0) {
            offset = 0L;
        }
        if (limit <= 0) {
            limit = 4L;
        }
        Aggregation agg = Aggregation.newAggregation(FanAction.class,
                // 查询条件
                Aggregation.match(criteria),
                Aggregation.sort(new Sort(Sort.Direction.DESC, "current")),
                // 组装返回哪些字段，注意这里project的字段必须包含查询条件，分组排序字段
                Aggregation.project("uid", "sid", "mid", "event", "current", "eventType"),
                // 分组，这里可以用first或者last来补充某个字段，详见 @see https://docs.mongodb.com/manual/aggregation/
                Aggregation.group("uid")
                        .first("uid").as("uid")
                        .first("sid").as("sid")
                        .first("mid").as("mid")
                        .first("current").as("current"),
                Aggregation.skip(offset),
                Aggregation.limit(limit)
        );
        AggregationResults<FanAction> aggregate = mongoTemplate.aggregate(agg, COLLECTION_NAME, FanAction.class);
        if (CollectionUtils.isEmpty(aggregate.getMappedResults())) {
            return null;
        }
        return aggregate.getMappedResults();
    }

    /**
     * 获取六天前的零点，即满足最近七天
     *
     * @return 六天前的凌晨毫秒数
     */
    private Long getTimeMillisSevenDaysBefore() {
        Date sevenDayBefore = DateUtil.begin(DateUtil.addDay(new Date(), -6));
        return sevenDayBefore.getTime();
    }
}
