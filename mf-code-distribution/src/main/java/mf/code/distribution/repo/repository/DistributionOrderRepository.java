package mf.code.distribution.repo.repository;

import mf.code.distribution.domain.aggregateroot.DistributionOrder;

/**
 * mf.code.distribution.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 19:24
 */
public interface DistributionOrderRepository {

	/**
	 * 通过 userId, shopId, orderId 获取分销订单
	 *
	 * @param userId
	 * @param shopId
	 * @param orderId
	 * @return
	 */
	DistributionOrder findByOrderId(Long userId, Long shopId, Long orderId);
}
