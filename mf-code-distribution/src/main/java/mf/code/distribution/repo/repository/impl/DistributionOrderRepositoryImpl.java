package mf.code.distribution.repo.repository.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.domain.aggregateroot.DistributionOrder;
import mf.code.distribution.domain.valueobject.OrderDistributionProperty;
import mf.code.distribution.domain.valueobject.ProductDistributionPolicy;
import mf.code.distribution.repo.repository.DistributionOrderRepository;
import mf.code.order.dto.AppletOrderResp;
import mf.code.order.dto.OrderBizResp;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderBizBizTypeEnum;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * mf.code.distribution.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 19:24
 */
@Slf4j
@Service
public class DistributionOrderRepositoryImpl implements DistributionOrderRepository {
	@Autowired
	private OrderAppService orderAppService;
	@Autowired
	private UserAppService userAppService;

	/**
	 * 通过 userId, shopId, orderId 获取分销订单
	 *
	 * @param userId
	 * @param shopId
	 * @param orderId
	 * @return
	 */
	@Override
	public DistributionOrder findByOrderId(Long userId, Long shopId, Long orderId) {
		// 获取订单详情
		String appletOrderStr = orderAppService.queryAppletOrder(orderId);
		if (StringUtils.isBlank(appletOrderStr)) {
			log.error("distribution服务器告警，查无此单，orderId = {}", orderId);
			return null;
		}
		AppletOrderResp appletOrderResp = JSON.parseObject(appletOrderStr, AppletOrderResp.class);
		// 获取用户信息
		UserResp userResp = userAppService.queryUser(userId);
		if (userResp == null) {
			log.error("distribution服务获取用户信息无效, userId = {}", userId);
			return null;
		}
		// 装配 distributionOrder
		return assemblyDistributionOrder(userResp, shopId, appletOrderResp);
	}

	private DistributionOrder assemblyDistributionOrder(UserResp userInfo, Long shopId, AppletOrderResp appletOrderResp) {
		Long orderId = appletOrderResp.getId();
		List<OrderBizResp> orderBizList = appletOrderResp.getOrderBizList();
		OrderResp orderInfo = appletOrderResp.getOrderInfo();

		OrderBizResp orderBiz = null;
		if (!CollectionUtils.isEmpty(orderBizList)) {
			for (OrderBizResp orderBizResp : orderBizList) {
				if (OrderBizBizTypeEnum.DISTRIBUTION.getCode() == orderBizResp.getBizType()) {
					orderBiz = orderBizResp;
					break;
				}
			}
		}

		DistributionOrder distributionOrder = new DistributionOrder();
		distributionOrder.setOrderId(orderId);
		distributionOrder.setShopId(shopId);
		distributionOrder.setUserId(userInfo.getId());
		distributionOrder.setUserInfo(userInfo);
		distributionOrder.setOrderInfo(orderInfo);
		distributionOrder.setOrderBizResp(orderBiz);

		// 生产 订单分销政策
		OrderDistributionProperty orderDistributionProperty = distributionOrder.queryOrderDistributionProperty();
		if (orderDistributionProperty == null) {
			log.error("此订单无分销属性, orderId = {}", orderId);
			return null;
		}
		Integer goodsCategoryRate = orderDistributionProperty.getGoodsCategoryRate();
		Integer transactionRate = orderDistributionProperty.getTransactionRate();
		Integer vipShopRate = orderDistributionProperty.getVipShopRate();
		ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(goodsCategoryRate, transactionRate, vipShopRate);

		distributionOrder.setProductDistributionPolicy(productDistributionPolicy);

		return distributionOrder;
	}
}
