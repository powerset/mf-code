package mf.code.distribution.repo.mongo.po;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * mf.code.statistics.mongo.domain
 * Description:
 *
 * @author gel
 * @date 2019-07-04 19:40
 */
@Data
@ToString
@NoArgsConstructor
@Document(value = "fan_action")
public class FanAction {

    @Id
    @Field("_id")
    private String id;
    /**
     * 用户ID
     */
    @Field(value = "uid")
    private Long uid;
    /**
     * 商户id
     */
    @Field(value = "mid")
    private Long mid;
    /**
     * 店铺id
     */
    @Indexed
    @Field(value = "sid")
    private Long sid;
    /**
     * 埋点事件
     *
     * @see mf.code.distribution.constant.FanActionEventEnum
     */
    @Indexed
    @Field(value = "event")
    private String event;
    /**
     * 埋点事件子类型，当前仅用于任务 FanActionEventEnum 中6新手任务、7日常任务、8店长任务。
     * 如有其它需求，也可不依赖于 @SEE 中ActivityDefTypeEnum枚举。可另行设置
     *
     * @see mf.code.activity.constant.ActivityDefTypeEnum
     */
    @Indexed
    @Field(value = "eventType")
    private String eventType;
    /**
     * 埋点通用id
     */
    @Indexed
    @Field(value = "eventid")
    private Long eventId;
    /**
     * 涉及金额(订单金额，任务金额)
     */
    @Field(value = "amount")
    private BigDecimal amount;
    /**
     * 给上级的税额
     */
    @Field(value = "tax")
    private BigDecimal tax;
    /**
     * 时间生成时间戳
     */
    @Indexed
    @Field(value = "current")
    private Long current;
    /**
     * 额外字段(存储个性化信息)
     */
    @Field(value = "extra")
    private Map<String, Object> extra;
    /**
     * 创建时间
     */
    @Field(value = "ctime")
    private Date ctime;

}
