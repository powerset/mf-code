package mf.code.distribution.repo.mongo.repository.operator;

import mf.code.distribution.repo.mongo.po.FanAction;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.distribution.repo.mongo.repository.operator
 * Description:
 *
 * @author gel
 * @date 2019-07-05 11:24
 */
public interface FanActionOperator {

    /**
     * 通过埋点事件类型、店铺、团队列表统计人数
     *
     * @param shopId
     * @param teamUidList
     * @param eventList
     * @return
     */
    long countUidForTeamManagerSevenDay(Long shopId, List<Long> teamUidList, List<String> eventList);

    /**
     * 通过埋点事件类型、店铺、团队列表统计埋点事件数
     *
     * @param shopId
     * @param teamUidList
     * @param eventList
     * @return
     */
    long countEventIdForTeamManagerSevenDay(Long shopId, List<Long> teamUidList, List<String> eventList);

    /**
     * 通过用户id分页查询粉丝用户id列表，用于用户商品浏览查询
     *
     * @param shopId
     * @param userId
     * @param typeList
     * @param offset
     * @param limit
     * @return
     */
    List<FanAction> pageListUidListGroupByUid(Long shopId, List<Long> userId, List<String> typeList, Long offset, Long limit);

    /**
     * 查询某个粉丝全期的粉丝明细
     *
     * @param shopId
     * @param userId
     * @param typeList
     * @param offset
     * @param limit
     * @return
     */
    List<FanAction> pageListForTeamManagerAll(Long shopId, Long userId, List<String> typeList, Long offset, Long limit);

    /**
     * 分页查询团队管理粉丝行为七日记录
     *
     * @param shopId
     * @param userIdList
     * @param typeList
     * @param offset
     * @param limit
     * @return
     */
    List<FanAction> pageListForTeamManagerSevenDay(Long shopId, List<Long> userIdList, List<String> typeList, Long offset, int limit);

    /**
     * 汇总团队成员七日缴税额度(订单，或者任务)
     *
     * @param shopId
     * @param teamUidList
     * @param eventList
     * @return
     */
    BigDecimal sumTaxForTeamManagerSevenDay(Long shopId, List<Long> teamUidList, List<String> eventList);

    /**
     * 查询该粉丝全部的记录条数
     *
     * @param shopId
     * @param userId
     * @param typeList
     * @return
     */
    long countForFansActiveAll(Long shopId, Long userId, List<String> typeList);

    /**
     * 通过店铺id，用户id, 埋点事件，埋点事件id查询一条埋点记录
     *
     * @param shopId
     * @param uid
     * @param event
     * @param eventId
     * @return
     */
    FanAction queryBySidAndUidAndEventAndEventid(Long shopId, Long uid, String event, Long eventId);
}
