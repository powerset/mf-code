package mf.code.distribution.repo.mongo.repository.operator.impl;

import mf.code.common.utils.DateUtil;
import mf.code.distribution.domain.valueobject.UserSubsidyPolicy;
import mf.code.distribution.repo.mongo.repository.operator.UserSubsidyPolicyOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;

/**
 * mf.code.distribution.repo.mongo.repository.operator.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 23:38
 */
public class UserSubsidyPolicyOperatorImpl implements UserSubsidyPolicyOperator {
	@Autowired
	private MongoTemplate mongoTemplate;

	public static final String collectionName = "user_subsidy_policy";

	/**
	 * 用户用户本月的 补贴政策
	 *
	 * @param userId
	 * @return
	 */
	@Override
	public UserSubsidyPolicy findByUserId(Long userId) {
		Date startTime = DateUtil.getBeginOfThisMonth();
		Date endTime = DateUtil.getEndOfThisMonth();

		Criteria criteria = Criteria
				.where("userId").is(userId)
				.and("ctime").gte(startTime).lte(endTime);
		return mongoTemplate.findOne(new Query(criteria), UserSubsidyPolicy.class, collectionName);
	}

	/**
	 * 更新用户本月的 补贴政策 -- 领取状态
	 */
	@Override
	public void saveOrUpdate(UserSubsidyPolicy userSubsidyPolicy) {
		String id = userSubsidyPolicy.getId();

		Criteria criteria = Criteria
				.where("id").is(id);

		Update update = Update
				.update("id", id)
				.set("userId", userSubsidyPolicy.getUserId())
				.set("subsidyRemark", userSubsidyPolicy.getSubsidyRemark())
				.set("minAmount", userSubsidyPolicy.getMinAmount())
				.set("maxAmount", userSubsidyPolicy.getMaxAmount())
				.set("indirectLowerMembersRate", userSubsidyPolicy.getIndirectLowerMembersRate())
				.set("subsidyAmount", userSubsidyPolicy.getSubsidyAmount())
				.set("subsidyDays", userSubsidyPolicy.getSubsidyDays())
				.set("subsidizedDays", userSubsidyPolicy.getSubsidizedDays())
				.set("subsidyAmountMap", userSubsidyPolicy.getSubsidyAmountMap())
				.set("utime", userSubsidyPolicy.getUtime())
				.set("ctime", userSubsidyPolicy.getCtime());

		mongoTemplate.upsert(new Query(criteria), update, UserSubsidyPolicy.class);

	}

}
