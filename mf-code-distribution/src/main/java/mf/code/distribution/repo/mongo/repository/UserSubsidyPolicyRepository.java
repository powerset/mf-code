package mf.code.distribution.repo.mongo.repository;

import mf.code.distribution.domain.valueobject.UserSubsidyPolicy;
import mf.code.distribution.repo.mongo.repository.operator.UserSubsidyPolicyOperator;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * mf.code.statistics.mongo.repository
 * Description:
 *
 * @author gel
 * @date 2019-03-13 14:08
 */
public interface UserSubsidyPolicyRepository extends MongoRepository<UserSubsidyPolicy, String>, UserSubsidyPolicyOperator {

}
