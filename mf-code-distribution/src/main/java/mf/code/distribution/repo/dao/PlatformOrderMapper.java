package mf.code.distribution.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.distribution.repo.po.PlatformOrder;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformOrderMapper extends BaseMapper<PlatformOrder> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(PlatformOrder record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(PlatformOrder record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    PlatformOrder selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(PlatformOrder record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(PlatformOrder record);
}
