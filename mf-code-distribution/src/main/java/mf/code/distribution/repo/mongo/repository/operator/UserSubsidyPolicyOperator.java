package mf.code.distribution.repo.mongo.repository.operator;

import mf.code.distribution.domain.valueobject.UserSubsidyPolicy;

import java.util.Date;

/**
 * mf.code.distribution.repo.mongo.repository.operator
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 23:49
 */
public interface UserSubsidyPolicyOperator {

	/**
	 * 用户用户本月的 补贴政策
	 *
	 * @param userId
	 * @return
	 */
	UserSubsidyPolicy findByUserId(Long userId);

	/**
	 * 更新用户本月的 补贴政策 -- 领取状态
	 */
	void saveOrUpdate(UserSubsidyPolicy userSubsidyPolicy);
}
