package mf.code.distribution.repo.repository;

import mf.code.distribution.domain.aggregateroot.DistributionUser;

import java.util.Date;
import java.util.List;

/**
 * mf.code.distribution.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 09:55
 */
public interface DistributionUserRepository {

	/**
	 * 获取用户信息
	 * @param userId
	 * @return
	 */
	DistributionUser findByUserId(Long userId);

	/**
	 * 通过 shopId, userId 获取
	 *
	 * @param shopId
	 * @param userId
	 * @return
	 */
	DistributionUser findByShopIdUserId(Long shopId, Long userId);

	/**
	 * 增装 付费升店长政策
	 * @param distributionUser
	 */
	void addShopManagerSalePolicy(DistributionUser distributionUser);

	/**
	 * 增装 订单历史记录
	 *
	 * @param distributionUser
	 * @param beginTime
	 * @param endTime
	 */
	void addUserOrderHistory(DistributionUser distributionUser, Date beginTime, Date endTime);

	/**
	 * 增装 用户商城团队 上级成员及下级成员
	 *
	 * @param distributionUser
	 */
	void addUserShopTeamForAll(DistributionUser distributionUser);

	/**
	 * 增装 用户商城团队 上级成员
	 *
	 * @param distributionUser
	 */
	void addUserShopTeamForUpperMember(DistributionUser distributionUser);

	/**
	 * 增装 用户商城团队 下级成员
	 *
	 * @param distributionUser
	 */
	void addUserShopTeamForLowerMember(DistributionUser distributionUser);

	/**
	 * 增装 用户推广收益
	 * @param distributionUser
	 */
	void addUserDistributionHistory(DistributionUser distributionUser);

	/**
	 * 增装 用户补贴政策
	 * @param distributionUser
	 */
	boolean addUserSubsidyPolicy(DistributionUser distributionUser, Long groupNo);

	/**
	 * 增装 用户任务收益
	 * @param distributionUser
	 */
	void addUserTaskIncome(DistributionUser distributionUser);

	/**
	 * 保存 平台补贴 业务状态
	 * @param distributionUser
	 */
	void savePlatformSubsidy(DistributionUser distributionUser);

	/**
	 * 保存或更新分销用户数据
	 * @param distributionUser
	 * @return
	 */
	boolean saveOrUpdate(DistributionUser distributionUser);


	// ----------------------------------------------------------------------------------------------

	/**
	 * 批量 获取 分销用户
	 * @param userIds
	 * @param shopId
	 * @return
	 */
	List<DistributionUser> listByIds(List<Long> userIds, Long shopId);


	/**
	 * 批量增装 本月 订单历史记录
	 *
	 * @param distributionUsers
	 */
	void listAddUserOrderHistory(List<DistributionUser> distributionUsers);

	/**
	 * 批量 查询 用户信息
	 * @param distributionUsers
	 * @param shopId
	 * @return
	 */
	void listAddUserDistributionHistory(List<DistributionUser> distributionUsers, Long shopId);

	/**
	 * 批量 获取 用户 任务信息
	 * @param distributionUsers
	 */
	void listAddUserIncomeTask(List<DistributionUser> distributionUsers);

}
