package mf.code.distribution.repo.mongo.repository;

import mf.code.distribution.repo.mongo.po.FanAction;
import mf.code.distribution.repo.mongo.repository.operator.FanActionOperator;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * mf.code.distribution.repo.mongo.repository
 * Description:
 *
 * @author gel
 * @date 2019-07-05 11:24
 */
public interface FanActionRepository extends MongoRepository<FanAction, String>, FanActionOperator {
}
