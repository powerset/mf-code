package mf.code.distribution.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface UpayWxOrderPendMapper extends BaseMapper<UpayWxOrderPend> {

	/**
	 * 根据order_no查询
	 *
	 * @param orderNo
	 * @return
	 */
	UpayWxOrderPend selectByOrderNoPlus(String orderNo);

	/***
	 * 根据条件查询
	 */
	List<UpayWxOrderPend> listPageByParams(Map<String, Object> params);

	/***
	 * 根据条件汇总
	 */
	int countUpayWxOrder(Map<String, Object> params);

	int updateOrderStatusPlus(UpayWxOrderPend record);

	BigDecimal sumUpayWxOrderTotalFee(Map<String, Object> params);

	/**
	 * 根据主键删除数据库的记录
	 *
	 * @param id
	 */
	int deleteByPrimaryKey(Long id);

	/**
	 * 新写入数据库记录
	 *
	 * @param record
	 */
	int insert(UpayWxOrderPend record);

	/**
	 * 动态字段,写入数据库记录
	 *
	 * @param record
	 */
	int insertSelective(UpayWxOrderPend record);

	/**
	 * 根据指定主键获取一条数据库记录
	 *
	 * @param id
	 */
	UpayWxOrderPend selectByPrimaryKey(Long id);

	/**
	 * 动态字段,根据主键来更新符合条件的数据库记录
	 *
	 * @param record
	 */
	int updateByPrimaryKeySelective(UpayWxOrderPend record);

	/**
	 * 根据主键来更新符合条件的数据库记录
	 *
	 * @param record
	 */
	int updateByPrimaryKey(UpayWxOrderPend record);

	/**
	 * 获取 biz_value的 total_fee 总和
	 * @param param
	 * @return
	 */
	List<Map> batchTotalContributionPendByUserIds(Map<String, Object> param);
}
