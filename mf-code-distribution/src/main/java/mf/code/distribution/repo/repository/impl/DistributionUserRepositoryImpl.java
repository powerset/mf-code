package mf.code.distribution.repo.repository.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.feignclient.OneAppService;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.common.property.SubsidyProperty;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.*;
import mf.code.distribution.repo.dao.UpayWxOrderPendMapper;
import mf.code.distribution.repo.mongo.repository.UserSubsidyPolicyRepository;
import mf.code.distribution.repo.po.CommonDict;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.CommonDictService;
import mf.code.distribution.service.ShopManagerSalePolicyService;
import mf.code.order.dto.AppletUserOrderHistoryDTO;
import mf.code.order.dto.OrderResp;
import mf.code.user.constant.*;
import mf.code.user.dto.AppletUserDTO;
import mf.code.user.dto.UserIncomeResp;
import mf.code.user.dto.UserResp;
import mf.code.user.dto.UserTaskIncomeResp;
import mf.code.user.vo.UpayWxOrderAboutVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.distribution.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 09:56
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class DistributionUserRepositoryImpl implements DistributionUserRepository {
	private final OrderAppService orderAppService;
	private final UserAppService userAppService;
	private final CommonDictService commonDictService;
	private final UpayWxOrderPendMapper upayWxOrderPendMapper;
	private final UserSubsidyPolicyRepository userSubsidyPolicyRepository;
	private final SubsidyProperty subsidyProperty;
	private final OneAppService oneAppService;
	private final ShopManagerSalePolicyService shopManagerSalePolicyService;

	/**
	 * 获取用户信息
	 * @param userId
	 * @return
	 */
	@Override
	public DistributionUser findByUserId(Long userId) {
		UserResp userResp = userAppService.queryUser(userId);
		if (userResp == null) {
			log.error("无效用户, userId = {}", userId);
			return null;
		}
		return assemblyDistributionUser(userResp);
	}

	/**
	 * 通过 shopId, userId 获取
	 *
	 * @param shopId
	 * @param userId
	 * @return
	 */
	@Override
	public DistributionUser findByShopIdUserId(Long shopId, Long userId) {
		AppletUserDTO appletUser = userAppService.queryAppletUser(userId, shopId);
		if (appletUser == null) {
			log.error("无效用户, userId = {}", userId);
			return null;
		}
		return assemblyDistributionUser(appletUser, shopId);
	}

	/**
	 * 增装 付费升店长政策
	 * @param distributionUser
	 */
	@Override
	public void addShopManagerSalePolicy(DistributionUser distributionUser) {
		ShopManagerSalePolicy shopManagerSalePolicy = shopManagerSalePolicyService.getShopManagerSalePolicy();
		distributionUser.setShopManagerSalePolicy(shopManagerSalePolicy);
	}

	/**
	 * 增装 订单历史记录
	 *
	 * @param distributionUser
	 * @param beginTime
	 * @param endTime
	 */
	@Override
	public void addUserOrderHistory(DistributionUser distributionUser, Date beginTime, Date endTime) {
		Long shopId = distributionUser.getShopId();
		Long userId = distributionUser.getUserId();

		Map<String, Object> param = new HashMap<>();
		param.put("shopId", shopId);
		param.put("userId", userId);
		param.put("beginTime", beginTime);
		param.put("endTime", endTime);
		AppletUserOrderHistoryDTO appletUserOrderHistoryDTO = orderAppService.queryUserOrderHistory(JSON.toJSONString(param));

		assemblyDistributionUser(distributionUser, appletUserOrderHistoryDTO);
	}

	/**
	 * 增装 用户商城团队 上级成员及下级成员
	 *
	 * @param distributionUser
	 */
	@Override
	public void addUserShopTeamForAll(DistributionUser distributionUser) {

		Long shopId = distributionUser.getShopId();
		Long userId = distributionUser.getUserId();
		UserShopTeam userShopTeam = this.getUserShopTeam(distributionUser);
		if (userShopTeam == null) {
			log.error("获取用户商城团队异常, shopId = {}, userId = {}", shopId, userId);
			distributionUser.setUserShopTeam(new UserShopTeam());
			return;
		}
		this.addUpperMember(userShopTeam);
		this.addLowerMember(userShopTeam);

		distributionUser.setUserShopTeam(userShopTeam);
	}

	/**
	 * 增装 用户商城团队 上级成员
	 *
	 * @param distributionUser
	 */
	@Override
	public void addUserShopTeamForUpperMember(DistributionUser distributionUser) {

		Long userId = distributionUser.getUserId();
		UserShopTeam userShopTeam = this.getUserShopTeam(distributionUser);
		if (userShopTeam == null) {
			log.error("获取用户商城团队异常, userId = {}", userId);
			distributionUser.setUserShopTeam(new UserShopTeam());
			return;
		}
		this.addUpperMember(userShopTeam);

		distributionUser.setUserShopTeam(userShopTeam);
	}

	/**
	 * 增装 用户商城团队 下级成员 -- 获取 团队成员时，须先获取 用户历史消费订单
	 *
	 * @param distributionUser
	 */
	@Override
	public void addUserShopTeamForLowerMember(DistributionUser distributionUser) {
		UserShopTeam userShopTeam = this.getUserShopTeam(distributionUser);
		this.addLowerMember(userShopTeam);
		distributionUser.setUserShopTeam(userShopTeam);
	}

	/**
	 * 增装 用户推广收益
	 * @param distributionUser
	 */
	@Override
	public void addUserDistributionHistory(DistributionUser distributionUser) {
		Long userId = distributionUser.getUserId();
		Date settledTime = null;
		if (distributionUser.getUserIncome() != null) {
			settledTime = distributionUser.getUserIncome().getSettledTime();
		}
		// 获取当前店铺下 用户 待结算收益记录
		Map<String, UpayWxOrderPend> pendingOrderMap = new HashMap<>();
		List<UpayWxOrderPend> pendingOrders = this.getShoppingIncome(settledTime, userId);
		for (UpayWxOrderPend upayWxOrderPend : pendingOrders) {
			pendingOrderMap.put(upayWxOrderPend.getId().toString(), upayWxOrderPend);
		}

		// 装配UserIncome
		UserDistributionHistory userDistributionHistory = new UserDistributionHistory();
		userDistributionHistory.setUserId(userId);
		userDistributionHistory.setPendingOrderMap(pendingOrderMap);

		distributionUser.setUserDistributionHistory(userDistributionHistory);
	}

	/**
	 * 增装 用户补贴政策
	 * @param distributionUser
	 */
	@Override
	public boolean addUserSubsidyPolicy(DistributionUser distributionUser, Long groupNo) {
		Long userId = distributionUser.getUserId();
		UserSubsidyPolicy userSubsidyPolicy = userSubsidyPolicyRepository.findByUserId(userId);
		if (userSubsidyPolicy == null) {
			if (groupNo == null) {
				return false;
			}
			Date now = new Date();
			userSubsidyPolicy = subsidyProperty.findUserSubsidyPlicyByGroupNo(groupNo);
			userSubsidyPolicy.setId(userId.toString() + "_" + DateUtil.dateToString(now, DateUtil.LONG_DATE_FORMAT));
			userSubsidyPolicy.setUserId(userId);
			userSubsidyPolicy.setSubsidizedDays(0);
			userSubsidyPolicy.setUtime(now);
			userSubsidyPolicy.setCtime(now);

			userSubsidyPolicy.calculateOrderSubsidyAmount();
		}
		distributionUser.setUserSubsidyPolicy(userSubsidyPolicy);
		return true;
	}

	/**
	 * 增装 用户任务收益
	 * @param distributionUser
	 */
	@Override
	public void addUserTaskIncome(DistributionUser distributionUser) {
		Long userId = distributionUser.getUserId();
		UserTaskIncomeResp userTaskIncomeResp =  oneAppService.queryUserTaskIncome(userId);
		UserTaskIncome userTaskIncome = new UserTaskIncome();
		if (userTaskIncomeResp != null) {
			BeanUtils.copyProperties(userTaskIncomeResp, userTaskIncome);
		}
		Map<String, String> taskIncome = userAppService.queryTotalTaskIncomeByUserId(distributionUser.getUserId());
		if (!CollectionUtils.isEmpty(taskIncome)) {
			userTaskIncome.setTotalAward(new BigDecimal(taskIncome.get("taskAward")));
			userTaskIncome.setTotalCommission(new BigDecimal(taskIncome.get("taskCommission")));
		}

		if (distributionUser.getUserInfo().getRole() != UserRoleEnum.SHOP_MANAGER.getCode()) {
			Map<String, Object> params = new HashMap<>();
			params.put("userId", userId);
			params.put("type", UpayWxOrderPendTypeEnum.REBATE.getCode());
			params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
			params.put("bizTypes", UpayWxOrderAboutVO.addTaskCommission());
			BigDecimal bigDecimal = upayWxOrderPendMapper.sumUpayWxOrderTotalFee(params);
			BigDecimal totalCommission = userTaskIncome.getTotalCommission();
			totalCommission = totalCommission == null ? BigDecimal.ZERO : totalCommission;
			bigDecimal = bigDecimal == null ? BigDecimal.ZERO : bigDecimal;
			totalCommission = totalCommission.add(bigDecimal);
			userTaskIncome.setTotalCommission(totalCommission);
		}
		distributionUser.setUserTaskIncome(userTaskIncome);
	}

	/**
	 * 保存 平台补贴 业务状态
	 * @param distributionUser
	 */
	@Override
	public void savePlatformSubsidy(DistributionUser distributionUser) {
		userSubsidyPolicyRepository.saveOrUpdate(distributionUser.getUserSubsidyPolicy());
	}

	/**
	 * 保存或更新分销用户数据
	 * @param distributionUser
	 * @return
	 */
	@Override
	public boolean saveOrUpdate(DistributionUser distributionUser) {
		UserResp userResp = distributionUser.getUserInfo();
		return userAppService.saveOrUpdateUser(userResp);
	}

	/**
	 * 获取用户商城团队
	 * @param distributionUser
	 * @return
	 */
	private UserShopTeam getUserShopTeam(DistributionUser distributionUser) {
		UserResp userInfo = distributionUser.getUserInfo();
		Long vipShopId = userInfo.getVipShopId();
		Long userId = userInfo.getId();

		UserRebatePolicy userRebatePolicy = distributionUser.getUserRebatePolicy();
		RebateProperty rebateProperty = userRebatePolicy.queryRebatePropertyBySpendAmount(null);

		UserShopTeam userShopTeam = new UserShopTeam();
		userShopTeam.setVipShopId(vipShopId);
		userShopTeam.setUserId(userId);
		userShopTeam.setScale(rebateProperty.getScale());
		return userShopTeam;
	}

	/**
	 * 增装 所属上级成员
	 *
	 * @param userShopTeam
	 */
	private void addUpperMember(UserShopTeam userShopTeam) {
		Long vipShopId = userShopTeam.getVipShopId();
		Long userId = userShopTeam.getUserId();
		Integer scale = userShopTeam.getScale();

		// 获取上级成员 数据结构：JSON.toJSONString(Map<level, UserId>)

		Map<Integer, Long> upperMember = userAppService.queryUpperMember(vipShopId, userId, scale);
		if (upperMember == null) {
			upperMember = new HashMap<>();
		}
		userShopTeam.setUpperMembers(upperMember);
	}

	/**
	 * 增装 下级成员
	 *
	 * @param userShopTeam
	 */
	private void addLowerMember(UserShopTeam userShopTeam) {
		Long vipShopId = userShopTeam.getVipShopId();
		Long userId = userShopTeam.getUserId();
		Integer scale = userShopTeam.getScale();

		// 获取下级成员 数据结构：JSON.toJSONString(Map<level, List<UserId>>)
		Map<Integer, List<Long>> lowerMember = userAppService.queryLowerMember(vipShopId, userId, scale);
		if (lowerMember == null) {
			lowerMember = new HashMap<>();
		}
		userShopTeam.setLowerMembers(lowerMember);
	}

	/**
	 * 获取 用户返利分佣政策
	 * @return
	 */
	private UserRebatePolicy getRebatePolicy() {
		CommonDict commonDict = commonDictService.selectByTypeKey("shoppingMall", "rebatePolicy");
		// 获取店铺 月度消费标准
		BigDecimal commissionStandard = this.getCommissionStandard();
		if (commonDict == null) {
			log.error("字典表数据库异常，GoodsPolicyImpl.getUserRebatePolicy():commonDict为空");
			// 缺省值
			RebateProperty rebateProperty1 = new RebateProperty();
			rebateProperty1.setAmountCondition(0);
			rebateProperty1.setRebateRate(10);
			rebateProperty1.setCommissionRate(60);
			rebateProperty1.setScale(5);
			rebateProperty1.setTeamRewardRate(4);
			rebateProperty1.setTaxRate(26);

			RebateProperty rebateProperty2 = new RebateProperty();
			rebateProperty2.setAmountCondition(100);
			rebateProperty2.setRebateRate(50);
			rebateProperty2.setCommissionRate(20);
			rebateProperty2.setScale(5);
			rebateProperty2.setTeamRewardRate(4);
			rebateProperty2.setTaxRate(26);

			List<RebateProperty> rebatePolicies = new ArrayList<>();
			rebatePolicies.add(rebateProperty1);
			rebatePolicies.add(rebateProperty2);

			UserRebatePolicy userRebatePolicy = new UserRebatePolicy();
			userRebatePolicy.setRebateProperties(rebatePolicies);
			userRebatePolicy.setCommissionStandard(commissionStandard);
			return userRebatePolicy;
		}

		List<RebateProperty> rebatePolicies = new ArrayList<>();
		String value = commonDict.getValue();
		JSONArray jsonArray = JSON.parseArray(value);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			RebateProperty rebateProperty = new RebateProperty();
			rebateProperty.setAmountCondition(jsonObject.getInteger("amountCondition"));
			rebateProperty.setRebateRate(jsonObject.getInteger("rebateRate"));
			rebateProperty.setCommissionRate(jsonObject.getInteger("commissionRate"));
			rebateProperty.setScale(jsonObject.getInteger("scale"));
			rebateProperty.setTeamRewardRate(jsonObject.getInteger("teamRewardRate"));
			rebateProperty.setTaxRate(jsonObject.getInteger("taxRate"));
			rebatePolicies.add(rebateProperty);
		}

		UserRebatePolicy userRebatePolicy = new UserRebatePolicy();
		userRebatePolicy.setRebateProperties(rebatePolicies);
		userRebatePolicy.setCommissionStandard(commissionStandard);
		return userRebatePolicy;
	}

	/**
	 * 获取店铺 月度消费标准
	 * 如需更改 月度消费标准，可在字典表 新增 一条记录。 例：{"beginTime":"2019-3-28","condition":100.00}
	 * @return
	 */
	private BigDecimal getCommissionStandard() {

		List<CommonDict> commonDicts = commonDictService.selectListByType("shopCS");
		if (CollectionUtils.isEmpty(commonDicts)) {
			log.error("字典表数据库异常，ShopPolicyImpl.getCommissionStandard():commonDicts为空");
			// 缺省值
			return new BigDecimal(100.00);
		}

		// 对 beginTime 进行倒序排列 （从大到小）
		commonDicts.sort((o1, o2) -> {
			String value1 = o1.getValue();
			String value2 = o2.getValue();
			JSONObject jo1 = JSON.parseObject(value1);
			JSONObject jo2 = JSON.parseObject(value2);
			Date beginTime1 = DateUtil.stringtoDate(jo1.getString("beginTime"), DateUtil.LONG_DATE_FORMAT);
			Date beginTime2 = DateUtil.stringtoDate(jo2.getString("beginTime"), DateUtil.LONG_DATE_FORMAT);
			long diff = beginTime1.getTime() - beginTime2.getTime();
			if (diff > 0) {
				return -1;
			} else if (diff < 0) {
				return 1;
			}
			return 0;
		});

		// 当前时间 是否满足 变更需求
		for (CommonDict commonDict : commonDicts) {
			String value = commonDict.getValue();
			JSONObject jsonObject = JSON.parseObject(value);
			Date beginTime = DateUtil.stringtoDate(jsonObject.getString("beginTime"), DateUtil.LONG_DATE_FORMAT);
			if (beginTime.getTime() < System.currentTimeMillis()) {
				return jsonObject.getBigDecimal("condition");
			}
		}

		// 缺省值
		return new BigDecimal(100.00);
	}

	/**
	 * 获取 当前店铺下 用户 待结算收益记录
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param upayBalanceUtime
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     shopId
	 *     type = 平台充值
	 *     status = 支付成功
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	private List<UpayWxOrderPend> getShoppingIncome(Date upayBalanceUtime, Long userId) {

		// 获取待结算 起始时间
		// 调整为 2月1日00:00:00 - 2月底23:59:59 产生的待结算收益，3月19日23:59:59 清算成 已结算收益 --- 对
		// 如果 结算时间 不是本月的，那么待结算收益取到 上上月1日到现在 的记录
		// 如果结算更新时间不为空， 并且结算更新时间大于本月的约定更新时间，说明此用户本月已结算， 则 待结算收益 取到 上月20日到现在的记录
		Date begin = DateUtil.getBeginOfLastMonth();
		if (upayBalanceUtime != null && DateUtil.getMonth(upayBalanceUtime) == DateUtil.getMonth(new Date())) {
			begin = DateUtil.getBeginOfThisMonth();
		}

		// 获取待结算 取值结束时间
		Date now = new Date();

		List<Integer> bizTypes = new ArrayList<>();
		bizTypes.add(UpayWxOrderBizTypeEnum.REBATE.getCode());
		bizTypes.add(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());

		QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				// .eq(UpayWxOrderPend::getShopId, shopId)
				.eq(UpayWxOrderPend::getUserId, userId)
				.eq(UpayWxOrderPend::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
				.eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode())
				.in(UpayWxOrderPend::getBizType, bizTypes)
				.between(UpayWxOrderPend::getPaymentTime, begin, now);
		return upayWxOrderPendMapper.selectList(wrapper);
	}

	// ------------------------------------------------------------------------------------------------

	/**
	 * 批量 获取 分销用户
	 * @param userIds
	 * @param shopId
	 * @return
	 */
	@Override
	public List<DistributionUser> listByIds(List<Long> userIds, Long shopId) {
		log.info("获取所有下级用户信息, userIds = {}", userIds);
		List<DistributionUser> distributionUsers = new ArrayList<>();

		// 分批查询 -- 每次 100
		int batchCount = 30;
		int sourListSize = userIds.size();
		int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
		int startIndext = 0;
		int stopIndext = 0;
		for(int i = 0; i < subCount; i++){
			stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
			List<Long> tempList = new ArrayList<>(userIds.subList(startIndext, stopIndext));
			if(tempList.size() > 0){

				List<AppletUserDTO> appletUserDTOList = userAppService.listAppletUser(shopId, tempList);
				if (CollectionUtils.isEmpty(appletUserDTOList)) {
					log.error("本次分批获取的 分销用户 为空, userIds = {} ", tempList);
					appletUserDTOList = new ArrayList<>();
				}
				for (AppletUserDTO appletUser : appletUserDTOList) {
					distributionUsers.add(assemblyDistributionUser(appletUser, shopId));
				}
			}
			startIndext = stopIndext;
		}

		return distributionUsers;
	}

	/**
	 * 批量增装 本月 订单历史记录
	 *
	 * @param distributionUsers
	 */
	@Override
	public void listAddUserOrderHistory(List<DistributionUser> distributionUsers) {
		Long shopId = distributionUsers.get(0).getShopId();
		List<Long> userIds = new ArrayList<>();
		for (DistributionUser distributionUser : distributionUsers) {
			userIds.add(distributionUser.getUserId());
		}
		// 本月消费订单
		Date beginTime = DateUtil.getBeginOfThisMonth();
		Date endTime = new Date();

		List<AppletUserOrderHistoryDTO> appletUserOrderHistoryTotalList = new ArrayList<>();
		// 分批查询 -- 每次 100
		int batchCount = 30;
		int sourListSize = userIds.size();
		int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
		int startIndext = 0;
		int stopIndext = 0;
		for(int i = 0; i < subCount; i++){
			stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
			List<Long> tempList = new ArrayList<>(userIds.subList(startIndext, stopIndext));
			if(tempList.size() > 0){
				Map<String, Object> param = new HashMap<>();
				param.put("shopId", shopId);
				param.put("userIds", tempList);
				param.put("beginTime", beginTime);
				param.put("endTime", endTime);
				List<AppletUserOrderHistoryDTO> appletUserOrderHistoryDTOList = orderAppService.listUserOrderHistory(JSON.toJSONString(param));
				if (!CollectionUtils.isEmpty(appletUserOrderHistoryDTOList)) {
					appletUserOrderHistoryTotalList.addAll(appletUserOrderHistoryDTOList);
				}
			}
			startIndext = stopIndext;
		}

		if (CollectionUtils.isEmpty(appletUserOrderHistoryTotalList)) {
			UserOrderHistory userOrderHistory = new UserOrderHistory();
			for (DistributionUser distributionUser : distributionUsers) {
				distributionUser.setUserOrderHistory(userOrderHistory);
			}
			return ;
		}

		Map<Long, AppletUserOrderHistoryDTO> userOrderHistoryMap = new HashMap<>();
		for (AppletUserOrderHistoryDTO appletUserOrderHistoryDTO : appletUserOrderHistoryTotalList) {
			userOrderHistoryMap.put(appletUserOrderHistoryDTO.getUserId(), appletUserOrderHistoryDTO);
		}

		for (DistributionUser distributionUser : distributionUsers) {
			Long userId = distributionUser.getUserId();

			UserOrderHistory userOrderHistory = new UserOrderHistory();
			AppletUserOrderHistoryDTO appletUserOrderHistoryDTO = userOrderHistoryMap.get(userId);
			if (appletUserOrderHistoryDTO != null) {
				BeanUtils.copyProperties(appletUserOrderHistoryDTO, userOrderHistory);
			}
			distributionUser.setUserOrderHistory(userOrderHistory);
		}
	}

	/**
	 * 批量 增装用户推广收益 到 distributionUser
	 * @param distributionUsers
	 * @param shopId
	 * @return
	 */
	@Override
	public void listAddUserDistributionHistory(List<DistributionUser> distributionUsers, Long shopId) {

		// 分批获取 当前店铺下 用户 待结算收益记录 Map<userId, List<UpayWxOrderPend>>
		Map<Long, List<UpayWxOrderPend>> pendingOrdersByUid = this.batchPendingOrders(distributionUsers, shopId);

		for (DistributionUser distributionUser : distributionUsers) {
			Long userId = distributionUser.getUserId();
			// 此用户的待结算收益
			Map<String, UpayWxOrderPend> pendingOrderMap = new HashMap<>();
			List<UpayWxOrderPend> pendingOrders = pendingOrdersByUid.get(userId);
			if(!CollectionUtils.isEmpty(pendingOrders)){
				for (UpayWxOrderPend upayWxOrderPend : pendingOrders) {
					pendingOrderMap.put(upayWxOrderPend.getId().toString(), upayWxOrderPend);
				}
			}

			UserDistributionHistory userDistributionHistory = new UserDistributionHistory();
			userDistributionHistory.setPendingOrderMap(pendingOrderMap);
			distributionUser.setUserDistributionHistory(userDistributionHistory);
		}
	}

	@Override
	public void listAddUserIncomeTask(List<DistributionUser> distributionUsers) {

		List<Long> userIds = new ArrayList<>();
		for (DistributionUser distributionUser : distributionUsers) {
			userIds.add(distributionUser.getUserId());
		}

		Map<Long, UserTaskIncomeResp> userTaskIncomeRespMap = new HashMap<>();
		Map<Long, Map<String, String>> userTaskIncomeMapTotal = new HashMap<>();
		// 分批查询 -- 每次 100
		int batchCount = 30;
		int sourListSize = userIds.size();
		int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
		int startIndext = 0;
		int stopIndext = 0;
		for(int i = 0; i < subCount; i++){
			stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
			List<Long> tempList = new ArrayList<>(userIds.subList(startIndext, stopIndext));
			if(tempList.size() > 0){

				List<UserTaskIncomeResp> userTaskIncomeRespList =  oneAppService.listUserTaskIncome(tempList);

				if (!CollectionUtils.isEmpty(userTaskIncomeRespList)) {
					for (UserTaskIncomeResp userTaskIncomeResp : userTaskIncomeRespList) {
						userTaskIncomeRespMap.put(userTaskIncomeResp.getUserId(), userTaskIncomeResp);
					}
				}

				Map<Long, Map<String, String>> userTaskIncomeMap = userAppService.listTotalTaskIncomeByUserIds(tempList);
				if (!CollectionUtils.isEmpty(userTaskIncomeMap)) {
					userTaskIncomeMapTotal.putAll(userTaskIncomeMap);
				}

			}
			startIndext = stopIndext;
		}

		for (DistributionUser distributionUser : distributionUsers) {
			UserTaskIncomeResp userTaskIncomeResp = userTaskIncomeRespMap.get(distributionUser.getUserId());
			UserTaskIncome userTaskIncome = new UserTaskIncome();
			if (userTaskIncomeResp != null) {
				BeanUtils.copyProperties(userTaskIncomeResp, userTaskIncome);
			}
			Map<String, String> taskIncome = userTaskIncomeMapTotal.get(distributionUser.getUserId());
			if (!CollectionUtils.isEmpty(taskIncome)) {
				userTaskIncome.setTotalAward(new BigDecimal(taskIncome.get("taskAward")));
				userTaskIncome.setTotalCommission(new BigDecimal(taskIncome.get("taskCommission")));
			}
			distributionUser.setUserTaskIncome(userTaskIncome);
		}

	}

	/**
	 * 分批获取 当前店铺下 用户 待结算收益记录 -- 对待结算 和 已结算 做了优化
	 * @param distributionUsers
	 * @param shopId
	 * @return
	 * <per>
	 *    userId
	 *    shopId
	 *    type = 平台充值
	 *    status = 支付成功
	 *    biz_type = 返利+分佣
	 *    payment_time = 待结算时间
	 * </per>
	 */
	private Map<Long, List<UpayWxOrderPend>> batchPendingOrders(List<DistributionUser> distributionUsers, Long shopId) {
		// 对 获取待结算收益 记录的时间 做个分类
		Date thisMonth = DateUtil.getBeginOfThisMonth();
		Date lastMonth = DateUtil.getBeginOfLastMonth();

		List<Long> thisMonthUserIds = new ArrayList<>();
		List<Long> lastMonthUserIds = new ArrayList<>();

		for (DistributionUser distributionUser : distributionUsers) {
			Long userId = distributionUser.getUserId();
			UserIncomeResp userIncome = distributionUser.getUserIncome();
			// 获取待结算 起始时间
			Date settledTime = userIncome.getSettledTime();
			// 如果结算更新时间不为空， 并且结算更新时间大于本月的约定更新时间，说明此用户本月已结算， 则 待结算收益 取到 本月1日到现在的记录
			if (settledTime != null && DateUtil.getMonth(settledTime) == DateUtil.getMonth(new Date())) {
				thisMonthUserIds.add(userId);
			} else {
				// 待结算收益 取到 上月1日到现在 的记录
				lastMonthUserIds.add(userId);
			}
		}

		List<UpayWxOrderPend> upayWxOrderPendsLastMonth = this.batchPendingOrdersByBegin(thisMonthUserIds, shopId, thisMonth);
		List<UpayWxOrderPend> upayWxOrderPendsTwoMonth = this.batchPendingOrdersByBegin(lastMonthUserIds, shopId, lastMonth);

		List<UpayWxOrderPend> upayWxOrderPendList = new ArrayList<>();
		upayWxOrderPendList.addAll(upayWxOrderPendsLastMonth);
		upayWxOrderPendList.addAll(upayWxOrderPendsTwoMonth);

		// 待结算收益记录 按用户id 分组
		Map<Long, List<UpayWxOrderPend>> upayWxOrderPendMap = new HashMap<>();
		for (DistributionUser distributionUser : distributionUsers) {
			Long userId = distributionUser.getUserId();
			List<UpayWxOrderPend> upayWxOrderPends = new ArrayList<>();
			for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPendList) {
				if (userId.equals(upayWxOrderPend.getUserId())) {
					upayWxOrderPends.add(upayWxOrderPend);
				}
			}
			upayWxOrderPendMap.put(userId, upayWxOrderPends);
		}
		return upayWxOrderPendMap;
	}

	/**
	 * 分批获取 当前店铺下 用户 待结算收益记录
	 * @param uids
	 * @param sid
	 * @param begin
	 * @return
	 * <per>
	 *    userId
	 *    shopId
	 *    type = 平台充值
	 *    status = 支付成功
	 *    biz_type = 返利+分佣
	 *    payment_time = 待结算时间
	 * </per>
	 */
	private List<UpayWxOrderPend> batchPendingOrdersByBegin(List<Long> uids, Long sid, Date begin) {
		List<UpayWxOrderPend> upayWxOrderPendList = new ArrayList<>();
		if (CollectionUtils.isEmpty(uids) || sid == null) {
			return upayWxOrderPendList;
		}
		// 获取待结算 取值结束时间
		Date now = new Date();
		List<Integer> bizTypes = new ArrayList<>();
		bizTypes.add(UpayWxOrderBizTypeEnum.REBATE.getCode());
		bizTypes.add(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());

		// 分批查询 -- 每次 100
		int batchCount = 100;
		int sourListSize = uids.size();
		int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
		int startIndext = 0;
		int stopIndext = 0;
		for(int i = 0; i < subCount; i++){
			stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
			List<Long> tempList = new ArrayList<>(uids.subList(startIndext, stopIndext));
			if(tempList.size() > 0){
				QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
				wrapper.lambda()
						.in(UpayWxOrderPend::getUserId, tempList)
						// .eq(UpayWxOrderPend::getShopId, sid)  2.1迭代 所有店铺待结算总和
						.eq(UpayWxOrderPend::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
						.eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode())
						.in(UpayWxOrderPend::getBizType, bizTypes)
						.between(UpayWxOrderPend::getPaymentTime, begin, now);
				List<UpayWxOrderPend> upayWxOrderPends = upayWxOrderPendMapper.selectList(wrapper);
				if (upayWxOrderPends == null) {
					upayWxOrderPends = new ArrayList<>();
				}
				upayWxOrderPendList.addAll(upayWxOrderPends);
			}
			startIndext = stopIndext;
		}
		return upayWxOrderPendList;
	}

	/**
	 * 分批获取 当前店铺下 用户从推广到现在的 可结算收益 的总和 -- 返利+贡献
	 * @param uids
	 * @param sid
	 * @return
	 */
	private Map<Long, BigDecimal> batchTotalSettleIncome(List<Long> uids, Long sid) {
		Map<Long, BigDecimal> totalSettledIncomeMap = new HashMap<>();
		if (CollectionUtils.isEmpty(uids) || sid == null) {
			return totalSettledIncomeMap;
		}

		List<Map> totalMap = new ArrayList<>();
		// 分批查询 -- 每次 100
		int batchCount = 100;
		int sourListSize = uids.size();
		int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
		int startIndext = 0;
		int stopIndext = 0;
		for(int i = 0; i < subCount; i++){
			stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
			List<Long> userIdList = new ArrayList<>(uids.subList(startIndext, stopIndext));
			if(userIdList.size() > 0){
				List<Map> totalSettledIncome = userAppService.batchSumTotalFeeSettledByUserIds(sid, userIdList);
				if (totalSettledIncome == null) {
					totalSettledIncome = new ArrayList<>();
				}
				totalMap.addAll(totalSettledIncome);
			}
			startIndext = stopIndext;
		}

		for (Map map : totalMap) {
			Long userId = Long.valueOf(map.get("userId").toString());
			BigDecimal totalSettledIncome = new BigDecimal(map.get("totalSettledIncome").toString());
			totalSettledIncomeMap.put(userId, totalSettledIncome);
		}
		return totalSettledIncomeMap;
	}

	private DistributionUser assemblyDistributionUser(UserResp userResp) {
		UserRebatePolicy userRebatePolicy = this.getRebatePolicy();

		DistributionUser distributionUser = new DistributionUser();
		distributionUser.setUserRebatePolicy(userRebatePolicy);
		distributionUser.setUserId(userResp.getId());
		distributionUser.setShopId(userResp.getVipShopId());
		distributionUser.setUserInfo(userResp);
		return distributionUser;
	}

	// 装配 userInfo 到 distributionUser
	private DistributionUser assemblyDistributionUser(AppletUserDTO appletUser, Long shopId) {
		UserRebatePolicy userRebatePolicy = this.getRebatePolicy();

		DistributionUser distributionUser = new DistributionUser();
		distributionUser.setUserRebatePolicy(userRebatePolicy);
		distributionUser.setShopId(shopId);
		distributionUser.setUserId(appletUser.getUserId());
		distributionUser.setUserInfo(appletUser.getUserResp());
		distributionUser.setUserIncome(appletUser.getUserIncomeResp());
		return distributionUser;
	}

	// 装配 UserOrderHistory 到distributionUser 中
	private void assemblyDistributionUser(DistributionUser distributionUser, AppletUserOrderHistoryDTO appletUserOrderHistoryDTO) {
		UserOrderHistory userOrderHistory = new UserOrderHistory();


		if (appletUserOrderHistoryDTO != null) {
			userOrderHistory.setUserId(appletUserOrderHistoryDTO.getUserId());
			userOrderHistory.setShopId(appletUserOrderHistoryDTO.getShopId());
			userOrderHistory.setBeginTime(appletUserOrderHistoryDTO.getBeginTime());
			userOrderHistory.setEndTime(appletUserOrderHistoryDTO.getEndTime());
			userOrderHistory.setLastSpendAmount(appletUserOrderHistoryDTO.getLastSpendAmount());
			userOrderHistory.setSpendAmount(appletUserOrderHistoryDTO.getSpendAmount());

			List<OrderResp> paidOrder = appletUserOrderHistoryDTO.getPaidOrder();
			List<OrderResp> afterSaleProcessing = appletUserOrderHistoryDTO.getAfterSaleProcessing();

			List<OrderResp> newPaidOrder = new ArrayList<>(paidOrder);
			List<OrderResp> newAfterSaleProcessing = new ArrayList<>(afterSaleProcessing);

			userOrderHistory.setPaidOrder(newPaidOrder);
			userOrderHistory.setAfterSaleProcessing(newAfterSaleProcessing);

		}

		distributionUser.setUserOrderHistory(userOrderHistory);
	}

}
