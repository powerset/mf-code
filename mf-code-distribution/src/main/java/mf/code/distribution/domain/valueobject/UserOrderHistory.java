package mf.code.distribution.domain.valueobject;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import mf.code.order.dto.OrderBizResp;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 09:04
 */
@Data
public class UserOrderHistory {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 联合标识：开始时间
	 */
	private Date beginTime;
	/**
	 * 联合标识：截止时间
	 */
	private Date endTime;
	/**
	 * 现在日期的上月消费金额 FIXME_OK 2.1迭代 按用户维度计算，应总和所有店铺消费的订单
	 */
	private BigDecimal lastSpendAmount;
	/**
	 * TODO_OK 2.1 迭代 按用户维度计算，应总和 指定时间周期的 所有店铺消费的订单
	 */
	private BigDecimal spendAmount;
	/**
	 * 指定时间周期的 已支付的订单 -- 含 分销属性
	 */
	private List<OrderResp> paidOrder = new ArrayList<>();
	/**
	 * 指定时间周期的 售后处理中的订单，此订单时间周期为 beginTime 至 now
	 */
	private List<OrderResp> afterSaleProcessing = new ArrayList<>();

	/**
	 * 统计用户消费金额的订单 = 已支付订单 - 售后处理中订单
	 */
	public List<OrderResp> queryPaidOrders() {

		List<OrderResp> paidOrders = new ArrayList<>();
		if (CollectionUtils.isEmpty(paidOrder)) {
			return paidOrders;
		}
		// 暂不 减去 售后处理中的订单 -- 如果启用，则注意目前逻辑中，上月消费金额 未减去 售后处理中的订单金额
		// Map<Long, OrderResp> afterSaleProcessingMap = new HashMap<>();
		// for (OrderResp order : this.afterSaleProcessing) {
		// 	afterSaleProcessingMap.put(order.getId(), order);
		// }
		for (OrderResp orderResp : this.paidOrder) {
			// if (afterSaleProcessingMap.containsKey(orderResp.getId())) {
			// 	continue;
			// }
			paidOrders.add(orderResp);
		}
		return paidOrders;
	}

	/**
	 * 本月20日 可结算订单 = 上月 交易成功 - 售后处理中订单
	 * beginTime = 上月开始
	 * endTiem = 上月结束
	 *
	 * @return
	 */
	public List<OrderResp> querySettledOrders() {
		List<OrderResp> settledOrders = new ArrayList<>();
		if (CollectionUtils.isEmpty(paidOrder)) {
			return settledOrders;
		}
		Map<Long, OrderResp> afterSaleProcessingMap = new HashMap<>();
		for (OrderResp order : this.afterSaleProcessing) {
			afterSaleProcessingMap.put(order.getId(), order);
		}
		for (OrderResp order : this.afterSaleProcessing) {
			if (OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode() != order.getStatus()) {
				continue;
			}
			if (afterSaleProcessingMap.containsKey(order.getId())) {
				continue;
			}
			settledOrders.add(order);
		}
		return settledOrders;
	}

	/**
	 * 通过订单id 获取对应订单信息
	 * @param orderId
	 * @return
	 */
	public OrderResp queryPaidOrderById(Long orderId) {
		for (OrderResp order : this.paidOrder) {
			if (orderId.equals(order.getId())) {
				return order;
			}
		}
		return null;
	}

	/**
	 * 累计消费金额
	 * @param orderFee
	 */
	public void addSpendAmount(BigDecimal orderFee) {
		if (this.spendAmount == null) {
			this.spendAmount = BigDecimal.ZERO;
		}
		this.spendAmount = this.spendAmount.add(orderFee);
	}
}
