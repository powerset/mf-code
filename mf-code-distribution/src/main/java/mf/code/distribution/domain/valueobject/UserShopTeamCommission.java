package mf.code.distribution.domain.valueobject;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 用户可分配佣金部分 各成员佣金所得
 *
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 00:28
 */
@Data
public class UserShopTeamCommission {
	/**
	 * 返利佣金
	 */
	private BigDecimal rebateCommission;

	/**
	 * 每个上级可获得的分佣金额
	 */
	private BigDecimal perPubCommission;

	/**
	 * 商城用户团队奖励 -- 暂由平台回收
	 */
	private BigDecimal teamRewardAmount;

	/**
	 * 用户分佣税务金额 -- 暂由平台回收
	 */

	private BigDecimal taxAmount;
	/**
	 * 是否拆分 -- 默认false 无拆分
	 */
	private boolean isSplit = false;

	/**
	 * 是否是达标部分 -- 默认不达标部分， 此字段只有当 isSplit = true 时，才有实际意义
	 */
	private boolean isReach = false;
}
