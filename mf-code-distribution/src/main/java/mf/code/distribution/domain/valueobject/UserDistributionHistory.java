package mf.code.distribution.domain.valueobject;

import lombok.Data;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.user.constant.UpayWxOrderPendBizTypeEnum;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 用户分销历史记录 == 团队贡献 + 购物返利
 * <p>
 * mf.code.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-27 20:11
 */
@Data
public class UserDistributionHistory {
	/**
	 * 收入所有者
	 */
	private Long userId;
	/**
	 * 总的待结算收益记录: 1月1日00:00:00 - 1月31日:23:59:59 产生的待结算收益，在 2月19日23:59:59 清算成 已结算收益
	 * Map<upayWxOrderPendId, upayWxOrderPend>
	 */
	private Map<String, UpayWxOrderPend> pendingOrderMap = new HashMap<>();

	/**
	 * 今日 待结算自购省(待结算购物返利)
	 * @return
	 */
	public BigDecimal queryRebatePendToday() {
		Date dateAfterZero = DateUtil.getDateAfterZero(new Date());

		// 非店长
		Collection<UpayWxOrderPend> upayWxOrderPends = this.pendingOrderMap.values();
		BigDecimal pendingIncome = BigDecimal.ZERO;
		// 今日 待结算购物返利
		for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
			Date paymentTime = upayWxOrderPend.getPaymentTime();
			// 过滤 支付时间比今天0点早的订单 ； 过滤 支付类型是团队贡献的订单
			if (paymentTime.compareTo(dateAfterZero) < 0
					|| UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()) {
				continue;
			}
			pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
		}
		return pendingIncome;
	}

	/**
	 * 今日分享赚 （今日预估收益；今日团队贡献）
	 *
	 * @return
	 */
	public BigDecimal queryEstimateIncomeToday() {
		Date dateAfterZero = DateUtil.getDateAfterZero(new Date());
		BigDecimal pendingIncome = BigDecimal.ZERO;
		for (UpayWxOrderPend upayWxOrderPend : this.pendingOrderMap.values()) {
			if (upayWxOrderPend.getPaymentTime().compareTo(dateAfterZero) >= 0
					&& UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()) {
				pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
			}
		}
		return pendingIncome;
	}

	/**
	 * 本月分享赚 （本月预估收益；本月团队贡献）
	 *
	 * @return
	 */
	public BigDecimal queryEstimateIncome() {
		int currentMonth = DateUtil.getMonth(new Date());
		BigDecimal pendingIncome = BigDecimal.ZERO;
		for (UpayWxOrderPend upayWxOrderPend : this.pendingOrderMap.values()) {
			int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
			if (month == currentMonth && UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()) {
				pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
			}
		}
		return pendingIncome;
	}

	/**
	 * 获取本月 匹配备注（平台补贴订单）的 待结算订单集合
	 *
	 * @param subsidyRemark
	 * @return
	 */
	public List<UpayWxOrderPend> querySubsidyPend(String subsidyRemark) {
		List<UpayWxOrderPend> subsidyPends = new ArrayList<>();
		int currentMonth = DateUtil.getMonth(new Date());
		for (UpayWxOrderPend upayWxOrderPend : this.pendingOrderMap.values()) {
			int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
			if (month == currentMonth
					&& UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()
					&& StringUtils.equals(upayWxOrderPend.getRemark(), subsidyRemark)) {
				subsidyPends.add(upayWxOrderPend);
			}
		}
		// 按金额从小到大排序
		subsidyPends.sort((o1, o2) -> Integer.compare(0, o2.getTotalFee().compareTo(o1.getTotalFee())));
		return subsidyPends;
	}
}
