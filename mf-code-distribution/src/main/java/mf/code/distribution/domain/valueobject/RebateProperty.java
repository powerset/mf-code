package mf.code.distribution.domain.valueobject;

import lombok.Data;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 08:59
 */
@Data
public class RebateProperty {
	/**
	 * 返利 比率 依据条件
	 */
	private Integer amountCondition;
	/**
	 * 返利比率 0-100
	 */
	private Integer rebateRate;
	/**
	 * 贡献比率 0-100
	 */
	private Integer commissionRate;
	/**
	 * 贡献层级
	 */
	private Integer scale;
	/**
	 * 商城团队奖励比例 4
	 */
	private Integer teamRewardRate;
	/**
	 * 用户分佣税务比例 26
	 */
	private Integer taxRate;
}
