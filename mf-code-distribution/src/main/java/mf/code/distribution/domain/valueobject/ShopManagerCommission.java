package mf.code.distribution.domain.valueobject;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-26 10:43
 */
@Data
public class ShopManagerCommission {

    private BigDecimal superiorCommission;

    private BigDecimal shopCommission;

    private BigDecimal platformCommission;
}
