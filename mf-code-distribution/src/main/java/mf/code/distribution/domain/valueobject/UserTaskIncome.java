package mf.code.distribution.domain.valueobject;

import lombok.Data;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.one.dto.UserCouponDTO;
import mf.code.one.dto.UserTaskDTO;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-17 13:44
 */
@Data
public class UserTaskIncome {
	/**
	 * 今日任务奖金
	 */
	private BigDecimal todayAward = BigDecimal.ZERO;
	/**
	 * 今日任务佣金
	 */
	private BigDecimal todayCommission = BigDecimal.ZERO;
	/**
	 * 累计任务奖金
	 */
	private BigDecimal totalAward = BigDecimal.ZERO;
	/**
	 * 累计任务佣金
	 */
	private BigDecimal totalCommission = BigDecimal.ZERO;
	/**
	 * 店长任务资格
	 */
	private Map<Long, UserTaskDTO> userTaskMap;
	/**
	 * 店长任务 奖励情况
	 */
	private Map<Long, UserCouponDTO> userCouponMap;

	/**
	 * 已获得 推荐店长 奖金数
	 */
	public int queryRecommendShopManagerSize() {
		int size = 0;
		if (CollectionUtils.isEmpty(userCouponMap)) {
			return size;
		}
		for (UserCouponDTO userCouponDTO : userCouponMap.values()) {
			if (userCouponDTO.getType() == UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
				size++;
			}
		}
		return size;
	}
}
