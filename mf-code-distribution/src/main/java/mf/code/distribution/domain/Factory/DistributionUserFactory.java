package mf.code.distribution.domain.Factory;

import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.RebateProperty;
import mf.code.distribution.domain.valueobject.UserOrderHistory;
import mf.code.distribution.domain.valueobject.UserRebatePolicy;
import mf.code.distribution.domain.valueobject.UserShopTeam;
import mf.code.user.dto.UserResp;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.domain.Factory
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 10:00
 */
public class DistributionUserFactory {

	/**
	 * 通过已有原型 初始化 订单消费记录
	 * @param prototype
	 * @return
	 */
	public static DistributionUser newDistributionUser(DistributionUser prototype) {

		UserResp userInfo = prototype.getUserInfo();
		UserOrderHistory userOrderHistory = prototype.getUserOrderHistory();
		UserRebatePolicy userRebatePolicy = prototype.getUserRebatePolicy();
		List<RebateProperty> rebateProperties = userRebatePolicy.getRebateProperties();
		UserShopTeam userShopTeam = prototype.getUserShopTeam();
		Map<Integer, List<Long>> lowerMembers = userShopTeam.getLowerMembers();


		// 用户信息副本
		UserResp userRespDTO = new UserResp();
		BeanUtils.copyProperties(userInfo, userRespDTO);

		// 不复制 订单消费记录
		UserOrderHistory newUserOrderHistory = new UserOrderHistory();

		// 用户分佣返利政策副本
		UserRebatePolicy newUserRebatePolicy = new UserRebatePolicy();
		List<RebateProperty> newRebateProperties = new ArrayList<>();
		for (RebateProperty rebateProperty : rebateProperties) {
			RebateProperty newRebateProperty = new RebateProperty();
			BeanUtils.copyProperties(rebateProperty, newRebateProperty);
			newRebateProperties.add(newRebateProperty);
		}
		newUserRebatePolicy.setRebateProperties(newRebateProperties);
		newUserRebatePolicy.setCommissionStandard(userRebatePolicy.getCommissionStandard());

		// 用户商城团队副本
		UserShopTeam newUserShopTeam = new UserShopTeam();
		Map<Integer, List<Long>> newLowerMembers = new HashMap<>();
		for (Map.Entry<Integer, List<Long>> entry : lowerMembers.entrySet()) {
			List<Long> newLowerIds = new ArrayList<>(entry.getValue());
			newLowerMembers.put(entry.getKey(), newLowerIds);
		}
		Map<Integer, Long> newUpperMembers = new HashMap<>(userShopTeam.getUpperMembers());
		newUserShopTeam.setUserId(userShopTeam.getUserId());
		newUserShopTeam.setVipShopId(userShopTeam.getVipShopId());
		newUserShopTeam.setScale(userShopTeam.getScale());
		newUserShopTeam.setUpperMembers(newUpperMembers);
		newUserShopTeam.setLowerMembers(newLowerMembers);

		// 装配 副本 分销用户
		DistributionUser distributionUser = new DistributionUser();
		distributionUser.setShopId(prototype.getShopId());
		distributionUser.setUserId(prototype.getUserId());
		distributionUser.setUserInfo(userRespDTO);
		distributionUser.setUserShopTeam(newUserShopTeam);
		distributionUser.setUserOrderHistory(newUserOrderHistory);
		distributionUser.setUserRebatePolicy(newUserRebatePolicy);

		return distributionUser;
	}
}
