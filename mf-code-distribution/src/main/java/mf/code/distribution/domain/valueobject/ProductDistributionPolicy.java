package mf.code.distribution.domain.valueobject;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;

import java.math.BigDecimal;

/**
 * 商品分销 相关政策
 * mf.code.goods.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-04 01:09
 */
@Slf4j
@Data
public class ProductDistributionPolicy {

	/**
	 * 商品类目抽佣比例（平台抽佣）默认值 10(%)
	 */
	private Integer goodsCategoryRate;
	/**
	 * 交易税率 默认值 2(%)
	 */
	private Integer transactionRate;
	/**
	 * 粉丝店铺所得分佣
	 */
	private Integer vipShopRate;

	public ProductDistributionPolicy(Integer goodsCategoryRate, Integer transactionRate, Integer vipShopRate) {
		this.goodsCategoryRate = goodsCategoryRate;
		this.transactionRate = transactionRate;
		this.vipShopRate = vipShopRate;
	}

	/**
	 * 计算各部分佣金所得
	 *
	 * @param price                计算的金额 -- 支付支付金额 或 商品价格
	 * @param selfSupportRatio     分销商抽佣比例
	 * @param platSupportRatio     供应商佣金比例
	 * @param distributionTypeEnum 分销类型
	 * @return
	 */
	public OrderCommissiones calculateOrderCommissionsForEachPart(BigDecimal price,
	                                                              BigDecimal selfSupportRatio,
	                                                              BigDecimal platSupportRatio,
	                                                              ProductDistributionTypeEnum distributionTypeEnum) {

		OrderCommissiones orderCommissiones = new OrderCommissiones();

		// 交易税 = 订单支付金额 * 交易税率 （可配置）
		BigDecimal transactionCommission = price.multiply(new BigDecimal(this.transactionRate))
				.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
		orderCommissiones.setTransactionCommission(transactionCommission);

		// 分销平台获取商品 分销
		if (distributionTypeEnum == ProductDistributionTypeEnum.RESELL) {
			// 供货商佣金 = 商品价格 * 佣金比率（自行设置）
			BigDecimal supplierCommission = price.multiply(platSupportRatio)
					.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
			// 供货商所得 = 商品价格 - 供货商佣金 -  交易税
			BigDecimal supplierIncome = price.subtract(supplierCommission).subtract(transactionCommission);

			// 平台抽佣 = 供货商佣金 * 10%(平台类目抽佣比率)
			BigDecimal platformCommission = supplierCommission.multiply(new BigDecimal(this.goodsCategoryRate))
					.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
			// 分销广场佣金 = 供货商佣金 - 平台抽佣
			BigDecimal squareCommission = supplierCommission.subtract(platformCommission);

			// 分销商抽佣总所得（含粉丝店铺所得，须拆分） = 分销广场佣金 * 分销商抽佣比率（自行配置） --  selfSupportRatio入库的也是分佣比率 -- 需转成抽佣计算所得
			BigDecimal distributorCommissionTotal = squareCommission.multiply(new BigDecimal(100).subtract(selfSupportRatio))
					.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);

			// 2.1迭代加入粉丝店铺所得，从分销商总所得佣金 按粉丝店铺分佣比率，抽出部分佣金给 粉丝店铺所得
			if (this.vipShopRate == null) {
				this.vipShopRate = 50;
			}
			BigDecimal vipShopIncome = distributorCommissionTotal.multiply(new BigDecimal(this.vipShopRate))
					.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);

			// 实际分销商所得 = 分销商抽佣总所得 - 粉丝店铺所得
			BigDecimal distributorCommission = distributorCommissionTotal.subtract(vipShopIncome);

			// 用户可分配佣金 X = 分销广场佣金 - 分销商抽佣总所得
			BigDecimal customerCommission = squareCommission.subtract(distributorCommissionTotal);

			log.info(">>>>>>>have find customerCommission="+customerCommission+",squareCommission="+squareCommission+",distributorCommissionTotal="+distributorCommissionTotal+",squareCommission="+squareCommission+",supplierCommission="+supplierCommission);

			orderCommissiones.setSupplierCommission(supplierCommission);
			orderCommissiones.setSupplierIncome(supplierIncome);
			orderCommissiones.setPlatformCommission(platformCommission);
			orderCommissiones.setSquareCommission(squareCommission);
			orderCommissiones.setDistributorCommission(distributorCommission);
			orderCommissiones.setVipShopIncome(vipShopIncome);
			orderCommissiones.setCustomerCommission(customerCommission);
		} else if (distributionTypeEnum == ProductDistributionTypeEnum.SELF_SELL
				|| distributionTypeEnum == ProductDistributionTypeEnum.PALTFORM_SELLSELF) {
			// 自卖分销 --  selfSupportRatio是分佣
			// 供货商佣金 = 订单支付金额 * 佣金比率（自行设置）
			BigDecimal supplierCommission = price.multiply(selfSupportRatio)
					.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
			// 供货商所得 = 订单支付金额 - 供货商佣金 - 交易税
			BigDecimal supplierIncome = price.subtract(supplierCommission).subtract(transactionCommission);

			// 平台抽佣 = 供货商佣金 * 10%(平台类目抽佣比率)
			BigDecimal platformCommission = supplierCommission.multiply(new BigDecimal(this.goodsCategoryRate))
					.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);

			// 用户可分配佣金 X = 供货商佣金 - 平台抽佣
			BigDecimal customerCommission = supplierCommission.subtract(platformCommission);

			log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>have find customerCommission="+customerCommission+",supplierCommission="+supplierCommission);


			orderCommissiones.setTransactionCommission(transactionCommission);
			orderCommissiones.setSupplierCommission(supplierCommission);
			orderCommissiones.setSupplierIncome(supplierIncome);
			orderCommissiones.setPlatformCommission(platformCommission);
			orderCommissiones.setCustomerCommission(customerCommission);
		} else {
			// 无分销
			// 商家所得 = 订单支付金额 - 交易税
			BigDecimal supplierIncome = price.subtract(transactionCommission);
			orderCommissiones.setSupplierIncome(supplierIncome);
			log.info(">>>>>>>>>have find supplierIncome="+supplierIncome);
		}

		log.info(">>>>>>>>>>>>>>>>calculateOrderCommissionsForEachPart and orderCommissiones="+orderCommissiones.toString());
		return orderCommissiones;
	}

	/**
	 * 通过商品分销属性 计算各部分佣金所得
	 *
	 * @param distributionDTO
	 * @return
	 */
	public OrderCommissiones calculateOrderCommissionsForEachPart(ProductDistributionDTO distributionDTO) {
		BigDecimal price = distributionDTO.getPrice();
		BigDecimal selfSupportRatio = distributionDTO.getSelfSupportRatio();
		BigDecimal platSupportRatio = distributionDTO.getPlatSupportRatio();
		ProductDistributionTypeEnum distributionTypeEnum = ProductDistributionTypeEnum.findByProductDistributionDTO(distributionDTO);
		log.info("have find calculateOrderCommissionsForEachPart price="+distributionDTO.getPrice() + ",selfSupportRatio="+selfSupportRatio+",platSupportRatio="+platSupportRatio+",distributionTypeEnum="+distributionTypeEnum);
		return this.calculateOrderCommissionsForEachPart(price, selfSupportRatio, platSupportRatio, distributionTypeEnum);
	}

	/**
	 * 通过历史订单分销属性 计算各部分佣金所得
	 * @param orderDistributionProperty
	 * @return
	 */
	public OrderCommissiones calculateOrderCommissionsForEachPart(OrderDistributionProperty orderDistributionProperty, BigDecimal orderFee) {
		BigDecimal selfSupportRatio = orderDistributionProperty.getSelfSupportRatio();
		BigDecimal platSupportRatio = orderDistributionProperty.getPlatSupportRatio();
		Long productId = orderDistributionProperty.getProductId();
		Long parentId = orderDistributionProperty.getParentId();

		ProductDistributionTypeEnum distributionTypeEnum = ProductDistributionTypeEnum.findByParams(productId, parentId, selfSupportRatio, platSupportRatio);
		return this.calculateOrderCommissionsForEachPart(orderFee, selfSupportRatio, platSupportRatio, distributionTypeEnum);
	}
}
