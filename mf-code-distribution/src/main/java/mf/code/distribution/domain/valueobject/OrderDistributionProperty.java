package mf.code.distribution.domain.valueobject;

import lombok.Data;
import mf.code.distribution.constant.ProductDistributionTypeEnum;

import java.math.BigDecimal;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 19:13
 */
@Data
public class OrderDistributionProperty {
	/**
	 * 商品id
	 */
	private Long productId;
	/**
	 * 父级商品id
	 */
	private Long parentId;
	/**
	 * 商品类目抽佣比例 平台抽佣 默认值 10 %
	 */
	private Integer goodsCategoryRate;
	/**
	 * 交易税率
	 */
	private Integer transactionRate;
	/**
	 * 粉丝店铺分佣
	 */
	private Integer vipShopRate;
	/**
	 * 分销抽佣比率
	 */
	private BigDecimal selfSupportRatio;
	/**
	 * 供应商分佣比率
	 */
	private BigDecimal platSupportRatio;

}
