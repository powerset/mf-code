package mf.code.distribution.domain.valueobject;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.goods.domain
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-04 00:19
 */
@Data
public class UserRebatePolicy {

	/**
	 * 消费达标额度
	 */
	private BigDecimal commissionStandard;
	/**
	 * 商品 返利分佣政策
	 */
	private List<RebateProperty> rebateProperties;

	/**
	 * 根据用户 本月花费金额 获取 返利政策
	 *
	 * @param spendAmount
	 * @return
	 */
	public RebateProperty queryRebatePropertyBySpendAmount(BigDecimal spendAmount) {
		// 选择 商品返利方案
		RebateProperty rebateProperty = rebateProperties.get(0);
		if (spendAmount == null) {
			return rebateProperty;
		}
		for (RebateProperty rp : rebateProperties) {
			if (new BigDecimal(rp.getAmountCondition()).compareTo(spendAmount) > 0) {
				break;
			}
			rebateProperty = rp;
		}
		return rebateProperty;
	}


	/**
	 * 计算用户可分配佣金部分 各成员佣金所得
	 *
	 * @param customerCommission 用户可分配佣金
	 * @param rebateProperty     用户返利政策
	 * @return
	 */
	public UserShopTeamCommission calculateCustomerCommissionsForEachMember(BigDecimal customerCommission, RebateProperty rebateProperty) {
		BigDecimal percent = new BigDecimal(100);

		// 返利佣金
		BigDecimal rebateCommission = customerCommission.multiply(new BigDecimal(rebateProperty.getRebateRate()))
				.divide(percent, 2, BigDecimal.ROUND_DOWN);

		// 每个上级分佣比率
		Integer perCommissionRate = rebateProperty.getCommissionRate() / rebateProperty.getScale();

		// 每个上级分佣金额
		BigDecimal perPubCommission = customerCommission.multiply(new BigDecimal(perCommissionRate))
				.divide(percent, 2, BigDecimal.ROUND_DOWN);

		// 商城团队奖励金额
		BigDecimal teamRewardAmount = customerCommission.multiply(new BigDecimal(rebateProperty.getTeamRewardRate()))
				.divide(percent, 2, BigDecimal.ROUND_DOWN);

		// 用户分佣税务金额 -- 余下部分 26%
		BigDecimal taxAmount = customerCommission.subtract(rebateCommission)
				.subtract(perPubCommission.multiply(new BigDecimal(rebateProperty.getScale())))
				.subtract(teamRewardAmount);

		UserShopTeamCommission userShopTeamCommission = new UserShopTeamCommission();
		userShopTeamCommission.setRebateCommission(rebateCommission);
		userShopTeamCommission.setPerPubCommission(perPubCommission);
		userShopTeamCommission.setTeamRewardAmount(teamRewardAmount);
		userShopTeamCommission.setTaxAmount(taxAmount);

		return userShopTeamCommission;
	}
}
