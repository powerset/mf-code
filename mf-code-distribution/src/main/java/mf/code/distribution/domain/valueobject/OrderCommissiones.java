package mf.code.distribution.domain.valueobject;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-09 11:23
 */
@Data
public class OrderCommissiones {
	/**
	 * 交易税金额
	 */
	private BigDecimal transactionCommission ;
	/**
	 * 平台抽佣金额 -- 平台所得
	 */
	private BigDecimal platformCommission;
	/**
	 * 供货商分佣金额
	 */
	private BigDecimal supplierCommission;
	/**
	 * 供货商所得金额
	 */
	private BigDecimal supplierIncome;
	/**
	 * 分销广场佣金
	 */
	private BigDecimal squareCommission;
	/**
	 * 分销商抽佣 -- 分销商所得
	 * tips: 如果分销商 也是 粉丝店铺 则 分销商所得应为 = distributorCommission + vipShopIncome
	 */
	private BigDecimal distributorCommission;
	/**
	 * 粉丝店铺所得
	 * tips: 如果分销商 也是 粉丝店铺 则 分销商所得应为 = distributorCommission + vipShopIncome
	 */
	private BigDecimal vipShopIncome;
	/**
	 * 用户可分配佣金 -- 用户所得
	 */
	private BigDecimal customerCommission;
}
