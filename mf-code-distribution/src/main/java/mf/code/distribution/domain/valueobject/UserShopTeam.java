package mf.code.distribution.domain.valueobject;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 用户商城团队
 * 唯一标识：userId + shopId 联合标识，即为 当前商城的用户的团队
 * <p>
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 13:24
 */
@Data
public class UserShopTeam {
    /**
     * 团队 所属用户id
     */
    private Long userId;
    /**
     * 团队单个粉丝价值
     */
    private BigDecimal fansValue = new BigDecimal("2.40");
    /**
     * 团队 所属店铺id -- 2.1此处为vipShopId
     */
    private Long vipShopId;
    /**
     * 层级 -- 如果 在一个流程中，层级发生变化，若变小，可复用，若增大，则需要重新获取
     */
    private Integer scale;
    /**
     * 所属上级成员 Map<level, UserId>
     * level = 0 直属上级成员id
     * level = 1 上层第二级成员id
     * level = 2 上层第三级成员id
     * level = 3 上层第四级成员id
     * level = 4 上层第五级成员id
     * <p>
     * level层级数 可由字典表配置
     */
    private Map<Integer, Long> upperMembers = new HashMap<>();
    /**
     * 下级成员 Map<level, List<UserId>>
     * level = 0 下层第一级成员id
     * level = 1 下层第二级成员id
     * level = 2 下层第三级成员id
     * level = 3 下层第四级成员id
     * level = 4 下层第五级成员id
     * <p>
     * level层级数 可由字典表配置
     */
    private Map<Integer, List<Long>> lowerMembers = new HashMap<>();

    /**
     * 获取非直接下级的团队成员ids
     *
     * @return
     */
    public List<Long> queryIndirectLowerMemberIds() {
        ArrayList<Long> inDirectLowerMemberIds = new ArrayList<>();
        if (CollectionUtils.isEmpty(this.lowerMembers)) {
            return inDirectLowerMemberIds;
        }
        for (Map.Entry<Integer, List<Long>> entry : this.lowerMembers.entrySet()) {
            Integer key = entry.getKey();
            if (key == 0) {
                // 排除 直接用户
                continue;
            }
            inDirectLowerMemberIds.addAll(entry.getValue());
        }
        return inDirectLowerMemberIds;
    }

    /**
     * 获取本人下级团队成员中的第一级ids
     *
     * @return
     */
    public List<Long> queryFirstLowerMemberIds() {
        ArrayList<Long> inDirectLowerMemberIds = new ArrayList<>();
        if (CollectionUtils.isEmpty(this.lowerMembers)) {
            return inDirectLowerMemberIds;
        }
        // get(0)直接获取下层第一级成员id
        inDirectLowerMemberIds.addAll(lowerMembers.get(0));
        return inDirectLowerMemberIds;
    }
}
