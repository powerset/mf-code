package mf.code.distribution.domain.valueobject;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 获取本月用户的补贴政策 -- ctime
 *
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 20:19
 */
@Data
@Document(value = "user_subsidy_policy")
public class UserSubsidyPolicy {
	/**
	 * 唯一标识：userId + 获取当前用户补贴政策时间
	 */
	@Id
	@Field("_id")
	private String id;
	/**
	 * 用户id
	 */
	@Field(value = "userId")
	private Long userId;
	/**
	 * 平台补贴订单 备注
	 */
	@Field(value = "subsidyRemark")
	private String subsidyRemark;
	/**
	 * 按配置的比例20% 获取当前作为补贴上级的非直接用户的人数, 非直接下级人数 * 20% <= 0 ，不补贴 --  -- 向下取整
	 */
	@Field(value = "indirectLowerMembersRate")
	private Integer indirectLowerMembersRate;
	/**
	 * 当时的补贴金额
	 */
	@Field(value = "subsidyAmount")
	private BigDecimal subsidyAmount;
	/**
	 * 多少日补贴完成 -- 默认28天
	 */
	@Field(value = "subsidyDays")
	private Integer subsidyDays;
	/**
	 * 配置每单补贴的金额范围 -- 最小金额：约80元商品的分佣金额
	 */
	@Field(value = "minAmount")
	private double minAmount;
	/**
	 * 配置每单补贴的金额范围 -- 最大金额：约120元商品的分佣金额
	 */
	@Field(value = "maxAmount")
	private double maxAmount;
	/**
	 * 已领补贴天数
	 */
	@Field(value = "subsidizedDays")
	private Integer subsidizedDays;
	/**
	 * 待补贴金额集合
	 */
	@Field(value = "subsidyAmountMap")
	private Map<Integer, List<BigDecimal>> subsidyAmountMap;
	/**
	 * 更新时间
	 */
	@Field(value = "utime")
	private Date utime;
	/**
	 * 创建时间
	 */
	@Field(value = "ctime")
	private Date ctime;

	/**
	 * 计算每天 应补贴的单量 及 每单的金额 -- 待补贴金额集合
	 */
	public void calculateOrderSubsidyAmount() {
		if (!CollectionUtils.isEmpty(this.subsidyAmountMap)) {
			return ;
		}
		// 计算 每天待补贴的金额
		List<BigDecimal> dayAmountList = this.calculateDaySubsidyAmountList();

		// ---- 计算每天应补贴的单量 及 每单的金额 ----
		this.subsidyAmountMap = new HashMap<>();
		for (int i = 0; i < dayAmountList.size(); i++) {
			List<BigDecimal> orderAmountList = new ArrayList<>();

			BigDecimal dayAmount = dayAmountList.get(i);

			while (dayAmount.intValue() > maxAmount) {
				// 计算 每单金额
				double randomAmount = Math.random() * (maxAmount - minAmount) + minAmount;
				// 此订单金额
				BigDecimal orderAmount = new BigDecimal(randomAmount).setScale(2, BigDecimal.ROUND_DOWN);
				orderAmountList.add(orderAmount);
				// 剩下金额
				dayAmount = dayAmount.subtract(orderAmount);
			}
			orderAmountList.add(dayAmount);

			this.subsidyAmountMap.put(i, orderAmountList);
		}
	}

	/**
	 * 计算每天 待补贴的金额
	 */
	private List<BigDecimal> calculateDaySubsidyAmountList () {
		List<BigDecimal> dayAmountList = new ArrayList<>();
		if (0 == this.subsidyDays || this.subsidyAmount.compareTo(BigDecimal.ZERO) == 0) {
			return dayAmountList;
		}

		if (1 == this.subsidyDays) {
			dayAmountList.add(this.subsidyAmount);
			return dayAmountList;
		}

		// ----- 计算每天应补贴的金额 -----
		// 后部分天数
		int afterDays = this.subsidyDays / 2;
		// 每天平均金额
		BigDecimal averAmount = this.subsidyAmount.divide(new BigDecimal(this.subsidyDays), 2, RoundingMode.DOWN);
		// 对下一天抽取时，相对上一天金额刻度的 偏移量
		int vi = 1;
		// 抽取金额的随机范围（金额刻度）
		int size = 4;
		// 金额的随机取值范围和偏移的金额刻度之间的重合部分大小（金额刻度）
		int overSize = size - vi;
		// 金额刻度长度为：
		int len = (afterDays - 1) * vi + size;
		// 每天平均金额下再按金额刻度 划分金额
		BigDecimal scaleAmount = averAmount.divide(new BigDecimal(len), 2, RoundingMode.DOWN);

		LinkedList<BigDecimal> amountList = new LinkedList<>();
		// 从中间开始计算随机金额，从小到大 -- 前一半使用rightPush, 后一半使用leftPush
		if (this.subsidyDays % 2 != 0) {
			// 添加中间那一天的金额
			amountList.addFirst(averAmount);
		}
		for (int i = 0; i < afterDays; i++) {
			BigDecimal stepAmount = scaleAmount.multiply(new BigDecimal(Math.random() * size + vi * i));
			stepAmount = stepAmount.setScale(2, RoundingMode.DOWN);
			amountList.addFirst(averAmount.subtract(stepAmount));
			amountList.addLast(averAmount.add(stepAmount));
		}
		// 矫正 资金总额
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (BigDecimal unitAmount : amountList) {
			totalAmount = totalAmount.add(unitAmount);
		}
		BigDecimal leftAmount = this.subsidyAmount.subtract(totalAmount);
		BigDecimal lastAmount = amountList.pollLast();
		if (lastAmount == null) {
			lastAmount = leftAmount;
		} else {
			lastAmount = lastAmount.add(leftAmount);
		}
		amountList.addLast(lastAmount);

		dayAmountList.addAll(amountList);
		return dayAmountList;
	}
}
