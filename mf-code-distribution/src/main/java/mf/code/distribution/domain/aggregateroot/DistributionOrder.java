package mf.code.distribution.domain.aggregateroot;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.domain.valueobject.OrderCommissiones;
import mf.code.distribution.domain.valueobject.OrderDistributionProperty;
import mf.code.distribution.domain.valueobject.ProductDistributionPolicy;
import mf.code.distribution.feignapi.dto.MerchantOrderIncomeResp;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.order.dto.OrderBizResp;
import mf.code.order.dto.OrderResp;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UserResp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 18:58
 */
@Slf4j
@Data
public class DistributionOrder {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 用户信息
	 */
	private UserResp userInfo;
	/**
	 * 购买商品的商城id
	 */
	private Long shopId;
	/**
	 * 订单id
	 */
	private Long orderId;

	/**
	 * 订单信息
	 */
	private OrderResp orderInfo;
	/**
	 * 订单分销业务信息
	 */
	private OrderBizResp orderBizResp;
	/**
	 * 订单分销政策
	 */
	private ProductDistributionPolicy productDistributionPolicy;

	/**
	 * 获取 分销属性
	 * @return
	 */
	public OrderDistributionProperty queryOrderDistributionProperty() {
		if (orderBizResp == null) {
			return null;
		}
		String bizValue = orderBizResp.getBizValue();
		return JSON.parseObject(bizValue, OrderDistributionProperty.class);
	}

	/**
	 * 计算各部分佣金所得
	 *
	 * @return
	 */
	public OrderCommissiones calculateOrderCommissionsForEachPart() {
		OrderDistributionProperty orderDistributionProperty = this.queryOrderDistributionProperty();
		if (orderDistributionProperty == null) {
			log.error("orderBiz 无信息, orderId = {}", this.orderId);
			return null;
		}

		BigDecimal price = this.orderInfo.getFee();
		BigDecimal selfSupportRatio = orderDistributionProperty.getSelfSupportRatio();
		BigDecimal platSupportRatio = orderDistributionProperty.getPlatSupportRatio();
		ProductDistributionTypeEnum distributionTypeEnum = this.getProductDistributionTypeEnum();

		return productDistributionPolicy
				.calculateOrderCommissionsForEachPart(price, selfSupportRatio, platSupportRatio, distributionTypeEnum);
	}

	/**
	 * 计算商户所得 -- 供应商 和 分销商
	 * @return
	 */
	public List<MerchantOrderIncomeResp> calculateMerchantOrderIncome(boolean purchaseInVipShop) {

		OrderCommissiones orderCommissiones = this.calculateOrderCommissionsForEachPart();
		if (orderCommissiones == null) {
			log.error("orderBiz 无信息, orderId = {}", this.orderId);
			return null;
		}

		ProductDistributionTypeEnum distributionTypeEnum = this.getProductDistributionTypeEnum();

		List<MerchantOrderIncomeResp> merchantOrderIncomeRespList = new ArrayList<>();
		// 没有分销 -- 平台情况不会出现，这里做冗余，因为只做平台分销的，最终商品销售出去，则是倒卖类型
		if (distributionTypeEnum == ProductDistributionTypeEnum.NONE
				|| distributionTypeEnum == ProductDistributionTypeEnum.PLATFORM) {
			// 那么 商户所得 = 支付金额 - 交易税

			// 获取商户所得
			BigDecimal merchantIncome = orderCommissiones.getSupplierIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 生成 商户所得利润返回
			MerchantOrderIncomeResp merchantOrderIncomeResp = new MerchantOrderIncomeResp();
			merchantOrderIncomeResp.setShopId(this.orderInfo.getShopId());
			merchantOrderIncomeResp.setTotalFee(merchantIncome);
			merchantOrderIncomeRespList.add(merchantOrderIncomeResp);
			return merchantOrderIncomeRespList;
		}
		// 自卖分销
		if (distributionTypeEnum == ProductDistributionTypeEnum.SELF_SELL
				|| distributionTypeEnum == ProductDistributionTypeEnum.PALTFORM_SELLSELF) {
			// 那么 商户所得 = 支付金额 - 佣金 - 交易税

			// 获取商户所得
			BigDecimal merchantIncome = orderCommissiones.getSupplierIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 生成 商户所得利润返回
			MerchantOrderIncomeResp merchantOrderIncomeResp = new MerchantOrderIncomeResp();
			merchantOrderIncomeResp.setShopId(this.orderInfo.getShopId());
			merchantOrderIncomeResp.setTotalFee(merchantIncome);
			merchantOrderIncomeRespList.add(merchantOrderIncomeResp);
			return merchantOrderIncomeRespList;
		}
		// 倒卖
		if (distributionTypeEnum == ProductDistributionTypeEnum.RESELL) {
			// 那么 供货商所得 = 商品金额 - 佣金 -交易税；分销商所得 = 分销广场佣金 - 抽佣比率

			// 指向供货商
			Long goodsShopId = this.orderInfo.getGoodsShopId();
			// 获取 供货商所得
			BigDecimal supplierIncome = orderCommissiones.getSupplierIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 生成 供货商所得利润返回
			MerchantOrderIncomeResp supplierOrderIncomeResp = new MerchantOrderIncomeResp();

			supplierOrderIncomeResp.setShopId(goodsShopId);
			supplierOrderIncomeResp.setTotalFee(supplierIncome);
			merchantOrderIncomeRespList.add(supplierOrderIncomeResp);

			// 获取分销商所得
			BigDecimal distributorCommission = orderCommissiones.getDistributorCommission()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 获取粉丝店铺所得
			BigDecimal vipShopIncome = orderCommissiones.getVipShopIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			if (purchaseInVipShop) {
				// 用户是在粉丝店铺购买的商品，则 分销商总所得 = 分销商所得 + 粉丝店铺加成所得
				distributorCommission = distributorCommission.add(vipShopIncome);
			} else {
				// 生成 粉丝店铺所得利润返回
				MerchantOrderIncomeResp vipShopIncomeResp = new MerchantOrderIncomeResp();
				vipShopIncomeResp.setShopId(this.userInfo.getVipShopId());
				vipShopIncomeResp.setTotalFee(vipShopIncome);
				merchantOrderIncomeRespList.add(vipShopIncomeResp);
			}

			// 生成 商户所得利润返回
			MerchantOrderIncomeResp merchantOrderIncomeResp = new MerchantOrderIncomeResp();
			merchantOrderIncomeResp.setShopId(this.orderInfo.getShopId());
			merchantOrderIncomeResp.setTotalFee(distributorCommission);
			merchantOrderIncomeRespList.add(merchantOrderIncomeResp);

			return merchantOrderIncomeRespList;
		}

		return null;
	}

	/**
	 * 用户是否是在粉丝店铺购买的商品
	 * @return
	 */
	public boolean isPurchaseInVipShop() {
		return this.userInfo.getVipShopId().equals(this.shopId);
	}

	/**
	 * 获取 订单分销属性
	 * @return
	 */
	public ProductDistributionTypeEnum getProductDistributionTypeEnum() {
		OrderDistributionProperty orderDistributionProperty = this.queryOrderDistributionProperty();
		if (orderDistributionProperty == null) {
			log.error("此订单无分销属性, orderId = {}", this.orderId);
			return null;
		}
		Long productId = orderDistributionProperty.getProductId();
		Long parentId = orderDistributionProperty.getParentId();
		BigDecimal selfSupportRatio = orderDistributionProperty.getSelfSupportRatio();
		BigDecimal platSupportRatio = orderDistributionProperty.getPlatSupportRatio();

		return ProductDistributionTypeEnum.findByParams(productId, parentId, selfSupportRatio, platSupportRatio);
	}

	/**
	 * 用户下单后 分配 用户商城团员分佣所得 -- 下单回调，待结算中
	 *
	 * @return
	 */
	public List<UpayWxOrderReq> orderDistributionCommission(OrderCommissiones orderCommissiones, String mchAppId, Long parentMerchantId, Long vipShopMerchantId, boolean purchaseInVipShop) {

		ProductDistributionTypeEnum distributionTypeEnum = this.getProductDistributionTypeEnum();

		// 初始化各部分订单流水
		List<UpayWxOrderReq> upayWxOrderReqs = new ArrayList<>();

		// 没有分销 -- 平台情况不会出现，这里做冗余，因为只做平台分销的，最终商品销售出去，则是倒卖类型
		if (distributionTypeEnum == null || distributionTypeEnum == ProductDistributionTypeEnum.NONE
				|| distributionTypeEnum == ProductDistributionTypeEnum.PLATFORM) {
			// 那么 商户所得 = 支付金额 - 交易税

			// 获取商户所得
			BigDecimal merchantIncome = orderCommissiones.getSupplierIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 生成 商户订单流水
			UpayWxOrderReq merchantOrder = newUpayWxOrder(this.orderInfo, mchAppId);
			merchantOrder.setTotalFee(merchantIncome);
			merchantOrder.setBizType(UpayWxOrderPendBizTypeEnum.SHOP_INCOME.getCode());
			upayWxOrderReqs.add(merchantOrder);
			return upayWxOrderReqs;
		}
		// 自卖分销
		if (distributionTypeEnum == ProductDistributionTypeEnum.SELF_SELL
				|| distributionTypeEnum == ProductDistributionTypeEnum.PALTFORM_SELLSELF) {
			// 那么 商户所得 = 支付金额 - 佣金 - 交易税

			// 获取商户所得
			BigDecimal merchantIncome = orderCommissiones.getSupplierIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 生成 商户订单流水
			UpayWxOrderReq merchantOrder = newUpayWxOrder(this.orderInfo, mchAppId);
			merchantOrder.setTotalFee(merchantIncome);
			merchantOrder.setBizType(UpayWxOrderPendBizTypeEnum.SHOP_INCOME.getCode());
			upayWxOrderReqs.add(merchantOrder);
			return upayWxOrderReqs;
		}
		// 倒卖
		if (distributionTypeEnum == ProductDistributionTypeEnum.RESELL) {
			// 那么 供货商所得 = 商品金额 - 佣金 -交易税；分销商所得 = 分销广场佣金 - 抽佣比率

			// 生成 供货商订单流水
			UpayWxOrderReq supplierMerchantOrder = newUpayWxOrder(this.orderInfo, mchAppId);
			// 获取 供货商所得
			BigDecimal supplierIncome = orderCommissiones.getSupplierIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);
			// 设置流水金额
			supplierMerchantOrder.setTotalFee(supplierIncome);
			supplierMerchantOrder.setShopId(this.orderInfo.getGoodsShopId());
			supplierMerchantOrder.setMchId(parentMerchantId);
			supplierMerchantOrder.setBizType(UpayWxOrderPendBizTypeEnum.SHOP_INCOME.getCode());
			upayWxOrderReqs.add(supplierMerchantOrder);

			// 获取分销商所得
			BigDecimal distributorCommission = orderCommissiones.getDistributorCommission()
					.setScale(2, BigDecimal.ROUND_DOWN);

			// 获取粉丝店铺所得
			BigDecimal vipShopIncome = orderCommissiones.getVipShopIncome()
					.setScale(2, BigDecimal.ROUND_DOWN);

			if (purchaseInVipShop) {
				// 用户是在粉丝店铺购买的商品，则 分销商总所得 = 分销商所得 + 粉丝店铺加成所得
				distributorCommission = distributorCommission.add(vipShopIncome);
			} else {
				// 生成 粉丝店铺所得利润返回
				UpayWxOrderReq vipShopMerchantOrder = newUpayWxOrder(this.orderInfo, mchAppId);
				vipShopMerchantOrder.setMchId(vipShopMerchantId);
				vipShopMerchantOrder.setShopId(this.userInfo.getVipShopId());
				vipShopMerchantOrder.setBizType(UpayWxOrderPendBizTypeEnum.SHOP_INCOME.getCode());
				vipShopMerchantOrder.setTotalFee(vipShopIncome);
				upayWxOrderReqs.add(vipShopMerchantOrder);
			}

			// 生成 商户订单流水
			UpayWxOrderReq merchantOrder = newUpayWxOrder(this.orderInfo, mchAppId);
			merchantOrder.setTotalFee(distributorCommission);
			merchantOrder.setBizType(UpayWxOrderPendBizTypeEnum.SHOP_INCOME.getCode());
			upayWxOrderReqs.add(merchantOrder);
			return upayWxOrderReqs;
		}
		return null;
	}

	/**
	 * 生成 商户订单流水
	 *
	 * @param order
	 * @return
	 */
	private UpayWxOrderReq newUpayWxOrder(OrderResp order, String mchAppId) {
		Date now = new Date();

		UpayWxOrderReq upayWxOrder = new UpayWxOrderReq();
		upayWxOrder.setMchId(order.getMerchantId());
		upayWxOrder.setShopId(order.getShopId());
		upayWxOrder.setUserId(order.getUserId());
		upayWxOrder.setType(UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
		upayWxOrder.setPayType(UpayWxOrderPayTypeEnum.CASH.getCode());
		upayWxOrder.setOrderNo(order.getOrderNo());
		upayWxOrder.setOrderName(order.getOrderName());
		upayWxOrder.setAppletAppId(mchAppId);
		upayWxOrder.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
		upayWxOrder.setTotalFee(order.getFee());
		upayWxOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
		upayWxOrder.setIpAddress(order.getIpAddress());
		upayWxOrder.setTradeId(order.getTradeId());
		upayWxOrder.setRemark(order.getRemark());
		upayWxOrder.setNotifyTime(now);
		upayWxOrder.setPaymentTime(now);
		upayWxOrder.setCtime(now);
		upayWxOrder.setUtime(now);
		upayWxOrder.setReqJson(order.getReqJson());
		upayWxOrder.setBizType(UpayWxOrderBizTypeEnum.REBATE.getCode());
		upayWxOrder.setBizValue(order.getId());
		upayWxOrder.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
		upayWxOrder.setRefundOrderId(0L);
		return upayWxOrder;
	}
}
