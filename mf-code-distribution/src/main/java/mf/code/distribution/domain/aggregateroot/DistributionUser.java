package mf.code.distribution.domain.aggregateroot;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.DelEnum;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.domain.valueobject.*;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.one.dto.UserCouponDTO;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderBizTypeEnum;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UserIncomeResp;
import mf.code.user.dto.UserResp;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 分销用户，业务概念为：店长
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 08:54
 */
@Slf4j
@Data
public class DistributionUser {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 付费升店长政策
	 */
	private ShopManagerSalePolicy shopManagerSalePolicy;
	/**
	 * 用户信息
	 */
	private UserResp userInfo;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 用户收益信息
	 */
	private UserIncomeResp userIncome;
	/**
	 * 用户消费订单
	 */
	private UserOrderHistory userOrderHistory;
	/**
	 * 用户商城团队 FIXME_OK 2.1迭代 按用户vipShop建立，所以商城团队持久化时，按vipShopId存储， 此时已实现访问其他店铺时，也能暂时商城团队信息
	 */
	private UserShopTeam userShopTeam;
	/**
	 * 用户分佣政策
	 */
	private UserRebatePolicy userRebatePolicy;
	/**
	 * 用户推广收益
	 */
	private UserDistributionHistory userDistributionHistory;
	/**
	 * 用户本月补贴政策
	 */
	private UserSubsidyPolicy userSubsidyPolicy;
	/**
	 * 用户任务收益
	 */
	private UserTaskIncome userTaskIncome;

	/**
	 * 获取 升级成为店长的缴纳金
	 * @return
	 */
	public BigDecimal queryShopManagerSalePrice() {
		if (this.shopManagerSalePolicy == null) {
			return BigDecimal.ZERO;
		}
		return this.shopManagerSalePolicy.getSalePrice();
	}

	/**
	 * 获取 升级成为店长的原价
	 * @return
	 */
	public BigDecimal queryShopManagerOriginalPrice() {
		if (this.shopManagerSalePolicy == null) {
			return new BigDecimal("188");
		}
		return this.shopManagerSalePolicy.getOriginalPrice();
	}

	/**
	 * 是否可以获得上次店长二维码奖励
	 * @return
	 */
	public boolean canGetUploadQRAward() {
		if (CollectionUtils.isEmpty(this.userTaskIncome.getUserTaskMap())) {
			return false;
		}
		Map<Long, UserCouponDTO> userCouponMap = this.userTaskIncome.getUserCouponMap();
		if (CollectionUtils.isEmpty(userCouponMap)) {
			return true;
		}
		for (UserCouponDTO userCoupon : userCouponMap.values()) {
			if (userCoupon.getType() == UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 上传店长二维码
	 * @param personalRQ
	 */
	public void uploadShopManagerQR(String personalRQ) {
		this.userInfo.setWxCode(personalRQ);
	}

	/**
	 * 推荐店长成功
	 */
	public boolean recommendShopManager() {
		if (CollectionUtils.isEmpty(this.userTaskIncome.getUserTaskMap())) {
			return false;
		}
		// 获取 已领取的
		return this.userTaskIncome.queryRecommendShopManagerSize() < 10;
	}

	/**
	 * 成为 店长
	 * @return
	 */
	public boolean becomeShopManager() {
		if (this.hasShopManagerRole()) {
			return false;
		}
		Date now = new Date();

		this.userInfo.setRole(UserRoleEnum.SHOP_MANAGER.getCode());
		this.userInfo.setRoleTime(now);
		this.userInfo.setUtime(now);
		return true;
	}

	/**
	 * 是否有店长角色
	 * @return true 有；false 没有
	 */
	public boolean hasShopManagerRole() {
		return this.userInfo.getRole() == UserRoleEnum.SHOP_MANAGER.getCode();
	}

	/**
	 * 今日收益
	 * 未成为店长 今天产生的 待结算自购省+任务奖金
	 * 成为店长   今日产生的：自购省（待结算）+分享赚（待结算）+任务奖金+任务佣金
	 * @return
	 */
	public BigDecimal queryTodayIncome() {
		// 非店长
		BigDecimal rebatePendToday = this.userDistributionHistory.queryRebatePendToday();
		BigDecimal todayAward = this.userTaskIncome.getTodayAward();
		BigDecimal todayIncome = rebatePendToday.add(todayAward);

		// 店长
		if (this.userInfo.getRole() == UserRoleEnum.SHOP_MANAGER.getCode()) {
			BigDecimal estimateIncomeToday = this.userDistributionHistory.queryEstimateIncomeToday();
			BigDecimal todayCommission = this.userTaskIncome.getTodayCommission();
			todayIncome = todayIncome.add(estimateIncomeToday).add(todayCommission);
		}
		return todayIncome;
	}

	/**
	 * 查询粉丝价值
	 * @return
	 */
	public BigDecimal queryFansValue() {
		return new BigDecimal(this.queryTeamSize() - 1).multiply(this.userShopTeam.getFansValue())
				.setScale(2, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 是否达到补贴金额上限(仅团队贡献部分，自己购物返利不算)
	 * @return true 达到， false 未达到
	 */
	public boolean hasReachMaximumSubsidyAmount() {
		BigDecimal estimateIncomeThisMonth = this.userDistributionHistory.queryEstimateIncome();
		BigDecimal subsidyAmount = this.userSubsidyPolicy.getSubsidyAmount();
		return subsidyAmount.compareTo(estimateIncomeThisMonth) < 0;
	}

	/**
	 * 团队人数是否达到补贴标准
	 * @return
	 */
	public boolean hasReachTeamSubsidyStandard() {
		// 非直接下级人数 * 20% <= 0 ，不补贴 -- 向下取整
		return this.userShopTeam.queryIndirectLowerMemberIds().size() * this.userSubsidyPolicy.getIndirectLowerMembersRate() / 100 != 0;
	}

	/**
	 * 是否已完成所有补贴
	 * @return
	 */
	public boolean hasCompleteSubsidy() {
		return this.userSubsidyPolicy.getSubsidizedDays().equals(this.userSubsidyPolicy.getSubsidyDays());
	}

	/**
	 * 本月预估收益
	 *
	 * @return
	 */
	public BigDecimal queryEstimateIncome() {
		return this.userDistributionHistory.queryEstimateIncome();
	}

	/**
	 * 获取当前直属上级
	 * @return
	 */
	public Long queryUpperId() {
		Map<Integer, Long> upperMembers = this.userShopTeam.getUpperMembers();
		if (!CollectionUtils.isEmpty(upperMembers)) {
			return upperMembers.get(0);
		}
		return null;
	}


	/**
	 * 获取当前店铺下 用户好友收益排行榜 （上级+用户+下一级）
	 *
	 * @return
	 */
	public List<Long> queryFriends() {
		List<Long> friendIds = new ArrayList<>();
		friendIds.add(this.userId);
		Map<Integer, Long> upperMembers = this.userShopTeam.getUpperMembers();
		if (!CollectionUtils.isEmpty(upperMembers)) {
			Long upperId = upperMembers.get(0);
			friendIds.add(upperId);
		}

		Map<Integer, List<Long>> lowerMembers = this.userShopTeam.getLowerMembers();
		if (!CollectionUtils.isEmpty(lowerMembers)) {
			List<Long> lowerIds = lowerMembers.get(0);
			friendIds.addAll(lowerIds);
		}
		return friendIds;
	}

	/**
	 * 获取 上级成员们
	 *
	 * @return
	 */
	public List<Long> queryUpperMembers() {
		Map<Integer, Long> upperMembers = this.userShopTeam.getUpperMembers();
		if (CollectionUtils.isEmpty(upperMembers)) {
			return new ArrayList<>();
		}
		return new ArrayList<>(upperMembers.values());
	}

	/**
	 * 获取 用户商城团员 -- 下5级+自己
	 *
	 * @return
	 */
	public List<Long> queryUserShopTeams() {
		List<Long> userShopTeamIds = new ArrayList<>();
		userShopTeamIds.add(this.userId);

		Map<Integer, List<Long>> lowerMembers = this.userShopTeam.getLowerMembers();
		if (CollectionUtils.isEmpty(lowerMembers)) {
			return userShopTeamIds;
		}
		for (List<Long> ids : lowerMembers.values()) {
			userShopTeamIds.addAll(ids);
		}
		return userShopTeamIds;
	}

	/**
	 * 某时间段的订单 是否达到 消费标准
	 *
	 * @return true 达标， false 不达标
	 */
	public boolean hasReachSpendStandard() {
		BigDecimal spendAmount = this.userOrderHistory.getSpendAmount();
		BigDecimal commissionStandard = this.userRebatePolicy.getCommissionStandard();

		return spendAmount.compareTo(commissionStandard) >= 0;
	}

	/**
	 * 上月订消费金额 是否达到 消费标准额度
	 */
	public boolean hasReachSpendStandardByLastSpendAmount() {
		BigDecimal lastSpendAmount = this.userOrderHistory.getLastSpendAmount();
		BigDecimal commissionStandard = this.userRebatePolicy.getCommissionStandard();
		if (lastSpendAmount == null) {
			return false;
		}
		return lastSpendAmount.compareTo(commissionStandard) >= 0;
	}

	/**
	 * 查询 本月消费 与 本月达标额度 的差值
	 *
	 * @return
	 */
	public BigDecimal queryLeftAmount() {
		BigDecimal spendAmount = this.userOrderHistory.getSpendAmount();
		BigDecimal commissionStandard = this.userRebatePolicy.getCommissionStandard();
		BigDecimal leftAmount = commissionStandard.subtract(spendAmount);
		return leftAmount.compareTo(BigDecimal.ZERO) > 0 ? leftAmount : BigDecimal.ZERO;

	}

	/**
	 * 本段期间内的 花费金额
	 *
	 * @return
	 */
	public BigDecimal querySpendAmount() {
		return this.userOrderHistory.getSpendAmount();
	}

	/**
	 * 本月20日 可结算订单 = 上月 已支付订单 - 售后处理中订单
	 *
	 * @return
	 */
	public List<OrderResp> querySettledOrders() {
		return this.userOrderHistory.querySettledOrders();
	}

	/**
	 * 获取历史订单的分销属性
	 *
	 * @param order
	 */
	public OrderDistributionProperty queryOrderDistributionProperty(OrderResp order) {
		String bizValue = order.getOrderBizBizValue();
		return JSON.parseObject(bizValue, OrderDistributionProperty.class);
	}

	/**
	 * 从推广至现在 累计已结算金额 (返利+贡献收入） -- 2.1.1 按平台维度处理
	 *
	 * @return
	 */
	public BigDecimal querySettledIncome() {
		BigDecimal contributionIncome = this.userIncome.getContributionIncome();
		BigDecimal totalRebateIncome = this.userIncome.getTotalRebateIncome();
		return contributionIncome.add(totalRebateIncome);
	}

	/**
	 * 总的待结算收益 (团队贡献 + 购买返利) 上月+本月
	 *
	 * @return
	 */
	public BigDecimal queryTotalPendingIncome() {
		BigDecimal pendingIncomeLastMonth = this.queryPendingIncomeLastMonth();
		BigDecimal pendingIncomeThisMonth = this.queryPendingIncomeThisMonth();
		return pendingIncomeLastMonth.add(pendingIncomeThisMonth);
	}

	/**
	 * 获取 用户 待结算购买返利 (上月+本月）
	 *
	 * @return
	 */
	public BigDecimal queryPendingRebateIncome() {
		Collection<UpayWxOrderPend> upayWxOrderPends = this.userDistributionHistory.getPendingOrderMap().values();
		BigDecimal pendingIncome = BigDecimal.ZERO;
		// 用户 待结算购物返利
		for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
			if (UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()) {
				continue;
			}
			pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
		}

		return pendingIncome;
	}

	/**
	 * 获取 累计收益
	 * 未成为店长：1.累计收益：已结算自购省+待结算自购省+任务奖金
	 * 成为店长：2.累计收益=自购省（已结算+待结算）+分享赚（已结算+待结算）+任务奖金+任务佣金
	 *
	 * @return
	 */
	public BigDecimal queryTotalIncome() {

		BigDecimal totalIncome = this.querySettledIncome().add(this.queryPendingRebateIncome()).add(this.userTaskIncome.getTotalAward());

		if (this.userInfo.getRole() == UserRoleEnum.SHOP_MANAGER.getCode()) {
			totalIncome = totalIncome.add(this.queryTotalContributionPending()).add(this.userTaskIncome.getTotalCommission());
		}
		return totalIncome;
	}

	/**
	 * 本月待结算收益 （返利 + 分佣，含消费达标逻辑）
	 *
	 * @return
	 */
	public BigDecimal queryPendingIncomeThisMonth() {
		int currentMonth = DateUtil.getMonth(new Date());

		Collection<UpayWxOrderPend> upayWxOrderPends = this.userDistributionHistory.getPendingOrderMap().values();

		BigDecimal pendingIncome = BigDecimal.ZERO;
		// 用户是否达到本月消费额度
		if (this.hasReachSpendStandard()) {
			for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
				int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
				if (month == currentMonth) {
					pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
				}
			}
		} else {
			for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
				if (UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()) {
					continue;
				}
				int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
				if (month == currentMonth) {
					pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
				}
			}
		}

		return pendingIncome;
	}

	/**
	 * 上月待结算收益（返利 + 分佣，含消费达标逻辑）
	 *
	 * @return
	 */
	public BigDecimal queryPendingIncomeLastMonth() {
		int currentMonth = DateUtil.getMonth(new Date());

		Collection<UpayWxOrderPend> upayWxOrderPends = this.userDistributionHistory.getPendingOrderMap().values();

		BigDecimal pendingIncome = BigDecimal.ZERO;
		if (this.hasReachSpendStandardByLastSpendAmount()) {
			for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
				int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
				if (month != currentMonth) {
					pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
				}
			}
		} else {
			for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
				if (UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode() == upayWxOrderPend.getBizType()) {
					continue;
				}
				int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
				if (month != currentMonth) {
					pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
				}
			}
		}

		return pendingIncome;
	}

	/**
	 * 获取 上月 + 本月的 所有待结算分佣收益 (如果上月不达标，则不记录金额）
	 * @return
	 */
	public BigDecimal queryTotalContributionPending() {
		Collection<UpayWxOrderPend> upayWxOrderPends = this.userDistributionHistory.getPendingOrderMap().values();
		BigDecimal pendingIncome = BigDecimal.ZERO;

		int currentMonth = DateUtil.getMonth(new Date());
		boolean reach = this.hasReachSpendStandardByLastSpendAmount();
		// 用户 待结算购物返利
		for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
			if (UpayWxOrderPendBizTypeEnum.REBATE.getCode() == upayWxOrderPend.getBizType()) {
				continue;
			}
			int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
			if (!reach && month != currentMonth) {
				continue;
			}
			pendingIncome = pendingIncome.add(upayWxOrderPend.getTotalFee());
		}

		return pendingIncome;
	}

	/**
	 * 获取 上月 待结算分佣收益
	 * @return
	 */
	public BigDecimal queryContributionPendLastMonth() {
		BigDecimal income = BigDecimal.ZERO;
		int currentMonth = DateUtil.getMonth(new Date());

		Collection<UpayWxOrderPend> upayWxOrderPends = this.userDistributionHistory.getPendingOrderMap().values();
		for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
			if (UpayWxOrderPendBizTypeEnum.REBATE.getCode() == upayWxOrderPend.getBizType()) {
				continue;
			}
			int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
			if (month != currentMonth) {
				income = income.add(upayWxOrderPend.getTotalFee());
			}
		}
		return income;
	}

	/**
	 * 获取 本月 待结算分佣收益
	 * @return
	 */
	public BigDecimal queryContributionPendThisMonth() {
		BigDecimal income = BigDecimal.ZERO;
		int currentMonth = DateUtil.getMonth(new Date());

		Collection<UpayWxOrderPend> upayWxOrderPends = this.userDistributionHistory.getPendingOrderMap().values();
		for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
			if (UpayWxOrderPendBizTypeEnum.REBATE.getCode() == upayWxOrderPend.getBizType()) {
				continue;
			}
			int month = DateUtil.getMonth(upayWxOrderPend.getPaymentTime());
			if (month == currentMonth) {
				income = income.add(upayWxOrderPend.getTotalFee());
			}
		}
		return income;
	}


	/**
	 * 获取 团员人数 （含自己）
	 * @return
	 */
	public int queryTeamSize() {
		return this.queryUserShopTeams().size();
	}

	/**
	 * 付费升店长 缴纳金分配
	 * @return
	 */
	public ShopManagerCommission calculateShopManagerCommissionForEachPart(BigDecimal totalFee) {
		if (this.shopManagerSalePolicy == null) {
			return null;
		}
		Long upperId = this.queryUpperId();
		return this.shopManagerSalePolicy.calculateShopManagerCommissionForEachPart(totalFee, upperId);
	}

	/**
	 * 通过 商品 计算 用户可以获得的返利金额
	 *
	 * @param customerCommission 用户可分配佣金
	 * @return
	 */
	public BigDecimal calculateRebateCommission(BigDecimal customerCommission) {
		// 用户消费金额
		BigDecimal spendAmount = this.querySpendAmount();
		// 当前消费金额下 获取用户返利政策
		RebateProperty rebateProperty = this.userRebatePolicy.queryRebatePropertyBySpendAmount(spendAmount);

		// 返利金额 = X * 返利比率（本月消费是否达标）
		return customerCommission.multiply(new BigDecimal(rebateProperty.getRebateRate()))
				.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 用户下单后 分配 用户商城团员分佣所得 -- 下单回调，待结算中
	 * 必须先 存储订单 -- 后计算各成员佣金所得
	 *
	 * @return
	 */
	public List<UpayWxOrderReq> orderDistributionCommission(List<UserShopTeamCommission> userShopTeamCommissionList, Long orderId, String mfAppId) {
		// 获取订单详情
		OrderResp order = this.userOrderHistory.queryPaidOrderById(orderId);
		if (order == null) {
			log.error("DistributionUser.orderDistributionCommission()，查无此订单, orderId = {}", orderId);
			return null;
		}
		if (CollectionUtils.isEmpty(userShopTeamCommissionList)) {
			log.info("非分销订单, orderId = {}", order.getId());
			return null;
		}

		List<UpayWxOrderReq> upayWxOrderList = new ArrayList<>();
		for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
			// 生成 返利 用户订单流水
			BigDecimal rebateCommission = userShopTeamCommission.getRebateCommission();
			// 生成 用户订单流水
			UpayWxOrderReq rebateUpayWxOrder = newUpayWxOrder(order, mfAppId);
			// 设置流水金额
			rebateUpayWxOrder.setTotalFee(rebateCommission);

			if (userShopTeamCommission.isSplit()) {

				// 如果为拆分订单
				if (userShopTeamCommission.isReach()) {
					// 重写remark
					rebateUpayWxOrder.setRemark("拆分_达标部分");
				} else {
					// 重写remark
					rebateUpayWxOrder.setRemark("拆分_未达标部分");
				}
			}
			upayWxOrderList.add(rebateUpayWxOrder);

			// 对所有上级成员 生成 分佣用户订单流水
			BigDecimal perPubCommission = userShopTeamCommission.getPerPubCommission();
			Map<Integer, Long> upperMembers = this.userShopTeam.getUpperMembers();
			if (CollectionUtils.isEmpty(upperMembers)) {
				log.info("用户没有上级, shopId = {}, userId = {}", this.shopId, this.userId);
				continue;
			}
			for (Long upperId : upperMembers.values()) {
				// 创建上级用户团队分佣订单流水, 并暂存redis中
				UpayWxOrderReq upayWxOrder = newUpayWxOrder(order, mfAppId);
				// 设置订单所有者
				upayWxOrder.setUserId(upperId);
				// 设置流水金额
				upayWxOrder.setTotalFee(perPubCommission);
				// 重设类型
				upayWxOrder.setBizType(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());
				// 重设收入来源
				upayWxOrder.setBizValue(this.userId);

				if (userShopTeamCommission.isSplit()) {
					// 如果为拆分订单
					if (userShopTeamCommission.isReach()) {
						// 重写remark
						rebateUpayWxOrder.setRemark("拆分_达标部分");
					} else {
						// 重写remark
						rebateUpayWxOrder.setRemark("拆分_未达标部分");
					}
				}
				upayWxOrderList.add(upayWxOrder);
			}
		}

		return upayWxOrderList;

	}

	/**
	 * 每月20日 结算, 首先添加订单
	 * 与用户下单计算佣金 相契合
	 * @param order
	 */
	public void addOrder(OrderResp order) {
		// 与用户下单计算佣金 相契合
		List<OrderResp> paidOrder = this.userOrderHistory.getPaidOrder();
		paidOrder.add(order);
		BigDecimal orderFee = order.getFee();
		this.userOrderHistory.addSpendAmount(orderFee);
	}

	/**
	 * 每月20日 结算 用户订单 -- 结算用户分销佣金
	 * <p>
	 * 必须先 存储订单 -- 后计算各成员佣金所得
	 *
	 * @param order
	 */
	public List<UpayWxOrderReq> settleDistributionCommission(List<UserShopTeamCommission> userShopTeamCommissionList, OrderResp order, List<Long> reachUpperIds, String mchAppId) {
		List<UpayWxOrderReq> upayWxOrderList = new ArrayList<>();
		for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
			// 生成 返利 用户订单流水
			BigDecimal rebateCommission = userShopTeamCommission.getRebateCommission();
			// 生成 未达标部分 用户订单流水
			UpayWxOrderReq rebateUpayWxOrder = newUpayWxOrder(order, mchAppId);
			// 设置流水金额
			rebateUpayWxOrder.setTotalFee(rebateCommission);

			if (userShopTeamCommission.isSplit()) {
				// 重写remark
				rebateUpayWxOrder.setRemark("拆分");
				// 如果为拆分订单
				if (userShopTeamCommission.isReach()) {
					// 重写order_no + _ + userId
					rebateUpayWxOrder.setOrderNo(order.getOrderNo() + "_" + this.userId + "_达标");
				} else {
					// 重写order_no + _ + userId
					rebateUpayWxOrder.setOrderNo(order.getOrderNo() + "_" + this.userId + "_未达标");
				}
			}
			rebateUpayWxOrder.setOrderNo(rebateUpayWxOrder.getOrderNo() + "_返利分佣结算");
			upayWxOrderList.add(rebateUpayWxOrder);

			// 对达标的上级成员 生成 分佣用户订单流水
			BigDecimal perPubCommission = userShopTeamCommission.getPerPubCommission();
			for (Long upperId : reachUpperIds) {
				// 创建上级用户团队分佣订单流水, 并暂存redis中
				UpayWxOrderReq upayWxOrder = newUpayWxOrder(order, mchAppId);
				// 设置订单所有者
				upayWxOrder.setUserId(upperId);
				// 设置流水金额
				upayWxOrder.setTotalFee(perPubCommission);
				// 重设类型
				upayWxOrder.setBizType(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());
				// 重设收入来源
				upayWxOrder.setBizValue(this.userId);

				if (userShopTeamCommission.isSplit()) {
					// 重写remark
					upayWxOrder.setRemark("拆分");
					// 如果为拆分订单
					if (userShopTeamCommission.isReach()) {
						// 重写order_no + _ + userId
						upayWxOrder.setOrderNo(order.getOrderNo() + "_" + upperId + "_达标");
					} else {
						// 重写order_no + _ + userId
						upayWxOrder.setOrderNo(order.getOrderNo() + "_" + upperId + "_未达标");
					}
				}
				rebateUpayWxOrder.setOrderNo(rebateUpayWxOrder.getOrderNo() + "_返利分佣结算");
				upayWxOrderList.add(upayWxOrder);
			}
		}

		return upayWxOrderList;
	}

	/**
	 * 按历史分销订单 计算各成员佣金所得
	 * <p>
	 * 必须先 存储订单 -- 后计算各成员佣金所得
	 *
	 * @param customerCommission
	 * @param orderFee
	 */
	public List<UserShopTeamCommission> calculateCustomerCommissionsForEachMember(BigDecimal customerCommission, BigDecimal orderFee) {

		// 获取 用户 已累计消费金额
		BigDecimal currentSpendAmount = this.querySpendAmount();
		// 已累计消费金额 的返利政策
		RebateProperty currentRebateProperty = this.userRebatePolicy.queryRebatePropertyBySpendAmount(currentSpendAmount);

		// 计算 此订单前的消费金额
		BigDecimal lastSpendAmount = currentSpendAmount.subtract(orderFee);
		// 获取当前累加后的 返利政策
		RebateProperty lastRebateProperty = this.userRebatePolicy.queryRebatePropertyBySpendAmount(lastSpendAmount);

		// 如果上一次的返利政策 和 这一次的返利政策 一样
		if (lastRebateProperty.getAmountCondition().equals(currentRebateProperty.getAmountCondition())) {

			UserShopTeamCommission userShopTeamCommission = this.userRebatePolicy.calculateCustomerCommissionsForEachMember(customerCommission, currentRebateProperty);

			List<UserShopTeamCommission> userShopTeamCommissionList = new ArrayList<>();
			userShopTeamCommissionList.add(userShopTeamCommission);
			return userShopTeamCommissionList;

		}

		// 不一样 那么 进行拆分金额

		// 消费 达标差额 -- 此部分金额 按上一次返利分佣政策 计算
		BigDecimal leftAmount = new BigDecimal(currentRebateProperty.getAmountCondition()).subtract(lastSpendAmount);
		// 消费 达标超额 -- 此部分金额 按本次返利分佣政策 计算
		BigDecimal superAmont = orderFee.subtract(leftAmount);
		// 消费 差额和超额比 == 用户可分配佣金 一类返利佣金和二类返利佣金比

		// 消费 超额部分的 用户可分配佣金 = 总可分配佣金 * （超额金额/总消费金额）
		BigDecimal superCommission = customerCommission.multiply(superAmont.divide(orderFee, 2, BigDecimal.ROUND_DOWN))
				.setScale(2, BigDecimal.ROUND_DOWN);
		// 消费 差额部分的 用户可分配金额 = 用户总的可分配佣金 - 超额部分用户可分配佣金
		BigDecimal leftCommission = customerCommission.subtract(superCommission);

		UserShopTeamCommission leftUserTeamCommission = this.userRebatePolicy.calculateCustomerCommissionsForEachMember(leftCommission, lastRebateProperty);
		leftUserTeamCommission.setSplit(true);
		leftUserTeamCommission.setReach(false);

		UserShopTeamCommission superUserTeamCommission = this.userRebatePolicy.calculateCustomerCommissionsForEachMember(superCommission, currentRebateProperty);
		superUserTeamCommission.setSplit(true);
		superUserTeamCommission.setReach(true);

		List<UserShopTeamCommission> userShopTeamCommissionList = new ArrayList<>();
		userShopTeamCommissionList.add(leftUserTeamCommission);
		userShopTeamCommissionList.add(superUserTeamCommission);

		return userShopTeamCommissionList;
	}

	/**
	 * 生成 用户订单流水
	 *
	 * @param order
	 * @return
	 */
	private UpayWxOrderReq newUpayWxOrder(OrderResp order, String mfAppId) {
		Date now = new Date();

		UpayWxOrderReq upayWxOrder = new UpayWxOrderReq();
		upayWxOrder.setMchId(order.getMerchantId());
		upayWxOrder.setShopId(order.getShopId());
		upayWxOrder.setUserId(order.getUserId());
		upayWxOrder.setType(UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
		upayWxOrder.setPayType(UpayWxOrderPayTypeEnum.CASH.getCode());
		upayWxOrder.setOrderNo(order.getOrderNo());
		upayWxOrder.setOrderName(order.getOrderName());
		upayWxOrder.setAppletAppId(mfAppId);
		upayWxOrder.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
		upayWxOrder.setTotalFee(order.getFee());
		upayWxOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
		upayWxOrder.setIpAddress(order.getIpAddress());
		upayWxOrder.setTradeId(order.getTradeId());
		upayWxOrder.setRemark(order.getRemark());
		upayWxOrder.setNotifyTime(now);
		upayWxOrder.setPaymentTime(now);
		upayWxOrder.setCtime(now);
		upayWxOrder.setUtime(now);
		upayWxOrder.setReqJson(order.getReqJson());
		upayWxOrder.setBizType(UpayWxOrderBizTypeEnum.REBATE.getCode());
		upayWxOrder.setBizValue(order.getId());
		upayWxOrder.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
		upayWxOrder.setRefundOrderId(0L);
		return upayWxOrder;
	}

	/**
	 * 生成补贴订单 --  做好 此用户今日 是否已补贴 -- 通过 订单编号 的唯一性 校验，进行防重
	 * @return
	 */
	public List<OrderResp> createSubsidyOrderList() {
		// 获取用户当前预估收益
		BigDecimal estimateIncome = this.userDistributionHistory.queryEstimateIncome();
		BigDecimal subsidyAmount = this.userSubsidyPolicy.getSubsidyAmount();

		List<OrderResp> orderRespList = new ArrayList<>();

		// 获取所有非直接下级用户的ids
		List<Long> indirectLowerMemberIds = this.userShopTeam.queryIndirectLowerMemberIds();
		// 获取用户 已领补贴天数 0天， 1天
		Integer subsidizedDays = this.userSubsidyPolicy.getSubsidizedDays();
		// 领取 当前天数下 待补贴订单数 及其 对应金额
		List<BigDecimal> orderAmountList = this.userSubsidyPolicy.getSubsidyAmountMap().get(subsidizedDays);
		if (CollectionUtils.isEmpty(orderAmountList)) {
			return new ArrayList<>();
		}

		Date now = new Date();
		for (int i = 0; i < orderAmountList.size(); i++) {
			// 随机获取为本用户补贴订单 的 非直接下级用户
			int randomIndex = (int)(Math.random() * indirectLowerMemberIds.size());
			Long userId = indirectLowerMemberIds.get(randomIndex);
			// 获取订单补贴金额
			BigDecimal orderAmount = orderAmountList.get(i);

			OrderResp orderResp = new OrderResp();
			// 实现订单数据库 唯一索引 防重 ，故定义 订单编号组成为： 补贴订单_2019-6-4_补贴订单序号
			String orderNo = "补贴订单_" + DateUtil.dateToString(now, DateUtil.LONG_DATE_FORMAT) + "_" + i + "_" + this.userId;
			// String orderNo = "补贴订单_" + DateUtil.dateToString(now, DateUtil.FORMAT_TOKEN) + "_" + i + "_" + this.userId;
			orderResp.setOrderNo(orderNo);
			orderResp.setOrderName("平台补贴订单");
			orderResp.setAddressId(0L);
			orderResp.setLogistics("订单物流对象");
			orderResp.setType(OrderTypeEnum.PLATFORM_SUBSIDY.getCode());
			orderResp.setPayChannel(OrderPayChannelEnum.CASH.getCode());
			orderResp.setStatus(OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
			orderResp.setOriginalfee(orderAmount);
			orderResp.setFee(orderAmount);
			orderResp.setNumber(1);
			orderResp.setTradeId("流水号 第三方流水号");
			orderResp.setRemark(this.userSubsidyPolicy.getSubsidyRemark());
			orderResp.setMerchantId(0L);
			orderResp.setShopId(this.userInfo.getVipShopId());
			orderResp.setUserId(userId);
			orderResp.setGoodsShopId(0L);
			orderResp.setGoodsId(0L);
			orderResp.setSkuId(0L);
			orderResp.setReqJson("订单入参，json对象");
			orderResp.setRespJson("订单入参，json对象");
			orderResp.setIpAddress("ip地址");
			orderResp.setNotifyTime(now);
			orderResp.setPaymentTime(now);
			orderResp.setSendTime(now);
			orderResp.setDealTime(now);
			orderResp.setCtime(now);
			orderResp.setUtime(now);
			orderResp.setBizType(OrderBizTypeEnum.SUBSIDY_USER.getCode());
			orderResp.setBizValue(this.userId);
			orderResp.setRefundOrderId(0L);
			orderResp.setBuyerMsg("买家留言");
			orderResp.setSellerMsg("卖家留言");
			orderResp.setDel(DelEnum.NO.getCode());

			orderRespList.add(orderResp);

			// 校验是否 达标 -- 暂不校验
			estimateIncome = estimateIncome.add(orderAmount);
			if (subsidyAmount.compareTo(estimateIncome) <= 0) {
				break;
			}
		}

		this.userSubsidyPolicy.setSubsidizedDays(subsidizedDays + 1);

		return orderRespList;
	}

	/**
	 * 生成 平台补贴待结算订单
	 * @param subsidyOrderList
	 * @param mfAppId
	 * @return
	 */
	public List<UpayWxOrderReq> createSubsidyUpayWxOrderReq(List<OrderResp> subsidyOrderList, String mfAppId) {
		List<UpayWxOrderReq> subsidyPend = new ArrayList<>();
		for (OrderResp order : subsidyOrderList) {
			// 生成 用户订单流水
			UpayWxOrderReq upayWxOrderReq = newUpayWxOrder(order, mfAppId);
			upayWxOrderReq.setUserId(order.getBizValue());
			upayWxOrderReq.setBizType(UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode());
			upayWxOrderReq.setBizValue(order.getUserId());
			subsidyPend.add(upayWxOrderReq);
		}
		return subsidyPend;
	}

	/**
	 * 取消 平台补贴待结算订单
	 * @return
	 */
	public List<UpayWxOrderPend> queryCancelSubsidyPendList() {
		if (!this.hasReachMaximumSubsidyAmount()) {
			// 未达到 最高 补贴金额 -- 不取消 平台补贴订单
			return null;
		}

		List<UpayWxOrderPend> subsidyPends =  this.userDistributionHistory.querySubsidyPend(this.userSubsidyPolicy.getSubsidyRemark());
		if (CollectionUtils.isEmpty(subsidyPends)) {
			// 没有 平台补贴订单
			return null;
		}

		// 获取超标金额
		BigDecimal estimateIncomeThisMonth = this.userDistributionHistory.queryEstimateIncome();
		BigDecimal subsidyAmount = this.userSubsidyPolicy.getSubsidyAmount();
		BigDecimal superAmount = estimateIncomeThisMonth.subtract(subsidyAmount);
		// 获取取消的订单信息
		List<UpayWxOrderPend> cancelSubsidyPendList = new ArrayList<>();
		Date now = new Date();

		for (UpayWxOrderPend upayWxOrderPend : subsidyPends) {
			// 当取消此平台补贴订单后，用户预估收益将小于平台补贴标准，则此订单不取消
			BigDecimal totalFee = upayWxOrderPend.getTotalFee();
			superAmount = superAmount.subtract(totalFee);
			if (superAmount.compareTo(BigDecimal.ZERO) < 0) {
				break;
			}

			upayWxOrderPend.setStatus(UpayWxOrderPendStatusEnum.FAILED.getCode());
			upayWxOrderPend.setUtime(now);
			cancelSubsidyPendList.add(upayWxOrderPend);
		}

		return cancelSubsidyPendList;
	}

}
