package mf.code.distribution.domain.valueobject;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 23:06
 */
@Data
public class ShopManagerSalePolicy {
    private BigDecimal originalPrice;
    private BigDecimal salePrice;
    private List<Map<String, Integer>> policyList;


    public ShopManagerCommission calculateShopManagerCommissionForEachPart() {
        return this.calculateShopManagerCommissionForEachPart(salePrice, -1L);
    }

    /**
     * 计算 各部分 店长缴纳金 所能获得的收益
     * @param amount
     * @param upperId
     * @return
     */
    public ShopManagerCommission calculateShopManagerCommissionForEachPart(BigDecimal amount, Long upperId) {
        if (salePrice.compareTo(BigDecimal.ZERO) == 0 || amount.compareTo(BigDecimal.ZERO) == 0) {
            return null;
        }
        Map<String, Integer> policy0 = new HashMap<>();
        Map<String, Integer> policy1 = new HashMap<>();
        for (Map<String, Integer> map : policyList){
            Integer hasSuperior = map.get("hasSuperior");
            if (hasSuperior == 0) {
                policy0 = map;
            }
            if (hasSuperior == 1) {
                policy1 = map;
            }
        }
        Map<String, Integer> policy;
        if (upperId == null) {
            policy = policy0;
        } else {
            policy = policy1;
        }
        if (CollectionUtils.isEmpty(policy)) {
            return null;
        }

        Integer hasSuperior = policy.get("hasSuperior");
        Integer superiorRate = policy.get("superiorRate");
        Integer platformRate = policy.get("platformRate");
        Integer shopRate = policy.get("shopRate");

        BigDecimal percent = new BigDecimal("100");
        if (hasSuperior == 0) {
            BigDecimal shopCommission = amount.multiply(new BigDecimal(shopRate))
                    .divide(percent, 2, BigDecimal.ROUND_DOWN);
            BigDecimal platformCommission = amount.subtract(shopCommission);

            ShopManagerCommission shopManagerCommission = new ShopManagerCommission();
            shopManagerCommission.setShopCommission(shopCommission);
            shopManagerCommission.setPlatformCommission(platformCommission);
            return shopManagerCommission;
        } else {
            BigDecimal shopCommission = amount.multiply(new BigDecimal(shopRate))
                    .divide(percent, 2, BigDecimal.ROUND_DOWN);
            BigDecimal superiorCommission = amount.multiply(new BigDecimal(superiorRate))
                    .divide(percent, 2, BigDecimal.ROUND_DOWN);
            BigDecimal platformCommission = amount.subtract(shopCommission).subtract(superiorCommission);
            ShopManagerCommission shopManagerCommission = new ShopManagerCommission();
            shopManagerCommission.setSuperiorCommission(superiorCommission);
            shopManagerCommission.setShopCommission(shopCommission);
            shopManagerCommission.setPlatformCommission(platformCommission);
            return shopManagerCommission;
        }
    }
}
