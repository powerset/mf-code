package mf.code.distribution.model.request.platform;

import lombok.Data;
import lombok.NonNull;

/**
 * mf.code.distribution.model.request.platform
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 16:15
 */
@Data
public class ShopManagerSalePolicyRequest {
    /**
     * 原价
     */
    private String originalPrice;
    /**
     * 缴纳金
     */
    private String salePrice;
    /**
     * 无上级的店铺分配比例
     */
    private String shopRate0;
    /**
     * 无上级的平台分配比例
     */
    private String platformRate0;
    /**
     * 有上级的上级分配比例
     */
    private String superiorRate1;
    /**
     * 有上级的店铺分配比例
     */
    private String shopRate1;
    /**
     * 有上级的平台分配比例
     */
    private String platformRate1;
}
