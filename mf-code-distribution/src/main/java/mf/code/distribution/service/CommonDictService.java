package mf.code.distribution.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.distribution.domain.valueobject.OrderDistributionProperty;
import mf.code.distribution.repo.po.CommonDict;

import java.util.List;
import java.util.Map;

/**
 * mf.code.common.service
 * Description: 字典表
 * -------------------------------------------------
 *  注意：数据字典表这里的接口不能随便添加
 * -------------------------------------------------
 * @author: gel
 * @date: 2018-11-21 11:14
 */
public interface CommonDictService extends IService<CommonDict> {

    /**
     * 创建
     *
     * @param commonDict
     * @return
     */
    Integer create(CommonDict commonDict);

    /**
     * 批量创建
     *
     * @param commonDictList
     * @return
     */
    Integer createList(List<CommonDict> commonDictList);

    /**
     * 更新
     *
     * @param commonDict
     * @return
     */
    Integer update(CommonDict commonDict);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    Integer delete(Long id);

    /**
     * 主键查询
     *
     * @param id
     * @return
     */
    CommonDict select(Long id);


    /**
     * 按照类型键值查询
     *
     * @return
     */
    CommonDict selectByTypeKey(String type, String key);

    /**
     * 按照类型查询
     *
     * @return
     */
    List<CommonDict> selectListByType(String type);

    /**
     * 查询列表
     *
     * @param params
     * @return
     */
    List<CommonDict> selectList(Map<String, Object> params);

    /**
     * 查询列表
     * @return
     */
    String selectValueByTypeKey(String type, String key);

    Boolean refreshRedisData(String type, String key);

    /**
     * 通过商品类目 获取 订单分销属性
     * @param productSpecs
     * @return
     */
	OrderDistributionProperty queryOrderDistributionProperty(Long productSpecs);
}
