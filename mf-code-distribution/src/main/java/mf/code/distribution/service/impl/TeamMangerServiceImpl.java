package mf.code.distribution.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.feignclient.GoodsAppService;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.dto.TeamMangerEventDTO;
import mf.code.distribution.dto.TeamMangerEventEntity;
import mf.code.distribution.dto.VisitUserInSevenDaysInfoDTO;
import mf.code.distribution.repo.mongo.po.FanAction;
import mf.code.distribution.repo.mongo.repository.FanActionRepository;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.TeamMangerService;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 团队管理服务类
 *
 * @author gel & 云山
 */
@Service
@Slf4j
public class TeamMangerServiceImpl implements TeamMangerService {
    @Autowired
    private FanActionRepository fanActionRepository;
    @Autowired(required = false)
    private GoodsAppService goodsAppService;
    @Autowired
    private DistributionUserRepository distributionUserRepository;
    @Autowired(required = false)
    private UserAppService userAppService;
    @Autowired(required = false)
    private OrderAppService orderAppService;

    /**
     * 查询粉丝用户七日访客
     *
     * @param shopId 店铺id
     * @param userId 用户id
     * @param offset 偏移量
     * @param limit  查询条数
     * @return 返回
     */
    @Override
    public SimpleResponse visitorsInSevenDays(Long shopId, Long userId, Long offset, Long limit) {
        TeamMangerEventEntity entry = new TeamMangerEventEntity();
        entry.setTeamList(new ArrayList<>());
        entry.setIsReachMark(false);
        entry.setCount(0L);
        //获取团队下的队员信息
        List<Long> userIdList = getTeamInfoByUid(shopId, userId);
        if (CollectionUtils.isEmpty(userIdList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, entry);
        }
        // fanAction查询条件
        final List<String> typeList = Arrays.asList(FanActionEventEnum.VIEW_GOODS.getCode());
        List<FanAction> fanActionList = fanActionRepository.pageListUidListGroupByUid(shopId, userIdList, typeList, offset, limit);
        if (CollectionUtils.isEmpty(fanActionList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, entry);
        }

        List<VisitUserInSevenDaysInfoDTO> vList = new ArrayList<>();

        for (FanAction fanAction : fanActionList) {
            VisitUserInSevenDaysInfoDTO vdto = new VisitUserInSevenDaysInfoDTO();
            // 查询下级用户查阅的商品信息
            List<FanAction> fanList = fanActionRepository.pageListForTeamManagerSevenDay(shopId,
                    Arrays.asList(fanAction.getUid()), typeList, 0L, 6);
            if (CollectionUtils.isEmpty(fanList)) {
                continue;
            }
            Long intervalTime = 0L;
            //根据用户ID获取用户的图片和姓名
            UserResp userInfo = userAppService.queryUser(fanAction.getUid());
            if (null == userInfo) {
                continue;
            }
            String userName = userInfo.getNickName();
            String userPic = userInfo.getAvatarUrl();
            List<TeamMangerEventDTO> teamMangerEventDTOList = new ArrayList<>();
            for (FanAction fa : fanList) {
                //获取产品信息
                GoodProductDTO goodProductDTO = goodsAppService.queryProduct(fa.getMid(), fa.getSid(), fa.getEventId());
                if (goodProductDTO == null) {
                    continue;
                }
                TeamMangerEventDTO teamMangerEventDTO = new TeamMangerEventDTO();
                teamMangerEventDTO.setMerchantId(goodProductDTO.getMerchantId());
                teamMangerEventDTO.setShopId(goodProductDTO.getShopId());
                teamMangerEventDTO.setProductId(goodProductDTO.getGoodsId());
                teamMangerEventDTO.setProductTitle(goodProductDTO.getTitle());
                teamMangerEventDTO.setSalsePrice(goodProductDTO.getPrice());
                teamMangerEventDTO.setThirdPrice(goodProductDTO.getOriginalPrice());
                List<String> picList = goodProductDTO.getGoodsPics();
                if (!CollectionUtils.isEmpty(picList)) {
                    teamMangerEventDTO.setMainPics(picList.get(0));
                }
                teamMangerEventDTO.setSalseNum(goodProductDTO.getSaleQuantity());
                if (fa.getCurrent() > intervalTime) {
                    intervalTime = fa.getCurrent();
                }
                teamMangerEventDTO.setUserId(fa.getUid().toString());
                teamMangerEventDTO.setUserName(userName);
                teamMangerEventDTO.setUserPics(userPic);
                teamMangerEventDTOList.add(teamMangerEventDTO);
            }
            if (CollectionUtils.isEmpty(teamMangerEventDTOList)) {
                continue;
            }
            vdto.setUserId(fanAction.getUid());
            vdto.setIntervalTime(getIntervalTime(intervalTime));
            vdto.setVisitInSevenDays(teamMangerEventDTOList);
            vList.add(vdto);
        }
        long count = fanActionRepository.countUidForTeamManagerSevenDay(shopId, userIdList, typeList);
        entry.setCount(count);
        entry.setTeamList(vList);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, entry);
    }


    /**
     * 七日订单
     */
    @Override
    public SimpleResponse newOrdersInSevenDays(Long shopId, Long userId, Long offset, Long limit) {
        // 先初始化数据，方便查询不到返回
        Boolean isReachMark = isReachMark(userId);
        TeamMangerEventEntity teamMangerEventEntity = new TeamMangerEventEntity();
        teamMangerEventEntity.setInSevenDaysList(new ArrayList<>());
        teamMangerEventEntity.setCount(0L);
        teamMangerEventEntity.setIsReachMark(isReachMark);
        teamMangerEventEntity.setTotalIncome(new BigDecimal("0"));

        //获取团队下的队员信息
        List<Long> userIdList = getTeamInfoByUid(shopId, userId);

        if (CollectionUtils.isEmpty(userIdList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, teamMangerEventEntity);
        }
        // fanAction中type查询条件
        final List<String> typeList = Arrays.asList(FanActionEventEnum.ORDER.getCode(), FanActionEventEnum.PAY.getCode());
        // 查询七天内的新订单
        List<FanAction> fanActionList = fanActionRepository.pageListForTeamManagerSevenDay(shopId, userIdList,
                typeList, offset, limit.intValue());
        if (CollectionUtils.isEmpty(fanActionList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, teamMangerEventEntity);
        }
        List<TeamMangerEventDTO> teamMangerEventDTOList = new ArrayList<>();
        // 直接使用amount和tax字段
        for (FanAction fanAction : fanActionList) {
            TeamMangerEventDTO teamMangerEventDTO = new TeamMangerEventDTO();
            teamMangerEventDTO.setUserId(fanAction.getUid().toString());
            UserResp userResp = userAppService.queryUser(fanAction.getUid());
            teamMangerEventDTO.setUserName(userResp.getNickName());
            teamMangerEventDTO.setUserPics(userResp.getAvatarUrl());
            teamMangerEventDTO.setIntervalTime(getIntervalTime(fanAction.getCurrent()));
            teamMangerEventDTO.setSalsePrice(fanAction.getAmount().toString());
            if (teamMangerEventDTO.getSalsePrice() == null) {
                teamMangerEventDTO.setIncome(BigDecimal.ZERO);
            }
            teamMangerEventDTO.setIncome(fanAction.getTax());
            if (teamMangerEventDTO.getIncome() == null) {
                teamMangerEventDTO.setIncome(BigDecimal.ZERO);
            }
            teamMangerEventDTO.setOrderId(fanAction.getEventId());
            // 订单状态,0订单创建。1订单支付。订单取消返回
            OrderResp orderResp = orderAppService.queryOrder(fanAction.getEventId());
            if (orderResp != null) {
                if (orderResp.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
                    teamMangerEventDTO.setOrderStatus(0);
                } else if (orderResp.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()
                        || orderResp.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()
                        || orderResp.getStatus() == OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()) {
                    teamMangerEventDTO.setOrderStatus(1);
                } else {
                    continue;
                }
            }
            teamMangerEventDTOList.add(teamMangerEventDTO);
        }
        if (CollectionUtils.isEmpty(teamMangerEventDTOList) || teamMangerEventDTOList.size() < 1) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, teamMangerEventEntity);
        }
        // 总条数
        long count = fanActionRepository.countEventIdForTeamManagerSevenDay(shopId, userIdList, typeList);
        BigDecimal totalIncome = fanActionRepository.sumTaxForTeamManagerSevenDay(shopId, userIdList, Arrays.asList(FanActionEventEnum.PAY.getCode()));
        teamMangerEventEntity.setInSevenDaysList(teamMangerEventDTOList);
        teamMangerEventEntity.setCount(count);
        teamMangerEventEntity.setIsReachMark(isReachMark);
        teamMangerEventEntity.setTotalIncome(totalIncome);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, teamMangerEventEntity);
    }


    /**
     * 7日完成任务
     */
    @Override
    public SimpleResponse finishTasksInSevenDays(Long shopId, Long userId, Long offset, int limit) {

        // 返回结果集，初始化返回字段
        TeamMangerEventEntity tEntity = new TeamMangerEventEntity();
        tEntity.setInSevenDaysList(new ArrayList<>());
        tEntity.setCount(0);
        tEntity.setTotalIncome(BigDecimal.ZERO);
        // 获取直属下级userIdList
        List<Long> userIdList = getTeamUnderLingInfoByUid(shopId, userId);
        if (CollectionUtils.isEmpty(userIdList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
        }
        List<String> typeList = new ArrayList<>();
        typeList.add(FanActionEventEnum.NEWBIE_TASK.getCode());
        typeList.add(FanActionEventEnum.DAILY_TASK.getCode());
        typeList.add(FanActionEventEnum.SHOP_MANAGER_TASK.getCode());

        // 根据直属下级和行为类型查询数据
        List<FanAction> fanActionList = fanActionRepository.pageListForTeamManagerSevenDay(shopId, userIdList, typeList, offset, limit);
        if (CollectionUtils.isEmpty(fanActionList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
        }

        // 数据重新封装
        List<TeamMangerEventDTO> teamMangerEventDTOList = transformTeamMangerEventDTOFinishTask(fanActionList);
        if (CollectionUtils.isEmpty(teamMangerEventDTOList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
        }
        BigDecimal totalIncome = fanActionRepository.sumTaxForTeamManagerSevenDay(shopId, userIdList, typeList);
        long count = fanActionRepository.countUidForTeamManagerSevenDay(shopId, userIdList, typeList);
        tEntity.setCount(count);
        tEntity.setInSevenDaysList(teamMangerEventDTOList);
        tEntity.setTotalIncome(totalIncome);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
    }

    /**
     * 封装完成任务信息
     *
     * @param fanActionList 粉丝完成任务记录列表
     * @return 返回结果 List<TeamMangerEventDTO>
     */
    private List<TeamMangerEventDTO> transformTeamMangerEventDTOFinishTask(List<FanAction> fanActionList) {
        List<TeamMangerEventDTO> teamMangerEventDTOList = new ArrayList<>();
        for (FanAction fa : fanActionList) {
            Long uid = fa.getUid();
            //事件的子类型
            String eventType = fa.getEventType();
            String event = fa.getEvent();
            TeamMangerEventDTO teamMangerEventDTO = new TeamMangerEventDTO();
            teamMangerEventDTO.setUserId(uid.toString());

            String intervalTime = getIntervalTime(fa.getCurrent());
            teamMangerEventDTO.setIntervalTime(intervalTime);

            String taskName = getTaskName(event);
            teamMangerEventDTO.setTaskName(taskName);

            teamMangerEventDTO.setTaskType(event);
            String subTaskName = getSubTaskName(eventType);
            teamMangerEventDTO.setIncome(fa.getTax());
            if (teamMangerEventDTO.getIncome() == null) {
                teamMangerEventDTO.setIncome(BigDecimal.ZERO);
            }
            teamMangerEventDTO.setSubTaskName(subTaskName);
            teamMangerEventDTO.setSubTaskType(eventType);
            //根据用户ID获取用户的图片和姓名
            UserResp userInfo = userAppService.queryUser(uid);
            if (null != userInfo) {
                teamMangerEventDTO.setUserName(userInfo.getNickName());
                teamMangerEventDTO.setUserPics(userInfo.getAvatarUrl());
            }
            teamMangerEventDTOList.add(teamMangerEventDTO);
        }
        return teamMangerEventDTOList;
    }

    /**
     * 获取团队下的人员信息
     *
     * @param userId
     * @return
     */
    private List<Long> getTeamInfoByUid(Long shopId, Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (null == distributionUser) {
            return null;
        }
        distributionUserRepository.addUserShopTeamForAll(distributionUser);
        List<Long> userIdList = distributionUser.queryUserShopTeams();
        userIdList.remove(userId);
        return userIdList;
    }

    /**
     * 获取直属下级信息
     *
     * @param shopId
     * @param userId
     * @return
     */
    private List<Long> getTeamUnderLingInfoByUid(Long shopId, Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (null == distributionUser) {
            return null;
        }
        distributionUserRepository.addUserShopTeamForAll(distributionUser);
        List<Long> userIdList = distributionUser.getUserShopTeam().queryFirstLowerMemberIds();
        userIdList.remove(userId);
        return userIdList;
    }


    /**
     * 粉丝行为明细
     */
    @Override
    public SimpleResponse fansActiveDetail(Long shopId, Long userId, Long fanUserId, Long offset, Long limit) {
        List<String> typeList = new ArrayList<>();
        typeList.add(FanActionEventEnum.LOGIN.getCode());
        typeList.add(FanActionEventEnum.ORDER.getCode());
        typeList.add(FanActionEventEnum.PAY.getCode());
        typeList.add(FanActionEventEnum.INVITATION.getCode());
        typeList.add(FanActionEventEnum.NEWBIE_TASK.getCode());
        typeList.add(FanActionEventEnum.DAILY_TASK.getCode());
        typeList.add(FanActionEventEnum.SHOP_MANAGER_TASK.getCode());
        typeList.add(FanActionEventEnum.SHOP_MANAGER.getCode());

        TeamMangerEventEntity tEntity = new TeamMangerEventEntity();
        // 查询粉丝用户
        UserResp userResp = userAppService.queryUser(fanUserId);
        tEntity.setUserName(userResp.getNickName());
        tEntity.setUserPic(userResp.getAvatarUrl());
        Boolean isReachMark = isReachMark(userId);
        tEntity.setInSevenDaysList(new ArrayList<>());
        tEntity.setIsReachMark(isReachMark);
        tEntity.setCount(0L);

        List<FanAction> fanActionList = fanActionRepository.pageListForTeamManagerAll(shopId, fanUserId, typeList, offset, limit);
        if (CollectionUtils.isEmpty(fanActionList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
        }
        //  组装结果
        List<TeamMangerEventDTO> teamMangerEventDTOList = transformTeamMangerEventDTOFansActives(fanActionList);
        if (CollectionUtils.isEmpty(teamMangerEventDTOList) || teamMangerEventDTOList.size() < 1) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
        }
        long count = fanActionRepository.countForFansActiveAll(shopId, fanUserId, typeList);
        tEntity.setInSevenDaysList(teamMangerEventDTOList);
        tEntity.setIsReachMark(isReachMark);
        tEntity.setCount(count);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, tEntity);
    }

    /**
     * 组装粉丝浏览记录
     *
     * @param fanActionList
     * @return
     */
    private List<TeamMangerEventDTO> transformTeamMangerEventDTOFansActives(List<FanAction> fanActionList) {
        Map<Long, UserResp> userRespMap = new HashMap<>();
        List<TeamMangerEventDTO> teamMangerEventDTOList = new ArrayList<>();
        for (FanAction fanAction : fanActionList) {
            TeamMangerEventDTO teamMangerEventDTO = new TeamMangerEventDTO();
            teamMangerEventDTO.setUserId(fanAction.getUid().toString());
            String intervalTime = getIntervalTime(fanAction.getCurrent());
            teamMangerEventDTO.setIntervalTime(intervalTime);

            UserResp userInfo;
            if (userRespMap.containsKey(fanAction.getUid())) {
                userInfo = userRespMap.get(fanAction.getUid());
            } else {
                userInfo = userAppService.queryUser(fanAction.getUid());
                userRespMap.put(userInfo.getId(), userInfo);
            }
            if (userInfo != null) {
                String userName = userInfo.getNickName();
                String userPic = userInfo.getAvatarUrl();
                teamMangerEventDTO.setUserName(userName);
                teamMangerEventDTO.setUserPics(userPic);
            }
            if (fanAction.getAmount() != null) {
                teamMangerEventDTO.setSalsePrice(fanAction.getAmount().toString());
            }
            String taskName = getTaskName(fanAction.getEvent());
            teamMangerEventDTO.setTaskName(taskName);
            teamMangerEventDTO.setTaskType(fanAction.getEvent());

            FanActionEventEnum enumByCode = FanActionEventEnum.getEnumByCode(fanAction.getEvent());
            if (enumByCode == null) {
                continue;
            }
            switch (enumByCode) {
                case LOGIN:
                case VIEW_GOODS:
                    break;
                case ORDER:
                case PAY:
                    //订单时
                    teamMangerEventDTO.setSalsePrice(fanAction.getAmount().toString());
                    teamMangerEventDTO.setIncome(fanAction.getTax());
                    break;
                case INVITATION:
                    if (!CollectionUtils.isEmpty(fanAction.getExtra())) {
                        int fansNum = (int) fanAction.getExtra().get("fansNum");
                        teamMangerEventDTO.setFansNum(fansNum);
                    }
                    break;
                case NEWBIE_TASK:
                case DAILY_TASK:
                case SHOP_MANAGER_TASK:
                    teamMangerEventDTO.setIncome(fanAction.getTax());
                    teamMangerEventDTO.setSubTaskType(fanAction.getEventType());
                    String subTaskName = getSubTaskName(fanAction.getEventType());
                    teamMangerEventDTO.setSubTaskName(subTaskName);
                    break;
                case SHOP_MANAGER:
                    teamMangerEventDTO.setIncome(fanAction.getTax());
                    break;
                default:
                    break;
            }
            teamMangerEventDTOList.add(teamMangerEventDTO);
        }
        return teamMangerEventDTOList;
    }


    /**
     * 是否达标
     *
     * @param userId
     * @return
     */
    private Boolean isReachMark(Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());
        return distributionUser.hasReachSpendStandard();
    }

    /**
     * 获取子任务名
     *
     * @param eventType
     * @return
     */
    private String getSubTaskName(String eventType) {
        if (StringUtils.isBlank(eventType)) {
            return null;
        }
        int code = Integer.valueOf(eventType);
        return ActivityDefTypeEnum.findByDesc(code);
    }

    /**
     * 获取任务名
     *
     * @param event
     * @return
     */
    private String getTaskName(String event) {
        if (StringUtils.isBlank(event)) {
            return null;
        }
        // 直接返回任务类型的描述信息
        return FanActionEventEnum.getDescByCode(event);
    }

    /**
     * 获取间隔时间
     *
     * @param time
     * @return
     */
    private String getIntervalTime(Long time) {
        if (null == time) {
            return null;
        }
        //天
        long nd = 1000 * 24 * 60 * 60;
        //小时
        long nh = 1000 * 60 * 60;
        //分钟
        long nm = 1000 * 60;

        // 获得两个时间的毫秒时间差异
        Date nowTime = new Date();

        long diff = nowTime.getTime() - time;

        String intervalTime;
        //大于60分钟
        if (diff > nd) {
            long day = diff / nd;
            intervalTime = day + "天";
        } else if (diff > nh) {
            long hour = diff % nd / nh;
            intervalTime = hour + "小时";
        } else {
            long min = diff % nd % nh / nm;
            intervalTime = min + "分钟";
        }

        return intervalTime;
    }
}
