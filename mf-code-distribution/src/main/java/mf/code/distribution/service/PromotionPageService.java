package mf.code.distribution.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.dto.UpayWxOrderReq;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 19:15
 */
public interface PromotionPageService {

	/**
	 * 获取推广收益页--用户收入
	 *
	 * @param userId
	 * @param shopId
	 * @param offset
	 * @param size
	 * @return
	 */
	SimpleResponse queryUserIncome(Long userId, Long shopId, Long offset, Long size);

	/**
	 * 获取推广收益页--用户收入
	 *
	 * @param userId
	 * @param shopId
	 * @param type 1.全球收益排行榜 2.好友收益排行榜
	 * @param offset
	 * @param size
	 * @return
	 */
	SimpleResponse queryUserIncomeV2(Long userId, Long shopId, Integer type, Long offset, Long size);

	/**
	 * 获取当前店铺下 用户团队贡献榜
	 *
	 * @param userId
	 * @param shopId
	 * @param offset
	 * @param size
	 * @return
	 */
	SimpleResponse queryTeamIncome(Long userId, Long shopId, String type, Long offset, Long size);

	/***
	 * 获取团队粉丝信息
	 * @param userId
	 * @param shopId
	 * @param merchantId
	 * @return
	 */
	SimpleResponse queryTeamNumInfo(Long userId, Long shopId, Long merchantId);

	/**
	 * 成为 店长 的弹窗状态
	 *
	 * @param userId
	 * @return
	 */
	SimpleResponse beShopManagerPopup(Long userId);

	/**
	 * 申请 店长
	 * @param shopId
	 * @param userId
	 * @return
	 */
	SimpleResponse becomeShopManager(Long shopId, Long userId);

	SimpleResponse becomeShopManagerV2(UpayWxOrderReq upayWxOrderReq);

	/**
	 * 获取商学院页
	 *
	 * @param userId
	 * @param shopId
	 * @return
	 */
	SimpleResponse queryBusinessSchoolPage(Long userId, Long shopId);

	/**
	 * 上传 店长二维码
	 * @param userId
	 * @param uploadShopManagerQR
	 * @return
	 */
	SimpleResponse uploadShopManagerQR(Long shopId, Long userId, Map<String, String> uploadShopManagerQR);

	/**
	 * 获取待结算订单明细页
	 * @param userId
	 * @param shopId
	 * @param bizType
	 * @param dateType
	 * @return
	 */
	SimpleResponse queryPendRecordsPage(Long userId, Long shopId, Integer bizType, Integer dateType, Long offset, Long size);

	/**
	 * 领取店长任务
	 * @param userId
	 * @return
	 */
	SimpleResponse doShopManagerTask(Long userId, Long shopId);

	/**
	 * 展示上传 二维码页
	 * @param merchantId
	 * @param shopId
	 * @param userId
	 * @return
	 */
	SimpleResponse showShopManagerQR(Long merchantId, Long shopId, Long userId);
}
