package mf.code.distribution.service.templatemessage.message;

/**
 * 微信模板消息
 *
 * mf.code.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-11 下午2:13
 */
public enum TemplateMessageEnum {
	/**
	 * 模板ID：9RQ6o9eZo-zUcYYT7f5h9HH02gyY8l6vbiW_fgQcXoU
	 * 标题：  审核结果通知
	 * 关键词：温馨提示 {{keyword1.DATA}}
	 *        审核内容 {{keyword2.DATA}}
	 *        后续步骤 {{keyword3.DATA}}
	 * -----------------------------------------
	 * 预览：
	 * 审核结果通知
	 * 2019年03月
	 *
	 * 温馨提示：申请认证讲师通过
	 * 审核内容：申请成为讲师
	 * 后续步骤：已成为讲师，马上招募自己的商家吧！
	 * -----------------------------------------
	 */
	AUDIT_RESULT_ADVICE("9RQ6o9eZo-zUcYYT7f5h9HH02gyY8l6vbiW_fgQcXoU", "审核结果通知"),

	/**
	 * 模板ID：HheFVHa1Vs3zsBE4u7qX72fQBW3hJ0NkwCZtIUgiPw8
	 * 标题：  收益到账通知
	 * 关键词：温馨提示 {{keyword1.DATA}}
	 *        收益来源 {{keyword2.DATA}}
	 *        金额    {{keyword3.DATA}}
	 * -----------------------------------------
	 * 预览：
	 * 收益到账通知
	 * 2019年03月
	 *
	 * 温馨提示：商家招募成功
	 * 收益来源：云店生活馆已成功入驻集客魔方
	 * 金额：    232元
	 * -----------------------------------------
	 */
	INCOME_ARRIVAL_ADVICE("HheFVHa1Vs3zsBE4u7qX72fQBW3hJ0NkwCZtIUgiPw8", "收益到账通知"),
	;

	/**
	 * 模板ID
	 */
	private String templateId;
	/**
	 * 标题
	 */
	private String title;

	TemplateMessageEnum(String templateId, String title) {
		this.templateId = templateId;
		this.title = title;
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getTitle() {
		return title;
	}
}
