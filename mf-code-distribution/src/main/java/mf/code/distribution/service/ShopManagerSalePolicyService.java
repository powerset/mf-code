package mf.code.distribution.service;

import mf.code.distribution.domain.valueobject.ShopManagerSalePolicy;

/**
 * mf.code.distribution.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 23:36
 */
public interface ShopManagerSalePolicyService {

    /**
     * 获取 付费升店长的政策
     *
     * @return
     */
    ShopManagerSalePolicy getShopManagerSalePolicy();

    /**
     * 保存或更新 付费升店长的政策
     *
     * @param shopManagerSalePolicy
     * @return
     */
    boolean saveOrUpdateShopManagerSalePolicy(ShopManagerSalePolicy shopManagerSalePolicy);
}
