package mf.code.distribution.service.templatemessage.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.common.caller.wxmp.WeixinMpService;
import mf.code.distribution.service.templatemessage.message.TemplateMessageEnum;
import mf.code.distribution.common.property.WxmpProperty;
import mf.code.distribution.common.property.WxpayProperty;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.templatemessage.TemplateMessageService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static mf.code.distribution.common.caller.wxmp.WeixinMpConstants.DOMAIN_WX_SENDTEMPLATEMESSAGE;

/**
 * mf.code.api.teacher.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-11 下午1:37
 */
@Slf4j
@Service
public class TemplateMessageServiceImpl implements TemplateMessageService {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private WxmpProperty wxmpProperty;
	@Autowired
	private WxpayProperty wxpayProperty;
	@Autowired
	private WeixinMpService weixinMpService;
	@Autowired
	private DistributionUserRepository distributionUserRepository;

	@Override
	public Map<String, Object> sendTemplateMessage(String templateId, DistributionUser distributionUser, Map<String, Object> data) {
		if (StringUtils.isBlank(templateId) || distributionUser == null || CollectionUtils.isEmpty(data)) {
			return null;
		}
		log.info("<<<<<<<<推送消息-入参：data：{}， template:{}, distributionUser:{}", data, templateId, distributionUser);
		Map<String, Object> reqParam = new HashMap<>();
		reqParam.put("touser", distributionUser.getUserInfo().getOpenId());
		reqParam.put("template_id", templateId);
		reqParam.put("form_id", queryRedisTeacherFormIds(distributionUser.getUserId()));
		reqParam.putAll(data);
		log.info("<<<<<<<<推送消息-开始：{}", reqParam);
		String accessToken = weixinMpService.getAccessToken(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret());
		if (StringUtils.isBlank(accessToken)) {
			log.error("accesstoken 为空：{}", accessToken);
			return null;
		}
		String accessTokenParam = "access_token=" + accessToken;
		String url = DOMAIN_WX_SENDTEMPLATEMESSAGE + "?" + accessTokenParam;
		String voStrParams = JSONObject.toJSONString(reqParam);
		Map<String, Object> map = this.weixinMpService.sendMessage(url, voStrParams);
		log.info("<<<<<<<<推送消息-结束返回: resp:{}", map);
		return map;

	}

	@Override
	public Map<String, Object> sendTemplateMessage(String templateId, Long shopId, Long userId, Map<String, Object> data) {
		DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
		return this.sendTemplateMessage(templateId, distributionUser, data);
	}

	private String queryRedisTeacherFormIds(Long userId) {
		//查询redis,参与人展现,直接倒叙排列
		BoundListOperations formIDS = this.stringRedisTemplate.boundListOps(wxmpProperty.getRedisKey(userId));
		List<Object> formIDStrs = formIDS.range(0, -1);
		if (formIDStrs == null || formIDStrs.size() == 0) {
			return null;
		}
		String formID = null;
		int index = 0;
		for (Object obj : formIDStrs) {
			JSONObject jsonObject = JSON.parseObject(obj.toString());
			//判断该formId是否为有效的 7天内有效
			long DayMillis = 24 * 60 * 60 * 1000;
			if ((System.currentTimeMillis() - jsonObject.getLong("ctime")) / DayMillis > 6) {
				//过期都删除
				this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(userId), index, obj);
				index++;
				continue;
			}
			formID = jsonObject.getString("formId");
			//用完即删
			this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(userId), index, obj);
			break;
		}
		return formID;
	}
}
