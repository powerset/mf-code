package mf.code.distribution.service.platform.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.api.feignclient.CommAppService;
import mf.code.distribution.api.feignclient.OneAppService;
import mf.code.distribution.domain.valueobject.ShopManagerSalePolicy;
import mf.code.distribution.model.request.platform.ShopManagerSalePolicyRequest;
import mf.code.distribution.service.ShopManagerSalePolicyService;
import mf.code.distribution.service.platform.PlatformShopManagerService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.api.platform.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 13:33
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PlatformShopManagerServiceImpl implements PlatformShopManagerService {
    private final ShopManagerSalePolicyService shopManagerSalePolicyService;
    private final OneAppService oneAppService;

    /**
     * 保存 店长缴纳金配置
     * @param shopManagerSalePolicyRequest
     * @return
     */
    @Override
    public SimpleResponse saveSalePolicy(ShopManagerSalePolicyRequest shopManagerSalePolicyRequest) {
        BigDecimal originalPrice = new BigDecimal(shopManagerSalePolicyRequest.getOriginalPrice());
        BigDecimal salePrice = new BigDecimal(shopManagerSalePolicyRequest.getSalePrice());
        Integer shopRate0 = Integer.valueOf(shopManagerSalePolicyRequest.getShopRate0());
        Integer platformRate0 = Integer.valueOf(shopManagerSalePolicyRequest.getPlatformRate0());
        Integer superiorRate1 = Integer.valueOf(shopManagerSalePolicyRequest.getSuperiorRate1());
        Integer shopRate1 = Integer.valueOf(shopManagerSalePolicyRequest.getShopRate1());
        Integer platformRate1 = Integer.valueOf(shopManagerSalePolicyRequest.getPlatformRate1());

        Map<String, Integer> policy0 = new HashMap<>();
        policy0.put("hasSuperior", 0);
        policy0.put("shopRate", shopRate0);
        policy0.put("platformRate", platformRate0);
        Map<String, Integer> policy1 = new HashMap<>();
        policy1.put("hasSuperior", 1);
        policy1.put("superiorRate", superiorRate1);
        policy1.put("shopRate", shopRate1);
        policy1.put("platformRate", platformRate1);
        List<Map<String, Integer>> policyList = new ArrayList<>();
        policyList.add(policy0);
        policyList.add(policy1);

        ShopManagerSalePolicy shopManagerSalePolicy = new ShopManagerSalePolicy();
        shopManagerSalePolicy.setOriginalPrice(originalPrice);
        shopManagerSalePolicy.setSalePrice(salePrice);
        shopManagerSalePolicy.setPolicyList(policyList);

        boolean success = shopManagerSalePolicyService.saveOrUpdateShopManagerSalePolicy(shopManagerSalePolicy);
        if (!success) {
            log.error("数据库异常，保存或更新失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
        }

        if (salePrice.compareTo(BigDecimal.ZERO) == 0) {
            oneAppService.offlineShopManagerActivityDef();
        }
        return new SimpleResponse();
    }

    /**
     * 获取 店长缴纳金配置
     *
     * @return
     */
    @Override
    public SimpleResponse querySalePolicy() {
        ShopManagerSalePolicy shopManagerSalePolicy = this.shopManagerSalePolicyService.getShopManagerSalePolicy();

        return new SimpleResponse(ApiStatusEnum.SUCCESS, shopManagerSalePolicy);
    }
}
