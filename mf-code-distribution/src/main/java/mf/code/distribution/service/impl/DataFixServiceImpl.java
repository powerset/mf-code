package mf.code.distribution.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.exception.RocketMQException;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.ShopAppService;
import mf.code.distribution.common.email.EmailService;
import mf.code.distribution.common.property.WxpayProperty;
import mf.code.distribution.constant.PlatformOrderBizTypeEnum;
import mf.code.distribution.constant.PlatformOrderStatusEnum;
import mf.code.distribution.constant.PlatformOrderTypeEnum;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.domain.Factory.DistributionUserFactory;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.*;
import mf.code.distribution.repo.po.PlatformOrder;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.DataFixService;
import mf.code.distribution.service.PlatformOrderService;
import mf.code.distribution.service.UpayWxOrderPendService;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.order.dto.OrderResp;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.distribution.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-01 10:05
 */
@Slf4j
@Service
public class DataFixServiceImpl implements DataFixService {
	@Autowired
	private WxpayProperty appProperty;
	@Autowired
	private DistributionUserRepository distributionUserRepository;
	@Autowired
	private OrderAppService orderAppService;
	@Autowired
	private PlatformOrderService platformOrderService;
	@Autowired
	private ShopAppService shopAppService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private WxpayProperty wxpayProperty;
	@Autowired
	private UpayWxOrderPendService upayWxOrderPendService;

	/**
	 * 商品售出，分配佣金 -- 商家及用户
	 * @return
	 */
	@Override
	public void distributionPlatformOrderPendFix() {
		// 定义结算订单的时间段 -- 上月开始-上月结束
		Date beginTimeDate = new Date(0);
		Date endTimeDate = new Date();

		String beginTime = DateUtil.dateToString(beginTimeDate, DateUtil.FORMAT_ONE);
		String endTime = DateUtil.dateToString(endTimeDate, DateUtil.FORMAT_ONE);
		// 获取上月所有用户所有店铺 所有可结算订单
		List<OrderResp> orderRespList = orderAppService.queryPaidOrdersWithBiz(beginTime, endTime);
		if (CollectionUtils.isEmpty(orderRespList)) {
			log.info("查无可结算订单， beginTimeDate = {}, endTimeDate = {}", beginTimeDate, endTimeDate);
			return;
		}

		// 定义key为 userId, 定义value为 List<OrderResp>
		Map<Long, List<OrderResp>> userShopOrderMap = new HashMap<>();
		for (OrderResp orderResp : orderRespList) {
			Long userId = orderResp.getUserId();
			List<OrderResp> userShopOrderList = userShopOrderMap.get(userId);
			if (CollectionUtils.isEmpty(userShopOrderList)) {
				userShopOrderList = new ArrayList<>();
			}
			userShopOrderList.add(orderResp);
			userShopOrderMap.put(userId, userShopOrderList);
		}

		UserRebatePolicy userRebatePolicy = new UserRebatePolicy();
		RebateProperty rebateProperty = new RebateProperty();
		rebateProperty.setTaxRate(26);
		rebateProperty.setTeamRewardRate(4);
		rebateProperty.setAmountCondition(0);
		rebateProperty.setCommissionRate(60);
		rebateProperty.setRebateRate(10);
		rebateProperty.setScale(5);

		// 开始对所有用户 所有可结算订单进行结算 -- 已用户为第一维度，是因为，用户的消费金额是按全平台所有店铺统计的
		for (Map.Entry<Long, List<OrderResp>> userShopEntry : userShopOrderMap.entrySet()) {
			// 获取用户Id
			Long userId = userShopEntry.getKey();
			// 获取此用户的 所有店铺的可结算订单
			List<OrderResp> orderResps = userShopEntry.getValue();
			// 可结算订单的 用户消费金额
			BigDecimal spendAmount = BigDecimal.ZERO;

			// 按店铺结算 Map<ShopId, List<OrderResp>>
			Map<Long, List<OrderResp>> shopOrderMap = new HashMap<>();
			for (OrderResp orderResp : orderResps) {
				Long shopId = orderResp.getShopId();
				List<OrderResp> shopOrderList = shopOrderMap.get(shopId);
				if (CollectionUtils.isEmpty(shopOrderList)) {
					shopOrderList = new ArrayList<>();
				}
				shopOrderList.add(orderResp);
				shopOrderMap.put(shopId, shopOrderList);

				spendAmount = spendAmount.add(orderResp.getFee());
			}

			// 获取以平台维度统计的用户数据 -- 获取分销用户
			DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
			if (distributionUser == null) {
				log.error("获取用户异常 -- userId = {}", userId);
				continue;
			}
			distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);


			// 开始对此用户下 所有店铺 所有可结算订单进行结算 -- 先user后shop 因为vipShop分佣
			for (Map.Entry<Long, List<OrderResp>> shopEntry : shopOrderMap.entrySet()) {
				Long shopId = shopEntry.getKey();
				List<OrderResp> distributionOrders = shopEntry.getValue();

				// 对此用户 此店铺下的可结算订单进行结算
				for (OrderResp order : distributionOrders) {
					Long orderId = order.getId();
					OrderDistributionProperty orderDistributionProperty = JSON.parseObject(order.getOrderBizBizValue(), OrderDistributionProperty.class);
					if (orderDistributionProperty == null) {
						log.error("此订单未获取到分销属性, orderId = {}", orderId);
						continue;
					}
					// 此订单的 平台类目抽佣比率
					Integer goodsCategoryRate = orderDistributionProperty.getGoodsCategoryRate();
					// 此订单的 平台交易税率
					Integer transactionRate = orderDistributionProperty.getTransactionRate();
					// 此订单的 粉丝店铺分佣
					Integer vipShopRate = orderDistributionProperty.getVipShopRate();
					// 获取商品的分销政策
					ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(goodsCategoryRate, transactionRate, vipShopRate);
					// 计算各部分的分销佣金
					OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(orderDistributionProperty, order.getFee());
					if (orderCommissiones == null) {
						log.error("获取订单各部分分销佣金异常, 为排除服务异常，延迟消费, orderId = {}", orderId);
						continue;
					}

					List<UpayWxOrderReq> platformOrderPendList = new ArrayList<>();
					// 获取平台订单明细
					List<UpayWxOrderReq> platformOrderPends = platformOrderPend(orderCommissiones, order, orderDistributionProperty);
					if (!CollectionUtils.isEmpty(platformOrderPends)) {
						platformOrderPendList.addAll(platformOrderPends);
					}
					BigDecimal customerCommission = orderCommissiones.getCustomerCommission();
					if (customerCommission == null) {
						log.info("此订单无用户分佣属性, 正常结算， orderId = {}", orderId);
						boolean save = upayWxOrderPendService.saveUpayWxOrderPend(platformOrderPendList);
						if (!save) {
							log.error("数据库异常, orderId = {}", orderId);
						}
						continue;
					}

					UserShopTeamCommission userShopTeamCommission = userRebatePolicy.calculateCustomerCommissionsForEachMember(customerCommission, rebateProperty);
					List<UserShopTeamCommission> userShopTeamCommissions = new ArrayList<>();
					userShopTeamCommissions.add(userShopTeamCommission);

					// 获取平台订单明细
					List<UpayWxOrderReq> userPlatformOrderPends = platformOrderPend(userShopTeamCommissions, order, distributionUser.queryUpperMembers(), orderDistributionProperty);
					if (!CollectionUtils.isEmpty(userPlatformOrderPends)) {
						platformOrderPendList.addAll(userPlatformOrderPends);
					}
					boolean save = upayWxOrderPendService.saveUpayWxOrderPend(platformOrderPendList);
					if (!save) {
						log.error("数据库异常, orderId = {}", orderId);
					}
				}

			}
		}
		emailService.sendSimpleMail("平台订单待结算补全,终于结束啦", "冲鸭~~~");
	}

	@Override
	public void distributionPlatformOrderFix() {

		// 定义结算订单的时间段 -- 上月开始-上月结束
		Date beginTimeDate = new Date(0);
		Date endTimeDate = DateUtil.getEndOfLastTwoMonth();

		String beginTime = DateUtil.dateToString(beginTimeDate, DateUtil.FORMAT_ONE);
		String endTime = DateUtil.dateToString(endTimeDate, DateUtil.FORMAT_ONE);

		// 获取上月所有用户所有店铺 所有可结算订单
		List<OrderResp> orderRespList = orderAppService.querySettlementOrdersByTime(beginTime, endTime);
		if (CollectionUtils.isEmpty(orderRespList)) {
			log.info("查无可结算订单， beginTimeDate = {}, endTimeDate = {}", beginTimeDate, endTimeDate);
			return;
		}

		// 定义key为 userId, 定义value为 List<OrderResp>
		Map<Long, List<OrderResp>> userShopOrderMap = new HashMap<>();
		for (OrderResp orderResp : orderRespList) {
			Long userId = orderResp.getUserId();
			List<OrderResp> userShopOrderList = userShopOrderMap.get(userId);
			if (CollectionUtils.isEmpty(userShopOrderList)) {
				userShopOrderList = new ArrayList<>();
			}
			userShopOrderList.add(orderResp);
			userShopOrderMap.put(userId, userShopOrderList);
		}

		// 开始对所有用户 所有可结算订单进行结算 -- 已用户为第一维度，是因为，用户的消费金额是按全平台所有店铺统计的
		for (Map.Entry<Long, List<OrderResp>> userShopEntry : userShopOrderMap.entrySet()) {
			// 获取用户Id
			Long userId = userShopEntry.getKey();
			// 获取此用户的 所有店铺的可结算订单
			List<OrderResp> orderResps = userShopEntry.getValue();
			// 可结算订单的 用户消费金额
			BigDecimal spendAmount = BigDecimal.ZERO;

			// 按店铺结算 Map<ShopId, List<OrderResp>>
			Map<Long, List<OrderResp>> shopOrderMap = new HashMap<>();
			for (OrderResp orderResp : orderResps) {
				Long shopId = orderResp.getShopId();
				List<OrderResp> shopOrderList = shopOrderMap.get(shopId);
				if (CollectionUtils.isEmpty(shopOrderList)) {
					shopOrderList = new ArrayList<>();
				}
				shopOrderList.add(orderResp);
				shopOrderMap.put(shopId, shopOrderList);

				spendAmount = spendAmount.add(orderResp.getFee());
			}

			// 获取以平台维度统计的用户数据 -- 获取分销用户
			DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
			distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);
			// 获取 分销用户的上级成员
			List<Long> upperMembers = distributionUser.queryUpperMembers();
			// 获取上月消费达标的上级成员
			List<Long> reachUpperIds = new ArrayList<>();
			for (Long upperMemberId : upperMembers) {
				// 获取 上级成员的分销情况
				DistributionUser upperMember = distributionUserRepository.findByUserId(upperMemberId);
				distributionUserRepository.addUserOrderHistory(upperMember, beginTimeDate, endTimeDate);
				// 判断上级成员 上月消费是否达标
				if (upperMember.hasReachSpendStandard() && upperMember.hasShopManagerRole()) {
					reachUpperIds.add(upperMemberId);
				}
			}
			// 初始化 结算状态
			DistributionUser settleUser = DistributionUserFactory.newDistributionUser(distributionUser);

			// 开始对此用户下 所有店铺 所有可结算订单进行结算 -- 先user后shop 因为vipShop分佣
			for (Map.Entry<Long, List<OrderResp>> shopEntry : shopOrderMap.entrySet()) {
				Long shopId = shopEntry.getKey();
				List<OrderResp> distributionOrders = shopEntry.getValue();

				// 对此用户 此店铺下的可结算订单进行结算
				for (OrderResp order : distributionOrders) {
					Long orderId = order.getId();
					// 获取此订单当时的分销属性
					OrderDistributionProperty orderDistributionProperty = settleUser.queryOrderDistributionProperty(order);
					if (orderDistributionProperty == null) {
						log.error("此订单未获取到分销属性, orderId = {}", orderId);
						continue;
					}
					// 此订单的 平台类目抽佣比率
					Integer goodsCategoryRate = orderDistributionProperty.getGoodsCategoryRate();
					// 此订单的 平台交易税率
					Integer transactionRate = orderDistributionProperty.getTransactionRate();
					// 此订单的 粉丝店铺分佣
					Integer vipShopRate = orderDistributionProperty.getVipShopRate();
					// 获取商品的分销政策
					ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(goodsCategoryRate, transactionRate, vipShopRate);
					// 计算各部分的分销佣金
					OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(orderDistributionProperty, order.getFee());

					// 获取 平台所得流水
					List<PlatformOrder> platformOrderList = settlePlatformOrder(orderCommissiones, order, orderDistributionProperty);
					// 存储 平台所得流水
					if (!CollectionUtils.isEmpty(platformOrderList)) {
						platformOrderService.saveBatch(platformOrderList);
					}

					// 获取用户分佣这部分的佣金
					BigDecimal customerCommission = orderCommissiones.getCustomerCommission();
					if (customerCommission == null) {
						log.info("此订单无分销属性, orderId = {}", orderId);
						continue;
					}
					// 添加订单
					settleUser.addOrder(order);
					// 计算用户返利分佣，各部分所得
					List<UserShopTeamCommission> userShopTeamCommissionList = settleUser.calculateCustomerCommissionsForEachMember(customerCommission, order.getFee());
					if (CollectionUtils.isEmpty(userShopTeamCommissionList)) {
						log.info("非分销订单, orderId = {}", order.getId());
						continue;
					}
					List<PlatformOrder> userPlatformOrderList = settlePlatformOrder(userShopTeamCommissionList, order, reachUpperIds, orderDistributionProperty);
					// 存储 平台所得流水
					if (!CollectionUtils.isEmpty(userPlatformOrderList)) {
						platformOrderService.saveBatch(userPlatformOrderList);
					}
				}
			}
		}
		emailService.sendSimpleMail("平台订单结算补全,终于结束啦", "冲鸭~~~");
	}

	/**
	 * 获取 用户相关的 平台订单流水 集合
	 *
	 * @param userShopTeamCommissionList
	 * @return
	 */
	private List<PlatformOrder> settlePlatformOrder(List<UserShopTeamCommission> userShopTeamCommissionList, OrderResp order, List<Long> reachUpperIds, OrderDistributionProperty orderDistributionProperty) {
		List<PlatformOrder> platformOrderList = new ArrayList<>();
		int size = reachUpperIds.size();
		BigDecimal perPubCommission = BigDecimal.ZERO;
		BigDecimal teamRewardAmount = BigDecimal.ZERO;
		BigDecimal taxAmount = BigDecimal.ZERO;

		for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
			perPubCommission = perPubCommission.add(userShopTeamCommission.getPerPubCommission());
			teamRewardAmount = teamRewardAmount.add(userShopTeamCommission.getTeamRewardAmount());
			taxAmount = taxAmount.add(userShopTeamCommission.getTaxAmount());
		}

		// 订单成交-上级缺失 用户分佣回收
		for (int i = 0; i < 5 - size; i++) {
			PlatformOrder platformOrderPerPub = newPlatformOrder(perPubCommission, order);
			if (platformOrderPerPub != null) {
				// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
				platformOrderPerPub.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getCode() + i);
				platformOrderPerPub.setOrderName(PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getDesc());
				platformOrderPerPub.setBizType(PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getCode());
				platformOrderPerPub.setBizValue(order.getId());
				platformOrderList.add(platformOrderPerPub);
			}
		}

		// 订单成交-用户返利分佣团队奖励部分回收
		PlatformOrder platformOrderTeamReward = newPlatformOrder(teamRewardAmount, order);
		if (platformOrderTeamReward != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderTeamReward.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getCode());
			platformOrderTeamReward.setOrderName(PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getDesc());
			platformOrderTeamReward.setBizType(PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getCode());
			platformOrderTeamReward.setBizValue(order.getId());
			platformOrderList.add(platformOrderTeamReward);
		}

		// 订单成交-用户返利分佣团队奖励部分回收
		PlatformOrder platformOrderTax = newPlatformOrder(taxAmount, order);
		if (platformOrderTax != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderTax.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getCode());
			platformOrderTax.setOrderName(PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getDesc());
			platformOrderTax.setBizType(PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getCode());
			platformOrderTax.setBizValue(order.getId());
			platformOrderList.add(platformOrderTax);
		}
		return resetProductProviderSettle(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 获取 商户相关的 平台订单流水 集合
	 *
	 * @param orderCommissiones
	 * @return
	 */
	private List<PlatformOrder> settlePlatformOrder(OrderCommissiones orderCommissiones, OrderResp order, OrderDistributionProperty orderDistributionProperty) {
		List<PlatformOrder> platformOrderList = new ArrayList<>();

		BigDecimal transactionCommission = orderCommissiones.getTransactionCommission();
		PlatformOrder platformOrderTransaction = newPlatformOrder(transactionCommission, order);
		if (platformOrderTransaction != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderTransaction.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.TRANSACTION.getCode());
			platformOrderTransaction.setOrderName(PlatformOrderBizTypeEnum.TRANSACTION.getDesc());
			platformOrderTransaction.setBizType(PlatformOrderBizTypeEnum.TRANSACTION.getCode());
			platformOrderTransaction.setBizValue(order.getId());
			platformOrderList.add(platformOrderTransaction);
		}
		BigDecimal specsCommission = orderCommissiones.getPlatformCommission();
		PlatformOrder platformOrderSpecs = newPlatformOrder(specsCommission, order);
		if (platformOrderSpecs != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderSpecs.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.SPECS_COMMISSION.getCode());
			platformOrderSpecs.setOrderName(PlatformOrderBizTypeEnum.SPECS_COMMISSION.getDesc());
			platformOrderSpecs.setBizType(PlatformOrderBizTypeEnum.SPECS_COMMISSION.getCode());
			platformOrderSpecs.setBizValue(order.getId());
			platformOrderList.add(platformOrderSpecs);
		}
		return resetProductProviderSettle(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 分销方店铺id和商户id 重新设置成 供货商店铺id和商户id
	 * @param platformOrderList
	 * @param orderDistributionProperty
	 * @param order
	 * @return
	 */
	private List<PlatformOrder> resetProductProviderSettle(List<PlatformOrder> platformOrderList, OrderDistributionProperty orderDistributionProperty, OrderResp order) {
		ProductDistributionTypeEnum productDistributionTypeEnum = ProductDistributionTypeEnum.findByParams(orderDistributionProperty.getProductId(),
				orderDistributionProperty.getParentId(),
				orderDistributionProperty.getSelfSupportRatio(),
				orderDistributionProperty.getPlatSupportRatio());
		if (productDistributionTypeEnum != ProductDistributionTypeEnum.RESELL) {
			return platformOrderList;
		}
		List<PlatformOrder> resetPlatformOrderList = new ArrayList<>();
		// 指向供货商
		Long goodsShopId = order.getGoodsShopId();
		Long parentMerchantId = shopAppService.getMerchantIdByShopId(goodsShopId);

		for (PlatformOrder platformOrder : platformOrderList) {
			platformOrder.setShopId(goodsShopId);
			platformOrder.setMerchantId(parentMerchantId);
			resetPlatformOrderList.add(platformOrder);
		}
		return resetPlatformOrderList;
	}

	/**
	 * 创建 平台订单流水信息
	 *
	 * @param totalFee
	 * @return 必须重写 orderNO  orderName bizType bizValue
	 */
	private PlatformOrder newPlatformOrder(BigDecimal totalFee, OrderResp order) {
		if (totalFee == null) {
			return null;
		}
		Date now = new Date();
		PlatformOrder platformOrder = new PlatformOrder();
		platformOrder.setMerchantId(order.getMerchantId());
		platformOrder.setShopId(order.getShopId());
		platformOrder.setUserId(order.getUserId());
		platformOrder.setType(PlatformOrderTypeEnum.PALTFORM_PRESTORE.getCode());
		platformOrder.setPayType(OrderPayTypeEnum.CASH.getCode());
		platformOrder.setPubAppId(appProperty.getMfAppId());
		platformOrder.setStatus(PlatformOrderStatusEnum.SUCCESS.getCode());
		platformOrder.setTotalFee(totalFee);
		platformOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
		platformOrder.setTradeId(order.getTradeId());
		platformOrder.setNotifyTime(now);
		platformOrder.setPaymentTime(now);
		platformOrder.setCtime(now);
		platformOrder.setUtime(now);
		platformOrder.setReqJson(order.getReqJson());
		return platformOrder;
	}

	/**
	 * 获取 用户相关的 平台订单流水 集合
	 * @param userShopTeamCommissionList
	 * @return
	 */
	private List<UpayWxOrderReq> platformOrderPend(List<UserShopTeamCommission> userShopTeamCommissionList, OrderResp order, List<Long> reachUpperIds, OrderDistributionProperty orderDistributionProperty) {
		List<UpayWxOrderReq> platformOrderList = new ArrayList<>();
		int size = reachUpperIds.size();
		BigDecimal perPubCommission = BigDecimal.ZERO;
		BigDecimal teamRewardAmount = BigDecimal.ZERO;
		BigDecimal taxAmount = BigDecimal.ZERO;

		for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
			perPubCommission = perPubCommission.add(userShopTeamCommission.getPerPubCommission());
			teamRewardAmount = teamRewardAmount.add(userShopTeamCommission.getTeamRewardAmount());
			taxAmount = taxAmount.add(userShopTeamCommission.getTaxAmount());
		}

		// 订单成交-上级缺失 用户分佣回收
		for (int i = 0; i < 5 - size; i++) {
			UpayWxOrderReq platformOrderPerPub = newUpayWxOrder(order);
			platformOrderPerPub.setTotalFee(perPubCommission);
			platformOrderPerPub.setBizType(UpayWxOrderPendBizTypeEnum.USER_COMMISSION_RECYCLING.getCode());
			platformOrderList.add(platformOrderPerPub);
		}

		// 订单成交-用户返利分佣团队奖励部分回收
		UpayWxOrderReq platformOrderTeamReward = newUpayWxOrder(order);
		platformOrderTeamReward.setTotalFee(teamRewardAmount);
		platformOrderTeamReward.setBizType(UpayWxOrderPendBizTypeEnum.USER_TEAM_REWARD.getCode());
		platformOrderList.add(platformOrderTeamReward);

		// 订单成交-用户返利分佣团队奖励部分回收
		UpayWxOrderReq platformOrderTax = newUpayWxOrder(order);
		platformOrderTax.setTotalFee(taxAmount);
		platformOrderTax.setBizType(UpayWxOrderPendBizTypeEnum.USER_COMMISSION_TAX.getCode());
		platformOrderList.add(platformOrderTax);

		return resetProductProvider(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 获取 商户相关的 平台订单流水 集合
	 * @param orderCommissiones
	 * @return
	 */
	private List<UpayWxOrderReq> platformOrderPend(OrderCommissiones orderCommissiones, OrderResp order, OrderDistributionProperty orderDistributionProperty) {
		List<UpayWxOrderReq> platformOrderList = new ArrayList<>();

		BigDecimal transactionCommission = orderCommissiones.getTransactionCommission();
		if (transactionCommission != null) {
			UpayWxOrderReq platformOrderTransaction = newUpayWxOrder(order);
			platformOrderTransaction.setTotalFee(transactionCommission);
			platformOrderTransaction.setBizType(UpayWxOrderPendBizTypeEnum.TRANSACTION.getCode());
			platformOrderList.add(platformOrderTransaction);
		}

		BigDecimal specsCommission = orderCommissiones.getPlatformCommission();
		if (specsCommission != null) {
			UpayWxOrderReq platformOrderSpecs = newUpayWxOrder(order);
			platformOrderSpecs.setTotalFee(specsCommission);
			platformOrderSpecs.setBizType(UpayWxOrderPendBizTypeEnum.SPECS_COMMISSION.getCode());
			platformOrderList.add(platformOrderSpecs);
		}


		return resetProductProvider(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 分销方店铺id和商户id 重新设置成 供货商店铺id和商户id
	 * @param platformOrderList
	 * @param orderDistributionProperty
	 * @param order
	 * @return
	 */
	private List<UpayWxOrderReq> resetProductProvider(List<UpayWxOrderReq> platformOrderList, OrderDistributionProperty orderDistributionProperty, OrderResp order) {
		ProductDistributionTypeEnum productDistributionTypeEnum = ProductDistributionTypeEnum.findByParams(orderDistributionProperty.getProductId(),
				orderDistributionProperty.getParentId(),
				orderDistributionProperty.getSelfSupportRatio(),
				orderDistributionProperty.getPlatSupportRatio());
		if (productDistributionTypeEnum != ProductDistributionTypeEnum.RESELL) {
			return platformOrderList;
		}
		List<UpayWxOrderReq> resetPlatformOrderList = new ArrayList<>();
		// 指向供货商
		Long goodsShopId = order.getGoodsShopId();
		Long parentMerchantId = shopAppService.getMerchantIdByShopId(goodsShopId);

		for (UpayWxOrderReq platformOrder : platformOrderList) {
			platformOrder.setShopId(goodsShopId);
			platformOrder.setMchId(parentMerchantId);
			resetPlatformOrderList.add(platformOrder);
		}
		return resetPlatformOrderList;
	}

	/**
	 * 生成 用户订单流水
	 *
	 * @param order
	 * @return 重写 biz_type totalFee
	 */
	private UpayWxOrderReq newUpayWxOrder(OrderResp order) {
		Date now = new Date();

		UpayWxOrderReq upayWxOrder = new UpayWxOrderReq();
		upayWxOrder.setMchId(order.getMerchantId());
		upayWxOrder.setShopId(order.getShopId());
		upayWxOrder.setUserId(order.getUserId());
		upayWxOrder.setType(UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
		upayWxOrder.setPayType(UpayWxOrderPayTypeEnum.CASH.getCode());
		upayWxOrder.setOrderNo(order.getOrderNo());
		upayWxOrder.setOrderName(order.getOrderName());
		upayWxOrder.setAppletAppId(wxpayProperty.getMfAppId());
		upayWxOrder.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
		upayWxOrder.setTotalFee(order.getFee());
		upayWxOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
		upayWxOrder.setIpAddress(order.getIpAddress());
		upayWxOrder.setTradeId(order.getTradeId());
		upayWxOrder.setRemark(order.getRemark());
		upayWxOrder.setAppletAppId(wxpayProperty.getMfAppId());
		upayWxOrder.setNotifyTime(now);
		upayWxOrder.setPaymentTime(now);
		upayWxOrder.setCtime(now);
		upayWxOrder.setUtime(now);
		upayWxOrder.setReqJson(order.getReqJson());
		upayWxOrder.setBizType(UpayWxOrderBizTypeEnum.REBATE.getCode());
		upayWxOrder.setBizValue(order.getId());
		upayWxOrder.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
		upayWxOrder.setRefundOrderId(0L);
		return upayWxOrder;
	}


}
