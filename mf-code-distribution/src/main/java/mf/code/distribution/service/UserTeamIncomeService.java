package mf.code.distribution.service;

import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.distribution.domain.aggregateroot.DistributionUser;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-31 21:49
 */
public interface UserTeamIncomeService {

	/**
	 * 获取当前店铺下 用户团队贡献的最新一条记录
	 * @param userId
	 * @param shopId
	 * @return
	 */
	Map<String, Object> queryLatestIncomeRecord(Long userId, Long shopId);

	/**
	 * 获取当前店铺下 用户好友收益排行榜 （上级+用户+下一级）
	 * @param distributionUser
	 * @param offset
	 * @param size
	 * @return
	 */
	AppletMybatisPageDto queryIncomeRankListOfFriends(DistributionUser distributionUser, Long offset, Long size);

	/**
	 * 获取当前店铺下 用户团队贡献榜
	 * @param distributionUser
	 * @param type 1全部 2已结清 3待结算
	 * @param offset
	 * @param size
	 * @return
	 */
	AppletMybatisPageDto queryRankListByTeamContribution(DistributionUser distributionUser, String type, Long offset, Long size);

	/**
	 * 把用户添加到 店铺累计收益排行榜
	 *
	 * @param userIds
	 * @param shopId
	 */
	void addIncomeRankListOfShopByUserIds(List<Long> userIds, Long shopId);

	/**
	 * 获取 全球收益排行版
	 * @param distributionUser
	 * @param offset
	 * @param size
	 * @return
	 */
	AppletMybatisPageDto queryIncomeRankListOfWorld(DistributionUser distributionUser, Long offset, Long size);
}
