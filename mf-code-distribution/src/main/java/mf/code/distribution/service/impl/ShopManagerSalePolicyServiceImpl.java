package mf.code.distribution.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.distribution.api.feignclient.CommAppService;
import mf.code.distribution.domain.valueobject.ShopManagerSalePolicy;
import mf.code.distribution.service.ShopManagerSalePolicyService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 23:37
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ShopManagerSalePolicyServiceImpl implements ShopManagerSalePolicyService {
    private final CommAppService commAppService;

    /**
     * 获取 付费升店长的政策
     *
     * @return
     */
    @Override
    public ShopManagerSalePolicy getShopManagerSalePolicy() {
        CommonDictDTO commonDictDTO = commAppService.selectByTypeKey("shopManager", "salePolicy");
        if (commonDictDTO == null) {
            log.info("暂无付费升店长配置");
            return null;
        }
        JSONObject jsonObject = JSON.parseObject(commonDictDTO.getValue());
        if (jsonObject == null) {
            log.error("付费升店长配置为空");
            return null;
        }

        ShopManagerSalePolicy shopManagerSalePolicy = new ShopManagerSalePolicy();
        shopManagerSalePolicy.setOriginalPrice(jsonObject.getBigDecimal("originalPrice"));
        shopManagerSalePolicy.setSalePrice(jsonObject.getBigDecimal("salePrice"));
        JSONArray policyJsonArray = jsonObject.getJSONArray("policyList");
        List<Map<String, Integer>> policyList = new ArrayList<>();
        for (Object obj : policyJsonArray) {
            Map<String, Integer> policy = new HashMap<>();
            JSONObject policyJsonObject = JSON.parseObject(JSON.toJSONString(obj));
            policy.put("hasSuperior", policyJsonObject.getInteger("hasSuperior"));
            policy.put("superiorRate", policyJsonObject.getInteger("superiorRate"));
            policy.put("shopRate", policyJsonObject.getInteger("shopRate"));
            policy.put("platformRate", policyJsonObject.getInteger("platformRate"));
            policyList.add(policy);
        }
        shopManagerSalePolicy.setPolicyList(policyList);
        return shopManagerSalePolicy;
    }

    /**
     * 保存或更新 付费升店长的政策
     *
     * @param shopManagerSalePolicy
     * @return
     */
    @Override
    public boolean saveOrUpdateShopManagerSalePolicy(ShopManagerSalePolicy shopManagerSalePolicy) {
        CommonDictDTO commonDictDTO = new CommonDictDTO();
        commonDictDTO.setType("shopManager");
        commonDictDTO.setKey("salePolicy");
        commonDictDTO.setValue(JSON.toJSONString(shopManagerSalePolicy));
        return commAppService.saveOrUpdateByTypeKey(commonDictDTO);
    }

}
