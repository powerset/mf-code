package mf.code.distribution.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.user.dto.UpayWxOrderReq;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月24日 14:31
 */
public interface UpayWxOrderPendService extends IService<UpayWxOrderPend> {

    /**
     * 对 List<UpayWxOrderReq> 批量入库
     * @param upayWxOrderReqs
     * @return
     */
    boolean saveUpayWxOrderPend(List<UpayWxOrderReq> upayWxOrderReqs);

    BigDecimal sumUpayWxOrderTotalFee(Map<String, Object> params);

    List<UpayWxOrderPend> listPageByParams(Map<String, Object> shopManagerParams);
}
