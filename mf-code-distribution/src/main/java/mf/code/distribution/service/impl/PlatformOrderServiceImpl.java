package mf.code.distribution.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.repo.dao.PlatformOrderMapper;
import mf.code.distribution.repo.po.PlatformOrder;
import mf.code.distribution.service.PlatformOrderService;
import org.springframework.stereotype.Service;

/**
 * mf.code.distribution.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-30 12:29
 */
@Slf4j
@Service
public class PlatformOrderServiceImpl extends ServiceImpl<PlatformOrderMapper, PlatformOrder> implements PlatformOrderService {
}
