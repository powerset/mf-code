package mf.code.distribution.service;

import mf.code.common.simpleresp.SimpleResponse;

public interface TeamMangerService {
    SimpleResponse newOrdersInSevenDays(Long shopId, Long userId, Long offset,Long limit);

    SimpleResponse finishTasksInSevenDays(Long shopId, Long userId, Long offset, int limit);

    SimpleResponse fansActiveDetail(Long shopId, Long userId, Long fanUserId, Long offset, Long limit);

    SimpleResponse visitorsInSevenDays(Long shopId, Long userId, Long offset, Long limit);
}
