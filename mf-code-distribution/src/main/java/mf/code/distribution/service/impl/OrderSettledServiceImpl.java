package mf.code.distribution.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.repo.dao.OrderSettledMapper;
import mf.code.distribution.repo.po.OrderSettled;
import mf.code.distribution.service.OrderSettledService;
import org.springframework.stereotype.Service;

/**
 * mf.code.distribution.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-05 13:39
 */
@Slf4j
@Service
public class OrderSettledServiceImpl extends ServiceImpl<OrderSettledMapper, OrderSettled> implements OrderSettledService {
}
