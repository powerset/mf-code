package mf.code.distribution.service.platform;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.model.request.platform.ShopManagerSalePolicyRequest;

/**
 * mf.code.distribution.api.platform.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 13:32
 */
public interface PlatformShopManagerService {
    /**
     * 保存 店长缴纳金配置
     *
     * @param shopManagerSalePolicyRequest
     * @return
     */
    SimpleResponse saveSalePolicy(ShopManagerSalePolicyRequest shopManagerSalePolicyRequest);

    /**
     * 获取 店长缴纳金配置
     *
     * @return
     */
    SimpleResponse querySalePolicy();
}
