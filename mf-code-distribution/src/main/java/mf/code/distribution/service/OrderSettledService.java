package mf.code.distribution.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.distribution.repo.po.OrderSettled;

/**
 * mf.code.distribution.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-05 13:39
 */
public interface OrderSettledService extends IService<OrderSettled> {
}
