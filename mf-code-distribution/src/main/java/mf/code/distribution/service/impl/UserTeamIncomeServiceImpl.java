package mf.code.distribution.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.common.redis.RedisKeyConstant;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.repo.dao.UpayWxOrderPendMapper;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.UserTeamIncomeService;
import mf.code.user.constant.*;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 用户收入和支出服务
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-31 21:50
 */
@Slf4j
@Service
public class UserTeamIncomeServiceImpl implements UserTeamIncomeService {
    @Autowired
    private UpayWxOrderPendMapper upayWxOrderPendMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DistributionUserRepository distributionUserRepository;
    @Autowired
    private UserAppService userAppService;

    /**
     * 获取当前店铺下 用户团队的最新一条贡献记录
     *
     * @param userId
     * @param shopId
     * @return
     */
    @Override
    public Map<String, Object> queryLatestIncomeRecord(Long userId, Long shopId) {
        QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayWxOrderPend::getUserId, userId)
                .eq(UpayWxOrderPend::getType, UpayWxOrderPendTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode())
                .eq(UpayWxOrderPend::getBizType, UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode());
        Page<UpayWxOrderPend> page = new Page<>(1, 1);
        page.setDesc("payment_time");
        IPage<UpayWxOrderPend> upayWxOrderPendIPage = upayWxOrderPendMapper.selectPage(page, wrapper);
        if (upayWxOrderPendIPage == null) {
            return null;
        }
        List<UpayWxOrderPend> records = upayWxOrderPendIPage.getRecords();
        if (CollectionUtils.isEmpty(records)) {
            return null;
        }
        UpayWxOrderPend upayWxOrderPend = records.get(0);
        Long suid = upayWxOrderPend.getBizValue();
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, suid);
        if (distributionUser == null) {
            log.error("查询用户出错，userId = {}", suid);
            return null;
        }

        Map<String, Object> latestIncomeInfo = new HashMap<>();
        latestIncomeInfo.put("nickName", distributionUser.getUserInfo().getNickName());
        latestIncomeInfo.put("totalFee", upayWxOrderPend.getTotalFee());

        return latestIncomeInfo;
    }

    /**
     * 获取当前店铺下 用户好友收益排行榜 （上级+用户+下一级）
     *
     * @param distributionUser
     * @param offset
     * @param size
     * @return
     */
    @Override
    public AppletMybatisPageDto queryIncomeRankListOfFriends(DistributionUser distributionUser, Long offset, Long size) {
        log.info("<<<<<<< 开始获取用户好友收益排行榜 <<<<<<<<<<<");
        Long userId = distributionUser.getUserId();
        Long shopId = distributionUser.getShopId();

        List<Long> friendIds = distributionUser.queryFriends();

        // 生成收益为0 的好友收益排行榜，再于与店铺真实收益排行榜进行交集，获取此用户真实的好友收益排行榜数据
        Set<ZSetOperations.TypedTuple<String>> tuples = new HashSet<>();
        for (Long friendId : friendIds) {
            ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(friendId.toString(), 0.0);
            tuples.add(longTypedTuple);
        }
        try {
            stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId, tuples);
            // 和 一个大而全的店铺真实收益排行榜 进行交集，获取此用户真实的好友收益排行榜 RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS +sid +":" +uid
            stringRedisTemplate.opsForZSet().intersectAndStore(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP,
                    RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId,
                    RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
            // 查看交集的数据 是否 完整
            Set<String> range = stringRedisTemplate.opsForZSet().range(
                    RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId, 0, -1);

            // 如果 真实获取到的交集大小 不等于 想要获取的列表大小，则说明 店铺 此用户的数据丢失。进行容错处理

            if (range == null || range.size() != friendIds.size()) {
                // 获取 没有数据的 ids
                List<Long> lostIds = new ArrayList<>();
                if (range == null) {
                    lostIds.addAll(friendIds);
                } else {
                    for (Long friendId : friendIds) {
                        if (range.contains(friendId.toString())) {
                            continue;
                        }
                        lostIds.add(friendId);
                    }
                }
                log.info("<<<<<<<<< lostIds = {} <<<<<<<<<", lostIds);
                if (!CollectionUtils.isEmpty(lostIds)) {
                    // 对 丢失的数据 进行修复: 把缺失的用户添加到 店铺累计收益排行榜中
                    this.addIncomeRankListOfShopByUserIds(lostIds, shopId);
                    // 重新获取一遍 好友收益排行榜
                    stringRedisTemplate.delete(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
                    stringRedisTemplate.opsForZSet().intersectAndStore(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP,
                            RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId,
                            RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
                }
            }
        } finally {
            stringRedisTemplate.delete(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId);
        }

        // 总大小
        Long total = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
        if (total == null) {
            log.error("排行榜异常，total 为空");
            return null;
        }
        // 分页获取 此用户真实的好友收益排行榜
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId, offset, offset + size - 1);
        if (CollectionUtils.isEmpty(typedTuples)) {
            log.error("排行榜异常, 获取排名为空");
            return null;
        }

        List<Long> rankUserIds = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            if (StringUtils.isNotBlank(value)) {
                rankUserIds.add(Long.valueOf(value));
            }
        }

        // 获取排行榜的用户信息
        List<DistributionUser> distributionUsers = distributionUserRepository.listByIds(rankUserIds, shopId);
        Map<Long, DistributionUser> distributionUserMap = new HashMap<>();
        for (DistributionUser dbUser : distributionUsers) {
            distributionUserMap.put(dbUser.getUserId(), dbUser);
        }

        //加工显示数据
        List<Map<String, Object>> rankListVO = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            Double score = typedTuple.getScore();
            if (StringUtils.isBlank(value) || score == null) {
                log.error("获取到的排行榜 值和分数异常，value = {}, score = {}", value, score);
                continue;
            }
            DistributionUser dbUser = distributionUserMap.get(Long.valueOf(value));
            if (dbUser == null) {
                log.error("获取排行榜的 用户信息异常，无效用户 userId = {}", value);
                continue;
            }
            UserResp userInfo = dbUser.getUserInfo();
            BigDecimal income = new BigDecimal(score.toString()).setScale(2, BigDecimal.ROUND_DOWN);

            Map<String, Object> userVO = new HashMap<>();
            userVO.put("userId", dbUser.getUserId());
            userVO.put("avatarUrl", userInfo.getAvatarUrl());
            userVO.put("nickName", userInfo.getNickName());
            userVO.put("income", income);
            userVO.put("role", userInfo.getRole());
            rankListVO.add(userVO);
        }

        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);
        appletMybatisPageDto.setContent(rankListVO);
        if (offset + size < total) {
            appletMybatisPageDto.setPullDown(true);
        }
        return appletMybatisPageDto;
    }

    /**
     * 获取当前店铺下 用户团队贡献榜
     *
     * @param distributionUser
     * @param type             1全部 2已结清 3待结算
     * @param offset
     * @param size
     * @return
     */
    @Override
    public AppletMybatisPageDto queryRankListByTeamContribution(DistributionUser distributionUser, String type, Long offset, Long size) {
        Long userId = distributionUser.getUserId();
        Long shopId = distributionUser.getShopId();
        // 获取待结算 起始时间 默认 待结算收益 取到 上上月1日到现在 的记录
        Date settledTime = distributionUser.getUserIncome().getSettledTime();

        // 如果结算更新时间不为空， 并且结算更新时间大于本月的约定更新时间，说明此用户本月已结算， 则 待结算收益 取到 上月1日到现在的记录
        Date beginTime = DateUtil.getBeginOfLastTwoMonth();
        if (settledTime != null && DateUtil.getMonth(settledTime) == DateUtil.getMonth(new Date())) {
            beginTime = DateUtil.getBeginOfLastMonth();
        }
        // 获取待结算 取值结束时间
        Date endTime = new Date();

        // 获取 用户商城团员 -- 下5级+自己
        List<Long> userShopTeamIds = distributionUser.queryUserShopTeams();
        userShopTeamIds.remove(userId);
        // 生成团队贡献 已结算排行榜
        // 初始化排行榜
        Set<ZSetOperations.TypedTuple<String>> initTuples = new HashSet<>();
        for (Long id : userShopTeamIds) {
            ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(id.toString(), 0.0);
            initTuples.add(longTypedTuple);
        }
        // 排除自己以后,可能是空列表需要直接返回数据
        if (CollectionUtils.isEmpty(initTuples)) {
            AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);
            appletMybatisPageDto.setContent(new ArrayList<>());
            appletMybatisPageDto.setPullDown(false);
            return appletMybatisPageDto;
        }
        stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_SETTLED + shopId + ":" + userId, initTuples);
        stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_PENDING + shopId + ":" + userId, initTuples);
        stringRedisTemplate.expire(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_SETTLED + shopId + ":" + userId, 30, TimeUnit.SECONDS);
        stringRedisTemplate.expire(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_PENDING + shopId + ":" + userId, 30, TimeUnit.SECONDS);

        // 获取 已结算的 团队贡献记录
        // 定义查询参数

        List<Map> settledTeamContribution = userAppService.batchTotalContributionSettledByUserIds(shopId, userShopTeamIds);
        if (settledTeamContribution == null) {
            settledTeamContribution = new ArrayList<>();
        }
        Set<ZSetOperations.TypedTuple<String>> tuples = new HashSet<>();
        for (Map map : settledTeamContribution) {
            ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(map.get("userId").toString(), Double.parseDouble(map.get("income").toString()));
            tuples.add(longTypedTuple);
        }

        // 添加真实收益
        if (!CollectionUtils.isEmpty(tuples)) {
            stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_SETTLED + shopId + ":" + userId, tuples);
        }

        // 生成团队贡献 待结算排行榜
        // 获取 待结算的 团队贡献记录
        // 定义查询参数

        List<Map> pendingTeamContribution = this.batchTotalContributionPendByUserIds(distributionUser.getUserId(), userShopTeamIds, shopId, beginTime, endTime);
        tuples.clear();
        for (Map map : pendingTeamContribution) {
            if (StringUtils.equals(map.get("userId").toString(), distributionUser.getUserId().toString())) {
                continue;
            }
            if (distributionUser.hasReachSpendStandard()) {
                ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(map.get("userId").toString(), Double.parseDouble(map.get("income").toString()));
                tuples.add(longTypedTuple);
            } else {
                ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(map.get("userId").toString(), 0.0);
                tuples.add(longTypedTuple);
            }
        }

        // ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(distributionUser.getUserId().toString(), Double.parseDouble(distributionUser.queryPendingRebateIncome().toString()));
        // tuples.add(longTypedTuple);

        if (!CollectionUtils.isEmpty(tuples)) {
            stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_PENDING + shopId + ":" + userId, tuples);
        }

        // 生成团队贡献 全部排行榜
        stringRedisTemplate.opsForZSet().unionAndStore(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_SETTLED + shopId + ":" + userId,
                RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_PENDING + shopId + ":" + userId,
                RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_TOTAL + shopId + ":" + userId);
        stringRedisTemplate.expire(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_TOTAL + shopId + ":" + userId, 30, TimeUnit.SECONDS);
        Long total = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_TOTAL + shopId + ":" + userId);

        if (total == null) {
            log.error("获取排行榜异常");
            return null;
        }
        Set<ZSetOperations.TypedTuple<String>> typedTuples = null;
        // 根据类型，获取相应的排行榜数据
        int tp = Integer.parseInt(type);
        if (tp == 1) {
            // 分页获取 全部 -- 团队贡献值
            typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                    RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_TOTAL + shopId + ":" + userId, offset, offset + size - 1);
        }

        if (tp == 2) {
            // 分页获取 已结算 -- 团队贡献值
            typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                    RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_SETTLED + shopId + ":" + userId, offset, offset + size - 1);
        }

        if (tp == 3) {
            // 分页获取 待结算 -- 团队贡献值
            typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                    RedisKeyConstant.INCOME_RANK_LIST_OF_TEAM_PENDING + shopId + ":" + userId, offset, offset + size - 1);
        }

        if (typedTuples == null) {
            log.error("获取排行榜类型不符, type = {}", type);
            return null;
        }

        List<Long> rankUserIds = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            if (StringUtils.isNotBlank(value)) {
                rankUserIds.add(Long.valueOf(value));
            }
        }
        if (CollectionUtils.isEmpty(rankUserIds)) {
            log.error("获取排行榜异常");
            return null;
        }
        // 获取排行榜的用户信息
        List<DistributionUser> distributionUsers = distributionUserRepository.listByIds(rankUserIds, shopId);
        Map<Long, DistributionUser> distributionUserMap = new HashMap<>();
        for (DistributionUser dbUser : distributionUsers) {
            distributionUserMap.put(dbUser.getUserId(), dbUser);
        }

        //加工显示数据
        List<Map<String, Object>> rankListVO = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            Double score = typedTuple.getScore();

            if (StringUtils.isBlank(value) || score == null) {
                continue;
            }
            DistributionUser dbUser = distributionUserMap.get(Long.valueOf(value));
            if (dbUser == null) {
                continue;
            }
            UserResp userInfo = dbUser.getUserInfo();
            BigDecimal income = new BigDecimal(score.toString()).setScale(2, BigDecimal.ROUND_DOWN);

            Map<String, Object> userVO = new HashMap<>();
            userVO.put("userId", dbUser.getUserId());
            userVO.put("avatarUrl", userInfo.getAvatarUrl());
            userVO.put("nickName", userInfo.getNickName());
            userVO.put("income", income);
            userVO.put("role", userInfo.getRole());
            rankListVO.add(userVO);
        }

        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);
        appletMybatisPageDto.setContent(rankListVO);

        if (offset + size < total) {
            appletMybatisPageDto.setPullDown(true);
        }
        return appletMybatisPageDto;
    }

    /**
     * 分批获取 此用户 的团队贡献记录 -- 待结算
     *
     * @param userIds
     * @return
     */
    private List<Map> batchTotalContributionPendByUserIds(Long owner, List<Long> userIds, Long shopId, Date beginTime, Date endTime) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", owner);
        // params.put("shopId", shopId);  2.1 迭代 累计所有店铺的待结算贡献
        params.put("type", UpayWxOrderPendTypeEnum.PALTFORMRECHARGE.getCode());
        params.put("bizType", UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode());
        params.put("status", UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode());
        params.put("beginTime", beginTime);
        params.put("endTime", endTime);

        List<Map> sumTotalFeeList = new ArrayList<>();
        // 分批查询 -- 每次 100
        int batchCount = 100;
        int sourListSize = userIds.size();
        int subCount = sourListSize % batchCount == 0 ? sourListSize / batchCount : sourListSize / batchCount + 1;
        int startIndext = 0;
        int stopIndext = 0;
        for (int i = 0; i < subCount; i++) {
            stopIndext = (i == subCount - 1) ? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
            List<Long> tempList = new ArrayList<>(userIds.subList(startIndext, stopIndext));
            if (tempList.size() > 0) {
                params.put("bizValues", tempList);
                List<Map> pendings = upayWxOrderPendMapper.batchTotalContributionPendByUserIds(params);
                if (pendings == null) {
                    pendings = new ArrayList<>();
                }
                sumTotalFeeList.addAll(pendings);
            }
            startIndext = stopIndext;
        }

        return sumTotalFeeList;
    }

    /**
     * 把用户添加到 店铺累计收益排行榜
     *
     * @param userIds
     * @param shopId
     */
    @Override
    public void addIncomeRankListOfShopByUserIds(List<Long> userIds, Long shopId) {
        List<String> userIdsStr = new ArrayList<>();
        for (Long userId : userIds) {
            userIdsStr.add(userId.toString());
        }
        stringRedisTemplate.opsForZSet().remove(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP, userIdsStr.toArray());


        // 批量获取 分销用户信息 -- 带收益信息
        List<DistributionUser> distributionUsers = distributionUserRepository.listByIds(userIds, shopId);
        if (CollectionUtils.isEmpty(distributionUsers)) {
            log.info("<<<<<<< 批量获取 分销用户 -- 带收益信息 为空 <<<<<<<<<<");
            return;
        }
        distributionUserRepository.listAddUserDistributionHistory(distributionUsers, shopId);
        distributionUserRepository.listAddUserOrderHistory(distributionUsers);
        distributionUserRepository.listAddUserIncomeTask(distributionUsers);

        // 添加 店铺的 用户收益排行榜
        Set<ZSetOperations.TypedTuple<String>> tuples = new HashSet<>();
        for (DistributionUser distributionUser : distributionUsers) {
            log.info("<<<<<<<< 生成收益排行榜， 处理中的用户信息 = {} <<<<<<<<<<", distributionUser);
            ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(distributionUser.getUserId().toString(), Double.valueOf(distributionUser.queryTotalIncome().toString()));
            tuples.add(longTypedTuple);
        }
        stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP, tuples);
        // stringRedisTemplate.expire(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP + shopId, 30, TimeUnit.SECONDS);
    }

    /**
     * 获取 全球收益排行版
     *
     * @param distributionUser
     * @param offset
     * @param size
     * @return
     */
    @Override
    public AppletMybatisPageDto queryIncomeRankListOfWorld(DistributionUser distributionUser, Long offset, Long size) {
        Long userId = distributionUser.getUserId();
        Long shopId = distributionUser.getShopId();

        List<Long> friendIds = distributionUser.queryFriends();

        // 生成收益为0 的好友收益排行榜，再于与店铺真实收益排行榜进行交集，获取此用户真实的好友收益排行榜数据
        Set<ZSetOperations.TypedTuple<String>> tuples = new HashSet<>();
        for (Long friendId : friendIds) {
            ZSetOperations.TypedTuple<String> longTypedTuple = new DefaultTypedTuple<>(friendId.toString(), 0.0);
            tuples.add(longTypedTuple);
        }
        stringRedisTemplate.opsForZSet().add(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId, tuples);
        stringRedisTemplate.expire(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId, 3, TimeUnit.SECONDS);
        // 和 一个大而全的店铺真实收益排行榜 进行交集，获取此用户真实的好友收益排行榜 RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS +sid +":" +uid
        stringRedisTemplate.opsForZSet().intersectAndStore(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP,
                RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId,
                RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
        // 查看交集的数据 是否 完整
        Set<String> range = stringRedisTemplate.opsForZSet().range(
                RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId, 0, -1);

        // 如果 真实获取到的交集大小 不等于 想要获取的列表大小，则说明 店铺 此用户的数据丢失。进行容错处理
        if (range == null || range.size() != friendIds.size()) {
            // 获取 没有数据的 ids
            List<Long> lostIds = new ArrayList<>();
            if (range == null) {
                lostIds.addAll(friendIds);
            } else {
                for (Long friendId : friendIds) {
                    if (range.contains(friendId.toString())) {
                        continue;
                    }
                    lostIds.add(friendId);
                }
            }
            if (!CollectionUtils.isEmpty(lostIds)) {
                // 对 丢失的数据 进行修复: 把缺失的用户添加到 店铺累计收益排行榜中
                this.addIncomeRankListOfShopByUserIds(lostIds, shopId);
                // 重新获取一遍 好友收益排行榜
                stringRedisTemplate.delete(RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
                stringRedisTemplate.opsForZSet().intersectAndStore(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP,
                        RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS_TEMP + userId,
                        RedisKeyConstant.INCOME_RANK_LIST_OF_FRIENDS + userId);
            }
        }

        // 总大小
        Long total = stringRedisTemplate.opsForZSet().size(RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP + userId);
        if (total == null) {
            log.error("排行榜异常，total 为空");
            return null;
        }
        // 分页获取 此用户真实的好友收益排行榜
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(
                RedisKeyConstant.INCOME_RANK_LIST_OF_SHOP, offset, offset + size - 1);
        if (CollectionUtils.isEmpty(typedTuples)) {
            log.error("排行榜异常, 获取排名为空");
            return null;
        }

        List<Long> rankUserIds = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            if (StringUtils.isNotBlank(value)) {
                rankUserIds.add(Long.valueOf(value));
            }
        }

        // 获取排行榜的用户信息
        List<DistributionUser> distributionUsers = distributionUserRepository.listByIds(rankUserIds, shopId);
        Map<Long, DistributionUser> distributionUserMap = new HashMap<>();
        for (DistributionUser dbUser : distributionUsers) {
            distributionUserMap.put(dbUser.getUserId(), dbUser);
        }

        //加工显示数据
        List<Map<String, Object>> rankListVO = new ArrayList<>();
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            Double score = typedTuple.getScore();
            if (StringUtils.isBlank(value) || score == null) {
                continue;
            }
            DistributionUser dbUser = distributionUserMap.get(Long.valueOf(value));
            if (dbUser == null) {
                continue;
            }
            UserResp userInfo = dbUser.getUserInfo();
            BigDecimal income = new BigDecimal(score.toString()).setScale(2, BigDecimal.ROUND_DOWN);

            Map<String, Object> userVO = new HashMap<>();
            userVO.put("userId", dbUser.getUserId());
            userVO.put("avatarUrl", userInfo.getAvatarUrl());
            userVO.put("nickName", userInfo.getNickName());
            userVO.put("income", income);
            userVO.put("role", userInfo.getRole());
            rankListVO.add(userVO);
        }

        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);
        appletMybatisPageDto.setContent(rankListVO);
        if (offset + size < total) {
            appletMybatisPageDto.setPullDown(true);
        }
        return appletMybatisPageDto;
    }
}
