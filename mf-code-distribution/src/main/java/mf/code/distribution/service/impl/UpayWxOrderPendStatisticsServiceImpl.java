package mf.code.distribution.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.repo.dao.UpayWxOrderPendMapper;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.service.UpayWxOrderPendStatisticsService;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderPendBizTypeEnum;
import mf.code.user.constant.UpayWxOrderPendStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * UpayWxOrderPend 的 统计服务层； 同 UpayWxOrderPendService
 * mf.code.distribution.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-20 09:09
 */
@Service
public class UpayWxOrderPendStatisticsServiceImpl implements UpayWxOrderPendStatisticsService {
	@Autowired
	private UpayWxOrderPendMapper upayWxOrderPendMapper;

	/**
	 * 获取用户 所有待结算 返利的总额
	 * @return
	 */
	@Override
	public BigDecimal sumRebateAmountPend(Date upayBalanceUtime, Long userId) {

		BigDecimal rebateAmountPendLastMonth = this.sumRebateAmountPendLastMonth(upayBalanceUtime, userId);
		BigDecimal rebateAmountPendThisMonth = this.sumRebateAmountPendThisMonth(userId);
		return rebateAmountPendLastMonth.add(rebateAmountPendThisMonth);
	}

	/**
	 * 获取用户 本月待结算 返利的总额
	 * @return
	 */
	@Override
	public BigDecimal sumRebateAmountPendThisMonth(Long userId) {

		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("bizType", UpayWxOrderPendBizTypeEnum.REBATE.getCode());
		params.put("status", UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode());
		params.put("paymentTimeBegin", DateUtil.getBeginOfThisMonth());
		params.put("paymentTimeEnd", new Date());
		return upayWxOrderPendMapper.sumUpayWxOrderTotalFee(params);
	}

	/**
	 * 获取用户 上月待结算 返利的总额
	 * @return
	 */
	@Override
	public BigDecimal sumRebateAmountPendLastMonth(Date upayBalanceUtime, Long userId) {

		if (upayBalanceUtime != null && DateUtil.getMonth(upayBalanceUtime) == DateUtil.getMonth(new Date())) {
			// 本月20日之后 已结算上月所有待结算订单 -- 所以上月待结算记录为空
			return BigDecimal.ZERO;
		}

		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("bizType", UpayWxOrderPendBizTypeEnum.REBATE.getCode());
		params.put("status", UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode());
		params.put("paymentTimeBegin", DateUtil.getBeginOfLastMonth());
		params.put("paymentTimeEnd", DateUtil.getEndOfLastMonth());
		return upayWxOrderPendMapper.sumUpayWxOrderTotalFee(params);
	}

	/**
	 * 获取用户 所有待结算 分佣的总额
	 * @return
	 */
	@Override
	public BigDecimal sumContributionAmountPend(Date upayBalanceUtime, Long userId) {

		BigDecimal rebateAmountPendLastMonth = this.sumContributionAmountPendLastMonth(upayBalanceUtime, userId);
		BigDecimal rebateAmountPendThisMonth = this.sumContributionAmountPendThisMonth(userId);
		return rebateAmountPendLastMonth.add(rebateAmountPendThisMonth);
	}

	/**
	 * 获取用户 本月待结算 分佣的总额
	 * @return
	 */
	@Override
	public BigDecimal sumContributionAmountPendThisMonth(Long userId) {

		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("bizType", UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode());
		params.put("status", UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode());
		params.put("paymentTimeBegin", DateUtil.getBeginOfThisMonth());
		params.put("paymentTimeEnd", new Date());
		return upayWxOrderPendMapper.sumUpayWxOrderTotalFee(params);
	}

	/**
	 * 获取用户 上月待结算 分佣的总额
	 * @return
	 */
	@Override
	public BigDecimal sumContributionAmountPendLastMonth(Date upayBalanceUtime, Long userId) {

		if (upayBalanceUtime != null && DateUtil.getMonth(upayBalanceUtime) == DateUtil.getMonth(new Date())) {
			// 本月20日之后 已结算上月所有待结算订单 -- 所以上月待结算记录为空
			return BigDecimal.ZERO;
		}

		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("bizType", UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode());
		params.put("status", UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode());
		params.put("paymentTimeBegin", DateUtil.getBeginOfLastMonth());
		params.put("paymentTimeEnd", DateUtil.getEndOfLastMonth());
		return upayWxOrderPendMapper.sumUpayWxOrderTotalFee(params);
	}

	/**
	 * 分页获取 用户返利 本月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	@Override
	public IPage<UpayWxOrderPend> pageRebateRecordsPendThisMonth(Long userId, Long offset, Long size) {

		QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				// .eq(UpayWxOrderPend::getShopId, shopId)  -- 不分店铺
				.eq(UpayWxOrderPend::getUserId, userId)
				.eq(UpayWxOrderPend::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
				// .eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode()) -- 不分状态
				.eq(UpayWxOrderPend::getBizType, UpayWxOrderBizTypeEnum.REBATE.getCode())
				.between(UpayWxOrderPend::getPaymentTime, DateUtil.getBeginOfThisMonth(), new Date());
		Page<UpayWxOrderPend> page = new Page<>(offset / size + 1, size);
		page.setAsc("id");

		return upayWxOrderPendMapper.selectPage(page, wrapper);
	}

	/**
	 * 分页获取 用户返利 上月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param upayBalanceUtime
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	@Override
	public IPage<UpayWxOrderPend> pageRebateRecordsPendLastMonth(Date upayBalanceUtime, Long userId, Long offset, Long size) {

		if (upayBalanceUtime != null && DateUtil.getMonth(upayBalanceUtime) == DateUtil.getMonth(new Date())) {
			// 本月20日之后 已结算上月所有待结算订单 -- 所以上月待结算记录为空
			return null;
		}

 		QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				// .eq(UpayWxOrderPend::getShopId, shopId)  -- 不分店铺
				.eq(UpayWxOrderPend::getUserId, userId)
				.eq(UpayWxOrderPend::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
				// .eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode()) -- 不分状态
				.eq(UpayWxOrderPend::getBizType, UpayWxOrderBizTypeEnum.REBATE.getCode())
				.between(UpayWxOrderPend::getPaymentTime, DateUtil.getBeginOfLastMonth(), DateUtil.getEndOfLastMonth());
		Page<UpayWxOrderPend> page = new Page<>(offset / size + 1, size);
		page.setAsc("id");

		return upayWxOrderPendMapper.selectPage(page, wrapper);
	}

	/**
	 * 分页获取 用户分佣（分享赚） 本月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	@Override
	public IPage<UpayWxOrderPend> pageContributionRecordsThisPend(Long userId, Long offset, Long size) {

		QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				// .eq(UpayWxOrderPend::getShopId, shopId)  -- 不分店铺
				.eq(UpayWxOrderPend::getUserId, userId)
				.eq(UpayWxOrderPend::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
				// .eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode()) -- 不分状态
				.eq(UpayWxOrderPend::getBizType, UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode())
				.between(UpayWxOrderPend::getPaymentTime, DateUtil.getBeginOfThisMonth(), new Date());
		Page<UpayWxOrderPend> page = new Page<>(offset / size + 1, size);
		page.setAsc("id");

		return upayWxOrderPendMapper.selectPage(page, wrapper);
	}

	/**
	 * 分页获取 用户分佣（分享赚） 上月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	@Override
	public IPage<UpayWxOrderPend> pageContributionRecordsLastPend(Date upayBalanceUtime, Long userId, Long offset, Long size) {

		if (upayBalanceUtime != null && DateUtil.getMonth(upayBalanceUtime) == DateUtil.getMonth(new Date())) {
			// 本月20日之后 已结算上月所有待结算订单 -- 所以上月待结算记录为空
			return null;
		}

		QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				// .eq(UpayWxOrderPend::getShopId, shopId)  -- 不分店铺
				.eq(UpayWxOrderPend::getUserId, userId)
				.eq(UpayWxOrderPend::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
				// .eq(UpayWxOrderPend::getStatus, UpayWxOrderPendStatusEnum.ORDERED_PENDING.getCode()) -- 不分状态
				.eq(UpayWxOrderPend::getBizType, UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode())
				.between(UpayWxOrderPend::getPaymentTime, DateUtil.getBeginOfLastMonth(), DateUtil.getEndOfLastMonth());
		Page<UpayWxOrderPend> page = new Page<>(offset / size + 1, size);
		page.setAsc("id");

		return upayWxOrderPendMapper.selectPage(page, wrapper);
	}


}
