package mf.code.distribution.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.RandomStrUtil;
import mf.code.distribution.common.property.WxpayProperty;
import mf.code.distribution.repo.dao.UpayWxOrderPendMapper;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.service.UpayWxOrderPendService;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.user.constant.JsapiSceneEnum;
import mf.code.user.dto.UpayWxOrderReq;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月24日 14:31
 */
@Slf4j
@Service
public class UpayWxOrderPendServiceImpl extends ServiceImpl<UpayWxOrderPendMapper, UpayWxOrderPend> implements UpayWxOrderPendService {
    @Autowired
    private UpayWxOrderPendMapper upayWxOrderPendMapper;

    /**
     * 对 List<UpayWxOrderReq> 批量入库
     * @param upayWxOrderReqs
     * @return
     */
    @Override
    public boolean saveUpayWxOrderPend(List<UpayWxOrderReq> upayWxOrderReqs) {

        List<UpayWxOrderPend> upayWxOrderPends = new ArrayList<>();
        for (UpayWxOrderReq upayWxOrderReq : upayWxOrderReqs) {
            UpayWxOrderPend upayWxOrderPend = new UpayWxOrderPend();
            BeanUtils.copyProperties(upayWxOrderReq, upayWxOrderPend);
            upayWxOrderPends.add(upayWxOrderPend);
        }
        return this.saveBatch(upayWxOrderPends);
    }

    /***
     * 汇总pend金额
     * @param params
     * @return
     */
    @Override
    public BigDecimal sumUpayWxOrderTotalFee(Map<String, Object> params) {
        return upayWxOrderPendMapper.sumUpayWxOrderTotalFee(params);
    }

    @Override
    public List<UpayWxOrderPend> listPageByParams(Map<String, Object> shopManagerParams) {
        return upayWxOrderPendMapper.listPageByParams(shopManagerParams);
    }
}
