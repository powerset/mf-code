package mf.code.distribution.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.distribution.repo.po.UpayWxOrderPend;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.distribution.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-20 09:04
 */
public interface UpayWxOrderPendStatisticsService {

	/**
	 * 获取用户 所有待结算 返利的总额
	 * @return
	 */
	BigDecimal sumRebateAmountPend(Date upayBalanceUtime, Long userId);

	/**
	 * 获取用户 本月待结算 返利的总额
	 * @return
	 */
	BigDecimal sumRebateAmountPendThisMonth(Long userId);

	/**
	 * 获取用户 上月待结算 返利的总额
	 * @return
	 */
	BigDecimal sumRebateAmountPendLastMonth(Date upayBalanceUtime, Long userId);

	/**
	 * 获取用户 所有待结算 分佣的总额
	 * @return
	 */
	BigDecimal sumContributionAmountPend(Date upayBalanceUtime, Long userId);

	/**
	 * 获取用户 本月待结算 分佣的总额
	 * @return
	 */
	BigDecimal sumContributionAmountPendThisMonth(Long userId);

	/**
	 * 获取用户 上月待结算 分佣的总额
	 * @return
	 */
	BigDecimal sumContributionAmountPendLastMonth(Date upayBalanceUtime, Long userId);

	/**
	 * 分页获取 用户返利 本月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	IPage<UpayWxOrderPend> pageRebateRecordsPendThisMonth(Long userId, Long offset, Long size);

	/**
	 * 分页获取 用户返利 上月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param upayBalanceUtime
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	IPage<UpayWxOrderPend> pageRebateRecordsPendLastMonth(Date upayBalanceUtime, Long userId, Long offset, Long size);

	/**
	 * 分页获取 用户分佣（分享赚） 本月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	IPage<UpayWxOrderPend> pageContributionRecordsThisPend(Long userId, Long offset, Long size);

	/**
	 * 分页获取 用户分佣（分享赚） 上月待结算
	 * 2月1日00:00:00 - 2月最后日:23:59:59 产生的待结算收益，在 3月19日23:59:59 清算成 已结算收益
	 *
	 * 结论：
	 * 3月日期在 19日23:59:59 之前，待结算收益 统计日期为：2月1日00:00:00 - now（现在）
	 * 3月日期在 19日23:59:59 之后，待结算收益 统计日期为：3月1日00:00:00 - now（现在）
	 * @param userId
	 * @return
	 *
	 * 查询条件
	 * <per>
	 *     userId
	 *     type = 平台充值
	 *     biz_type = 返利+分佣
	 *     payment_time = 待结算时间
	 * </per>
	 */
	IPage<UpayWxOrderPend> pageContributionRecordsLastPend(Date upayBalanceUtime, Long userId, Long offset, Long size);
}
