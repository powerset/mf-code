package mf.code.distribution.service.templatemessage;

import mf.code.distribution.service.templatemessage.message.TemplateMessageEnum;
import mf.code.distribution.domain.aggregateroot.DistributionUser;

import java.util.Map;

/**
 * 微信消息模板
 * mf.code.api.teacher.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-11 下午1:36
 */
public interface TemplateMessageService {

	/**
	 * 通过传入的模板消息标题 发送通知
	 * @param templateId 消息模板标题：消息模板枚举
	 */
	Map<String, Object> sendTemplateMessage(String templateId, DistributionUser distributionUser, Map<String, Object> data);

	Map<String, Object> sendTemplateMessage(String templateId, Long shopId, Long userId, Map<String, Object> data);
}
