package mf.code.distribution.service;

import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.scheduling.annotation.Async;

/**
 * mf.code.distribution.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-01 10:04
 */
public interface DataFixService {

	@Async
	void distributionPlatformOrderPendFix();

	@Async
	void distributionPlatformOrderFix();
}
