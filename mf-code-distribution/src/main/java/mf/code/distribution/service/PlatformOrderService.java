package mf.code.distribution.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.distribution.repo.po.PlatformOrder;

/**
 * mf.code.distribution.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-30 12:29
 */
public interface PlatformOrderService extends IService<PlatformOrder> {
}
