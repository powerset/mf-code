package mf.code.distribution.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.applet.feignservice.DistributionAppServiceImpl;
import mf.code.distribution.api.feignclient.*;
import mf.code.distribution.common.property.WxmpProperty;
import mf.code.distribution.common.redis.RedisKeyConstant;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.constant.PlatformOrderBizTypeEnum;
import mf.code.distribution.constant.PlatformOrderStatusEnum;
import mf.code.distribution.constant.PlatformOrderTypeEnum;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.ShopManagerCommission;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.distribution.feignapi.dto.UserIdListDTO;
import mf.code.distribution.repo.mongo.repository.FanActionRepository;
import mf.code.distribution.repo.po.PlatformOrder;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.*;
import mf.code.distribution.service.templatemessage.TemplateMessageService;
import mf.code.distribution.service.templatemessage.message.TemplateMessageData;
import mf.code.goods.dto.ProductEntity;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.one.dto.ShopManagerCodeDTO;
import mf.code.one.dto.UserTaskDTO;
import mf.code.order.dto.OrderResp;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderMfTradeTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqList;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 19:16
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PromotionPageServiceImpl implements PromotionPageService {
    private final DistributionUserRepository distributionUserRepository;
    private final UserTeamIncomeService userTeamIncomeService;
    private final ShopAppService shopAppService;
    private final StringRedisTemplate stringRedisTemplate;
    private final OneAppService oneAppService;
    private final UpayWxOrderPendStatisticsService upayWxOrderPendStatisticsService;
    private final OrderAppService orderAppService;
    private final GoodsAppService goodsAppService;
    private final UserAppService userAppService;
    private final TemplateMessageService templateMessageService;
    private final WxmpProperty wxmpProperty;
    private final RocketMQTemplate rocketMQTemplate;
    private final FanActionRepository fanActionRepository;
    private final PlatformOrderService platformOrderService;
    private final UpayWxOrderPendService upayWxOrderPendService;

    @Value("${expectFans.value}")
    private String expectFansvalue;

    /**
     * 获取推广收益页--用户收入
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryUserIncome(Long userId, Long shopId, Long offset, Long size) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        // 本月时间段
        Date begin = DateUtil.getBeginOfThisMonth();
        Date now = new Date();
        distributionUserRepository.addUserOrderHistory(distributionUser, begin, now);
        distributionUserRepository.addUserShopTeamForAll(distributionUser);
        distributionUserRepository.addUserDistributionHistory(distributionUser);
        distributionUserRepository.addUserTaskIncome(distributionUser);

        BigDecimal commissionStandard = distributionUser.getUserRebatePolicy().getCommissionStandard();
        BigDecimal spendAmount = distributionUser.querySpendAmount();

        // 页面显示数据
        Map<String, Object> pageVO = new HashMap<>();

        pageVO.put("condition", commissionStandard);
        pageVO.put("spendingAmount", spendAmount);
        pageVO.put("leftAmount", distributionUser.hasReachSpendStandard() ? null : commissionStandard.subtract(spendAmount));
        pageVO.put("settledIncome", distributionUser.querySettledIncome());
        pageVO.put("pendingIncome", distributionUser.queryTotalPendingIncome());
        pageVO.put("totalIncome", distributionUser.queryTotalIncome());
        pageVO.put("estimateIncome", distributionUser.queryEstimateIncome());
        pageVO.put("teamSize", distributionUser.queryTeamSize());
        pageVO.put("rankList", userTeamIncomeService.queryIncomeRankListOfFriends(distributionUser, offset, size));

        return new SimpleResponse(ApiStatusEnum.SUCCESS, pageVO);
    }

    /**
     * 获取推广收益页--用户收入
     *
     * @param userId
     * @param shopId
     * @param type   1.全球收益排行榜 2.好友收益排行榜
     * @param offset
     * @param size
     * @return layerType: 0.无弹窗, 1.无粉丝弹窗, 2.有粉丝弹窗
     */
    @Override
    public SimpleResponse queryUserIncomeV2(Long userId, Long shopId, Integer type, Long offset, Long size) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        // 未成为店长
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("layerType", 0);
        resultVO.put("guideStatus", 0);
        if (!distributionUser.hasShopManagerRole()) {
            // 未成为店长：
            distributionUserRepository.addShopManagerSalePolicy(distributionUser);
            distributionUserRepository.addUserShopTeamForLowerMember(distributionUser);
            // 无粉丝团队
            resultVO.put("layerType", 1);
            resultVO.put("rebateRate", 50 + "%");
            resultVO.put("commissionRate", 20 + "%");
            resultVO.put("shopManagerPrice", 0);
            resultVO.put("salePrice", distributionUser.queryShopManagerSalePrice());
            resultVO.put("originalPrice", distributionUser.queryShopManagerOriginalPrice());
            String leftSize = stringRedisTemplate.opsForValue().get(RedisKeyConstant.SHOPMANAGER_LEFTSIZE + userId);
            if (StringUtils.isBlank(leftSize)) {
                leftSize = "10";
            } else if (Integer.valueOf(leftSize) <= 1) {
                leftSize = "2";
            }
            leftSize = Integer.valueOf(leftSize) - 1 + "";
            resultVO.put("leftSize", leftSize);
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.SHOPMANAGER_LEFTSIZE + userId, leftSize);
            // 已有粉丝团队
            if (distributionUser.queryTeamSize() > 1) {
                resultVO.put("layerType", 2);
                resultVO.put("teamSize", distributionUser.queryTeamSize() - 1);
                resultVO.put("fansValue", distributionUser.queryFansValue());
            }
        }
        // 本月时间段
        Date begin = DateUtil.getBeginOfThisMonth();
        Date now = new Date();
        distributionUserRepository.addUserOrderHistory(distributionUser, begin, now);
        distributionUserRepository.addUserShopTeamForAll(distributionUser);
        distributionUserRepository.addUserDistributionHistory(distributionUser);
        distributionUserRepository.addUserTaskIncome(distributionUser);

        BigDecimal commissionStandard = distributionUser.getUserRebatePolicy().getCommissionStandard();
        BigDecimal spendAmount = distributionUser.querySpendAmount();

        // 页面显示数据
        Map<String, Object> pageVO = new HashMap<>(resultVO);
        pageVO.put("role", distributionUser.getUserInfo().getRole() == null ? 0 : distributionUser.getUserInfo().getRole());
        pageVO.put("condition", commissionStandard);
        pageVO.put("pendingIncome", distributionUser.queryTotalPendingIncome());
        pageVO.put("spendingAmount", spendAmount);
        pageVO.put("leftAmount", distributionUser.hasReachSpendStandard() ? null : commissionStandard.subtract(spendAmount));
        pageVO.put("estimateIncome", distributionUser.queryEstimateIncome());

        pageVO.put("totalIncome", distributionUser.queryTotalIncome());
        pageVO.put("todayIncome", distributionUser.queryTodayIncome());
        pageVO.put("taskAward", distributionUser.getUserTaskIncome().getTotalAward());
        pageVO.put("taskCommission", distributionUser.getUserTaskIncome().getTotalCommission());
        pageVO.put("rebateSettled", distributionUser.getUserIncome().getTotalRebateIncome());
        pageVO.put("rebatePending", distributionUser.queryPendingRebateIncome());
        pageVO.put("contributionSettled", distributionUser.getUserIncome().getContributionIncome());
        pageVO.put("contributionPending", distributionUser.queryTotalContributionPending());
        pageVO.put("teamSize", distributionUser.queryTeamSize() - 1);

        // 联系专属导师
        Long upperId = distributionUser.getUserShopTeam().getUpperMembers().get(0);
        if (upperId == null) {
            String teacherQR = "https://asset.wxyundian.com/public/applet/sixthPashe/DefaultTeacherQR.png";
            Map<String, Object> shopInfo = shopAppService.queryShopInfoByLogin(distributionUser.getUserInfo().getVipShopId().toString());
            if (!CollectionUtils.isEmpty(shopInfo)) {
                Map shop = (Map) shopInfo.get("shopInfo");
                List serviceWx = shop.get("serviceWx") == null ? new ArrayList() : (List) shop.get("serviceWx");
                if (!CollectionUtils.isEmpty(serviceWx)) {
                    teacherQR = JSON.parseObject(JSON.toJSONString(serviceWx.get(0))).getString("pic");
                }
            }
            pageVO.put("teacherQR", teacherQR);
        } else {
            DistributionUser upperUser = distributionUserRepository.findByUserId(upperId);
            String wxCode = upperUser.getUserInfo().getWxCode();
            if (StringUtils.isNotBlank(wxCode)) {
                pageVO.put("teacherQR", wxCode);
            } else {
                pageVO.put("teacherQR", "https://asset.wxyundian.com/public/applet/sixthPashe/DefaultTeacherQR.png");
            }
        }

        if (type == 1) {
            pageVO.put("rankList", userTeamIncomeService.queryIncomeRankListOfWorld(distributionUser, offset, size));
        }
        if (type == 2) {
            pageVO.put("rankList", userTeamIncomeService.queryIncomeRankListOfFriends(distributionUser, offset, size));
        }
        // 今日剩余收益
        Map<String, Object> stringObjectMap = oneAppService.queryNewcomerTaskInfo(distributionUser.getUserInfo().getVipShopId(), userId);
        BigDecimal leftAmount = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(stringObjectMap)) {
            leftAmount = new BigDecimal(stringObjectMap.get("newbieAmount").toString()).add(new BigDecimal(stringObjectMap.get("shopManagerAmount").toString())).add(new BigDecimal(stringObjectMap.get("dailyAmount").toString()));
        }

        String hasTask = "0";
        if (!CollectionUtils.isEmpty(stringObjectMap)) {
            BigDecimal shopManagerAmount = new BigDecimal(stringObjectMap.get("shopManagerAmount").toString());
            if (shopManagerAmount.compareTo(BigDecimal.ZERO) > 0) {
                hasTask = "1";
            }
        }
        pageVO.put("hasTask", hasTask);

        String inviteAward = stringRedisTemplate.opsForValue().get(RedisKeyConstant.SHOP_MANAGER_INVITE_FANS_AWARD + userId);
        if (StringUtils.isNotBlank(inviteAward)) {

            Map<String, Object> inviteDialog = new HashMap<>();
            Map map = JSONObject.parseObject(inviteAward, Map.class);
            if (map != null && map.get("amount") != null) {
                inviteDialog.put("amount", map.get("amount"));
                inviteDialog.put("leftAmount", leftAmount);
                pageVO.put("inviteDialog", inviteDialog);
                stringRedisTemplate.delete(RedisKeyConstant.SHOP_MANAGER_INVITE_FANS_AWARD + userId);
            }
        }

        Long recommendSize = stringRedisTemplate.opsForList().size(RedisKeyConstant.RECOMMEND_SHOP_MANAGER_AWARD + userId);
        recommendSize = recommendSize == null ? 0 : recommendSize;
        if (recommendSize > 0) {
            BigDecimal totalAmount = BigDecimal.ZERO;
            for (int i = 0; i < recommendSize; i++) {
                String amount = stringRedisTemplate.opsForList().rightPop(RedisKeyConstant.RECOMMEND_SHOP_MANAGER_AWARD + userId);
                if (StringUtils.isNotBlank(amount)) {
                    totalAmount = totalAmount.add(new BigDecimal(amount));
                }
            }
            Map<String, Object> recommendDialog = new HashMap<>();
            recommendDialog.put("size", recommendSize);
            recommendDialog.put("amount", totalAmount);
            recommendDialog.put("leftAmount", leftAmount);
            pageVO.put("recommendDialog", recommendDialog);
        }

        String guide = stringRedisTemplate.opsForValue().get(RedisKeyConstant.SHOPMANAGER_GUIDE + userId);
        if (StringUtils.isNotBlank(guide)) {
            pageVO.put("guideStatus", 1);
            stringRedisTemplate.delete(RedisKeyConstant.SHOPMANAGER_GUIDE + userId);
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS, pageVO);
    }

    /**
     * 获取当前店铺下 用户团队贡献榜
     *
     * @param userId
     * @param shopId
     * @param type
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryTeamIncome(Long userId, Long shopId, String type, Long offset, Long size) {

        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        // 本月时间段
        Date begin = DateUtil.getBeginOfThisMonth();
        Date now = new Date();
        distributionUserRepository.addUserOrderHistory(distributionUser, begin, now);
        distributionUserRepository.addUserShopTeamForAll(distributionUser);
        distributionUserRepository.addUserDistributionHistory(distributionUser);

        // 页面显示数据
        Map<String, Object> pageVO = new HashMap<>();
        // 团队店长人数
        UserIdListDTO userIdListDTO = new UserIdListDTO();
        List<Long> fanUserIdList = distributionUser.getUserShopTeam().queryFirstLowerMemberIds();
        fanUserIdList.remove(userId);
        userIdListDTO.setUserIdList(fanUserIdList);
        pageVO.put("shopManagerSize", userAppService.countShopManagerByUserIdList(userIdListDTO));
        List<Long> teamsUserIdList = distributionUser.queryUserShopTeams();
        teamsUserIdList.remove(userId);
        // 最近来过的人数，即最近登录过人数
        long loginCount = fanActionRepository.countUidForTeamManagerSevenDay(shopId, teamsUserIdList, Arrays.asList(FanActionEventEnum.VIEW_GOODS.getCode()));
        pageVO.put("loginSize", loginCount);
        // 最近完成任务的人数
        long taskCount = fanActionRepository.countUidForTeamManagerSevenDay(shopId, distributionUser.getUserShopTeam().queryFirstLowerMemberIds(),
                Arrays.asList(FanActionEventEnum.DAILY_TASK.getCode(),
                        FanActionEventEnum.NEWBIE_TASK.getCode(),
                        FanActionEventEnum.SHOP_MANAGER_TASK.getCode()));
        pageVO.put("finishTaskSize", taskCount);
        // 最近新订单数
        long orderCount = fanActionRepository.countEventIdForTeamManagerSevenDay(shopId, teamsUserIdList,
                Arrays.asList(FanActionEventEnum.ORDER.getCode(), FanActionEventEnum.PAY.getCode()));
        pageVO.put("newOrderSize", orderCount);
        // 粉丝人数
        pageVO.put("fansSize", distributionUser.queryTeamSize() - 1);
        // 上月待结算，TODO 20190704团队管理版本开始不使用
        pageVO.put("pendingIncomeLastMonth", distributionUser.queryPendingIncomeLastMonth());
        // 本月待结算 TODO 20190704团队管理版本开始不使用
        pageVO.put("pendingIncomeThismonth", distributionUser.queryPendingIncomeThisMonth());
        // 贡献排行榜
        pageVO.put("rankList", userTeamIncomeService.queryRankListByTeamContribution(distributionUser, type, offset, size));

        return new SimpleResponse(ApiStatusEnum.SUCCESS, pageVO);
    }

    /***
     * 获取团队粉丝信息
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    @Override
    public SimpleResponse queryTeamNumInfo(Long userId, Long shopId, Long merchantId) {
        //获取店铺信息
        Map<String, Object> shopInfoMap = shopAppService.queryShopInfoForSeller(String.valueOf(shopId));
        if (CollectionUtils.isEmpty(shopInfoMap)) {
            log.error("<<<<<<<<获取店铺信息异常,shopInfoResp:{}", shopInfoMap);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "未获取到店铺信息");
        }
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        distributionUserRepository.addUserShopTeamForLowerMember(distributionUser);

        int teamNum = distributionUser.queryTeamSize() - 1;

        BigDecimal fansValue = new BigDecimal(expectFansvalue).multiply(new BigDecimal(teamNum)).setScale(2, BigDecimal.ROUND_DOWN);

        SimpleResponse simpleResponse = new SimpleResponse();

        Map<String, Object> resp = new HashMap<>();
        resp.put("fans", teamNum);
        resp.put("expectValue", fansValue);
        //判断店铺群是否有码
        boolean existGroupCode = shopInfoMap.get("groupCode") != null && StringUtils.isNotBlank(shopInfoMap.get("groupCode").toString());
        resp.put("groupCode", existGroupCode ? shopInfoMap.get("groupCode").toString() : "");
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /**
     * 成为 店长 的弹窗状态
     *
     * @param userId
     * @return dialogType: 0.无弹窗, 1.无粉丝弹窗, 2.有粉丝弹窗
     */
    @Override
    public SimpleResponse beShopManagerPopup(Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        // 已成为店长
        if (distributionUser.hasShopManagerRole()) {
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("dialogType", 0);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
        }
        // 未成为店长：
        distributionUserRepository.addUserShopTeamForLowerMember(distributionUser);
        // 已有粉丝团队
        if (distributionUser.queryTeamSize() > 1) {
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("dialogType", 2);
            resultVO.put("teamSize", distributionUser.queryTeamSize() - 1);
            resultVO.put("fansValue", distributionUser.queryFansValue());
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
        }
        // 无粉丝团队
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("dialogType", 1);
        resultVO.put("fansValue", 2000);
        resultVO.put("rebateRate", 50 + "%");
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 申请 店长
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse becomeShopManager(Long shopId, Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        boolean success = distributionUser.becomeShopManager();
        if (!success) {
            log.info("已成为店长");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "你已是店长");
        }
        success = distributionUserRepository.saveOrUpdate(distributionUser);
        if (!success) {
            log.error("申请店长,数据库更新数据失败, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
        }
        // 上级获得 粉丝升店长奖励 -- 1.确定上级存在店长任务，且 有获得奖励的资格 --redis
        distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);
        BigDecimal awardRecommend = BigDecimal.ZERO;
        Long upperId = distributionUser.queryUpperId();
        if (upperId != null) {
            DistributionUser pubUser = distributionUserRepository.findByUserId(upperId);
            distributionUserRepository.addUserTaskIncome(pubUser);
            if (pubUser.recommendShopManager()) {
                Collection<UserTaskDTO> values = pubUser.getUserTaskIncome().getUserTaskMap().values();
                Long activityDefId = null;
                for (UserTaskDTO userTaskDTO : values) {
                    activityDefId = userTaskDTO.getActivityDefId();
                }
                String amount = oneAppService.giveAwardRecommendShopManager(upperId, activityDefId);
                if (StringUtils.isNotBlank(amount)) {
                    // 成功推荐粉丝成为店长
                    stringRedisTemplate.opsForList().leftPush(mf.code.common.RedisKeyConstant.RECOMMEND_SHOP_MANAGER_AWARD + upperId, amount);
                    distributionUserRepository.addUserOrderHistory(pubUser, DateUtil.getBeginOfThisMonth(), new Date());
                    String isDialog = "0";
                    if (!pubUser.hasReachSpendStandard()) {
                        isDialog = "1";
                    }
                    awardRecommend = new BigDecimal(amount);
                    Map<String, Object> data = TemplateMessageData.fansBecomeShopManagerAward(pubUser.getUserInfo().getShopId(),
                            upperId, awardRecommend, 29, isDialog, pubUser.getUserInfo().getNickName());
                    templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), pubUser.getUserInfo().getShopId(), upperId, data);
                }
            }
        }
        oneAppService.saveShopManagerCommissionByPend(userId, shopId);
        stringRedisTemplate.delete(RedisKeyConstant.SHOPMANAGER_LEFTSIZE + userId);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.SHOPMANAGER_GUIDE + userId, "1");

        Map<String, Object> stringObjectMap = oneAppService.queryNewcomerTaskInfo(distributionUser.getUserInfo().getVipShopId(), userId);
        String dialogType = "0";
        if (!CollectionUtils.isEmpty(stringObjectMap)) {
            BigDecimal shopManagerAmount = new BigDecimal(stringObjectMap.get("shopManagerAmount").toString());
            if (shopManagerAmount.compareTo(BigDecimal.ZERO) > 0) {
                dialogType = "1";
            }
        }
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("dialogType", dialogType);

        FanActionForMQ fanActionForMQ = new FanActionForMQ();
        fanActionForMQ.setMid(distributionUser.getUserInfo().getMerchantId());
        fanActionForMQ.setSid(distributionUser.getShopId());
        fanActionForMQ.setUid(distributionUser.getUserId());
        fanActionForMQ.setEvent(FanActionEventEnum.SHOP_MANAGER.getCode());
        fanActionForMQ.setEventId(0L);
        fanActionForMQ.setTax(awardRecommend);
        fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
        rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
                JSON.toJSONString(fanActionForMQ), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("成为店长埋点发送成功");
                    }

                    @Override
                    public void onException(Throwable e) {
                        log.info("成为店长埋点发送失败" + JSON.toJSONString(fanActionForMQ));
                    }
                });
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 申请 店长
     *
     * @param upayWxOrderReq
     * @return
     */
    @Override
    public SimpleResponse becomeShopManagerV2(UpayWxOrderReq upayWxOrderReq) {
        Long userId = upayWxOrderReq.getUserId();
        Long shopId = upayWxOrderReq.getShopId();
        BigDecimal totalFee = upayWxOrderReq.getTotalFee();

        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }

        boolean success = distributionUser.becomeShopManager();
        if (!success) {
            log.info("已成为店长");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "你已是店长");
        }
        success = distributionUserRepository.saveOrUpdate(distributionUser);
        if (!success) {
            log.error("申请店长,数据库更新数据失败, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
        }

        // 上级获得 粉丝升店长奖励 -- 1.确定上级存在店长任务，且 有获得奖励的资格 --redis
        distributionUserRepository.addShopManagerSalePolicy(distributionUser);
        distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);

        BigDecimal award = BigDecimal.ZERO;

        Long upperId = distributionUser.queryUpperId();
        // 2.6.1 迭代 付费升店长 缴纳金分配
        ShopManagerCommission shopManagerCommission = distributionUser.calculateShopManagerCommissionForEachPart(totalFee);
        if (shopManagerCommission != null) {

            Date now = new Date();
            // 初始化各部分订单流水
            BigDecimal superiorCommission = shopManagerCommission.getSuperiorCommission();
            BigDecimal shopCommission = shopManagerCommission.getShopCommission();
            BigDecimal platformCommission = shopManagerCommission.getPlatformCommission();

            if (shopCommission != null && shopCommission.compareTo(BigDecimal.ZERO) > 0) {
                UpayWxOrderReq merchantOrder = new UpayWxOrderReq();
                BeanUtils.copyProperties(upayWxOrderReq, merchantOrder);
                merchantOrder.setId(null);
                merchantOrder.setType(mf.code.merchant.constants.OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode());
                merchantOrder.setStatus(mf.code.merchant.constants.OrderStatusEnum.ORDERED.getCode());
                merchantOrder.setTotalFee(shopCommission);
                merchantOrder.setBizType(BizTypeEnum.SHOP_MANAGER_PAY.getCode());
                merchantOrder.setBizValue(upayWxOrderReq.getId());
                merchantOrder.setTradeType(MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode());
                merchantOrder.setCtime(now);
                merchantOrder.setUtime(now);

                List<UpayWxOrderReq> merchantOrderList = new ArrayList<>();
                merchantOrderList.add(merchantOrder);
                UpayWxOrderReqList upayWxOrderReqList = new UpayWxOrderReqList();
                upayWxOrderReqList.setUpayWxOrderReqList(merchantOrderList);
                orderAppService.batchUpdateMerchantBalance(upayWxOrderReqList);
            }

            if (platformCommission != null && platformCommission.compareTo(BigDecimal.ZERO) > 0) {
                PlatformOrder platformOrder = new PlatformOrder();
                BeanUtils.copyProperties(upayWxOrderReq, platformOrder);
                platformOrder.setId(null);
                platformOrder.setType(PlatformOrderTypeEnum.PALTFORM_PRESTORE.getCode());
                platformOrder.setStatus(PlatformOrderStatusEnum.SUCCESS.getCode());
                platformOrder.setTotalFee(platformCommission);
                platformOrder.setBizType(PlatformOrderBizTypeEnum.SHOP_MANAGER_PAY.getCode());
                platformOrder.setBizValue(upayWxOrderReq.getId());
                platformOrder.setPubAppId(upayWxOrderReq.getAppletAppId());
                platformOrder.setCtime(now);
                platformOrder.setUtime(now);

                platformOrderService.save(platformOrder);
            }

            if (superiorCommission != null && superiorCommission.compareTo(BigDecimal.ZERO) > 0) {
                UpayWxOrderReq userPayWxOrder = new UpayWxOrderReq();
                BeanUtils.copyProperties(upayWxOrderReq, userPayWxOrder);
                userPayWxOrder.setId(null);
                userPayWxOrder.setOrderNo(upayWxOrderReq.getOrderNo() + "_付费升店长");
                userPayWxOrder.setType(UpayWxOrderTypeEnum.REBATE.getCode());
                userPayWxOrder.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
                userPayWxOrder.setTotalFee(superiorCommission);
                userPayWxOrder.setBizType(UpayWxOrderBizTypeEnum.SHOP_MANAGER_PAY.getCode());
                userPayWxOrder.setUserId(upperId);
                userPayWxOrder.setBizValue(upayWxOrderReq.getId());
                userPayWxOrder.setTradeType(1);
                userPayWxOrder.setCtime(now);
                userPayWxOrder.setUtime(now);

                List<UpayWxOrderReq> upayWxOrderReqs = new ArrayList<>();
                upayWxOrderReqs.add(userPayWxOrder);

                DistributionUser upperUser = distributionUserRepository.findByUserId(upperId);
                if (upperUser.hasShopManagerRole()) {
                    UpayWxOrderReqList upayWxOrderReqList = new UpayWxOrderReqList();
                    upayWxOrderReqList.setUpayWxOrderReqList(upayWxOrderReqs);
                    userAppService.batchUpdateUserBalance(upayWxOrderReqList);
                } else {
                    upayWxOrderPendService.saveUpayWxOrderPend(upayWxOrderReqs);
                }
                award = award.add(superiorCommission);
                distributionUserRepository.addUserOrderHistory(upperUser, DateUtil.getBeginOfThisMonth(), new Date());
                int scene = 28;
                String isDialog = "0";
                if (upperUser.hasShopManagerRole()) {
                    scene = 39;
                    if (upperUser.hasReachSpendStandard()) {
                        isDialog = "0";
                    }
                }
                Map<String, Object> data = TemplateMessageData.fansBecomeShopManagerAward(upperUser.getUserInfo().getShopId(),
                        upperId, superiorCommission, scene, isDialog, distributionUser.getUserInfo().getNickName());
                templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), upperUser.getUserInfo().getShopId(), upperId, data);
            }
        }

        BigDecimal awardRecommend = BigDecimal.ZERO;
        if (upperId != null) {
            // 原逻辑
            DistributionUser pubUser = distributionUserRepository.findByUserId(upperId);
            distributionUserRepository.addUserTaskIncome(pubUser);
            if (pubUser.recommendShopManager()) {
                Collection<UserTaskDTO> values = pubUser.getUserTaskIncome().getUserTaskMap().values();
                Long activityDefId = null;
                for (UserTaskDTO userTaskDTO : values) {
                    activityDefId = userTaskDTO.getActivityDefId();
                }
                String amount = oneAppService.giveAwardRecommendShopManager(upperId, activityDefId);
                if (StringUtils.isNotBlank(amount)) {
                    // 成功推荐粉丝成为店长
                    award = award.add(new BigDecimal(amount));
                    distributionUserRepository.addUserOrderHistory(pubUser, DateUtil.getBeginOfThisMonth(), new Date());
                    int scene = 28;
                    String isDialog = "0";
                    if (pubUser.hasShopManagerRole()) {
                        scene = 39;
                        if (pubUser.hasReachSpendStandard()) {
                            isDialog = "0";
                        }
                    }
                    awardRecommend = new BigDecimal(amount);
                    Map<String, Object> data = TemplateMessageData.fansBecomeShopManagerAward(pubUser.getUserInfo().getShopId(),
                            upperId, awardRecommend, scene, isDialog, distributionUser.getUserInfo().getNickName());
                    templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), pubUser.getUserInfo().getShopId(), upperId, data);
                }
            }
        }
        if (award.compareTo(BigDecimal.ZERO) > 0) {
            stringRedisTemplate.opsForList().leftPush(mf.code.common.RedisKeyConstant.RECOMMEND_SHOP_MANAGER_AWARD + upperId, award.toString());
        }
        oneAppService.saveShopManagerCommissionByPend(userId, shopId);
        stringRedisTemplate.delete(RedisKeyConstant.SHOPMANAGER_LEFTSIZE + userId);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.SHOPMANAGER_GUIDE + userId, "1");

        Map<String, Object> stringObjectMap = oneAppService.queryNewcomerTaskInfo(distributionUser.getUserInfo().getVipShopId(), userId);
        String dialogType = "0";
        if (!CollectionUtils.isEmpty(stringObjectMap)) {
            BigDecimal shopManagerAmount = new BigDecimal(stringObjectMap.get("shopManagerAmount").toString());
            if (shopManagerAmount.compareTo(BigDecimal.ZERO) > 0) {
                dialogType = "1";
            }
        }
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("dialogType", dialogType);

        FanActionForMQ fanActionForMQ = new FanActionForMQ();
        fanActionForMQ.setMid(distributionUser.getUserInfo().getMerchantId());
        fanActionForMQ.setSid(distributionUser.getShopId());
        fanActionForMQ.setUid(distributionUser.getUserId());
        fanActionForMQ.setEvent(FanActionEventEnum.SHOP_MANAGER.getCode());
        fanActionForMQ.setEventId(0L);
        fanActionForMQ.setTax(awardRecommend);
        fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
        rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
                JSON.toJSONString(fanActionForMQ), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("成为店长埋点发送成功");
                    }

                    @Override
                    public void onException(Throwable e) {
                        log.info("成为店长埋点发送失败" + JSON.toJSONString(fanActionForMQ));
                    }
                });
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 获取商学院页
     *
     * @param userId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse queryBusinessSchoolPage(Long userId, Long shopId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        if (!distributionUser.hasShopManagerRole()) {
            log.info("请先升级成为店长");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请先升级成为店长");
        }
        Map<String, Object> shopMap = shopAppService.queryShopInfoForSeller(distributionUser.getUserInfo().getVipShopId().toString());
        if (CollectionUtils.isEmpty(shopMap)) {
            log.error("无效店铺，shopId = {}", distributionUser.getUserInfo().getVipShopId().toString());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "无效店铺");
        }
        Map<String, Object> resultVO = new HashMap<>();
        Object groupCode = shopMap.get("groupCode");
        resultVO.put("groupCode", groupCode == null || StringUtils.isBlank(groupCode.toString()) ? "https://asset.wxyundian.com/public/applet/sixthPashe/DefaultMerchantGroupQR.jpg" : groupCode);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 上传 店长二维码
     *
     * @param userId
     * @param uploadShopManagerQR
     * @return
     */
    @Override
    public SimpleResponse uploadShopManagerQR(Long shopId, Long userId, Map<String, String> uploadShopManagerQR) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        ShopManagerCodeDTO shopManagerCodeDTO = new ShopManagerCodeDTO();
        shopManagerCodeDTO.setUserId(userId);
        shopManagerCodeDTO.setGroupCodePath(uploadShopManagerQR.get("groupQR"));
        String url = oneAppService.saveGroupCodeForShopManager(shopManagerCodeDTO);
        if (StringUtils.isBlank(url)) {
            log.error("上传店长群二维码,数据库更新数据失败, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "请上传正确的二维码图片");
        }
        shopManagerCodeDTO.setGroupCodePath(uploadShopManagerQR.get("personalQR"));
        String personalQR = oneAppService.getWxQRFromPicUrl(shopManagerCodeDTO);
        if (StringUtils.isBlank(personalQR)) {
            log.error("获取店长个人二维码,数据库更新数据失败, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "请上传正确的二维码图片");
        }
        distributionUser.uploadShopManagerQR(personalQR);
        boolean success = distributionUserRepository.saveOrUpdate(distributionUser);
        if (!success) {
            log.error("上传店长个人二维码,数据库更新数据失败, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
        }
        Map<String, Object> resultVO = giveAwardUploadShopManagerQR(distributionUser, shopId);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    private Map<String, Object> giveAwardUploadShopManagerQR(DistributionUser distributionUser, Long shopId) {
        distributionUserRepository.addUserTaskIncome(distributionUser);
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("dialogStatus", 0);
        if (distributionUser.canGetUploadQRAward()) {
            Collection<UserTaskDTO> values = distributionUser.getUserTaskIncome().getUserTaskMap().values();
            Long activityDefId = null;
            for (UserTaskDTO userTaskDTO : values) {
                activityDefId = userTaskDTO.getActivityDefId();
            }
            String amount = oneAppService.giveAwardUploadShopManagerQR(distributionUser.getUserId(), activityDefId);
            if (StringUtils.isNotBlank(amount)) {
                Map<String, Object> stringObjectMap = oneAppService.queryNewcomerTaskInfo(shopId, distributionUser.getUserId());
                BigDecimal leftAmount = BigDecimal.ZERO;
                if (!CollectionUtils.isEmpty(stringObjectMap)) {
                    leftAmount = new BigDecimal(stringObjectMap.get("newbieAmount").toString())
                            .add(new BigDecimal(stringObjectMap.get("shopManagerAmount").toString()))
                            .add(new BigDecimal(stringObjectMap.get("dailyAmount").toString()));
                }
                resultVO.put("dialogStatus", 1);
                Map<String, Object> wxCodeDialog = new HashMap<>();
                wxCodeDialog.put("amount", amount);
                wxCodeDialog.put("leftAmount", leftAmount);
                resultVO.put("wxCodeDialog", wxCodeDialog);
            }
        }
        return resultVO;
    }

    /**
     * 获取待结算订单明细页
     *
     * @param userId
     * @param shopId
     * @param bizType
     * @param dateType
     * @return
     */
    @Override
    public SimpleResponse queryPendRecordsPage(Long userId, Long shopId, Integer bizType, Integer dateType, Long offset, Long size) {
        if (bizType == 1 && dateType == 1) {
            // 自购省 本月待结算 页
            return this.queryRebatePendThisMonthRecordsPage(userId, shopId, offset, size);
        }
        if (bizType == 1 && dateType == 2) {
            // 自购省 上月待结算 页
            return this.queryRebatePendLastMonthRecordsPage(userId, shopId, offset, size);
        }
        if (bizType == 2 && dateType == 1) {
            // 分享赚 本月待结算 页
            return this.queryContributionThisMonthRecordsPage(userId, shopId, offset, size);
        }
        if (bizType == 2 && dateType == 2) {
            // 分享赚 上月待结算 页
            return this.queryContributionLastMonthRecordsPage(userId, shopId, offset, size);
        }
        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "type or dateType 参数异常");
    }

    /**
     * 领取店长任务
     *
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse doShopManagerTask(Long userId, Long shopId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        if (!distributionUser.getUserInfo().getVipShopId().equals(shopId)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请返回我的店铺领取任务");
        }
        // 成为店长， 此处扣库存，--- 扣库存成功 标识，此店长有店长任务
        Long activityDefId = oneAppService.getShopManagerTask(distributionUser.getUserId(), distributionUser.getShopId());
        if (activityDefId == null || activityDefId == 0L) {
            log.info("店长任务，库存不足");
            String message = activityDefId == null ? "啊哦，商家店长任务正在上线哦~~" : "点击太快，请稍候再试";
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), message);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    /**
     * 展示上传 二维码页
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse showShopManagerQR(Long merchantId, Long shopId, Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        if (distributionUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("personalQR", distributionUser.getUserInfo().getWxCode());
        resultVO.put("groupQR", oneAppService.getGroupCodeByUserId(userId));
        resultVO.put("dialogStatus", 0);
        if (StringUtils.isNotBlank(distributionUser.getUserInfo().getWxCode())) {
            resultVO.putAll(giveAwardUploadShopManagerQR(distributionUser, shopId));
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 分享赚 本月待结算 页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    private SimpleResponse queryContributionThisMonthRecordsPage(Long userId, Long shopId, Long offset, Long size) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户 userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
        }
        distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());

        BigDecimal thisMonthAmount = upayWxOrderPendStatisticsService.sumContributionAmountPendThisMonth(userId);
        BigDecimal lastMonthAmount = upayWxOrderPendStatisticsService.sumContributionAmountPendLastMonth(distributionUser.getUserIncome().getSettledTime(), userId);
        if (!distributionUser.hasReachSpendStandardByLastSpendAmount()) {
            lastMonthAmount = BigDecimal.ZERO;
        }

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("totalAmount", thisMonthAmount.add(lastMonthAmount));
        resultVO.put("thisMonthAmount", thisMonthAmount);
        resultVO.put("LastMonthAmount", lastMonthAmount);
        resultVO.put("condition", distributionUser.getUserRebatePolicy().getCommissionStandard());
        resultVO.put("reachStatus", 0);
        if (distributionUser.hasReachSpendStandard()) {
            resultVO.put("reachStatus", 1);
        }

        // 获取返利信息
        IPage<UpayWxOrderPend> upayWxOrderPendIPage = upayWxOrderPendStatisticsService.pageContributionRecordsThisPend(userId, offset, size);
        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = this.pageContributionPendRecordsVO(upayWxOrderPendIPage, offset, size);
        resultVO.put("list", appletMybatisPageDto);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 分享赚 上月待结算 页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    private SimpleResponse queryContributionLastMonthRecordsPage(Long userId, Long shopId, Long offset, Long size) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户 userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
        }
        distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());

        BigDecimal thisMonthAmount = upayWxOrderPendStatisticsService.sumContributionAmountPendThisMonth(userId);
        BigDecimal lastMonthAmount = upayWxOrderPendStatisticsService.sumContributionAmountPendLastMonth(distributionUser.getUserIncome().getSettledTime(), userId);

        if (!distributionUser.hasReachSpendStandardByLastSpendAmount()) {
            lastMonthAmount = BigDecimal.ZERO;
        }

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("totalAmount", thisMonthAmount.add(lastMonthAmount));
        resultVO.put("thisMonthAmount", thisMonthAmount);
        resultVO.put("LastMonthAmount", lastMonthAmount);

        // 获取返利信息
        IPage<UpayWxOrderPend> upayWxOrderPendIPage = null;
        if (lastMonthAmount.compareTo(BigDecimal.ZERO) != 0) {
            // 获取返利信息
            upayWxOrderPendIPage = upayWxOrderPendStatisticsService.pageContributionRecordsLastPend(distributionUser.getUserIncome().getSettledTime(), userId, offset, size);
        }

        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = this.pageContributionPendRecordsVO(upayWxOrderPendIPage, offset, size);
        resultVO.put("list", appletMybatisPageDto);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    private AppletMybatisPageDto<Map<String, Object>> pageContributionPendRecordsVO(IPage<UpayWxOrderPend> upayWxOrderPendIPage, Long offset, Long size) {
        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);

        if (upayWxOrderPendIPage == null || CollectionUtils.isEmpty(upayWxOrderPendIPage.getRecords())) {
            return appletMybatisPageDto;
        }
        List<UpayWxOrderPend> records = upayWxOrderPendIPage.getRecords();
        List<Long> userIds = new ArrayList<>();
        for (UpayWxOrderPend upayWxOrderpend : records) {
            userIds.add(upayWxOrderpend.getBizValue());
        }
        // 获取返利的订单信息
        List<UserResp> userRespList = userAppService.listByIds(userIds);
        if (CollectionUtils.isEmpty(userRespList)) {
            log.error("获取用户信息异常");
            return appletMybatisPageDto;
        }
        Map<Long, UserResp> userRespMap = new HashMap<>();
        for (UserResp userResp : userRespList) {
            userRespMap.put(userResp.getId(), userResp);
        }

        List<Map<String, Object>> content = new ArrayList<>();
        for (UpayWxOrderPend upayWxOrderPend : records) {
            Map<String, Object> contentMap = new HashMap<>();
            UserResp userResp = userRespMap.get(upayWxOrderPend.getBizValue());
            if (userResp == null) {
                continue;
            }

            contentMap.put("title", "团员" + userResp.getNickName() + "购买商品贡献");
            contentMap.put("indexName", "团队贡献");
            contentMap.put("time", DateUtil.dateToString(upayWxOrderPend.getPaymentTime(), DateUtil.POINT_DATE_FORMAT_TWO));
            contentMap.put("price", "+" + upayWxOrderPend.getTotalFee());
            contentMap.put("tips", DateUtil.dateToString(DateUtil.getBeginThe20thOfNextMonth(upayWxOrderPend.getPaymentTime()), DateUtil.SHORT_DATE_FORMAT_POINT) + "可结算");
            contentMap.put("status", upayWxOrderPend.getStatus());

            content.add(contentMap);
        }
        appletMybatisPageDto.setContent(content);
        if (offset + size < upayWxOrderPendIPage.getTotal()) {
            appletMybatisPageDto.setPullDown(true);
        }
        return appletMybatisPageDto;
    }

    /**
     * 自购省 上月待结算 页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    private SimpleResponse queryRebatePendLastMonthRecordsPage(Long userId, Long shopId, Long offset, Long size) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户 userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
        }

        BigDecimal thisMonthAmount = upayWxOrderPendStatisticsService.sumRebateAmountPendThisMonth(userId);
        BigDecimal lastMonthAmount = upayWxOrderPendStatisticsService.sumRebateAmountPendLastMonth(distributionUser.getUserIncome().getSettledTime(), userId);

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("totalAmount", thisMonthAmount.add(lastMonthAmount));
        resultVO.put("thisMonthAmount", thisMonthAmount);
        resultVO.put("LastMonthAmount", lastMonthAmount);

        // 获取返利信息
        IPage<UpayWxOrderPend> upayWxOrderPendIPage = upayWxOrderPendStatisticsService.pageRebateRecordsPendLastMonth(distributionUser.getUserIncome().getSettledTime(), userId, offset, size);
        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = this.pageRebatePendRecordsVO(upayWxOrderPendIPage, offset, size);
        resultVO.put("list", appletMybatisPageDto);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /**
     * 自购省 本月待结算 页
     *
     * @param userId
     * @param shopId
     * @return
     */
    private SimpleResponse queryRebatePendThisMonthRecordsPage(Long userId, Long shopId, Long offset, Long size) {
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("无效用户 userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
        }

        BigDecimal thisMonthAmount = upayWxOrderPendStatisticsService.sumRebateAmountPendThisMonth(userId);
        BigDecimal lastMonthAmount = upayWxOrderPendStatisticsService.sumRebateAmountPendLastMonth(distributionUser.getUserIncome().getSettledTime(), userId);

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("totalAmount", thisMonthAmount.add(lastMonthAmount));
        resultVO.put("thisMonthAmount", thisMonthAmount);
        resultVO.put("LastMonthAmount", lastMonthAmount);

        // 获取返利信息
        IPage<UpayWxOrderPend> upayWxOrderPendIPage = upayWxOrderPendStatisticsService.pageRebateRecordsPendThisMonth(userId, offset, size);
        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = this.pageRebatePendRecordsVO(upayWxOrderPendIPage, offset, size);
        resultVO.put("list", appletMybatisPageDto);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    private AppletMybatisPageDto<Map<String, Object>> pageRebatePendRecordsVO(IPage<UpayWxOrderPend> upayWxOrderPendIPage, Long offset, Long size) {
        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);

        if (upayWxOrderPendIPage == null || CollectionUtils.isEmpty(upayWxOrderPendIPage.getRecords())) {
            return appletMybatisPageDto;
        }
        List<UpayWxOrderPend> records = upayWxOrderPendIPage.getRecords();

        // 获取返利的订单信息
        List<Long> orderIds = new ArrayList<>();
        for (UpayWxOrderPend upayWxOrderpend : records) {
            orderIds.add(upayWxOrderpend.getBizValue());
        }
        List<OrderResp> orderResps = orderAppService.listByIds(orderIds);
        if (CollectionUtils.isEmpty(orderResps)) {
            log.error("获取订单信息异常");
            return appletMybatisPageDto;
        }
        Map<Long, OrderResp> orderRespMap = new HashMap<>();

        // 获取商品信息
        List<Long> goodsIds = new ArrayList<>();
        for (OrderResp orderResp : orderResps) {
            orderRespMap.put(orderResp.getId(), orderResp);
            goodsIds.add(orderResp.getGoodsId());
        }
        List<ProductEntity> productDTOList = goodsAppService.listByIds(goodsIds);
        if (CollectionUtils.isEmpty(productDTOList)) {
            log.error("获取商品信息异常");
            return appletMybatisPageDto;
        }
        Map<Long, ProductEntity> goodProductDTOMap = new HashMap<>();
        for (ProductEntity goodsProductDTO : productDTOList) {
            goodProductDTOMap.put(goodsProductDTO.getId(), goodsProductDTO);
        }

        List<Map<String, Object>> content = new ArrayList<>();
        for (UpayWxOrderPend upayWxOrderPend : records) {
            Map<String, Object> contentMap = new HashMap<>();
            OrderResp orderResp = orderRespMap.get(upayWxOrderPend.getBizValue());
            if (orderResp == null) {
                continue;
            }
            ProductEntity goodProductDTO = goodProductDTOMap.get(orderResp.getGoodsId());
            if (goodProductDTO == null) {
                continue;
            }

            contentMap.put("title", goodProductDTO.getProductTitle());
            contentMap.put("indexName", "购买返利");
            contentMap.put("time", DateUtil.dateToString(upayWxOrderPend.getPaymentTime(), DateUtil.POINT_DATE_FORMAT_TWO));
            contentMap.put("price", "+" + upayWxOrderPend.getTotalFee());
            contentMap.put("tips", DateUtil.dateToString(DateUtil.getBeginThe20thOfNextMonth(), DateUtil.SHORT_DATE_FORMAT_POINT) + "可结算");
            contentMap.put("status", upayWxOrderPend.getStatus());

            content.add(contentMap);
        }
        appletMybatisPageDto.setContent(content);
        if (offset + size < upayWxOrderPendIPage.getTotal()) {
            appletMybatisPageDto.setPullDown(true);
        }
        return appletMybatisPageDto;
    }

}
