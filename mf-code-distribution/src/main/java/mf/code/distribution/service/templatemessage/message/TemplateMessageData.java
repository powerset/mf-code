package mf.code.distribution.service.templatemessage.message;

import mf.code.common.WeixinMpConstants;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述:
 *
 * @param:
 * @return:
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2019/4/19 0019 14:45
 */
public class TemplateMessageData {

	/***
	 * 本人的 订单返现到账
	 * @param cashBack 返现余额
	 * @return
	 */
	public static Map<String, Object> orderCashBackByMyself(Long shopId, Long userId, BigDecimal cashBack, String shopName) {
		Map<String, Object> dataInfo = new HashMap<>();
		Object[] objects = new Object[]{cashBack, "您的订单，今日购买待返现", "以上是您今日在" + shopName + "购买获得的待结算金额，确认收货7天后在每月20号即可返现到钱包"};
		dataInfo = getTempteDate(objects.length, objects);

		Map<String, Object> data = new HashMap<>();
		data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(shopId, userId, 26, "0"));
		data.put("data", dataInfo);
		data.put("emphasis_keyword", "keyword1.DATA");
		return data;
	}

	/***
	 * 本人的 所有上级 订单返现到账
	 * @param cashBack 返现余额
	 * @return 其中 from ：前端交互 26 跳推广收益
	 */
	public static Map<String, Object> orderCashBackByMaster(Long shopId, Long userId, BigDecimal cashBack, int scene, String isDialog) {
		Map<String, Object> dataInfo;
		Object[] objects = new Object[]{cashBack, "粉丝下单返现", "升级店长可得粉丝收益，确认收货7天后在每月20号即可返现到钱包"};
		dataInfo = getTempteDate(objects.length, objects);

		Map<String, Object> data = new HashMap<>();
		data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(shopId, userId, scene, isDialog));
		data.put("data", dataInfo);
		data.put("emphasis_keyword", "keyword1.DATA");
		return data;
	}

	/***
	 * 本人成为店长，上级 奖励通知
	 * @param cashBack 返现余额
	 * @return 其中 from ：前端交互 26 跳推广收益
	 */
	public static Map<String, Object> fansBecomeShopManagerAward(Long shopId, Long userId, BigDecimal cashBack, int scene, String isDialog, String userName) {
		Map<String, Object> dataInfo;
		Object[] objects = new Object[]{cashBack, "粉丝" + userName + "升级成为店长", "升级店长可得粉丝收益，确认收货7天后在每月20号即可返现到钱包"};
		dataInfo = getTempteDate(objects.length, objects);

		Map<String, Object> data = new HashMap<>();
		data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(shopId, userId, scene, isDialog));
		data.put("data", dataInfo);
		data.put("emphasis_keyword", "keyword1.DATA");
		return data;
	}

	/***
	 * 粉丝完成任务 发送消息模板
	 * @param cashBack 返现余额
	 * @return 其中 from ：前端交互 26 跳推广收益
	 */
	public static Map<String, Object> fansCompleteTask(Long shopId, Long userId, BigDecimal cashBack, int scene, String isDialog, String userName, String taskName) {
		Map<String, Object> dataInfo;
		Object[] objects = new Object[]{cashBack, "粉丝" + userName + "完成了" + taskName, "具体余额请查看详情"};
		dataInfo = getTempteDate(objects.length, objects);

		Map<String, Object> data = new HashMap<>();
		data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(shopId, userId, scene, isDialog));
		data.put("data", dataInfo);
		data.put("emphasis_keyword", "keyword1.DATA");
		return data;
	}

	/***
	 * 本人的 结算 收益到账
	 * @param cashBack 返现余额
	 * @return 其中 from ：前端交互 26 跳推广收益
	 */
	public static Map<String, Object> settledCashBackByMyself(Long shopId, Long userId, BigDecimal cashBack, String shopName) {
		Map<String, Object> dataInfo = new HashMap<>();
		Object[] objects = new Object[]{cashBack, "上月订单结算", "以上是您的" + shopName + "的上月累计收益结算，现已到账，赶快去提现吧！"};
		dataInfo = getTempteDate(objects.length, objects);

		Map<String, Object> data = new HashMap<>();
		data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(shopId, userId, 26, "0"));
		data.put("data", dataInfo);
		data.put("emphasis_keyword", "keyword1.DATA");
		return data;
	}

	/********************************私有方法******************************************/
	private static Map<String, Object> getTempteDate(int keywordNum, Object[] objects) {
		Map<String, Object> m = new HashMap<>();
		for (int i = 0; i < keywordNum; i++) {
			Map map = new HashMap();
			String key = "keyword" + (i + 1);
			String value = objects[i].toString();
			map.put("value", value);
			m.put(key, map);
		}
		return m;
	}

	private static String getPageUrl(Long shopId,
	                                 Long userId,
	                                 int from,
	                                 String isDialog) {
		//source--推送消息来源
		return "source=1&shopId=" + shopId +
				"&userId=" + userId +
				"&from=" + from +
				"&isDialog=" + isDialog;
	}
}
