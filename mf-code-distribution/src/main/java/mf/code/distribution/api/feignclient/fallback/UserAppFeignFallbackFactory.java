package mf.code.distribution.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.feignapi.dto.UserIdListDTO;
import mf.code.user.dto.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Slf4j
@Component
public class UserAppFeignFallbackFactory implements FallbackFactory<UserAppService> {
    @Override
    public UserAppService create(Throwable throwable) {
        return new UserAppService() {
            @Override
            public UserResp queryUser(Long userId) {
                return null;
            }

            @Override
            public List<Long> selectAllIds() {
                return null;
            }


            @Override
            public void updateUserBalanceSettledTimeByUserIdShopId(Long userId, Long shopId) {

            }

            @Override
            public Map<Integer, Long> queryUpperMember(Long vipShopId, Long userId, Integer scale) {
                return null;
            }

            @Override
            public Map<Integer, List<Long>> queryLowerMember(Long vipShopId, Long userId, Integer scale) {
                return null;
            }

            @Override
            public void batchUpdateUserBalance(UpayWxOrderReqList upayWxOrderReqList) {

            }


            @Override
            public AppletUserDTO queryAppletUser(Long userId, Long shopId) {
                return null;
            }

            @Override
            public List<AppletUserDTO> listAppletUser(Long shopId, List<Long> userIds) {
                log.error("熔断处理， 批量获取分销用户失败");
                return null;
            }

            @Override
            public String queryTotalSettledIncome(Long shopId, Long userId) {
                return null;
            }

            @Override
            public List<Map> batchSumTotalFeeSettledByUserIds(Long sid, List<Long> userIdList) {
                return null;
            }

            @Override
            public List<Map> batchTotalContributionSettledByUserIds(Long shopId, List<Long> userIdList) {
                return null;
            }

            @Override
            public boolean saveOrUpdateUser(UserResp userResp) {
                return false;
            }

            @Override
            public List<UserResp> listByIds(List<Long> userIds) {
                return null;
            }

            @Override
            public Map<String, String> queryTotalTaskIncomeByUserId(Long userId) {
                log.error("熔断处理");
                return null;
            }

            @Override
            public Map<Long, Map<String, String>> listTotalTaskIncomeByUserIds(List<Long> userIds) {
                log.error("熔断处理");
                return null;
            }

            @Override
            public void updateUpayBalanceSettledTimeByAll() {

            }

            /**
             * 查询一级下属的店长数
             *
             * @param userIdListDTO
             * @return
             */
            @Override
            public Long countShopManagerByUserIdList(UserIdListDTO userIdListDTO) {
                return 0L;
            }

        };
    }
}
