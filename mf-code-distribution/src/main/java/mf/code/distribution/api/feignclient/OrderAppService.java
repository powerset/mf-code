package mf.code.distribution.api.feignclient;

import mf.code.distribution.api.feignclient.fallback.OrderAppFeignFallbackFactory;
import mf.code.distribution.common.config.feign.FeignLogConfiguration;
import mf.code.order.dto.AppletUserOrderHistoryDTO;
import mf.code.order.dto.OrderResp;
import mf.code.order.dto.OrderRespListDTO;
import mf.code.user.dto.UpayWxOrderReqList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@FeignClient(name = "mf-code-order", fallbackFactory = OrderAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OrderAppService {

    /**
     * 存储订单信息
     * @return
     */
    @PostMapping("/feignapi/order/applet/v7/batchSaveOrderRespList")
    boolean batchSaveOrderRespList(@RequestBody OrderRespListDTO orderRespListDTO);

    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByOrder")
    int countGoodsNumByOrder(@RequestParam(name = "goodsId") Long goodsId);

    /**
     * 通过orderid 获取订单信息
     *
     * @param orderId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrder")
    OrderResp queryOrder(@RequestParam(name = "orderId") Long orderId);

    /**
     * 根据店铺id,产品id统计订单数
     *
     * @param shopId
     * @param productId
     * @return int统计数量
     */
    @GetMapping("/feignapi/order/applet/v5/summaryOrderByShopIdAndProductId")
    int summaryOrderByShopIdAndProductId(@RequestParam(name = "shopId") Long shopId,
                                         @RequestParam(name = "productId") Long productId);

    /**
     * 获取 此用户在此店铺的 所有 可结算订单 -- 进行结算处理
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderSettledByUserIdShopId")
    List<OrderResp> queryOrderSettledByUserIdShopId(@RequestParam("userId") Long userId,
                                                    @RequestParam("shopId") Long shopId);

    /**
     * 查询 用户 在此店铺得 本月的消费记录
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderRecordThisMonth")
    Map<String, OrderResp> queryOrderRecordThisMonth(@RequestParam("userId") Long userId,
                                                     @RequestParam("shopId") Long shopId);

    /**
     * 通过商品id 获取商品销量
     *
     * @param productIds
     * @return Map<productId ,   salesVolume>
     */
    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByProductIds")
    Map<Long, Integer> countGoodsNumByProductIds(@RequestParam("productIds") List<Long> productIds);

    /**
     * 获取 当前店铺下 用户上级成员 某段时间内 消费金额
     *
     * @param param <per>
     *              List<Long> userIds
     *              Long shopId
     *              Date beginTime
     *              Date endTime
     *              </per>
     * @return JSON.toJSONString(Map < Long, BigDecimal >)
     * <per>
     * Long userId
     * BigDecimal 消费金额
     * </per>
     */
    @GetMapping("/feignapi/order/applet/v5/listUserSpendAmount")
    String listUserSpendAmount(@RequestParam("param") Map<String, Object> param);

    /**
     * 获取 当前店铺下 某段时间内 用户订单历史记录
     * <per>
     * Long userId
     * Long shopId
     * Date beginTime
     * Date endTime
     * </per>
     *
     * @param param
     * @return JSON.toJSONString(AppletUserOrderHistoryDTO)
     */
    @GetMapping(value = "/feignapi/order/applet/v5/queryUserOrderHistory", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AppletUserOrderHistoryDTO queryUserOrderHistory(@RequestParam("param") String param);

    /**
     * 获取 订单信息
     *
     * @param orderId
     * @return JSON.toJSONString(AppletOrderResp)
     */
    @GetMapping("/feignapi/order/applet/v5/queryAppletOrder")
    String queryAppletOrder(@RequestParam("orderId") Long orderId);

    /**
     * 批量获取 当前店铺下 某段时间内 用户订单历史记录
     * <per>
     * List<Long> userIds
     * Long shopId
     * Date beginTime
     * Date endTime
     * </per>
     *
     * @param param
     * @return JSON.toJSONString(List<AppletUserOrderHistoryDTO>)
     */
    @GetMapping(value = "/feignapi/order/applet/v5/listUserOrderHistory", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    List<AppletUserOrderHistoryDTO> listUserOrderHistory(@RequestParam("param") String param);

    /**
     * 获取某时间段 所有未在售后处理中的分销业务订单 (可结算订单)
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/feignapi/order/applet/v6/querySettlementOrdersByTime")
    List<OrderResp> querySettlementOrdersByTime(@RequestParam("beginTime") String beginTime,
                                                @RequestParam("endTime") String endTime);

    /**
     * 通过 时间范围 获取所有用户 所有店铺的 已支付-未退款订单
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/feignapi/order/applet/v8/queryPaidOrdersWithBiz")
    List<OrderResp> queryPaidOrdersWithBiz(@RequestParam("beginTime") String beginTime,
                                           @RequestParam("endTime") String endTime);

    /**
     * 通过 订单编号 批量取消 订单
     * @param orderNoList
     * @return
     */
    @GetMapping("/feignapi/order/applet/v7/cancelOrderByOrderNoList")
    boolean cancelOrderByOrderNoList(@RequestParam("orderNoList") List<String> orderNoList);

    /**
     * 获取 此用户 可结算的平台补贴订单
     * @param userId
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/feignapi/order/applet/v7/querySettlementSubsidyOrdersByTime")
    List<OrderResp> querySettlementSubsidyOrdersByTime(@RequestParam("userId") Long userId,
                                                       @RequestParam("beginTime") Date beginTime,
                                                       @RequestParam("endTime") Date endTime);

    /**
     * 通过订单id 获取订单信息
     * @param orderIds
     * @return
     */
    @GetMapping("/feignapi/order/applet/v7/listByIds")
	List<OrderResp> listByIds(@RequestParam("orderIds") List<Long> orderIds);

    /**
     * 批量更新用户余额
     */
    @PostMapping("/feignapi/order/applet/batchUpdateMerchantBalance")
    void batchUpdateMerchantBalance(@RequestBody UpayWxOrderReqList upayWxOrderReqList);
}
