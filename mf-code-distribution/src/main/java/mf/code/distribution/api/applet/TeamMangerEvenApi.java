package mf.code.distribution.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.service.TeamMangerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/distribution/applet/v5/teammanger")
@Slf4j
public class TeamMangerEvenApi {
    @Autowired
    private TeamMangerService teamMangerService;

    /**
     * 7日访客
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping(value = "/visitorsInSevenDays")
    public SimpleResponse visitorsInSevenDays(@RequestParam("shopId") Long shopId,
                                              @RequestParam("userId") Long userId,
                                              @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                              @RequestParam(value = "limit", defaultValue = "4") Long limit){
        return teamMangerService.visitorsInSevenDays(shopId,userId,offset,limit);
    }

    /**
     * 7日订单
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping(value = "/newOrdersInSevenDays")
    public SimpleResponse newOrdersInSevenDays(@RequestParam("shopId") Long shopId,
                                               @RequestParam("userId") Long userId,
                                               @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                               @RequestParam(value = "limit", defaultValue = "4") Long limit){
        return teamMangerService.newOrdersInSevenDays(shopId,userId,offset,limit);
    }

    /**
     * 7日完成任务
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping(value = "/finsishTasksInSevenDays")
    public SimpleResponse finsishTasksInSevenDays(@RequestParam("shopId") Long shopId,
                                                  @RequestParam("userId") Long userId,
                                                  @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                                  @RequestParam(value = "limit", defaultValue = "4") int limit){
        return teamMangerService.finishTasksInSevenDays(shopId,userId,offset,limit);
    }

    /**
     * f粉丝行文明细
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping(value = "/fansActiveDetail")
    public SimpleResponse fansActiveDetail(@RequestParam("shopId") Long shopId,
                                           @RequestParam("userId") Long userId,
                                           @RequestParam("fanUserId") Long fanUserId,
                                           @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                           @RequestParam(value = "limit",defaultValue = "4") Long limit){
        return teamMangerService.fansActiveDetail(shopId, userId, fanUserId, offset, limit);
    }


}
