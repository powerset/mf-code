package mf.code.distribution.api.feignclient;

import mf.code.comm.dto.CommonDictDTO;
import mf.code.distribution.api.feignclient.fallback.CommAppFeignFallbackFactory;
import mf.code.distribution.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mf.code.distribution.api.feignclient
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 22:57
 */
@FeignClient(name = "mf-code-comm", fallbackFactory = CommAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface CommAppService {

    /**
     * 按照类型键值查询
     *
     * @param type
     * @param key
     * @return
     */
    @GetMapping("/feignapi/comm/selectByTypeKey")
    CommonDictDTO selectByTypeKey(@RequestParam("type") String type,
                                  @RequestParam("key") String key);

    /**
     * 保存或更新数据
     *
     * @param commonDictDTO
     * @return
     */
    @PostMapping("/feignapi/comm/saveOrUpdateByTypeKey")
    boolean saveOrUpdateByTypeKey(@RequestBody CommonDictDTO commonDictDTO);
}
