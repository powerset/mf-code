package mf.code.distribution.api.temp;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.common.job.DistributionScheduledService;
import mf.code.distribution.service.DataFixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.distribution.api.temp
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-01 20:39
 */
@Slf4j
@RestController
@RequestMapping("/tempApi/distribution/dataFix")
public class TempApi {

	@Autowired
	private DataFixService dataFixService;
//	@Autowired
//	private DistributionScheduledService distributionScheduledService;

	/**
	 * 20结算 平台订单补全
	 *
	 * @return
	 */
	@GetMapping("/distributionPlatformOrderFix")
	public SimpleResponse distributionPlatformOrderFix() {
		dataFixService.distributionPlatformOrderFix();
		return new SimpleResponse();
	}

	/**
	 * 用户支付成功，平台待结算订单补全
	 *
	 * @return
	 */
	@GetMapping("/distributionPlatformOrderPendFix")
	public SimpleResponse distributionPlatformOrderPendFix() {
		dataFixService.distributionPlatformOrderPendFix();
		return new SimpleResponse();
	}

	/**
	 * 用户返利佣金20结算接口
	 *
	 * @return
	 */
	@GetMapping("/userOrderSettlement")
	public SimpleResponse userOrderSettlement() {
//		distributionScheduledService.userOrderSettlement();
		return new SimpleResponse();
	}
}
