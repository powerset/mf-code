package mf.code.distribution.api.platform;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.common.property.SubsidyProperty;
import mf.code.distribution.domain.valueobject.UserSubsidyPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.api.platform
 * Description:
 *
 * @author gel
 * @date 2019-06-18 16:25
 */
@Slf4j
@RestController
@RequestMapping("/api/distribution/platform/subsidy")
public class SubsidyPolicyApi {

    @Autowired
    private SubsidyProperty subsidyProperty;

    /**
     * 按照日期查询补贴政策参数
     *
     * @param date
     * @return
     */
    @GetMapping("/policy")
    public SimpleResponse policy(@RequestParam(value = "date", required = false) String date) {
        /**
         * TODO 暂时没有用到时间参数，因为apollo修改配置并没有显式涉及到到修改日期。待后续开发
         * 1.获取补贴政策配置分组数量
         * 2.根据数量遍历调用解析方法
         * 3.返回结果
         */
        int groupSize = subsidyProperty.queryGroupSize();
        // 参数执行时间
        // String execDate = "2019-06";
        List<Map<String, Object>> resultMapList = new ArrayList<>();
        for (int i = 0; i < groupSize; i++) {
            // 这里为什么是long型的分组数
            UserSubsidyPolicy userSubsidyPolicyByGroupNo = subsidyProperty.findUserSubsidyPlicyByGroupNo(Long.valueOf(i));
            Map<String, Object> policyMap = BeanMapUtil.beanToMapIgnore(userSubsidyPolicyByGroupNo,
                    "id", "userId", "subsidizedDays", "subsidyAmountMap", "utime", "ctime");
            policyMap.put("execDate", date);
            resultMapList.add(policyMap);
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("policyList", resultMapList);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultMap);
    }
}
