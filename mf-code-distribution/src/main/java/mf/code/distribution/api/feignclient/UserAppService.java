package mf.code.distribution.api.feignclient;

import mf.code.distribution.api.feignclient.fallback.UserAppFeignFallbackFactory;
import mf.code.distribution.common.config.feign.FeignLogConfiguration;
import mf.code.distribution.feignapi.dto.UserIdListDTO;
import mf.code.user.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 19:38
 */
@FeignClient(name = "mf-code-user", fallbackFactory = UserAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface UserAppService {

    /***
     * 获取用户信息
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryUser")
    UserResp queryUser(@RequestParam("userId") Long userId);

    /**
     * 获取所有授权的用户ids FIXME 不能一次性查询全表
     *
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/selectAllIds")
    List<Long> selectAllIds();


    /**
     * 更新 此用户 在此店铺下 的余额结算时间
     */
    @RequestMapping("/feignapi/user/applet/v7/updateUserBalanceSettledTimeByUserIdShopId")
    void updateUserBalanceSettledTimeByUserIdShopId(@RequestParam("userId") Long userId,
                                                    @RequestParam("shopId") Long shopId);


    /**
     * 获取 当前商城的用户的所属上级成员
     *
     * @param vipShopId
     * @param userId
     * @param scale
     * @return JSON.toJSONString(Map < level, UserId >)
     */
    @RequestMapping("/feignapi/user/applet/v5/queryUpperMember")
    Map<Integer, Long> queryUpperMember(@RequestParam("vipShopId") Long vipShopId,
                                        @RequestParam("userId") Long userId,
                                        @RequestParam("scale") Integer scale);

    /**
     * 获取 当前商城的用户的所属上级成员
     *
     * @param vipShopId
     * @param userId
     * @param scale
     * @return JSON.toJSONString(Map < level, UserId >)
     */
    @RequestMapping("/feignapi/user/applet/v5/queryLowerMember")
    Map<Integer, List<Long>> queryLowerMember(@RequestParam("vipShopId") Long vipShopId,
                                              @RequestParam("userId") Long userId,
                                              @RequestParam("scale") Integer scale);

    /**
     * 批量更新用户余额
     *
     * @param upayWxOrderReqList upayWxOrderReqs = List<UpayWxOrderReq>
     *                     upayWxOrders = JSON.toJSONString(upayWxOrderReqs)
     */
    @PostMapping("/feignapi/user/applet/v5/batchUpdateUserBalance")
    void batchUpdateUserBalance(@RequestBody UpayWxOrderReqList upayWxOrderReqList);


    /***
     * 获取 用户信息 和 用户收益信息
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryAppletUser")
    AppletUserDTO queryAppletUser(@RequestParam("userId") Long userId,
                                  @RequestParam("shopId") Long shopId);

    /**
     * 批量获取 获取 用户信息 和 用户收益信息
     *
     * @param shopId
     * @param userIds
     * @return
     */
    @GetMapping(value = "/feignapi/user/applet/v5/listAppletUser")
    List<AppletUserDTO> listAppletUser(@RequestParam("shopId") Long shopId,
                                       @RequestParam("userIds") List<Long> userIds);

    /**
     * 获取 单个用户推广到现在 所有已结算收益 -- 返利+贡献
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryTotalSettledIncome")
    String queryTotalSettledIncome(@RequestParam("shopId") Long shopId,
                                   @RequestParam("userId") Long userId);

    /**
     * 批量获取 用户 累计已结算总额 -- 返利及贡献
     *
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/batchSumTotalSettledIncome")
    List<Map> batchSumTotalFeeSettledByUserIds(@RequestParam("sid") Long sid,
                                               @RequestParam("userIdList") List<Long> userIdList);

    /**
     * 分批获取 此用户 的团队 -从最初贡献开始-到现在的所有已结算贡献总金额 -- 贡献
     *
     * @return
     */
    @GetMapping(value = "/feignapi/user/applet/v5/batchTotalContributionSettledByUserIds")
    List<Map> batchTotalContributionSettledByUserIds(@RequestParam("shopId") Long shopId,
                                                     @RequestParam("userIdList") List<Long> userIdList);


    /**
     * 保存或更新用户信息
     *
     * @param userResp
     * @return
     */
    @PostMapping(value = "/feignapi/user/applet/v8/saveOrUpdateUser")
    boolean saveOrUpdateUser(@RequestBody UserResp userResp);

    /**
     * 通过主键 批量获取用户信息
     *
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/user/applet/v8/listByIds")
    List<UserResp> listByIds(@RequestParam("userIds") List<Long> userIds);

    /**
     * 查询一级下属的店长数
     *
     * @param userIdListDTO
     * @return
     */
    @PostMapping("/feignapi/user/applet/v5/countShopManagerFirstLevel")
    Long countShopManagerByUserIdList(@RequestBody UserIdListDTO userIdListDTO);

    /**
     * 通过 用户id 获取全平台的 用户任务收益情况
     * @param userId
     * @return taskAward 任务奖金 taskCommission 任务佣金
     */
    @GetMapping("/feignapi/user/applet/v9/queryTotalTaskIncomeByUserId")
    Map<String, String> queryTotalTaskIncomeByUserId(@RequestParam("userId") Long userId);

    /**
     * 批量获取 用户们 全平台的任务收益情况
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/user/applet/v9/listTotalTaskIncomeByUserIds")
    Map<Long, Map<String, String>> listTotalTaskIncomeByUserIds(@RequestParam("userIds") List<Long> userIds);

    /**
     * 20日结算结束，更新所有用户的结算时间
     * @return
     */
    @GetMapping("/feignapi/user/applet/v9/updateUpayBalanceSettledTimeByAll")
    void updateUpayBalanceSettledTimeByAll();
}
