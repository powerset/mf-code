package mf.code.distribution.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.distribution.api.feignclient.CommAppService;
import org.springframework.stereotype.Component;

/**
 * mf.code.distribution.api.feignclient.fallback
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 22:58
 */
@Component
@Slf4j
public class CommAppFeignFallbackFactory implements FallbackFactory<CommAppService> {
    @Override
    public CommAppService create(Throwable cause) {
        return new CommAppService() {

            @Override
            public CommonDictDTO selectByTypeKey(String type, String key) {
                return null;
            }

            @Override
            public boolean saveOrUpdateByTypeKey(CommonDictDTO commonDictDTO) {
                return false;
            }
        };
    }
}
