package mf.code.distribution.api.seller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.service.platform.PlatformShopManagerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 店长缴纳金管理Api
 * <p>
 * mf.code.distribution.api.platform
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 11:52
 */
@RestController
@RequestMapping("/api/distribution/seller/shopManager")
@RequiredArgsConstructor
@Slf4j
public class SellerShopManagerApi {
    private final PlatformShopManagerService platformShopManagerService;

    /**
     * 获取 店长缴纳金配置
     *
     * @return
     */
    @GetMapping("/querySalePolicy")
    public SimpleResponse querySalePolicy() {

        return platformShopManagerService.querySalePolicy();
    }
}
