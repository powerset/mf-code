package mf.code.distribution.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.common.email.EmailService;
import mf.code.order.dto.AppletUserOrderHistoryDTO;
import mf.code.order.dto.OrderResp;
import mf.code.order.dto.OrderRespListDTO;
import mf.code.user.dto.UpayWxOrderReqList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@Slf4j
@Component
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Autowired
    private EmailService emailService;

    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public boolean batchSaveOrderRespList(OrderRespListDTO orderRespListDTO) {
                return false;
            }

            @Override
            public int countGoodsNumByOrder(Long goodsId) {
                return 0;
            }

            @Override
            public OrderResp queryOrder(Long orderId) {
                return null;
            }

            @Override
            public int summaryOrderByShopIdAndProductId(Long shopId, Long productId) {
                return 0;
            }

            @Override
            public List<OrderResp> queryOrderSettledByUserIdShopId(Long userId, Long shopId) {
                return null;
            }

            @Override
            public Map<String, OrderResp> queryOrderRecordThisMonth(Long userId, Long shopId) {
                return null;
            }

            @Override
            public Map<Long, Integer> countGoodsNumByProductIds(List<Long> productIds) {
                return null;
            }

            @Override
            public String listUserSpendAmount(Map<String, Object> param) {
                return null;
            }

            @Override
            public AppletUserOrderHistoryDTO queryUserOrderHistory(String param) {
                emailService.sendSimpleMail("feiapi熔断邮件",
                        "/feignapi/order/applet/v5/queryUserOrderHistory熔断, distributionUserRepository.addUserOrderHistory异常， params = "+ param);
                log.error("addUserOrderHistory 方法 被熔断");
                return null;
            }

            @Override
            public String queryAppletOrder(Long orderId) {
                return null;
            }

            @Override
            public List<AppletUserOrderHistoryDTO> listUserOrderHistory(String param) {
                return null;
            }

            @Override
            public List<OrderResp> querySettlementOrdersByTime(String beginTime, String endTime) {
                log.error("熔断处理");
                return null;
            }

            @Override
            public List<OrderResp> queryPaidOrdersWithBiz(String beginTime, String endTime) {
                return null;
            }

            @Override
            public boolean cancelOrderByOrderNoList(List<String> orderNoList) {
                return false;
            }

            @Override
            public List<OrderResp> querySettlementSubsidyOrdersByTime(Long userId, Date beginTime, Date endTime) {
                return null;
            }

            @Override
            public List<OrderResp> listByIds(List<Long> orderIds) {
                return null;
            }

            @Override
            public void batchUpdateMerchantBalance(UpayWxOrderReqList upayWxOrderReqList) {

            }
        };
    }
}
