package mf.code.distribution.api.feignclient;

import mf.code.distribution.api.feignclient.fallback.ShopAppFeignFallbackFactory;
import mf.code.distribution.common.config.feign.FeignLogConfiguration;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.dto.MerchantOrderReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * mf.code.shop.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 20:34
 */
@FeignClient(value = "mf-code-shop", fallbackFactory = ShopAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface ShopAppService {

	/**
	 * 微信用户 登录小程序 返回相关店铺信息
	 * @param shopId
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/v5/queryShopInfoByLogin")
	Map<String, Object> queryShopInfoByLogin(@RequestParam("shopId") String shopId);

	/**
	 * 根据店铺id获取商户id
	 *
	 * @param shopId 店铺id
	 * @return 商户id
	 */
	@GetMapping("/feignapi/shop/applet/v5/getMerchantIdByShopId")
	Long getMerchantIdByShopId(@RequestParam("shopId") Long shopId);

	/**
	 * 获取所有有效店铺的ids
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/v5/selectAllIds")
	List<Long> selectAllIds();

	/**
	 * 插入记录 并 更新余额
	 * @param merchantOrderReq
	 * @return
	 */
	@PostMapping("/feignapi/shop/applet/v5/updateMerchantBalance")
	Integer updateMerchantBalance(@RequestBody MerchantOrderReq merchantOrderReq);

	/**
	 * 商户查询 返回相关店铺信息
	 *
	 * @param shopId
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/v5/queryShopInfoForSeller")
	Map<String, Object> queryShopInfoForSeller(@RequestParam("shopId") String shopId);

	/**
	 * 通过 merchantId 获取 抖带带主播信息
	 * @param merchantId
	 * @return
	 */
	@GetMapping("feignapi/shop/applet/queryMerchantById")
	MerchantDTO queryMerchantById(@RequestParam("merchantId") Long merchantId);
}
