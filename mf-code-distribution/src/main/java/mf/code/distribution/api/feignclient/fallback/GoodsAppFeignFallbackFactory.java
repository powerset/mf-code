package mf.code.distribution.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.api.feignclient.GoodsAppService;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * mf.code.goods.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 11:10
 */
@Component
@Slf4j
public class GoodsAppFeignFallbackFactory implements FallbackFactory<GoodsAppService> {
    @Override
    public GoodsAppService create(Throwable cause) {
        return new GoodsAppService() {

            @Override
            public GoodProductDTO queryProduct(Long merchantId, Long shopId, Long goodsId) {
                return null;
            }

            @Override
            public List<ProductEntity> listByIds(List<Long> goodsIds) {
                return null;
            }
        };
    }
}
