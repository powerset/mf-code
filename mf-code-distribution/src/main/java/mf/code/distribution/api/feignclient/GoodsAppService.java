package mf.code.distribution.api.feignclient;

import mf.code.distribution.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import mf.code.distribution.common.config.feign.FeignLogConfiguration;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.ProductEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * mf.code.goods.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 14:10
 */
@FeignClient(name = "mf-code-goods", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface GoodsAppService {
    @GetMapping("/feignapi/goods/applet/v5/queryProduct")
    GoodProductDTO queryProduct(@RequestParam("merchantId") Long merchantId,
                                @RequestParam("shopId") Long shopId,
                                @RequestParam("goodsId") Long goodsId);

    /**
     * 通过主键 批量获取商品信息
     * @param goodsIds
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v8/listByIds")
	List<ProductEntity> listByIds(@RequestParam("goodsIds") List<Long> goodsIds);
}
