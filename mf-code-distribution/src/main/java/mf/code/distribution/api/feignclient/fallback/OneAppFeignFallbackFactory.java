package mf.code.distribution.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.api.feignclient.OneAppService;
import mf.code.one.dto.ActivityDefDTO;
import mf.code.one.dto.ShopManagerCodeDTO;
import mf.code.user.dto.UserTaskIncomeResp;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.api.feignclient.fallback
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:07
 */
@Slf4j
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable cause) {
        return new OneAppService() {

            @Override
            public void offlineShopManagerActivityDef() {

            }

            @Override
            public UserTaskIncomeResp queryUserTaskIncome(Long userId) {
                return null;
            }

            @Override
            public Long getShopManagerTask(Long userId, Long shopId) {
                return null;
            }

            @Override
            public ActivityDefDTO findShopManagerInviteFansTask(Long activityDefId) {
                return null;
            }

            @Override
            public String giveAwardRecommendShopManager(Long userId, Long activityDefId) {
                return null;
            }

            @Override
            public String giveAwardInviteFans(Long userId, Long activityDefId) {
                return null;
            }

            @Override
            public String giveAwardUploadShopManagerQR(Long userId, Long activityDefId) {
                return null;
            }

            @Override
            public List<UserTaskIncomeResp> listUserTaskIncome(List<Long> userIds) {
                return null;
            }

            @Override
            public Map<String, Object> queryNewcomerTaskInfo(Long shopId, Long userId) {
                return null;
            }

            @Override
            public String saveGroupCodeForShopManager(ShopManagerCodeDTO shopManagerCodeDTO) {
                log.error("熔断处理，saveGroupCodeForShopManager");
                return null;
            }

            @Override
            public String getGroupCodeByUserId(Long userId) {
                return null;
            }

            @Override
            public String getWxQRFromPicUrl(ShopManagerCodeDTO shopManagerCodeDTO) {
                return null;
            }

            @Override
            public SimpleResponse saveShopManagerCommissionByPend(Long userId, Long shopId) {
                return null;
            }
        };
    }
}
