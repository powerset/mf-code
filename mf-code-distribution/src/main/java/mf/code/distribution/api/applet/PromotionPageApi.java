package mf.code.distribution.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.distribution.service.PromotionPageService;
import mf.code.user.dto.UpayWxOrderReq;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.api.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-04 13:31
 */
@Slf4j
@RestController
@RequestMapping("/api/distribution/applet/v5/promotionpage")
public class PromotionPageApi {
    @Autowired
    private PromotionPageService promotionPageService;

    /**
     * 推广收益页  获取用户收益情况
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/queryUserIncome")
    public SimpleResponse queryUserIncome(@RequestParam("userId") String userId,
                                          @RequestParam("shopId") String shopId,
                                          @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                          @RequestParam(value = "size", defaultValue = "16") Long size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        return promotionPageService.queryUserIncome(Long.valueOf(userId), Long.valueOf(shopId), offset, size);
    }

    /**
     * 推广收益页  获取用户收益情况
     *
     * @param userId
     * @param shopId
     * @param type   1.全球收益排行榜 2.好友收益排行榜
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/queryUserIncomeV2")
    public SimpleResponse queryUserIncomeV2(@RequestParam("userId") String userId,
                                            @RequestParam("shopId") String shopId,
                                            @RequestParam("type") String type,
                                            @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                            @RequestParam(value = "size", defaultValue = "16") Long size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(type) || !RegexUtils.StringIsNumber(type)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        return promotionPageService.queryUserIncomeV2(Long.valueOf(userId), Long.valueOf(shopId), Integer.valueOf(type), offset, size);
    }

    /**
     * 获取当前店铺下 用户团队贡献榜
     *
     * @param userId
     * @param shopId
     * @param type   1全部 2已结清 3待结算
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/queryTeamIncome")
    public SimpleResponse queryTeamIncome(@RequestParam("userId") String userId,
                                          @RequestParam("shopId") String shopId,
                                          @RequestParam("type") String type,
                                          @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                          @RequestParam(value = "size", defaultValue = "16") Long size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        return promotionPageService.queryTeamIncome(Long.valueOf(userId), Long.valueOf(shopId), type, offset, size);
    }

    /***
     * 获取团队粉丝信息
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    @GetMapping("/queryTeamNumInfo")
    public SimpleResponse queryTeamNumInfo(@RequestParam("userId") Long userId,
                                           @RequestParam("shopId") Long shopId,
                                           @RequestParam("merchantId") Long merchantId) {
        if (userId == null || userId == 0
                || shopId == null || shopId == 0
                || merchantId == null || merchantId == 0) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return promotionPageService.queryTeamNumInfo(userId, shopId, merchantId);
    }

    /**
     * 成为 店长 的弹窗状态
     *
     * @param userId
     * @return
     */
    @GetMapping("/beShopManagerPopup")
    public SimpleResponse beShopManagerPopup(@RequestParam("userId") String userId) {
        if (StringUtils.isBlank(userId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return promotionPageService.beShopManagerPopup(Long.valueOf(userId));
    }

    /**
     * 成为 店长
     *
     * @param userId
     * @return
     */
    @GetMapping("/becomeShopManager")
    public SimpleResponse becomeShopManager(@RequestParam("shopId") String shopId,
                                            @RequestParam("userId") String userId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return promotionPageService.becomeShopManager(Long.valueOf(shopId), Long.valueOf(userId));
    }

    /**
     * 展示上传 二维码页
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/showShopManagerQR")
    public SimpleResponse showShopManagerQR(@RequestParam("merchantId") String merchantId,
                                            @RequestParam("shopId") String shopId,
                                            @RequestParam("userId") String userId) {
        if (StringUtils.isBlank(merchantId) || !RegexUtils.StringIsNumber(merchantId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)) {
            log.error("参数异常, merchantId = {}, shopId = {}, userId = {}", merchantId, shopId, userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        return promotionPageService.showShopManagerQR(Long.valueOf(merchantId), Long.valueOf(shopId), Long.valueOf(userId));
    }

    /**
     * 上传 店长二维码 （全民群主）
     *
     * @param userId
     * @return
     */
    @GetMapping("/uploadShopManagerQR")
    public SimpleResponse uploadShopManagerQR(@RequestParam("shopId") String shopId,
                                              @RequestParam("userId") String userId,
                                              @RequestParam("shopManagerQR") String shopManagerQR) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(shopManagerQR)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        String[] split = shopManagerQR.split(",");
        if (split.length < 2) {
            log.error("请上传2张二维码");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请上传2张二维码");
        }
        Map<String, String> wxCode = new HashMap<>();
        wxCode.put("personalQR", split[0]);
        wxCode.put("groupQR", split[1]);
        return promotionPageService.uploadShopManagerQR(Long.valueOf(shopId), Long.valueOf(userId), wxCode);
    }

    /**
     * 获取商学院页
     *
     * @return
     */
    @GetMapping("/queryBusinessSchoolPage")
    public SimpleResponse queryBusinessSchoolPage(@RequestParam("userId") String userId,
                                                  @RequestParam("shopId") String shopId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return promotionPageService.queryBusinessSchoolPage(Long.valueOf(userId), Long.valueOf(shopId));
    }

    /**
     * 分销收益明细
     *
     * @param userId
     * @param shopId
     * @param bizType  1.自购省待结算 2.分享赚待结算
     * @param dateType 1.本月 2.上月
     * @return
     */
    @GetMapping("/queryPendRecordsPage")
    public SimpleResponse queryPendRecordsPage(@RequestParam("userId") String userId,
                                               @RequestParam("shopId") String shopId,
                                               @RequestParam(value = "bizType", defaultValue = "1") Integer bizType,
                                               @RequestParam(value = "dateType", defaultValue = "1") Integer dateType,
                                               @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                               @RequestParam(value = "size", defaultValue = "16") Long size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return promotionPageService.queryPendRecordsPage(Long.valueOf(userId), Long.valueOf(shopId), bizType, dateType, offset, size);
    }

    /**
     * 领取店长任务
     *
     * @param userId
     * @return
     */
    @GetMapping("/doShopManagerTask")
    public SimpleResponse doShopManagerTask(@RequestParam("userId") String userId,
                                            @RequestParam("shopId") String shopId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return promotionPageService.doShopManagerTask(Long.valueOf(userId), Long.valueOf(shopId));
    }

}
