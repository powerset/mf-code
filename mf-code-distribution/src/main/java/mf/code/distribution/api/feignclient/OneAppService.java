package mf.code.distribution.api.feignclient;

import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.CommonCodeTypeEnum;
import mf.code.common.CommonCodeUserTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.api.feignclient.fallback.OneAppFeignFallbackFactory;
import mf.code.distribution.common.config.feign.FeignLogConfiguration;
import mf.code.one.dto.ActivityDefDTO;
import mf.code.one.dto.ShopManagerCodeDTO;
import mf.code.user.dto.UserTaskIncomeResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * mf.code.user.api.feignclient
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:06
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {


    /**
     * 下线 全平台所有店铺中 任务三佣金为0的店长任务
     * @return
     */
    @Async
    @GetMapping("/feignapi/one/platform/offlineShopManagerActivityDef")
    void offlineShopManagerActivityDef();

    /**
     * 查询 用户任务收益
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/queryUserTaskIncome")
    UserTaskIncomeResp queryUserTaskIncome(@RequestParam("userId") Long userId);

    /**
     * 领取店长任务，扣绑定店铺库存 库存
     *
     * @param userId
     * @param shopId
     * @return activityDefId
     */
    @GetMapping("/feignapi/one/applet/v8/getShopManagerTask")
    Long getShopManagerTask(@RequestParam("userId") Long userId,
                            @RequestParam("shopId") Long shopId);

    /**
     * 获取 店长 邀请粉丝任务
     *
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/findShopManagerInviteFansTask")
    ActivityDefDTO findShopManagerInviteFansTask(@RequestParam("activityDefId") Long activityDefId);

    /**
     * 发展粉丝成为店长 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardRecommendShopManager")
    String giveAwardRecommendShopManager(@RequestParam("userId") Long userId,
                                         @RequestParam("activityDefId") Long activityDefId);

    /**
     * 店长 邀请粉丝达标 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardInviteFans")
    String giveAwardInviteFans(@RequestParam("userId") Long userId,
                               @RequestParam("activityDefId") Long activityDefId);

    /**
     * 店长 上传二维码 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardUploadShopManagerQR")
    String giveAwardUploadShopManagerQR(@RequestParam("userId") Long userId,
                                        @RequestParam("activityDefId") Long activityDefId);

    /**
     * 批量获取 用户任务收益
     *
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/listUserTaskIncome")
    List<UserTaskIncomeResp> listUserTaskIncome(@RequestParam("userIds") List<Long> userIds);

    /**
     * 获取新人任务
     *
     * @param shopId
     * @param userId
     * @return null 无活动定义 非null 返回以下内容
     * Map<String, Object> resultVO = new HashMap<>();
     * resultVO.put("newbieAmount", totalAmount); 可以做的新人任务金额总和
     * resultVO.put("shopManagerAmount", totalAmount); 可以做的店长任务金额总和
     * resultVO.put("dailyAmount", totalAmount);可以做的日常任务金额总和
     */
    @GetMapping("/feignapi/one/applet/v6/queryNewcomerTaskInfo")
    Map<String, Object> queryNewcomerTaskInfo(@RequestParam("shopId") Long shopId,
                                              @RequestParam("userId") Long userId);

    /**
     * 小程序端店长添加群二维码
     *
     * @param shopManagerCodeDTO
     * @return
     */
    @PostMapping("/feignapi/one/applet/saveGroupCodeForShopManager")
    String saveGroupCodeForShopManager(@RequestBody ShopManagerCodeDTO shopManagerCodeDTO);

    /**
     * 小程序端店长查询群二维码
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/getGroupCodeByUserId")
    String getGroupCodeByUserId(@RequestParam(value = "userId") Long userId);

    /**
     * 从图片中获取微信二维码
     *
     * @param shopManagerCodeDTO
     * @return
     */
    @PostMapping("/feignapi/one/applet/getWxQRFromPicUrl")
    String getWxQRFromPicUrl(@RequestBody ShopManagerCodeDTO shopManagerCodeDTO);

    /***
     * 成为店长时，查看是否有下级佣金的记录，有：统一存储到upay_wx_order内
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/saveShopManagerCommissionByPend")
    SimpleResponse saveShopManagerCommissionByPend(@RequestParam(value = "userId") Long userId,
                                                   @RequestParam(value = "shopId") Long shopId);
}
