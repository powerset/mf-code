package mf.code.distribution.api.applet.feignservice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.NumberValidationUtil;
import mf.code.distribution.common.property.WxmpProperty;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.domain.aggregateroot.DistributionOrder;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.*;
import mf.code.distribution.dto.DistributeInfoEntity;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.MerchantOrderIncomeResp;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.repo.repository.DistributionOrderRepository;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.*;
import mf.code.distribution.service.templatemessage.TemplateMessageService;
import mf.code.distribution.service.templatemessage.message.TemplateMessageData;
import mf.code.goods.dto.ProductDTO;
import mf.code.goods.dto.ProductDistributCommissionEntity;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.user.constant.UpayWxOrderPendBizTypeEnum;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.distribution.api.applet.feignservice
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-08 19:10
 */
@Slf4j
@RestController
public class DistributionAppServiceImpl {
    @Autowired
    private DistributionUserRepository distributionUserRepository;
    @Autowired
    private DistributionOrderRepository distributionOrderRepository;
    @Autowired
    private UpayWxOrderPendService upayWxOrderPendService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private TemplateMessageService templateMessageService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private UserTeamIncomeService userTeamIncomeService;
    @Autowired
    private PromotionPageService promotionPageService;
    @Autowired
    private ShopManagerSalePolicyService shopManagerSalePolicyService;
    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /**
     * 付费升店长，发展一个粉丝成为店长可获得的奖金
     *
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/getAmountByShopManagerPay")
    public String getAmountByShopManagerPay() {
        // 判断是否有上级
        ShopManagerSalePolicy shopManagerSalePolicy = shopManagerSalePolicyService.getShopManagerSalePolicy();
        if (shopManagerSalePolicy == null) {
            return "0";
        }
        ShopManagerCommission shopManagerCommission = shopManagerSalePolicy.calculateShopManagerCommissionForEachPart();
        if (shopManagerCommission == null) {
            return "0";
        }
        BigDecimal superiorCommission = shopManagerCommission.getSuperiorCommission();
        return superiorCommission == null ? "0" : superiorCommission.toString();
    }

    @PostMapping("/feignapi/distribution/applet/becomeShopManagerV2")
    public SimpleResponse becomeShopManagerV2(@RequestBody UpayWxOrderReq upayWxOrderReq) {
        return promotionPageService.becomeShopManagerV2(upayWxOrderReq);
    }

    /**
     * 获取店长待结算记录
     *
     * @param shopManagerParams
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/queryShopManagerPend")
    public List<UpayWxOrderReq> queryShopManagerPend(@RequestBody Map<String, Object> shopManagerParams) {
        if (CollectionUtils.isEmpty(shopManagerParams)) {
            log.error("参数异常");
            return null;
        }
        List<UpayWxOrderPend> upayWxOrderPendList = upayWxOrderPendService.listPageByParams(shopManagerParams);
        if (CollectionUtils.isEmpty(upayWxOrderPendList)) {
            return null;
        }

        List<UpayWxOrderReq> upayWxOrderReqList = new ArrayList<>();
        for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPendList) {
            UpayWxOrderReq upayWxOrderReq = new UpayWxOrderReq();
            BeanUtils.copyProperties(upayWxOrderPend, upayWxOrderReq);
            upayWxOrderReqList.add(upayWxOrderReq);
        }

        return upayWxOrderReqList;

    }


    /**
     * 全球收益排行榜 更新
     * 任务奖励 入库后  再调此方法
     *
     * @param shopId
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v8/addIncomeRankListOfShopByUserIds")
    public SimpleResponse addIncomeRankListOfShopByUserIds(@RequestParam("shopId") Long shopId, @RequestParam("userIds") List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds) || shopId == null) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        userTeamIncomeService.addIncomeRankListOfShopByUserIds(userIds, shopId);
        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    /**
     * 本月是否达标
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v8/hasReachSpendStandardThisMonth")
    public boolean hasReachSpendStandardThisMonth(@RequestParam("userId") Long userId) {
        DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
        distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());
        return distributionUser.hasReachSpendStandard();
    }

    /***
     * 获取用户的团队粉丝数(包含自己)
     * @param shopId
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryTeamUsers")
    public Map<Long, Integer> queryTeamUser(@RequestParam("shopId") Long shopId,
                                            @RequestParam("userIds") List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            log.error("<<<<<<<< 获取团队粉丝数目, userIds:{}", userIds);
            return null;
        }
        Map<Long, Integer> resp = new HashMap<>();
        for (Long userId : userIds) {
            DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
            if (distributionUser == null) {
                log.error("无效用户, userId = {}", userId);
                return null;
            }
            distributionUserRepository.addUserShopTeamForAll(distributionUser);
            int i = distributionUser.queryTeamSize();
            resp.put(userId, i);
        }
        log.info("<<<<<<<< 获取团队粉丝数目返回, resp:{}", resp);
        return resp;
    }

    /**
     * 获取 历史订单 的商家所得金额
     *
     * @param orderIds
     * @return Map<OrderIdCashBack>
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryOrderHistoryRebate")
    public Map<Long, BigDecimal> queryOrderHistoryRebate(@RequestParam("orderIds") List<Long> orderIds) {
        if (CollectionUtils.isEmpty(orderIds)) {
            log.error("参数异常, orderIds = {}", orderIds);
            return null;
        }
        QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayWxOrderPend::getBizType, UpayWxOrderPendBizTypeEnum.REBATE.getCode())
                .in(UpayWxOrderPend::getBizValue, orderIds);
        List<UpayWxOrderPend> upayWxOrderPends = upayWxOrderPendService.list(wrapper);
        if (CollectionUtils.isEmpty(upayWxOrderPends)) {
            log.error("查无数据，orderIds = {}", orderIds);
            return null;
        }
        Map<Long, BigDecimal> resultVO = new HashMap<>();
        for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
            Long orderId = upayWxOrderPend.getBizValue();
            BigDecimal cashBack = resultVO.get(orderId);
            BigDecimal totalFee = upayWxOrderPend.getTotalFee();

            if (cashBack != null) {
                cashBack = cashBack.add(totalFee);
            } else {
                cashBack = totalFee;
            }
            resultVO.put(orderId, cashBack);
        }

        return resultVO;
    }

    /**
     * 查询 在此店铺内， 用户本月消费和收入的概况
     *
     * @param userId
     * @param shopId
     * @return statusCode = 0: 用户本月 预估收益，消费条件，消费金额，达标差额
     * statusCode = 1: 本月累计收益，最新团员贡献佣金
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryDistributionInfo")
    public UserDistributionInfoDTO queryDistributionInfo(@RequestParam("userId") Long userId,
                                                         @RequestParam("shopId") Long shopId) {
        if (userId == null || userId < 1 || shopId == null || shopId < 1) {
            log.error("参数异常");
            return null;
        }
        // 查询此时间段的用户订单 -- 消费记录
        Date beginTime = DateUtil.getBeginOfThisMonth();
        Date endTime = new Date();

        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.error("用户信息不存在, uid = {}, sid = {}", userId, shopId);
            return null;
        }
        distributionUserRepository.addUserOrderHistory(distributionUser, beginTime, endTime);
        distributionUserRepository.addUserDistributionHistory(distributionUser);

        BigDecimal spendingAmountTomonth = distributionUser.querySpendAmount();

        UserDistributionInfoDTO userDistributionInfo = new UserDistributionInfoDTO();
        userDistributionInfo.setSpendingAmountTomonth(spendingAmountTomonth);
        userDistributionInfo.setEstimateIncome(distributionUser.queryEstimateIncome());
        userDistributionInfo.setCommissionStandard(distributionUser.getUserRebatePolicy().getCommissionStandard());
        userDistributionInfo.setLeftAmount(distributionUser.queryLeftAmount());

        return userDistributionInfo;
    }

    /**
     * 获取订单分销属性
     */
    @PostMapping("/feignapi/distribution/applet/v5/queryOrderDistributionProperty")
    public String queryOrderDistributionProperty(@RequestBody ProductDistributionDTO product) {
        if (product == null || product.getProductSpecs() == null) {
            log.error("参数异常, product = {}", product);
            return null;
        }
        // 商品类目
        Long productSpecs = product.getProductSpecs();

        OrderDistributionProperty orderDistributionProperty = commonDictService.queryOrderDistributionProperty(productSpecs);
        if (orderDistributionProperty == null) {
            // 容错处理，配置默认分销属性
            orderDistributionProperty = new OrderDistributionProperty();
            orderDistributionProperty.setGoodsCategoryRate(10);
            orderDistributionProperty.setTransactionRate(2);
            orderDistributionProperty.setVipShopRate(50);
        }
        BeanUtils.copyProperties(product, orderDistributionProperty);

        return JSON.toJSONString(orderDistributionProperty);

    }

    /**
     * 获取 用户本月 商品返利金额
     *
     * @param product
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/getRebateCommission")
    public String getRebateCommission(@RequestBody ProductDistributionDTO product) {
        if (product == null) {
            log.error("参数异常");
            return null;
        }
        OrderDistributionProperty orderDistributionProperty = commonDictService.queryOrderDistributionProperty(product.getProductSpecs());
        if (orderDistributionProperty == null) {
            // 容错处理，配置默认分销属性
            orderDistributionProperty = new OrderDistributionProperty();
            orderDistributionProperty.setGoodsCategoryRate(10);
            orderDistributionProperty.setTransactionRate(2);
            orderDistributionProperty.setVipShopRate(50);
        }
        BeanUtils.copyProperties(product, orderDistributionProperty);

        ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(
                orderDistributionProperty.getGoodsCategoryRate(),
                orderDistributionProperty.getTransactionRate(),
                orderDistributionProperty.getVipShopRate());
        OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(product);
        BigDecimal customerCommission = orderCommissiones.getCustomerCommission();
        if (customerCommission == null) {
            log.info("此商品无用户分销属性, productId = {}", product.getProductId());
            return null;
        }
        // 获取分销用户
        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(product.getShopId(), product.getUserId());
        if (distributionUser == null) {
            log.error("无效用户, 或 用户服务器异常, userId = {}", product.getUserId());
            return null;
        }
        distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());

        return distributionUser.calculateRebateCommission(customerCommission).toString();
    }

    /**
     * 商品售出，获取 商户所得利润
     * -- 必须订单订单存储之后 才能调用
     *
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/getMerchantOrderIncome")
    public String getMerchantOrderIncome(@RequestParam("userId") Long userId,
                                         @RequestParam("shopId") Long shopId,
                                         @RequestParam("orderId") Long orderId) {
        if (userId == null || shopId == null || orderId == null) {
            log.error("参数异常");
            return null;
        }
        // 获取订单详情
        DistributionOrder distributionOrder = distributionOrderRepository.findByOrderId(userId, shopId, orderId);
        if (distributionOrder == null) {
            log.error("查无此单，orderId = {}", orderId);
            return null;
        }
        boolean purchaseInVipShop = distributionOrder.isPurchaseInVipShop();
        if (distributionOrder.getOrderInfo().getSource().equals(OrderSourceEnum.DOUYIN_SHOP_MANAGER.getCode())
                || distributionOrder.getOrderInfo().getSource().equals(OrderSourceEnum.DOUYIN_SHOP.getCode())) {
            purchaseInVipShop = true;
        }

        List<MerchantOrderIncomeResp> merchantOrderIncomeRespList = distributionOrder.calculateMerchantOrderIncome(purchaseInVipShop);
        if (CollectionUtils.isEmpty(merchantOrderIncomeRespList)) {
            log.error("此订单无分销属性, orderId = {}", orderId);
            return null;
        }

        return JSON.toJSONString(merchantOrderIncomeRespList);
    }

    /**
     * 获取用户商品返利
     *
     * @param appletUserProductRebate
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/queryProductRebateByUserIdShopId")
    public AppletUserProductRebateDTO queryProductRebateByUserIdShopId(@RequestBody AppletUserProductRebateDTO appletUserProductRebate) {
        log.info(">>>>>>>>>>have enter into queryProductRebateByUserIdShopId and appletUserProductRebate=" + appletUserProductRebate);
        if (appletUserProductRebate == null) {
            log.error("参数异常");
            return null;
        }
        Long userId = appletUserProductRebate.getUserId();
        Long shopId = appletUserProductRebate.getShopId();
        List<ProductDistributionDTO> productDistributionDTOList = appletUserProductRebate.getProductDistributionDTOList();
        if (CollectionUtils.isEmpty(productDistributionDTOList)) {
            log.info(">>>>>>>>>>>>>>>>have find productDistributionDTOList is null");
            return appletUserProductRebate;
        }

        // 消费时间段 -- 本月
        Date beginTime = DateUtil.getBeginOfThisMonth();
        Date endTime = new Date();

        log.info(">>>>>>>> have find distributionUser  and shopId=" + shopId + ",userId=" + userId);

        DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
        if (distributionUser == null) {
            log.info(">>>>>>>> have find distributionUser is null and shopId=" + shopId + ",userId=" + userId);
            return appletUserProductRebate;
        }
        distributionUserRepository.addUserOrderHistory(distributionUser, beginTime, endTime);

        for (ProductDistributionDTO productDistributionDTO : productDistributionDTOList) {
            productDistributionDTO.setUserId(userId);
            productDistributionDTO.setShopId(shopId);

            OrderDistributionProperty orderDistributionProperty = commonDictService.queryOrderDistributionProperty(productDistributionDTO.getProductSpecs());
            if (orderDistributionProperty == null) {
                // 容错处理，配置默认分销属性
                orderDistributionProperty = new OrderDistributionProperty();
                orderDistributionProperty.setGoodsCategoryRate(10);
                orderDistributionProperty.setTransactionRate(2);
                orderDistributionProperty.setVipShopRate(50);
            }
            BeanUtils.copyProperties(productDistributionDTO, orderDistributionProperty);

            ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(
                    orderDistributionProperty.getGoodsCategoryRate(),
                    orderDistributionProperty.getTransactionRate(),
                    orderDistributionProperty.getVipShopRate());

            OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(productDistributionDTO);
            BigDecimal customerCommission = orderCommissiones.getCustomerCommission();
            if (customerCommission == null) {
                log.info(">>>>>>>>>have find customerCommission is null ");
                continue;
            }

            BigDecimal rebateCommission = distributionUser.calculateRebateCommission(customerCommission);

            log.info(">>>>>>>>>have find rebateCommission =" + rebateCommission);
            productDistributionDTO.setRebate(rebateCommission);
        }

        log.info(">>>>>>>>>>>>>>>>>> appletUserProductRebate={}", appletUserProductRebate);
        return appletUserProductRebate;
    }

    /**
     * 存储用户获取pend存储
     *
     * @param reqs
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v8/saveUpayWxOrderPend")
    public SimpleResponse saveUpayWxOrderPend(@RequestBody List<UpayWxOrderReq> reqs) {
        if (CollectionUtils.isEmpty(reqs)) {
            return new SimpleResponse();
        }

        boolean save = upayWxOrderPendService.saveUpayWxOrderPend(reqs);
        if (!save) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "存储异常");
        }
        return new SimpleResponse();
    }

    /**
     * 粉丝完成任务 发送消息模板
     *
     * @param nickName   完成人
     * @param upperId    上级
     * @param commission 上级获得的佣金
     * @param taskName   任务名称
     */
    @GetMapping("/feignapi/distribution/applet/v8/sendTemplateMessage")
    public SimpleResponse sendTemplateMessage(@RequestParam("nickName") String nickName,
                                              @RequestParam("upperId") Long upperId,
                                              @RequestParam("commission") String commission,
                                              @RequestParam("taskName") String taskName) {
        if (StringUtils.isBlank(nickName) || upperId == null || upperId < 1 || StringUtils.isBlank(commission)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        DistributionUser pubUser = distributionUserRepository.findByUserId(upperId);
        if (!pubUser.hasShopManagerRole()) {
            log.info("上级不是店长，不发送 消息");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "上级不是店长，不发送 消息");
        }
        log.info("<<<<<<<<<< 粉丝完成任务 发送消息模板 开始发送 推送给用户 = {}<<<<<<<<<<<<<<", upperId);
        distributionUserRepository.addUserOrderHistory(pubUser, DateUtil.getBeginOfThisMonth(), new Date());
        String isDialog = "0";
        int scene = 28;
        if (pubUser.getUserInfo().getRole().equals(UserRoleEnum.SHOP_MANAGER.getCode())) {
            scene = 39;
            if (pubUser.hasReachSpendStandard()) {
                isDialog = "0";
            }
        }

        Map<String, Object> data = TemplateMessageData.fansCompleteTask(pubUser.getShopId(),
                upperId, new BigDecimal(commission), scene, isDialog, nickName, taskName);
        log.info("<<<<<<<<<< 粉丝下单 消息模板 开始发送 推送给内容 = {}<<<<<<<<<<<<<<", data);
        templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), pubUser.getShopId(), upperId, data);
        return new SimpleResponse();
    }


    /***
     * 汇总pend金额
     * @param params
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v8/sumTotalFeeByUpayWxOrderPend")
    public String sumTotalFeeByUpayWxOrderPend(@RequestBody Map<String, Object> params) {
        BigDecimal bigDecimal = upayWxOrderPendService.sumUpayWxOrderTotalFee(params);
        if (bigDecimal == null) {
            return BigDecimal.ZERO.toString();
        }
        return bigDecimal.toString();
    }

    /**
     * 获取首页佣金收益情况，包括获取当前用户的商品返利金额、在此店铺内用户推广收益情况
     *
     * @param appletUserProductRebateDTO
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v8/homePageDistributionInfo")
    public Map<String, Object> homePageDistributionInfo(@RequestBody AppletUserProductRebateDTO appletUserProductRebateDTO) {
        if (appletUserProductRebateDTO == null) {
            log.error("appletUserProductRebateDTO");
            return null;
        }
        log.info("<<<<<<<<<< appletUserProductRebateDTO = {}", appletUserProductRebateDTO);

        // 获取 对此用户 的商品返利金额
        AppletUserProductRebateDTO productRebate = queryProductRebateByUserIdShopId(appletUserProductRebateDTO);
        if (productRebate == null) {
            log.error("获取用户的商品返利金额 异常, userId = {}, shopId = {}", appletUserProductRebateDTO.getUserId(), appletUserProductRebateDTO.getShopId());
            return null;
        }

        AppletMybatisPageDto<ProductDistributionDTO> distributionProductPage = new AppletMybatisPageDto<>(appletUserProductRebateDTO.getSize(),
                appletUserProductRebateDTO.getOffset());
        distributionProductPage.setContent(productRebate.getProductDistributionDTOList());
        if (appletUserProductRebateDTO.getOffset() + appletUserProductRebateDTO.getSize() < appletUserProductRebateDTO.getTotal()) {
            distributionProductPage.setPullDown(true);
        }

        if (!CollectionUtils.isEmpty(productRebate.getProductDistributionDTOList())) {
            for (ProductDistributionDTO productDistributionDTO : productRebate.getProductDistributionDTOList()) {

                try {
                    JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                    if (mainPics.size() == 0) {
                        continue;
                    }
                    productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);
                } catch (Exception e) {
                    productDistributionDTO.setMainPics(productDistributionDTO.getMainPics());
                }

                String salesVolume = productDistributionDTO.getSalesVolume();
                int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                productDistributionDTO.setSalesVolume(NumberValidationUtil.convertNumberForString(totalSale));
            }
        }

        // 查询 在此店铺内， 用户 推广收益情况
        UserDistributionInfoDTO userDistributionInfo = queryDistributionInfo(appletUserProductRebateDTO.getUserId(), appletUserProductRebateDTO.getShopId());

        Map<String, Object> result = new HashMap<>(2);
        result.put("distributionProductPage", distributionProductPage);
        result.put("userDistributionInfo", userDistributionInfo);

        return result;
    }

    @GetMapping("/feignapi/distribution/applet/v5/sumTotalFeePendByBizTypes")
    public String sumTotalFeePendByBizTypes(@RequestParam("userId") Long userId) {
        return null;
    }

    /**
     * 批量获取  获取商品的分销佣金  bug ask qc  19/8/26
     *
     * @param distributeInfoEntityList
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/getDistrIncodeByProduct/batch")
    public List<ProductDistributCommissionEntity> getDistrIncodeByProductBatch(@RequestBody List<DistributeInfoEntity> distributeInfoEntityList) {
        List<ProductDistributCommissionEntity> resultList = new ArrayList<>();
        for (DistributeInfoEntity distributeInfoEntity : distributeInfoEntityList) {
            resultList.add(this.getDistrIncodeByProduct(distributeInfoEntity));
        }
        return resultList;
    }


    /**
     * 获取商品的分销佣金 bug ask qc 19/8/26
     *
     * @param distributeInfoEntity
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/getDistrIncodeByProduct")
    public ProductDistributCommissionEntity getDistrIncodeByProduct(@RequestBody DistributeInfoEntity distributeInfoEntity) {
        if (distributeInfoEntity == null) {
            log.info("获取商品分销佣金入参错误,入参为:null");
            return null;
        }
        Long productSpecs = distributeInfoEntity.getProductSpec();
        BigDecimal price = distributeInfoEntity.getPrice();
        BigDecimal selfSupportRatio = distributeInfoEntity.getSelfSupportRadio();
        BigDecimal platSupportRatio = distributeInfoEntity.getPlatSupportRadio();
        if (productSpecs == null || price == null || selfSupportRatio == null || platSupportRatio == null) {
            log.info("获取商品分销佣金入参错误,入参为:{}", distributeInfoEntity);
            return null;
        }
        OrderDistributionProperty orderDistributionProperty = commonDictService.queryOrderDistributionProperty(productSpecs);
        ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(
                orderDistributionProperty.getGoodsCategoryRate(),
                orderDistributionProperty.getTransactionRate(),
                orderDistributionProperty.getVipShopRate()
        );
        OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(
                price,
                selfSupportRatio,
                platSupportRatio,
                ProductDistributionTypeEnum.RESELL);

        // 分销广场展示的分销佣金
        BigDecimal squareCommission = orderCommissiones.getSquareCommission();

        ProductDistributCommissionEntity entity = new ProductDistributCommissionEntity();
        entity.setPlatDistributCommission(squareCommission);
        // 计算佣金比例 百分比
        entity.setPlatDistributRadio(squareCommission.divide(price, 3, BigDecimal.ROUND_DOWN).multiply(new BigDecimal(100)));
        log.info("qc-test->>>>>>>>>>>>> 入参={},计算结果={}", distributeInfoEntity, entity);
        return entity;
    }
}
