package mf.code.distribution.api.platform;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.distribution.model.request.platform.ShopManagerSalePolicyRequest;
import mf.code.distribution.service.platform.PlatformShopManagerService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 店长缴纳金管理Api
 * <p>
 * mf.code.distribution.api.platform
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 11:52
 */
@RestController
@RequestMapping("/api/distribution/platform/shopManager")
@RequiredArgsConstructor
@Slf4j
public class PlatformShopManagerApi {
    private final PlatformShopManagerService platformShopManagerService;

    /**
     * 保存 店长缴纳金配置
     *
     * @param shopManagerSalePolicyRequest
     * @return
     */
    @PostMapping("/saveSalePolicy")
    public SimpleResponse saveSalePolicy(@RequestBody ShopManagerSalePolicyRequest shopManagerSalePolicyRequest) {
        if (shopManagerSalePolicyRequest == null) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        String originalPriceStr = shopManagerSalePolicyRequest.getOriginalPrice();
        String salePriceStr = shopManagerSalePolicyRequest.getSalePrice();
        String shopRate0Str = shopManagerSalePolicyRequest.getShopRate0();
        String platformRate0Str = shopManagerSalePolicyRequest.getPlatformRate0();
        String superiorRate1Str = shopManagerSalePolicyRequest.getSuperiorRate1();
        String shopRate1Str = shopManagerSalePolicyRequest.getShopRate1();
        String platformRate1Str = shopManagerSalePolicyRequest.getPlatformRate1();

        if (StringUtils.isBlank(originalPriceStr) || !RegexUtils.isAmount(originalPriceStr)
                || StringUtils.isBlank(salePriceStr)
                || StringUtils.isBlank(shopRate0Str) || !RegexUtils.StringIsNumber(shopRate0Str)
                || StringUtils.isBlank(platformRate0Str) || !RegexUtils.StringIsNumber(platformRate0Str)
                || StringUtils.isBlank(superiorRate1Str) || !RegexUtils.StringIsNumber(superiorRate1Str)
                || StringUtils.isBlank(shopRate1Str) || !RegexUtils.StringIsNumber(shopRate1Str)
                || StringUtils.isBlank(platformRate1Str) || !RegexUtils.StringIsNumber(platformRate1Str)) {
            log.error("参数异常, shopManagerSalePolicyRequest = {}", shopManagerSalePolicyRequest);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        BigDecimal originalPrice = new BigDecimal(originalPriceStr);
        BigDecimal salePrice = new BigDecimal(salePriceStr);
        int shopRate0 = Integer.parseInt(shopRate0Str);
        int platformRate0 = Integer.parseInt(platformRate0Str);
        int superiorRate1 = Integer.parseInt(superiorRate1Str);
        int shopRate1 = Integer.parseInt(shopRate1Str);
        int platformRate1 = Integer.parseInt(platformRate1Str);
        if (originalPrice.compareTo(salePrice) < 0
                || shopRate0 > 100 || platformRate0 != (100 - shopRate0)
                || (superiorRate1 + shopRate1) > 100 || platformRate1 != (100 - superiorRate1 - shopRate1)) {
            log.error("参数异常, shopManagerSalePolicyRequest = {}", shopManagerSalePolicyRequest);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        return platformShopManagerService.saveSalePolicy(shopManagerSalePolicyRequest);
    }

    /**
     * 获取 店长缴纳金配置
     *
     * @return
     */
    @GetMapping("/querySalePolicy")
    public SimpleResponse querySalePolicy() {

        return platformShopManagerService.querySalePolicy();
    }

}
