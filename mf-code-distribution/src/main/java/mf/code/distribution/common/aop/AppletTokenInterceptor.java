package mf.code.distribution.common.aop;

import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.RegexUtils;
import mf.code.common.utils.TokenUtil;
import mf.code.distribution.api.feignclient.ShopAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.common.aop
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-06 9:08
 */
@Slf4j
public class AppletTokenInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private ShopAppService shopAppService;

    @Value("${applet.token.debug.mode}")
    private Integer debugMode;
    private final String TOKEN_NAME = "token";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String project = request.getHeader("project");
        if (StringUtils.isBlank(project)) {
            return tokenErrorReturn(response);
        }
        if ("jkmf".equals(project)) {
            return this.checkTokenForJKMF(request, response, handler);
        }
        if ("ddd".equals(project)) {
            return this.checkTokenForDDD(request, response, handler);
        }
        if ("dxp".equals(project)) {
            return this.checkTokenForDXP(request, response, handler);
        }
        return tokenErrorReturn(response);
    }

    private boolean checkTokenForDXP(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // TODO: 此处 可对指定API再放行处理

        Map<String, String> reqParams = getRequestParams(request);
        String userId = reqParams.get("userId");

        // 如果关掉校验直接通过
        if (debugMode != null && debugMode == 1) {
            log.warn("appletToken校验已关闭");
            return true;
        }

        // 正常验签
        String token = request.getHeader(TOKEN_NAME);
        if (StringUtils.isBlank(token)) {
            log.error("token不存在, 需前端静默请求AppletLoginApi，验证用户是否授权或生成新token");
            return tokenErrorReturn(response);
        }
        if (StringUtils.isBlank(userId)) {
            log.error("请求参数中，不存在merchantId参数");
            return tokenErrorReturn(response);
        }
        UserResp userResp = userAppService.queryUser(Long.valueOf(userId));

        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.DOU_XIAO_PU);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(response);
        }
        if (!StringUtils.equals(userId, stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 请求参数中的uid不匹配");
            return tokenErrorReturn(response);
        }
        if (userResp == null || !StringUtils.equals(userResp.getOpenId(), stringObjectMap.get("user"))) {
            log.error("token解密user数据 与 数据库中的openId不匹配");
            return tokenErrorReturn(response);
        }
        String md5Str = TokenUtil.md5forDigest(userResp.getId().toString(), userResp.getOpenId(), userResp.getCtime(), TokenUtil.DOU_XIAO_PU);
        if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
            log.error("token解密后验签失败,token = " + token);
            return tokenErrorReturn(response);
        }
        return true;
    }

    private boolean checkTokenForDDD(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // TODO: 此处 可对指定API再放行处理

        Map<String, String> reqParams = getRequestParams(request);
        String merchantId = reqParams.get("merchantId");

        // 如果关掉校验直接通过
        if (debugMode != null && debugMode == 1) {
            log.warn("appletToken校验已关闭");
            return true;
        }
        // 正常验签
        String token = request.getHeader(TOKEN_NAME);
        if (StringUtils.isBlank(token)) {
            log.error("token不存在, 需前端静默请求AppletLoginApi，验证用户是否授权或生成新token");
            return tokenErrorReturn(response);
        }
        if (StringUtils.isBlank(merchantId)) {
            log.error("请求参数中，不存在merchantId参数");
            return tokenErrorReturn(response);
        }
        MerchantDTO merchant = shopAppService.queryMerchantById(Long.valueOf(merchantId));

        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.DOU_DAI_DAI);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(response);
        }
        if (!StringUtils.equals(merchantId, stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 请求参数中的uid不匹配");
            return tokenErrorReturn(response);
        }
        if (merchant == null || !StringUtils.equals(merchant.getOpenId(), stringObjectMap.get("user"))) {
            log.error("token解密user数据 与 数据库中的openId不匹配");
            return tokenErrorReturn(response);
        }
        String md5Str = TokenUtil.md5forDigest(merchant.getId().toString(), merchant.getOpenId(), merchant.getCtime(), TokenUtil.DOU_DAI_DAI);
        if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
            log.error("token解密后验签失败,token = " + token);
            return tokenErrorReturn(response);
        }
        return true;
    }

    private boolean checkTokenForJKMF(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Map<String, String> reqParams = getRequestParams(request);
        String userId = reqParams.get("userId");

        // 如果关掉校验直接通过
        if (debugMode != null && debugMode == 1) {
            return true;
        }

        // 正常验签
        String token = request.getHeader(TOKEN_NAME);
        if (StringUtils.isBlank(token)) {
            log.error("token不存在, 需前端静默请求AppletLoginApi，验证用户是否授权或生成新token");
            return tokenErrorReturn(response);
        }
        // 问题：新人大转盘 试试手气 请求参数中 uid获取不到……从token中 获取uid 容错
        if (StringUtils.isBlank(userId)) {
            log.error("请求参数中，不存在uid参数");
            return tokenErrorReturn(response);
        }
        UserResp user = userAppService.queryUser(Long.valueOf(userId));
        if (user == null) {
            log.error("无效用户");
            return tokenErrorReturn(response);
        }

        /**
         *   uid 用户id，商户id，平台运营用户id
         *   user 小程序openid，微信公众号openid，商户手机号，平台运营用户手机号
         *   ctime 用户，商户，平台运营用户的数据记录的创建时间，yyyyMMddHHmmss格式
         *   now 系统时间，yyyyMMddHHmmss格式
         *   digest
         *
         *   XXX 固定值，场景不同，结尾语不同
         *   YYY 固定值，场景不同，结尾语不同
         *   ZZZ 固定值，场景不同，结尾语不同
         */
        String systemToken = TokenUtil.encryptToken(userId, user.getOpenId(), user.getCtime(), TokenUtil.APPLET);
        log.info("<<<<<<<<<<<<<< 请求requestToken = {}, systemToken = {} <<<<<<<<<<<", token, systemToken);

        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.APPLET);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(response);
        }
        if (!StringUtils.equals(userId, stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 请求参数中的uid不匹配");
            return tokenErrorReturn(response);
        }
        if (!StringUtils.equals(user.getOpenId(), stringObjectMap.get("user"))) {
            log.error("token解密user数据 与 数据库中的openId不匹配");
            return tokenErrorReturn(response);
        }
        String md5Str = TokenUtil.md5forDigest(user.getId().toString(), user.getOpenId(), user.getCtime(), TokenUtil.APPLET);
        if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
            log.error("token解密后验签失败,token = {}, digest = {}, md5Str = {}" + token, stringObjectMap.get("digest"), md5Str);
            return tokenErrorReturn(response);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    }

    private Map<String, String> getRequestParams(HttpServletRequest request) {
        String uid = request.getParameter("uid");
        String userId = request.getParameter("userId");
        String pubUid = request.getParameter("pub_uid");
        String aid = request.getParameter("aid");
        String activityId = request.getParameter("activityId");
        String pubType = request.getParameter("type");
        String merchantId = request.getParameter("merchantId");

        Map<String, String> reqParams = new HashMap<>();

        if (StringUtils.isNotBlank(uid) && RegexUtils.StringIsNumber(uid)) {
            reqParams.put("userId", uid);
        }
        if (StringUtils.isNotBlank(userId) && RegexUtils.StringIsNumber(userId)) {
            reqParams.put("userId", userId);
        }
        if (StringUtils.isNotBlank(pubUid) && RegexUtils.StringIsNumber(pubUid)) {
            reqParams.put("pubUserId", pubUid);
        }
        if (StringUtils.isNotBlank(aid) && RegexUtils.StringIsNumber(aid)) {
            reqParams.put("activityId", aid);
        }
        if (StringUtils.isNotBlank(activityId) && RegexUtils.StringIsNumber(activityId)) {
            reqParams.put("activityId", activityId);
        }
        if (StringUtils.isNotBlank(pubType) && RegexUtils.StringIsNumber(pubType)) {
            reqParams.put("pubType", pubType);
        }
        if (StringUtils.isNotBlank(merchantId) && RegexUtils.StringIsNumber(merchantId)) {
            reqParams.put("merchantId", merchantId);
        }

        return reqParams;
    }

    /**
     * token 异常
     *
     * @param response 入参HttpServletResponse
     * @return 返回boolean
     */
    private boolean tokenErrorReturn(HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            JSONObject res = new JSONObject();
            res.put("code", "-2");
            res.put("message", "需要重新登录");
            res.put("data", "");
            out.append(res.toString());
            out.flush();
        } catch (IOException e) {
            log.error("response发送失败:" + e);
            try {
                response.sendError(500);
            } catch (IOException e1) {
                log.error("这里不应该出错:" + e);
            }
        }
        return false;
    }
}
