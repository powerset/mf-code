package mf.code.distribution.common.redis;

/**
 * mf.code.user.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 00:13
 */
public class RedisKeyConstant {

	/**
	 * 店长发展粉丝成为店长
	 */
	public static final String RECOMMEND_SHOP_MANAGER_AWARD = "jkmf:distribution:task:shopmanager:recommend:";// <uid> 奖励者

	/**
	 * 店长邀请粉丝奖励
	 */
	public static final String SHOP_MANAGER_INVITE_FANS_AWARD = "jkmf:distribution:task:shopmanager:invite:";// <uid> 奖励者

	/**
	 * 定时任务 用户返利分佣 结算防重
	 */
	public static final String USER_ORDER_SETTLEMENT = "jkmf:distribution:order:settlement:forbid:repeat";

	public static final String PLATFORM_SUBSIDY_USER_ESTIMATE_INCOME = "jkmf:distribution:estimateincome:forbidrepeat:subsidy";


	/**
	 * 团队佣金结算
	 */
	public static final String TEAM_COMMISSION_SETTLED = "jkmf:distribution:team:commission:settlement:";//<shopId>:<userId>

	/**
	 * 达到团队分佣标准的用户
	 */
	public static final String MEET_TEAM_COMMISSION_STANDARD_USER = "jkmf:distribution:team:commission:meet:";// <shopId>


	/**
	 * 当前店铺下 所有用户的收益排行榜
	 */
	public static final String INCOME_RANK_LIST_OF_SHOP = "jkmf:distribution:rank:income:shop:"; //<shopId>

	/**
	 * 获取当前店铺下 用户好友收益排行榜(临时-score:0)
	 */
	public static final String INCOME_RANK_LIST_OF_FRIENDS_TEMP = "jkmf:distribution:rank:income:friend:temp:"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 用户好友收益排行榜(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_FRIENDS = "jkmf:distribution:rank:income:friend:"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 团队贡献榜 已结算(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_TEAM_SETTLED = "jkmf:distribution:rank:income:team:settled"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 团队贡献榜 已结算(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_TEAM_PENDING = "jkmf:distribution:rank:income:team:pending"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 团队贡献榜 已结算(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_TEAM_TOTAL = "jkmf:distribution:rank:income:team:total"; //<shopId>:<userId>

	/**
	 * 店长剩余数
	 */
	public static final String SHOPMANAGER_LEFTSIZE = "jkmf:distribution:shopmanager:leftsize:"; // <uid>
	/**
	 * 店长指导链路
	 */
	public static final String SHOPMANAGER_GUIDE = "jkmf:distribution:shopmanager:guide:"; // <uid>
}
