package mf.code.distribution.common.config;


import mf.code.distribution.common.aop.AppletTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 17:28
 */
@Configuration
public class TokenConfig extends WebMvcConfigurationSupport {

    /**
     * 添加拦截器之前先自己创建一下这个Spring Bean
     * 将拦截器注册为bean也交给spring管理,这样拦截器内部注入其他的bean对象也就可以被spring识别了
     */
    @Bean
    AppletTokenInterceptor appletTokenInterceptor() {
        return new AppletTokenInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注意，这里添加和忽略的path，都是controller中配置的path开始，但是不包含server.servlet.context-path配置的path
        registry.addInterceptor(appletTokenInterceptor()).addPathPatterns("/api/distribution/applet/**").excludePathPatterns(
                "/api/user/applet/v5/login",
                "/api/web/applet/homepage/queryHomePage",
                "/api/goods/applet/v5/getProductsByGroupId",
                "/api/goods/applet/v1/goods/plate/distribution/getSpecesAndBanners",
                "/api/goods/applet/v1/goods/plate/distribution/list",
                "/api/applet/v3/checkpointTaskSpace/getBarrage",
                "/api/goods/applet/v5/getGoodsDetail",
                "/api/comment/applet/queryProductCommentPage"
        );

        super.addInterceptors(registry);
    }

    /**
     * 静态资源
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }
}
