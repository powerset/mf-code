package mf.code.distribution.common.caller.wxmp.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * mf.code.common.caller.wxmp.vo
 *
 * @description: 推送消息
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月26日 14:53
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WxSendMsgVo {
    public static final String HOME_PAGE = "pages/index/login";

    //用户openid
    private String touser;
    //模版id
    private String template_id;
    //默认跳到小程序首页
    private String page;
    //收集到的用户formid
    private String form_id;
    //放大那个推送字段
    private String emphasis_keyword;
    //推送文字
    private Map<String, TemplateData> data;

    @Data
    public static class TemplateData {
        //keyword1：，keyword2：，keyword3：，keyword4：，keyword5备注
        private String value;//,,依次排下去
    }

    public void from(String templateID, String openID, String formID, String skipUrl, Map<String, TemplateData> data, String emphasisKeyword) {
        this.setTemplate_id(templateID);
        this.setTouser(openID);
        this.setForm_id(formID);
        this.setPage(HOME_PAGE);
        if (StringUtils.isNotBlank(emphasisKeyword)) {
            this.setEmphasis_keyword(emphasisKeyword);
        }
        if (StringUtils.isNotBlank(skipUrl)) {
            this.setPage(HOME_PAGE + "?" + skipUrl);
        }
        if (data != null) {
            this.setData(data);
        }
    }
}
