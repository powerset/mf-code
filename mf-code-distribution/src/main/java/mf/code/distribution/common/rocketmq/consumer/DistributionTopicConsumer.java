package mf.code.distribution.common.rocketmq.consumer;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.distribution.common.rocketmq.consumer.impl.DistributionTopicConsumerImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

/**
 * mf.code.common.rocketmq.consumer
 * Description:
 *
 * @author gel
 * @date 2019-05-06 19:04
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "distribution", consumerGroup = "distribution-consumer")
public class DistributionTopicConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    @Autowired
    private DistributionTopicConsumerImpl distributionTopicConsumerImpl;

    @Override
    public void onMessage(MessageExt message) {
        String bizValue = new String(message.getBody(), Charset.forName("UTF-8"));
        String msgId = message.getMsgId();
        String tags = message.getTags();
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.DISTRIBUTION_ORDER.getTag(), tags)) {
            distributionTopicConsumerImpl.createOrder(msgId, bizValue);
        }
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.DISTRIBUTION_ORDER_REFUND.getTag(), tags)) {
            distributionTopicConsumerImpl.cancelOrder(msgId, bizValue);
        }
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        // set consumer consume message from now
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

}
