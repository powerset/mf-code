package mf.code.distribution.common.rocketmq.consumer;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.distribution.common.rocketmq.consumer.impl.BizTypeTopicConsumerService;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

/**
 * mf.code.distribution.common.rocketmq.consumer
 * Description:
 *
 * @author gel
 * @date 2019-07-08 10:41
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "bizLog", consumerGroup = "bizLog-consumer")
public class BizTypeTopicConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {

    @Autowired
    private BizTypeTopicConsumerService bizTypeTopicConsumerService;

    @Override
    public void onMessage(MessageExt message) {
        String bizValue = new String(message.getBody(), Charset.forName("UTF-8"));
        String msgId = message.getMsgId();
        String tags = message.getTags();
        log.info("fan_action埋点消息:" + message.toString());
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION.getTag(), tags)) {
            bizTypeTopicConsumerService.saveBizLog(msgId, bizValue);
        } else {
            log.error("无消费类型：" + message.toString());
        }

    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }
}
