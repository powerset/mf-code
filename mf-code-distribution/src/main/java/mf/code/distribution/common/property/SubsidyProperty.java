package mf.code.distribution.common.property;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.domain.valueobject.UserSubsidyPolicy;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.distribution.common.property
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-12 11:27
 */
@ConfigurationProperties(prefix="subsidy")
@Data
@Component("subsidyProperty")
@RefreshScope
public class SubsidyProperty {
	// 平台补贴配置参数
	private String configParams;

	/**
	 * 获取补贴分组数
	 * @return
	 */
	public int queryGroupSize() {
		if (StringUtils.isBlank(this.configParams)) {
			return 0;
		}
		String[] split = this.configParams.split(";");
		// 如果组的具体参数个数 不为6
		if (split[0].split(",").length != 6) {
			return 0;
		}
		return split.length;
	}

	/**
	 * 通过当前组数 获取相应 分组的配置
	 * 平台补贴订单group1,20,5,2,2.5,3.5;平台补贴订单group2,20,10,3,2.5,3.5;平台补贴订单group3,20,15,4,2.5,3.5
	 * @param groupNO
	 * @return
	 */
	public UserSubsidyPolicy findUserSubsidyPlicyByGroupNo(Long groupNO) {
		String groupSubSidyParams = this.configParams.split(";")[groupNO.intValue()];
		String[] subsidyParams = groupSubSidyParams.split(",");

		String subsidyRemark = subsidyParams[0];
		String indirectLowerMembersRate = subsidyParams[1];
		String subsidyAmount = subsidyParams[2];
		String subsidyDays = subsidyParams[3];
		String minAmount = subsidyParams[4];
		String maxAmount = subsidyParams[5];

		UserSubsidyPolicy userSubsidyPolicy = new UserSubsidyPolicy();
		// 按配置的比例20% 获取当前作为补贴上级的非直接用户的人数, 非直接下级人数 * 20% <= 0 ，不补贴 --  -- 向下取整
		userSubsidyPolicy.setIndirectLowerMembersRate(Integer.valueOf(indirectLowerMembersRate));
		// 当时的补贴金额
		userSubsidyPolicy.setSubsidyAmount(new BigDecimal(subsidyAmount));
		// 多少日补贴完成 -- 默认28天
		userSubsidyPolicy.setSubsidyDays(Integer.valueOf(subsidyDays));
		// 配置每单补贴的金额范围 -- 最小金额：约80元商品的分佣金额
		userSubsidyPolicy.setMinAmount(Double.valueOf(minAmount));
		// 配置每单补贴的金额范围 -- 最大金额：约120元商品的分佣金额
		userSubsidyPolicy.setMaxAmount(Double.valueOf(maxAmount));
		// 平台补贴订单 备注
		userSubsidyPolicy.setSubsidyRemark(subsidyRemark);

		return userSubsidyPolicy;
	}
}
