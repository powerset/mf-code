package mf.code.distribution.common.redis;

import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * redis防重工具
 */
public class RedisForbidRepeat {

    public static final String TASK = "task:submit:repeat:taskid:uid:";
    public static final String ACTIVITY_PLAN_ITEM = "activity:plan:item:repeat:key:";
    public static final String ACTIVITY_PLAN = "activity:plan:repeat:key:";
    public static final String TASKSPACE = "activity:plan:repeat:key:";


    /**
     * 防重
     *
     * @param redisTemplate
     * @param key
     * @return true: 无此key / false: 已经有此key
     */
    public static boolean judge(StringRedisTemplate redisTemplate, String key) {
        return redisTemplate.opsForValue().setIfAbsent(key, "1");
    }

    /**
     * @param redisTemplate redsi
     * @param key           redis key
     * @param timeout       过期时间
     * @param unit          时间单位
     * @return true: 无此key / false: 已经有此key
     */
    public static boolean judge(StringRedisTemplate redisTemplate, String key, long timeout, TimeUnit unit) {
        if (redisTemplate.opsForValue().setIfAbsent(key, "1")) {
            redisTemplate.expire(key, timeout, unit);
            return true;
        }
        return false;
    }

    public static boolean NX(StringRedisTemplate redisTemplate, String redisKey) {
        return RedisForbidRepeat.NX(redisTemplate, redisKey, 3, TimeUnit.SECONDS);
    }
    public static boolean NX(StringRedisTemplate redisTemplate, String redisKey, int timeout, TimeUnit unit) {
        if (redisTemplate.opsForValue().setIfAbsent(redisKey, "1")) {
            redisTemplate.expire(redisKey, timeout, unit);
            return true;
        }
        return false;
    }

    public static SimpleResponse NXError() {
        SimpleResponse response = new SimpleResponse();
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "redis防重拦截");
        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
        response.setMessage("点击太快，请稍候再试");
        response.setData(resultVO);
        return response;
    }

}
