package mf.code.distribution.common.config;

import mf.code.distribution.common.filter.AccessFilter;
import mf.code.distribution.common.filter.AppletTokenFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-11-10 15:36
 */
@Configuration
public class FilterConfig {

    @Value("${seller.uid.name}")
    private String sellerUidName;
    @Value("${seller.token.debug.mode}")
    private String debugMode;
    @Value("${platform.uid.name}")
    private String platformUidName;
    @Value("${platform.token.debug.mode}")
    private String platformDebugMode;
    /**
     * 注册跨域filter
     */
    @Bean
    public FilterRegistrationBean accessFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        //注入过滤器
        registrationBean.setFilter(new AccessFilter());
        //拦截规则
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        //过滤器名称
        registrationBean.setName("accessFilter");
        //过滤器顺序
        registrationBean.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
        return registrationBean;
    }

    /**
     * 注册appletTokenFilter
     */
    @Bean
    public FilterRegistrationBean appletTokenFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        AppletTokenFilter appletTokenFilter = new AppletTokenFilter();
        registrationBean.setFilter(appletTokenFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/api/distribution/applet/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.setName("appletTokenFilter");
        return registrationBean;
    }
}
