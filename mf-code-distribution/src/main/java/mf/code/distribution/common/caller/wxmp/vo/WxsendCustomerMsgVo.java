package mf.code.distribution.common.caller.wxmp.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * mf.code.common.caller.wxmp.vo
 *
 * @description: 发送客服消息给用户
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月05日 10:28
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WxsendCustomerMsgVo {
    public static final String MSGTYPE_TEXT = "text";
    public static final String MSGTYPE_IMAGE = "image";
    public static final String MSGTYPE_LINK = "link";
    public static final String MSGTYPE_MINIPROGRAMPAGE = "miniprogrampage";

    //用户openid
    private String touser;
    //消息类型
    private String msgtype;
    //文本消息内容，msgtype="text" 时必填
    private Text text;
    //图片消息，msgtype="image" 时必填,该功能需要 《新增素材接口》 进行交互 上传图片文件获得
    private Image image;
    //图片消息，msgtype="link" 时必填
    private Link link;
    //小程序卡片，msgtype="miniprogrampage" 时必填
    private Miniprogrampage miniprogrampage;

    public void from(String openID , String msgtype){
        this.touser = openID;
        this.msgtype = msgtype;
    }

    @Data
    public static class Miniprogrampage {
        //消息标题
        private String title;
        //小程序的页面路径，跟app.json对齐，支持参数，比如pages/index/index?foo=bar
        private String pagepath;
        //小程序消息卡片的封面， image 类型的 media_id，通过 新增素材接口 上传图片文件获得，建议大小为 520*416
        private String thumb_media_id;
    }

    @Data
    public static class Link {
        //消息标题
        private String title;
        //图文链接消息
        private String description;
        //图文链接消息被点击后跳转的链接
        private String url;
        //图文链接消息的图片链接，支持 JPG、PNG 格式，较好的效果为大图 640 X 320，小图 80 X 80
        private String thumb_url;
    }

    @Data
    public static class Image {
        //发送的图片的媒体ID，通过 新增素材接口 上传图片文件获得
        private String media_id;
    }

    @Data
    public static class Text {
        //文本消息內容
        private String content;
    }

    /***
     *
     * 关于重试的消息排重，有 msgid 的消息推荐使用 msgid 排重。事件类型消息推荐使用 FromUserName + CreateTime 排重。
     *
     * errcode 的合法值
     *
     * 值	说明
     * 0	请求成功
     * -1	系统繁忙，此时请开发者稍候再试
     * 40001	获取 access_token 时 AppSecret 错误，或者 access_token 无效。请开发者认真比对 AppSecret 的正确性，或查看是否正在为恰当的小程序调用接口
     * 40002	不合法的凭证类型
     * 40003	不合法的 OpenID，请开发者确认 OpenID 否是其他小程序的 OpenID
     * 45015	回复时间超过限制
     * 45047	客服接口下行条数超过上限
     * 48001	API 功能未授权，请确认小程序已获得该接口
     *
     *
     * 发送文本消息
     * {
     *   "touser":"OPENID",
     *   "msgtype":"text",
     *   "text":
     *   {
     *     "content":"Hello World"
     *   }
     * }
     * 发送文本消息时，支持添加可跳转小程序的文字连接
     * 文本内容...<a href="http://www.qq.com" data-miniprogram-appid="appid" data-miniprogram-path="pages/index/index">点击跳小程序</a>
     * 说明：
     * data-miniprogram-appid 项，填写小程序appid，则表示该链接跳转小程序；
     * data-miniprogram-path项，填写小程序路径，路径与app.json中保持一致，可带参数；
     * 对于不支持data-miniprogram-appid 项的客户端版本，如果有herf项，则仍然保持跳href中的链接；
     * 小程序发带小程序文字链的文本消息，data-miniprogram-appid必须是该小程序的appid。
     *
     * 发送图片消息
     * {
     *   "touser":"OPENID",
     *   "msgtype":"image",
     *   "image": {
     *     "media_id":"MEDIA_ID"
     *   }
     * }
     *
     * 发送图文链接
     * 每次可以发送一个图文链接
     *
     * {
     *   "touser": "OPENID",
     *   "msgtype": "link",
     *   "link": {
     *     "title": "Happy Day",
     *     "description": "Is Really A Happy Day",
     *     "url": "URL",
     *     "thumb_url": "THUMB_URL"
     *   }
     * }
     *
     * 发送小程序卡片
     * {
     *  "touser":"OPENID",
     *  "msgtype":"miniprogrampage",
     *  "miniprogrampage": {
     *    "title":"title",
     *    "pagepath":"pagepath",
     *    "thumb_media_id":"thumb_media_id"
     *  }
     * }
     */
}
