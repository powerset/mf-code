package mf.code.distribution.common.rocketmq.consumer.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.OrderCommissiones;
import mf.code.distribution.domain.valueobject.OrderDistributionProperty;
import mf.code.distribution.domain.valueobject.ProductDistributionPolicy;
import mf.code.distribution.domain.valueobject.UserShopTeamCommission;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.distribution.repo.mongo.po.FanAction;
import mf.code.distribution.repo.mongo.repository.FanActionRepository;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.common.rocketmq.consumer.impl
 * Description:
 *
 * @author gel
 * @date 2019-07-08 18:01
 */
@Slf4j
@Service
public class BizTypeTopicConsumerService {

    @Autowired
    private FanActionRepository fanActionRepository;
    @Autowired
    private DistributionUserRepository distributionUserRepository;

    public void saveBizLog(String msgId, String bizValue) {
        FanActionForMQ fanActionForMQ = JSON.parseObject(bizValue, FanActionForMQ.class);
        FanAction fanAction = new FanAction();
        BeanUtils.copyProperties(fanActionForMQ, fanAction);
        // 如果是邀请成为新用户则需要补充粉丝数目
        if (StringUtils.equals(fanActionForMQ.getEvent(), FanActionEventEnum.INVITATION.getCode())) {
            // 存在重复调用问题
            FanAction queryFanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(fanActionForMQ.getSid(), fanActionForMQ.getUid(),
                    fanActionForMQ.getEvent(), fanActionForMQ.getEventId());
            if (queryFanAction != null) {
                return;
            }
            Map<String, Object> fansNum = new HashMap<>();
            DistributionUser distributionUser = distributionUserRepository.findByUserId(fanActionForMQ.getUid());
            distributionUserRepository.addUserShopTeamForLowerMember(distributionUser);
            List<Long> longList = distributionUser.queryUserShopTeams();
            fansNum.put("fansNum", 0);
            if (!CollectionUtils.isEmpty(longList)) {
                fansNum.put("fansNum", longList.size() - 1);
            }
            fanAction.setExtra(fansNum);
        }
        // 如果是订单创建和支付，需要计算佣金
        if (StringUtils.equals(fanAction.getEvent(), FanActionEventEnum.ORDER.getCode())) {
            FanAction queryFanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(fanActionForMQ.getSid(), fanActionForMQ.getUid(),
                    FanActionEventEnum.ORDER.getCode(), fanActionForMQ.getEventId());
            if (queryFanAction != null) {
                return;
            }
            if (!calcDistributionFroOrder(fanActionForMQ, fanAction)) {
                return;
            }
        }
        if (StringUtils.equals(fanAction.getEvent(), FanActionEventEnum.PAY.getCode())) {
            FanAction queryFanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(fanActionForMQ.getSid(), fanActionForMQ.getUid(),
                    FanActionEventEnum.ORDER.getCode(), fanActionForMQ.getEventId());
            if (queryFanAction == null) {
                return;
            }
            fanAction = queryFanAction;
            fanAction.setCurrent(DateUtil.getCurrentMillis());
            fanAction.setEvent(FanActionEventEnum.PAY.getCode());
            if (!calcDistributionFroOrder(fanActionForMQ, fanAction)) {
                return;
            }
        }
        // 如果是取消订单，则更改记录为取消
        if (StringUtils.equals(fanAction.getEvent(), FanActionEventEnum.CANCEL_ORDER.getCode())) {
            FanAction queryFanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(fanActionForMQ.getSid(), fanActionForMQ.getUid(),
                    FanActionEventEnum.ORDER.getCode(), fanActionForMQ.getEventId());
            if (queryFanAction == null) {
                queryFanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(fanActionForMQ.getSid(), fanActionForMQ.getUid(),
                        FanActionEventEnum.PAY.getCode(), fanActionForMQ.getEventId());
            }
            // 不存在订单记录，无需继续记录
            if (queryFanAction == null) {
                return;
            }
            fanAction = queryFanAction;
            fanAction.setEvent(FanActionEventEnum.CANCEL_ORDER.getCode());
            fanAction.setCurrent(DateUtil.getCurrentMillis());
        }
        if (StringUtils.equals(fanAction.getEvent(), FanActionEventEnum.VIEW_GOODS.getCode())) {
            // 如果用户重复查看某个商品直接更新这条记录
            FanAction queryFanAction = fanActionRepository.queryBySidAndUidAndEventAndEventid(fanActionForMQ.getSid(), fanActionForMQ.getUid(),
                    fanActionForMQ.getEvent(), fanActionForMQ.getEventId());
            if (queryFanAction != null) {
                fanAction = queryFanAction;
                fanAction.setCurrent(DateUtil.getCurrentMillis());
            }
        }
        fanAction.setCtime(new Date());
        fanActionRepository.save(fanAction);
    }

    /**
     * 计算订单分销金额
     *
     * @param fanActionForMQ
     * @param fanAction
     * @return
     */
    private boolean calcDistributionFroOrder(FanActionForMQ fanActionForMQ, FanAction fanAction) {
        OrderDistributionProperty orderDistributionProperty = JSON.parseObject(JSON.toJSONString(fanActionForMQ.getExtra()), OrderDistributionProperty.class);
        if (orderDistributionProperty == null) {
            log.error("此订单未获取到分销属性, orderId = {}", fanActionForMQ.getEventId());
            return false;
        }
        // 此订单的 平台类目抽佣比率
        Integer goodsCategoryRate = orderDistributionProperty.getGoodsCategoryRate();
        // 此订单的 平台交易税率
        Integer transactionRate = orderDistributionProperty.getTransactionRate();
        // 此订单的 粉丝店铺分佣
        Integer vipShopRate = orderDistributionProperty.getVipShopRate();
        // 获取商品的分销政策
        ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(goodsCategoryRate, transactionRate, vipShopRate);
        // 计算各部分的分销佣金
        OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(orderDistributionProperty, fanActionForMQ.getAmount());
        if (orderCommissiones != null && orderCommissiones.getCustomerCommission() != null) {
            // 获取分销用户
            DistributionUser distributionUser = distributionUserRepository.findByUserId(fanActionForMQ.getUid());
            if (distributionUser == null) {
                log.error("无效用户, 或 用户服务器异常, userId = {}", fanActionForMQ.getUid());
                return false;
            }
            // 添加历史订单，方便按照不同的就算比例计算应得分销佣金
            distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());
            List<UserShopTeamCommission> userShopTeamCommissionList = distributionUser.calculateCustomerCommissionsForEachMember(orderCommissiones.getCustomerCommission(), fanActionForMQ.getAmount());
            if (!CollectionUtils.isEmpty(userShopTeamCommissionList)) {
                BigDecimal commission = BigDecimal.ZERO;
                for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
                    commission = commission.add(userShopTeamCommission.getPerPubCommission());
                }
                fanAction.setTax(commission);
            }
        }
        if (fanAction.getTax() == null) {
            fanAction.setTax(BigDecimal.ZERO);
        }
        return true;
    }
}
