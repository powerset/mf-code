package mf.code.distribution.common.job;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.ShopAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.common.property.SubsidyProperty;
import mf.code.distribution.common.property.WxmpProperty;
import mf.code.distribution.common.property.WxpayProperty;
import mf.code.distribution.common.redis.RedisForbidRepeat;
import mf.code.distribution.common.redis.RedisKeyConstant;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.UpayWxOrderPendService;
import mf.code.distribution.service.UserTeamIncomeService;
import mf.code.distribution.service.templatemessage.TemplateMessageService;
import mf.code.distribution.service.templatemessage.message.TemplateMessageData;
import mf.code.order.dto.OrderResp;
import mf.code.order.dto.OrderRespListDTO;
import mf.code.user.dto.UpayWxOrderReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 平台补贴用户预估收益
 *
 * mf.code.distribution.common.job
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-03 16:48
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class SubsidyUserScheduledService {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private DistributionUserRepository distributionUserRepository;
	@Autowired
	private UserAppService userAppService;
	@Autowired
	private OrderAppService orderAppService;
	@Autowired
	private UpayWxOrderPendService upayWxOrderPendService;
	@Autowired
	private WxpayProperty appProperty;
	@Autowired
	private SubsidyProperty subsidyProperty;
	@Autowired
	private ShopAppService shopAppService;
	@Autowired
	private TemplateMessageService templateMessageService;
	@Autowired
	private WxmpProperty wxmpProperty;
	@Autowired
	private UserTeamIncomeService userTeamIncomeService;

	@Value("${taskExecutor.jobMode}")
	private Integer jobMode = 1;


	/**
	 * 每天执行异常
	 */
	// @Scheduled(fixedDelay = 60000)
	@Scheduled(cron = "0 0 1 1/1 * ?")
	public void userOrderSettlement() {
		if (jobMode == 0) {
			return;
		}
		// 补贴防重
		boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.PLATFORM_SUBSIDY_USER_ESTIMATE_INCOME, 20, TimeUnit.HOURS);
		if (!success) {
			log.error("平台补贴用户预估收益，redis防重，已有定时任务开始执行");
			return;
		}
		// 获取平台所有用户id -- TODO
		List<Long> userIds = userAppService.selectAllIds();
		if (CollectionUtils.isEmpty(userIds)) {
			return;
		}
		int groupSize = subsidyProperty.queryGroupSize();
		if (groupSize == 0) {
			log.info("未配置 平台补贴参数");
			return;
		}

		log.info("<<<<<<<<<<<<<补贴订单器开始执行<<<<<<<<<<<<<<");
		for (Long userId : userIds) {
			Long groupNo = userId % groupSize;

			DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
			if (distributionUser == null) {
				continue ;
			}
			distributionUserRepository.addUserSubsidyPolicy(distributionUser, groupNo);

			if (distributionUser.hasCompleteSubsidy()) {
				// 已完成
				continue;
			}
			distributionUserRepository.addUserShopTeamForLowerMember(distributionUser);
			if (!distributionUser.hasReachTeamSubsidyStandard()) {
				// 不达标
				continue;
			}
			distributionUserRepository.addUserDistributionHistory(distributionUser);
			// 是否 达到 补贴金额上限 -- 达到，则取消补贴订单的逻辑，应在 用户下单支付处
			if (distributionUser.hasReachMaximumSubsidyAmount()) {
				// 预估收益 已到最高 补贴金额
				continue;
			}
			// 生成补贴订单
			List<OrderResp> subsidyOrderList = distributionUser.createSubsidyOrderList();
			if (CollectionUtils.isEmpty(subsidyOrderList)) {
				continue;
			}
			OrderRespListDTO orderRespListDTO = new OrderRespListDTO();
			orderRespListDTO.setOrderRespList(subsidyOrderList);
			boolean save = orderAppService.batchSaveOrderRespList(orderRespListDTO);
			if (!save) {
				// 未补贴成功 或 已补贴
				continue;
			}
			// 生成 补贴待结算收益
			List<UpayWxOrderReq> upayWxOrderReqs = distributionUser.createSubsidyUpayWxOrderReq(subsidyOrderList, appProperty.getMfAppId());
			if (CollectionUtils.isEmpty(upayWxOrderReqs)) {
				log.error("生成补贴待结算收益 异常，须排查");
				continue;
			}
			save = upayWxOrderPendService.saveUpayWxOrderPend(upayWxOrderReqs);
			if (!save) {
				log.error("存储补贴待结算收益 异常, upayWxOrderReqs = {}", upayWxOrderReqs);
				continue;
			}
			// 持久化信息
			distributionUserRepository.savePlatformSubsidy(distributionUser);
			userTeamIncomeService.addIncomeRankListOfShopByUserIds(userIds, distributionUser.getUserInfo().getVipShopId());

			Map<String, Object> shopInfo = shopAppService.queryShopInfoByLogin(distributionUser.getUserInfo().getVipShopId().toString());
			if (CollectionUtils.isEmpty(shopInfo)) {
				return ;
			}
			try {
				Map shop = (Map)shopInfo.get("shopInfo");
				String shopName = shop.get("shopName").toString();
				// 发送模板消息通知 -- 用户返利分佣
				for (UpayWxOrderReq upayWxOrderReq : upayWxOrderReqs) {
					Long sendUserId = upayWxOrderReq.getUserId();

					if (sendUserId.equals(userId)) {
						Map<String, Object> data = TemplateMessageData.orderCashBackByMyself(upayWxOrderReq.getShopId(), sendUserId, upayWxOrderReq.getTotalFee(), shopName);
						templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), distributionUser, data);
					} else {
						log.info("<<<<<<<<<< 粉丝下单 消息模板 开始发送 推送给用户 = {}<<<<<<<<<<<<<<", sendUserId);
						Map<String, Object> sceneAndDialogStatus = getSceneAndDialogStatus(sendUserId);
						Map<String, Object> data = TemplateMessageData.orderCashBackByMaster(upayWxOrderReq.getShopId(),
								sendUserId, upayWxOrderReq.getTotalFee(), (int)sceneAndDialogStatus.get("scene"), sceneAndDialogStatus.get("isDialog").toString());
						log.info("<<<<<<<<<< 粉丝下单 消息模板 开始发送 推送给内容 = {}<<<<<<<<<<<<<<", data);
						templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), upayWxOrderReq.getShopId(), sendUserId, data);
					}

				}
			} catch (Exception e) {
				log.error("发送消息模板异常，e = {}", e);
			}
		}
	}

	public Map<String, Object> getSceneAndDialogStatus(Long userId) {
		DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
		distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());

		int scene = 28;
		String isDialog = "0";
		if (distributionUser.hasShopManagerRole()) {
			scene = 38;
			if (distributionUser.hasReachSpendStandard()) {
				isDialog = "0";
			}
		}
		Map<String, Object> resultVO = new HashMap<>();
		resultVO.put("scene", scene);
		resultVO.put("isDialog", isDialog);
		return resultVO;
	}
}
