package mf.code.distribution.common.rocketmq.consumer.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.exception.RocketMQException;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.ShopAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.common.property.WxmpProperty;
import mf.code.distribution.common.property.WxpayProperty;
import mf.code.distribution.constant.PlatformOrderBizTypeEnum;
import mf.code.distribution.constant.PlatformOrderStatusEnum;
import mf.code.distribution.constant.PlatformOrderTypeEnum;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.domain.aggregateroot.DistributionOrder;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.OrderCommissiones;
import mf.code.distribution.domain.valueobject.OrderDistributionProperty;
import mf.code.distribution.domain.valueobject.UserShopTeamCommission;
import mf.code.distribution.repo.po.PlatformOrder;
import mf.code.distribution.repo.po.UpayWxOrderPend;
import mf.code.distribution.repo.repository.DistributionOrderRepository;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.UpayWxOrderPendService;
import mf.code.distribution.service.UserTeamIncomeService;
import mf.code.distribution.service.templatemessage.TemplateMessageService;
import mf.code.distribution.service.templatemessage.message.TemplateMessageData;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.OrderResp;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.distribution.common.rocketmq.consumer.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-20 09:16
 */
@Slf4j
@Service
public class DistributionTopicConsumerImpl {
	@Autowired
	private UpayWxOrderPendService upayWxOrderPendService;
	@Autowired
	private UserTeamIncomeService userTeamIncomeService;
	@Autowired
	private DistributionOrderRepository distributionOrderRepository;
	@Autowired
	private DistributionUserRepository distributionUserRepository;
	@Autowired
	private ShopAppService shopAppService;
	@Autowired
	private WxpayProperty wxpyaProperty;
	@Autowired
	private WxmpProperty wxmpProperty;
	@Autowired
	private TemplateMessageService templateMessageService;
	@Autowired
	private OrderAppService orderAppService;

	/**
	 * 订单取消，对商品返利及分佣的记录 设置失效
	 */
	@Transactional
	public void cancelOrder(String msgId, String bizValue) {
		JSONObject jsonObject = JSON.parseObject(bizValue);
		String orderNo = jsonObject.getString("orderNo");
		String shopId = jsonObject.getString("shopId");
		if (StringUtils.isBlank(orderNo) || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
			log.error("参数异常");
			return ;
		}
		QueryWrapper<UpayWxOrderPend> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(UpayWxOrderPend::getOrderNo, orderNo);
		List<UpayWxOrderPend> upayWxOrderPends = upayWxOrderPendService.list(wrapper);
		if (CollectionUtils.isEmpty(upayWxOrderPends)) {
			log.error("没有分佣数据, 创建订单数据没有生成，取消订单消息先与支付订单消费到达，则让取消订单消息delay");
			throw new RocketMQException();
		}

		List<Long> userIds = new ArrayList<>();
		List<UpayWxOrderPend> updateList = new ArrayList<>();
		for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
			Long userId = upayWxOrderPend.getUserId();
			upayWxOrderPend.setStatus(UpayWxOrderPendStatusEnum.FAILED.getCode());
			updateList.add(upayWxOrderPend);
			if (userIds.contains(userId)) {
				continue;
			}
			userIds.add(userId);
		}


		boolean save = upayWxOrderPendService.updateBatchById(updateList);
		if (!save) {
			log.error("数据库异常，为排除服务异常，延迟消费, orderNo = {}", orderNo);
			throw new RocketMQException();
		}

		// 更新 用户团队 店铺收益
		userTeamIncomeService.addIncomeRankListOfShopByUserIds(userIds, Long.valueOf(shopId));
	}

	/**
	 * 商品售出，分配佣金 -- 商家及用户
	 * @return
	 */
	@Transactional
	public void createOrder(String msgId, String bizValue) {
		log.info("<<<<<<<<<<<<< 商品售出，分配佣金 -- 商家及用户 <<<<<<<<<<<<<");
		JSONObject jsonObject = JSON.parseObject(bizValue);
		String userId = jsonObject.getString("userId");
		String orderId = jsonObject.getString("orderId");
		String shopId = jsonObject.getString("shopId");
		if ( StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
				|| StringUtils.isBlank(orderId) || !RegexUtils.StringIsNumber(orderId)
				|| StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
			log.error("参数异常");
			return ;
		}
		Long oid = Long.valueOf(orderId);
		Long sid = Long.valueOf(shopId);
		Long uid = Long.valueOf(userId);
		// 获取订单详情
		DistributionOrder distributionOrder = distributionOrderRepository.findByOrderId(uid, sid, oid);
		if (distributionOrder == null) {
			log.error("查无此单, 为排除服务异常，延迟消费，orderId = {}", orderId);
			throw new RocketMQException();
		}
		// 分用户购买的平台进行 特殊处理 分销业务
		if (distributionOrder.getOrderInfo().getSource().equals(OrderSourceEnum.DOUYIN_SHOP_MANAGER.getCode())
				|| distributionOrder.getOrderInfo().getSource().equals(OrderSourceEnum.DOUYIN_SHOP.getCode())) {
			this.createOrderByDouYin(distributionOrder, oid, sid, uid);
			return;
		}
		this.createOrderByJKMF(distributionOrder, oid, sid, uid);


	}

	/**
	 * 抖音小程序 的分销商品购买
	 * @param distributionOrder
	 * @param orderId
	 * @param shopId
	 * @param userId
	 */
	private void createOrderByDouYin(DistributionOrder distributionOrder, Long orderId, Long shopId, Long userId) {
		OrderResp order = distributionOrder.getOrderInfo();
		// 指向供货商
		Long goodsShopId = order.getGoodsShopId();
		Long parentMerchantId = shopAppService.getMerchantIdByShopId(goodsShopId);
		// 指向粉丝店铺
		Long vipShopId = distributionOrder.getUserInfo().getVipShopId();
		Long vipShopMerchantId = shopAppService.getMerchantIdByShopId(vipShopId);

		OrderDistributionProperty orderDistributionProperty = distributionOrder.queryOrderDistributionProperty();
		OrderCommissiones orderCommissiones = distributionOrder.calculateOrderCommissionsForEachPart();
		List<UpayWxOrderReq> merchantUpayWxOrderReqs = distributionOrder.orderDistributionCommission(orderCommissiones, wxpyaProperty.getMfAppId(), parentMerchantId, vipShopMerchantId, true);
		if (CollectionUtils.isEmpty(merchantUpayWxOrderReqs)) {
			log.error("此订单无分销属性, 但肯定有商户结算, 为排除服务异常，延迟消费，orderId = {}", orderId);
			throw new RocketMQException();
		}
		if (orderCommissiones == null) {
			log.error("获取订单各部分分销佣金异常, 为排除服务异常，延迟消费, orderId = {}", orderId);
			throw new RocketMQException();
		}
		// 获取平台订单明细
		List<UpayWxOrderReq> platformOrderPends = platformOrderPend(orderCommissiones, order, orderDistributionProperty);
		if (!CollectionUtils.isEmpty(platformOrderPends)) {
			merchantUpayWxOrderReqs.addAll(platformOrderPends);
		}
		boolean save = upayWxOrderPendService.saveUpayWxOrderPend(merchantUpayWxOrderReqs);
		if (!save) {
			log.error("数据库异常，为排除服务异常，延迟消费, orderId = {}", orderId);
			throw new RocketMQException();
		}
	}

	/**
	 * 集客魔方小程序 的分销商品购买
	 * @param distributionOrder
	 * @param orderId
	 * @param shopId
	 * @param userId
	 */
	private void createOrderByJKMF(DistributionOrder distributionOrder, Long orderId, Long shopId, Long userId) {
		OrderResp order = distributionOrder.getOrderInfo();
		// 指向供货商
		Long goodsShopId = order.getGoodsShopId();
		Long parentMerchantId = shopAppService.getMerchantIdByShopId(goodsShopId);
		// 指向粉丝店铺
		Long vipShopId = distributionOrder.getUserInfo().getVipShopId();
		Long vipShopMerchantId = shopAppService.getMerchantIdByShopId(vipShopId);

		OrderDistributionProperty orderDistributionProperty = distributionOrder.queryOrderDistributionProperty();
		OrderCommissiones orderCommissiones = distributionOrder.calculateOrderCommissionsForEachPart();
		boolean purchaseInVipShop = distributionOrder.isPurchaseInVipShop();
		List<UpayWxOrderReq> merchantUpayWxOrderReqs = distributionOrder.orderDistributionCommission(orderCommissiones, wxpyaProperty.getMfAppId(), parentMerchantId, vipShopMerchantId, purchaseInVipShop);
		if (CollectionUtils.isEmpty(merchantUpayWxOrderReqs)) {
			log.error("此订单无分销属性, 但肯定有商户结算, 为排除服务异常，延迟消费，orderId = {}", orderId);
			throw new RocketMQException();
		}
		if (orderCommissiones == null) {
			log.error("获取订单各部分分销佣金异常, 为排除服务异常，延迟消费, orderId = {}", orderId);
			throw new RocketMQException();
		}
		// 获取平台订单明细
		List<UpayWxOrderReq> platformOrderPends = platformOrderPend(orderCommissiones, order, orderDistributionProperty);
		if (!CollectionUtils.isEmpty(platformOrderPends)) {
			merchantUpayWxOrderReqs.addAll(platformOrderPends);
		}

		if (orderCommissiones.getCustomerCommission() == null) {
			log.info("此订单无用户分佣属性, 正常消费， orderId = {}", orderId);
			boolean save = upayWxOrderPendService.saveUpayWxOrderPend(merchantUpayWxOrderReqs);
			if (!save) {
				log.error("数据库异常，为排除服务异常，延迟消费, orderId = {}", orderId);
				throw new RocketMQException();
			}
			return ;
		}
		BigDecimal customerCommission = orderCommissiones.getCustomerCommission();

		DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
		if (distributionUser == null) {
			log.error("无效用户, 为排除服务异常，延迟消费, userId = {}", userId);
			throw new RocketMQException();
		}
		distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());
		distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);

		List<UserShopTeamCommission> userShopTeamCommissionList = distributionUser.calculateCustomerCommissionsForEachMember(customerCommission, order.getFee());
		List<UpayWxOrderReq> upayWxOrderReqs = distributionUser.orderDistributionCommission(userShopTeamCommissionList, orderId, wxpyaProperty.getMfAppId());
		if (!CollectionUtils.isEmpty(upayWxOrderReqs)) {
			merchantUpayWxOrderReqs.addAll(upayWxOrderReqs);
		}
		// 获取平台订单明细
		List<UpayWxOrderReq> userPlatformOrderPends = platformOrderPend(userShopTeamCommissionList, order, distributionUser.queryUpperMembers(), orderDistributionProperty);
		if (!CollectionUtils.isEmpty(userPlatformOrderPends)) {
			merchantUpayWxOrderReqs.addAll(userPlatformOrderPends);
		}

		boolean save = upayWxOrderPendService.saveUpayWxOrderPend(merchantUpayWxOrderReqs);
		if (!save) {
			log.error("数据库异常，为排除服务异常，延迟消费, orderId = {}", orderId);
			throw new RocketMQException();
		}

		// 更新 用户团队 店铺收益
		List<Long> upperMembers = distributionUser.queryUpperMembers();
		upperMembers.add(distributionOrder.getUserId());
		userTeamIncomeService.addIncomeRankListOfShopByUserIds(upperMembers, shopId);

		Map<String, Object> shopInfo = shopAppService.queryShopInfoByLogin(shopId.toString());
		if (CollectionUtils.isEmpty(shopInfo)) {
			return ;
		}

		try {
			Map shop = (Map)shopInfo.get("shopInfo");
			String shopName = shop.get("shopName").toString();
			// 发送模板消息通知 -- 用户返利分佣
			for (UpayWxOrderReq upayWxOrderReq : upayWxOrderReqs) {
				Long sendUserId = upayWxOrderReq.getUserId();

				if (sendUserId.equals(userId)) {
					Map<String, Object> data = TemplateMessageData.orderCashBackByMyself(upayWxOrderReq.getShopId(), sendUserId, upayWxOrderReq.getTotalFee(), shopName);
					templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), distributionUser, data);
				} else {
					log.info("<<<<<<<<<< 粉丝下单 消息模板 开始发送 推送给用户 = {}<<<<<<<<<<<<<<", sendUserId);
					Map<String, Object> sceneAndDialogStatus = getSceneAndDialogStatus(sendUserId);
					Map<String, Object> data = TemplateMessageData.orderCashBackByMaster(upayWxOrderReq.getShopId(),
							sendUserId, upayWxOrderReq.getTotalFee(), (int)sceneAndDialogStatus.get("scene"), sceneAndDialogStatus.get("isDialog").toString());
					log.info("<<<<<<<<<< 粉丝下单 消息模板 开始发送 推送给内容 = {}<<<<<<<<<<<<<<", data);
					templateMessageService.sendTemplateMessage(wxmpProperty.getOrderCashBackMsgTmpId(), upayWxOrderReq.getShopId(), sendUserId, data);
				}

			}
		} catch (Exception e) {
			log.error("发送消息模板异常，e = {}", e);
		}

		// 2.2.1迭代 平台补贴用户
		// 是否 达到 补贴金额上限 -- 达到，则取消补贴订单的逻辑，应在 用户下单支付处
		this.cancelSubsidyOrder(upperMembers);
	}

	/**
	 * 是否 达到 补贴金额上限 -- 达到，则取消补贴订单的逻辑，应在 用户下单支付处
	 * @param upperMembers
	 */
	public void cancelSubsidyOrder(List<Long> upperMembers) {
		if (CollectionUtils.isEmpty(upperMembers)) {
			return;
		}
		for (Long upperMember : upperMembers) {
			DistributionUser distributionUser = distributionUserRepository.findByUserId(upperMember);
			boolean success = distributionUserRepository.addUserSubsidyPolicy(distributionUser, null);
			if (!success) {
				continue;
			}
			distributionUserRepository.addUserShopTeamForLowerMember(distributionUser);
			if (!distributionUser.hasReachTeamSubsidyStandard()) {
				// 不达标 -- 不会产生补贴订单
				continue;
			}
			distributionUserRepository.addUserDistributionHistory(distributionUser);
			// 获取将取消的补贴订单
			List<UpayWxOrderPend> upayWxOrderPends = distributionUser.queryCancelSubsidyPendList();
			if (CollectionUtils.isEmpty(upayWxOrderPends)) {
				log.info("没有平台待取消的补贴订单");
				continue;
			}
			List<String> orderNoList = new ArrayList<>();
			for (UpayWxOrderPend upayWxOrderPend : upayWxOrderPends) {
				orderNoList.add(upayWxOrderPend.getOrderNo());
			}
			boolean cancel = orderAppService.cancelOrderByOrderNoList(orderNoList);
			if (!cancel) {
				log.error("平台补贴订单取消失败，orderNoList = {}", orderNoList);
				continue;
			}
			boolean update = upayWxOrderPendService.updateBatchById(upayWxOrderPends);
			if (!update) {
				log.error("平台补贴待结算订单取消失败， upayWxOrderPends=  {}", upayWxOrderPends);
			}
		}
	}

	public Map<String, Object> getSceneAndDialogStatus(Long userId) {
		DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
		distributionUserRepository.addUserOrderHistory(distributionUser, DateUtil.getBeginOfThisMonth(), new Date());

		int scene = 28;
		String isDialog = "0";
		if (distributionUser.hasShopManagerRole()) {
			scene = 38;
			if (distributionUser.hasReachSpendStandard()) {
				isDialog = "0";
			}
		}
		Map<String, Object> resultVO = new HashMap<>();
		resultVO.put("scene", scene);
		resultVO.put("isDialog", isDialog);
		return resultVO;
	}

	/**
	 * 获取 用户相关的 平台订单流水 集合
	 * @param userShopTeamCommissionList
	 * @return
	 */
	private List<UpayWxOrderReq> platformOrderPend(List<UserShopTeamCommission> userShopTeamCommissionList, OrderResp order, List<Long> reachUpperIds, OrderDistributionProperty orderDistributionProperty) {
		List<UpayWxOrderReq> platformOrderList = new ArrayList<>();
		int size = reachUpperIds.size();
		BigDecimal perPubCommission = BigDecimal.ZERO;
		BigDecimal teamRewardAmount = BigDecimal.ZERO;
		BigDecimal taxAmount = BigDecimal.ZERO;

		for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
			perPubCommission = perPubCommission.add(userShopTeamCommission.getPerPubCommission());
			teamRewardAmount = teamRewardAmount.add(userShopTeamCommission.getTeamRewardAmount());
			taxAmount = taxAmount.add(userShopTeamCommission.getTaxAmount());
		}

		// 订单成交-上级缺失 用户分佣回收
		for (int i = 0; i < 5 - size; i++) {
			UpayWxOrderReq platformOrderPerPub = newUpayWxOrder(order);
			platformOrderPerPub.setTotalFee(perPubCommission);
			platformOrderPerPub.setBizType(UpayWxOrderPendBizTypeEnum.USER_COMMISSION_RECYCLING.getCode());
			platformOrderList.add(platformOrderPerPub);
		}

		// 订单成交-用户返利分佣团队奖励部分回收
		UpayWxOrderReq platformOrderTeamReward = newUpayWxOrder(order);
		platformOrderTeamReward.setTotalFee(teamRewardAmount);
		platformOrderTeamReward.setBizType(UpayWxOrderPendBizTypeEnum.USER_TEAM_REWARD.getCode());
		platformOrderList.add(platformOrderTeamReward);

		// 订单成交-用户返利分佣团队奖励部分回收
		UpayWxOrderReq platformOrderTax = newUpayWxOrder(order);
		platformOrderTax.setTotalFee(taxAmount);
		platformOrderTax.setBizType(UpayWxOrderPendBizTypeEnum.USER_COMMISSION_TAX.getCode());
		platformOrderList.add(platformOrderTax);

		return resetProductProvider(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 获取 商户相关的 平台订单流水 集合
	 * @param orderCommissiones
	 * @return
	 */
	private List<UpayWxOrderReq> platformOrderPend(OrderCommissiones orderCommissiones, OrderResp order, OrderDistributionProperty orderDistributionProperty) {
		List<UpayWxOrderReq> platformOrderList = new ArrayList<>();

		BigDecimal transactionCommission = orderCommissiones.getTransactionCommission();
		if (transactionCommission != null) {
			UpayWxOrderReq platformOrderTransaction = newUpayWxOrder(order);
			platformOrderTransaction.setTotalFee(transactionCommission);
			platformOrderTransaction.setBizType(UpayWxOrderPendBizTypeEnum.TRANSACTION.getCode());
			platformOrderList.add(platformOrderTransaction);
		}

		BigDecimal specsCommission = orderCommissiones.getPlatformCommission();
		if (specsCommission != null) {
			UpayWxOrderReq platformOrderSpecs = newUpayWxOrder(order);
			platformOrderSpecs.setTotalFee(specsCommission);
			platformOrderSpecs.setBizType(UpayWxOrderPendBizTypeEnum.SPECS_COMMISSION.getCode());
			platformOrderList.add(platformOrderSpecs);
		}

		return resetProductProvider(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 分销方店铺id和商户id 重新设置成 供货商店铺id和商户id
	 * @param platformOrderList
	 * @param orderDistributionProperty
	 * @param order
	 * @return
	 */
	private List<UpayWxOrderReq> resetProductProvider(List<UpayWxOrderReq> platformOrderList, OrderDistributionProperty orderDistributionProperty, OrderResp order) {
		ProductDistributionTypeEnum productDistributionTypeEnum = ProductDistributionTypeEnum.findByParams(orderDistributionProperty.getProductId(),
				orderDistributionProperty.getParentId(),
				orderDistributionProperty.getSelfSupportRatio(),
				orderDistributionProperty.getPlatSupportRatio());
		if (productDistributionTypeEnum != ProductDistributionTypeEnum.RESELL) {
			return platformOrderList;
		}
		List<UpayWxOrderReq> resetPlatformOrderList = new ArrayList<>();
		// 指向供货商
		Long goodsShopId = order.getGoodsShopId();
		Long parentMerchantId = shopAppService.getMerchantIdByShopId(goodsShopId);

		for (UpayWxOrderReq platformOrder : platformOrderList) {
			platformOrder.setShopId(goodsShopId);
			platformOrder.setMchId(parentMerchantId);
			resetPlatformOrderList.add(platformOrder);
		}
		return resetPlatformOrderList;
	}

	/**
	 * 生成 用户订单流水
	 *
	 * @param order
	 * @return 重写 biz_type totalFee
	 */
	private UpayWxOrderReq newUpayWxOrder(OrderResp order) {
		Date now = new Date();

		UpayWxOrderReq upayWxOrder = new UpayWxOrderReq();
		upayWxOrder.setMchId(order.getMerchantId());
		upayWxOrder.setShopId(order.getShopId());
		upayWxOrder.setUserId(order.getUserId());
		upayWxOrder.setType(UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
		upayWxOrder.setPayType(UpayWxOrderPayTypeEnum.CASH.getCode());
		upayWxOrder.setOrderNo(order.getOrderNo());
		upayWxOrder.setOrderName(order.getOrderName());
		upayWxOrder.setAppletAppId(wxpyaProperty.getMfAppId());
		upayWxOrder.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
		upayWxOrder.setTotalFee(order.getFee());
		upayWxOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
		upayWxOrder.setIpAddress(order.getIpAddress());
		upayWxOrder.setTradeId(order.getTradeId());
		upayWxOrder.setRemark(order.getRemark());
		upayWxOrder.setNotifyTime(now);
		upayWxOrder.setPaymentTime(now);
		upayWxOrder.setCtime(now);
		upayWxOrder.setUtime(now);
		upayWxOrder.setReqJson(order.getReqJson());
		upayWxOrder.setBizType(UpayWxOrderBizTypeEnum.REBATE.getCode());
		upayWxOrder.setBizValue(order.getId());
		upayWxOrder.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
		upayWxOrder.setRefundOrderId(0L);
		return upayWxOrder;
	}
}
