package mf.code.distribution.common.job;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.api.feignclient.OrderAppService;
import mf.code.distribution.api.feignclient.ShopAppService;
import mf.code.distribution.api.feignclient.UserAppService;
import mf.code.distribution.common.property.WxpayProperty;
import mf.code.distribution.common.redis.RedisForbidRepeat;
import mf.code.distribution.common.redis.RedisKeyConstant;
import mf.code.distribution.constant.PlatformOrderBizTypeEnum;
import mf.code.distribution.constant.PlatformOrderStatusEnum;
import mf.code.distribution.constant.PlatformOrderTypeEnum;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.distribution.domain.Factory.DistributionUserFactory;
import mf.code.distribution.domain.aggregateroot.DistributionUser;
import mf.code.distribution.domain.valueobject.OrderCommissiones;
import mf.code.distribution.domain.valueobject.OrderDistributionProperty;
import mf.code.distribution.domain.valueobject.ProductDistributionPolicy;
import mf.code.distribution.domain.valueobject.UserShopTeamCommission;
import mf.code.distribution.repo.po.OrderSettled;
import mf.code.distribution.repo.po.PlatformOrder;
import mf.code.distribution.repo.repository.DistributionUserRepository;
import mf.code.distribution.service.OrderSettledService;
import mf.code.distribution.service.PlatformOrderService;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.order.dto.OrderResp;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqList;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.user.common.job
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 10:34
 */

@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class DistributionScheduledService {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private UserAppService userAppService;
	@Autowired
	private WxpayProperty appProperty;
	@Autowired
	private OrderSettledService orderSettledService;
	@Autowired
	private DistributionUserRepository distributionUserRepository;
	@Autowired
	private OrderAppService orderAppService;
	@Autowired
	private PlatformOrderService platformOrderService;
	@Autowired
	private ShopAppService shopAppService;

	@Value("${taskExecutor.jobMode}")
	private Integer jobMode = 1;

	// @Scheduled(fixedDelay = 60000)
	@Scheduled(cron = "0 0 0 20 * ? ")
	public void userOrderSettlement() {
		if (jobMode == 0) {
			return;
		}
		if (DateUtil.getDay(new Date()) != 20) {
			log.error("今天不是20日");
			return;
		}
		// 结算防重
		boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.USER_ORDER_SETTLEMENT, 1, TimeUnit.DAYS);
		if (!success) {
			log.error("分销结算，redis防重，已有定时任务开始对上月订单进行结算");
			return;
		}
		// 定义结算订单的时间段 -- 上月开始-上月结束
		Date beginTimeDate = DateUtil.getBeginOfLastMonth();
		Date endTimeDate = DateUtil.getEndOfLastMonth();

		String beginTime = DateUtil.dateToString(beginTimeDate, DateUtil.FORMAT_ONE);
		String endTime = DateUtil.dateToString(endTimeDate, DateUtil.FORMAT_ONE);
		// 获取上月所有用户所有店铺 所有可结算订单
		List<OrderResp> orderRespList = orderAppService.querySettlementOrdersByTime(beginTime, endTime);
		if (CollectionUtils.isEmpty(orderRespList)) {
			log.info("查无可结算订单");
			return;
		}

		// 定义key为 userId, 定义value为 List<OrderResp>
		Map<Long, List<OrderResp>> userShopOrderMap = new HashMap<>();
		for (OrderResp orderResp : orderRespList) {
			Long userId = orderResp.getUserId();
			List<OrderResp> userShopOrderList = userShopOrderMap.get(userId);
			if (CollectionUtils.isEmpty(userShopOrderList)) {
				userShopOrderList = new ArrayList<>();
			}
			userShopOrderList.add(orderResp);
			userShopOrderMap.put(userId, userShopOrderList);
		}

		// 开始对所有用户 所有可结算订单进行结算 -- 已用户为第一维度，是因为，用户的消费金额是按全平台所有店铺统计的
		for (Map.Entry<Long, List<OrderResp>> userShopEntry : userShopOrderMap.entrySet()) {
			// 获取用户Id
			Long userId = userShopEntry.getKey();
			// 获取此用户的 所有店铺的可结算订单
			List<OrderResp> orderResps = userShopEntry.getValue();
			// 可结算订单的 用户消费金额
			BigDecimal spendAmount = BigDecimal.ZERO;

			// 按店铺结算 Map<ShopId, List<OrderResp>>
			Map<Long, List<OrderResp>> shopOrderMap = new HashMap<>();
			for (OrderResp orderResp : orderResps) {
				Long shopId = orderResp.getShopId();
				List<OrderResp> shopOrderList = shopOrderMap.get(shopId);
				if (CollectionUtils.isEmpty(shopOrderList)) {
					shopOrderList = new ArrayList<>();
				}
				shopOrderList.add(orderResp);
				shopOrderMap.put(shopId, shopOrderList);

				spendAmount = spendAmount.add(orderResp.getFee());
			}

			// 获取以平台维度统计的用户数据 -- 获取分销用户
			DistributionUser distributionUser = distributionUserRepository.findByUserId(userId);
			distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);
			// 获取 分销用户的上级成员
			List<Long> upperMembers = distributionUser.queryUpperMembers();
			// 获取上月消费达标的上级成员
			List<Long> reachUpperIds = new ArrayList<>();
			for (Long upperMemberId : upperMembers) {
				// 获取 上级成员的分销情况
				DistributionUser upperMember = distributionUserRepository.findByUserId(upperMemberId);
				distributionUserRepository.addUserOrderHistory(upperMember, beginTimeDate, endTimeDate);
				// 判断上级成员 上月消费是否达标
				if (upperMember.hasReachSpendStandard() && upperMember.hasShopManagerRole()) {
					reachUpperIds.add(upperMemberId);
				}
			}


			// 初始化 结算状态
			DistributionUser settleUser = DistributionUserFactory.newDistributionUser(distributionUser);

			// 开始对此用户下 所有店铺 所有可结算订单进行结算 -- 先user后shop 因为vipShop分佣
			for (Map.Entry<Long, List<OrderResp>> shopEntry : shopOrderMap.entrySet()) {
				Long shopId = shopEntry.getKey();
				List<OrderResp> distributionOrders = shopEntry.getValue();

				// 对此用户 此店铺下的可结算订单进行结算
				for (OrderResp order : distributionOrders) {
					Long orderId = order.getId();
					// 获取此订单当时的分销属性
					OrderDistributionProperty orderDistributionProperty = settleUser.queryOrderDistributionProperty(order);
					if (orderDistributionProperty == null) {
						log.error("此订单未获取到分销属性, orderId = {}", orderId);
						continue;
					}
					// 对结算订单进行转储
					OrderSettled orderSettled = new OrderSettled();
					BeanUtils.copyProperties(order, orderSettled);
					orderSettled.setOrderId(orderId);
					try {
						orderSettledService.save(orderSettled);
					} catch (Exception e) {
						log.info("此单已结算, orderId = {}", orderId);
					}
					// 此订单的 平台类目抽佣比率
					Integer goodsCategoryRate = orderDistributionProperty.getGoodsCategoryRate();
					// 此订单的 平台交易税率
					Integer transactionRate = orderDistributionProperty.getTransactionRate();
					// 此订单的 粉丝店铺分佣
					Integer vipShopRate = orderDistributionProperty.getVipShopRate();
					// 获取商品的分销政策
					ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(goodsCategoryRate, transactionRate, vipShopRate);
					// 计算各部分的分销佣金
					OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(orderDistributionProperty, order.getFee());

					// 获取 平台所得流水
					List<PlatformOrder> platformOrderList = settlePlatformOrder(orderCommissiones, order, orderDistributionProperty);
					// 存储 平台所得流水
					if (!CollectionUtils.isEmpty(platformOrderList)) {
						try {
							platformOrderService.saveBatch(platformOrderList);
						} catch (Exception e) {
							log.info("此单平台已结算, orderId = {}", orderId);
						}
					}

					// 获取用户分佣这部分的佣金
					BigDecimal customerCommission = orderCommissiones.getCustomerCommission();
					if (customerCommission == null) {
						log.info("此订单无分销属性, orderId = {}", orderId);
						continue;
					}
					// 添加订单
					settleUser.addOrder(order);
					// 计算用户返利分佣，各部分所得
					List<UserShopTeamCommission> userShopTeamCommissionList = settleUser.calculateCustomerCommissionsForEachMember(customerCommission, order.getFee());
					if (CollectionUtils.isEmpty(userShopTeamCommissionList)) {
						log.info("非分销订单, orderId = {}", order.getId());
						continue;
					}
					List<PlatformOrder> userPlatformOrderList = settlePlatformOrder(userShopTeamCommissionList, order, reachUpperIds, orderDistributionProperty);
					// 存储 平台所得流水
					if (!CollectionUtils.isEmpty(userPlatformOrderList)) {
						try {
							platformOrderService.saveBatch(userPlatformOrderList);
						} catch (Exception e) {
							log.info("此单平台已结算, orderId = {}", orderId);
						}
					}

					// 获取自己返利和上级分佣记录
					List<UpayWxOrderReq> upayWxOrderReqs = settleUser.settleDistributionCommission(userShopTeamCommissionList, order, reachUpperIds, appProperty.getMfAppId());
					if (CollectionUtils.isEmpty(upayWxOrderReqs)) {
						continue;
					}
					// 插入流水记录 并 更新余额

					UpayWxOrderReqList upayWxOrderReqList = new UpayWxOrderReqList();
					upayWxOrderReqList.setUpayWxOrderReqList(upayWxOrderReqs);
					userAppService.batchUpdateUserBalance(upayWxOrderReqList);
				}
				// 更新用户余额的结算时间
				userAppService.updateUserBalanceSettledTimeByUserIdShopId(userId, shopId);
			}

			// 2.2.1迭代 结算平台补贴订单
			// 用户消费 是否达标
			BigDecimal commissionStandard = distributionUser.getUserRebatePolicy().getCommissionStandard();
			if (spendAmount.compareTo(commissionStandard) >= 0) {
				platformSubsidyUserOrderSettlement(distributionUser, beginTimeDate, endTimeDate);
			}
		}
		// 本月结算结束，更新所有用户的结算日期
		userAppService.updateUpayBalanceSettledTimeByAll();
	}

	/**
	 * 获取 用户相关的 平台订单流水 集合
	 *
	 * @param userShopTeamCommissionList
	 * @return
	 */
	private List<PlatformOrder> settlePlatformOrder(List<UserShopTeamCommission> userShopTeamCommissionList, OrderResp order, List<Long> reachUpperIds, OrderDistributionProperty orderDistributionProperty) {
		List<PlatformOrder> platformOrderList = new ArrayList<>();
		int size = reachUpperIds.size();
		BigDecimal perPubCommission = BigDecimal.ZERO;
		BigDecimal teamRewardAmount = BigDecimal.ZERO;
		BigDecimal taxAmount = BigDecimal.ZERO;

		for (UserShopTeamCommission userShopTeamCommission : userShopTeamCommissionList) {
			perPubCommission = perPubCommission.add(userShopTeamCommission.getPerPubCommission());
			teamRewardAmount = teamRewardAmount.add(userShopTeamCommission.getTeamRewardAmount());
			taxAmount = taxAmount.add(userShopTeamCommission.getTaxAmount());
		}

		// 订单成交-上级缺失 用户分佣回收
		for (int i = 0; i < 5 - size; i++) {
			PlatformOrder platformOrderPerPub = newPlatformOrder(perPubCommission, order);
			if (platformOrderPerPub != null) {
				// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
				platformOrderPerPub.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getCode() + i);
				platformOrderPerPub.setOrderName(PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getDesc());
				platformOrderPerPub.setBizType(PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getCode());
				platformOrderPerPub.setBizValue(order.getId());
				platformOrderList.add(platformOrderPerPub);
			}
		}

		// 订单成交-用户返利分佣团队奖励部分回收
		PlatformOrder platformOrderTeamReward = newPlatformOrder(teamRewardAmount, order);
		if (platformOrderTeamReward != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderTeamReward.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getCode());
			platformOrderTeamReward.setOrderName(PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getDesc());
			platformOrderTeamReward.setBizType(PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getCode());
			platformOrderTeamReward.setBizValue(order.getId());
			platformOrderList.add(platformOrderTeamReward);
		}

		// 订单成交-用户返利分佣团队奖励部分回收
		PlatformOrder platformOrderTax = newPlatformOrder(taxAmount, order);
		if (platformOrderTax != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderTax.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getCode());
			platformOrderTax.setOrderName(PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getDesc());
			platformOrderTax.setBizType(PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getCode());
			platformOrderTax.setBizValue(order.getId());
			platformOrderList.add(platformOrderTax);
		}
		return resetProductProvider(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 获取 商户相关的 平台订单流水 集合
	 *
	 * @param orderCommissiones
	 * @return
	 */
	private List<PlatformOrder> settlePlatformOrder(OrderCommissiones orderCommissiones, OrderResp order, OrderDistributionProperty orderDistributionProperty) {
		List<PlatformOrder> platformOrderList = new ArrayList<>();

		BigDecimal transactionCommission = orderCommissiones.getTransactionCommission();
		PlatformOrder platformOrderTransaction = newPlatformOrder(transactionCommission, order);
		if (platformOrderTransaction != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderTransaction.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.TRANSACTION.getCode());
			platformOrderTransaction.setOrderName(PlatformOrderBizTypeEnum.TRANSACTION.getDesc());
			platformOrderTransaction.setBizType(PlatformOrderBizTypeEnum.TRANSACTION.getCode());
			platformOrderTransaction.setBizValue(order.getId());
			platformOrderList.add(platformOrderTransaction);
		}
		BigDecimal specsCommission = orderCommissiones.getPlatformCommission();
		PlatformOrder platformOrderSpecs = newPlatformOrder(specsCommission, order);
		if (platformOrderSpecs != null) {
			// 交易税得orderNo组成 支付订单的orderNo（唯一） + 业务类型（区别） -- 防重）
			platformOrderSpecs.setOrderNo(order.getOrderNo() + PlatformOrderBizTypeEnum.SPECS_COMMISSION.getCode());
			platformOrderSpecs.setOrderName(PlatformOrderBizTypeEnum.SPECS_COMMISSION.getDesc());
			platformOrderSpecs.setBizType(PlatformOrderBizTypeEnum.SPECS_COMMISSION.getCode());
			platformOrderSpecs.setBizValue(order.getId());
			platformOrderList.add(platformOrderSpecs);
		}
		return resetProductProvider(platformOrderList, orderDistributionProperty, order);
	}

	/**
	 * 分销方店铺id和商户id 重新设置成 供货商店铺id和商户id
	 * @param platformOrderList
	 * @param orderDistributionProperty
	 * @param order
	 * @return
	 */
	private List<PlatformOrder> resetProductProvider(List<PlatformOrder> platformOrderList, OrderDistributionProperty orderDistributionProperty, OrderResp order) {
		ProductDistributionTypeEnum productDistributionTypeEnum = ProductDistributionTypeEnum.findByParams(orderDistributionProperty.getProductId(),
				orderDistributionProperty.getParentId(),
				orderDistributionProperty.getSelfSupportRatio(),
				orderDistributionProperty.getPlatSupportRatio());
		if (productDistributionTypeEnum != ProductDistributionTypeEnum.RESELL) {
			return platformOrderList;
		}
		List<PlatformOrder> resetPlatformOrderList = new ArrayList<>();
		// 指向供货商
		Long goodsShopId = order.getGoodsShopId();
		Long parentMerchantId = shopAppService.getMerchantIdByShopId(goodsShopId);

		for (PlatformOrder platformOrder : platformOrderList) {
			platformOrder.setShopId(goodsShopId);
			platformOrder.setMerchantId(parentMerchantId);
			resetPlatformOrderList.add(platformOrder);
		}
		return resetPlatformOrderList;
	}

	/**
	 * 创建 平台订单流水信息
	 *
	 * @param totalFee
	 * @return 必须重写 orderNO  orderName bizType bizValue
	 */
	private PlatformOrder newPlatformOrder(BigDecimal totalFee, OrderResp order) {
		if (totalFee == null) {
			return null;
		}
		Date now = new Date();
		PlatformOrder platformOrder = new PlatformOrder();
		platformOrder.setMerchantId(order.getMerchantId());
		platformOrder.setShopId(order.getShopId());
		platformOrder.setUserId(order.getUserId());
		platformOrder.setType(PlatformOrderTypeEnum.PALTFORM_PRESTORE.getCode());
		platformOrder.setPayType(OrderPayTypeEnum.CASH.getCode());
		platformOrder.setPubAppId(appProperty.getMfAppId());
		platformOrder.setStatus(PlatformOrderStatusEnum.SUCCESS.getCode());
		platformOrder.setTotalFee(totalFee);
		platformOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
		platformOrder.setTradeId(order.getTradeId());
		platformOrder.setNotifyTime(now);
		platformOrder.setPaymentTime(now);
		platformOrder.setCtime(now);
		platformOrder.setUtime(now);
		platformOrder.setReqJson(order.getReqJson());
		return platformOrder;
	}

	/**
	 * 2.2.1迭代 结算平台补贴订单
	 */
	private void platformSubsidyUserOrderSettlement(DistributionUser distributionUser, Date beginTime, Date endTime) {
		Long userId = distributionUser.getUserId();
		// 获取 平台补贴 可结算订单
		List<OrderResp> orderRespList = orderAppService.querySettlementSubsidyOrdersByTime(userId, beginTime, endTime);
		if (CollectionUtils.isEmpty(orderRespList)) {
			log.info("此用户无平台补贴订单, userId = {}", userId);
			return;
		}
		// 批量转储
		List<OrderSettled> orderSettleds = new ArrayList<>();
		for (OrderResp order : orderRespList) {
			Long orderId = order.getId();
			// 对结算订单进行转储
			OrderSettled orderSettled = new OrderSettled();
			BeanUtils.copyProperties(order, orderSettled);
			orderSettled.setOrderId(orderId);
			orderSettleds.add(orderSettled);
		}

		// 对结算订单进行转储
		try {
			orderSettledService.saveBatch(orderSettleds);
		} catch (Exception e) {
			log.info("此单已结算, orderSettleds = {}", orderSettleds);
		}

		// 结算 平台补贴订单 生成补贴记录
		List<UpayWxOrderReq> upayWxOrderReqs = distributionUser.createSubsidyUpayWxOrderReq(orderRespList, appProperty.getMfAppId());
		// 插入流水记录 并 更新余额
		UpayWxOrderReqList upayWxOrderReqList = new UpayWxOrderReqList();
		upayWxOrderReqList.setUpayWxOrderReqList(upayWxOrderReqs);
		userAppService.batchUpdateUserBalance(upayWxOrderReqList);
		// 更新用户余额的结算时间
		userAppService.updateUserBalanceSettledTimeByUserIdShopId(userId, distributionUser.getUserInfo().getVipShopId());
	}


/*

    @Scheduled(cron = "0 0 0 20 * ? ")
//    @Scheduled(fixedDelay = 60000)
    public void userOrderSettlementScheduled() {
        if (jobMode == 0) {
            return;
        }
        if (DateUtil.getDay(new Date()) != 20) {
            log.error("今天不是20日");
            return;
        }
        //结算防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.USER_ORDER_SETTLEMENT, 1, TimeUnit.DAYS);
//        //暂时存在
//        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.USER_ORDER_SETTLEMENT, 30, TimeUnit.SECONDS);
        if (!success) {
            log.error("分销结算，redis防重，已有定时任务开始本次结算上月订单");
            return;
        }
        // 获取所有店铺
        List<Long> shopIds = shopAppService.selectAllIds();
        // 获取所有用户
        List<Long> userIds = userAppService.selectAllIds();
        // 定义结算订单的时间段 -- 上月开始-上月结束
        Date beginTime = DateUtil.getBeginOfLastMonth();
        Date endTime = DateUtil.getEndOfLastMonth();

        for (Long shopId : shopIds) {
            // 此次结算 用户id
            for (Long userId : userIds) {
                // 获取 分销用户
                DistributionUser distributionUser = distributionUserRepository.findByShopIdUserId(shopId, userId);
                if (distributionUser == null) {
                    log.error("获取用户信息失败，userId = {}", userId);
                    continue;
                }
                distributionUserRepository.addUserOrderHistory(distributionUser, beginTime, endTime);
                distributionUserRepository.addUserShopTeamForUpperMember(distributionUser);
                // 获取 分销用户的上级成员
                List<Long> upperMembers = distributionUser.queryUpperMembers();
                // 获取上月消费达标的上级成员
                List<Long> reachUpperIds = new ArrayList<>();
                for (Long upperMemberId : upperMembers) {
                    // 获取 上级成员的分销情况
                    DistributionUser upperMember = distributionUserRepository.findByShopIdUserId(shopId, upperMemberId);
                    distributionUserRepository.addUserOrderHistory(upperMember, beginTime, endTime);
                    // 判断上级成员 上月消费是否达标
                    if (upperMember.hasReachSpendStandard()) {
                        reachUpperIds.add(upperMemberId);
                    }
                }
                List<OrderResp> distributionOrders = distributionUser.querySettledOrders();
                // 初始化 结算状态
                DistributionUser settleUser = DistributionUserFactory.newDistributionUser(distributionUser);
                // 对订单进行结算
                for (OrderResp order : distributionOrders) {
                    Long orderId = order.getId();
                    // 获取此订单当时的分销属性
                    OrderDistributionProperty orderDistributionProperty = distributionUser.queryOrderDistributionProperty(order);
                    if (orderDistributionProperty == null) {
                        log.error("此订单未获取到分销属性, orderId = {}", orderId);
                        continue;
                    }
                    // 对结算订单进行转储
                    OrderSettled orderSettled = new OrderSettled();
                    BeanUtils.copyProperties(order, orderSettled);
                    orderSettled.setOrderId(orderId);
                    try {
                        orderSettledService.insertSelective(orderSettled);
                    } catch (Exception e) {
                        log.info("此单已结算, orderId = {}", orderId);
                    }
                    // 此订单的 平台类目抽佣比率
                    Integer goodsCategoryRate = orderDistributionProperty.getGoodsCategoryRate();
                    // 此订单的 平台交易税率
                    Integer transactionRate = orderDistributionProperty.getTransactionRate();
                    // 此订单的 粉丝店铺分佣
                    Integer vipShopRate = orderDistributionProperty.getVipShopRate();
                    // 获取商品的分销政策
                    ProductDistributionPolicy productDistributionPolicy = new ProductDistributionPolicy(goodsCategoryRate, transactionRate, vipShopRate);
                    // 计算各部分的分销佣金
                    OrderCommissiones orderCommissiones = productDistributionPolicy.calculateOrderCommissionsForEachPart(orderDistributionProperty, order.getFee());
                    // 获取用户分佣这部分的佣金
                    BigDecimal customerCommission = orderCommissiones.getCustomerCommission();
                    if (customerCommission == null) {
                        log.info("此订单无分销属性, orderId = {}", orderId);
                        continue;
                    }
                    // 获取自己返利和上级分佣记录
                    List<UpayWxOrderReq> upayWxOrderReqs = settleUser.settleDistributionCommission(customerCommission, order, reachUpperIds, appProperty.getMfAppId());
                    if (CollectionUtils.isEmpty(upayWxOrderReqs)) {
                        continue;
                    }
                    // 插入流水记录 并 更新余额
                    String upayWxOrders = JSON.toJSONString(upayWxOrderReqs);
                    userAppService.batchUpdateUserBalance(upayWxOrders);
                }
            }
            // 更新用户余额的结算时间
            userAppService.updateUserBalanceSettledTime(shopId);
        }
    }
*/

}
