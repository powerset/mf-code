package mf.code.junit;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.user.common.rocketmq.consumer.InviteTopicConsumer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.distribution.junit
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 12:45
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TemplateMsgTest {
    @Autowired
    private InviteTopicConsumer inviteTopicConsumer;

    @Test
    public void templateMsgLogin(){
        Map<String, String> inviteParams = new HashMap<>();
        inviteParams.put("pubUserId", "688");
        inviteParams.put("userId", "689");
        String topicAndTagsByEnum = RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM);
        inviteTopicConsumer.shoppingTeam(topicAndTagsByEnum, JSON.toJSONString(inviteParams));
    }
}
