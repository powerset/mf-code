package mf.code.junit;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.user.service.UserTeamIncomeService;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.distribution.junit
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 12:45
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class FunctionTest {
    // @Autowired
    // private RocketMQTemplate rocketMQTemplate;
    //
    //
    // @Test
    // public void buildShoppingTeamTest() {
    //     /**
    //      * 331 邀请 332
    //      * 332 邀请 333
    //      * 333 邀请 334
    //      * 334 邀请 335
    //      * 335 邀请 336
    //      * 336 邀请 337
    //      * 337 邀请 338
    //      * 338 邀请 439
    //      * 439 邀请 339，340
    //      * 340 邀请 341，346
    //      * 346 邀请 342，347
    //      * 347 邀请 343，348
    //      * 348 邀请 344，349
    //      * 349 邀请 345，350
    //      */
    //
    //     for (int i = 331; i < 338; i++) {
    //         String pubUserId = String.valueOf(i);
    //         String userId = String.valueOf(i + 1);
    //
    //         try {
    //             // 建立商城团队
    //             Map<String, String> inviteParams = new HashMap<>();
    //             inviteParams.put("pubUserId", pubUserId);
    //             inviteParams.put("userId", userId);
    //             rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM),
    //                     JSON.toJSONString(inviteParams));
    //             log.info("建立商城团队消息生产：" + JSON.toJSONString(inviteParams));
    //         } catch (Exception e) {
    //             log.error("建立商城团队消息生产异常, exception = {}", e);
    //         }
    //     }
    //
    //     try {
    //         // 建立商城团队
    //         Map<String, String> inviteParams = new HashMap<>();
    //         inviteParams.put("pubUserId", "338");
    //         inviteParams.put("userId", "439");
    //         rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM),
    //                 JSON.toJSONString(inviteParams));
    //         log.info("建立商城团队消息生产：" + JSON.toJSONString(inviteParams));
    //     } catch (Exception e) {
    //         log.error("建立商城团队消息生产异常, exception = {}", e);
    //     }
    //
    //     try {
    //         // 建立商城团队
    //         Map<String, String> inviteParams = new HashMap<>();
    //         inviteParams.put("pubUserId", "439");
    //         inviteParams.put("userId", "339");
    //         rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM),
    //                 JSON.toJSONString(inviteParams));
    //         log.info("建立商城团队消息生产：" + JSON.toJSONString(inviteParams));
    //     } catch (Exception e) {
    //         log.error("建立商城团队消息生产异常, exception = {}", e);
    //     }
    //
    //     for (int i = 339; i < 345; i++) {
    //         String pubUserId = String.valueOf(i);
    //         String userId = String.valueOf(i + 10);
    //
    //         try {
    //             // 建立商城团队
    //             Map<String, String> inviteParams = new HashMap<>();
    //             inviteParams.put("pubUserId", pubUserId);
    //             inviteParams.put("userId", userId);
    //             rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM),
    //                     JSON.toJSONString(inviteParams));
    //             log.info("建立商城团队消息生产：" + JSON.toJSONString(inviteParams));
    //         } catch (Exception e) {
    //             log.error("建立商城团队消息生产异常, exception = {}", e);
    //         }
    //
    //         userId = String.valueOf(i + 11);
    //         try {
    //             // 建立商城团队
    //             Map<String, String> inviteParams = new HashMap<>();
    //             inviteParams.put("pubUserId", pubUserId);
    //             inviteParams.put("userId", userId);
    //             rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM),
    //                     JSON.toJSONString(inviteParams));
    //             log.info("建立商城团队消息生产：" + JSON.toJSONString(inviteParams));
    //         } catch (Exception e) {
    //             log.error("建立商城团队消息生产异常, exception = {}", e);
    //         }
    //
    //     }
    //
    // }

}
