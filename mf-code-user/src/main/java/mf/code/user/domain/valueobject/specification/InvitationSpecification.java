package mf.code.user.domain.valueobject.specification;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.aggregateroot.AppletActivity;
import mf.code.common.utils.RegexUtils;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.UserPubJoin;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * 邀请规格
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-23 下午3:03
 */
@Slf4j
@Data
public abstract class InvitationSpecification {
	/**
	 * 平台id， 在各邀请关系中，我们有时需要对平台进行特殊处理
 	 */
	protected Long platformId;
	/**
	 * 活动id
	 */
	protected Long activityId;
	/**
	 * 活动定义id
	 */
	protected Long activityDefId;
	/**
	 * 邀请类型
	 */
	protected Integer userPubJoinType;
	/**
	 * 店铺id
	 */
	protected Long shopId;
	/**
	 * 邀请向量类型：1下级 2上级
	 */
	protected InvitationTypeEnum invitationTypeEnum;
	/**
	 * 	邀请维度类型，即查找数据范围：1店铺维度的邀请关系 2活动维度的邀请关系 3平台维度的邀请关系
 	 */
	protected InvitationDimensionEnum invitationDimension;
	/**
	 * 	针对活动 是否 有邀请属性
 	 */
	protected Boolean isInvite = true;

	/**
	 * 判断用户是否可以被邀请
	 * @param appletPubUser
	 * @param appletSubUser
	 * @param invitationRecords
	 * @return
	 */
	public abstract boolean canBeInvited(AppletUser appletPubUser, AppletUser appletSubUser, Map<Long, UserPubJoin> invitationRecords);

	public Boolean isInvite () {
		return isInvite;
	}
}
