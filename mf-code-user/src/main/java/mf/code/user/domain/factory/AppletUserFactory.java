package mf.code.user.domain.factory;

import com.alibaba.fastjson.JSONObject;
import mf.code.common.utils.RegexUtils;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * mf.code.user.domain.factory
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-20 17:15
 */
public class AppletUserFactory {

	public static AppletUser newAppletUser(JSONObject verifyResultData) {
		String openid = verifyResultData.getString("openid");
		JSONObject userInfo = verifyResultData.getJSONObject("userInfo");
		String merchantId = verifyResultData.getString("merchantId");
		String sessionKey = verifyResultData.getString("sessionKey");
		String shopId = verifyResultData.getString("shopId");
		String scene = verifyResultData.getString("scene");
		Date now = new Date(System.currentTimeMillis() / 1000 * 1000);

		User user = new User();
		user.setNickName(userInfo.getString("nickName"));
		user.setGender(userInfo.getIntValue("gender"));
		user.setCity(userInfo.getString("city"));
		user.setProvince(userInfo.getString("province"));
		user.setCountry(userInfo.getString("country"));
		user.setAvatarUrl(userInfo.getString("avatarUrl"));
		user.setGrantStatus(1);
		user.setGrantTime(now);
		user.setCtime(now);
		if (userInfo.containsKey("unionId")) {
			user.setUnionId(userInfo.getString("unionId"));
		}
		user.setOpenId(openid);
		user.setSessionKey(sessionKey);

		if (StringUtils.isNotBlank(merchantId) && RegexUtils.StringIsNumber(merchantId)) {
			user.setMerchantId(Long.valueOf(merchantId));
		} else {
			user.setMerchantId(1L);
		}
		if (StringUtils.isNotBlank(shopId) && RegexUtils.StringIsNumber(shopId)) {
			user.setShopId(Long.valueOf(shopId));
		} else {
			user.setShopId(39L);
		}
		if (StringUtils.isNotBlank(scene) && RegexUtils.StringIsNumber(scene)) {
			user.setScene(Integer.valueOf(scene));
		}
		user.setUtime(now);

		// 装配 appletUser
		AppletUser appletUser = new AppletUser();
		appletUser.setId(user.getId());
		appletUser.setUserInfo(user);

		return appletUser;
	}
}
