package mf.code.user.domain.aggregateroot;

import lombok.Data;
import mf.code.user.repo.po.User;

/**
 * mf.code.user.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-08 22:49
 */
@Data
public class DouXiaoPuUser {
    /**
     * 抖小铺的用户id
     */
    private Long userId;
    /**
     * 抖小铺的用户信息
     */
    private User userInfo;
}
