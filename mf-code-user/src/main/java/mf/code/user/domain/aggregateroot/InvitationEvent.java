package mf.code.user.domain.aggregateroot;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.user.domain.valueobject.specification.InvitationSpecification;
import mf.code.user.domain.valueobject.specification.InviteShoppingTeamSpecification;
import mf.code.user.repo.po.User;
import mf.code.user.repo.po.UserPubJoin;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.user.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 15:09
 */
@Slf4j
@Data
public class InvitationEvent {
	private AppletUser appletPubUser;
	private AppletUser appletSubUser;
	private InvitationSpecification invitationSpecification;
	private UserPubJoin recordInfo;
	private Date invitationTime;

	public InvitationEvent(AppletUser appletPubUser, AppletUser appletSubUser, InvitationSpecification invitationSpecification) {
		this.appletPubUser = appletPubUser;
		this.appletSubUser = appletSubUser;
		this.invitationSpecification = invitationSpecification;
		this.invitationTime = new Date();
		this.recordInfo = newUserPubJoin();
	}

	public void resetRecordInfo (UserPubJoin recordInfo) {
		recordInfo.setUserId(this.appletPubUser.getId());
		recordInfo.setTotalScottare(BigDecimal.ZERO);
		recordInfo.setUtime(this.invitationTime);
		this.recordInfo = recordInfo;
	}

	private UserPubJoin newUserPubJoin() {
		// 获取被邀请者信息
		User user = this.appletSubUser.getUserInfo();

		UserPubJoin userPubJoin = new UserPubJoin();
		userPubJoin.setUserId(this.appletPubUser.getId());
		userPubJoin.setPubTime(this.invitationTime);
		userPubJoin.setSubUid(this.appletSubUser.getId());
		userPubJoin.setSubNick(user.getNickName());
		userPubJoin.setSubAvatar(user.getAvatarUrl());
		userPubJoin.setSubLoginTime(user.getGrantTime());
		userPubJoin.setSubActionStatus(0);
		userPubJoin.setSubActionTime(new Date(0));
		userPubJoin.setTotalScottare(BigDecimal.ZERO);
		userPubJoin.setTotalCommission(BigDecimal.ZERO);
		userPubJoin.setCtime(this.invitationTime);
		userPubJoin.setUtime(this.invitationTime);
		userPubJoin.setActivityId(this.invitationSpecification.getActivityId());
		userPubJoin.setActivityDefId(this.invitationSpecification.getActivityDefId());
		userPubJoin.setType(invitationSpecification.getUserPubJoinType());
		if (this.invitationSpecification instanceof InviteShoppingTeamSpecification) {
			userPubJoin.setShopId(this.appletSubUser.getUserInfo().getVipShopId());
		} else {
			userPubJoin.setShopId(this.appletSubUser.getUserInfo().getShopId());
		}

		if (this.invitationSpecification.getActivityId() == 0L) {
			userPubJoin.setSubActionStatus(1);
			userPubJoin.setSubActionTime(this.invitationTime);
		}

		return userPubJoin;
	}
}
