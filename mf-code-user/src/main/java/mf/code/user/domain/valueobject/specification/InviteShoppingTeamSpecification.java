package mf.code.user.domain.valueobject.specification;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.UserPubJoin;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * 商城邀请规格
 * mf.code.user.domain.valueobject.specification
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 17:46
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class InviteShoppingTeamSpecification extends InvitationSpecification{

	public InviteShoppingTeamSpecification(AppletUser appletSubUser) {
		super.userPubJoinType = UserPubJoinTypeEnum.SHOPPINGMALL.getCode();
		super.activityId = 0L;
		// 2.1商城团队关系，由用户的vipShopId决定
		super.shopId = appletSubUser.getUserInfo().getVipShopId();
		super.invitationTypeEnum = InvitationTypeEnum.INVITED;
		// 由vipShop做全平台唯一关系的限制。 配置SHOP维度，则每个店铺会生成一条对应平台的记录；配置Platform维度，则所有店铺 用户只对应一条平台记录
		super.invitationDimension = InvitationDimensionEnum.SHOP;
		super.isInvite = true;
	}

	public InviteShoppingTeamSpecification(Long vipShopId) {
		super.userPubJoinType = UserPubJoinTypeEnum.SHOPPINGMALL.getCode();
		super.activityId = 0L;
		super.shopId = vipShopId;
		super.invitationTypeEnum = InvitationTypeEnum.INVITED;
		// 由vipShop做全平台唯一关系的限制。 配置SHOP维度，则每个店铺会生成一条对应平台的记录；配置Platform维度，则所有店铺 用户只对应一条平台记录
		// 针对用户查询邀请记录时，是按店铺维度 --- 但在建立邀请关系时，查询用户被邀请记录时，则又是按平台维度……FIXME_OK
		super.invitationDimension = InvitationDimensionEnum.PLATFORM;
		super.isInvite = true;
	}

	/**
	 * 针对用户查询邀请记录时，是按店铺维度 --- 但在建立邀请关系时，查询用户被邀请记录时，则又是按平台维度……
	 * 建立商城团队时，通过传入邀请维度，指定按平台维度查询
	 * @param appletSubUser
	 * @param invitationDimensionEnum
	 */
	public InviteShoppingTeamSpecification(AppletUser appletSubUser, InvitationDimensionEnum invitationDimensionEnum) {
		super.userPubJoinType = UserPubJoinTypeEnum.SHOPPINGMALL.getCode();
		super.activityId = 0L;
		// 2.1商城团队关系，由用户的vipShopId决定
		super.shopId = appletSubUser.getUserInfo().getVipShopId();
		super.invitationTypeEnum = InvitationTypeEnum.INVITED;
		// 由vipShop做全平台唯一关系的限制。 配置SHOP维度，则每个店铺会生成一条对应平台的记录；配置Platform维度，则所有店铺 用户只对应一条平台记录
		super.invitationDimension = invitationDimensionEnum;
		super.isInvite = true;
	}

	/**
	 * 判断用户是否可以被邀请 -- 财富大闯关邀请的规则
	 * 查询邀请记录时出错，或未查询记录时，无法被邀请
	 * 并且 查询到的记录为空时，可以被邀请 -- 按type类型和subUserId条件查询，就可以保证平台记录也只有一条了
	 * 并且 查询到的记录 上级是平台的话，也不能被邀请（和矿工的不同点）
	 * 并且 被邀请者进入的店铺 和 邀请者的粉丝店铺不符，无法被邀请 -- 此条之前的条件满足后，之后条件在2.1的迭代中，只要vipShopId匹配即可邀请成功
	 * 并且 被邀请者的粉丝店铺 和 邀请者的粉丝店铺不符，无法被邀请
	 * ps: 如果上级是平台，被邀请成功后，平台的记录会直接修改成邀请者的记录，即不再存在平台记录（此规则 不适用商城邀请）
	 * @param appletPubUser
	 * @param appletSubUser
	 * @param invitationRecords
	 * @return
	 */
	public boolean canBeInvited(AppletUser appletPubUser, AppletUser appletSubUser, Map<Long, UserPubJoin> invitationRecords) {
		if (invitationRecords == null) {
			log.error("未生成邀请记录");
			return false;
		}

		// 如果不为空，不管是平台记录，还是用户记录，都不能被邀请
		if (!CollectionUtils.isEmpty(invitationRecords)) {
			return false;
		}

		// 记录为空，如果 appletPubUser是平台，则无需判定 店铺属性匹配
		if (appletPubUser.getId().equals(super.platformId)) {
			return true;
		}

		// 如果 appletPubUser 不是平台， 则 进行店铺属性的判定：
		Long pubUserVipShopId = appletPubUser.getUserInfo().getVipShopId();
		// 判断pubUser是不是在自己的粉丝店铺 建立团队 -- 此条不满足2.1迭代要求，废弃
		// if (!appletSubUser.getUserInfo().getShopId().equals(pubUserVipShopId)) {
		// 	// 被邀请进来的用户更新店铺后的shopId，与邀请者的粉丝店铺Id不匹配，说明邀请者是在非他粉丝店铺中邀请的，不能成为团队
		// 	log.info("非此店铺粉丝，无法成为团队成员");
		// 	return false;
		// }

		// 判断user和pubUser是不是同一个店铺的粉丝 -- 2.1.4迭代 取消此判断
		// if (!appletSubUser.getUserInfo().getVipShopId().equals(pubUserVipShopId)) {
		// 	log.info("邀请者和被邀请者的vipShopId不匹配，无法成为团队成员");
		// 	return false;
		// }

		return true;
	}

}
