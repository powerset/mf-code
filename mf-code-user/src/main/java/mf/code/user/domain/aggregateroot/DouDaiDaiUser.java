package mf.code.user.domain.aggregateroot;

import lombok.Data;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.repo.po.User;

/**
 * mf.code.user.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-08 10:15
 */
@Data
public class DouDaiDaiUser {
    /**
     * 抖带带主播id
     */
    private Long merchantId;
    /**
     * 抖带带主播信息
     */
    private MerchantDTO merchantInfo;
    /**
     * 抖带带主播店铺信息
     */
    private MerchantShopDTO merchantShopInfo;
    /**
     * 抖带带主播信息在user表的映射
     */
    private User userInfo;
}
