package mf.code.user.domain.valueobject.specification;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.UserPubJoin;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-19 下午4:22
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class ActivityInvitationSpecification extends InvitationSpecification{

	public ActivityInvitationSpecification(Activity activity) {
		Integer activityType = activity.getType();

		super.activityId = activity.getId();
		super.activityDefId = activity.getActivityDefId();
		super.userPubJoinType = UserPubJoinTypeEnum.getCodeByActivityType(activityType);
		super.shopId = activity.getShopId();
		super.invitationTypeEnum = InvitationTypeEnum.INVITATION;
		super.invitationDimension = InvitationDimensionEnum.ACTIVITY;
		if (ActivityTypeEnum.LUCK_WHEEL.getCode() == activityType) {
			super.invitationDimension = InvitationDimensionEnum.SHOP;
		}
		if (ActivityTypeEnum.ORDER_REDPACK.getCode() == activityType) {
			super.isInvite = false;
		}
		if (ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode() == activityType) {
			super.isInvite = false;
		}
	}

	/**
	 * 判断用户是否可以被邀请 -- 通用活动邀请的规则
	 * @param appletPubUser
	 * @param appletSubUser
	 * @param invitationRecords
	 * @return true 可以邀请 false：之前已邀请
	 */
	public boolean canBeInvited(AppletUser appletPubUser, AppletUser appletSubUser, Map<Long, UserPubJoin> invitationRecords) {
		if (invitationRecords == null) {
			log.error("未生成邀请记录");
			return false;
		}
		if (CollectionUtils.isEmpty(invitationRecords)) {
			return true;
		}

		for (UserPubJoin userPubJoin : invitationRecords.values()) {
			if (userPubJoin.getSubUid().equals(appletSubUser.getId())) {
				log.info("曾今邀请过此用户");
				return false;
			}
		}
		return true;
	}
}
