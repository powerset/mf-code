package mf.code.user.domain.aggregateroot;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.common.DelEnum;
import mf.code.common.utils.RegexUtils;
import mf.code.user.dto.AppletUserDTO;
import mf.code.user.dto.UserIncomeResp;
import mf.code.user.dto.UserResp;
import mf.code.user.repo.po.User;
import mf.code.user.repo.po.UserAddress;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-27 10:41
 */
@Data
public class AppletUser {
	/**
	 * 用户id
	 */
	private Long id;
	/**
	 * 用户信息
	 */
	private User userInfo;
	/**
	 * 地址信息
	 * Map<addressId, userAddress>
	 */
	private Map<String, UserAddress> userAddressInfo = new HashMap<>();
	/**
	 * 用户收入信息
	 */
	private UserIncomeResp userIncome;

	/**
	 * 是否有粉丝店铺
	 *
	 * @return true 有粉丝店铺， false 无粉丝店铺
	 */
	public boolean hasVipShop() {
		return this.userInfo.getVipShopId() != null;
	}

	/**
	 * 此店铺是否是vipShopId
	 *
	 * @return true 是粉丝店铺， false 不是粉丝店铺
	 */
	public boolean isInVipShop() {
		return this.userInfo.getVipShopId().equals(this.userInfo.getShopId());
	}

	/**
	 * 新增地址
	 *
	 * @param userAddress
	 * @return
	 */
	public UserAddress addAddress(UserAddress userAddress) {
		// 新增地址的限制 最多10条
		if (this.userAddressInfo.size() >= 10) {
			return null;
		}
		Date now = new Date();
		userAddress.setDel(DelEnum.NO.getCode());
		userAddress.setCtime(now);
		userAddress.setUtime(now);
		this.userAddressInfo.put("0", userAddress);
		return userAddress;
	}

	/**
	 * 更新地址
	 *
	 * @param userAddress
	 * @return
	 */
	public UserAddress updateAddress(UserAddress userAddress) {
		// 校验 该用户是否有此地址
		if (!this.userAddressInfo.containsKey(userAddress.getId().toString())) {
			return null;
		}
		Date now = new Date();
		userAddress.setUtime(now);
		this.userAddressInfo.put(userAddress.getId().toString(), userAddress);
		return userAddress;
	}

	/**
	 * 删除地址
	 *
	 * @param userAddress
	 * @return
	 */
	public UserAddress deleteAddress(UserAddress userAddress) {
		// 校验 该用户是否有此地址
		if (!this.userAddressInfo.containsKey(userAddress.getId().toString())) {
			return null;
		}
		Date now = new Date();
		userAddress.setUtime(now);
		userAddress.setDel(DelEnum.YES.getCode());
		this.userAddressInfo.remove(userAddress.getId().toString());
		return userAddress;
	}

	/**
	 * 成为店铺粉丝 -- 调用此方法前，请先调用 this.hasVipShop() 进行判断
	 *
	 * @return
	 */
	public void beVipShop() {
		// 成为当前店铺的粉丝
		this.userInfo.setVipShopId(this.userInfo.getShopId());
	}

	/**
	 * 深拷贝一份 用户信息 用于 数据传输
	 *
	 * @return
	 */
	public AppletUserDTO copyAppletUser() {
		UserResp userResp = new UserResp();
		UserIncomeResp userIncomeResp = new UserIncomeResp();
		BeanUtils.copyProperties(this.userInfo, userResp);
		BeanUtils.copyProperties(this.userIncome, userIncomeResp);

		AppletUserDTO appletUserDTO = new AppletUserDTO();
		appletUserDTO.setUserId(this.id);
		appletUserDTO.setUserResp(userResp);
		appletUserDTO.setUserIncomeResp(userIncomeResp);
		return appletUserDTO;
	}

	/**
	 * 用户登录 -- 更新用户信息
	 *
	 * @return
	 */
	public void login(JSONObject verifyResultData) {
		JSONObject userInfo = verifyResultData.getJSONObject("userInfo");
		String merchantId = verifyResultData.getString("merchantId");
		String shopId = verifyResultData.getString("shopId");
		String scene = verifyResultData.getString("scene");
		Date now = new Date(System.currentTimeMillis() / 1000 * 1000);

		if (userInfo != null) {
			this.userInfo.setNickName(userInfo.getString("nickName"));
			this.userInfo.setGender(userInfo.getIntValue("gender"));
			this.userInfo.setCity(userInfo.getString("city"));
			this.userInfo.setProvince(userInfo.getString("province"));
			this.userInfo.setCountry(userInfo.getString("country"));
			this.userInfo.setAvatarUrl(userInfo.getString("avatarUrl"));
			if (userInfo.containsKey("unionId")) {
				this.userInfo.setUnionId(userInfo.getString("unionId"));
			}
		}

		if (StringUtils.isNotBlank(merchantId) && RegexUtils.StringIsNumber(merchantId)) {
			this.userInfo.setMerchantId(Long.valueOf(merchantId));
		}
		if (StringUtils.isNotBlank(shopId) && RegexUtils.StringIsNumber(shopId)) {
			this.userInfo.setShopId(Long.valueOf(shopId));
		}
		if (StringUtils.isNotBlank(scene) && RegexUtils.StringIsNumber(scene)) {
			this.userInfo.setScene(Integer.valueOf(scene));
		}
		this.userInfo.setUtime(now);
	}
}
