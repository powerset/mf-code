package mf.code.user.api.applet;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.service.AppletUserLoginService;
import mf.code.user.service.DouDaiDaiLoginService;
import mf.code.user.service.DouXiaoPuLoginService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * mf.code.user.api.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 16:24
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/user/applet/v5")
public class loginApi {
    private final AppletUserLoginService appletUserLoginService;
    private final DouDaiDaiLoginService douDaiDaiLoginService;
    private final DouXiaoPuLoginService douXiaoPuLoginService;
    @Value("${dxp.default.merchantId}")
    private String dxpDefaultMerchantId;
    @Value("${dxp.default.shopId}")
    private String dxpDefaultShopId;

    /**
     * 微信用户 登录小程序
     *
     * @param wxUserSession
     * @return
     */
    @PostMapping("/login")
    public SimpleResponse login(@RequestBody WxUserSession wxUserSession, HttpServletRequest httpServletRequest) {
        String project = httpServletRequest.getHeader("project");
        if (StringUtils.isBlank(project)) {
            return  new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "head参数缺失");
        }
        if ("jkmf".equals(project)) {
            return appletUserLoginService.login(wxUserSession);
        }
        if ("ddd".equals(project)) {
            return douDaiDaiLoginService.login(wxUserSession);
        }
        if ("dxp".equals(project)) {
            if (StringUtils.isBlank(wxUserSession.getMerchantId()) || StringUtils.isBlank(wxUserSession.getShopId())) {
                wxUserSession.setMerchantId(dxpDefaultMerchantId);
                wxUserSession.setShopId(dxpDefaultShopId);
            }
            return douXiaoPuLoginService.login(wxUserSession);
        }
        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "head参数异常");

    }
}
