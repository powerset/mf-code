package mf.code.user.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.dto.MerchantOrderReq;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.api.feignclient.ShopAppService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
@Slf4j
public class ShopAppFeignFallbackFactory implements FallbackFactory<ShopAppService> {
    @Override
    public ShopAppService create(Throwable throwable) {
        return new ShopAppService() {
            @Override
            public Map<String, Object> queryShopInfoByLogin(String shopId) {
                return null;
            }

            @Override
            public Long getMerchantIdByShopId(Long shopId) {
                return null;
            }

            @Override
            public List<Long> selectAllIds() {
                return null;
            }

            @Override
            public Integer updateMerchantBalance(MerchantOrderReq merchantOrderReq) {
                return null;
            }

            @Override
            public boolean saveViewShop(Long userId, Long shopId) {
                return false;
            }

            @Override
            public boolean deleteShopFootprint(Long userId, Long shopId) {
                return false;
            }

            @Override
            public MerchantDTO queryMerchantByOpenId(String openId) {
                return null;
            }

            @Override
            public MerchantDTO queryMerchantById(Long merchantId) {
                return null;
            }

            @Override
            public Long saveOrUpdateMerchant(MerchantDTO merchantDTO) {
                return null;
            }

            @Override
            public MerchantShopDTO findShopByMerchantId(Long merchantId) {
                return null;
            }

            @Override
            public Long saveOrUpdateMerchantShop(MerchantShopDTO merchantShopDTO) {
                return null;
            }
        };
    }
}
