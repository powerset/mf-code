package mf.code.user.api.applet.dto;

import lombok.Data;

/**
 * mf.code.api.applet.login.domain
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-15 下午4:07
 */
@Data
public class WxUserSession {
	// 唯一标识
	private String code;
	// 用户密文信息
	private String encryptedData;
	// 用于解密向量
	private String iv;
	// 用户明文信息
	private String userinfo;

	// 小程序id
	private String appid;
	// 小程序密钥
	private String appSecret;

	// 用户邀请者
	private String pub_uid;
	// 邀请的活动ID
	private String aid;
	// 活动所属商户
	private String merchantId;
	// 活动所属店铺
	private String shopId;
	// 用户登录场景标识
	private String scene;
	// 非活动邀请的类型
	private String pubType;
	// entryStatus
	private String entryStatus;

	// recall
	private String recallScene;
}
