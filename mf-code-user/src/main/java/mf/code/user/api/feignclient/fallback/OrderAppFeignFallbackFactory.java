package mf.code.user.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.order.dto.DoudaidaiMypageResp;
import mf.code.order.dto.OrderResp;
import mf.code.user.api.feignclient.OrderAppService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@Component
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public Integer queryOrderNumByProductIds(Long shopId, Long userId, List<Long> productIds) {
                return null;
            }

            @Override
            public int countGoodsNumByOrder(Long goodsId) {
                return 0;
            }

            @Override
            public OrderResp selectOrder(Long shopId, Long userId, String time) {
                return new OrderResp();
            }

            @Override
            public OrderResp queryOrder(Long orderId) {
                return null;
            }

            @Override
            public int summaryOrderByShopIdAndProductId(Long shopId, Long productId) {
                return 0;
            }

            @Override
            public List<OrderResp> queryOrderSettledByUserIdShopId(Long userId, Long shopId) {
                return null;
            }

            @Override
            public Map<String, OrderResp> queryOrderRecordThisMonth(Long userId, Long shopId) {
                return null;
            }

            @Override
            public Map<Long, Integer> countGoodsNumByProductIds(List<Long> productIds) {
                return null;
            }

            @Override
            public DoudaidaiMypageResp getDoudaidaiMypageData(Long merchantId, Long shopId) {
                return null;
            }
        };
    }
}
