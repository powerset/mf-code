package mf.code.user.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.feignservice.UserAppServiceImpl;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.AppletUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.api.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-15 17:21
 */
@Slf4j
@RestController
@RequestMapping("/api/user/applet/fixDate")
public class fixUserVipShopIdApi {
	@Autowired
	private AppletUserRepository appletUserRepository;
	@Autowired
	private UserAppServiceImpl userAppService;
	@Autowired
	private ShopAppService shopAppService;

	/**
	 * 修复粉丝店铺
	 * @param userId
	 * @return
	 */
	@GetMapping("/userVipShopId")
	public SimpleResponse login(@RequestParam(name = "userId") Long userId) {
		AppletUser appletUser = appletUserRepository.findById(userId);
		if (appletUser == null) {
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此用户");
		}
		User userInfo = appletUser.getUserInfo();
		Map<Integer, List<Long>> integerListMap = userAppService.queryLowerMember(userInfo.getVipShopId(), userId, 5);
		if (CollectionUtils.isEmpty(integerListMap)) {
			Map<String, Object> resultVO = new HashMap<>();
			resultVO.put("affectedRows",0);
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		List<Long> userIds = new ArrayList<>();
		for (List<Long> levelUserIds : integerListMap.values()) {
			userIds.addAll(levelUserIds);
		}
		List<AppletUser> appletUsers = appletUserRepository.listByIds(userIds);
		for (AppletUser fixUser : appletUsers) {
			User user = fixUser.getUserInfo();
			log.info("update userId = {}, shopId = {}, vipShopId = {}", user.getId(), user.getShopId(), user.getVipShopId());
			user.setVipShopId(user.getShopId());

			boolean success = appletUserRepository.saveOrUpdateUserInfo(fixUser);
			if (!success) {
				log.error("update error: userId = {}, shopId = {}, vipShopId = {}", user.getId(), user.getShopId(), user.getVipShopId());
				continue;
			}
			shopAppService.deleteShopFootprint(user.getId(), user.getVipShopId());
		}
		Map<String, Object> resultVO = new HashMap<>();
		resultVO.put("affectedRows",userIds.size());
		return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
	}
}
