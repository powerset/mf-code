package mf.code.user.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.service.GenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.user.api.applet
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月25日 11:49
 */
@Slf4j
@RestController
@RequestMapping("/api/user/applet/v8/poster")
public class UserPosterApi {
    @Autowired
    private GenerateService generateService;

    /***
     * 推荐粉丝成为店长海报
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/recommendFans")
    public SimpleResponse recommendFans(@RequestParam("merchantId") Long merchantId,
                                        @RequestParam("shopId") Long shopId,
                                        @RequestParam("from") Integer fromId,
                                        @RequestParam("userId") Long userId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0
                || fromId == null || fromId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        return generateService.recommendFans(merchantId, shopId, userId, fromId);
    }

    /***
     * 分享自己的群，生成粉丝群
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/groupPoster")
    public SimpleResponse groupPoster(@RequestParam("merchantId") Long merchantId,
                                      @RequestParam("shopId") Long shopId,
                                      @RequestParam("userId") Long userId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        return generateService.groupPoster(merchantId, shopId, userId);
    }
}
