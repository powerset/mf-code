package mf.code.user.api.feignclient;

import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.dto.MerchantOrderReq;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.api.feignclient.fallback.ShopAppFeignFallbackFactory;
import mf.code.user.common.config.feign.FeignLogConfiguration;
import mf.code.user.domain.aggregateroot.AppletUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * mf.code.shop.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 20:34
 */
@FeignClient(value = "mf-code-shop", fallbackFactory = ShopAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface ShopAppService {

	/**
	 * 微信用户 登录小程序 返回相关店铺信息
	 *
	 * @param shopId
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/v5/queryShopInfoByLogin")
	Map<String, Object> queryShopInfoByLogin(@RequestParam("shopId") String shopId);

	/**
	 * 根据店铺id获取商户id
	 *
	 * @param shopId 店铺id
	 * @return 商户id
	 */
	@GetMapping("/feignapi/shop/applet/v5/getMerchantIdByShopId")
	Long getMerchantIdByShopId(@RequestParam("shopId") Long shopId);

	/**
	 * 获取所有有效店铺的ids
	 *
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/v5/selectAllIds")
	List<Long> selectAllIds();

	/**
	 * 插入记录 并 更新余额
	 *
	 * @param merchantOrderReq
	 * @return
	 */
	@PostMapping("/feignapi/shop/applet/v5/updateMerchantBalance")
	Integer updateMerchantBalance(@RequestBody MerchantOrderReq merchantOrderReq);

	/**
	 * 保存用户浏览店铺的足迹
	 *
	 * @param userId
	 * @param shopId
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/v7/saveViewShop")
	boolean saveViewShop(@RequestParam("userId") Long userId,
	                     @RequestParam("shopId") Long shopId);

	/**
	 * 删除足迹
	 *
	 * @param userId
	 * @param shopId
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/deleteShopFootprint")
	boolean deleteShopFootprint(@RequestParam("userId") Long userId,
	                            @RequestParam("shopId") Long shopId);

	/**
	 * 通过 openId 获取 抖带带主播信息
	 * @param openId
	 * @return
	 */
	@GetMapping("feignapi/shop/applet/queryMerchantByOpenId")
    MerchantDTO queryMerchantByOpenId(@RequestParam("openId") String openId);

	/**
	 * 通过 merchantId 获取 抖带带主播信息
	 * @param merchantId
	 * @return
	 */
	@GetMapping("feignapi/shop/applet/queryMerchantById")
	MerchantDTO queryMerchantById(@RequestParam("merchantId") Long merchantId);

	/**
	 * 保存或更新merchant
	 * @param merchantDTO
	 * @return
	 */
	@PostMapping("feignapi/shop/applet/saveOrUpdateMerchant")
	Long saveOrUpdateMerchant(@RequestBody MerchantDTO merchantDTO);

	/**
	 * 通过merchantId查店铺
	 *
	 * @param merchantId
	 * @return
	 */
	@GetMapping("/feignapi/shop/applet/findShopByMerchantId")
	MerchantShopDTO findShopByMerchantId(@RequestParam("merchantId") Long merchantId);

	/**
	 * 保存或更新merchantShopInfo
	 * @param merchantShopDTO
	 * @return
	 */
	@PostMapping("feignapi/shop/applet/saveOrUpdateMerchantShop")
	Long saveOrUpdateMerchantShop(@RequestBody MerchantShopDTO merchantShopDTO);
}
