package mf.code.user.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.one.dto.ActivityDefDTO;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.dto.UserTaskIncomeResp;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.api.feignclient.fallback
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:07
 */
@Slf4j
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable cause) {
        return new OneAppService() {
            @Override
            public Map<String, Object> queryOpenRedPackageStatus(Long shopId, Long userId) {
                return new HashMap<>();
            }

            @Override
            public Map<String, Object> queryNewcomerTaskInfo(Long shopId, Long userId) {
                return new HashMap<>();
            }

            @Override
            public UserTaskIncomeResp queryUserTaskIncome(Long userId) {
                log.error("熔断处理，获取店长任务信息异常");
                return null;
            }

            @Override
            public ActivityDefDTO findShopManagerInviteFansTask(Long activityDefId) {
                return null;
            }

            @Override
            public String giveAwardInviteFans(Long userId, Long activityDefId) {
                return null;
            }

            @Override
            public String getGroupCodeByUserId(Long userId) {
                return null;
            }

            @Override
            public Map<String, Object> queryFullReimbursementInfo(Long shopId, Long userId) {
                return new HashMap<>();
            }
        };
    }
}
