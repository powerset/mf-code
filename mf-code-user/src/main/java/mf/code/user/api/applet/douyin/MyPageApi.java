package mf.code.user.api.applet.douyin;/**
 * create by qc on 2019/8/9 0009
 */

import lombok.RequiredArgsConstructor;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.service.MyPageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * mf.code.api.applet.v12.douyin
 * 抖带带-我的页面
 *
 * @author gbf
 * 2019/8/9 0009、09:57
 */
@RestController
@RequestMapping("/api/user/applet/v12/douyin")
@RequiredArgsConstructor
public class MyPageApi {
    private final MyPageService myPageService;

    /**
     * 抖带带-我的页面
     *
     * @param userID 用户
     * @param shopID 店铺
     * @return SimpleResponse
     */
    @GetMapping("doudaidaimypage")
    public SimpleResponse doudaidaiMyPage(@RequestParam(name = "userId") final Long userID,
                                          @RequestParam(name = "shopId") final Long shopID) {
        return this.myPageService.doudaidaiMyPage(userID, shopID);
    }
}
