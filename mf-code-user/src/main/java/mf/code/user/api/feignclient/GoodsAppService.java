package mf.code.user.api.feignclient;

import mf.code.user.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import mf.code.user.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * mf.code.goods.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 14:10
 */
@FeignClient(name = "mf-code-goods", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface GoodsAppService {

    /***
     * 获取店铺的商品编号集合
     * @param shopId
     * @param type 0:普通商品 1：新人专享商品 ProductShowTypeEnum 枚举里
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/getProductIds")
    List<Long> getProductIds(@RequestParam("shopId") Long shopId,
                             @RequestParam(value = "type", defaultValue = "0") Integer type);
}
