package mf.code.user.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.RebatePolicyDTO;
import mf.code.user.api.feignclient.fallback.DistributionAppFeignFallbackFactory;
import mf.code.user.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-08 19:11
 */
@FeignClient(value = "mf-code-distribution", fallbackFactory = DistributionAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface DistributionAppService {

    @GetMapping("/feignapi/distribution/applet/v8/hasReachSpendStandardThisMonth")
    boolean hasReachSpendStandardThisMonth(@RequestParam("userId") Long userId);

    /**
     * 获取 用户返利金额
     *
     * @param productDTO
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/getRebateCommission")
    String getRebateCommission(@RequestBody ProductDistributionDTO productDTO);

    /**
     * 支付成功 处理用户返利和分佣
     *
     * @param userId
     * @param shopId
     * @param orderId
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/distributionCommission")
    SimpleResponse distributionCommission(@RequestParam("userId") Long userId,
                                          @RequestParam("shopId") Long shopId,
                                          @RequestParam("orderId") Long orderId);

    /**
     * 获取用户商品返利
     *
     * @param distributionDTOLists
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryUserRebateByProduct")
    Map<Long, String> queryUserRebateByProduct(@RequestParam("distributionDTOLists") List<String> distributionDTOLists);

    /***
     * 获取用户的团队粉丝数(包含自己)
     * @param shopId
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryTeamUsers")
    Map<Long, Integer> queryTeamUser(@RequestParam("shopId") Long shopId,
                                     @RequestParam("userIds") List<Long> userIds);

    @GetMapping("/feignapi/distribution/applet/v5/sumTotalFeePendByBizTypes")
    String sumTotalFeePendByBizTypes(@RequestParam("userId") Long userId);
}
