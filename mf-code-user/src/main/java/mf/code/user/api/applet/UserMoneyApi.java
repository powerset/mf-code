package mf.code.user.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.service.MktTransfersService;
import mf.code.user.service.UserMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * mf.code.user.api.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-15 09:55
 */
@Slf4j
@RestController
@RequestMapping("/api/user/applet/v5/userMoney")
public class UserMoneyApi {
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private MktTransfersService mktTransfersService;

    /***
     * 提现前置的控制弹窗
     * @param shopID
     * @param userID
     * @return
     */
    @GetMapping("/queryMktTransferDialog")
    public SimpleResponse queryMktTransferDialog(@RequestParam(name = "merchantId") Long merchantId,
                                                 @RequestParam(name = "shopId") Long shopID,
                                                 @RequestParam(name = "userId") Long userID) {
        //是否有带解锁金额，是否有下单记录，是否有未支付商品订单
        return mktTransfersService.checkLockMoney(userID, shopID, merchantId);
    }

    /***
     * 提现成功后的控制弹窗
     * <pre>
     *
     * </pre>
     * @param shopID
     * @param userID
     * @param source 1:闯关进来 2:报销活动进来
     * @return
     */
    @GetMapping("/queryMktTransferSuccessDialog")
    public SimpleResponse queryMktTransferSuccessDialog(@RequestParam(name = "merchantId") Long merchantId,
                                                        @RequestParam(name = "shopId") Long shopID,
                                                        @RequestParam(name = "userId") Long userID,
                                                        @RequestParam(name = "source", required = false, defaultValue = "0") Integer source) {
        //提现成功的具体跳转
        return mktTransfersService.queryMktTransferSuccessDialog(merchantId, shopID, userID, source);
    }

    /***
     * 用户提现-企业付款(公众号支付)
     * @param map
     * @return
     */
    @RequestMapping(path = "/createMktTransfersTradeOrder", method = RequestMethod.POST)
    public SimpleResponse createMktTransfersTradeOrder(@RequestBody Map<String, Object> map) {
        return this.userMoneyService.createMktTransfers(map);
    }

    /***
     * 查询我的余额
     * @param userID
     * @return
     */
    @RequestMapping(path = "/queryUserBalance", method = RequestMethod.GET)
    public SimpleResponse queryUserBalance(@RequestParam(name = "merchantId") Long merchantID,
                                           @RequestParam(name = "shopId") Long shopID,
                                           @RequestParam(name = "userId") Long userID) {
        return this.userMoneyService.queryUserBalance(merchantID, shopID, userID);
    }


    /**
     * 签署提现协议d
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @RequestMapping(path = "/signingAgreement", method = RequestMethod.GET)
    public SimpleResponse signingAgreement(@RequestParam(name = "merchantId") Long merchantId,
                                           @RequestParam(name = "shopId") Long shopId,
                                           @RequestParam(name = "userId") Long userId) {
        // 入参校验
        if (merchantId == null || merchantId <= 0
                || shopId == null || shopId <= 0
                || userId == null || userId <= 0) {
            log.error("参数异常");
            new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return this.userMoneyService.signingAgreement(merchantId, shopId, userId);
    }

    /***
     * 订单相关弹幕
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 1 提现
     * @return
     */
    @GetMapping("/barrages")
    public SimpleResponse barrages(@RequestParam("merchantId") Long merchantId,
                                   @RequestParam("shopId") Long shopId,
                                   @RequestParam(name = "userId") Long userId,
                                   @RequestParam(name = "type") int type) {
        return userMoneyService.barrages(merchantId, shopId, userId, type);
    }
}
