package mf.code.user.api.feignclient;

import mf.code.one.dto.ActivityDefDTO;
import mf.code.user.api.feignclient.fallback.OneAppFeignFallbackFactory;
import mf.code.user.common.config.feign.FeignLogConfiguration;
import mf.code.user.dto.UserTaskIncomeResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.user.api.feignclient
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:06
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {

    /**
     * 拆红包是否完成了
     *
     * @param shopId
     * @param userId
     * @return null无活动定义；有活动定义 且用户：0没有活动, 2有进行中活动, 1 完成-金额待解锁
     */
    @GetMapping("/feignapi/one/applet/v6/queryOpenRedPackageStatus")
    Map<String, Object> queryOpenRedPackageStatus(@RequestParam("shopId") Long shopId,
                                                  @RequestParam("userId") Long userId);

    /**
     * 获取新人任务
     *
     * @param shopId
     * @param userId
     * @return null 无活动定义 非null 返回以下内容
     * Map<String, Object> resultVO = new HashMap<>();
     * resultVO.put("amount", totalAmount); totalAmount: 新人任务金额总和
     */
    @GetMapping("/feignapi/one/applet/v6/queryNewcomerTaskInfo")
    Map<String, Object> queryNewcomerTaskInfo(@RequestParam("shopId") Long shopId,
                                              @RequestParam("userId") Long userId);

    /**
     * 查询 用户任务收益
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/queryUserTaskIncome")
    UserTaskIncomeResp queryUserTaskIncome(@RequestParam("userId") Long userId);

    /**
     * 获取 店长 邀请粉丝任务
     *
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/findShopManagerInviteFansTask")
    ActivityDefDTO findShopManagerInviteFansTask(@RequestParam("activityDefId") Long activityDefId);

    /**
     * 店长 邀请粉丝达标 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardInviteFans")
    String giveAwardInviteFans(@RequestParam("userId") Long userId,
                               @RequestParam("activityDefId") Long activityDefId);

    /**
     * 小程序端店长查询群二维码
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/getGroupCodeByUserId")
    String getGroupCodeByUserId(@RequestParam(value = "userId") Long userId);

    /***
     * 查询报销活动信息
     *
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v9/queryFullReimbursementInfo")
    Map<String, Object> queryFullReimbursementInfo(@RequestParam("shopId") Long shopId,
                                                   @RequestParam("userId") Long userId);
}
