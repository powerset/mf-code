package mf.code.user.api.applet.feignservice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.feignapi.dto.UserIdListDTO;
import mf.code.user.api.feignclient.DistributionAppService;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.dto.*;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import mf.code.user.repo.dao.UpayBalanceMapper;
import mf.code.user.repo.dao.UpayWxOrderMapper;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.UpayBalance;
import mf.code.user.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;
import mf.code.user.repo.po.UserAddress;
import mf.code.user.repo.repository.AppletUserRepository;
import mf.code.user.repo.repository.UpayWxOrderService;
import mf.code.user.repo.repository.UserAddressRepository;
import mf.code.user.repo.repository.UserRepository;
import mf.code.user.service.UpayBalanceService;
import mf.code.user.service.UserTeamIncomeService;
import mf.code.user.vo.UpayWxOrderAboutVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.user.api.applet.feignservice
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 10:30
 */
@Slf4j
@RestController
public class UserAppServiceImpl {
    @Autowired
    private UserTeamIncomeService userTeamIncomeService;
    @Autowired
    private UserAddressRepository userAddressRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayWxOrderMapper upayWxOrderMapper;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private AppletUserRepository appletUserRepository;
    @Autowired
    private UpayBalanceMapper upayBalanceMapper;
    @Autowired
    private DistributionAppService distributionAppService;

    /**
     * 通过主键 批量获取用户信息
     *
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/user/applet/v8/listByIds")
    public List<UserResp> listByIds(@RequestParam("userIds") List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            log.error("参数异常");
            return null;
        }
        List<User> users = userRepository.listByIds(userIds);
        if (CollectionUtils.isEmpty(users)) {
            return null;
        }
        List<UserResp> userRespList = new ArrayList<>();
        for (User user : users) {
            UserResp userResp = new UserResp();
            BeanUtils.copyProperties(user, userResp);
            userRespList.add(userResp);
        }
        return userRespList;
    }
	/**
	 * 批量获取 用户们 全平台的任务收益情况
	 * @param userIds
	 * @return
	 */
	@GetMapping("/feignapi/user/applet/v9/listTotalTaskIncomeByUserIds")
	public Map<Long, Map<String, String>> listTotalTaskIncomeByUserIds(@RequestParam("userIds") List<Long> userIds) {
		if (CollectionUtils.isEmpty(userIds)) {
			log.error("参数异常");
			return null;
		}

		//任务奖金汇总
		List<Map> taskAwardList = upayWxOrderService.sumTotalFeeByBizTypes(null, userIds, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode(), UpayWxOrderAboutVO.addTaskAward());

		Map<Long, String> taskAwardMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(taskAwardList)) {
			for (Map taskAward : taskAwardList) {
				Long userId = Long.valueOf(taskAward.get("userId").toString());
				String totalFee = taskAward.get("totalFee").toString();
				taskAwardMap.put(userId, totalFee);
			}
		}


		//任务佣金汇总
		List<Map> taskCommissionList = upayWxOrderService.sumTotalFeeByBizTypes(null, userIds, UpayWxOrderTypeEnum.REBATE.getCode(), UpayWxOrderAboutVO.addTaskCommission());
		Map<Long, String> taskCommissionMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(taskAwardList)) {
			for (Map taskCommission : taskCommissionList) {
				Long userId = Long.valueOf(taskCommission.get("userId").toString());
				String totalFee = taskCommission.get("totalFee").toString();
				taskCommissionMap.put(userId, totalFee);
			}
		}

		Map<Long, Map<String, String>> taskIncomeMap = new HashMap<>();
		for (Map.Entry entry : taskAwardMap.entrySet()) {
			Long userId = (Long) entry.getKey();
			String taskAward = entry.getValue().toString();

			String commission = taskCommissionMap.get(userId);
			String taskCommission = StringUtils.isBlank(commission) ? "0" : commission;

			Map<String, String> taskIncome = new HashMap<>();
			taskIncome.put("taskAward", taskAward);
			taskIncome.put("taskCommission", taskCommission);
			taskIncomeMap.put(userId, taskIncome);
		}
		return taskIncomeMap;
	}

	/**
	 * 通过 用户id 获取全平台的 用户任务收益情况
	 * @param userId
	 * @return taskAward 任务奖金 taskCommission 任务佣金
	 */
	@GetMapping("/feignapi/user/applet/v9/queryTotalTaskIncomeByUserId")
	public Map<String, String> queryTotalTaskIncomeByUserId(@RequestParam("userId") Long userId) {
		if (userId == null || userId < 1) {
			log.error("参数异常");
			return null;
		}

        User user = userRepository.getById(userId);
		if (user == null) {
		    log.error("查无此用户, userId = {}", userId);
		    return null;
        }

        //任务奖金汇总
		BigDecimal taskAward = upayWxOrderService.sumTotalFeeByBizTypes(null, userId, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode(), UpayWxOrderAboutVO.addTaskAward());
		//任务佣金汇总
		BigDecimal taskCommission = upayWxOrderService.sumTotalFeeByBizTypes(null, userId, UpayWxOrderTypeEnum.REBATE.getCode(), UpayWxOrderAboutVO.addTaskCommission());

		Map<String, String> resultVO = new HashMap<>();
		resultVO.put("taskAward", taskAward.toString());
		resultVO.put("taskCommission", taskCommission.toString());
		return resultVO;
	}

    /**
     * 保存或更新用户信息
     *
     * @param userResp
     * @return
     */
    @PostMapping(value = "/feignapi/user/applet/v8/saveOrUpdateUser")
    public boolean saveOrUpdateUser(@RequestBody UserResp userResp) {
        User user = new User();
        BeanUtils.copyProperties(userResp, user);
        return userRepository.saveOrUpdate(user);
    }

    /***
     * 插入用户
     * @param userResp
     * @return
     */
    @PostMapping("/feignapi/user/applet/v5/addUser")
    public Long addUser(@RequestBody UserResp userResp) {
        User user = new User();
        BeanUtils.copyProperties(userResp, user);
        user.setCtime(new Date());
        user.setUtime(new Date());
        int row = userMapper.insertSelective(user);
        if (row == 0) {
            return null;
        }
        return user.getId();
    }

    /***
     * 查询收货地址
     * @param addressId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryAdddress")
    public UserAddressReq queryAdddress(@RequestParam("addressId") Long addressId) {
        UserAddress userAddress = this.userAddressRepository.selectById(addressId);
        if (userAddress == null) {
            return null;
        }

        UserAddressReq resp = new UserAddressReq();
        BeanUtils.copyProperties(userAddress, resp);
        return resp;
    }

    /***
     * 获取用户信息
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryUser")
    public UserResp queryUser(@RequestParam("userId") Long userId) {
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            return null;
        }

        UserResp resp = new UserResp();
        BeanUtils.copyProperties(user, resp);
        return resp;
    }

    /***
     * 获取 用户信息 和 用户收益信息
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryAppletUser")
    public AppletUserDTO queryAppletUser(@RequestParam("userId") Long userId,
                                         @RequestParam("shopId") Long shopId) {

        // 获取 appletUser
        AppletUser appletUser = appletUserRepository.findById(userId);
        if (appletUser == null) {
            log.error("user服务器异常，获取用户信息失败, userId = {}", userId);
            return null;
        }
        appletUserRepository.addUserIncome(appletUser, shopId);

        return appletUser.copyAppletUser();
    }

    /**
     * 批量获取 获取 用户信息 和 用户收益信息
     *
     * @param shopId
     * @param userIds
     * @return
     */
    @GetMapping(value = "/feignapi/user/applet/v5/listAppletUser")
    public List<AppletUserDTO> listAppletUser(@RequestParam("shopId") Long shopId,
                                              @RequestParam("userIds") List<Long> userIds) {

        if (shopId == null || shopId < 1 || CollectionUtils.isEmpty(userIds)) {
            log.error("参数异常, shopId = {}, userIds = {}", shopId, userIds);
            return null;
        }
        List<AppletUser> appletUsers = appletUserRepository.listByIds(userIds);
        if (CollectionUtils.isEmpty(appletUsers)) {
            log.error("user服务器异常，获取用户信息失败, userIds = {}", userIds);
            return null;
        }
        appletUserRepository.listAddUserIncome(appletUsers, shopId);

        List<AppletUserDTO> appletUserDTOList = new ArrayList<>();
        for (AppletUser appletUser : appletUsers) {
            appletUserDTOList.add(appletUser.copyAppletUser());
        }
        return appletUserDTOList;
    }

    /**
     * 批量 获取 用户信息
     *
     * @param userIds
     * @return
     */
    @GetMapping(value = "/feignapi/user/applet/v5/listUser")
    List<UserResp> listUser(@RequestParam("userIds") List<Long> userIds) {

        if (CollectionUtils.isEmpty(userIds)) {
            log.error("user服务器，参数异常, userIds = {}", userIds);
            return null;
        }
        List<AppletUser> appletUsers = appletUserRepository.listByIds(userIds);
        if (CollectionUtils.isEmpty(appletUsers)) {
            log.error("user服务器异常，获取用户信息失败, userIds = {}", userIds);
            return null;
        }

        List<UserResp> userRespList = new ArrayList<>();
        for (AppletUser appletUser : appletUsers) {
            UserResp userResp = new UserResp();
            User userInfo = appletUser.getUserInfo();
            BeanUtils.copyProperties(userInfo, userResp);
            userRespList.add(userResp);
        }
        return userRespList;
    }

    /**
     * 获取所有授权的用户ids
     *
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/selectAllIds")
    public List<Long> selectAllIds() {
        return userMapper.selectAllIds();
    }

    /**
     * 插入记录 并 更新余额
     *
     * @param upayWxOrderReq
     * @return
     */
    @PostMapping("/feignapi/user/applet/v5/updateUserBalance")
    public Integer updateUserBalance(@RequestBody UpayWxOrderReq upayWxOrderReq) {
        if (upayWxOrderReq == null) {
            log.error("参数异常");
            return 0;
        }
        // 创建用户余额流水
        UpayWxOrder upayWxOrder = new UpayWxOrder();
        BeanUtils.copyProperties(upayWxOrderReq, upayWxOrder);
        int insert = upayWxOrderMapper.insertSelective(upayWxOrder);
        if (insert == 0) {
            log.info("触发 订单防重机制 order_no = {}", upayWxOrder.getOrderNo());
            return insert;
        } else {
            Integer rows = upayBalanceService.updateBalance(upayWxOrder.getUserId(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getTotalFee());
            if (rows == 0) {
                log.error("商户金额更新失败：userId = {}, merchantId = {}, shopId = {}", upayWxOrder.getUserId(), upayWxOrder.getMchId(), upayWxOrder.getShopId());
            }
            return rows;
        }
    }

    /**
     * 批量更新用户余额
     *
     * @param upayWxOrderReqList upayWxOrderReqs = List<UpayWxOrderReq>
     *                     upayWxOrders = JSON.toJSONString(upayWxOrderReqs)
     */
    @PostMapping("/feignapi/user/applet/v5/batchUpdateUserBalance")
    public void batchUpdateUserBalance(@RequestBody UpayWxOrderReqList upayWxOrderReqList) {
        if (upayWxOrderReqList == null || CollectionUtils.isEmpty(upayWxOrderReqList.getUpayWxOrderReqList())) {
            log.error("用户余额结算异常，接受到的参数为空");
            return;
        }

        for (UpayWxOrderReq upayWxOrderReq : upayWxOrderReqList.getUpayWxOrderReqList()) {
            // 创建用户余额流水
            UpayWxOrder upayWxOrder = new UpayWxOrder();
            BeanUtils.copyProperties(upayWxOrderReq, upayWxOrder);
            try {
                int insert = upayWxOrderMapper.insertSelective(upayWxOrder);
                if (insert == 0) {
                    log.info("触发 订单防重机制 order_no = {}", upayWxOrder.getOrderNo());
                    continue;
                }
            } catch (Exception e) {
                log.error("触发 订单防重机制 order_no = {}, e = {}", upayWxOrder.getOrderNo(), e);
                continue;
            }

            try {
                int rows = upayBalanceService.updateBalance(upayWxOrder.getUserId(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getTotalFee());
                if (rows == 0) {
                    log.error("用户金额更新失败：userId = {}, merchantId = {}, shopId = {}", upayWxOrder.getUserId(), upayWxOrder.getMchId(), upayWxOrder.getShopId());
                }
            } catch (Exception e) {
                log.error("用户金额更新失败：userId = {}, merchantId = {}, shopId = {}, e = {}", upayWxOrder.getUserId(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), e);
            }

        }
    }

    /**
     * 获取此用户的上级ids
     *
     * @param userId
     * @param vipShopId
     * @param scale     层级数
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryUpperIds")
    public List<Long> queryUpperIds(@RequestParam("userId") Long userId,
                                    @RequestParam("vipShopId") Long vipShopId,
                                    @RequestParam("scale") Integer scale) {
        List<Long> upperIds = new ArrayList<>();
        if (userId == null || vipShopId == null || scale == null) {
            log.error("参数异常");
            return upperIds;
        }
        return userTeamIncomeService.getUpperIds(userId, vipShopId, scale);
    }

    @PostMapping("/feignapi/user/applet/v5/saveUpayWxOrder")
    public int saveUpayWxOrder(@RequestBody UpayWxOrderReqDTO reqDTO) {
        return this.upayWxOrderService.insertSelective(reqDTO);
    }

    /***
     * 商户端获取用户详情和收货地址
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryUserAddress")
    public SimpleResponse queryUserAndAddressForSeller(@RequestParam(value = "userId") Long userId,
                                                       @RequestParam(value = "addressId") Long addressId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("用户不存在");
            return simpleResponse;
        }
        UserAddress userAddress = this.userAddressRepository.selectById(addressId);
        if (userAddress == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("收货地址不存在");
            return simpleResponse;
        }

        SellerUserAddressResultDTO resultDTO = new SellerUserAddressResultDTO();
        resultDTO.setUserId(userId);
        resultDTO.setVipShopId(user.getVipShopId());
        resultDTO.setAddressId(addressId);
        resultDTO.setAvatarUrl(user.getAvatarUrl());
        resultDTO.setNick(user.getNickName());
        resultDTO.setCountry(userAddress.getCountry());
        resultDTO.setProvince(userAddress.getProvince());
        resultDTO.setCity(userAddress.getCity());
        resultDTO.setStreet(userAddress.getStreet());
        resultDTO.setDetail(userAddress.getDetail());
        resultDTO.setUserName(userAddress.getNick());
        resultDTO.setPhone(userAddress.getPhone());
        simpleResponse.setData(resultDTO);
        return simpleResponse;
    }

    /**
     * 获取用户地址信息map
     *
     * @param addressIdList
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryAddressMap")
    public SimpleResponse queryAddressMapForSeller(@RequestParam(value = "addressIdList") List<Long> addressIdList) {
        List<UserAddress> userAddressList = this.userAddressRepository.selectBatchById(addressIdList);
        SimpleResponse simpleResponse = new SimpleResponse();
        if (CollectionUtils.isEmpty(userAddressList)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            return simpleResponse;
        }
        Set<Long> userIdSet = new HashSet<>();
        for (UserAddress userAddress : userAddressList) {
            userIdSet.add(userAddress.getUserId());
        }
        List<User> userList = userRepository.listByIds(new ArrayList<>(userIdSet));
        Map<Long, User> userMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userList)) {
            for (User user : userList) {
                userMap.put(user.getId(), user);
            }
        }
        Map<String, SellerUserAddressResultDTO> resultDTOMap = new HashMap<>();
        for (UserAddress userAddress : userAddressList) {
            SellerUserAddressResultDTO resultDTO = new SellerUserAddressResultDTO();
            resultDTO.setUserId(userAddress.getUserId());
            resultDTO.setAddressId(userAddress.getId());
            resultDTO.setCity(userAddress.getCity());
            resultDTO.setCountry(userAddress.getCountry());
            resultDTO.setProvince(userAddress.getProvince());
            resultDTO.setStreet(userAddress.getStreet());
            resultDTO.setDetail(userAddress.getDetail());
            User user = userMap.get(userAddress.getUserId());
            if (user != null) {
                resultDTO.setNick(user.getNickName());
            }
            resultDTO.setUserName(userAddress.getNick());
            resultDTO.setPhone(userAddress.getPhone());
            resultDTOMap.put(userAddress.getId().toString(), resultDTO);
        }
        simpleResponse.setData(resultDTOMap);
        return simpleResponse;
    }

    /***
     * 商户端获取用户地址信息
     *
     * @param addressIdList
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryAddressList")
    public SimpleResponse queryAddressListForSeller(@RequestParam(value = "addressIdList") List<Long> addressIdList) {
        List<UserAddress> userAddressList = this.userAddressRepository.selectBatchById(addressIdList);
        SimpleResponse simpleResponse = new SimpleResponse();
        if (CollectionUtils.isEmpty(userAddressList)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            return simpleResponse;
        }
        List<SellerUserAddressResultDTO> resultDTOList = new ArrayList<>();
        for (UserAddress userAddress : userAddressList) {
            SellerUserAddressResultDTO resultDTO = new SellerUserAddressResultDTO();
            resultDTO.setUserId(userAddress.getUserId());
            resultDTO.setAddressId(userAddress.getId());
            resultDTO.setCity(userAddress.getCity());
            resultDTO.setCountry(userAddress.getCountry());
            resultDTO.setProvince(userAddress.getProvince());
            resultDTO.setStreet(userAddress.getStreet());
            resultDTO.setDetail(userAddress.getDetail());
            resultDTO.setNick(userAddress.getNick());
            resultDTO.setPhone(userAddress.getPhone());
            resultDTOList.add(resultDTO);
        }
        simpleResponse.setData(resultDTOList);
        return simpleResponse;
    }

    /**
     * 通过买家名模糊查询用户Id列表
     *
     * @param nick
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryAddressIdByAddressNickOrPhoneForSeller")
    public SimpleResponse queryAddressIdByAddressNickOrPhoneForSeller(@RequestParam(value = "nick", required = false) String nick,
                                                                      @RequestParam(value = "phone", required = false) String phone) {
        SimpleResponse simpleResponse = new SimpleResponse();
        QueryWrapper<UserAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().select(UserAddress::getId, UserAddress::getUserId);
        if (StringUtils.isBlank(nick) && StringUtils.isBlank(phone)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            return simpleResponse;
        }
        if (StringUtils.isNotBlank(nick)) {
            queryWrapper.lambda().eq(UserAddress::getNick, nick);
        }
        if (StringUtils.isNotBlank(phone)) {
            queryWrapper.lambda().eq(UserAddress::getPhone, phone);
        }
        Page<UserAddress> page = new Page<>(1, 1);
        IPage<UserAddress> userAddressPage = userAddressRepository.queryPage(page, queryWrapper);
        List<UserAddress> records = userAddressPage.getRecords();

        if (CollectionUtils.isEmpty(records)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            return simpleResponse;
        }
        List<String> addressIdList = new ArrayList<>();
        for (UserAddress userAddress : records) {
            addressIdList.add(userAddress.getId().toString());
        }
        simpleResponse.setData(addressIdList);
        return simpleResponse;
    }

    /**
     * 更新 此店铺下 所有用户余额结算时间
     */
    @RequestMapping("/feignapi/user/applet/v5/updateUserBalanceSettledTime")
    public void updateUserBalanceSettledTime(@RequestParam("shopId") Long shopId) {
        upayBalanceService.updateUserBalanceSettledTime(shopId);
    }

    /**
     * 更新 此用户 在此店铺下 的余额结算时间
     */
    @RequestMapping("/feignapi/user/applet/v7/updateUserBalanceSettledTimeByUserIdShopId")
    public void updateUserBalanceSettledTimeByUserIdShopId(@RequestParam("userId") Long userId,
                                                           @RequestParam("shopId") Long shopId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("shopId", shopId);
        params.put("updateTime", new Date());

        upayBalanceMapper.updateUserBalanceSettledTime(params);
    }

    /**
     * 获取 当前商城的用户的所属上级成员
     *
     * @param vipShopId
     * @param userId
     * @param scale
     * @return JSON.toJSONString(Map < level, UserId >)
     */
    @RequestMapping("/feignapi/user/applet/v5/queryUpperMember")
    public Map<Integer, Long> queryUpperMember(@RequestParam("vipShopId") Long vipShopId,
                                               @RequestParam("userId") Long userId,
                                               @RequestParam("scale") Integer scale) {
        if (vipShopId == null || userId == null || scale == null) {
            return null;
        }
        return userTeamIncomeService.queryUpperMember(vipShopId, userId, scale);
    }

    /**
     * 获取 当前商城的用户的所属上级成员
     *
     * @param vipShopId
     * @param userId
     * @param scale
     * @return JSON.toJSONString(Map < level, List < UserId > >)
     */
    @RequestMapping("/feignapi/user/applet/v5/queryLowerMember")
    public Map<Integer, List<Long>> queryLowerMember(@RequestParam(value = "vipShopId", required = false) Long vipShopId,
                                                     @RequestParam(value = "userId", required = false) Long userId,
                                                     @RequestParam(value = "scale", required = false) Integer scale) {
        if (vipShopId == null || userId == null || scale == null) {
            return null;
        }
        return userTeamIncomeService.queryLowerMember(vipShopId, userId, scale);
    }

    /**
     * 获取 商城用户 最近一次财务更新时间
     *
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryUpayBalanceLastUpdateTime")
    public String queryUpayBalanceLastUpdateTime(@RequestParam("shopId") Long shopId,
                                                 @RequestParam("userId") Long userId) {
        if (shopId == null || shopId < 1 || userId == null || userId < 1) {
            log.error("参数异常, shopId = {}, userId = {}", shopId, userId);
            return null;
        }
        // 获取用户最近一次财务更新时间
        QueryWrapper<UpayBalance> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayBalance::getUserId, userId)
                .eq(UpayBalance::getShopId, shopId);
        UpayBalance upayBalance = upayBalanceService.getOne(wrapper);
        if (upayBalance == null) {
            log.error("数据库异常，获取用户余额失败, shopId = {}, userId = {}", shopId, userId);
            return null;
        }
        return DateUtil.dateToString(upayBalance.getSettledTime(), DateUtil.FORMAT_ONE);
    }

    /**
     * 获取单个用户推广到现在 所有已结算收益 -- 返利+贡献
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryTotalSettledIncome")
    public String sumTotalFeeSettledByUserId(@RequestParam("shopId") Long shopId,
                                             @RequestParam("userId") Long userId) {
        if (shopId == null || shopId < 1 || userId == null || userId < 1) {
            log.error("参数异常, shopId = {}, userId = {}", shopId, userId);
            return null;
        }
        List<Integer> bizTypes = new ArrayList<>();
        bizTypes.add(UpayWxOrderBizTypeEnum.REBATE.getCode());
        bizTypes.add(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());

        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("shopId", shopId);
        params.put("type", UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("bizTypes", bizTypes);
        BigDecimal totalSettledIncome = upayWxOrderMapper.sumTotalFeeByBizType(params);
        if (totalSettledIncome == null) {
            return null;
        }
        return totalSettledIncome.toString();
    }

    /**
     * 批量获取 用户们 累计已结算总额 -- 返利及贡献
     *
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/batchSumTotalSettledIncome")
    public List<Map> batchSumTotalFeeSettledByUserIds(@RequestParam("sid") Long sid,
                                                      @RequestParam("userIdList") List<Long> userIdList) {
        if (sid == null || sid < 1 || CollectionUtils.isEmpty(userIdList)) {
            log.error("参数异常， sid = {}, userIdList", sid, userIdList);
            return null;
        }
        List<Integer> bizTypes = new ArrayList<>();
        bizTypes.add(UpayWxOrderBizTypeEnum.REBATE.getCode());
        bizTypes.add(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());

        Map<String, Object> params = new HashMap<>();
        params.put("userIds", userIdList);
        params.put("shopId", sid);
        params.put("type", UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("bizTypes", bizTypes);

        return upayWxOrderMapper.batchSumTotalFeeSettledByUserIds(params);
    }

    /**
     * 分批获取 此用户 的团队 -从最初贡献开始-到现在的所有已结算贡献总金额 -- 贡献
     *
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/batchTotalContributionSettledByUserIds")
    public List<Map> batchTotalContributionSettledByUserIds(@RequestParam("shopId") Long shopId,
                                                            @RequestParam("userIdList") List<Long> userIdList) {
        if (shopId == null || shopId < 1 || CollectionUtils.isEmpty(userIdList)) {
            log.error("参数异常， sid = {}, userIdList", shopId, userIdList);
            return null;
        }

        Map<String, Object> params = new HashMap<>();
        // params.put("shopId", shopId); 2.1 迭代 累计所有店铺的待结算贡献
        params.put("type", UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
        params.put("bizType", UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("bizValues", userIdList);

        return upayWxOrderMapper.batchTotalContributionSettledByUserIds(params);

    }

    @PostMapping("/feignapi/user/applet/v5/saveAddress")
    public String saveAddress(@RequestBody UserAddressReq req) {
        UserAddress userAddress = new UserAddress();
        BeanUtils.copyProperties(req, userAddress);
        userAddress.setNick(req.getNick());
        userAddressRepository.insertSelective(userAddress);
        req.setId(userAddress.getId());

        return JSONObject.toJSONString(req);
    }

    /**
     * 查询一级下属的店长数
     *
     * @param userIdListDTO
     * @return
     */
    @PostMapping("/feignapi/user/applet/v5/countShopManagerFirstLevel")
    public Long countShopManagerByUserIdList(@RequestBody UserIdListDTO userIdListDTO) {
        List<Long> userIdList = userIdListDTO.getUserIdList();
        if (CollectionUtils.isEmpty(userIdList)) {
            return 0L;
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(User::getId, userIdList)
                .eq(User::getRole, UserRoleEnum.SHOP_MANAGER.getCode());
        return (long) userRepository.count(queryWrapper);
    }

    /**
     * 20日结算结束，更新所有用户的结算时间
     * @return
     */
    @GetMapping("/feignapi/user/applet/v9/updateUpayBalanceSettledTimeByAll")
    public void updateUpayBalanceSettledTimeByAll() {
        Map<String, Object> params = new HashMap<>();
        params.put("updateTime", new Date());
        upayBalanceMapper.updateUserBalanceSettledTime(params);
    }
}
