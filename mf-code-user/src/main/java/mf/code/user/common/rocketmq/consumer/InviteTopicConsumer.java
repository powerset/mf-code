package mf.code.user.common.rocketmq.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.domain.aggregateroot.AppletActivity;
import mf.code.activity.repo.repository.AppletActivityRepository;
import mf.code.common.RedisKeyConstant;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.exception.RocketMQException;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.one.dto.ActivityDefDTO;
import mf.code.one.dto.UserCouponDTO;
import mf.code.one.dto.UserTaskDTO;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.domain.aggregateroot.InvitationEvent;
import mf.code.user.domain.valueobject.specification.*;
import mf.code.user.dto.UserTaskIncomeResp;
import mf.code.user.repo.dao.UserPubJoinMapper;
import mf.code.user.repo.po.UserPubJoin;
import mf.code.user.repo.repository.AppletUserRepository;
import mf.code.user.repo.repository.InvitationEventRepository;
import mf.code.user.repo.repository.InvitationFactory;
import mf.code.user.service.AsyncWxTemplateMsgService;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.common.rocketmq.consumer
 * Description:
 *
 * @author gel
 * @date 2019-05-06 19:04
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "invite", consumerGroup = "invite-consumer")
public class InviteTopicConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {

    @Autowired
    private AppletUserRepository appletUserRepository;
    @Autowired
    private AppletActivityRepository appletActivityRepository;
    @Autowired
    private InvitationFactory invitationFactory;
    @Autowired
    private InvitationEventRepository invitationEventRepository;
    @Autowired
    private AsyncWxTemplateMsgService asyncWxTemplateMsgService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private UserPubJoinMapper userPubJoinMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void onMessage(MessageExt message) {
        String bizValue = new String(message.getBody(), Charset.forName("UTF-8"));
        String msgId = message.getMsgId();
        String tags = message.getTags();
        // if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.CATCH_MINER.getTag(), tags)) {
        // 	catchMiner(msgId, bizValue);
        // }
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.SHOPPING_TEAM.getTag(), tags)) {
            shoppingTeam(msgId, bizValue);
        }
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.INVITE_ACTIVITY.getTag(), tags)) {
            inviteActivity(msgId, bizValue);
        }
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        // set consumer consume message from now
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }


    /**
     * 商城团队
     *
     * @param msgId
     * @param bizValue
     */
    public void shoppingTeam(String msgId, String bizValue) {
        log.info("商城消息消费成功" + bizValue);
        JSONObject jsonObject = JSON.parseObject(bizValue);
        String pubUserId = jsonObject.getString("pubUserId");
        String userId = jsonObject.getString("userId");
        String activityId = jsonObject.getString("activityId");

        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)) {
            log.error("参数异常，非商城团队业务消费, userId = {}", userId);
            return;
        }
        // 获取 用户信息
        AppletUser appletSubUser = appletUserRepository.findById(Long.valueOf(userId));
        if (appletSubUser == null) {
            log.error("获取用户信息异常, 为排除服务异常，延迟消费, userId = {}", userId);
            throw new RocketMQException();
        }
        AppletUser appletPubUser;
        if (StringUtils.isBlank(pubUserId) || !RegexUtils.StringIsNumber(pubUserId)) {
            appletPubUser = appletUserRepository.findPlatform();
        } else {
            appletPubUser = appletUserRepository.findById(Long.valueOf(pubUserId));
        }
        if (appletPubUser == null) {
            log.error("获取用户信息异常, 为排除服务异常，延迟消费, pubUserId = {}", pubUserId);
            throw new RocketMQException();
        }
        InvitationSpecification invitationSpecification = new InviteShoppingTeamSpecification(appletSubUser, InvitationDimensionEnum.PLATFORM);

        boolean firstLogin = false;
        // 与邀请者 建立团队关系
        InvitationEvent invitationEvent = invitationFactory.newInvitationEvent(appletPubUser, appletSubUser, invitationSpecification);
        if (invitationEvent == null) {
            log.info("已有商城团队");
        } else {
            Boolean saveOrUpdate = invitationEventRepository.saveOrUpdate(invitationEvent);
            if (!saveOrUpdate) {
                log.error("数据库异常，为排除服务异常，延迟消费, pubUserId = {}, userId = {}", pubUserId, userId);
                throw new RocketMQException();
            }
            firstLogin = true;
        }
        //登录时异步推送消息
        asyncWxTemplateMsgService.wxtemplateMsg(appletSubUser, pubUserId, firstLogin);

        // 2.4迭代 全额报销活动 获取助力资格
        log.info("<<<<<<<<上下级从属关系，可助力活动1 pubUserId:{}, userid:{} activityId:{}", pubUserId,
                userId, activityId);
        if (invitationEvent != null && StringUtils.isNotBlank(pubUserId) && RegexUtils.StringIsNumber(pubUserId)
                && StringUtils.isNotBlank(activityId) && RegexUtils.StringIsNumber(activityId)) {
            AppletActivity appletActivity = appletActivityRepository.findById(activityId);
            log.info("<<<<<<<<上下级从属关系，可助力活动2 appletActivity:{}", appletActivity);
            if (appletActivity.getActivityInfo().getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                // 获取 此次邀请的规则
                InvitationSpecification activityIS = new ActivityInvitationSpecification(appletActivity.getActivityInfo());

                // 建立邀请关系
                InvitationEvent activityIE = invitationFactory.newInvitationEvent(appletPubUser, appletSubUser, activityIS);
                if (activityIE == null) {
                    log.info("已有活动邀请");
                    return;
                }
                // 持久化数据
                Boolean saveOrUpdate = invitationEventRepository.saveOrUpdate(activityIE);
                if (!saveOrUpdate) {
                    log.error("数据库异常，为排除服务异常，延迟消费, pubUserId = {}, userId = {}", pubUserId, userId);
                    throw new RocketMQException();
                }
                log.info("<<<<<<<<上下级从属关系，可助力活动3 userid:{}", userId);
                stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_ASSISTABLE_KEY + activityId + ":" + userId, "1");
            }
        }

        // 店长任务 邀请粉丝任务
        if (StringUtils.isBlank(pubUserId) || !RegexUtils.StringIsNumber(pubUserId)) {
            return;
        }
        UserTaskIncomeResp userTaskIncomeResp = oneAppService.queryUserTaskIncome(Long.valueOf(pubUserId));
        if (userTaskIncomeResp == null) {
            log.error("获取店长任务异常");
            return;
        }
        if (CollectionUtils.isEmpty(userTaskIncomeResp.getUserTaskMap())) {
            log.info("没有领取店长任务");
            return;
        }
        UserTaskDTO userTaskDTO = new UserTaskDTO();
        for (UserTaskDTO userTask : userTaskIncomeResp.getUserTaskMap().values()) {
            userTaskDTO = userTask;
        }
        Map<Long, UserCouponDTO> userCouponMap = userTaskIncomeResp.getUserCouponMap();
        if (!CollectionUtils.isEmpty(userCouponMap)) {
            for (UserCouponDTO userCouponDTO : userCouponMap.values()) {
                if (userCouponDTO.getType() == UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode()) {
                    log.info("已完成店长 邀请粉丝任务");
                    return;
                }
            }
        }
        QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.SHOPPINGMALL.getCode())
                .eq(UserPubJoin::getUserId, pubUserId)
                .ge(UserPubJoin::getCtime, userTaskDTO.getCtime());
        Integer size = userPubJoinMapper.selectCount(wrapper);

        ActivityDefDTO activityDefDTO = oneAppService.findShopManagerInviteFansTask(userTaskDTO.getActivityDefId());
        // 2.6.1 迭代 当活动定义被挂起时，用户无法得到奖励
        if (activityDefDTO == null) {
            log.info("邀请粉丝的 活动定义被挂起，无法得到奖励");
            return;
        }
        if (size >= activityDefDTO.getCondPersionCount()) {
            String amount = oneAppService.giveAwardInviteFans(Long.valueOf(pubUserId), userTaskDTO.getActivityDefId());
            int inviteNum = activityDefDTO.getStartNum();
            Map<String, Object> map = new HashMap<>();
            map.put("amount", amount);
            map.put("inviteNum", inviteNum);
            if (StringUtils.isNotBlank(amount)) {
                stringRedisTemplate.opsForValue().set(RedisKeyConstant.SHOP_MANAGER_INVITE_FANS_AWARD + pubUserId, JSONObject.toJSONString(map));
            }
        }
    }

    /**
     * 活动邀请
     *
     * @param msgId
     * @param bizValue
     */
    private void inviteActivity(String msgId, String bizValue) {
        log.info("活动邀请消息消费成功" + bizValue);
        JSONObject jsonObject = JSON.parseObject(bizValue);
        String pubUserId = jsonObject.getString("pubUserId");
        String userId = jsonObject.getString("userId");
        String activityId = jsonObject.getString("activityId");

        if (StringUtils.isBlank(pubUserId) || !RegexUtils.StringIsNumber(pubUserId)
                || StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(activityId) || !RegexUtils.StringIsNumber(activityId)) {
            log.info("参数异常，非活动邀请业务");
            return;
        }
        // 获取 发起邀请的活动
        AppletActivity appletActivity = appletActivityRepository.findById(activityId);
        if (appletActivity == null) {
            log.error("获取用户信息异常, 为排除服务异常，延迟消费, activityId = {}", activityId);
            throw new RocketMQException();
        }

        // 平台唯一类活动邀请，放置商城团队邀请中处理
        if (appletActivity.getActivityInfo().getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            return;
        }

        // 获取 用户信息
        AppletUser appletSubUser = appletUserRepository.findById(Long.valueOf(userId));
        if (appletSubUser == null) {
            log.error("获取用户信息异常, 为排除服务异常，延迟消费, userId = {}", userId);
            throw new RocketMQException();
        }
        AppletUser appletPubUser = appletUserRepository.findById(Long.valueOf(pubUserId));
        if (appletPubUser == null) {
            log.error("获取用户信息异常, 为排除服务异常，延迟消费, userId = {}", pubUserId);
            throw new RocketMQException();
        }

        // 获取 此次邀请的规则
        InvitationSpecification invitationSpecification = new ActivityInvitationSpecification(appletActivity.getActivityInfo());

        // 建立邀请关系
        InvitationEvent invitationEvent = invitationFactory.newInvitationEvent(appletPubUser, appletSubUser, invitationSpecification);
        if (invitationEvent == null) {
            log.info("已有活动邀请");
            return;
        }
        // 持久化数据
        Boolean saveOrUpdate = invitationEventRepository.saveOrUpdate(invitationEvent);
        if (!saveOrUpdate) {
            log.error("数据库异常，为排除服务异常，延迟消费, pubUserId = {}, userId = {}", pubUserId, userId);
            throw new RocketMQException();
        }
    }


    // /**
    //  * 抓矿工
    //  *
    //  * @param msgId
    //  * @param bizValue
    //  */
    // private void catchMiner(String msgId, String bizValue) {
    // 	log.info("抓矿工消息消费成功" + bizValue);
    // 	JSONObject jsonObject = JSON.parseObject(bizValue);
    // 	String pubUserId = jsonObject.getString("pubUserId");
    // 	String userId = jsonObject.getString("userId");
    //
    // 	if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)) {
    // 		log.info("参数异常，非抓矿工业务");
    // 		return;
    // 	}
    // 	// 获取 用户信息
    // 	AppletUser appletSubUser = appletUserRepository.findById(Long.valueOf(userId));
    // 	if (appletSubUser == null) {
    // 		log.error("获取用户信息异常, 为排除服务异常，延迟消费, userId = {}", userId);
    // 		throw new RocketMQException();
    // 	}
    // 	AppletUser appletPubUser;
    // 	if (StringUtils.isBlank(pubUserId) || !RegexUtils.StringIsNumber(pubUserId)) {
    // 		appletPubUser = appletUserRepository.findPlatform();
    // 	} else {
    // 		appletPubUser = appletUserRepository.findById(Long.valueOf(pubUserId));
    // 	}
    // 	if (appletPubUser == null) {
    // 		log.error("获取用户信息异常, 为排除服务异常，延迟消费, pubUserId = {}", pubUserId);
    // 		throw new RocketMQException();
    // 	}
    // 	InvitationSpecification invitationSpecification = new InviteShoppingTeamSpecification(appletSubUser, InvitationDimensionEnum.PLATFORM);
    // 	// 转化成财富大闯关邀请类型
    // 	invitationSpecification.setUserPubJoinType(UserPubJoinTypeEnum.CHECKPOINTS.getCode());
    //
    // 	// 与邀请者 建立财富大闯关 关系
    // 	InvitationEvent invitationEvent = invitationFactory.newInvitationEvent(appletPubUser, appletSubUser, invitationSpecification);
    // 	if (invitationEvent == null) {
    // 		log.info("已有成为他人矿工");
    // 		return;
    // 	}
    // 	Boolean saveOrUpdate = invitationEventRepository.saveOrUpdate(invitationEvent);
    // 	if (!saveOrUpdate) {
    // 		log.error("数据库异常，为排除服务异常，延迟消费, pubUserId = {}, userId = {}", pubUserId, userId);
    // 		throw new RocketMQException();
    // 	}
    // }

}
