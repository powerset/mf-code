package mf.code.user.common.caller.wxmp;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.WeixinMpConstants;
import mf.code.common.utils.BufferedImageUtil;
import mf.code.common.utils.HttpUtil;
import mf.code.common.utils.HttpsUtil;
import mf.code.user.common.caller.aliyunoss.OssCaller;
import mf.code.user.common.caller.wxmp.vo.WxsendCustomerMsgVo;
import mf.code.user.common.property.AppProperty;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月25日 11:42
 */
@Service
@Slf4j
public class WeixinMpServiceImpl implements WeixinMpService {
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AppProperty wxpayProperty;

    @Override
    public String getAccessToken(String appid, String appSecret) {
        String accessToken = null;
        boolean mfappid = appid.equals(wxpayProperty.getMfAppId());
        boolean teacherappid = appid.equals(wxpayProperty.getTeacherAppId());
        if (mfappid && StringUtils.isNotBlank(this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.APPLET_ACCESS_TOKEN))) {
            accessToken = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.APPLET_ACCESS_TOKEN);
            return accessToken;
        }
        if (teacherappid && StringUtils.isNotBlank(this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.TEACHER_ACCESS_TOKEN))) {
            accessToken = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.TEACHER_ACCESS_TOKEN);
            return accessToken;
        }

        String param = "grant_type=client_credential&appid=" + appid + "&secret=" + appSecret;
        JSONObject obj = JSONObject.parseObject(HttpUtil.sendGet(WeixinMpConstants.DOMAIN_ACCESS_TOKEN, param));
        if (obj != null && StringUtils.isNotBlank(obj.getString("access_token"))) {
            if (mfappid) {
                this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.APPLET_ACCESS_TOKEN, obj.getString("access_token"), 7000, TimeUnit.SECONDS);
            }
            if (teacherappid) {
                this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.TEACHER_ACCESS_TOKEN, obj.getString("access_token"), 7000, TimeUnit.SECONDS);
            }
            accessToken = obj.getString("access_token");
            return accessToken;
        }
        log.error("获取acctoken失败：{}", obj);
        return null;
    }

    /**
     * 生成二维码并上传oss
     *
     * @param scene 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
     * @param width
     * @return
     */
    @Override
    public String getWXACodeUnlimit(String appid, String appSecret, String scene, String path, Integer width, boolean isHyaline) {
        String ossUrl = "";
        String accessToken = this.getAccessToken(appid, appSecret);
        if (StringUtils.isBlank(accessToken)) {
            log.error("accesstoken 为空：{}", accessToken);
            return null;
        }
        try {
            if (this.generaterCodePo(appid, null, accessToken, scene, width, isHyaline) == null) {
                log.error("Exception {}", "数据流未获到");
                return null;
            }
            BufferedInputStream bis = new BufferedInputStream(this.generaterCodePo(appid, null, accessToken, scene, width, isHyaline));
            byte[] arr = new byte[4096];
            int len;

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            while ((len = bis.read(arr)) != -1) {
                bos.write(arr, 0, len);
            }
            byte[] bytes2 = bos.toByteArray();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes2);
            //上传aliyunoss
            String bucket = path + scene + ".png";
            ossUrl = this.ossCaller.uploadObject2OSSInputstream(inputStream, bucket);
            log.info("微信小程序码上传到oss:{}", ossUrl);
        } catch (Exception e) {
            log.error("Exception {}", e);
        }
        return ossUrl;
    }

    /***
     * 生成圆形的小程序码的bufferImg
     * @param scene
     * @param path
     * @param width
     * @return
     */
    @Override
    public BufferedImage getRoundnessWXACodeUnlimit(String appid, String appSecret, String scene, String path, Integer width) {
        String accessToken = this.getAccessToken(appid, appSecret);
        if (StringUtils.isBlank(accessToken)) {
            log.error("accesstoken 为空：{}", accessToken);
            return null;
        }

        BufferedImage image = null;
        ByteArrayInputStream inputStream = null;
        BufferedInputStream bis = null;
        try {
            if (this.generaterCodePo(appid, null, accessToken, scene, width, false) == null) {
                log.error("Exception {}", "数据流未获到");
                return null;
            }
            bis = new BufferedInputStream(this.generaterCodePo(appid, null, accessToken, scene, width, false));
            byte[] arr = new byte[4096];
            int len;

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            while ((len = bis.read(arr)) != -1) {
                bos.write(arr, 0, len);
            }
            byte[] bytes2 = bos.toByteArray();
            inputStream = new ByteArrayInputStream(bytes2);

            //处理图片将其压缩成圆方形的小图
            BufferedImage convertImage = BufferedImageUtil.appletImageFinalImage(inputStream);
            image = BufferedImageUtil.getAppletRoundImage(convertImage);
        } catch (Exception e) {
            log.error("Exception {}", e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (bis != null) {
                    bis.close();
                }
            } catch (IOException e) {
                log.error("IOException {}", e);
            }
        }
        return image;
    }

    private InputStream generaterCodePo(String appId, String pageScene, String accessToken, String scene, Integer width, boolean isHyaline) {
        JSONObject paramJson = new JSONObject();
        try {
            String param = "access_token=" + accessToken;
            URL url = new URL(WeixinMpConstants.DOMAIN_WX_ACODE_UNLIMIT + "?" + param);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            // 提交模式
            httpURLConnection.setRequestMethod("POST");
            // conn.setConnectTimeout(10000);//连接超时 单位毫秒
            // conn.setReadTimeout(2000);//读取超时 单位毫秒
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            //最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
            paramJson.put("scene", scene);

            paramJson.put("page", this.getPages(appId, pageScene));
            paramJson.put("width", 430);
            if (width != null && width > 0) {
                paramJson.put("width", width);
            }
            //      paramJson.put("auto_color", true);//自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false
            //      line_color生效
            paramJson.put("auto_color", false);
            JSONObject lineColor = new JSONObject();
            lineColor.put("r", 255);
            lineColor.put("g", 255);
            lineColor.put("b", 255);
            //auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示，默认全 0
            paramJson.put("line_color", lineColor);
            if (isHyaline) {
                paramJson.put("is_hyaline", true);//是否需要透明底色，为 true 时，生成透明底色的小程序码，默认 false
            }
            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            return httpURLConnection.getInputStream();
        } catch (Exception e) {
            log.error("Exception {}", e);
        }
        return null;
    }

    /***
     * 获取具体的业务落地页
     * @param appId
     * @param scene
     * @return
     */
    private String getPages(String appId, String scene) {
        String page = "";
        boolean mfappid = appId.equals(wxpayProperty.getMfAppId());
        boolean teacherappid = appId.equals(wxpayProperty.getTeacherAppId());
        if (mfappid) {
            page = WeixinMpConstants.PAGE;
        }
        if (teacherappid) {
            page = WeixinMpConstants.PAGE_TEACHER_INIT;
        }
        return page;
    }

    /***
     * 把媒体文件上传到微信服务器。目前仅支持图片。用于发送客服消息或被动回复用户消息。
     * @param file
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> uploadTempMedia(File file, String type) {
        return null;
    }

    /***
     * 发送客服消息给用户
     * @param vo 客服消息模板
     * @return
     */
    @Override
    public Map<String, Object> sendCustomerMessage(String appid, String appSecret, WxsendCustomerMsgVo vo) {
        String accessToken = this.getAccessToken(appid, appSecret);
        if (StringUtils.isBlank(accessToken)) {
            log.error("accesstoken 为空：{}", accessToken);
            return null;
        }
        String accessTokenParam = "access_token=" + accessToken;
        String url = WeixinMpConstants.DOMAIN_WX_SENDCUSTOMERMESSAGE + "?" + accessTokenParam;
        String voStrParams = JSONObject.toJSONString(vo);

        return this.sendMessage(url, voStrParams);
    }


    @Override
    public Map<String, Object> sendMessage(String url, String params) {
        String resp = "";
        try {
            resp = HttpsUtil.doPost(url, params, null, null, 10000, 2000);
            log.info("send template msg:{} jsonParams:{}", resp, params);
        } catch (Exception e) {
            log.error("Exception {}", e);
        }

        Map<String, Object> respMap = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(resp)) {
            JSONObject jsonObject = JSONObject.parseObject(resp);
            respMap = JSONObject.toJavaObject(jsonObject, Map.class);
        }
        return respMap;
    }
}
