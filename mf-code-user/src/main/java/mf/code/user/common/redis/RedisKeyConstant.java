package mf.code.user.common.redis;

/**
 * mf.code.user.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 00:13
 */
public class RedisKeyConstant {

	/**
	 * 抖小铺 名称缓存
	 */
	public static final String DOU_YIN_APP_SELLER_NAME="app:douyin:seller:name";
	/**
	 * 用户信息缓存
	 * 10秒
	 */
	public static final String CACHE_USER = "jkmf:user:cache:"; //<uid>

	/**
	 * 用户收益数据缓存
	 * 3秒
	 */
	public static final String CACHE_USER_INCOME = "jkmf:user:cache:income:"; //<uid>:<sid>

	/**
	 * 小程序登录 通过openid 对用户增删该查时 进行防重处理
	 */
	public static final String MF_APPLETUSER_LOGIN_FORBID_REPEAT = "jkmf:user:login:forbid:repeat:";//<openid>

	/**
	 * 推送点击率统计
	 */
	public static final String RECALL_PUSH_STATISTICS="mf:merchant:recall:push:";//<shopId>:<pushId>:{userId1,userId2......}

	/**
	 * 数据字典键
	 * key--> common:dict:<type>:<key>
	 * value--> 1
	 */
	public static final String COMMON_DICT = "common:dict:";

	/**
	 * 当前店铺下 所有用户的收益排行榜
	 */
	public static final String INCOME_RANK_LIST_OF_SHOP = "jkmf:user:rank:income:shop:temp:"; //<shopId>

	/**
	 * 获取当前店铺下 用户好友收益排行榜(临时-score:0)
	 */
	public static final String INCOME_RANK_LIST_OF_FRIENDS_TEMP = "jkmf:user:rank:income:friend:temp:"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 用户好友收益排行榜(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_FRIENDS = "jkmf:user:rank:income:friend:"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 团队贡献榜 已结算(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_TEAM_SETTLED = "jkmf:user:rank:income:team:settled"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 团队贡献榜 已结算(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_TEAM_PENDING = "jkmf:user:rank:income:team:pending"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 团队贡献榜 已结算(真实score)
	 */
	public static final String INCOME_RANK_LIST_OF_TEAM_TOTAL = "jkmf:user:rank:income:team:total"; //<shopId>:<userId>

	/**
	 * 获取当前店铺下 用户最近一次财务更新时间
	 */
	public static final String INCOME_LAST_UPDATE_TIME = "jkmf:user:income:update:time:"; //<shopId>:<userId>

	/**
	 * 用户提现key
	 */
	public static final String USERPRESENTMONEY = "user:presentMoney:";//<uid>
	/**
	 * 用户收益数据缓存
	 * 3秒
	 */
	public static final String CACHE_USER_ORDER = "jkmf:user:cache:order:"; //<uid>:<sid>
	/**
	 * 粉丝店铺弹窗
	 */
	public static final String USER_VIPSHOP_POPUP = "jkmf:user:vipshop:";//<shopId>:<userId>


	/***
	 * 登录用户的推送redis
	 * key:jkmf:user:login:template:<uid>
	 * value:"0"
	 * timeout:
	 */
	public static final String USER_LOGIN_TEMPLATEMSG = "jkmf:user:login:template:";

	/***
	 * 登录用户的推送防重redis
	 * key:jkmf:user:login:template:repeat:<uid>
	 * value:"0"
	 * timeout:
	 */
	public static final String USER_LOGIN_TEMPLATEMSG_REPEAT = "jkmf:user:login:template:repeat:";

	/***
	 * 登录用户的队列通知防重
	 * key:jkmf:user:login:bizlog:repeat:<uid>
	 * value:"0"
	 * timeout:自然日
	 */
	public static final String USER_LOGIN_BIZ_LOG_REPEAT = "jkmf:user:login:bizlog:repeat:";
}
