package mf.code.user.common.config.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.IllegalSQLInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.extension.plugins.SqlExplainInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * mf.code.user.common.config.mybatisplus
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-30 15:07
 */
@Configuration
@Profile(value = {"dev", "test2", "test3"})
public class MybatisPlusConfigTest {

	/**
	 * <p>
	 * 性能分析拦截器，用于输出每条 SQL 语句及其执行时间
	 * </p>
	 */
	@Bean
	public PerformanceInterceptor performanceInterceptor() {
		return new PerformanceInterceptor();
	}

	/**
	 * <p>
	 * 防止全表更新与删除
	 * </p>
	 */
	@Bean
	public SqlExplainInterceptor sqlExplainInterceptor() {
		return new SqlExplainInterceptor();
	}
}
