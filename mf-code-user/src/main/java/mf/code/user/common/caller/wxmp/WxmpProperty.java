package mf.code.user.common.caller.wxmp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "tmplatemessage")
@Data
@Component
public class WxmpProperty {
    //邀请结果提醒(发生时间、温馨提示、备注)
    private String inviteresultMsgTmpId;
    //新客户访问提醒(昵称,访问时间,温馨提示)
    private String newcustomervisitMsgTmpId;

    private String formIdRedisKey;

    public String getRedisKey(Long uid) {
        return this.formIdRedisKey.replace("A", uid.toString());
    }
}
