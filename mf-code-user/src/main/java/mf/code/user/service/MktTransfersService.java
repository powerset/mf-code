package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.user.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月14日 21:40
 */
public interface MktTransfersService {
    /***
     * 是否有带解锁金额，是否有下单记录，是否有未支付商品订单
     * @param userID
     * @param shopId
     * @return
     */
    SimpleResponse checkLockMoney(Long userID, Long shopId, Long merchantId);

    /***
     * 提现成功后的控制弹窗
     * <pre>
     *
     * </pre>
     * @param shopID
     * @param userID
     * @return
     */
    SimpleResponse queryMktTransferSuccessDialog(Long merchantId, Long shopID, Long userID, Integer source);
}
