package mf.code.user.service.impl;/**
 * create by qc on 2019/8/9 0009
 */

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserInfoService;
import org.springframework.stereotype.Service;

/**
 * @author gbf
 * 2019/8/9 0009、11:43
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserInfoServiceImpl extends ServiceImpl<UserMapper, User> implements UserInfoService {
}
