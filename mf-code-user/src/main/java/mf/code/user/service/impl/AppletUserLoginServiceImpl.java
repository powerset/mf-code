package mf.code.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.common.utils.TokenUtil;
import mf.code.statistics.constant.MongoUserActionScene;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.statistics.po.FissionUser;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.common.property.AppProperty;
import mf.code.user.common.redis.RedisForbidRepeat;
import mf.code.user.common.redis.RedisKeyConstant;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.domain.factory.AppletUserFactory;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.AppletUserRepository;
import mf.code.user.service.AppletUserLoginService;
import mf.code.user.service.FansV3Service;
import mf.code.user.service.RecallV3Service;
import mf.code.user.service.UserVerifyService;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-29 21:22
 */
@Slf4j
@Service
public class AppletUserLoginServiceImpl implements AppletUserLoginService {
    @Autowired
    private AppletUserRepository appletUserRepository;
    @Autowired
    private UserVerifyService userVerifyService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private RecallV3Service recallV3Service;
    @Autowired
    private FansV3Service fansV3Service;
    @Autowired(required = false)
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private AppProperty appProperty;
    @Autowired
    private OneAppService oneAppService;

    /**
     * 微信用户 登录程序 v7
     * 1.通过微信验证用户有效性
     * 2.通过openId查询是否存在该用户
     * 2.1如果没有则判断用户授权情况，授权则创建
     * 2.2如果有该用户，则更新用户信息，有个埋点
     * 3.判断是否新用户，使用vipShop判断，并赋值保存
     * 4.建立邀请关系
     * 5.生成token
     * 6.处理弹窗
     *
     * @param wxUserSession
     * @return
     */
    @Override
    public SimpleResponse login(WxUserSession wxUserSession) {
        // 校验session 有效性
        SimpleResponse verifyResult = userVerifyService.loginVerify(wxUserSession, appProperty);
        if (verifyResult.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            log.error("登录校验异常");
            return verifyResult;
        }
        JSONObject verifyResultData = JSON.parseObject(verifyResult.getData().toString());
        // 获取用户信息
        String openid = verifyResultData.getString("openid");
        JSONObject userInfoJson = verifyResultData.getJSONObject("userInfo");
        // 加锁
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openid);
        if (!success) {
            log.error("redis 登录防重, openid = {}", openid);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "正在登录中……");
        }
        // 获取用户
        AppletUser appletUser = appletUserRepository.findByOpenid(openid);
        boolean isNewbie = false;
        if (appletUser == null) {
            if (userInfoJson == null) {
                log.info("用户未授权");
                stringRedisTemplate.delete(RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openid);
                // 获取用户登录信息
                Map<String, Object> userLoginInfo = new HashMap<>();

                if (StringUtils.isBlank(wxUserSession.getScene()) || !RegexUtils.StringIsNumber(wxUserSession.getScene())) {
                    userLoginInfo.put("scene", "");
                } else {
                    userLoginInfo.put("scene", wxUserSession.getScene());
                }
                Map<String, Object> shopInfo = shopAppService.queryShopInfoByLogin("0");
                if (CollectionUtils.isEmpty(shopInfo)) {
                    log.error("网络异常，请稍候重试");
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "网络异常，请稍候重试");
                }
                userLoginInfo.putAll(shopInfo);
                /** 集客魔方-尾货清仓版 加入版本信息*/
                userLoginInfo.put("purchaseVersion", shopInfo.get("purchaseVersion"));
                /** 集客魔方-尾货清仓版 加入版本信息*/
                return new SimpleResponse(ApiStatusEnum.SUCCESS, userLoginInfo);
            } else {
                // 新增用户
                appletUser = AppletUserFactory.newAppletUser(verifyResultData);
            }
        } else {
            // 更新用户
            appletUser.login(verifyResultData);
        }
        // 成为店铺粉丝
        User userInfo = appletUser.getUserInfo();
        //是否是第一次登录
        if (!appletUser.hasVipShop()) {
            appletUser.beVipShop();
            stringRedisTemplate.opsForValue().set(
                    RedisKeyConstant.USER_VIPSHOP_POPUP + userInfo.getVipShopId() + ":" + appletUser.getUserInfo().getOpenId(),
                    "1");

            // ############  列表埋点 是否新人 ##############
            isNewbie = true;
            log.info(">>>>>>>>>>>>>>>>>>>>>>>> 登陆上报埋点-是否新人={}", isNewbie);

        }
        // 数据持久化
        boolean saveOrUpdate = appletUserRepository.saveOrUpdateUserInfo(appletUser);

        if (saveOrUpdate && isNewbie) {
            // ############  列表埋点 开始 ##############
            this.report(wxUserSession, appletUser);
            // ############  列表埋点 结束 ##############
        }

        Long userId = userInfo.getId();
        String pubUid = wxUserSession.getPub_uid();
        // 解锁
        stringRedisTemplate.delete(RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openid);
        if (!saveOrUpdate) {
            log.error("保存或更新用户失败，userId = {}", userInfo.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
        }
        // 保存 用户浏览店铺的足迹
        if (!appletUser.isInVipShop()) {
            shopAppService.saveViewShop(userInfo.getId(), userInfo.getShopId());
        }

        // 建立邀请关系
        buildInviteRelationship(wxUserSession, userInfo);

        // 获取用户登录信息
        Map<String, Object> userLoginInfo = new HashMap<>();
        // 生成用户令牌
        String token = TokenUtil.encryptToken(userInfo.getId().toString(), userInfo.getOpenId(), userInfo.getCtime(), TokenUtil.APPLET);
        log.info("<<<<<<<<<<<< 登录时生成的token = {} <<<<<<<<<<<", token);
        userLoginInfo.put("token", token);
        userLoginInfo.put("userId", userInfo.getId());
        userLoginInfo.put("nickName", userInfo.getNickName());
        userLoginInfo.put("mobile", userInfo.getMobile());
        userLoginInfo.put("avatarUrl", userInfo.getAvatarUrl());
        userLoginInfo.put("shopId", userInfo.getShopId());
        userLoginInfo.put("merchantId", userInfo.getMerchantId());
        if (StringUtils.isBlank(wxUserSession.getScene()) || !RegexUtils.StringIsNumber(wxUserSession.getScene())) {
            userLoginInfo.put("scene", "");
        } else {
            userLoginInfo.put("scene", userInfo.getScene());
        }
        // 获取当前浏览的店铺信息
        Long shopId;
        if (StringUtils.isNotBlank(wxUserSession.getEntryStatus())
                && RegexUtils.StringIsNumber(wxUserSession.getEntryStatus())
                && Integer.parseInt(wxUserSession.getEntryStatus()) == 1) {
            shopId = userInfo.getVipShopId();
        } else {
            shopId = userInfo.getShopId();
        }

        if (shopId == null) {
            log.error("shopId为空");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "登录店铺未知");
        }
        Map<String, Object> shopInfo = shopAppService.queryShopInfoByLogin(shopId.toString());
        if (CollectionUtils.isEmpty(shopInfo)) {
            log.error("网络异常，请稍候重试");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "网络异常，请稍候重试");
        }
        userLoginInfo.putAll(shopInfo);

        // 登录者 是否需要弹vip窗
        Boolean vipShopKey = stringRedisTemplate.hasKey(RedisKeyConstant.USER_VIPSHOP_POPUP + userInfo.getShopId() + ":" + appletUser.getUserInfo().getOpenId());
        vipShopKey = vipShopKey == null ? false : vipShopKey;
        if (vipShopKey) {
            Integer userCount = appletUserRepository.getShopFans(appletUser);

            Map<String, Object> dialogInfo = new HashMap<>();
            dialogInfo.put("userName", userInfo.getNickName());
            dialogInfo.put("userAvatar", userInfo.getAvatarUrl());
            dialogInfo.put("shopName", StringUtils.substring(((Map) shopInfo.get("shopInfo")).get("shopName").toString(), 0, 6));
            dialogInfo.put("userCount", userCount);

            // 2.3迭代 updateCheckpointsAndPromotion -------- 处理 新人vip店铺弹窗样式 ------------
            Map<String, Object> openRedPackageStatus = oneAppService.queryOpenRedPackageStatus(shopId, userId);
            Map<String, Object> newcomerTaskInfo = oneAppService.queryNewcomerTaskInfo(shopId, userId);

            if ((CollectionUtils.isEmpty(newcomerTaskInfo) || BigDecimal.ZERO.compareTo(new BigDecimal(newcomerTaskInfo.get("newbieAmount").toString())) >= 0) && CollectionUtils.isEmpty(openRedPackageStatus)) {
                dialogInfo.put("dialogType", 1);
            } else if (!CollectionUtils.isEmpty(newcomerTaskInfo) && BigDecimal.ZERO.compareTo(new BigDecimal(newcomerTaskInfo.get("newbieAmount").toString())) < 0) {
                dialogInfo.put("dialogType", 2);
                dialogInfo.put("amount", newcomerTaskInfo.get("newbieAmount"));
                dialogInfo.putAll(newcomerTaskInfo);
            } else {
                dialogInfo.put("dialogType", 3);
                dialogInfo.putAll(openRedPackageStatus);
            }
            // -------type: 1.原来vip店铺样式; 2.第365位粉丝+送你20元现金作为见面礼+收下; 3.第365位粉丝+送你20元红包+拆开红包----

            userLoginInfo.put("shopVipDialog", dialogInfo);
            stringRedisTemplate.delete(RedisKeyConstant.USER_VIPSHOP_POPUP + userInfo.getVipShopId() + ":" + appletUser.getUserInfo().getOpenId());

        }
        // ############  登录埋点 开始 ##############
        asyncSendLoginMq(appletUser);
        // ############  登录埋点 结束 ##############
        /** 集客魔方-尾货清仓版 加入版本信息*/
        userLoginInfo.put("purchaseVersion", shopInfo.get("purchaseVersion"));
        /** 集客魔方-尾货清仓版 加入版本信息*/
        return new SimpleResponse(ApiStatusEnum.SUCCESS, userLoginInfo);
    }

    /**
     * 异步发送埋点数据
     *
     * @param appletUser
     */
    private void asyncSendLoginMq(AppletUser appletUser) {
        String redisKey = RedisKeyConstant.USER_LOGIN_BIZ_LOG_REPEAT + appletUser.getUserInfo().getId();
        Boolean canSend = stringRedisTemplate.opsForValue().setIfAbsent(redisKey, DateUtil.getCurrentMillis() + "",
                DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);
        if (canSend == null || !canSend) {
            return;
        }
        FanActionForMQ fanActionForMQ = new FanActionForMQ();
        fanActionForMQ.setMid(appletUser.getUserInfo().getMerchantId());
        fanActionForMQ.setSid(appletUser.getUserInfo().getShopId());
        fanActionForMQ.setUid(appletUser.getUserInfo().getId());
        fanActionForMQ.setEvent(FanActionEventEnum.LOGIN.getCode());
        fanActionForMQ.setEventId(0L);
        fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
        rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
                JSON.toJSONString(fanActionForMQ), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("登录埋点发送成功");
                    }

                    @Override
                    public void onException(Throwable e) {
                        log.info("登录埋点发送失败");
                    }
                });
    }

    /**
     * 裂变埋点上报
     *
     * @param wxUserSession
     * @param appletUser
     */
    private void report(WxUserSession wxUserSession, AppletUser appletUser) {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>> 登陆上报-裂变人数 >>>>>>>>>>>>>>>>>>>>>>>");
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>> wxUserSession={}", wxUserSession);
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>> appletUser={}", appletUser);
        try {
            if (!StringUtils.isBlank(wxUserSession.getPub_uid())) {
                log.info(">>>>>>>>>>>>>>>>>>>>>>>>>> 登陆上报-裂变人数-有上级 >>>>>>>>>>>>>>>>>>>>>>>");
                FissionUser fissionUser = new FissionUser();
                // 组装
                fissionUser.setMid(wxUserSession.getMerchantId());
                fissionUser.setSid(wxUserSession.getShopId());
                fissionUser.setParentId(wxUserSession.getPub_uid());
                fissionUser.setSubId(appletUser.getId().toString());
                fissionUser.setActivityId(wxUserSession.getAid());
                fissionUser.setScene(MongoUserActionScene.FISSION_POINT.getCode().toString());
                fissionUser.setCtime(new Date());
                log.info(">>>>>>>>>>>>>>>>>>>>>>>>>> 登陆上报-裂变人数-上报实体={} >>>>>>>>>>>>>>>>>>>>>>>", fissionUser);
                // 上报
                rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.STATISTICS_REPORT_FISSION), JSON.toJSONString(fissionUser));
                log.info(">>>>>>>>>>>>>>>>>>>>>>>>>> 登陆上报-裂变人数-成功结束-逻辑 >>>>>>>>>>>>>>>>>>>>>>>");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("登陆上报 裂变埋点上报错误!");
        }
    }

    /**
     * 建立邀请关系 -- 生产消息
     *
     * @param wxUserSession
     * @param userInfo
     */
    private void buildInviteRelationship(WxUserSession wxUserSession, User userInfo) {
        String pubUid = wxUserSession.getPub_uid();

        try {
            // 建立商城团队
            Map<String, String> inviteParams = new HashMap<>();
            inviteParams.put("pubUserId", pubUid);
            inviteParams.put("userId", userInfo.getId().toString());
            inviteParams.put("activityId", wxUserSession.getAid());
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.SHOPPING_TEAM),
                    JSON.toJSONString(inviteParams));
            log.info("建立商城团队消息生产：" + JSON.toJSONString(inviteParams));
        } catch (Exception e) {
            log.error("建立商城团队消息生产异常, exception = {}", e);
        }
        // try {
        // 	// 抓矿工
        // 	Map<String, String> inviteParams = new HashMap<>();
        // 	inviteParams.put("pubUserId", pubUid);
        // 	inviteParams.put("userId", userInfo.getId().toString());
        // 	rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.CATCH_MINER),
        // 			JSON.toJSONString(inviteParams));
        // 	log.info("抓矿工消息生产：" + JSON.toJSONString(inviteParams));
        // } catch (Exception e) {
        // 	log.error("抓矿工消息生产异常, exception = {}", e);
        // }
        try {
            // 活动邀请
            Map<String, String> inviteParams = new HashMap<>();
            inviteParams.put("pubUserId", pubUid);
            inviteParams.put("userId", userInfo.getId().toString());
            inviteParams.put("activityId", wxUserSession.getAid());
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.INVITE_ACTIVITY),
                    JSON.toJSONString(inviteParams));
            log.info("活动邀请消息生产：" + JSON.toJSONString(inviteParams));
        } catch (Exception e) {
            log.error("活动邀请消息生产异常, exception = {}", e);
        }

        // 粉丝召回
        try {
            recallV3Service.incrScanTime(wxUserSession.getRecallScene());
            //recallScene: 8,52,11,9146,uid36,pid19
            log.info("粉丝召回推送：" + wxUserSession.getRecallScene());
            fansV3Service.asyncIncrClickPush(wxUserSession.getRecallScene());
        } catch (Exception e) {
            log.error("粉丝召回计数失败：" + JSON.toJSONString(wxUserSession));
        }
    }
}
