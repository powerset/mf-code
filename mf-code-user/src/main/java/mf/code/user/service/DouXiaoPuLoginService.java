package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.dto.WxUserSession;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 15:41
 */
public interface DouXiaoPuLoginService {

    /**
     * 抖小铺 用户登录
     * @param wxUserSession
     * @return
     */
    SimpleResponse login(WxUserSession wxUserSession);
}
