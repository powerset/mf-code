package mf.code.user.service;

import mf.code.user.domain.aggregateroot.AppletUser;

/**
 * mf.code.user.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月04日 19:29
 */
public interface AsyncWxTemplateMsgService {

    void wxtemplateMsg(AppletUser appletUser, String pubUid, boolean firstLogin);
}
