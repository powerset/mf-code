package mf.code.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.HttpsUtil;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.service.DouYinUserVerifyService;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.codehaus.xfire.util.Base64;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 16:06
 */
@Service
@Slf4j
public class DouYinUserVerifyServiceImpl implements DouYinUserVerifyService {

    /**
     * 抖带带用户小程序 登录session校验
     * @param wxUserSession
     * @return
     */
    @Override
    public SimpleResponse loginVerify(WxUserSession wxUserSession,  String appId, String appSecret) {
        String url = "https://developer.toutiao.com/api/apps/jscode2session";
        Map<String, String> param = new HashMap<>();
        param.put("appid", appId);
        param.put("secret", appSecret);
        param.put("code", wxUserSession.getCode());
        // param.put("grant_type", "authorization_code");

        String session;
        try {
            session = HttpsUtil.doGet(url, param);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("抖音登录回调异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "抖音登录回调异常");
        }
        if (StringUtils.isBlank(session)) {
            log.error("session 为空");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "session 为空");
        }
        JSONObject sessionJO = JSON.parseObject(session);
        if (sessionJO.containsKey("errcode")) {
            log.error("errcode:{}, errmsg:{}", sessionJO.getIntValue("errcode"), sessionJO.getString("errmsg"));
            return new SimpleResponse(sessionJO.getIntValue("errcode"), sessionJO.getString("errmsg"));
        }

        // 用户信息密文 解析
        JSONObject userInfo = decodeEncryptedData(wxUserSession.getEncryptedData(), wxUserSession.getIv(), sessionJO.getString("session_key"));

        Map<String, Object> verifyResult = new HashMap<>();
        verifyResult.put("openid", sessionJO.getString("openid"));
        verifyResult.put("sessionKey", sessionJO.getString("session_key"));
        verifyResult.put("merchantId", wxUserSession.getMerchantId());
        verifyResult.put("shopId", wxUserSession.getShopId());
        verifyResult.put("scene", wxUserSession.getScene());
        verifyResult.put("userInfo", userInfo);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, JSON.toJSONString(verifyResult));
    }

    // 用户信息密文 解析
    private JSONObject decodeEncryptedData(String encryptedData, String iv, String sessionKey) {
        // 检验参数
        if (StringUtils.isBlank(encryptedData)
                || StringUtils.isBlank(iv)
                || StringUtils.isBlank(sessionKey)) {
            return null;
        }

        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decode(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv);
        try {
            // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
            byte[] resultByte = cipher.doFinal(dataByte);

            if (null != resultByte && resultByte.length > 0) {
                String userinfo = new String(resultByte, StandardCharsets.UTF_8);
                return JSON.parseObject(userinfo);
            }

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidParameterSpecException
                | IllegalBlockSizeException | BadPaddingException
                | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchProviderException e) {
            log.error("解析异常：" + e);
            return null;
        }

        return null;
    }
}
