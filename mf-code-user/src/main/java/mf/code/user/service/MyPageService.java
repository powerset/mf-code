package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * 我的页面
 * create by qc on 2019/8/9 0009
 */
public interface MyPageService {
    /**
     * 我的页面
     *
     * @param userId 用户
     * @param shopId 店铺
     * @return SimpleResponse
     */
    SimpleResponse doudaidaiMyPage(Long userId, Long shopId);
}
