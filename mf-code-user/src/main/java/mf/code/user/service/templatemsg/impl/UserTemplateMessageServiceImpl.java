package mf.code.user.service.templatemsg.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.user.common.caller.wxmp.WeixinMpService;
import mf.code.user.common.caller.wxmp.WxmpProperty;
import mf.code.user.common.property.AppProperty;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.UserRepository;
import mf.code.user.service.templatemsg.UserTemplateMessageService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static mf.code.common.WeixinMpConstants.DOMAIN_WX_SENDTEMPLATEMESSAGE;

/**
 * mf.code.user.service.templatemsg.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月04日 15:54
 */
@Service
@Slf4j
public class UserTemplateMessageServiceImpl implements UserTemplateMessageService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AppProperty wxpayProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WxmpProperty wxmpProperty;

    @Async
    @Override
    public void sendTemplateMessage(String templateId, User user, Map<String, Object> data) {
        log.info("<<<<<<<<推送消息-入参：data：{}， template:{}, user:{}", data, templateId, user);
        if (StringUtils.isBlank(templateId) || user == null || CollectionUtils.isEmpty(data)) {
            return;
        }
        Map<String, Object> reqParam = new HashMap<>();
        reqParam.put("touser", user.getOpenId());
        reqParam.put("template_id", templateId);
        String formId = queryRedisTeacherFormIds(user.getId());
        if(StringUtils.isBlank(formId)){
            return;
        }
        reqParam.put("form_id", formId);
        reqParam.putAll(data);
        log.info("<<<<<<<<推送消息-开始：{}", reqParam);
        String accessToken = weixinMpService.getAccessToken(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret());
        if (StringUtils.isBlank(accessToken)) {
            log.error("accesstoken 为空：{}", accessToken);
            return;
        }
        String accessTokenParam = "access_token=" + accessToken;
        String url = DOMAIN_WX_SENDTEMPLATEMESSAGE + "?" + accessTokenParam;
        String voStrParams = JSONObject.toJSONString(reqParam);
        Map<String, Object> map = this.weixinMpService.sendMessage(url, voStrParams);
        log.info("<<<<<<<<推送消息-结束返回: resp:{}", map);
    }

    @Override
    public void sendTemplateMessage(String templateId, Long userId, Map<String, Object> data) {
        User user = this.userRepository.selectById(userId);
        log.info("<<<<<<<<推送消息用户查询结果：{}", user);
        this.sendTemplateMessage(templateId, user, data);
    }

    private String queryRedisTeacherFormIds(Long tid) {
        //查询redis,参与人展现,直接倒叙排列
        BoundListOperations formIDS = this.stringRedisTemplate.boundListOps(wxmpProperty.getRedisKey(tid));
        List<Object> formIDStrs = formIDS.range(0, -1);
        if (formIDStrs == null || formIDStrs.size() == 0) {
            return null;
        }
        String formID = null;
        int index = 0;
        for (Object obj : formIDStrs) {
            JSONObject jsonObject = JSON.parseObject(obj.toString());
            //判断该formId是否为有效的 7天内有效
            long DayMillis = 24 * 60 * 60 * 1000;
            if ((System.currentTimeMillis() - jsonObject.getLong("ctime")) / DayMillis > 6) {
                //过期都删除
                this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(tid), index, obj);
                index++;
                continue;
            }
            formID = jsonObject.getString("formId");
            //用完即删
            this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(tid), index, obj);
            break;
        }
        return formID;
    }
}
