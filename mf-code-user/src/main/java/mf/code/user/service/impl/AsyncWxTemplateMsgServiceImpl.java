package mf.code.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.user.api.applet.feignservice.UserAppServiceImpl;
import mf.code.user.api.feignclient.DistributionAppService;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.common.caller.wxmp.WxmpProperty;
import mf.code.user.common.redis.RedisForbidRepeat;
import mf.code.user.common.redis.RedisKeyConstant;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.UserRepository;
import mf.code.user.service.AsyncWxTemplateMsgService;
import mf.code.user.service.templatemsg.TemplateMessageData;
import mf.code.user.service.templatemsg.UserTemplateMessageService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.user.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月04日 19:29
 */
@Service
@Slf4j
public class AsyncWxTemplateMsgServiceImpl implements AsyncWxTemplateMsgService {
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private UserAppServiceImpl userAppService;
    @Autowired
    private UserTemplateMessageService userTemplateMessageService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DistributionAppService distributionAppService;

    @Value("${tmplatemessage.userloginTemplateTime}")
    private String userloginTemplateTime;

    /***
     * 登录时的推送消息
     * @param appletUser
     * @param pubUid
     * @param firstLogin
     */
    @Async
    @Override
    public void wxtemplateMsg(AppletUser appletUser, String pubUid, boolean firstLogin) {
        log.info("<<<<<<<<登录推送开始 userInfo:{} pubuid:{} firstLogin:{}", appletUser, pubUid, firstLogin);
        Long merchantId = shopAppService.getMerchantIdByShopId(appletUser.getUserInfo().getVipShopId());
        //新人首次进来，推送逻辑
        if (firstLogin) {
            firstLoginTemplateMsg(appletUser, pubUid, merchantId);
        }
        //每次进入推送逻辑
        loginTemplateMsg(appletUser, pubUid, firstLogin, merchantId);
    }


    /***
     * 首次登录的推送
     * @param appletUser
     * @param pubUid
     */
    private void firstLoginTemplateMsg(AppletUser appletUser, String pubUid, Long merchantId) {
        List<Long> upIds = new ArrayList<>();
        Long shopId = appletUser.getUserInfo().getVipShopId();
        if (StringUtils.isNotBlank(pubUid)) {
            //获取直系上级，pubUid
            long pubUId = NumberUtils.toLong(pubUid);
            upIds.add(pubUId);
            //获取所有上4级
            Map<Integer, Long> levelUpperIdMap = userAppService.queryUpperMember(shopId, pubUId, 4);
            log.info("<<<<<<<<推送消息的-首次登录-获取所有上级， upperIds:{}", CollectionUtils.isEmpty(levelUpperIdMap) ? null : levelUpperIdMap.values());
            if (!CollectionUtils.isEmpty(levelUpperIdMap)) {
                upIds.addAll(levelUpperIdMap.values());
            }
        }
        log.info("<<<<<<<<推送消息-首次登录-获取上级的用户， userId:{} upperIds:{}", appletUser.getUserInfo().getId(), upIds);

        //需要推送的userIds = upIds
        if (!CollectionUtils.isEmpty(upIds)) {
            for (Long userId : upIds) {
                //此刻是包含自己的
                Integer teamSum = 0;
                Map<Integer, List<Long>> teamUserSize = userAppService.queryLowerMember(shopId, userId, 5);
                if (CollectionUtils.isEmpty(teamUserSize)) {
                    log.error("<<<<<<<<获取该用户的所有下级成员未空，userId:{}", userId);
                    continue;
                }
                for (Map.Entry<Integer, List<Long>> entry : teamUserSize.entrySet()) {
                    teamSum = teamSum + entry.getValue().size();
                }
                log.info("<<<<<<<<获取该用户的所有下级成员返回，userId:{} teamSum:{}", userId, teamSum);
                if (teamSum != null && teamSum > 0) {
                    // 获取 此上级的角色及消费达标状态


                    User user = userRepository.getById(userId);
                    int scene = 28;
                    String isDialog = "0";
                    if (user.getRole().equals(UserRoleEnum.SHOP_MANAGER.getCode())) {
                        scene = 36;
                        if (distributionAppService.hasReachSpendStandardThisMonth(userId)) {
                            isDialog = "0";
                        }
                    }
                    Map<String, Object> data = TemplateMessageData.inviteresult(merchantId, shopId, new Date(), teamSum, scene, isDialog);
                    userTemplateMessageService.sendTemplateMessage(wxmpProperty.getInviteresultMsgTmpId(), userId, data);
                }
            }
        }
    }

    /***
     * 登录的推送
     * @param appletUser
     * @param pubUid
     * @param firstLogin
     * @param merchantId 商户编号
     */
    private void loginTemplateMsg(AppletUser appletUser, String pubUid, boolean firstLogin, Long merchantId) {
        String redisRepeatKey = RedisKeyConstant.USER_LOGIN_TEMPLATEMSG_REPEAT + appletUser.getUserInfo().getId();
        if (!RedisForbidRepeat.NX(stringRedisTemplate, redisRepeatKey, 3, TimeUnit.SECONDS)) {
            log.error("<<<<<<<<异步推送还在进行中...不能重复");
            return;
        }
        try {
            //存储redis，不能无限制的推送
            String redisKey = RedisKeyConstant.USER_LOGIN_TEMPLATEMSG + appletUser.getUserInfo().getId();
            String str = stringRedisTemplate.opsForValue().get(redisKey);
            if (StringUtils.isNotBlank(str)) {
                return;
            }
            List<Long> upIds = new ArrayList<>();
            Long shopId = appletUser.getUserInfo().getVipShopId();
            String nick = appletUser.getUserInfo().getNickName();
            Map<Integer, Long> levelUpperIdMap = userAppService.queryUpperMember(shopId, appletUser.getUserInfo().getId(), 5);
            log.info("<<<<<<<<推送消息的-每次登录-获取所有上级， upperIds:{}", CollectionUtils.isEmpty(levelUpperIdMap) ? null : levelUpperIdMap.values());
            if (!CollectionUtils.isEmpty(levelUpperIdMap)) {
                upIds.addAll(levelUpperIdMap.values());
            }
            log.info("<<<<<<<<推送消息的用户， userIds:{}", upIds);
            //需要推送的userIds = upIds
            if (!CollectionUtils.isEmpty(upIds)) {
                for (Long userId : upIds) {
                    // 获取 此上级的角色及消费达标状态
                    // 获取 此上级的角色及消费达标状态

                    User user = userRepository.getById(userId);
                    int scene = 28;
                    String isDialog = "0";
                    if (user.getRole().equals(UserRoleEnum.SHOP_MANAGER.getCode())) {
                        scene = 37;
                        if (distributionAppService.hasReachSpendStandardThisMonth(userId)) {
                            isDialog = "0";
                        }
                    }

                    Map<String, Object> data = TemplateMessageData.newcustomervisit(merchantId, shopId, nick, new Date(), scene, isDialog);
                    userTemplateMessageService.sendTemplateMessage(wxmpProperty.getNewcustomervisitMsgTmpId(), userId, data);
                }
            }
            //时间间隔
            int timeout = 0;
            if (StringUtils.isBlank(userloginTemplateTime)) {
                //默认720分钟=12小时
                timeout = 720;
            } else {
                timeout = NumberUtils.toInt(userloginTemplateTime);
            }
            //用户推送结束后，间隔多久在给推送资格
            stringRedisTemplate.opsForValue().set(redisKey, "0", timeout, TimeUnit.MINUTES);
        } finally {
            //删除防重rediskey
            stringRedisTemplate.delete(redisRepeatKey);
        }
    }

    public Map<String, Object> getSceneAndDialogStatus(Long userId) {
        User user = userRepository.getById(userId);
        int scene = 28;
        String isDialog = "0";
        if (user.getRole().equals(UserRoleEnum.SHOP_MANAGER.getCode())) {
            scene = 36;
            if (distributionAppService.hasReachSpendStandardThisMonth(userId)) {
                isDialog = "0";
            }
        }
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("scene", scene);
        resultVO.put("isDialog", isDialog);
        return resultVO;
    }

}
