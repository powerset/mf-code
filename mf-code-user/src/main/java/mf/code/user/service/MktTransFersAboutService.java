package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.repo.po.User;

/**
 * mf.code.user.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月19日 14:28
 */
public interface MktTransFersAboutService {
    /***
     * 提现成功后的存在拆红包的弹窗
     * @param merchantId
     * @param shopID
     * @param user
     * @return
     */
    SimpleResponse cashSuccessDialogExistOpenRedPack(Long merchantId, Long shopID, User user);

    /***
     * 完成2个新手任务后提现
     * @return
     */
    SimpleResponse cashSuccessDialogNewbiwTaskNumUp();
}
