package mf.code.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.repo.dao.UpayBalanceMapper;
import mf.code.user.repo.po.UpayBalance;
import mf.code.user.repo.repository.UpayWxOrderService;
import mf.code.user.service.UpayBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 14:58
 */
@Slf4j
@Service
public class UpayBalanceServiceImpl extends ServiceImpl<UpayBalanceMapper, UpayBalance> implements UpayBalanceService {
    @Autowired
    private UpayBalanceMapper upayBalanceMapper;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private ShopAppService shopAppService;

    /**
     * 更新商户余额
     *
     * @param userId
     * @param mchId
     * @param shopId
     * @param totalFee
     */
    @Override
    public Integer updateBalance(Long userId, Long mchId, Long shopId, BigDecimal totalFee) {
        int rows;

        Date now = new Date();
        QueryWrapper<UpayBalance> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayBalance::getShopId, shopId)
                .eq(UpayBalance::getUserId, userId);
        UpayBalance upayBalance = upayBalanceMapper.selectOne(wrapper);
        if (upayBalance == null) {
            // 新增一条余额记录
            upayBalance = new UpayBalance();
            upayBalance.setUserId(userId);
            if (mchId == null || mchId == 0L) {
                mchId = shopAppService.getMerchantIdByShopId(shopId);
            }
            upayBalance.setMerchantId(mchId);
            upayBalance.setShopId(shopId);
            upayBalance.setBalance(totalFee);
            upayBalance.setCreatedAt(now);
            upayBalance.setUpdatedAt(now);
            rows = upayBalanceMapper.insertSelective(upayBalance);
            if (rows == 0) {
                log.error("用户余额创建失败，userId = {}, merchantId = {}, shopId = {}", userId, mchId, shopId);
            }
        } else {
            // 更新用户余额
            Map<String, Object> params = new HashMap<>();
            params.put("userId", userId);
            params.put("shopId", shopId);
            params.put("balance", totalFee);
            params.put("utime", now);

            rows = upayBalanceMapper.updateBalanceByParams(params);
            if (rows == 0) {
                log.error("用户余额更新失败，userId = {}, merchantId = {}, shopId = {}", userId, mchId, shopId);
                // TODO 发送邮件报警
            }
        }
        return rows;
    }

    /**
     * 更新用户余额结算时间 -- 返利分佣
     *
     * @param shopId
     * @return
     */
    @Override
    public int updateUserBalanceSettledTime(Long shopId) {
        Map<String, Object> params = new HashMap<>();
        params.put("shopId", shopId);
        params.put("updateTime", new Date());

        return upayBalanceMapper.updateUserBalanceSettledTime(params);
    }

    @Override
    public UpayBalance query(long merchantID, long shopID, long userID) {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("merchantId", merchantID);
        objectMap.put("shopId", shopID);
        objectMap.put("userId", userID);
        List<UpayBalance> upayBalances = this.upayBalanceMapper.listPageByParams(objectMap);
        if (upayBalances == null || upayBalances.size() == 0) {
            return null;
        }
        return upayBalances.get(0);
    }

    @Override
    public UpayBalance update(UpayBalance upayBalance) {
        upayBalance.setUpdatedAt(new Date());
        this.upayBalanceMapper.updateByPrimaryKeySelective(upayBalance);
        return upayBalance;
    }

    @Override
    public int updateLessUserBalance(long merchantID, long shopID, long userID, BigDecimal taskBalance, BigDecimal shopBalance, BigDecimal openRedPackBalance) {
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantID);
        params.put("shopId", shopID);
        params.put("userId", userID);
        params.put("taskBalance", taskBalance);
        params.put("shoppingBalance", shopBalance);
        params.put("openRedPackBalance", openRedPackBalance);
        params.put("updateAt", new Date());
        return upayBalanceMapper.updateLessUserBalanceByParams(params);
    }
}
