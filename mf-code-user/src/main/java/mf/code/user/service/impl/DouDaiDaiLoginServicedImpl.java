package mf.code.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.TokenUtil;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.common.property.AppProperty;
import mf.code.user.common.redis.RedisForbidRepeat;
import mf.code.user.common.redis.RedisKeyConstant;
import mf.code.user.domain.aggregateroot.DouDaiDaiUser;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.DouDaiDaiUserRepository;
import mf.code.user.service.DouDaiDaiLoginService;
import mf.code.user.service.DouYinUserVerifyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 15:56
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class DouDaiDaiLoginServicedImpl implements DouDaiDaiLoginService {
    private final DouYinUserVerifyService douDaiDaiUserVerifyService;
    private final StringRedisTemplate stringRedisTemplate;
    private final AppProperty appProperty;
    private final DouDaiDaiUserRepository douDaiDaiUserRepository;


    /**
     * 抖带带 用户登录
     * @param wxUserSession
     * @return
     */
    @Override
    public SimpleResponse login(WxUserSession wxUserSession) {
        // 校验session 有效性
        SimpleResponse verifyResult = douDaiDaiUserVerifyService.loginVerify(wxUserSession, appProperty.getDouDaiDaiAppId(), appProperty.getDouDaiDaiSecret());
        if (verifyResult.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            log.error("登录校验异常");
            return verifyResult;
        }
        JSONObject verifyResultData = JSON.parseObject(verifyResult.getData().toString());
        // 获取用户信息
        String openId = verifyResultData.getString("openid");
        // 加锁
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openId);
        if (!success) {
            log.error("redis 登录防重, openid = {}", openId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "正在登录中……");
        }
        // 获取用户
        DouDaiDaiUser douDaiDaiUser = douDaiDaiUserRepository.findByLogin(verifyResultData);
        if (douDaiDaiUser == null) {
            log.info("用户未授权");
            stringRedisTemplate.delete(RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "用户未授权");
        }
        boolean save = douDaiDaiUserRepository.saveByLogin(douDaiDaiUser);
        if (!save) {
            log.error("保存或更新用户失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
        }
        // 解锁
        stringRedisTemplate.delete(RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openId);

        MerchantDTO merchantInfo = douDaiDaiUser.getMerchantInfo();
        User userInfo = douDaiDaiUser.getUserInfo();
        MerchantShopDTO merchantShopInfo = douDaiDaiUser.getMerchantShopInfo();

        // 获取用户登录信息
        Map<String, Object> userLoginInfo = new HashMap<>();
        // 生成用户令牌
        String token = TokenUtil.encryptToken(merchantInfo.getId().toString(), merchantInfo.getOpenId(), merchantInfo.getCtime(), TokenUtil.DOU_DAI_DAI);
        log.info("<<<<<<<<<<<< 登录时生成的token = {} <<<<<<<<<<<", token);
        userLoginInfo.put("token", token);
        userLoginInfo.put("userId", userInfo.getId());
        userLoginInfo.put("nickName", merchantInfo.getName());
        userLoginInfo.put("mobile", StringUtils.isBlank(merchantInfo.getPhone()) ? null : merchantInfo.getPhone());
        userLoginInfo.put("avatarUrl", merchantInfo.getAvatarUrl());
        userLoginInfo.put("shopId", merchantShopInfo.getId());
        userLoginInfo.put("merchantId", merchantInfo.getId());

        return new SimpleResponse(ApiStatusEnum.SUCCESS, userLoginInfo);
    }


}
