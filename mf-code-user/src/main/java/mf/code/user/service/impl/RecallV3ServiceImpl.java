package mf.code.user.service.impl;/**
 * create by qc on 2019/2/19 0019
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.user.repo.dao.RecallMsgMapper;
import mf.code.user.service.RecallV3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gbf
 */
@Slf4j
@Service
public class RecallV3ServiceImpl implements RecallV3Service {
    @Autowired
    private RecallMsgMapper recallMsgMapper;

    @Async
    @Override
    public void incrScanTime(String recallScene) {
        Map m = new HashMap(3);
        if (recallScene != null && !"".equals(recallScene)) {
            String[] split = recallScene.split(",");
            if (split.length < 3) {
                return;
            }
            m.put("merchantId", split[0]);
            m.put("shopId", split[1]);
            Integer scene = Integer.parseInt(split[2]);
            if (scene == 5 || scene == 7 || scene == 11) {
                m.put("goodsId", split[3]);
            }
            if (scene == 10 || scene == 9) {
                m.put("activityId", split[3]);
            }
        }
        recallMsgMapper.incrScanTime(m);
    }

}
