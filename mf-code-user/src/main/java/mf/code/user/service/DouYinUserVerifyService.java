package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.common.property.AppProperty;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 16:02
 */
public interface DouYinUserVerifyService {

    /**
     * 抖带带用户小程序 登录session校验
     * @param wxUserSession
     * @return
     */
    SimpleResponse loginVerify(WxUserSession wxUserSession, String appId, String appSecret);
}
