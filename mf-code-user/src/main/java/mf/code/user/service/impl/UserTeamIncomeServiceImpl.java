package mf.code.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.user.domain.valueobject.specification.InvitationSpecification;
import mf.code.user.domain.valueobject.specification.InvitationTypeEnum;
import mf.code.user.domain.valueobject.specification.InviteShoppingTeamSpecification;
import mf.code.user.repo.po.UserPubJoin;
import mf.code.user.repo.repository.InvitationEventRepository;
import mf.code.user.service.UserTeamIncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * 用户商城团队服务
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-31 21:50
 */
@Slf4j
@Service
public class UserTeamIncomeServiceImpl implements UserTeamIncomeService {
	@Autowired
	private InvitationEventRepository invitationEventRepository;

	/**
	 * 获取 此商城 我的团队 上级用户id
	 *
	 * @return
	 */
	@Override
	public Long getUpperId(Long userId, Long vipShopId) {
		// 获取 商城团队 邀请规则
		InvitationSpecification invitationSpecification = new InviteShoppingTeamSpecification(vipShopId);
		// 获取 用户 被邀请记录
		return getUpperIdByUserIdInvitationSpecification(userId, invitationSpecification);
	}

	/**
	 * 获取 此商城 我的团队 上级ids
	 *
	 * @param userId
	 * @param vipShopId
	 * @param scale
	 * @return
	 */
	@Override
	public List<Long> getUpperIds(Long userId, Long vipShopId, Integer scale) {
		List<Long> upperIds = new ArrayList<>();
		for (int i = 0; i < scale; i++) {
			Long upperId = this.getUpperId(userId, vipShopId);
			if (upperId == null) {
				break;
			}
			upperIds.add(upperId);
			userId = upperId;
		}
		return upperIds;
	}

	/**
	 * 获取 当前商城的用户的所属上级成员
	 *
	 * @param vipShopId
	 * @param userId
	 * @param scale
	 * @return JSON.toJSONString(Map < level, List < UserId > >)
	 */
	@Override
	public Map<Integer, Long> queryUpperMember(Long vipShopId, Long userId, Integer scale) {
		// 获取 商城团队 邀请规则
		InvitationSpecification invitationSpecification = new InviteShoppingTeamSpecification(vipShopId);

		Map<Integer, Long> upperMember = new HashMap<>();

		for (int i = 0; i < scale; i++) {
			Long upperId = this.getUpperIdByUserIdInvitationSpecification(userId, invitationSpecification);
			if (upperId == null) {
				break;
			}
			upperMember.put(i, upperId);
			userId = upperId;
		}
		return upperMember;
	}

	/**
	 * 获取 当前商城的用户的所属上级成员
	 *
	 * @param vipShopId
	 * @param userId
	 * @param scale
	 * @return JSON.toJSONString(Map < level, List < UserId > >)
	 */
	@Override
	public Map<Integer, List<Long>> queryLowerMember(Long vipShopId, Long userId, Integer scale) {
		Map<Integer, List<Long>> lowerMember = new HashMap<>();

		// 获取 团队人数
		List<Long> ids = new ArrayList<>();
		ids.add(userId);
		for (int i = 0; i < scale; i++) {
			List<Long> lowerIds = this.getNextGroupIds(vipShopId, ids);
			if (CollectionUtils.isEmpty(lowerIds)) {
				break;
			}
			lowerMember.put(i, lowerIds);
			ids = lowerIds;
		}

		return lowerMember;
	}

	/**
	 * 获取 此商城 我的团队 上级用户id
	 *
	 * @return
	 */
	private Long getUpperIdByUserIdInvitationSpecification(Long userId, InvitationSpecification invitationSpecification) {
		// 获取 用户 被邀请记录
		Map<Long, UserPubJoin> invitedRecords = invitationEventRepository.findRecordsBySpecification(null, userId, invitationSpecification);
		if (CollectionUtils.isEmpty(invitedRecords)) {
			log.error("没有上级 -- 未绑定到平台");
			return null;
		}
		// 判断 是否是 平台记录
		if (invitedRecords.size() > 1) {
			log.error("团队上级邀请记录大于1条");
			return null;
		}
		// 获取上级标识
		Long pubUid = null;
		for (UserPubJoin userPubJoin : invitedRecords.values()) {
			pubUid = userPubJoin.getUserId();
		}
		if (pubUid == null) {
			log.error("获取上级异常");
			return null;
		}
		// 判断上级标识 是否是 平台
		if (pubUid.equals(invitationSpecification.getPlatformId())) {
			log.info("上级是平台");
			return null;
		}
		return pubUid;
	}

	/**
	 * 获取 此商城 我下一级团队的 所有ids
	 *
	 * @param ids
	 * @return
	 */
	private List<Long> getNextGroupIds(Long vipShopId, List<Long> ids) {
		if (vipShopId == null || CollectionUtils.isEmpty(ids)) {
			log.info("此用户没有下一级团队了");
			return null;
		}
		// 获取 商城团队 邀请规则
		InvitationSpecification invitationSpecification = new InviteShoppingTeamSpecification(vipShopId);
		invitationSpecification.setInvitationTypeEnum(InvitationTypeEnum.INVITATION);

		// 获取 ids 邀请记录
		Map<Long, UserPubJoin> invitedRecords = invitationEventRepository.findRecordsBySpecification(ids, null, invitationSpecification);
		if (CollectionUtils.isEmpty(invitedRecords)) {
			return null;
		}

		// 获取下级团队ids
		List<Long> groupIds = new ArrayList<>();
		for (UserPubJoin userPubJoin : invitedRecords.values()) {
			groupIds.add(userPubJoin.getSubUid());
		}
		return groupIds;
	}
}
