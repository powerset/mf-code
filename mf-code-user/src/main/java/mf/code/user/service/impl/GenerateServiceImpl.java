package mf.code.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.constant.GenerateCodeSceneEnum;
import mf.code.common.WeixinMpConstants;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.feignservice.UserAppServiceImpl;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.common.caller.wxmp.WeixinMpService;
import mf.code.user.common.property.AppProperty;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.UserRepository;
import mf.code.user.service.GenerateService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月25日 13:44
 */
@Service
@Slf4j
public class GenerateServiceImpl implements GenerateService {
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private AppProperty wxpayProperty;
    @Autowired
    private UserAppServiceImpl userAppService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OneAppService oneAppService;

    /***
     * 推荐粉丝成为店长海报
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse recommendFans(Long merchantId, Long shopId, Long userId, Integer fromId) {
        //获取该用户的粉丝数
        Integer teamSum = 0;
        Map<Integer, List<Long>> teamUserSize = userAppService.queryLowerMember(shopId, userId, 5);
        if (CollectionUtils.isEmpty(teamUserSize)) {
            log.error("<<<<<<<<获取该用户的所有下级成员未空，userId:{}", userId);
        }

        List<Long> userIds = new ArrayList<>();
        for (Map.Entry<Integer, List<Long>> entry : teamUserSize.entrySet()) {
            teamSum = teamSum + entry.getValue().size();
            userIds.addAll(entry.getValue());
        }
        //获取店长数
        int count = 0;
        if (!CollectionUtils.isEmpty(userIds)) {
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .in(User::getId, userIds)
                    .eq(User::getRole, 1)
            ;
            count = userRepository.count(wrapper);
        }

        String path = WeixinMpConstants.WXCODESHOPPOSTERPATH + "/" + GenerateCodeSceneEnum.SHAREPOSTER.getDesc() + shopId + System.currentTimeMillis() + "/";
        Object[] objects = new Object[]{merchantId, shopId, GenerateCodeSceneEnum.SHAREPOSTER.getCode(), 0, userId, fromId};
        String wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), this.getStrArray(objects, ","), path, 430, false);
        if (StringUtils.isBlank(wxaCodeUnlimit)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "生成小程序码失败");
        }
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put("qrcodeImg", wxaCodeUnlimit);
        resp.put("fansNum", teamSum);
        resp.put("shoperNum", count);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 分享自己的群，生成粉丝群
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse groupPoster(Long merchantId, Long shopId, Long userId) {
        Map<String, Object> resp = new HashMap<String, Object>();
        //小程序端店长查询群二维码
        String groupCodeByUserId = oneAppService.getGroupCodeByUserId(userId);
        resp.put("qrcodeImg", groupCodeByUserId);
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    private String getStrArray(Object[] array, String separator) {
        if (array == null || array.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(array[0]);
        for (int i = 1; i < array.length; i++) {
            sb.append(separator).append(array[i]);
        }
        return sb.toString();
    }
}
