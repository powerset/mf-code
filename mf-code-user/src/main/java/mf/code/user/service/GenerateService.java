package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.user.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月25日 13:44
 */
public interface GenerateService {
    /***
     * 推荐粉丝成为店长海报
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse recommendFans(Long merchantId, Long shopId, Long userId, Integer fromId);

    /***
     * 分享自己的群，生成粉丝群
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse groupPoster(Long merchantId, Long shopId, Long userId);
}
