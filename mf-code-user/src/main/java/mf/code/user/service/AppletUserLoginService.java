package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.common.property.AppProperty;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-29 21:22
 */
public interface AppletUserLoginService {

	/**
	 * 微信用户 登录程序 v7
	 * @param wxUserSession
	 * @return
	 */
	SimpleResponse login(WxUserSession wxUserSession);
}
