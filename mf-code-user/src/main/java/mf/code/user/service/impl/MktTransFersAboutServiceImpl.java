package mf.code.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.api.applet.feignservice.UserAppServiceImpl;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.constant.CashSuccessDialogEnum;
import mf.code.user.dto.CashSuccessDialogDTO;
import mf.code.user.repo.po.User;
import mf.code.user.service.MktTransFersAboutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月19日 14:28
 */
@Service
@Slf4j
public class MktTransFersAboutServiceImpl implements MktTransFersAboutService {
    @Autowired
    private UserAppServiceImpl userAppService;
    @Autowired
    private OneAppService oneAppService;

    /***
     * 提现成功后的存在拆红包的弹窗
     * @param merchantId
     * @param shopID
     * @param user
     * @return
     */
    @Override
    public SimpleResponse cashSuccessDialogExistOpenRedPack(Long merchantId, Long shopID, User user) {
        CashSuccessDialogDTO dto = new CashSuccessDialogDTO();
        //判断是否成为店长
        boolean shopManager = false;
        if (user.getRole() == 1) {
            shopManager = true;
        }

        if (shopManager) {
            //拆红包是否存在,提现成功后的拆红包弹窗@百川
            Map<String, Object> openRedPackageMap = oneAppService.queryOpenRedPackageStatus(shopID, user.getId());
            if (openRedPackageMap != null) {
                openRedPackageMap.put("type", CashSuccessDialogEnum.ONEMORE_OPENREDPACK.getCode());
                return new SimpleResponse(ApiStatusEnum.SUCCESS, openRedPackageMap);
            }
        } else {
            Integer teamSum = 0;
            Map<Integer, List<Long>> teamUserSize = userAppService.queryLowerMember(shopID, user.getId(), 5);
            if (CollectionUtils.isEmpty(teamUserSize)) {
                log.error("<<<<<<<<获取该用户的所有下级成员未空，userId:{}", user.getId());
            }
            for (Map.Entry<Integer, List<Long>> entry : teamUserSize.entrySet()) {
                teamSum = teamSum + entry.getValue().size();
            }
            dto.setType(CashSuccessDialogEnum.LEVELUP_SHOPMANAGER.getCode());
            BigDecimal estimateProfit = new BigDecimal(String.valueOf(teamSum)).multiply(new BigDecimal("2.4")).setScale(2, BigDecimal.ROUND_DOWN);
            dto.setEstimateProfit(estimateProfit.toString());
            dto.setTeamSize(teamSum);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
    }

    /***
     * 完成2个新手任务后提现
     * @return
     */
    @Override
    public SimpleResponse cashSuccessDialogNewbiwTaskNumUp() {
        CashSuccessDialogDTO dto = new CashSuccessDialogDTO();
        dto.setType(CashSuccessDialogEnum.FINISH_NEWBIETASK.getCode());
        return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
    }
}
