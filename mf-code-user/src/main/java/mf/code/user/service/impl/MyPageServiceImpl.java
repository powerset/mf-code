package mf.code.user.service.impl;/**
 * create by qc on 2019/8/9 0009
 */

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.DoudaidaiMypageResp;
import mf.code.user.api.feignclient.OrderAppService;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.repo.po.User;
import mf.code.user.service.MyPageService;
import mf.code.user.service.UserInfoService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 我的页面
 *
 * @author gbf
 * 2019/8/9 0009、10:47
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MyPageServiceImpl implements MyPageService {
    private final UserInfoService userInfoService;
    private final ShopAppService shopAppService;
    private final OrderAppService orderAppService;

    /**
     * 我的页面
     *
     * @param userId 用户
     * @param shopId 店铺
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse doudaidaiMyPage(Long userId, Long shopId) {
        // 用户校验
        User user = userInfoService.getById(userId);
        if (user == null) {
            return new SimpleResponse(1, "用户id错误");
        }
        // 店铺校验
        Long merchantId = shopAppService.getMerchantIdByShopId(shopId);
        if (merchantId == null) {
            return new SimpleResponse(2, "店铺id错误");
        }
        DoudaidaiMypageResp doudaidaiMypageData = orderAppService.getDoudaidaiMypageData(merchantId, shopId);
        if (doudaidaiMypageData == null) {
            log.error("调用Order服务OrderAppService#getDoudaidaiMypageData返回null,商户id={},shopId={}", merchantId, shopId);
            return new SimpleResponse(3, "获取数据错误");
        }
        Map<String, String> userMap = new HashMap<>();
        userMap.put("avatar_url", user.getAvatarUrl() == null ? "尚无头像" : user.getAvatarUrl());
        userMap.put("nickname", user.getNickName() == null ? "尚无昵称" : user.getNickName());
        userMap.put("douyin_id", "调研结果:无法获取douyin_id");

        Map<String, Number> commissionMap = new HashMap<>();
        commissionMap.put("total", doudaidaiMypageData.getCommissionTotal());
        commissionMap.put("balance", doudaidaiMypageData.getCommissionBalance());
        commissionMap.put("waitClose", doudaidaiMypageData.getCommissionWaitClose());

        Map<String, Number> shopMap = new HashMap<>();
        shopMap.put("totalFee", doudaidaiMypageData.getOrderTotalFee());
        shopMap.put("totalNum", doudaidaiMypageData.getOrderTotalNum());
        shopMap.put("ytdTotalFee", doudaidaiMypageData.getOrderYtdTotalFee());
        shopMap.put("ytdTotalNum", doudaidaiMypageData.getOrderYtdTotalNum());
        shopMap.put("ytdCommission", doudaidaiMypageData.getOrderYtdCommission());

        Map<String, Map> dataMap = new HashMap<>();
        dataMap.put("user", userMap);
        dataMap.put("commission", commissionMap);
        dataMap.put("shopOrder", shopMap);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, dataMap);
    }
}
