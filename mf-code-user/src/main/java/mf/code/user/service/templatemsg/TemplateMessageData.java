package mf.code.user.service.templatemsg;

import mf.code.common.WeixinMpConstants;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import org.apache.commons.lang.time.DateFormatUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述:
 *
 * @param:
 * @return:
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2019/4/19 0019 14:45
 */
public class TemplateMessageData {

    /***
     * 新客户
     * @param merchantId
     * @param shopId
     * @param nick 昵称
     * @param date 时间
     * @return
     */
    public static Map<String, Object> newcustomervisit(Long merchantId, Long shopId, String nick, Date date, int scene, String isDialog) {
        Map<String, Object> dataInfo;
        Object[] objects = new Object[]{nick, DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss"), "粉丝" + nick + "刚刚来商城逛过，下单后可能会为你带来2.4元收益"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, scene, isDialog));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 邀请结果
     * @param merchantId
     * @param shopId
     * @param date 时间
     * @param sumFans 第几位粉丝
     * @return
     */
    public static Map<String, Object> inviteresult(Long merchantId, Long shopId, Date date, int sumFans, int scene, String isDialog) {
        Map<String, Object> dataInfo;
        Object[] objects = new Object[]{DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss"), "第" + sumFans + "个粉丝加入你的团队", "预计可为你带来2.4元收益"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, scene, isDialog));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /********************************私有方法******************************************/
    private static Map<String, Object> getTempteDate(int keywordNum, Object[] objects) {
        Map<String, Object> m = new HashMap<>();
        for (int i = 0; i < keywordNum; i++) {
            Map map = new HashMap();
            String key = "keyword" + (i + 1);
            String value = objects[i].toString();
            map.put("value", value);
            m.put(key, map);
        }
        return m;
    }

    private static String getPageUrl(Long merchantId,
                                     Long shopId,
                                     int from,
                                     String isDialog) {
        //source--推送消息来源
        String result = "source=1&merchantId=" + merchantId +
                "&shopId=" + shopId +
                "&from=" + from +
                "&isDialog=" + isDialog;
        return result;
    }
}
