package mf.code.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.user.repo.po.UpayBalance;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 14:58
 */
public interface UpayBalanceService extends IService<UpayBalance> {

	/**
	 * 更新用户余额
	 * @param userId
	 * @param mchId
	 * @param shopId
	 * @param totalFee
	 */
	Integer updateBalance(Long userId, Long mchId, Long shopId, BigDecimal totalFee);

	/**
	 * 更新用户余额结算时间 -- 返利分佣
	 * @param shopId
	 * @return
	 */
	int updateUserBalanceSettledTime(Long shopId);

	UpayBalance query(long merchantID, long shopID, long userID);

	UpayBalance update(UpayBalance upayBalance);

	/****
	 * 更新用户金额
	 * @param merchantID
	 * @param shopID
	 * @param userID
	 * @param taskBalance 减少的任务收益
	 * @param shopBalance 减少的推广收益
	 * @param openRedPackBalance 减少的拆红包收益
	 * @return
	 */
	int updateLessUserBalance(long merchantID, long shopID, long userID, BigDecimal taskBalance, BigDecimal shopBalance, BigDecimal openRedPackBalance);
}
