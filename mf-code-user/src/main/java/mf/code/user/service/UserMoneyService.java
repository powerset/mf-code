package mf.code.user.service;

import mf.code.common.simpleresp.SimpleResponse;

import java.util.Map;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-15 09:58
 */
public interface UserMoneyService {

	/***
	 * 用户提现-企业付款(公众号)
	 * @param map
	 * @return
	 */
	SimpleResponse createMktTransfers(Map<String, Object> map);

	/***
	 * 查询用户余额
	 * @param userID
	 * @return
	 */
	SimpleResponse queryUserBalance(Long merchantID, Long shopID, Long userID);

	/**
	 * 签署提现协议
	 * @param merchantId
	 * @param shopId
	 * @param userId
	 * @return
	 */
	SimpleResponse signingAgreement(Long merchantId, Long shopId, Long userId);

	/***
	 * 订单相关弹幕
	 * @param merchantId
	 * @param shopId
	 * @param userId
	 * @param type 1 提现
	 * @return
	 */
    SimpleResponse barrages(Long merchantId, Long shopId, Long userId, int type);
}
