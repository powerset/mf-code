package mf.code.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.OrderStatusEnum;
import mf.code.merchant.constants.OrderTypeEnum;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.common.caller.wxpay.WeixinPayConstants;
import mf.code.user.common.caller.wxpay.WeixinPayService;
import mf.code.user.common.constant.UserCashProtocalEnum;
import mf.code.user.common.property.AppProperty;
import mf.code.user.common.redis.RedisKeyConstant;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderMfTradeTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.UpayBalance;
import mf.code.user.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.UpayWxOrderService;
import mf.code.user.vo.UpayWxOrderAboutVO;
import mf.code.user.service.UpayBalanceService;
import mf.code.user.service.UserMoneyService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-15 09:58
 */
@Slf4j
@Service
public class UserMoneyServiceImpl implements UserMoneyService {
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private AppProperty appProperty;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private OneAppService oneAppService;

    /***
     * 企业付款-用户提现
     * @param map
     * @return
     */
    @Override
    public SimpleResponse createMktTransfers(Map<String, Object> map) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> respMap = new HashMap<String, Object>();
        //入参安全性验证
        if (!(StringUtils.isNotBlank(map.get("userId").toString()) && StringUtils.isNotBlank(map.get("amount").toString())
                && RegexUtils.StringIsNumber(map.get("userId").toString()) && RegexUtils.StringIsNumber(map.get("amount").toString())
                && StringUtils.isNotBlank(map.get("shopId").toString()) && RegexUtils.StringIsNumber(map.get("shopId").toString()))) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        /**
         * step1:基础数据梳理
         */
        Long userID = NumberUtils.toLong(map.get("userId").toString());
        Long shopID = NumberUtils.toLong(map.get("shopId").toString());
        // 提前--先校验是否签署协议， 再校验金额是否满足提现
        User user = this.userMapper.selectByPrimaryKey(userID);
        if (user == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            d.setMessage("该用户不存在");
            return d;
        }
		/*if (user.getCashProtocal() == null || user.getCashProtocal() != UserCashProtocalEnum.SIGN.getCode()) {
			d.setCode(20190415);
			d.setMessage("提现协议未签署");
			return d;
		}*/

        BigDecimal amount = new BigDecimal(map.get("amount").toString());
        if (amount.compareTo(BigDecimal.ZERO) == 0 || amount.compareTo(new BigDecimal("0.3")) < 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            d.setMessage("最低0.3元提现");
            return d;
        }

//		String str = "目前为测试阶段";
//		if(StringUtils.isNotBlank(str)){
//			d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
//			d.setMessage("请稍后...");
//			return d;
//		}

        //redis去重
        if (!this.stringRedisTemplate.opsForValue().setIfAbsent(RedisKeyConstant.USERPRESENTMONEY + userID, "")) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("微信提现正在触发，请勿重复");
            return d;
        }

        BigDecimal canCashMoney = BigDecimal.ZERO;
        try {

            Long merchantID = this.shopAppService.getMerchantIdByShopId(shopID);
            UpayBalance upayBalance = this.upayBalanceService.query(merchantID, shopID, userID);
            //用户的安全性验证
            if (user == null || merchantID == null || upayBalance == null) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                d.setMessage("该用户|店铺|余额 不存在");
                return d;
            }

            //余额的准确性处理 BigDecimal 0表示相等，-1表示小于，1表示大于
            //获取余额提现金额
            BigDecimal taskIncome = upayBalance.getBalance().setScale(2, BigDecimal.ROUND_DOWN);
            BigDecimal promotionIncome = upayBalance.getShoppingBalance().setScale(2, BigDecimal.ROUND_DOWN);
            BigDecimal openRedPackBalance = upayBalance.getOpenredpackBalance().setScale(2, BigDecimal.ROUND_DOWN);
            //是否带有解锁金额
            QueryWrapper<UpayWxOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.lambda()
                    .eq(UpayWxOrder::getShopId, upayBalance.getShopId())
                    .eq(UpayWxOrder::getUserId, upayBalance.getUserId())
                    .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                    .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
                    .in(UpayWxOrder::getBizType, Arrays.asList(
                            UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode(),UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode()
                    ))
                    .eq(UpayWxOrder::getMfTradeType, UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode())
            ;
            BigDecimal openRedPackLockSumMoney = BigDecimal.ZERO;
            UpayWxOrder upayWxOrder1 = upayWxOrderService.queryParams(orderQueryWrapper);
            if (upayWxOrder1 != null) {
                //锁定金额
                openRedPackLockSumMoney = upayWxOrder1.getTotalFee();
            }
            canCashMoney = taskIncome.add(promotionIncome).add(openRedPackBalance).subtract(openRedPackLockSumMoney);
            if (canCashMoney.compareTo(BigDecimal.ZERO) == 0 || canCashMoney.compareTo(amount) < 0) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                d.setMessage("提现余额和账户余额不匹配，提现余额：" + amount + "元，账户可提现余额：" + canCashMoney + "元");
                log.error("提现余额和账户余额不匹配，提现余额：{} 元，账户可提现余额：{}", amount, canCashMoney);
                return d;
            }

            /**
             * step2:调用微信企业支付接口
             */
            //提现描述
            String remark = appProperty.getMfName() + "-" + appProperty.getDesc() + userID;
            Map<String, Object> reqMapParams = this.weixinPayService.getTransferReqParams(user.getOpenId(), null, amount, remark, WeixinPayConstants.PAYSCENE_APPLET_APPID);
            Map<String, Object> xmlMap = this.weixinPayService.transfer(reqMapParams);
            log.info("用户提现-微信支付返回：xmlMap {}", xmlMap);
            //通信标识+交易标识  状态都成功时 做数据处理
            if (xmlMap == null) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
                return d;
            }
            if ((StringUtils.isNotBlank(xmlMap.get("return_code").toString()) && "FAIL".equals(xmlMap.get("return_code").toString().toUpperCase()))
                    || (StringUtils.isNotBlank(xmlMap.get("result_code").toString()) && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase()))) {
                if (StringUtils.isNotBlank(xmlMap.get("result_code").toString())
                        && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase())
                        && StringUtils.isNotBlank(xmlMap.get("err_code").toString().toUpperCase())
                        && "SYSTEMERROR".equals(xmlMap.get("err_code").toString().toUpperCase())) {//为这种错误时，需确认是否真的失败
                    boolean checkStatus = this.weixinPayService.checkTransfers(xmlMap.get("nonce_str").toString(), xmlMap.get("sign").toString(), xmlMap.get("partner_trade_no").toString());
                    if (!checkStatus) {
                        //TODO:提现失败，是重试，还是让用户重新走流程
                        d.setStatusEnum(ApiStatusEnum.ERROR_WXPAY);
                        d.setMessage("微信零钱出现异常，请重新发起提现请求");
                    }
                }
                d.setStatusEnum(ApiStatusEnum.ERROR_WXPAY);
                if (StringUtils.isNotBlank(xmlMap.get("return_msg").toString()) || StringUtils.isNotBlank(xmlMap.get("err_code_des").toString())) {
                    d.setMessage(StringUtils.isNotBlank(xmlMap.get("err_code_des").toString()) ? xmlMap.get("err_code_des").toString() : xmlMap.get("return_msg").toString());
                }
                return d;
            }
            //插入成功，设置过期时间
            stringRedisTemplate.expire(RedisKeyConstant.USERPRESENTMONEY + userID, 4, TimeUnit.SECONDS);
            /**
             * step3:db处理  订单表的处理 订单表-的创建-用户余额的更新
             */
            //订单表的创建
            String orderName = user.getNickName() + "余额提现";
            UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(userID, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.USERCASH.getCode(),
                    xmlMap.get("partner_trade_no").toString(), orderName, merchantID, shopID,
                    OrderStatusEnum.ORDERED.getCode(), amount, IpUtil.getServerIp(), xmlMap.get("payment_no").toString(), remark,
                    DateUtil.parseDate(xmlMap.get("payment_time").toString(), null, "yyyy-MM-dd HH:mm:ss"),
                    null, JSON.toJSONString(reqMapParams), null, null);
            this.upayWxOrderService.create(upayWxOrder);
            //更新余额表
            this.upayBalanceService.updateLessUserBalance(merchantID, shopID, userID, taskIncome, promotionIncome, openRedPackBalance.subtract(openRedPackLockSumMoney));
        } finally {
            //结束时，对key进行删除处理
            this.stringRedisTemplate.delete(RedisKeyConstant.USERPRESENTMONEY + userID);
        }

        //拆红包是否存在,提现成功后的拆红包弹窗@百川
        Map<String, Object> openRedPackageMap = oneAppService.queryOpenRedPackageStatus(shopID, userID);
        if (openRedPackageMap != null) {
            openRedPackageMap.put("cashAmount", canCashMoney);
            d.setData(openRedPackageMap);
        }

        return d;
    }

    /***
     * 查询用户余额
     * @param userID
     * @return
     */
    @Override
    public SimpleResponse queryUserBalance(Long merchantID, Long shopID, Long userID) {
        SimpleResponse d = new SimpleResponse();

        if (userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        Map<String, Object> resp = new HashMap<String, Object>();
        UpayBalance upayBalance = this.upayBalanceService.query(merchantID, shopID, userID);
        if (upayBalance == null) {
            resp.put("balance", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("taskIncome", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("promotionIncome", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("openRedpackBalance", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("lockMoney", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("selfPurchaseRebate", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("shareCommission", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("taskAward", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.put("taskCommission", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            d.setData(resp);
            return d;
        }
        BigDecimal taskIncome = upayBalance.getBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal promotionIncome = upayBalance.getShoppingBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal openRedPackBalance = upayBalance.getOpenredpackBalance().setScale(2, BigDecimal.ROUND_DOWN);

        //是否带有解锁金额
        QueryWrapper<UpayWxOrder> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.lambda()
                .eq(UpayWxOrder::getShopId, shopID)
                .eq(UpayWxOrder::getUserId, userID)
                .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
                .in(UpayWxOrder::getBizType,
                        Arrays.asList(UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode(),
                                UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode()))
                .eq(UpayWxOrder::getMfTradeType, UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode())
        ;
        UpayWxOrder upayWxOrder = upayWxOrderService.queryParams(orderQueryWrapper);
        resp.put("lockMoney", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
        if (upayWxOrder != null) {
            //锁定金额
            BigDecimal openRedPackLockSumMoney = upayWxOrder.getTotalFee();
            resp.put("lockMoney", openRedPackLockSumMoney);
        }
        //自购省收益汇总
        BigDecimal selfPurchaseRebate = upayWxOrderService.sumTotalFeeByBizTypes(shopID, userID, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode(), UpayWxOrderAboutVO.addSelfPurchaseRebate());
        //分享赚收益汇总
        BigDecimal shareCommission = upayWxOrderService.sumTotalFeeByBizTypes(shopID, userID, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode(), UpayWxOrderAboutVO.addShareCommission());
        //任务奖金汇总
        BigDecimal taskAward = upayWxOrderService.sumTotalFeeByBizTypes(shopID, userID, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode(), UpayWxOrderAboutVO.addTaskAward());
        //任务佣金汇总
        BigDecimal taskCommission = upayWxOrderService.sumTotalFeeByBizTypes(shopID, userID, UpayWxOrderTypeEnum.REBATE.getCode(), UpayWxOrderAboutVO.addTaskCommission());

        resp.put("selfPurchaseRebate", selfPurchaseRebate);
        resp.put("shareCommission", shareCommission);
        resp.put("taskAward", taskAward);
        resp.put("taskCommission", taskCommission);

        resp.put("taskIncome", taskIncome);
        resp.put("promotionIncome", promotionIncome);
        resp.put("openRedpackBalance", openRedPackBalance);
        resp.put("balance", taskIncome.add(promotionIncome).add(openRedPackBalance));
        d.setData(resp);
        return d;
    }

    @Override
    public SimpleResponse signingAgreement(Long merchantId, Long shopId, Long userId) {
        User user = this.userMapper.selectById(userId);
        if (user == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "用户不存在");
        }
        user.setCashProtocal(UserCashProtocalEnum.SIGN.getCode());
        int rows = this.userMapper.updateByPrimaryKeySelective(user);
        if (rows == 0) {
            log.error("数据库更新失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
        }
        return new SimpleResponse();
    }

    /***
     * 订单相关弹幕
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 1 提现
     * @return
     */
    @Override
    public SimpleResponse barrages(Long merchantId, Long shopId, Long userId, int type) {
        QueryWrapper<UpayWxOrder> queryWrapper = new QueryWrapper<>();
        Page<UpayWxOrder> page = new Page<>(1, 20);
        queryWrapper.lambda()
                .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.USERCASH.getCode())
                .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
        ;
        queryWrapper.orderByDesc("id");
        IPage<UpayWxOrder> iPage = upayWxOrderService.page(page, queryWrapper);
        List<UpayWxOrder> upayWxOrders = iPage.getRecords();

        SimpleResponse simpleResponse = new SimpleResponse();
        Map resp = new HashMap();
        resp.put("barrages", new ArrayList<>());
        simpleResponse.setData(resp);
        if (CollectionUtils.isEmpty(upayWxOrders)) {
            return simpleResponse;
        }
        List<Long> userIds = new ArrayList<>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            if (userIds.indexOf(upayWxOrder.getUserId()) == -1) {
                userIds.add(upayWxOrder.getUserId());
            }
        }
        List<User> users = userMapper.selectBatchIds(userIds);
        if (CollectionUtils.isEmpty(users)) {
            return simpleResponse;
        }
        Map<Long, User> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.getId(), user);
        }
        List<Object> list = new ArrayList<>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            User user = userMap.get(upayWxOrder.getUserId());
            if (user == null) {
                log.error("<<<<<<<弹幕异常，该用户不存在，userId={}", upayWxOrder.getUserId());
                continue;
            }
            Map map = new HashMap();
            map.put("nickName", user.getNickName());
            map.put("avatarUrl", user.getAvatarUrl());
            map.put("money", upayWxOrder.getTotalFee());
            list.add(map);
        }
        resp.put("barrages", list);
        simpleResponse.setData(resp);
        return simpleResponse;
    }
}
