package mf.code.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityStatusEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.one.redis.RedisKeyConstant;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.user.api.feignclient.GoodsAppService;
import mf.code.user.api.feignclient.OneAppService;
import mf.code.user.api.feignclient.OrderAppService;
import mf.code.user.constant.*;
import mf.code.user.dto.CashSuccessDialogDTO;
import mf.code.user.repo.po.UpayBalance;
import mf.code.user.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.UpayWxOrderService;
import mf.code.user.repo.repository.UserRepository;
import mf.code.user.service.MktTransFersAboutService;
import mf.code.user.service.MktTransfersService;
import mf.code.user.service.UpayBalanceService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.user.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月14日 21:40
 */
@Service
@Slf4j
public class MktTransfersServiceImpl implements MktTransfersService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MktTransFersAboutService mktTransFersAboutService;
    @Autowired
    private OneAppService oneAppService;

    /***
     * 是否有带解锁金额，是否有下单记录，是否有未支付商品订单
     * @param userID
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse checkLockMoney(Long userID, Long shopId, Long merchantId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resp = new HashMap<>();
        resp.put("dialogs", new ArrayList<>());
        simpleResponse.setData(resp);
        //是否带有解锁金额
        QueryWrapper<UpayWxOrder> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.lambda()
                .eq(UpayWxOrder::getShopId, shopId)
                .eq(UpayWxOrder::getUserId, userID)
                .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
                .in(UpayWxOrder::getBizType, Arrays.asList(UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode(),
                        UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode()))
                .eq(UpayWxOrder::getMfTradeType, UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode())
        ;
        orderQueryWrapper.orderByAsc("id");
        UpayWxOrder upayWxOrder = upayWxOrderService.queryParams(orderQueryWrapper);
        if (upayWxOrder == null) {
            return simpleResponse;
        }

        //锁定金额
        BigDecimal openRedPackLockSumMoney = upayWxOrder.getTotalFee();
        //获取用户余额
        UpayBalance upayBalance = upayBalanceService.query(merchantId, shopId, userID);
        if (upayBalance == null) {
            return simpleResponse;
        }

        BigDecimal taskIncome = upayBalance.getBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal promotionIncome = upayBalance.getShoppingBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal openRedPackBalance = upayBalance.getOpenredpackBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal balance = taskIncome.add(promotionIncome).add(openRedPackBalance);

        //可提金额
        BigDecimal canCashBalance = balance.subtract(openRedPackLockSumMoney);

        boolean canCash = canCashBalance != null && canCashBalance.compareTo(BigDecimal.ZERO) > 0;
        boolean existLockMoney = openRedPackLockSumMoney != null && openRedPackLockSumMoney.compareTo(BigDecimal.ZERO) > 0;
        log.info("<<<<<<<<可提金额-canCashBalance={}, 待解锁金额-lockMoney={}", canCashBalance, openRedPackLockSumMoney);
        List<Object> list = new ArrayList();
        if (canCash && existLockMoney) {
            //x元可提，y元待解锁
            Map<String, Object> map = new HashMap<>();
            map.put("lockMoney", openRedPackLockSumMoney);
            map.put("canCashMoney", canCashBalance);
            //去解锁
            map.put("status", 0);
            list.add(map);
        }

        OrderResp orderResp = orderAppService.selectOrder(shopId, userID, DateFormatUtils.format(upayWxOrder.getCtime(), "yyyyMMddHHmmss"));
        log.info("<<<<<<<<查詢是否有商品订单信息：order:{}", orderResp);
        if (orderResp == null || orderResp.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            //去下单 弹窗
            Map<String, Object> map1 = new HashMap<>();
            map1.put("lockMoney", openRedPackLockSumMoney);
            //去下单
            map1.put("status", 1);
            map1.put("pageType", 2);
            //跳转页面 首页 或者 新人专享列表
            //获取新人专享的商品集合
            List<Long> productIds = goodsAppService.getProductIds(shopId, ProductShowTypeEnum.NEWBIE.getCode());
            if (!CollectionUtils.isEmpty(productIds)) {
                //查询新人专享是否下过单
                Integer num = orderAppService.queryOrderNumByProductIds(shopId, userID, productIds);
                if (num != null && num > 0) {
                    map1.put("pageType", 1);
                }
            }
            list.add(map1);
        }

        if (orderResp != null && orderResp.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            //去支付弹窗
            Map<String, Object> map2 = new HashMap<>();
            map2.put("lockMoney", openRedPackLockSumMoney);
            //去支付
            map2.put("status", 2);
            list.add(map2);
        }

        log.info("<<<<<<<< 提现弹窗汇总:{}", JSONObject.toJSONString(list));
        resp.put("dialogs", list);
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 提现成功后的控制弹窗
     * <pre>
     *
     * </pre>
     * @param shopID
     * @param userID
     * @return
     */
    @Override
    public SimpleResponse queryMktTransferSuccessDialog(Long merchantId, Long shopID, Long userID, Integer source) {
        if (merchantId == null || merchantId == 0 || shopID == null || shopID == 0 || userID == null || userID == 0) {
            log.error("<<<<<<<<提现成功后的弹窗-入参获取异常, mid:{} shopId:{} userId:{}", merchantId, shopID, userID);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, new CashSuccessDialogDTO());
        }
        User user = userRepository.selectById(userID);
        if (user == null) {
            log.error("<<<<<<<<提现成功后的弹窗-获取用户信息错误..., user:{} userId:{}", user, userID);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, new CashSuccessDialogDTO());
        }

        //完成任务的redisKey
        String redisKey = RedisKeyConstant.FINISH_DONE_TASK_ZSET + userID;
        // 分页获取 此用户真实的好友收益排行榜
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(redisKey, 0, -1);
        if (CollectionUtils.isEmpty(typedTuples)) {
            log.error("提现成功后的弹窗-完成任务的redis数据为空");
            return new SimpleResponse(ApiStatusEnum.SUCCESS, new CashSuccessDialogDTO());
        }

        List<Integer> newbieTasks = Arrays.asList(
                UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode(),
                UpayWxOrderBizTypeEnum.NEWBIE_TASK_BONUS.getCode(),
                UpayWxOrderBizTypeEnum.NEWBIE_TASK_REDPACK.getCode()
        );
        int newbieTaskNum = 0;
        boolean existOpenRedPack = false;
        boolean full_reimbursement = false;
        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            String value = typedTuple.getValue();
            if (StringUtils.isNotBlank(value)) {
                //判断是否含有拆红包
                Integer bizType = Integer.valueOf(value);
                if (bizType == UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode()) {
                    existOpenRedPack = true;
                    break;
                }
                if (bizType == UpayWxOrderBizTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                    full_reimbursement = true;
                    break;
                }
                if (newbieTasks.contains(bizType)) {
                    newbieTaskNum++;
                }
            }
        }
        //是否有拆红包||报销活动 进入是否成为店长弹窗流程
        if (existOpenRedPack || full_reimbursement) {
            SimpleResponse simpleResponse = mktTransFersAboutService.cashSuccessDialogExistOpenRedPack(merchantId, shopID, user);
            //删除redis
            stringRedisTemplate.delete(redisKey);
            return simpleResponse;
        }

        //判断是否有进行中的报销活动
        if (!full_reimbursement) {
            Map<String, Object> map = oneAppService.queryFullReimbursementInfo(shopID, userID);
            if (!CollectionUtils.isEmpty(map)) {
                map.put("type", CashSuccessDialogEnum.FULL_REIMBURSEMENT.getCode());
                return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
            }
        }

        //新手任务完成两个
        if (newbieTaskNum >= 2) {
            SimpleResponse simpleResponse = mktTransFersAboutService.cashSuccessDialogNewbiwTaskNumUp();
            //删除redis
            stringRedisTemplate.delete(redisKey);
            return simpleResponse;
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, new CashSuccessDialogDTO());
    }
}
