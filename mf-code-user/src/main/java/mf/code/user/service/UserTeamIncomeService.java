package mf.code.user.service;

import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.user.domain.aggregateroot.AppletUser;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-31 21:49
 */
public interface UserTeamIncomeService {

	/**
	 * 获取 此商城 我的团队 上级用户id
	 * @return
	 */
	Long getUpperId(Long userId, Long shopId);

	/**
	 * 获取 此商城 我的团队 所有上级ids
	 * @param userId
	 * @param vipShopId
	 * @param scale
	 * @return
	 */
	List<Long> getUpperIds(Long userId, Long vipShopId, Integer scale);

	/**
	 * ###################################################################
	 */

	/**
	 * 获取 当前商城的用户的所属上级成员
	 * @param vipShopId
	 * @param userId
	 * @param scale
	 * @return JSON.toJSONString(Map<level, UserId>)
	 */
	Map<Integer, Long> queryUpperMember(Long vipShopId, Long userId, Integer scale);

	/**
	 * 获取 当前商城的用户的所属上级成员
	 * @param shopId
	 * @param userId
	 * @param scale
	 * @return JSON.toJSONString(Map<level, List<UserId>>)
	 */
	Map<Integer, List<Long>> queryLowerMember(Long shopId, Long userId, Integer scale);
}
