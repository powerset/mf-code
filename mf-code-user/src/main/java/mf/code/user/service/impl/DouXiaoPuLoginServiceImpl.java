package mf.code.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.TokenUtil;
import mf.code.user.api.applet.dto.WxUserSession;
import mf.code.user.common.property.AppProperty;
import mf.code.user.common.redis.RedisForbidRepeat;
import mf.code.user.common.redis.RedisKeyConstant;
import mf.code.user.domain.aggregateroot.DouXiaoPuUser;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.DouXiaoPuUserRepository;
import mf.code.user.service.DouXiaoPuLoginService;
import mf.code.user.service.DouYinUserVerifyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 15:57
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class DouXiaoPuLoginServiceImpl implements DouXiaoPuLoginService {
    private final DouYinUserVerifyService douYinUserVerifyService;
    private final AppProperty appProperty;
    private final StringRedisTemplate stringRedisTemplate;
    private final DouXiaoPuUserRepository douXiaoPuUserRepository;


    /**
     * 抖小铺 用户登录
     * @param wxUserSession
     * @return
     */
    @Override
    public SimpleResponse login(WxUserSession wxUserSession) {
        // 校验session 有效性
        SimpleResponse verifyResult = douYinUserVerifyService.loginVerify(wxUserSession, appProperty.getDouXiaoPuAppId(), appProperty.getDouXiaoPuSecret());
        if (verifyResult.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            log.error("登录校验异常");
            return verifyResult;
        }
        JSONObject verifyResultData = JSON.parseObject(verifyResult.getData().toString());
        // 获取用户信息
        String openId = verifyResultData.getString("openid");
        // 加锁
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openId);
        if (!success) {
            log.error("redis 登录防重, openid = {}", openId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "正在登录中……");
        }
        // 获取用户
        DouXiaoPuUser douXiaoPuUser = douXiaoPuUserRepository.findByLogin(verifyResultData);
        if (douXiaoPuUser == null) {
            log.info("用户未授权");
            stringRedisTemplate.delete(RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "用户未授权");
        }
        boolean save = douXiaoPuUserRepository.saveByLogin(douXiaoPuUser);
        if (!save) {
            log.error("保存或更新用户失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
        }
        // 解锁
        stringRedisTemplate.delete(RedisKeyConstant.MF_APPLETUSER_LOGIN_FORBID_REPEAT + openId);

        User userInfo = douXiaoPuUser.getUserInfo();

        // 获取用户登录信息
        Map<String, Object> userLoginInfo = new HashMap<>();
        // 生成用户令牌
        String token = TokenUtil.encryptToken(userInfo.getId().toString(), userInfo.getOpenId(), userInfo.getCtime(), TokenUtil.DOU_XIAO_PU);
        log.info("<<<<<<<<<<<< 登录时生成的token = {} <<<<<<<<<<<", token);
        userLoginInfo.put("token", token);
        userLoginInfo.put("userId", userInfo.getId());
        userLoginInfo.put("nickName", userInfo.getNickName());
        userLoginInfo.put("mobile", userInfo.getMobile());
        userLoginInfo.put("avatarUrl", userInfo.getAvatarUrl());
        userLoginInfo.put("shopId", userInfo.getShopId());
        userLoginInfo.put("merchantId", userInfo.getMerchantId());

        return new SimpleResponse(ApiStatusEnum.SUCCESS, userLoginInfo);
    }
}
