package mf.code.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.user.repo.po.User;

/**
 * create by qc on 2019/8/9 0009
 */
public interface UserInfoService extends IService<User> {

}
