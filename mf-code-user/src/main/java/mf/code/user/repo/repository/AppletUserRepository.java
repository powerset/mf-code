package mf.code.user.repo.repository;

import com.alibaba.fastjson.JSONObject;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.UserAddress;

import java.util.List;

/**
 * mf.code.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-27 11:07
 */
public interface AppletUserRepository {

	/**
	 * 通过openid查询用户
	 * @param openid openid
	 * @return UserAggregaterRoot
	 */
	AppletUser findByOpenid(String openid);

	/**
	 * 根据主键，保存或更新用户信息
	 * @param appletUser
	 * @return
	 */
	boolean saveOrUpdateUserInfo(AppletUser appletUser);

	/**
	 * 通过uid查询用户
	 * @param userId
	 * @return
	 */
	AppletUser findById(Long userId);

	/**
	 * 通过userIds 批量查询用户
	 * @param userIds
	 * @return
	 */
	List<AppletUser> listByIds(List<Long> userIds);

	/**
	 * 增装 用户收益 到 appUser
	 * @param appletUser
	 * @param shopId
	 */
	void addUserIncome(AppletUser appletUser, Long shopId);

	/**
	 * 批量 增装 用户收益 到 appUser
	 * @param appletUsers
	 */
	void listAddUserIncome(List<AppletUser> appletUsers, Long shopId);

	/**
	 * 获取平台用户
	 * @return
	 */
	AppletUser findPlatform();

	/**
	 * 获取带有收获地址信息的用户
	 * @param uid
	 * @return
	 */
	AppletUser findByIdWithAddress(Long uid);

	/**
	 * 保存、更新或删除 用户地址
	 * @param userAddress userAddress
	 * @return userAddress
	 */
	UserAddress saveOrUpdateAddress(UserAddress userAddress);

	/**
	 * 更新用户信息
	 * @param appletUser
	 */
	boolean updateUserInfo(AppletUser appletUser);

	/**
	 * 获取 店铺粉丝数量
	 * @param appletUser
	 * @return
	 */
	Integer getShopFans(AppletUser appletUser);
}
