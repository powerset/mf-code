package mf.code.user.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * upay_wx_order
 * 用户订单表，jkmfv1.0创建
 */
public class UpayWxOrder implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 商户号 冗余字段
     */
    private Long mchId;

    /**
     * 店铺编号
     */
    private Long shopId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单类型 0：用户提现 1：平台金额充值 2:平台退款3:用户支付 4:商城返利及分佣
     */
    private Integer type;

    /**
     * 支付方式：1:余额 2:微信 
     */
    private Integer payType;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 小程序appid 冗余字段
     */
    private String appletAppId;

    /**
     * 订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）
     */
    private Integer status;

    /**
     * 总金额,单位元
     */
    private BigDecimal totalFee;

    /**
     * 交易方式：1：JSAPI--公众号支付|小程序支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    private Integer tradeType;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 流水号
     */
    private String tradeId;

    /**
     * 备注说明
     */
    private String remark;

    /**
     * 回调处理系统时间
     */
    private Date notifyTime;

    /**
     * 响应数据中的支付时间
     */
    private Date paymentTime;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 订单入参，json对象
     */
    private String reqJson;

    /**
     * 业务类型，1:"活动计划类"),2:"轮盘抽奖"),3:"回填订单"),4:"收藏加购"),5:"好评返现"),6:"拆红包活动"),7:"助力活动"),8:"免单抽奖"),9:"新人有礼"), 10:财富大闯关, 11:购买返利, 12:团队贡献
     */
    private Integer bizType;

    /**
     * 业务类型针对的值1:活动编号，2:活动定义编号3:活动任务编号4活动任务编号5活动任务编号6活动编号7活动编号8活动编号9活动编号
     */
    private Long bizValue;

    /**
     * 当交易方式为JSAPI的时候，1:为公众号支付 2:小程序支付
     */
    private Integer jsapiScene;

    /**
     * 退款时，需要退款的订单主键编号
     */
    private Long refundOrderId;

    /**
     * 魔方交易方式:1待结算 2已结算
     */
    private Integer mfTradeType;

    /**
     * upay_wx_order
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户号 冗余字段
     * @return mch_id 商户号 冗余字段
     */
    public Long getMchId() {
        return mchId;
    }

    /**
     * 商户号 冗余字段
     * @param mchId 商户号 冗余字段
     */
    public void setMchId(Long mchId) {
        this.mchId = mchId;
    }

    /**
     * 店铺编号
     * @return shop_id 店铺编号
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺编号
     * @param shopId 店铺编号
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 订单类型 0：用户提现 1：平台金额充值 2:平台退款3:用户支付 4:商城返利及分佣
     * @return type 订单类型 0：用户提现 1：平台金额充值 2:平台退款3:用户支付 4:商城返利及分佣
     */
    public Integer getType() {
        return type;
    }

    /**
     * 订单类型 0：用户提现 1：平台金额充值 2:平台退款3:用户支付 4:商城返利及分佣
     * @param type 订单类型 0：用户提现 1：平台金额充值 2:平台退款3:用户支付 4:商城返利及分佣
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 支付方式：1:余额 2:微信 
     * @return pay_type 支付方式：1:余额 2:微信 
     */
    public Integer getPayType() {
        return payType;
    }

    /**
     * 支付方式：1:余额 2:微信 
     * @param payType 支付方式：1:余额 2:微信 
     */
    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    /**
     * 订单编号
     * @return order_no 订单编号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 订单编号
     * @param orderNo 订单编号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 订单名称
     * @return order_name 订单名称
     */
    public String getOrderName() {
        return orderName;
    }

    /**
     * 订单名称
     * @param orderName 订单名称
     */
    public void setOrderName(String orderName) {
        this.orderName = orderName == null ? null : orderName.trim();
    }

    /**
     * 小程序appid 冗余字段
     * @return applet_app_id 小程序appid 冗余字段
     */
    public String getAppletAppId() {
        return appletAppId;
    }

    /**
     * 小程序appid 冗余字段
     * @param appletAppId 小程序appid 冗余字段
     */
    public void setAppletAppId(String appletAppId) {
        this.appletAppId = appletAppId == null ? null : appletAppId.trim();
    }

    /**
     * 订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）
     * @return status 订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）
     * @param status 订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 总金额,单位元
     * @return total_fee 总金额,单位元
     */
    public BigDecimal getTotalFee() {
        return totalFee;
    }

    /**
     * 总金额,单位元
     * @param totalFee 总金额,单位元
     */
    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    /**
     * 交易方式：1：JSAPI--公众号支付|小程序支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     * @return trade_type 交易方式：1：JSAPI--公众号支付|小程序支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    public Integer getTradeType() {
        return tradeType;
    }

    /**
     * 交易方式：1：JSAPI--公众号支付|小程序支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     * @param tradeType 交易方式：1：JSAPI--公众号支付|小程序支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    /**
     * ip地址
     * @return ip_address ip地址
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * ip地址
     * @param ipAddress ip地址
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress == null ? null : ipAddress.trim();
    }

    /**
     * 流水号
     * @return trade_id 流水号
     */
    public String getTradeId() {
        return tradeId;
    }

    /**
     * 流水号
     * @param tradeId 流水号
     */
    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    /**
     * 备注说明
     * @return remark 备注说明
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注说明
     * @param remark 备注说明
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 回调处理系统时间
     * @return notify_time 回调处理系统时间
     */
    public Date getNotifyTime() {
        return notifyTime;
    }

    /**
     * 回调处理系统时间
     * @param notifyTime 回调处理系统时间
     */
    public void setNotifyTime(Date notifyTime) {
        this.notifyTime = notifyTime;
    }

    /**
     * 响应数据中的支付时间
     * @return payment_time 响应数据中的支付时间
     */
    public Date getPaymentTime() {
        return paymentTime;
    }

    /**
     * 响应数据中的支付时间
     * @param paymentTime 响应数据中的支付时间
     */
    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 订单入参，json对象
     * @return req_json 订单入参，json对象
     */
    public String getReqJson() {
        return reqJson;
    }

    /**
     * 订单入参，json对象
     * @param reqJson 订单入参，json对象
     */
    public void setReqJson(String reqJson) {
        this.reqJson = reqJson == null ? null : reqJson.trim();
    }

    /**
     * 业务类型，1:"活动计划类"),2:"轮盘抽奖"),3:"回填订单"),4:"收藏加购"),5:"好评返现"),6:"拆红包活动"),7:"助力活动"),8:"免单抽奖"),9:"新人有礼"), 10:财富大闯关, 11:购买返利, 12:团队贡献
     * @return biz_type 业务类型，1:"活动计划类"),2:"轮盘抽奖"),3:"回填订单"),4:"收藏加购"),5:"好评返现"),6:"拆红包活动"),7:"助力活动"),8:"免单抽奖"),9:"新人有礼"), 10:财富大闯关, 11:购买返利, 12:团队贡献
     */
    public Integer getBizType() {
        return bizType;
    }

    /**
     * 业务类型，1:"活动计划类"),2:"轮盘抽奖"),3:"回填订单"),4:"收藏加购"),5:"好评返现"),6:"拆红包活动"),7:"助力活动"),8:"免单抽奖"),9:"新人有礼"), 10:财富大闯关, 11:购买返利, 12:团队贡献
     * @param bizType 业务类型，1:"活动计划类"),2:"轮盘抽奖"),3:"回填订单"),4:"收藏加购"),5:"好评返现"),6:"拆红包活动"),7:"助力活动"),8:"免单抽奖"),9:"新人有礼"), 10:财富大闯关, 11:购买返利, 12:团队贡献
     */
    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    /**
     * 业务类型针对的值1:活动编号，2:活动定义编号3:活动任务编号4活动任务编号5活动任务编号6活动编号7活动编号8活动编号9活动编号
     * @return biz_value 业务类型针对的值1:活动编号，2:活动定义编号3:活动任务编号4活动任务编号5活动任务编号6活动编号7活动编号8活动编号9活动编号
     */
    public Long getBizValue() {
        return bizValue;
    }

    /**
     * 业务类型针对的值1:活动编号，2:活动定义编号3:活动任务编号4活动任务编号5活动任务编号6活动编号7活动编号8活动编号9活动编号
     * @param bizValue 业务类型针对的值1:活动编号，2:活动定义编号3:活动任务编号4活动任务编号5活动任务编号6活动编号7活动编号8活动编号9活动编号
     */
    public void setBizValue(Long bizValue) {
        this.bizValue = bizValue;
    }

    /**
     * 当交易方式为JSAPI的时候，1:为公众号支付 2:小程序支付
     * @return jsapi_scene 当交易方式为JSAPI的时候，1:为公众号支付 2:小程序支付
     */
    public Integer getJsapiScene() {
        return jsapiScene;
    }

    /**
     * 当交易方式为JSAPI的时候，1:为公众号支付 2:小程序支付
     * @param jsapiScene 当交易方式为JSAPI的时候，1:为公众号支付 2:小程序支付
     */
    public void setJsapiScene(Integer jsapiScene) {
        this.jsapiScene = jsapiScene;
    }

    /**
     * 退款时，需要退款的订单主键编号
     * @return refund_order_id 退款时，需要退款的订单主键编号
     */
    public Long getRefundOrderId() {
        return refundOrderId;
    }

    /**
     * 退款时，需要退款的订单主键编号
     * @param refundOrderId 退款时，需要退款的订单主键编号
     */
    public void setRefundOrderId(Long refundOrderId) {
        this.refundOrderId = refundOrderId;
    }

    /**
     * 魔方交易方式:1待结算 2已结算
     * @return mf_trade_type 魔方交易方式:1待结算 2已结算
     */
    public Integer getMfTradeType() {
        return mfTradeType;
    }

    /**
     * 魔方交易方式:1待结算 2已结算
     * @param mfTradeType 魔方交易方式:1待结算 2已结算
     */
    public void setMfTradeType(Integer mfTradeType) {
        this.mfTradeType = mfTradeType;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", mchId=").append(mchId);
        sb.append(", shopId=").append(shopId);
        sb.append(", userId=").append(userId);
        sb.append(", type=").append(type);
        sb.append(", payType=").append(payType);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", orderName=").append(orderName);
        sb.append(", appletAppId=").append(appletAppId);
        sb.append(", status=").append(status);
        sb.append(", totalFee=").append(totalFee);
        sb.append(", tradeType=").append(tradeType);
        sb.append(", ipAddress=").append(ipAddress);
        sb.append(", tradeId=").append(tradeId);
        sb.append(", remark=").append(remark);
        sb.append(", notifyTime=").append(notifyTime);
        sb.append(", paymentTime=").append(paymentTime);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", reqJson=").append(reqJson);
        sb.append(", bizType=").append(bizType);
        sb.append(", bizValue=").append(bizValue);
        sb.append(", jsapiScene=").append(jsapiScene);
        sb.append(", refundOrderId=").append(refundOrderId);
        sb.append(", mfTradeType=").append(mfTradeType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}