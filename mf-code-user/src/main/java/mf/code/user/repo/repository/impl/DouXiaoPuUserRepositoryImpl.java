package mf.code.user.repo.repository.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.utils.RegexUtils;
import mf.code.merchant.constants.MerchantRoleEnum;
import mf.code.merchant.constants.MerchantTypeEnum;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.domain.aggregateroot.DouDaiDaiUser;
import mf.code.user.domain.aggregateroot.DouXiaoPuUser;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.DouXiaoPuUserRepository;
import mf.code.user.repo.repository.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-08 22:50
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class DouXiaoPuUserRepositoryImpl implements DouXiaoPuUserRepository {
    private final UserRepository userRepository;

    /**
     * 登录时 获取用户信息
     * @param verifyResultData
     * @return
     */
    @Override
    public DouXiaoPuUser findByLogin(JSONObject verifyResultData) {
        // 获取用户信息
        String openId = verifyResultData.getString("openid");
        JSONObject userInfoJson = verifyResultData.getJSONObject("userInfo");
        String merchantId = verifyResultData.getString("merchantId");
        String shopId = verifyResultData.getString("shopId");
        String scene = verifyResultData.getString("scene");

        // 获取user表的映射信息
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(User::getOpenId, openId)
                .eq(User::getRole, UserRoleEnum.DOU_XIAO_PU.getCode());
        User user = userRepository.getOne(wrapper);

        if (user == null) {
            if (userInfoJson == null) {
                log.info("用户未授权");
                return null;
            } else {
                // 新增用户
                user = this.newUser(verifyResultData);
            }
        }
        if (StringUtils.isNotBlank(merchantId) && RegexUtils.StringIsNumber(merchantId)) {
            user.setMerchantId(Long.valueOf(merchantId));
        }
        if (StringUtils.isNotBlank(shopId) && RegexUtils.StringIsNumber(shopId)) {
            user.setShopId(Long.valueOf(shopId));
            if (user.getVipShopId() == null) {
                user.setVipShopId(Long.valueOf(shopId));
            }
        }
        if (StringUtils.isNotBlank(scene) && RegexUtils.StringIsNumber(scene)) {
            user.setScene(Integer.valueOf(scene));
        }
        user.setUtime(new Date());

        DouXiaoPuUser douXiaoPuUser = new DouXiaoPuUser();
        douXiaoPuUser.setUserId(user.getId());
        douXiaoPuUser.setUserInfo(user);

        return douXiaoPuUser;
    }

    /**
     * 登录时  保存用户信息
     * @param douXiaoPuUser
     * @return
     */
    @Override
    public boolean saveByLogin(DouXiaoPuUser douXiaoPuUser) {
        User userInfo = douXiaoPuUser.getUserInfo();
        // 更新或保存抖带带主播user表的映射信息
        boolean saveOrUpdate = userRepository.saveOrUpdate(userInfo);
        if (!saveOrUpdate) {
            log.error("数据库异常，更新或保存抖带带主播user表的映射信息失败");
            throw new RuntimeException();
        }
        return true;
    }

    /**
     * 通过 userId 获取用户信息
     * @param userId
     * @return
     */
    @Override
    public DouXiaoPuUser findById(Long userId) {
        User user = userRepository.getById(userId);
        if (user == null) {
            log.error("查无此用户, userId = {}", userId);
            return null;
        }
        DouXiaoPuUser douXiaoPuUser = new DouXiaoPuUser();
        douXiaoPuUser.setUserId(userId);
        douXiaoPuUser.setUserInfo(user);
        return douXiaoPuUser;
    }

    private User newUser(JSONObject verifyResultData) {
        String openid = verifyResultData.getString("openid");
        JSONObject userInfo = verifyResultData.getJSONObject("userInfo");
        Date now = new Date(System.currentTimeMillis() / 1000 * 1000);

        User user = new User();
        user.setOpenId(openid);
        user.setNickName(userInfo.getString("nickName"));
        user.setAvatarUrl(userInfo.getString("avatarUrl"));
        user.setRole(UserRoleEnum.DOU_XIAO_PU.getCode());
        user.setRoleTime(now);
        user.setGrantStatus(1);
        user.setGrantTime(now);
        user.setUtime(now);
        user.setCtime(now);

        return user;
    }
}
