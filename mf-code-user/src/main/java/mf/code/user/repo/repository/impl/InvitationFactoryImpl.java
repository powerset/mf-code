package mf.code.user.repo.repository.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.domain.aggregateroot.InvitationEvent;
import mf.code.user.domain.valueobject.specification.InvitationSpecification;
import mf.code.user.repo.po.UserPubJoin;
import mf.code.user.repo.repository.InvitationEventRepository;
import mf.code.user.repo.repository.InvitationFactory;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * mf.code.user.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 11:37
 */
@Slf4j
@Service
public class InvitationFactoryImpl implements InvitationFactory {
	@Autowired
	private InvitationEventRepository invitationEventRepository;
	@Autowired
	private RocketMQTemplate rocketMQTemplate;

	/**
	 * 建立邀请关系
	 * @param appletPubUser
	 * @param appletSubUser
	 * @param invitationSpecification
	 */
	@Override
	public InvitationEvent newInvitationEvent(AppletUser appletPubUser, AppletUser appletSubUser, InvitationSpecification invitationSpecification) {
		// 检验参数
		if (appletPubUser == null || appletSubUser == null || invitationSpecification == null
				|| appletPubUser.getId().equals(appletSubUser.getId())) {
			log.info("非邀请登录");
			return null;
		}
		Long pubUid = appletPubUser.getId();
		Long subUid = appletSubUser.getId();

		// 根据邀请向量 查询出的 邀请记录或被邀请记录
		Map<Long, UserPubJoin> invitationRecords = invitationEventRepository.findRecordsBySpecification(pubUid, subUid, invitationSpecification);

		// 由邀请规则 判断 是否可以 建立邀请关系
		if (!invitationSpecification.canBeInvited(appletPubUser, appletSubUser, invitationRecords)) {
			return null;
		}
		// 创建邀请事件
		InvitationEvent invitationEvent = new InvitationEvent(appletPubUser, appletSubUser, invitationSpecification);
		// 成功邀请并创建关系，发送埋点数据
		FanActionForMQ fanActionForMQ = new FanActionForMQ();
		fanActionForMQ.setMid(appletPubUser.getUserInfo().getMerchantId());
		fanActionForMQ.setSid(appletPubUser.getUserInfo().getShopId());
		fanActionForMQ.setUid(appletPubUser.getId());
		fanActionForMQ.setEvent(FanActionEventEnum.INVITATION.getCode() + "");
		fanActionForMQ.setEventId(0L);
		fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
		rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
				JSON.toJSONString(fanActionForMQ), new SendCallback() {
					@Override
					public void onSuccess(SendResult sendResult) {
						log.info("建立团队发送成功");
					}

					@Override
					public void onException(Throwable e) {
						log.info("建立团队发送失败" + JSON.toJSONString(fanActionForMQ));
					}
				});

		// 财富大闯关 特殊处理： -- 如果有记录，并且还可以通过校验，说明那条记录是平台记录。
		// 此处 是把 平台的记录 替换成 邀请者
		if (UserPubJoinTypeEnum.CHECKPOINTS.getCode() == invitationSpecification.getUserPubJoinType()) {
			if (CollectionUtils.isEmpty(invitationRecords)) {
				return invitationEvent;
			}
			for (UserPubJoin userPubJoin : invitationRecords.values()) {
				if (userPubJoin.getUserId().equals(invitationSpecification.getPlatformId())) {
					invitationEvent.resetRecordInfo(userPubJoin);
					return invitationEvent;
				}
			}
		}

		return invitationEvent;
	}
}
