package mf.code.user.repo.repository;

import com.alibaba.fastjson.JSONObject;
import mf.code.user.domain.aggregateroot.DouDaiDaiUser;

/**
 * mf.code.user.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-08 10:15
 */
public interface DouDaiDaiUserRepository {
    /**
     * 登录时 获取用户信息
     * @param verifyResultData
     * @return
     */
    DouDaiDaiUser findByLogin(JSONObject verifyResultData);

    /**
     * 登录时  保存用户信息
     * @param douDaiDaiUser
     * @return
     */
    boolean saveByLogin(DouDaiDaiUser douDaiDaiUser);

    /**
     * 通过 merchantId 获取用户信息
     * @param merchantId
     * @return
     */
    DouDaiDaiUser findById(Long merchantId);
}
