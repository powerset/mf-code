package mf.code.user.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.dto.UserIncomeResp;
import mf.code.user.repo.dao.UpayWxOrderMapper;
import mf.code.user.repo.dao.UserAddressMapper;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.UpayBalance;
import mf.code.user.repo.po.User;
import mf.code.user.repo.po.UserAddress;
import mf.code.user.repo.repository.AppletUserRepository;
import mf.code.user.service.UpayBalanceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-27 11:08
 */
@Slf4j
@Service
public class AppletUserRepositoryImpl implements AppletUserRepository {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserAddressMapper userAddressMapper;
	@Autowired
	private UpayWxOrderMapper upayWxOrderMapper;
	@Autowired
	private UpayBalanceService upayBalanceService;

	/**
	 * 通过openid查询用户
	 * @param openid openid
	 * @return UserAggregaterRoot
	 */
	@Override
	public AppletUser findByOpenid(String openid) {
		if (StringUtils.isBlank(openid)) {
			return null;
		}

		QueryWrapper<User> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(User::getOpenId, openid);
		User user = userMapper.selectOne(wrapper);

		// 装配用户
		return assemblyAppletUser(user);
	}

	/**
	 * 根据主键，保存或更新用户信息
	 * @param appletUser
	 * @return
	 */
	@Override
	public boolean saveOrUpdateUserInfo(AppletUser appletUser) {
		User userInfo = appletUser.getUserInfo();

		int rows;
		if (userInfo.getId() == null) {
			rows = userMapper.insertSelective(userInfo);

		} else {
			rows = userMapper.updateByPrimaryKeySelective(userInfo);
		}

		appletUser.setId(userInfo.getId());

		return rows != 0;
	}

	/**
	 * 通过uid查询用户
	 * @param userId uid
	 * @return UserAggregaterRoot
	 */
	@Override
	public AppletUser findById(Long userId) {
		if (userId == null) {
			return null;
		}

		// 获取用户
		User user = this.userMapper.selectById(userId);
		if (user == null) {
			return null;
		}

		// 装配用户
		return assemblyAppletUser(user);
	}

	/**
	 * 增装 用户收益 到 appUser
	 * @param appletUser
	 * @param shopId
	 */
	@Override
	public void addUserIncome(AppletUser appletUser, Long shopId) {
		Long userId = appletUser.getId();

		// 获取用户最近一次财务更新时间
		QueryWrapper<UpayBalance> upayBalancesWrapper = new QueryWrapper<>();
		upayBalancesWrapper.lambda()
				.eq(UpayBalance::getUserId, userId)
				.eq(UpayBalance::getShopId, appletUser.getUserInfo().getVipShopId());
		UpayBalance upayBalance = upayBalanceService.getOne(upayBalancesWrapper);

		// 获取 从推广到现在 商城购物总返利金额
		BigDecimal totalRebateIncome = this.sumTotalFeeByBizType(userId, UpayWxOrderBizTypeEnum.REBATE.getCode());

		// 获取 从推广到现在 商城团队总贡献给我的金额
		BigDecimal contributionIncome = this.sumTotalFeeByBizType(userId, UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());

		UserIncomeResp userIncome = new UserIncomeResp();
		userIncome.setUserId(userId);
		userIncome.setShopId(shopId);
		userIncome.setTotalRebateIncome(totalRebateIncome);
		userIncome.setContributionIncome(contributionIncome);
		if (upayBalance != null) {
			userIncome.setSettledTime(upayBalance.getSettledTime());
		}
		appletUser.setUserIncome(userIncome);
	}

	/**
	 * 增装 用户收益 到 appUser
	 * @param appletUsers
	 */
	@Override
	public void listAddUserIncome(List<AppletUser> appletUsers, Long shopId) {
		List<Long> userIds = new ArrayList<>();
		for (AppletUser appletUser : appletUsers) {
			userIds.add(appletUser.getId());
		}

		// 批量 获取用户最近一次财务更新时间
		QueryWrapper<UpayBalance> upayBalancesWrapper = new QueryWrapper<>();
		upayBalancesWrapper.lambda()
				.in(UpayBalance::getUserId, userIds)
				.eq(UpayBalance::getShopId, shopId);
		List<UpayBalance> upayBalances = upayBalanceService.list(upayBalancesWrapper);
		Map<Long, UpayBalance> upayBalanceMap = new HashMap<>();
		for (UpayBalance upayBalance : upayBalances) {
			upayBalanceMap.put(upayBalance.getUserId(), upayBalance);
		}

		// 批量获取 从推广到现在 商城购物总返利金额 Map<UserId, BigDecimal>
		List<Map> totalRebateIncomeList = this.listSumTotalFeeByBizType(userIds, UpayWxOrderBizTypeEnum.REBATE.getCode());
		Map<Long, BigDecimal> totalRebateIncomeMap = new HashMap<>();
		for (Map map : totalRebateIncomeList) {
			Object userId = map.get("userId");
			Object totalFee = map.get("totalFee");
			totalRebateIncomeMap.put(Long.valueOf(userId.toString()), new BigDecimal(totalFee.toString()));
		}

		// 批量获取 从推广到现在 商城团队总贡献给我的金额 Map<UserId, BigDecimal>
		List<Map> contributionIncomeList = this.listSumTotalFeeByBizType(userIds, UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());
		Map<Long, BigDecimal> contributionIncomeMap = new HashMap<>();
		for (Map map : contributionIncomeList) {
			Object userId = map.get("userId");
			Object totalFee = map.get("totalFee");
			contributionIncomeMap.put(Long.valueOf(userId.toString()), new BigDecimal(totalFee.toString()));
		}

		for (AppletUser appletUser : appletUsers) {
			Long userId = appletUser.getId();
			UpayBalance upayBalance = upayBalanceMap.get(userId);
			BigDecimal totalRebateIncome = totalRebateIncomeMap.get(userId);
			BigDecimal contibutionIncome = contributionIncomeMap.get(userId);

			UserIncomeResp userIncome = new UserIncomeResp();
			userIncome.setUserId(userId);
			userIncome.setShopId(shopId);
			userIncome.setTotalRebateIncome(totalRebateIncome == null ? BigDecimal.ZERO : totalRebateIncome);
			userIncome.setContributionIncome(contibutionIncome == null ? BigDecimal.ZERO : contibutionIncome);
			if (upayBalance != null) {
				userIncome.setSettledTime(upayBalance.getSettledTime());
			}
			appletUser.setUserIncome(userIncome);
		}

	}

	// 获取 从推广到现在 总金额
	private List<Map> listSumTotalFeeByBizType(List<Long> userIds, int bizType) {
		Map<String, Object> params = new HashMap<>();
		params.put("userIds", userIds);
		// params.put("shopId", shopId); 2.1.1迭代 按平台维度获取
		params.put("type", UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
		params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
		params.put("bizType", bizType);
		return upayWxOrderMapper.listSumTotalFeeByBizType(params);
	}

	// 获取 从推广到现在 总金额
	private BigDecimal sumTotalFeeByBizType(Long userId, Integer bizType) {

		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		// params.put("shopId", shopId); 2.1.1迭代 按平台维度获取
		params.put("type", UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
		params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
		params.put("bizType", bizType);
		BigDecimal totalFee = upayWxOrderMapper.sumTotalFeeByBizType(params);

		return totalFee == null ? BigDecimal.ZERO : totalFee;
	}

	/**
	 * 通过userIds 批量查询用户
	 * @param userIds
	 * @return
	 */
	@Override
	public List<AppletUser> listByIds(List<Long> userIds) {
		List<User> userList = this.userMapper.selectBatchIds(userIds);

		// 装配用户
		List<AppletUser> appletUsers = new ArrayList<>();
		for (User user : userList) {
			// 装配数据
			appletUsers.add(assemblyAppletUser(user));
		}
		return appletUsers;
	}

	/**
	 * 获取平台用户
	 * @return
	 */
	@Override
	public AppletUser findPlatform() {
		QueryWrapper<User> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(User::getMerchantId, 0)
				.eq(User::getShopId, 0)
				.eq(User::getGrantStatus, -2);
		User user = this.userMapper.selectOne(wrapper);
		// 装配用户
		return assemblyAppletUser(user);
	}

	/**
	 * 获取带有收获地址信息的用户
	 * @return
	 */
	@Override
	public AppletUser findByIdWithAddress(Long uid) {
		AppletUser userAggregaterRoot = this.findById(uid);
		if (userAggregaterRoot == null) {
			return null;
		}
		// 获取用户地址
		QueryWrapper<UserAddress> uaWrapper = new QueryWrapper<>();
		uaWrapper.lambda()
				.eq(UserAddress::getUserId, uid)
				.eq(UserAddress::getDel, DelEnum.NO.getCode());
		List<UserAddress> userAddresses = userAddressMapper.selectList(uaWrapper);

		// 生成用户地址对象
		Map<String, UserAddress> userAddressMap = new HashMap<>();
		for (UserAddress userAddress : userAddresses) {
			userAddressMap.put(userAddress.getId().toString(), userAddress);
		}
		userAggregaterRoot.setUserAddressInfo(userAddressMap);
		return userAggregaterRoot;
	}

	/**
	 * 保存、更新或删除 用户地址
	 * @param userAddress userAddress
	 * @return userAddress
	 */
	@Override
	public UserAddress saveOrUpdateAddress(UserAddress userAddress) {
		int success;
		if (userAddress.getId() == null) {
			success = userAddressMapper.insertSelective(userAddress);
		}else {
			success = userAddressMapper.updateByPrimaryKeySelective(userAddress);
		}
		if (success == 0) {
			return null;
		}
		return userAddress;
	}

	/**
	 * 更新用户信息
	 * @param appletUser
	 */
	@Override
	public boolean updateUserInfo(AppletUser appletUser) {
		return userMapper.updateByPrimaryKeySelective(appletUser.getUserInfo()) != 0;
	}

	private AppletUser assemblyAppletUser(User user) {
		if (user == null) {
			return null;
		}

		AppletUser appletUser = new AppletUser();
		appletUser.setId(user.getId());
		appletUser.setUserInfo(user);

		return appletUser;
	}

	/**
	 * 获取 店铺粉丝数量
	 * @param appletUser
	 * @return
	 */
	@Override
	public Integer getShopFans(AppletUser appletUser) {
		boolean success = this.updateUserInfo(appletUser);
		if (!success) {
			log.error("数据更新失败");
		}
		QueryWrapper<User> userWrapper = new QueryWrapper<>();
		userWrapper.lambda()
				.eq(User::getVipShopId, appletUser.getUserInfo().getVipShopId())
				.eq(User::getGrantStatus, 1);
		return userMapper.selectCount(userWrapper) + 102789;
	}
}
