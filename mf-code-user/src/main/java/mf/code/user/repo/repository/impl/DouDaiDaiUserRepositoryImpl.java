package mf.code.user.repo.repository.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.constants.MerchantRoleEnum;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.merchant.constants.MerchantTypeEnum;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.api.feignclient.ShopAppService;
import mf.code.user.common.redis.RedisKeyConstant;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.domain.aggregateroot.DouDaiDaiUser;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.DouDaiDaiUserRepository;
import mf.code.user.repo.repository.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-08 10:16
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class DouDaiDaiUserRepositoryImpl implements DouDaiDaiUserRepository {
    private final ShopAppService shopAppService;
    private final UserRepository userRepository;
    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 登录时 获取用户信息
     *
     * @param verifyResultData
     * @return
     */
    @Override
    public DouDaiDaiUser findByLogin(JSONObject verifyResultData) {
        // 获取用户信息
        String openId = verifyResultData.getString("openid");
        JSONObject userInfoJson = verifyResultData.getJSONObject("userInfo");

        MerchantDTO merchantDTO = shopAppService.queryMerchantByOpenId(openId);
        if (merchantDTO == null) {
            if (userInfoJson == null) {
                log.info("用户未授权");
                return null;
            } else {
                // 新增用户
                merchantDTO = this.newMerchantDTO(verifyResultData);
            }
        } else {
            // 更新用户
            merchantDTO.setPhone(StringUtils.isNotBlank(merchantDTO.getPhone()) ? merchantDTO.getPhone() : "");
            merchantDTO.setLastLoginTime(new Date());
        }

        MerchantShopDTO merchantShopDTO;
        User user;
        if (merchantDTO.getId() == null) {
            // 新用户 创建商铺
            merchantShopDTO = this.newMerchantShopDTO(merchantDTO);
            // 处理用户表
            user = this.newUser(merchantDTO);
        } else {
            // 获取店铺信息
            merchantShopDTO = shopAppService.findShopByMerchantId(merchantDTO.getId());
            if (merchantShopDTO == null) {
                log.error("数据库异常，请稍候再试");
                return null;
            }
            merchantShopDTO.setUtime(new Date());
            // 获取user表的映射信息
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(User::getMerchantId, merchantDTO.getId())
                    .eq(User::getRole, UserRoleEnum.DOU_DAI_DAI.getCode());
            user = userRepository.getOne(wrapper);
            if (user == null) {
                log.error("数据库异常，请稍候再试");
                return null;
            }
            user.setUtime(new Date());
        }

        DouDaiDaiUser douDaiDaiUser = new DouDaiDaiUser();
        douDaiDaiUser.setMerchantInfo(merchantDTO);
        douDaiDaiUser.setMerchantShopInfo(merchantShopDTO);
        douDaiDaiUser.setUserInfo(user);

        return douDaiDaiUser;
    }

    /**
     * 登录时  保存用户信息
     *
     * @param douDaiDaiUser
     * @return
     */
    @Transactional
    @Override
    public boolean saveByLogin(DouDaiDaiUser douDaiDaiUser) {
        MerchantDTO merchantDTO = douDaiDaiUser.getMerchantInfo();
        MerchantShopDTO merchantShopDTO = douDaiDaiUser.getMerchantShopInfo();
        User userInfo = douDaiDaiUser.getUserInfo();
        // 更新或保存抖带带主播商户信息
        Long merchantId = shopAppService.saveOrUpdateMerchant(merchantDTO);
        if (merchantId == null) {
            log.error("服务异常，更新或保存抖带带主播商户信息失败");
            throw new RuntimeException();
        }
        douDaiDaiUser.setMerchantId(merchantId);
        merchantDTO.setId(merchantId);
        merchantShopDTO.setMerchantId(merchantId);
        userInfo.setMerchantId(merchantId);
        // 更新或保存抖带带主播店铺信息
        Long shopId = shopAppService.saveOrUpdateMerchantShop(merchantShopDTO);
        if (shopId == null) {
            log.error("服务异常，更新或保存抖带带主播商户信息失败");
            throw new RuntimeException();
        }
        merchantShopDTO.setId(shopId);
        userInfo.setShopId(shopId);
        userInfo.setVipShopId(shopId);
        // 更新或保存抖带带主播user表的映射信息
        boolean saveOrUpdate = userRepository.saveOrUpdate(userInfo);
        if (!saveOrUpdate) {
            log.error("数据库异常，更新或保存抖带带主播user表的映射信息失败");
            throw new RuntimeException();
        }
        return true;
    }

    /**
     * 通过 merchantId 获取用户信息
     *
     * @param merchantId
     * @return
     */
    @Override
    public DouDaiDaiUser findById(Long merchantId) {
        MerchantDTO merchant = shopAppService.queryMerchantById(merchantId);
        if (merchant == null) {
            log.error("服务异常，无此用户信息");
            return null;
        }
        DouDaiDaiUser douDaiDaiUser = new DouDaiDaiUser();
        douDaiDaiUser.setMerchantId(merchantId);
        douDaiDaiUser.setMerchantInfo(merchant);
        return douDaiDaiUser;
    }

    private MerchantShopDTO newMerchantShopDTO(MerchantDTO merchantDTO) {
        String nameKey = RedisKeyConstant.DOU_YIN_APP_SELLER_NAME;
        String appName = stringRedisTemplate.opsForValue().get(nameKey);
        if (StringUtils.isBlank(appName)) {
            appName = "集客小铺";
            stringRedisTemplate.opsForValue().set(nameKey, appName);
        }
        Date now = new Date();
        MerchantShopDTO merchantShop = new MerchantShopDTO();
        merchantShop.setMerchantId(merchantDTO.getId());
        merchantShop.setNick(merchantDTO.getName());
        merchantShop.setShopName(appName);
        merchantShop.setCategoryId(17L);
        merchantShop.setOpenstatus(1);
        merchantShop.setOpenreason("");
        merchantShop.setDiststatus(0);
        merchantShop.setDistreason("");
        merchantShop.setPhone("");
        merchantShop.setUsername(merchantDTO.getName());
        merchantShop.setShowFlag(0);
        merchantShop.setPurchaseVersion(MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode());
        Map<String, String> purchaseJson = new HashMap<>();
        purchaseJson.put("purchaseTime", DateUtil.dateToString(now, DateUtil.FORMAT_TWO));
        purchaseJson.put("expireTime", DateUtil.dateToString(DateUtils.addYears(now, 100), DateUtil.FORMAT_TWO));
        merchantShop.setPurchaseJson(JSON.toJSONString(purchaseJson));
        merchantShop.setDel(DelEnum.NO.getCode());
        merchantShop.setUtime(now);
        merchantShop.setCtime(now);

        return merchantShop;
    }

    private MerchantDTO newMerchantDTO(JSONObject verifyResultData) {
        String openid = verifyResultData.getString("openid");
        JSONObject userInfo = verifyResultData.getJSONObject("userInfo");
        Date now = new Date(System.currentTimeMillis() / 1000 * 1000);

        MerchantDTO merchant = new MerchantDTO();
        merchant.setRole(MerchantRoleEnum.DOU_DAI_DAI.getCode());
        merchant.setName(userInfo.getString("nickName"));
        merchant.setAvatarUrl(userInfo.getString("avatarUrl"));
        merchant.setType(MerchantTypeEnum.PARERNT_MERCHANT.getCode());
        merchant.setGrantStatus(1);
        merchant.setGrantTime(now);
        merchant.setOpenId(openid);
        merchant.setDel(DelEnum.NO.getCode());
        merchant.setUtime(now);
        merchant.setCtime(now);

        merchant.setPhone("");
        merchant.setPassword("");
        merchant.setStatus(0);
        merchant.setLastLoginTime(now);
        merchant.setLoginErrorCount(0);
        merchant.setParentId(0L);
        merchant.setSubStatus(0);
        merchant.setPurchaseVersionJson("2");

        return merchant;
    }

    private User newUser(MerchantDTO merchantDTO) {
        User user = new User();
        user.setOpenId("douDaiDai_" + merchantDTO.getOpenId());
        user.setMerchantId(merchantDTO.getId());
        user.setMobile(merchantDTO.getPhone());
        user.setNickName(merchantDTO.getName());
        user.setAvatarUrl(merchantDTO.getAvatarUrl());
        user.setGender(0);
        user.setGrantStatus(1);
        user.setRole(UserRoleEnum.DOU_DAI_DAI.getCode());
        user.setRoleTime(merchantDTO.getGrantTime());
        user.setGrantTime(merchantDTO.getGrantTime());
        user.setCtime(merchantDTO.getCtime());
        user.setUtime(merchantDTO.getUtime());
        return user;
    }
}
