package mf.code.user.repo.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.User;
import mf.code.user.repo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.user.repo.repository.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-16 20:44
 */
@Service
public class UserRepositoryImpl extends ServiceImpl<UserMapper, User> implements UserRepository {
    @Autowired
    private UserMapper userMapper;

    /**
     * 通过userIds 批量查询用户
     *
     * @param userIds
     * @return
     */
    @Override
    public List<User> listByIds(List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return new ArrayList<>();
        }
        return userMapper.selectBatchIds(userIds);
    }

    @Override
    public User selectById(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }
}
