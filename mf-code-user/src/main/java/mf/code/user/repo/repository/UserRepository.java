package mf.code.user.repo.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.repo.po.User;

import java.util.List;

/**
 * mf.code.user.repo.repository
 * Description:
 *
 * @author gel
 * @date 2019-04-16 20:44
 */
public interface UserRepository extends IService<User> {

    /**
     * 通过userIds 批量查询用户
     *
     * @param userIds
     * @return
     */
    List<User> listByIds(List<Long> userIds);

    User selectById(Long userId);
}
