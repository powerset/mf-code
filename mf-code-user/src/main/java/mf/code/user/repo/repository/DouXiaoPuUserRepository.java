package mf.code.user.repo.repository;

import com.alibaba.fastjson.JSONObject;
import mf.code.user.domain.aggregateroot.DouXiaoPuUser;

/**
 * mf.code.user.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-08 22:50
 */
public interface DouXiaoPuUserRepository {
    /**
     * 登录时 获取用户信息
     * @param verifyResultData
     * @return
     */
    DouXiaoPuUser findByLogin(JSONObject verifyResultData);

    /**
     * 登录时  保存用户信息
     * @param douXiaoPuUser
     * @return
     */
    boolean saveByLogin(DouXiaoPuUser douXiaoPuUser);

    /**
     * 通过 userId 获取用户信息
     * @param userId
     * @return
     */
    DouXiaoPuUser findById(Long userId);
}
