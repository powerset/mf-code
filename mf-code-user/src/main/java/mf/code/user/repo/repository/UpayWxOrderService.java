package mf.code.user.repo.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.repo.po.UpayWxOrder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.repo.repository
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 19:52
 */
public interface UpayWxOrderService extends IService<UpayWxOrder> {
    /***
     * 插入一条交易
     * @param upayWxOrderReqDTO
     * @return
     */
    Integer insertSelective(UpayWxOrderReqDTO upayWxOrderReqDTO);

    /***
     * 给po赋值
     * @param userID
     * @param payType
     * @param type
     * @param orderNo
     * @param orderName
     * @param merchantID
     * @param shopID
     * @param status
     * @param amount
     * @param ipAddress
     * @param tradeID
     * @param remark
     * @param paymentTime
     * @param notifyTime
     * @param reqJson
     * @param bizType
     * @param bizValue
     * @return
     */
    UpayWxOrder addPo(Long userID,
                      Integer payType,
                      Integer type,
                      String orderNo,
                      String orderName,
                      Long merchantID,
                      Long shopID,
                      Integer status,
                      BigDecimal amount,
                      String ipAddress,
                      String tradeID,
                      String remark,
                      Date paymentTime,
                      Date notifyTime,
                      String reqJson,
                      Integer bizType,
                      Long bizValue);

    /***
     *   创建订单
     *   userID
     *   type
     *   OrderNo
     *   OrderName
     *   appletID
     *   merchantID
     *   shopID
     *   status
     *   amount
     *   tradeType
     *   ipAddress
     *   tradeID
     *   remark
     *   paymentTime
     * @return
     */
    UpayWxOrder create(UpayWxOrder upayWxOrde);

    BigDecimal openRedPackLockSumMoney(Long shopId, Long userId);

    BigDecimal sumTotalFeeByBizTypes(Long shopId, Long userId, Integer type, List<Integer> bizTypes);

    List<Map> sumTotalFeeByBizTypes(Long shopId, List<Long> userIds, Integer type, List<Integer> bizTypes);

    UpayWxOrder queryParams(QueryWrapper<UpayWxOrder> wrapper);
}
