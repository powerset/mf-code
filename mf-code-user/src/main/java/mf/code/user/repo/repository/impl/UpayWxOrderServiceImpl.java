package mf.code.user.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.RandomStrUtil;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.user.common.property.AppProperty;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.repo.dao.UpayWxOrderMapper;
import mf.code.user.repo.po.UpayWxOrder;
import mf.code.user.repo.repository.UpayWxOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.repo.repository.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 19:52
 */
@Service
@Slf4j
public class UpayWxOrderServiceImpl extends ServiceImpl<UpayWxOrderMapper, UpayWxOrder> implements UpayWxOrderService {
    @Autowired
    private UpayWxOrderMapper upayWxOrderMapper;
    @Autowired
    private AppProperty appProperty;


    @Override
    public Integer insertSelective(UpayWxOrderReqDTO upayWxOrderReqDTO) {
        Date now = new Date();
        UpayWxOrder upayWxOrder = new UpayWxOrder();
        BeanUtils.copyProperties(upayWxOrderReqDTO, upayWxOrder);
        upayWxOrder.setCtime(now);
        upayWxOrder.setUtime(now);
        upayWxOrder.setNotifyTime(now);
        upayWxOrder.setPaymentTime(now);
        upayWxOrder.setMchId(upayWxOrderReqDTO.getMerchantId());
        upayWxOrder.setTotalFee(new BigDecimal(upayWxOrderReqDTO.getTotalFee()));

        int i = this.upayWxOrderMapper.insertSelective(upayWxOrder);
        return i;
    }

    @Override
    public UpayWxOrder addPo(Long userID,
                             Integer payType,
                             Integer type,
                             String orderNo,
                             String orderName,
                             Long merchantID,
                             Long shopID,
                             Integer status,
                             BigDecimal amount,
                             String ipAddress,
                             String tradeID,
                             String remark,
                             Date paymentTime,
                             Date notifyTime,
                             String reqJson,
                             Integer bizType,
                             Long bizValue) {
        UpayWxOrder po = new UpayWxOrder();
        if (userID != null) {
            po.setUserId(userID);
        }
        if (payType != null) {
            po.setPayType(payType);
            if (payType == OrderPayTypeEnum.WEIXIN.getCode()) {
                po.setAppletAppId(appProperty.getMfAppId());
                po.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
                po.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
            }
        }
        if (type != null) {
            po.setType(type);
        }
        if (StringUtils.isNotBlank(orderNo)) {
            po.setOrderNo(orderNo);
        } else {
            po.setOrderNo("V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis());
        }
        if (StringUtils.isNotBlank(orderName)) {
            po.setOrderName(orderName);
        }
        if (merchantID != null) {
            po.setMchId(merchantID);
        }
        if (shopID != null) {
            po.setShopId(shopID);
        }
        if (status != null) {
            po.setStatus(status);
        }
        if (amount != null) {
            po.setTotalFee(amount);
        }
        if (StringUtils.isNotBlank(ipAddress)) {
            po.setIpAddress(ipAddress);
        }
        if (StringUtils.isNotBlank(tradeID)) {
            po.setTradeId(tradeID);
        }
        if (StringUtils.isNotBlank(remark)) {
            po.setRemark(remark);
        }
        if (paymentTime != null) {
            po.setPaymentTime(paymentTime);
        }
        if (notifyTime != null) {
            po.setNotifyTime(notifyTime);
        }
        if (StringUtils.isNotBlank(reqJson)) {
            po.setReqJson(reqJson);
        } else {
            po.setReqJson("");
        }
        if (bizType != null) {
            po.setBizType(bizType);
        }
        if (bizValue != null) {
            po.setBizValue(bizValue);
        }

        return po;
    }

    @Override
    public UpayWxOrder create(UpayWxOrder upayWxOrder) {
        upayWxOrder.setCtime(new Date());
        upayWxOrder.setUtime(new Date());
        this.upayWxOrderMapper.insertSelective(upayWxOrder);
        return upayWxOrder;
    }

    /***
     * 获取拆红包是否有解锁收益
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public BigDecimal openRedPackLockSumMoney(Long shopId, Long userId) {
        Map<String, Object> params = new HashMap<>();
        params.put("shopId", shopId);
        params.put("userId", userId);
        params.put("type", UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("BizType", UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode());
        params.put("mfTradeType", UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode());
        return upayWxOrderMapper.sumTotalFeeByBizType(params);
    }

    /***
     * 获取单个用户 的根据biz_type 划分具体的收益累计
     * @param shopId
     * @param userId
     * @param bizTypes
     * @return
     */
    @Override
    public BigDecimal sumTotalFeeByBizTypes(Long shopId, Long userId, Integer type, List<Integer> bizTypes) {
        Map<String, Object> params = new HashMap<>();
        params.put("shopId", shopId);
        params.put("userId", userId);
        params.put("type", type);
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("bizTypes", bizTypes);
        params.put("mfTradeType", UpayWxOrderMfTradeTypeEnum.CLEANINGED.getCode());
        List<Map> totalFeeByUserIds = upayWxOrderMapper.sumTotalFeeByBizTypes(params);
        return CollectionUtils.isEmpty(totalFeeByUserIds) ? BigDecimal.ZERO : new BigDecimal(totalFeeByUserIds.get(0).get("totalFee").toString());
    }

    @Override
    public List<Map> sumTotalFeeByBizTypes(Long shopId, List<Long> userIds, Integer type, List<Integer> bizTypes) {
        Map<String, Object> params = new HashMap<>();
        params.put("shopId", shopId);
        params.put("userIds", userIds);
        params.put("type", type);
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("bizTypes", bizTypes);
        params.put("mfTradeType", UpayWxOrderMfTradeTypeEnum.CLEANINGED.getCode());
        return upayWxOrderMapper.sumTotalFeeByBizTypes(params);
    }

    @Override
    public UpayWxOrder queryParams(QueryWrapper<UpayWxOrder> wrapper) {
        List<UpayWxOrder> upayWxOrders = upayWxOrderMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(upayWxOrders)) {
            return null;
        }
        for(UpayWxOrder upayWxOrder : upayWxOrders){
            if(upayWxOrder.getMfTradeType() == UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode()){
                return upayWxOrder;
            }
        }
        return null;
    }
}
