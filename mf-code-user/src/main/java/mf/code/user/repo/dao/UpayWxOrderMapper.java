package mf.code.user.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.user.repo.po.UpayWxOrder;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface UpayWxOrderMapper extends BaseMapper<UpayWxOrder> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(UpayWxOrder record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(UpayWxOrder record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    UpayWxOrder selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UpayWxOrder record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UpayWxOrder record);

    /**
     * 分批获取 此用户 的团队 -从最初贡献开始-到现在的所有已结算贡献总金额 -- 贡献
     * @param param
     * @return
     */
    List<Map> batchTotalContributionSettledByUserIds(Map<String, Object> param);

    /**
     * 获取单个用户 从推广到现在 已结算总金额 -- 返利+贡献
     * @param params
     * @return
     */
    BigDecimal sumTotalFeeByBizType(Map<String, Object> params);

    /**
     * 获取单个用户 的根据biz_type 划分具体的收益累计
     * @param params
     * @return
     */
    List<Map> sumTotalFeeByBizTypes(Map<String, Object> params);

    /**
     * 批量获取 所有用户 从推广到现在 已结算总金额 -- 返利+贡献
     * @param params
     * @return
     */
    List<Map> batchSumTotalFeeSettledByUserIds(Map<String, Object> params);

    /**
     * 批量 获取 从推广到现在 总金额 从推广到现在 总金额
     * @param params
     * @return
     */
    List<Map> listSumTotalFeeByBizType(Map<String, Object> params);
}
