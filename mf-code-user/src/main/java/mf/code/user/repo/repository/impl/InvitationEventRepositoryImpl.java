package mf.code.user.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.RegexUtils;
import mf.code.user.domain.aggregateroot.AppletUser;
import mf.code.user.domain.aggregateroot.InvitationEvent;
import mf.code.user.domain.valueobject.specification.InvitationDimensionEnum;
import mf.code.user.domain.valueobject.specification.InvitationSpecification;
import mf.code.user.domain.valueobject.specification.InvitationTypeEnum;
import mf.code.user.repo.dao.UserPubJoinMapper;
import mf.code.user.repo.po.UserPubJoin;
import mf.code.user.repo.repository.AppletUserRepository;
import mf.code.user.repo.repository.InvitationEventRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 15:11
 */
@Slf4j
@Service
public class InvitationEventRepositoryImpl implements InvitationEventRepository {
	@Autowired
	private UserPubJoinMapper userPubJoinMapper;
	@Autowired
	private AppletUserRepository appletUserRepository;

	/**
	 * 保存或更新 邀请记录
	 * @param invitationEvent
	 * @return
	 */
	@Override
	public Boolean saveOrUpdate(InvitationEvent invitationEvent) {
		UserPubJoin userPubJoin = invitationEvent.getRecordInfo();
		if (userPubJoin.getId() == null) {
			int save = userPubJoinMapper.insertSelective(userPubJoin);
			return save != 0;
		} else {
			int update = userPubJoinMapper.updateByPrimaryKeySelective(userPubJoin);
			return update != 0;
		}
	}

	/**
	 * 通过 邀请规则 查询 邀请记录 -- 此方法区分查询 被邀请记录 还是 邀请记录
	 * @param pubUid
	 * @param subUid
	 * @param invitationSpecification
	 * @return
	 */
	@Override
	public Map<Long, UserPubJoin> findRecordsBySpecification(Long pubUid, Long subUid, InvitationSpecification invitationSpecification) {
		// 获取参数
		if (invitationSpecification == null) {
			log.info("参数为空，获取邀请记录失败");
			return null;
		}

		List<Long> subUids = new ArrayList<>();
		if (subUid != null) {
			subUids.add(subUid);
		}

		List<Long> pubUids = new ArrayList<>();
		if (pubUid != null) {
			pubUids.add(pubUid);
		}

		return this.findRecordsBySpecification(pubUids, subUids, invitationSpecification);
	}

	/**
	 * 通过 邀请规则 批量查询 邀请记录 -- 此方法区分查询 被邀请记录 还是 邀请记录
	 * @param pubUids
	 * @param subUids
	 * @param invitationSpecification
	 * @return
	 */
	@Override
	public Map<Long, UserPubJoin> findRecordsBySpecification(List<Long> pubUids, List<Long> subUids, InvitationSpecification invitationSpecification) {
		// 获取参数
		if (invitationSpecification == null) {
			log.info("参数为空，获取邀请记录失败");
			return null;
		}

		// 查询被邀请列表，关注我是否已被邀请了
		if (InvitationTypeEnum.INVITED == invitationSpecification.getInvitationTypeEnum()) {
			if (CollectionUtils.isEmpty(subUids)) {
				log.error("subUids 为空");
				return null;
			}
			Map<Long, UserPubJoin> records = new HashMap<>();
			// 分批查询 -- 每次 100
			int batchCount = 100;
			int sourListSize = subUids.size();
			int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
			int startIndext = 0;
			int stopIndext = 0;
			for(int i = 0; i < subCount; i++){
				stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
				List<Long> tempList = new ArrayList<>(subUids.subList(startIndext, stopIndext));
				if(tempList.size() > 0){
					records.putAll(findInvitedBySpecification(tempList, invitationSpecification));
				}
				startIndext = stopIndext;
			}
			return records;
		}

		// 查询邀请列表，关注我邀请了哪些人，主要用于判断 曾今是否邀请过用户
		if (InvitationTypeEnum.INVITATION == invitationSpecification.getInvitationTypeEnum()) {
			if (CollectionUtils.isEmpty(pubUids)) {
				log.error("pubUids 为空");
				return null;
			}
			Map<Long, UserPubJoin> records = new HashMap<>();
			// 分批查询 -- 每次 100
			int batchCount = 100;
			int sourListSize = pubUids.size();
			int subCount = sourListSize % batchCount==0 ? sourListSize / batchCount : sourListSize / batchCount+1;
			int startIndext = 0;
			int stopIndext = 0;
			for(int i = 0; i < subCount; i++){
				stopIndext = (i == subCount - 1)? stopIndext + sourListSize % batchCount : stopIndext + batchCount;
				List<Long> tempList = new ArrayList<>(pubUids.subList(startIndext, stopIndext));
				if(tempList.size() > 0){
					records.putAll(findInvitationBySpecification(tempList, invitationSpecification));
				}
				startIndext = stopIndext;
			}
			return records;
		}

		log.error("未配置 邀请类型： invitationTypeEnum 参数");
		return null;
	}

	/**
	 * 通过邀请规则 查询 在此店铺 上级邀请我的记录
	 * @param uid
	 * @param sid
	 * @param invitationSpecification
	 * @return
	 */
	@Override
	public Map<Long, UserPubJoin> findUpperRecords(String uid, String sid, InvitationSpecification invitationSpecification) {
		if (StringUtils.isBlank(uid) || !RegexUtils.StringIsNumber(uid) || invitationSpecification == null) {
			log.error("参数异常");
			return null;
		}
		if (InvitationTypeEnum.INVITATION == invitationSpecification.getInvitationTypeEnum()) {
			log.error("查询 取向不符");
			return null;
		}


		return null;
	}

	/**
	 * 通过 SubUid 和 邀请类型 获取 被邀请记录
	 * 被邀请记录里包含平台记录
	 * @param subUids
	 * @param invitationSpecification
	 * @return
	 */
	private Map<Long, UserPubJoin> findInvitedBySpecification(List<Long> subUids, InvitationSpecification invitationSpecification) {
		if (CollectionUtils.isEmpty(subUids) || invitationSpecification == null) {
			log.error("参数异常");
			return new HashMap<>();
		}
		AppletUser platform = appletUserRepository.findPlatform();
		invitationSpecification.setPlatformId(platform.getId());
		// 默认 平台维度查找数据 InvitationDimensionEnum.PLATFORM == invitationSpecification.getInvitationDimension()
		// 此记录里包含平台邀请
		QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.in(UserPubJoin::getSubUid, subUids)
				.eq(UserPubJoin::getType, invitationSpecification.getUserPubJoinType());

		if (InvitationDimensionEnum.SHOP == invitationSpecification.getInvitationDimension()) {
			wrapper.lambda()
					.eq(UserPubJoin::getShopId, invitationSpecification.getShopId());
		}
		if (InvitationDimensionEnum.ACTIVITY == invitationSpecification.getInvitationDimension()) {
			wrapper.lambda()
					.eq(UserPubJoin::getActivityId, invitationSpecification.getActivityId());
		}

		List<UserPubJoin> userPubJoins = userPubJoinMapper.selectList(wrapper);

		// 装配数据
		Map<Long, UserPubJoin> invitedRecords = new HashMap<>();
		for (UserPubJoin userPubJoin : userPubJoins) {
			invitedRecords.put(userPubJoin.getId(), userPubJoin);
		}
		return invitedRecords;
	}

	/**
	 * 通过 邀请规则 获取用户邀请记录 -- 只支持 活动 或 店铺 维度的查询
	 * @param pubUids
	 * @param invitationSpecification
	 * @return
	 */
	private Map<Long, UserPubJoin> findInvitationBySpecification(List<Long> pubUids, InvitationSpecification invitationSpecification) {
		if (CollectionUtils.isEmpty(pubUids) || invitationSpecification == null || !invitationSpecification.isInvite()) {
			log.error("参数异常 或 无邀请功能");
			return new HashMap<>();
		}

		QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.in(UserPubJoin::getUserId, pubUids)
				.eq(UserPubJoin::getType, invitationSpecification.getUserPubJoinType());

		if (invitationSpecification.getInvitationDimension() == InvitationDimensionEnum.ACTIVITY) {
			wrapper.lambda()
					.eq(UserPubJoin::getActivityId, invitationSpecification.getActivityId());
		}
		if (invitationSpecification.getInvitationDimension() == InvitationDimensionEnum.SHOP) {
			wrapper.lambda()
					.eq(UserPubJoin::getShopId, invitationSpecification.getShopId());
		}

		List<UserPubJoin> userPubJoins = userPubJoinMapper.selectList(wrapper);
		if (userPubJoins == null) {
			userPubJoins = new ArrayList<>();
		}
		Map<Long, UserPubJoin> invitationRecords = new HashMap<>();
		for (UserPubJoin userPubJoin : userPubJoins) {
			invitationRecords.put(userPubJoin.getId(), userPubJoin);
		}
		return invitationRecords;
	}
}
