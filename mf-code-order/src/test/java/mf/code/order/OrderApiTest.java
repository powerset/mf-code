package mf.code.order;

import lombok.extern.slf4j.Slf4j;
import mf.code.order.api.applet.GoodsOrderApi;
import mf.code.order.api.applet.feignservice.OrderAppServiceImpl;
import mf.code.order.dto.DoudaidaiMypageResp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.order
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月18日 21:39
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderApiTest {
    @Autowired
    private GoodsOrderApi orderApi;
    @Autowired
    private OrderAppServiceImpl orderAppService;

    @Test
    public void queryMypageData() {
        DoudaidaiMypageResp doudaidaiMypageData = orderAppService.getDoudaidaiMypageData(8L, 8L);
        System.out.println(doudaidaiMypageData);
    }


    @Test
    public void queryGoodsOrderDetail() {
        Long merchantId = 150L;
        Long shopId = 110L;
        Long userId = 36L;
        Long orderId = 245L;

        this.orderApi.queryGoodsOrderDetail(merchantId, shopId, userId, orderId);
    }

    @Test
    public void payGoodsOrder() {
        Long merchantId = 150L;
        Long shopId = 114L;
        Long userId = 944L;
        Long orderId = 1917L;

        // merchantId, shopId, userId, addressId, buyerMsg, orderId, request
        this.orderApi.payGoodsOrder(merchantId, shopId, userId, 200L, "", orderId, null);
    }
}
