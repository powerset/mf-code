package mf.code.order;

import lombok.extern.slf4j.Slf4j;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.service.OrderStatusService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.order
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月18日 20:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderStatusServiceTest {
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;

    @Test
    public void buyerOrderTimeout(){
        Long orderId = 249L;
        Order order = this.goodsOrderRepository.selectById(orderId);
        this.orderStatusService.buyerOrderTimeout(order);
    }


}
