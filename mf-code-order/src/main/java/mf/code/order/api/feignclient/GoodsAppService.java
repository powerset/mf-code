package mf.code.order.api.feignclient;

import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.order.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import mf.code.order.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 14:10
 */
@FeignClient(name = "mf-code-goods", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface GoodsAppService {
    /***
     * 随机获取一个在售商品的sku信息
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/queryRandomSkuOnsale")
    GoodsSkuDTO queryRandomSkuOnsale();

    @GetMapping("/feignapi/goods/applet/v5/queryProductSkuList")
    Map<Long, GoodsSkuDTO> queryProductSkuList(@RequestParam("merchantId") Long merchantId,
                                               @RequestParam("shopId") Long shopId,
                                               @RequestParam("goodsId") List<Long> goodsIds);

    @GetMapping("/feignapi/goods/applet/v5/queryProductSku")
    GoodsSkuDTO queryProductSku(@RequestParam("merchantId") Long merchantId,
                                @RequestParam("shopId") Long shopId,
                                @RequestParam("goodsId") Long goodsId,
                                @RequestParam("skuId") Long skuId,
                                @RequestParam("userId") Long userId);

    @GetMapping("/feignapi/goods/applet/v5/queryProduct")
    GoodProductDTO queryProduct(@RequestParam("merchantId") Long merchantId,
                                @RequestParam("shopId") Long shopId,
                                @RequestParam("goodsId") Long goodsId);

    /**
     * 分页展示  所有可售商品
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/pageGoodsSale")
    AppletMybatisPageDto pageGoodsSale(@RequestParam("shopId") Long shopId,
                                       @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                       @RequestParam(value = "size", defaultValue = "16") Long size);

    /**
     * 获取 商品分销属性
     *
     * @param goodsId
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/queryGoodsDistribution")
    ProductDistributionDTO queryGoodsDistribution(@RequestParam("goodsId") String goodsId);

    /**
     * 商户根据skuId列表查询商品信息
     *
     * @param skuIdList
     * @return
     */
    @GetMapping("/feignapi/goods/seller/v5/queryProductSkuMapBySkuIdList")
    SimpleResponse queryProductSkuMapBySkuIdList(@RequestParam("skuIdList") List<Long> skuIdList);

    /**
     * 库存消费
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @return SimpleResponse
     */
    @GetMapping("/feignapi/goods/seller/v5/consumeStock")
    SimpleResponse consumeStock(@RequestParam("productSkuId") Long productSkuId,
                                @RequestParam("stockNumber") int stockNumber,
                                @RequestParam("orderId") Long orderId);

    /**
     * 还库存
     *
     * @param productSkuId 产品的skuid
     * @param stockNumber  消耗库存的数量
     * @return SimpleResponse
     */
    @GetMapping("/feignapi/goods/seller/v5/giveBackStock")
    SimpleResponse giveBackStock(@RequestParam("productSkuId") Long productSkuId,
                                 @RequestParam("stockNumber") int stockNumber,
                                 @RequestParam("orderId") Long orderId);

    /***
     * 获取供应商商品经过平台分佣之后的售价和比例
     */
    @GetMapping("/feignapi/goods/applet/sku/getPlatformDistRateCommission")
    String getPlatformDistRateCommission(@RequestParam("merchantId") Long merchantId,
                                         @RequestParam("shopId") Long shopId,
                                         @RequestParam("skuId") Long skuId);
}
