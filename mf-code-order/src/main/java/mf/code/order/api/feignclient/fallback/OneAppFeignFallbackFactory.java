package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.UserCouponDTO;
import mf.code.order.api.feignclient.OneAppService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.api.feignclient.fallback
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:07
 */
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable cause) {
        return new OneAppService() {
            @Override
            public Map<String, Object> queryOpenRedPackageStatus(Long shopId, Long userId) {
                return new HashMap<>();
            }

            @Override
            public SimpleResponse completeOpenRedPackagePay(Long activityId) {
                return null;
            }

            /**
             * 查询用户是否拥有尚未使用的平台活动中奖资格
             *
             * @param userId
             * @param userTaskId
             */
            @Override
            public SimpleResponse checkUserTaskForPlatOrderBack(Long userId, Long userTaskId) {
                return null;
            }

            /**
             * 查询用户是否拥有尚未使用的平台活动中奖资格
             *
             * @param userId
             * @param userTaskId
             */
            @Override
            public SimpleResponse updateUserTaskForPlatOrderBack(Long userId, Long userTaskId) {
                return null;
            }

            /**
             * 通过优惠券id查询优惠券
             *
             * @param couponId
             * @return
             */
            @Override
            public UserCouponDTO findCouponById(Long couponId) {
                return null;
            }

            /**
             * 使用优惠券
             *
             * @param discountId
             */
            @Override
            public SimpleResponse useUserCoupon(Long discountId) {
                return null;
            }

            /**
             * 返回优惠券
             *
             * @param couponId
             * @return
             */
            @Override
            public SimpleResponse backUserCoupon(Long couponId) {
                return null;
            }
        };
    }
}
