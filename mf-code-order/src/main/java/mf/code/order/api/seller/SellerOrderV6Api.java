package mf.code.order.api.seller;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.SellerOrderListReqDTO;
import mf.code.order.dto.SellerOrderListResultDTO;
import mf.code.order.service.SellerOrderV6ApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order/seller/v6/order/")
public class SellerOrderV6Api {

    @Autowired
    private SellerOrderV6ApiService sellerOrderV6ApiService;

    /**
     * 查询订单列表
     *
     * @param orderNo     订单号
     * @param productId   产品id
     * @param buyerName   买家名
     * @param buyerPhone  买家手机号
     * @param cStartTime  下单开始时间
     * @param cEndTime    下单结束时间
     * @param orderStatus 订单状态
     * @param tabType     支付类型
     * @param orderType   订单类型
     * @param checkStatus 评价状态：0全部，1待评价，2已评价
     * @param page        页码
     * @param size        条数
     * @return 返回列表
     */
    @GetMapping(value = "list")
    public SimpleResponse list(@RequestParam(value = "orderNo", required = false) String orderNo,
                               @RequestParam(value = "merchantId", required = false) Long merchantId,
                               @RequestParam(value = "shopId", required = false) Long shopId,
                               @RequestParam(value = "productId", required = false) Long productId,
                               @RequestParam(value = "productName", required = false) String productName,
                               @RequestParam(value = "buyerName", required = false) String buyerName,
                               @RequestParam(value = "buyerPhone", required = false) String buyerPhone,
                               @RequestParam(value = "cStartTime", required = false) String cStartTime,
                               @RequestParam(value = "cEndTime", required = false) String cEndTime,
                               @RequestParam(value = "orderStatus", required = false) Integer orderStatus,
                               @RequestParam(value = "tabType", required = false) Integer tabType,
                               @RequestParam(value = "orderType", required = false) Integer orderType,
                               @RequestParam(value = "checkStatus", required = false) String checkStatus,
                               @RequestParam(value = "page", defaultValue = "1") Integer page,
                               @RequestParam(value = "size", defaultValue = "10") Integer size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        SellerOrderListReqDTO reqDTO = new SellerOrderListReqDTO();
        reqDTO.setMerchantId(merchantId.toString());
        reqDTO.setShopId(shopId.toString());
        reqDTO.setOrderNo(orderNo);
        if (productId != null) {
            reqDTO.setProductId(productId.toString());
        }
        reqDTO.setProductName(productName);
        reqDTO.setBuyerName(buyerName);
        reqDTO.setBuyerPhone(buyerPhone);
        reqDTO.setCStartTime(cStartTime);
        reqDTO.setCEndTime(cEndTime);
        reqDTO.setOrderStatus(orderStatus);
        reqDTO.setTabType(tabType);
        reqDTO.setOrderType(orderType);
        reqDTO.setCheckStatus(checkStatus);
        reqDTO.setPage(page <= 0 ? 1 : page);
        reqDTO.setSize(size <= 0 ? 10 : size);
        SellerOrderListResultDTO resultDTO = sellerOrderV6ApiService.getOrderList(reqDTO);
        simpleResponse.setData(resultDTO);
        return simpleResponse;
    }
}
