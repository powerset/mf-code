package mf.code.order.api.applet;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.order.common.caller.wxpay.WXPayUtil;
import mf.code.order.common.caller.wxpay.WeixinPayService;
import mf.code.order.common.caller.wxpay.WxpayProperty;
import mf.code.order.service.OrderWxPayService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.order.api.applet.feignservice
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月10日 21:18
 */
@RestController
@RequestMapping("/api/order/wx")
@Slf4j
public class WxCallbackApi {
    @Autowired
    private OrderWxPayService orderWxPayService;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private WxpayProperty wxpayProperty;

    /**
     * 微信支付回调
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/applet/pay/callback")
    public String wxPayCallback(HttpServletRequest request) {
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        log.info("开始进入回调--");
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信支付回调失败!没有字段参数返回";
        }
        //验证签名
        Map<String, Object> checkSign = CertHttpUtil.paraFilter(wxXmlMap);
        String sortSign = SortUtil.buildSignStr(checkSign);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //对回调的参数进行验签
        if (!MD5Util.md5(sign).toUpperCase()
                .equals(wxXmlMap.get("sign"))) {
            return "wxpayNotify:微信支付回调失败!签名不一致";
        }

        //获取基本数据
        String resultCode = wxXmlMap.get("result_code").toString();
        if ("FAIL".equals(resultCode)) {
            return "支付失败";
        }
        // 订单号
        String orderNo = wxXmlMap.get("out_trade_no").toString();
        //流水号
        String transactionId = wxXmlMap.get("transaction_id").toString();

        // 金额,分转元
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(wxXmlMap.get("total_fee").toString())).divide(new BigDecimal(100));
        // 微信支付时间
        Date paymentTime = DateUtil.parseDate(wxXmlMap.get("time_end").toString(), null, "yyyyMMddHHmmss");
        //微信支付openid标识
        String payOpenId = wxXmlMap.get("openid").toString();

        return this.orderWxPayService.wxPayCallback(orderNo, transactionId, totalFee, paymentTime, payOpenId);
    }

    /**
     * 微信  退款回调
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/applet/refund/callback")
    public String wxRefundCallback(HttpServletRequest request) {
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信退款回调失败!没有字段参数返回";
        }
        String decryptDataReqInfo = null;
        if (wxXmlMap.get("return_code") != null && "SUCCESS".equals(wxXmlMap.get("return_code"))) {
            decryptDataReqInfo = DESUtil.decryptData(wxXmlMap.get("req_info").toString(), wxpayProperty.getMchSecretKey());
        }
        log.info(">>>>>>>>>>decryptDataReqInfo:{}", decryptDataReqInfo);
        Map<String, Object> xmlMapReqInfo = new HashMap<>();
        if (StringUtils.isNotBlank(decryptDataReqInfo)) {
            xmlMapReqInfo = WXPayUtil.xmlToMap(decryptDataReqInfo);
            log.info(">>>>>>>>>>xmlMapReqInfo:{}", xmlMapReqInfo);
        }
        if (xmlMapReqInfo == null || xmlMapReqInfo.size() == 0) {
            return "wxpayNotify:微信退款回调失败!req_info解密没有字段返回";
        }
        //获取基本数据 // SUCCESS-退款成功 CHANGE-退款异常 REFUNDCLOSE—退款关闭
        String refundStatus = xmlMapReqInfo.get("refund_status").toString();
        if (!"SUCCESS".equals(refundStatus)) {
            return "退款未成功";
        }
        // 原订单号
        String orderNo = xmlMapReqInfo.get("out_trade_no").toString();
        //退款订单号
        String refundOrderNo = xmlMapReqInfo.get("out_refund_no").toString();
        // 申请退款退款金额
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(xmlMapReqInfo.get("refund_fee").toString())).divide(new BigDecimal(100));
        //实际退款金额
        BigDecimal settlementRefundFee = BigDecimal.valueOf(NumberUtils.toInt(xmlMapReqInfo.get("settlement_refund_fee").toString())).divide(new BigDecimal(100));
        //微信订单号
        String transactionId = xmlMapReqInfo.get("transaction_id").toString();

        return this.orderWxPayService.wxRefundCallback(orderNo, refundOrderNo, totalFee, settlementRefundFee, transactionId);
    }

    @GetMapping("/testSimpleProductDetail")
    public SimpleResponse xxx(){
        SimpleResponse simpleResponse = new SimpleResponse();
        String json = "[\n" +
                "    {\n" +
                "        \"userId\":122,\n" +
                "        \"productId\":1259,\n" +
                "        \"parentId\":1259,\n" +
                "        \"merchantId\":972,\n" +
                "        \"shopId\":42,\n" +
                "        \"selfSupportRatio\":95,\n" +
                "        \"platSupportRatio\":3,\n" +
                "        \"price\":9.4,\n" +
                "        \"thirdPrice\":14.8,\n" +
                "        \"productTitle\":\"清扬 女士三重净屑技术100ml\",\n" +
                "        \"mainPics\":'[\"https://asset.wxyundian.com/data/seller/goods/215/215_1563509939572wj9.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_1563509944206V3K.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_1563509948996hA7.jpg\"]',\n" +
                "        \"salesVolume\":\"21\",\n" +
                "        \"productSpecs\":50025705,\n" +
                "        \"rebate\":0.02,\n" +
                "        \"mainBanner\":\"https://asset.wxyundian.com/data/seller/goods/215/215_1563509939572wj9.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"userId\":122,\n" +
                "        \"productId\":1145,\n" +
                "        \"parentId\":1145,\n" +
                "        \"merchantId\":49,\n" +
                "        \"shopId\":42,\n" +
                "        \"selfSupportRatio\":95,\n" +
                "        \"platSupportRatio\":10,\n" +
                "        \"price\":22,\n" +
                "        \"thirdPrice\":99,\n" +
                "        \"productTitle\":\"韩国damah黑魔法一次性洗脸巾女纯棉无菌洁面卸妆8\",\n" +
                "        \"mainPics\":'[\"https://asset.wxyundian.com/data/seller/goods/17/17_15633297636217Wz.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563329748271xC4.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563329758335kZU.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563329767733668.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563329738476k35.jpg\"]',\n" +
                "        \"salesVolume\":\"8\",\n" +
                "        \"productSpecs\":50010788,\n" +
                "        \"rebate\":0.18,\n" +
                "        \"mainBanner\":\"https://asset.wxyundian.com/data/seller/goods/17/17_15633297636217Wz.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"userId\":122,\n" +
                "        \"productId\":1348,\n" +
                "        \"parentId\":1348,\n" +
                "        \"merchantId\":49,\n" +
                "        \"shopId\":42,\n" +
                "        \"selfSupportRatio\":95,\n" +
                "        \"platSupportRatio\":10,\n" +
                "        \"price\":19.9,\n" +
                "        \"thirdPrice\":39.9,\n" +
                "        \"productTitle\":\"泰国fibroin童颜蚕丝深层补水保湿提亮肤色\",\n" +
                "        \"mainPics\":'[\"https://asset.wxyundian.com/data/seller/goods/17/17_1563848609540TLw.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563848615351dBm.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_156384864106627v.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563848648012kVr.jpg\",\"https://asset.wxyundian.com/data/seller/goods/17/17_1563848654798r4Y.jpg\"]',\n" +
                "        \"salesVolume\":\"4\",\n" +
                "        \"productSpecs\":1801,\n" +
                "        \"rebate\":0.17,\n" +
                "        \"mainBanner\":\"https://asset.wxyundian.com/data/seller/goods/17/17_1563848609540TLw.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"userId\":122,\n" +
                "        \"productId\":1228,\n" +
                "        \"parentId\":1228,\n" +
                "        \"merchantId\":973,\n" +
                "        \"shopId\":42,\n" +
                "        \"selfSupportRatio\":95,\n" +
                "        \"platSupportRatio\":5,\n" +
                "        \"price\":39.9,\n" +
                "        \"thirdPrice\":69.9,\n" +
                "        \"productTitle\":\"LiLiA去淡化黑眼圈眼袋抗皱细纹眼霜\",\n" +
                "        \"mainPics\":'[\"https://asset.wxyundian.com/data/seller/goods/216/216_1563430091205QC2.jpg\",\"https://asset.wxyundian.com/data/seller/goods/216/216_1563430094262uL7.jpeg\",\"https://asset.wxyundian.com/data/seller/goods/216/216_1563430098142FWK.jpg\",\"https://asset.wxyundian.com/data/seller/goods/216/216_1563430102550HJt.jpg\",\"https://asset.wxyundian.com/data/seller/goods/216/216_1563430105966sgZ.jpg\"]',\n" +
                "        \"salesVolume\":\"3\",\n" +
                "        \"productSpecs\":1801,\n" +
                "        \"rebate\":0.17,\n" +
                "        \"mainBanner\":\"https://asset.wxyundian.com/data/seller/goods/216/216_1563430091205QC2.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"userId\":122,\n" +
                "        \"productId\":1176,\n" +
                "        \"parentId\":1176,\n" +
                "        \"merchantId\":972,\n" +
                "        \"shopId\":42,\n" +
                "        \"selfSupportRatio\":95,\n" +
                "        \"platSupportRatio\":3,\n" +
                "        \"price\":9.4,\n" +
                "        \"thirdPrice\":12.6,\n" +
                "        \"productTitle\":\"奇强内衣抑菌皂 促销装 洗内裤100g*2块\",\n" +
                "        \"mainPics\":'[\"https://asset.wxyundian.com/data/seller/goods/215/215_15633549582716Y0.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_15633549623343v8.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_1563354966134l6n.jpg\"]',\n" +
                "        \"salesVolume\":\"3\",\n" +
                "        \"productSpecs\":50016348,\n" +
                "        \"rebate\":0.02,\n" +
                "        \"mainBanner\":\"https://asset.wxyundian.com/data/seller/goods/215/215_15633549582716Y0.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"userId\":122,\n" +
                "        \"productId\":1168,\n" +
                "        \"parentId\":1168,\n" +
                "        \"merchantId\":972,\n" +
                "        \"shopId\":42,\n" +
                "        \"selfSupportRatio\":95,\n" +
                "        \"platSupportRatio\":3,\n" +
                "        \"price\":9.4,\n" +
                "        \"thirdPrice\":9.9,\n" +
                "        \"productTitle\":\"三笑B22N舒适软毛深层洁净成人牙刷 *5支\",\n" +
                "        \"mainPics\":'[\"https://asset.wxyundian.com/data/seller/goods/215/215_1563438422666e5Z.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_1563438430265NaS.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_156343843621186u.jpg\",\"https://asset.wxyundian.com/data/seller/goods/215/215_15634384404934x9.jpg\"]',\n" +
                "        \"salesVolume\":\"3\",\n" +
                "        \"productSpecs\":50016348,\n" +
                "        \"rebate\":0.02,\n" +
                "        \"mainBanner\":\"https://asset.wxyundian.com/data/seller/goods/215/215_1563438422666e5Z.jpg\"\n" +
                "    }\n" +
                "    ]";
        simpleResponse.setData(JSONObject.parseArray(json));
        return simpleResponse;
    }
}
