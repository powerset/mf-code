package mf.code.order.api.platform;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.PlatformCashDTO;
import mf.code.order.service.MerchantOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.order.api.platform
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月06日 17:18
 */
@RestController
@RequestMapping("/api/order/platform/v5")
@Slf4j
public class PlatformMerchantOrderApi {
    @Autowired
    private MerchantOrderService merchantOrderService;

    /***
     * 财务管理-商家提现列表
     * @param platformId
     * @return
     */
    @GetMapping("/getCashLog")
    public SimpleResponse getCashLog(@RequestParam(name = "platformId") String platformId,
                                     @RequestParam(name = "phone", required = false) String phone,
                                     @RequestParam(name = "shopName", required = false) String shopName,
                                     @RequestParam(name = "start", required = false) String start,
                                     @RequestParam(name = "end", required = false) String end,
                                     @RequestParam(name = "size", defaultValue = "10") Integer limit,
                                     @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum) {
        return this.merchantOrderService.getCashLog(platformId, phone, shopName, start, end, limit, pageNum);
    }

    /***
     * 根据商户账号查询旗下的所有未删除的店铺
     * @param phone
     * @return 店铺集合
     */
    @GetMapping("/getShopListByMerchantPhone")
    public SimpleResponse getShopListByMerchantPhone(@RequestParam(name = "platformId") String platformId,
                                                     @RequestParam(name = "phone", required = false) String phone) {
        return this.merchantOrderService.getShopListByMerchantPhone(platformId, phone);
    }

    /***
     * 运营平台人员-提交提现记录
     * @param platformCashDTO
     * @return
     */
    @PostMapping("/commitCashRecord")
    public SimpleResponse commitCashRecord(@RequestBody PlatformCashDTO platformCashDTO) {
        return this.merchantOrderService.commitCashRecord(platformCashDTO);
    }
}
