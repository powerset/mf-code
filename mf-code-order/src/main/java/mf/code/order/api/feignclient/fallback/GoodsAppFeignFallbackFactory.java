package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.order.api.feignclient.GoodsAppService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 11:10
 */
@Component
@Slf4j
public class GoodsAppFeignFallbackFactory implements FallbackFactory<GoodsAppService> {
    @Override
    public GoodsAppService create(Throwable cause) {
        return new GoodsAppService() {
            @Override
            public GoodsSkuDTO queryRandomSkuOnsale() {
                return new GoodsSkuDTO();
            }

            @Override
            public Map<Long, GoodsSkuDTO> queryProductSkuList(Long merchantId, Long shopId, List<Long> goodsIds) {
                return new HashMap<>();
            }

            @Override
            public GoodsSkuDTO queryProductSku(Long merchantId, Long shopId, Long goodsId, Long skuId, Long userId) {
                return null;
            }

            @Override
            public GoodProductDTO queryProduct(Long merchantId, Long shopId, Long goodsId) {
                return null;
            }

            @Override
            public AppletMybatisPageDto pageGoodsSale(Long shopId, Long offset, Long size) {
                return new AppletMybatisPageDto();
            }

            @Override
            public ProductDistributionDTO queryGoodsDistribution(String goodsId) {
                return new ProductDistributionDTO();
            }

            @Override
            public SimpleResponse queryProductSkuMapBySkuIdList(List<Long> skuIdList) {
                return null;
            }

            @Override
            public SimpleResponse consumeStock(Long productSkuId, int stockNumber, Long orderId) {
                return new SimpleResponse(ApiStatusEnum.ERROR);
            }

            @Override
            public SimpleResponse giveBackStock(Long productSkuId, int stockNumber, Long orderId) {
                return new SimpleResponse(ApiStatusEnum.ERROR);
            }

            @Override
            public String getPlatformDistRateCommission(Long merchantId, Long shopId, Long skuId) {
                return null;
            }
        };
    }
}
