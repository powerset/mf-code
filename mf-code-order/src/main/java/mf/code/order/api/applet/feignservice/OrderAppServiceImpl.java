package mf.code.order.api.applet.feignservice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.common.caller.wxpay.WXPayUtil;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.domain.aggregateroot.AppletOrder;
import mf.code.order.domain.aggregateroot.OrderUser;
import mf.code.order.domain.valueobject.UserOrderHistory;
import mf.code.order.dto.*;
import mf.code.order.feignapi.constant.*;
import mf.code.order.repo.dao.MerchantOrderMapper;
import mf.code.order.repo.dao.OrderMapper;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.AppletOrderRepository;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.OrderUserRepository;
import mf.code.order.service.*;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqList;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.order.api.applet.feignservice
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:44
 */
@Slf4j
@RestController
public class OrderAppServiceImpl {
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderUserRepository orderUserRepository;
    @Autowired
    private AppletOrderRepository appletOrderRepository;
    @Autowired
    private MerchantOrderMapper merchantOrderMapper;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;
    @Autowired
    private OrderAboutService orderAboutService;
    @Autowired
    private MerchantOrderService merchantOrderService;
    @Autowired
    private OrderCallBackService orderCallBackService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 抖带带-我的页面订单/佣金数据
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping("/feignclient/order/applet/doudaidai/mypageData")
    public DoudaidaiMypageResp getDoudaidaiMypageData(@RequestParam("merchantId") Long merchantId,
                                                      @RequestParam("shopId") Long shopId) {
        if (merchantId == null) {
            log.error("merchantId参数错误");
            return null;
        }
        if (shopId == null) {
            log.error("shopId参数错误");
            return null;
        }
        DoudaidaiMypageResp doudaidaiMypageResp = new DoudaidaiMypageResp();
        Date yesterday = DateUtil.addDay(new Date(), -1);
        Date yesterdayEnd = DateUtil.end(yesterday);
        Date yesterdayBegin = DateUtil.begin(yesterday);
        // 累计佣金       √
        doudaidaiMypageResp.setCommissionTotal(merchantOrderService.commissionTotal(shopId, null, null));
        // 可提现余额     √
        doudaidaiMypageResp.setCommissionBalance(this.handleGetBalanceFunc(merchantOrderService.getBalance(merchantId, shopId)));
        // 待结算佣金     √
        doudaidaiMypageResp.setCommissionWaitClose(merchantOrderService.commissionWaitClose(shopId, null, null));
        // 昨日佣金       √
        doudaidaiMypageResp.setOrderYtdCommission(merchantOrderService.commissionTotal(shopId, yesterdayBegin, yesterdayEnd));

        // 销售金额
        doudaidaiMypageResp.setOrderTotalFee(orderService.orderTotalFee(shopId, null, null));
        doudaidaiMypageResp.setOrderYtdTotalFee(orderService.orderTotalFee(shopId, yesterdayBegin, yesterdayEnd));
        // 订单量
        doudaidaiMypageResp.setOrderTotalNum(orderService.orderNumber(shopId, null, null));
        doudaidaiMypageResp.setOrderYtdTotalNum(orderService.orderNumber(shopId, yesterdayBegin, yesterdayEnd));

        return doudaidaiMypageResp;
    }

    private BigDecimal handleGetBalanceFunc(SimpleResponse simpleResponse) {
        BigDecimal balance = new BigDecimal(0);
        if (simpleResponse != null) {
            Map data = (Map) simpleResponse.getData();
            if (data != null) {
                Object balanceObj = data.get("balance");
                if (balance != null) {
                    String tempStr = balanceObj.toString();
                    balance = new BigDecimal(tempStr);
                }
            }
        }
        return balance;
    }


    /***
     * 获取订单分页信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 0：全部 1待付款 2 待发货 3待收货 4 交易成功 5 退款/售后
     * @param limit
     * @param offset
     * @return
     */
    @GetMapping("/feignclient/order/applet/getGoodsOrderByPage")
    public SimpleResponse getGoodsOrderByPage(@RequestParam("merchantId") Long merchantId,
                                              @RequestParam("shopId") Long shopId,
                                              @RequestParam(name = "userId") Long userId,
                                              @RequestParam(name = "type", defaultValue = "1", required = false) int type,
                                              @RequestParam(name = "limit", required = false, defaultValue = "10") int limit,
                                              @RequestParam(name = "offset", required = false, defaultValue = "0") int offset) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入有效参数...");
        }
        return this.orderService.getGoodsOrderByPage(merchantId, shopId, userId, type, limit, offset);
    }

	/**
	 * 通过 订单编号 批量取消 订单
	 *
	 * @param orderNoList
	 * @return
	 */
	@GetMapping("/feignapi/order/applet/v7/cancelOrderByOrderNoList")
	public boolean cancelOrderByOrderNoList(@RequestParam("orderNoList") List<String> orderNoList) {
		if (CollectionUtils.isEmpty(orderNoList)) {
			log.error("参数异常，orderNoList = {}", orderNoList);
			return false;
		}
		QueryWrapper<Order> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.in(Order::getOrderNo, orderNoList);
		List<Order> orders = goodsOrderRepository.list(wrapper);
		if (CollectionUtils.isEmpty(orders)) {
			log.info("查无此订单编号的订单， orderNoList = {}", orderNoList);
			return true;
		}
		List<Order> cancelOrders = new ArrayList<>();
		Date now = new Date();
		for (Order order : orders) {
			order.setStatus(OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode());
			order.setUtime(now);
			cancelOrders.add(order);
		}
		return goodsOrderRepository.updateBatchById(cancelOrders);
	}

    /**
     * 存储订单信息
     *
     * @return
     */
    @PostMapping("/feignapi/order/applet/v7/batchSaveOrderRespList")
    public boolean batchSaveOrderRespList(@RequestBody OrderRespListDTO orderRespListDTO) {
        if (orderRespListDTO == null || CollectionUtils.isEmpty(orderRespListDTO.getOrderRespList())) {
            return false;
        }
        List<Order> orderList = new ArrayList<>();
        for (OrderResp orderResp : orderRespListDTO.getOrderRespList()) {
            Order order = new Order();
            BeanUtils.copyProperties(orderResp, order);
            orderList.add(order);
        }
        try {
            return goodsOrderRepository.saveBatch(orderList);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByOrder")
    public int countGoodsNumByOrder(@RequestParam(name = "goodsId") Long goodsId) {
        return this.goodsOrderRepository.countByGoodsIdsByOrder(goodsId);
    }

    @GetMapping("/feignapi/order/applet/v5/queryOrder")
    public OrderResp queryOrder(@RequestParam(name = "orderId") Long orderId) {
        if (orderId == null) {
            return null;
        }
        Order goodsOrder = this.goodsOrderRepository.selectById(orderId);
        OrderResp resp = new OrderResp();
        BeanUtils.copyProperties(goodsOrder, resp);
        return resp;
    }

    @GetMapping("/feignapi/order/applet/v5/queryOrderByOrderNo")
    public OrderResp queryOrderByOrderNo(@RequestParam(name = "orderNo") Long orderNo) {
        if (orderNo == null) {
            return null;
        }
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Order::getOrderNo, orderNo)
                .eq(Order::getDel, DelEnum.NO.getCode());

        Order goodsOrder = orderMapper.selectOne(queryWrapper);
        OrderResp resp = new OrderResp();
        BeanUtils.copyProperties(goodsOrder, resp);
        return resp;
    }

    /***
     * 根据时间查询是否有下单/购买订单
     * @param shopId 店铺编号
     * @param userId 用户编号
     * @param time 时间格式：yyyyyMMddHHmmss
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/selectOrder")
    public OrderResp selectOrder(@RequestParam(name = "shopId") Long shopId,
                                 @RequestParam(name = "userId") Long userId,
                                 @RequestParam(name = "time") String time) {
        Date date = DateUtil.parseDate(time, null, "yyyyMMddHHmmss");
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
        ;
        if (date != null) {
            wrapper.and(params -> params.ge("ctime", date));
        }
        wrapper.orderByDesc("id");
        List<Order> orders = goodsOrderRepository.list(wrapper);
        if (CollectionUtils.isEmpty(orders)) {
            return null;
        }
        OrderResp resp = new OrderResp();
        BeanUtils.copyProperties(orders.get(0), resp);
        return resp;
    }

    /**
     * 根据店铺id,产品id统计订单数
     *
     * @param shopId
     * @param skuIdList
     * @return int统计数量
     */
    @GetMapping("/feignapi/order/applet/v5/summaryOrderByShopIdAndProductId")
    public int summaryOrderByShopIdAndProductId(@RequestParam(name = "shopId") Long shopId,
                                                @RequestParam(name = "skuIdList") List<Long> skuIdList) {
        Map<String, Object> conditionMap = new HashMap<>();
        conditionMap.put("shopId", shopId);
        conditionMap.put("skuIdList", skuIdList);
        Long result = goodsOrderRepository.summaryOrderByShopIdAndProductId(conditionMap);
        if (result != null) {
            return result.intValue();
        }
        return 0;
    }

    /**
     * 获取 此用户在此店铺的 所有 可结算订单 -- 进行结算处理
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderSettledByUserIdShopId")
    public List<OrderResp> queryOrderSettledByUserIdShopId(@RequestParam("userId") Long userId, @RequestParam("shopId") Long shopId) {
        if (userId == null || shopId == null || userId <= 0 || shopId <= 0) {
            log.error("参数异常");
            return null;
        }
        OrderUser orderUser = orderUserRepository.findByShopIdUserId(shopId, userId);
        if (orderUser == null) {
            log.error("无效用户, userId = {}", userId);
            return null;
        }
        orderUserRepository.addUserOrderHistory(orderUser, DateUtil.getBeginOfLastMonth(), DateUtil.getEndOfLastMonth());
        UserOrderHistory userOrder = orderUser.getUserOrderHistory();

        return userOrder.querySettledOrders();
    }

	/**
	 * 获取 当前店铺下 用户上级成员 某段时间内 消费金额
	 *
	 * @param param <per>
	 *              List<Long> userIds
	 *              Long shopId
	 *              Date beginTime
	 *              Date endTime
	 *              </per>
	 * @return JSON.toJSONString(Map < Long, BigDecimal >)
	 * <per>
	 * Long userId
	 * BigDecimal 消费金额
	 * </per>
	 */
	@GetMapping("/feignapi/order/applet/v5/listUserSpendAmount")
	public String listUserSpendAmount(@RequestParam("param") Map<String, Object> param) {
		if (CollectionUtils.isEmpty(param) || param.size() < 4) {
			log.error("参数异常");
			return null;
		}
		Long shopId = (Long) param.get("shopId");
		List userIds = (List) param.get("userIds");
		Date beginTime = (Date) param.get("beginTime");
		Date endTime = (Date) param.get("endTime");

        Map<Long, BigDecimal> userSpendAmountMap = new HashMap<>();
        for (Object obj : userIds) {
            Long userId = (Long) obj;
            OrderUser orderUser = orderUserRepository.findByShopIdUserId(shopId, userId);
            if (orderUser == null) {
                log.error("无效用户，userId = {}", userId);
                continue;
            }
            orderUserRepository.addUserOrderHistory(orderUser, beginTime, endTime);
            UserOrderHistory userOrder = orderUser.getUserOrderHistory();
            userSpendAmountMap.put(userId, userOrder.querySpendAmount());
        }

        return JSON.toJSONString(userSpendAmountMap);
    }

    /**
     * 查询 用户 在此店铺得 本月的消费记录
     *
     * @param userId
     * @param shopId
     * @return 查询条件：
     * <per>
     * userId
     * shopId
     * type = 商品购买-付款
     * status = 支付成功
     * payment_time = 本月
     * </per>
     */
    @GetMapping("/feignapi/order/applet/v5/queryOrderRecordThisMonth")
    public Map<String, OrderResp> queryOrderRecordThisMonth(@RequestParam("userId") Long userId,
                                                            @RequestParam("shopId") Long shopId) {
        if (userId == null || userId <= 0
                || shopId == null || shopId <= 0) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return null;
        }

        // 配置查询条件
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getUserId, userId)
                .eq(Order::getShopId, shopId)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .eq(Order::getStatus, OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode())
                .between(Order::getPaymentTime, DateUtil.getBeginOfThisMonth(), new Date());
        List<Order> orders = goodsOrderRepository.list(wrapper);

        // 处理返回结果
        Map<String, OrderResp> result = new HashMap<>();
        if (!CollectionUtils.isEmpty(orders)) {
            for (Order order : orders) {
                OrderResp orderResp = new OrderResp();
                BeanUtils.copyProperties(order, orderResp);
                result.put(order.getId().toString(), orderResp);
            }
        }
        return result;
    }

    /**
     * 通过商品id 获取商品销量
     *
     * @param productIds
     * @return Map<productIdsalesVolume>
     */
    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByProductIds")
    public Map<String, String> countGoodsNumByProductIds(@RequestParam("productIds") List<Long> productIds) {
        Map<String, String> salesVolumeMap = new HashMap<>();
        if (CollectionUtils.isEmpty(productIds)) {
            return salesVolumeMap;
        }
        for (Long productId : productIds) {
            salesVolumeMap.put(productId.toString(), String.valueOf(this.countGoodsNumByOrder(productId)));
        }
        return salesVolumeMap;
    }

    /**
     * 获取 当前店铺下 某段时间内 用户订单历史记录
     * <per>
     * Long userId
     * Long shopId
     * Date beginTime
     * Date endTime
     * </per>
     *
     * @param param
     * @return JSON.toJSONString(AppletUserOrderHistoryDTO)
     */
    @GetMapping(value = "/feignapi/order/applet/v5/queryUserOrderHistory")
    public AppletUserOrderHistoryDTO queryUserOrderHistory(@RequestParam("param") String param) {
        if (StringUtils.isBlank(param)) {
            log.error("参数异常");
            return null;
        }
        JSONObject jsonObject = JSON.parseObject(param);
        Long shopId = jsonObject.getLong("shopId");
        Long userId = jsonObject.getLong("userId");
        Date beginTime = jsonObject.getDate("beginTime");
        Date endTime = jsonObject.getDate("endTime");

        OrderUser orderUser = orderUserRepository.findByShopIdUserId(shopId, userId);
        if (orderUser == null) {
            log.error("无效用户，userId = {}", userId);
            return null;
        }

        orderUserRepository.addUserOrderHistory(orderUser, beginTime, endTime);
        return orderUser.copyUserOrderHistory();
    }

	/**
	 * 批量获取 当前店铺下 某段时间内 用户订单历史记录
	 * <per>
	 * List<Long> userIds
	 * Long shopId
	 * Date beginTime
	 * Date endTime
	 * </per>
	 *
	 * @param param
	 * @return JSON.toJSONString(List < AppletUserOrderHistoryDTO >)
	 */
	@GetMapping(value = "/feignapi/order/applet/v5/listUserOrderHistory")
	List<AppletUserOrderHistoryDTO> listUserOrderHistory(@RequestParam("param") String param) {
		if (StringUtils.isBlank(param)) {
			log.error("参数异常");
			return null;
		}
		JSONObject jsonObject = JSON.parseObject(param);
		JSONArray userIdJsonArray = jsonObject.getJSONArray("userIds");

        Long shopId = jsonObject.getLong("shopId");
        List<Long> userIds = userIdJsonArray.toJavaList(Long.class);
        Date beginTime = jsonObject.getDate("beginTime");
        Date endTime = jsonObject.getDate("endTime");

        List<OrderUser> orderUsers = orderUserRepository.listByShopIdUserList(shopId, userIds);

        if (CollectionUtils.isEmpty(orderUsers)) {
            log.error("获取用户服务异常，shopId = {}, userIds = {}", shopId, userIds);
            return null;
        }
        orderUserRepository.listAddUserOrderHistory(orderUsers, shopId, beginTime, endTime);

        List<AppletUserOrderHistoryDTO> appletUserOrderHistoryDTOList = new ArrayList<>();
        for (OrderUser orderUser : orderUsers) {
            AppletUserOrderHistoryDTO appletUserOrderHistoryDTO = orderUser.copyUserOrderHistory();
            appletUserOrderHistoryDTOList.add(appletUserOrderHistoryDTO);
        }

        return appletUserOrderHistoryDTOList;
    }


    /**
     * 获取 订单信息
     *
     * @param orderId
     * @return JSON.toJSONString(AppletOrderResp)
     */
    @GetMapping("/feignapi/order/applet/v5/queryAppletOrder")
    public String queryAppletOrder(@RequestParam("orderId") Long orderId) {
        if (orderId == null || orderId < 1) {
            return null;
        }
        AppletOrder appletOrder = appletOrderRepository.findById(orderId);
        if (appletOrder == null) {
            log.error("order服务器告警：查无此订单, orderId = {}", orderId);
            return null;
        }

        AppletOrderResp appletOrderResp = appletOrder.copyAppletOrder();
        return JSON.toJSONString(appletOrderResp);
    }

    @GetMapping("/feignapi/order/applet/v5/queryOrderNumByProductIds")
    public Integer queryOrderNumByProductIds(@RequestParam("shopId") Long shopId,
                                             @RequestParam("userId") Long userId,
                                             @RequestParam("productIds") List<Long> productIds) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .in(Order::getStatus, Arrays.asList(
                        OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                        OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
                        OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()))
                .in(Order::getGoodsId, productIds)
        ;
        int count = goodsOrderRepository.count(wrapper);
        log.info("<<<<<<<< 根据类型查询订单数目 shopId:{}", shopId);
        return count;
    }

    /**
     * 获取某时间段 所有未在售后处理中的分销业务订单 (可结算订单) -- 只查集客魔方订单
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/feignapi/order/applet/v6/querySettlementOrdersByTime")
    public List<OrderResp> querySettlementOrdersByTime(@RequestParam("beginTime") String beginTime,
                                                       @RequestParam("endTime") String endTime) {
        if (StringUtils.isBlank(beginTime) || StringUtils.isBlank(endTime)) {
            log.error("参数异常， beginTime = {}, endTime = {}", beginTime, endTime);
            return null;
        }
        Date beginTimeDate = DateUtil.stringtoDate(beginTime, DateUtil.FORMAT_ONE);
        Date endTimeDate = DateUtil.stringtoDate(endTime, DateUtil.FORMAT_ONE);


        // 获取某时间段 所有交易成功的分销业务订单
        Map<String, Object> params = new HashMap<>();
        params.put("type", OrderTypeEnum.PAY.getCode());
        params.put("status", OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
        params.put("beginTime", beginTimeDate);
        params.put("endTime", endTimeDate);
        params.put("bizType", OrderBizBizTypeEnum.DISTRIBUTION.getCode());
        params.put("source", OrderSourceEnum.JKMF.getCode());
        List<OrderResp> orders = orderMapper.selectListWithBiz(params);
        if (CollectionUtils.isEmpty(orders)) {
            log.info("查无可结算订单");
            return null;
        }

        // 获取某时间段 正在售后处理中的订单
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .ne(Order::getType, OrderTypeEnum.PAY.getCode())
                .in(Order::getStatus, Arrays.asList(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode(),
                        OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                        OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()))
                .between(Order::getCtime, beginTimeDate, new Date());
        List<Order> afterSales = orderMapper.selectList(wrapper);
        Map<Long, Order> afterSalesMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(afterSales)) {
            for (Order order : afterSales) {
                afterSalesMap.put(order.getRefundOrderId(), order);
            }
        }
        if (CollectionUtils.isEmpty(afterSalesMap)) {
            return orders;
        }

        // 返回 获取某时间段 所有未在售后处理中的分销业务订单
        List<OrderResp> settlementOrder = new ArrayList<>();
        for (OrderResp order : orders) {
            if (afterSalesMap.containsKey(order.getId())) {
                continue;
            }
            settlementOrder.add(order);
        }

        return settlementOrder;
    }

	/**
	 * 获取 此用户 可结算的平台补贴订单
	 *
	 * @param userId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	@GetMapping("/feignapi/order/applet/v7/querySettlementSubsidyOrdersByTime")
	public List<OrderResp> querySettlementSubsidyOrdersByTime(@RequestParam("userId") Long userId,
	                                                   @RequestParam("beginTime") Date beginTime,
	                                                   @RequestParam("endTime") Date endTime) {
		if (userId < 1) {
			log.error("参数异常, userId = {}", userId);
			return null;
		}
		List<Integer> bizTypeList = new ArrayList<>();
		bizTypeList.add(OrderBizTypeEnum.SUBSIDY_USER.getCode());
		bizTypeList.add(OrderBizTypeEnum.RESELL.getCode());
		QueryWrapper<Order> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Order::getType, OrderTypeEnum.PLATFORM_SUBSIDY.getCode())
				.eq(Order::getStatus, OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode())
				.in(Order::getBizType, bizTypeList)
				.eq(Order::getBizValue, userId)
				.between(Order::getCtime, beginTime, endTime);
		List<Order> orderList = goodsOrderRepository.list(wrapper);
		if (CollectionUtils.isEmpty(orderList)) {
			log.info("此用户无平台补贴订单， userId = {}", userId);
			return null;
		}
		List<OrderResp> orderRespList = new ArrayList<>();
		for (Order order : orderList) {
			OrderResp orderResp = new OrderResp();
			BeanUtils.copyProperties(order, orderResp);
			orderRespList.add(orderResp);
		}

        return orderRespList;
    }

    /**
     * 通过订单id 获取订单信息
     *
     * @param orderIds
     * @return
     */
    @GetMapping("/feignapi/order/applet/v7/listByIds")
    public List<OrderResp> listByIds(@RequestParam("orderIds") List<Long> orderIds) {
        if (CollectionUtils.isEmpty(orderIds)) {
            log.error("参数异常");
            return null;
        }
        Collection<Order> orders = goodsOrderRepository.listByIds(orderIds);
        if (CollectionUtils.isEmpty(orders)) {
            return null;
        }
        List<OrderResp> orderRespList = new ArrayList<>();
        for (Order order : orders) {
            OrderBizStatusEnum bizStatus = this.orderAboutService.getBizStatus(order);
            OrderResp orderResp = new OrderResp();
            BeanUtils.copyProperties(order, orderResp);
            orderResp.setBizStatus(bizStatus.getCode());
            orderRespList.add(orderResp);
        }
        return orderRespList;
    }

    /**
     * 通过 时间范围 获取所有用户 所有店铺的 已支付-未退款订单
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/feignapi/order/applet/v8/queryPaidOrdersWithBiz")
    public List<OrderResp> queryPaidOrdersWithBiz(@RequestParam("beginTime") String beginTime,
                                                  @RequestParam("endTime") String endTime) {
        if (StringUtils.isBlank(beginTime) || StringUtils.isBlank(endTime)) {
            log.error("参数异常");
            return null;
        }
        Date beginTimeDate = DateUtil.stringtoDate(beginTime, DateUtil.FORMAT_ONE);
        Date endTimeDate = DateUtil.stringtoDate(endTime, DateUtil.FORMAT_ONE);

        Map<String, Object> params = new HashMap<>();
        params.put("type", OrderTypeEnum.PAY.getCode());
        params.put("statusList", Arrays.asList(OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
                OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
        params.put("beginTime", beginTimeDate);
        params.put("endTime", endTimeDate);
        params.put("bizType", OrderBizBizTypeEnum.DISTRIBUTION.getCode());
        List<OrderResp> orders = orderMapper.selectListWithBiz(params);

        return orders == null ? new ArrayList<>() : orders;

    }

    /**
     * 批量更新用户余额
     */
    @PostMapping("/feignapi/order/applet/batchUpdateMerchantBalance")
    public void batchUpdateMerchantBalance(@RequestBody UpayWxOrderReqList upayWxOrderReqList) {
        if (upayWxOrderReqList == null || CollectionUtils.isEmpty(upayWxOrderReqList.getUpayWxOrderReqList())) {
            log.error("用户余额结算异常，接受到的参数为空");
            return;
        }

        for (UpayWxOrderReq upayWxOrderReq : upayWxOrderReqList.getUpayWxOrderReqList()) {
            // 创建用户余额流水
            MerchantOrder merchantOrder = new MerchantOrder();
            BeanUtils.copyProperties(upayWxOrderReq, merchantOrder);
            merchantOrder.setMfTradeType(upayWxOrderReq.getTradeType());
            merchantOrder.setMerchantId(upayWxOrderReq.getMchId());
            try {
                int insert = merchantOrderMapper.insertSelective(merchantOrder);
                if (insert == 0) {
                    log.info("触发 订单防重机制 order_no = {}", merchantOrder.getOrderNo());
                    continue;
                }
            } catch (Exception e) {
                log.error("触发 订单防重机制 order_no = {}, e = {}", merchantOrder.getOrderNo(), e);
                continue;
            }

            try {
                int rows = merchantBalanceRepository.updateBalance(merchantOrder.getMerchantId(), merchantOrder.getShopId(), merchantOrder.getTotalFee());
                if (rows == 0) {
                    log.error("用户金额更新失败：merchantId = {}, shopId = {}, amount = {}", merchantOrder.getMerchantId(), merchantOrder.getShopId(), merchantOrder.getTotalFee());
                }
            } catch (Exception e) {
                log.error("用户金额更新失败：merchantId = {}, shopId = {}, amount = {}, e = {}", merchantOrder.getMerchantId(), merchantOrder.getShopId(), merchantOrder.getTotalFee(), e);
            }
        }
    }

    /**
     * 店铺订单总额-销售金额
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/orderTotalFee")
    public String orderTotalFee(@RequestParam("shopId") Long shopId,
                                @RequestParam(name = "begin", required = false) Date begin,
                                @RequestParam(name = "end", required = false) Date end) {
        BigDecimal bigDecimal = orderService.orderTotalFee(shopId, begin, end);
        return bigDecimal.toString();
    }

    /**
     * 查询订单数
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/feignapi/order/applet/orderNumber")
    public Integer orderNumber(@RequestParam("shopId") Long shopId,
                               @RequestParam(name = "begin", required = false) Date begin,
                               @RequestParam(name = "end", required = false) Date end) {
        Integer integer = orderService.orderNumber(shopId, begin, end);
        return integer;
    }

    /**
     * 店铺总佣金
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/feignapi/order/applet/commissionTotal")
    public String commissionTotal(@RequestParam("shopId") Long shopId,
                                  @RequestParam(name = "begin", required = false) Date begin,
                                  @RequestParam(name = "end", required = false) Date end) {
        BigDecimal bigDecimal = merchantOrderService.commissionTotal(shopId, begin, end);
        return bigDecimal.toString();
    }

    /***
     * 购物订单-支付回调的处理
     *
     * @param dto
     * @return
     */
    @PostMapping("/feignapi/order/applet/updateOrderByPayCallback")
    @Transactional
    public SimpleResponse updateOrderByPayCallback(@RequestBody AlipayAppPayCallBackDTO dto) {
        log.info("<<<<<<<<开始更新订单 body:{}", JSONObject.toJSONString(dto));
        QueryWrapper<Order> orderWrapper = new QueryWrapper<>();
        orderWrapper.lambda()
                .eq(Order::getOrderNo, dto.getOutTradeNo());
        Order order = this.goodsOrderRepository.selectOne(orderWrapper);
        if (order == null) {
            log.error("订单已不存在，orderNo:{}", dto.getOutTradeNo());
        }
        //若订单已经不再是待付款状态直接返回
        if (order.getStatus() != OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            log.warn("支付回调警告-回调方法调用订单，该订单已经不是待付款状态");
            return new SimpleResponse();
        }
        if (StringUtils.isBlank(dto.getTotalAmount()) || new BigDecimal(dto.getTotalAmount()).subtract(order.getFee()).compareTo(BigDecimal.ZERO) != 0) {
            log.warn("支付失败-下单金额跟回调的金额不匹配 totalPrice:{};orderFee:{}", dto.getTotalAmount(), order.getFee());
        }

        order.setPaymentTime(DateUtil.parseDate(dto.getGmtPayment(), null, "yyyy-MM-dd HH:mm:ss"));
        order.setTradeId(dto.getTradeNo());
        order.setStatus(OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        order.setFee(new BigDecimal(dto.getTotalAmount()).setScale(2, BigDecimal.ROUND_DOWN));
        order.setNotifyTime(new Date());
        order.setUtime(new Date());
        int i = orderStatusService.buyerPurchaseSuccess(order);
        log.info("订单更新成功与否：{} orderId:{}", i, order.getId());
        if (i == 0) {
            log.error("订单更新成功与否 orderId:{}", order.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1);
        }
        //若该订单是抖小铺买家购买商品，需要返佣给商家
        if(order.getSource() == OrderSourceEnum.DOUYIN_SHOP.getCode()){
            orderCallBackService.payOrderCallBack(order);
        }
        GoodsOrderReq req = WxCallbackAboutDTO.addCreateOrderRedisKey(order);
        stringRedisTemplate.delete(RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey());
        log.info("<<<<<<<<更新订单结束 simpleResponse:{}", JSONObject.toJSONString(new SimpleResponse()));
        return new SimpleResponse();
    }
}
