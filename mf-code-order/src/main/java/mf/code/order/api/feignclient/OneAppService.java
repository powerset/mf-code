package mf.code.order.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.UserCouponDTO;
import mf.code.order.api.feignclient.fallback.OneAppFeignFallbackFactory;
import mf.code.order.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.user.api.feignclient
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:06
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {

    /**
     * 拆红包是否完成了
     *
     * @param shopId
     * @param userId
     * @return null无活动定义；有活动定义 且用户：0没有活动, 2有进行中活动, 1 完成-金额待解锁
     */
    @GetMapping("/feignapi/one/applet/v6/queryOpenRedPackageStatus")
    Map<String, Object> queryOpenRedPackageStatus(@RequestParam("shopId") Long shopId,
                                                  @RequestParam("userId") Long userId);

    /**
     * 完成支付 -- 拆红包任务完成
     *
     * @param activityId
     */
    @GetMapping("/feignapi/one/applet/v6/completeOpenRedPackagePay")
    SimpleResponse completeOpenRedPackagePay(@RequestParam("activityId") Long activityId);

    /**
     * 查询用户是否拥有尚未使用的平台活动中奖资格
     */
    @GetMapping("/feignapi/one/applet/checkUserTaskForPlatOrderBack")
    SimpleResponse checkUserTaskForPlatOrderBack(@RequestParam(value = "userId") Long userId,
                                                 @RequestParam(value = "userTaskId") Long userTaskId);

    /**
     * 查询用户是否拥有尚未使用的平台活动中奖资格
     */
    @GetMapping("/feignapi/one/applet/updateUserTaskForPlatOrderBack")
    SimpleResponse updateUserTaskForPlatOrderBack(@RequestParam(value = "userId") Long userId,
                                                  @RequestParam(value = "userTaskId") Long userTaskId);


    /**
     * 通过优惠券id查询优惠券
     *
     * @param couponId
     * @return
     */
    @GetMapping("/feignapi/one/applet/coupon/findCouponById")
    UserCouponDTO findCouponById(@RequestParam("couponId") Long couponId);

    /**
     * 使用优惠券
     *
     * @param couponId
     * @return
     */
    @GetMapping("/feignapi/one/applet/coupon/useUserCoupon")
    SimpleResponse useUserCoupon(@RequestParam("couponId") Long couponId);

    /**
     * 返回优惠券
     *
     * @param couponId
     * @return
     */
    @GetMapping("/feignapi/one/applet/coupon/backUserCoupon")
    SimpleResponse backUserCoupon(@RequestParam("couponId") Long couponId);
}
