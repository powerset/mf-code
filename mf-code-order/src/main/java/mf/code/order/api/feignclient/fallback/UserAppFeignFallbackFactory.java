package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class UserAppFeignFallbackFactory implements FallbackFactory<UserAppService> {
    @Override
    public UserAppService create(Throwable throwable) {
        return new UserAppService() {
            @Override
            public Long addUser(UserResp userResp) {
                return 0L;
            }

            @Override
            public SimpleResponse cancelDistributionCommission(String orderNo, String shopId) {
                return null;
            }

            @Override
            public UserAddressReq queryAdddress(Long addressId) {
                return null;
            }

            @Override
            public UserResp queryUser(Long userId) {
                return null;
            }

            @Override
            public int saveUpayWxOrder(UpayWxOrderReqDTO reqDTO) {
                return 0;
            }

            @Override
            public SimpleResponse queryUserAndAddressForSeller(Long userId, Long addressId) {
                return null;
            }

            @Override
            public SimpleResponse queryAddressMapForSeller(List<Long> addressIdList) {
                return null;
            }

            @Override
            public SimpleResponse queryAddressIdByAddressNickOrPhoneForSeller(String nick, String phone) {
                return null;
            }

            @Override
            public List<UserResp> listUser(List<Long> userIds) {
                return null;
            }

            @Override
            public String saveAddress(UserAddressReq req) {
                return null;
            }
        };
    }
}
