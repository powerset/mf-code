package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.alipay.AppPayReq;
import mf.code.alipay.RequestRefundReq;
import mf.code.alipay.ResponseRefundDTO;
import mf.code.alipay.ResponseTradeAppPayDTO;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.douyin.DouyinPayReq;
import mf.code.order.api.feignclient.CommAppService;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * mf.code.api.feignclient.fallback
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:42
 */
@Component
public class CommAppFeignFallbackFactory implements FallbackFactory<CommAppService> {

    @Override
    public CommAppService create(Throwable cause) {
        return new CommAppService() {
            @Override
            public ResponseTradeAppPayDTO appAlipay(AppPayReq req) {
                return null;
            }

            @Override
            public Map douyinPay(DouyinPayReq req) {
                return null;
            }

            @Override
            public Map douyinPayScene(int payScene) {
                return null;
            }

            @Override
            public ResponseRefundDTO alipayOrderRefund(RequestRefundReq requestRefundReq) {
                return null;
            }
        };
    }
}
