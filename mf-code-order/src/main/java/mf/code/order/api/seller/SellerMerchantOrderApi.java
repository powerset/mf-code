package mf.code.order.api.seller;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.service.MerchantOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.order.api.platform
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月06日 17:18
 */
@RestController
@RequestMapping("/api/order/seller/v5")
@Slf4j
public class SellerMerchantOrderApi {
    @Autowired
    private MerchantOrderService merchantOrderService;

    /***
     * 财务管理-商家余额
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping("/merchantOrder/getBalance")
    public SimpleResponse getBalance(@RequestParam(name = "merchantId") Long merchantId,
                                     @RequestParam(name = "shopId") Long shopId) {
        return this.merchantOrderService.getBalance(merchantId, shopId);
    }
}
