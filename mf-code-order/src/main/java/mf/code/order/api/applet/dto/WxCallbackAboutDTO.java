package mf.code.order.api.applet.dto;

import com.alibaba.fastjson.JSONObject;
import mf.code.common.utils.IpUtil;
import mf.code.merchant.constants.*;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderPendPayTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.order.api.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 20:07
 */
public class WxCallbackAboutDTO {

    public static UserAddressReq addAddressDTO(String province, String city, String street, String detail, Long userId, String nick, String phone) {
        UserAddressReq req = new UserAddressReq();
        req.setCountry("中国");
        req.setProvince(province);
        req.setCity(city);
        req.setStreet(street);
        req.setDetail(detail);
        req.setUserId(userId);
        req.setNick(nick);
        req.setPhone(phone);
        return req;
    }

    //用户购买的交易明细
    public static UpayWxOrderReqDTO addPayUpayWxOrderReqDTO(Order order, String appId) {
        UpayWxOrderReqDTO upayWxOrderReqDTO = new UpayWxOrderReqDTO();
        BeanUtils.copyProperties(order, upayWxOrderReqDTO);
        upayWxOrderReqDTO.setPayType(UpayWxOrderPendPayTypeEnum.WEIXIN.getCode());
        if (order.getPayChannel() == OrderPayChannelEnum.ALIPAY.getCode()) {
            upayWxOrderReqDTO.setPayType(UpayWxOrderPendPayTypeEnum.ALIPAY.getCode());
        }
        upayWxOrderReqDTO.setType(UpayWxOrderTypeEnum.USERPAY.getCode());
        upayWxOrderReqDTO.setBizType(UpayWxOrderBizTypeEnum.SHOPPING.getCode());
        upayWxOrderReqDTO.setBizValue(order.getId());
        upayWxOrderReqDTO.setAppletAppId(appId);
        upayWxOrderReqDTO.setJsapiScene(2);
        upayWxOrderReqDTO.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
        upayWxOrderReqDTO.setTotalFee(order.getFee().toString());
        upayWxOrderReqDTO.setTradeType(1);
        return upayWxOrderReqDTO;
    }

    //用户退款的交易明细
    public static UpayWxOrderReqDTO addRefundUpayWxOrderReqDTO(Order refundOrder, String appId) {
        UpayWxOrderReqDTO upayWxOrderReqDTO = new UpayWxOrderReqDTO();
        BeanUtils.copyProperties(refundOrder, upayWxOrderReqDTO);
        upayWxOrderReqDTO.setPayType(UpayWxOrderPendPayTypeEnum.WEIXIN.getCode());
        if (refundOrder.getPayChannel() == OrderPayChannelEnum.ALIPAY.getCode()) {
            upayWxOrderReqDTO.setPayType(UpayWxOrderPendPayTypeEnum.ALIPAY.getCode());
        }
        upayWxOrderReqDTO.setType(UpayWxOrderTypeEnum.PALTFORMREFUND.getCode());
        upayWxOrderReqDTO.setBizType(UpayWxOrderBizTypeEnum.SHOPPING.getCode());
        upayWxOrderReqDTO.setBizValue(refundOrder.getId());
        upayWxOrderReqDTO.setAppletAppId(appId);
        upayWxOrderReqDTO.setJsapiScene(2);
        upayWxOrderReqDTO.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
        upayWxOrderReqDTO.setTotalFee(refundOrder.getFee().toString());
        upayWxOrderReqDTO.setTradeType(1);
        return upayWxOrderReqDTO;
    }

    //merchantId + shopId + userId + goodsId + skuId + number + bizType + payType + discountType + discountId;
    public static GoodsOrderReq addCreateOrderRedisKey(Order goodsOrder) {
        GoodsOrderReq req = new GoodsOrderReq();
        req.setMerchantId(goodsOrder.getMerchantId());
        req.setShopId(goodsOrder.getShopId());
        req.setUserId(goodsOrder.getUserId());
        req.setGoodsId(goodsOrder.getGoodsId());
        req.setSkuId(goodsOrder.getSkuId());
        req.setNumber(goodsOrder.getNumber().toString());
        req.setBizType(goodsOrder.getBizType());
        req.setPayType(goodsOrder.getPayChannel());
        req.setDiscountType(0);
        if (goodsOrder.getDiscountType() != null) {
            req.setDiscountType(goodsOrder.getDiscountType());
        }
        req.setDiscountId(0L);
        if (goodsOrder.getDiscountId() != null) {
            req.setDiscountId(goodsOrder.getDiscountId());
        }
        return req;
    }

    //购买商品存储merchantorder
    public static MerchantOrder addMerchantOrderByProduct(Order goodsOrder, String merchantOrderNo, String pubAppId, Long merchantId, Long shopId, BigDecimal goodsFee) {
        MerchantOrder merchantOrder = new MerchantOrder();
        merchantOrder.setMerchantId(merchantId);
        merchantOrder.setShopId(shopId);
        merchantOrder.setShopName("");
        merchantOrder.setCustomerJson("");
        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode());
        merchantOrder.setPayType(goodsOrder.getPayChannel());
        merchantOrder.setOrderNo(merchantOrderNo);
        merchantOrder.setOrderName("购买商品");
        merchantOrder.setPubAppId(pubAppId);
        merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
        merchantOrder.setTotalFee(goodsFee);
        merchantOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
        merchantOrder.setIpAddress(IpUtil.getIpAddress(null));
        merchantOrder.setTradeId("");
        merchantOrder.setRemark("购买商品");
        merchantOrder.setNotifyTime(new Date());
        merchantOrder.setPaymentTime(new Date());
        merchantOrder.setCtime(new Date());
        merchantOrder.setUtime(new Date());
        merchantOrder.setReqJson("");
        merchantOrder.setBizType(BizTypeEnum.SHOPPING.getCode());
        if(goodsOrder.getSource() == OrderSourceEnum.DOUYIN_SHOP.getCode()){
            merchantOrder.setBizType(BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode());
        }
        merchantOrder.setBizValue(goodsOrder.getId());
        merchantOrder.setRefundOrderId(0L);
        merchantOrder.setMfTradeType(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode());
        merchantOrder.setPayOpenId("");
        return merchantOrder;
    }

    //退款商品存储merchantorder
    public static MerchantOrder addRefundMerchantOrderByProduct(Order goodsOrder, String merchantOrderNo, String pubAppId, Long merchantId, Long shopId, BigDecimal goodsFee) {
        MerchantOrder merchantOrder = new MerchantOrder();
        merchantOrder.setMerchantId(merchantId);
        merchantOrder.setShopId(shopId);
        merchantOrder.setShopName("");
        merchantOrder.setCustomerJson("");
        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
        merchantOrder.setPayType(goodsOrder.getPayChannel());
        merchantOrder.setOrderNo(merchantOrderNo);
        merchantOrder.setOrderName("退款-购买商品");
        merchantOrder.setPubAppId(pubAppId);
        merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
        merchantOrder.setTotalFee(goodsFee);
        merchantOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
        merchantOrder.setIpAddress(IpUtil.getIpAddress(null));
        merchantOrder.setTradeId("");
        merchantOrder.setRemark("退款-购买商品");
        merchantOrder.setNotifyTime(new Date());
        merchantOrder.setPaymentTime(new Date());
        merchantOrder.setCtime(new Date());
        merchantOrder.setUtime(new Date());
        merchantOrder.setReqJson("");
        merchantOrder.setBizType(BizTypeEnum.SHOPPING.getCode());
        if(goodsOrder.getSource() == OrderSourceEnum.DOUYIN_SHOP.getCode()){
            merchantOrder.setBizType(BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode());
        }
        merchantOrder.setBizValue(goodsOrder.getId());
        merchantOrder.setRefundOrderId(0L);
        if (goodsOrder.getType() == mf.code.order.feignapi.constant.OrderTypeEnum.REFUNDMONEY.getCode()) {
            merchantOrder.setMfTradeType(MfTradeTypeEnum.GOODS_SALE_REFUND.getCode());
        } else if (goodsOrder.getType() == mf.code.order.feignapi.constant.OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()) {
            merchantOrder.setMfTradeType(MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode());
        }
        merchantOrder.setPayOpenId("");
        return merchantOrder;
    }

    //线下提现-运营手输入库操作
    public static MerchantOrder addMerchantOrderByOfflineCash(Long merchantId, Long shopId, String shopName, String orderName,
                                                              String orderNo, String fee, String platformId, String pubAppId, List<String> pics) {
        MerchantOrder merchantOrder = new MerchantOrder();
        merchantOrder.setMerchantId(merchantId);
        merchantOrder.setShopId(shopId);
        merchantOrder.setShopName(shopName);
        merchantOrder.setCustomerJson("");
        merchantOrder.setType(OrderTypeEnum.USERCASH.getCode());
        merchantOrder.setPayType(OrderPayTypeEnum.CASH.getCode());
        merchantOrder.setOrderNo(orderNo);
        merchantOrder.setOrderName(orderName);
        merchantOrder.setPubAppId(pubAppId);
        merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
        merchantOrder.setTotalFee(new BigDecimal(fee));
        merchantOrder.setTradeType(WxTradeTypeEnum.MKTTRANSFER.getCode());
        merchantOrder.setIpAddress(IpUtil.getIpAddress(null));
        merchantOrder.setTradeId("");
        merchantOrder.setRemark(platformId);
        merchantOrder.setNotifyTime(new Date());
        merchantOrder.setPaymentTime(new Date());
        merchantOrder.setCtime(new Date());
        merchantOrder.setUtime(new Date());
        merchantOrder.setReqJson(JSONObject.toJSONString(pics));
        merchantOrder.setBizType(0);
        merchantOrder.setBizValue(0L);
        merchantOrder.setRefundOrderId(0L);
        merchantOrder.setMfTradeType(MfTradeTypeEnum.CASH.getCode());
        merchantOrder.setPayOpenId("");
        return merchantOrder;
    }

    //购买商品待结算->已结算
    public static MerchantOrder updateMerchantOrderByProduct(MerchantOrder merchantOrder) {
        merchantOrder.setMfTradeType(MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode());
        merchantOrder.setUtime(new Date());
        return merchantOrder;
    }

    //购买商品之后,用户退款成功(采用不更新方式,添加记录方式,所以此刻"英雄无用武之地")
    public static MerchantOrder updateUserRefundMerchantOrderByProduct(MerchantOrder merchantOrder) {
        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
        merchantOrder.setUtime(new Date());
        return merchantOrder;
    }
}
