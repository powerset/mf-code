package mf.code.order.api.feignclient;

import mf.code.alipay.AppPayReq;
import mf.code.alipay.RequestRefundReq;
import mf.code.alipay.ResponseRefundDTO;
import mf.code.alipay.ResponseTradeAppPayDTO;
import mf.code.douyin.DouyinPayReq;
import mf.code.order.api.feignclient.fallback.CommAppFeignFallbackFactory;
import mf.code.order.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.api.feignclient
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:42
 */
@FeignClient(name = "mf-code-comm", fallbackFactory = CommAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface CommAppService {

    /***
     * app alipay支付
     *
     * @param req
     * @return
     */
    @PostMapping("/feignapi/comm/appAlipay")
    ResponseTradeAppPayDTO appAlipay(@RequestBody AppPayReq req);

    /***
     * 抖音下单获取订单号
     *
     * @param req
     * @return
     */
    @PostMapping("/feignapi/comm/douyinPay")
    Map douyinPay(@RequestBody DouyinPayReq req);

    @GetMapping("/feignapi/comm/douyinPayScene")
    Map douyinPayScene(@RequestParam("payScene") int payScene);

    /***
     * 申请退款
     *
     * @param requestRefundReq
     * @return
     */
    @PostMapping("/feignapi/comm/alipayOrderRefund")
    ResponseRefundDTO alipayOrderRefund(@RequestBody RequestRefundReq requestRefundReq);
}
