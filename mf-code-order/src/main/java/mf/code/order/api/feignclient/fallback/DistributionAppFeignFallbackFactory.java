package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.RebatePolicyDTO;
import mf.code.order.api.feignclient.DistributionAppService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class DistributionAppFeignFallbackFactory implements FallbackFactory<DistributionAppService> {
    @Override
    public DistributionAppService create(Throwable throwable) {
        return new DistributionAppService() {
            @Override
            public Map<Long, BigDecimal> queryOrderHistoryRebate(List<Long> orderIds) {
                return new HashMap<>();
            }

            @Override
            public String queryOrderDistributionProperty(ProductDistributionDTO product) {
                return null;
            }

            @Override
            public String getRebateCommission(ProductDistributionDTO productDTO) {
                return null;
            }

            @Override
            public String getMerchantOrderIncome(Long userId, Long shopId, Long orderId) {
                return null;
            }

            @Override
            public SimpleResponse distributionCommission(Long userId, Long shopId, Long orderId) {
                return null;
            }

            @Override
            public Map<Long, String> queryUserRebateByProduct(List<String> distributionDTOLists) {
                return null;
            }
        };
    }
}
