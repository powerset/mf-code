package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.dto.MerchantOrderReq;
import mf.code.order.api.feignclient.ShopAppService;
import mf.code.shop.dto.ShopListResultDTO;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class ShopAppFeignFallbackFactory implements FallbackFactory<ShopAppService> {
    @Override
    public ShopAppService create(Throwable throwable) {
        return new ShopAppService() {
            @Override
            public Map<String, Object> queryShopInfoByLogin(String shopId) {
                return null;
            }

            @Override
            public Long getMerchantIdByShopId(Long shopId) {
                return null;
            }

            @Override
            public List<Long> selectAllIds() {
                return null;
            }

            @Override
            public Integer updateMerchantBalance(MerchantOrderReq merchantOrderReq) {
                return null;
            }

            /**
             * 商户查询 返回相关店铺信息
             *
             * @param shopId
             * @return
             */
            @Override
            public Map<String, Object> queryShopInfoForSeller(String shopId) {
                return new HashMap<>();
            }

            /***
             * 根据商户id查询旗下的所有未删除的店铺
             * @param merchantId
             * @return 店铺集合
             */
            @Override
            public ShopListResultDTO getMerchantIdByShopList(Long merchantId) {
                return null;
            }

            @Override
            public MerchantDTO queryMerchantById(Long merchantId) {
                return null;
            }
        };
    }
}
