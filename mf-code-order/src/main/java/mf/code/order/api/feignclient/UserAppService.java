package mf.code.order.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.api.feignclient.fallback.UserAppFeignFallbackFactory;
import mf.code.order.common.config.feign.FeignLogConfiguration;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * mf.code.user.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 19:38
 */
@FeignClient(name = "mf-code-user", fallbackFactory = UserAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class
)
public interface UserAppService {
    @PostMapping("/feignapi/user/applet/v5/addUser")
    Long addUser(@RequestBody UserResp userResp);
    /**
     * 订单取消，对商品返利及分佣的记录 设置失效
     */
    @PostMapping("/feignapi/user/applet/v5/cancelDistributionCommission")
    SimpleResponse cancelDistributionCommission(@RequestParam("orderNo") String orderNo,
                                                @RequestParam("shopId") String shopId);

    /***
     * 查询收货地址
     * @param addressId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryAdddress")
    UserAddressReq queryAdddress(@RequestParam("addressId") Long addressId);

    /***
     * 获取用户信息
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/user/applet/v5/queryUser")
    UserResp queryUser(@RequestParam("userId") Long userId);


    @PostMapping("/feignapi/user/applet/v5/saveUpayWxOrder")
    int saveUpayWxOrder(@RequestBody UpayWxOrderReqDTO reqDTO);

    /**
     * 获取用户地址信息和用户信息
     *
     * @param userId
     * @param addressId
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryUserAddress")
    SimpleResponse queryUserAndAddressForSeller(@RequestParam(value = "userId") Long userId,
                                                @RequestParam(value = "addressId") Long addressId);

    /**
     * 获取用户地址信息map
     *
     * @param addressIdList
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryAddressMap")
    SimpleResponse queryAddressMapForSeller(@RequestParam(value = "addressIdList") List<Long> addressIdList);

    /**
     * 通过买家名模糊查询用户地址Id列表
     *
     * @param nick
     * @return
     */
    @GetMapping("/feignapi/user/seller/v5/queryAddressIdByAddressNickOrPhoneForSeller")
    SimpleResponse queryAddressIdByAddressNickOrPhoneForSeller(@RequestParam(value = "nick") String nick,
                                                               @RequestParam(value = "phone") String phone);

    /**
     * 批量 获取 用户信息
     * @param userIds
     * @return
     */
    @GetMapping(value = "/feignapi/user/applet/v5/listUser")
    List<UserResp> listUser(@RequestParam("userIds") List<Long> userIds);

    @PostMapping("/feignapi/user/applet/v5/saveAddress")
    String saveAddress(@RequestBody UserAddressReq req);
}
