package mf.code.order.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.dto.ApplyRefundReqDTO;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.dto.GoodsOrderUpdateReq;
import mf.code.order.dto.RefundLogisticsReqDTO;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.service.OrderAlipayService;
import mf.code.order.service.OrderService;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * mf.code.order.api
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 18:45
 */
@RestController
@RequestMapping("/api/order/applet/v5")
@Slf4j
public class GoodsOrderApi {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderAlipayService orderAlipayService;
    @Autowired
    private UserAppService userAppService;

    /***
     * 测试流程抖音创建订单
     *
     */
    @PostMapping("/testCreateGoodsOrder")
    public SimpleResponse douyinCreateGoodsOrder(@RequestBody Map req, HttpServletRequest request) {
        UserResp user = this.userAppService.queryUser(NumberUtils.toLong(req.get("userId").toString()));
        if (user == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "获取用户异常");
        }
        Order order = new Order();
        String outTradeNo = DateFormatUtils.format(new Date(), "yyMMddHHmmss") + 1 + "999" + RandomStrUtil.randomNumberStr(6);
        order.setMerchantId(150L);
        order.setShopId(114L);
        order.setType(OrderTypeEnum.PAY.getCode());
        order.setPayChannel(OrderPayChannelEnum.ALIPAY.getCode());
        order.setUserId(35L);
        order.setFee(new BigDecimal("0.02"));
        order.setNumber(1);
        order.setOrderNo(outTradeNo);
        order.setAddressId(53L);
        order.setOrderName("测试");
        order.setGoodsId(1120L);
        order.setGoodsShopId(1120L);
        order.setSkuId(2037L);
        String ipAddress = IpUtil.getIpAddress(request);
        order.setIpAddress(ipAddress);
        GoodProductDTO goodProductDTO = new GoodProductDTO();
        goodProductDTO.setTitle("测试");
        goodProductDTO.setGoodsId(353L);
        goodProductDTO.setParentId(353L);
        GoodsOrderReq params = new GoodsOrderReq();
        return orderAlipayService.douyinAlipayAppType(order, user, goodProductDTO, params);
    }

    /***
     * 创建订单
     * @param goodsOrder
     * @return
     */
    @PostMapping("/createGoodsOrder")
    public SimpleResponse createGoodsOrder(@RequestBody GoodsOrderReq goodsOrder, HttpServletRequest request) {
        //step1:入参安全性验证
        if (!goodsOrder.checkValid()) {
            log.error("<<<<<<<<创建订单入参异常:{}", goodsOrder);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入有效参数");
        }
        String ipAddress = IpUtil.getIpAddress(request);
        return this.orderService.createGoodsOrder(goodsOrder, ipAddress);
    }

    /***
     * 查询订单状态
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @GetMapping("/queryOrderStatus")
    public SimpleResponse queryOrderStatus(@RequestParam("merchantId") Long merchantId,
                                           @RequestParam("shopId") Long shopId,
                                           @RequestParam("userId") Long userId,
                                           @RequestParam("orderId") Long orderId) {
        return this.orderService.queryOrderStatus(merchantId, shopId, userId, orderId);
    }

    /***
     * 支付成功后的展现
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @GetMapping("/queryPaySuccess")
    public SimpleResponse queryPaySuccess(@RequestParam("merchantId") Long merchantId,
                                          @RequestParam("shopId") Long shopId,
                                          @RequestParam("userId") Long userId,
                                          @RequestParam("orderId") Long orderId) {
        return this.orderService.queryPaySuccess(userId, orderId);
    }

    /***
     * 售后申请页
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @GetMapping("/applyRefund")
    public SimpleResponse applyRefund(@RequestParam("merchantId") Long merchantId,
                                      @RequestParam("shopId") Long shopId,
                                      @RequestParam("userId") Long userId,
                                      @RequestParam("orderId") Long orderId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0 || orderId == null || orderId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入有效参数...");
        }
        return this.orderService.applyRefund(merchantId, shopId, userId, orderId);
    }

    /***
     * 售后申请提交
     * @param req
     * @return
     */
    @PostMapping("/applyRefund")
    public SimpleResponse applyRefund(@RequestBody ApplyRefundReqDTO req) {
        if (!req.checkValid()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请填写完必填的售后信息...");
        }
        return this.orderService.applyRefund(req);
    }

    /***
     * 订单创建了,未支付,去付款
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @param request
     * @return
     */
    @GetMapping("/payGoodsOrder")
    public SimpleResponse payGoodsOrder(@RequestParam("merchantId") Long merchantId,
                                        @RequestParam("shopId") Long shopId,
                                        @RequestParam("userId") Long userId,
                                        @RequestParam(value = "addressId", required = false) Long addressId,
                                        @RequestParam(value = "buyerMsg", required = false) String buyerMsg,
                                        @RequestParam("orderId") Long orderId, HttpServletRequest request) {
        return this.orderService.payGoodsOrder(merchantId, shopId, userId, addressId, buyerMsg, orderId, request);
    }

    /***
     * 更新订单状态
     * @param goodsOrderUpdateReq
     * @return
     */
    @PostMapping("/updateGoodsOrder")
    public SimpleResponse updateGoodsOrder(@RequestBody GoodsOrderUpdateReq goodsOrderUpdateReq) {
        return this.orderService.updateGoodsOrderByStatus(goodsOrderUpdateReq);
    }

    /***
     * 删除订单
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @GetMapping("/delGoodsOrder")
    public SimpleResponse delGoodsOrder(@RequestParam("merchantId") Long merchantId,
                                        @RequestParam("shopId") Long shopId,
                                        @RequestParam("userId") Long userId,
                                        @RequestParam("orderId") Long orderId) {
        return this.orderService.delGoodsOrder(merchantId, shopId, userId, orderId);
    }

    /***
     * 获取订单详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @GetMapping("/queryGoodsOrderDetail")
    public SimpleResponse queryGoodsOrderDetail(@RequestParam("merchantId") Long merchantId,
                                                @RequestParam("shopId") Long shopId,
                                                @RequestParam("userId") Long userId,
                                                @RequestParam("orderId") Long orderId) {
        return this.orderService.queryGoodsOrderDetail(merchantId, shopId, userId, orderId);
    }

    /***
     * 获取售后订单详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId 退款订单编号
     * @return
     */
    @GetMapping("/queryRefundGoodsOrderDetail")
    public SimpleResponse queryRefundGoodsOrderDetail(@RequestParam("merchantId") Long merchantId,
                                                      @RequestParam("shopId") Long shopId,
                                                      @RequestParam("userId") Long userId,
                                                      @RequestParam("orderId") Long orderId) {
        return this.orderService.queryRefundGoodsOrderDetail(merchantId, shopId, userId, orderId);
    }


    /***
     * 获取订单分页信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 0：全部 1待付款 2 待发货 3待收货 4 交易成功 5 退款/售后
     * @param limit
     * @param offset
     * @return
     */
    @GetMapping("/queryOrderByPage")
    public SimpleResponse getGoodsOrderByPage(@RequestParam("merchantId") Long merchantId,
                                              @RequestParam("shopId") Long shopId,
                                              @RequestParam(name = "userId") Long userId,
                                              @RequestParam(name = "type", defaultValue = "1", required = false) int type,
                                              @RequestParam(name = "limit", required = false, defaultValue = "10") int limit,
                                              @RequestParam(name = "offset", required = false, defaultValue = "0") int offset) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入有效参数...");
        }
        return this.orderService.getGoodsOrderByPage(merchantId, shopId, userId, type, limit, offset);
    }

    /***
     * 退货填写物流信息
     * @param req
     * @return
     */
    @PostMapping("/addRefundLogisticsInfo")
    public SimpleResponse addRefundLogisticsInfo(@RequestBody RefundLogisticsReqDTO req) {
        if (!req.checkValid()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入有效参数...");
        }
        return this.orderService.addRefundLogisticsInfo(req);
    }
}
