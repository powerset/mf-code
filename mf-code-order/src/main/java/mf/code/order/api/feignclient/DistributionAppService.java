package mf.code.order.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.RebatePolicyDTO;
import mf.code.order.api.feignclient.fallback.DistributionAppFeignFallbackFactory;
import mf.code.order.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-08 19:11
 */
@FeignClient(value = "mf-code-distribution", fallbackFactory = DistributionAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface DistributionAppService {

    @GetMapping("/feignapi/distribution/applet/v5/queryOrderHistoryRebate")
    Map<Long, BigDecimal> queryOrderHistoryRebate(@RequestParam("orderIds") List<Long> orderIds);

    /**
     * 获取订单分销属性
     */
    @PostMapping("/feignapi/distribution/applet/v5/queryOrderDistributionProperty")
    String queryOrderDistributionProperty(@RequestBody ProductDistributionDTO product);

    /**
     * 获取 用户返利金额
     *
     * @param productDTO
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v5/getRebateCommission")
    String getRebateCommission(@RequestBody ProductDistributionDTO productDTO);

    /**
     * 商品售出，获取 商户所得利润
     *
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/getMerchantOrderIncome")
    String getMerchantOrderIncome(@RequestParam("userId") Long userId,
                                  @RequestParam("shopId") Long shopId,
                                  @RequestParam("orderId") Long orderId);

    /**
     * 支付成功 处理用户返利和分佣
     *
     * @param userId
     * @param shopId
     * @param orderId
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/distributionCommission")
    SimpleResponse distributionCommission(@RequestParam("userId") Long userId,
                                          @RequestParam("shopId") Long shopId,
                                          @RequestParam("orderId") Long orderId);

    /**
     * 获取用户商品返利
     *
     * @param distributionDTOLists
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v5/queryUserRebateByProduct")
    Map<Long, String> queryUserRebateByProduct(@RequestParam("distributionDTOLists") List<String> distributionDTOLists);
}
