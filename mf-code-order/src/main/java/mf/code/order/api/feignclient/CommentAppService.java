package mf.code.order.api.feignclient;

import mf.code.comment.dto.OrderCommentDTO;
import mf.code.order.api.feignclient.fallback.CommentAppFeignFallbackFactory;
import mf.code.order.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(value = "mf-code-comment", fallbackFactory = CommentAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface CommentAppService {
    /**
     * 根据shopId和订单id分页查询评论信息
     * @param orderIdList
     * @param shopId
     * @param statusList -1初始值（未评论）,0买家已评论，1审核通过，2不通过
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/feignapi/comment/seller/getCommentListByShopAndOrderIds")
    List<OrderCommentDTO> getCommentListByShopAndOrderIds(@RequestParam(value="orderIdList",required = false)List<Long> orderIdList, @RequestParam(value="statusList",required = false)List<Integer> statusList, @RequestParam(value="shopId",required = false)Long shopId, @RequestParam("page")int page, @RequestParam("size")int size);



    /**
     * 获取评论信息总数
     * @param orderIdList
     * @param statusList
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/comment/seller/getCountOfCommentListNum")
    Long getCountOfCommentListNum(@RequestParam(value="orderIdList",required = false)List<Long> orderIdList,@RequestParam(value="statusList",required = false)List<Integer> statusList,
                                      @RequestParam(value="shopId",required = false)Long shopId);

    /**
     * 批量 获取 订单评价的状态
     *
     * @param orderIds
     * @return 0 未评价， 1已评价
     */
    @GetMapping("/feignapi/comment/applet/listOrderCommentStatus")
    Map<Long, String> listOrderCommentStatus(@RequestParam("orderIds") List<Long> orderIds);

}
