package mf.code.order.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.comment.dto.OrderCommentDTO;
import mf.code.order.api.feignclient.CommentAppService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CommentAppFeignFallbackFactory implements FallbackFactory<CommentAppService> {
    @Override
    public CommentAppService create(Throwable throwable) {
        return new CommentAppService(){

            @Override
            public List<OrderCommentDTO> getCommentListByShopAndOrderIds(List<Long> orderIdList, List<Integer> commentStatus, Long shopId, int page, int size) {
                return null;
            }

            @Override
            public Long getCountOfCommentListNum(List<Long> orderIdList, List<Integer> statusList, Long shopId) {
                return null;
            }

            @Override
            public Map<Long, String> listOrderCommentStatus(List<Long> orderIds) {
                return null;
            }
        };
    }
}
