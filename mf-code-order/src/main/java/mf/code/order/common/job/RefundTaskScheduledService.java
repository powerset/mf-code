package mf.code.order.common.job;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.common.caller.wxmp.WxmpProperty;
import mf.code.order.common.caller.wxpay.WeixinPayConstants;
import mf.code.order.common.caller.wxpay.WeixinPayService;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.service.OrderService;
import mf.code.order.service.OrderStatusService;
import mf.code.order.service.templateMsg.OrderTemplateMessageService;
import mf.code.order.service.templateMsg.TemplateMessageData;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.common.job
 * Description: 查询订单状态-并得知是否退款 定时器
 *
 * @author: 夜辰
 * @date: 2018-11-13 11:26
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class RefundTaskScheduledService {
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderTemplateMessageService orderTemplateMessageService;
    @Autowired
    private WxmpProperty wxmpProperty;

    @Value("${taskExecutor.jobMode}")
    private Integer jobMode;

    private final int MAX_LIMIT = 30;

    /***
     * 查询扫出wx后台订单
     *
     * <return_msg><![CDATA]></return_msg>
     * <appid><![CDATA[wxb7784511b3f1d7cd]]></appid>
     * <mch_id><![CDATA[1515160051]]></mch_id>
     * <nonce_str><![CDATA[CoYkMR5vwJ1IR7Wa]]></nonce_str>
     * <sign><![CDATA[1B2E96985ADA57567E6F6D6B3896DFE2]]></sign>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <openid><![CDATA[ofPkK0kw8alJfQkF6--_rn99qN8Y]]></openid>
     * <is_subscribe><![CDATA[N]]></is_subscribe>
     * <trade_type><![CDATA[NATIVE]]></trade_type>
     * <bank_type><![CDATA[CFT]]></bank_type>
     * <total_fee>1</total_fee>
     * <fee_type><![CDATA[CNY]]></fee_type>
     * <transaction_id><![CDATA[4200000204201812194820907317]]></transaction_id>
     * <out_trade_no><![CDATA[V26yzCzK1545225200356]]></out_trade_no>
     * <attach><![CDATA[%E6%B5%8B%E8%AF%951]]></attach>
     * <time_end><![CDATA[20181219211529]]></time_end>
     * <trade_state><![CDATA[REFUND]]></trade_state>
     * <cash_fee>1</cash_fee>
     * <trade_state_desc><![CDATA[订单发生过退款，退款详情请查询退款单]]></trade_state_desc>
     * </xml>
     */

    /**
     * 扫出 订单状态-并得知是否退款
     */
    @Scheduled(fixedDelay = 60000)
    public void appletRefundScheduled() {
        //根据配置内的jobMode得知是否跑定时器
        if (jobMode == 0) {
            return;
        }
        int pageNum = 1;
        for (; ; pageNum++) {
            //对于小于两分钟的，不做处理
            Date date = DateUtils.addMinutes(new Date(), -2);
            //每次只查询30个
            QueryWrapper<Order> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                    .in(Order::getPayChannel, Arrays.asList(OrderPayChannelEnum.WX.getCode(), OrderPayChannelEnum.ALIPAY.getCode()))
                    .eq(Order::getStatus, OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode())
                    .lt(Order::getCtime, date)
            ;
            Page<Order> page = new Page<>(pageNum, MAX_LIMIT);
            page.setAsc("ctime");
            IPage<Order> orderIPage = goodsOrderRepository.queryPage(page, wrapper);
            List<Order> orderList = orderIPage.getRecords();
            if (CollectionUtils.isEmpty(orderList)) {
                break;
            }
            for (Order order : orderList) {
                //订单redis是否还存在
                GoodsOrderReq req = WxCallbackAboutDTO.addCreateOrderRedisKey(order);
                String redisKey = RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey();
                String str = this.stringRedisTemplate.opsForValue().get(redisKey);
                boolean orderRedisKey = false;
                if (StringUtils.isNotBlank(str)) {
                    orderRedisKey = true;
                }

                Map<String, Object> respQueryOrderMap = this.selectWxOrderInfo(order.getOrderNo(), WeixinPayConstants.PAYSCENE_APPLET_APPID);
                if (CollectionUtils.isEmpty(respQueryOrderMap)) {
                    continue;
                }
                //判断该订单是否是wx
                if (order.getSource() == OrderSourceEnum.JKMF.getCode()) {
                    boolean wxOrderInfoStatusSuccess = this.getWxOrderStatusInfo(respQueryOrderMap);
                    if (wxOrderInfoStatusSuccess) {
                        order.setPaymentTime(DateUtil.parseDate(respQueryOrderMap.get("time_end").toString(), null, "yyyyMMddHHmmss"));
                        order.setTradeId(respQueryOrderMap.get("transaction_id").toString());

                        int i1 = this.orderStatusService.buyerConfirmReceipt(order);
                        if (i1 == 0) {
                            log.error("<<<<<<<待付款定时器-更新订单-异常001：orderId:{}", order.getId());
                        }

                        //存在rediskey 删除
                        if (orderRedisKey) {
                            this.stringRedisTemplate.delete(redisKey);
                        }

                        //退款流程
                        SimpleResponse simpleResponse = this.orderService.refundOrder(order.getId(), order.getFee(), "商品支付异常，退款");
                        if (simpleResponse == null || simpleResponse.error()) {
                            log.error("<<<<<<<待付款定时器-退款-异常003：simpleResponse:{}", JSONObject.toJSONString(simpleResponse));
                        }

                        //退库存
                        SimpleResponse simpleResponse1 = this.goodsAppService.giveBackStock(order.getSkuId(), order.getNumber(), order.getId());
                        if (simpleResponse1.error()) {
                            log.error("<<<<<<<待付款定时器-退库存-异常002：orderId:{}", order.getId());
                        }
                        orderService.backUserCoupon(order);
                        continue;
                    }
                }else {
                    //TODO: alipay查询数据状态
                }



                //不存在了,并且订单创建时间超过15分钟(冗余判断),做超时处理
                long time = System.currentTimeMillis() - order.getCtime().getTime();
                if (!orderRedisKey && time > 15 * 60 * 1000) {
                    order.setRespJson(JSONObject.toJSONString(respQueryOrderMap));
                    int i1 = orderStatusService.buyerOrderTimeout(order);
                    //退库存
                    SimpleResponse simpleResponse = this.goodsAppService.giveBackStock(order.getSkuId(), order.getNumber(), order.getId());
                    if (simpleResponse == null || simpleResponse.error()) {
                        log.error("<<<<<<<待付款定时器-退库存-异常005：orderId:{}", order.getId());
                    }
                    orderService.backUserCoupon(order);

                    //判断该订单是否是wx
                    if (order.getSource() == OrderSourceEnum.JKMF.getCode()) {
                        //超时消息推送
                        try {
                            GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(order.getMerchantId(), order.getShopId(), order.getGoodsId(), order.getSkuId(), order.getUserId());
                            if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                                log.error("<<<<<<<<超时消息推送异常-商品信息未获取到：orderId:{}", order.getId());
                                continue;
                            }
                            Map<String, Object> data = TemplateMessageData.orderTimeoutCancel(order.getMerchantId(), order.getShopId(), order.getId(), order.getOrderNo(), goodProductSkuDTO.getTitle(), order.getFee());
                            this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getOrdercancelMsgTmpId(), order.getUserId(), data);
                        } catch (Exception e) {
                            log.error("<<<<<<<<超时消息推送异常：{}", e);
                            log.error("<<<<<<<<超时消息推送异常：orderId:{}", order.getId());
                        }
                    }
                }

                //判断该订单是否是wx
                if (order.getSource() == OrderSourceEnum.JKMF.getCode()) {
                    //订单已经存在5分钟,推送消息
                    String tempalateMsgRedisKey = RedisKeyConstant.GOODSORDER_TEMPLATEMSG + order.getId();
                    String existTempalateMsgRedis = this.stringRedisTemplate.opsForValue().get(tempalateMsgRedisKey);
                    if (orderRedisKey && time > 5 * 60 * 1000 && StringUtils.isBlank(existTempalateMsgRedis)) {
                        try {
                            GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(order.getMerchantId(), order.getShopId(), order.getGoodsId(), order.getSkuId(), order.getUserId());
                            if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                                log.error("<<<<<<<<超时消息推送异常-商品信息未获取到：orderId:{}", order.getId());
                                continue;
                            }
                            Map<String, Object> data = TemplateMessageData.willPay(order.getMerchantId(), order.getShopId(), order.getId(), order.getOrderNo(), goodProductSkuDTO.getTitle(), order.getFee());
                            this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getOrderWillPayNoticeMsgTmpId(), order.getUserId(), data);
                            this.stringRedisTemplate.opsForValue().set(tempalateMsgRedisKey, "0", 15, TimeUnit.MINUTES);
                        } catch (Exception e) {
                            log.error("<<<<<<<<待付款5分钟后消息推送异常：{}", e);
                            log.error("<<<<<<<<待付款5分钟后消息推送异常：orderId:{}", order.getId());
                        }
                    }
                }
            }
        }
    }

    private Map<String, Object> selectWxOrderInfo(String orderNo, Integer payScene) {
        Map<String, Object> queryParams = this.weixinPayService.getQueryOrderParams(orderNo, payScene);
        Map<String, Object> respQueryOrderMap = this.weixinPayService.queryOrder(queryParams);
        log.info("<<<<<<<<<< 查询微信该订单信息:{}", respQueryOrderMap);
        return respQueryOrderMap;
    }

    private Boolean getWxOrderStatusInfo(Map<String, Object> respQueryOrderMap) {
        boolean returnCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("return_code") != null &&
                StringUtils.isNotBlank(respQueryOrderMap.get("return_code").toString()) &&
                "SUCCESS".equals(respQueryOrderMap.get("return_code"));
        boolean resultCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("result_code") != null &&
                StringUtils.isNotBlank(respQueryOrderMap.get("result_code").toString()) &&
                "SUCCESS".equals(respQueryOrderMap.get("result_code"));
        boolean tradeStateSuccess = respQueryOrderMap != null && respQueryOrderMap.get("trade_state") != null &&
                StringUtils.isNotBlank(respQueryOrderMap.get("trade_state").toString()) &&
                "SUCCESS".equals(respQueryOrderMap.get("trade_state"));
        /***
         * SUCCESS—支付成功
         * REFUND—转入退款
         * NOTPAY—未支付
         * CLOSED—已关闭
         * REVOKED—已撤销（刷卡支付）
         * USERPAYING--用户支付中
         * PAYERROR--支付失败(其他原因，如银行返回失败)
         */
        if (returnCodeSuccess && resultCodeSuccess && tradeStateSuccess) {
            return true;
        } else {
            return false;
        }
    }
}
