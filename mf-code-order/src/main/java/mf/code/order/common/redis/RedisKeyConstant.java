package mf.code.order.common.redis;

/**
 * redis键
 *
 * @author gel
 */
public class RedisKeyConstant {

    /**
     * 数据字典键
     * key--> common:dict:<type>:<key>
     * value--> 1
     */
    public static final String COMMON_DICT = "common:dict:";

    /***
     * 订单redis
     */
    public static final String GOODORDER_ORDER = "jkmf:order:create:";

    /***
     * 推送消息redis
     */
    public static final String GOODSORDER_TEMPLATEMSG = "jkmf:order:templateMsg:";//<orderId>

    /***
     * 订单回调去重
     */
    public static final String GOODORDER_FORBID_KEY = "jkmf:order:callback:forbid:";

    /***
     * 快递物流redis存储
     */
    public static final String LOGISTICS_REDIS_KEY = "jkmf:order:logistics:";//code_num

    /***
     * 假订单的生成key条件
     * 平台每产生5个订单，生成一个假订单
     * key：jkmf:order:successOrder
     * value: 自增
     * <pre>
     *     满5重置，下单时自增
     * </pre>
     */
    public static final String SUCCESS_ORDER_KEY = "jkmf:order:successOrder";

    /**
     * 含有分销属性商品销量排行榜
     */
    public static final String PRODUCT_SALESVOLUME_RANKING_LIST="jkmf:product:salesvolume:ranking:list";

    /**
     * 平台商品热销
     */
    public static final String RANKING_LIST_PLAT = "mf:statis:sv:rank:plat:";//<data>
    /**
     * 店铺商品热销
     */
    public static final String RANKING_LIST_SHOP = "mf:statis:sv:rank:shop:";//<shopID>:<date>
}
