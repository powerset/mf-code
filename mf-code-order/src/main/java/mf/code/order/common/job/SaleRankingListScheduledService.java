package mf.code.order.common.job;/**
 * create by qc on 2019/6/28 0028
 */

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.Order;
import mf.code.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 销量排行榜
 *
 * @author gbf
 * 2019/6/28 0028、20:27
 */

@Slf4j
@Component
@Profile(value = {"test2", "prod", "dev"})
public class SaleRankingListScheduledService {
    @Autowired
    private OrderService orderService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GoodsAppService goodsAppService;

    /**
     * zset步长
     */
    public static final int STEP_SIZE = 1;

    /**
     * 查询昨日支付订单,
     */
    @Scheduled(cron = "0 0 0 * * *")
    public void findPayOrderJob() {
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^商品热销订单排行任务 1.准备查询 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        List statList = new ArrayList();
        statList.add(OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        statList.add(OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        statList.add(OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
        statList.add(OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode());
        statList.add(OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode());

        Date yesterday = DateUtil.addDay(new Date(), -1);
        Date yesterdayBegin = DateUtil.getDateAfterZero(yesterday);
        Date yesterdayEnd = DateUtil.getDateBeforeZero(yesterday);

        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .between(Order::getUtime, yesterdayBegin, yesterdayEnd)
                .eq(Order::getDel, DelEnum.NO.getCode())
                .in(Order::getStatus, statList)
                .ne(Order::getGoodsId, 0)
        ;
        List<Order> goodsOrders = orderService.findByStatus(wrapper);
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^商品热销订单排行任务 2.查询结果一共有数据{},条,分别为:{}", goodsOrders.size(), goodsOrders);
        if (goodsOrders.size() == 0) {
            return;
        }
        String yesterdayStr = DateUtil.dateToString(yesterday, DateUtil.LONG_DATE_FORMAT);
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^商品热销订单排行任务 3.昨日zset key为:{}");
        // 平台排行榜key
        String platZsetYesterdayKey = RedisKeyConstant.RANKING_LIST_PLAT + yesterdayStr;
        for (Order order : goodsOrders) {
            Long goodsId = order.getGoodsId();
            Integer saleNumber = order.getNumber();
            String goodsIdStr;
            if (goodsId == null) {
                continue;
            }
            if (order.getShopId() == null) {
                continue;
            }
            goodsIdStr = goodsId.toString();
            // 店铺排行榜key
            String shopZsetYesterdayKey = RedisKeyConstant.RANKING_LIST_SHOP + order.getShopId() + ":" + yesterdayStr;
            String shopZsetYesterdayKeyParent = null;

            //如果是分销的商品
            if (!order.getShopId().equals(order.getGoodsShopId())) {
                shopZsetYesterdayKeyParent = RedisKeyConstant.RANKING_LIST_SHOP + order.getGoodsShopId() + ":" + yesterdayStr;
            }

            // 店铺排行 - 销售店铺
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 销售商-店铺={},商品={}", order.getShopId(), order.getGoodsId());
            stringRedisTemplate.opsForZSet().incrementScore(shopZsetYesterdayKey, goodsIdStr, saleNumber);

            //平台商品id
            String platGoodsIdStr = null;
            // 店铺排行-供应商店铺
            if (shopZsetYesterdayKeyParent != null) {
                GoodProductDTO goodProductDTO = goodsAppService.queryProduct(order.getMerchantId(), order.getShopId(), order.getGoodsId());
                if (goodProductDTO != null) {
                    Long parentId = goodProductDTO.getParentId();
                    if (parentId != null) {
                        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 发货商-店铺={},商品={}", order.getGoodsShopId(), parentId);
                        stringRedisTemplate.opsForZSet().incrementScore(shopZsetYesterdayKeyParent, parentId.toString(), saleNumber);

                        // 平台商品
                        platGoodsIdStr = parentId.toString();
                    }
                }
            } else {
                platGoodsIdStr = goodsIdStr;
            }
            // 平台排行
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 平台商品={}", platGoodsIdStr);
            stringRedisTemplate.opsForZSet().incrementScore(platZsetYesterdayKey, platGoodsIdStr, saleNumber);
        }
    }
}
