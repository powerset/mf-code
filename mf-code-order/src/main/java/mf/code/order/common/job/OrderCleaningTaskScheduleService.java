package mf.code.order.common.job;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.repo.repository.MerchantRepository;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.order.common.job
 *
 * @description: 商品订单维权过后的结算--取消其定义
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月24日 21:15
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class OrderCleaningTaskScheduleService {
    @Value("${taskExecutor.jobMode}")
    private Integer jobMode;
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;
    @Autowired
    private MerchantRepository merchantRepository;

    private final int MAX_LIMIT = 30;

    @Scheduled(fixedRate = 60000)
    public void orderCleaningScheduled() {
        //根据配置内的jobMode得知是否跑定时器
        if (jobMode == 0) {
            return;
        }
        log.info("<<<<<<<<开始进入结算商品订单定时器~~");

        int pageNum = 1;
        for (; ; pageNum++) {
            QueryWrapper<MerchantOrder> merchantOrderQueryWrapper = new QueryWrapper<>();
            merchantOrderQueryWrapper.lambda()
                    .eq(MerchantOrder::getType, mf.code.merchant.constants.OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode())
                    .eq(MerchantOrder::getStatus, mf.code.merchant.constants.OrderStatusEnum.ORDERED.getCode())
                    .eq(MerchantOrder::getBizType, BizTypeEnum.SHOPPING.getCode())
                    .eq(MerchantOrder::getMfTradeType, MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode())
            ;
            Page<MerchantOrder> page = new Page<>(pageNum, MAX_LIMIT);
            page.setAsc("ctime");
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderRepository.page(page, merchantOrderQueryWrapper);
            List<MerchantOrder> merchantOrders = merchantOrderIPage.getRecords();
            if (CollectionUtils.isEmpty(merchantOrders)) {
                log.info("<<<<<<<<获取待结算订单未空，结束结算商品订单定时器~~");
                break;
            }

            //获得所有的orderIds
            List<Long> orderIds = new ArrayList<>();
            Map<Long, MerchantOrder> merchantOrderMap = new HashMap<>();
            for (MerchantOrder merchantOrder : merchantOrders) {
                if (orderIds.indexOf(merchantOrder.getBizValue()) == -1) {
                    orderIds.add(merchantOrder.getBizValue());
                }
                merchantOrderMap.put(merchantOrder.getBizValue(), merchantOrder);
            }

            if (CollectionUtils.isEmpty(orderIds)) {
                log.error("<<<<<<<<根据商户待结算订单，查询商品订单编号集合为空，结束定时器~~ merchantOrders:{}", JSONObject.toJSONString(merchantOrders));
                continue;
            }

            //对于收货之后，小于7天的，不做处理
            Date date = DateUtils.addDays(new Date(), -7);
            //每次只查询30个
            QueryWrapper<Order> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .in(Order::getId, orderIds)
                    .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                    .eq(Order::getPayChannel, OrderPayChannelEnum.WX.getCode())
                    .eq(Order::getStatus, OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode())
                    .lt(Order::getDealTime, date)
            ;
            List<Order> orderList = goodsOrderRepository.list(wrapper);
            if (CollectionUtils.isEmpty(orderList)) {
                log.info("<<<<<<<<根绝商品订单编号集合，查询订单未空~~ order:{}", JSONObject.toJSONString(orderList));
                continue;
            }
            log.info("<<<<<<<<此次结算商品订单对象, size:{}", orderList.size());

            for (Order order : orderList) {
                MerchantOrder merchantOrder = merchantOrderMap.get(order.getId());
                if(merchantOrder == null){
                    continue;
                }

                MerchantOrder merchantOrder1 = WxCallbackAboutDTO.updateMerchantOrderByProduct(merchantOrder);
                this.merchantOrderRepository.updateById(merchantOrder1);

                Long parentMerchantId = this.merchantRepository.getParentMerchantId(merchantOrder.getMerchantId());
                QueryWrapper<MerchantBalance> balanceQueryWrapper = new QueryWrapper<>();
                balanceQueryWrapper.lambda()
                        .eq(MerchantBalance::getMerchantId, parentMerchantId)
                        .eq(MerchantBalance::getCustomerId, merchantOrder.getMerchantId())
                        .eq(MerchantBalance::getShopId, merchantOrder.getShopId())
                ;
                //存储or更新余额
                MerchantBalance merchantBalance = this.merchantBalanceRepository.getOne(balanceQueryWrapper);
                if (merchantBalance == null) {
                    merchantBalance = new MerchantBalance();
                    merchantBalance.setMerchantId(parentMerchantId);
                    merchantBalance.setShopId(merchantOrder.getShopId());
                    merchantBalance.setCustomerId(merchantOrder.getMerchantId());
                    merchantBalance.setDepositCash(BigDecimal.ZERO);
                    merchantBalance.setDepositAdvance(merchantOrder.getTotalFee());
                    merchantBalance.setCtime(new Date());
                    merchantBalance.setUtime(new Date());
                    merchantBalance.setVersion(0);
                    this.merchantBalanceRepository.insertSelective(merchantBalance);
                } else {
                    merchantBalanceRepository.addUpdateByVersionId(merchantBalance, merchantOrder.getTotalFee(), BigDecimal.ZERO);
                }
            }
        }
    }
}
