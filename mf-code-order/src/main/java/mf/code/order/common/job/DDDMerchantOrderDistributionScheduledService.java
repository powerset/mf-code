package mf.code.order.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.repo.repository.MerchantRepository;
import mf.code.order.service.OrderCallBackService;
import mf.code.order.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.common.job
 * Description: 查询订单状态-并得知是否退款 定时器
 *
 * @author: 夜辰
 * @date: 2018-11-13 11:26
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class DDDMerchantOrderDistributionScheduledService {
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;

    private final int MAX_LIMIT = 30;

    /**
     * 抖音卖家版-结算-所得佣金金额
     */
    @Scheduled(cron = "0 0 0 20 * ? ")
    public void MerchantOrderDistributionS() {
        log.info("<<<<<<<<开始进入结算商品订单定时器~~");
        int pageNum = 1;
        for (; ; pageNum++) {
            QueryWrapper<MerchantOrder> merchantOrderQueryWrapper = new QueryWrapper<>();
            merchantOrderQueryWrapper.lambda()
                    .eq(MerchantOrder::getType, mf.code.merchant.constants.OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode())
                    .eq(MerchantOrder::getStatus, mf.code.merchant.constants.OrderStatusEnum.ORDERED.getCode())
                    .eq(MerchantOrder::getBizType, BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode())
                    .eq(MerchantOrder::getMfTradeType, MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode())
            ;
            Page<MerchantOrder> page = new Page<>(pageNum, MAX_LIMIT);
            page.setAsc("ctime");
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderRepository.page(page, merchantOrderQueryWrapper);
            List<MerchantOrder> merchantOrders = merchantOrderIPage.getRecords();
            if (CollectionUtils.isEmpty(merchantOrders)) {
                log.info("<<<<<<<<获取待结算订单未空，结束结算商品订单定时器~~");
                break;
            }

            for (MerchantOrder merchantOrder : merchantOrders) {
                merchantOrder.setMfTradeType(MfTradeTypeEnum.GOODS_SALE_CLEANING_INTO_BALANCE.getCode());
                merchantOrder.setUtime(new Date());
                merchantOrderRepository.updateById(merchantOrder);

                Long parentMerchantId = this.merchantRepository.getParentMerchantId(merchantOrder.getMerchantId());
                QueryWrapper<MerchantBalance> balanceQueryWrapper = new QueryWrapper<>();
                balanceQueryWrapper.lambda()
                        .eq(MerchantBalance::getMerchantId, parentMerchantId)
                        .eq(MerchantBalance::getCustomerId, merchantOrder.getMerchantId())
                        .eq(MerchantBalance::getShopId, merchantOrder.getShopId())
                ;
                //存储or更新余额
                MerchantBalance merchantBalance = this.merchantBalanceRepository.getOne(balanceQueryWrapper);
                if (merchantBalance == null) {
                    merchantBalance = new MerchantBalance();
                    merchantBalance.setMerchantId(parentMerchantId);
                    merchantBalance.setShopId(merchantOrder.getShopId());
                    merchantBalance.setCustomerId(merchantOrder.getMerchantId());
                    merchantBalance.setDepositCash(BigDecimal.ZERO);
                    merchantBalance.setDepositAdvance(merchantOrder.getTotalFee());
                    merchantBalance.setCtime(new Date());
                    merchantBalance.setUtime(new Date());
                    merchantBalance.setVersion(0);
                    this.merchantBalanceRepository.insertSelective(merchantBalance);
                } else {
                    merchantBalanceRepository.addUpdateByVersionId(merchantBalance, merchantOrder.getTotalFee(), BigDecimal.ZERO);
                }
            }

        }
    }
}
