package mf.code.order.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.service.OrderStatusService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * mf.code.order.common.job
 *
 * @description: 7天后自动确认收货
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月16日 21:05
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class AutoOrderConfirmTaskScheduledService {
    @Value("${taskExecutor.jobMode}")
    private Integer jobMode;
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private OrderStatusService orderStatusService;

    private final int MAX_LIMIT = 30;


    @Scheduled(fixedRate = 60000)
    public void appletRefundScheduled() {
        //根据配置内的jobMode得知是否跑定时器
        if (jobMode == 0) {
            return;
        }

        int pageNum = 1;
        for (; ; pageNum++) {
            //对于发货之后，小于7天的，不做处理
            Date date = DateUtils.addDays(new Date(), -7);
            //每次只查询30个
            QueryWrapper<Order> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                    .in(Order::getPayChannel, Arrays.asList(OrderPayChannelEnum.WX.getCode(), OrderPayChannelEnum.ALIPAY.getCode()))
                    .eq(Order::getStatus, OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode())
                    .lt(Order::getSendTime, date)
            ;
            Page<Order> page = new Page<>(pageNum, MAX_LIMIT);
            page.setAsc("ctime");
            IPage<Order> orderIPage = goodsOrderRepository.queryPage(page, wrapper);
            List<Order> orderList = orderIPage.getRecords();
            if (CollectionUtils.isEmpty(orderList)) {
                break;
            }
            for (Order order : orderList) {
                if (StringUtils.isBlank(order.getLogistics())) {
                    log.error("<<<<<<<<物流信息不存在，异常过程订单orderId:{}", order.getId());
                }

                //7天后自动确认收货
                int i1 = this.orderStatusService.buyerConfirmReceipt(order);
                if (i1 == 0) {
                    log.error("<<<<<<<待付款定时器-更新订单-异常001：orderId:{}", order.getId());
                }
            }
        }
    }
}
