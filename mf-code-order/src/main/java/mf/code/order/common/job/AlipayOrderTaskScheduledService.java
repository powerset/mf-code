package mf.code.order.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.service.OrderCallBackService;
import mf.code.order.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * mf.code.common.job
 * Description: 查询订单状态-并得知是否退款 定时器
 *
 * @author: 夜辰
 * @date: 2018-11-13 11:26
 */
@Slf4j
@Component
@Profile(value = {"dev", "test2", "prod"})
public class AlipayOrderTaskScheduledService {
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private OrderCallBackService orderCallBackService;
    @Autowired
    private OrderStatusService orderStatusService;

    @Value("${taskExecutor.alipayJobMode}")
    private Integer jobMode;

    private final int MAX_LIMIT = 30;

    /**
     * 订单直接支付成功-trade_id不用顾忌
     */
    @Scheduled(fixedDelay = 5000)
    public void appletRefundScheduled() {
        //根据配置内的jobMode得知是否跑定时器 0为关闭，1为开启
        if (jobMode == 0) {
            return;
        }
        int pageNum = 1;
        for (; ; pageNum++) {
            //每次只查询30个
            QueryWrapper<Order> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                    .eq(Order::getPayChannel, OrderPayChannelEnum.ALIPAY.getCode())
                    .eq(Order::getStatus, OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode())
            ;
            Page<Order> page = new Page<>(pageNum, MAX_LIMIT);
            page.setAsc("ctime");
            IPage<Order> orderIPage = goodsOrderRepository.queryPage(page, wrapper);
            List<Order> orderList = orderIPage.getRecords();
            if (CollectionUtils.isEmpty(orderList)) {
                break;
            }
            for (Order order : orderList) {
                order.setPaymentTime(new Date());
                order.setStatus(OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
                order.setNotifyTime(new Date());
                order.setUtime(new Date());
                int i = orderStatusService.buyerPurchaseSuccess(order);
                log.info("订单更新成功与否：{} orderId:{}", i, order.getId());
                if (i == 0) {
                    log.error("订单更新成功与否 orderId:{}", order.getId());
                    continue;
                }
                orderCallBackService.payOrderCallBack(order);
            }
        }
    }
}
