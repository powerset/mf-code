package mf.code.order.domain.aggregateroot;

import lombok.Data;
import mf.code.order.domain.valueobject.UserOrderHistory;
import mf.code.order.dto.AppletUserOrderHistoryDTO;
import mf.code.order.dto.OrderBizResp;
import mf.code.order.dto.OrderResp;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.po.OrderBiz;
import mf.code.user.dto.UserResp;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * 订单用户：与订单交易相关的 用户信息
 *
 * mf.code.order.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 10:52
 */
@Data
public class OrderUser {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 用户信息
	 */
	private UserResp userInfo;
	/**
	 * 订单历史记录
	 */
	private UserOrderHistory userOrderHistory;

	/**
	 * 深拷贝一份用户订单历史记录，用于数据传输
	 * @return
	 */
	public AppletUserOrderHistoryDTO copyUserOrderHistory() {
		AppletUserOrderHistoryDTO appletUserOrderHistoryDTO = new AppletUserOrderHistoryDTO();

		appletUserOrderHistoryDTO.setUserId(this.userOrderHistory.getUserId());
		appletUserOrderHistoryDTO.setShopId(this.userOrderHistory.getShopId());
		appletUserOrderHistoryDTO.setBeginTime(this.userOrderHistory.getBeginTime());
		appletUserOrderHistoryDTO.setEndTime(this.userOrderHistory.getEndTime());
		appletUserOrderHistoryDTO.setLastSpendAmount(this.userOrderHistory.getLastSpendAmount());
		appletUserOrderHistoryDTO.setSpendAmount(this.userOrderHistory.getSpendAmount());

		// 处理 已支付的订单
		List<OrderResp> paidOrder = this.userOrderHistory.getPaidOrder();
		if (!CollectionUtils.isEmpty(paidOrder)) {
			List<OrderResp> newPaidOrder = new ArrayList<>(paidOrder);
			appletUserOrderHistoryDTO.setPaidOrder(newPaidOrder);
		}


		// 处理 售后处理中的订单
		List<Order> afterSaleProcessing = this.userOrderHistory.getAfterSaleProcessing();
		List<OrderResp> afterSaleProcessingResp = new ArrayList<>();
		if (!CollectionUtils.isEmpty(afterSaleProcessing)) {
			for (Order order : afterSaleProcessing) {
				OrderResp orderResp = new OrderResp();
				BeanUtils.copyProperties(order, orderResp);
				afterSaleProcessingResp.add(orderResp);
			}
		}
		appletUserOrderHistoryDTO.setAfterSaleProcessing(afterSaleProcessingResp);

		return appletUserOrderHistoryDTO;
	}
}
