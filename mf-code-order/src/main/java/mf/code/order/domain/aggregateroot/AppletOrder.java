package mf.code.order.domain.aggregateroot;

import lombok.Data;
import mf.code.order.dto.AppletOrderResp;
import mf.code.order.dto.OrderBizResp;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderBizBizTypeEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.po.OrderBiz;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.domain.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 13:52
 */
@Data
public class AppletOrder {
	/**
	 * 订单id
	 */
	private Long id;
	/**
	 * 订单详情
	 */
	private Order orderInfo;
	/**
	 * 订单相关业务属性
	 */
	private List<OrderBiz> orderBizList;

	/**
	 * 获取订单分销业务属性
	 * @return
	 */
	public OrderBiz queryOrderDistributionBiz() {
		if (CollectionUtils.isEmpty(orderBizList)) {
			return null;
		}
		for (OrderBiz orderBize : orderBizList) {
			if (OrderBizBizTypeEnum.DISTRIBUTION.getCode() == orderBize.getBizType()) {
				return orderBize;
			}
		}
		return null;
	}

	/**
	 * 复制一份订单信息，用于数据传输
	 * @return
	 */
	public AppletOrderResp copyAppletOrder() {

		OrderResp orderResp = new OrderResp();
		BeanUtils.copyProperties(this.orderInfo, orderResp);

		List<OrderBizResp> orderBizRespList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(orderBizList)) {
			for (OrderBiz orderBiz : this.orderBizList) {
				OrderBizResp orderBizResp = new OrderBizResp();
				BeanUtils.copyProperties(orderBiz, orderBizResp);
				orderBizRespList.add(orderBizResp);
			}
		}

		AppletOrderResp appletOrderResp = new AppletOrderResp();
		appletOrderResp.setId(this.id);
		appletOrderResp.setOrderInfo(orderResp);
		appletOrderResp.setOrderBizList(orderBizRespList);
		return appletOrderResp;
	}
}
