package mf.code.order.domain.valueobject;

import lombok.Data;
import mf.code.order.dto.OrderResp;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.po.OrderBiz;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 当前店铺下 某时间段的 用户的消费订单
 *
 * mf.code.order.domain
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-06 22:59
 */
@Data
public class UserOrderHistory {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 联合标识：开始时间
	 */
	private Date beginTime;
	/**
	 * 联合标识：截止时间
	 */
	private Date endTime;
	/**
	 * 现在日期的上月消费金额 FIXME_OK 2.1 按用户维度计算，应总和所有店铺消费的订单
	 */
	private BigDecimal lastSpendAmount;
	/**
	 * TODO_OK 2.1 迭代 按用户维度计算，应总和 指定时间周期的 所有店铺消费的订单
	 */
	private BigDecimal spendAmount;
	/**
	 * 已支付的订单 -- 带有分销属性
	 */
	private List<OrderResp> paidOrder;
	/**
	 * 售后处理中的订单，此订单时间周期为 beginTime 至 now
	 */
	private List<Order> afterSaleProcessing;

	/**
	 * 本月20日 可结算订单 = 上月 已支付订单 - 售后处理中订单
	 * beginTime = 上月开始
	 * endTiem = 上月结束
	 *
	 * @return
	 */
	public List<OrderResp> querySettledOrders() {
		List<OrderResp> settledOrders = new ArrayList<>();
		if (CollectionUtils.isEmpty(paidOrder)) {
			return settledOrders;
		}
		Map<Long, Order> afterSaleProcessingMap = new HashMap<>();
		for (Order order : this.afterSaleProcessing) {
			afterSaleProcessingMap.put(order.getId(), order);
		}
		for (OrderResp orderResp : this.paidOrder) {
			if (afterSaleProcessingMap.containsKey(orderResp.getId())) {
				continue;
			}
			settledOrders.add(orderResp);
		}
		return settledOrders;
	}

	/**
	 * 本段期间内的 花费金额
	 * @return
	 */
	public BigDecimal querySpendAmount() {
		BigDecimal spendAmount = BigDecimal.ZERO;
		List<OrderResp> orders = this.querySettledOrders();
		if (CollectionUtils.isEmpty(orders)) {
			return spendAmount;
		}
		for (OrderResp order : orders) {
			spendAmount = spendAmount.add(order.getFee());
		}
		return spendAmount;
	}



}
