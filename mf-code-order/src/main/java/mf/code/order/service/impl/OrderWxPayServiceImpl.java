package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.MD5Util;
import mf.code.common.utils.RandomStrUtil;
import mf.code.common.utils.SortUtil;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.constants.OrderTypeEnum;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.common.caller.wxpay.WXPayUtil;
import mf.code.order.common.caller.wxpay.WeixinPayConstants;
import mf.code.order.common.caller.wxpay.WeixinPayService;
import mf.code.order.common.caller.wxpay.WxpayProperty;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.repo.repository.MerchantRepository;
import mf.code.order.service.*;
import mf.code.user.dto.UpayWxOrderReqDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class OrderWxPayServiceImpl implements OrderWxPayService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private OrderAboutService orderAboutService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private OrderService orderService;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;
    @Autowired
    private OrderCallBackService orderCallBackService;

    @Override
    public SimpleResponse wxPayType(Order goodsOrder, GoodsOrderReq req, String openId, GoodProductDTO goodProductDTO) {
        String body = this.getWxPayBody(goodsOrder);
        Map<String, Object> unifiedorderReqParams = this.weixinPayService.getUnifiedorderReqParams(goodsOrder.getFee(), goodsOrder.getOrderNo(), goodsOrder.getIpAddress(),
                wxpayProperty.getMfAppId(), WeixinPayConstants.JSAPI, goodsOrder.getRemark(), openId, WeixinPayConstants.APPLET_NOTIFY_SOURCE, body);
        goodsOrder.setReqJson(JSON.toJSONString(unifiedorderReqParams));
        this.orderStatusService.buyerPurchaseing(goodsOrder, goodProductDTO, req);

        //扣库存
        SimpleResponse simpleResponse1 = this.goodsAppService.consumeStock(req.getSkuId(), NumberUtils.toInt(req.getNumber()), goodsOrder.getId());
        if (simpleResponse1.error()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "商品库存出现异常，亲，重试下哦");
        }

        //step5: 拼接调用微信平台统一下单接口的参数
        //统一下单的返回
        Map<String, Object> unifiedorderRespMap = this.weixinPayService.unifiedorder(unifiedorderReqParams);
        log.info("小程序支付后的结果 xmlMap:{}", unifiedorderRespMap);
        if (unifiedorderRespMap == null || (StringUtils.isNotBlank(unifiedorderRespMap.get("return_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("return_code").toString()))
                || (StringUtils.isNotBlank(unifiedorderRespMap.get("result_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("result_code").toString()))) {
            String msg = "";
            if (StringUtils.isNotBlank(unifiedorderRespMap.get("return_msg").toString())) {
                msg = unifiedorderRespMap.get("return_msg").toString();
            } else if (StringUtils.isNotBlank(unifiedorderRespMap.get("err_code_des").toString())) {
                msg = unifiedorderRespMap.get("err_code_des").toString();
            } else {
                msg = "微信充值异常...";
            }
            //失败异常处理，订单失败，退库存
            //更新db
            goodsOrder.setRemark(msg);
            goodsOrder.setDel(DelEnum.YES.getCode());
            int i = this.orderStatusService.buyerOrderCreateWxException(goodsOrder);
            if (i == 0) {
                log.error("<<<<<<<<创建支付订单-调用微信接口异常001,orderId:{}");
            }

            //退库存
            SimpleResponse simpleResponse = this.goodsAppService.giveBackStock(goodsOrder.getSkuId(), goodsOrder.getNumber(), goodsOrder.getId());
            if (simpleResponse.error()) {
                log.error("<<<<<<<待付款定时器-退库存-异常002：orderId:{}", goodsOrder.getId());
            }
            orderService.backUserCoupon(goodsOrder);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), msg);
        }
        goodsOrder.setRespJson(JSON.toJSONString(unifiedorderRespMap));
        this.goodsOrderRepository.updateById(goodsOrder);

        Map<String, Object> respMap = new HashMap<>();
        respMap.put("timeStamp", String.valueOf(DateUtil.getCurrentSeconds()));
        respMap.put("nonceStr", RandomStrUtil.randomStr(28));
        String packageInfo = "prepay_id=" + unifiedorderRespMap.get("prepay_id");
        respMap.put("package", packageInfo);
        respMap.put("signType", "MD5");
        String sortSign = SortUtil.buildSignStr(respMap);
        String sign = "appId=" + wxpayProperty.getMfAppId() + "&" + sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        log.info("小程序返回前端的sign：{}", sign);
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        //签名sign
        respMap.put("paySign", sign);
        respMap.put("orderId", goodsOrder.getId());

        //15分钟过期
        int timeoutMinute = 15;
        respMap.put("cutoffTime", DateUtils.addMinutes(new Date(), timeoutMinute));
        respMap.put("leftTime", this.orderAboutService.getWillPayLeftTimeSecond(goodsOrder));

        SimpleResponse d = new SimpleResponse();
        d.setData(respMap);
        //存储redis 15分钟过期
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey(), JSON.toJSONString(d), timeoutMinute, TimeUnit.MINUTES);
        return d;
    }

    @Override
    public String wxPayCallback(String orderNo, String transactionId, BigDecimal totalFee, Date paymentTime, String payOpenId) {
        //redis去重
        String redisForbidKey = RedisKeyConstant.GOODORDER_FORBID_KEY + orderNo;
        Boolean aBoolean = stringRedisTemplate.opsForValue().setIfAbsent(redisForbidKey, "");
        if (aBoolean != null && !aBoolean) {
            return "微信支付回调失败!回调正在触发，请勿重复";
        }

        QueryWrapper<Order> orderWrapper = new QueryWrapper<>();
        orderWrapper.lambda()
                .eq(Order::getOrderNo, orderNo);
        Order order = this.goodsOrderRepository.selectOne(orderWrapper);
        if (order == null) {
            log.error("订单已不存在，orderNo:{}", orderNo);
        }
        //若订单已经不再是待付款状态直接返回
        if (order.getStatus() != OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            log.warn("支付回调警告-回调方法调用订单，该订单已经不是待付款状态");
            return WXPayUtil.resp();
        }
        if (totalFee.subtract(order.getFee()).compareTo(BigDecimal.ZERO) != 0) {
            log.warn("支付失败-下单金额跟回调的金额不匹配 totalPrice:{};orderFee:{}", totalFee, order.getFee());
        }

        order.setPaymentTime(paymentTime);
        order.setTradeId(transactionId);
        order.setStatus(OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        order.setFee(totalFee.setScale(2, BigDecimal.ROUND_DOWN));
        order.setNotifyTime(new Date());
        order.setUtime(new Date());

        int i = orderStatusService.buyerPurchaseSuccess(order);
        log.info("订单更新成功与否：{} orderId:{}", i, order.getId());
        if (i == 0) {
            log.error("订单更新成功与否 orderId:{}", order.getId());
            return "更新失败";
        }
        //异步处理业务
        orderCallBackService.payOrderCallBack(order);
        return WXPayUtil.resp();
    }

    @Override
    public String wxRefundCallback(String orderNo, String refundOrderNo, BigDecimal totalFee, BigDecimal settlementRefundFee, String transactionId) {
        //redis去重
        String redisForbidKey = RedisKeyConstant.GOODORDER_FORBID_KEY + refundOrderNo;
        if (!stringRedisTemplate.opsForValue().setIfAbsent(redisForbidKey, "")) {
            return "微信退款正在触发，请勿重复";
        }

        QueryWrapper<Order> orderWrapper = new QueryWrapper<>();
        orderWrapper.lambda()
                .eq(Order::getOrderNo, orderNo);
        Order oldGoodsOrder = this.goodsOrderRepository.selectOne(orderWrapper);
        if (oldGoodsOrder == null) {
            log.error("原订单已不存在，orderNo:{}", orderNo);
            return WXPayUtil.resp();
        }
        orderWrapper = new QueryWrapper<>();
        orderWrapper.lambda()
                .eq(Order::getOrderNo, refundOrderNo);
        Order refundGoodsOrder = this.goodsOrderRepository.selectOne(orderWrapper);
        BigDecimal totalPrice = refundGoodsOrder.getFee().multiply(new BigDecimal(refundGoodsOrder.getNumber()));
        if (settlementRefundFee.subtract(totalPrice).compareTo(BigDecimal.ZERO) != 0) {
            return "退款失败-退款金额跟回调的金额不匹配";
        }
        int i = this.orderStatusService.refundSuccess(refundGoodsOrder);
        log.info("退款订单更新成功与否：{}", i);

        //refundOrderCallBackBiz
        orderCallBackService.refundOrderCallBackBiz(refundGoodsOrder, oldGoodsOrder);
        return WXPayUtil.resp();
    }

    @Override
    public SimpleResponse wxRefund(String oldOrderNo, BigDecimal oldOrderFee, String refundOrderNo, BigDecimal refundFee, String refundDesc) {
        //开始申请退款
        Map<String, Object> param = weixinPayService.getRefundPayOrderReqParams(oldOrderNo, refundOrderNo,
                oldOrderFee, refundFee, WeixinPayConstants.APPLET_NOTIFY_SOURCE, refundDesc);
        Map<String, Object> result = weixinPayService.refundPayOrder(param);
        log.info("<<<<<<<<<< refund resp: {} req: {}", result, param);
        String WX_RETURN_CODE_VAL = "FAIL";
        String WX_RETURN_CODE_KEY = "return_code";
        if (result.get(WX_RETURN_CODE_KEY) != null && WX_RETURN_CODE_VAL.equals(result.get(WX_RETURN_CODE_KEY))
                || result.get("result_code") != null && WX_RETURN_CODE_VAL.equals(result.get("result_code"))) {
            String errMsg = null;
            if (result.get("err_code") != null && StringUtils.isNotBlank(result.get("err_code").toString())) {
                errMsg = result.get("err_code").toString();
                if (result.get("err_code_des") != null && StringUtils.isNotBlank(result.get("err_code_des").toString())) {
                    errMsg = result.get("err_code_des").toString();
                }
            } else if (result.get("return_msg") != null && StringUtils.isNotBlank(result.get("return_msg").toString())) {
                errMsg = result.get("return_msg").toString();
            }
            return new SimpleResponse(-1, "调用微信申请退款接口有误,微信返回代码:" + result == null ? "" : errMsg);
        }
        return new SimpleResponse(0, "success");
    }

    /***
     * 获取订单..商品详情
     * @param goodsOrder
     * @return
     */
    private String getWxPayBody(Order goodsOrder) {
        return goodsOrder.getOrderName();
    }
}
