package mf.code.order.service;

import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.order.feignapi.constant.OrderBizStatusEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.repo.po.Order;

import java.util.List;
import java.util.Map;

/**
 * mf.code.order.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月09日 16:58
 */
public interface OrderAboutService {
    /***
     * 获取业务状态
     * @param goodsOrder
     * @return
     */
    OrderBizStatusEnum getBizStatus(Order goodsOrder);

    Map getOrderGoodsInfo(Long merchantId, Long shopId, Long goodsId, Long skuId, Long userId);

    Map getOrderGoodsInfoBySkuMap(Order order, Map<Long, GoodsSkuDTO> goodsSkuDTOMap);

    Map<Long, Order> getOldRefundOrderMap(List<Order> goodsOrders);

    /***
     * 获取数据库状态
     * @param orderBizStatusEnum
     * @return
     */
    OrderStatusEnum getOrderStatusFromBizStatus(OrderBizStatusEnum orderBizStatusEnum);

    /***
     * 获取协商历史
     * @param merchantId
     * @param shopId
     * @param orderId
     * @param userId
     * @return
     */
    List<Map> queryChatHistory(Long merchantId, Long shopId, Long orderId, Long userId);

    /***
     * 获取订单号 yyMMddHHmmss1(13位)+ shopid(10000)+2019 +6随机数字组成
     * @param shopId
     * @return
     */
    String getOrderNo(Long shopId);

    /***
     * 获取待付款订单的剩余时间,单位s
     */
    int getWillPayLeftTimeSecond(Order order);
}
