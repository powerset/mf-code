package mf.code.order.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.repo.po.Order;

/**
 * mf.code.order.service
 * Description: 无需支付的订单支付流程
 *
 * @author gel
 * @date 2019-07-17 19:42
 */
public interface OrderNoPayService {

    /**
     * 完成订单
     *
     * @param goodsOrder     订单
     * @param req            入参
     * @param goodProductDTO 商品信息
     * @return SimpleResponse
     */
    SimpleResponse completeOrder(Order goodsOrder, GoodsOrderReq req, GoodProductDTO goodProductDTO);
}
