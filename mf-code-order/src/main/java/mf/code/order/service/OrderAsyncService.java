package mf.code.order.service;

import mf.code.order.repo.po.Order;

/**
 * mf.code.order.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月16日 09:06
 */
public interface OrderAsyncService {

    void generateFalseOrder();

    void merchantAmountAchieved(Order order);
}
