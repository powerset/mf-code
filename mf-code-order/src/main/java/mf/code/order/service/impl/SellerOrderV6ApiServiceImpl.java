package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.dto.OrderCommentDTO;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.SellerProductAndSkuResultDTO;
import mf.code.order.api.feignclient.CommentAppService;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.constant.SellerOrderTypeEnum;
import mf.code.order.dto.SellerOrderInfoDTO;
import mf.code.order.dto.SellerOrderListReqDTO;
import mf.code.order.dto.SellerOrderListResultDTO;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.GoodsOrderV6Repository;
import mf.code.order.service.SellerOrderV6ApiService;
import mf.code.user.dto.SellerUserAddressResultDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Slf4j
@Service
public class SellerOrderV6ApiServiceImpl implements SellerOrderV6ApiService {

    @Autowired
    private GoodsOrderV6Repository goodsOrderV6Repository;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private GoodsAppService goodsAppService;

    @Autowired
    private CommentAppService commentAppService;

    /**
     * 查询订单列表
     *
     * @param reqDTO
     * @return
     */
    @Override
    public SellerOrderListResultDTO getOrderList(SellerOrderListReqDTO reqDTO) {
        Assert.notNull(reqDTO.getTabType(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "tab没有传");
        Map<String, Object> params = new HashMap<>();
        SellerOrderListResultDTO resultDTO = new SellerOrderListResultDTO();
        resultDTO.setPage(reqDTO.getPage());
        resultDTO.setSize(reqDTO.getSize());
        // 转换订单参数
        SimpleResponse paramSimpleResponse = changeParamsForSelectOrderList(reqDTO, params);
        if (paramSimpleResponse.error()) {
            return resultDTO;
        }
        Map<String, Object> resultMap = goodsOrderV6Repository.pageOrderListForSeller(params, reqDTO);
        if (null == resultMap.get("list")) {
            return resultDTO;
        }
        List<Order> orderList = (List<Order>) resultMap.get("list");
        resultDTO.setCount((Long) resultMap.get("count"));
        if (CollectionUtils.isEmpty(orderList)) {
            resultDTO.setOrderList(new ArrayList<>());
            return resultDTO;
        }
        Set<Long> addressIdSet = new HashSet<>();
        Set<Long> skuIdSet = new HashSet<>();
        // 退款订单所绑定的支付订单的id列表
        Set<Long> orderIdSet = new HashSet<>();
        // 支付订单列表
        Set<Long> originOrderIdSet = new HashSet<>();
        for (Order order : orderList) {
            // 地址查询，买家信息
            if (order.getAddressId() != null) {
                addressIdSet.add(order.getAddressId());
            }
            // 商品信息
            if (order.getSkuId() != null) {
                skuIdSet.add(order.getSkuId());
            }
            // 退款所绑定的支付订单id
            if (order.getRefundOrderId() != null && order.getRefundOrderId() > 0) {
                orderIdSet.add(order.getRefundOrderId());
            }
            // 支付订单id
            if (order.getType() == OrderTypeEnum.PAY.getCode()) {
                originOrderIdSet.add(order.getId());
            }
        }
        // feign调用查询地址,addressIdSet必定有值
        SimpleResponse addressResponse = userAppService.queryAddressMapForSeller(new ArrayList<>(addressIdSet));
        Map<String, SellerUserAddressResultDTO> addressResultDTOMap = new HashMap<>();
        if (addressResponse == null || addressResponse.error()) {
            resultDTO.setOrderList(new ArrayList<>());
            return resultDTO;
        }
        String jsonString = JSON.toJSONString(addressResponse.getData());
        addressResultDTOMap = JSONObject.parseObject(jsonString, new TypeReference<HashMap<String, SellerUserAddressResultDTO>>() {
        });
        if (CollectionUtils.isEmpty(addressResultDTOMap)) {
            addressResultDTOMap = new HashMap<>();
        }
        // feign调用查询商品信息，skuIdSet必定有值
        SimpleResponse simpleResponse = goodsAppService.queryProductSkuMapBySkuIdList(new ArrayList<>(skuIdSet));
        Map<String, SellerProductAndSkuResultDTO> productAndSkuMap = new HashMap<>();
        if (simpleResponse == null || simpleResponse.error()) {
            resultDTO.setOrderList(new ArrayList<>());
            return resultDTO;
        }
        String skuString = JSON.toJSONString(simpleResponse.getData());
        productAndSkuMap = JSONObject.parseObject(skuString, new TypeReference<HashMap<String, SellerProductAndSkuResultDTO>>() {
        });
        if (CollectionUtils.isEmpty(productAndSkuMap)) {
            productAndSkuMap = new HashMap<>();
        }
        // 如果是退款订单，查询支付订单（太无奈）
        Map<Long, Order> originalOrderMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(orderIdSet)) {
            List<Order> originalOrderList = goodsOrderV6Repository.selectOrderListByIdListForSeller(new ArrayList<>(orderIdSet));
            for (Order originalOrder : originalOrderList) {
                originalOrderMap.put(originalOrder.getId(), originalOrder);
            }
        }
        // 如果是支付订单，查询退款订单（真无奈，待重构）
        Map<Long, Order> payOrderMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(originOrderIdSet)) {
            List<Order> originalOrderList = goodsOrderV6Repository.selectRefundByPayOrderIdList(new ArrayList<>(originOrderIdSet));
            if (!CollectionUtils.isEmpty(originalOrderList)) {
                for (Order originalOrder : originalOrderList) {
                    // 缓存退款订单，key支付订单id，value倒叙第一条退款记录。列表id倒叙
                    if (payOrderMap.get(originalOrder.getRefundOrderId()) != null) {
                        continue;
                    }
                    payOrderMap.put(originalOrder.getRefundOrderId(), originalOrder);
                }
            }
        }
        List<SellerOrderInfoDTO> orderInfoDTOList = new ArrayList<>();
        for (Order order : orderList) {
            SellerOrderInfoDTO infoDTO = new SellerOrderInfoDTO();
            infoDTO.setId(order.getId());
            // 不能操作任何功能按钮
            infoDTO.setOperate(0);
            // 商品来源店铺才能也能操作所有操作
            if (StringUtils.equals(reqDTO.getShopId(), order.getGoodsShopId().toString())) {
                infoDTO.setOperate(1);
            } else {
                infoDTO.setOperate(1);
                // 不是商品所属店铺只有部分操作权限，比如收货，发货不能执行
                if (order.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()
                        || order.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()) {
                    infoDTO.setOperate(0);
                }
            }


            //添加评价状态,只有当订单状态为交易成功的时候才能有订单状态
            if (order.getStatus().equals(OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode())) {
                log.info(">>>>>>>>>>>>>>>>>>>ui find orderStatus=" + JSON.toJSONString(order.getStatus()));
                Long orderId = order.getId();
                List<Long> orderIdList = new ArrayList<>();
                orderIdList.add(orderId);
                //根据订单id查询订单在评论表中的状态
                List<OrderCommentDTO> orderCommentDTOList = commentAppService.getCommentListByShopAndOrderIds(orderIdList, null, null, 1, 500);
                infoDTO.setCommentStatus("--");
                if (!CollectionUtils.isEmpty(orderCommentDTOList)) {
                    OrderCommentDTO orderCommentDTO = orderCommentDTOList.get(0);
                    //直销商户-可回复评价
//                    if (reqDTO.getShopId().equals(order.getGoodsShopId())) {
                        //1待评价，2已评价
                        if (orderCommentDTO.getStatus() < 0) {
                            infoDTO.setCommentStatus("1");
                        } else if (orderCommentDTO.getStatus() >= 0) {
                            infoDTO.setCommentStatus("2");
//                        }
                    }
                }
            }

            infoDTO.setOrderId(order.getId());
            infoDTO.setOrderNo(order.getOrderNo());
            infoDTO.setStatus(order.getStatus());
            infoDTO.setType(order.getType());
            // 退款订单列表需要查询原始类型订单数据
            if (order.getType() != OrderTypeEnum.PAY.getCode()) {
                if (originalOrderMap.get(order.getRefundOrderId()) != null) {
                    Order originalOrder = originalOrderMap.get(order.getRefundOrderId());
                    infoDTO.setOrderNo(originalOrder.getOrderNo());
                }
            }
            // 支付订单列表需要查询退款订单数据，应使用refundType和refundStatus来优先判断是否能点击后续功能按钮
            if (order.getType() == OrderTypeEnum.PAY.getCode()) {
                if (payOrderMap.get(order.getId()) != null) {
                    Order refundOrder = payOrderMap.get(order.getId());
                    infoDTO.setRefundType(refundOrder.getType());
                    infoDTO.setRefundStatus(refundOrder.getStatus());
                    // 支付订单里存在退款订单则不允许做操作
                    infoDTO.setOperate(0);
                    // 如果支付订单状态为待发货，但是用户提出退款，商家拒绝后依然可以发货成功
                    if (order.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()
                            && (refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()
                            || refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode())) {
                        infoDTO.setRefundType(null);
                        infoDTO.setRefundStatus(null);
                        infoDTO.setOperate(1);
                    }
                }
            }
            infoDTO.setCreateTime(DateUtil.dateToString(order.getCtime(), DateUtil.FORMAT_ONE));
            infoDTO.setFee(order.getFee());
            infoDTO.setBuyerMsg(order.getBuyerMsg());
            infoDTO.setSellerMsg(order.getSellerMsg());
            infoDTO.setProductNum(order.getNumber());
            // 买家信息
            SellerUserAddressResultDTO userAddress = addressResultDTOMap.get(order.getAddressId().toString());
            if (userAddress != null) {
                infoDTO.setBuyerAddress(userAddress.getCountry() + userAddress.getProvince() + userAddress.getCity() + userAddress.getStreet() + userAddress.getDetail());
                infoDTO.setBuyerNick(userAddress.getUserName());
                infoDTO.setWxNick(userAddress.getNick());
                infoDTO.setBuyerPhone(userAddress.getPhone());
            }
            infoDTO.setOrderType(SellerOrderTypeEnum.SELF_SUPPORT.getCode());
            // 商品信息
            SellerProductAndSkuResultDTO productAndSku = productAndSkuMap.get(order.getSkuId().toString());
            if (productAndSku != null) {
                infoDTO.setProductMainPics(JSONArray.parseArray(productAndSku.getProductSrcMainPics(), String.class).get(0));
                infoDTO.setProductPrice(productAndSku.getProductPrice());
                infoDTO.setProductTitle(productAndSku.getProductTitle());
                infoDTO.setSkuSpecs(productAndSku.getProductSpecsDesc());
                // 订单类型： 1分销商品订单 2供应商品订单 3自营商品订单
                if (order.getShopId().equals(order.getGoodsShopId())) {
                    // 自营商品
                    infoDTO.setOrderType(SellerOrderTypeEnum.SELF_SUPPORT.getCode());
                } else if (order.getShopId().equals(Long.valueOf(reqDTO.getShopId()))) {
                    // 分销商品
                    infoDTO.setOrderType(SellerOrderTypeEnum.DISTRIBUTION.getCode());
                } else {
                    // 供应商品
                    infoDTO.setOrderType(SellerOrderTypeEnum.SUPPLY.getCode());
                }
            }

            orderInfoDTOList.add(infoDTO);
        }
        resultDTO.setOrderList(orderInfoDTOList);
        return resultDTO;
    }

    /**
     * 转换组装参数 商户查询订单列表
     *
     * @param reqDTO
     * @param params
     */
    private SimpleResponse changeParamsForSelectOrderList(SellerOrderListReqDTO reqDTO, Map<String, Object> params) {
        SimpleResponse paramSimpleResponse = new SimpleResponse();
        if (StringUtils.isNotBlank(reqDTO.getOrderNo())) {
            params.put("orderNo", reqDTO.getOrderNo());
        }
        if (!StringUtils.isEmpty(reqDTO.getCheckStatus())) {
            params.put("checkStatus", reqDTO.getCheckStatus());
            params.put("shopIdOrGoodsShopId", reqDTO.getShopId());
        }

        if (StringUtils.isNotBlank(reqDTO.getBuyerName()) || StringUtils.isNotBlank(reqDTO.getBuyerPhone())) {
            // 先查询用户地址信息，获得用户id
            SimpleResponse simpleResponse = userAppService.queryAddressIdByAddressNickOrPhoneForSeller(reqDTO.getBuyerName(), reqDTO.getBuyerPhone());
            if (simpleResponse == null || simpleResponse.error()) {
                paramSimpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                paramSimpleResponse.setMessage("参数错误");
                return paramSimpleResponse;
            }
            String jsonString = JSON.toJSONString(simpleResponse.getData());
            List<String> addressIdList = JSONObject.parseObject(jsonString, new TypeReference<List<String>>() {
            });
            params.put("addressIdList", addressIdList);
        }
        // TODO 可能有问题
        if (StringUtils.isNumeric(reqDTO.getProductId())) {
            params.put("goodsId", reqDTO.getProductId());
        }
        // 9 为查询全部状态订单, 状态和类型的参数暂时不要颠倒
        if (reqDTO.getOrderStatus() != null) {
            if (reqDTO.getOrderStatus() == OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode()
                    || reqDTO.getOrderStatus() == OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()) {
                params.put("statusList", Arrays.asList(OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode(), OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()));
            } else {
                params.put("status", reqDTO.getOrderStatus());
            }

            if (reqDTO.getOrderStatus() == 9) {
                params.remove("status");
            }
        }
        if (reqDTO.getTabType() != null) {
            // 全部
            if (reqDTO.getTabType() == 0) {
                params.put("type", OrderTypeEnum.PAY.getCode());
            }
            // 待发货
            if (reqDTO.getTabType() == 1) {
                params.put("type", OrderTypeEnum.PAY.getCode());
                params.put("status", OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
            }
            // 退款退货
            if (reqDTO.getTabType() == 2) {
                if (StringUtils.isNotBlank(reqDTO.getOrderNo())) {
                    // 因为只有每条支付订单只有两条退款订单记录，所以直接查询拼接
                    List<Order> orderList = goodsOrderV6Repository.selectRefundByPayOrderNo(reqDTO.getOrderNo());
                    if (!CollectionUtils.isEmpty(orderList)) {
                        List<Long> refundOrderIdList = new ArrayList<>();
                        for (Order order : orderList) {
                            refundOrderIdList.add(order.getId());
                        }
                        params.remove("orderNo");
                        params.put("idList", refundOrderIdList);
                    }
                }
                params.put("typeList", Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()));
            }
        } else {
            params.put("typeList", Arrays.asList(OrderTypeEnum.PAY.getCode(),
                    OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()));
        }
        if (StringUtils.isNotBlank(reqDTO.getCStartTime()) && StringUtils.isNotBlank(reqDTO.getCEndTime())) {
            params.put("cStartTime", DateUtil.stringtoDate(reqDTO.getCStartTime(), DateUtil.FORMAT_ONE));
            params.put("cEndTime", DateUtil.stringtoDate(reqDTO.getCEndTime(), DateUtil.FORMAT_ONE));
        }
        /*
         * 订单类型：
         * 0全部
         * 1分销商品订单
         * 2供应商品订单
         * 3自营商品订单
         * TODO 这里该不该解析呢
         */
        // 因为需要查询出分销订单，所以不能加商户id做查询条件
        if (reqDTO.getOrderType() != null) {
            if (reqDTO.getOrderType() == SellerOrderTypeEnum.DISTRIBUTION.getCode()) {
                params.put("shopId", reqDTO.getShopId());
                params.put("shopIdEqGoodsShopId", 0);
            }
            if (reqDTO.getOrderType() == SellerOrderTypeEnum.SUPPLY.getCode()) {
                params.put("goodsShopId", reqDTO.getShopId());
                params.put("shopIdEqGoodsShopId", 0);
            }
            if (reqDTO.getOrderType() == SellerOrderTypeEnum.SELF_SUPPORT.getCode()) {
                params.put("shopId", reqDTO.getShopId());
                params.put("shopIdEqGoodsShopId", 1);
            }
            if (reqDTO.getOrderType() == 0) {
                params.put("shopIdOrGoodsShopId", reqDTO.getShopId());
            }
        } else {
            params.put("shopIdOrGoodsShopId", reqDTO.getShopId());
        }
        params.put("page", reqDTO.getPage());
        params.put("size", reqDTO.getSize());
        params.put("limit", reqDTO.getSize());
        params.put("offset", reqDTO.getSize() * (reqDTO.getPage() - 1));
        // 查询包括用户已删除的订单
        return paramSimpleResponse;
    }

}
