package mf.code.order.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.utils.RandomStrUtil;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.order.api.applet.dto.OrderStatusData;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.feignapi.constant.*;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.po.OrderReturnsApply;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.OrderReturnsApplyRepository;
import mf.code.order.service.OrderAboutService;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.order.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月09日 16:58
 */
@Service
@Slf4j
public class OrderAboutServiceImpl implements OrderAboutService {
    @Autowired
    private OrderReturnsApplyRepository orderReturnsApplyRepository;
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private UserAppService userAppService;

    /***
     * 获取前端所对应的业务状态
     * @param goodsOrder
     * @return
     */
    @Override
    public OrderBizStatusEnum getBizStatus(Order goodsOrder) {
        //查询是否存在退款订单
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        //支付场景
        if (goodsOrder.getType() == OrderTypeEnum.PAY.getCode()) {
            if (OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.WILLPAY;
            } else if (OrderStatusEnum.WILLSEND_OR_REFUND.getCode() == goodsOrder.getStatus()) {
                wrapper.lambda()
                        .eq(Order::getRefundOrderId, goodsOrder.getId())
                        .in(Order::getType, Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()))
                        .eq(Order::getStatus, OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode())
                ;
                int countOrderNum = this.goodsOrderRepository.countByGoodsIdsByRefundOrderId(goodsOrder.getId(), OrderStatusData.getRefundingStatus());
                if (countOrderNum > 0) {
                    return OrderBizStatusEnum.WILLSEND_REFUNDING;
                } else {
                    return OrderBizStatusEnum.WILLSEND;
                }
            } else if (OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode() == goodsOrder.getStatus()) {
                int countOrderNum = this.goodsOrderRepository.countByGoodsIdsByRefundOrderId(goodsOrder.getId(), OrderStatusData.getRefundingStatus());
                if (countOrderNum > 0) {
                    return OrderBizStatusEnum.WILLRECEIVE_REFUNDING;
                } else {
                    return OrderBizStatusEnum.WILLRECEIVE;
                }
            } else if (OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode() == goodsOrder.getStatus()) {
                int countOrderNum = this.goodsOrderRepository.countByGoodsIdsByRefundOrderId(goodsOrder.getId(), OrderStatusData.getRefundingStatus());
                if (countOrderNum > 0) {
                    return OrderBizStatusEnum.TRADE_SUCCESS_REFUND_ING;
                } else {
                    return OrderBizStatusEnum.TRADE_SUCCESS;
                }

            } else if (OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.TRADE_CLOSE_ORDER_CANCEL;
            } else if (OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.TRADE_CLOSE_REFUND_SUCCESS;
            } else if (OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.TRADE_CLOSE_ORDER_CANCEL;
            } else if (OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.TRADE_CLOSE_REFUND_SUCCESS;
            } else {
                log.error("该订单出现超出预算以外的状态:{}", goodsOrder);
                //TODO:是否发送邮件
            }
        } else {
            //退款、退款退货
            if (OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.REFUND_ING_WAIT_SELLER_DEAL;
            } else if (OrderStatusEnum.WILLSEND_OR_REFUND.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.REFUND_ING_WAIT_BUYER_DEAL;
            } else if (OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode() == goodsOrder.getStatus()) {
                //买家填写订单后,该退款退货订单的状态展现?
                return OrderBizStatusEnum.REFUND_ING_WAIT_SELLER_RECEIVE;
            } else if (OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.REFUND_SUCCESS;
            } else if (OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.REFUND_CLOSE;
            } else if (OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode() == goodsOrder.getStatus()) {
                return OrderBizStatusEnum.REFUND_FAILED;
            } else if (OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode() == goodsOrder.getStatus()) {
                //退款，退货超时，暂时没意义
            } else {
                log.error("该订单出现超出预算以外的状态");
                //TODO:是否发送邮件
            }
        }
        return null;
    }

    @Override
    public Map getOrderGoodsInfo(Long merchantId, Long shopId, Long goodsId, Long skuId, Long userId) {
        GoodsSkuDTO goodsSkuDTO = this.goodsAppService.queryProductSku(merchantId, shopId, goodsId, skuId, userId);
        Map goodsMap = new HashMap();
        if (goodsSkuDTO != null) {
            goodsMap.put("title", goodsSkuDTO.getTitle());
            goodsMap.put("goodsPic", goodsSkuDTO.getPic());
            goodsMap.put("details", goodsSkuDTO.getSkus());
            goodsMap.put("price", goodsSkuDTO.getPrice());
            goodsMap.put("cashback", goodsSkuDTO.getCashback());
        } else {
            return null;
        }
        return goodsMap;
    }

    @Override
    public Map getOrderGoodsInfoBySkuMap(Order order, Map<Long, GoodsSkuDTO> goodsSkuDTOMap) {
        if (goodsSkuDTOMap == null) {
            return new HashMap();
        }
        Map goodsMap = new HashMap();
        GoodsSkuDTO goodsSkuDTO = goodsSkuDTOMap.get(order.getSkuId());
        if (goodsSkuDTO != null) {
            goodsMap.put("title", goodsSkuDTO.getTitle());
            goodsMap.put("goodsPic", goodsSkuDTO.getPic());
            goodsMap.put("details", goodsSkuDTO.getSkus());
            goodsMap.put("price", goodsSkuDTO.getPrice());
            goodsMap.put("cashback", goodsSkuDTO.getCashback());
        } else {
            return null;
        }
        return goodsMap;
    }

    @Override
    public Map<Long, Order> getOldRefundOrderMap(List<Order> goodsOrders) {
        List<Long> oldOrderIds = new ArrayList<>();
        for (Order order : goodsOrders) {
            if (order.getRefundOrderId() != null && order.getRefundOrderId() > 0 && oldOrderIds.indexOf(order.getRefundOrderId()) == -1) {
                oldOrderIds.add(order.getRefundOrderId());
            }
        }
        if (CollectionUtils.isEmpty(oldOrderIds)) {
            return new HashMap<>();
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Order::getId, oldOrderIds).eq(Order::getDel, DelEnum.NO.getCode());
        List<Order> oldOrders = this.goodsOrderRepository.queryList(wrapper);
        if (CollectionUtils.isEmpty(oldOrders)) {
            return new HashMap<>();
        }
        Map<Long, Order> orderMap = new HashMap<>();
        for (Order order : oldOrders) {
            orderMap.put(order.getId(), order);
        }
        return orderMap;
    }

    @Override
    public OrderStatusEnum getOrderStatusFromBizStatus(OrderBizStatusEnum orderBizStatusEnum) {
        if (orderBizStatusEnum == OrderBizStatusEnum.TRADE_SUCCESS) {
            //3:待收货--->确认收货-----> 5
            return OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS;
        } else if (orderBizStatusEnum == OrderBizStatusEnum.TRADE_CLOSE_ORDER_CANCEL) {
            //0待付款->取消订单7
            return OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL;
        } else if (orderBizStatusEnum == OrderBizStatusEnum.REFUND_CLOSE) {
            //9:退款-等待商家处理; 10:退款-等待买家退货; ---> 撤销申请 --> 13
            return OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL;
        }
        return null;
    }

    /***
     * 协商历史
     * @param merchantId
     * @param shopId
     * @param orderId
     * @param userId
     * @return
     */
    @Override
    public List<Map> queryChatHistory(Long merchantId, Long shopId, Long orderId, Long userId) {
        QueryWrapper<OrderReturnsApply> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(OrderReturnsApply::getMerchantId, merchantId)
                .eq(OrderReturnsApply::getShopId, shopId)
                .eq(OrderReturnsApply::getOrderId, orderId)
                .eq(OrderReturnsApply::getUserId, userId)
        ;
        wrapper.orderByAsc("ctime");
        List<OrderReturnsApply> orderReturnsApplies = this.orderReturnsApplyRepository.queryList(wrapper);
        if (CollectionUtils.isEmpty(orderReturnsApplies)) {
            return new ArrayList<>();
        }
        UserResp user = this.userAppService.queryUser(userId);
        if (user == null) {
            return null;
        }
        List<Map> list = new ArrayList<>();
        for (OrderReturnsApply orderReturnsApply : orderReturnsApplies) {
            Map map = new HashMap();
            map.put("time", DateFormatUtils.format(orderReturnsApply.getCtime(), "yyyy-MM-dd HH:mm:ss"));
            map.put("type", OrderReturnsApplyTypeEnum.findByCodeOrderType(orderReturnsApply.getType()).getCode());
            List<String> pics = JSONArray.parseArray(orderReturnsApply.getPic(), String.class);
            if (!CollectionUtils.isEmpty(pics)) {
                map.put("pic", pics.get(0));
            }
            map.put("price", orderReturnsApply.getFee().toString());
            map.put("applyType", orderReturnsApply.getApplyType());
            map.put("status", orderReturnsApply.getStatus());
            map.put("reason", orderReturnsApply.getWhy());
            map.put("note", orderReturnsApply.getNote());
            if (orderReturnsApply.getApplyType() == OrderReturnsApplyApplyTypeEnum.USER.getCode()) {
                map.put("nick", user.getNickName());
            } else {
                map.put("nick", "商家");
            }
            list.add(map);
        }
        return list;
    }

    @Override
    public String getOrderNo(Long shopId) {
        String shopIdstr = String.valueOf(2019 + shopId);
        if (shopIdstr.length() == 4) {
            shopIdstr = 0 + shopIdstr;
        }
        //订单号yyMMddHHmmss1(13位)+ shopid(10000)+2019 +6随机数字组成
        String outTraderNo = DateFormatUtils.format(new Date(), "yyMMddHHmmss") + 1 + shopIdstr + RandomStrUtil.randomNumberStr(6);
        return outTraderNo;
    }

    @Override
    public int getWillPayLeftTimeSecond(Order order) {
        long nomalTimeSecond = 15 * 60;
        long subSecond = (System.currentTimeMillis() - order.getCtime().getTime()) / 1000;
        Long leftSecond = nomalTimeSecond - subSecond;
        if (leftSecond > 0) {
            return leftSecond.intValue();
        }
        return 0;
    }
}
