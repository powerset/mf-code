package mf.code.order.service.templateMsg;

import mf.code.common.WeixinMpConstants;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import org.apache.commons.lang.time.DateFormatUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述:
 *
 * @param:
 * @return:
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2019/4/19 0019 14:45
 */
public class TemplateMessageData {

    /***
     * 退款-等待买家退货
     * @param merchantId
     * @param shopId
     * @param refundOrderId 退款订单的主键
     * @param oldOrderNo
     * @param goodsName
     * @param orderTypeEnum
     * @param refundReason order表的buyerMsg信息
     * @param refundFee
     * @return
     */
    public static Map<String, Object> refundApplyBuyerReturnGoods(Long merchantId, Long shopId, Long refundOrderId,
                                                                  String oldOrderNo, String goodsName, OrderTypeEnum orderTypeEnum,
                                                                  String refundReason, BigDecimal refundFee) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{oldOrderNo, goodsName, orderTypeEnum.getMessage(), refundReason, refundFee, "申请已通过，等待买家退货", "您的申请已通过，请您尽快退货，点击前往"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 21, refundOrderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 退款成功
     * @param merchantId
     * @param shopId
     * @param refundOrderId 退款订单的主键
     * @param oldOrderNo
     * @param goodsName
     * @param orderTypeEnum
     * @param refundReason order表的buyerMsg信息
     * @param refundFee
     * @return
     */
    public static Map<String, Object> refundApplySuccess(Long merchantId, Long shopId, Long refundOrderId,
                                                         String oldOrderNo, String goodsName, OrderTypeEnum orderTypeEnum,
                                                         String refundReason, BigDecimal refundFee) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{oldOrderNo, goodsName, orderTypeEnum.getMessage(), refundReason, refundFee, "退款已成功", "您的退款已成功，前往查看"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 21, refundOrderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 退款拒绝
     * @param merchantId
     * @param shopId
     * @param refundOrderId 退款订单的主键
     * @param oldOrderNo
     * @param goodsName
     * @param orderTypeEnum
     * @param refundReason order表的buyerMsg信息
     * @param refundFee
     * @return
     */
    public static Map<String, Object> refundApplyRefuse(Long merchantId, Long shopId, Long refundOrderId,
                                                        String oldOrderNo, String goodsName, OrderTypeEnum orderTypeEnum,
                                                        String refundReason, BigDecimal refundFee) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{oldOrderNo, goodsName, orderTypeEnum.getMessage(), refundReason, refundFee, "退款已关闭", "卖家拒绝了您的退款申请，前往查看"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 21, refundOrderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    public static Map<String, Object> orderSend(Long merchantId, Long shopId, Long orderId,
                                                String orderNo, String goodsName, String logisticsName, String logisticsNum) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{orderNo, goodsName, logisticsName, logisticsNum, "您的商品很快就飞奔到您手上咯！请注意查收"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 20, orderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 待付款  待支付状态订单，生成订单5分钟后立即推送，一个订单只推送一次
     * @param merchantId
     * @param shopId
     * @param orderId
     * @param orderNo
     * @param goodsName
     * @param fee
     * @return
     */
    public static Map<String, Object> willPay(Long merchantId, Long shopId, Long orderId,
                                              String orderNo, String goodsName, BigDecimal fee) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{orderNo, goodsName, fee, "未支付，即将自动取消", "点击进入订单详情页完成支付"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 20, orderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    public static Map<String, Object> paySuccess(Long merchantId, Long shopId, Long orderId,
                                                 String orderNo, String goodsName, BigDecimal fee,
                                                 OrderPayChannelEnum orderPayChannelEnum, Date paymentTime) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{orderNo, goodsName, fee, orderPayChannelEnum.getMessage(), DateFormatUtils.format(paymentTime, "yyyy-MM-dd HH:mm:ss"),
                "我们会尽快发货，请敬候！如有疑问请进入小程序联系客服，祝您生活愉快"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 20, orderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /**
     * 超时
     * <p>
     * ②订单超时已取消：超过15分钟未支付，系统自动取消
     **/
    public static Map<String, Object> orderTimeoutCancel(Long merchantId, Long shopId, Long orderId,
                                                         String orderNo, String goodsName, BigDecimal fee) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{orderNo, goodsName, fee, "订单超时已取消", "点击立即查看"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 20, orderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /**
     * 商家拒绝发货
     * <p>
     * ③商家库存不足已取消：商家
     **/
    public static Map<String, Object> orderMerchantRefuseCancel(Long merchantId, Long shopId, Long orderId,
                                                                String orderNo, String goodsName, BigDecimal fee) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{orderNo, goodsName, fee, "商家库存不足已取消", "点击立即查看"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 20, orderId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 本人的 订单返现到账
     * @param cashBack 返现余额
     * @return
     */
    public static Map<String, Object> orderCashBackByMyself(Long merchantId, Long shopId,
                                                            BigDecimal cashBack) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{cashBack, "您的订单，今日购买待返现", "以上是您今日在店铺名称购买获得的待结算金额，确认收货7天后在每月20号即可返现到钱包"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 23, 0L));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 本人的 所有上级 订单返现到账
     * @param cashBack 返现余额
     * @return  其中 from ：前端交互 23 跳推广收益
     */
    public static Map<String, Object> orderCashBackByMaster(Long merchantId, Long shopId,
                                                            BigDecimal cashBack) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{cashBack, "团员订单，今日购买待返现", "以上是您的团员今日在店铺名称购买获得的待结算金额，确认收货7天后在每月20号即可返现到钱包"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 23, 0L));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /********************************私有方法******************************************/
    private static Map<String, Object> getTempteDate(int keywordNum, Object[] objects) {
        Map<String, Object> m = new HashMap<>();
        for (int i = 0; i < keywordNum; i++) {
            Map map = new HashMap();
            String key = "keyword" + (i + 1);
            String value = objects[i].toString();
            map.put("value", value);
            m.put(key, map);
        }
        return m;
    }

    private static String getPageUrl(Long merchantId,
                                     Long shopId,
                                     int from,
                                     Long orderId) {
        //source--推送消息来源
        String result = "source=1&merchantId=" + String.valueOf(merchantId) +
                "&shopId=" + String.valueOf(shopId) +
                "&from=" + String.valueOf(from) +
                "&orderId=" + String.valueOf(orderId);
        return result;
    }
}
