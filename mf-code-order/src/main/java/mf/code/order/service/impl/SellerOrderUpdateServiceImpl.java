package mf.code.order.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.Assert;
import mf.code.order.dto.*;
import mf.code.order.feignapi.constant.OrderReturnsApplyStatusEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.service.OrderStatusService;
import mf.code.order.service.SellerOrderQueryService;
import mf.code.order.service.SellerOrderUpdateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * mf.code.order.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:38
 */
@Slf4j
@Service
public class SellerOrderUpdateServiceImpl implements SellerOrderUpdateService {
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private SellerOrderQueryService sellerOrderQueryService;
    @Autowired
    private OrderStatusService orderStatusService;

    /**
     * 卖家更新订单备注
     *
     * @param reqDTO
     * @return
     */
    @Override
    public Integer updateSellerMsg(SellerOrderUpdateMsgReqDTO reqDTO) {
        Assert.isBlank(reqDTO.getSellerMsg(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请填写备注信息");
        Assert.isInteger(reqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "店铺编号错误");
        Assert.isBlank(reqDTO.getOrderNo(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "订单编号错误");

        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        Assert.notNull(order, ApiStatusEnum.ERROR_BUS_NO14.getCode(), "订单编号错误");
        Assert.isTrue(order.getGoodsShopId().toString().equals(reqDTO.getShopId()), ApiStatusEnum.ERROR_BUS_NO15.getCode(), "您无权修改该订单");
        Order updateOrder = new Order();
        updateOrder.setId(order.getId());
        updateOrder.setSellerMsg(reqDTO.getSellerMsg());
        updateOrder.setUtime(new Date());
        return goodsOrderRepository.updateSelective(updateOrder);
    }

    /**
     * 卖家发货
     *
     * @param reqDTO
     * @return
     */
    @Override
    public Integer sendProduct(SellerSendProductReqDTO reqDTO) {
        Assert.isInteger(reqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "店铺编号错误");
        Assert.isBlank(reqDTO.getOrderNo(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "订单编号错误");
        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        Assert.notNull(order, ApiStatusEnum.ERROR_BUS_NO14.getCode(), "订单编号错误");
        // 发货只有商品归属者才可以
        Assert.isTrue(order.getGoodsShopId().toString().equals(reqDTO.getShopId()), ApiStatusEnum.ERROR_BUS_NO15.getCode(), "您无权修改该订单");
        Assert.isTrue(OrderStatusEnum.WILLSEND_OR_REFUND.getCode() == order.getStatus(), ApiStatusEnum.ERROR_BUS_NO16.getCode(), "该订单状态不是待发货状态");
        SellerLogisticsCompanyResultDTO logisticsCompanyResultDTO = sellerOrderQueryService.getLogisticsCompany(new SellerOrderCommonReqDTO());
        List<LogisticsCompany> logisticsCompanyList = logisticsCompanyResultDTO.getLogisticsCompanyList();
        String name = "";
        for (LogisticsCompany company : logisticsCompanyList) {
            if (company.getCode().equals(reqDTO.getLogisticsCode())) {
                name = company.getName();
            }
        }
        order.setUtime(new Date());
        int count = orderStatusService.sellerFillExpress(order.getId(), name, reqDTO.getLogisticsNum(), reqDTO.getLogisticsCode());
        Assert.isTrue(count != 0, ApiStatusEnum.ERROR_BUS_NO17.getCode(), "发货处理失败");
        return count;
    }

    /**
     * 卖家处理退款申请
     *
     * @param reqDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer auditRefund(SellerAuditRefundReqDTO reqDTO) {
        Assert.isInteger(reqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "店铺编号错误");
        Assert.isNumber(reqDTO.getAuditStatus(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "审核状态错误");
        Assert.isBlank(reqDTO.getOrderNo(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "订单编号错误");
        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        Assert.notNull(order, ApiStatusEnum.ERROR_BUS_NO14.getCode(), "订单编号错误");
        List<Order> refundOrderList = goodsOrderRepository.selectRefundByPayOrderNo(reqDTO.getOrderNo());
        Assert.isTrue(!CollectionUtils.isEmpty(refundOrderList), ApiStatusEnum.ERROR_BUS_NO15.getCode(), "暂无退款订单");
        Assert.isTrue(order.getGoodsShopId().toString().equals(reqDTO.getShopId()), ApiStatusEnum.ERROR_BUS_NO16.getCode(), "您无权修改该订单");
        int count = 0;
        // 暂时没有好的方式查询，所以遍历
        for (Order refundOrder : refundOrderList) {
            // 只有退款申请才会执行
            if (refundOrder.getStatus() != OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
                continue;
            }
            refundOrder.setSellerMsg(reqDTO.getAuditReason());
            if (Integer.valueOf(reqDTO.getAuditStatus()) == OrderReturnsApplyStatusEnum.PASS.getCode()) {
                count = orderStatusService.sellerAgreeRefund(refundOrder, reqDTO.getAuditPic());
            } else {
                count = orderStatusService.sellerRefuseRefund(refundOrder, reqDTO.getAuditPic());
            }
        }
        Assert.isTrue(count != 0, ApiStatusEnum.ERROR_BUS_NO17.getCode(), "处理退款申请失败");
        return count;
    }

    /**
     * 卖家取消订单
     *
     * @param reqDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer cancelOrder(SellerOrderCancelReqDTO reqDTO) {
        Assert.isTrue(StringUtils.isNoneBlank(reqDTO.getReason()),ApiStatusEnum.ERROR_BUS_NO14.getCode(), "请填写拒绝理由");
        Assert.isInteger(reqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "店铺编号错误");
        Assert.isBlank(reqDTO.getOrderNo(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "订单编号错误");
        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        Assert.notNull(order, ApiStatusEnum.ERROR_BUS_NO14.getCode(), "订单编号错误");
        Assert.isTrue(order.getGoodsShopId().toString().equals(reqDTO.getShopId()), ApiStatusEnum.ERROR_BUS_NO15.getCode(), "您无权修改该订单");
        Assert.isTrue(OrderStatusEnum.WILLSEND_OR_REFUND.getCode() == order.getStatus(), ApiStatusEnum.ERROR_BUS_NO16.getCode(), "该订单状态不是待发货状态");
        order.setSellerMsg(reqDTO.getReason());
        order.setUtime(new Date());
        int count = orderStatusService.sellerRefuseSend(order);
        Assert.isTrue(count != 0, ApiStatusEnum.ERROR_BUS_NO16.getCode(), "订单取消失败");
        return count;
    }

    /**
     * 卖家确认收货
     *
     * @param reqDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer confirmRefund(SellerRefundConfirmReqDTO reqDTO) {
        Assert.isInteger(reqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "店铺编号错误");
        Assert.isBlank(reqDTO.getOrderNo(), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "订单编号错误");
        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        List<Order> orderList = goodsOrderRepository.selectRefundByPayOrderNo(reqDTO.getOrderNo());
        Assert.isTrue(!CollectionUtils.isEmpty(orderList), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "订单编号错误");
        // 这里判断错误，确认收货只有商品归属者才可以
        Assert.isTrue(order.getGoodsShopId().toString().equals(reqDTO.getShopId()), ApiStatusEnum.ERROR_BUS_NO15.getCode(), "您无权修改该订单");
        int count = 0;
        // 暂时没有好的方式查询，所以遍历
        for (Order refundOrder : orderList) {
            if (refundOrder.getStatus() != OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()) {
                continue;
            }
            refundOrder.setUtime(new Date());
            count = orderStatusService.sellerConfirmReceipt(refundOrder);
            Assert.isTrue(count != 0, ApiStatusEnum.ERROR_BUS_NO16.getCode(), "确认收货失败，请稍后重试");
        }
        return count;
    }
}
