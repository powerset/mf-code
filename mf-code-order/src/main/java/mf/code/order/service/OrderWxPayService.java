package mf.code.order.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.repo.po.Order;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;

public interface OrderWxPayService {
    /***
     * wx支付
     * @param goodsOrder 订单类
     * @param req 请求体
     * @param openId 用户openid
     * @return
     */
    SimpleResponse wxPayType(Order goodsOrder, GoodsOrderReq req, String openId, GoodProductDTO goodProductDTO);

    /***
     * wx支付回调
     * @param orderNo 订单编号
     * @param transactionId 交易流水
     * @param totalFee 支付金额
     * @param paymentTime 支付时间
     * @param payOpenId 支付人标识
     * @return
     */
    String wxPayCallback(String orderNo, String transactionId, BigDecimal totalFee, Date paymentTime, String payOpenId);

    /***
     * wx退款回调
     * @param orderNo 原订单编号
     * @param refundOrderNo 退款订单编号
     * @param totalFee 申请退款退款金额
     * @param settlementRefundFee 实际退款金额
     * @param transactionId 退款流水
     * @return
     */
    String wxRefundCallback(String orderNo, String refundOrderNo, BigDecimal totalFee, BigDecimal settlementRefundFee, String transactionId);

    /***
     * wx退款
     * @param oldOrderNo 原订单编号
     * @param oldOrderFee 原订单价格
     * @param refundOrderNo 退款订单编号
     * @param refundFee 退款订单价格
     * @param refundDesc 退款描述
     * @return
     */
    SimpleResponse wxRefund(String oldOrderNo, BigDecimal oldOrderFee, String refundOrderNo, BigDecimal refundFee, String refundDesc);
}
