package mf.code.order.service.templateMsg;

import mf.code.user.dto.UserResp;

import java.util.Map;

/**
 * 功能描述:
 * 微信消息模板
 *
 * @param:
 * @return:
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2019/4/19 0019 16:58
 */
public interface OrderTemplateMessageService {

    /**
     * 通过传入的模板消息标题 发送通知
     */
    void sendTemplateMessage(String templateId, UserResp user, Map<String, Object> data);

    void sendTemplateMessage(String templateId, Long userId, Map<String, Object> data);
}
