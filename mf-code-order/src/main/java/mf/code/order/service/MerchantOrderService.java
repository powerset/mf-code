package mf.code.order.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.merchant.constants.MerchantOrderTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.order.dto.PlatformCashDTO;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.order.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月06日 20:04
 */
public interface MerchantOrderService {
    /***
     * 财务管理-商家提现列表
     * @param platformId
     * @return
     */
    SimpleResponse getCashLog(String platformId, String phone, String shopName, String start, String end, Integer limit, Integer pageNum);

    /***
     * 财务管理-商家余额
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse getBalance(Long merchantId, Long shopId);

    /***
     * 根据商户账号查询旗下的所有未删除的店铺
     * @param phone
     * @return 店铺集合
     */
    SimpleResponse getShopListByMerchantPhone(String platformId, String phone);

    /***
     * 运营平台人员-提交提现记录
     * @param platformCashDTO
     * @return
     */
    SimpleResponse commitCashRecord(PlatformCashDTO platformCashDTO);

    /**
     * 店铺总佣金
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    BigDecimal commissionTotal(Long shopId, Date begin, Date end);

    /**
     * 店铺待结算佣金
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    BigDecimal commissionWaitClose(Long shopId, Date begin, Date end);

    /**
     * 店铺佣金收入列表
     *
     * @param shopId                店铺
     * @param mfTradeTypeEnums      结算类型 4商品售出待结算5商品售出已结算
     * @param merchantOrderTypeEnum 订单类型 2商户-存钱账户 3商户退款
     * @param begin                 开始时间
     * @param end                   结束时间
     * @return List<MerchantOrder>
     */
    List<MerchantOrder> getMerchantOrderListByConditions(Long shopId,
                                                         List<MfTradeTypeEnum> mfTradeTypeEnums,
                                                         MerchantOrderTypeEnum merchantOrderTypeEnum,
                                                         Date begin, Date end);



}
