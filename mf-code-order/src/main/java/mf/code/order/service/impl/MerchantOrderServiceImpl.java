package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.MybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.merchant.constants.*;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.ShopAppService;
import mf.code.order.common.caller.wxpay.WxpayProperty;
import mf.code.order.dto.PlatformCashDTO;
import mf.code.order.dto.PlatformMerchantOrderCashDTO;
import mf.code.order.dto.PlatformMerchantShopBalanceDTO;
import mf.code.order.repo.po.Merchant;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.repo.repository.MerchantRepository;
import mf.code.order.service.MerchantOrderService;
import mf.code.order.service.OrderService;
import mf.code.shop.dto.ShopDetail;
import mf.code.shop.dto.ShopListResultDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.order.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月06日 20:04
 */
@Service
@Slf4j
public class MerchantOrderServiceImpl implements MerchantOrderService {
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Value("${merchant.canCash}")
    private String merchantCanCashMinPrice;


    /***
     * 财务管理-商家提现列表
     * @param platformId
     * @return
     */
    @Override
    public SimpleResponse getCashLog(String platformId, String phone, String shopName, String start, String end, Integer limit, Integer pageNum) {
        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isBlank(start) || StringUtils.isBlank(end)) {
            Date endDay = new Date();
            startTime = DateUtils.addDays(endDay, -30);
            endTime = DateUtil.getDateBeforeZero(endDay);
        } else {
            startTime = DateUtil.parseDate(start, null, "yyyy-MM-dd");
            endTime = DateUtil.getDateBeforeZero(DateUtil.parseDate(end, null, "yyyy-MM-dd"));
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        // 分页查询 排序方式
        Page<MerchantOrder> page = new Page<>(pageNum, limit);
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .eq(MerchantOrder::getType, OrderTypeEnum.USERCASH.getCode())
                .between(MerchantOrder::getCtime, startTime, endTime)
        ;
        MybatisPageDto<PlatformMerchantOrderCashDTO> mybatisPageDto = new MybatisPageDto<>(pageNum, limit, 0, 0);
        simpleResponse.setData(mybatisPageDto);
        if (StringUtils.isNotBlank(phone)) {
            Merchant merchant = this.merchantRepository.getbyPhone(phone);
            if (merchant == null) {
                return simpleResponse;
            } else {
                wrapper.and(params -> params.eq("merchant_id", merchant.getId()));
            }
        }
        if (StringUtils.isNotBlank(shopName)) {
            wrapper.and(params -> params.eq("shop_name", shopName));
        }

        wrapper.orderByDesc("ctime");
        IPage<MerchantOrder> merchantOrderIPage = this.merchantOrderRepository.page(page, wrapper);
        List<MerchantOrder> merchantOrderList = merchantOrderIPage.getRecords();
        if (CollectionUtils.isEmpty(merchantOrderList)) {
            return simpleResponse;
        }

        //根据merchantOrder获取商户
        Map<Long, Merchant> merchantMap = merchantRepository.getMerchantByMerchantOrder(merchantOrderList);

        for (MerchantOrder merchantOrder : merchantOrderList) {
            PlatformMerchantOrderCashDTO dto = new PlatformMerchantOrderCashDTO();
            dto.setPhone("");
            dto.setPics(new ArrayList<>());
            Merchant merchant = merchantMap.get(merchantOrder.getMerchantId());
            if (merchant != null) {
                dto.setPhone(merchant.getPhone());
            }
            if (StringUtils.isNotBlank(merchantOrder.getReqJson())) {
                List<String> pics = JSON.parseArray(merchantOrder.getReqJson(), String.class);
                dto.setPics(pics);
            }
            dto.setCtime(merchantOrder.getCtime());
            //线下提现，remark用作处理人备注
            dto.setDealUser(merchantOrder.getRemark());
            dto.setFee(merchantOrder.getTotalFee().toString());
            dto.setId(merchantOrder.getId());
            dto.setShopName(merchantOrder.getShopName());
            mybatisPageDto.getContent().add(dto);
        }

        mybatisPageDto.from(merchantOrderIPage.getCurrent(), merchantOrderIPage.getSize(), merchantOrderIPage.getTotal(), merchantOrderIPage.getPages());
        simpleResponse.setData(mybatisPageDto);
        return simpleResponse;
    }

    /***
     * 财务管理-商家余额
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse getBalance(Long merchantId, Long shopId) {
        Assert.isTrue(merchantId != null && merchantId > 0 && shopId != null && shopId > 0, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入正确的商户号和店铺号");
        Long parentMerchantId = this.merchantRepository.getParentMerchantId(merchantId);
        QueryWrapper<MerchantBalance> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantBalance::getMerchantId, parentMerchantId)
                .eq(MerchantBalance::getCustomerId, merchantId)
                .eq(MerchantBalance::getShopId, shopId)
        ;

        SimpleResponse simpleResponse = new SimpleResponse();
        Map map = new HashMap<>();
        map.put("balance", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());

        MerchantBalance merchantBalance = this.merchantBalanceRepository.getOne(wrapper);
        if (merchantBalance != null) {
            map.put("balance", merchantBalance.getDepositAdvance());
        }
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 根据商户账号查询旗下的所有未删除的店铺
     * @param phone
     * @return 店铺集合
     */
    @Override
    public SimpleResponse getShopListByMerchantPhone(String platformId, String phone) {
        Assert.isTrue(StringUtils.isNotBlank(phone), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "商户号入参异常");
        Assert.isTrue(RegexUtils.isMobileSimple(phone), ApiStatusEnum.ERROR_BUS_NO2.getCode(), "商户号请准确");
        Merchant merchant = this.merchantRepository.getbyPhone(phone);
        Assert.isTrue(merchant != null, ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该商户号不存在");

        ShopListResultDTO merchantIdByShopList = shopAppService.getMerchantIdByShopList(merchant.getId());
        Assert.isTrue(merchantIdByShopList != null && !CollectionUtils.isEmpty(merchantIdByShopList.getShopList()), ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该商户下未绑定店铺or获取店铺异常，请自己确认并和技术人员确认");

        Long parentMerchantId = this.merchantRepository.getParentMerchantId(merchant.getId());
        QueryWrapper<MerchantBalance> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantBalance::getMerchantId, parentMerchantId)
        ;
        List<MerchantBalance> merchantBalances = merchantBalanceRepository.list(wrapper);
        Map<Long, MerchantBalance> merchantBalanceMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(merchantBalances)) {
            for (MerchantBalance merchantBalance : merchantBalances) {
                merchantBalanceMap.put(merchantBalance.getShopId(), merchantBalance);
            }
        }

        PlatformMerchantShopBalanceDTO resp = new PlatformMerchantShopBalanceDTO();
        resp.setShops(new ArrayList<>());

        for (ShopDetail shopDetail : merchantIdByShopList.getShopList()) {
            PlatformMerchantShopBalanceDTO.PlatformMerchantShopBalance balance = new PlatformMerchantShopBalanceDTO.PlatformMerchantShopBalance();
            balance.setBalance(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            MerchantBalance merchantBalance = merchantBalanceMap.get(shopDetail.getShopId());
            if (merchantBalance != null) {
                balance.setBalance(merchantBalance.getDepositAdvance().toString());
            }
            balance.setShopId(shopDetail.getShopId());
            balance.setShopName(shopDetail.getCustomShopName());
            balance.setMerchantId(merchant.getId());
            resp.getShops().add(balance);
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 运营平台人员-提交提现记录
     * @param platformCashDTO
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse commitCashRecord(PlatformCashDTO platformCashDTO) {
        log.info("<<<<<<<< dto:{}", platformCashDTO);
        List<String> pics = JSONArray.parseArray(platformCashDTO.getPics(), String.class);
        Assert.isTrue(platformCashDTO.checkValid(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        //获取主账号的商户编号
        Long parentMerchantId = this.merchantRepository.getParentMerchantId(platformCashDTO.getMerchantId());
        QueryWrapper<MerchantBalance> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantBalance::getMerchantId, parentMerchantId)
                .eq(MerchantBalance::getCustomerId, platformCashDTO.getMerchantId())
                .eq(MerchantBalance::getShopId, platformCashDTO.getShopId())
        ;
        MerchantBalance merchantBalance = this.merchantBalanceRepository.getOne(wrapper);
        Assert.isTrue(merchantBalance != null && merchantBalance.getDepositAdvance().subtract(new BigDecimal(merchantCanCashMinPrice)).compareTo(BigDecimal.ZERO) > 0, ApiStatusEnum.ERROR_BUS_NO3.getCode(), "可提现金额不能低于500元");
        Assert.isTrue(merchantBalance.getDepositAdvance().subtract(new BigDecimal(platformCashDTO.getBalance())).compareTo(new BigDecimal(merchantCanCashMinPrice)) >= 0, ApiStatusEnum.ERROR_BUS_NO4.getCode(), "提完现之后，保证金低于500元");

        //订单表的创建
        String orderNo = "V2" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        Map<String, Object> map = this.shopAppService.queryShopInfoForSeller(String.valueOf(platformCashDTO.getShopId()));
        Assert.isTrue(map != null && map.get("shopName") != null, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "获取店铺信息异常,请联系@夜辰");
        String shopName = map.get("shopName").toString();
        String orderName = shopName + "发起提现";

        MerchantOrder merchantOrder = WxCallbackAboutDTO.addMerchantOrderByOfflineCash(platformCashDTO.getMerchantId(), platformCashDTO.getShopId(),
                shopName, orderName, orderNo, platformCashDTO.getBalance(), platformCashDTO.getPlatformUser(), wxpayProperty.getPubAppId(), pics);
        log.info("<<<<<<<< merchantOrder:{}", JSONObject.toJSONString(merchantOrder));
        int i1 = merchantOrderRepository.insertSelective(merchantOrder);
        Assert.isTrue(i1 > 0, ApiStatusEnum.ERROR_BUS_NO6.getCode(), "插入商户余额表异常");
        //余额更新
        int i = merchantBalanceRepository.lessUpdateByVersionId(merchantBalance, new BigDecimal(platformCashDTO.getBalance()), BigDecimal.ZERO);
        if (i == 0) {
            log.error("<<<<<<<<余额更新失败, req:{}, mechantBalance:{}", platformCashDTO, merchantBalance);
        }
        Assert.isTrue(i > 0, ApiStatusEnum.ERROR_BUS_NO5.getCode(), "余额更新失败");

        return new SimpleResponse();
    }

    /**
     * 获取店铺的累计佣金 抖音v1 gbf
     *
     * @param shopId 店铺id
     * @param begin  开始时间 如不以时间筛选,begin=null
     * @param end    结束时间 同begin
     * @return 返回累计佣金
     */
    @Override
    public BigDecimal commissionTotal(Long shopId, Date begin, Date end) {
        boolean getYesterdayData = false;
        String redisKey = "dy:ddd:mp:commission:" + shopId + ":";
        if (begin != null && end != null) {
            String dateStrBeg = DateUtil.dateToString(begin, DateUtil.getYYYYMMDD());
            String dateStrEnd = DateUtil.dateToString(end, DateUtil.getYYYYMMDD());
            if (dateStrBeg.equals(dateStrEnd)) {
                getYesterdayData = true;
                redisKey = redisKey + dateStrBeg;
                String redisCacheCommissionTotal = stringRedisTemplate.opsForValue().get(redisKey);
                if (redisCacheCommissionTotal != null) {
                    try {
                        return new BigDecimal(redisCacheCommissionTotal);
                    } catch (Exception e) {
                        log.error("<<<<<<<<exception:{}", e);
                    }
                }
            }
        }
        List<MfTradeTypeEnum> mfTradeTypeEnumList = new ArrayList<>();
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING);
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_CLEANINGED);
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_CLEANING_INTO_BALANCE);

        //总收入
        List<MerchantOrder> merchantOrderIncome = this.getMerchantOrderListByConditions(shopId, mfTradeTypeEnumList, MerchantOrderTypeEnum.MERCHANT_INCOME, begin, end);
        BigDecimal totalIncome = this.calcTotalfeeByMerchantOrderList(merchantOrderIncome);

        //退款
        mfTradeTypeEnumList.clear();
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_REFUND);
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS);
        List<MerchantOrder> merchantorderRefund = this.getMerchantOrderListByConditions(shopId, mfTradeTypeEnumList, MerchantOrderTypeEnum.MERCHANT_PAY, begin, end);
        BigDecimal totalRefund = this.calcTotalfeeByMerchantOrderList(merchantorderRefund);
        BigDecimal result = totalIncome.subtract(totalRefund);
        if (getYesterdayData) {
            Long secondDiff = DateUtil.secondDiff(new Date(), DateUtil.getDateBeforeZero(new Date()));
            stringRedisTemplate.opsForValue().set(redisKey, result.toString(), secondDiff, TimeUnit.SECONDS);
        }
        return result;
    }

    /**
     * 待结算佣金
     *
     * @param shopId 店铺id
     * @param begin  开始时间 如不以时间筛选,begin=null
     * @param end    结束时间 同begin
     * @return 返回待结算佣金
     */
    @Override
    public BigDecimal commissionWaitClose(Long shopId, Date begin, Date end) {
        // TODO: 2019/8/10 0010  redis缓存昨日数据
        List<MfTradeTypeEnum> mfTradeTypeEnumList = new ArrayList<>();
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING);
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_CLEANINGED);
        //总收入
        List<MerchantOrder> merchantOrderIncome = this.getMerchantOrderListByConditions(shopId, mfTradeTypeEnumList, MerchantOrderTypeEnum.MERCHANT_INCOME, begin, end);
        BigDecimal totalIncome = this.calcTotalfeeByMerchantOrderList(merchantOrderIncome);

        //退款
        mfTradeTypeEnumList.clear();
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_REFUND);
        mfTradeTypeEnumList.add(MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS);
        List<MerchantOrder> merchantorderRefund = this.getMerchantOrderListByConditions(shopId, mfTradeTypeEnumList, MerchantOrderTypeEnum.MERCHANT_PAY, begin, end);
        BigDecimal totalRefund = this.calcTotalfeeByMerchantOrderList(merchantorderRefund);
        return totalIncome.subtract(totalRefund);
    }


    /**
     * 通过List<MerchantOrder> 获取 总金额
     *
     * @param list
     * @return
     */
    private BigDecimal calcTotalfeeByMerchantOrderList(List<MerchantOrder> list) {
        BigDecimal result = new BigDecimal(0);
        if (CollectionUtils.isEmpty(list)) {
            return result;
        }
        for (MerchantOrder merchantOrder : list) {
            result = result.add(merchantOrder.getTotalFee());
        }
        return result;
    }

    /**
     * 店铺佣金收入列表 抖音v1 gbf
     *
     * @param shopId                店铺
     * @param mfTradeTypeEnums      结算类型 4商品售出待结算5商品售出已结算
     * @param merchantOrderTypeEnum 订单类型 2商户-存钱账户 3商户退款
     * @return
     */
    private List<MerchantOrder> getMerchantOrderListByConditions(Long shopId,
                                                                 List<MfTradeTypeEnum> mfTradeTypeEnums,
                                                                 MerchantOrderTypeEnum merchantOrderTypeEnum) {
        return this.getMerchantOrderListByConditions(shopId, mfTradeTypeEnums, merchantOrderTypeEnum, null, null);
    }

    /**
     * 店铺佣金收入列表 抖音v1 gbf
     *
     * @param shopId                店铺
     * @param mfTradeTypeEnums      结算类型 4商品售出待结算5商品售出已结算
     * @param merchantOrderTypeEnum 订单类型 2商户-存钱账户 3商户退款
     * @param begin                 开始时间
     * @param end                   结束时间
     * @return List<MerchantOrder>
     */
    @Override
    public List<MerchantOrder> getMerchantOrderListByConditions(Long shopId,
                                                                List<MfTradeTypeEnum> mfTradeTypeEnums,
                                                                MerchantOrderTypeEnum merchantOrderTypeEnum,
                                                                Date begin, Date end) {
        QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getStatus, MerchantOrderStatusEnum.ORDERED.getCode())
                .eq(MerchantOrder::getBizType,BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode())
                .eq(MerchantOrder::getType, merchantOrderTypeEnum.getCode());

        if (mfTradeTypeEnums != null) {
            List<Integer> mfTradeTypeList = new ArrayList<>();
            for (MfTradeTypeEnum e : mfTradeTypeEnums) {
                mfTradeTypeList.add(e.getCode());
            }
            if (mfTradeTypeList.size() > 0) {
                queryWrapper.lambda().in(MerchantOrder::getMfTradeType, mfTradeTypeList);
            }
        }
        if (begin != null) {
            queryWrapper.lambda().ge(MerchantOrder::getPaymentTime, begin);
        }
        if (end != null) {
            queryWrapper.lambda().le(MerchantOrder::getPaymentTime, end);
        }
        return merchantOrderRepository.list(queryWrapper);
    }
}
