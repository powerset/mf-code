package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.AppPayReq;
import mf.code.alipay.RequestRefundReq;
import mf.code.alipay.ResponseRefundDTO;
import mf.code.alipay.ResponseTradeAppPayDTO;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.MD5Util;
import mf.code.common.utils.SortUtil;
import mf.code.douyin.DouyinPayReq;
import mf.code.douyin.DouyinPaySignDTO;
import mf.code.douyin.constant.DouyinPaySceneEnum;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.api.feignclient.CommAppService;
import mf.code.order.api.feignclient.DistributionAppService;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.repo.po.Order;
import mf.code.order.service.OrderAboutService;
import mf.code.order.service.OrderAlipayService;
import mf.code.order.service.OrderCallBackService;
import mf.code.order.service.OrderStatusService;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.order.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月10日 10:09
 */
@Service
@Slf4j
public class OrderAlipayServiceImpl implements OrderAlipayService {
    @Autowired
    private CommAppService commAppService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private OrderAboutService orderAboutService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private OrderCallBackService orderCallBackService;

    /***
     * 抖音-alipay-app支付方式
     *
     * @param goodsOrder
     * @param user
     * @param goodProductDTO
     * @return
     */
    @Override
    public SimpleResponse douyinAlipayAppType(Order goodsOrder, UserResp user, GoodProductDTO goodProductDTO, GoodsOrderReq req) {
        String title = goodProductDTO.getTitle();
        goodsOrder.setSource(OrderSourceEnum.DOUYIN_SHOP.getCode());
        Map param = new HashMap<>();
        int payScene = DouyinPaySceneEnum.DOU_SHOP.getCode();
        param.put("payscene", DouyinPaySceneEnum.DOU_SHOP.getCode());
        String openId = user.getOpenId();
        if (user.getRole() == UserRoleEnum.DOU_DAI_DAI.getCode()) {
            param.put("payscene", DouyinPaySceneEnum.DOU_SHOP_MERCHANT.getCode());
            payScene = DouyinPaySceneEnum.DOU_SHOP_MERCHANT.getCode();
            goodsOrder.setSource(OrderSourceEnum.DOUYIN_SHOP_MANAGER.getCode());

            String[] s1 = user.getOpenId().split("_");
            if (s1.length < 2) {
                log.error("<<<<<<<<<抖音支付 openid获取异常： openId:{}", user.getOpenId());
            }
            openId = user.getOpenId().split("_")[1];
        }

        //判断下单是否是抖带带取样品下单操作，若是，则按最低价卖：去除佣金后
        if (goodsOrder.getSource() == OrderSourceEnum.DOUYIN_SHOP_MANAGER.getCode()) {
            String platformDistRateCommission = goodsAppService.getPlatformDistRateCommission(goodsOrder.getMerchantId(), goodsOrder.getShopId(),
                    goodsOrder.getSkuId());
            log.error("<<<<<<<<获取供应商商品经过平台分佣之后的售价和比例,resp:{}", platformDistRateCommission);
            if (StringUtils.isNotBlank(platformDistRateCommission)) {
                //获取购买的件数，得到总减免金额
                BigDecimal lessTotalMoney = new BigDecimal(platformDistRateCommission).multiply(new BigDecimal(String.valueOf(goodsOrder.getNumber())));
                goodsOrder.setFee(goodsOrder.getFee().subtract(lessTotalMoney));
            }
        }
        goodsOrder.setReqJson("");
        orderStatusService.buyerPurchaseing(goodsOrder, goodProductDTO, null);

        //扣库存
        SimpleResponse simpleResponse1 = goodsAppService.consumeStock(goodsOrder.getSkuId(), goodsOrder.getNumber(), goodsOrder.getId());
        if (simpleResponse1.error()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "商品库存出现异常，亲，重试下哦");
        }

        BigDecimal totalAmount = goodsOrder.getFee().setScale(2, BigDecimal.ROUND_DOWN);
        long amount = totalAmount.multiply(new BigDecimal(100)).longValue();

        //先获取-抖音订单号
        DouyinPayReq douyinPayReq = new DouyinPayReq();
        douyinPayReq.setIp(goodsOrder.getIpAddress());
        douyinPayReq.setUid(openId);
        douyinPayReq.setOutOrderNo(goodsOrder.getOrderNo());
        douyinPayReq.setBody(title);
        douyinPayReq.setSubject(title);
        douyinPayReq.setTotalAmount(amount);

        douyinPayReq.setExtParam(JSONObject.toJSONString(param));
        Map douyinPayRespMap = commAppService.douyinPay(douyinPayReq);
        log.info("<<<<<<<<douyin pay resp:{}", douyinPayRespMap);
        if (douyinPayRespMap == null || douyinPayRespMap.get("trade_no") == null) {
            log.error("<<<<<<<<<抖音支付 创建订单数据异常： douyinPayRespMap:{}", douyinPayRespMap);
            //退库存
            SimpleResponse simpleResponse = this.goodsAppService.giveBackStock(goodsOrder.getSkuId(), goodsOrder.getNumber(), goodsOrder.getId());
            if (simpleResponse.error()) {
                log.error("<<<<<<<待付款定时器-退库存-异常002：orderId:{}", goodsOrder.getId());
            }
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "抖音支付 返回数据无支付单号");
        }
        //抖音支付单号
        String douyinTradeNo = douyinPayRespMap.get("trade_no").toString();

        //alipay支付
        AppPayReq appPayReq = new AppPayReq();
        appPayReq.setOutTradeNo(goodsOrder.getOrderNo());
        appPayReq.setTotalAmount(totalAmount.toString());
        appPayReq.setSubject(title);
        appPayReq.setGoodsType("1");
        appPayReq.setTimeoutExpress("15m");
        try {
            appPayReq.setPassbackParams(URLEncoder.encode(JSONObject.toJSONString(param), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("<<<<<<<< urlEncode exception:{}", e);
        }
        ResponseTradeAppPayDTO responseTradeAppPayDTO = commAppService.appAlipay(appPayReq);
        log.info("<<<<<<<<alipay resp:{}", responseTradeAppPayDTO);
        if (responseTradeAppPayDTO == null || StringUtils.isBlank(responseTradeAppPayDTO.getBody())) {
            log.error("<<<<<<<<<支付宝支付 支付数据返回异常： responseTradeAppPayDTO:{}", responseTradeAppPayDTO);
            //退库存
            SimpleResponse simpleResponse = this.goodsAppService.giveBackStock(goodsOrder.getSkuId(), goodsOrder.getNumber(), goodsOrder.getId());
            if (simpleResponse.error()) {
                log.error("<<<<<<<待付款定时器-退库存-异常002：orderId:{}", goodsOrder.getId());
            }
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "支付宝支付 支付数据返回异常");
        }

        /**拼装数据返回**/
        Map map = commAppService.douyinPayScene(payScene);
        if (map == null) {
            log.error("<<<<<<<查询支付分配的appid异常-异常002：resp:{}", map);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查询支付分配的appid异常");
        }
        String appId = map.get("appId").toString();
        String merchantId = map.get("merchantId").toString();
        String appSecret = map.get("appSecret").toString();

        String timestamp = System.currentTimeMillis() + "";
        Map paramsUrl = new HashMap();
        paramsUrl.put("url", responseTradeAppPayDTO.getBody());
        String alipayParamsUrl = JSONObject.toJSONString(paramsUrl);

        DouyinPaySignDTO signDTO = new DouyinPaySignDTO();
        signDTO.setApp_id(appId);
        signDTO.setMerchant_id(merchantId);
        signDTO.setSign_type("MD5");
        signDTO.setTimestamp(timestamp);
        signDTO.setTotal_amount(amount);
        signDTO.setParams(alipayParamsUrl);
        signDTO.setTrade_no(douyinTradeNo);
        signDTO.setUid(openId);
        String sortSign = SortUtil.buildSignStr(BeanMapUtil.beanToMap(signDTO));
        String sign = sortSign + appSecret;
        log.info("<<<<<<<<sort sign:{}", sign);
        //注：MD5签名方式
        sign = MD5Util.md5(sign);

        Map<String, Object> data = new HashMap<>();
        data.put("appId", appId);
        data.put("signType", "MD5");
        data.put("timestamp", timestamp);
        data.put("tradeNo", douyinTradeNo);
        data.put("merchantId", merchantId);
        data.put("uid", openId);
        data.put("totalAmount", amount);
        data.put("params", alipayParamsUrl);

        data.put("sign", sign);
        data.put("payChannel", "ALIPAY_NO_SIGN");
        data.put("payType", "ALIPAY_APP");
        data.put("method", "tp.trade.confirm");

        Map riskInfo = new HashMap();
        riskInfo.put("ip", goodsOrder.getIpAddress());
        data.put("riskInfo", JSONObject.toJSONString(riskInfo));

        data.put("orderId", goodsOrder.getId());
        data.put("leftTime", orderAboutService.getWillPayLeftTimeSecond(goodsOrder));

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(data);

        //存储redis 15分钟过期
        if (req != null) {
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey(), JSON.toJSONString(simpleResponse), 15, TimeUnit.MINUTES);
        }
        return simpleResponse;
    }

    @Override
    public SimpleResponse alipayRefund(Order refundOrder, Order oldOrder, BigDecimal refundFee) {
        String redisForbidKey = RedisKeyConstant.GOODORDER_FORBID_KEY + refundOrder.getOrderNo();
        if (!stringRedisTemplate.opsForValue().setIfAbsent(redisForbidKey, "")) {
            return new SimpleResponse();
        }
        RequestRefundReq requestRefundReq = new RequestRefundReq();
        requestRefundReq.setOutTradeNo(oldOrder.getOrderNo());
        requestRefundReq.setRefundAmount(refundFee.setScale(2, BigDecimal.ROUND_DOWN).toString());
        requestRefundReq.setOutRequestNo(refundOrder.getOrderNo());
        requestRefundReq.setRefundReason("退款订单");
        ResponseRefundDTO responseRefundDTO = commAppService.alipayOrderRefund(requestRefundReq);
        if (StringUtils.isEmpty(responseRefundDTO.getSubCode())) {
            if (!CollectionUtils.isEmpty(responseRefundDTO.getParams()) && responseRefundDTO.getParams().get("biz_content") != null) {
                Map map = JSONObject.parseObject(responseRefundDTO.getParams().get("biz_content"), Map.class);
                if (!CollectionUtils.isEmpty(map) && map.get("refund_amount") != null) {
                    BigDecimal alipayRefundAmount = new BigDecimal(map.get("refund_amount").toString());
                    refundOrder.setFee(alipayRefundAmount);
                    refundOrder.setPaymentTime(new Date());
                    //校验退款金额是否匹配
                    if (alipayRefundAmount.compareTo(refundFee) != 0) {
                        log.error("<<<<<<<<alipay退款金额与申请退款金额不一致，采用实际alipay返回的退款金额为准. fee:{}, resp fee:{}", refundFee, alipayRefundAmount);
                    }
                }
            }
            //退款成功后的更新
            int i = this.orderStatusService.refundSuccess(refundOrder);
            log.info("退款订单更新成功与否：{}", i);
            //退款后的业务处理
            orderCallBackService.refundOrderCallBackBiz(refundOrder, oldOrder);
            return new SimpleResponse();
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1);
        }
    }
}
