package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.distribution.feignapi.dto.MerchantOrderIncomeResp;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.constants.OrderTypeEnum;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.DistributionAppService;
import mf.code.order.api.feignclient.ShopAppService;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.common.caller.wxpay.WxpayProperty;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.repo.repository.MerchantRepository;
import mf.code.order.service.OrderCallBackService;
import mf.code.user.dto.UpayWxOrderReqDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.order.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月20日 10:12
 */
@Slf4j
@Service
public class OrderCallBackServiceImpl implements OrderCallBackService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;

    @Async
    @Override
    public void payOrderCallBack(Order order) {
        //redis去重
        String redisForbidKey = RedisKeyConstant.GOODORDER_FORBID_KEY + order.getOrderNo();
        try {
            //插入成功，设置过期时间
            stringRedisTemplate.expire(redisForbidKey, 3, TimeUnit.SECONDS);

            //业务 分销佣金 消息队列,交易明细插入
            //交易明细
            UpayWxOrderReqDTO upayWxOrderReqDTO = WxCallbackAboutDTO.addPayUpayWxOrderReqDTO(order, wxpayProperty.getMfAppId());
            try {
                int upayWxOrderNum = this.userAppService.saveUpayWxOrder(upayWxOrderReqDTO);
                if (upayWxOrderNum == 0) {
                    log.error("<<<<<<<<创建订单-回调过程插入交易明细记录异常001:{}", upayWxOrderReqDTO);
                }
            } catch (Exception e) {
                log.error("<<<<<<<<创建订单-回调过程插入交易明细记录异常001:{}", upayWxOrderReqDTO);
            }

            //商户交易明细
            try {
                String merchantOrderIncome = this.distributionAppService.getMerchantOrderIncome(order.getUserId(), order.getShopId(), order.getId());
                log.info("<<<<<<<<商品购买商户交易明细1：{}", merchantOrderIncome);
                if (StringUtils.isNotBlank(merchantOrderIncome)) {
                    List<MerchantOrderIncomeResp> merchantOrderIncomeResps = JSONArray.parseArray(merchantOrderIncome, MerchantOrderIncomeResp.class);
                    log.info("<<<<<<<<商品购买商户交易明细2：{}", merchantOrderIncomeResps);
                    int index = 1;
                    for (MerchantOrderIncomeResp merchantOrderIncomeResp : merchantOrderIncomeResps) {
                        Long merchantId = shopAppService.getMerchantIdByShopId(merchantOrderIncomeResp.getShopId());
                        String merchantOrderNo = order.getOrderNo() + "_" + index;
                        MerchantOrder merchantOrder = WxCallbackAboutDTO.addMerchantOrderByProduct(order, merchantOrderNo, wxpayProperty.getPubAppId(), merchantId, merchantOrderIncomeResp.getShopId(), merchantOrderIncomeResp.getTotalFee());
                        this.merchantOrderRepository.save(merchantOrder);
                        index++;
                    }
                }
            } catch (Exception e) {
                log.error("<<<<<<<<创建订单-回调过程插入商户交易明细记录异常002:{},e:{}", upayWxOrderReqDTO, e);
            }

            //分销佣金消息队列
            Map<String, Object> tempMap = new HashMap<>();
            tempMap.put("orderId", order.getId());
            tempMap.put("shopId", order.getShopId());
            tempMap.put("userId", order.getUserId());
            try {
                this.rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.DISTRIBUTION_ORDER), JSON.toJSONString(tempMap));
                log.info("rocketmq消息生产：" + JSON.toJSONString(tempMap));
            } catch (Exception e) {
                log.error("rocketmq消息生产错误：" + JSON.toJSONString(tempMap), e);
            }

            GoodsOrderReq req = WxCallbackAboutDTO.addCreateOrderRedisKey(order);
            this.stringRedisTemplate.delete(RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey());
        } finally {
            this.stringRedisTemplate.delete(redisForbidKey);
        }
    }

    @Async
    @Override
    public void refundOrderCallBackBiz(Order refundOrder, Order oldOrder) {
        if (oldOrder.getSource() == OrderSourceEnum.DOUYIN_SHOP_MANAGER.getCode()) {
            return;
        }
        //redis去重
        String redisForbidKey = RedisKeyConstant.GOODORDER_FORBID_KEY + refundOrder.getOrderNo();

        try {
            //插入成功，设置过期时间
            stringRedisTemplate.expire(redisForbidKey, 3, TimeUnit.SECONDS);

            //分销佣金 用户,交易明细
            //用户交易明细
            UpayWxOrderReqDTO upayWxOrderReqDTO = WxCallbackAboutDTO.addRefundUpayWxOrderReqDTO(refundOrder, wxpayProperty.getMfAppId());
            try {
                int upayWxOrderNum = this.userAppService.saveUpayWxOrder(upayWxOrderReqDTO);
                if (upayWxOrderNum == 0) {
                    log.error("<<<<<<<<退款订单-回调过程插入交易明细记录异常001:{}", upayWxOrderReqDTO);
                }
            } catch (Exception e) {
                log.error("<<<<<<<<退款订单-回调过程插入交易明细记录异常001:{}", upayWxOrderReqDTO);
            }

            //商户交易明细
            try {
                QueryWrapper<MerchantOrder> merchantOrderQueryWrapper = new QueryWrapper<>();
                merchantOrderQueryWrapper.lambda()
                        .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode())
                        .eq(MerchantOrder::getBizValue, refundOrder.getRefundOrderId())
                        .in(MerchantOrder::getBizType, Arrays.asList(BizTypeEnum.SHOPPING.getCode(),BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode()))
                ;
                List<MerchantOrder> merchantOrders = this.merchantOrderRepository.list(merchantOrderQueryWrapper);
                if (!CollectionUtils.isEmpty(merchantOrders)) {
                    int index = 1;
                    for (MerchantOrder merchantOrder : merchantOrders) {
                        String merchantOrderNo = oldOrder.getOrderNo() + "@" + index;
                        MerchantOrder merchantOrder1 = WxCallbackAboutDTO.addRefundMerchantOrderByProduct(refundOrder, merchantOrderNo, wxpayProperty.getPubAppId(), merchantOrder.getMerchantId(), merchantOrder.getShopId(), merchantOrder.getTotalFee());
                        this.merchantOrderRepository.save(merchantOrder1);
                        index++;

                        //当退款时，wx支付场景，这笔订单时已结算状态,则余额做相应的减法处理
                        if (merchantOrder.getMfTradeType() == MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode() && oldOrder.getPayChannel() == OrderPayChannelEnum.WX.getCode()) {
                            //退款时，余额进行相应的扣除
                            Long parentMerchantId = this.merchantRepository.getParentMerchantId(merchantOrder.getMerchantId());
                            QueryWrapper<MerchantBalance> balanceQueryWrapper = new QueryWrapper<>();
                            balanceQueryWrapper.lambda()
                                    .eq(MerchantBalance::getMerchantId, parentMerchantId)
                                    .eq(MerchantBalance::getCustomerId, merchantOrder.getMerchantId())
                                    .eq(MerchantBalance::getShopId, merchantOrder.getShopId())
                            ;
                            //存储or更新余额
                            MerchantBalance merchantBalance = this.merchantBalanceRepository.getOne(balanceQueryWrapper);
                            if (merchantBalance == null) {
                                log.error("<<<<<<<<该账户余额不存在，异常，异常，异常~~~");
                            }
                            merchantBalanceRepository.lessUpdateByVersionId(merchantBalance, merchantOrder.getTotalFee(), BigDecimal.ZERO);
                        }
                    }
                }
            } catch (Exception e) {
                log.error("<<<<<<<<退款订单-回调过程更新商户交易明细记录异常002:{}, e:{}", upayWxOrderReqDTO, e);
            }
            //取消分销佣金队列
            Map<String, Object> tempMap = new HashMap<>();
            tempMap.put("orderNo", oldOrder.getOrderNo());
            tempMap.put("shopId", String.valueOf(refundOrder.getShopId()));
            try {
                this.rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.DISTRIBUTION_ORDER_REFUND), JSON.toJSONString(tempMap));
                log.info("<<<<<<<<退款订单-rocketmq消息生产：{}", JSON.toJSONString(tempMap));
            } catch (Exception e) {
                log.error("<<<<<<<<退款订单：{}，rocketmq消息生产错误：{}", JSON.toJSONString(tempMap), e);
            }
        } finally {
            this.stringRedisTemplate.delete(redisForbidKey);
        }
    }
}
