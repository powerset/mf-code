package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.goods.dto.ProductRankSalesVolumeDTO;
import mf.code.order.api.applet.dto.OrderStatusData;
import mf.code.order.api.feignclient.DistributionAppService;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.api.feignclient.OneAppService;
import mf.code.order.common.caller.wxmp.WxmpProperty;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.feignapi.constant.*;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.po.OrderBiz;
import mf.code.order.repo.po.OrderLog;
import mf.code.order.repo.po.OrderReturnsApply;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.OrderBizRepository;
import mf.code.order.repo.repository.OrderLogRepository;
import mf.code.order.repo.repository.OrderReturnsApplyRepository;
import mf.code.order.service.*;
import mf.code.order.service.templateMsg.OrderTemplateMessageService;
import mf.code.order.service.templateMsg.TemplateMessageData;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.service.impl
 *
 * @auther yechen
 * @email wangqingfeng@wxyundian.com
 * @Date 2019年04月11日 17:48
 */
@Service
@Slf4j
public class OrderStatusServiceImpl implements OrderStatusService {
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private OrderLogRepository orderLogRepository;
    @Autowired
    private OrderWxPayService orderWxPayService;
    @Autowired
    private OrderReturnsApplyRepository orderReturnsApplyRepository;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private OrderTemplateMessageService orderTemplateMessageService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private OrderBizRepository orderBizRepository;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private OrderAsyncService orderAsyncService;
    @Autowired
    private OrderAlipayService orderAlipayService;

    /***
     * 买家下单中(生成待付款订单)
     *
     * ps:流程转向：
     * 无->待付款
     * @param order 买家支付订单 order内所有基本信息得持有
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Order buyerPurchaseing(Order order, GoodProductDTO goodProductDTO, GoodsOrderReq req) {
        order.setStatus(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        order.setCtime(new Date());
        order.setUtime(new Date());
        Integer integer = this.goodsOrderRepository.insertSelective(order);
        if (integer == 0) {
            log.error("<<<<<<<<买家下单异常：支付订单信息：{}", order);
        }
        Assert.isTrue(integer == 1, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "创建订单存储异常");

        //处理订单当时的业务
        // 判断订单是否需要分销，秒杀或者平台免单抽奖
        boolean needDistribution = true;
        if (req != null && (req.getNotShow() == ProductConstant.ProductSku.NotShow.FLASH_SALE
                || req.getNotShow() == ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK)) {
            needDistribution = false;
        }
        if (needDistribution) {
            //此刻下单时的分销比例,订单业务存储
            log.info("<<<<<<<<分销业务值req:{}", JSONObject.toJSONString(goodProductDTO));
            if (goodProductDTO != null) {
                ProductDistributionDTO reqDto = new ProductDistributionDTO();
                BeanUtils.copyProperties(goodProductDTO, reqDto);
                reqDto.setProductId(goodProductDTO.getGoodsId());
                reqDto.setParentId(goodProductDTO.getParentId());
                log.info("<<<<<<<<分销业务值resp:{}", JSONObject.toJSONString(reqDto));
                String distirbutionProperty = distributionAppService.queryOrderDistributionProperty(reqDto);
                if (StringUtils.isNotBlank(distirbutionProperty)) {
                    OrderBiz orderBiz = new OrderBiz();
                    orderBiz.setOrderId(order.getId());
                    orderBiz.setBizType(OrderBizBizTypeEnum.DISTRIBUTION.getCode());
                    orderBiz.setBizValue(distirbutionProperty);
                    orderBiz.setDel(DelEnum.NO.getCode());
                    int i = this.orderBizRepository.insertSelective(orderBiz);
                    if (i == 0) {
                        log.error("<<<<<<<<买家下单异常：存储订单业务异常：{}", orderBiz);
                    }
                } else {
                    log.error("分销属性配置获取异常：{}", distirbutionProperty);
                }
                asyncSendCancelOrderMQMessage(order, distirbutionProperty, FanActionEventEnum.ORDER);
            }
        }
        return order;
    }

    /***
     * 买家下单成功
     *
     * ps:流程转向：
     * 待付款->待发货
     * @param order 买家支付订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerPurchaseSuccess(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        params.put("utime", new Date());
        params.put("paymentTime", order.getPaymentTime());
        params.put("tradeId", order.getTradeId());
        params.put("fee", order.getFee());
        params.put("notifyTime", order.getNotifyTime());
        params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());

        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<买家下单成功异常：支付订单信息：{}", order);
        }
        Assert.isTrue(i == 1, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "买家下单成功存储数据库异常");

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<买家下单成功存储orderLog异常：支付订单信息：{}", order);
        }

        if (order.getPayChannel() == OrderPayChannelEnum.WX.getCode()) {
            try {
                GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(order.getMerchantId(), order.getShopId(), order.getGoodsId(), order.getSkuId(), order.getUserId());
                if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                    log.error("<<<<<<<<获取订单商品信息异常：推送消息终止-orderId:{}", order.getId());
                    return i;
                }
                Map<String, Object> data = TemplateMessageData.paySuccess(order.getMerchantId(), order.getShopId(), order.getId(),
                        order.getOrderNo(), goodProductSkuDTO.getTitle(), order.getFee(), OrderPayChannelEnum.findByCode(order.getPayChannel()), order.getPaymentTime());
                this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getOrderPaySuccessMsgTmpId(), order.getUserId(), data);
            } catch (Exception e) {
                log.error("<<<<<<<<买家下单成功推送消息异常-orderId:{}", order.getId());
            }
        }
        // 20190604 start 添加创建订单消息队列，处理商品销量排行
        ProductRankSalesVolumeDTO volumeDTO = new ProductRankSalesVolumeDTO();
        BeanUtils.copyProperties(order, volumeDTO);
        volumeDTO.setOrderId(order.getId());
        try {
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.PRODUCT_RANK_SALES_VOLUME.getTopic() + ":"
                    + RocketMqTopicTagEnum.PRODUCT_RANK_SALES_VOLUME.getTag(), JSON.toJSONString(volumeDTO));
        } catch (Exception e) {
            log.error("<<<<<<<<通知消息队列异常：{}", volumeDTO);
        }
        // 20190604 end 添加创建订单消息队列，处理商品销量排行
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.PAY);
        return i;
    }

    /***
     * 卖家填写快递信息
     *
     * ps:流程转向
     * 待发货->待收货
     * @param orderId 买家支付订单编号
     * @param logisticsName 快递名
     * @param logisticsNum 快递号
     * @param logisticsCode 快递码
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sellerFillExpress(Long orderId, String logisticsName, String logisticsNum, String logisticsCode) {
        Order order = this.goodsOrderRepository.selectById(orderId);
        Assert.isTrue(order != null, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "订单编号" + orderId + "的订单不存在");

        //判断是否存在退款
        int countOrderNum = this.goodsOrderRepository.countByGoodsIdsByRefundOrderId(order.getId(), OrderStatusData.getRefundingStatus());
        Assert.isTrue(countOrderNum == 0, ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该订单已经发起退款,请去退款中查看");

        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        //物流信息
        Map map = new HashMap();
        map.put("name", logisticsName);
        map.put("num", logisticsNum);
        map.put("code", logisticsCode);
        order.setLogistics(JSONObject.toJSONString(map));
        order.setStatus(OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        order.setUtime(new Date());
        order.setSendTime(new Date());
        Integer integer = this.goodsOrderRepository.updateSelective(order);
        if (integer == 0) {
            log.error("<<<<<<<<卖家填写快递信息异常：订单信息：{}", order);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<卖家填写快递信息orderLog异常：支付订单信息：{}", order);
        }

        try {
            GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(order.getMerchantId(), order.getShopId(), order.getGoodsId(), order.getSkuId(), order.getUserId());
            if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                log.error("<<<<<<<<获取订单商品信息异常：推送消息终止-orderId:{}", order.getId());
                return integer;
            }
            Map<String, Object> data = TemplateMessageData.orderSend(order.getMerchantId(), order.getShopId(), order.getId(),
                    order.getOrderNo(), goodProductSkuDTO.getTitle(), logisticsName, logisticsNum);
            this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getOrderSendNoticeMsgTmpId(), order.getUserId(), data);
        } catch (Exception e) {
            log.error("<<<<<<<<订单发货提醒推送消息异常-orderId:{}", order.getId());
        }
        return integer;
    }

    /***
     * 买家确认收货
     *
     * ps:流程转向：
     * 交易成功(确认收货)
     * 0.待收货->交易成功(确认收货)
     * 1.回调过程异常-定时器，查找订单状态已付款(此时订单状态-待付款)(直接确认收货，做删除处理(小程序确保该订单不展现，交易明细得有记录)->走退款流程)
     * @param order 买家支付订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerConfirmReceipt(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
        params.put("utime", new Date());
        params.put("dealTime", new Date());

        if (order.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()) {
            params.put("oldstatus", OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        } else if (order.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            params.put("tradeId", order.getTradeId());
            params.put("paymentTime", order.getPaymentTime());
            params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        } else {
            log.error("<<<<<<<<买家确认收货原状态异常：订单编号为：{}，类型为：{} ,状态为：{}", order.getId(), OrderTypeEnum.findByCode(order.getType()).getMessage(),
                    OrderStatusEnum.findByCode(order.getStatus()).getMessage());
        }
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<买家确认收货异常：支付订单信息：{}", order);
        }
        Assert.isTrue(i == 1, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "买家确认收货存储数据库异常");

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<买家确认收货存储orderLog异常：支付订单信息：{}", order);
        }

        //确认收货-商家金额到账
        orderAsyncService.merchantAmountAchieved(order);

        // 发送 评价消息
        try {
            Map<String, String> commentParams = new HashMap<>();
            commentParams.put("shopId", order.getShopId().toString());
            commentParams.put("merchantId", order.getMerchantId().toString());
            commentParams.put("userId", order.getUserId().toString());
            commentParams.put("orderId", order.getId().toString());
            commentParams.put("orderNO", order.getOrderNo());
            commentParams.put("goodsId", order.getGoodsId().toString());
            commentParams.put("skuId", order.getSkuId().toString());
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.COMMENT_INIT),
                    JSON.toJSONString(commentParams));
            log.info("发送 评价消息：" + JSON.toJSONString(commentParams));
        } catch (Exception e) {
            log.error("发送 评价消息异常, exception = " + e);
        }


        return i;
    }

    /***
     * 支付订单的交易关闭
     * 退款场景：
     * 3.待收货->交易关闭(退款成功时)
     * 4.交易成功->交易关闭(退款成功时)
     * 5.待发货->交易关闭(退款成功时)
     * @param order 买家支付订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int payTradeClose(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode());
        params.put("utime", new Date());
        params.put("oldstatus", OrderStatusEnum.findByCode(order.getStatus()).getCode());
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<支付订单的交易关闭异常：支付订单信息：{}", order);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<支付订单的交易关闭orderLog异常：支付订单信息：{}", order);
        }
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.CANCEL_ORDER);
        return i;
    }

    /**
     * 异步发送取消订单消息
     *
     * @param order
     */
    private void asyncSendCancelOrderMQMessage(Order order, String distributionProperty, FanActionEventEnum fanActionEventEnum) {
        FanActionForMQ fanActionForMQ = new FanActionForMQ();
        fanActionForMQ.setMid(order.getMerchantId());
        fanActionForMQ.setSid(order.getShopId());
        fanActionForMQ.setUid(order.getUserId());
        fanActionForMQ.setEvent(fanActionEventEnum.getCode());
        fanActionForMQ.setEventId(order.getId());
        fanActionForMQ.setAmount(order.getFee());
        fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
        if (StringUtils.isNotBlank(distributionProperty)) {
            fanActionForMQ.setExtra(JSON.parseObject(distributionProperty));
        } else {
            OrderBiz orderBiz = orderBizRepository.selectByOrderId(order.getId());
            if (orderBiz != null) {
                fanActionForMQ.setExtra(JSON.parseObject(orderBiz.getBizValue()));
            }
        }
        rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
                JSON.toJSONString(fanActionForMQ), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("订单埋点发送成功" + fanActionEventEnum.getDesc());
                    }

                    @Override
                    public void onException(Throwable e) {
                        log.info("订单埋点发送失败" + fanActionEventEnum.getDesc() + JSON.toJSONString(fanActionForMQ));
                    }
                });
    }

    /***
     * 买家主动取消
     *
     * ps:流程转向：
     * 待付款->交易关闭
     * @param order
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerCancel(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode());
        params.put("utime", new Date());
        params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());

        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<买家主动取消异常：支付订单信息：{}", order);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<买家主动取消orderLog异常：支付订单信息：{}", order);
        }

        //退库存
        SimpleResponse simpleResponse1 = this.goodsAppService.giveBackStock(order.getSkuId(), order.getNumber(), order.getId());
        Assert.isTrue(simpleResponse1 != null && !simpleResponse1.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), "退库存异常");
        orderService.backUserCoupon(order);
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.CANCEL_ORDER);
        return i;
    }

    /***
     * 订单超时
     *
     * ps:流程转向：
     * 待付款->交易关闭
     * @param order
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerOrderTimeout(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        log.info("<<<<<<<<order:{}", order);
        BeanUtils.copyProperties(order, orderLog);
        log.info("<<<<<<<<order:{}", order);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode());
        params.put("utime", new Date());
        params.put("respJson", order.getRespJson());
        params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<订单超时异常：支付订单信息：{}", order);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<订单超时orderLog异常：支付订单信息：{}", order);
        }
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.CANCEL_ORDER);
        return i;
    }

    /***
     * 买家购买时，调用wx异常
     * @param order
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerOrderCreateWxException(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode());
        params.put("remark", order.getRemark());
        params.put("utime", new Date());
        params.put("del", order.getDel());
        params.put("respJson", order.getRespJson());
        params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<买家购买时，调用wx更新订单异常：支付订单信息：{}", order);
        }
        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<买家购买时，调用wx异常orderLog异常：支付订单信息：{}", order);
        }
        return i;
    }

    /***
     * 商家拒绝发货
     *
     * ps:流程转向：
     * 待发货->交易关闭
     * @param order
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sellerRefuseSend(Order order) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(order, orderLog);
        orderLog.setOrderId(order.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", order.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode());
        params.put("utime", new Date());
        params.put("oldstatus", OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        params.put("sellerMsg", order.getSellerMsg());
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<订单超时异常：支付订单信息：{}", order);
        }

        //发起退款
        SimpleResponse simpleResponse = this.orderService.refundOrder(order.getId(), order.getFee(), order.getSellerMsg());
        Assert.isTrue(!simpleResponse.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), simpleResponse.getMessage());

        //退库存
        SimpleResponse simpleResponse1 = this.goodsAppService.giveBackStock(order.getSkuId(), order.getNumber(), order.getId());
        Assert.isTrue(simpleResponse1 != null && !simpleResponse1.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), "退库存异常");

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<商家拒绝发货orderLog异常：支付订单信息：{}", order);
        }

        QueryWrapper<Order> refundWrapper = new QueryWrapper<>();
        refundWrapper.lambda()
                .eq(Order::getRefundOrderId, order.getId())
                .eq(Order::getDel, DelEnum.NO.getCode())
                .orderByDesc(Order::getCtime)
        ;
        List<Order> refundOrders = this.goodsOrderRepository.queryList(refundWrapper);
        if (!CollectionUtils.isEmpty(refundOrders)) {
            Order refundOrder = refundOrders.get(0);
            //存储order_returns_apply
            OrderReturnsApply orderReturnsApply = new OrderReturnsApply();
            orderReturnsApply.setOrderId(refundOrder.getId());
            orderReturnsApply.setMerchantId(refundOrder.getMerchantId());
            orderReturnsApply.setShopId(refundOrder.getShopId());
            orderReturnsApply.setUserId(refundOrder.getUserId());
            orderReturnsApply.setApplyType(OrderReturnsApplyApplyTypeEnum.MERCHANT.getCode());

            orderReturnsApply.setType(OrderReturnsApplyTypeEnum.REFUNDMONEY.getCode());
            orderReturnsApply.setWhy(refundOrder.getSellerMsg());
            orderReturnsApply.setStatus(OrderReturnsApplyStatusEnum.NO_STOCK.getCode());
            orderReturnsApply.setNote(refundOrder.getRemark());
            orderReturnsApply.setPic("");
            orderReturnsApply.setFee(refundOrder.getFee());
            this.orderReturnsApplyRepository.insertSelective(orderReturnsApply);
        }

        //拒绝推送消息
        try {
            GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(order.getMerchantId(), order.getShopId(), order.getGoodsId(), order.getSkuId(), order.getUserId());
            if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                log.error("<<<<<<<<获取订单商品信息异常：拒绝推送消息终止-orderId:{}", order.getId());
                return i1;
            }
            Map<String, Object> data = TemplateMessageData.orderMerchantRefuseCancel(order.getMerchantId(), order.getShopId(), order.getId(), order.getOrderNo(), goodProductSkuDTO.getTitle(), order.getFee());
            this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getOrdercancelMsgTmpId(), order.getUserId(), data);
        } catch (Exception e) {
            log.error("<<<<<<<<商家拒绝发货推送消息异常-orderId:{}", order.getId());
        }
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.CANCEL_ORDER);
        return i1;
    }

    /***
     * 买家发起退款申请
     *
     * @param refundOrder 退款订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerRefundStart(Order refundOrder, String pic, BigDecimal fee) {
        Integer integer = this.goodsOrderRepository.insertSelective(refundOrder);
        if (integer == 0) {
            log.error("<<<<<<<<买家发起退款申请异常：需要退款的订单信息：{}", refundOrder);
        }
        OrderTypeEnum orderTypeEnum = OrderTypeEnum.findByCode(refundOrder.getType());

        //存储order_returns_apply
        OrderReturnsApply orderReturnsApply = new OrderReturnsApply();
        orderReturnsApply.setOrderId(refundOrder.getId());
        orderReturnsApply.setMerchantId(refundOrder.getMerchantId());
        orderReturnsApply.setShopId(refundOrder.getShopId());
        orderReturnsApply.setUserId(refundOrder.getUserId());
        orderReturnsApply.setApplyType(OrderReturnsApplyApplyTypeEnum.USER.getCode());
        OrderReturnsApplyTypeEnum orderReturnsApplyTypeEnum = OrderReturnsApplyTypeEnum.findByCode(orderTypeEnum);
        Assert.isTrue(orderReturnsApplyTypeEnum != null, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请选择正确的退款方式");

        orderReturnsApply.setType(orderReturnsApplyTypeEnum.getCode());
        orderReturnsApply.setWhy(refundOrder.getRemark());
        orderReturnsApply.setStatus(OrderReturnsApplyStatusEnum.START.getCode());
        orderReturnsApply.setNote(refundOrder.getBuyerMsg());
        orderReturnsApply.setPic(pic);
        orderReturnsApply.setFee(fee);
        this.orderReturnsApplyRepository.insertSelective(orderReturnsApply);

        return integer;
    }

    /***
     * 买家撤销退款订单
     *
     * ps:流程转向：
     * 退款申请->退款撤销
     * @param refundOrder 退款订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerCancelRefund(Order refundOrder) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(refundOrder, orderLog);
        orderLog.setOrderId(refundOrder.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", refundOrder.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode());
        params.put("utime", new Date());
        /***
         * 仅退款： 原先状态为： 申请中
         *
         * 退货退款：  原先状态为： 申请中 或者 买家发货中
         */
        if (refundOrder.getType() == OrderTypeEnum.REFUNDMONEY.getCode()) {
            params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        }

        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<买家撤销退款订单异常：退款订单信息：{}", refundOrder);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<买家撤销退款订单orderLog异常：退款订单信息：{}", refundOrder);
        }

        //存储order_returns_apply
        int i2 = this.orderReturnsApplyRepository.insertSelectByUserCancel(refundOrder);
        if (i2 == 0) {
            log.error("<<<<<<<<买家撤销退款订单order_returns_apply异常：退款订单信息：{}", refundOrder);
        }

        return i;
    }

    /***
     * 卖家拒绝退款/退款退货
     *
     * ps:流程转向：
     * 退款申请->退款拒绝
     * 退款待收货->退款拒绝(TODO:暂无定义)
     * @param refundOrder 退款订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sellerRefuseRefund(Order refundOrder, String pic) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(refundOrder, orderLog);
        orderLog.setOrderId(refundOrder.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", refundOrder.getId());
        params.put("status", OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode());
        params.put("utime", new Date());
        params.put("sellerMsg", new Date());
        if (refundOrder.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        } else {
            log.error("<<<<<<<<卖家拒绝退款/退款退货异常：订单编号为：{}，类型为：{} ,状态为：{}", refundOrder.getId(), OrderTypeEnum.findByCode(refundOrder.getType()).getMessage(),
                    OrderStatusEnum.findByCode(refundOrder.getStatus()).getMessage());
        }
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<卖家拒绝退款/退款退货异常：退款订单信息：{}", refundOrder);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<卖家拒绝退款/退款退货orderLog异常：支付订单信息：{}", refundOrder);
        }

        //拒绝售后记录
        int i2 = orderReturnsApplyRepository.insertSelectiveByMerchantAudit(refundOrder.getId(), refundOrder.getSellerMsg(),
                null, pic, OrderReturnsApplyProductStatusEnum.UNRECEIVED, OrderReturnsApplyStatusEnum.REFUSE);
        if (i2 == 0) {
            log.error("<<<<<<<<卖家拒绝退款/退款退货orderRetrunApply异常：退款订单信息：{}", refundOrder);
        }

        //拒绝退款的推送
        try {
            Order order = this.goodsOrderRepository.selectById(refundOrder.getRefundOrderId());
            if (order == null) {
                log.error("<<<<<<<<查找退款单的原订单异常：refund_order_id:{}", refundOrder.getId());
                return i;
            }
            GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(refundOrder.getMerchantId(), refundOrder.getShopId(), refundOrder.getGoodsId(), refundOrder.getSkuId(), refundOrder.getUserId());
            if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                log.error("<<<<<<<<获取订单商品信息异常：推送消息终止-orderId:{}", refundOrder.getId());
                return i;
            }
            Map<String, Object> data = TemplateMessageData.refundApplyRefuse(refundOrder.getMerchantId(), refundOrder.getShopId(), refundOrder.getId(),
                    order.getOrderNo(), goodProductSkuDTO.getTitle(), OrderTypeEnum.findByCode(refundOrder.getType()), refundOrder.getRemark(), refundOrder.getFee());
            this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getRefundApplyMsgTmpId(), refundOrder.getUserId(), data);
        } catch (Exception e) {
            log.error("<<<<<<<<卖家拒绝退款/退款退货推送消息异常-orderId:{}", refundOrder.getId());
        }

        return i;
    }

    /***
     * 卖家同意退款/退货退款
     * eg:退款：直接退金额
     *    退货退款：等待买家发货
     *
     * ps:流程转向：退款申请->退款发货(退款退货：商家同意，等待买家发货)
     * @param refundOrder 退款订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sellerAgreeRefund(Order refundOrder, String pic) {
        Order refundOrderExist = this.goodsOrderRepository.selectById(refundOrder.getId());
        Assert.isTrue(refundOrderExist != null, ApiStatusEnum.ERROR_BUS_NO12.getCode(), "该退款订单不存在");
        boolean checkValid = refundOrderExist.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode();
        Assert.isTrue(checkValid, ApiStatusEnum.ERROR_BUS_NO13.getCode(), "该订单已经处理结束");
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(refundOrder, orderLog);
        orderLog.setOrderId(refundOrder.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", refundOrder.getId());
        params.put("utime", new Date());
        params.put("oldstatus", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        boolean onlyRefundMoney = false;
        if (refundOrder.getType() == OrderTypeEnum.REFUNDMONEY.getCode()) {
            params.put("status", OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
            onlyRefundMoney = true;
        } else if (refundOrder.getType() == OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()) {
            params.put("status", OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        }
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<卖家同意退款异常：需要退款的订单信息：{}", refundOrder);
        }
        Assert.isTrue(i != 0, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "卖家同意退款异常");

        //仅退款，发起退款
        if (onlyRefundMoney) {
            //退库存
            SimpleResponse simpleResponse1 = this.goodsAppService.giveBackStock(refundOrder.getSkuId(), refundOrder.getNumber(), refundOrder.getId());
            Assert.isTrue(simpleResponse1 != null && !simpleResponse1.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), "退库存异常");
            //查找原订单
            Order order = this.goodsOrderRepository.selectById(refundOrder.getRefundOrderId());
            Assert.isTrue(order != null, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "原订单查找不到异常");

            //发起退款
            //判断该订单是wx支付还是支付宝支付
            if (order.getPayChannel() == OrderPayChannelEnum.WX.getCode()) {
                SimpleResponse simpleResponse = this.orderWxPayService.wxRefund(order.getOrderNo(), order.getFee(), refundOrder.getOrderNo(), refundOrder.getFee(), "商品退款");
                Assert.isTrue(!simpleResponse.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), simpleResponse.getMessage());
            } else if (order.getPayChannel() == OrderPayChannelEnum.ALIPAY.getCode()) {
                SimpleResponse simpleResponse = this.orderAlipayService.alipayRefund(refundOrder, order, refundOrder.getFee());
                Assert.isTrue(!simpleResponse.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), simpleResponse.getMessage());
            }
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<卖家同意退款/退货退款orderLog异常：支付订单信息：{}", refundOrder);
        }

        //通过售后记录
        int i2 = orderReturnsApplyRepository.insertSelectiveByMerchantAudit(refundOrder.getId(), refundOrder.getSellerMsg(),
                null, pic, OrderReturnsApplyProductStatusEnum.UNRECEIVED, OrderReturnsApplyStatusEnum.PASS);
        if (i2 == 0) {
            log.error("<<<<<<<<卖家同意退款/退货退款orderRetrunApply异常：退款订单信息：{}", refundOrder);
        }

        //若是退款退货情况
        if (!onlyRefundMoney) {
            //拒绝退款的推送
            try {
                Order order = this.goodsOrderRepository.selectById(refundOrder.getRefundOrderId());
                if (order == null) {
                    log.error("<<<<<<<<查找退款单的原订单异常：refund_order_id:{}", refundOrder.getId());
                    return i;
                }
                GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(refundOrder.getMerchantId(), refundOrder.getShopId(), refundOrder.getGoodsId(), refundOrder.getSkuId(), refundOrder.getUserId());
                if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                    log.error("<<<<<<<<获取订单商品信息异常：推送消息终止-orderId:{}", refundOrder.getId());
                    return i;
                }
                Map<String, Object> data = TemplateMessageData.refundApplyBuyerReturnGoods(refundOrder.getMerchantId(), refundOrder.getShopId(), refundOrder.getId(),
                        order.getOrderNo(), goodProductSkuDTO.getTitle(), OrderTypeEnum.findByCode(refundOrder.getType()), refundOrder.getRemark(), refundOrder.getFee());
                this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getRefundApplyMsgTmpId(), refundOrder.getUserId(), data);
            } catch (Exception e) {
                log.error("<<<<<<<<退款退货审核通过推送消息异常-orderId:{}", refundOrder.getId());
            }
        }

        return i;
    }

    /***
     * 买家填写退货快递物流
     *
     * ps:流程转向：退款待发货->退款待收货(买家发货后，等待商家确认收货)
     * @param refundOrder 退款订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int buyerFillExpress(Order refundOrder) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(refundOrder, orderLog);
        orderLog.setOrderId(refundOrder.getId());

        refundOrder.setStatus(OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        refundOrder.setUtime(new Date());
        refundOrder.setSendTime(new Date());
        Integer integer = this.goodsOrderRepository.updateSelective(refundOrder);
        if (integer == 0) {
            log.error("<<<<<<<<买家填写退货快递物流异常：需要退货的订单信息：{}", refundOrder);
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<买家填写退货快递物流orderLog异常：支付订单信息：{}", refundOrder);
        }

        return integer;
    }

    /***
     * 卖家对退款退货订单确认收货
     *
     * ps:流程转向：
     *
     * 待收货->退款成功
     * @param refundOrder 退款订单
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sellerConfirmReceipt(Order refundOrder) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(refundOrder, orderLog);
        orderLog.setOrderId(refundOrder.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", refundOrder.getId());
        params.put("status", OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
        params.put("utime", new Date());
        params.put("oldstatus", OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());

        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<卖家对退款退货订单确认收货异常：需要退款退货的订单信息：{}", refundOrder);
        }
        Assert.isTrue(i != 0, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "卖家同意退款异常");

        //退库存
        SimpleResponse simpleResponse1 = this.goodsAppService.giveBackStock(refundOrder.getSkuId(), refundOrder.getNumber(), refundOrder.getId());
        Assert.isTrue(simpleResponse1 != null && !simpleResponse1.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), "退库存异常");

        //查找原订单
        Order order = this.goodsOrderRepository.selectById(refundOrder.getRefundOrderId());
        Assert.isTrue(order != null, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "原订单查找不到异常");

        //发起退款
        //判断该订单是wx支付还是支付宝支付
        if (order.getPayChannel() == OrderPayChannelEnum.WX.getCode()) {
            SimpleResponse simpleResponse = this.orderWxPayService.wxRefund(order.getOrderNo(), order.getFee(), refundOrder.getOrderNo(), refundOrder.getFee(), "商品退款");
            Assert.isTrue(!simpleResponse.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), simpleResponse.getMessage());
        } else if (order.getPayChannel() == OrderPayChannelEnum.ALIPAY.getCode()) {
            SimpleResponse simpleResponse = this.orderAlipayService.alipayRefund(refundOrder, order, refundOrder.getFee());
            Assert.isTrue(!simpleResponse.error(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), simpleResponse.getMessage());
        }

        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<卖家对退款退货订单确认收货orderLog异常：支付订单信息：{}", refundOrder);
        }
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.CANCEL_ORDER);
        return i;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int refundSuccess(Order refundOrder) {
        //orderLog 存储
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(refundOrder, orderLog);
        orderLog.setOrderId(refundOrder.getId());

        Map<String, Object> params = new HashMap<>();
        params.put("id", refundOrder.getId());
        params.put("fee", refundOrder.getFee());
        params.put("tradeId", refundOrder.getTradeId());
        params.put("utime", new Date());
        params.put("notifyTime", new Date());
        params.put("dealTime", new Date());
        params.put("status", OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
        params.put("oldstatus", OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        if (StringUtils.isNotBlank(refundOrder.getTradeId())) {
            params.put("tradeId", refundOrder.getTradeId());
        }
        int i = this.goodsOrderRepository.updateBySelectiveForAtomicity(params);
        if (i == 0) {
            log.error("<<<<<<<<卖家对退款退货订单确认收货异常：需要退款退货的订单信息：{}", refundOrder);
        }

        //查找原订单
        Order order = this.goodsOrderRepository.selectById(refundOrder.getRefundOrderId());
        Assert.isTrue(order != null, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "原订单查找不到异常");

        //原订单状态更新
        /***
         * TRADE_SUCCESS_REFUND_ING(6, "交易成功-退款中"),
         * WILLSEND_REFUNDING(2, "代发货-退款中;"),
         */
        this.payTradeClose(order);


        int i1 = this.orderLogRepository.insertSelective(orderLog);
        if (i1 == 0) {
            log.error("<<<<<<<<退款成功orderLog异常：支付订单信息：{}", refundOrder);
        }
        // 通知取消订单
        asyncSendCancelOrderMQMessage(order, "", FanActionEventEnum.CANCEL_ORDER);
        //退款成功的推送消息
        try {
            GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(refundOrder.getMerchantId(), refundOrder.getShopId(), refundOrder.getGoodsId(), refundOrder.getSkuId(), refundOrder.getUserId());
            if (goodProductSkuDTO == null || StringUtils.isBlank(goodProductSkuDTO.getTitle())) {
                log.error("<<<<<<<<获取订单商品信息异常：推送消息终止-orderId:{}", refundOrder.getId());
                return i;
            }
            Map<String, Object> data = TemplateMessageData.refundApplySuccess(refundOrder.getMerchantId(), refundOrder.getShopId(), refundOrder.getId(),
                    order.getOrderNo(), goodProductSkuDTO.getTitle(), OrderTypeEnum.findByCode(refundOrder.getType()), refundOrder.getRemark(), refundOrder.getFee());
            this.orderTemplateMessageService.sendTemplateMessage(wxmpProperty.getRefundApplyMsgTmpId(), refundOrder.getUserId(), data);
        } catch (Exception e) {
            log.error("<<<<<<<<退款成功推送消息异常-orderId:{}", refundOrder.getId());
        }

        return i;
    }
}
