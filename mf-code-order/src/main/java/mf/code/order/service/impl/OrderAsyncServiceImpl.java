package mf.code.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.*;
import mf.code.distribution.constant.ProductDistributionTypeEnum;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.repo.repository.MerchantRepository;
import mf.code.order.service.OrderAboutService;
import mf.code.order.service.OrderAsyncService;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * mf.code.order.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月16日 09:06
 */
@Service
@Slf4j
public class OrderAsyncServiceImpl implements OrderAsyncService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private OrderAboutService orderAboutService;
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private MerchantBalanceRepository merchantBalanceRepository;

    //没成功多少单，开始创建虚拟订单
    private final static int successOrderNum = 5;

    @Async
    @Override
    public void generateFalseOrder() {
        log.info("<<<<<<<<假订单开始进入");
        String reidsKey = RedisKeyConstant.SUCCESS_ORDER_KEY;
        //真实订单的数目
        String s = stringRedisTemplate.opsForValue().get(reidsKey);
        log.info("<<<<<<<<假订单-现存在的真实订单数目：{}", s);
        if (StringUtils.isBlank(s)) {
            return;
        }
        //拆红包迭代中，产品定义的每5单产生一条假订单
        int index = NumberUtils.toInt(s);
        if (index < successOrderNum) {
            return;
        }

        //获取收货地址,格式："北京市,北京市,海淀区,闵庄路北京市海淀区京香花园156号楼"
        String randomAddress = UserAddressUtil.getRandomAddress();
        if (StringUtils.isBlank(randomAddress)) {
            //如果真没获取到地址，那假订单就放下一次
            log.error("<<<<<<<<假订单-没获取到地址，randomAddress:{}", randomAddress);
            return;
        }
        List<Object> list = Arrays.asList(randomAddress.split(","));
        if (CollectionUtils.isEmpty(list) || list.size() < 4) {
            //地址获取异常
            log.error("<<<<<<<<假订单-地址获取异常or地址数量不满4层结构， address:{}", list);
            return;
        }
        String province = list.get(0).toString();
        String city = list.get(1).toString();
        String street = list.get(2).toString();
        String detail = list.get(3).toString();
        //根据收货地址，获取手机号
        String randomUserPhone = UserPhoneUtil.getRandomUserPhone(province);
        if (StringUtils.isBlank(randomUserPhone)) {
            log.error("<<<<<<<<假订单-根据省份，获取手机号异常，province:{}", province);
            return;
        }
        //获取收货人-姓
        String randomFirstName = UserFirstNameUtil.getRandomFirstName();
        if (StringUtils.isBlank(randomFirstName)) {
            log.error("<<<<<<<<假订单-获取收货人-姓异常，姓:{}", randomFirstName);
            return;
        }
        //获取收货人-名
        String randomSecondName = UserSecondNameUtil.getRandomSecondName();
        if (StringUtils.isBlank(randomSecondName)) {
            log.error("<<<<<<<<假订单-获取收货人-名异常，名:{}", randomSecondName);
            return;
        }
        String nick = randomFirstName + randomSecondName;

        //获取用户wxnick，wx头像
        String randomWxNick = WxNickUtil.getRandomWxNick();
        if (StringUtils.isBlank(randomWxNick)) {
            log.error("<<<<<<<<假订单-获取用户wxnick异常，wxnick:{}", randomWxNick);
            return;
        }
        String randomWxAvatarUrlFromOss = WxNickUtil.getRandomWxAvatarUrlFromOss();
        if (StringUtils.isBlank(randomWxAvatarUrlFromOss)) {
            log.error("<<<<<<<<假订单-获取用户wx头像异常，wx头像:{}", randomWxAvatarUrlFromOss);
            return;
        }
        //存储user
        UserResp userReq = new UserResp();
        userReq.setAvatarUrl(randomWxAvatarUrlFromOss);
        userReq.setNickName(randomWxNick);
        userReq.setGrantStatus(-1);
        String openId = DateFormatUtils.format(new Date(), "yyMMddHHmmss") + RandomStrUtil.randomStr(10);
        userReq.setOpenId(openId);
        Long userId = userAppService.addUser(userReq);
        log.info("<<<<<<<<假订单-创建虚拟用户返回：userId:{}", userId);
        if (userId == null) {
            log.error("<<<<<<<<假订单-存储用户异常, reqDTO:{}", userReq);
            return;
        }

        //存储address
        UserAddressReq addressReq = WxCallbackAboutDTO.addAddressDTO(province, city, street, detail, userId, nick, randomUserPhone);
        log.info("<<<<<<<<假订单-创建虚拟地址：req:{}", JSONObject.toJSONString(addressReq));
        String strAddress = userAppService.saveAddress(addressReq);
        log.info("<<<<<<<<假订单-创建虚拟地址返回：simpleResponse:{}", strAddress);
        if (StringUtils.isBlank(strAddress)) {
            log.error("<<<<<<<<假订单-创建虚拟地址异常, resp:{}", strAddress);
            return;
        }
        UserAddressReq resp = JSONObject.parseObject(strAddress, UserAddressReq.class);
        Long addressId = resp.getId();

        //获取随机sku信息
        GoodsSkuDTO goodProductSkuDTO = goodsAppService.queryRandomSkuOnsale();
        if (goodProductSkuDTO == null) {
            log.error("<<<<<<<<假订单-获取随机sku信息异常, sku:{}", goodProductSkuDTO);
            return;
        }
        Long merchantId = goodProductSkuDTO.getMerchantId();
        Long shopId = goodProductSkuDTO.getShopId();
        Long goodsId = goodProductSkuDTO.getGoodsId();
        //获取商品信息
        GoodProductDTO goodProductDTO = goodsAppService.queryProduct(merchantId, shopId, goodsId);
        if (goodProductDTO == null) {
            log.error("<<<<<<<<假订单-获取商品信息异常, product:{}", goodProductDTO);
            return;
        }

        //开始下超时订单

        //商品价格
        BigDecimal fee = new BigDecimal(goodProductSkuDTO.getPrice()).multiply(new BigDecimal("1"));
        BigDecimal originalFee = new BigDecimal(goodProductSkuDTO.getOriginalPrice()).multiply(new BigDecimal("1"));
        //step2:拼装微信统一下单的参数
        //订单号
        String outTraderNo = this.orderAboutService.getOrderNo(shopId);
        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = IpUtil.getIpAddress(null);
        String attach = "商品支付订单";
        String orderName = "购买商品支付订单";

        log.info("<<<<<<<<假订单开始创建");
        //step3:储存db
        //logistics,trade_id,goods_shop_id,req_json,resp_json,notify_time,payment_time,send_time,deal_time,
        Order goodsOrder = new Order();
        goodsOrder.setOrderNo(outTraderNo);
        goodsOrder.setOrderName(orderName);
        goodsOrder.setAddressId(addressId);
        goodsOrder.setType(OrderTypeEnum.PAY.getCode());
        goodsOrder.setPayChannel(OrderPayChannelEnum.WX.getCode());
        goodsOrder.setStatus(OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode());
        goodsOrder.setFee(fee);
        goodsOrder.setOriginalfee(originalFee);
        goodsOrder.setNumber(NumberUtils.toInt("1"));
        goodsOrder.setRemark(attach);
        goodsOrder.setMerchantId(merchantId);
        goodsOrder.setShopId(shopId);
        goodsOrder.setUserId(userId);
        goodsOrder.setGoodsShopId(goodProductDTO.getParentShopId());
        goodsOrder.setGoodsId(goodsId);
        goodsOrder.setSkuId(goodProductSkuDTO.getSkuId());
        goodsOrder.setIpAddress(ipAddress);
        ProductDistributionTypeEnum distributionTypeEnum = ProductDistributionTypeEnum.findByParams(goodProductDTO.getGoodsId(), goodProductDTO.getParentId(), goodProductDTO.getSelfSupportRatio(), goodProductDTO.getPlatSupportRatio());
        goodsOrder.setBizType(distributionTypeEnum.getCode());
        goodsOrder.setBuyerMsg("");
        goodsOrder.setCtime(new Date());
        goodsOrder.setUtime(new Date());
        Integer integer = this.goodsOrderRepository.insertSelective(goodsOrder);
        if (integer == 0) {
            log.error("<<<<<<<<存储订单 异常:{}", integer);
            return;
        }
        //下订单成功后,rediskey 原子性-5
        stringRedisTemplate.opsForValue().increment(reidsKey, -successOrderNum);
        log.info("<<<<<<<<假订单开始创建结束");
        return;
    }

    @Async
    @Override
    public void merchantAmountAchieved(Order order) {
        //确认收货的是抖带带-商家版的话，不做任何更新处理
        if (order.getSource() == OrderSourceEnum.DOUYIN_SHOP_MANAGER.getCode()) {
            return;
        }
        //这笔订单用户确认收货给商家金额进行结算
        QueryWrapper<MerchantOrder> merchantOrderQueryWrapper = new QueryWrapper<>();
        merchantOrderQueryWrapper.lambda()
                .eq(MerchantOrder::getType, mf.code.merchant.constants.OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode())
                .eq(MerchantOrder::getStatus, mf.code.merchant.constants.OrderStatusEnum.ORDERED.getCode())
                .in(MerchantOrder::getBizType, Arrays.asList(BizTypeEnum.SHOPPING.getCode(), BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode()))
                .eq(MerchantOrder::getBizValue, order.getId())
                .eq(MerchantOrder::getMfTradeType, MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode())
        ;
        List<MerchantOrder> merchantOrders = merchantOrderRepository.list(merchantOrderQueryWrapper);
        if (CollectionUtils.isEmpty(merchantOrders)) {
            return;
        }

        for (MerchantOrder merchantOrder : merchantOrders) {
            WxCallbackAboutDTO.updateMerchantOrderByProduct(merchantOrder);
            merchantOrderRepository.updateById(merchantOrder);

            //判断该订单是否是集客魔方的订单-直接结算
            if (merchantOrder.getBizType() == BizTypeEnum.SHOPPING.getCode()) {
                Long parentMerchantId = this.merchantRepository.getParentMerchantId(merchantOrder.getMerchantId());
                QueryWrapper<MerchantBalance> balanceQueryWrapper = new QueryWrapper<>();
                balanceQueryWrapper.lambda()
                        .eq(MerchantBalance::getMerchantId, parentMerchantId)
                        .eq(MerchantBalance::getCustomerId, merchantOrder.getMerchantId())
                        .eq(MerchantBalance::getShopId, merchantOrder.getShopId())
                ;
                //存储or更新余额
                MerchantBalance merchantBalance = this.merchantBalanceRepository.getOne(balanceQueryWrapper);
                if (merchantBalance == null) {
                    merchantBalance = new MerchantBalance();
                    merchantBalance.setMerchantId(parentMerchantId);
                    merchantBalance.setShopId(merchantOrder.getShopId());
                    merchantBalance.setCustomerId(merchantOrder.getMerchantId());
                    merchantBalance.setDepositCash(BigDecimal.ZERO);
                    merchantBalance.setDepositAdvance(merchantOrder.getTotalFee());
                    merchantBalance.setCtime(new Date());
                    merchantBalance.setUtime(new Date());
                    merchantBalance.setVersion(0);
                    this.merchantBalanceRepository.insertSelective(merchantBalance);
                } else {
                    merchantBalanceRepository.addUpdateByVersionId(merchantBalance, merchantOrder.getTotalFee(), BigDecimal.ZERO);
                }
            }
        }
    }
}
