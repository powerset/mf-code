package mf.code.order.service;

import mf.code.order.repo.po.Order;

/**
 * mf.code.order.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月20日 10:12
 */
public interface OrderCallBackService {

    /***
     * 购买商品回调--
     *
     * @param order
     * @return
     */
    void payOrderCallBack(Order order);

    /***
     * 退款成功后的额外业务处理
     *
     * @param refundOrder
     */
    void refundOrderCallBackBiz(Order refundOrder, Order oldOrder);
}
