package mf.code.order.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.repo.po.Order;
import mf.code.user.dto.UserResp;

import java.math.BigDecimal;

/**
 * mf.code.order.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月10日 10:09
 */
public interface OrderAlipayService {
    /***
     * 抖音-alipay-app支付方式
     *
     * @param goodsOrder
     * @param user
     * @param goodProductDTO
     * @return
     */
    SimpleResponse douyinAlipayAppType(Order goodsOrder, UserResp user, GoodProductDTO goodProductDTO, GoodsOrderReq req);


    SimpleResponse alipayRefund(Order refundOrder, Order oldOrder, BigDecimal refundFee);
}
