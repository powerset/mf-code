package mf.code.order.service;

import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.repo.po.Order;

import java.math.BigDecimal;

/**
 * mf.code.order.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月11日 17:47
 */
public interface OrderStatusService {
    /***
     * 买家下单中(生成待付款订单)
     *
     * ps:流程转向：
     * 无->待付款
     * @param order 买家支付订单 order内所有基本信息得持有
     * @return
     */
    Order buyerPurchaseing(Order order, GoodProductDTO goodProductDTO, GoodsOrderReq req);


    /***
     * 买家下单成功
     *
     * ps:流程转向：
     * 待付款->待发货
     * @param order 买家支付订单 order内所有基本信息得持有
     * @return
     */
    int buyerPurchaseSuccess(Order order);

    /***
     * 卖家填写快递信息
     *
     * ps:流程转向
     * 待发货->待收货
     * @param orderId 买家支付订单编号
     * @param logisticsName 快递名
     * @param logisticsNum 快递号
     * @param logisticsCode 快递码
     * @return
     */
    int sellerFillExpress(Long orderId, String logisticsName, String logisticsNum, String logisticsCode);

    /***
     * 买家确认收货
     *
     * ps:流程转向：
     * 交易成功(确认收货)
     * 0.待收货->交易成功(确认收货)
     * 1.回调过程异常-定时器，查找订单状态已付款(此时订单状态-待付款)(直接确认收货，做删除处理(小程序确保该订单不展现，交易明细得有记录)->走退款流程)
     * @param order 买家支付订单
     * @return
     */
    int buyerConfirmReceipt(Order order);

    /***
     * 支付订单的交易关闭
     * 退款场景：
     * 3.待收货->交易关闭(退款成功时)
     * 4.交易成功->交易关闭(退款成功时)
     * 5.待发货->交易关闭(退款成功时)
     * @param order 买家支付订单
     * @return
     */
    int payTradeClose(Order order);

    /***
     * 买家主动取消
     *
     * ps:流程转向：
     * 待付款->交易关闭
     * @param order
     * @return
     */
    int buyerCancel(Order order);

    /***
     * 订单超时
     *
     * ps:流程转向：
     * 待付款->交易关闭
     * @param order
     * @return
     */
    int buyerOrderTimeout(Order order);

    /***
     * 买家购买时，调用wx异常
     * @param order
     * @return
     */
    int buyerOrderCreateWxException(Order order);

    /***
     * 商家拒绝发货
     *
     * ps:流程转向：
     * 待发货->交易关闭
     * @param order
     * @return
     */
    int sellerRefuseSend(Order order);

    /***
     * 买家发起退款申请
     *
     * @param refundOrder 退款订单
     * @return
     */
    int buyerRefundStart(Order refundOrder, String pic, BigDecimal fee);

    /***
     * 买家撤销退款订单
     *
     * ps:流程转向：
     * 退款申请->退款撤销
     * @param refundOrder 退款订单
     * @return
     */
    int buyerCancelRefund(Order refundOrder);

    /***
     * 卖家拒绝退款/退款退货
     *
     * ps:流程转向：
     * 退款申请->退款拒绝
     * 退款待收货->退款拒绝(TODO:暂无定义)
     * @param refundOrder 退款订单
     * @return
     */
    int sellerRefuseRefund(Order refundOrder, String pic);

    /***
     * 卖家同意退款/退货退款
     * eg:退款：直接退金额
     *    退货退款：等待买家发货
     *
     * ps:流程转向：退款申请->退款发货(退款退货：商家同意，等待买家发货)
     * @param refundOrder 退款订单
     * @return
     */
    int sellerAgreeRefund(Order refundOrder, String pic);

    /***
     * 买家填写退货快递物流
     *
     * ps:流程转向：退款待发货->退款待收货(买家发货后，等待商家确认收货)
     * @param refundOrder 退款订单
     * @return
     */
    int buyerFillExpress(Order refundOrder);

    /***
     * 卖家对退款退货订单确认收货
     *
     * ps:流程转向：
     *
     * 待收货->退款成功
     * @param refundOrder 退款订单
     * @return
     */
    int sellerConfirmReceipt(Order refundOrder);


    /***
     * 退款订单到账-状态变更
     */
    int refundSuccess(Order refundOrder);
}
