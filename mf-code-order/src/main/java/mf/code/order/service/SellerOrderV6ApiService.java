package mf.code.order.service;

import mf.code.order.dto.SellerOrderListReqDTO;
import mf.code.order.dto.SellerOrderListResultDTO;

public interface SellerOrderV6ApiService {
    /**
     * 查询订单列表
     * @param reqDTO
     * @return
     */
    SellerOrderListResultDTO getOrderList(SellerOrderListReqDTO reqDTO);
}
