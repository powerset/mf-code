package mf.code.order.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.ApplyRefundReqDTO;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.dto.GoodsOrderUpdateReq;
import mf.code.order.dto.RefundLogisticsReqDTO;
import mf.code.order.repo.po.Order;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.order.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 10:24
 */
public interface OrderService {

    /***
     * 创建订单
     * @param goodsOrder
     * @return
     */
    SimpleResponse createGoodsOrder(GoodsOrderReq goodsOrder, String ipAddress);

    /***
     * 申请退款
     * @param orderId 需要退款的订单主键编号
     * @param refundFee 退款金额
     * @param refundDesc 退款描述
     * @return
     */
    SimpleResponse refundOrder(Long orderId, BigDecimal refundFee, String refundDesc);

    /***
     * 订单创建了在支付失效内,未支付,去付款
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @param request
     * @return
     */
    SimpleResponse payGoodsOrder(Long merchantId,
                                 Long shopId,
                                 Long userId,
                                 Long addressId,
                                 String buyerMsg,
                                 Long orderId,
                                 HttpServletRequest request);

    /***
     * 更新订单
     * @param goodsOrderUpdateReq
     * @return
     */
    SimpleResponse updateGoodsOrderByStatus(GoodsOrderUpdateReq goodsOrderUpdateReq);

    /***
     * 删除订单
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse delGoodsOrder(Long merchantId,
                                 Long shopId,
                                 Long userId,
                                 Long orderId);

    /***
     * 获取订单详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse queryGoodsOrderDetail(Long merchantId,
                                         Long shopId,
                                         Long userId,
                                         Long orderId);

    /***
     * 获取订单分页信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 0：全部 1待付款 2 待发货 3待收货 4 交易成功 5 退款/售后
     * @param limit
     * @param offset
     * @return
     */
    SimpleResponse getGoodsOrderByPage(Long merchantId,
                                       Long shopId,
                                       Long userId,
                                       int type,
                                       int limit,
                                       int offset);

    /***
     * 售后申请提交
     * @param req
     * @return
     */
    SimpleResponse applyRefund(ApplyRefundReqDTO req);

    /***
     * 售后申请页
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse applyRefund(Long merchantId, Long shopId, Long userId, Long orderId);

    /***
     * 获取售后订单详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse queryRefundGoodsOrderDetail(Long merchantId, Long shopId, Long userId, Long orderId);

    /***
     * 退货填写物流信息
     * @param req
     * @return
     */
    SimpleResponse addRefundLogisticsInfo(RefundLogisticsReqDTO req);

    /***
     * 查询订单状态
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse queryOrderStatus(Long merchantId, Long shopId, Long userId, Long orderId);

    /***
     * 支付成功后的展现
     * @param userId
     * @param orderId
     * @return
     */
    SimpleResponse queryPaySuccess(Long userId, Long orderId);

    /**
     * 定时任务
     * 查找付款订单生成排行榜
     *
     * @param wrapper
     * @return
     */
    List<Order> findByStatus(QueryWrapper<Order> wrapper);

    /**
     * 退还用户优惠券
     *
     * @param order
     * @return
     */
    SimpleResponse backUserCoupon(Order order);

    /**
     * 根据条件查询订单
     */
    List<Order> getOrderListByConditions(Long shopId, Integer type, Integer[] statusArr, Date begin, Date end);

    /**
     * 店铺订单总额-销售金额
     *
     * @param shopId
     * @return
     */
    BigDecimal orderTotalFee(Long shopId, Date begin, Date end);

    /**
     * 查询订单数
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    Integer orderNumber(Long shopId, Date begin, Date end);

}
