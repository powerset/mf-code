package mf.code.order.service.impl;

import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.MerchantOrderIncomeResp;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.*;
import mf.code.order.common.caller.wxpay.WxpayProperty;
import mf.code.order.constant.OrderDiscountTypeEnum;
import mf.code.order.dto.GoodsOrderReq;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.MerchantOrderRepository;
import mf.code.order.service.OrderNoPayService;
import mf.code.order.service.OrderStatusService;
import mf.code.user.dto.UpayWxOrderReqDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-07-17 19:43
 */
@Slf4j
@Service
public class OrderNoPayServiceImpl implements OrderNoPayService {

    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;

    /**
     * 完成订单
     *
     * @param order          订单
     * @param req            入参
     * @param goodProductDTO 商品信息
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse completeOrder(Order order, GoodsOrderReq req, GoodProductDTO goodProductDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        orderStatusService.buyerPurchaseing(order, goodProductDTO, req);
        //扣库存
        SimpleResponse simpleResponse1 = goodsAppService.consumeStock(req.getSkuId(), NumberUtils.toInt(req.getNumber()), order.getId());
        if (simpleResponse1.error()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "商品库存出现异常，亲，重试下哦");
        }
        Date now = new Date();
        order.setPaymentTime(now);
        order.setStatus(OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        order.setNotifyTime(now);
        order.setUtime(now);
        int i = this.orderStatusService.buyerPurchaseSuccess(order);
        if (i == 0) {
            log.error("<<<<<<<<创建订单-零元支付修改状态失败");
        }
        if (req.getDiscountType() == OrderDiscountTypeEnum.ORDER_BACK.getCode()) {
            // 更新任务状态
            SimpleResponse simpleResponse2 = oneAppService.updateUserTaskForPlatOrderBack(req.getUserId(), req.getDiscountId());
            if (simpleResponse2.error()) {
                return simpleResponse2;
            }
        }
        //业务 分销佣金 消息队列,交易明细插入
        UpayWxOrderReqDTO upayWxOrderReqDTO = WxCallbackAboutDTO.addPayUpayWxOrderReqDTO(order, wxpayProperty.getMfAppId());
        try {
            int upayWxOrderNum = this.userAppService.saveUpayWxOrder(upayWxOrderReqDTO);
            if (upayWxOrderNum == 0) {
                log.error("<<<<<<<<创建订单-零元支付插入交易明细记录异常001:{}", upayWxOrderReqDTO);
            }
        } catch (Exception e) {
            log.error("<<<<<<<<创建订单-零元支付插入交易明细记录异常001:{}", upayWxOrderReqDTO);
        }

        //商户交易明细
        try {
            // 这个订单其实没有金额，算出来的条数应该只是涉及到那个店铺而已
            String merchantOrderIncome = this.distributionAppService.getMerchantOrderIncome(order.getUserId(), order.getShopId(), order.getId());
            log.info("<<<<<<<<商品购买商户交易明细1：{}", merchantOrderIncome);
            if (StringUtils.isNotBlank(merchantOrderIncome)) {
                List<MerchantOrderIncomeResp> merchantOrderIncomeResps = JSONArray.parseArray(merchantOrderIncome, MerchantOrderIncomeResp.class);
                log.info("<<<<<<<<商品购买商户交易明细2：{}", merchantOrderIncomeResps);
                int index = 1;
                for (MerchantOrderIncomeResp merchantOrderIncomeResp : merchantOrderIncomeResps) {
                    Long merchantId = shopAppService.getMerchantIdByShopId(merchantOrderIncomeResp.getShopId());
                    String merchantOrderNo = order.getOrderNo() + "_" + index;
                    MerchantOrder merchantOrder = WxCallbackAboutDTO.addMerchantOrderByProduct(order, merchantOrderNo,
                            wxpayProperty.getPubAppId(), merchantId, merchantOrderIncomeResp.getShopId(), merchantOrderIncomeResp.getTotalFee());
                    merchantOrderRepository.save(merchantOrder);
                    index++;
                }
            }
        } catch (Exception e) {
            log.error("<<<<<<<<创建订单-零元支付插入商户交易明细记录异常002:{},e:{}", upayWxOrderReqDTO, e);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("orderId", order.getId());
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }
}
