package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.RequestRefundReq;
import mf.code.alipay.ResponseRefundDTO;
import mf.code.comment.CommentStatusEnum;
import mf.code.common.DelEnum;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.IpUtil;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.GoodProductDTO;
import mf.code.goods.dto.GoodsSkuDTO;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.UserCouponDTO;
import mf.code.order.api.applet.dto.WxCallbackAboutDTO;
import mf.code.order.api.feignclient.*;
import mf.code.order.common.caller.logistics.LogisticsService;
import mf.code.order.common.redis.RedisKeyConstant;
import mf.code.order.constant.OrderDiscountTypeEnum;
import mf.code.order.constant.OrderSourceEnum;
import mf.code.order.dto.*;
import mf.code.order.feignapi.constant.OrderBizStatusEnum;
import mf.code.order.feignapi.constant.OrderPayChannelEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.CommonDict;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.CommonDictRepository;
import mf.code.order.repo.repository.GoodsOrderRepository;
import mf.code.order.service.*;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.order.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 10:24
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private OrderWxPayService orderWxPayService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private LogisticsService logisticsService;
    @Autowired
    private OrderAboutService orderAboutService;
    @Autowired
    private CommonDictRepository commonDictRepository;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private OrderAsyncService orderAsyncService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private OrderNoPayService orderNoPayService;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private CommentAppService commentAppService;
    @Autowired
    private OrderAlipayService orderAlipayService;
    @Autowired
    private CommAppService commAppService;
    @Autowired
    private OrderCallBackService orderCallBackService;

    /***
     * 创建订单
     * 1、校验是否能下单
     * 2、下单调用其他操作
     *
     * @param req
     * @return
     */
    @Override
    @Transactional
    public SimpleResponse createGoodsOrder(GoodsOrderReq req, String ipAddress) {
        // 因为存在之前分销订单逻辑问题，不能直接使用传入的商品，因为可能是平台商城进入，传入商品id为平台商品id，不是分销商品id
        GoodProductDTO goodProductDTO = this.goodsAppService.queryProduct(req.getMerchantId(), req.getShopId(), req.getGoodsId());
        if (goodProductDTO == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品已下架");
        }
        // 重新赋值请求参数
        Long goodsId = goodProductDTO.getGoodsId();
        req.setGoodsId(goodsId);

        // 优先寻找未支付的相同订单属性的订单信息，存在直接返回
        String orderStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey());
        if (StringUtils.isNotBlank(orderStr)) {
            SimpleResponse simpleResponse = JSONObject.parseObject(orderStr, SimpleResponse.class);
            Map map = (Map) simpleResponse.getData();
            Order order = this.goodsOrderRepository.selectById(NumberUtils.toLong(map.get("orderId").toString()));
            if (order != null) {
                map.put("leftTime", this.orderAboutService.getWillPayLeftTimeSecond(order));
                simpleResponse.setData(map);
                return simpleResponse;
            }
        }
        //若该商品是新人专享
        if (goodProductDTO.getShowType() == ProductShowTypeEnum.NEWBIE.getCode()) {
            //判断该商品是否已经购买过
            int i = goodsOrderRepository.selectCountOrderByProductId(req.getMerchantId(), req.getShopId(), req.getUserId(), goodsId);
            if (i > 0) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "新人专享商品购买次数已达上限");
            }
        }
        GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(req.getMerchantId(), req.getShopId(), goodsId, req.getSkuId(), req.getUserId());
        if (goodProductSkuDTO == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该商品品类已下架");
        }
        UserResp user = this.userAppService.queryUser(req.getUserId());
        if (user == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "获取用户异常");
        }
        //判断商品库存
        if (goodProductSkuDTO.getStock() == null || goodProductSkuDTO.getStock() < NumberUtils.toInt(req.getNumber())) {
            int stock = 0;
            if (goodProductSkuDTO.getStock() != null) {
                stock = goodProductSkuDTO.getStock();
            }
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "库存不够啦，当前库存只剩下" + stock);
        }

        //商品价格
        BigDecimal fee = new BigDecimal(goodProductSkuDTO.getPrice()).multiply(new BigDecimal(req.getNumber()));
        BigDecimal originalFee = new BigDecimal(goodProductSkuDTO.getOriginalPrice()).multiply(new BigDecimal(req.getNumber()));

        // 如果sku是秒杀或者回填订单活动则不计算分销，将not_show字段回填入入参，方便后续操作查询
        req.setNotShow(goodProductSkuDTO.getNotShow());
        // 判断活动免费领取/优惠券购买
        int discountType = OrderDiscountTypeEnum.NONE.getCode();
        if (req.getDiscountType() != null) {
            discountType = req.getDiscountType();
        }
        Long discountId = req.getDiscountId();
        OrderDiscountTypeEnum byCode = OrderDiscountTypeEnum.findByCode(discountType);
        switch (byCode) {
            case NONE:
                break;
            case COUPON:
                // 检查优惠券是否为自己的，如果不是则直接返回错误
                UserCouponDTO couponById = oneAppService.findCouponById(discountId);
                if (couponById != null
                        && couponById.getStatus() == UserCouponStatusEnum.RECEIVIED.getCode()
                        && couponById.getUserId().equals(req.getUserId())) {
                    fee = fee.subtract(couponById.getAmount());
                    // TODO 暂时将优惠券使用放在这里
                    SimpleResponse simpleResponse = oneAppService.useUserCoupon(discountId);
                    if (simpleResponse == null || simpleResponse.error()) {
                        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "优惠券使用异常，请稍后重试");
                    }
                } else {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "优惠信息查询异常，如确认无误，请稍后重试");
                }
                break;
            case ASSIST:
                break;
            case ORDER_BACK:
                // 如果能查询到优惠信息直接返回
                List<Order> orderList = goodsOrderRepository.selectListByDiscountTypeAndId(discountType, discountId, req.getUserId());
                if (!CollectionUtils.isEmpty(orderList)) {
                    Map<String, Object> resultMap = new HashMap<>();
                    resultMap.put("orderId", orderList.get(0).getId());
                    return new SimpleResponse(ApiStatusEnum.SUCCESS, resultMap);
                }
                SimpleResponse simpleResponse = oneAppService.checkUserTaskForPlatOrderBack(req.getUserId(), discountId);
                if (simpleResponse == null) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "网络故障，请稍后重试");
                }
                if (simpleResponse.error()) {
                    return simpleResponse;
                }
                fee = BigDecimal.ZERO;
                break;
            default:
                break;
        }
        //step2:拼装微信统一下单的参数
        //订单号
        String outTraderNo = this.orderAboutService.getOrderNo(req.getShopId());
        String attach = "商品支付订单";
        String orderName = "购买商品支付订单";

        //step3:储存db
        //logistics,trade_id,goods_shop_id,req_json,resp_json,notify_time,payment_time,send_time,deal_time,
        Order goodsOrder = new Order();
        goodsOrder.setOrderNo(outTraderNo);
        goodsOrder.setOrderName(orderName);
        goodsOrder.setAddressId(req.getAddressId());
        goodsOrder.setType(OrderTypeEnum.PAY.getCode());
        goodsOrder.setPayChannel(req.getPayType());
        goodsOrder.setStatus(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        goodsOrder.setFee(fee);
        goodsOrder.setOriginalfee(originalFee);
        goodsOrder.setNumber(NumberUtils.toInt(req.getNumber()));
        goodsOrder.setRemark(attach);
        goodsOrder.setMerchantId(req.getMerchantId());
        goodsOrder.setShopId(req.getShopId());
        goodsOrder.setUserId(req.getUserId());
        goodsOrder.setGoodsShopId(goodProductDTO.getParentShopId());
        // 因为可能是平台商城直接购买，所以这里直接采用查询商品的id
        goodsOrder.setGoodsId(goodProductDTO.getGoodsId());
        goodsOrder.setSkuId(req.getSkuId());
        goodsOrder.setIpAddress(ipAddress);
        goodsOrder.setBizType(req.getBizType());
        goodsOrder.setBuyerMsg(req.getBuyerMsg());
        goodsOrder.setDiscountType(discountType);
        goodsOrder.setDiscountId(discountId);
        goodsOrder.setCtime(new Date());
        goodsOrder.setUtime(new Date());
        goodsOrder.setSource(OrderSourceEnum.JKMF.getCode());

        // 判断支付金额是否不需要支付 TODO 待后续提供更好的处理方式
        if (fee.compareTo(BigDecimal.ZERO) <= 0) {
            // 直接成功。执行订单更新状态，更新商户订单，更新
            return orderNoPayService.completeOrder(goodsOrder, req, goodProductDTO);
        }
        //判断支付方式
        if (OrderPayChannelEnum.CASH.getCode() == req.getPayType()) {
            //余额付款方式
            //TODO:
            this.orderPayReport(goodsOrder);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO8.getCode(), "余额支付暂不支持，敬请期待");
        } else if (OrderPayChannelEnum.WX.getCode() == req.getPayType()) {
            //扫码付款方式|充值
            SimpleResponse simpleResponse = this.orderWxPayService.wxPayType(goodsOrder, req, user.getOpenId(), goodProductDTO);
            if (simpleResponse.error()) {
                throw new IllegalArgumentException(simpleResponse.getMessage());
            }
            this.orderPayReport(goodsOrder);
            return simpleResponse;
        } else if (OrderPayChannelEnum.ALIPAY.getCode() == req.getPayType()) {
            //支付宝-app支付方式
            SimpleResponse simpleResponse = this.orderAlipayService.douyinAlipayAppType(goodsOrder, user, goodProductDTO, req);
            if (simpleResponse.error()) {
                throw new IllegalArgumentException(simpleResponse.getMessage());
            }
            this.orderPayReport(goodsOrder);
            return simpleResponse;
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO9.getCode(), "支付方式本平台不支持，请联系客服解决");
        }
    }

    /**
     * 订单支付埋点上报
     */
    private void orderPayReport(Order order) {
        log.info("^^^^^^^^^^^^^^ 订单支付埋点上报消息生产-开始");
        GoodsOrderResp goodsOrder = new GoodsOrderResp();
        try {
            goodsOrder.setOrderId(order.getId());
            goodsOrder.setMerchantId(order.getMerchantId());
            goodsOrder.setShopId(order.getShopId());
            goodsOrder.setGoodsId(order.getGoodsId());
            goodsOrder.setUserId(order.getUserId());
            goodsOrder.setNumber(order.getNumber().toString());
            goodsOrder.setType(order.getType());
            goodsOrder.setStatus(order.getStatus());
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.STATISTICS_REPORT_ORDER_PAY.getTopic() + ":" + RocketMqTopicTagEnum.STATISTICS_REPORT_ORDER_PAY.getTag(),
                    JSON.toJSONString(goodsOrder));
            log.info("^^^^^^^^^^^^^^ 订单支付埋点上报消息生产-成功");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @Transactional
    public SimpleResponse refundOrder(Long orderId, BigDecimal refundFee, String refundDesc) {
        Order order = this.goodsOrderRepository.selectById(orderId);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该编号对应的订单不存在");
        }

        //创建退款订单
        String outTraderNo = this.orderAboutService.getOrderNo(order.getShopId());
        String attach = "商品退款订单";
        String orderName = "退款订单";
        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = IpUtil.getIpAddress(null);
        Order refundOrder = new Order();
        BeanUtils.copyProperties(order, refundOrder);
        refundOrder.setOrderNo(outTraderNo);
        refundOrder.setOrderName(orderName);
        refundOrder.setFee(refundFee);
        refundOrder.setRemark(attach);
        refundOrder.setType(OrderTypeEnum.REFUNDMONEY.getCode());
        refundOrder.setStatus(OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        refundOrder.setTradeId(null);
        refundOrder.setReqJson(null);
        refundOrder.setRespJson(null);
        refundOrder.setCtime(new Date());
        refundOrder.setUtime(new Date());
        refundOrder.setNotifyTime(null);
        refundOrder.setPaymentTime(null);
        refundOrder.setSendTime(null);
        refundOrder.setDealTime(null);
        refundOrder.setIpAddress(ipAddress);
        refundOrder.setRefundOrderId(order.getId());
        this.goodsOrderRepository.insertSelective(refundOrder);

        if (order.getPayChannel() == OrderPayChannelEnum.CASH.getCode()) {
            //TODO:余额退款
            return null;
        } else if (order.getPayChannel() == OrderPayChannelEnum.WX.getCode()) {
            //微信退款
            SimpleResponse simpleResponse = this.orderWxPayService.wxRefund(order.getOrderNo(), order.getFee(), outTraderNo, refundFee, refundDesc);
            if (simpleResponse.error()) {
                throw new IllegalArgumentException(simpleResponse.getMessage());
            }
            return simpleResponse;
        } else if (order.getPayChannel() == OrderPayChannelEnum.ALIPAY.getCode()) {
            //抖音-支付宝支付
            SimpleResponse simpleResponse = orderAlipayService.alipayRefund(refundOrder, order, refundFee);
            if (simpleResponse.error()) {
                throw new IllegalArgumentException(simpleResponse.getMessage());
            }
            return simpleResponse;
        } else {
            //支付渠道不支持退款
            log.warn("<<<<<<<<支付渠道不支持退款");
            return null;
        }
    }

    /***
     * 订单创建了在支付失效内,未支付,去付款
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @param request
     * @return
     */
    @Override
    public SimpleResponse payGoodsOrder(Long merchantId, Long shopId, Long userId, Long addressId, String buyerMsg, Long orderId, HttpServletRequest request) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0 || orderId == null || orderId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "支付条件不满足哦...");
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, merchantId)
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .eq(Order::getId, orderId)
                .eq(Order::getStatus, OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode())
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        List<Order> goodsOrders = this.goodsOrderRepository.queryList(wrapper);
        if (CollectionUtils.isEmpty(goodsOrders)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单已过期失效");
        }
        Order goodsOrder = goodsOrders.get(0);

        // 原则上先产生订单，然后再支付，所以到支付这里订单的商品数据应该是正确的
        GoodProductDTO goodProductDTO = this.goodsAppService.queryProduct(goodsOrder.getMerchantId(), goodsOrder.getShopId(), goodsOrder.getGoodsId());
        if (goodProductDTO == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品已下架");
        }
        // 如果返回的商品id和订单的商品id不同，说明平台商城商品复制的步骤上出现了错误
        if (!goodProductDTO.getGoodsId().equals(goodsOrder.getGoodsId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "订单数据异常，请重新下单");
        }
        GoodsSkuDTO goodProductSkuDTO = this.goodsAppService.queryProductSku(goodsOrder.getMerchantId(), goodsOrder.getShopId(), goodsOrder.getGoodsId(), goodsOrder.getSkuId(), goodsOrder.getUserId());
        if (goodProductSkuDTO == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该商品品类已下架");
        }

        boolean update = false;
        if (addressId != null && addressId > 0) {
            goodsOrder.setAddressId(addressId);
            update = true;
        }
        if (StringUtils.isNotBlank(buyerMsg)) {
            goodsOrder.setBuyerMsg(buyerMsg);
            update = true;
        }
        if (update) {
            goodsOrder.setUtime(new Date());
            this.goodsOrderRepository.updateSelective(goodsOrder);
        }

        GoodsOrderReq req = WxCallbackAboutDTO.addCreateOrderRedisKey(goodsOrder);
        String str = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.GOODORDER_ORDER + req.getRedisKey());
        if (StringUtils.isBlank(str)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单已过期失效...");
        }
        SimpleResponse simpleResponse = JSONObject.parseObject(str, SimpleResponse.class);
        Map map = (Map) simpleResponse.getData();
        map.put("leftTime", this.orderAboutService.getWillPayLeftTimeSecond(goodsOrder));
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 更新订单状态
     * @param goodsOrderUpdateReq
     * @return
     */
    @Override
    public SimpleResponse updateGoodsOrderByStatus(GoodsOrderUpdateReq goodsOrderUpdateReq) {
        if (goodsOrderUpdateReq.getMerchantId() == null || goodsOrderUpdateReq.getMerchantId() == 0 ||
                goodsOrderUpdateReq.getShopId() == null || goodsOrderUpdateReq.getShopId() == 0 ||
                goodsOrderUpdateReq.getUserId() == null || goodsOrderUpdateReq.getUserId() == 0 ||
                goodsOrderUpdateReq.getOrderId() == null || goodsOrderUpdateReq.getOrderId() == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "更新条件不满足哦...");
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, goodsOrderUpdateReq.getMerchantId())
                .eq(Order::getShopId, goodsOrderUpdateReq.getShopId())
                .eq(Order::getUserId, goodsOrderUpdateReq.getUserId())
                .eq(Order::getId, goodsOrderUpdateReq.getOrderId())
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        List<Order> goodsOrders = this.goodsOrderRepository.queryList(wrapper);
        if (CollectionUtils.isEmpty(goodsOrders)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单已过期，无需更新");
        }
        Order goodsOrder = goodsOrders.get(0);


        //3:待收货--->确认收货-----> 5 OrderBizStatusEnum.TRADE_CLOSE_ORDER_CANCEL
        //0待付款->取消订单7 TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL
        //9:退款-等待商家处理; 10:退款-等待买家退货; ---> 撤销申请 --> 13 TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL
        if (goodsOrderUpdateReq.getStatus() != null) {
            OrderBizStatusEnum orderBizStatusEnum = OrderBizStatusEnum.findByCode(goodsOrderUpdateReq.getStatus());
            OrderStatusEnum orderStatusEnum = this.orderAboutService.getOrderStatusFromBizStatus(orderBizStatusEnum);
            if (orderStatusEnum == OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS) {
                this.orderStatusService.buyerConfirmReceipt(goodsOrder);
            } else if (goodsOrder.getType() == OrderTypeEnum.PAY.getCode() && orderStatusEnum == OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL) {
                this.orderStatusService.buyerCancel(goodsOrder);
            } else if (orderStatusEnum == OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL) {
                this.orderStatusService.buyerCancelRefund(goodsOrder);
            }
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        return simpleResponse;
    }

    /***
     * 删除订单
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @Override
    public SimpleResponse delGoodsOrder(Long merchantId, Long shopId, Long userId, Long orderId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0 || orderId == null || orderId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "删除条件不满足哦...");
        }
        Order order = this.goodsOrderRepository.selectById(orderId);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单不存在哦，请重试");
        }
        order.setDel(DelEnum.YES.getCode());
        order.setUtime(new Date());
        this.goodsOrderRepository.updateSelective(order);
        return new SimpleResponse();
    }

    /***
     * 获取订单详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @Override
    public SimpleResponse queryGoodsOrderDetail(Long merchantId, Long shopId, Long userId, Long orderId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0 || orderId == null || orderId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "获取订单详情条件不满足哦...");
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Order::getMerchantId, merchantId).eq(Order::getShopId, shopId).eq(Order::getUserId, userId)
                .eq(Order::getId, orderId).eq(Order::getDel, DelEnum.NO.getCode())
        ;
        List<Order> goodsOrders = this.goodsOrderRepository.queryList(wrapper);
        if (CollectionUtils.isEmpty(goodsOrders)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单已经删除");
        }
        Order goodsOrder = goodsOrders.get(0);

        GoodsOrderDetailResp resp = new GoodsOrderDetailResp();
        BeanUtils.copyProperties(goodsOrder, resp);
        resp.setNumber(String.valueOf(goodsOrder.getNumber()));
        resp.setPrice(goodsOrder.getFee().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setOriginalPrice(goodsOrder.getOriginalfee().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.setCtime(goodsOrder.getCtime());
        resp.setPaytime(goodsOrder.getPaymentTime());
        resp.setSendtime(goodsOrder.getSendTime());
        resp.setDealtime(goodsOrder.getDealTime());
        resp.setOrderId(goodsOrder.getId());
        resp.setDiscountType(goodsOrder.getDiscountType());
        resp.setDiscountId(goodsOrder.getDiscountId());
        resp.setDiscountAmount(goodsOrder.getOriginalfee().subtract(goodsOrder.getFee()));
        //获取状态
        OrderBizStatusEnum bizStatus = this.orderAboutService.getBizStatus(goodsOrder);
        resp.setStatus(bizStatus.getCode());
        //获取sku商品信息
        Map orderGoodsInfo = this.orderAboutService.getOrderGoodsInfo(merchantId, shopId, goodsOrder.getGoodsId(), goodsOrder.getSkuId(), userId);
        if (orderGoodsInfo != null) {
            resp.setGoodsInfo(orderGoodsInfo);
        }
        resp.setCashback(BigDecimal.ZERO.toString());
        if (orderGoodsInfo != null && orderGoodsInfo.get("cashback") != null) {
            resp.setCashback(orderGoodsInfo.get("cashback").toString());
        }

        //获取物流信息
        if (StringUtils.isNotBlank(goodsOrder.getLogistics())) {
            Map<String, Object> jsonMap = JSONObject.parseObject(goodsOrder.getLogistics(), Map.class);
            String logisticsCode = "";
            String logisticsNum = "";
            if (jsonMap != null && jsonMap.get("code") != null && jsonMap.get("num") != null) {
                logisticsCode = jsonMap.get("code").toString();
                logisticsNum = jsonMap.get("num").toString();
            }
            //查询物流信息
            LogisticsInfoDto logisticsInfoDto = this.logisticsService.queryLogistics(logisticsCode, logisticsNum);
            Map map = new HashMap();
            map.put("num", logisticsNum);
            map.put("name", jsonMap.get("name").toString());
            if (logisticsInfoDto != null && !CollectionUtils.isEmpty(logisticsInfoDto.getLogistics())) {
                LogisticsInfoDto.LogisticsInfo logisticsInfo = logisticsInfoDto.getLogistics().get(0);
                //获取物流信息
                map.put("time", DateFormatUtils.format(logisticsInfo.getTime(), "yyyy.MM.dd HH:mm:ss"));
                map.put("context", logisticsInfo.getContext());
                map.put("location", logisticsInfo.getLocation());
                map.put("phone", logisticsInfo.getPhone());
                map.put("status", logisticsInfoDto.getStatus());
            }
            resp.setLogistics(map);
        }

        //获取收货地址
        UserAddressReq userAddressReq = this.userAppService.queryAdddress(goodsOrder.getAddressId());
        if (userAddressReq != null) {
            Map<String, Object> map = BeanMapUtil.beanToMapIgnore(userAddressReq, "user_id", "country_code", "province_code", "city_code", "postal_code");
            resp.setAddress(map);
        }

        //订单详情页的倒计时
        if (goodsOrder.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            resp.setLeftTime((long) this.orderAboutService.getWillPayLeftTimeSecond(goodsOrder));
        }

        if (goodsOrder.getStatus() == OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()) {
            List<Long> orderIds = new ArrayList<>();
            orderIds.add(orderId);
            Map<Long, String> orderCommentStatus = this.commentAppService.listOrderCommentStatus(orderIds);
            resp.setCommentStatus(orderCommentStatus == null ? "0" : orderCommentStatus.get(orderId));
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 获取订单分页信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type 0：全部 1待付款 2 待发货 3待收货 4 交易成功 5 退款/售后
     * @param limit
     * @param offset
     * @return
     */
    @Override
    public SimpleResponse getGoodsOrderByPage(Long merchantId, Long shopId, Long userId, int type, int limit, int offset) {
        Page<Order> page = new Page<Order>(offset / limit + 1, limit);
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, merchantId)
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        /***
         * 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);
         *         5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时)OrderStatusEnum
         */
        if (type == 0) {
            wrapper.and(params -> params.eq("type", OrderTypeEnum.PAY.getCode()));
        } else if (type == 1) {
            wrapper.and(params -> params.eq("type", OrderTypeEnum.PAY.getCode()));
            wrapper.and(params -> params.eq("status", OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()));
        } else if (type == 2) {
            wrapper.and(params -> params.eq("type", OrderTypeEnum.PAY.getCode()));
            wrapper.and(params -> params.eq("status", OrderStatusEnum.WILLSEND_OR_REFUND.getCode()));
        } else if (type == 3) {
            wrapper.and(params -> params.eq("type", OrderTypeEnum.PAY.getCode()));
            wrapper.and(params -> params.eq("status", OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()));
        } else if (type == 4) {
            wrapper.and(params -> params.eq("type", OrderTypeEnum.PAY.getCode()));
            wrapper.and(params -> params.eq("status", OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
            wrapper.lambda()
                    .gt(Order::getDealTime, DateUtil.addDay(new Date(), -7));
        } else if (type == 5) {
            wrapper.and(params -> params.in("type", Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode())));
        } else {
            log.error("类型筛选有误");
            return new SimpleResponse();
        }

        page.setDesc("ctime");
        IPage<Order> iPage = this.goodsOrderRepository.queryPage(page, wrapper);
        List<Order> goodsOrders = iPage.getRecords();
        AppletMybatisPageDto<GoodsOrderResp> appletMybatisPageDto = new AppletMybatisPageDto<>(limit, offset);
        appletMybatisPageDto.setContent(new ArrayList<>());

        SimpleResponse simpleResponse = new SimpleResponse();
        if (CollectionUtils.isEmpty(goodsOrders)) {
            simpleResponse.setData(appletMybatisPageDto);
            return simpleResponse;
        }

        //若是退款,则需要获取退款的原订单编号作为orderNo展现
        Map<Long, Order> oldRefundOrderMap = this.orderAboutService.getOldRefundOrderMap(goodsOrders);
        List<Long> orderIds = new ArrayList<>();
        List<Long> goodsIds = new ArrayList<>();
        for (Order goodsOrder : goodsOrders) {
            if (orderIds.indexOf(goodsOrder.getId()) == -1) {
                orderIds.add(goodsOrder.getId());
            }
            if (goodsIds.indexOf(goodsOrder.getGoodsId()) == -1) {
                goodsIds.add(goodsOrder.getGoodsId());
            }
        }

        Map<Long, GoodsSkuDTO> goodsSkuDTOMap = goodsAppService.queryProductSkuList(merchantId, shopId, goodsIds);

        //获取订单返现佣金
        Map<Long, BigDecimal> cashBackMap = distributionAppService.queryOrderHistoryRebate(orderIds);
        log.info("<<<<<<<<< 获取订单返现佣金:{}", cashBackMap);

        Map<Long, String> orderCommentStatus = commentAppService.listOrderCommentStatus(orderIds);
        if (CollectionUtils.isEmpty(orderCommentStatus)) {
            orderCommentStatus = new HashMap<>();
        }

        for (Order goodsOrder : goodsOrders) {
            GoodsOrderResp resp = new GoodsOrderResp();
            BeanUtils.copyProperties(goodsOrder, resp);
            resp.setOrderId(goodsOrder.getId());
            resp.setNumber(String.valueOf(goodsOrder.getNumber()));
            resp.setPrice(goodsOrder.getFee().setScale(2, BigDecimal.ROUND_DOWN).toString());
            resp.setOriginalPrice(goodsOrder.getOriginalfee().setScale(2, BigDecimal.ROUND_DOWN).toString());
            if (goodsOrder.getRefundOrderId() != null && goodsOrder.getRefundOrderId() > 0) {
                //若是退款，则订单号展现用原订单的订单号展现
                Order oldOrder = oldRefundOrderMap.get(goodsOrder.getRefundOrderId());
                if (oldOrder != null) {
                    resp.setOrderNo(oldOrder.getOrderNo());
                }
            }
            //获取状态
            OrderBizStatusEnum bizStatus = this.orderAboutService.getBizStatus(goodsOrder);
            resp.setStatus(bizStatus.getCode());
            //获取商品信息
            Map orderGoodsInfo = this.orderAboutService.getOrderGoodsInfoBySkuMap(goodsOrder, goodsSkuDTOMap);
            if (orderGoodsInfo != null) {
                resp.setGoodsInfo(orderGoodsInfo);
            }
            resp.setCashback(BigDecimal.ZERO.toString());
            if (cashBackMap != null) {
                BigDecimal cashback = cashBackMap.get(goodsOrder.getId());
                log.info("<<<<<<<<< 获取订单返现佣金 orderId:{} cashBack:{}", goodsOrder.getId(), cashback);
                resp.setCashback(cashback != null ? cashback.toString() : BigDecimal.ZERO.toString());
            }

            if (resp.getStatus() == OrderBizStatusEnum.TRADE_SUCCESS.getCode()) {
                resp.setCommentStatus(orderCommentStatus.get(goodsOrder.getId()));
            }

            appletMybatisPageDto.getContent().add(resp);
        }
        boolean pullDown = false;
        //是否还有下一页
        if (iPage.getPages() > offset / limit + 1) {
            pullDown = true;
        }
        appletMybatisPageDto.setPullDown(pullDown);
        simpleResponse.setData(appletMybatisPageDto);
        return simpleResponse;
    }

    @Override
    @Transactional
    public SimpleResponse applyRefund(ApplyRefundReqDTO req) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Order::getMerchantId, req.getMerchantId()).eq(Order::getShopId, req.getShopId()).eq(Order::getId, req.getOrderId())
                .eq(Order::getUserId, req.getUserId()).eq(Order::getDel, DelEnum.NO.getCode())
        ;
        Order order = this.goodsOrderRepository.selectOne(wrapper);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单已不存在");
        }
        if (order.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该订单还未付款，不能发起退款");
        }
        if (order.getDealTime() != null && DateUtils.addDays(new Date(), -7).getTime() > order.getDealTime().getTime()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "该订单已过维权期，不能发起退款");
        }
        OrderTypeEnum orderTypeEnum = OrderTypeEnum.findByCode(req.getType());
        if (orderTypeEnum == null || orderTypeEnum == OrderTypeEnum.PAY) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "请选择正确的退款方式");
        }
        if (order.getNumber() < req.getNumber()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10.getCode(), "退款数量不能大于购买时数量");
        }
        //验证退款金额<=实付款
        if (new BigDecimal(req.getPrice()).compareTo(order.getFee()) > 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "退款金额不能大于实付金额");
        }
        //判断是否存在进行中的退款单(注:本过滤条件-退款拒绝后还可以允许重新发起退款)
        wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Order::getMerchantId, req.getMerchantId()).eq(Order::getShopId, req.getShopId()).eq(Order::getRefundOrderId, req.getOrderId())
                .eq(Order::getUserId, req.getUserId()).eq(Order::getDel, DelEnum.NO.getCode())
                .in(Order::getStatus, Arrays.asList(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode(), OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                        OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(), OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()))
        ;
        Order existRefundOrder = this.goodsOrderRepository.selectOne(wrapper);
        if (existRefundOrder != null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "该退款已发起，请勿重复");
        }

        int i = this.goodsOrderRepository.countByGoodsIdsByRefundOrderId(req.getOrderId(), null);
        if (i >= 2) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "退款发起次数超限，上限2次");
        }

        if (order.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode() && orderTypeEnum.getCode() == OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "待发货订单只能退款哦");
        }

        if (order.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode() && order.getType() == OrderTypeEnum.PAY.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "订单已发货,不能退款");
        }

        //订单号
        String outTraderNo = this.orderAboutService.getOrderNo(order.getShopId());
        Order refundOrder = new Order();
        BeanUtils.copyProperties(order, refundOrder);
        //获取退款理由列表
        if (req.getReason() == null || req.getReason() < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "请选择正确的退款理由");
        }
        CommonDict commonDict = this.commonDictRepository.selectByTypeKey("product_order", "refund_reason");
        if (commonDict == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO8.getCode(), "页面开小差了，请刷新重试");
        }
        List<String> refundReasonList = Arrays.asList(commonDict.getValue().split(","));
        String reason = refundReasonList.get(req.getReason());

        //退款金额
        BigDecimal refundFee = new BigDecimal(req.getPrice()).setScale(2, BigDecimal.ROUND_DOWN);
        refundOrder.setType(orderTypeEnum.getCode());
        refundOrder.setOrderNo(outTraderNo);
        refundOrder.setOrderName("购买商品退款订单");
        refundOrder.setRefundOrderId(order.getId());
        refundOrder.setCtime(new Date());
        refundOrder.setNumber(req.getNumber());
        refundOrder.setUtime(new Date());
        refundOrder.setPaymentTime(null);
        refundOrder.setDealTime(null);
        refundOrder.setLogistics(null);
        refundOrder.setNotifyTime(null);
        refundOrder.setBuyerMsg(req.getRemark());
        refundOrder.setRemark(reason);
        refundOrder.setStatus(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        refundOrder.setFee(refundFee);

        //将用户上传凭证，转list处理
        String strPic = JSONArray.toJSONString(Arrays.asList(req.getPic()));
        this.orderStatusService.buyerRefundStart(refundOrder, strPic, new BigDecimal(req.getPrice()));

        SimpleResponse simpleResponse = new SimpleResponse();
        Map map = new HashMap();
        map.put("orderId", refundOrder.getId());
        simpleResponse.setData(map);
        return simpleResponse;
    }

    @Override
    public SimpleResponse applyRefund(Long merchantId, Long shopId, Long userId, Long orderId) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, merchantId)
                .eq(Order::getShopId, shopId)
                .eq(Order::getId, orderId)
                .eq(Order::getUserId, userId)
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        Order order = this.goodsOrderRepository.selectOne(wrapper);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该订单已不存在");
        }

        if (order.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode() && order.getType() == OrderTypeEnum.PAY.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "订单已发货,不能退款");
        }

        GoodsOrderDetailResp resp = new GoodsOrderDetailResp();
        BeanUtils.copyProperties(order, resp);
        //获取商品信息
        Map orderGoodsInfo = this.orderAboutService.getOrderGoodsInfo(merchantId, shopId, order.getGoodsId(), order.getSkuId(), userId);
        if (orderGoodsInfo != null) {
            resp.setGoodsInfo(orderGoodsInfo);
        }
        resp.setCashback(BigDecimal.ZERO.toString());
        if (orderGoodsInfo != null && orderGoodsInfo.get("cashback") != null) {
            resp.setCashback(orderGoodsInfo.get("cashback").toString());
        }
        resp.setOrderId(order.getId());
        resp.setNumber(String.valueOf(order.getNumber()));
        resp.setPrice(order.getFee().toString());
        resp.setOriginalPrice(order.getOriginalfee().toString());
        resp.setCtime(order.getCtime());
        resp.setPaytime(order.getPaymentTime());
        resp.setSendtime(order.getSendTime());
        resp.setDealtime(order.getDealTime());
        //获取退款理由列表
        CommonDict commonDict = this.commonDictRepository.selectByTypeKey("product_order", "refund_reason");
        if (commonDict == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "页面开小差了，请刷新重试");
        }
        List<Object> list = Arrays.asList(commonDict.getValue().split(","));
        resp.setRefundReasonList(list);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    @Override
    public SimpleResponse queryRefundGoodsOrderDetail(Long merchantId, Long shopId, Long userId, Long orderId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0 || orderId == null || orderId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "获取订单详情条件不满足哦...");
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Order::getMerchantId, merchantId).eq(Order::getShopId, shopId).eq(Order::getUserId, userId)
                .eq(Order::getId, orderId).eq(Order::getDel, DelEnum.NO.getCode())
                .orderByDesc(Order::getCtime)
        ;
        List<Order> goodsOrders = this.goodsOrderRepository.queryList(wrapper);
        if (CollectionUtils.isEmpty(goodsOrders)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单已经删除");
        }
        Order goodsOrder = goodsOrders.get(0);
        if (goodsOrder.getType() == OrderTypeEnum.PAY.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该订单未发起退款");
        }
        if (goodsOrder.getType() == OrderTypeEnum.PAY.getCode() && goodsOrder.getStatus() != OrderStatusEnum.WILLSEND_OR_REFUND.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "商家已取消该订单，不能发起售后");
        }

        RefundOrderDetailRespDTO resp = new RefundOrderDetailRespDTO();
        BeanUtils.copyProperties(goodsOrder, resp);
        resp.setOrderId(goodsOrder.getId());
        resp.setPrice(goodsOrder.getFee().toString());
        resp.setReason(goodsOrder.getRemark());
        resp.setNumber(String.valueOf(goodsOrder.getNumber()));

        //获取sku商品信息
        Map orderGoodsInfo = this.orderAboutService.getOrderGoodsInfo(merchantId, shopId, goodsOrder.getGoodsId(), goodsOrder.getSkuId(), userId);
        if (orderGoodsInfo != null) {
            resp.setGoodsInfo(orderGoodsInfo);
        }
        resp.setCashback(BigDecimal.ZERO.toString());
        if (orderGoodsInfo != null && orderGoodsInfo.get("cashback") != null) {
            resp.setCashback(orderGoodsInfo.get("cashback").toString());
        }

        //获取状态
        OrderBizStatusEnum bizStatus = this.orderAboutService.getBizStatus(goodsOrder);
        resp.setStatus(bizStatus.getCode());

        //获取协商历史
        List<Map> chatHistory = this.orderAboutService.queryChatHistory(merchantId, shopId, goodsOrder.getId(), userId);
        if (chatHistory != null) {
            resp.setChatHistory(chatHistory);
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    @Override
    public SimpleResponse addRefundLogisticsInfo(RefundLogisticsReqDTO req) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, req.getMerchantId())
                .eq(Order::getShopId, req.getShopId())
                .eq(Order::getId, req.getOrderId())
                .eq(Order::getUserId, req.getUserId())
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        Order order = this.goodsOrderRepository.selectOne(wrapper);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单不存在");
        }
        if (order.getType() == OrderTypeEnum.PAY.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该订单不是退款单");
        }
        //{"name":"快递名","num":"快递单号","code":"快递类型名"}
        Map map = new HashMap();
        map.put("name", req.getName());
        map.put("num", req.getNum());
        map.put("code", req.getCode());
        order.setLogistics(JSONObject.toJSONString(map));
        order.setStatus(OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        order.setSendTime(new Date());
        order.setUtime(new Date());
        this.goodsOrderRepository.updateSelective(order);

        SimpleResponse simpleResponse = new SimpleResponse();
        return simpleResponse;
    }

    @Override
    public SimpleResponse queryOrderStatus(Long merchantId, Long shopId, Long userId, Long orderId) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, merchantId)
                .eq(Order::getShopId, shopId)
                .eq(Order::getId, orderId)
                .eq(Order::getUserId, userId)
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        Order order = this.goodsOrderRepository.selectOne(wrapper);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该订单不存在");
        }

        //获取状态
        OrderBizStatusEnum bizStatus = this.orderAboutService.getBizStatus(order);

        Map map = new HashMap();
        map.put("status", bizStatus.getCode());
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 支付成功后的展现
     * @param userId
     * @param orderId
     * @return
     */
    @Override
    public SimpleResponse queryPaySuccess(Long userId, Long orderId) {
        Order order = this.goodsOrderRepository.selectById(orderId);
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该订单不存在");
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(new HashMap<>());

        //拆红包信息@百川
        Map<String, Object> openRedPackageMap = oneAppService.queryOpenRedPackageStatus(order.getShopId(), userId);
        log.info("<<<<<<<<拆红包信息 resp:{}", openRedPackageMap);
        Long activityId = 0L;
        if (openRedPackageMap != null) {
            simpleResponse.setData(openRedPackageMap);
            if (openRedPackageMap.get("activityId") != null) {
                activityId = NumberUtils.toLong(openRedPackageMap.get("activityId").toString());
            }
        }

        //有拆红包活动时，支付成功..
        if (openRedPackageMap != null && activityId > 0) {
            //更新拆红包活动状态(若有拆红包),@百川,更新wxPayOrder@夜辰
            SimpleResponse simpleResponse1 = oneAppService.completeOpenRedPackagePay(activityId);
            log.info("<<<<<<<<更新拆红包活动状态 resp:{}", simpleResponse1);
            Assert.isTrue(!simpleResponse1.error(), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "更新拆红包活动状态异常，请联系@百川");
        }


        /**下单成功后订单数目+1*/
        String successOrderRedisKey = RedisKeyConstant.SUCCESS_ORDER_KEY;
        stringRedisTemplate.opsForValue().increment(successOrderRedisKey, 1);
        //异步是否创建假订单
        orderAsyncService.generateFalseOrder();

        return simpleResponse;
    }

    /**
     * 查询付款订单
     *
     * @param wrapper
     * @return
     */
    @Override
    public List<Order> findByStatus(QueryWrapper<Order> wrapper) {
        return goodsOrderRepository.list(wrapper);
    }

    /**
     * 退还用户优惠券
     *
     * @param order
     * @return
     */
    @Override
    public SimpleResponse backUserCoupon(Order order) {
        if (order == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1);
        }
        if (order.getDiscountType() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1);
        }
        if (order.getDiscountType() != OrderDiscountTypeEnum.COUPON.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2);
        }
        if (order.getDiscountId() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2);
        }
        return oneAppService.backUserCoupon(order.getDiscountId());
    }

    /**
     * 查询订单
     *
     * @param shopId
     * @param type
     * @param statusArr
     * @param begin
     * @param end
     * @return
     */
    @Override
    public List<Order> getOrderListByConditions(Long shopId, Integer type, Integer[] statusArr, Date begin, Date end) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Order::getShopId, shopId)
                .eq(Order::getType, type)
                .eq(Order::getSource, OrderSourceEnum.DOUYIN_SHOP.getCode())
                .in(Order::getStatus, statusArr)
        ;
        if (begin != null) {
            queryWrapper.lambda().ge(Order::getPaymentTime, begin);
        }
        if (end != null) {
            queryWrapper.lambda().le(Order::getPaymentTime, end);
        }
        return goodsOrderRepository.list(queryWrapper);
    }

    /**
     * 订单金额
     *
     * @param shopId 店铺id
     * @param begin  开始时间 如不以时间筛选,begin=null
     * @param end    结束时间 同begin
     * @return 订单销售金额
     */
    @Override
    public BigDecimal orderTotalFee(Long shopId, Date begin, Date end) {
        List<Order> orderListByConditions = this.getOrderListByConditions(shopId, OrderTypeEnum.PAY.getCode(), OrderStatusEnum.payStatus(), begin, end);
        return this.calcTotalfeeByMerchantOrderList(orderListByConditions);
    }

    /**
     * 通过List<Order> 获取 总金额
     *
     * @param list
     * @return
     */
    private BigDecimal calcTotalfeeByMerchantOrderList(List<Order> list) {
        BigDecimal result = new BigDecimal(0);
        if (CollectionUtils.isEmpty(list)) {
            return result;
        }
        for (Order order : list) {
            if (order.getNumber() == null || order.getFee() == null) {
                continue;
            }
            result = result.add(order.getFee().multiply(new BigDecimal(order.getNumber())));
        }
        return result;
    }

    /**
     * 订单数
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    @Override
    public Integer orderNumber(Long shopId, Date begin, Date end) {
        List<Order> orderListByConditions = this.getOrderListByConditions(shopId, OrderTypeEnum.PAY.getCode(), OrderStatusEnum.payStatus(), begin, end);
        if (orderListByConditions != null) {
            return orderListByConditions.size();
        }
        return 0;
    }
}
