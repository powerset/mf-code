package mf.code.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.goods.dto.SellerProductAndSkuResultDTO;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.order.api.feignclient.GoodsAppService;
import mf.code.order.api.feignclient.ShopAppService;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.common.caller.logistics.LogisticsService;
import mf.code.order.constant.SellerOrderTypeEnum;
import mf.code.order.dto.*;
import mf.code.order.feignapi.constant.OrderReturnsApplyApplyTypeEnum;
import mf.code.order.feignapi.constant.OrderReturnsApplyStatusEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.po.*;
import mf.code.order.repo.repository.*;
import mf.code.order.service.SellerOrderQueryService;
import mf.code.user.dto.SellerUserAddressResultDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.order.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-11 11:49
 */
@Slf4j
@Service
public class SellerOrderQueryServiceImpl implements SellerOrderQueryService {

    @Autowired
    private GoodsOrderRepository goodsOrderRepository;
    @Autowired
    private LogisticsService logisticsService;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private CommonDictRepository commonDictRepository;
    @Autowired
    private OrderLogRepository orderLogRepository;
    @Autowired
    private OrderReturnsApplyRepository orderReturnsApplyRepository;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;

    /**
     * 查询订单列表
     *
     * @param reqDTO
     * @return
     */
    @Override
    public SellerOrderListResultDTO getOrderList(SellerOrderListReqDTO reqDTO) {

        Assert.notNull(reqDTO.getTabType(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "tab没有传");
        Map<String, Object> params = new HashMap<>();
        SellerOrderListResultDTO resultDTO = new SellerOrderListResultDTO();
        resultDTO.setPage(reqDTO.getPage());
        resultDTO.setSize(reqDTO.getSize());
        // 转换订单参数
        SimpleResponse paramSimpleResponse = changeParamsForSelectOrderList(reqDTO, params);
        if (paramSimpleResponse.error()) {
            return resultDTO;
        }
        Map<String, Object> resultMap = goodsOrderRepository.pageOrderListForSeller(params);
        if (null == resultMap.get("list")) {
            return resultDTO;
        }
        List<Order> orderList = (List<Order>) resultMap.get("list");
        resultDTO.setCount((Long) resultMap.get("count"));
        if (CollectionUtils.isEmpty(orderList)) {
            resultDTO.setOrderList(new ArrayList<>());
            return resultDTO;
        }
        Set<Long> addressIdSet = new HashSet<>();
        Set<Long> skuIdSet = new HashSet<>();
        // 退款订单所绑定的支付订单的id列表
        Set<Long> orderIdSet = new HashSet<>();
        // 支付订单列表
        Set<Long> originOrderIdSet = new HashSet<>();
        for (Order order : orderList) {
            // 地址查询，买家信息
            if (order.getAddressId() != null) {
                addressIdSet.add(order.getAddressId());
            }
            // 商品信息
            if (order.getSkuId() != null) {
                skuIdSet.add(order.getSkuId());
            }
            // 退款所绑定的支付订单id
            if (order.getRefundOrderId() != null && order.getRefundOrderId() > 0) {
                orderIdSet.add(order.getRefundOrderId());
            }
            // 支付订单id
            if (order.getType() == OrderTypeEnum.PAY.getCode()) {
                originOrderIdSet.add(order.getId());
            }
        }
        // feign调用查询地址,addressIdSet必定有值
        SimpleResponse addressResponse = userAppService.queryAddressMapForSeller(new ArrayList<>(addressIdSet));
        Map<String, SellerUserAddressResultDTO> addressResultDTOMap = new HashMap<>();
        if (addressResponse == null || addressResponse.error()) {
            resultDTO.setOrderList(new ArrayList<>());
            return resultDTO;
        }
        String jsonString = JSON.toJSONString(addressResponse.getData());
        addressResultDTOMap = JSONObject.parseObject(jsonString, new TypeReference<HashMap<String, SellerUserAddressResultDTO>>() {
        });
        if (CollectionUtils.isEmpty(addressResultDTOMap)) {
            addressResultDTOMap = new HashMap<>();
        }
        // feign调用查询商品信息，skuIdSet必定有值
        SimpleResponse simpleResponse = goodsAppService.queryProductSkuMapBySkuIdList(new ArrayList<>(skuIdSet));
        Map<String, SellerProductAndSkuResultDTO> productAndSkuMap = new HashMap<>();
        if (simpleResponse == null || simpleResponse.error()) {
            resultDTO.setOrderList(new ArrayList<>());
            return resultDTO;
        }
        String skuString = JSON.toJSONString(simpleResponse.getData());
        productAndSkuMap = JSONObject.parseObject(skuString, new TypeReference<HashMap<String, SellerProductAndSkuResultDTO>>() {
        });
        if (CollectionUtils.isEmpty(productAndSkuMap)) {
            productAndSkuMap = new HashMap<>();
        }
        // 如果是退款订单，查询支付订单（太无奈）
        Map<Long, Order> originalOrderMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(orderIdSet)) {
            List<Order> originalOrderList = goodsOrderRepository.selectOrderListByIdListForSeller(new ArrayList<>(orderIdSet));
            for (Order originalOrder : originalOrderList) {
                originalOrderMap.put(originalOrder.getId(), originalOrder);
            }
        }
        // 如果是支付订单，查询退款订单（真无奈，待重构）
        Map<Long, Order> payOrderMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(originOrderIdSet)) {
            List<Order> originalOrderList = goodsOrderRepository.selectRefundByPayOrderIdList(new ArrayList<>(originOrderIdSet));
            if (!CollectionUtils.isEmpty(originalOrderList)) {
                for (Order originalOrder : originalOrderList) {
                    // 缓存退款订单，key支付订单id，value倒叙第一条退款记录。列表id倒叙
                    if (payOrderMap.get(originalOrder.getRefundOrderId()) != null) {
                        continue;
                    }
                    payOrderMap.put(originalOrder.getRefundOrderId(), originalOrder);
                }
            }
        }
        List<SellerOrderInfoDTO> orderInfoDTOList = new ArrayList<>();
        for (Order order : orderList) {
            SellerOrderInfoDTO infoDTO = new SellerOrderInfoDTO();
            infoDTO.setId(order.getId());
            // 不能操作任何功能按钮
            infoDTO.setOperate(0);
            // 商品来源店铺才能也能操作所有操作
            if (StringUtils.equals(reqDTO.getShopId(), order.getGoodsShopId().toString())) {
                infoDTO.setOperate(1);
            } else {
                infoDTO.setOperate(1);
                // 不是商品所属店铺只有部分操作权限，比如收货，发货不能执行
                if (order.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()
                        || order.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()) {
                    infoDTO.setOperate(0);
                }
            }
            infoDTO.setOrderId(order.getId());
            infoDTO.setOrderNo(order.getOrderNo());
            infoDTO.setStatus(order.getStatus());
            infoDTO.setType(order.getType());
            // 退款订单列表需要查询原始类型订单数据
            if (order.getType() != OrderTypeEnum.PAY.getCode()) {
                if (originalOrderMap.get(order.getRefundOrderId()) != null) {
                    Order originalOrder = originalOrderMap.get(order.getRefundOrderId());
                    infoDTO.setOrderNo(originalOrder.getOrderNo());
                }
            }
            // 支付订单列表需要查询退款订单数据，应使用refundType和refundStatus来优先判断是否能点击后续功能按钮
            if (order.getType() == OrderTypeEnum.PAY.getCode()) {
                if (payOrderMap.get(order.getId()) != null) {
                    Order refundOrder = payOrderMap.get(order.getId());
                    infoDTO.setRefundType(refundOrder.getType());
                    infoDTO.setRefundStatus(refundOrder.getStatus());
                    // 支付订单里存在退款订单则不允许做操作
                    infoDTO.setOperate(0);
                    // 如果支付订单状态为待发货，但是用户提出退款，商家拒绝后依然可以发货成功
                    if (order.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()
                            && (refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()
                            || refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode())) {
                        infoDTO.setRefundType(null);
                        infoDTO.setRefundStatus(null);
                        infoDTO.setOperate(1);
                    }
                }
            }
            infoDTO.setCreateTime(DateUtil.dateToString(order.getCtime(), DateUtil.FORMAT_ONE));
            infoDTO.setFee(order.getFee());
            infoDTO.setBuyerMsg(order.getBuyerMsg());
            infoDTO.setSellerMsg(order.getSellerMsg());
            infoDTO.setProductNum(order.getNumber());
            // 买家信息
            SellerUserAddressResultDTO userAddress = addressResultDTOMap.get(order.getAddressId().toString());
            if (userAddress != null) {
                infoDTO.setBuyerAddress(userAddress.getCountry() + userAddress.getProvince() + userAddress.getCity() + userAddress.getStreet() + userAddress.getDetail());
                infoDTO.setBuyerNick(userAddress.getUserName());
                infoDTO.setWxNick(userAddress.getNick());
                infoDTO.setBuyerPhone(userAddress.getPhone());
            }
            infoDTO.setOrderType(SellerOrderTypeEnum.SELF_SUPPORT.getCode());
            // 商品信息
            SellerProductAndSkuResultDTO productAndSku = productAndSkuMap.get(order.getSkuId().toString());
            if (productAndSku != null) {
                infoDTO.setProductMainPics(JSONArray.parseArray(productAndSku.getProductSrcMainPics(), String.class).get(0));
                infoDTO.setProductPrice(productAndSku.getProductPrice());
                infoDTO.setProductTitle(productAndSku.getProductTitle());
                infoDTO.setSkuSpecs(productAndSku.getProductSpecsDesc());
                // 订单类型： 1分销商品订单 2供应商品订单 3自营商品订单
                if (order.getShopId().equals(order.getGoodsShopId())) {
                    // 自营商品
                    infoDTO.setOrderType(SellerOrderTypeEnum.SELF_SUPPORT.getCode());
                } else if (order.getShopId().equals(Long.valueOf(reqDTO.getShopId()))) {
                    // 分销商品
                    infoDTO.setOrderType(SellerOrderTypeEnum.DISTRIBUTION.getCode());
                } else {
                    // 供应商品
                    infoDTO.setOrderType(SellerOrderTypeEnum.SUPPLY.getCode());
                }
            }

            orderInfoDTOList.add(infoDTO);
        }
        resultDTO.setOrderList(orderInfoDTOList);
        return resultDTO;
    }

    /**
     * 转换组装参数 商户查询订单列表
     *
     * @param reqDTO
     * @param params
     */
    private SimpleResponse changeParamsForSelectOrderList(SellerOrderListReqDTO reqDTO, Map<String, Object> params) {
        SimpleResponse paramSimpleResponse = new SimpleResponse();
        if (StringUtils.isNotBlank(reqDTO.getOrderNo())) {
            params.put("orderNo", reqDTO.getOrderNo());
        }
        if (StringUtils.isNotBlank(reqDTO.getBuyerName()) || StringUtils.isNotBlank(reqDTO.getBuyerPhone())) {
            // 先查询用户地址信息，获得用户id
            SimpleResponse simpleResponse = userAppService.queryAddressIdByAddressNickOrPhoneForSeller(reqDTO.getBuyerName(), reqDTO.getBuyerPhone());
            if (simpleResponse == null || simpleResponse.error()) {
                paramSimpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                paramSimpleResponse.setMessage("参数错误");
                return paramSimpleResponse;
            }
            String jsonString = JSON.toJSONString(simpleResponse.getData());
            List<String> addressIdList = JSONObject.parseObject(jsonString, new TypeReference<List<String>>() {
            });
            params.put("addressIdList", addressIdList);
        }
        // TODO 可能有问题
        if (StringUtils.isNumeric(reqDTO.getProductId())) {
            params.put("goodsId", reqDTO.getProductId());
        }
        // 9 为查询全部状态订单, 状态和类型的参数暂时不要颠倒
        if (reqDTO.getOrderStatus() != null) {
            if (reqDTO.getOrderStatus() == OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode()
                    || reqDTO.getOrderStatus() == OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()) {
                params.put("statusList", Arrays.asList(OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode(), OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()));
            } else {
                params.put("status", reqDTO.getOrderStatus());
            }

            if (reqDTO.getOrderStatus() == 9) {
                params.remove("status");
            }
        }
        if (reqDTO.getTabType() != null) {
            // 全部
            if (reqDTO.getTabType() == 0) {
                params.put("type", OrderTypeEnum.PAY.getCode());
            }
            // 待发货
            if (reqDTO.getTabType() == 1) {
                params.put("type", OrderTypeEnum.PAY.getCode());
                params.put("status", OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
            }
            // 退款退货
            if (reqDTO.getTabType() == 2) {
                if (StringUtils.isNotBlank(reqDTO.getOrderNo())) {
                    // 因为只有每条支付订单只有两条退款订单记录，所以直接查询拼接
                    List<Order> orderList = goodsOrderRepository.selectRefundByPayOrderNo(reqDTO.getOrderNo());
                    if (!CollectionUtils.isEmpty(orderList)) {
                        List<Long> refundOrderIdList = new ArrayList<>();
                        for (Order order : orderList) {
                            refundOrderIdList.add(order.getId());
                        }
                        params.remove("orderNo");
                        params.put("idList", refundOrderIdList);
                    }
                }
                params.put("typeList", Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()));
            }
        } else {
            params.put("typeList", Arrays.asList(OrderTypeEnum.PAY.getCode(),
                    OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()));
        }
        if (StringUtils.isNotBlank(reqDTO.getCStartTime()) && StringUtils.isNotBlank(reqDTO.getCEndTime())) {
            params.put("cStartTime", DateUtil.stringtoDate(reqDTO.getCStartTime(), DateUtil.FORMAT_ONE));
            params.put("cEndTime", DateUtil.stringtoDate(reqDTO.getCEndTime(), DateUtil.FORMAT_ONE));
        }
        /*
         * 订单类型：
         * 0全部
         * 1分销商品订单
         * 2供应商品订单
         * 3自营商品订单
         * TODO 这里该不该解析呢
         */
        // 因为需要查询出分销订单，所以不能加商户id做查询条件
        if (reqDTO.getOrderType() != null) {
            if (reqDTO.getOrderType() == SellerOrderTypeEnum.DISTRIBUTION.getCode()) {
                params.put("shopId", reqDTO.getShopId());
                params.put("shopIdEqGoodsShopId", 0);
            }
            if (reqDTO.getOrderType() == SellerOrderTypeEnum.SUPPLY.getCode()) {
                params.put("goodsShopId", reqDTO.getShopId());
                params.put("shopIdEqGoodsShopId", 0);
            }
            if (reqDTO.getOrderType() == SellerOrderTypeEnum.SELF_SUPPORT.getCode()) {
                params.put("shopId", reqDTO.getShopId());
                params.put("shopIdEqGoodsShopId", 1);
            }
            if (reqDTO.getOrderType() == 0) {
                params.put("shopIdOrGoodsShopId", reqDTO.getShopId());
            }
        } else {
            params.put("shopIdOrGoodsShopId", reqDTO.getShopId());
        }
        params.put("page", reqDTO.getPage());
        params.put("size", reqDTO.getSize());
        params.put("limit", reqDTO.getSize());
        params.put("offset", reqDTO.getSize() * (reqDTO.getPage() - 1));
        // 查询包括用户已删除的订单
        return paramSimpleResponse;
    }

    /**
     * 查询订单详情
     *
     * @param reqDTO
     * @return
     */
    @Override
    public SellerOrderDetailResultDTO getOrderDetail(SellerOrderCommonReqDTO reqDTO) {
        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        Assert.isTrue(reqDTO.getShopId().equals(order.getShopId()) || reqDTO.getShopId().equals(order.getGoodsShopId()),
                ApiStatusEnum.ERROR_BUS_NO14.getCode(), "您无权查看此订单");
        Assert.notNull(order, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "查无订单信息！");
        Assert.isTrue(order.getType() == OrderTypeEnum.PAY.getCode(), ApiStatusEnum.ERROR_BUS_NO12.getCode(),
                "当前订单数据错误，请联系运营人员");
        SellerOrderDetailResultDTO resultDTO = new SellerOrderDetailResultDTO();
        resultDTO.setOrderId(order.getId().toString());
        resultDTO.setOrderNo(order.getOrderNo());
        resultDTO.setType(order.getType());
        resultDTO.setStatus(order.getStatus());
        int orderType = 0;
        // 订单类型： 1分销商品订单 2供应商品订单 3自营商品订单
        if (order.getShopId().equals(order.getGoodsShopId())) {
            // 自营商品
            orderType = SellerOrderTypeEnum.SELF_SUPPORT.getCode();
        } else if (order.getShopId().equals(reqDTO.getShopId())) {
            // 分销商品
            orderType = SellerOrderTypeEnum.DISTRIBUTION.getCode();
        } else {
            // 供应商品
            orderType = SellerOrderTypeEnum.SUPPLY.getCode();
        }
        resultDTO.setOrderType(orderType);
        // 结算，订单收货结算
        MerchantOrder merchantOrder = null;
        if (order.getStatus() == OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()) {
            QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(MerchantOrder::getMerchantId, reqDTO.getMerchantId())
                    .eq(MerchantOrder::getShopId, reqDTO.getShopId())
                    .eq(MerchantOrder::getBizType, BizTypeEnum.SHOPPING.getCode())
                    .eq(MerchantOrder::getBizValue, order.getId());
            List<MerchantOrder> list = merchantOrderRepository.list(queryWrapper);
            if (!CollectionUtils.isEmpty(list)) {
                // merchantOrder，同一bizType为更新操作，此处应为一条
                merchantOrder = list.get(0);
            }
        }
        // 订单top信息
        SellerTopInfo topInfo = changeOrderToSellerTopInfo(order, orderType, merchantOrder);
        resultDTO.setTopInfo(topInfo);

        // 订单详情
        Map<String, SellerProductAndSkuResultDTO> productAndSkuMap = new HashMap<>();
        SimpleResponse productAndSkuResponse = goodsAppService.queryProductSkuMapBySkuIdList(Arrays.asList(order.getSkuId()));
        if (productAndSkuResponse != null && !productAndSkuResponse.error()) {
            String jsonString = JSON.toJSONString(productAndSkuResponse.getData());
            productAndSkuMap = JSONObject.parseObject(jsonString, new TypeReference<HashMap<String, SellerProductAndSkuResultDTO>>() {
            });
        }
        SellerOrderInfo orderInfo = changeOrderToSellerOrderInfo(order, orderType, productAndSkuMap, merchantOrder);
        resultDTO.setOrderInfo(orderInfo);

        // 买家信息
        SellerUserAddressResultDTO userAddressResultDTO = null;
        SellerBuyerInfo buyerInfo = new SellerBuyerInfo();
        SimpleResponse userAndAddressResponse = userAppService.queryUserAndAddressForSeller(order.getUserId(), order.getAddressId());
        if (userAndAddressResponse != null && !userAndAddressResponse.error()) {
            String jsonString = JSON.toJSONString(userAndAddressResponse.getData());
            userAddressResultDTO = JSONObject.parseObject(jsonString, new TypeReference<SellerUserAddressResultDTO>() {
            });
            // 查询买家个人信息，和店铺名
            buyerInfo.setNick(userAddressResultDTO.getNick());
            buyerInfo.setAvatarUrl(userAddressResultDTO.getAvatarUrl());
            buyerInfo.setPhone(userAddressResultDTO.getPhone());
            String vipShopId = userAddressResultDTO.getVipShopId() == null ? order.getShopId().toString() : userAddressResultDTO.getVipShopId().toString();
            Map<String, Object> shopInfo = this.shopAppService.queryShopInfoForSeller(vipShopId);
            if (shopInfo.get("shopName") != null) {
                buyerInfo.setFromShopName(shopInfo.get("shopName").toString());
            }
        }
        buyerInfo.setBuyerMsg(order.getBuyerMsg());
        resultDTO.setBuyerInfo(buyerInfo);

        // 查询买家收货地址信息
        SellerLogisticsInfo logisticsInfo = changUserAddressToSellerLogisticsInfo(order, userAddressResultDTO);
        resultDTO.setLogisticsInfo(logisticsInfo);

        // 退款信息
        SellerRefundInfo refundInfo = changeRefundOrderToSellerRefundInfo(reqDTO, order, resultDTO, productAndSkuMap);
        resultDTO.setRefundInfo(refundInfo);

        // 其他信息
        SellerOtherInfo otherInfo = changeOrderLogToSellerOtherInfo(reqDTO, order, userAddressResultDTO);
        resultDTO.setOtherInfo(otherInfo);
        return resultDTO;
    }

    /**
     * 拼接退款信息
     *
     * @param reqDTO
     * @param order
     * @param resultDTO
     * @param productAndSkuMap
     * @return
     */
    private SellerRefundInfo changeRefundOrderToSellerRefundInfo(SellerOrderCommonReqDTO reqDTO, Order order, SellerOrderDetailResultDTO resultDTO, Map<String, SellerProductAndSkuResultDTO> productAndSkuMap) {
        SellerRefundInfo refundInfo = new SellerRefundInfo();
        List<SellerRefundInfoItem> refundList = new ArrayList<>();
        // 判断订单状态，编号倒叙查询退款记录,这里暂不优化，略麻烦
        List<Order> refundOrderList = goodsOrderRepository.selectRefundByPayOrderNo(reqDTO.getOrderNo());
        if (!CollectionUtils.isEmpty(refundOrderList)) {
            Order refundOrder = refundOrderList.get(0);
            resultDTO.setRefundType(refundOrderList.get(0).getType());
            resultDTO.setRefundStatus(refundOrderList.get(0).getStatus());
            // 这里依旧需要判断，如果支付订单状态为待发货，但是用户提出退款，商家拒绝后依然可以发货成功
            if (order.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()
                    && (refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()
                    || refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode())) {
                resultDTO.setRefundType(null);
                resultDTO.setRefundStatus(null);
            }
            for (Order refund : refundOrderList) {
                SellerRefundInfoItem refundInfoItem = changeRefundOrderToSellerRefundInfoItem(order, refund, productAndSkuMap);
                refundList.add(refundInfoItem);
            }
        }
        refundInfo.setRefundList(refundList);
        return refundInfo;
    }

    /**
     * 拼接订单其他信息
     *
     * @param reqDTO
     * @param order
     * @param userAddressResultDTO
     * @return
     */
    private SellerOtherInfo changeOrderLogToSellerOtherInfo(SellerOrderCommonReqDTO reqDTO, Order order, SellerUserAddressResultDTO userAddressResultDTO) {
        SellerOtherInfo otherInfo = new SellerOtherInfo();
        // 判断是否取消订单，再添加otherInfo
        if (OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode() == order.getStatus()
                || OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode() == order.getStatus()) {
            if (userAddressResultDTO != null) {
                otherInfo.setCancelName(userAddressResultDTO.getUserName());
            }
            // TODO 取消订单状态流转
            List<OrderLog> orderLogs = orderLogRepository.queryListForSeller(reqDTO.getShopId(), order.getId());
            StringBuilder sb = new StringBuilder();
            if (!CollectionUtils.isEmpty(orderLogs)) {
                for (OrderLog orderLog : orderLogs) {
                    if (orderLog.getType() == OrderTypeEnum.PAY.getCode()) {
                        OrderStatusEnum.findByCode(order.getStatus());
                        sb.append(OrderStatusEnum.findByCode(order.getStatus()).getMessage());
                    }
                }
            }
            otherInfo.setTypeChangeLog(sb.toString());
            otherInfo.setCancelTime(DateUtil.dateToString(order.getUtime(), DateUtil.FORMAT_ONE));
            otherInfo.setCancelReason(order.getBuyerMsg());
            if (OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode() == order.getStatus()) {
                otherInfo.setCancelReason("支付超时取消");
            }
        }
        return otherInfo;
    }

    /**
     * 拼接支付订单物流信息
     *
     * @param order
     * @param userAddressResultDTO
     * @return
     */
    private SellerLogisticsInfo changUserAddressToSellerLogisticsInfo(Order order, SellerUserAddressResultDTO userAddressResultDTO) {
        // 物流信息
        SellerLogisticsInfo logisticsInfo = new SellerLogisticsInfo();
        if (userAddressResultDTO != null) {
            logisticsInfo.setBuyerAddress(userAddressResultDTO.getCountry() + userAddressResultDTO.getProvince()
                    + userAddressResultDTO.getCity() + userAddressResultDTO.getStreet() + userAddressResultDTO.getDetail());
            logisticsInfo.setBuyerNick(userAddressResultDTO.getUserName());
            logisticsInfo.setBuyerPhone(userAddressResultDTO.getPhone());
        }
        if (StringUtils.isNotBlank(order.getLogistics())) {
            Map jsonMap = JSONObject.parseObject(order.getLogistics(), Map.class);
            if (jsonMap != null && jsonMap.get("name") != null && jsonMap.get("num") != null) {
                logisticsInfo.setLogisticsName(jsonMap.get("name").toString());
                logisticsInfo.setLogisticsNum(jsonMap.get("num").toString());
            }
        }
        return logisticsInfo;
    }


    /**
     * 拼接摘要信息
     *
     * @param order
     * @param orderType
     * @return
     */
    private SellerTopInfo changeOrderToSellerTopInfo(Order order, int orderType, MerchantOrder merchantOrder) {
        SellerTopInfo topInfo = new SellerTopInfo();
        topInfo.setType(order.getType());
        topInfo.setStatus(order.getStatus());
        topInfo.setBuyerMsg(order.getBuyerMsg());
        topInfo.setCreateTime(DateUtil.dateToString(order.getCtime(), DateUtil.FORMAT_ONE));
        topInfo.setOrderId(order.getId().toString());
        topInfo.setOrderNo(order.getOrderNo());
        topInfo.setOrderStatus(order.getStatus());
        topInfo.setOrderType(orderType);
        topInfo.setOriginalFee(order.getOriginalfee());
        topInfo.setSellerMsg(order.getSellerMsg());
        // 结算
        if (merchantOrder != null) {
            // 结算状态(0:待结算，1:已结算)
            topInfo.setSettleStatus(0);
            if (merchantOrder.getMfTradeType() == MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode()) {
                topInfo.setSettleStatus(1);
                topInfo.setSettleTime(DateUtil.dateToString(merchantOrder.getUtime(), DateUtil.FORMAT_ONE));
            }
            topInfo.setSettleAmount(merchantOrder.getTotalFee());
        }
        return topInfo;
    }

    /**
     * 拼接refund数据
     *
     * @param order
     * @param refundOrder
     * @param productAndSkuMap
     * @return
     */
    private SellerRefundInfoItem changeRefundOrderToSellerRefundInfoItem(Order order, Order refundOrder, Map<String, SellerProductAndSkuResultDTO> productAndSkuMap) {
        SellerRefundInfoItem refundInfoItem = new SellerRefundInfoItem();
        if (refundOrder.getSendTime() != null) {
            refundInfoItem.setBuyerSendTime(DateUtil.dateToString(refundOrder.getSendTime(), DateUtil.FORMAT_ONE));
        }
        refundInfoItem.setRefundAmount(refundOrder.getFee());
        refundInfoItem.setRefundNum(refundOrder.getNumber());

        refundInfoItem.setRefundType(refundOrder.getType());
        // 是否要汉字
        refundInfoItem.setRefundStatus(refundOrder.getStatus());
        refundInfoItem.setRefundTime(DateUtil.dateToString(refundOrder.getCtime(), DateUtil.FORMAT_ONE));
        refundInfoItem.setSellerDealTime(DateUtil.dateToString(refundOrder.getDealTime(), DateUtil.FORMAT_ONE));
        refundInfoItem.setProductId(order.getGoodsId().toString());
        if (!CollectionUtils.isEmpty(productAndSkuMap)) {
            for (SellerProductAndSkuResultDTO productAndSkuResultDTO : productAndSkuMap.values()) {
                refundInfoItem.setProductMainPics(JSONArray.parseArray(productAndSkuResultDTO.getProductSrcMainPics(), String.class).get(0));
                refundInfoItem.setProductTitle(productAndSkuResultDTO.getProductTitle());
            }
        }
        // 退款详情
        List<OrderReturnsApply> orderReturnsApplies = orderReturnsApplyRepository.selectById(refundOrder.getId());
        if (!CollectionUtils.isEmpty(orderReturnsApplies)) {
            for (OrderReturnsApply orderReturnsApply : orderReturnsApplies) {
                if (orderReturnsApply.getApplyType() == OrderReturnsApplyApplyTypeEnum.MERCHANT.getCode()) {
                    refundInfoItem.setAuditReason(orderReturnsApply.getWhy());
                    refundInfoItem.setAuditTime(DateUtil.dateToString(orderReturnsApply.getCtime(), DateUtil.FORMAT_ONE));
                    if (orderReturnsApply.getStatus() == OrderReturnsApplyStatusEnum.REFUSE.getCode()) {
                        refundInfoItem.setSellerResult("审核不通过");
                    }
                    if (orderReturnsApply.getStatus() == OrderReturnsApplyStatusEnum.PASS.getCode()) {
                        refundInfoItem.setSellerResult("审核通过");
                    }
                    if (StringUtils.isNotBlank(orderReturnsApply.getPic()) && JsonParseUtil.booJsonArr(orderReturnsApply.getPic())) {
                        refundInfoItem.setSellerVoucher(JSONArray.parseArray(orderReturnsApply.getPic(), String.class));
                    } else {
                        refundInfoItem.setSellerVoucher(Arrays.asList(orderReturnsApply.getPic()));
                    }
                }
                if (orderReturnsApply.getApplyType() == OrderReturnsApplyApplyTypeEnum.USER.getCode()) {
                    refundInfoItem.setBuyerResult(orderReturnsApply.getWhy());
                    if (StringUtils.isNotBlank(orderReturnsApply.getPic()) && JsonParseUtil.booJsonArr(orderReturnsApply.getPic())) {
                        refundInfoItem.setBuyerVoucher(JSONArray.parseArray(orderReturnsApply.getPic(), String.class));
                    } else {
                        refundInfoItem.setBuyerVoucher(Arrays.asList(orderReturnsApply.getPic()));
                    }
                    refundInfoItem.setRefundReason(orderReturnsApply.getWhy());
                    refundInfoItem.setRefundDesc(orderReturnsApply.getNote());
                }
            }
        }
        if (refundOrder.getStatus() == OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode()) {
            refundInfoItem.setRefundResult("退款/退货申请");
        }
        if (refundOrder.getStatus() == OrderStatusEnum.WILLSEND_OR_REFUND.getCode()) {
            refundInfoItem.setRefundResult("待买家发货");
        }
        if (refundOrder.getStatus() == OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()) {
            refundInfoItem.setRefundResult("退款/退货审核通过");
        }
        if (refundOrder.getStatus() == OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()) {
            refundInfoItem.setRefundResult("退款/退货完成");
        }
        if (refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode()) {
            refundInfoItem.setRefundResult("退款/退货撤销");
        }
        if (refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode()) {
            refundInfoItem.setRefundResult("退款/退货审核拒绝");
        }
        if (refundOrder.getStatus() == OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode()) {
            refundInfoItem.setRefundResult("退款超时");
        }
        return refundInfoItem;
    }

    /**
     * 拼接订单信息数据
     *
     * @param order
     * @param orderType
     * @param productAndSkuMap
     * @return
     */
    private SellerOrderInfo changeOrderToSellerOrderInfo(Order order, Integer orderType, Map<String, SellerProductAndSkuResultDTO> productAndSkuMap, MerchantOrder merchantOrder) {
        SellerOrderInfo orderInfo = new SellerOrderInfo();
        orderInfo.setType(order.getType());
        orderInfo.setStatus(order.getStatus());
        orderInfo.setBuyerMsg(order.getBuyerMsg());
        orderInfo.setOrderId(order.getId().toString());
        orderInfo.setOrderNo(order.getOrderNo());
        orderInfo.setOrderType(orderType);
        orderInfo.setOrderStatus(order.getStatus());
        orderInfo.setCreateTime(DateUtil.dateToString(order.getCtime(), DateUtil.FORMAT_ONE));
        orderInfo.setPaymentTime(order.getPaymentTime() == null ? "" : DateUtil.dateToString(order.getPaymentTime(), DateUtil.FORMAT_ONE));
        orderInfo.setSendTime(order.getSendTime() == null ? "" : DateUtil.dateToString(order.getSendTime(), DateUtil.FORMAT_ONE));
        orderInfo.setDealTime(order.getDealTime() == null ? "" : DateUtil.dateToString(order.getDealTime(), DateUtil.FORMAT_ONE));
        List<SellerOrderInfoProduct> sellerOrderInfoProductList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productAndSkuMap)) {
            // 添加productsku
            for (SellerProductAndSkuResultDTO productAndSkuResultDTO : productAndSkuMap.values()) {
                SellerOrderInfoProduct product = new SellerOrderInfoProduct();
                Map<String, Object> shopInfo = this.shopAppService.queryShopInfoForSeller(order.getShopId().toString());
                if (shopInfo.get("shopName") != null) {
                    product.setDealShopName(shopInfo.get("shopName").toString());
                }
                product.setFee(order.getFee());
                product.setNumber(order.getNumber());
                product.setOrderStatus(order.getStatus());
                product.setProductMainPics(JSONArray.parseArray(productAndSkuResultDTO.getProductSrcMainPics(), String.class).get(0));
                product.setProductTitle(productAndSkuResultDTO.getProductTitle());
                // order中shopId和goodsShopId来比较，判断是否添加为自营商品
                product.setProductSrc(productAndSkuResultDTO.getProductSrc());
                if (orderType == SellerOrderTypeEnum.DISTRIBUTION.getCode()) {
                    product.setProductSrc("分销商品");
                }
                if (orderType == SellerOrderTypeEnum.SUPPLY.getCode()) {
                    product.setProductSrc("自营商品");
                }
                if (orderType == SellerOrderTypeEnum.SELF_SUPPORT.getCode()) {
                    product.setProductSrc("自营商品");
                }
                product.setUnitAmount(productAndSkuResultDTO.getProductPrice());
                product.setSkuSpecs(productAndSkuResultDTO.getProductSpecsDesc());
                sellerOrderInfoProductList.add(product);
            }
        }
        orderInfo.setProductList(sellerOrderInfoProductList);
        orderInfo.setSellerMsg(order.getSellerMsg());
        // 结算
        if (merchantOrder != null) {
            // 结算状态(0:待结算，1:已结算)
            orderInfo.setSettleStatus(0);
            if (merchantOrder.getMfTradeType() == MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode()) {
                orderInfo.setSettleStatus(1);
                orderInfo.setSettleTime(DateUtil.dateToString(merchantOrder.getUtime(), DateUtil.FORMAT_ONE));
            }
            orderInfo.setSettleAmount(merchantOrder.getTotalFee());
        }
        return orderInfo;
    }

    /**
     * 查询物流信息
     *
     * @param reqDTO
     * @return
     */
    @Override
    public LogisticsInfoDto getLogistics(LogisticsSelectReqDTO reqDTO) {
        Order order = goodsOrderRepository.selectPayByOrderNo(reqDTO.getOrderNo());
        Assert.notNull(order, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "查无物流信息！");
        Assert.isBlank(order.getLogistics(), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "暂时没有查到物流信息！");
        Map<String, Object> logisticsMap = JSONObject.parseObject(order.getLogistics());
        Assert.notNull(logisticsMap.get("code"), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "该订单暂无物流信息");
        Assert.notNull(logisticsMap.get("num"), ApiStatusEnum.ERROR_BUS_NO14.getCode(), "该订单暂无物流单号");
        Map<String, Object> jsonMap = JSONObject.parseObject(order.getLogistics(), Map.class);
        //查询物流信息
        LogisticsInfoDto logisticsInfoDto = this.logisticsService.queryLogistics(logisticsMap.get("code").toString(), logisticsMap.get("num").toString());
        if (logisticsInfoDto == null) {
            logisticsInfoDto = new LogisticsInfoDto();
            //-1无此快递信息
            logisticsInfoDto.setStatus(-1);
        }
        logisticsInfoDto.setName(jsonMap.get("name").toString());
        logisticsInfoDto.setNum(logisticsMap.get("num").toString());
        return logisticsInfoDto;
    }

    /**
     * 查询物流公司列表
     *
     * @param reqDTO
     * @return
     */
    @Override
    public SellerLogisticsCompanyResultDTO getLogisticsCompany(SellerOrderCommonReqDTO reqDTO) {
        CommonDict commonDict = commonDictRepository.selectByTypeKey("logistics", "list");
        Assert.notNull(commonDict, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "暂无物流公司，请稍后查询");
        String logisticsCompany = commonDict.getValue1();
        List<LogisticsCompany> companyList = JSONArray.parseArray(logisticsCompany, LogisticsCompany.class);
        SellerLogisticsCompanyResultDTO resultDTO = new SellerLogisticsCompanyResultDTO();
        resultDTO.setLogisticsCompanyList(companyList);
        return resultDTO;
    }

    /**
     * 查询维权售后申请
     *
     * @param reqDTO
     * @return
     */
    @Override
    public SellerRefundInfoItem getRefundInfo(SellerOrderCommonReqDTO reqDTO) {
        List<Order> orderList = goodsOrderRepository.selectRefundByPayOrderNo(reqDTO.getOrderNo());
        Assert.isTrue(CollectionUtils.isEmpty(orderList), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "查询无此订单信息");
        // 待查询
        SellerRefundInfoItem sellerRefundInfoItem = new SellerRefundInfoItem();
        sellerRefundInfoItem.setAuditReason("");
        sellerRefundInfoItem.setAuditTime("");
        sellerRefundInfoItem.setBuyerResult("");
        sellerRefundInfoItem.setBuyerSendTime("");
        sellerRefundInfoItem.setBuyerVoucher(null);
        sellerRefundInfoItem.setProductId("");
        sellerRefundInfoItem.setProductMainPics("");
        sellerRefundInfoItem.setProductTitle("");
        sellerRefundInfoItem.setRefundAmount(BigDecimal.ZERO);
        sellerRefundInfoItem.setRefundDesc("");
        sellerRefundInfoItem.setRefundNum(1);
        sellerRefundInfoItem.setRefundReason("");
        sellerRefundInfoItem.setRefundTime("");
        sellerRefundInfoItem.setRefundType(1);
        sellerRefundInfoItem.setSellerDealTime("");
        sellerRefundInfoItem.setSellerResult("");
        sellerRefundInfoItem.setSellerVoucher(null);
        return sellerRefundInfoItem;
    }

    /**
     * 导出待发货订单数据
     *
     * @param reqDTO
     * @return
     */
    @Override
    public String export(SellerOrderListReqDTO reqDTO) {
        // 异步任务，redis存储任务锁。完成上传oss，设置定期删除oss，链接下发
        return "";
    }
}
