package mf.code.order.repo.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.order.dto.SellerOrderListReqDTO;
import mf.code.order.repo.po.Order;

import java.util.List;
import java.util.Map;

public interface GoodsOrderV6Repository  extends IService<Order> {
    /**
     * 商户端查询订单列表
     *
     * @param params
     * @return
     */
    Map<String, Object> pageOrderListForSeller(Map<String, Object> params, SellerOrderListReqDTO reqDTO);

    /**
     * 按照订单编号查询退款订单
     *
     * @param orderNo
     * @return
     */
    List<Order> selectRefundByPayOrderNo(String orderNo);

    /**
     * 商户端通过订单id列表查询订单数据
     *
     * @param orderIdList
     * @return
     */
    List<Order> selectOrderListByIdListForSeller(List<Long> orderIdList);

    /**
     * 按照订单编号查询退款订单,id倒叙不能变
     *
     * @param idList
     * @return
     */
    List<Order> selectRefundByPayOrderIdList(List<Long> idList);
}
