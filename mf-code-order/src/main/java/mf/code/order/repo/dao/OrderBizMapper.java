package mf.code.order.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.order.repo.po.OrderBiz;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderBizMapper extends BaseMapper<OrderBiz> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(OrderBiz record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderBiz record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    OrderBiz selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderBiz record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(OrderBiz record);
}
