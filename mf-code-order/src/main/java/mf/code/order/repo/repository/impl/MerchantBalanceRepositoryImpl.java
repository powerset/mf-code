package mf.code.order.repo.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import feign.Param;
import lombok.extern.slf4j.Slf4j;
import mf.code.order.repo.dao.MerchantBalanceMapper;
import mf.code.order.repo.po.MerchantBalance;
import mf.code.order.repo.repository.MerchantBalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.order.repo.repository.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月24日 21:01
 */
@Service
@Slf4j
public class MerchantBalanceRepositoryImpl extends ServiceImpl<MerchantBalanceMapper, MerchantBalance> implements MerchantBalanceRepository {
    @Autowired
    private MerchantBalanceMapper merchantBalanceMapper;

    /***
     * 金额减少更新
     * @param merchantBalance
     * @param lessDepositAdvance
     * @param lessDepositCash
     * @return
     */
    @Override
    public int lessUpdateByVersionId(MerchantBalance merchantBalance, BigDecimal lessDepositAdvance, BigDecimal lessDepositCash) {
        Map<String, Object> map = new HashMap<>();
        map.put("merchantId",merchantBalance.getMerchantId());
        map.put("shopId",merchantBalance.getShopId());
        map.put("customerId",merchantBalance.getCustomerId());
        map.put("utime",new Date());
        map.put("depositAdvance",lessDepositAdvance);
        map.put("depositCash",lessDepositCash);
        map.put("version",merchantBalance.getVersion());
        return this.merchantBalanceMapper.lessUpdateByVersionId(map);
    }

    /***
     * 金额增加更新
     * @param merchantBalance
     * @param addDepositAdvance
     * @param addDepositCash
     * @return
     */
    @Override
    public int addUpdateByVersionId(MerchantBalance merchantBalance, BigDecimal addDepositAdvance, BigDecimal addDepositCash) {
        Map<String, Object> map = new HashMap<>();
        map.put("merchantId",merchantBalance.getMerchantId());
        map.put("shopId",merchantBalance.getShopId());
        map.put("customerId",merchantBalance.getCustomerId());
        map.put("utime",new Date());
        map.put("depositAdvance",addDepositAdvance);
        map.put("depositCash",addDepositCash);
        map.put("version",merchantBalance.getVersion());
        return this.merchantBalanceMapper.addUpdateByVersionId(map);
    }

    @Override
    public int insertSelective(MerchantBalance merchantBalance){
        return this.merchantBalanceMapper.insertSelective(merchantBalance);
    }

    @Override
    public int updateBalance(Long merchantId, Long shopId, BigDecimal totalFee) {
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("totalFee", totalFee);
        params.put("utime", new Date());
        return this.merchantBalanceMapper.updateBalance(params);
    }
}
