package mf.code.order.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.order.domain.aggregateroot.AppletOrder;
import mf.code.order.dto.AppletOrderResp;
import mf.code.order.repo.dao.OrderBizMapper;
import mf.code.order.repo.dao.OrderMapper;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.po.OrderBiz;
import mf.code.order.repo.repository.AppletOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * mf.code.order.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 14:00
 */
@Slf4j
@Service
public class AppletOrderRepositoryImpl implements AppletOrderRepository {
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private OrderBizMapper orderBizMapper;

	/**
	 * 通过 id 获取
	 *
	 * @param orderId
	 * @return
	 */
	@Override
	public AppletOrder findById(Long orderId) {
		Order order = orderMapper.selectById(orderId);
		if (order == null) {
			return null;
		}
		QueryWrapper<OrderBiz> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(OrderBiz::getOrderId, orderId);
		List<OrderBiz> orderBizs = orderBizMapper.selectList(wrapper);

		// 装配 appletOrder
		AppletOrder appletOrder = new AppletOrder();
		appletOrder.setId(orderId);
		appletOrder.setOrderInfo(order);
		appletOrder.setOrderBizList(orderBizs);
		return appletOrder;
	}
}
