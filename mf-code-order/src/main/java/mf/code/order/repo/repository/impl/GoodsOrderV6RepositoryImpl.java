package mf.code.order.repo.repository.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.comment.CommentStatusEnum;
import mf.code.comment.dto.OrderCommentDTO;
import mf.code.common.DelEnum;
import mf.code.order.api.feignclient.CommentAppService;
import mf.code.order.dto.SellerOrderListReqDTO;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.dao.OrderMapper;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderV6Repository;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@Slf4j
public class GoodsOrderV6RepositoryImpl extends ServiceImpl<OrderMapper, Order> implements GoodsOrderV6Repository {
    /**
     * 评价状态：0全部
     */
    private static final String IS_CHECKED_ALL = "0";
    /**
     * 评价状态：1待评价，
     */
    private static final String IS_CHECKED_NO = "1";
    /**
     * 评价状态：2已评价
     */
    private static final String IS_CHECKED_YES = "2";

    /**
     * 订单状态：交易成功
     */
    private static final int ORDER_STATUS_SUCCESS = 3;

    @Autowired
    private OrderMapper goodsOrderMapper;
    @Autowired
    private CommentAppService commentAppService;

    /**
     * 商户端查询订单列表
     *
     * @param params
     * @return
     */
    @Override
    public Map<String, Object> pageOrderListForSeller(Map<String, Object> params, SellerOrderListReqDTO reqDTO) {
        log.info(">>>>>>>>>>>>>info pageOrderListForSeller");
        Map<String, Object> result = new HashMap<>();
        if (CollectionUtils.isEmpty(params)) {
            return result;
        }
        Long count = 0L;
        List<Order> orderList = new ArrayList<>();

        result.put("count", count);
        result.put("list", orderList);

        //此处做修改，根据评论状态查询评论信息，评论信息作为主表，反查订单表信息
        //当checkStatus=0时，走原来的老流程，查询全部
        String checkStatus = (String) params.get("checkStatus");
        if (StringUtils.equals(IS_CHECKED_ALL, checkStatus)) {
            count = goodsOrderMapper.countOrderListForSeller(params);
            orderList = goodsOrderMapper.pageOrderListForSeller(params);
            result.put("count", count);
            result.put("list", orderList);
            return result;
        }

        Long shopId = NumberUtils.toLong(reqDTO.getShopId());
        int page = Integer.valueOf(params.get("page").toString());
        int size = Integer.valueOf(params.get("size").toString());

        //分装查询条件查询评论列表
        List<OrderCommentDTO> orderCommentDTOList = new ArrayList<>();
        if (StringUtils.equals(IS_CHECKED_NO, checkStatus)) {
            orderCommentDTOList = getUnCheckedOrderList(shopId, page, size);
            //count = getUnCheckedOrderListNUm(shopId);
        } else if (StringUtils.equals(IS_CHECKED_YES, checkStatus)) {
            orderCommentDTOList = getCheckedOrderList(shopId, page, size);
            //count = getCheckedOrderListNum(shopId);
        } else {
            return result;
        }

        //获取订单id，并替换请求中的orderIdList
        if (CollectionUtils.isEmpty(orderCommentDTOList)) {
            log.info(">>>>>>>>>>>>>>>> 1555151 orderCommentDTOList is null");
            return result;
        }

        List<Long> orderIdList = new ArrayList<>();
        for (OrderCommentDTO d : orderCommentDTOList) {
            Long orderId = d.getOrderId();
            orderIdList.add(orderId);
        }

        if (CollectionUtils.isEmpty(orderIdList)) {
            log.info(">>>>>>>>>>>>123126 have find d orderIdList is null ");
            return result;
        }

        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>11212 orderIdList = " + JSON.toJSONString(orderIdList));
        params.put("orderIdList", orderIdList);
        orderList = goodsOrderMapper.pageOrderListForSellerV6(params);

        count = goodsOrderMapper.countOrderListForSellerV6(params);
        log.info(">>>>>>>>>>>>>>> 5656 have find orderList=" + JSON.toJSONString(orderList) + ",count=" + count);

        result.put("count", count);
        result.put("list", orderList);
        return result;
    }


    /**
     * 获取未评价列表size
     *
     * @param shopId
     * @return
     */
    private Long getUnCheckedOrderListNUm(Long shopId) {
        List<Long> orderIdList = getOrderList(shopId);
        List<Integer> commnetStatusList = new ArrayList<>();
        commnetStatusList.add(CommentStatusEnum.INIT.getCode());
        if (CollectionUtils.isEmpty(orderIdList)) {
            return 0L;
        }
        return commentAppService.getCountOfCommentListNum(orderIdList, commnetStatusList, shopId);
    }

    /**
     * 查询未评价的订单列表
     * 待评价订单列表：先查询订单表，获取7天内交易成功的订单orderIdList(7天后都自动评价成功)，不分页，然后在查询评论表，分页获取评论标的commentList，
     *
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    private List<OrderCommentDTO> getUnCheckedOrderList(Long shopId, int page, int size) {
        List<Long> orderIdList = getOrderList(shopId);
        if (CollectionUtils.isEmpty(orderIdList)) {
            log.info(">>>>> 13 0have orderIdList is null");
            return null;
        }
        List<Integer> statusList = new ArrayList<>();
        statusList.add(CommentStatusEnum.INIT.getCode());

        //根据shopId和订单id分页查询评论信息
        List<OrderCommentDTO> orderCommentDTOList = commentAppService.getCommentListByShopAndOrderIds(orderIdList, statusList, shopId, 1, 500);

        if (CollectionUtils.isEmpty(orderCommentDTOList)) {

            log.info(">>>>>>>>>>> 00 have find orderCommentDTOList is null");
            return null;
        }

        return orderCommentDTOList;
    }

    /**
     * 获取OrderId
     *
     * @param shopId
     * @return
     */
    private List<Long> getOrderList(Long shopId) {
        //获取过去7天的时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 7);
        Date passDay = calendar.getTime();

        List<Integer> orderStatus = new ArrayList<>();
        orderStatus.add(ORDER_STATUS_SUCCESS);

        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(Order::getStatus, orderStatus)
                .eq(Order::getDel, DelEnum.NO.getCode())
                .eq(Order::getShopId, shopId)
                .between(Order::getCtime, passDay, new Date());

        List<Order> orderList = goodsOrderMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(orderList)) {
            return null;
        }

        List<Long> orderIdList = new ArrayList<>();

        //获取orderList
        for (Order o : orderList) {
            Long orderId = o.getId();
            orderIdList.add(orderId);
        }

        if (CollectionUtils.isEmpty(orderIdList)) {
            return null;
        }
        return orderIdList;
    }

    /**
     * 查询已评价的订单列表
     * 已评价订单列表：先分页查询评论表中已审核通过的commentList，然后在查询order表，获取orderList
     *
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    private List<OrderCommentDTO> getCheckedOrderList(Long shopId, int page, int size) {
        List<Integer> statusList = new ArrayList<>();
        statusList.add(CommentStatusEnum.COMMENT.getCode());
        statusList.add(CommentStatusEnum.PASS.getCode());
        statusList.add(CommentStatusEnum.FAIL.getCode());
        List<OrderCommentDTO> orderCommentDTOList = commentAppService.getCommentListByShopAndOrderIds(null, statusList, shopId, page, size);
        if (CollectionUtils.isEmpty(orderCommentDTOList)) {
            log.info(">>>>>>>>>>11 getCheckedOrderList  orderCommentDTOList is null");
            return null;
        }

        return orderCommentDTOList;
    }

    /**
     * 获取已评论的订单列表size
     *
     * @param shopId
     * @return
     */
    private Long getCheckedOrderListNum(Long shopId) {
        List<Integer> statusList = new ArrayList<>();
        statusList.add(CommentStatusEnum.PASS.getCode());
        return commentAppService.getCountOfCommentListNum(null, statusList, shopId);
    }


    /**
     * 按照订单编号查询退款订单,倒叙查询
     *
     * @param orderNo 原支付订单编号
     * @return 退款订单列表
     */
    @Override
    public List<Order> selectRefundByPayOrderNo(String orderNo) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getOrderNo, orderNo)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .eq(Order::getDel, DelEnum.NO.getCode());
        Order payOrder = goodsOrderMapper.selectOne(wrapper);
        if (payOrder == null) {
            return new ArrayList<>();
        }
        QueryWrapper<Order> refundWrapper = new QueryWrapper<>();
        refundWrapper.lambda()
                .eq(Order::getRefundOrderId, payOrder.getId())
                .in(Order::getType, Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()))
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        refundWrapper.orderByDesc("id");
        return goodsOrderMapper.selectList(refundWrapper);
    }

    /**
     * 商户端通过订单id列表查询订单数据
     *
     * @param orderIdList
     * @return
     */
    @Override
    public List<Order> selectOrderListByIdListForSeller(List<Long> orderIdList) {
        if (CollectionUtils.isEmpty(orderIdList)) {
            return new ArrayList<>();
        }
        return goodsOrderMapper.selectBatchIds(orderIdList);
    }

    /**
     * 按照订单编号查询退款订单,id倒叙不能变
     *
     * @param idList
     * @return
     */
    @Override
    public List<Order> selectRefundByPayOrderIdList(List<Long> idList) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Order::getRefundOrderId, idList)
                .orderByDesc(Order::getId);
        return goodsOrderMapper.selectList(wrapper);
    }

}
