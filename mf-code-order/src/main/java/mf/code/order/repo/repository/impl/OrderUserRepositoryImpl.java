package mf.code.order.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.DateUtil;
import mf.code.order.api.feignclient.UserAppService;
import mf.code.order.domain.aggregateroot.OrderUser;
import mf.code.order.domain.valueobject.UserOrderHistory;
import mf.code.order.dto.OrderResp;
import mf.code.order.feignapi.constant.OrderBizBizTypeEnum;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.dao.OrderMapper;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.OrderUserRepository;
import mf.code.user.dto.UserResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.order.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 10:57
 */
@Slf4j
@Service
public class OrderUserRepositoryImpl implements OrderUserRepository {
	@Autowired
	private UserAppService userAppService;
	@Autowired
	private OrderMapper orderMapper;

	/**
	 * 通过 shopId, userId 获取
	 *
	 * @param shopId
	 * @param userId
	 * @return 不含用户订单历史记录
	 */
	@Override
	public OrderUser findByShopIdUserId(Long shopId, Long userId) {
		UserResp userRespDTO = userAppService.queryUser(userId);
		if (userRespDTO == null) {
			log.error("无效用户, userId = {}", userId);
			return null;
		}

		return assemblyOrderUser(shopId, userRespDTO);
	}

	/**
	 * 增装 用户历史订单记录
	 *
	 * @param orderUser
	 * @param beginTime
	 * @param endTime
	 */
	@Override
	public void addUserOrderHistory(OrderUser orderUser, Date beginTime, Date endTime) {
		Long shopId = orderUser.getShopId();
		Long userId = orderUser.getUserId();
		// 获取 此用户在此店铺下 某段时间的 已支付的订单
		List<OrderResp> paidOrders = this.getPaidOrdersWithBiz(shopId, userId, beginTime, endTime);
		// 获取 此用户在此店铺下 beginTime 到现在的 售后处理中的订单
		List<Order> afterSaleProcessingOrders = this.getAfterSaleProcessingOrders(shopId, userId, beginTime, new Date());
		// 获取 此用户 所有店铺 上月消费订单的金额总和
		BigDecimal lastSpendAmount = this.getSpendAmount(userId, DateUtil.getBeginOfLastMonth(), DateUtil.getEndOfLastMonth());
		// 获取 此用户 指定时间周期的 所有店铺消费的订单金额总和
		BigDecimal spendAmount = this.getSpendAmount(userId, beginTime, endTime);

		UserOrderHistory userOrderHistory = new UserOrderHistory();
		userOrderHistory.setUserId(userId);
		userOrderHistory.setShopId(shopId);
		userOrderHistory.setBeginTime(beginTime);
		userOrderHistory.setEndTime(endTime);
		userOrderHistory.setLastSpendAmount(lastSpendAmount);
		userOrderHistory.setSpendAmount(spendAmount);
		userOrderHistory.setPaidOrder(paidOrders);
		userOrderHistory.setAfterSaleProcessing(afterSaleProcessingOrders);

		orderUser.setUserOrderHistory(userOrderHistory);
	}

	/**
	 * 批量获取 同一店铺下 的订单用户
	 * @param shopId
	 * @param userIds
	 * @return
	 */
	@Override
	public List<OrderUser> listByShopIdUserList(Long shopId, List<Long> userIds) {
		List<UserResp> users = userAppService.listUser(userIds);
		if (CollectionUtils.isEmpty(users)) {
			log.error("user服务器异常，获取用户信息失败, userIds = {}", userIds);
			return null;
		}

		List<OrderUser> orderUsers = new ArrayList<>();
		for (UserResp user : users) {
			orderUsers.add(assemblyOrderUser(shopId, user));
		}

		return orderUsers;
	}

	/**
	 * 批量获取 用户 订单记录
	 * @param orderUsers
	 * @param shopId
	 * @param beginTime
	 * @param endTime
	 */
	@Override
	public void listAddUserOrderHistory(List<OrderUser> orderUsers, Long shopId, Date beginTime, Date endTime) {
		List<Long> userIds = new ArrayList<>();
		for (OrderUser orderUser : orderUsers) {
			userIds.add(orderUser.getUserId());
		}

		Map<Long, UserOrderHistory> userOrderHistoryMap = this.listUserOrderHistory(userIds, shopId, beginTime, endTime);

		for (OrderUser orderUser : orderUsers) {
			Long userId = orderUser.getUserId();
			UserOrderHistory userOrderHistory = userOrderHistoryMap.get(userId);
			if (userOrderHistory == null) {
				userOrderHistory = new UserOrderHistory();
			}
			orderUser.setUserOrderHistory(userOrderHistory);
		}
	}

	/**
	 * 批量获取 当前店铺下 某时间段的 用户的消费订单
	 * @param userIds
	 * @param shopId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	private Map<Long, UserOrderHistory> listUserOrderHistory(List<Long> userIds, Long shopId, Date beginTime, Date endTime) {
		// 获取 此用户在此店铺下 某段时间的 已支付的订单 Map<userId, List<Order>>
		Map<Long, List<OrderResp>> userOrdersMap = this.listPaidOrdersWithBiz(shopId, userIds, beginTime, endTime);

		// 获取 此用户在此店铺下 beginTime 到现在的 售后处理中的订单 Map<userId, List<Order>>
		Map<Long, List<Order>> afterSaleProcessingOrders = this.listAfterSaleProcessingOrders(shopId, userIds, beginTime, new Date());

		// 获取 上月消费金额 Map<userId, BigDecimal>
		Map<Long, BigDecimal> lastSpendAmountMap = this.listSpendAmount(shopId, userIds, DateUtil.getBeginOfLastMonth(), DateUtil.getEndOfLastMonth());

		// 获取 上月消费金额 Map<userId, BigDecimal>
		Map<Long, BigDecimal> spendAmountMap = this.listSpendAmount(shopId, userIds, beginTime, endTime);

		Map<Long, UserOrderHistory> userOrderHistoryMap = new HashMap<>();
		for (Long userId : userIds) {
			BigDecimal lastSpendAmount = lastSpendAmountMap.get(userId);
			BigDecimal spendAmount = spendAmountMap.get(userId);

			UserOrderHistory userOrderHistory = new UserOrderHistory();
			userOrderHistory.setUserId(userId);
			userOrderHistory.setShopId(shopId);
			userOrderHistory.setBeginTime(beginTime);
			userOrderHistory.setEndTime(endTime);
			userOrderHistory.setLastSpendAmount(lastSpendAmount == null ? BigDecimal.ZERO : lastSpendAmount);
			userOrderHistory.setSpendAmount(spendAmount == null ? BigDecimal.ZERO : spendAmount);
			userOrderHistory.setPaidOrder(userOrdersMap.get(userId));
			userOrderHistory.setAfterSaleProcessing(afterSaleProcessingOrders.get(userId));

			userOrderHistoryMap.put(userId, userOrderHistory);
		}

		return userOrderHistoryMap;
	}

	/**
	 * 批量 获取 用户们 上月在店铺的消费金额
	 * @param shopId
	 * @param userIds
	 * @return
	 */
	private Map<Long, BigDecimal> listSpendAmount(Long shopId, List<Long> userIds, Date beginTime, Date endTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("userIdList", userIds);
		params.put("type", OrderTypeEnum.PAY.getCode());
		params.put("statusList", Arrays.asList(OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
				OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
				OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
		params.put("beginTime", beginTime);
		params.put("endTime", endTime);
		List<Map> userTotalFeeList = this.orderMapper.sumFeeByPaymentTime(params);
		if (CollectionUtils.isEmpty(userTotalFeeList)) {
			return new HashMap<>();
		}

		Map<Long, BigDecimal> spendingAmountMap = new HashMap<>();
		for (Map map : userTotalFeeList) {
			Object userId = map.get("userId");
			Object totalFee = map.get("totalFee");
			if (userId == null || totalFee == null) {
				continue;
			}
			spendingAmountMap.put(Long.valueOf(userId.toString()), new BigDecimal(totalFee.toString()));
		}
		return spendingAmountMap;
	}

	/**
	 * 批量获取 此用户在此店铺下 beginTime 到现在的 售后处理中的订单
	 *
	 * @param shopId
	 * @param userIds
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	private Map<Long, List<Order>> listAfterSaleProcessingOrders(Long shopId, List<Long> userIds, Date beginTime, Date endTime) {
		QueryWrapper<Order> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.in(Order::getUserId, userIds)
				.eq(Order::getShopId, shopId)
				.ne(Order::getType, OrderTypeEnum.PAY.getCode())
				.in(Order::getStatus, Arrays.asList(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode(),
						OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
						OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()))
				.between(Order::getCtime, beginTime, endTime);
		// 顺序是为了确定 哪张订单 需要进行金额拆分
		wrapper.orderByAsc("ctime");
		List<Order> orderList = orderMapper.selectList(wrapper);

		if (CollectionUtils.isEmpty(orderList)) {
			return new HashMap<>();
		}
		Map<Long, List<Order>> userOrdersMap = new HashMap<>();
		for (Order order : orderList) {
			Long userId = order.getUserId();
			List<Order> orders = userOrdersMap.get(userId);
			if (CollectionUtils.isEmpty(orders)) {
				orders = new ArrayList<>();
			}
			orders.add(order);
			userOrdersMap.put(userId, orders);
		}
		return userOrdersMap;
	}

	/**
	 * 批量获取 此用户在此店铺下 某段时间的 已支付的订单
	 *
	 * @param shopId
	 * @param userIds
	 * @param beginTime
	 * @param endTime
	 * @return Map<userId, List<Order>>
	 */
	private Map<Long, List<OrderResp>> listPaidOrdersWithBiz(Long shopId, List<Long> userIds, Date beginTime, Date endTime) {

		Map<String, Object> params = new HashMap<>();
		params.put("userIdList", userIds);
		params.put("shopId", shopId);
		params.put("type", OrderTypeEnum.PAY.getCode());
		params.put("statusList", Arrays.asList(OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
				OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
				OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
		params.put("beginTime", beginTime);
		params.put("endTime", endTime);
		params.put("bizType", OrderBizBizTypeEnum.DISTRIBUTION.getCode());
		List<OrderResp> orderRespList = orderMapper.selectListWithBiz(params);

		if (CollectionUtils.isEmpty(orderRespList)) {
			return new HashMap<>();
		}
		Map<Long, List<OrderResp>> userOrdersMap = new HashMap<>();
		for (OrderResp order : orderRespList) {
			Long userId = order.getUserId();
			List<OrderResp> orders = userOrdersMap.get(userId);
			if (CollectionUtils.isEmpty(orders)) {
				orders = new ArrayList<>();
			}
			orders.add(order);
			userOrdersMap.put(userId, orders);
		}
		return userOrdersMap;
	}

	/**
	 * 获取 此用户在此店铺下 beginTime 到现在的 售后处理中的订单
	 *
	 * @param shopId
	 * @param userId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	private List<Order> getAfterSaleProcessingOrders(Long shopId, Long userId, Date beginTime, Date endTime) {
		QueryWrapper<Order> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Order::getUserId, userId)
				.eq(Order::getShopId, shopId)
				.ne(Order::getType, OrderTypeEnum.PAY.getCode())
				.in(Order::getStatus, Arrays.asList(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode(),
						OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
						OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode()))
				.between(Order::getCtime, beginTime, endTime);
		// 顺序是为了确定 哪张订单 需要进行金额拆分
		wrapper.orderByAsc("ctime");
		List<Order> orders = orderMapper.selectList(wrapper);

		return orders == null ? new ArrayList<>() : orders;
	}

	/**
	 * 获取 此用户在此店铺下 某段时间的 已支付的订单
	 *
	 * @param shopId
	 * @param userId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	private List<OrderResp> getPaidOrdersWithBiz(Long shopId, Long userId, Date beginTime, Date endTime) {

		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("shopId", shopId);
		params.put("type", OrderTypeEnum.PAY.getCode());
		params.put("statusList", Arrays.asList(OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
				OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
				OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
		params.put("beginTime", beginTime);
		params.put("endTime", endTime);
		params.put("bizType", OrderBizBizTypeEnum.DISTRIBUTION.getCode());
		List<OrderResp> orders = orderMapper.selectListWithBiz(params);

		return orders == null ? new ArrayList<>() : orders;
	}

	// 获取 现在日期的 上月消费金额
	private BigDecimal getSpendAmount(Long userId, Date beginTime, Date endTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("type", OrderTypeEnum.PAY.getCode());
		params.put("statusList", Arrays.asList(OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
				OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
				OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
		params.put("beginTime", beginTime);
		params.put("endTime", endTime);
		List<Map> userTotalFeeList = this.orderMapper.sumFeeByPaymentTime(params);
		if (CollectionUtils.isEmpty(userTotalFeeList)) {
			return BigDecimal.ZERO;
		} else {
			Map map = userTotalFeeList.get(0);
			Object totalFee = map.get("totalFee");
			return new BigDecimal(totalFee.toString());
		}
	}

	// 装配 用户信息 到 orderUser
	private OrderUser assemblyOrderUser(Long shopId, UserResp userRespDTO) {
		OrderUser orderUser = new OrderUser();
		orderUser.setShopId(shopId);
		orderUser.setUserId(userRespDTO.getId());
		orderUser.setUserInfo(userRespDTO);

		return orderUser;
	}
}
