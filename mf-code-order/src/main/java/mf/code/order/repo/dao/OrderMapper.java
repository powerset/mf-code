package mf.code.order.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.order.dto.OrderResp;
import mf.code.order.repo.po.Order;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface OrderMapper extends BaseMapper<Order> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(Order record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(Order record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    Order selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Order record);
    /**
     * 根据店铺id,产品id统计订单数
     */
    Long summaryOrderByShopIdAndProductId(Map<String, Object> conditionMap);

    /**
     * 查询 上月的总消费
     * @param params
     * @return
     *
     * 查询条件：
     * <per>
     *     userId or userIdList
     *     shopId
     *     type = 商品购买-付款
     *     status = 支付成功
     *     payment_time = 上月
     * </per>
     */
    List<Map> sumFeeByPaymentTime(Map<String, Object> params);

    int updateBySelectiveForAtomicity(Map<String, Object> params);

    /**
     * 商户后台查询订单列表
     *
     * @param params
     * @return
     */
    List<Order> pageOrderListForSeller(Map<String, Object> params);

    /**
     * 商户后台查询订单列表 --重构
     * @param params
     * @return
     */
    List<Order> pageOrderListForSellerV6(Map<String, Object> params);

    /**
     * 商户后台查询订单列表总数
     *
     * @param params
     * @return
     */
    Long countOrderListForSeller(Map<String, Object> params);

    Long countOrderListForSellerV6(Map<String, Object> params);

    /**
     * 联表查询 order表，orderBiz表。
     * @param params
     * <per>
     *     params.put("userId", userId); 获 userIdList
     * 		params.put("shopId", shopId);
     * 		params.put("type", OrderTypeEnum.PAY.getCode());
     * 		params.put("statusList", Arrays.asList(OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
     * 				OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
     * 				OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()));
     * 		params.put("beginTime", beginTime);
     * 		params.put("endTime", endTime);
     * 	    默认 -- payment_time 正序
     * </per>
     * @return
     */
	List<OrderResp> selectListWithBiz(Map<String, Object> params);



}
