package mf.code.order.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.order.repo.dao.MerchantMapper;
import mf.code.order.repo.po.Merchant;
import mf.code.order.repo.po.MerchantOrder;
import mf.code.order.repo.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * merchant
 * 商户
 */
@Service
@Slf4j
public class MerchantRepositoryImpl implements MerchantRepository {
    @Autowired
    private MerchantMapper merchantMapper;

    /**
     * 主键查询商户信息
     *
     * @param id
     * @return
     */
    @Override
    public Merchant getMerchant(Long id) {
        return merchantMapper.selectByPrimaryKey(id);
    }

    /***
     * 获取商户的父商户编号
     * @param merchantId
     * @return
     */
    @Override
    public Long getParentMerchantId(Long merchantId) {
        Merchant merchant = this.getMerchant(merchantId);
        if (merchant != null && merchant.getParentId() != null && merchant.getParentId() > 0) {
            return merchant.getParentId();
        }
        return merchantId;
    }

    @Override
    public Merchant getbyPhone(String phone) {
        QueryWrapper<Merchant> wrapper = new QueryWrapper();
        wrapper.lambda()
                .eq(Merchant::getPhone, phone)
                .eq(Merchant::getDel, DelEnum.NO.getCode())
        ;
        wrapper.orderByDesc("id");
        List<Merchant> merchants = this.merchantMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(merchants)) {
            return null;
        }
        return merchants.get(0);
    }

    @Override
    public Map<Long, Merchant> getMerchantByMerchantOrder(List<MerchantOrder> merchantOrderList) {
        if (CollectionUtils.isEmpty(merchantOrderList)) {
            return new HashMap<>();
        }
        List<Long> merchantIds = new ArrayList<>();
        for (MerchantOrder merchantOrder : merchantOrderList) {
            if (merchantIds.indexOf(merchantOrder.getMerchantId()) == -1) {
                merchantIds.add(merchantOrder.getMerchantId());
            }
        }
        QueryWrapper<Merchant> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Merchant::getId, merchantIds)
                .eq(Merchant::getDel, DelEnum.NO.getCode());

        List<Merchant> merchants = this.merchantMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(merchants)) {
            return new HashMap<>();
        }

        Map<Long, Merchant> merchantMap = new HashMap<>();
        for (Merchant merchant : merchants) {
            merchantMap.put(merchant.getId(), merchant);
        }
        return merchantMap;
    }
}