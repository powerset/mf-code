package mf.code.order.repo.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.order.repo.po.OrderBiz;

/**
 * mf.code.order.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 12:46
 */
public interface OrderBizRepository extends IService<OrderBiz> {
	int insertSelective(OrderBiz orderBiz);

    /**
     * 根据订单id查询订单biz分销数据
     *
     * @param orderId
     * @return
     */
    OrderBiz selectByOrderId(Long orderId);
}
