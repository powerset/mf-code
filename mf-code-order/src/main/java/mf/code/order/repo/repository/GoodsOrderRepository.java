package mf.code.order.repo.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.order.repo.po.Order;

import java.util.List;
import java.util.Map;

/**
 * mf.code.order.repo.repository
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 17:49
 */
public interface GoodsOrderRepository extends IService<Order> {

    Order selectById(Long orderId);

    Integer insertSelective(Order goodsOrder);

    Integer updateSelective(Order goodsOrder);

    int deleteById(Long id);

    List<Order> queryList(QueryWrapper<Order> wrapper);

    int countByWrapper(QueryWrapper<Order> wrapper);

    IPage<Order> queryPage(Page page, QueryWrapper<Order> wrapper);

    int countByGoodsIdsByOrder(Long goodsId);

    /***
     * 根据店铺，用户，状态 获取条数
     * @param shopId
     * @param userId
     * @param orderStatus
     * @return
     */
    int countByStatusByShopUser(Long shopId, Long userId, int orderStatus);

    int updateByPrimaryKeySelective(Order order);

    Order selectOne(QueryWrapper<Order> orderWrapper);

    int countByGoodsIdsByRefundOrderId(Long orderId, List<Integer> orderStatus);

    /**
     * 按照订单编号查询订单
     *
     * @param orderNo
     * @return
     */
    Order selectPayByOrderNo(String orderNo);

    /**
     * 按照订单编号查询退款订单
     *
     * @param orderNo
     * @return
     */
    List<Order> selectRefundByPayOrderNo(String orderNo);

    /**
     * 按照订单编号查询退款订单,id倒叙不能变
     *
     * @param idList
     * @return
     */
    List<Order> selectRefundByPayOrderIdList(List<Long> idList);

    /**
     * 根据店铺id,产品id统计订单数
     */
    Long summaryOrderByShopIdAndProductId(Map<String, Object> conditionMap);

    /***
     * 原子性更新状态
     * @param params
     * @return
     */
    int updateBySelectiveForAtomicity(Map<String, Object> params);

    /**
     * 商户端查询订单列表
     *
     * @param params
     * @return
     */
    Map<String, Object> pageOrderListForSeller(Map<String, Object> params);

    /**
     * 商户端通过订单id列表查询订单数据
     *
     * @param orderIdList
     * @return
     */
    List<Order> selectOrderListByIdListForSeller(List<Long> orderIdList);

    /***
     * 根据商品id，查询该商品是否购买过
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    int selectCountOrderByProductId(Long merchantId, Long shopId, Long userId, Long goodsId);

    /**
     * 根据免单任务查询用户是否已下单
     *
     * @param discountType
     * @param discountId
     * @param userId
     * @return
     */
    List<Order> selectListByDiscountTypeAndId(Integer discountType, Long discountId, Long userId);
}
