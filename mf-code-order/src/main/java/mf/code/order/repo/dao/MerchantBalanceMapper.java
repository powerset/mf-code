package mf.code.order.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import feign.Param;
import mf.code.order.repo.po.MerchantBalance;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@Repository
public interface MerchantBalanceMapper extends BaseMapper<MerchantBalance> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(MerchantBalance record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(MerchantBalance record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    MerchantBalance selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(MerchantBalance record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(MerchantBalance record);

    int lessUpdateByVersionId(Map param);

    int addUpdateByVersionId(Map param);

    int updateBalance(Map<String, Object> param);
}
