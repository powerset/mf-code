package mf.code.order.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.common.DelEnum;
import mf.code.common.utils.DateUtil;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.order.feignapi.constant.OrderTypeEnum;
import mf.code.order.repo.dao.OrderMapper;
import mf.code.order.repo.po.Order;
import mf.code.order.repo.repository.GoodsOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.order.repo.repository.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 17:49
 */
@Service
public class GoodsOrderRepositoryImpl extends ServiceImpl<OrderMapper, Order> implements GoodsOrderRepository {
    @Autowired
    private OrderMapper goodsOrderMapper;

    @Override
    public Order selectById(Long orderId) {
        return this.goodsOrderMapper.selectByPrimaryKey(orderId);
    }

    @Override
    public Integer insertSelective(Order goodsOrder) {
        goodsOrder.setCtime(new Date());
        goodsOrder.setUtime(new Date());
        return this.goodsOrderMapper.insertSelective(goodsOrder);
    }

    @Override
    public Integer updateSelective(Order goodsOrder) {
        goodsOrder.setUtime(new Date());
        return this.goodsOrderMapper.updateByPrimaryKeySelective(goodsOrder);
    }

    @Override
    public int deleteById(Long id) {
        return this.goodsOrderMapper.deleteById(id);
    }

    @Override
    public List<Order> queryList(QueryWrapper<Order> wrapper) {
        List<Order> goodsOrders = this.goodsOrderMapper.selectList(wrapper);
        return goodsOrders;
    }

    @Override
    public int countByWrapper(QueryWrapper<Order> wrapper) {
        return this.goodsOrderMapper.selectCount(wrapper);
    }

    @Override
    public IPage<Order> queryPage(Page page, QueryWrapper<Order> wrapper) {
        return this.goodsOrderMapper.selectPage(page, wrapper);
    }

    /***
     * 汇总已售件数
     * @param goodsId
     * @return
     */
    @Override
    public int countByGoodsIdsByOrder(Long goodsId) {
        QueryWrapper<Order> wrapper = new QueryWrapper();
        wrapper.lambda()
                .eq(Order::getGoodsId, goodsId)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .in(Order::getStatus, Arrays.asList(
                        OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                        OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
                        OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode(),
                        OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode(),
                        OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode()
                ))
        ;
        return this.goodsOrderMapper.selectCount(wrapper);
    }

    @Override
    public int countByStatusByShopUser(Long shopId, Long userId, int orderStatus) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .eq(Order::getStatus, orderStatus)
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        return this.goodsOrderMapper.selectCount(wrapper);
    }

    @Override
    public int updateByPrimaryKeySelective(Order order) {
        return this.goodsOrderMapper.updateByPrimaryKeySelective(order);
    }

    @Override
    public Order selectOne(QueryWrapper<Order> orderWrapper) {
        return this.goodsOrderMapper.selectOne(orderWrapper);
    }

    @Override
    public int countByGoodsIdsByRefundOrderId(Long orderId, List<Integer> orderStatus) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getRefundOrderId, orderId)
                .in(Order::getType, Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()))
        ;
        if (orderStatus != null) {
            wrapper.and(params -> params.in("status", orderStatus));
        }
        return this.goodsOrderMapper.selectCount(wrapper);
    }

    /**
     * 按照订单编号查询付款订单
     *
     * @param orderNo 支付订单编号
     * @return 支付订单数据
     */
    @Override
    public Order selectPayByOrderNo(String orderNo) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getOrderNo, orderNo)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .eq(Order::getDel, DelEnum.NO.getCode())
                .orderByDesc(Order::getId)
        ;
        return goodsOrderMapper.selectOne(wrapper);
    }

    /**
     * 按照订单编号查询退款订单,倒叙查询
     *
     * @param orderNo 原支付订单编号
     * @return 退款订单列表
     */
    @Override
    public List<Order> selectRefundByPayOrderNo(String orderNo) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getOrderNo, orderNo)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .eq(Order::getDel, DelEnum.NO.getCode());
        Order payOrder = goodsOrderMapper.selectOne(wrapper);
        if (payOrder == null) {
            return new ArrayList<>();
        }
        QueryWrapper<Order> refundWrapper = new QueryWrapper<>();
        refundWrapper.lambda()
                .eq(Order::getRefundOrderId, payOrder.getId())
                .in(Order::getType, Arrays.asList(OrderTypeEnum.REFUNDMONEY.getCode(), OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()))
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        refundWrapper.orderByDesc("id");
        return goodsOrderMapper.selectList(refundWrapper);
    }

    /**
     * 按照订单编号查询退款订单,id倒叙不能变
     *
     * @param idList
     * @return
     */
    @Override
    public List<Order> selectRefundByPayOrderIdList(List<Long> idList) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Order::getRefundOrderId, idList)
                .orderByDesc(Order::getId);
        return goodsOrderMapper.selectList(wrapper);
    }

    /**
     * 根据店铺id,产品id统计订单数
     */
    @Override
    public Long summaryOrderByShopIdAndProductId(Map<String, Object> conditionMap) {
        return goodsOrderMapper.summaryOrderByShopIdAndProductId(conditionMap);
    }

    @Override
    public int updateBySelectiveForAtomicity(Map<String, Object> params) {
        return goodsOrderMapper.updateBySelectiveForAtomicity(params);
    }

    /**
     * 商户端查询订单列表
     *
     * @param params
     * @return
     */
    @Override
    public Map<String, Object> pageOrderListForSeller(Map<String, Object> params) {
        Map<String, Object> result = new HashMap<>();
        if (CollectionUtils.isEmpty(params)) {
            return result;
        }
        Long count = goodsOrderMapper.countOrderListForSeller(params);
        List<Order> orderList = goodsOrderMapper.pageOrderListForSeller(params);
        result.put("count", count);
        result.put("list", orderList);
        return result;
    }

    /**
     * 商户端通过订单id列表查询订单数据
     *
     * @param orderIdList
     * @return
     */
    @Override
    public List<Order> selectOrderListByIdListForSeller(List<Long> orderIdList) {
        if (CollectionUtils.isEmpty(orderIdList)) {
            return new ArrayList<>();
        }
        return goodsOrderMapper.selectBatchIds(orderIdList);
    }

    /***
     * 根据商品id，查询该商品是否购买过
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public int selectCountOrderByProductId(Long merchantId, Long shopId, Long userId, Long goodsId) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getMerchantId, merchantId)
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .eq(Order::getGoodsId, goodsId)
                .eq(Order::getType, OrderTypeEnum.PAY.getCode())
                .in(Order::getStatus, Arrays.asList(
                        OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                        OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
                        OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode()))
        ;
        return goodsOrderMapper.selectCount(wrapper);
    }

    /**
     * 根据免单任务查询用户是否已下单
     *
     * @param discountType
     * @param discountId
     * @param userId
     * @return
     */
    @Override
    public List<Order> selectListByDiscountTypeAndId(Integer discountType, Long discountId, Long userId) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Order::getDiscountType, discountType)
                .eq(Order::getDiscountId, discountId)
                .eq(Order::getUserId, userId);
        return goodsOrderMapper.selectList(queryWrapper);
    }
}
