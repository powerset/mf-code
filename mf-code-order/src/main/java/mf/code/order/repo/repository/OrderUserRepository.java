package mf.code.order.repo.repository;

import mf.code.order.domain.aggregateroot.OrderUser;

import java.util.Date;
import java.util.List;

/**
 * mf.code.order.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 10:57
 */
public interface OrderUserRepository {

	/**
	 * 通过 shopId, userId 获取
	 * @param shopId
	 * @param userId
	 * @return 不含用户订单历史记录
	 */
	OrderUser findByShopIdUserId(Long shopId, Long userId);

	/**
	 * 增装 用户历史订单记录
	 *
	 * @param orderUser
	 * @param beginTime
	 * @param endTime
	 */
	void addUserOrderHistory(OrderUser orderUser, Date beginTime, Date endTime);

	/**
	 * 批量获取 同一店铺下 的订单用户
	 * @param shopId
	 * @param userIds
	 * @return
	 */
	List<OrderUser> listByShopIdUserList(Long shopId, List<Long> userIds);

	/**
	 * 批量获取 用户 订单记录

	 * @param shopId
	 * @param beginTime
	 * @param endTime
	 */
	void listAddUserOrderHistory(List<OrderUser> orderUsers, Long shopId,  Date beginTime, Date endTime);
}
