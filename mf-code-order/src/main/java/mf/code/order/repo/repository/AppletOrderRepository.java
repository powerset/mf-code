package mf.code.order.repo.repository;

import mf.code.order.domain.aggregateroot.AppletOrder;
import mf.code.order.dto.AppletOrderResp;

import java.util.Date;
import java.util.List;

/**
 * mf.code.order.repo.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 13:59
 */
public interface AppletOrderRepository {

	/**
	 * 通过 id 获取
	 *
	 * @param orderId
	 * @return
	 */
	AppletOrder findById(Long orderId);
}
