package mf.code.order.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.order.feignapi.constant.OrderBizBizTypeEnum;
import mf.code.order.repo.dao.OrderBizMapper;
import mf.code.order.repo.po.OrderBiz;
import mf.code.order.repo.repository.OrderBizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * mf.code.order.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 12:46
 */
@Slf4j
@Service
public class OrderBizRepositoryImpl extends ServiceImpl<OrderBizMapper, OrderBiz> implements OrderBizRepository {
	@Autowired
	private OrderBizMapper orderBizMapper;

	@Override
	public int insertSelective(OrderBiz orderBiz){
		orderBiz.setCtime(new Date());
		orderBiz.setUtime(new Date());
		return orderBizMapper.insertSelective(orderBiz);
	}

    /**
     * 根据订单id查询订单biz分销数据
     *
     * @param orderId
     * @return
     */
	@Override
	public OrderBiz selectByOrderId(Long orderId){
        QueryWrapper<OrderBiz> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(OrderBiz::getOrderId, orderId)
                .eq(OrderBiz::getBizType, OrderBizBizTypeEnum.DISTRIBUTION.getCode());
        return orderBizMapper.selectOne(queryWrapper);
	}
}
