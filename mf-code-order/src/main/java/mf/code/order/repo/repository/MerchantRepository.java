package mf.code.order.repo.repository;

import mf.code.order.repo.po.Merchant;
import mf.code.order.repo.po.MerchantOrder;

import java.util.List;
import java.util.Map;

/**
 * merchant
 * 商户
 */
public interface MerchantRepository{

    Merchant getMerchant(Long id);

    /***
     * 获取主商户的商户编号
     * @param merchantId
     * @return
     */
    Long getParentMerchantId(Long merchantId);

    /***
     * 根据手机号获取商户信息
     * @param phone
     * @return
     */
    Merchant getbyPhone(String phone);

    /***
     * 根据merchantOrder获取商户
     * @param merchantOrderList
     * @return
     */
    Map<Long, Merchant> getMerchantByMerchantOrder(List<MerchantOrder> merchantOrderList);
}