package mf.code.order.repo.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * order
 * 商品订单表,用于商品购买,退款,查账
 */
@TableName("`order`")
public class Order implements Serializable {
    /**
     * 商品订单主键id
     */
    private Long id;

    /**
     * 订单编号 本系统的订单编号
     */
    private String orderNo;

    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 收货地址编号
     */
    private Long addressId;

    /**
     * 订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
     */
    private String logistics;

    /**
     * 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货 4：平台补贴
     */
    @TableField("`type`")
    private Integer type;

    /**
     * 支付渠道：1:余额 2:微信 3：支付宝
     */
    private Integer payChannel;

    /**
     * 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时);7, "交易关闭(退款成功)
     */
    @TableField("`status`")
    private Integer status;

    /**
     * 原金额,单位元
     */
    private BigDecimal originalfee;

    /**
     * 实付金额,单位元
     */
    private BigDecimal fee;

    /**
     * 商品件数
     */
    private Integer number;

    /**
     * 流水号 第三方流水号
     */
    private String tradeId;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 商户号-主账号
     */
    private Long merchantId;

    /**
     * 店铺号
     */
    private Long shopId;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 商品所属的店铺编号
     */
    private Long goodsShopId;

    /**
     * 商品编号
     */
    private Long goodsId;

    /**
     * 商品sku编号
     */
    private Long skuId;

    /**
     * 订单入参，json对象
     */
    private String reqJson;

    /**
     * 微信订单返回，json对象
     */
    private String respJson;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 回调处理系统时间
     */
    private Date notifyTime;

    /**
     * 响应数据中的支付时间
     */
    private Date paymentTime;

    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 成交时间|收货时间
     */
    private Date dealTime;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 业务类型
(0, "无分销属性"),
(1, "自卖分销"),
(2, "放到平台分销"),
(3, "自卖分销+放到平台分销"),
4, "平台获取分销--从平台市场拿到自己店里售卖"),
(4, "平台补贴用户"),
     */
    private Integer bizType;

    /**
     * 业务类型针对的值1:TODO;2:TODO;
     */
    private Long bizValue;

    /**
     * 退款时，需要退款的订单主键编号(冗余)
     */
    private Long refundOrderId;

    /**
     * 买家留言
     */
    private String buyerMsg;

    /**
     * 卖家留言
     */
    private String sellerMsg;

    /**
     * 删除标记 . 0:正常 1:删除
     */
    private Integer del;

    /**
     * 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     */
    private Integer discountType;

    /**
     * 优惠id（优惠券类型为优惠券id，其他为相应的活动id）
     */
    private Long discountId;

    /**
     * 订单来源 0:集客名品 1:抖带带 2:抖小铺
     */
    private Integer source;

    /**
     * order
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商品订单主键id
     * @return id 商品订单主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商品订单主键id
     * @param id 商品订单主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 订单编号 本系统的订单编号
     * @return order_no 订单编号 本系统的订单编号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 订单编号 本系统的订单编号
     * @param orderNo 订单编号 本系统的订单编号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 订单名称
     * @return order_name 订单名称
     */
    public String getOrderName() {
        return orderName;
    }

    /**
     * 订单名称
     * @param orderName 订单名称
     */
    public void setOrderName(String orderName) {
        this.orderName = orderName == null ? null : orderName.trim();
    }

    /**
     * 收货地址编号
     * @return address_id 收货地址编号
     */
    public Long getAddressId() {
        return addressId;
    }

    /**
     * 收货地址编号
     * @param addressId 收货地址编号
     */
    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    /**
     * 订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
     * @return logistics 订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
     */
    public String getLogistics() {
        return logistics;
    }

    /**
     * 订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
     * @param logistics 订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
     */
    public void setLogistics(String logistics) {
        this.logistics = logistics == null ? null : logistics.trim();
    }

    /**
     * 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货 4：平台补贴
     * @return type 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货 4：平台补贴
     */
    public Integer getType() {
        return type;
    }

    /**
     * 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货 4：平台补贴
     * @param type 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货 4：平台补贴
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 支付渠道：1:余额 2:微信 3：支付宝
     * @return pay_channel 支付渠道：1:余额 2:微信 3：支付宝
     */
    public Integer getPayChannel() {
        return payChannel;
    }

    /**
     * 支付渠道：1:余额 2:微信 3：支付宝
     * @param payChannel 支付渠道：1:余额 2:微信 3：支付宝
     */
    public void setPayChannel(Integer payChannel) {
        this.payChannel = payChannel;
    }

    /**
     * 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时);7, "交易关闭(退款成功)
     * @return status 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时);7, "交易关闭(退款成功)
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时);7, "交易关闭(退款成功)
     * @param status 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时);7, "交易关闭(退款成功)
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 原金额,单位元
     * @return originalFee 原金额,单位元
     */
    public BigDecimal getOriginalfee() {
        return originalfee;
    }

    /**
     * 原金额,单位元
     * @param originalfee 原金额,单位元
     */
    public void setOriginalfee(BigDecimal originalfee) {
        this.originalfee = originalfee;
    }

    /**
     * 实付金额,单位元
     * @return fee 实付金额,单位元
     */
    public BigDecimal getFee() {
        return fee;
    }

    /**
     * 实付金额,单位元
     * @param fee 实付金额,单位元
     */
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    /**
     * 商品件数
     * @return number 商品件数
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 商品件数
     * @param number 商品件数
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 流水号 第三方流水号
     * @return trade_id 流水号 第三方流水号
     */
    public String getTradeId() {
        return tradeId;
    }

    /**
     * 流水号 第三方流水号
     * @param tradeId 流水号 第三方流水号
     */
    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    /**
     * 备注信息
     * @return remark 备注信息
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注信息
     * @param remark 备注信息
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 商户号-主账号
     * @return merchant_id 商户号-主账号
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户号-主账号
     * @param merchantId 商户号-主账号
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺号
     * @return shop_id 店铺号
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺号
     * @param shopId 店铺号
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 用户编号
     * @return user_id 用户编号
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户编号
     * @param userId 用户编号
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 商品所属的店铺编号
     * @return goods_shop_id 商品所属的店铺编号
     */
    public Long getGoodsShopId() {
        return goodsShopId;
    }

    /**
     * 商品所属的店铺编号
     * @param goodsShopId 商品所属的店铺编号
     */
    public void setGoodsShopId(Long goodsShopId) {
        this.goodsShopId = goodsShopId;
    }

    /**
     * 商品编号
     * @return goods_id 商品编号
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商品编号
     * @param goodsId 商品编号
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商品sku编号
     * @return sku_id 商品sku编号
     */
    public Long getSkuId() {
        return skuId;
    }

    /**
     * 商品sku编号
     * @param skuId 商品sku编号
     */
    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    /**
     * 订单入参，json对象
     * @return req_json 订单入参，json对象
     */
    public String getReqJson() {
        return reqJson;
    }

    /**
     * 订单入参，json对象
     * @param reqJson 订单入参，json对象
     */
    public void setReqJson(String reqJson) {
        this.reqJson = reqJson == null ? null : reqJson.trim();
    }

    /**
     * 微信订单返回，json对象
     * @return resp_json 微信订单返回，json对象
     */
    public String getRespJson() {
        return respJson;
    }

    /**
     * 微信订单返回，json对象
     * @param respJson 微信订单返回，json对象
     */
    public void setRespJson(String respJson) {
        this.respJson = respJson == null ? null : respJson.trim();
    }

    /**
     * ip地址
     * @return ip_address ip地址
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * ip地址
     * @param ipAddress ip地址
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress == null ? null : ipAddress.trim();
    }

    /**
     * 回调处理系统时间
     * @return notify_time 回调处理系统时间
     */
    public Date getNotifyTime() {
        return notifyTime;
    }

    /**
     * 回调处理系统时间
     * @param notifyTime 回调处理系统时间
     */
    public void setNotifyTime(Date notifyTime) {
        this.notifyTime = notifyTime;
    }

    /**
     * 响应数据中的支付时间
     * @return payment_time 响应数据中的支付时间
     */
    public Date getPaymentTime() {
        return paymentTime;
    }

    /**
     * 响应数据中的支付时间
     * @param paymentTime 响应数据中的支付时间
     */
    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    /**
     * 发货时间
     * @return send_time 发货时间
     */
    public Date getSendTime() {
        return sendTime;
    }

    /**
     * 发货时间
     * @param sendTime 发货时间
     */
    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    /**
     * 成交时间|收货时间
     * @return deal_time 成交时间|收货时间
     */
    public Date getDealTime() {
        return dealTime;
    }

    /**
     * 成交时间|收货时间
     * @param dealTime 成交时间|收货时间
     */
    public void setDealTime(Date dealTime) {
        this.dealTime = dealTime;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 业务类型
 (0, "无分销属性"),
 (1, "自卖分销"),
 (2, "放到平台分销"),
 (3, "自卖分销+放到平台分销"),
 4, "平台获取分销--从平台市场拿到自己店里售卖"),
 (4, "平台补贴用户"),
     * @return biz_type 业务类型
 (0, "无分销属性"),
 (1, "自卖分销"),
 (2, "放到平台分销"),
 (3, "自卖分销+放到平台分销"),
 4, "平台获取分销--从平台市场拿到自己店里售卖"),
 (4, "平台补贴用户"),
     */
    public Integer getBizType() {
        return bizType;
    }

    /**
     * 业务类型
 (0, "无分销属性"),
 (1, "自卖分销"),
 (2, "放到平台分销"),
 (3, "自卖分销+放到平台分销"),
 4, "平台获取分销--从平台市场拿到自己店里售卖"),
 (4, "平台补贴用户"),
     * @param bizType 业务类型
 (0, "无分销属性"),
 (1, "自卖分销"),
 (2, "放到平台分销"),
 (3, "自卖分销+放到平台分销"),
 4, "平台获取分销--从平台市场拿到自己店里售卖"),
 (4, "平台补贴用户"),
     */
    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    /**
     * 业务类型针对的值1:TODO;2:TODO;
     * @return biz_value 业务类型针对的值1:TODO;2:TODO;
     */
    public Long getBizValue() {
        return bizValue;
    }

    /**
     * 业务类型针对的值1:TODO;2:TODO;
     * @param bizValue 业务类型针对的值1:TODO;2:TODO;
     */
    public void setBizValue(Long bizValue) {
        this.bizValue = bizValue;
    }

    /**
     * 退款时，需要退款的订单主键编号(冗余)
     * @return refund_order_id 退款时，需要退款的订单主键编号(冗余)
     */
    public Long getRefundOrderId() {
        return refundOrderId;
    }

    /**
     * 退款时，需要退款的订单主键编号(冗余)
     * @param refundOrderId 退款时，需要退款的订单主键编号(冗余)
     */
    public void setRefundOrderId(Long refundOrderId) {
        this.refundOrderId = refundOrderId;
    }

    /**
     * 买家留言
     * @return buyer_msg 买家留言
     */
    public String getBuyerMsg() {
        return buyerMsg;
    }

    /**
     * 买家留言
     * @param buyerMsg 买家留言
     */
    public void setBuyerMsg(String buyerMsg) {
        this.buyerMsg = buyerMsg == null ? null : buyerMsg.trim();
    }

    /**
     * 卖家留言
     * @return seller_msg 卖家留言
     */
    public String getSellerMsg() {
        return sellerMsg;
    }

    /**
     * 卖家留言
     * @param sellerMsg 卖家留言
     */
    public void setSellerMsg(String sellerMsg) {
        this.sellerMsg = sellerMsg == null ? null : sellerMsg.trim();
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @return del 删除标记 . 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @param del 删除标记 . 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     * @return discount_type 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     */
    public Integer getDiscountType() {
        return discountType;
    }

    /**
     * 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     * @param discountType 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     */
    public void setDiscountType(Integer discountType) {
        this.discountType = discountType;
    }

    /**
     * 优惠id（优惠券类型为优惠券id，其他为相应的活动id）
     * @return discount_id 优惠id（优惠券类型为优惠券id，其他为相应的活动id）
     */
    public Long getDiscountId() {
        return discountId;
    }

    /**
     * 优惠id（优惠券类型为优惠券id，其他为相应的活动id）
     * @param discountId 优惠id（优惠券类型为优惠券id，其他为相应的活动id）
     */
    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    /**
     * 订单来源 0:集客名品 1:抖带带 2:抖小铺
     * @return source 订单来源 0:集客名品 1:抖带带 2:抖小铺
     */
    public Integer getSource() {
        return source;
    }

    /**
     * 订单来源 0:集客名品 1:抖带带 2:抖小铺
     * @param source 订单来源 0:集客名品 1:抖带带 2:抖小铺
     */
    public void setSource(Integer source) {
        this.source = source;
    }

    /**
     *
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", orderName=").append(orderName);
        sb.append(", addressId=").append(addressId);
        sb.append(", logistics=").append(logistics);
        sb.append(", type=").append(type);
        sb.append(", payChannel=").append(payChannel);
        sb.append(", status=").append(status);
        sb.append(", originalfee=").append(originalfee);
        sb.append(", fee=").append(fee);
        sb.append(", number=").append(number);
        sb.append(", tradeId=").append(tradeId);
        sb.append(", remark=").append(remark);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", userId=").append(userId);
        sb.append(", goodsShopId=").append(goodsShopId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", skuId=").append(skuId);
        sb.append(", reqJson=").append(reqJson);
        sb.append(", respJson=").append(respJson);
        sb.append(", ipAddress=").append(ipAddress);
        sb.append(", notifyTime=").append(notifyTime);
        sb.append(", paymentTime=").append(paymentTime);
        sb.append(", sendTime=").append(sendTime);
        sb.append(", dealTime=").append(dealTime);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", bizType=").append(bizType);
        sb.append(", bizValue=").append(bizValue);
        sb.append(", refundOrderId=").append(refundOrderId);
        sb.append(", buyerMsg=").append(buyerMsg);
        sb.append(", sellerMsg=").append(sellerMsg);
        sb.append(", del=").append(del);
        sb.append(", discountType=").append(discountType);
        sb.append(", discountId=").append(discountId);
        sb.append(", source=").append(source);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
