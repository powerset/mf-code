package mf.code.order.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * order_biz
 * 订单业务
 */
public class OrderBiz implements Serializable {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 订单主键
     */
    private Long orderId;

    /**
     * 业务类型，0："分销场景"
     */
    private Integer bizType;

    /**
     * 业务信息
     */
    private String bizValue;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 删除标记 . 0:正常 1:删除
     */
    private Integer del;

    /**
     * order_biz
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * @return id 主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键id
     * @param id 主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 订单主键
     * @return order_id 订单主键
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * 订单主键
     * @param orderId 订单主键
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 业务类型，0："分销场景"
     * @return biz_type 业务类型，0："分销场景"
     */
    public Integer getBizType() {
        return bizType;
    }

    /**
     * 业务类型，0："分销场景"
     * @param bizType 业务类型，0："分销场景"
     */
    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    /**
     * 业务信息
     * @return biz_value 业务信息
     */
    public String getBizValue() {
        return bizValue;
    }

    /**
     * 业务信息
     * @param bizValue 业务信息
     */
    public void setBizValue(String bizValue) {
        this.bizValue = bizValue == null ? null : bizValue.trim();
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @return del 删除标记 . 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @param del 删除标记 . 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", bizType=").append(bizType);
        sb.append(", bizValue=").append(bizValue);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}