package mf.code.order.repo.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.order.repo.po.MerchantBalance;

import java.math.BigDecimal;

/**
 * mf.code.order.repo.repository
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月24日 21:01
 */
public interface MerchantBalanceRepository extends IService<MerchantBalance> {
    int lessUpdateByVersionId(MerchantBalance merchantBalance, BigDecimal lessDepositAdvance, BigDecimal lessDepositCash);

    int addUpdateByVersionId(MerchantBalance merchantBalance, BigDecimal addDepositAdvance, BigDecimal addDepositCash);

    int insertSelective(MerchantBalance merchantBalance);

    int updateBalance(Long merchantId, Long shopId, BigDecimal totalFee);
}
