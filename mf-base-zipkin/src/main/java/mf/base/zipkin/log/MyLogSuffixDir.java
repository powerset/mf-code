package mf.base.zipkin.log;
import ch.qos.logback.core.PropertyDefinerBase;
import org.apache.commons.lang.StringUtils;

/**
 * <pre>
 * MyLogSuffixDir
 * 定制 日志文件路径 与 server.port环境变量 有关联关系
 *   例如
 *     java命令行参数为
 *       -Dserver.port=8001
 *     日志文件路径为
 *       /home⁩/code⁩/logs⁩/mf-code-one-8001/all⁩/all-%d{yyyy-MM-dd}.log
 *       /home⁩/code⁩/logs⁩/mf-code-one-8001/error/error-%d{yyyy-MM-dd}.log
 * </pre>
 */
public class MyLogSuffixDir extends PropertyDefinerBase {

    @Override
    public String getPropertyValue() {
        String serverPort = System.getProperty("server.port");
        if (StringUtils.isNotBlank(serverPort)) {
            return "-" + serverPort;
        }
        return "";
    }
}
