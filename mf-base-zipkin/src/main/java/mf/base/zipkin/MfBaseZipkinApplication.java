package mf.base.zipkin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import zipkin2.server.internal.EnableZipkinServer;

@SpringBootApplication
@EnableZipkinServer
@Slf4j
public class MfBaseZipkinApplication {
	public static void main(String[] args) {
		new SpringApplicationBuilder(MfBaseZipkinApplication.class).run(args);
		log.info("ELK启动日志标识:MF-CODE-ZIPKIN");
	}

}
