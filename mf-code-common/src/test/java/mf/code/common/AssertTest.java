package mf.code.common;/**
 * create by qc on 2019/3/23 0023
 */

import mf.code.common.utils.Assert;

/**
 * @author gbf
 * 2019/3/23 0023、10:01
 */
public class AssertTest {
    public static void main(String[] args) {
        int i = 0;
        int j = 5;
        int m = 7;
        int n = 3;
        Assert.isRightNumberSection(i, 0, 5, 1, "i 指标不再合法范围");
        Assert.isRightNumberSection(j, 0, 5, 2, "j 指标不再合法范围");
        //Assert.isRightNumberSection(m, 0, 5, 3, "m 指标不再合法范围");
        Assert.isRightNumberSection(n, 0, 5, 4, "n 指标不再合法范围");
        System.out.println("数字校验###########################");

        String s = "";
        //Assert.hasText(s, 444, "string is blank");
        System.out.println("str not blank");

        String num = "aaa9999999999999999999999999999999999999999999999";
        Assert.isInteger(num, 3333, "not a number");
        System.out.println("num is a number");
    }
}
