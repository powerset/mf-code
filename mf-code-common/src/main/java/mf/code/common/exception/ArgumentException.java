package mf.code.common.exception;/**
 * create by qc on 2019/3/22 0022
 */

/**
 * 前端传参错误
 *
 * @author gbf
 * 2019/3/22 0022、16:51
 */
public class ArgumentException extends RuntimeException {
    private Integer code = -1;
    private String errorMessage;
    private Object data = "";

    public ArgumentException() {
        super();
    }

    public ArgumentException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public ArgumentException(Integer code, String errorMessage) {
        super(errorMessage);
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public ArgumentException(Integer code, String errorMessage, Object data) {
        super(errorMessage);
        this.code = code;
        this.data = data;
        this.errorMessage = errorMessage;
    }

    public ArgumentException(Integer code, String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.code = code;
        this.errorMessage = errorMessage;
    }

    /**
     * 用指定原因构造一个新的异常
     */
    public ArgumentException(Throwable cause) {
        super(cause);
    }

    public Integer getCode() {
        return this.code;
    }

    public Object getData() {
        return this.data;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
