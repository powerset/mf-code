package mf.code.common.exception;/**
 * create by qc on 2018/12/13 0013
 */

/**
 * @author gbf
 * @description :
 */
public class HttpRequestException extends Exception {
    private Integer code;
    private Object msg;

    public HttpRequestException() {
        super();
    }

    public HttpRequestException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public HttpRequestException(Integer code, String message, Object msg) {
        super(message);
        this.code = code;
        this.msg = msg;
    }

    public HttpRequestException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }
}
