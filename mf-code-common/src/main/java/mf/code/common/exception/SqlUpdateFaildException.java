package mf.code.common.exception;

/**
 * @author gel
 */
public class SqlUpdateFaildException extends RuntimeException {
    private Integer code = -1;
    //
    private Object data = "";

    public SqlUpdateFaildException() {
        super();
    }

    public SqlUpdateFaildException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public SqlUpdateFaildException(Integer code, String message, Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }

    public SqlUpdateFaildException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    //用指定原因构造一个新的异常
    public SqlUpdateFaildException(Throwable cause) {
        super(cause);
    }

    public Integer getCode(){
        return this.code;
    }

    public Object getData(){
        return this.data;
    }
}
