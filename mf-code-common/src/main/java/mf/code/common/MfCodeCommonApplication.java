package mf.code.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MfCodeCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(MfCodeCommonApplication.class, args);

	}


}
