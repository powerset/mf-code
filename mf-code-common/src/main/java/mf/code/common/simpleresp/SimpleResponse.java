package mf.code.common.simpleresp;

/**
 *
 */
public class SimpleResponse extends AirResponse {

    public SimpleResponse() {
        this.code= ApiStatusEnum.SUCCESS.getCode();
        this.message= ApiStatusEnum.SUCCESS.getMessage();
    }

    public SimpleResponse(ApiStatusEnum statusEnum) {
        this.code= statusEnum.getCode();
        this.message= statusEnum.getMessage();
    }

    public SimpleResponse(Integer code, String message) {
        this.code= code;
        this.message= message;
    }

    public SimpleResponse(ApiStatusEnum statusEnum, Object data) {
        this.code= statusEnum.getCode();
        this.message= statusEnum.getMessage();
        this.data = data;
    }

    public SimpleResponse(Integer code, String message, Object data) {
        this.code= code;
        this.message= message;
        this.data = data;
    }

    public void setStatusEnum(ApiStatusEnum statusEnum) {
        this.code=statusEnum.getCode();
        this.message=statusEnum.getMessage();
    }

    public boolean error(){
        return this.code != ApiStatusEnum.SUCCESS.getCode();
    }
}
