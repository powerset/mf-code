package mf.code.common.simpleresp;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description api响应基类
 */
public abstract class AirResponse {
    private Map<String,Object> respMap = new HashMap<>();
    /**状态码*/
    protected Integer code=0;
    /**状态描述*/
    String message;
    /**数据result*/
    public Object data = respMap;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
