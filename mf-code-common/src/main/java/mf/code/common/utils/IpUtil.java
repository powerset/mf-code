package mf.code.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.*;
import java.util.Enumeration;

@Slf4j
public class IpUtil {

    public static String getIpAddress(HttpServletRequest request) {
        if (request == null) {
            return IpUtil.getServerIp();
        }
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip == null || ip.length() == 0) {
            ip = IpUtil.getServerIp();
        }

        int index = ip.indexOf(",");
        if(index > 0){
            log.info("<<<<<<<<<< ip:{}", ip);
            ip = ip.substring(0, ip.indexOf(","));
        }
        return ip;
    }

    public static String Ip2Provice(String ip) {
        if (StringUtils.equals(ip, "127.0.0.1")) {
            return "浙江省";
        }
        // http://ip.ws.126.net/ipquery?ip=123.139.94.139
        String url = "http://ip.ws.126.net/ipquery";
        byte[] bytes = HttpUtil.sendGetReturnByte(url, "ip=" +ip);
        String result = null;
        try{
            if (bytes != null) {
                result = new String(bytes, "GBK");
            }
        } catch (Exception e) {
            log.error("{}", e);
            return null;
        }
        if (StringUtils.isEmpty(result)) {
            return null;
        }
        int beginIndex = result.indexOf("province:\"");
        int endIndex = result.lastIndexOf("\"");
        return result.substring(beginIndex+10, endIndex);
    }

    public static String getServerIp() {
        // 获取操作系统类型
        String sysType = System.getProperties().getProperty("os.name");
        String ip;
        if (sysType.toLowerCase().startsWith("win")) {  // 如果是Windows系统，获取本地IP地址
            String localIP = null;
            try {
                localIP = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                log.error(e.getMessage(), e);
            }
            if (localIP != null) {
                return localIP;
            }
        } else {
            ip = IpUtil.getIpByEthNum("eth0"); // 兼容Linux
            if (ip != null) {
                return ip;
            }
        }
        return "获取服务器IP错误";
    }

    private static String getIpByEthNum(String ethNum) {
        try {
            Enumeration allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip;
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                if (ethNum.equals(netInterface.getName())) {
                    Enumeration addresses = netInterface.getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        ip = (InetAddress) addresses.nextElement();
                        if (ip != null && ip instanceof Inet4Address) {
                            return ip.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            log.error(e.getMessage(), e);
        }
        return "获取服务器IP错误";
    }
}
