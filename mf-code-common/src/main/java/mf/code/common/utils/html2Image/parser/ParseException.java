package mf.code.common.utils.html2Image.parser;

/**
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2018/10/26 0026 20:52
 */
public class ParseException extends RuntimeException {
	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}
}
