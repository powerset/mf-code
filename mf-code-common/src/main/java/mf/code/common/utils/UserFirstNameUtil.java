package mf.code.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月14日 18:03
 */
public class UserFirstNameUtil {
    private static List<String> userFirstNameList = new ArrayList<String>();

    static {
        userFirstNameList.add("李");userFirstNameList.add("曾");userFirstNameList.add("彭");userFirstNameList.add("魏");
        userFirstNameList.add("戴");userFirstNameList.add("孔");userFirstNameList.add("易");userFirstNameList.add("黎");
        userFirstNameList.add("常");userFirstNameList.add("武");userFirstNameList.add("乔");userFirstNameList.add("贺");
        userFirstNameList.add("赖");userFirstNameList.add("龚");userFirstNameList.add("文");userFirstNameList.add("刘");
        userFirstNameList.add("卢");userFirstNameList.add("薛");userFirstNameList.add("汪");userFirstNameList.add("康");
        userFirstNameList.add("杨");userFirstNameList.add("蔡");userFirstNameList.add("阎");userFirstNameList.add("任");
        userFirstNameList.add("邱");userFirstNameList.add("黄");userFirstNameList.add("潘");userFirstNameList.add("范");
        userFirstNameList.add("江");userFirstNameList.add("吴");userFirstNameList.add("石");userFirstNameList.add("顾");
        userFirstNameList.add("孙");userFirstNameList.add("谭");userFirstNameList.add("邵");userFirstNameList.add("王");
        userFirstNameList.add("吕");userFirstNameList.add("贾");userFirstNameList.add("夏");userFirstNameList.add("白");
        userFirstNameList.add("张");userFirstNameList.add("苏");userFirstNameList.add("丁");userFirstNameList.add("钟");
        userFirstNameList.add("崔");userFirstNameList.add("陈");userFirstNameList.add("蒋");userFirstNameList.add("叶");
        userFirstNameList.add("田");userFirstNameList.add("毛");userFirstNameList.add("赵");userFirstNameList.add("余");
        userFirstNameList.add("姜");userFirstNameList.add("秦");userFirstNameList.add("周");userFirstNameList.add("杜");
        userFirstNameList.add("方");userFirstNameList.add("史");userFirstNameList.add("徐");userFirstNameList.add("姚");
        userFirstNameList.add("侯");userFirstNameList.add("胡");userFirstNameList.add("廖");userFirstNameList.add("孟");
        userFirstNameList.add("邹");userFirstNameList.add("龙");userFirstNameList.add("熊");userFirstNameList.add("万");
        userFirstNameList.add("金");userFirstNameList.add("段");userFirstNameList.add("陆");userFirstNameList.add("雷");
        userFirstNameList.add("郝");userFirstNameList.add("钱");userFirstNameList.add("汤");userFirstNameList.add("尹");
        userFirstNameList.add("朱");userFirstNameList.add("谢");userFirstNameList.add("罗");userFirstNameList.add("萧");
        userFirstNameList.add("何");userFirstNameList.add("冯");userFirstNameList.add("郑");userFirstNameList.add("袁");
        userFirstNameList.add("高");userFirstNameList.add("韩");userFirstNameList.add("梁");userFirstNameList.add("程");
        userFirstNameList.add("郭");userFirstNameList.add("于");userFirstNameList.add("傅");userFirstNameList.add("邓");
        userFirstNameList.add("林");userFirstNameList.add("唐");userFirstNameList.add("宋");userFirstNameList.add("曹");
        userFirstNameList.add("马");userFirstNameList.add("董");userFirstNameList.add("沈");userFirstNameList.add("许");
    }

    /***
     * random.nextInt 介于[0,n)
     * @return
     */
    public static String getRandomFirstName() {
        int max = userFirstNameList.size();
        Random random = new Random();
        int s = random.nextInt(max);
        return userFirstNameList.get(s);
    }

//    public static void main(String[] args) {
//        System.out.println(UserFirstNameUtil.getRandomFirstName());
//    }
}
