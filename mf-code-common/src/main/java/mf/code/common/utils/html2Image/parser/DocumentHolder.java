package mf.code.common.utils.html2Image.parser;

import org.w3c.dom.Document;

/**
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2018/10/26 0026 20:52
 */
public interface DocumentHolder {
	Document getDocument();
}
