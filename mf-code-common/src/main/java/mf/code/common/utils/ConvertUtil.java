package mf.code.common.utils;

import org.apache.commons.lang.StringUtils;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月07日 11:41
 */
public class ConvertUtil {

    /**
     * 转换乱码
     * @param s
     * @return
     */
    public static String toUtf8String(String s){
        StringBuffer sb = new StringBuffer();
        for (int i=0;i<s.length();i++){
            char c = s.charAt(i);
            if (c >= 0 && c <= 255){
                sb.append(c);
            }else{
                byte[] b;
                try {
                    b = Character.toString(c).getBytes("utf-8");
                }catch (Exception ex) {
                    b = new byte[0];
                }
                for (int j = 0; j < b.length; j++) {
                    int k = b[j];
                    if (k < 0) {
                        k += 256;
                    }
                    sb.append("%" + Integer.toHexString(k).toUpperCase());
                }
            }
        }
        return sb.toString();
    }

    /**
     * 每相隔一个字符过滤
     * @return
     */
    public static String dataMasking(String source){
        if(StringUtils.isBlank(source)){
            return source;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < source.length(); i++) {
            if(i%2 == 1 && i != source.length() - 1){
                sb.append("*");
            }else {
                sb.append(source.charAt(i));
            }
        }
        return sb.toString();
    }

//    public static void main(String[] args) {
//        System.out.println(dataMasking("大华店铺"));
//    }
}
