package mf.code.common.utils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @Date: 2018/10/9 0009 14:19
 * @Description:
 */
public class TokenUtil {
    public static final String TEACHER = "teacher";
    public static final String APPLET = "applet";
    public static final String DOU_DAI_DAI = "douDaiDai";
    public static final String DOU_XIAO_PU = "douXiaoPu";
    public static final String SAAS = "saas";
    public static final String PLATFORM = "platform";

    private static Map<String, List<String>> sceneMap = new HashMap<>();

    /**
     * 下列三个参数说明
     * 1、DES相关的scene需要是8的倍数
     * 2、md5的场景参数
     * 3、token的场景参数
     */
    static {
        sceneMap.put("douDaiDai", Arrays.asList("12345678", "wqf", "yechen123"));
        sceneMap.put("douXiaoPu", Arrays.asList("12345678", "wqf", "yechen123"));
        sceneMap.put("teacher", Arrays.asList("12345678", "wqf", "yechen123"));
        sceneMap.put("applet", Arrays.asList("12345678", "wqf", "yechen123"));
        sceneMap.put("saas", Arrays.asList("12345678", "wqf", "yechen123"));
        sceneMap.put("platform", Arrays.asList("12345678", "wqf", "yechen123"));
    }

    /**
     * 功能描述:token 加密 token 生成机制：key+digest
     *
     * @param: [uid, user, ctime]
     * @param: uid用户id，商户id，平台运营用户id
     * @param: user 小程序openid，微信公众号openid，商户手机号，平台运营用户手机号
     * @param: ctime 用户，商户，平台运营用户的数据记录的创建时间，yyyyMMddHHmmss格式
     * @return: java.lang.String
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/10 0010 09:13
     */
    public static String encryptToken(String uid, String user, Date ctime, String type) {
        if (uid == null || user == null || ctime == null) {
            return null;
        }
        if (sceneMap.get(type) == null) {
            return null;
        }
        List<String> listScene = sceneMap.get(type);

        //step1:生成key
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = sdf.format(new Date());
        String keyStr = uid + "," + user + "," + date;
        String key = DESUtil.encrypt(keyStr, listScene.get(0));
        //step2:生成digest
        String digestStr = uid + "," + user + "," + sdf.format(ctime) + "," + listScene.get(1);
        String digest = MD5Util.md5(digestStr);
        //step3:生成token
        String tokenStr = key + "@@" + digest;
        String token = DESUtil.encrypt(tokenStr, listScene.get(2));
        return token;
    }

    /**
     * 因为采用可逆算法，此工具解密方法不做任何校验，请各自调用方自己处理
     *
     * @param tokenSecret
     * @param type
     * @return
     */
    public static Map<String, String> decryptToken(String tokenSecret, String type) {

        if (tokenSecret == null) {
            return null;
        }
        if (sceneMap.get(type) == null) {
            return null;
        }
        List<String> listScene = sceneMap.get(type);


        String token = DESUtil.decryptor(tokenSecret, listScene.get(2));
        if (token == null) {
            return null;
        }
        //获取加密key
        String keyDes = token.substring(0, token.indexOf("@@"));
        //获取解密的key
        String keyDecr = DESUtil.decryptor(keyDes, listScene.get(0));
        if (keyDecr == null) {
            return null;
        }
        List<String> list = Arrays.asList(keyDecr.split(","));

        if (list.size() < 3) {
            return null;
        }
        Map<String, String> map = new HashMap<>();
        map.put("uid", list.get(0));
        map.put("user", list.get(1));
        map.put("now", list.get(2));
        //获取加密digest
        String digestDes = token.substring(token.indexOf("@@") + 2);
        map.put("digest", digestDes);
        return map;
    }

    /**
     * md5规则 digest = MD5(uid+","+user+","+ctime+","+YYY")
     *
     * @param uid
     * @param user
     * @param ctime
     * @param type
     * @return
     */
    public static String md5forDigest(String uid, String user, Date ctime, String type) {
        List<String> listScene = sceneMap.get(type);
        if (uid == null || user == null || ctime == null || type == null) {
            return null;
        }
        if (sceneMap.get(type) == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(uid).append(",")
                .append(user).append(",")
                .append(DateUtil.dateToString(ctime, DateUtil.FORMAT_TOKEN)).append(",")
                .append(listScene.get(1));
        return MD5Util.md5(sb.toString());
    }


    //测试
//    public static void main(String args[]) {
//        /**生成算法
//         key = DES(uid+","+user+","+now, "XXX")
//         digest = MD5(uid+","+user+","+ctime+","+YYY")
//         token = DES(key+"@@"+digest, "ZZZ")*/
//        //加密token
//        Date now = new Date();
//        String token = TokenUtil.encryptToken("123456", "user", now, SAAS);
//        System.out.println("3：加密token:" + token);
//        System.out.println(token.length());
//
//        //解密token
//        Map<String, String> map = TokenUtil.decryptToken(token, SAAS);
//        System.out.println("4：解密token:" + map);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//        String digestStr = "123456" + "," + "user" + "," + sdf.format(now) + "," + "wqf";
//        System.out.println(digestStr);
//        String digest = MD5Util.md5(digestStr);
//        System.out.println(digest);
//
//    }
}
