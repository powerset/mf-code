package mf.code.common.utils;/**
 * create by gbf on 2018/11/5 0005
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author gbf
 * @description : 判断是否为正确的json格式
 */
public class JsonParseUtil {

    public static boolean booJsonObj(String string) {
        if (string == null) {
            return false;
        }
        try {
            JSONObject jsonObject = JSON.parseObject(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean booJsonArr(String string) {
        if (string == null) {
            return false;
        }
        try {
            JSONArray arr = JSON.parseArray(string);
            if (arr.get(0).toString() == null) {
                return false;
            }
            if ("".equals(arr.get(0).toString())) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static Map<String, String[]> parseToMap(String jsonString) {
        if (jsonString == null) {
            return null;
        }
        Map<String, String[]> resultMap = new HashMap<>();
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            if (jsonObject == null) {
                return resultMap;
            }
            Set<Map.Entry<String, Object>> entries = jsonObject.entrySet();
            for (Map.Entry entry : entries) {
                // 参数值为空直接丢弃
                if(entry.getValue() == null){
                    continue;
                }
                resultMap.put(entry.getKey().toString(), new String[]{entry.getValue().toString()});
            }
        } catch (Exception e) {
            return null;
        }
        return resultMap;
    }
}
