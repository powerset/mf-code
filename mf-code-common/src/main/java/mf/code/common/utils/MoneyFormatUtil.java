package mf.code.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * mf.code.common.utils
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-07 下午6:04
 */
public class MoneyFormatUtil {

	/**
	 * 超过10000，格式转化为 1.00万
	 * 1000以下 格式转化为 1000.00
	 *
	 * 舍弃 小数点 2位之后 的数字
	 * @param yuan 金额
	 * @return String
	 */
	public static String yuanToWanyuan(BigDecimal yuan) {
		BigDecimal wanyuan = new BigDecimal(10000);
		if (yuan.compareTo(wanyuan) >= 0) {
			return yuan.divide(wanyuan, 2, RoundingMode.DOWN) + "万";
		}
		yuan = yuan.setScale(2, RoundingMode.DOWN);
		return yuan.toString();
	}
}
