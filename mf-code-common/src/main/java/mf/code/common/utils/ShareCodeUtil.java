package mf.code.common.utils;

import org.apache.commons.lang.StringUtils;

import java.util.Random;

/**
 * mf.code.common.utils
 *
 * @description: 邀请码生成器，算法原理：<br/>
 * 1) 获取id: 1127738 <br/>
 * 2) 使用自定义进制转为：gpm6 <br/>
 * 3) 转为字符串，并在后面加'o'字符：gpm6o <br/>
 * 4）在后面随机产生若干个随机数字字符：gpm6o7 <br/>
 * 转为自定义进制后就不会出现o这个字符，然后在后面加个'o'，这样就能确定唯一性。最后在后面产生一些随机字符进行补全。<br/>
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月04日 17:06
 */
public class ShareCodeUtil {
    /**
     * 自定义进制 o,l 去掉 避免与0,1冲突
     */
    private static final char[] r = new char[]{'Q', '0', 'A', '1', 'E', '8', 'S', '2', 'D', 'Z', 'X', '9', 'C', '7', 'P', '5', 'I', 'K', '3', 'M', 'J', 'U', 'F', 'R', '4', 'V', 'Y', 'T', 'N', '6', 'B', 'G', 'H'};

    /**
     * 定义一个字符用来补全邀请码长度（该字符前面是计算出来的邀请码，后面是用来补全用的）
     */
    private static final char b = 'W';

    /**
     * 进制长度
     */
    private static final int binLen = r.length;

    /**
     * 序列最小长度
     */
    private static final int s = 5;

    /**
     * 补位字符串
     */
    private static final String e = "A";

    /**
     * 根据ID生成六位随机码
     *
     * @param indexStr 前缀
     * @param id       ID
     * @return 随机码
     */
    public static String toSerialCode(String indexStr, long id) {
        if (StringUtils.isBlank(indexStr)) {
            indexStr = e;
        }
        char[] buf = new char[32];
        int charPos = 32;

        while ((id / binLen) > 0) {
            int ind = (int) (id % binLen);
            buf[--charPos] = r[ind];
            id /= binLen;
        }
        buf[--charPos] = r[(int) (id % binLen)];
        String str = new String(buf, charPos, (32 - charPos));
        // 不够长度的自动随机补全
        if (str.length() < s) {
            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();
            sb.append(b);
            for (int i = 1; i < s - str.length(); i++) {
                sb.append(r[rnd.nextInt(binLen)]);
            }
            str += sb.toString();
        }
        return indexStr + str;
    }

    public static long codeToId(String code) {
        //去除一个前缀
        code = code.substring(1);
        char chs[] = code.toCharArray();
        long res = 0L;
        for (int i = 0; i < chs.length; i++) {
            int ind = 0;
            for (int j = 0; j < binLen; j++) {
                if (chs[i] == r[j]) {
                    ind = j;
                    break;
                }
            }
            if (chs[i] == b) {
                break;
            }
            if (i > 0) {
                res = res * binLen + ind;
            } else {
                res = ind;
            }
        }
        return res;
    }
}
