package mf.code.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.util.Base64Utils;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static javax.crypto.Cipher.DECRYPT_MODE;

/**
 * 密码，长度要是8的倍数    注意此处为简单密码  简单应用 要求不高时可用此密码
 * DES是一种对称加密算法，所谓对称加密算法即：加密和解密使用相同密钥的算法。DES加密算法出自IBM的研究，
 * 后来被美国政府正式采用，之后开始广泛流传，但是近些年使用越来越少，因为DES使用56位密钥，以现代计算能力，
 * 24小时内即可被破解
 */
@Slf4j
public class DESUtil {

    /**
     * 功能描述:加密数据
     *
     * @param: [data, scene] scene必须是8的倍数
     * @return: java.lang.String
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/9 0009 14:53
     */

    public static String encrypt(String data, String scene) {  //对string进行BASE64Encoder转换
        byte[] bt = encryptByKey(data.getBytes(), scene);
        return Base64.encodeBase64String(bt);
    }

    /**
     * @param data
     * @return
     * @throws Exception
     * @Method: encrypt
     * @Description: 解密数据
     * @date 2018年9月28日
     */
    public static String decryptor(String data, String scene) {  //对string进行BASE64Encoder转换
        try {
            byte[] bt = decrypt(Base64.decodeBase64(data), scene);
            String strs = new String(bt);
            return strs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 加密
     *
     * @param datasource byte[]
     * @param key        String
     * @return byte[]
     */
    private static byte[] encryptByKey(byte[] datasource, String key) {
        try {
            SecureRandom random = new SecureRandom();

            DESKeySpec desKey = new DESKeySpec(key.getBytes());
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            //正式执行加密操作
            return cipher.doFinal(datasource);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 解密
     *
     * @param src byte[]
     * @param key String
     * @return byte[]
     * @throws Exception
     */
    private static byte[] decrypt(byte[] src, String key) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom random = new SecureRandom();
        // 创建一个DESKeySpec对象
        DESKeySpec desKey = new DESKeySpec(key.getBytes());
        // 创建一个密匙工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        // 将DESKeySpec对象转换成SecretKey对象
        SecretKey securekey = keyFactory.generateSecret(desKey);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES");
        // 用密匙初始化Cipher对象
        cipher.init(DECRYPT_MODE, securekey, random);
        // 真正开始解密操作
        return cipher.doFinal(src);
    }


    /***
     * 微信退款申请成功异步通知使用AES解密
     * @param str
     * @param mchSecretKey
     * @return
     * @throws Exception
     */
    public static String decryptData(String str, String mchSecretKey) {
        byte[] b = Base64Utils.decode(str.getBytes());
        SecretKeySpec key = new SecretKeySpec(MD5Util.md5(mchSecretKey).toLowerCase().getBytes(), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            return new String(cipher.doFinal(b), "utf-8");
        } catch (Exception e) {
           log.error("Exception : {}",e);
        }
        return null;
    }

    /****
     * 测试 退款aes解密
     * @param args
     * @throws Exception
     */
//    public static void main(String[] args) throws Exception {
//        String str = "hE4ze0rRMnuAPpIEzyY2KXT7+VUBqC0dLA/Cs88kqeTOUoPKVyv/kMdS0AhmAZIyI+jGGUM+ii1+6NlHPL0AQot+bU9cdHzMU5HHnkjpI9D25sdlbTK9JYQgcSMKFOVJJLcD9P+k16sCKb/dn/YW4HGroQGXRXFicHd6C1CDVsuVdfo3NjqINFWTYoj9AErQLeFtayqA9/Tto/mdio4ZA8wX34Kclo1/Y5M9BccsUFvpZezhPhg/IeS/uV+YpMrXm3cWCq55/M4Nt7KC+8lXau18cD+lNYkqDacHBXxRZI0vNgTZM9GBoePd7b+KRmFsWrqp+JEeJvUrajLRq+s1I0j+jvNAjJQzn/SAU3BAGuOx93N7BOCi1FGp00EfKNr0VCYtsrjYUkbm2pJNrd9367fDrQ+NAMSJDVGm9wXRKVtI573nEOWuKHjbormoCOX4BPpYYGM3pgap3JUkSAOGSt+h2WRop6QJT9h22eZCAU8k24nYkh5cQVN6Sgv+G7Mv8G01XmmsUhri/BIbOap2CvTLsCXkuzBAJQE05FuJg976xJs1ve89zA7QUSZ5+V9h4rV+wl306llQY/txmGVpG+Sb9pt/toe3JRBtAYnji9/LxfJG3Qh1ebCqAAiwpzPOho/6c+N7LhPUXsY32I7enllk0OC+zLzeoWqwSoIPUfSIdo7ndBqqpbtNVS1MCuPQDDEmCxvKP0/fSb0mTObtKIsxgvJwnWHQJFNXj2rY3weFchdWSXwnm8xqd9fsRypPS+Iyau7RiTAT6vGGvWT3W89IXYueM3kiLGmqh1CArvFCpASTLoRTtz+mNX+7SYR657F0PeLuiYG1M1v2/3VNP9pvTr5eHMiYocTJwnKw7G3WkXiH0Sf+V5tI/cF4ogqoWs/r698lLEL6iOVjUGhbH4RxvNWZ4U11Y7qBaIOIcagvMdERmqoFYn97Gq2RkqqSli3hudqKJGqPPn7P265NV1miGYm4Pd8SnYlIzQhatRzW+rmEW9UPNOnlXi4OYFmUd9z+DxXp3MAtn9DQdokMCAiN4QI04Rq67jf99WfC9nXYFjD6vKBqL6hrdGTsGdW4";
//        String key = "FwHb47vJar3NedeJ6dhfskIbuPPyMfTK";
//        Map<String, Object> xmlMap = XmlMapUtil.xmlToMap(DESUtil.decryptData(str, key));
//        System.out.println(xmlMap);
//    }

    /**
     * AES解密
     *
     * @param base64Data
     * @return
     * @throws Exception
     */
    private static final String ALGORITHM = "AES";
    private static final String ALGORITHM_MODE_PADDING = "AES/ECB/PKCS5Padding";

    public static String decryptData1(String data, String mchkey) throws Exception {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, DESUtil.generateKey(mchkey));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new Exception("NoSuchAlgorithmException", e);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            throw new Exception("NoSuchPaddingException", e);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new Exception("InvalidKeyException", e);

        }
        try {

            byte[] buf = cipher.doFinal(Base64Utils.decode(data.getBytes()));

            return new String(buf);

        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new Exception("IllegalBlockSizeException", e);
        } catch (BadPaddingException e) {
            e.printStackTrace();
            throw new Exception("BadPaddingException", e);
        }
    }

    private static SecretKey generateKey(String secretKey) throws NoSuchAlgorithmException {
        SecureRandom secureRandom = new SecureRandom(secretKey.getBytes());

        // 为我们选择的DES算法生成一个KeyGenerator对象
        KeyGenerator kg = null;
        try {
            kg = KeyGenerator.getInstance(ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
        }
        kg.init(secureRandom);
        //kg.init(56, secureRandom);

        // 生成密钥
        return kg.generateKey();
    }


//        //测试
//        public static void main(String args[]) {
//                //待加密内容
//                String str = "123456@12345678@123456";
//                String scene = "12345678";
//                String result = DESUtil.encrypt(str,scene);
//                System.out.println("加密后："+result + "长度为:"+ result.length());
//                //直接将如上内容解密
//                try {
//                        String decryResult = DESUtil.decryptor(result,scene);
//                        System.out.println("解密后："+decryResult);
//                } catch (Exception e1) {
//                        e1.printStackTrace();
//                }
//        }
}