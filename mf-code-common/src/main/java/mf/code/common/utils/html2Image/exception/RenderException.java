package mf.code.common.utils.html2Image.exception;

/**
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2018/10/26 0026 20:52
 */
public class RenderException extends RuntimeException {
	public RenderException(String message, Throwable cause) {
		super(message, cause);
	}
}
