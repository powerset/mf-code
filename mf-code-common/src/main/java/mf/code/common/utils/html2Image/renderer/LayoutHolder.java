package mf.code.common.utils.html2Image.renderer;

import org.xhtmlrenderer.render.Box;

/**
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2018/10/26 0026 20:52
 */
public interface LayoutHolder {
	Box getRootBox();
}
