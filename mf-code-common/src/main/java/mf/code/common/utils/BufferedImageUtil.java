package mf.code.common.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月12日 14:39
 */
@Slf4j
public class BufferedImageUtil {
    private static final String applet_poster_model = "https://asset.wxyundian.com/data-test3/forbid_poster_model/poster_model.png";
    private static final String public_address_poster_model = "https://asset.wxyundian.com/data/forbid_public_address_poster_model/public_address_poster_model3.png";
    private static final String public_address_log_model = "https://asset.wxyundian.com/data/forbid_public_address_poster_model/public_address_poster_log_model.png";

    public static BufferedImage getPublicAddressPoster(String nickName, String nickImg, String pubCodeStr) {
        BufferedImage formatAvatarImage = null;
        boolean img = false;
        if (StringUtils.isNotBlank(nickImg)) {
            img = true;
        }
        try {
            BufferedImage posterModelImage = ImageIO.read(new URL(public_address_poster_model));
            BufferedImage pubQrCodeImage = createQrCode(pubCodeStr, 195, 195);

            int width = posterModelImage.getWidth();
            int height = posterModelImage.getHeight();
            formatAvatarImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.drawImage(posterModelImage, 0, 0, width, height, null);
            graphics.drawImage(pubQrCodeImage, 41, 1114, 205, 205, null);
            if (img) {
                BufferedImage nickImage = getAppletRoundImage(ImageIO.read(new URL(nickImg)));
                graphics.drawImage(nickImage, 31, 27, 118, 118, null);
            }
            if (StringUtils.isNotBlank(nickName)) {
                graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.BOLD, 24));
                graphics.setColor(new Color(229, 178, 90));
                graphics.drawString("我是" + nickName, 169, 84);
            }
        } catch (Exception e) {
            log.error("Exception :{}", e);
        }
        return formatAvatarImage;
    }

    public static BufferedImage getPublicAddressPoster(String pubCodeStr) {
        BufferedImage formatAvatarImage = null;
        try {
            BufferedImage posterModelImage = ImageIO.read(new URL(public_address_poster_model));
            BufferedImage pubQrCodeImage = createQrCode(pubCodeStr, 195, 195);

            int width = posterModelImage.getWidth();
            int height = posterModelImage.getHeight();
            formatAvatarImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.drawImage(posterModelImage, 0, 0, width, height, null);
            graphics.drawImage(pubQrCodeImage, 41, 1114, 205, 205, null);
        } catch (Exception e) {
            log.error("Exception :{}", e);
        }
        return formatAvatarImage;
    }

    public static BufferedImage getappletPoster(BufferedImage appletBufferImg, String appletCodeUrl, String userUrl, String goodsImgUrl, String goodsName, String goodsPrice) {
        BufferedImage formatAvatarImage = null;
        try {
            BufferedImage posterModelImage = ImageIO.read(new URL(applet_poster_model));
            BufferedImage userImage = BufferedImageUtil.createRoundedImage(userUrl, 60, true);
            BufferedImage goodsImage = BufferedImageUtil.createSquareImage(goodsImgUrl, 300, 290);
            if (appletBufferImg == null && StringUtils.isNotBlank(appletCodeUrl)) {
                appletBufferImg = BufferedImageUtil.createRoundedImage(appletCodeUrl, 120, false);
            }
            int width = posterModelImage.getWidth();
            int height = posterModelImage.getHeight();
            formatAvatarImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.drawImage(posterModelImage, 0, 0, width, height, null);

            graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 26));
            graphics.setColor(new Color(0, 0, 0));
            graphics.drawString("奖品", 407, 390);
            graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 24));
            String title1 = "";
            String title2 = "";
            if (goodsName.length() <= 9) {
                title1 = goodsName;
            } else {
                title1 = goodsName.substring(0, 9);
                graphics.drawString(title1, 407, 430);
                title2 = goodsName.replaceAll(title1, "").length() > 9 ?
                        goodsName.replaceAll(title1, "").substring(0, 9) : goodsName.replaceAll(title1, "");
            }
            graphics.drawString(title1, 407, 430);
            graphics.drawString(title2, 407, 470);

            graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 18));
            graphics.setColor(new Color(250, 0, 0));
            graphics.drawString("价值￥", 407, 560);
            graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 24));
            graphics.setColor(new Color(250, 0, 0));
            graphics.drawString(goodsPrice, 460, 560);

            graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 18));
            graphics.setColor(new Color(100, 100, 100));
            graphics.drawString("自动开奖", 407, 600);

            graphics.drawImage(goodsImage, 87, 349, 300, 290, null);
            graphics.drawImage(appletBufferImg, 255, 835, 240, 240, null);
            graphics.drawImage(userImage, 75, 52, 100, 100, null);

            graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.BOLD, 18));
            graphics.setColor(new Color(255, 255, 255));
            graphics.drawString("该奖品由云店生活馆商家赞助", 440, 710);
            graphics.dispose();
        } catch (Exception e) {
            log.error("Exception :{}", e);
        }
        return formatAvatarImage;
    }

    /***
     * 设置字体
     * @param fontName 字体
     * @param fontType 字形
     * @param size 字大小
     * @return
     */
    private static Font setFont(String fontName, int fontType, int size) {
        Font font = new Font(fontName, fontType, size);
        return font;
    }

    /***
     * 将图片生成圆形
     * @param image 图片
     * @param radius 半径
     * @return
     * @throws Exception
     */
    public static BufferedImage createRoundedImage(String image, int radius, boolean isUser) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new URL(image));
        } catch (Exception e) {
            log.error("IOException :{}", e);
        }
        // 1. 把正方形生成圆形
        BufferedImage bufferedImage = convertRoundedImage(img, radius, isUser);
        return bufferedImage;
    }

    /***
     * 生成圆形
     * @param bufferedImage
     * @param radius
     * @return
     */
    private static BufferedImage convertRoundedImage(BufferedImage bufferedImage, int radius, boolean isUser) {
        BufferedImage formatAvatarImage = new BufferedImage(radius, radius, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = formatAvatarImage.createGraphics();
        //把图片切成一个圆
        {
            int border = 1;
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            //图片是一个圆型
            Ellipse2D.Double shape = new Ellipse2D.Double(border, border, radius, radius);
            //需要保留的区域
            graphics.setClip(shape);
            graphics.drawImage(bufferedImage.getScaledInstance(radius, radius, Image.SCALE_SMOOTH), border, border, radius, radius, null);
            graphics.dispose();
        }
        if (isUser) {
            //在圆图外面再画一个圆
            {
                //新创建一个graphics，这样画的圆不会有锯齿
                graphics = formatAvatarImage.createGraphics();
                graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                int border = 1;
                //画笔是4.5个像素，BasicStroke的使用可以查看下面的参考文档
                //使画笔时基本会像外延伸一定像素，具体可以自己使用的时候测试
                Stroke s = new BasicStroke(1.0F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
                graphics.setStroke(s);
//                graphics.setColor(new Color(248, 103, 135));
                graphics.drawOval(border, border, radius - border, radius - border);
                graphics.dispose();
            }
        }
        return formatAvatarImage;
    }

    /***
     * 压缩方形
     * @param image
     * @param width
     * @param height
     * @return
     * @throws Exception
     */
    private static BufferedImage createSquareImage(String image, int width, int height) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new URL(image));
        } catch (IOException e) {
            log.error("IOException :{}", e);
        }
        BufferedImage formatAvatarImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = formatAvatarImage.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int border = 0;
        //把图片切成一个方形
        {
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //图片是一个方形
            graphics.drawImage(img.getScaledInstance(width, height, Image.SCALE_SMOOTH), border, border, width, height, null);
            graphics.dispose();
        }
        return formatAvatarImage;
    }

    private static final String MODEL_URL = "https://asset.wxyundian.com/data-test3/forbid_poster_model/applet_model.png";

    /***
     * 两个图片重合叠加
     * @return
     * @throws Exception
     */
    public static BufferedImage appletImageFinalImage(InputStream inputStream) {
        BufferedImage buffImg = null;
        BufferedImage avatarImage = null;
        try {
            avatarImage = ImageIO.read(inputStream);
            int width = avatarImage.getWidth();
            //留一个像素的空白区域，这个很重要，画圆的时候把这个覆盖
            // 透明底的图片
            BufferedImage formatAvatarImage = new BufferedImage(width, width, BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.drawImage(avatarImage, 0, 0, width, width, null);
            graphics.dispose();

            buffImg = ImageIO.read(new URL(MODEL_URL));//底图
            graphics = buffImg.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.drawImage(formatAvatarImage, buffImg.getWidth() / 2 - width / 2, buffImg.getWidth() / 2 - width / 2, width, width, null);
            graphics.dispose();// 释放图形上下文使用的系统资源
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buffImg;
    }

    /***
     * 获取一个圆形图片
     * @param avatarImage
     * @return
     */
    public static BufferedImage getAppletRoundImage(BufferedImage avatarImage) {
        int width = avatarImage.getWidth();
        // 透明底的图片
        BufferedImage formatAvatarImage = new BufferedImage(width, width, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = formatAvatarImage.createGraphics();
        //把图片切成一个圓
        {
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //留一个像素的空白区域，这个很重要，画圆的时候把这个覆盖
            int border = 0;
            //图片是一个圆型
            Ellipse2D.Double shape = new Ellipse2D.Double(border, border, width - border * 2, width - border * 2);
            //需要保留的区域
            graphics.setClip(shape);
            graphics.drawImage(avatarImage, border, border, width - border * 2, width - border * 2, null);
            graphics.dispose();
        }
        //在圆图外面再画一个圆
        {
            //新创建一个graphics，这样画的圆不会有锯齿
            graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int border = 1;
            //画笔是4.5个像素，BasicStroke的使用可以查看下面的参考文档
            //使画笔时基本会像外延伸一定像素，具体可以自己使用的时候测试
            Stroke s = new BasicStroke(4.5F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
            graphics.setStroke(s);
            graphics.setColor(new Color(229, 178, 90));
            graphics.drawOval(border, border, width - border * 2, width - border * 2);
            graphics.dispose();
        }
        return formatAvatarImage;
    }

    public static boolean checkUrlIsValid(String urlStr) {
        URL url;
        try {
            url = new URL(urlStr);
            InputStream in = url.openStream();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /***
     * 创建一个二维码字符串生成二维码图片
     * @param url
     * @param width
     * @param height
     * @return
     */
    public static BufferedImage createQrCode(String url, int width, int height) {
        try {
            Map<EncodeHintType, String> hints = new HashMap<>();
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, String.valueOf(1)); //设置白边
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H.name());
            BitMatrix bitMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, width, height, hints);
            //生成二维码图片
            BufferedImage image = toBufferedImage(bitMatrix);
            //生成二维码中间log
            BufferedImage imageLog = setMatrixLogo(image, public_address_log_model);
            return imageLog;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //将字符串生成图片
    private static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? Color.BLACK.getRGB() : Color.WHITE.getRGB());
            }
        }
        image.flush();
        return image;
    }

    //生成二维码中间log
    public static BufferedImage setMatrixLogo(BufferedImage matrixImage, String logUri) throws IOException {
        /**
         * 读取二维码图片，并构建绘图对象
         */
        Graphics2D g2 = matrixImage.createGraphics();
        int matrixWidth = matrixImage.getWidth();
        int matrixHeigh = matrixImage.getHeight();
        BufferedImage logo = ImageIO.read(new URL(logUri));//读取logo图片

        //开始绘制图片前两个、/5*2/5*2 后/5
        g2.drawImage(logo, matrixWidth / 5 * 2, matrixHeigh / 5 * 2, matrixWidth / 5, matrixHeigh / 5, null);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //绘制边框
        BasicStroke stroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        // 设置笔画对象
        g2.setStroke(stroke);
        //指定弧度的圆角矩形
        RoundRectangle2D.Float round = new RoundRectangle2D.Float(matrixWidth / 5 * 2, matrixHeigh / 5 * 2, matrixWidth / 5, matrixHeigh / 5, 20, 20);
        g2.setColor(Color.white);
        // 绘制圆弧矩形
        g2.draw(round);

        //设置logo 有一道灰色边框
        BasicStroke stroke2 = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        // 设置笔画对象
        g2.setStroke(stroke2);
        RoundRectangle2D.Float round2 = new RoundRectangle2D.Float(matrixWidth / 5 * 2 + 2, matrixHeigh / 5 * 2 + 2, matrixWidth / 5 - 4, matrixHeigh / 5 - 4, 20, 20);
        g2.setColor(new Color(220, 220, 220));
        g2.draw(round2);// 绘制圆弧矩形
        g2.dispose(); //执行刷出返回带logo二维码
        logo.flush();
        matrixImage.flush();
        return setClip(matrixImage);
    }

    /**
     * 图片切圆角
     *
     * @param srcImage
     * @return
     */
    public static BufferedImage setClip(BufferedImage srcImage) {
        int width = srcImage.getWidth();
        int height = srcImage.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gs = image.createGraphics();
        //把图片切成一个圆
        {
            gs.setComposite(AlphaComposite.SrcOut);
            gs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //需要保留的区域
            gs.fill(new RoundRectangle2D.Float(0, 0, width, height, 50, 50));
            gs.setComposite(AlphaComposite.SrcAtop);
            gs.drawImage(srcImage, 0, 0, null);
            gs.dispose();
        }
        image.flush();
        return image;
    }


    /*************************************************************************************************************************/
//    private static final String URL = "https://jkmf.oss-cn-hangzhou.aliyuncs.com/data-test3/wxcode/shop/1/1%2C1.png";
//    public static void main(String[] args) throws Exception {
//        BufferedImage avatarImage = BufferedImageUtil.appletImageFinalImage(URL);
//        int width = avatarImage.getWidth();
//        // 透明底的图片
//        BufferedImage formatAvatarImage = new BufferedImage(width, width, BufferedImage.TYPE_4BYTE_ABGR);
//        Graphics2D graphics = formatAvatarImage.createGraphics();
//        //把图片切成一个圓
//        {
//            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//            //留一个像素的空白区域，这个很重要，画圆的时候把这个覆盖
//            int border = 0;
//            //图片是一个圆型
//            Ellipse2D.Double shape = new Ellipse2D.Double(border, border, width - border * 2, width - border * 2);
//            //需要保留的区域
//            graphics.setClip(shape);
//            graphics.drawImage(avatarImage, border, border, width - border * 2, width - border * 2, null);
//            graphics.dispose();
//        }
//        try (OutputStream os = new FileOutputStream("E:\\temp-avatar-1.png")) {
//            ImageIO.write(formatAvatarImage, "PNG", os);
//        }
//    }

    //    public static void main(String[] args) throws Exception {
//        BufferedImage avatarImage = ImageIO.read(new URL(poster_model));
//
//        BufferedImage userImage = BufferedImageUtil.createRoundedImage(PERSONAVATAR, 60);
//        BufferedImage appletImage = BufferedImageUtil.createRoundedImage(URL, 120);
//        BufferedImage goodsImage = BufferedImageUtil.createSquareImage(goods_img, 300, 290);
//
//        int width = avatarImage.getWidth();
//        System.out.println(width);
//        int height = avatarImage.getHeight();
//        System.out.println(height);
//        BufferedImage formatAvatarImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//        Graphics2D graphics = formatAvatarImage.createGraphics();
//        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//        graphics.drawImage(avatarImage, 0, 0, width, height, null);
//
//        graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 26));
//        graphics.setColor(new Color(0, 0, 0));
//        graphics.drawString("奖品", 407, 390);
//
//        graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 24));
//        String title1 = goods_name.substring(0, 9);
//        graphics.drawString(title1, 407, 426);
//        String title2 = goods_name.replaceAll(title1, "").length() > 9 ? goods_name.replaceAll(title1, "").substring(0, 9) : goods_name.replaceAll(title1, "");
//        graphics.drawString(title2, 407, 466);
//
//        graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 18));
//        graphics.setColor(new Color(250, 0, 0));
//        graphics.drawString("价值￥", 407, 560);
//
//        graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 24));
//        graphics.setColor(new Color(250, 0, 0));
//        graphics.drawString("128", 460, 560);
//
//        graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.PLAIN, 18));
//        graphics.setColor(new Color(100, 100, 100));
//        graphics.drawString("自动开奖", 407, 600);
//
//        graphics.drawImage(goodsImage, 87, 349, 300, 290, null);
//        graphics.drawImage(appletImage, 255, 837, 240, 240, null);
//        graphics.drawImage(userImage, 75, 52, 100, 100, null);
//        graphics.setFont(BufferedImageUtil.setFont("Microsoft Yahei", Font.BOLD, 18));
//        graphics.setColor(new Color(255, 255, 255));
//        graphics.drawString("该奖品由云店生活馆商家赞助", 440, 710);
//        graphics.dispose();
//
//        try (OutputStream os = new FileOutputStream("E:\\user-avatar.png")) {
//            ImageIO.write(formatAvatarImage, "PNG", os);
//        }
//    }
    public static void main(String[] args) throws IOException {
        String url = "http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOYJOrUGCdDhfeiatpibpnsoSyEnsbE5icial20hiaxErR02xVJURS5AV5XkSNgDSv4fH1U8hYbBe2wuw/132";
        BufferedImage avatarImage = ImageIO.read(new URL(url));
        int width = 120;
        // 透明底的图片
        BufferedImage formatAvatarImage = new BufferedImage(width, width, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = formatAvatarImage.createGraphics();
        //把图片切成一个圓
        {
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //留一个像素的空白区域，这个很重要，画圆的时候把这个覆盖
            int border = 0;
            //图片是一个圆型
            Ellipse2D.Double shape = new Ellipse2D.Double(border, border, width - border * 2, width - border * 2);
            //需要保留的区域
            graphics.setClip(shape);
            graphics.drawImage(avatarImage, border, border, width - border * 2, width - border * 2, null);
            graphics.dispose();
        }
        //在圆图外面再画一个圆
        {
            //新创建一个graphics，这样画的圆不会有锯齿
            graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int border = 3;
            //画笔是4.5个像素，BasicStroke的使用可以查看下面的参考文档
            //使画笔时基本会像外延伸一定像素，具体可以自己使用的时候测试
            Stroke s = new BasicStroke(4.5F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
            graphics.setStroke(s);
            graphics.setColor(new Color(229, 178, 90));
            graphics.drawOval(border, border, width - border * 2, width - border * 2);
            graphics.dispose();
        }
        try (OutputStream os = new FileOutputStream("E:/temp-avatar.png")) {
            ImageIO.write(formatAvatarImage, "PNG", os);
        }
    }
    //248,103,135 海报底板颜色

    private static final String code_str = "https://test3-applet.wxyundian.com/api/seller/merchant/fission/wxLogin";

//    public static void main(String[] args) throws Exception {
//        String url = code_str;
//
//        Map<EncodeHintType, String> hints = new HashMap<>();
//        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
//        BufferedImage image = createQrCode(url, 250, 250);
//        try (OutputStream os = new FileOutputStream("E:/temppub1.jpg")) {
//            ImageIO.write(image, "PNG", os);
//        }
//    }
}
