package mf.code.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月14日 18:17
 */
public class UserSecondNameUtil {
    private static List<String> userSecondNameList = new ArrayList<String>();

    static {
        userSecondNameList.add("睿");userSecondNameList.add("悦");userSecondNameList.add("子");userSecondNameList.add("轩");userSecondNameList.add("雨");userSecondNameList.add("涵");userSecondNameList.add("峰");userSecondNameList.add("蔼");userSecondNameList.add("颖");userSecondNameList.add("俊");userSecondNameList.add("达");userSecondNameList.add("乎");userSecondNameList.add("浩宇");userSecondNameList.add("欣怡");userSecondNameList.add("浩");userSecondNameList.add("妍");userSecondNameList.add("梓");userSecondNameList.add("宇");userSecondNameList.add("梓");userSecondNameList.add("萱");userSecondNameList.add("鹏");userSecondNameList.add("仁");
        userSecondNameList.add("灵");userSecondNameList.add("威");userSecondNameList.add("耀");userSecondNameList.add("安");userSecondNameList.add("子轩");userSecondNameList.add("子涵");userSecondNameList.add("贤");userSecondNameList.add("慧");userSecondNameList.add("焕");userSecondNameList.add("旺");userSecondNameList.add("坦");userSecondNameList.add("子涵");userSecondNameList.add("诗涵");userSecondNameList.add("博");userSecondNameList.add("涵");userSecondNameList.add("浩");userSecondNameList.add("泽");userSecondNameList.add("欣");userSecondNameList.add("怡");userSecondNameList.add("艳");userSecondNameList.add("容");userSecondNameList.add("睿");userSecondNameList.add("英");userSecondNameList.add("兴");userSecondNameList.add("静");userSecondNameList.add("浩然");userSecondNameList.add("梓涵");
        userSecondNameList.add("瑞");userSecondNameList.add("玥");userSecondNameList.add("宇");userSecondNameList.add("杰");userSecondNameList.add("子");userSecondNameList.add("彤");userSecondNameList.add("德");userSecondNameList.add("锐");userSecondNameList.add("健");userSecondNameList.add("荣");userSecondNameList.add("顺");userSecondNameList.add("雨泽");userSecondNameList.add("雨涵");userSecondNameList.add("昊");userSecondNameList.add("蕊");userSecondNameList.add("俊");userSecondNameList.add("豪");userSecondNameList.add("思");userSecondNameList.add("琪");userSecondNameList.add("轩");userSecondNameList.add("哲");userSecondNameList.add("壮");
        userSecondNameList.add("华");userSecondNameList.add("通");userSecondNameList.add("宇轩");userSecondNameList.add("可馨");userSecondNameList.add("义");userSecondNameList.add("显");userSecondNameList.add("雄");userSecondNameList.add("盛");userSecondNameList.add("和");userSecondNameList.add("良");userSecondNameList.add("敦");userSecondNameList.add("挺");userSecondNameList.add("盈");userSecondNameList.add("泰");userSecondNameList.add("伦");userSecondNameList.add("迪");userSecondNameList.add("秀");userSecondNameList.add("丰");userSecondNameList.add("然");userSecondNameList.add("正");userSecondNameList.add("明");userSecondNameList.add("伟");userSecondNameList.add("余");userSecondNameList.add("宁");userSecondNameList.add("清");userSecondNameList.add("晓");userSecondNameList.add("武");userSecondNameList.add("昌");userSecondNameList.add("定");
    }

    /***
     * random.nextInt 介于[0,n)
     * @return
     */
    public static String getRandomSecondName() {
        int max = userSecondNameList.size();
        Random random = new Random();
        int s = random.nextInt(max);
        return userSecondNameList.get(s);
    }

//    public static void main(String[] args) {
//        System.out.println(UserSecondNameUtil.getRandomSecondName());
//    }
}
