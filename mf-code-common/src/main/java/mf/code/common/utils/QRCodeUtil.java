package mf.code.common.utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.IOUtils;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Hashtable;

/**
 * mf.code.common.utils
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-15 下午1:02
 */
@Slf4j
public class QRCodeUtil {

    /**
     * 创建二维码
     * @param url
     * @param format
     * @return
     * @throws IOException
     */
    public static InputStream createQRCode(String url,String format) {
        int width = 500;
        int height = 500;
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, width, height, hints);

            BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix);
            ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
            ImageOutputStream imOut = ImageIO.createImageOutputStream(os);

            ImageIO.write(image, format, imOut);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
            byte[] b = os.toByteArray();//从流中获取数据数组。

            InputStream is = new ByteArrayInputStream(b);
            IOUtils.closeQuietly(imOut);

            return is;


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            //DeleteFileUtil.delete(fileDirectory);
        }
        return null;
    }

    /**
     * 解析出二维码的url
     * 无用
     * @param file
     * @param fileDirectory
     * @throws NotFoundException
     */
    public static void anlaysisQRCode(File file,String fileDirectory) throws NotFoundException {
        MultiFormatReader formatReader=new MultiFormatReader();
        BufferedImage image=null;
        try {
            image = ImageIO.read(file);
            BinaryBitmap binaryBitmap =new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
            Hashtable hints=new Hashtable();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            Result result=formatReader.decode(binaryBitmap,hints);
            log.info("解析结果："+result.toString());
            log.info("解析格式:"+result.getBarcodeFormat());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            // DeleteFileUtil.delete(fileDirectory);
        }
    }
}
