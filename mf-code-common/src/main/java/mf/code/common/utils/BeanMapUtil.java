package mf.code.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanMap;

import java.lang.reflect.Field;
import java.util.*;

/**
 * mf.code.common.utils
 * Description:
 *
 * @author: gel
 * @date: 2018-11-16 14:49
 */
@Slf4j
public class BeanMapUtil {
    /**
     * 将对象装换为map
     *
     * @param bean
     * @return
     */
    public static <T> Map<String, Object> beanToMap(T bean) {

        Map<String, Object> map = new HashMap<>(16);
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(key + "", beanMap.get(key));
            }
        }
        return map;
    }


    /**
     * 将对象装换为map, 排除字段
     *
     * @param bean
     * @return
     */
    public static <T> Map<String, Object> beanToMapIgnore(T bean, String... values) {
        List<String> ignoreList = values != null ? Arrays.asList(values) : new ArrayList<>();
        Map<String, Object> map = new HashMap<>(16);
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                if(key == null){
                    continue;
                }
                if(ignoreList.contains(key.toString())){
                    continue;
                }
                map.put(key.toString(), beanMap.get(key));
            }
        }
        return map;
    }


    /**
     * 将对象装换为map, 排除字段
     *
     * @param beanList
     * @param values
     * @return
     */
    public static <T> List<Map<String, Object>> beanListToMapIgnore(List<T> beanList, String... values) {
        List<String> ignoreList = values != null ? Arrays.asList(values) : new ArrayList<>();
        List<Map<String, Object>> list = new ArrayList<>();
        if (beanList != null && beanList.size() > 0) {
            T bean;
            for (int i = 0, size = beanList.size(); i < size; i++) {
                bean = beanList.get(i);
                Map<String, Object> map = new HashMap<>(16);
                BeanMap beanMap = BeanMap.create(bean);
                for (Object key : beanMap.keySet()) {
                    if(key == null){
                        continue;
                    }
                    String keyStr = key.toString();
                    if(ignoreList.contains(keyStr)){
                        continue;
                    }
                    map.put(keyStr, beanMap.get(key));
                }
                list.add(map);
            }
        }
        return list;
    }

    /**
     * 将map装换为javabean对象
     *
     * @param map
     * @param bean
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, T bean) {
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(map);
        return bean;
    }

    /**
     * 将List<T>转换为List<Map<String, Object>>
     *
     * @param objList
     * @return
     */
    public static <T> List<Map<String, Object>> objectsToMaps(List<T> objList) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (objList != null && objList.size() > 0) {
            Map<String, Object> map = null;
            T bean = null;
            for (int i = 0, size = objList.size(); i < size; i++) {
                bean = objList.get(i);
                map = beanToMap(bean);
                list.add(map);
            }
        }
        return list;
    }

    /**
     * 将List<Map<String,Object>>转换为List<T>
     *
     * @param maps
     * @param clazz
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static <T> List<T> mapsToObjects(List<Map<String, Object>> maps, Class<T> clazz) throws InstantiationException, IllegalAccessException {
        List<T> list = new ArrayList<>();
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = null;
            T bean = null;
            for (int i = 0, size = maps.size(); i < size; i++) {
                map = maps.get(i);
                bean = clazz.newInstance();
                mapToBean(map, bean);
                list.add(bean);
            }
        }
        return list;
    }

    public static Map<String, Object> obj2Map(Object obj) {

        Map<String, Object> map = new HashMap<String, Object>();
        // System.out.println(obj.getClass());
        // 获取f对象对应类中的所有属性域
        Field[] fields = obj.getClass().getDeclaredFields();
        for (int i = 0, len = fields.length; i < len; i++) {
            String varName = fields[i].getName();
            //varName = varName.toLowerCase();//将key置为小写，默认为对象的属性
            try {
                // 获取原来的访问控制权限
                boolean accessFlag = fields[i].isAccessible();
                // 修改访问控制权限
                fields[i].setAccessible(true);
                // 获取在对象f中属性fields[i]对应的对象中的变量
                Object o = fields[i].get(obj);
                if(o != null){
                    map.put(varName, o.toString());
                }
                // System.out.println("传入的对象中包含一个如下的变量：" + varName + " = " + o);
                // 恢复访问控制权限
                fields[i].setAccessible(accessFlag);
            } catch (IllegalArgumentException ex) {
                log.error("IllegalArgumentException : {}",ex);
            } catch (IllegalAccessException ex) {
                log.error("IllegalAccessException : {}",ex);
            }
        }
        return map;
    }
}
