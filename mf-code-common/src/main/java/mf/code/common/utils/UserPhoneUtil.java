package mf.code.common.utils;

import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月14日 19:19
 */
public class UserPhoneUtil {
    private static Map<String, List<String>> userPhoneMap = new HashMap<String, List<String>>();

    static {
        userPhoneMap.put("北京市", Arrays.asList("1301127", "1312165"));
        userPhoneMap.put("上海市", Arrays.asList("1306171", "1348251"));
        userPhoneMap.put("天津市", Arrays.asList("1881260", "1560214"));
        userPhoneMap.put("云南省", Arrays.asList("1376901", "1331259"));
        userPhoneMap.put("内蒙古自治区", Arrays.asList("1308712", "1323470"));
        userPhoneMap.put("吉林省", Arrays.asList("1307979", "1357852"));
        userPhoneMap.put("四川省", Arrays.asList("1532809", "1890817"));
        userPhoneMap.put("宁夏回族自治区", Arrays.asList("1522628", "1571958"));
        userPhoneMap.put("安徽省", Arrays.asList("1386679", "1595516"));
        userPhoneMap.put("山东省", Arrays.asList("1337640", "1368864"));
        userPhoneMap.put("山西省", Arrays.asList("1320983", "1361062"));
        userPhoneMap.put("广东省", Arrays.asList("1316878", "1341038"));
        userPhoneMap.put("广西省", Arrays.asList("1309793"));
        userPhoneMap.put("新疆维吾尔自治区", Arrays.asList("1309503"));
        userPhoneMap.put("江苏省", Arrays.asList("1365517"));
        userPhoneMap.put("江西省", Arrays.asList("1357696"));
        userPhoneMap.put("河北省", Arrays.asList("1383120"));
        userPhoneMap.put("河南省", Arrays.asList("1513874"));
        userPhoneMap.put("浙江省", Arrays.asList("1309379"));
        userPhoneMap.put("海南省", Arrays.asList("1321570"));
        userPhoneMap.put("湖北省", Arrays.asList("1309416"));
        userPhoneMap.put("湖南省", Arrays.asList("1321264"));
        userPhoneMap.put("甘肃省", Arrays.asList("1365932"));
        userPhoneMap.put("福建省", Arrays.asList("1380062"));
        userPhoneMap.put("西藏自治区", Arrays.asList("1322891"));
        userPhoneMap.put("贵州省", Arrays.asList("1303780"));
        userPhoneMap.put("辽宁省", Arrays.asList("1389863"));
        userPhoneMap.put("重庆市", Arrays.asList("1304830"));
        userPhoneMap.put("陕西省", Arrays.asList("1314923"));
        userPhoneMap.put("青海省", Arrays.asList("1319576"));
        userPhoneMap.put("黑龙江省", Arrays.asList("1580457"));
    }

    /***
     * random.nextInt 介于[0,n)
     * @return
     */
    public static String getRandomUserPhone(String key) {
        List<String> list = userPhoneMap.get(key);
        if (CollectionUtils.isEmpty(list)) {
            return "";
        }
        int max = list.size();
        Random random = new Random();
        int s = random.nextInt(max);
        return list.get(s) + RandomStrUtil.randomNumberStr(4);
    }

//    public static void main(String[] args) {
//        String key = "宁夏回族自治区";
//        System.out.println(UserPhoneUtil.getRandomUserPhone(key));
//    }
}
