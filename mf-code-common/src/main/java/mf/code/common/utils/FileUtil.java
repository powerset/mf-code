package mf.code.common.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;

/**
 * mf.code.common.utils
 * Description:
 *
 * @author: gel
 * @date: 2018-11-10 17:01
 */
public class FileUtil {

    //验证图片格式
    public static boolean checkFileType(String[] allow,String fileName) {
        Iterator<String> type = Arrays.asList(allow).iterator();
        while (type.hasNext()) {
            String ext = type.next();
            if (fileName.endsWith(ext)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 创建指定目录
     * @param dir
     */
    public static void createFileDir(String dir){
        File file = new File(dir);
        if(!file.exists()){
            file.mkdirs();
        }
    }
}
