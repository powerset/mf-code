package mf.code.common.utils;

import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * mf.code.common.utils
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月14日 17:24
 */
public class WxNickUtil {
    private static final String avatarUrl_front = "https://asset.wxyundian.com/data-test3/forbid_robot_user/";// robot1.jpg
    private static List<String> wxNickList = new ArrayList<String>();

    static {
        wxNickList.add("留の笑颜");wxNickList.add("咆哮");wxNickList.add("绳情");wxNickList.add("ˉ夨落旳尐");
        wxNickList.add("来自火星球");wxNickList.add("一生荒唐°");wxNickList.add("执念，爱");wxNickList.add("青瓷清茶倾");wxNickList.add("呆萌");
        wxNickList.add("可儿");wxNickList.add("高冷低能");wxNickList.add("不浪漫罪");wxNickList.add("发型不乱");wxNickList.add("╭摇划花");
        wxNickList.add("龙卷风卷");wxNickList.add("猫腻");wxNickList.add("厌归人。");wxNickList.add("醉红颜");
        wxNickList.add("又落空");wxNickList.add("桃洛憬");wxNickList.add("请在乎我");wxNickList.add("熏染");wxNickList.add("巴黎盛开");
        wxNickList.add("白恍");wxNickList.add("我要变勇");wxNickList.add("揉乱头发");wxNickList.add("烟花巷陌");wxNickList.add("艺菲");
        wxNickList.add("玩味");wxNickList.add("秒淘你心");wxNickList.add("柠栀");wxNickList.add("你身上有");wxNickList.add("如你所愿");
        wxNickList.add("没过试用");wxNickList.add("陌上花");wxNickList.add("开心的笨");wxNickList.add("在哪跌倒");wxNickList.add("限量版女");
        wxNickList.add("蝶恋花╮");wxNickList.add("优雅的叶");wxNickList.add("ー半忧伤");wxNickList.add("君临臣");wxNickList.add("夏日倾情");
        wxNickList.add("闲肆");wxNickList.add("暖瞳");wxNickList.add("珠穆郎马");wxNickList.add("站上冰箱");wxNickList.add("断秋风");
        wxNickList.add("拥菢过后");wxNickList.add("安好如初");wxNickList.add("男神大妈");wxNickList.add("晨与橙与");wxNickList.add("泡泡龙");
        wxNickList.add("刺心爱人");wxNickList.add("陌然淺笑");wxNickList.add("龙吟凤");wxNickList.add("别留遗憾");wxNickList.add("太易動情");
        wxNickList.add("烟雨萌萌");wxNickList.add("伤离别");wxNickList.add("心贝");wxNickList.add("嗯咯");wxNickList.add("病房");wxNickList.add("罪歌");
        wxNickList.add("久爱不厌");wxNickList.add("々爱被冰");wxNickList.add("单身i");wxNickList.add("人心叵测");wxNickList.add("爱你的小");wxNickList.add("封心锁爱");
        wxNickList.add("生命一旅");wxNickList.add("(り。薆情");wxNickList.add("红尘滚滚");wxNickList.add("赋流云");wxNickList.add("余温散尽");wxNickList.add("無極卍盜");
        wxNickList.add("我就是这");wxNickList.add("她最好i");wxNickList.add("←极§速");wxNickList.add("窒息");wxNickList.add("自繩自縛");wxNickList.add("念安я");
        wxNickList.add("陌上蔷薇");wxNickList.add("虚伪了的");wxNickList.add("嘲笑！");wxNickList.add("北朽暖栀");wxNickList.add("命该如此");wxNickList.add("傲世九天");
        wxNickList.add("﹏櫻之舞");wxNickList.add("浅笑√倾");wxNickList.add("你瞒我瞒");wxNickList.add("各自安好");wxNickList.add("墨染殇雪");wxNickList.add("温柔腔");
        wxNickList.add("落花忆梦");wxNickList.add("灵魂摆渡");wxNickList.add("莫飞霜");wxNickList.add("疲倦了");wxNickList.add("若他只爱");wxNickList.add("南初");wxNickList.add("不忘初心");
    }

    /***
     * random.nextInt 介于[0,n)
     * @return
     */
    public static String getRandomWxNick() {
        int max = wxNickList.size();
        Random random = new Random();
        int s = random.nextInt(max);
        return wxNickList.get(s);
    }

    public static String getRandomWxAvatarUrlFromOss() {
        int urlRandomNum = NumberUtils.toInt(RobotUserUtil.getRandom(1, 110));
        return avatarUrl_front + "robot" + urlRandomNum + ".jpg";
    }

    public static void main(String[] args) {
        System.out.println(WxNickUtil.getRandomWxNick());
        System.out.println(WxNickUtil.getRandomWxAvatarUrlFromOss());
    }
}
