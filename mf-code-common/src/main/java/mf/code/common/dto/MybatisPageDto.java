package mf.code.common.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.shop.api
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 09:58
 */
@Data
public class MybatisPageDto<T> {

    private List<T> content = new ArrayList<>();
    private long size;// 每页条数
    private long total;// 总条数
    private long currentPage;// 当前页
    private long pages;//总页码

    public MybatisPageDto() {
    }

    public MybatisPageDto(long current, long size, long total, long pages) {
        this.size = size;
        this.total = total;
        this.currentPage = current;
        this.pages = pages;
        this.content = new ArrayList<>();
    }

    public void from(long current, long size, long total, long pages) {
        this.size = size;
        this.total = total;
        this.currentPage = current;
        this.pages = pages;
    }

    public void from(long current, long size, long total) {
        this.size = size;
        this.total = total;
        this.currentPage = current;
        this.pages = total % size == 0 ? total / size : (int) Math.floor(total / size) + 1;
    }
}

