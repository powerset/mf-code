//package mf.code.comm.mongo.po;
//
//import lombok.Data;
//import lombok.ToString;
//import org.apache.commons.lang.time.DateFormatUtils;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;
//import org.springframework.data.mongodb.core.mapping.Field;
//
//import java.util.Date;
//
///**
// * mf.code.comm.mongo.po
// *
// * @description:
// * @author: yechen
// * @email: wangqingfeng@wxyundian.com
// * @date: 2019年06月10日 18:10
// */
//@Data
//@ToString
//@Document(value = "user_share")
//public class UserShare {
//    @Id
//    @Field("_id")
//    private String id;
//    /**
//     * 商户编号
//     */
//    @Field("merchant_id")
//    private Long merchantId;
//    /**
//     * 店铺编号
//     */
//    @Field("shop_id")
//    private Long shopId;
//    /**
//     * 用户编号
//     */
//    @Field("user_id")
//    private Long userId;
//    /**
//     * 用户编号
//     */
//    @Field("nick_name")
//    private String nickName;
//    /**
//     * 用户头像
//     */
//    @Field("avatar")
//    private String avatar;
//    /**
//     * 用户次数
//     */
//    @Field("user_time")
//    private int userTime;
//    /**
//     * 用户分享类型
//     */
//    @Field("biz_type")
//    private int bizType;
//    /**
//     * 用户分享类型对应的业务值
//     */
//    @Field("biz_value")
//    private Long bizValue;
//    /**
//     * 创建时间
//     */
//    @Field(value = "ctime")
//    private Date ctime;
//    /**
//     * 更新时间
//     */
//    @Field(value = "utime")
//    private Date utime;
//
//
//    public void setupId(int bizType, long bizValue, long userId, Date time) {
//        this.id = DateFormatUtils.format(time, "yyyyMMddHHmmss") + "_" + userId + "_" + bizValue + "_" + bizType;
//    }
//}
