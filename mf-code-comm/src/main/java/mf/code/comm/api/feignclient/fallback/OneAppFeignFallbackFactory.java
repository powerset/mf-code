package mf.code.comm.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.comm.api.feignclient.OneAppService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.CommonDictReqDTO;
import org.springframework.stereotype.Component;

/**
 * mf.code.user.api.feignclient.fallback
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:07
 */
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable cause) {
        return new OneAppService() {
            @Override
            public SimpleResponse createCommonDict(CommonDictReqDTO commonDictReq) {
                return null;
            }

            @Override
            public SimpleResponse queryCommonDict(String type, String key) {
                return null;
            }
        };
    }
}
