package mf.code.comm.api.feignservice;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.AppPayReq;
import mf.code.alipay.RequestRefundReq;
import mf.code.alipay.ResponseRefundDTO;
import mf.code.comm.common.caller.alipay.req.RequestTradeAppPayDTO;
import mf.code.alipay.ResponseTradeAppPayDTO;
import mf.code.comm.common.caller.alipay.AlipayService;
import mf.code.comm.common.caller.alipay.req.RequestTradeRefundDTO;
import mf.code.comm.common.caller.douyin.DouyinProperty;
import mf.code.comm.common.caller.douyin.DouyinService;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.comm.repo.po.CommonDict;
import mf.code.comm.service.CommonDictService;
import mf.code.comm.service.TemplateMessageService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.comm.common.caller.douyin.req.RequestTradeCreateDTO;
import mf.code.douyin.DouyinPayReq;
import mf.code.douyin.constant.DouyinPaySceneEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.comm.api.feignservice
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:29
 */
@Slf4j
@RestController
public class CommAppServiceImpl {
    @Autowired
    private TemplateMessageService templateMessageService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private AlipayService alipayService;
    @Autowired
    private DouyinService douyinService;
    @Autowired
    private DouyinProperty douyinProperty;

    /***
     * 消息推送
     * @param templateMsgDTO
     * @return
     */
    @PostMapping("/feignapi/one/applet/v9/sendJkmfTemplateMsg")
    public SimpleResponse sendJkmfTemplateMsg(@RequestBody TemplateMsgDTO templateMsgDTO) {
        templateMessageService.jkmfSendTemplateMessage(templateMsgDTO.getTemplateId(), templateMsgDTO.getUser(), templateMsgDTO.getData());
        return new SimpleResponse();
    }

    /**
     * 按照类型键值查询
     *
     * @param type
     * @param key
     * @return
     */
    @GetMapping("/feignapi/comm/selectByTypeKey")
    public CommonDictDTO selectByTypeKey(@RequestParam("type") String type,
                                         @RequestParam("key") String key) {
        if (StringUtils.isBlank(type) || StringUtils.isBlank(key)) {
            log.error("参数异常");
            return null;
        }
        CommonDict commonDict = commonDictService.selectByTypeKey(type, key);
        if (commonDict == null) {
            log.error("查无记录, type = {}, key = {}", type, key);
            return null;
        }
        CommonDictDTO commonDictDTO = new CommonDictDTO();
        BeanUtils.copyProperties(commonDict, commonDictDTO);
        return commonDictDTO;
    }

    /**
     * 保存或更新数据
     *
     * @param commonDictDTO
     * @return
     */
    @PostMapping("/feignapi/comm/saveOrUpdateByTypeKey")
    public boolean saveOrUpdateByTypeKey(@RequestBody CommonDictDTO commonDictDTO) {
        if (commonDictDTO == null) {
            log.error("参数异常");
            return false;
        }
        Date now = new Date();

        String type = commonDictDTO.getType();
        String key = commonDictDTO.getKey();
        CommonDict commonDict = commonDictService.selectByTypeKey(type, key);
        if (commonDict == null) {
            CommonDict newCommonDict = new CommonDict();
            BeanUtils.copyProperties(commonDictDTO, newCommonDict);
            newCommonDict.setCtime(now);
            newCommonDict.setUtime(now);
            Integer save = commonDictService.create(newCommonDict);
            return save != 0;
        }
        Long id = commonDict.getId();
        BeanUtils.copyProperties(commonDictDTO, commonDict);
        commonDict.setId(id);
        commonDict.setUtime(now);
        Integer update = commonDictService.update(commonDict);
        return update != 0;
    }

    /***
     * app alipay支付
     *
     * @param req
     * @return
     */
    @PostMapping("/feignapi/comm/appAlipay")
    public ResponseTradeAppPayDTO appAlipay(@RequestBody AppPayReq req) {
        RequestTradeAppPayDTO requestTradeAppPayDTO = new RequestTradeAppPayDTO();
        BeanUtils.copyProperties(req, requestTradeAppPayDTO);
        AlipayTradeAppPayRequest alipayTradeAppPayRequest = alipayService.combineAppTradePayParams(requestTradeAppPayDTO, req.getReturnUrl());
        ResponseTradeAppPayDTO responseTradeAppPayDTO = alipayService.appTradePay(alipayTradeAppPayRequest);
        return responseTradeAppPayDTO;
    }

    /***
     * 抖音下单获取订单号
     *
     * @param req
     * @return
     */
    @PostMapping("/feignapi/comm/douyinPay")
    public Map douyinPay(@RequestBody DouyinPayReq req) {
        RequestTradeCreateDTO requestTradeAppPayDTO = new RequestTradeCreateDTO();
        BeanUtils.copyProperties(req, requestTradeAppPayDTO);
        Map<String, Object> map = douyinService.tradeCreate(requestTradeAppPayDTO, req.getIp());
        return map;
    }


    /***
     * 抖音下单获取场景信息
     *
     * @param payScene
     * @return
     */
    @GetMapping("/feignapi/comm/douyinPayScene")
    public Map douyinPayScene(@RequestParam("payScene") int payScene) {
        Map map = new HashMap();
        if (DouyinPaySceneEnum.DOU_SHOP_MERCHANT.getCode() == payScene) {
            map.put("appId", douyinProperty.getMerchantVersionAppId());
            map.put("merchantId", douyinProperty.getMerchantVersionMerchantId());
            map.put("appSecret", douyinProperty.getMerchantVersionSecret());
        } else {
            map.put("appId", douyinProperty.getUserVersionAppId());
            map.put("merchantId", douyinProperty.getUserVersionMerchantId());
            map.put("appSecret", douyinProperty.getUserVersionSecret());
        }
        return map;
    }

    @PostMapping("/feignapi/comm/alipayOrderRefund")
    public ResponseRefundDTO alipayOrderRefund(@RequestBody RequestRefundReq requestRefundReq) {
        RequestTradeRefundDTO req = new RequestTradeRefundDTO();
        BeanUtils.copyProperties(requestRefundReq, req);
        AlipayTradeRefundRequest refundRequest = alipayService.combineTradeRefundParams(req);
        log.info("<<<<<<<<alipay退款入参 refund req:{}", refundRequest);
        ResponseRefundDTO responseRefundDTO = alipayService.refundResp(refundRequest);
        log.info("<<<<<<<<alipay退款返回 refund resp:{}", responseRefundDTO);
        return  responseRefundDTO;
//        if(!StringUtils.isEmpty(responseRefundDTO.getSubCode())){
//            System.out.println("错误欢迎入驻：^_^");
//        }
    }
}
