package mf.code.comm.api.feignclient;

import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.comm.api.feignclient.fallback.OrderAppFeignFallbackFactory;
import mf.code.comm.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.order.dto.OrderResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@FeignClient(name = "mf-code-order", fallbackFactory = OrderAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OrderAppService {

    /***
     * 购物订单-支付回调的处理
     *
     * @param dto
     * @return
     */
    @PostMapping("/feignapi/order/applet/updateOrderByPayCallback")
    SimpleResponse updateOrderByPayCallback(@RequestBody AlipayAppPayCallBackDTO dto);
}
