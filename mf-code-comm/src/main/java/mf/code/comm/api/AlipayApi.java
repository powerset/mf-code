package mf.code.comm.api;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.comm.common.caller.alipay.AlipayProperty;
import mf.code.comm.common.caller.alipay.AlipayService;
import mf.code.common.utils.SortUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * mf.code.comm.api
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月09日 14:55
 */
@RestController
@RequestMapping("/api/comm/alipay")
@Slf4j
public class AlipayApi {
    @Autowired
    private AlipayService alipayService;
    @Autowired
    private AlipayProperty alipayProperty;

    @PostMapping(value = "/apptradePay/callback")
    public String apptradePayCallback(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用
            if (SortUtil.isMessyCode(valueStr)) {
                try {
                    valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
                } catch (UnsupportedEncodingException e) {
                    log.error("<<<<<<<< 乱码异常：{}", e);
                }
            }
            params.put(name, valueStr);
        }
        //排序
        Map<String, String> sortParams = SortUtil.sortMapByKey(params);
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV1(sortParams, alipayProperty.getPublicKey(), alipayProperty.getCharset(), alipayProperty.getSignType());
        } catch (AlipayApiException e) {
            log.error("<<<<<<<<<app pay callback exception:{}", e);
        }
        if (!signVerified) {
            // 验证失败
            // 调试用，写文本函数记录程序运行情况是否正常
            String sWord = AlipaySignature.getSignCheckContentV1(params);
            log.info("------验证程序是否正常----------:{}", sWord);
            return "";
        }
        try {
            //买家支付宝账号
            String buyerLogonId = new String(request.getParameter("buyer_logon_id").getBytes("ISO-8859-1"), "UTF-8");
            //买家支付宝用户号
            String buyerId = new String(request.getParameter("buyer_id").getBytes("ISO-8859-1"), "UTF-8");
            //支付宝交易号
            String tradeNo = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");
            // 商户订单号
            String outTradeNo = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
            // 交易状态
            String tradeStatus = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");
            //公用回传参数
            String passbackParams = new String(request.getParameter("passback_params").getBytes("ISO-8859-1"), "UTF-8");
            //订单金额 本次交易支付的订单金额，单位为人民币（元）
            String totalAmount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");
            //交易付款时间 格式为yyyy-MM-dd HH:mm:ss
            String gmtPayment = new String(request.getParameter("gmt_payment").getBytes("ISO-8859-1"), "UTF-8");
            //实收金额 商家在交易中实际收到的款项，单位为元
            String receiptAmount = request.getParameter("receipt_amount") != null?
                    new String(request.getParameter("receipt_amount").getBytes("ISO-8859-1"), "UTF-8"):"";
            //付款金额 用户在交易中支付的金额
            String buyerPayAmount = request.getParameter("buyer_pay_amount") != null?
                    new String(request.getParameter("buyer_pay_amount").getBytes("ISO-8859-1"), "UTF-8"):"";
            //交易创建时间 格式为yyyy-MM-dd HH:mm:ss
            String gmtCreate = new String(request.getParameter("gmt_create").getBytes("ISO-8859-1"), "UTF-8");
            AlipayAppPayCallBackDTO dto = new AlipayAppPayCallBackDTO();
            dto.setBuyerId(buyerId);
            dto.setBuyerLogonId(buyerLogonId);
            dto.setTradeNo(tradeNo);
            dto.setOutTradeNo(outTradeNo);
            dto.setTradeStatus(tradeStatus);
            dto.setPassbackParams(passbackParams);
            dto.setTotalAmount(totalAmount);
            dto.setGmtPayment(gmtPayment);
            dto.setReceiptAmount(receiptAmount);
            dto.setBuyerPayAmount(buyerPayAmount);
            dto.setGmtCreate(gmtCreate);

            return alipayService.apptradePayCallback(dto);
        } catch (UnsupportedEncodingException e) {
            log.error("<<<<<<<< 解析回调数据异常：{}", e);
            return "";
        }
    }
}
