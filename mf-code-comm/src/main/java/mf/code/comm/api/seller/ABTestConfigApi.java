package mf.code.comm.api.seller;/**
 * create by qc on 2019/7/1 0001
 */

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.shop.dto.ABTestConfigDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用开关接口
 *
 * @author gbf
 * 2019/7/1 0001、10:50
 */
@RestController
@RequestMapping(value = "/api/comm/seller/config/v1")
@Slf4j
public class ABTestConfigApi {

    /**
     * 开关 打开
     */
    private static final String OPEN = "1";
    /**
     * 开关 关闭
     */
    private static final String OFF = "0";

    /**
     * 功能开关key
     */
    private static final String FEATURE_FLAG = "feature_flag";
    /**
     * ab测试key
     */
    private static final String ABTEST_FLAG = "abtest_flag";
    /**
     * 内存用户id 逗号隔开
     */
    private static final String ABTEST_UIDS = "abtest_uids";
    /**
     * 内测用户标识
     */
    private static final String IS_CLOSE_BETA_USER = "isCloseBetaTestUser";

    /**
     * 分隔符
     */
    @Value("${separator}")
    private String separator;
    /**
     * Apollo配置中心的命名空间
     */
    @Value("${namespace}")
    private String namespace;


    /**
     * 通用开关
     *
     * @param configDTO 商户id
     * @return SimpleResponse
     */
    @PostMapping(value = "switchConfig")
    public SimpleResponse switchConfig(@RequestBody ABTestConfigDTO configDTO) {
        log.info("测试:从配置中心拉配置校验:separator={}", separator);
        // 商户id
        String merchantId = configDTO.getMerchantId();
        if (StringUtils.isBlank(merchantId)) {
            return new SimpleResponse(1, "商户id不能为空");
        }
        // 功能标识字符串
        String featureStr = configDTO.getFeatureStr();
        if (StringUtils.isBlank(featureStr)) {
            return new SimpleResponse(2, "功能标识不能为空");
        }

        Map<String, Object> resultMap = new HashMap<>();
        // 获取apollo配置
        Config config = ConfigService.getConfig(namespace);
        String featureFlag = config.getProperty(featureStr + "." + FEATURE_FLAG, OFF);
        log.info("Apollo配置:{}:{}", featureStr + "." + FEATURE_FLAG, featureFlag);
        String abtestFlag = config.getProperty(featureStr + "." + ABTEST_FLAG, OFF);
        log.info("Apollo配置:{}:{}", featureStr + "." + ABTEST_FLAG, abtestFlag);
        // 配置是否开启
        boolean openFeature = OPEN.equals(featureFlag);
        boolean openAbtest = OPEN.equals(abtestFlag);

        resultMap.put(FEATURE_FLAG, openFeature ? OPEN : OFF);
        resultMap.put(ABTEST_FLAG, openAbtest ? OPEN : OFF);

        if (openAbtest && openAbtest) {
            String uids = config.getProperty(featureStr + "." + ABTEST_UIDS, "");
            log.info("Apollo配置:{}:{}", featureStr + "." + ABTEST_UIDS, uids);
            if (uids.contains(separator + merchantId + separator))
                resultMap.put(IS_CLOSE_BETA_USER, OPEN);
            else
                resultMap.put(IS_CLOSE_BETA_USER, OFF);
        } else {
            resultMap.put(IS_CLOSE_BETA_USER, OFF);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultMap);
    }
}
