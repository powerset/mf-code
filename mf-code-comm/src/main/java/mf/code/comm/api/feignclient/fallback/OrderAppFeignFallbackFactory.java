package mf.code.comm.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.comm.api.feignclient.OrderAppService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.stereotype.Component;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@Component
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public SimpleResponse updateOrderByPayCallback(AlipayAppPayCallBackDTO dto) {
                return null;
            }
        };
    }
}
