package mf.code.comm.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.comm.service.GenerateCodeService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.comm.api.applet
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月11日 09:49
 */
@RestController
@Slf4j
@RequestMapping(value = {"/api/comm/applet/v8/generateCode", "/api/comm/seller/v8/generateCode", "/api/comm/platform/v8/generateCode"})
public class GenerateCodeApi {
    @Autowired
    private GenerateCodeService generateCodeService;

    /***
     * 生成码
     * @param merchantId
     * @param shopId
     * @param userId
     * @param bizType GenerateCodeBizTypeEnum
     * @param bizValue 针对bizType对应的具体的业务值
     * @param scene GenerateCodeSceneEnum
     * @return
     */
    @GetMapping("/jkmf")
    public SimpleResponse generateCode(@RequestParam("merchantId") Long merchantId,
                                       @RequestParam("shopId") Long shopId,
                                       @RequestParam("bizType") Integer bizType,
                                       @RequestParam("bizValue") Long bizValue,
                                       @RequestParam("scene") Integer scene,
                                       @RequestParam("from") Integer fromId,
                                       @RequestParam("userId") Long userId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0 || userId == null || userId == 0
                || bizType == null || bizType == 0 || bizValue == null || bizValue == 0 || scene == null || scene == 0
                || fromId == null || fromId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        return generateCodeService.generateCode(merchantId, shopId, userId, bizType, bizValue, scene, fromId);
    }

    @GetMapping("/v2/jkmf")
    public SimpleResponse genereatoeCode(@RequestParam(name = "merchantId", defaultValue = "1") Long merchantId,
                                         @RequestParam(name = "shopId", defaultValue = "39") Long shopId,
                                         @RequestParam("bizType") Integer bizType,
                                         @RequestParam(name = "bizValue", required = false) String bizValue,
                                         @RequestParam(value = "userId", defaultValue = "0") Long userId,
                                         @RequestParam("path") String path) {
        return generateCodeService.generateCodeV2(merchantId, shopId, userId, bizType, bizValue, path);
    }

    @GetMapping("/v2/jkmf/query")
    public SimpleResponse queryCode(@RequestParam("key") String key) {
        return generateCodeService.queryCode(key);
    }
}
