package mf.code.comm.api.feignclient;

import mf.code.comm.api.feignclient.fallback.OneAppFeignFallbackFactory;
import mf.code.comm.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.CommonDictReqDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mf-code-one服务间调用feignClient
 *
 * @author gel
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {

    /***
     * 存储数据字典表
     *
     * @param commonDictReq
     * @return
     */
    @PostMapping("/feignapi/one/applet/v10/saveCommonDict")
    SimpleResponse createCommonDict(@RequestBody CommonDictReqDTO commonDictReq);

    /***
     * 查询字典表配置信息
     *
     * @param type
     * @param key
     * @return
     */
    @GetMapping("/feignapi/one/applet/v10/queryCommonDictByAppletCode")
    SimpleResponse queryCommonDict(@RequestParam("type") String type, @RequestParam("key") String key);
}
