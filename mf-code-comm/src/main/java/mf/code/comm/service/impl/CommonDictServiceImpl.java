package mf.code.comm.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import mf.code.comm.repo.dao.CommonDictMapper;
import mf.code.comm.repo.po.CommonDict;
import mf.code.comm.service.CommonDictService;
import mf.code.common.RedisKeyConstant;
import mf.code.common.utils.DateUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.comm.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 19:08
 */
@Service
@RequiredArgsConstructor
public class CommonDictServiceImpl implements CommonDictService {
    private final CommonDictMapper commonDictMapper;
    private final StringRedisTemplate stringRedisTemplate;

    private int timeout = 1;

    /**
     * 创建
     *
     * @param commonDict 入参
     * @return 创建条数
     */
    @Override
    public Integer create(CommonDict commonDict) {
        Date now = new Date();
        commonDict.setCtime(now);
        commonDict.setUtime(now);
        return commonDictMapper.insertSelective(commonDict);
    }

    /**
     * 批量创建
     *
     * @param commonDictList 入参
     * @return 创建条数
     */
    @Override
    public Integer createList(List<CommonDict> commonDictList) {
        if (CollectionUtils.isEmpty(commonDictList)) {
            return 0;
        }
        Date now = new Date();
        for (CommonDict commonDict : commonDictList) {
            commonDict.setCtime(now);
            commonDict.setUtime(now);
        }
        return commonDictMapper.insertList(commonDictList);
    }

    /**
     * 更新
     *
     * @param commonDict 入参
     * @return 更新条数
     */
    @Override
    public Integer update(CommonDict commonDict) {
        refreshRedisData(commonDict.getType(), commonDict.getKey());
        commonDict.setUtime(new Date());
        return commonDictMapper.updateByPrimaryKeySelective(commonDict);
    }

    /**
     * 删除
     *
     * @param id id
     * @return 删除条数
     */
    @Override
    public Integer delete(Long id) {
        CommonDict commonDict = commonDictMapper.selectByPrimaryKey(id);
        if (commonDict == null) {
            return 0;
        }
        refreshRedisData(commonDict.getType(), commonDict.getKey());
        return commonDictMapper.deleteByPrimaryKey(id);
    }

    /**
     * 主键查询
     *
     * @param id id
     * @return 数据
     */
    @Override
    public CommonDict select(Long id) {
        return commonDictMapper.selectByPrimaryKey(id);
    }

    /**
     * 按照类型键值查询
     *
     * @param type 类型
     * @param key 键值
     * @return 数据
     */
    @Override
    public CommonDict selectByTypeKey(String type, String key) {
        // 获取redis值
        String dataKey = RedisKeyConstant.COMMON_DICT + type + ":" + key;
        String redisData = stringRedisTemplate.opsForValue().get(dataKey);
        CommonDict commonDictObj = JSON.parseObject(redisData, CommonDict.class);
        if (commonDictObj != null) {
            return commonDictObj;
        }
        CommonDict commonDict = commonDictMapper.selectByTypeKey(type, key);
        if (commonDict == null) {
            return null;
        }
        // 若redis没有则查询并插入redis
        stringRedisTemplate.opsForValue().set(dataKey, JSON.toJSONString(commonDict), DateUtil.getNaturalDay2Second(), TimeUnit.SECONDS);
        return commonDict;
    }

    /**
     * 按照类型查询
     *
     * @param type 类型
     * @return 数据
     */
    @Override
    public List<CommonDict> selectListByType(String type) {
        Map<String, Object> params = new HashMap<>();
        params.put("type",type);
        return commonDictMapper.selectByParams(params);
    }

    /**
     * 查询列表
     *
     * @param params
     * @return
     */
    @Override
    public List<CommonDict> selectList(Map<String, Object> params) {
        return commonDictMapper.selectByParams(params);
    }

    /**
     * 根据type和key查询唯一字典数据
     *
     * @param type
     * @param key
     * @return
     */
    @Override
    public String selectValueByTypeKey(String type, String key) {
        // 获取redis值
        String dataKey = RedisKeyConstant.COMMON_DICT + type + ":" + key;
        String redisData = stringRedisTemplate.opsForValue().get(dataKey);
        CommonDict commonDictObj = JSON.parseObject(redisData, CommonDict.class);
        if (commonDictObj != null) {
            return commonDictObj.getValue();
        }
        // 若redis没有则查询并插入redis
        CommonDict commonDict = commonDictMapper.selectByTypeKey(type, key);
        if (commonDict == null) {
            return null;
        }
        stringRedisTemplate.opsForValue().set(dataKey, JSON.toJSONString(commonDict), timeout, TimeUnit.DAYS);
        return commonDict.getValue();
    }

    /**
     * 删除对应的redis缓存
     *
     * @param type
     * @param key
     * @return
     */
    @Override
    public Boolean refreshRedisData(String type, String key) {
        String dataKey = RedisKeyConstant.COMMON_DICT + type + ":" + key;
        return stringRedisTemplate.delete(dataKey);
    }
}
