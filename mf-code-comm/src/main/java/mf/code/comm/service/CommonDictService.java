package mf.code.comm.service;

import mf.code.comm.repo.po.CommonDict;

import java.util.List;
import java.util.Map;

/**
 * mf.code.comm.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 19:08
 */
public interface CommonDictService {
    /**
     * 创建
     *
     * @param commonDict
     * @return
     */
    Integer create(CommonDict commonDict);

    /**
     * 批量创建
     *
     * @param commonDictList
     * @return
     */
    Integer createList(List<CommonDict> commonDictList);

    /**
     * 更新
     *
     * @param commonDict
     * @return
     */
    Integer update(CommonDict commonDict);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    Integer delete(Long id);

    /**
     * 主键查询
     *
     * @param id
     * @return
     */
    CommonDict select(Long id);


    /**
     * 按照类型键值查询
     *
     * @return
     */
    CommonDict selectByTypeKey(String type, String key);

    /**
     * 按照类型查询
     *
     * @return
     */
    List<CommonDict> selectListByType(String type);

    /**
     * 查询列表
     *
     * @param params
     * @return
     */
    List<CommonDict> selectList(Map<String, Object> params);

    /**
     * 查询列表
     * @return
     */
    String selectValueByTypeKey(String type, String key);

    Boolean refreshRedisData(String type, String key);
}
