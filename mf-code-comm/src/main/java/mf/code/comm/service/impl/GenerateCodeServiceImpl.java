package mf.code.comm.service.impl;

import com.alibaba.fastjson.JSONObject;
import mf.code.comm.api.feignclient.OneAppService;
import mf.code.comm.common.caller.wxmp.WeixinMpService;
import mf.code.comm.common.caller.wxmp.WxpayProperty;
import mf.code.comm.constant.GenerateCodeBizTypeEnum;
import mf.code.comm.constant.GenerateCodeSceneEnum;
import mf.code.comm.service.GenerateCodeService;
import mf.code.common.WeixinMpConstants;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.CommonDictReqDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.comm.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月11日 11:39
 */
@Service
public class GenerateCodeServiceImpl implements GenerateCodeService {
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private OneAppService oneAppService;

    @Override
    public SimpleResponse generateCode(Long merchantId, Long shopId, Long userId, Integer bizType, Long bizValue, Integer scene, Integer fromId) {
        String path = WeixinMpConstants.WXCODESHOPPOSTERPATH + "/" + GenerateCodeSceneEnum.findByDesc(scene) + shopId + bizValue + "/";
        if (GenerateCodeBizTypeEnum.productScene(bizType)) {
            path = WeixinMpConstants.WXCODE_GOODS_PATH + GenerateCodeSceneEnum.findByDesc(scene) + shopId + bizValue + "/";
        }
        Object[] objects = new Object[]{merchantId, shopId, scene, bizValue, userId, fromId};
        String wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), this.getStrArray(objects, ","), path, 430, false);
        if (StringUtils.isBlank(wxaCodeUnlimit)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "生成小程序码失败");
        }
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put("appletCode", wxaCodeUnlimit);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    @Override
    public SimpleResponse generateCodeV2(Long merchantId, Long shopId, Long userId, Integer bizType, String bizValue, String path) {
        CommonDictReqDTO commonDictReqDTO = new CommonDictReqDTO();
        commonDictReqDTO.setType("appletCode");
        String key = GenerateCodeBizTypeEnum.findByName(bizType);
        if (StringUtils.isBlank(key)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "业务类型异常");
        }
        commonDictReqDTO.setKey(key);

        Map map = new HashMap();
        map.put("path", path);
        map.put("merchantId", merchantId);
        map.put("shopId", shopId);
        map.put("bizValue", "");
        if (StringUtils.isNotBlank(bizValue)) {
            map.put("bizValue", bizValue);
        }
        commonDictReqDTO.setValue(JSONObject.toJSONString(map));
        commonDictReqDTO.setValue1("");
        SimpleResponse commonDict = oneAppService.createCommonDict(commonDictReqDTO);
        if (commonDict.error()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "创建小程序码key异常");
        }

        Object[] objects = new Object[]{key};
        String wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), this.getStrArray(objects, ","), path + "/", 430, false);
        if (StringUtils.isBlank(wxaCodeUnlimit)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "生成小程序码失败");
        }
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put("appletCode", wxaCodeUnlimit);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    @Override
    public SimpleResponse queryCode(String key) {
        SimpleResponse simpleResponse = oneAppService.queryCommonDict("appletCode", key);
        return simpleResponse;
    }

    private String getStrArray(Object[] array, String separator) {
        if (array == null || array.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(array[0]);
        for (int i = 1; i < array.length; i++) {
            sb.append(separator).append(array[i]);
        }
        return sb.toString();
    }

}
