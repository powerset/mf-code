package mf.code.comm.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.common.caller.wxmp.WeixinMpService;
import mf.code.comm.common.caller.wxmp.WxmpProperty;
import mf.code.comm.common.caller.wxmp.WxpayProperty;
import mf.code.comm.service.TemplateMessageService;
import mf.code.user.dto.UserResp;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static mf.code.common.WeixinMpConstants.DOMAIN_WX_SENDTEMPLATEMESSAGE;

/**
 * mf.code.comm.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:31
 */
@Service
@Slf4j
public class TemplateMessageServiceImpl implements TemplateMessageService{
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WxmpProperty wxmpProperty;

    @Async
    @Override
    public void jkmfSendTemplateMessage(String templateId, UserResp user, Map<String, Object> data) {
        log.info("<<<<<<<<推送消息-入参：data：{}， template:{}, user:{}", data, templateId, user);
        if (StringUtils.isBlank(templateId) || user == null || CollectionUtils.isEmpty(data)) {
            return;
        }
        Map<String, Object> reqParam = new HashMap<>();
        reqParam.put("touser", user.getOpenId());
        reqParam.put("template_id", templateId);
        reqParam.put("form_id", queryRedisTeacherFormIds(user.getId()));
        reqParam.putAll(data);
        log.info("<<<<<<<<推送消息-开始：{}", reqParam);
        String accessToken = weixinMpService.getAccessToken(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret());
        if (StringUtils.isBlank(accessToken)) {
            log.error("accesstoken 为空：{}", accessToken);
            return;
        }
        String accessTokenParam = "access_token=" + accessToken;
        String url = DOMAIN_WX_SENDTEMPLATEMESSAGE + "?" + accessTokenParam;
        String voStrParams = JSONObject.toJSONString(reqParam);
        Map<String, Object> map = this.weixinMpService.sendMessage(url, voStrParams);
        log.info("<<<<<<<<推送消息-结束返回:{}", map);
    }

    private String queryRedisTeacherFormIds(Long tid) {
        //查询redis,参与人展现,直接倒叙排列
        BoundListOperations formIDS = this.stringRedisTemplate.boundListOps(wxmpProperty.getRedisKey(tid));
        List<Object> formIDStrs = formIDS.range(0, -1);
        if (formIDStrs == null || formIDStrs.size() == 0) {
            return null;
        }
        String formID = null;
        int index = 0;
        for (Object obj : formIDStrs) {
            JSONObject jsonObject = JSON.parseObject(obj.toString());
            //判断该formId是否为有效的 7天内有效
            long DayMillis = 24 * 60 * 60 * 1000;
            if ((System.currentTimeMillis() - jsonObject.getLong("ctime")) / DayMillis > 6) {
                //过期都删除
                this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(tid), index, obj);
                index++;
                continue;
            }
            formID = jsonObject.getString("formId");
            //用完即删
            this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(tid), index, obj);
            break;
        }
        return formID;
    }
}
