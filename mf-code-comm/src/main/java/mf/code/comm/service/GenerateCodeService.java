package mf.code.comm.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.comm.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月11日 11:39
 */
public interface GenerateCodeService {
    /***
     * 生成码
     * @param merchantId
     * @param shopId
     * @param userId
     * @param bizType
     * @param bizValue
     * @param scene
     * @return
     */
    SimpleResponse generateCode(Long merchantId, Long shopId, Long userId, Integer bizType, Long bizValue, Integer scene, Integer fromId);

    SimpleResponse generateCodeV2(Long merchantId, Long shopId, Long userId, Integer bizType, String bizValue, String path);

    SimpleResponse queryCode(String key);
}
