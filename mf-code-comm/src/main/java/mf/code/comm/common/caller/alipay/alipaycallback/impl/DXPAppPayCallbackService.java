package mf.code.comm.common.caller.alipay.alipaycallback.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.alipay.constant.AlipayPaySceneEnum;
import mf.code.comm.api.feignclient.OrderAppService;
import mf.code.comm.common.caller.alipay.alipaycallback.AbstractAlipayCallbackService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.comm.common.caller.alipay.alipaycallback.impl
 *
 * @description: 抖音-抖小铺
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月20日 13:59
 */
@Service
@Slf4j
public class DXPAppPayCallbackService extends AbstractAlipayCallbackService {
    @Autowired
    private OrderAppService orderAppService;

    @Override
    public AlipayPaySceneEnum getType() {
        return AlipayPaySceneEnum.DOU_SHOP;
    }

    @Override
    public String appPayCallbackBiz(AlipayAppPayCallBackDTO dto) {
        SimpleResponse simpleResponse = orderAppService.updateOrderByPayCallback(dto);
        log.info("<<<<<<<<抖小铺支付回调处理返回，simpleResponse：{}", JSONObject.toJSONString(simpleResponse));
        if (simpleResponse == null) {
            log.error("<<<<<<<<抖小铺支付回调处理异常，simpleResponse：{}", simpleResponse);
            return "fail";
        }
        if (simpleResponse == null || simpleResponse.error()) {
            log.error("<<<<<<<<抖小铺支付回调处理异常，simpleResponse：{}", JSONObject.toJSONString(simpleResponse));
            return "fail";
        }
        return "success";
    }
}
