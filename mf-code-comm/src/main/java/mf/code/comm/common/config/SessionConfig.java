package mf.code.comm.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 15:11
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 28800, redisNamespace = "mf:session")
public class SessionConfig {
}
