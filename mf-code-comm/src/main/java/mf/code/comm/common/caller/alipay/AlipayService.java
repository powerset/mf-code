package mf.code.comm.common.caller.alipay;

import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import mf.code.alipay.*;
import mf.code.comm.common.caller.alipay.req.RequestTradeAppPayDTO;
import mf.code.comm.common.caller.alipay.req.RequestTradeRefundDTO;

/**
 * mf.code.comm.common.caller.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 16:45
 */
public interface AlipayService {

    /***
     * alipay 支付查询入参拼接
     *
     * @param alipayQueryDTO
     * @return
     */
    AlipayTradeQueryRequest combineQueryParams(AlipayQueryDTO alipayQueryDTO);


    /***
     * alipay查询
     *
     * @param alipayTradeQueryRequest
     * @return
     */
    ResponseAlipayQueryDTO alipayQuery(AlipayTradeQueryRequest alipayTradeQueryRequest);
    /***
     * app支付拼接入参
     *
     * @param dto
     * @return
     */
    AlipayTradeAppPayRequest combineAppTradePayParams(RequestTradeAppPayDTO dto, String returnUrl);

    /***
     * app支付
     *
     * @param alipayTradeAppPayRequest
     * @return
     */
    ResponseTradeAppPayDTO appTradePay(AlipayTradeAppPayRequest alipayTradeAppPayRequest);

    /***
     * app支付回调
     * dto
     * <pre>
     *      * @param outTradeNo     商户订单号
     *      * @param tradeNo        支付宝交易号
     *      * @param tradeStatus    交易状态
     *      * @param totalAmount    订单金额 本次交易支付的订单金额，单位为人民币（元）
     *      * @param gmtPayment     交易付款时间 格式为yyyy-MM-dd HH:mm:ss
     *      * @param passbackParams 公用回传参数
     *      * @param buyerLogonId   买家支付宝账号
     *      * @param buyerId        买家支付宝用户号
     *      * @param receiptAmount  实收金额 商家在交易中实际收到的款项，单位为元
     *      * @param buyerPayAmount 付款金额 用户在交易中支付的金额
     *      * @param gmtCreate      交易创建时间 格式为yyyy-MM-dd HH:mm:ss
     * </pre>
     * @return
     */
    String apptradePayCallback(AlipayAppPayCallBackDTO dto);


    /***
     * alipay退款-拼接入参
     *
     * @param dto
     * @return
     */
    AlipayTradeRefundRequest combineTradeRefundParams(RequestTradeRefundDTO dto);

    /***
     * alipay退款-返回
     *
     * @param refundRequest
     * @return
     */
    ResponseRefundDTO refundResp(AlipayTradeRefundRequest refundRequest);
}
