package mf.code.comm.common.config.prometheus;

import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.hotspot.DefaultExports;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mf.code.user.common.config.prometheus
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-26 15:13
 */
@Configuration
public class PrometheusConfig {

	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		DefaultExports.initialize();
		return new ServletRegistrationBean(new MetricsServlet(), "/metrics");
	}
}
