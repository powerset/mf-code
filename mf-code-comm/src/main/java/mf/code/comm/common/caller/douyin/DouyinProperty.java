package mf.code.comm.common.caller.douyin;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * mf.code.comm.common.caller.douyin
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月09日 17:34
 */
@ConfigurationProperties(prefix = "douyinpay")
@Data
@Component
public class DouyinProperty {
    private String gatewayUrl;
    private String method;
    private String charset;
    private String signType;
    private String merchantVersionAppId;
    private String merchantVersionMerchantId;
    private String merchantVersionSecret;
    private String UserVersionAppId;
    private String UserVersionMerchantId;
    private String UserVersionSecret;
}
