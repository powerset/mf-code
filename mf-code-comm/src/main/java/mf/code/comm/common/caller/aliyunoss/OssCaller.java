package mf.code.comm.common.caller.aliyunoss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class OssCaller {

    @Value("${aliyunoss.bucketName}")
    private String bucketName;
    @Value("${aliyunoss.bucketUrlConsole}")
    private String bucketUrlConsole;
    @Value("${aliyunoss.bucketUrlService}")
    private String bucketUrlService;
    @Value("${aliyunoss.bucketUrlCatalog}")
    private String bucketCatalog;
    @Value("${aliyunoss.endpoint}")
    private String endpoint; // 阿里云OSS API的内或外网域名 http://oss-cn-hangzhou.aliyuncs.com
    @Value("${aliyunoss.accessKeyId}")
    private String accessKeyId; // 阿里云API的密钥Access Key ID LTAIG8XrluMHrJnn
    @Value("${aliyunoss.accessKeySecret}")
    private String accessKeySecret; // 阿里云API的密钥Access Key Secret iSy6CHqtJHgAENc6zxP7EG8sIhYe7C
    @Value("${aliyunoss.cdnUrl}")
    private String CDNURL;

    private static Map<String, String> fileExtensionContentTypeMap = new HashMap<>();
    static {
        fileExtensionContentTypeMap.put(".bmp", "image/bmp");
        fileExtensionContentTypeMap.put(".jpg", "image/jpg");
        fileExtensionContentTypeMap.put(".gif", "image/gif");
        fileExtensionContentTypeMap.put(".jpeg", "image/jpeg");
        fileExtensionContentTypeMap.put(".png", "image/png");
        fileExtensionContentTypeMap.put(".html", "text/html");
        fileExtensionContentTypeMap.put(".txt", "text/plain");
        fileExtensionContentTypeMap.put(".vsd", "application/vnd.visio");
        fileExtensionContentTypeMap.put(".ppt", "application/vnd.ms-powerpoint");
        fileExtensionContentTypeMap.put(".doc", "application/msword");
        fileExtensionContentTypeMap.put(".xml", "text/xml");
        fileExtensionContentTypeMap.put(".mp4", "video/mp4");
        fileExtensionContentTypeMap.put(".zip", "application/octet-stream");
    }

    /**
     *
     * 功能描述:
     *
     * @param: [client 客户端对象]
     * @param: [is 输入流]
     * @param: [diskName 文件存放的第一级目录名]
     * @param: [fileName 文件存放的后续连接 目录名]
     *
     * @return: java.lang.String 返回的是地址
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/19 0019 13:41
     */
    public String uploadObject2OSSInputstream(InputStream is, String objectName){
        String resultStr;
        String imgUrl = null;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(is.available());
            metadata.setCacheControl("no-cache");
            metadata.setHeader("Pragma", "no-cache");
            metadata.setContentEncoding("utf-8");
            metadata.setContentType(getContentType(objectName));
            //上传文件
            objectName = bucketCatalog + objectName;

            PutObjectResult putResult = ossClient.putObject(bucketName, objectName, is, metadata); //
            //解析结果
            resultStr = putResult.getETag();
            if(StringUtils.isNotBlank(resultStr)) {
                imgUrl = CDNURL + "/" + objectName;
            }
        } catch (Exception e) {
            log.error("上传阿里云OSS服务器异常.{}  e:{}" ,e.getMessage(), e);
        }finally {
            ossClient.shutdown();
        }
        return imgUrl;
    }

    //授权给第三方上传URL上传方式

    /**
     * 通过文件名判断并获取OSS服务文件上传时文件的contentType
     * @param fileName 文件名
     * @return: 文件的contentType
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/19 0019 14:17
     */
    private String getContentType(String fileName){
        String fileExtension = fileName.substring(fileName.lastIndexOf("."));
        String contentTypeStr = fileExtensionContentTypeMap.get(fileExtension);
        if (StringUtils.isBlank(contentTypeStr)) {
            log.warn("上传OSS的类型不存在.");
            return "text/html";
        }
        return contentTypeStr;
    }

    /**
     * 替换图片类型url，详细文档：https://help.aliyun.com/document_detail/44687.html?spm=a2c4g.11186623.6.1179.440fc1f6OgX3IE
     * http://bucket.<endpoint>/object?x-oss-process=image/action,parame_value
     * action：用户对图片做的操作。
     * parame：用户对图片做的操作所对应的参数
     * quality,Q_60 为图片压缩，但是只有图片为jpg才会生效，所以还是需要在后面添加format,jpg
     *
     * 图片处理支持的格式有：jpg、png、bmp、gif、webp、tiff
     *
     * @param ossUrl
     * @return
     */
    public String cdnReplace(String ossUrl) {
        if(ossUrl.contains(bucketUrlConsole)){
            ossUrl = ossUrl.replace(bucketUrlConsole,CDNURL);
        }
        return ossUrl;// + "?x-oss-process=image/quality,Q_60/format,jpg";
    }
}
