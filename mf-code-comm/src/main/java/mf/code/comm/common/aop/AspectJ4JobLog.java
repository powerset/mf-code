package mf.code.comm.common.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author gel
 */
@Component
@Aspect
@Slf4j
public class AspectJ4JobLog {

    @Value("${taskExecutor.jobMode}")
    private Integer jobMode = 0;

    @Around("execution(public * mf.code.*.job..*.*(..)))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        if(jobMode == null || jobMode != 1){
            return null;
        }
        long startMs = System.currentTimeMillis();
        Signature signature = proceedingJoinPoint.getSignature();
        String name = signature.getDeclaringTypeName() + "#" + signature.getName();
        log.info("JobLog-before, name: {}, no: {}", name, startMs);

        Object result = proceedingJoinPoint.proceed();
        long ms = System.currentTimeMillis() - startMs;
        log.info("JobLog--endend, name: {}, no: {}, time: {}", name, startMs, ms);
        return result;
    }
}

