package mf.code.comm.common.config.feign;

import feign.Contract;
import org.springframework.context.annotation.Bean;

/**
 * 注意：该类可以不写@Configuration注解；如果加了@Configuration注解，
 * 那么该类不能放在主应用程序上下文@ComponentScan所扫瞄的包中
 * mf.code.user.common.config.feign
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-18 16:50
 */
public class FeignConfiguration {
	/**
	 * 将契约改为feign原生的默认契约。这样就可以使用feign自带的注解了
	 * @return 默认的feign契约
	 */
	@Bean
	public Contract feignContract() {
		return new Contract.Default();
	}
}
