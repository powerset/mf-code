package mf.code.comm.common.caller.wxmp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="tmplatemessage")
@Data
@Component
public class WxmpProperty {
    private String formIdRedisKey;

    private String teacherFormIdRedisKey;


    /*************集客师模板*****************/
    //收益到账通知	温馨提示、收益来源、金额
    private String teacherIncomeReachMsgTmpId;
    //审核结果通知	温馨提示、审核内容、后续步骤
    private String teacherAuditingResultMsgTmpId;


    public String getRedisKey(Long uid){
        return this.formIdRedisKey.replace("A",uid.toString());
    }

    public String getTeacherRedisKey(Long tid){
        return this.teacherFormIdRedisKey.replace("A",tid.toString());
    }
}
