package mf.code.comm.common.caller.douyin;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.common.caller.alipay.AlipayProperty;
import mf.code.comm.common.caller.douyin.req.RequestTradeCreateDTO;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.HttpsUtil;
import mf.code.common.utils.MD5Util;
import mf.code.common.utils.SortUtil;
import mf.code.douyin.constant.DouyinPaySceneEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.util.*;

/**
 * mf.code.comm.common.caller.douyin
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 17:24
 */
@Service
@Slf4j
public class DouyinServiceImpl implements DouyinService {
    @Autowired
    private DouyinProperty douyinProperty;
    @Autowired
    private AlipayProperty alipayProperty;

    /***
     * 获取accessToken
     *
     * @param appid
     * @param appSecret
     * @return
     */
    @Override
    public String getAccessToken(String appid, String appSecret) {
        return null;
    }

    @Override
    public Map<String, Object> tradeCreate(RequestTradeCreateDTO requestTradeCreateDTO, String ip) {
        Map<String, Object> params = new LinkedHashMap<>();
        Map<String, Object> bizParams = new LinkedHashMap<>();

        Map extMap = JSONObject.parseObject(requestTradeCreateDTO.getExtParam(), Map.class);
        if (extMap == null) {
            return new LinkedHashMap<>();
        }
        if (extMap.get("payscene") == null || StringUtils.isBlank(extMap.get("payscene").toString())) {
            return new LinkedHashMap<>();
        }
        Integer payScene = NumberUtils.toInt(extMap.get("payscene").toString());
        String appId = douyinProperty.getMerchantVersionAppId();
        String merchantId = douyinProperty.getMerchantVersionMerchantId();
        String appSecret = douyinProperty.getMerchantVersionSecret();
        if (payScene == DouyinPaySceneEnum.DOU_SHOP.getCode()) {
            appId = douyinProperty.getUserVersionAppId();
            merchantId = douyinProperty.getUserVersionMerchantId();
            appSecret = douyinProperty.getUserVersionSecret();
        }

        Map<String, Object> riskInfoMap = new HashMap<>();
        riskInfoMap.put("ip", ip);

        bizParams.put("out_order_no", requestTradeCreateDTO.getOutOrderNo());
        bizParams.put("uid", requestTradeCreateDTO.getUid());
        bizParams.put("merchant_id", merchantId);
        bizParams.put("total_amount", requestTradeCreateDTO.getTotalAmount());
        bizParams.put("currency", "CNY");
        bizParams.put("subject", requestTradeCreateDTO.getSubject());
        bizParams.put("body", requestTradeCreateDTO.getBody());
        bizParams.put("trade_time", DateUtil.getCurrentSeconds() + "");
        //15分过期
        bizParams.put("valid_time", "600");
        bizParams.put("notify_url", alipayProperty.getNotifyUrl());
        bizParams.put("risk_info", JSONObject.toJSONString(riskInfoMap));

        params.put("app_id", appId);
        params.put("format", "JSON");
        params.put("charset", douyinProperty.getCharset());
        params.put("sign_type", douyinProperty.getSignType());
        params.put("timestamp", DateUtil.getCurrentSeconds() + "");
        params.put("version", "1.0");
        params.put("biz_content", JSONObject.toJSONString(bizParams));
        params.put("method", douyinProperty.getMethod());

        String sortSign = SortUtil.buildSignStr(params);
        String sign = sortSign + appSecret;
        log.info("<<<<<<<<sort sign:{}", sign);
        //注：MD5签名方式
        sign = MD5Util.md5(sign);
        params.put("sign", sign);//签名	sign

        Map<String, Object> map = null;
        try {
            map = sendDouyinGateway(douyinProperty.getGatewayUrl(), params);
        } catch (Exception e) {
            log.error("<<<<<<<<Exception:{}", e);
        }
        log.info("<<<<<<<<resp:{}", map);
        return map;
    }


    public Map<String, Object> sendDouyinGateway(String url, Map params) {
        String resp = "";
        try {
            resp = HttpsUtil.doPostForm(url, params, "application/x-www-form-urlencoded;charset=utf-8");
            log.info("send post form resp:{} params:{}", resp, params);
        } catch (Exception e) {
            log.error("Exception {}", e);
        }

        Map<String, Object> respMap = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(resp)) {
            JSONObject jsonObject = JSONObject.parseObject(resp);
            respMap = JSONObject.toJavaObject(jsonObject, Map.class);

            if (respMap.get("response") != null && StringUtils.isNotBlank(respMap.get("response").toString())) {
                JSONObject jsonObjectFinal = JSONObject.parseObject(respMap.get("response").toString());
                respMap = JSONObject.toJavaObject(jsonObjectFinal, Map.class);
            } else {
                log.error("<<<<<<<<抖音pay resp error:{}", resp);
            }
        }
        return respMap;
    }
}
