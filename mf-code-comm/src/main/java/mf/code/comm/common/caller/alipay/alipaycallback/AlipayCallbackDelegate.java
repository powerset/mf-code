package mf.code.comm.common.caller.alipay.alipaycallback;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.alipay.constant.AlipayPaySceneEnum;
import mf.code.common.exception.ArgumentException;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * mf.code.comm.common.caller.alipay.alipaycallback
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月20日 11:45
 */
@Component
@Slf4j
public class AlipayCallbackDelegate {
    private static Map<AlipayPaySceneEnum, AbstractAlipayCallbackService> registerService = new ConcurrentHashMap<>(128);

    @Autowired
    private List<AbstractAlipayCallbackService> abstractAlipayCallbackServices;

    @PostConstruct
    public void init() {
        for (AbstractAlipayCallbackService abstractAlipayCallbackService : abstractAlipayCallbackServices) {
            registerService.put(abstractAlipayCallbackService.getType(), abstractAlipayCallbackService);
        }
    }

    private AbstractAlipayCallbackService getAlipayCallbackService(int type) {
        AbstractAlipayCallbackService abstractAlipayCallbackService = registerService.get(AlipayPaySceneEnum.findByCode(type));
        if (abstractAlipayCallbackService == null) {
            throw new ArgumentException("因版本问题，暂未提供！");
        }
        return abstractAlipayCallbackService;
    }


    /**
     * 执行alipay-app-pay-callback
     *
     * @param dto
     * @return
     */
    public String appPayCallBackConverter(AlipayAppPayCallBackDTO dto) {
        //解析公有参数
        Map passbackParams = JSONObject.parseObject(dto.getPassbackParams(), Map.class);
        if (CollectionUtils.isEmpty(passbackParams) || passbackParams.get("payscene") == null) {
            log.error("<<<<<<<<解析异常 passbackParams:{}", passbackParams);
            return "fall";
        }
        int type = NumberUtils.toInt(passbackParams.get("payscene").toString());
        AbstractAlipayCallbackService converterService = getAlipayCallbackService(type);
        return converterService.appPayCallBack(dto);
    }
}
