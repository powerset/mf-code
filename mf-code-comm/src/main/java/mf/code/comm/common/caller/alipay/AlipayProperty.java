package mf.code.comm.common.caller.alipay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Data
@Component
public class AlipayProperty {
    private String appId;
    private String privateKey;
    private String publicKey;
    private String gatewayUrl;
    private String format;
    private String charset;
    private String signType;

    private String notifyUrl;
}
