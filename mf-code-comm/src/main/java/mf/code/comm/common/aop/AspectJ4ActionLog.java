package mf.code.comm.common.aop;

import com.alibaba.fastjson.JSON;
import io.prometheus.client.Histogram;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.common.config.prometheus.Metrics;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Aspect
@Slf4j
public class AspectJ4ActionLog {
    private static final int URL = 0;
    private static final int METHOD = 1;
    private static final int PARAMS = 2;

    private static final int CONTENT_TYPE_ERROR = 0;
    private static final int CONTENT_TYPE_JSON = 1;
    private static final int CONTENT_TYPE_XML = 2;
    private static final int CONTENT_TYPE_FROM_DATA = 3;

    @Around("execution(public * mf.code.*.api..*.*Api.*(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        String[] infos = getUrlMethodParam(proceedingJoinPoint);

        // 获取url、method、status
        String url = infos[URL];
        String method = infos[METHOD];

        // 增加 Http request 数量
        Metrics.gauge.labels(url, method).inc();

        long startMs = System.currentTimeMillis();
        log.info("apiLog-before, url={}, method={}, params={}", infos[URL], infos[METHOD], infos[PARAMS]);

        // 请求消耗时间--开始 默认的buckets范围为{.005, .01, .025, .05, .075, .1, .25, .5, .75, 1, 2.5, 5, 7.5, 10}
        Histogram.Timer timer = Metrics.histogram.labels(url, method).startTimer();
        // 业务处理
        Object result = proceedingJoinPoint.proceed();
        // 请求消耗时间--结束
        timer.observeDuration();
        // 记录http总请求数量
        Metrics.counter.labels(url, method).inc();

        long ms = System.currentTimeMillis() - startMs;
        log.info("apiLog-end, url={}, respone={},ms={}", infos[URL], JSON.toJSONString(result), ms);
        if (ms > 1000) {
            // 邮件报警
        }

        // 减少 Http response 数量， 差值则为 服务器正在处理的数量
        Metrics.gauge.labels(url, method).dec();
        return result;
    }

    private String[] getUrlMethodParam(ProceedingJoinPoint proceedingJoinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String url = request.getRequestURL().toString();
        String method = request.getMethod();
        String paramJson;
        if ("GET".equalsIgnoreCase(method)) {
            Map<String, String[]> parameterMap = request.getParameterMap();
            paramJson = JSON.toJSONString(parameterMap);
        } else {
            paramJson = getPostJson(request, proceedingJoinPoint);
        }
        String[] infos = {url, method, paramJson};
        return infos;
    }

    /**
     * 解析post请求--返回json字符串
     *
     * @param request
     * @param proceedingJoinPoint
     * @return
     */
    private String getPostJson(HttpServletRequest request, ProceedingJoinPoint proceedingJoinPoint) {
        int contentType = judgeContentType(request);

        if (contentType == CONTENT_TYPE_XML) {
            return "{\"msg\":\"不解析微信回调参数\"}";//ParseWxUtils.parse(request);//解析微信的参数
        } else if (contentType == CONTENT_TYPE_JSON) {
            return JSON.toJSONString(handleObjectArray(proceedingJoinPoint));
        } else if (contentType == CONTENT_TYPE_FROM_DATA) {
            Map<String, String[]> parameterMap = request.getParameterMap();
            return JSON.toJSONString(parameterMap);
        } else {
            return "parse post body error";
        }
    }

    /**
     * post请求解析--处理数组
     *
     * @param proceedingJoinPoint
     * @return
     */
    private String handleObjectArray(ProceedingJoinPoint proceedingJoinPoint) {
        Object[] os = proceedingJoinPoint.getArgs();
        List args = new ArrayList();
        for (int i = 0; i < os.length; i++) {
            if (os[i] instanceof HttpServletRequest || os[i] instanceof HttpServletResponse) {
                continue;
            }
            args.add(os[i]);
        }
        return JSON.toJSONString(args);
    }

    /**
     * 判断Content Type
     *
     * @param request
     * @return
     */
    private int judgeContentType(HttpServletRequest request) {
        String contentType = request.getContentType();
        contentType = contentType == null ? "" : contentType;
        if (contentType.toLowerCase().contains("xml")) {
            return CONTENT_TYPE_XML;
        } else if (contentType.toLowerCase().contains("json")) {
            return CONTENT_TYPE_JSON;
        } else if (contentType.toLowerCase().contains("form-data")) {
            return CONTENT_TYPE_FROM_DATA;
        }
        return CONTENT_TYPE_ERROR;
    }
}

