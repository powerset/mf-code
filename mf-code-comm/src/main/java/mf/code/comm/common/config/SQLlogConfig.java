package mf.code.comm.common.config;

import mf.code.comm.common.aop.SQLInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * mf.code.shop.common.config
 * Description:
 *
 * @author gel
 * @date 2019-04-10 14:22
 */
@Configuration
public class SQLlogConfig {

    @Bean
    public SQLInterceptor sqlStatsInterceptor() {
        SQLInterceptor sqlStatsInterceptor = new SQLInterceptor();
        Properties properties = new Properties();
        properties.setProperty("dialect", "mysql");
        sqlStatsInterceptor.setProperties(properties);
        return sqlStatsInterceptor;
    }
}
