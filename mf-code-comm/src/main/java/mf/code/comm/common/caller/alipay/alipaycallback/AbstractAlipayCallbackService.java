package mf.code.comm.common.caller.alipay.alipaycallback;

import mf.code.alipay.AlipayAppPayCallBackDTO;
import mf.code.alipay.constant.AlipayPaySceneEnum;
import mf.code.douyin.constant.DouyinPaySceneEnum;
import org.springframework.stereotype.Component;

/**
 * mf.code.comm.common.caller.alipay.alipaycallback
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月20日 11:51
 */
@Component
public abstract class AbstractAlipayCallbackService {

    /**
     * alipay-app-pay-callback
     *
     * @param dto
     * @return
     */
    protected String appPayCallbackBiz(AlipayAppPayCallBackDTO dto) {
        return "success";
    }

    /**
     * 骨架方法 -- 执行alipay-app-pay-callback
     *
     * @param dto
     * @return
     */
    final String appPayCallBack(AlipayAppPayCallBackDTO dto) {
        return appPayCallbackBiz(dto);
    }


    public abstract AlipayPaySceneEnum getType();
}
