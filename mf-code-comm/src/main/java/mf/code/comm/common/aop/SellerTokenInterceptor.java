package mf.code.comm.common.aop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * mf.code.common.aop
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 15:28
 */
@Slf4j
public class SellerTokenInterceptor extends HandlerInterceptorAdapter {

}
