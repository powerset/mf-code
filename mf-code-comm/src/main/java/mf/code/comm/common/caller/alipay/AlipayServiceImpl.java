package mf.code.comm.common.caller.alipay;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.*;
import mf.code.comm.common.caller.alipay.alipaycallback.AlipayCallbackDelegate;
import mf.code.comm.common.caller.alipay.req.RequestTradeAppPayDTO;
import mf.code.comm.common.caller.alipay.req.RequestTradeRefundDTO;
import mf.code.comm.common.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * mf.code.comm.common.caller.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 16:45
 */
@Service
@Slf4j
public class AlipayServiceImpl implements AlipayService {
    @Autowired
    private AlipayProperty alipayProperty;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public AlipayTradeQueryRequest combineQueryParams(AlipayQueryDTO alipayQueryDTO) {
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent(JSONObject.toJSONString(alipayQueryDTO));
        return request;
    }

    @Override
    public ResponseAlipayQueryDTO alipayQuery(AlipayTradeQueryRequest alipayTradeQueryRequest) {
        AlipayClient alipayClient = new DefaultAlipayClient(alipayProperty.getGatewayUrl(), alipayProperty.getAppId(),
                alipayProperty.getPrivateKey(), alipayProperty.getFormat(), alipayProperty.getCharset(),
                alipayProperty.getPublicKey(), alipayProperty.getSignType());

        log.info("<<<<<<<<alipay req:{}", alipayTradeQueryRequest);
        ResponseAlipayQueryDTO resp = null;
        try {
            AlipayTradeQueryResponse response = alipayClient.execute(alipayTradeQueryRequest);
            if(response != null){
                resp = new ResponseAlipayQueryDTO();
                BeanUtils.copyProperties(response, resp);
            }
            if(!response.isSuccess()){
                System.out.println("调用失败");
            }
        } catch (AlipayApiException e) {
            log.error("<<<<<<<<<app pay exception:{}", e);
        }
        log.info("<<<<<<<<alipay resp:{}", resp);
        return resp;
    }

    /***
     * app支付拼接入参
     *
     * @param dto
     * @return
     */
    @Override
    public AlipayTradeAppPayRequest combineAppTradePayParams(RequestTradeAppPayDTO dto, String returnUrl) {
        AlipayTradeAppPayRequest alipayTradeAppPayRequest = new AlipayTradeAppPayRequest();
        if (StringUtils.isNotBlank(returnUrl)) {
            alipayTradeAppPayRequest.setReturnUrl(returnUrl);
        }
        alipayTradeAppPayRequest.setNotifyUrl(alipayProperty.getNotifyUrl());
        alipayTradeAppPayRequest.setBizContent(JSONObject.toJSONString(dto));
        return alipayTradeAppPayRequest;
    }

    /***
     * app支付
     *
     * @param alipayTradeAppPayRequest
     * @return
     */
    @Override
    public ResponseTradeAppPayDTO appTradePay(AlipayTradeAppPayRequest alipayTradeAppPayRequest) {
        AlipayClient alipayClient = new DefaultAlipayClient(alipayProperty.getGatewayUrl(), alipayProperty.getAppId(),
                alipayProperty.getPrivateKey(), alipayProperty.getFormat(), alipayProperty.getCharset(),
                alipayProperty.getPublicKey(), alipayProperty.getSignType());

        log.info("<<<<<<<<alipay req:{}", alipayTradeAppPayRequest);
        ResponseTradeAppPayDTO resp = null;
        try {
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(alipayTradeAppPayRequest);
            if (response != null) {
                resp = new ResponseTradeAppPayDTO();
                BeanUtils.copyProperties(response, resp);
            }
            if (!response.isSuccess()) {
                //TODO:异常存储log
            }
        } catch (AlipayApiException e) {
            log.error("<<<<<<<<<app pay exception:{}", e);
        }
        log.info("<<<<<<<<alipay resp:{}", resp);
        return resp;
    }

    /***
     * app支付回调
     * dto
     * <pre>
     *      * @param outTradeNo     商户订单号
     *      * @param tradeNo        支付宝交易号
     *      * @param tradeStatus    交易状态
     *      * @param totalAmount    订单金额 本次交易支付的订单金额，单位为人民币（元）
     *      * @param gmtPayment     交易付款时间 格式为yyyy-MM-dd HH:mm:ss
     *      * @param passbackParams 公用回传参数
     *      * @param buyerLogonId   买家支付宝账号
     *      * @param buyerId        买家支付宝用户号
     *      * @param receiptAmount  实收金额 商家在交易中实际收到的款项，单位为元
     *      * @param buyerPayAmount 付款金额 用户在交易中支付的金额
     *      * @param gmtCreate      交易创建时间 格式为yyyy-MM-dd HH:mm:ss
     * </pre>
     * @return
     *  <ps>
     *      程序执行完后必须打印输出“success”（不包含引号）。如果商户反馈给支付宝的字符不是 success 这7个字符，支付宝服务器会不断重发通知，
     *      直到超过24小时22分钟。一般情况下，25小时以内完成8次通知（通知的间隔频率一般是：4m,10m,10m,1h,2h,6h,15h）；
     *  </ps>
     */
    @Override
    public String apptradePayCallback(AlipayAppPayCallBackDTO dto) {
        String redisForbidKey = RedisKeyConstant.GOODORDER_FORBID_KEY + dto.getOutTradeNo();
        Boolean aBoolean = stringRedisTemplate.opsForValue().setIfAbsent(redisForbidKey, "");
        if (aBoolean != null && !aBoolean) {
            return "微信支付回调失败!回调正在触发，请勿重复";
        }

        try {
            dto.setPassbackParams(URLDecoder.decode(dto.getPassbackParams(), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("<<<<<<<<<passbackParams url 转义异常:{}", e);
        }
        log.info("<<<<<<<<欢迎进入支付宝回调方法体 notify resp:{}", JSONObject.toJSONString(dto));
        String notify = "";
        if (dto.getTradeStatus().equals(AlipayConstants.TRADE_FINISHED)) {
            /**商户签约的产品支持退款功能的前提下，买家付款成功**/
            // 判断该笔订单是否在商户网站中已经做过处理
            // 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
            // 如果有做过处理，不执行商户的业务程序
            // 注意：
            // 退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            // 更新订单表
            //夜辰 支付回调后的业务处理
            AlipayCallbackDelegate alipayCallbackDelegate = new AlipayCallbackDelegate();
            notify = alipayCallbackDelegate.appPayCallBackConverter(dto);
        } else if (dto.getTradeStatus().equals(AlipayConstants.TRADE_SUCCESS)) {
            /**商户签约的产品不支持退款功能的前提下，买家付款成功；
             * 或者，商户签约的产品支持退款功能的前提下，交易已经成功并且已经超过可退款期限**/
            // 判断该笔订单是否在商户网站中已经做过处理
            // 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
            // 如果有做过处理，不执行商户的业务程序
            // 注意：
            // 付款完成后，支付宝系统发送该交易状态通知
            //夜辰 支付回调后的业务处理
            AlipayCallbackDelegate alipayCallbackDelegate = new AlipayCallbackDelegate();
            notify = alipayCallbackDelegate.appPayCallBackConverter(dto);
        } else if (dto.getTradeStatus().equals(AlipayConstants.TRADE_CLOSED)) {
            //交易关闭
            //夜辰 支付回调后的业务处理
            AlipayCallbackDelegate alipayCallbackDelegate = new AlipayCallbackDelegate();
            notify = alipayCallbackDelegate.appPayCallBackConverter(dto);
        }
        return notify;
    }

    /***
     * alipay退款-拼接入参
     *
     * @param dto
     * @return
     */
    @Override
    public AlipayTradeRefundRequest combineTradeRefundParams(RequestTradeRefundDTO dto) {
        AlipayTradeRefundRequest refundRequest = new AlipayTradeRefundRequest();
        refundRequest.setBizContent(JSONObject.toJSONString(dto));
        return refundRequest;
    }

    /***
     * alipay退款-返回
     *
     * @param refundRequest
     * @return
     */
    @Override
    public ResponseRefundDTO refundResp(AlipayTradeRefundRequest refundRequest) {
        AlipayClient alipayClient = new DefaultAlipayClient(alipayProperty.getGatewayUrl(), alipayProperty.getAppId(),
                alipayProperty.getPrivateKey(), alipayProperty.getFormat(), alipayProperty.getCharset(),
                alipayProperty.getPublicKey(), alipayProperty.getSignType());

        ResponseRefundDTO resp = null;
        try {
            AlipayTradeRefundResponse response = alipayClient.execute(refundRequest);
            if (response != null) {
                resp = new ResponseRefundDTO();
                BeanUtils.copyProperties(response, resp);
            }
            if (!response.isSuccess()) {
                //TODO:异常存储log
            }
        } catch (AlipayApiException e) {
            log.error("<<<<<<<<<app pay exception:{}", e);
        }
        return resp;
    }
}
