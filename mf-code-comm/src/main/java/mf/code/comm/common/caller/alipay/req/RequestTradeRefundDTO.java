package mf.code.comm.common.caller.alipay.req;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 16:47
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestTradeRefundDTO {
    /***
     * 订单支付时传入的商户订单号,不能和 trade_no同时为空
     * 最大长度：64
     * <<可选>>
     */
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    /***
     * 支付宝交易号，和商户订单号不能同时为空
     * 最大长度：64
     * <<可选>>
     */
    @JSONField(name = "trade_no")
    private String tradeNo;
    /***
     * 需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数
     * <必选>
     */
    @JSONField(name = "refund_amount")
    private String refundAmount;
    /***
     * 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。
     * <可选>
     */
    @JSONField(name = "out_request_no")
    private String outRequestNo;
    /***
     * 退款的原因说明
     * <可选>
     */
    @JSONField(name = "refund_reason")
    private String refundReason;
}
