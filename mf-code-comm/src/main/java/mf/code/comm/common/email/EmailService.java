package mf.code.comm.common.email;

/**
 * mf.code.common.log.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-07 下午2:54
 */
public interface EmailService {
	void sendSimpleMail(String subject, String content);
	void sendHtmlMail(String subject, String content);
	void sendAttachmentsMail(String subject, String content, String filePath);
	void sendInlineResourceMail(String subject, String content, String rscPath, String rscId);
}
