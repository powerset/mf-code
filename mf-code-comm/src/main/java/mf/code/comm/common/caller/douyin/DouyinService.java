package mf.code.comm.common.caller.douyin;

import mf.code.comm.common.caller.douyin.req.RequestTradeCreateDTO;

import java.util.Map;

/**
 * mf.code.comm.common.caller.douyin
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 17:24
 */
public interface DouyinService {
    /***
     * 获取accessToken
     *
     * @param appid
     * @param appSecret
     * @return
     */
    String getAccessToken(String appid, String appSecret);

    /***
     * 生成预支付订单
     *
     * @param requestTradeCreateDTO
     * @param ip
     * @return
     */
    Map<String, Object> tradeCreate(RequestTradeCreateDTO requestTradeCreateDTO, String ip);
}
