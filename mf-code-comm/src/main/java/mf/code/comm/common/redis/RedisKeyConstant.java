package mf.code.comm.common.redis;

/**
 * mf.code.user.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-02 00:13
 */
public class RedisKeyConstant {
	/***
	 * 推文分享人信息
	 * key: jkmf:extension:sharepersons:list:<推文编号>
	 * value:
	 */
	public static final String EXTENSION_SHARE_PERSONS = "jkmf:comm:weekextension:sharepersons:zset:";


	/***
	 * 订单回调去重
	 */
	public static final String GOODORDER_FORBID_KEY = "jkmf:order:callback:forbid:";
}
