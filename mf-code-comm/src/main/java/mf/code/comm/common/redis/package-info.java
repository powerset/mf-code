/**
 * <pre>
 * 远程调用外部服务接口
 * aliyundayu 阿里云短信服务
 * aliyunoss 阿里云OSS服务
 * wxmp 微信公众平台
 * wxpay.mchpay 微信支付-企业付款
 * wxpay.scanpay 微信支付-扫码支付
 * wxpay.wxapay 微信支付-小程序拉起收银台
 * xxbtop 喜销宝
 * </pre>
 */
package mf.code.comm.common.redis;
