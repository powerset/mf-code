package mf.code.comm.common.config;

import mf.code.comm.common.filter.AccessFilter;
import mf.code.comm.common.filter.AppletTokenFilter;
import mf.code.comm.common.filter.PlatformTokenFilter;
import mf.code.comm.common.filter.SellerTokenFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-11-10 15:36
 */
@Configuration
public class FilterConfig {

    @Value("${seller.uid.name}")
    private String sellerUidName;
    @Value("${seller.token.debug.mode}")
    private String debugMode;
    @Value("${platform.uid.name}")
    private String platformUidName;
    @Value("${platform.token.debug.mode}")
    private String platformDebugMode;

    /**
     * 注册跨域filter
     */
    @Bean
    public FilterRegistrationBean accessFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        //注入过滤器
        registrationBean.setFilter(new AccessFilter());
        //拦截规则
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        //过滤器名称
        registrationBean.setName("accessFilter");
        //过滤器顺序
        registrationBean.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
        return registrationBean;
    }

    /**
     * 注册sellerTokenFilter
     */
    @Bean
    public FilterRegistrationBean sellerTokenFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        SellerTokenFilter sellerTokenFilter = new SellerTokenFilter();
        registrationBean.setFilter(sellerTokenFilter);
        registrationBean.addInitParameter("uidName", sellerUidName);
        registrationBean.addInitParameter("debugMode", debugMode);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/api/goods/seller/*");
        urlPatterns.add("/api/comm/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.setName("sellerTokenFilter");
        return registrationBean;
    }

    /**
     * 注册appletTokenFilter
     */
    @Bean
    public FilterRegistrationBean appletTokenFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        AppletTokenFilter appletTokenFilter = new AppletTokenFilter();
        registrationBean.setFilter(appletTokenFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/api/*/applet/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.setName("appletTokenFilter");
        return registrationBean;
    }

    /**
     * 注册platformTokenFilter
     */
    @Bean
    public FilterRegistrationBean platformTokenFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PlatformTokenFilter platformTokenFilter = new PlatformTokenFilter();
        registrationBean.setFilter(platformTokenFilter);
        registrationBean.addInitParameter("uidName", platformUidName);
        registrationBean.addInitParameter("debugMode", platformDebugMode);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/api/*/platform/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.setName("platformTokenFilter");
        return registrationBean;
    }

}
