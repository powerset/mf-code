package mf.code.comm.common.redis;

import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * redis防重工具
 */
public class RedisForbidRepeat {

    public static boolean NX(StringRedisTemplate redisTemplate, String redisKey) {
        return RedisForbidRepeat.NX(redisTemplate, redisKey, 3, TimeUnit.SECONDS);
    }

    public static boolean NX(StringRedisTemplate redisTemplate, String redisKey, int timeout, TimeUnit unit) {
        if (redisTemplate.opsForValue().setIfAbsent(redisKey, "1")) {
            redisTemplate.expire(redisKey, timeout, unit);
            return true;
        }
        return false;
    }

    public static boolean delKey(StringRedisTemplate redisTemplate, String redisKey) {
        return redisTemplate.delete(redisKey);
    }
}
