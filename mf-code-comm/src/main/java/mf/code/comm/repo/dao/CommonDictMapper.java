package mf.code.comm.repo.dao;

import mf.code.comm.repo.po.CommonDict;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CommonDictMapper {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(CommonDict record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(CommonDict record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    CommonDict selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(CommonDict record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(CommonDict record);

    /**
     * 批量更新
     *
     * @param commonDictList
     * @return
     */
    int insertList(@Param(value = "commonDictList") List<CommonDict> commonDictList);

    /**
     * 根据类型主键查询
     *
     * @param type
     * @param key
     * @return
     */
    CommonDict selectByTypeKey(@Param(value = "type") String type, @Param(value = "key") String key);

    /**
     * 动态查询
     *
     * @param params
     * @return
     */
    List<CommonDict> selectByParams(Map<String, Object> params);
}
