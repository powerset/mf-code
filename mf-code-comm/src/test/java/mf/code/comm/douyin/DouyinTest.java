package mf.code.comm.douyin;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.comm.common.caller.douyin.DouyinService;
import mf.code.comm.common.caller.douyin.req.RequestTradeCreateDTO;
import mf.code.common.utils.RandomStrUtil;
import mf.code.douyin.DouyinPayReq;
import mf.code.douyin.constant.DouyinPaySceneEnum;
import mf.code.user.constant.UserRoleEnum;
import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.comm.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月09日 10:04
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DouyinTest {
    @Autowired
    private DouyinService douyinService;

    @Test
    public void douyinPayTest() {
        DouyinPayReq douyinPayReq = new DouyinPayReq();
        douyinPayReq.setIp("115.195.61.0");

        String outTradeNo = DateFormatUtils.format(new Date(), "yyMMddHHmmss") + 1 + "999" + RandomStrUtil.randomNumberStr(6);
        douyinPayReq.setOutOrderNo(outTradeNo);
        douyinPayReq.setBody("交易订单");
        douyinPayReq.setSubject("交易订单");
        douyinPayReq.setTotalAmount(new BigDecimal("0.1").multiply(new BigDecimal(100)).longValue());
        douyinPayReq.setUid("01Xekkx5.guHOR.R");
        Map param = new HashMap<>();
        param.put("payscene", DouyinPaySceneEnum.DOU_SHOP.getCode());
        douyinPayReq.setExtParam(JSONObject.toJSONString(param));
        RequestTradeCreateDTO requestTradeCreateDTO = new RequestTradeCreateDTO();
        BeanUtils.copyProperties(douyinPayReq, requestTradeCreateDTO);
        Map<String, Object> map = douyinService.tradeCreate(requestTradeCreateDTO, douyinPayReq.getIp());
        System.out.println(JSONObject.toJSONString(map));
    }
}
