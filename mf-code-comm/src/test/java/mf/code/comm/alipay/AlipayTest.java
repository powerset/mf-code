package mf.code.comm.alipay;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import lombok.extern.slf4j.Slf4j;
import mf.code.alipay.*;
import mf.code.comm.common.caller.alipay.req.RequestTradeAppPayDTO;
import mf.code.comm.common.caller.alipay.AlipayService;
import mf.code.comm.common.caller.alipay.req.RequestTradeRefundDTO;
import mf.code.common.utils.RandomStrUtil;
import mf.code.douyin.constant.DouyinPaySceneEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.comm.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月09日 10:04
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AlipayTest {
    @Autowired
    private AlipayService alipayService;

    @Test
    public void appAlipayTest() {
        AppPayReq req = new AppPayReq();
        String outTradeNo = DateFormatUtils.format(new Date(), "yyMMddHHmmss") + 1 + "999" + RandomStrUtil.randomNumberStr(6);
        req.setOutTradeNo(outTradeNo);
        req.setTotalAmount("0.01");
        req.setSubject("测试");
        req.setGoodsType("1");
        req.setTimeoutExpress("90m");
        req.setReturnUrl("https://www.baidu.com");
        try {
            Map param = new HashMap<>();
            param.put("payscene", DouyinPaySceneEnum.DOU_SHOP.getCode());
            req.setPassbackParams(URLEncoder.encode(JSONObject.toJSONString(param), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("<<<<<<<< urlEncode exception:{}", e);
        }
        RequestTradeAppPayDTO requestTradeAppPayDTO = new RequestTradeAppPayDTO();
        BeanUtils.copyProperties(req, requestTradeAppPayDTO);
        AlipayTradeAppPayRequest alipayTradeAppPayRequest = alipayService.combineAppTradePayParams(requestTradeAppPayDTO, req.getReturnUrl());
        ResponseTradeAppPayDTO responseTradeAppPayDTO = alipayService.appTradePay(alipayTradeAppPayRequest);
        System.out.println(JSONObject.toJSONString(responseTradeAppPayDTO));
    }


    @Test
    public void refundTest() {
        RequestRefundReq requestRefundReq = new RequestRefundReq();
        requestRefundReq.setOutTradeNo("1908201742061999899287");
        requestRefundReq.setRefundAmount("0.01");
        String outTradeNo = DateFormatUtils.format(new Date(), "yyMMddHHmmss") + 1 + "999" + RandomStrUtil.randomNumberStr(6);
        requestRefundReq.setOutRequestNo("1908201742061999899287");
        RequestTradeRefundDTO req = new RequestTradeRefundDTO();
        BeanUtils.copyProperties(requestRefundReq, req);
        AlipayTradeRefundRequest refundRequest = alipayService.combineTradeRefundParams(req);
        System.out.println(JSONObject.toJSONString(refundRequest));
        ResponseRefundDTO responseRefundDTO = alipayService.refundResp(refundRequest);
        if(!StringUtils.isEmpty(responseRefundDTO.getSubCode())){
            System.out.println("错误欢迎入驻：^_^");
        }else {
            System.out.println("正确欢迎来临：^_^");
        }
        System.out.println(JSONObject.toJSONString(responseRefundDTO));
    }


    @Test
    public void queryAlipayTest(){
        AlipayQueryDTO dto = new AlipayQueryDTO();
        dto.setOutTradeNo("1908201742061999899287");
        AlipayTradeQueryRequest request = alipayService.combineQueryParams(dto);
        ResponseAlipayQueryDTO resp = alipayService.alipayQuery(request);
        //<<<<<<<<{"tradeNo":"2019082022001453260518944703","tradeStatus":"TRADE_CLOSED"}
        log.info("<<<<<<<<{}",JSONObject.toJSONString(resp));
    }
}
