package mf.base.eureka.common.config;//package mf.base.eureka.common.config;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import mf.base.filter.TokenValidateGatewayFilterFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * mf.base.eureka.common.config
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年03月26日 11:13
// */
//@Configuration
//public class TokenValidateGatewayFilterFactoryConfig {
//
//    @Bean
//    public TokenValidateGatewayFilterFactory tokenValidateGatewayFilterFactory(ObjectMapper objectMapper){
//        return new TokenValidateGatewayFilterFactory(objectMapper);
//    }
//}
