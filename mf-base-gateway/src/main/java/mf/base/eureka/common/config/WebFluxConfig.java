package mf.base.eureka.common.config;//package mf.base.eureka.common.config;
//
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.reactive.DispatcherHandler;
//import org.springframework.web.reactive.config.EnableWebFlux;
//import org.springframework.web.reactive.config.WebFluxConfigurer;
//import org.springframework.web.server.WebHandler;
//
///**
// * @Auther: yechen
// * @Email: wangqingfeng@wxyundian.com
// * @Date: 2018/10/7 0007 19:07
// * @Description:
// */
//
//@Configuration
//@EnableWebFlux
//public class WebFluxConfig implements WebFluxConfigurer {
//
//    @Bean
//    public WebHandler webHandler(ApplicationContext applicationContext) {
//        DispatcherHandler dispatcherHandler = new DispatcherHandler(applicationContext);
//        return dispatcherHandler;
//    }
//}
