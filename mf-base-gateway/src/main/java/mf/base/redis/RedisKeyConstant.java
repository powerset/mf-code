package mf.base.redis;

/**
 * redis键
 *
 * @author gel
 */
public class RedisKeyConstant {

    /***
     * 集客魔方小程序的formid存储rediskey
     */
    public static final String JKMF_SHOPPINGMALL_TEMPLATEMSGKEY = "templateMessage:formId:userId:";//userId
}
