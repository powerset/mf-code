package mf.base.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @Date: 2018/10/7 0007 19:08
 * @Description:
 */
@RestController
@RequestMapping("/mfgateway")
public class HelloController {

    @RequestMapping(path = "/hello",method = RequestMethod.GET)
    public String helloworld() {
        return "helloworld";
    }
}
