//package mf.base.filter;
//
//import com.alibaba.fastjson.JSONObject;
//import io.micrometer.core.instrument.MeterRegistry;
//import io.netty.buffer.ByteBufAllocator;
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//import mf.base.filter.fetcher.GatewayContextFilterFetcher;
//import mf.base.filter.fetcher.GatewayContextFilterTokenFetcher;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.cloud.gateway.route.Route;
//import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
//import org.springframework.core.Ordered;
//import org.springframework.core.io.buffer.DataBuffer;
//import org.springframework.core.io.buffer.DataBufferUtils;
//import org.springframework.core.io.buffer.NettyDataBufferFactory;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.codec.HttpMessageReader;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
//import org.springframework.http.server.reactive.ServerHttpResponse;
//import org.springframework.stereotype.Component;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.reactive.function.server.HandlerStrategies;
//import org.springframework.web.reactive.function.server.ServerRequest;
//import org.springframework.web.server.ServerWebExchange;
//import org.springframework.web.util.UriComponentsBuilder;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//
//import java.net.URI;
//import java.nio.charset.StandardCharsets;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//
///**
// * mf.base.filter
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年04月20日 09:04
// */
//@Slf4j
//@Component
//public class RequestRecordFilter implements GlobalFilter, Ordered {
//    @Autowired
//    MeterRegistry meterRegistry;
//
//    @Autowired
//    private GatewayContextFilterFetcher gatewayContextFilterFetcher;
//    @Autowired
//    private GatewayContextFilterTokenFetcher gatewayContextFilterTokenFetcher;
//    /**
//     * default HttpMessageReader
//     */
//    private static final List<HttpMessageReader<?>> messageReaders = HandlerStrategies.withDefaults().messageReaders();
//
//
//    @Override
//    public int getOrder() {
//        return Integer.MIN_VALUE;
//    }
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        /**
//         * save request path and serviceId into gateway context
//         */
//        ServerHttpRequest request = exchange.getRequest();
//        String path = request.getPath().pathWithinApplication().value();
//        URI requestUri = request.getURI();
//        AccessRecord accessRecord = new AccessRecord();
//        accessRecord.setPath(requestUri.getPath());
//        accessRecord.setQueryString(request.getQueryParams());
//        /**
//         * save gateway context into exchange
//         */
//        HttpHeaders headers = request.getHeaders();
//        MediaType contentType = headers.getContentType();
//
//        Route gatewayUrl = exchange.getRequiredAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
//        URI uri = gatewayUrl.getUri();
//        String method = request.getMethodValue();
//        String URIPath = request.getURI().toString();
//        String instance = uri.getAuthority();
//        HttpHeaders header = request.getHeaders();
//        log.info("*********************请求信息*******************");
//        log.info("uri:{}", uri);
//        log.info("URIPath:{}", URIPath);
//        log.info("path:{}", path);
//        log.info("method:{}", method);
//        log.info("instance:{}", instance);
//        log.info("header:{}", header);
//        log.info("cookies", request.getCookies());
//        log.info("****************************************");
//
//        if ("POST".equals(method) && !MediaType.MULTIPART_FORM_DATA_VALUE.equals(contentType)) {
//            String bodyStr = resolveBodyFromRequest(request);
//
//            //下面将请求体再次封装写回到 request 里,传到下一级.
//            URI ex = UriComponentsBuilder.fromUri(requestUri).build(true).toUri();
//            ServerHttpRequest newRequest = request.mutate().uri(ex).build();
//            DataBuffer bodyDataBuffer = stringBuffer(bodyStr);
//            Flux<DataBuffer> bodyFlux = Flux.just(bodyDataBuffer);
//            newRequest = new ServerHttpRequestDecorator(newRequest) {
//                @Override
//                public Flux<DataBuffer> getBody() {
//                    return bodyFlux;
//                }
//            };
//            accessRecord.setBody(bodyStr);
//            ServerWebExchange newExchange = exchange.mutate().request(newRequest).build();
//            return returnMono(chain, newExchange, accessRecord);
//        } else {
//            return returnMono(chain, exchange, accessRecord);
//        }
//
////        long contentLength = headers.getContentLength();
////        if (contentLength > 0) {
////            if (MediaType.APPLICATION_JSON.equals(contentType) || MediaType.APPLICATION_JSON_UTF8.equals(contentType)) {
////                return readBody(exchange, chain, gatewayContext, header, request);
////            }
////        }
////        if ("GET".equalsIgnoreCase(method)) {
////            Map requestQueryParams = request.getQueryParams();
////            //得到Get请求的请求参数后，做你想做的事
////            gatewayContextFilterFetcher.saveRedisTemplateMsgFormIdGet(header, requestQueryParams);
////            log.info("GET入参：{}", JSON.toJSONString(requestQueryParams));
////            //验证token的有效性
//////            SimpleResponse simpleResponse = this.gatewayContextFilterTokenFetcher.checkTokenValid(request, requestQueryParams);
//////            if (simpleResponse == null || simpleResponse.error()) {
//////                return this.checkToken(simpleResponse, exchange);
//////            }
////        }
////        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
////            collectHttpResponseMetrics(exchange);
////        }));
//    }
//
//    private Mono<Void> returnMono(GatewayFilterChain chain, ServerWebExchange exchange, AccessRecord accessRecord) {
//        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
//            Long startTime = exchange.getAttribute("startTime");
//            if (startTime != null) {
//                long executeTime = (System.currentTimeMillis() - startTime);
//                accessRecord.setExpendTime(executeTime);
//                accessRecord.setHttpCode(Objects.requireNonNull(exchange.getResponse().getStatusCode()).value());
//                log.info("<<<<<<<<入参：{}", accessRecord);
//            }
//        }));
//    }
//
//    @Data
//    private class AccessRecord {
//        private String path;
//        private String body;
//        private MultiValueMap<String, String> queryString;
//        private long expendTime;
//        private int httpCode;
//    }
//
//    /**
//     * 获取请求体中的字符串内容
//     *
//     * @param serverHttpRequest
//     * @return
//     */
//    private String resolveBodyFromRequest(ServerHttpRequest serverHttpRequest) {
//        //获取请求体
//        Flux<DataBuffer> body = serverHttpRequest.getBody();
//        StringBuilder sb = new StringBuilder();
//
//        body.subscribe(buffer -> {
//            byte[] bytes = new byte[buffer.readableByteCount()];
//            buffer.read(bytes);
//            DataBufferUtils.release(buffer);
//            String bodyString = new String(bytes, StandardCharsets.UTF_8);
//            sb.append(bodyString);
//        });
//        return sb.toString();
//    }
//
//    private DataBuffer stringBuffer(String value) {
//        byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
//        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(ByteBufAllocator.DEFAULT);
//        DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
//        buffer.write(bytes);
//        return buffer;
//    }
//
//    /**
//     * 采集返回指标信息 -- prometheus
//     *
//     * @param exchange
//     */
//    private void collectHttpResponseMetrics(ServerWebExchange exchange) {
//
//        ServerHttpRequest request = exchange.getRequest();
//        ServerHttpResponse response = exchange.getResponse();
//        Route gatewayUrl = exchange.getRequiredAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
//
//        String path = request.getPath().pathWithinApplication().value();
//        String method = request.getMethodValue();
//
//        HttpStatus statusCode = response.getStatusCode();
//
//        URI uri = gatewayUrl.getUri();
//
//        String status = statusCode != null ? String.valueOf(statusCode.value()) : "NOT_FOUND";
//
//        meterRegistry.counter("http_requests_count",
//                "application", uri.getAuthority(),
//                "path", path,
//                "method", method,
//                "status", status)
//                .increment();
//    }
//
//    //异常token的异常返回
////    private Mono<Void> checkToken(SimpleResponse simpleResponse, ServerWebExchange exchange) {
////        //验证token的有效性
////        if (simpleResponse == null) {
////            simpleResponse = new SimpleResponse();
////        }
////        simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_TOKEN);
////        simpleResponse.setMessage("请传入有效的token");
////        byte[] bytes = JSONObject.toJSONString(simpleResponse).getBytes(StandardCharsets.UTF_8);
////        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
////        return exchange.getResponse().writeWith(Flux.just(buffer));
////    }
//
//    /**
//     * ReadJsonBody
//     *
//     * @param exchange
//     * @param chain
//     * @return
//     */
//    private Mono<Void> readBody(ServerWebExchange exchange, GatewayFilterChain chain, GatewayContext gatewayContext, HttpHeaders header, ServerHttpRequest request) {
//        /**
//         * join the body
//         */
//        return DataBufferUtils.join(exchange.getRequest().getBody())
//                .flatMap(dataBuffer -> {
//                    /**
//                     * read the body Flux<Databuffer>
//                     */
//                    DataBufferUtils.retain(dataBuffer);
//                    Flux<DataBuffer> cachedFlux = Flux.defer(() -> Flux.just(dataBuffer.slice(0, dataBuffer.readableByteCount())));
//                    /**
//                     * repackage ServerHttpRequest
//                     */
//                    ServerHttpRequest mutatedRequest = new ServerHttpRequestDecorator(exchange.getRequest()) {
//                        @Override
//                        public Flux<DataBuffer> getBody() {
//                            return cachedFlux;
//                        }
//                    };
//                    /**
//                     * mutate exchage with new ServerHttpRequest
//                     */
//                    ServerWebExchange mutatedExchange = exchange.mutate().request(mutatedRequest).build();
//                    /**
//                     * read body string with default messageReaders
//                     */
//                    return ServerRequest.create(mutatedExchange, messageReaders)
//                            .bodyToMono(String.class)
//                            .doOnNext(objectValue -> {
//                                gatewayContext.setCacheBody(objectValue);
//                                log.info("<<<<<<<<POST JSON入参：{}", objectValue);
//                                //得到POST JSON请求的请求参数后，做你想做的事
//                                try {
//                                    gatewayContextFilterFetcher.saveRedisTemplateMsgFormIdPost(header, objectValue);
//                                } catch (Exception e) {
//                                    log.error("<<<<<<<<Exception:{}", e);
//                                }
//                                //验证token
//                                Map requestQueryParams = JSONObject.parseObject(objectValue, Map.class);
//                                //验证token的有效性
////                                SimpleResponse simpleResponse = this.gatewayContextFilterTokenFetcher.checkTokenValid(request, requestQueryParams);
////                                if (simpleResponse == null || !simpleResponse.error()) {
////                                    Mono<Void> voidMono = checkToken(simpleResponse, exchange);
////                                    mutatedExchange.getResponse().setComplete();
////                                    log.warn(":{}",voidMono);
////                                }
//                            }).then(chain.filter(mutatedExchange));
//                });
//    }
//
////    /**
////     * ReadFormData
////     *
////     * @param exchange
////     * @param chain
////     * @return
////     */
////    private Mono<Void> readFormData(ServerWebExchange exchange, GatewayFilterChain chain, GatewayContext gatewayContext) {
////        HttpHeaders headers = exchange.getRequest().getHeaders();
////        return exchange.getFormData()
////                .doOnNext(multiValueMap -> {
////                    gatewayContext.setFormData(multiValueMap);
////                    log.debug("[GatewayContext]Read FormData:{}", multiValueMap);
////                })
////                .then(Mono.defer(() -> {
////                    Charset charset = headers.getContentType().getCharset();
////                    charset = charset == null ? StandardCharsets.UTF_8 : charset;
////                    String charsetName = charset.name();
////                    MultiValueMap<String, String> formData = gatewayContext.getFormData();
////                    /**
////                     * formData is empty just return
////                     */
////                    if (null == formData || formData.isEmpty()) {
////                        return chain.filter(exchange);
////                    }
////                    StringBuilder formDataBodyBuilder = new StringBuilder();
////                    String entryKey;
////                    List<String> entryValue;
////                    try {
////                        /**
////                         * remove system param ,repackage form data
////                         */
////                        for (Map.Entry<String, List<String>> entry : formData.entrySet()) {
////                            entryKey = entry.getKey();
////                            entryValue = entry.getValue();
////                            if (entryValue.size() > 1) {
////                                for (String value : entryValue) {
////                                    formDataBodyBuilder.append(entryKey).append("=").append(URLEncoder.encode(value, charsetName)).append("&");
////                                }
////                            } else {
////                                formDataBodyBuilder.append(entryKey).append("=").append(URLEncoder.encode(entryValue.get(0), charsetName)).append("&");
////                            }
////                        }
////                    } catch (UnsupportedEncodingException e) {
////                        //ignore URLEncode Exception
////                    }
////                    /**
////                     * substring with the last char '&'
////                     */
////                    String formDataBodyString = "";
////                    if (formDataBodyBuilder.length() > 0) {
////                        formDataBodyString = formDataBodyBuilder.substring(0, formDataBodyBuilder.length() - 1);
////                    }
////                    /**
////                     * get data bytes
////                     */
////                    byte[] bodyBytes = formDataBodyString.getBytes(charset);
////                    int contentLength = bodyBytes.length;
////                    ServerHttpRequestDecorator decorator = new ServerHttpRequestDecorator(
////                            exchange.getRequest()) {
////                        /**
////                         * change content-length
////                         * @return
////                         */
////                        @Override
////                        public HttpHeaders getHeaders() {
////                            HttpHeaders httpHeaders = new HttpHeaders();
////                            httpHeaders.putAll(super.getHeaders());
////                            if (contentLength > 0) {
////                                httpHeaders.setContentLength(contentLength);
////                            } else {
////                                httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
////                            }
////                            return httpHeaders;
////                        }
////
////                        /**
////                         * read bytes to Flux<Databuffer>
////                         * @return
////                         */
////                        @Override
////                        public Flux<DataBuffer> getBody() {
////                            return DataBufferUtils.read(new ByteArrayResource(bodyBytes), new NettyDataBufferFactory(ByteBufAllocator.DEFAULT), contentLength);
////                        }
////                    };
////                    ServerWebExchange mutateExchange = exchange.mutate().request(decorator).build();
////                    log.info("<<<<<<<<POST formData入参：{}", formDataBodyString);
////                    // 得到POST formData请求的请求参数后，做你想做的事
////                    return chain.filter(mutateExchange);
////                }));
////    }
//
//    @Data
//    public class GatewayContext {
//
//        public static final String CACHE_GATEWAY_CONTEXT = "cacheGatewayContext";
//
//        /**
//         * cache json body
//         */
//        private String cacheBody;
//        /**
//         * cache formdata
//         */
//        private MultiValueMap<String, String> formData;
//        /**
//         * cache reqeust path
//         */
//        private String path;
//    }
//
//}