package mf.base.filter.token.impl;

import lombok.extern.slf4j.Slf4j;
import mf.base.filter.token.TokenConverter;
import mf.base.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * mf.base.filter.token.impl
 *
 * @description: platform 平台运营token
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月28日 08:59
 */
@Service
@Slf4j
public class PlatformTokenConverter implements TokenConverter {
    @Value("${token.debug.mode}")
    private Integer debugMode;
    @Value("${platform.token.name}")
    private String tokenName;
    @Value("${platform.uid.name}")
    private String uidName;

    @Override
    public boolean support(String uriPath) {
        if (uriPath.indexOf("/platform/") > -1) {
            return true;
        }
        return false;
    }

    @Override
    public SimpleResponse run(ServerHttpRequest request, Map requestQueryParams) {
        return null;
    }
}
