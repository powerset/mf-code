package mf.base.filter.token;

import mf.base.simpleresp.SimpleResponse;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.Map;

/**
 * mf.base.filter.token
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月28日 08:57
 */
public interface TokenConverter {

    boolean support(String uriPath);

    SimpleResponse run(ServerHttpRequest request, Map requestQueryParams);
}
