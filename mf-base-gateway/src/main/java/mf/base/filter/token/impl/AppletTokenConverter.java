package mf.base.filter.token.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.base.filter.token.TokenConverter;
import mf.base.repo.po.User;
import mf.base.simpleresp.ApiStatusEnum;
import mf.base.simpleresp.SimpleResponse;
import mf.base.util.DateUtil;
import mf.base.util.RequestSignUtil;
import mf.base.util.TokenUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * mf.base.filter.token.impl
 *
 * @description: applet 小程序token
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月28日 08:59
 */
@Service
@Slf4j
public class AppletTokenConverter implements TokenConverter {
    @Value("${token.debug.mode}")
    private Integer debugMode;
    @Value("${mf.sign.key}")
    private String signKey;
    @Value("${mf.sign.name}")
    private String signName;
    @Value("${mf.sign.date}")
    private String signDateName;
    @Value("${mf.sign.debugMode}")
    private Integer signDebugMode;
    private final String TOKEN_NAME = "token";
    private final Long tenMinute = 10L * 60 * 1000;

//    @Autowired
//    private UserRepository userRepository;

    @Override
    public boolean support(String uriPath) {
        if (uriPath.indexOf("/applet/") > -1) {
            return true;
        }
        return false;
    }

    @Override
    public SimpleResponse run(ServerHttpRequest request, Map requestQueryParams) {
        SimpleResponse simpleResponse = new SimpleResponse();
        HttpHeaders headers = request.getHeaders();
        String method = request.getMethodValue();
        String uriPath = request.getURI().getPath();
        simpleResponse = this.checkSign(headers, method, uriPath, requestQueryParams);

        String token = "";
        if (headers.get(TOKEN_NAME) != null && StringUtils.isNotBlank(headers.get(TOKEN_NAME).toString())) {
            List<String> value = headers.get(TOKEN_NAME);
            token = value.get(0);
            if (StringUtils.isBlank(token)) {
                log.error("token不存在, 需前端静默请求AppletLoginApi，验证用户是否授权或生成新token");
                return tokenErrorReturn(simpleResponse);
            }
        }
        String userId = "";
        if ("GET".equalsIgnoreCase(method)) {
            userId = JSONObject.parseArray(requestQueryParams.get("userId").toString(), String.class).get(0);
        } else {
            userId = requestQueryParams.get("userId").toString();
        }

        /**
         *   uid 用户id，商户id，平台运营用户id
         *   user 小程序openid，微信公众号openid，商户手机号，平台运营用户手机号
         *   ctime 用户，商户，平台运营用户的数据记录的创建时间，yyyyMMddHHmmss格式
         *   now 系统时间，yyyyMMddHHmmss格式
         *   digest
         *
         *   XXX 固定值，场景不同，结尾语不同
         *   YYY 固定值，场景不同，结尾语不同
         *   ZZZ 固定值，场景不同，结尾语不同
         */
        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.APPLET);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(simpleResponse);
        }

        if (!StringUtils.equals(userId, stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 请求参数中的uid不匹配");
            return tokenErrorReturn(simpleResponse);
        }

//        User user = this.userRepository.selectById(NumberUtils.toLong(userId));
//        if (user == null || !StringUtils.equals(user.getOpenId(), stringObjectMap.get("user"))) {
//            log.error("token解密user数据 与 数据库中的openId不匹配");
//            return tokenErrorReturn(simpleResponse);
//        }
//        String md5Str = TokenUtil.md5forDigest(userId, user.getOpenId(), user.getCtime(), TokenUtil.APPLET);
//        if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
//            log.error("token解密后验签失败,token = " + token);
//            return tokenErrorReturn(simpleResponse);
//        }

        return simpleResponse;
    }

    //验证签名
    private SimpleResponse checkSign(HttpHeaders headers, String method, String uriPath, Map requestQueryParams) {
        SimpleResponse simpleResponse = new SimpleResponse();
        try {
            String sign = "";
            String signDateStr = "";
            if (headers.get(signName) != null && StringUtils.isNotBlank(headers.get(signName).toString())) {
                List<String> value = headers.get(signName);
                sign = value.get(0);
            }
            if ("GET".equalsIgnoreCase(method)) {
                signDateStr = JSONObject.parseArray(requestQueryParams.get(signDateName).toString(), String.class).get(0);
            } else {
                signDateStr = requestQueryParams.get(signDateName).toString();
            }

            if (StringUtils.isBlank(sign) || !StringUtils.isNumeric(signDateStr)) {
                log.error("{}，请求没有mfsign字段,或者没有signdate", uriPath);
                if (signDebugMode != null && signDebugMode != 1) {
                    return signErrorReturn(simpleResponse);
                }
            } else {
                // 判断签名时间，是否有效
                Long signDate = Long.valueOf(signDateStr);
                if (Math.abs(DateUtil.getCurrentMillis() - signDate) > tenMinute) {
                    log.error("{}，签名不匹配,入参:{}, 加密时间：{}, 当前时间：{}", uriPath, JSONObject.toJSONString(requestQueryParams), signDate, DateUtil.getCurrentMillis());
                    // 签名debug模式
                    if (signDebugMode != null && signDebugMode != 1) {
                        return signErrorReturn(simpleResponse);
                    }
                }
                // String signature = RequestSignUtil.generateSignatureForReq(requestQueryParams, signKey);
                // if (!StringUtils.equals(signature, sign)) {
                //     log.error("{}，签名不匹配,入参:{}, client:{}，server:{}", uriPath, JSONObject.toJSONString(requestQueryParams), sign, signature);
                //     // 签名debug模式
                //     if (signDebugMode != null && signDebugMode != 1) {
                //         return signErrorReturn(simpleResponse);
                //     }
                // }
            }
        } catch (Exception e) {
            log.error("校验签名异常", e);
        }
        return simpleResponse;
    }


    /**
     * 签名异常
     *
     * @param simpleResponse 入参simpleResponse
     * @return 返回boolean
     */
    private SimpleResponse signErrorReturn(SimpleResponse simpleResponse) {
        simpleResponse.setCode(ApiStatusEnum.ERROR_SIGN.getCode());
        simpleResponse.setMessage("签名错误，请使用微信小程序登录");
        return simpleResponse;
    }

    /**
     * token 异常
     *
     * @param simpleResponse 入参simpleResponse
     * @return 返回boolean
     */
    private SimpleResponse tokenErrorReturn(SimpleResponse simpleResponse) {
        simpleResponse.setCode(ApiStatusEnum.ERROR_TOKEN.getCode());
        simpleResponse.setMessage("需要重新登录");
        return simpleResponse;
    }
}
