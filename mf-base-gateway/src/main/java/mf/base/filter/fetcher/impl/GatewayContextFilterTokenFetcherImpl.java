package mf.base.filter.fetcher.impl;

import lombok.extern.slf4j.Slf4j;
import mf.base.filter.fetcher.GatewayContextFilterTokenFetcher;
import mf.base.filter.token.TokenConverter;
import mf.base.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * mf.base.filter.fetcher.impl
 *
 * @description: token 验证的请求
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月26日 10:18
 */
@Service
@Slf4j
public class GatewayContextFilterTokenFetcherImpl implements GatewayContextFilterTokenFetcher {
    @Value("${token.debug.mode}")
    private Integer debugMode;
    @Autowired
    private List<TokenConverter> converters;

    @Override
    public SimpleResponse checkTokenValid(ServerHttpRequest request, Map requestQueryParams) {
        //mode = 0 不校验token
        if (debugMode == 0) {
            return new SimpleResponse();
        }
        URI uri = request.getURI();
        String uriPath = uri.getPath();

        //TODO:无需验证的接口集合


        if (!CollectionUtils.isEmpty(this.converters)) {
            for (TokenConverter converter : converters) {
                if (converter.support(uriPath)) {
                    return converter.run(request, requestQueryParams);
                }
            }
        }

        return new SimpleResponse();
    }
}