package mf.base.filter.fetcher;

import org.springframework.http.HttpHeaders;

import java.util.Map;

/**
 * mf.base.filter.fetcher
 *
 * @description: formId的过滤取样器
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月20日 17:41
 */
public interface GatewayContextFilterFetcher {
    /***
     *
     * @param header 请求头信息
     * @param requestQueryParams get请求的所有入参
     */
    void saveRedisTemplateMsgFormIdGet(HttpHeaders header, Map requestQueryParams);

    /***
     *
     * @param header 请求头信息
     * @param objectValue post请求json请求体
     */
    void saveRedisTemplateMsgFormIdPost(HttpHeaders header, String objectValue);
}
