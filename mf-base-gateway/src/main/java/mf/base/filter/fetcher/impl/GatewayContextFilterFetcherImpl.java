package mf.base.filter.fetcher.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.base.filter.fetcher.GatewayContextFilterFetcher;
import mf.base.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.base.filter.fetcher.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月20日 17:43
 */
@Service
@Slf4j
public class GatewayContextFilterFetcherImpl implements GatewayContextFilterFetcher {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final String MOCK_FORM = "the formId is a mock one";

    @Override
    public void saveRedisTemplateMsgFormIdGet(HttpHeaders header, Map requestQueryParams) {
        if (requestQueryParams == null || requestQueryParams.get("formId") == null ||
                requestQueryParams.get("userId") == null || StringUtils.isBlank(requestQueryParams.get("userId").toString())) {
            return;
        }
        //根据header区分不同的小程序
        String redisKey = this.getProjectAppleteRedisKey(header);
        if (StringUtils.isBlank(redisKey)) {
            return;
        }

        //"["1","2"]"
        List<String> formIdList = JSONObject.parseArray(requestQueryParams.get("formId").toString(), String.class);
        if (CollectionUtils.isEmpty(formIdList)) {
            return;
        }
        String arrayFormIdsStr = JSONObject.parseArray(requestQueryParams.get("formId").toString(), String.class).get(0);
        if (StringUtils.isBlank(arrayFormIdsStr)) {
            return;
        }

        List<String> formIdsStrs = JSONObject.parseArray(arrayFormIdsStr, String.class);
        if (CollectionUtils.isEmpty(formIdsStrs)) {
            return;
        }

        List<String> userIds = JSONObject.parseArray(requestQueryParams.get("userId").toString(), String.class);
        if(CollectionUtils.isEmpty(userIds)){
            return;
        }
        String userId = userIds.get(0);
        log.info("<<<<<<<<formIds:{}", formIdsStrs);
        this.saveFormId(redisKey, formIdsStrs, NumberUtils.toLong(userId));
    }

    @Override
    public void saveRedisTemplateMsgFormIdPost(HttpHeaders header, String objectValue) {
        Map map = JSONObject.parseObject(objectValue, Map.class);
        if (map == null || map.get("formId") == null || map.get("userId") == null) {
            return;
        }
        //根据header区分不同的小程序
        String redisKey = this.getProjectAppleteRedisKey(header);
        if (StringUtils.isBlank(redisKey)) {
            return;
        }
        List<String> arrayFormIdsStr = JSONObject.parseArray(map.get("formId").toString(), String.class);
        Long userId = NumberUtils.toLong(map.get("userId").toString());
        log.info("<<<<<<<<formIds:{}", arrayFormIdsStr);
        this.saveFormId(redisKey, arrayFormIdsStr, userId);
    }


    /***
     * 拦截的小程序是哪个
     * @param header
     * @return
     */
    private String getProjectAppleteRedisKey(HttpHeaders header) {
        String project = "";
        if (header.get("project") != null && StringUtils.isNotBlank(header.get("project").toString())) {
            List<String> value = header.get("project");
            if ("jkmf".equals(value.get(0))) {
                return RedisKeyConstant.JKMF_SHOPPINGMALL_TEMPLATEMSGKEY;
            }
        }
        return project;
    }

    /***
     * 存储formId
     * @param redisKey
     * @param formIds
     * @param userId
     */
    private void saveFormId(String redisKey, List<String> formIds, Long userId) {
        if(CollectionUtils.isEmpty(formIds)){
            return;
        }
        redisKey = redisKey + userId;
        //查询redis,参与人展现,直接倒叙排列
        Map<String, Object> formIdMap = new HashMap<>();
        BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
        List<Object> strs = activityPersons.range(0, -1);
        if (!CollectionUtils.isEmpty(strs)) {
            for (Object object : strs) {
                JSONObject jsonObject = JSON.parseObject(object.toString());
                String formID = jsonObject.getString("formId");
                formIdMap.put(formID, jsonObject.toString());
            }
        }
        //刚进来的formId自我去重
        List<String> needFormIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(formIds) && !formIds.contains(MOCK_FORM)) {
            for (String formId : formIds) {
                if (!MOCK_FORM.equals(formId) && needFormIds.indexOf(formId) == -1) {
                    needFormIds.add(formId);
                }
            }
        }
        //formId进行存储redis
        if (!CollectionUtils.isEmpty(needFormIds)) {
            for (String formId : needFormIds) {
                //去重
                Object object = formIdMap.get(formId);
                if (object == null) {
                    JSONObject userJsonObject = this.processTemplateMessageUserInfo(formId);
                    this.stringRedisTemplate.opsForList().rightPush(redisKey, userJsonObject.toJSONString());
                    this.stringRedisTemplate.expire(redisKey, 7, TimeUnit.DAYS);
                }
            }
        }
    }

    // 私有方法：加工用户邀请列表的用户信息
    private JSONObject processTemplateMessageUserInfo(String formId) {
        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("formId", formId);
        userJsonObject.put("ctime", System.currentTimeMillis());
        return userJsonObject;
    }
}
