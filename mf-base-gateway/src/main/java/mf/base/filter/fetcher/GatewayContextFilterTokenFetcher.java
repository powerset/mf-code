package mf.base.filter.fetcher;

import mf.base.simpleresp.SimpleResponse;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.Map;

/**
 * mf.base.filter.fetcher
 *
 * @description: token过滤取样器
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月26日 10:18
 */
public interface GatewayContextFilterTokenFetcher {
    //验证token的有效性
    SimpleResponse checkTokenValid(ServerHttpRequest request, Map requestQueryParams);
}
