package mf.base.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * mf.code.common.filter
 * Description: 解决跨域请求的问题
 *
 * @author: AAF
 * @date: 2018-10-07 15:29
 */
@Component
public class AccessFilter extends HttpServlet implements Filter {
    private static final long serialVersionUID = 1L;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        //设置允许跨域的配置
        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");
        httpResponse.setHeader("Access-Control-Max-Age", "3000");
        httpResponse.setHeader("Access-Control-Allow-Headers", "uid, token, shopId, scene, debugToken, mfsign," +
                "X-Forwarded-For, User-Agent, Origin, No-Cache, Authorization, Cookie, X-Requested-With, Content-Type, Content-Length, Accept, Accept-Encoding, Accept-Language, xhrFields, Access-Control-Allow-Headers");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        chain.doFilter(request, httpResponse);
    }
}
