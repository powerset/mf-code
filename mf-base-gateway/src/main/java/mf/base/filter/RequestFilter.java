//package mf.base.filter;
//
//import com.alibaba.fastjson.JSON;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.gateway.filter.GatewayFilter;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.core.Ordered;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.util.Map;
//
///**
// * @author lili
// * @description ${DESCRIPTION}
// * @create 2018-12-11 11:50
// * @since
// **/
//
//@Slf4j
//@Component
//public class RequestFilter implements GatewayFilter, Ordered {
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        ServerHttpRequest request = exchange.getRequest();
//        String method = request.getMethodValue();
//        HttpHeaders headers = request.getHeaders();
//        MediaType contentType = headers.getContentType();
//
//        if ("GET".equalsIgnoreCase(method)) {
//            Map requestQueryParams = request.getQueryParams();
//            log.info("GET入参：{}", JSON.toJSONString(requestQueryParams));
//        }
//        if (MediaType.APPLICATION_JSON.equals(contentType) || MediaType.APPLICATION_JSON_UTF8.equals(contentType)) {
//            Object requestBody = exchange.getAttribute("cachedRequestBodyObject");
//            log.info("request body is:{}", requestBody);
//        }
//
//        return chain.filter(exchange);
//    }
//
//    @Override
//    public int getOrder() {
//        return Integer.MIN_VALUE;
//    }
//}
