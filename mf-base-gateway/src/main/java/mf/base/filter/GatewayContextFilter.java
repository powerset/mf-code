package mf.base.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.base.filter.fetcher.GatewayContextFilterFetcher;
import mf.base.filter.fetcher.GatewayContextFilterTokenFetcher;
import mf.base.util.DateUtil;
import mf.base.util.RequestSignUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.HttpMessageReader;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * mf.base.filter
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月20日 09:04
 */
@Slf4j
@Component
public class GatewayContextFilter implements GlobalFilter, Ordered {

	@Value("${mf.sign.key}")
	private String signKey;
	@Value("${mf.sign.name}")
	private String signName;
	@Value("${mf.sign.date}")
	private String signDateName;
	@Value("${mf.sign.debugMode}")
	private Integer signDebugMode;
	private final Long tenMinute = 10L * 60 * 1000;

	@Autowired
	MeterRegistry meterRegistry;

	@Autowired
	private GatewayContextFilterFetcher gatewayContextFilterFetcher;
	@Autowired
	private GatewayContextFilterTokenFetcher gatewayContextFilterTokenFetcher;
	/**
	 * default HttpMessageReader
	 */
	private static final List<HttpMessageReader<?>> messageReaders = HandlerStrategies.withDefaults().messageReaders();


	@Override
	public int getOrder() {
		return Integer.MIN_VALUE;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		/**
		 * save request path and serviceId into gateway context
		 */
		ServerHttpRequest request = exchange.getRequest();
		HttpHeaders headers = request.getHeaders();
		Route gatewayUrl = exchange.getRequiredAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
		String URIPath = request.getURI().toString();
		String path = request.getPath().pathWithinApplication().value();
		GatewayContext gatewayContext = new GatewayContext();
		gatewayContext.setPath(path);
		// save gateway context into exchange
		exchange.getAttributes().put(GatewayContext.CACHE_GATEWAY_CONTEXT, gatewayContext);
		MediaType contentType = headers.getContentType();
		String method = request.getMethodValue();
		URI uri = gatewayUrl.getUri();
		String instance = uri.getAuthority();

		long contentLength = headers.getContentLength();

		// 添加校验sign
		if (URIPath.matches(".*/applet/.*") && "GET".equalsIgnoreCase(method)) {
			try {
				// 判断签名时间，是否有效
				// GET 请求
				String sign = headers.getFirst(signName);

				MultiValueMap<String, String> queryParams = request.getQueryParams();
				String signDateStr = queryParams.getFirst(signDateName);

				if (StringUtils.isBlank(sign) || !StringUtils.isNumeric(signDateStr)) {
					log.error(URIPath + "，请求没有mfsign字段,或者没有signdate");
					// 签名debug模式
					if (signDebugMode != null && signDebugMode != 1) {
						exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
						return exchange.getResponse().setComplete();
					}
				} else {
					Long signDate = Long.valueOf(signDateStr);
					if (Math.abs(DateUtil.getCurrentMillis() - signDate) > tenMinute) {
						log.error(URIPath + "，签名不匹配,入参:" + JSONObject.toJSONString(queryParams)
								+ ", 加密时间：" + signDate + ", 当前时间：" + DateUtil.getCurrentMillis());
						// 签名debug模式
						if (signDebugMode != null && signDebugMode != 1) {
							exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
							return exchange.getResponse().setComplete();
						}
					}

					String signature = RequestSignUtil.generateSignatureForReq(queryParams, signKey);
					if (signature == null || !StringUtils.equals(signature, sign)) {
						log.error(URIPath + "，签名不匹配,入参:" + JSONObject.toJSONString(queryParams)
								+ ", client:" + sign + "，server:" + signature);
						// 签名debug模式
						if (signDebugMode != null && signDebugMode != 1) {
							exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
							return exchange.getResponse().setComplete();
						}
					}
				}
			} catch (Exception e) {
				log.error("校验签名异常", e);

				exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
				return exchange.getResponse().setComplete();
			}
		}

		if (contentLength > 0) {
			if (MediaType.APPLICATION_JSON.equals(contentType) || MediaType.APPLICATION_JSON_UTF8.equals(contentType)) {
				return readBody(exchange, chain, gatewayContext, headers, request);
			}
		}
		if ("GET".equalsIgnoreCase(method)) {
			Map requestQueryParams = request.getQueryParams();
			//得到Get请求的请求参数后，做你想做的事
			gatewayContextFilterFetcher.saveRedisTemplateMsgFormIdGet(headers, requestQueryParams);
			log.info("<<<<<<<<URIPath:{}, GET入参：{}", URIPath, JSON.toJSONString(requestQueryParams));
			//验证token的有效性
//            SimpleResponse simpleResponse = this.gatewayContextFilterTokenFetcher.checkTokenValid(request, requestQueryParams);
//            if (simpleResponse == null || simpleResponse.error()) {
//                return this.checkToken(simpleResponse, exchange);
//            }
		}
		return chain.filter(exchange).then(Mono.fromRunnable(() -> {
			collectHttpResponseMetrics(exchange);
		}));
	}

	/**
	 * 采集返回指标信息 -- prometheus
	 *
	 * @param exchange
	 */
	private void collectHttpResponseMetrics(ServerWebExchange exchange) {

		ServerHttpRequest request = exchange.getRequest();
		ServerHttpResponse response = exchange.getResponse();
		Route gatewayUrl = exchange.getRequiredAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);

		String path = request.getPath().pathWithinApplication().value();
		String method = request.getMethodValue();

		HttpStatus statusCode = response.getStatusCode();

		URI uri = gatewayUrl.getUri();

		String status = statusCode != null ? String.valueOf(statusCode.value()) : "NOT_FOUND";
		meterRegistry.counter("http_requests_count",
				"application", uri.getAuthority(),
				"path", path,
				"method", method,
				"status", status)
				.increment();
	}

	//异常token的异常返回
//    private Mono<Void> checkToken(SimpleResponse simpleResponse, ServerWebExchange exchange) {
//        //验证token的有效性
//        if (simpleResponse == null) {
//            simpleResponse = new SimpleResponse();
//        }
//        simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_TOKEN);
//        simpleResponse.setMessage("请传入有效的token");
//        byte[] bytes = JSONObject.toJSONString(simpleResponse).getBytes(StandardCharsets.UTF_8);
//        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
//        return exchange.getResponse().writeWith(Flux.just(buffer));
//    }

	/**
	 * ReadJsonBody
	 *
	 * @param exchange
	 * @param chain
	 * @return
	 */
	private Mono<Void> readBody(ServerWebExchange exchange, GatewayFilterChain chain, GatewayContext gatewayContext, HttpHeaders header, ServerHttpRequest request) {
		/**
		 * join the body
		 */
		return DataBufferUtils.join(exchange.getRequest().getBody())
				.flatMap(dataBuffer -> {
					/**
					 * read the body Flux<Databuffer>
					 */
					DataBufferUtils.retain(dataBuffer);
					Flux<DataBuffer> cachedFlux = Flux.defer(() -> Flux.just(dataBuffer.slice(0, dataBuffer.readableByteCount())));
					/**
					 * repackage ServerHttpRequest
					 */
					ServerHttpRequest mutatedRequest = new ServerHttpRequestDecorator(exchange.getRequest()) {
						@Override
						public Flux<DataBuffer> getBody() {
							return cachedFlux;
						}
					};
					/**
					 * mutate exchage with new ServerHttpRequest
					 */
					ServerWebExchange mutatedExchange = exchange.mutate().request(mutatedRequest).build();
					/**
					 * read body string with default messageReaders
					 */
					AtomicBoolean flag = new AtomicBoolean(false);
					Mono<Void> mono = ServerRequest.create(mutatedExchange, messageReaders)
							.bodyToMono(String.class)
							.doOnNext(objectValue -> {
								gatewayContext.setCacheBody(objectValue);
								log.info("<<<<<<<<URIPath:{}, POST JSON入参：{}", request.getURI().toString(), objectValue);
								//得到POST JSON请求的请求参数后，做你想做的事
								try {
									gatewayContextFilterFetcher.saveRedisTemplateMsgFormIdPost(header, objectValue);
								} catch (Exception e) {
									log.error("<<<<<<<<Exception:{}", e);
								}

								JSONObject jsonObject = JSONObject.parseObject(objectValue);

								// POST 请求
								String URIPath = request.getURI().toString();
								HttpHeaders headers = request.getHeaders();
								String sign = headers.getFirst(signName);
								String signDateStr = jsonObject.getString(signDateName);

								if (URIPath.matches(".*/applet/.*")) {
									try {

										if (StringUtils.isBlank(sign) || !StringUtils.isNumeric(signDateStr)) {
											log.error(URIPath + "，请求没有mfsign字段,或者没有signdate");
											// 签名debug模式
											if (signDebugMode != null && signDebugMode != 1) {
												flag.set(true);
											}
										} else {
											Long signDate = Long.valueOf(signDateStr);
											if (Math.abs(DateUtil.getCurrentMillis() - signDate) > tenMinute) {
												log.error(URIPath + "，签名不匹配,入参:" + jsonObject.toJSONString()
														+ ", 加密时间：" + signDate + ", 当前时间：" + DateUtil.getCurrentMillis());
												// 签名debug模式
												if (signDebugMode != null && signDebugMode != 1) {
													flag.set(true);
												}
											}

											String signature = RequestSignUtil.generateSignatureForReqBody(jsonObject, signKey);
											if (signature == null || !StringUtils.equals(signature, sign)) {
												log.error(URIPath + "，签名不匹配,入参:" + jsonObject.toJSONString()
														+ ", client:" + sign + "，server:" + signature);
												// 签名debug模式
												if (signDebugMode != null && signDebugMode != 1) {
													flag.set(true);
												}
											}
										}
									} catch (Exception e) {
										log.error("校验签名异常", e);
										flag.set(true);
									}
								}

							}).then(chain.filter(mutatedExchange));
					if (flag.get()) {
						exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
						return exchange.getResponse().setComplete();
					}
					return mono;

				});
	}

//    /**
//     * ReadFormData
//     *
//     * @param exchange
//     * @param chain
//     * @return
//     */
//    private Mono<Void> readFormData(ServerWebExchange exchange, GatewayFilterChain chain, GatewayContext gatewayContext) {
//        HttpHeaders headers = exchange.getRequest().getHeaders();
//        return exchange.getFormData()
//                .doOnNext(multiValueMap -> {
//                    gatewayContext.setFormData(multiValueMap);
//                    log.debug("[GatewayContext]Read FormData:{}", multiValueMap);
//                })
//                .then(Mono.defer(() -> {
//                    Charset charset = headers.getContentType().getCharset();
//                    charset = charset == null ? StandardCharsets.UTF_8 : charset;
//                    String charsetName = charset.name();
//                    MultiValueMap<String, String> formData = gatewayContext.getFormData();
//                    /**
//                     * formData is empty just return
//                     */
//                    if (null == formData || formData.isEmpty()) {
//                        return chain.filter(exchange);
//                    }
//                    StringBuilder formDataBodyBuilder = new StringBuilder();
//                    String entryKey;
//                    List<String> entryValue;
//                    try {
//                        /**
//                         * remove system param ,repackage form data
//                         */
//                        for (Map.Entry<String, List<String>> entry : formData.entrySet()) {
//                            entryKey = entry.getKey();
//                            entryValue = entry.getValue();
//                            if (entryValue.size() > 1) {
//                                for (String value : entryValue) {
//                                    formDataBodyBuilder.append(entryKey).append("=").append(URLEncoder.encode(value, charsetName)).append("&");
//                                }
//                            } else {
//                                formDataBodyBuilder.append(entryKey).append("=").append(URLEncoder.encode(entryValue.get(0), charsetName)).append("&");
//                            }
//                        }
//                    } catch (UnsupportedEncodingException e) {
//                        //ignore URLEncode Exception
//                    }
//                    /**
//                     * substring with the last char '&'
//                     */
//                    String formDataBodyString = "";
//                    if (formDataBodyBuilder.length() > 0) {
//                        formDataBodyString = formDataBodyBuilder.substring(0, formDataBodyBuilder.length() - 1);
//                    }
//                    /**
//                     * get data bytes
//                     */
//                    byte[] bodyBytes = formDataBodyString.getBytes(charset);
//                    int contentLength = bodyBytes.length;
//                    ServerHttpRequestDecorator decorator = new ServerHttpRequestDecorator(
//                            exchange.getRequest()) {
//                        /**
//                         * change content-length
//                         * @return
//                         */
//                        @Override
//                        public HttpHeaders getHeaders() {
//                            HttpHeaders httpHeaders = new HttpHeaders();
//                            httpHeaders.putAll(super.getHeaders());
//                            if (contentLength > 0) {
//                                httpHeaders.setContentLength(contentLength);
//                            } else {
//                                httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
//                            }
//                            return httpHeaders;
//                        }
//
//                        /**
//                         * read bytes to Flux<Databuffer>
//                         * @return
//                         */
//                        @Override
//                        public Flux<DataBuffer> getBody() {
//                            return DataBufferUtils.read(new ByteArrayResource(bodyBytes), new NettyDataBufferFactory(ByteBufAllocator.DEFAULT), contentLength);
//                        }
//                    };
//                    ServerWebExchange mutateExchange = exchange.mutate().request(decorator).build();
//                    log.info("<<<<<<<<POST formData入参：{}", formDataBodyString);
//                    // 得到POST formData请求的请求参数后，做你想做的事
//                    return chain.filter(mutateExchange);
//                }));
//    }

	@Data
	public class GatewayContext {

		public static final String CACHE_GATEWAY_CONTEXT = "cacheGatewayContext";

		/**
		 * cache json body
		 */
		private String cacheBody;
		/**
		 * cache formdata
		 */
		private MultiValueMap<String, String> formData;
		/**
		 * cache reqeust path
		 */
		private String path;
	}

}
