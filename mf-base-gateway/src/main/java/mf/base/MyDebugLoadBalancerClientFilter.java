package mf.base;

import com.google.common.base.Strings;
import com.netflix.loadbalancer.Server;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.gateway.config.LoadBalancerProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.LoadBalancerClientFilter;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * GW扩展调试LoadBalancerClientFilter
 * <pre>
 * 请求头包含debug@xxx的情况，置换服务发现及负载均衡的处理结果，根据请求头字段的值重新构造Server实例，以方便开发调试
 * isDebugMode方法暂时硬编码，TODO 以后做在配置中心
 * 例
 *   curl -H 'debug_user:httpbin.org:80' http://localhost:7008/api/user/?debug=1
 *   注意 debug@xxx的值一定要有:
 *   注意 请求头会附带着传给GW后面的服务
 * RibbonLoadBalancerClient同样扩展MyDebugRibbonLoadBalancerClient
 * </pre>
 * @author 梦典
 */
public class MyDebugLoadBalancerClientFilter extends LoadBalancerClientFilter {
    private static final Log log = LogFactory.getLog(LoadBalancerClientFilter.class);
    private LoadBalancerClient loadBalancer = null;
    private LoadBalancerProperties properties;

    public MyDebugLoadBalancerClientFilter(LoadBalancerClient loadBalancer, LoadBalancerProperties properties) {
        super(loadBalancer, properties);
        this.loadBalancer = loadBalancer;
        this.properties = properties;
    }

    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        URI url = (URI) exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR);
        String schemePrefix = (String) exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_SCHEME_PREFIX_ATTR);
        if (url != null && ("lb".equals(url.getScheme()) || "lb".equals(schemePrefix))) {
            ServerWebExchangeUtils.addOriginalRequestUrl(exchange, url);
            log.trace("LoadBalancerClientFilter url before: " + url);
            ServiceInstance instance = this.choose(exchange);
            if (instance == null) {
                throw NotFoundException.create(this.properties.isUse404(), "Unable to find instance for " + url.getHost());
            } else {
                URI uri = exchange.getRequest().getURI();
                URI requestUrl = this.loadBalancer.reconstructURI(instance, uri);
                log.trace("LoadBalancerClientFilter url chosen: " + requestUrl);
                // START
                requestUrl = myFilter(uri, url, instance, requestUrl, exchange);
                // END
                exchange.getAttributes().put(ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR, requestUrl);
                return chain.filter(exchange);
            }
        } else {
            return chain.filter(exchange);
        }
    }

    /**
     * @param uri        原始请求uri
     * @param url        GW路由定义url
     * @param instance   负载均衡选择结果服务实例
     * @param requestUrl 负载均衡选择结果服务url
     * @param exchange   ServerWebExchange
     * @return
     */
    private URI myFilter(URI uri, URI url, ServiceInstance instance, URI requestUrl, ServerWebExchange exchange) {
        ServerHttpRequest req = exchange.getRequest();
        HttpHeaders httpHeaders = req.getHeaders();
        for (String key : httpHeaders.keySet()) {
            if (key.startsWith("debug_") && key.length() > 6) {
                String name = key.substring(6);
                if (isDebugMode(name, instance)) {
                    List<String> valueList = (List<String>) httpHeaders.get(key);
                    if (!valueList.isEmpty() && valueList.get(0) != null && !valueList.get(0).isEmpty() && valueList.get(0).indexOf(":") > -1) {
                        String strValue = valueList.get(0);
                        String strHost = strValue.substring(0, strValue.indexOf(":"));
                        String strPort = strValue.substring(strValue.indexOf(":") + 1);
                        log.info("debug mode, name=" + name + ", instance=" + instance + ", host=" + strHost + ", port=" + strPort);
                        Server newServer = new Server("http", strHost, Integer.parseInt(strPort));
                        URI newUri = reconstructURIWithServer(newServer, url);
                        log.info(newUri);
                        return newUri;
                    }
                }
            }
        }
        return requestUrl;
    }

    private boolean isDebugMode(String name, ServiceInstance instance) {
        if ("user".equals(name) && "MF-CODE-USER".equals(instance.getServiceId())) {
            return true;
        }
        return false;
    }

    public URI reconstructURIWithServer(Server server, URI original) {
        String host = server.getHost();
        int port = server.getPort();
        String scheme = server.getScheme();
        if (host.equals(original.getHost()) && port == original.getPort() && scheme == original.getScheme()) {
            return original;
        } else {
            if (scheme == null) {
                scheme = original.getScheme();
            }
//            if (scheme == null) {
//                scheme = (String)this.deriveSchemeAndPortFromPartialUri(original).first();
//            }
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(scheme).append("://");
                if (!Strings.isNullOrEmpty(original.getRawUserInfo())) {
                    sb.append(original.getRawUserInfo()).append("@");
                }
                sb.append(host);
                if (port >= 0) {
                    sb.append(":").append(port);
                }
                sb.append(original.getRawPath());
                if (!Strings.isNullOrEmpty(original.getRawQuery())) {
                    sb.append("?").append(original.getRawQuery());
                }
                if (!Strings.isNullOrEmpty(original.getRawFragment())) {
                    sb.append("#").append(original.getRawFragment());
                }
                URI newURI = new URI(sb.toString());
                return newURI;
            } catch (URISyntaxException var8) {
                throw new RuntimeException(var8);
            }
        }
    }
}
