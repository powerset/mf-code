//package mf.base.repo.dao;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import mf.base.repo.po.User;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface UserMapper extends BaseMapper<User> {
//    /**
//     *  根据主键删除数据库的记录
//     *
//     * @param id
//     */
//    int deleteByPrimaryKey(Long id);
//
//    /**
//     *  新写入数据库记录
//     *
//     * @param record
//     */
//    int insert(User record);
//
//    /**
//     *  动态字段,写入数据库记录
//     *
//     * @param record
//     */
//    int insertSelective(User record);
//
//    /**
//     *  根据指定主键获取一条数据库记录
//     *
//     * @param id
//     */
//    User selectByPrimaryKey(Long id);
//
//    /**
//     *  动态字段,根据主键来更新符合条件的数据库记录
//     *
//     * @param record
//     */
//    int updateByPrimaryKeySelective(User record);
//
//    /**
//     *  根据主键来更新符合条件的数据库记录
//     *
//     * @param record
//     */
//    int updateByPrimaryKey(User record);
//}