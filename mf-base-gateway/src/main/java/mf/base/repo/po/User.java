package mf.base.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * user
 * 用户表 用于记录用户的登录信息,验证用户授权状态,获取用户信息
 */
public class User implements Serializable {
    /**
     * 用户ID
     */
    private Long id;

    /**
     * openid
     */
    private String openId;

    /**
     * 商户Id，merchant表的主键
     */
    private Long merchantId;

    /**
     * 对应商户id下的店铺id
     */
    private Long shopId;

    /**
     * 登录场景
     */
    private Integer scene;

    /**
     * sessionKey
     */
    private String sessionKey;

    /**
     * 公众平台openid
     */
    private String wxopenid;

    /**
     * unionid
     */
    private String unionId;

    /**
     * 用户手机号码
     */
    private String mobile;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像图片地址
     */
    private String avatarUrl;

    /**
     * 性别，0:未知；1:男；2:女
     */
    private Integer gender;

    /**
     * 城市
     */
    private String city;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 用户授权状态，0:未授权，1:授权，-1机器人，-2平台
     */
    private Integer grantStatus;

    /**
     * 用户授权时间
     */
    private Date grantTime;

    /**
     * 此用户属于为哪个店铺的粉丝
     */
    private Long vipShopId;

    /**
     * 提现协议 0 未签署，1签署
     */
    private Integer cashProtocal;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * user
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     * @return id 用户ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户ID
     * @param id 用户ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * openid
     * @return open_id openid
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * openid
     * @param openId openid
     */
    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    /**
     * 商户Id，merchant表的主键
     * @return merchant_id 商户Id，merchant表的主键
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户Id，merchant表的主键
     * @param merchantId 商户Id，merchant表的主键
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 对应商户id下的店铺id
     * @return shop_id 对应商户id下的店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 对应商户id下的店铺id
     * @param shopId 对应商户id下的店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 登录场景
     * @return scene 登录场景
     */
    public Integer getScene() {
        return scene;
    }

    /**
     * 登录场景
     * @param scene 登录场景
     */
    public void setScene(Integer scene) {
        this.scene = scene;
    }

    /**
     * sessionKey
     * @return session_key sessionKey
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * sessionKey
     * @param sessionKey sessionKey
     */
    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey == null ? null : sessionKey.trim();
    }

    /**
     * 公众平台openid
     * @return wxopenid 公众平台openid
     */
    public String getWxopenid() {
        return wxopenid;
    }

    /**
     * 公众平台openid
     * @param wxopenid 公众平台openid
     */
    public void setWxopenid(String wxopenid) {
        this.wxopenid = wxopenid == null ? null : wxopenid.trim();
    }

    /**
     * unionid
     * @return union_id unionid
     */
    public String getUnionId() {
        return unionId;
    }

    /**
     * unionid
     * @param unionId unionid
     */
    public void setUnionId(String unionId) {
        this.unionId = unionId == null ? null : unionId.trim();
    }

    /**
     * 用户手机号码
     * @return mobile 用户手机号码
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 用户手机号码
     * @param mobile 用户手机号码
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 昵称
     * @return nick_name 昵称
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * 昵称
     * @param nickName 昵称
     */
    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    /**
     * 头像图片地址
     * @return avatar_url 头像图片地址
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     * 头像图片地址
     * @param avatarUrl 头像图片地址
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl == null ? null : avatarUrl.trim();
    }

    /**
     * 性别，0:未知；1:男；2:女
     * @return gender 性别，0:未知；1:男；2:女
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 性别，0:未知；1:男；2:女
     * @param gender 性别，0:未知；1:男；2:女
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 城市
     * @return city 城市
     */
    public String getCity() {
        return city;
    }

    /**
     * 城市
     * @param city 城市
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * 国家
     * @return country 国家
     */
    public String getCountry() {
        return country;
    }

    /**
     * 国家
     * @param country 国家
     */
    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    /**
     * 省份
     * @return province 省份
     */
    public String getProvince() {
        return province;
    }

    /**
     * 省份
     * @param province 省份
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * 用户授权状态，0:未授权，1:授权，-1机器人，-2平台
     * @return grant_status 用户授权状态，0:未授权，1:授权，-1机器人，-2平台
     */
    public Integer getGrantStatus() {
        return grantStatus;
    }

    /**
     * 用户授权状态，0:未授权，1:授权，-1机器人，-2平台
     * @param grantStatus 用户授权状态，0:未授权，1:授权，-1机器人，-2平台
     */
    public void setGrantStatus(Integer grantStatus) {
        this.grantStatus = grantStatus;
    }

    /**
     * 用户授权时间
     * @return grant_time 用户授权时间
     */
    public Date getGrantTime() {
        return grantTime;
    }

    /**
     * 用户授权时间
     * @param grantTime 用户授权时间
     */
    public void setGrantTime(Date grantTime) {
        this.grantTime = grantTime;
    }

    /**
     * 此用户属于为哪个店铺的粉丝
     * @return vip_shop_id 此用户属于为哪个店铺的粉丝
     */
    public Long getVipShopId() {
        return vipShopId;
    }

    /**
     * 此用户属于为哪个店铺的粉丝
     * @param vipShopId 此用户属于为哪个店铺的粉丝
     */
    public void setVipShopId(Long vipShopId) {
        this.vipShopId = vipShopId;
    }

    /**
     * 提现协议 0 未签署，1签署
     * @return cash_protocal 提现协议 0 未签署，1签署
     */
    public Integer getCashProtocal() {
        return cashProtocal;
    }

    /**
     * 提现协议 0 未签署，1签署
     * @param cashProtocal 提现协议 0 未签署，1签署
     */
    public void setCashProtocal(Integer cashProtocal) {
        this.cashProtocal = cashProtocal;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", openId=").append(openId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", scene=").append(scene);
        sb.append(", sessionKey=").append(sessionKey);
        sb.append(", wxopenid=").append(wxopenid);
        sb.append(", unionId=").append(unionId);
        sb.append(", mobile=").append(mobile);
        sb.append(", nickName=").append(nickName);
        sb.append(", avatarUrl=").append(avatarUrl);
        sb.append(", gender=").append(gender);
        sb.append(", city=").append(city);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", grantStatus=").append(grantStatus);
        sb.append(", grantTime=").append(grantTime);
        sb.append(", vipShopId=").append(vipShopId);
        sb.append(", cashProtocal=").append(cashProtocal);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}