//package mf.base.repo.repository.impl;
//
//import mf.base.repo.dao.UserMapper;
//import mf.base.repo.po.User;
//import mf.base.repo.repository.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
///**
// * mf.base.repo.repository.impl
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年04月28日 17:09
// */
//@Service
//public class UserRepositoryImpl implements UserRepository {
//    @Autowired
//    private UserMapper userMapper;
//
//    @Override
//    public User selectById(Long userId){
//        return this.userMapper.selectById(userId);
//    }
//}
