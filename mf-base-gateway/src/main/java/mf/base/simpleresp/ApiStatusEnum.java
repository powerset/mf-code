package mf.base.simpleresp;

/**
 * @Description 响应枚举数据
 */
public enum ApiStatusEnum {

    /**
     * 成功
     */
    SUCCESS(0, "success"),
    ERROR_SYSTEM(-1, "error"),
    ERROR_TOKEN(-2, "token error"),
    ERROR_SIGN(-3, "sign error"),
    ERROR_PERMISSION(-4, "permission error"),
    /**
     * 以上系统占用请避让
     * ----------------------------------------------------
     *
     * ----------------------------------------------------
     * 以下业务异常
     */

    /**
     * 具体业务异常返回
     */
    ERROR_BUS_NO1(1, "业务1判断异常"),
    ERROR_BUS_NO2(2, "业务2判断异常"),
    ERROR_BUS_NO3(3, "业务3判断异常"),
    ERROR_BUS_NO4(4, "业务4判断异常"),
    ERROR_BUS_NO5(5, "业务5判断异常"),
    ERROR_BUS_NO6(6, "创建失败,已经创建过此二维码"),
    ERROR_BUS_NO7(7, "必填信息不合法或未填写"),
    ERROR_BUS_NO8(8, "商户没有进行中的任务或活动"),
    ERROR_BUS_NO9(9, "未查到此活动"),
    ERROR_BUS_NO10(10, "点击太快，请稍后再试"),
    ERROR_BUS_NO11(11, "未查到此用户"),
    ERROR_BUS_NO12(12, "您已参与了此活动，赶快邀请好友增加中奖概率吧"),
    ERROR_BUS_NO13(13, "活动未开始，请稍后再来"),
    ERROR_BUS_NO14(14, "活动已结束，请参加其他活动"),
    ERROR_BUS_NO15(15, "该邀请人未参与此活动，您可以通过自己参加此活动"),
    ERROR_BUS_NO16(16, "该订单编号非本店订单"),
    ERROR_BUS_NO17(17, "该订单已参与活动，请勿重复提交"),
    ERROR_BUS_NO18(18, "审核中，请勿重复提交"),
    ERROR_BUS_NO19(19, "请先审核截图，再提交订单号"),
    ERROR_BUS_NO20(20, "该订单编号非商品订单"),
    ERROR_BUS_NO21(21, "不可参加自己发起的活动"),
    ERROR_BUS_NO22(22, "您的抽奖次数已用完，可以邀请好友获取更多机会"),
    ERROR_BUS_NO23(23, "请提交3月之内交易的订单"),
    ERROR_BUS_NO24(24, "店铺不合法"),
    ERROR_BUS_NO25(25, "商户不合法"),
    ERROR_BUS_NO26(26, "用户不合法"),
    // 2000+ 商户后台 业务异常处理信息
    ERROR_BUS_NO2001(2001,"不是 对进行中活动的修改操作"),

    ERROR_BUS_NO727(727, "活动不合法"),
    ERROR_BUS_NO728(728, "活动定义不合法"),

    ERROR_BUS_NO7000(7000, "每天参与次数受限"),
    ERROR_BUS_NO7001(7001, "先分享才可以参与"),
    ERROR_BUS_NO7002(7002, "今天还剩一次参与机会"),
    ERROR_BUS_NO7100(7100, "日期不合法"),
    /**
     * ------------------------
     * 业务错误码
     */

    ERROR_DB(900, "数据库异常"),
    ERROR(1000, "保存数据异常！"),
    ERROR_WXPAY(1001, "调用微信支付接口异常！"),
    ERROR_WITHDRAW(1002, "提现码错误"),
    ERROR_PARAM_NOT_FIND(1003, "参数缺失"),
    ERROR_PARAM_NOT_VALID(1004, "参数异常"),
    ERROR_PARAM_MERCHANT(1005, "商户ID错误"),
    ERROR_PARAM_LENGTH(1006, "字符长度过长"),
    ERROR_INTERNET_CONNECTION(1007, "网络延迟，连接失败");

    private int code;
    private String message;

    ApiStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
