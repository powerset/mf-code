package mf.code.one.vo;

import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.user.repo.vo
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 11:01
 */
public class UserCouponAboutVO {

    /***
     * 新手
     * @return
     */
    public static List<Integer> addNewbieTaskType() {
        List<Integer> list = new ArrayList<>();
        list.add(UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode());
        return list;
    }

    /***
     * 店长
     * @return
     */
    public static List<Integer> addShopManagerTaskType() {
        List<Integer> list = new ArrayList<>();
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        return list;
    }
}
