package mf.code.one.redis;


/**
 * 功能描述:
 *
 * @param: redis键
 * @auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @date: 2019/6/19 0019 13:44
 */
public class RedisKeyConstant {

    /**
     * 用户完成任务时，存储任务类型 并且记录完成任务的次数,用于提现成功后的弹窗-弹完既删除
     * ZSET
     * key: jkmf:one:finishAllTypeTask:userId:
     * value: upay_wx_order.biz_type
     * score: 完成次数
     */
    public static String FINISH_DONE_TASK_ZSET = "jkmf:one:finishTask:userId:";

    /***
     * 新手任务提交的防重处理
     */
    public static String NEWBIETASK_COMMIT_REPEAT = "jkmf:one:newbiwTask:repeat:activityDefId:";

    /**
     * 用户全额报销助力资格
     */
    public static String ACTIVITY_ASSISTABLE_KEY = "jkmf:one:activity:assistable:"; //<uid>
}
