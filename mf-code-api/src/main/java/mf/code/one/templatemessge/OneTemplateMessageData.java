package mf.code.one.templatemessge;

import mf.code.common.WeixinMpConstants;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.one.templatemessge
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:09
 */
public class OneTemplateMessageData {
    /***
     * 报销活动-前几人直接返现
     * @param merchantId
     * @param shopId
     * @param amount 助力金额
     * @param remainAmount 剩余需要助力的金额
     * @param nickName 助力人
     * @return
     */
    public static Map<String, Object> fullReimbursementDirectCash(Long merchantId, Long shopId,
                                                                  BigDecimal amount,
                                                                  BigDecimal remainAmount,
                                                                  String nickName,
                                                                  Long activityId) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{amount + "元", nickName + "帮你助力的报销款",
                "收益已到账钱包，快去提现吧！你还有" + remainAmount + "元待报销，继续加油哦！"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 31, activityId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }

    /***
     * 报销活动-完成返现
     * @param merchantId
     * @param shopId
     * @param amount 助力金额
     * @return
     */
    public static Map<String, Object> fullReimbursementSuccess(Long merchantId, Long shopId,
                                                               BigDecimal amount,
                                                               Long activityId) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{amount + "元", "成功完成订单报销活动",
                "收益已到账钱包，快去提现吧！"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 31, activityId));
        data.put("data", dataInfo);
//        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }


    /***
     * 报销活动-快过期
     * @param merchantId
     * @param shopId
     * @param neddAssistNum 还需助力的人数
     * @param activityId
     * @return
     */
    public static Map<String, Object> fullReimbursementWillExpire(Long merchantId, Long shopId,
                                                                  int neddAssistNum,
                                                                  Long activityId) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{"活动即将过期", "你的报销活动还剩1小时就过期啦，快去邀请好友帮你助力报销!",
                "再邀请" + neddAssistNum + "个好友助力就能完成啦！加油哦！"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 31, activityId));
        data.put("data", dataInfo);
        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }


    /***
     * 0元抽免单-开奖推送
     *
     * @param merchantId
     * @param shopId
     * @param activityId
     * @return
     */
    public static Map<String, Object> freeOrderBack(Long merchantId, Long shopId,
                                                    Long activityId) {
        Map<String, Object> dataInfo = new HashMap<>();
        Object[] objects = new Object[]{"开奖结果公布", "《抽奖锦鲤》活动", "请及时查看结果，以免错过领奖"};
        dataInfo = getTempteDate(objects.length, objects);

        Map<String, Object> data = new HashMap<>();
        data.put("page", WeixinMpConstants.PAGE + "?" + getPageUrl(merchantId, shopId, 33, activityId));
        data.put("data", dataInfo);
        data.put("emphasis_keyword", "keyword1.DATA");
        return data;
    }


    /********************************私有方法******************************************/
    private static Map<String, Object> getTempteDate(int keywordNum, Object[] objects) {
        Map<String, Object> m = new HashMap<>();
        for (int i = 0; i < keywordNum; i++) {
            Map map = new HashMap();
            String key = "keyword" + (i + 1);
            String value = objects[i].toString();
            map.put("value", value);
            m.put(key, map);
        }
        return m;
    }

    private static String getPageUrl(Long merchantId,
                                     Long shopId,
                                     int from,
                                     Long activityId) {
        //source--推送消息来源
        String result = "source=1&merchantId=" + String.valueOf(merchantId) +
                "&shopId=" + String.valueOf(shopId) +
                "&from=" + String.valueOf(from) +
                "&activityId=" + String.valueOf(activityId);
        return result;
    }
}
