package mf.code.one.dto;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.one.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月25日 15:41
 */
@Data
public class UserOrderReqDTO {

    /***
     * 商户编号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;
    /***
     * 用户编号
     */
    private Long userId;
    /***
     * 业务类型
     */
    private String bizType;
    /***
     * 业务值
     */
    private String bizvalue;
    /***
     * 支付类型
     */
    private int payType;

    /***
     * 金额
     */
    private String amount;

    public boolean check() {
        if (this.merchantId == null || this.merchantId == 0
                || this.shopId == null || this.shopId == 0
                || this.userId == null || this.userId == 0
                || StringUtils.isBlank(this.bizType)
                || StringUtils.isBlank(amount)
                || this.payType == 0) {
            return false;
        }
        return true;
    }
}
