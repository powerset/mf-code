package mf.code.one.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.one.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-19 19:42
 */
@Data
public class ActivityDefDTO {

	/**
	 * 活动id
	 */
	private Long id;

	/**
	 * 商户id
	 */
	private Long merchantId;

	/**
	 * 定义活动的父级编号
	 */
	private Long parentId;

	/**
	 * 商户店铺id
	 */
	private Long shopId;

	/**
	 * 活动类型
	 1:计划内活动 (废弃)
	 2:用户订单活动=免单抽奖 v1
	 3:新人有礼活动 (废弃)
	 4:新人大转盘
	 5:回填订单红包活动 v1
	 6:助力活动=7:赠品活动
	 7:赠品活动=6:助力活动
	 8:红包活动（回填订单，收藏加购，好评晒图)  (废弃)
	 9:拆红包
	 10好评晒图 v1
	 11收藏加购 v1
	 12免单抽奖活动 v2
	 13赠品活动 v2
	 14,回填订单红包任务 v2
	 15,好评晒图 v2
	 16,收藏加购 v2
	 */
	private Integer type;

	/**
	 * 活动状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束 5已结算
	 */
	private Integer status;

	/**
	 * 活动标题
	 */
	private String title;

	/**
	 * 活动描述
	 */
	private String desc;

	/**
	 * 开始时间
	 */
	private Date startTime;

	/**
	 * 活动结束时间
	 */
	private Date endTime;

	/**
	 * Json格式，商品id列表[1,2]
	 */
	private String goodsIds;

	/**
	 * 商品id
	 */
	private Long goodsId;

	/**
	 * 商品价格
	 */
	private BigDecimal goodsPrice;

	/**
	 * 扣库存方式：1提前占用库存法 2先到先得减库存法
	 */
	private Integer stockType;

	/**
	 * 库存定义数量
	 */
	private Integer stockDef;

	/**
	 * 库存数量
	 */
	private Integer stock;

	/**
	 * 保证金定义额度
	 */
	private BigDecimal depositDef;

	/**
	 * 保证金额度
	 */
	private BigDecimal deposit;

	/**
	 * 保证金支付状态（-1已失效，0待付款，1已付款）
	 */
	private Integer depositStatus;

	/**
	 * 保证金支付凭证地址
	 */
	private String voucher;

	/**
	 *
	 */
	private String keyWord;

	/**
	 * 同一活动定义单个用户可发起的总次数
	 */
	private Integer startNum;

	/**
	 * 发布时间
	 */
	private Date publishTime;

	/**
	 * 活动时间（从发起抽奖到抽奖结束的时间段） 单位 分钟
	 */
	private Integer condDrawTime;

	/**
	 * 开奖条件-满足人数开奖 单位 人数
	 */
	private Integer condPersionCount;

	/**
	 * 每个计划内活动中奖人数
	 */
	private Integer hitsPerDraw;

	/**
	 * 成长任务单红包金额
	 */
	private BigDecimal taskRpAmount;

	/**
	 * 成长任务红包定义个数
	 */
	private Integer taskRpNumDef;

	/**
	 * 成长任务红包个数
	 */
	private Integer taskRpNum;

	/**
	 * 成长任务红包定义保证金
	 */
	private BigDecimal taskRpDepositDef;

	/**
	 * 成长任务红包保证金
	 */
	private BigDecimal taskRpDeposit;

	/**
	 * 总保证金额度（活动保证金和任务保证金和）
	 */
	private BigDecimal totalDeposit;

	/**
	 * 中奖任务-需要完成时间 单位 分钟
	 */
	private Integer missionNeedTime;

	/**
	 * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 优惠券id
	 */
	private String merchantCouponJson;

	/**
	 * 本活动不需要用户提交领奖任务截图: 0不提交, 1需提交
	 */
	private Integer weighting;

	/**
	 * 提交申请时间
	 */
	private Date applyTime;

	/**
	 * 审核时间
	 */
	private Date auditingTime;

	/**
	 * 审核不通过原因
	 */
	private String auditingReason;

	/**
	 * 删除标记 . 0:正常 1:删除
	 */
	private Integer del;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 修改时间
	 */
	private Date utime;

	/**
	 * 客服微信号
	 */
	private String serviceWx;

	/**
	 * 发起活动支付金额
	 */
	private BigDecimal jeton;

	/**
	 * 额外任务佣金（免单抽奖和免单赠品额外使用）
	 */
	private BigDecimal commission;

	/**
	 *
	 */
	private String amountJson;

	/**
	 * 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
	 */
	private Integer finishType;

	/**
	 * 审核方式（1:人工审核 2:自动审核）
	 */
	private Integer auditType;
}
