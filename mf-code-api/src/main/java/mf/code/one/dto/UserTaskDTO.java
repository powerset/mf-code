package mf.code.one.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.one.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-18 18:59
 */
@Data
public class UserTaskDTO {

	/**
	 * 用户中奖任务记录的主键
	 */
	private Long id;

	/**
	 * 活动类型 0:幸运大转盘1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包(包裹)7:红包任务-收藏加购 8助力活动发起者 9:好评返现 11:拆红包
	 */
	private Integer type;

	/**
	 * 领取任务的用户ID
	 */
	private Long userId;

	/**
	 * 商户ID,merchant表主键
	 */
	private Long merchantId;

	/**
	 * 商户店铺ID
	 */
	private Long shopId;

	/**
	 * 活动定义id
	 */
	private Long activityDefId;

	/**
	 * 活动ID, activity表的主键
	 */
	private Long activityId;

	/**
	 * 商品id
	 */
	private Long goodsId;

	/**
	 * 关键词
	 */
	private String keyWords;

	/**
	 * 中奖任务ID, activity_task主键
	 */
	private Long activityTaskId;

	/**
	 * user_activity_id表id
	 */
	private Long userActivityId;

	/**
	 * 用户奖品ID, user_coupon表的主键
	 */
	private Long userCouponId;

	/**
	 * 红包金额（元）
	 */
	private BigDecimal rpAmount;

	/**
	 * 订单号
	 */
	private String orderId;

	/**
	 * 截图地址-截图任务使用（json字符串）
	 */
	private String pics;

	/**
	 * 任务开始执行时间
	 */
	private Date startTime;

	/**
	 * 任务提交时间
	 */
	private Date receiveTime;

	/**
	 * 用户任务 审核时间
	 */
	private Date auditingTime;

	/**
	 * 用户任务 状态(-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功)
	 */
	private Integer status;

	/**
	 * 审核原因，[{"index":0,"reason":"一"},{"index":1, "reason":""},{"index":2, "reason":"三"}]，这里说明是第二张图没有审核原因
	 */
	private String auditingReason;

	/**
	 * 用户是否已读 0:未读，1:已读
	 */
	private Integer read;

	/**
	 * 库存标识，0:未退 1:已退
	 */
	private Integer stockFlag;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 更新时间
	 */
	private Date utime;

	/**
	 * 用户提交商品url
	 */
	private String taskGoodsUrl;
}
