package mf.code.one.dto;

import lombok.Data;

/**
 * mf.code.one.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-25 20:24
 */
@Data
public class ShopManagerCodeDTO {
    private Long userId;
    private String groupCodePath;
}
