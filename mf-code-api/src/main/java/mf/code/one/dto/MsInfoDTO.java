package mf.code.one.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.one.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月16日 19:45
 */
@Data
public class MsInfoDTO {
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 秒杀批次开始时间 毫秒时间戳
     */
    private String time;
    /**
     * 秒杀批次结束时间 毫秒时间戳
     */
    private String endTime;

    /**
     * skuid,价格,秒杀库存，秒初始量
     */
    private List<String> skus;
}
