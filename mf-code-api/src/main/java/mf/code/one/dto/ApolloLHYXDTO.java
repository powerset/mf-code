package mf.code.one.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.one.dto
 * Description: 联合营销部分会场配置，好无奈呀还得分开写
 *
 * @author gel
 * @date 2019-07-16 18:06
 */
@Data
public class ApolloLHYXDTO {

    private Integer status;
    private Long time;
    private Long count;
    private BigDecimal money;
    private List<Banner> banner;
    private String hotIds;
    private String group1Ids;
    private String group2Ids;
    private List<Recommend> recommend;
    private List<String> times;
    @Data
    public static class Banner {
        private String url;
        private Long gid;
    }

    @Data
    public static class Recommend {
        private Long gid;
    }
}
