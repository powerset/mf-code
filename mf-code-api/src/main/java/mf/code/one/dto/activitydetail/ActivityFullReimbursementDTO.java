package mf.code.one.dto.activitydetail;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mf.code.common.dto.AppletMybatisPageDto;

/**
 * mf.code.one.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月04日 15:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ActivityFullReimbursementDTO extends ActivityModeDTO {
    /***
     * 累计报销
     */
    private String reimburseAmountTotal;
    /***
     * 待报销总额度
     */
    private String toBeReimburse;
    /***
     * 待报销总额度 中的已报销部分
     */
    private String reimbursed;
    /***
     * 待报销总额度 中进入钱包的部分
     */
    private String moneyInWallet;
    /***
     * 帮拆金额
     */
    private String helpAmount;
    /***
     * 1 邀请好友助力 2帮好友助力 3 已帮好友助力，参加报销 4:作为老用户，参与报销 5：助力满额
     */
    private int status;

    /***
     * 用户的助力情况
     */
    private AppletMybatisPageDto<AssertUserDto> list;


    /***
     * 活动报销额度的最小值
     */
    private String min;
    /***
     * 活动报销的最大值
     */
    private String max;
}
