package mf.code.one.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GrantCouponDTO implements Serializable {
    /**
     * 用户id列表
     */
    private List<Long> userIds;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 活动定义id
     */
    private Long activityDefId;

    /**
     * 优惠券id
     */
    private Long couponId;
}
