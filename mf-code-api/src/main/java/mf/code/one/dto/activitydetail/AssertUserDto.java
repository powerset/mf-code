package mf.code.one.dto.activitydetail;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.one.dto.activitydetail
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月04日 16:19
 */
@Data
public class AssertUserDto {
    /***
     * 昵称
     */
    private String nickName;
    /***
     * 用户头像
     */
    private String avatarUrl;
    /***
     * 日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date date;
    /***
     * 金额
     */
    private String amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
}
