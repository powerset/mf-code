package mf.code.one.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CouponInfoDTO {

    /**
     * 用户优惠券id
     */
    private Long UserCouponId;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠券定义id
     */
    private Long couponId;

    /**
     * 门槛金额
     */
    private BigDecimal limitAmount;

    /**
     * 优惠券金额
     */
    private BigDecimal amount;

    /**
     * 开始时间
     */
    private Date displayStartTime;

    /**
     * 结束时间
     */
    private Date displayEndTime;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 商品主图
     */
    private String mainPic;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 优惠券状态
     */
    private Integer status;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

}
