package mf.code.one.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * merchant_shop_coupon
 * 商家优惠券表
 */
@Data
public class SellerShopCouponDTO {
    /**
     * 优惠券id
     */
    private Long id;
    /**
     * 优惠券id
     */
    private Long couponId;
    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 优惠券名称
     */
    private String name;

    /**
     * 优惠券描述
     */
    private String desc;

    /**
     * 优惠券金额
     */
    private BigDecimal amount;

    /**
     * 优惠券展示时间，开始时间
     */
    private String displayStartTime;

    /**
     * 优惠券展示时间，结束时间
     */
    private String displayEndTime;

    /**
     * 使用门槛
     */
    private BigDecimal limitAmount;

    /**
     * 优惠券类型：1淘宝优惠券，2商品优惠券
     */
    private Integer type;

    /**
     * 发行总量
     */
    private Integer stockDef;

    /**
     * 已发发行量
     */
    private Integer stock;

    /**
     * 每人限领取张数
     */
    private Integer limitPerNum;

    /**
     * 优惠券状态: 1:发行中，2已结束
     */
    private Integer couponStatus;

    /**
     * 商品ID
     */
    private Long productId;

}
