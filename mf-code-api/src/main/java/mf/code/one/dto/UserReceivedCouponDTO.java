package mf.code.one.dto;

import lombok.Data;

import java.util.Date;

/**
 * 优惠券的领取和使用情况信息
 * author:yunshan
 */
@Data
public class UserReceivedCouponDTO {
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 用户图像
     */
    private String userPic;
    /**
     * 领取时间
     */
    private Date ctime;
    /**
     * 领取张数
     */
    private int receiveNum;
    /**
     * 使用状态：0：未领奖，1：已领奖，2已使用 3已过期
     */
    private int status;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 优惠券id
     */
    private Long couponId;
}
