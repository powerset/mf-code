package mf.code.one.dto;

import lombok.Data;

/**
 * mf.code.api.applet.dto
 * Description:
 *
 * @author: gel
 * @date: 2019-02-20 15:47
 */
@Data
public class CommonDictReqDTO {
    private Long id;
    private String type;
    private String key;
    /***
     * 业务字符串
     */
    private String value;
    /***
     * 用户编号
     */
    private String value1;
}
