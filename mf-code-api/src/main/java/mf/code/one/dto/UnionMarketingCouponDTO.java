package mf.code.one.dto;

import lombok.Data;

import java.util.List;

/**
 * 优惠券响应bean
 * author：yunshan
 */
@Data
public class UnionMarketingCouponDTO {
    /**
     * 总数
     */
    private Integer totalCount;
    /**
     * 当前页
     */
    private Long page;
    /**
     * 每页条数
     */
    private Long size;

    /**
     * 每页条数
     */
    private Integer  limit ;

    /**
     * 偏移量
     */
    private Integer offset;

    /**
     * 是否可下拉
     */
    private Boolean isPullDown;
    /**
     * 已使用张数
     */
    private Integer usedCount;

    /**
     * 已使用张数
     */
    private Integer receiveCount;

    /**
     * 优惠券集合
     */
    private List<SellerMerchantShopCouponDTO> couponList;

    private List<UserReceivedCouponDTO> userCouponInfoList;

    private List<UserCouponDTO> userCouponList;

    private List<MerchantShopCouponDTO> mList;

    /**
     * 小程序查看优惠券-可以使用张数
     */
    private Integer canUseCount;
    /**
     * 小程序查看优惠券-不可使用张数
     */
    private Integer notUseCount;
    /**
     * 小程序查看优惠券
     */
    private List<CouponInfoDTO> couponInfoDTOList;


}
