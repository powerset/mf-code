package mf.code.one.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.one.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-18 18:59
 */
@Data
public class UserCouponDTO {

	/**
	 * 用户奖品记录表ID主键
	 */
	private Long id;

	/**
	 * 店铺id
	 */
	private Long shopId;

	/**
	 * 商户id
	 */
	private Long merchantId;

	/**
	 * 用户id，user表的主键
	 */
	private Long userId;

	/**
	 * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6拆红包
	 */
	private Integer type;

	/**
	 * 活动ID, activity表的主键
	 */
	private Long activityId;

	/**
	 * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
	 */
	private Long activityDefId;

	/**
	 * activity_task表id
	 */
	private Long activityTaskId;

	/**
	 * 奖金（元）
	 */
	private BigDecimal amount;

	/**
	 * 活动定义佣金和当时税率，{"commission_def":10, "rate": 0.1}
	 */
	private String commissionDef;

	/**
	 * 所得的额外任务佣金
	 */
	private BigDecimal commission;

	/**
	 * 获奖时间
	 */
	private Date awardTime;

	/**
	 * 领奖状态(0：未领奖，1：已领奖)
	 */
	private Integer status;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 更新时间
	 */
	private Date utime;
}
