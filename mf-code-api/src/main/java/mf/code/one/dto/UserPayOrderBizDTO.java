package mf.code.one.dto;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.one.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月29日 09:08
 */
@Data
public class UserPayOrderBizDTO {
    private String remark;

    private Long bizValue;


    public boolean check() {
        if (StringUtils.isNotBlank(this.remark) || (this.bizValue != null && this.bizValue > 0)) {
            return true;
        }
        return false;
    }
}
