package mf.code.one.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MerchantShopCouponDTO {
    /**
     * 商品店铺优惠券id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 优惠券名称
     */
    private String name;

    /**
     * 优惠券描述
     */
    private String desc;

    /**
     * 优惠券金额
     */
    private BigDecimal amount;

    /**
     * 优惠券限制金额
     */
    private BigDecimal limitAmount;

    /**
     * 优惠券当前库存
     */
    private Integer stock;

    /**
     * 优惠券库存
     */
    private Integer stockDef;

    /**
     * 已发放数量 = 定义库存 - 当前库存
     */
    private Integer receiveNum;

    /**
     * 优惠券类型
     */
    private Integer type;

    /**
     * 每人限领张数
     */
    private Integer limitPerNum;

    /**
     * 优惠券展示时间，开始时间
     */
    private Date displayStartTime;

    /**
     * 优惠券展示时间，结束时间
     */
    private Date displayEndTime;

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 优惠券状态
     */
    private Integer couponStatus;

    /**
     * 关联商品id
     */
    private Long productId;
}
