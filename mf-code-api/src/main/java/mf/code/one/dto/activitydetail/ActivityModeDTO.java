package mf.code.one.dto.activitydetail;

import lombok.Data;

import java.util.List;


/**
 * mf.code.one.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月04日 14:23
 */
@Data
public class ActivityModeDTO {
    /***
     * 活动定义编号
     */
    private Long activityDefId;
    /***
     * 活动编号
     */
    private Long activityId;
    /***
     * 库存
     */
    private int stock;
    /***
     * 用户昵称
     */
    private String nickName;
    /***
     * 用户头像
     */
    private String avatarUrl;
    /***
     * 弹窗信息
     */
    private List<Dialog> dialog;
    /***
     * 剩余时间
     */
    private Long remainingTime;


    @Data
    public static class Dialog {
        private int type;
        private String nickName;
        private String avatarUrl;
        private String amount;
    }
}
