package mf.code.one.dto;

import lombok.Data;

@Data
public class SellerMerchantShopCouponDTO {

    private MerchantShopCouponDTO merchantShopCoupon;

    /**
     *人群范围 :1全部
     */
    private Integer userScope;

    /**
     *商品范围
     */
    private Integer goodsScope;

}
