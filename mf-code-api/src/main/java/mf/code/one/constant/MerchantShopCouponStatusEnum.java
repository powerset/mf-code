package mf.code.one.constant;

/**
 * mf.code.one.constant
 * Description: 优惠券状态: 1:发行中，2已结束
 *
 * @author gel
 * @date 2019-07-18 10:18
 */
public enum MerchantShopCouponStatusEnum {
    /**
     * 0:创建中
     */
    INIT(0, "创建中"),
    /**
     * 1:发行中
     */
    PUBLISH(1, "发行中"),
    /**
     * 2下线
     */
    UNPUBLISH(2, "下线"),
    /**
     * 3过期
     */
    EXPIRE(3, "过期"),
    /**
     * -1强制下线
     */
    BAN(-1, "强制下线"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    MerchantShopCouponStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static MerchantShopCouponStatusEnum findByCode(int code) {
        for (MerchantShopCouponStatusEnum statusEnum : MerchantShopCouponStatusEnum.values()) {
            if (statusEnum.getCode() == code) {
                return statusEnum;
            }
        }
        return null;
    }
}
