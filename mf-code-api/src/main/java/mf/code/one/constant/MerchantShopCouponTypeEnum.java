package mf.code.one.constant;

/**
 * mf.code.one.constant
 * Description: 优惠券类型：1淘宝优惠券，2商品优惠券
 *
 * @author gel
 * @date 2019-07-18 10:05
 */
public enum MerchantShopCouponTypeEnum {
    /***
     * 1淘宝优惠券
     */
    TAOBAO(1, "淘宝优惠券"),
    /***
     * 商品优惠券
     */
    GOODS(2, "商品优惠券"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    MerchantShopCouponTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static MerchantShopCouponTypeEnum findByCode(int code) {
        for (MerchantShopCouponTypeEnum typeEnum : MerchantShopCouponTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
