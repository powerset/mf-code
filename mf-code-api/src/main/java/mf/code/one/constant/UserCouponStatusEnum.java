package mf.code.one.constant;

/**
 * mf.code.common.constant
 * Description: 0：未领奖，1：已领奖，2已使用 3已过期
 *
 * @author: 百川
 * @date: 2018-11-16 下午8:29
 */
public enum UserCouponStatusEnum {
    /**
     * 0 未领奖
     */
    UNRECEIVED(0, "未领奖"),
    /**
     * 已领奖
     */
    RECEIVIED(1, "已领奖"),
    /**
     * 2已使用
     */
    USED(2, "已使用"),
    /**
     * 3已过期
     */
    EXPIRE(3, "已过期"),
    ;
    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String message;

    UserCouponStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static UserCouponStatusEnum findByCode(Integer code) {
        for (UserCouponStatusEnum statusEnum : UserCouponStatusEnum.values()) {
            if (statusEnum.getCode() == code) {
                return statusEnum;
            }
        }
        return null;
    }
}
