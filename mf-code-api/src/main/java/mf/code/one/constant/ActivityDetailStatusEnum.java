package mf.code.one.constant;

/**
 * mf.code.one.constant
 *
 * @description: 活动的状态-前端交互的展现
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月17日 09:27
 */
public enum ActivityDetailStatusEnum {
    //1 邀请好友助力 2帮好友助力 3 已帮好友助力，参加报销 4:作为老用户，参与报销 5：助力满额
    /***
     * 邀请好友助力
     */
    SELF_INVITE(1, "本人需邀请助力"),
    /***
     * 帮好友助力
     */
    HELP_FRIEND(2, "帮好友助力"),
    /***
     * 作为老用户，参与
     */
    OLD_USER_CREATE(3, "作为老用户，参与"),
    /***
     * 已帮好友助力，自己参与
     */
    HELPED_FRIEND_CREATE(4, "已帮好友助力，自己参与"),
    /***
     * 已开奖
     */
    AWARDED(5, "已开奖"),
    /***
     * 已过期
     */
    EXPIRED(6, "已过期"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityDetailStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ActivityDetailStatusEnum findByCode(int code) {
        for (ActivityDetailStatusEnum typeEnum : ActivityDetailStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
