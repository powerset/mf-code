package mf.code.one.constant;

/**
 * mf.code.one.constant
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月11日 08:47
 */
public enum ActivityApiEntrySceneEnum {
    //1.创建入口 2.助力入口 3.查询入口
    /***
     * 创建入口
     */
    CREATE(1, "创建入口"),
    /***
     * 助力入口
     */
    ASSIST(2, "助力入口"),
    /***
     * 查询入口
     */
    QUERY(3, "查询入口"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityApiEntrySceneEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ActivityApiEntrySceneEnum findByCode(int code) {
        for (ActivityApiEntrySceneEnum typeEnum : ActivityApiEntrySceneEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
