package mf.code.one.constant;

/**
 * mf.code.one.constant
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月17日 09:27
 */
public enum CheckpointTaskSpaceTypeEnum {
    /***
     * 新手任务--3个宝箱种类
     */
    NEWBIE_TASK(1, "新手任务"),
    /***
     * 店长任务
     */
    SHOP_MANAGER_TASK(2, "店长任务"),
    /***
     * 日常任务
     */
    DAILY_TASK(3, "日常任务"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    CheckpointTaskSpaceTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static CheckpointTaskSpaceTypeEnum findByCode(int code) {
        for (CheckpointTaskSpaceTypeEnum typeEnum : CheckpointTaskSpaceTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
