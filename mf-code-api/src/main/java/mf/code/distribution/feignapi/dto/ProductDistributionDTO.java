package mf.code.distribution.feignapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDistributionDTO {
    /**
     * 用户id
     */
    private Long userId;
    /***
     * 商品编号 或 匹配主键
     */
    private Long productId;
    /***
     * 供货商的商品编号
     */
    private Long parentId;
    /**
     * 商户id
     */
    private Long merchantId;
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 状态
     */
    private Integer status;

    /***
     * 自营分销比例
     */
    private BigDecimal selfSupportRatio;
    /***
     * 平台抽佣比例
     */
    private BigDecimal platSupportRatio;
    /***
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 第三方价格
     */
    private BigDecimal thirdPrice;

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 商品主图
     */
    private String mainPics;
    /**
     * 销量
     */
    private String salesVolume;

    /**
     * 商品类目
     */
    private Long productSpecs;

    /**
     * 上架时间
     */
    private Date putawayTime;

    /**
     * -------分销加工----
     */
    private BigDecimal rebate;

    /***
     * 库存
     */
    private Integer stock;
    /**
     * 分销佣金
     */
    private BigDecimal commission;
    /***
     * 总库存
     */
    private Integer stockDef;

    /***
     * 初始销量
     */
    private Integer initSale;

    /**
     * 秒杀skuId，秒杀专用
     */
    private Long skuId;

    /**
     * 分销佣金
     */
    private BigDecimal commissionNum;


    /******是否上架在该店铺**true:在 false:不在*****/
    private boolean onShop;
}
