package mf.code.distribution.feignapi.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.distribution.feignclient.dto
 * Description:
 *
 * @author gel
 * @date 2019-07-08 16:44
 */
@Data
public class UserIdListDTO {
    private List<Long> userIdList;
}
