package mf.code.distribution.feignapi.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.feignapi.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月17日 17:55
 */
@Data
public class ActivityProductDTO extends ProductDistributionDTO {

    /***
     * 展示活动类型
     */
    private int showType;
    /***
     * 截止时间
     */
    private Long cutoffTime;
    /***
     * 商品详细图片
     */
    private List<String> picDetail;

    /**
     * 商品主图
     */
    private List<String> goodPics;

    /***
     * 商品类型
     */
    private int goodsType;

    /***
     * 商品属性
     */
    private List<Map> skus;
}
