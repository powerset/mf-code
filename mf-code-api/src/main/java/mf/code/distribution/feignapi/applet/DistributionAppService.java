package mf.code.distribution.feignapi.applet;//package mf.code.distribution.feignclient.applet;
//
//import mf.code.common.simpleresp.SimpleResponse;
//import mf.code.distribution.feignclient.dto.DistributionDTO;
//import mf.code.goods.dto.RebatePolicyDTO;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import java.util.List;
//import java.util.Map;
//
///**
// * mf.code.distribution.feignclient.applet
// * Description:
// *
// * @author: 百川
// * @date: 2019-04-08 19:11
// */
//@FeignClient(value = "mf-code-distribution")
//public interface DistributionAppService {
//
//	/**
//	 * 获取 用户返利金额
//	 * @param productDTO
//	 * @return
//	 */
//	@PostMapping("/feignclient/distribution/applet/v5/getRebateCommission")
//	String getRebateCommission(@RequestBody DistributionDTO productDTO);
//
//	/**
//	 * 通过 用户本月消费 获取返利政策
//	 *
//	 * @param spendingAmountTomonth
//	 * @return
//	 */
//	@GetMapping("/feignclient/distribution/applet/v5/getRebatePolicy")
//	RebatePolicyDTO getRebatePolicy(@RequestParam("spendingAmountTomonth") String spendingAmountTomonth);
//
//	/**
//	 * 获取 消费达标金额
//	 * @return
//	 */
//	@GetMapping("/feignclient/distribution/applet/v5/getCommissionStandard")
//	String getCommissionStandard();
//
//	/**
//	 * 支付成功 处理用户返利和分佣
//	 * @param userId
//	 * @param shopId
//	 * @param orderId
//	 * @return
//	 */
//	@GetMapping("/feignclient/distribution/applet/v5/distributionCommission")
//	SimpleResponse distributionCommission(@RequestParam("userId") Long userId,
//										  @RequestParam("shopId") Long shopId,
//										  @RequestParam("orderId") Long orderId);
//
//	/**
//	 * 获取用户商品返利
//	 * @param distributionDTOLists
//	 * @return
//	 */
//	@GetMapping("/feignclient/distribution/applet/v5/queryUserRebateByProduct")
//	Map<Long, String> queryUserRebateByProduct(@RequestParam("distributionDTOLists") List<String> distributionDTOLists);
//}
