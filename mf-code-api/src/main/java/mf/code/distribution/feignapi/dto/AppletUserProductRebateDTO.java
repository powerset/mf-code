package mf.code.distribution.feignapi.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.distribution.feignclient.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-09 20:25
 */
@Data
public class AppletUserProductRebateDTO {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 偏移量
	 */
	private Long offset;
	/**
	 * 大小限制
	 */
	private Long size;
	/**
	 * 总条数
	 */
	private Integer total;
	/**
	 * 商品列表
	 */
	private List<ProductDistributionDTO> productDistributionDTOList;
}
