package mf.code.distribution.feignapi.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品售出，商户所得利润
 * mf.code.distribution.feignclient.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-24 10:14
 */
@Data
public class MerchantOrderIncomeResp {
	private Long shopId;
	/**
	 * 商品售出，商户所得利润
	 */
	private BigDecimal totalFee;
}
