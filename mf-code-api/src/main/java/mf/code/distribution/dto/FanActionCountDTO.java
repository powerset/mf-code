package mf.code.distribution.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.distribution.dto
 * Description:
 *
 * @author gel
 * @date 2019-07-08 15:31
 */
@Data
public class FanActionCountDTO {
    private String _id;
    private Long sid;
    private Long uid;
    private Long count;
    private BigDecimal sum;
}
