package mf.code.distribution.dto;

import lombok.Data;

import java.util.List;

@Data
public class VisitUserInSevenDaysInfoDTO {
    /**
     * 访问者ID
     */
    private Long userId;
    /**
     * 访问间隔时间
     */
    private String intervalTime;
    /**
     * 访问类容
     */
    private List<TeamMangerEventDTO> visitInSevenDays;
}
