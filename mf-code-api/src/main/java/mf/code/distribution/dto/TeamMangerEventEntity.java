package mf.code.distribution.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 聚合查询结果集
 * @author gel
 */
@Data
public class TeamMangerEventEntity {
    /**
     * 7日访问
     */
    private List<VisitUserInSevenDaysInfoDTO> teamList;
    /**
     * 总数
     */
    private long count;
    /**
     * 行为信息
     */
    private List<TeamMangerEventDTO> inSevenDaysList;
    /**
     * 总收入
     */
    private BigDecimal totalIncome;
    /**
     * 是否达标
     */
    private Boolean isReachMark;

    private String userName;

    private String userPic;
}
