package mf.code.distribution.dto;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class TeamMangerEventDTO {
    /**
     * 商品id
     */
    private Long productId;
    /**
     * 商户ID
     */
    private Long merchantId;
    /**
     * 店铺ID
     */
    private Long shopId;
    /**
     * 商品主图
     */
    private String mainPics;
    /**
     * 商品名称
     */
    private String productTitle;
    /**
     * 售价
     */
    private String salsePrice;
    /**
     * 淘宝价
     */
    private String thirdPrice;
    /**
     * 间隔时间
     */
    private String intervalTime;

    /**
     * 销量
     */
    private int salseNum;

    /**
     * 收益
     */
    private BigDecimal income;

    /**
     * 用户图片
     */
    private String userPics;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 是否达标
     */
    private  Boolean isReachMark;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 任务类型
     */
    private String taskType;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 子任务名
     */
    private String subTaskName;

    private String subTaskType;
    /**
     * 粉丝数量
     */
    private int fansNum;

    /**
     * 订单状态 0待付款，1已付款
     */
    private int orderStatus;


}
