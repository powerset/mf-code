package mf.code.distribution.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.distribution.dto
 * Description:
 *
 * @author gel
 * @date 2019-07-05 15:46
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FanActionForMQ {
    /**
     * 用户ID
     */
    private Long uid;
    /**
     * 商户id
     */
    private Long mid;
    /**
     * 店铺id
     */
    private Long sid;
    /**
     * 埋点事件
     *
     * @see mf.code.distribution.constant.FanActionEventEnum
     */
    private String event;
    /**
     * 埋点事件子类型，当前仅用于任务 FanActionEventEnum 中6新手任务、7日常任务、8店长任务。
     * 如有其它需求，也可不依赖于 @SEE 中ActivityDefTypeEnum枚举。可另行设置
     *
     * @see mf.code.activity.constant.ActivityDefTypeEnum
     */
    private String eventType;
    /**
     * 埋点通用id
     */
    private Long eventId;
    /**
     * 涉及金额(订单金额，任务金额)
     */
    private BigDecimal amount;
    /**
     * 给上级的税额
     */
    private BigDecimal tax;
    /**
     * 时间生成时间戳
     */
    private Long current;
    /**
     * 额外字段(存储个性化信息)
     */
    private Map<String, Object> extra;
}



