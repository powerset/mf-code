package mf.code.distribution.dto;/**
 * create by qc on 2019/8/26 0026
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * 分销基础信息实体
 *
 * @author gbf
 * 2019/8/26 0026、14:22
 */
@Data
public class DistributeInfoEntity {

    /**
     * 类目
     */
    private Long productSpec;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 平台分销佣金比例
     */
    public BigDecimal platSupportRadio;
    /**
     * 店铺分销佣金比例
     */
    private BigDecimal selfSupportRadio;
}
