package mf.code.distribution.constant;

import mf.code.activity.constant.UserCouponTypeEnum;

/**
 * mf.code.distribution.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-30 11:52
 */
public enum PlatformOrderBizTypeEnum {
    TRANSACTION(1, "订单成交-商户交易税"),
    SPECS_COMMISSION(2, "订单成交-平台商品类目抽佣"),
    USER_COMMISSION_RECYCLING(3, "订单成交-上级缺失 用户分佣回收"),
    USER_COMMISSION_TAX(4, "订单成交-用户返利分佣税务部分回收"),
    USER_TEAM_REWARD(5, "订单成交-用户返利分佣团队奖励部分回收"),
    USER_TEAM_LUCKY_WHEEL(6, "转盘"),
    USER_TASK_FILLORDER_BACK(7, "回填"),
    USER_TASK_GOODS_COMMENT(8, "好评"),
    USER_TASK_FAV_CART(9, "收藏"),
    USER_TASK_MCH_PLAN(10, "商户计划"),
    USER_TASK_ORDER_BACK(11, "免单"),
    USER_TASK_NEW_MAN_START(12, "新人"),
    USER_TASK_ASSIST(13, "助力"),
    USER_TASK_OPEN_RED_PACKAGE(14, "拆红包"),
    USER_TASK_NEWBIE_TASK_BONUS(15, "加群享福利，扫一扫"),
    USER_TASK_NEWBIE_TASK_REDPACK(16, "查看攻略，拿红包"),
    USER_TASK_SHOP_MANAGER_TASK_ADD_WX(17, "设置店长微信群"),
    USER_TASK_SHOP_MANAGER_TASK_INVITE_FANS(18, "邀请粉丝"),
    USER_TASK_SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER(19, "推荐粉丝成为店长"),
    USER_TASK_NEWBIE_TASK_OPENREDPACK_AMOUNT(20, "新人红包，奖励到"),
    SHOP_MANAGER_PAY(21, "付费升店长"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    PlatformOrderBizTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }


    public static PlatformOrderBizTypeEnum findByUserCouponType(int type) {
        if (UserCouponTypeEnum.MCH_PLAN.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_MCH_PLAN;
        }
        if (UserCouponTypeEnum.LUCKY_WHEEL.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TEAM_LUCKY_WHEEL;
        }
        if (UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode() == type
                || UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_OPEN_RED_PACKAGE;
        }
        if (UserCouponTypeEnum.ORDER_BACK_START.getCode() == type
                || UserCouponTypeEnum.ORDER_BACK_START_V2.getCode() == type
                || UserCouponTypeEnum.ORDER_BACK_JOIN.getCode() == type
                || UserCouponTypeEnum.ORDER_BACK_JOIN_V2.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_ORDER_BACK;
        }
        if (UserCouponTypeEnum.ASSIST.getCode() == type
                || UserCouponTypeEnum.ASSIST_V2.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_ASSIST;
        }
        if (UserCouponTypeEnum.NEW_MAN_START.getCode() == type
                || UserCouponTypeEnum.NEW_MAN_JOIN.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_NEW_MAN_START;
        }
        if (UserCouponTypeEnum.FAV_CART.getCode() == type
                || UserCouponTypeEnum.FAV_CART_V2.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_FAV_CART;
        }
        if (UserCouponTypeEnum.ORDER_RED_PACKET.getCode() == type
                || UserCouponTypeEnum.ORDER_RED_PACKET_V2.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_FILLORDER_BACK;
        }
        if (UserCouponTypeEnum.GOODS_COMMENT.getCode() == type
                || UserCouponTypeEnum.GOODS_COMMENT_V2.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_GOODS_COMMENT;
        }

        if (UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_NEWBIE_TASK_BONUS;
        }
        if (UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_NEWBIE_TASK_REDPACK;
        }
        if (UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_NEWBIE_TASK_OPENREDPACK_AMOUNT;
        }
        if (UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_SHOP_MANAGER_TASK_ADD_WX;
        }
        if (UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_SHOP_MANAGER_TASK_INVITE_FANS;
        }
        if (UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode() == type) {
            return PlatformOrderBizTypeEnum.USER_TASK_SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER;
        }
        // 类型异常
        return null;
    }
}
