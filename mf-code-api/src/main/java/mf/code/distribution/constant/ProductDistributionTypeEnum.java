package mf.code.distribution.constant;

import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * mf.code.goods.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-04 01:54
 */
public enum ProductDistributionTypeEnum {
    /**
     * 无分销属性
     */
    NONE(0, "无分销属性"),
    /**
     * 自卖分销
     */
    SELF_SELL(1, "自卖分销"),
    /**
     * 平台分销
     */
    PLATFORM(2, "平台分销"),
    /**
     * 自营+平台分销
     */
    PALTFORM_SELLSELF(3, "自营+平台分销"),
    /**
     * 倒卖--平台获取分销
     */
    RESELL(4, "倒卖--平台获取分销"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ProductDistributionTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 通过 商品分销属性 获取分销类型
     *
     * @param productDistributionDTO
     * @return
     */
    public static ProductDistributionTypeEnum findByProductDistributionDTO(ProductDistributionDTO productDistributionDTO) {
        BigDecimal selfSupportRatio = productDistributionDTO.getSelfSupportRatio();
        BigDecimal platSupportRatio = productDistributionDTO.getPlatSupportRatio();
        Long productId = productDistributionDTO.getProductId();
        Long parentId = productDistributionDTO.getParentId();

        return findByParams(productId, parentId, selfSupportRatio, platSupportRatio);
    }

    /**
     * 通过 各分销属性配置 获取分销类型
     * @param productId
     * @param parentId
     * @param selfSupportRatio
     * @param platSupportRatio
     * @return
     */
    public static ProductDistributionTypeEnum findByParams(Long productId,
                                                           Long parentId,
                                                           BigDecimal selfSupportRatio,
                                                           BigDecimal platSupportRatio) {
        if (productId == null || parentId == null) {
            return ProductDistributionTypeEnum.NONE;
        }

        if (selfSupportRatio == null && platSupportRatio != null) {
            return ProductDistributionTypeEnum.PLATFORM;
        } else if (selfSupportRatio != null && platSupportRatio == null
                && StringUtils.equals(productId.toString(), parentId.toString())) {
            return ProductDistributionTypeEnum.SELF_SELL;
        } else if (selfSupportRatio != null && platSupportRatio != null
                && !StringUtils.equals(productId.toString(), parentId.toString())) {
            return ProductDistributionTypeEnum.RESELL;
        } else if (selfSupportRatio != null && platSupportRatio != null
                && StringUtils.equals(productId.toString(), parentId.toString())) {
            return ProductDistributionTypeEnum.PALTFORM_SELLSELF;
        } else {
            return ProductDistributionTypeEnum.NONE;
        }
    }
}
