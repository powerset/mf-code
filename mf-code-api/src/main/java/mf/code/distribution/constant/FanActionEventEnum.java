package mf.code.distribution.constant;


import org.apache.commons.lang.StringUtils;

/**
 * mf.code.distribution.constant
 * Description: 1登录，2浏览，3下单（未支付），4付款，5邀请，6新手任务，7日常任务，8店长任务，9升级店长。
 *
 * @author gel
 * @date 2019-07-05 15:52
 */
public enum FanActionEventEnum {

    /**
     * 1、登录
     */
    LOGIN("1", "login", "登录"),
    /**
     * 2、浏览
     */
    VIEW_GOODS("2", "viewGoods", "浏览商品"),
    /**
     * 3、下单（未支付）
     */
    ORDER("3", "order", "下单"),
    /**
     * 4、付款
     */
    PAY("4", "pay", "付款"),
    /**
     * 5、邀请
     */
    INVITATION("5", "invitation", "邀请"),
    /**
     * 6、新手任务
     */
    NEWBIE_TASK("6", "newbie_task", "新手任务"),
    /**
     * 7、日常任务
     */
    DAILY_TASK("7", "daily_task", "日常任务"),
    /**
     * 8、店长任务
     */
    SHOP_MANAGER_TASK("8", "shop_manager_task", "店长任务"),
    /**
     * 9、升级店长
     */
    SHOP_MANAGER("9", "shop_manager", "成为店长"),
    /**
     * 10、订单取消
     */
    CANCEL_ORDER("10", "cancel_order", "取消订单"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 类型
     */
    private String type;
    /**
     * 描述
     */
    private String desc;

    FanActionEventEnum(String code, String type, String desc) {
        this.code = code;
        this.type = type;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }


    public static String getDescByCode(String code) {
        if (StringUtils.isBlank(code)) {
            return "";
        }
        for (FanActionEventEnum eventEnum : FanActionEventEnum.values()) {
            if (StringUtils.equals(code, eventEnum.getCode())) {
                return eventEnum.getDesc();
            }
        }
        return "";
    }

    public static FanActionEventEnum getEnumByCode(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (FanActionEventEnum eventEnum : FanActionEventEnum.values()) {
            if (StringUtils.equals(code, eventEnum.getCode())) {
                return eventEnum;
            }
        }
        return null;
    }
}
