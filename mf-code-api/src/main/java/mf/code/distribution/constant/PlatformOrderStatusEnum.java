package mf.code.distribution.constant;

/**
 * mf.code.distribution.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-30 12:06
 */
public enum PlatformOrderStatusEnum {

	ORDERING(0, "进行中"),
	SUCCESS(1, "成功"),
	FAILED(2, "失效"),
	TIMEOUT(3, "订单超时（只存在逻辑）");
	private int code;
	private String message;

	PlatformOrderStatusEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
