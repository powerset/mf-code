package mf.code.distribution.constant;

/**
 * mf.code.distribution.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-30 11:35
 */
public enum PlatformOrderTypeEnum {

	USECASH(0, "平台提现"),
	PALTFOR_PAY(1, "平台-付款"),
	PALTFORM_PRESTORE(2, "平台-入账"),
	PALTFORMREFUND(3, "平台退款"),
	;

	/**
	 * code
	 */
	private int code;
	/**
	 * 描述
	 */
	private String desc;

	PlatformOrderTypeEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
