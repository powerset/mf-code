package mf.code.statistics.constant;/**
 * create by qc on 2019/7/8 0008
 */

/**
 * @author gbf
 * 2019/7/8 0008、10:26
 */
public enum MongoUserActionScene {

    /**
     * 通用埋点
     */
    COMMON_POINT(0, "通用埋点"),
    /**
     * 自定义埋点
     */
    CUSTOM_POINT(1, "自定义埋点"),
    /**
     * 裂变埋点
     */
    FISSION_POINT(2, "裂变埋点");

    private int code;
    private String message;

    MongoUserActionScene(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode(){
        return this.code;
    }
}
