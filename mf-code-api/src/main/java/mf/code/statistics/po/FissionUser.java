package mf.code.statistics.po;/**
 * create by qc on 2019/6/26 0026
 */

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author gbf
 * 2019/6/26 0026、09:39
 */
@Data
@ToString
@NoArgsConstructor
public class FissionUser {


    private String id;
    /**
     * 商户
     */
    private String mid;
    /**
     * 店铺
     */
    private String sid;
    /**
     * 邀请人id
     */
    private String parentId;
    /**
     * 被邀请人id
     */
    private String subId;
    /**
     * 活动类型
     */
    private String activityType;
    /**
     * 活动id
     */
    private String activityId;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 创建时间
     */
    private Date ctime;
    /**
     * 自增记录 记录从有记录开始当日是第几日.
     * 1.近n天计算
     * 2.
     */
    private String dateIndex;

    /**
     * 场景
     */
    private String scene;
}
