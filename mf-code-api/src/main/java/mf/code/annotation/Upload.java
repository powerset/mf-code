package mf.code.annotation;

import java.lang.annotation.*;

/**
 * mf.code.annotation
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:06
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Upload {
}
