package mf.code.douyin;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * mf.code.douyin
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月19日 13:49
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DouyinPaySignDTO {
    /***
     * 支付分配给业务方的 id
     * 是否参与签名：是
     */
    private String app_id;
    /***
     * 签名算法，暂支持 MD5
     * 是否参与签名：是
     */
    private String sign_type;
    /***
     * 发送请求的时间戳
     * 是否参与签名：是
     */
    private String timestamp;
    /***
     * 支付订单号
     * 是否参与签名：是
     */
    private String trade_no;
    /***
     * 商户 id
     * 是否参与签名：是
     */
    private String merchant_id;
    /***
     * 用户的唯一标识 id，开发者请传 openid
     * 是否参与签名：是
     */
    private String uid;
    /***
     * 订单金额，单位为分
     * 是否参与签名：是
     */
    private Long total_amount;
    /***
     * 传递给支付方的支付信息，标准 json 格式字符串(JSON.stringify({url: "...."}))，不同的支付方参数格式不一样
     * 是否参与签名：是
     */
    private String params;
}
