package mf.code.douyin;

import lombok.Data;

/**
 * mf.code.douyin.constant
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月10日 10:52
 */
@Data
public class DouyinPayReq {

    private String ip;

    /***
     * 商户网站唯一订单号
     * 最大长度：32
     * <必选>
     */
    private String outOrderNo;

    /***
     * 唯一标识用户的id，小程序开发者请传open_id。
     * 最大长度：32
     * <必选>
     */
    private String uid;
    /***
     * 金额，分为单位，应传整型
     * 最大长度：32
     * <必选>
     */
    private Long totalAmount;

    /***
     * 商户订单名称
     * 最大长度：200
     * <必选>
     */
    private String subject;

    /***
     * 商户订单详情
     * 最大长度：200
     * <必选>
     */
    private String body;
    /***
     * 	服务器异步通知http地址 <=> 请填支付宝下单接口对应的异步通知url
     * 	最大长度：500
     *  <必选>
     */
    private String notifyUrl;

    /***
     * 扩展参数，json格式， 用来上送商户自定义信息
     * 最大长度: 1024
     * <可选>
     */
    private String extParam;
    /**---------------------------------------------------------------------------------*/
    /***
     * 下单时间戳，unix时间戳
     * 最大长度：14
     * <必选>
     */
    private String tradeTime;
    /***
     *  订单有效时间（单位 秒）
     *  最大长度：14
     *  <必选>
     */
    private String validTime;
    /***
     * 风控信息，标准的json字符串格式，目前需要传入用户的真实ip： "{"ip":"123.123.123.1"}"
     * 最大长度：2048
     * <必选>
     */
    private String riskInfo;
    /***
     * 支付分配给业务方的商户号
     * 最大长度：32
     * <必选>
     */
    private String merchantId;
    /***
     * 币种 CNY
     * 最大长度：9
     * <必选>
     */
    private String currency;
    /***
     * 折扣 格式（3段）:订单号^金额^方式|订单号^金额^方式。
     * 方式目前仅支持红包: coupon如：423423^1^coupon。
     * 可选，目前暂不支持
     * 最大长度：1000
     * <可选>
     */
    private String payDiscount;
    /***
     * 平台手续费
     * 最大长度：20
     * <可选>
     */
    private String serviceFee;
}
