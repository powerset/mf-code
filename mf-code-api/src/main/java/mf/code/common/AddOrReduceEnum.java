package mf.code.common;

/**
 * mf.code.common
 * Description:
 *
 * @author gel
 * @date 2019-05-17 15:35
 */
public enum AddOrReduceEnum {
    /**
     * 加
     */
    ADD(0, "加"),
    /**
     * 减
     */
    REDUCE(1, "减"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    AddOrReduceEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
