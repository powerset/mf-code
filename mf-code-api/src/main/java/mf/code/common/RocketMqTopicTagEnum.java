package mf.code.common;

import lombok.ToString;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.user.constant
 * Description:
 *
 * @author: gel
 * @date: 2019-02-25 10:17
 */
@ToString
public enum RocketMqTopicTagEnum {

    /**
     * ======================== mf-code-one 模版类消息 =====================
     */
    /**
     * 财富大闯关模版消息
     */
    TEMPLATE_CHECKPOINT(1, "template", "checkpoint", "财富大闯关模版消息"),
    /**
     * 粉丝召回
     */
    TEMPLATE_PUSH_RECALL(1, "template", "push_recall", "粉丝召回"),


    /**
     * ======================== mf-code-user 邀请类消息 =====================
     */
    /**
     * 抓矿工
     */
    CATCH_MINER(2, "invite", "catch_miner", "抓矿工"),
    /**
     * 活动邀请
     */
    INVITE_ACTIVITY(2, "invite", "invite_activity", "活动邀请"),
    /**
     * 商城团队
     */
    SHOPPING_TEAM(2, "invite", "shopping_team", "商城团队"),

    /**
     * ========================= mf-code-order 通知分销计算  =================
     */

    /**
     * 商品订单返佣
     */
    DISTRIBUTION_ORDER(3, "distribution", "order", "商品订单返佣"),
    /**
     * 商品订单返佣取消
     */
    DISTRIBUTION_ORDER_REFUND(3, "distribution", "order_refund", "商品订单返佣取消"),


    /**
     * 服务器业务埋点日志
     */
    BIZ_LOG_FAN_ACTION(3, "bizLog", "fan_action", "服务器业务埋点日志"),

    /**
     * ========================= mf-code-goods 商品销量排行  =================
     */

    /**
     * 商品销量排行
     */
    PRODUCT_RANK_SALES_VOLUME(4, "product_rank", "sale_volume", "商品销量排行"),


    /**
     * ========================= mf-code-statistics 埋点上报  =================
     */
    STATISTICS_REPORT(5, "report", "report", "前端通用和参与埋点上报v2"),
    STATISTICS_REPORT_FISSION(6, "fissionReport", "fission_report", "裂变后端埋点上报"),
    STATISTICS_REPORT_MERCHANT_LOGIN(7, "merchant_login", "merchant_login", "商户端登陆埋点上报"),
    STATISTICS_REPORT_ORDER_PAY(9, "order_pay", "order_pay", "订单支付埋点上报"),

    /**
     * ======================== mf-code-comment 评价类消息 =====================
     */
    /**
     * 初始化评价
     */
    COMMENT_INIT(8, "comment", "init", "初始化评价"),

    ;

    /**
     * code
     */
    private int code;
    /**
     * topic
     */
    private String topic;
    /**
     * tag
     */
    private String tag;
    /**
     * 描述
     */
    private String desc;

    RocketMqTopicTagEnum(int code, String topic, String tag, String desc) {
        this.code = code;
        this.desc = desc;
        this.topic = topic;
        this.tag = tag;
    }

    public static RocketMqTopicTagEnum findByTopicAndTag(String topic, String tag) {
        for (RocketMqTopicTagEnum topicTagEnum : RocketMqTopicTagEnum.values()) {
            if (StringUtils.equals(topicTagEnum.getTopic(), topic) && StringUtils.equals(topicTagEnum.getTag(), tag)) {
                return topicTagEnum;
            }
        }
        return null;
    }

    /**
     * 直接拼接rocket-spring-boot-start中接受监听所需参数
     *
     * @param rocketMqTopicTagEnum
     * @return
     */
    public static String findTopicAndTagsByEnum(RocketMqTopicTagEnum rocketMqTopicTagEnum) {

        if (rocketMqTopicTagEnum == null) {
            return "";
        }
        return rocketMqTopicTagEnum.getTopic() + ":" + rocketMqTopicTagEnum.getTag();
    }

    public int getCode() {
        return code;
    }

    public String getTopic() {
        return topic;
    }

    public String getTag() {
        return tag;
    }

    public String getDesc() {
        return desc;
    }


}
