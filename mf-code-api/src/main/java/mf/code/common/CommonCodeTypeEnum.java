package mf.code.common;

/**
 * mf.code.common
 * Description: 二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）
 *
 * @author gel
 * @date 2019-06-25 16:09
 */
public enum CommonCodeTypeEnum {
    /**
     * 1：活码
     */
    LIVE_CODE(1, "活码"),
    /**
     * 2：群二维码
     */
    GROUP_CODE(2, "群二维码"),
    /**
     * 3：个人微信号
     */
    PERSONAL_CODE(3, "个人微信号"),
    /**
     * 4：公众号
     */
    PUBLIC_CODE(4, "公众号"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    CommonCodeTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
