package mf.code.common;

/**
 * mf.code.common
 * Description: 用户类型（1：用户，2：商户）
 *
 * @author gel
 * @date 2019-06-25 16:09
 */
public enum CommonCodeUserTypeEnum {
    /**
     * 1：用户
     */
    USER(1, "用户"),
    /**
     * 2：商户
     */
    MERCHANT(2, "商户"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    CommonCodeUserTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
