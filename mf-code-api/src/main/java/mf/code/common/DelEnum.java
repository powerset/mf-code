package mf.code.common;

/**
 * mf.code.user.constant
 * Description:
 *
 * @author: gel
 * @date: 2018-11-02 11:49
 */
public enum DelEnum {

    /**
     * 未删除
     */
    NO(0, "未删除"),
    /**
     * 已删除
     */
    YES(1, "已删除"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    DelEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
