package mf.code.common;

/**
 * mf.code.common
 * Description: 二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）
 *
 * @author gel
 * @date 2019-06-25 16:09
 */
public enum CommonCodeStatusEnum {
    /**
     * 0：初始状态
     */
    INIT(0, "初始状态"),
    /**
     * 1：使用中
     */
    USED(1, "使用中"),
    /**
     * 2：次数扫完
     */
    STOP(2, "次数扫完"),
    /**
     * 3：自动过期
     */
    EXPIRE(3, "自动过期"),
    /**
     * 4：被禁用
     */
    BAN(4, "被禁用"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    CommonCodeStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
