package mf.code.order.constant;

/**
 * mf.code.order.constant
 * Description: 优惠类型 0：无优惠，1：优惠券，2：助力免单，3：抽奖免单
 *
 * @author gel
 * @date 2019-07-16 15:41
 */
public enum OrderDiscountTypeEnum {
    /**
     * 无优惠
     */
    NONE(0, "无优惠"),
    /**
     * 优惠券
     */
    COUPON(1, "优惠券"),
    /**
     * 助力免单
     */
    ASSIST(2, "助力免单"),
    /**
     * 抽奖免单
     */
    ORDER_BACK(3, "抽奖免单"),
    ;
    private int code;
    private String message;

    OrderDiscountTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderDiscountTypeEnum findByCode(Integer code) {
        for (OrderDiscountTypeEnum typeEnum : OrderDiscountTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return NONE;
    }
}
