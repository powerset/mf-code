package mf.code.order.constant;

/**
 * mf.code.order.constant
 * Description:
 *
 * @author gel
 * @date 2019-04-13 17:08
 */
public enum SellerOrderTypeEnum {
    /**
     * 分销商品订单
     */
    DISTRIBUTION(1, "分销商品订单"),
    /**
     * 供应商品订单
     */
    SUPPLY(2, "供应商品订单"),
    /**
     * 自营商品订单
     */
    SELF_SUPPORT(3, "自营商品订单"),
    ;
    private int code;
    private String message;

    SellerOrderTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static SellerOrderTypeEnum findByCode(Integer code) {
        for (SellerOrderTypeEnum typeEnum : SellerOrderTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
