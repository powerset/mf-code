package mf.code.order.constant;

/**
 * mf.code.order.constant
 * Description: 优惠类型 0：无优惠，1：优惠券，2：助力免单，3：抽奖免单
 *
 * @author gel
 * @date 2019-07-16 15:41
 */
public enum OrderSourceEnum {
    /**
     * 集客名品
     */
    JKMF(0, "集客名品"),
    /**
     * 抖带带
     */
    DOUYIN_SHOP_MANAGER(1, "抖带带(抖小铺-卖家版)"),
    /**
     * 抖小铺
     */
    DOUYIN_SHOP(2, "抖小铺"),
    ;
    private int code;
    private String message;

    OrderSourceEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderSourceEnum findByCode(Integer code) {
        for (OrderSourceEnum typeEnum : OrderSourceEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return JKMF;
    }
}
