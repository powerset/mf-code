package mf.code.order.feignapi.constant;

public enum OrderBizStatusEnum {
    /***
     * 订单状态
     * 0:待付款;
     * 1:待发货(支付成功);
     * 2:代发货-退款中;
     * 3:待收货;
     * 4:待收货-退款中(暂时没有-冗余以后);
     * 5:交易成功(确认收货);
     * 6:交易成功-退款中;
     * 7:交易关闭-订单取消;
     * 8:交易关闭-退款成功-支付类型订单;
     * 9:退款-等待商家处理;
     * 10:退款-等待买家退货;
     * 11:退款失败;
     * 12:退款成功-退款类型订单;
     * 13:退款/退款退货 关闭-撤销售后申请;
     */
    WILLPAY(0, "待付款"),
    WILLSEND(1, "待发货(支付成功)"),
    WILLSEND_REFUNDING(2, "代发货-退款中;"),
    WILLRECEIVE(3, "待收货"),
    WILLRECEIVE_REFUNDING(4, "待收货-退款中(暂时没有-冗余以后)"),
    TRADE_SUCCESS(5, "交易成功(确认收货)"),
    TRADE_SUCCESS_REFUND_ING(6, "交易成功-退款中"),
    TRADE_CLOSE_ORDER_CANCEL(7, "交易关闭-订单取消"),
    TRADE_CLOSE_REFUND_SUCCESS(8, "交易关闭-退款成功-支付类型订单"),
    REFUND_ING_WAIT_SELLER_DEAL(9, "退款-等待商家处理"),
    REFUND_ING_WAIT_BUYER_DEAL(10, "退款-等待买家退货"),
    REFUND_FAILED(11, "退款失败"),
    REFUND_SUCCESS(12, "退款成功-退款类型订单"),
    REFUND_CLOSE(13, "退款/退款退货 关闭-撤销售后申请"),
    REFUND_ING_WAIT_SELLER_RECEIVE(14, "退款-等待商家收货"),
    ;
    private int code;
    private String message;

    OrderBizStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderBizStatusEnum findByCode(Integer code) {
        for (OrderBizStatusEnum typeEnum : OrderBizStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
