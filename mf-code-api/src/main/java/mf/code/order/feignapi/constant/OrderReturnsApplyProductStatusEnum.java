package mf.code.order.feignapi.constant;

/**
 * mf.code.order.feignclient.constant
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 11:19
 */
public enum OrderReturnsApplyProductStatusEnum {
    //货物状态 0:已收到货 1:未收到货
    RECEIVED(0, "已收到货"),
    UNRECEIVED(1, "未收到货"),
    ;
    private int code;
    private String message;

    OrderReturnsApplyProductStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderReturnsApplyStatusEnum findByCode(Integer code) {
        for (OrderReturnsApplyStatusEnum typeEnum : OrderReturnsApplyStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
