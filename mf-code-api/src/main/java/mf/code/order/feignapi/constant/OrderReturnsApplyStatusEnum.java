package mf.code.order.feignapi.constant;

/**
 * mf.code.order.feignclient.constant
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 10:30
 */
public enum OrderReturnsApplyStatusEnum {
    /***
     * 审核状态
     * -1：拒绝；
     * 0：发起；
     * 1：通过；
     * 2：撤销
     */
    REFUSE(-1, "拒绝"),
    START(0, "发起"),
    PASS(1, "通过"),
    CANCEL(2, "撤销"),
    NO_STOCK(3, "无库存"),
    ;
    private int code;
    private String message;

    OrderReturnsApplyStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderReturnsApplyStatusEnum findByCode(Integer code) {
        for (OrderReturnsApplyStatusEnum typeEnum : OrderReturnsApplyStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
