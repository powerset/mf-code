package mf.code.order.feignapi.constant;

public enum OrderBizBizTypeEnum {
    /***
     * 业务状态
     * 0:分销业务;
     */
    DISTRIBUTION(0, "分销业务"),
    ;
    private int code;
    private String message;

    OrderBizBizTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderBizBizTypeEnum findByCode(Integer code) {
        for (OrderBizBizTypeEnum typeEnum : OrderBizBizTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
