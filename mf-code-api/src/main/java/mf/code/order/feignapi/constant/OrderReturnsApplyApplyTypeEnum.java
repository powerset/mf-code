package mf.code.order.feignapi.constant;

/**
 * mf.code.order.feignclient.constant
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 10:49
 */
public enum OrderReturnsApplyApplyTypeEnum {
    //申请类型 1：用户 2：商户
    USER(1, "用户"),
    MERCHANT(2, "商户"),
    ;
    private int code;
    private String message;

    OrderReturnsApplyApplyTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderReturnsApplyApplyTypeEnum findByCode(Integer code) {
        for (OrderReturnsApplyApplyTypeEnum typeEnum : OrderReturnsApplyApplyTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
