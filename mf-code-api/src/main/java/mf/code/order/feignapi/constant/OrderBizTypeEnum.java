package mf.code.order.feignapi.constant;

import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.goods.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-04 01:54
 */
public enum OrderBizTypeEnum {
    /**
     * 无分销属性
     */
    NONE(0, "无分销属性"),
    /**
     * 自卖分销
     */
    SELF_SELL(1, "自卖分销"),
    /**
     * 放到平台分销
     */
    PLATFORM(2, "放到平台分销"),
    /**
     * 自营+平台分销
     */
    PALTFORM_SELLSELF(3, "自卖分销+放到平台分销"),
    /**
     * 平台获取分销
     */
    RESELL(4, "平台获取分销--从平台市场拿到自己店里售卖"),
    /**
     * 平台补贴用户
     */
    SUBSIDY_USER(5, "平台补贴用户"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    OrderBizTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

	public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 通过 商品属性 获取分销类型
     *
     * @param goodsMap
     * @return
     */
    public static OrderBizTypeEnum getDistributionTypeEnum(Map goodsMap) {
        // 商户抽佣设置
        BigDecimal selfSupportRatio = null;
        if (goodsMap.get("selfSupportRatio") != null) {
            selfSupportRatio = new BigDecimal(goodsMap.get("selfSupportRatio").toString());
        }
        // 平台佣金设置
        BigDecimal platSupportRatio = null;
        if (goodsMap.get("platSupportRatio") != null) {
            platSupportRatio = new BigDecimal(goodsMap.get("platSupportRatio").toString());
        }
        // 商品id
        Long productId = Long.valueOf(goodsMap.get("productId").toString());

        Long parentId = Long.valueOf(goodsMap.get("parentId").toString()) ;
        return getDistributionTypeEnum(selfSupportRatio, platSupportRatio, productId, parentId);
    }

    public static OrderBizTypeEnum getDistributionTypeEnum(BigDecimal selfSupportRatio, BigDecimal platSupportRatio, Long productId, Long parentId) {

        if (selfSupportRatio == null && platSupportRatio != null) {
            return OrderBizTypeEnum.PLATFORM;
        } else if (selfSupportRatio != null && platSupportRatio == null
                && StringUtils.equals(productId.toString(), parentId.toString())) {
            return OrderBizTypeEnum.SELF_SELL;
        } else if (selfSupportRatio != null && platSupportRatio != null
                && !StringUtils.equals(productId.toString(), parentId.toString())) {
            return OrderBizTypeEnum.RESELL;
        } else if (selfSupportRatio != null && platSupportRatio != null
                && StringUtils.equals(productId.toString(), parentId.toString())) {
            return OrderBizTypeEnum.PALTFORM_SELLSELF;
        } else {
            return OrderBizTypeEnum.NONE;
        }

    }

    public static OrderBizTypeEnum getDistributionTypeEnum(ProductDistributionDTO distributionDTO) {
        BigDecimal selfSupportRatio = distributionDTO.getSelfSupportRatio();
        BigDecimal platSupportRatio = distributionDTO.getPlatSupportRatio();
        Long productId = distributionDTO.getProductId();
        Long parentId = distributionDTO.getParentId();

        return getDistributionTypeEnum(selfSupportRatio, platSupportRatio, productId, parentId);
    }
}
