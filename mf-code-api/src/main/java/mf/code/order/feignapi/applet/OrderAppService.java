package mf.code.order.feignapi.applet;//package mf.code.order.feignclient.applet;
//
//import mf.code.order.dto.OrderResp;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import java.util.List;
//import java.util.Map;
//
///**
// * mf.code.order.feignclient.applet
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年04月03日 14:40
// */
//@FeignClient("mf-code-order")
//public interface OrderAppService {
//
//    @GetMapping("/feignclient/order/applet/v5/countGoodsNumByOrder")
//    int countGoodsNumByOrder(@RequestParam(name = "goodsId") Long goodsId);
//
//    /**
//     * 通过orderid 获取订单信息
//     *
//     * @param orderId
//     * @return
//     */
//    @GetMapping("/feignclient/order/applet/v5/queryOrder")
//    OrderResp queryOrder(@RequestParam(name = "orderId") Long orderId);
//
//    /**
//     * 查询 用户 在此店铺得 本月的消费金额
//     * @param userId
//     * @param shopId
//     * @return
//     *
//     * 查询条件：
//     * <per>
//     *     userId
//     *     shopId
//     *     type = 商品购买-付款
//     *     status = 支付成功
//     *     payment_time = 本月
//     * </per>
//     */
//    @GetMapping("/feignclient/order/applet/v5/queryAmountOfConsumptionThisMonth")
//    String queryAmountOfConsumptionThisMonth(@RequestParam("userId") Long userId,
//                                             @RequestParam("shopId") Long shopId);
//
//    /**
//     * 根据店铺id,产品id统计订单数
//     *
//     * @param shopId
//     * @param productId
//     * @return int统计数量
//     */
//    @GetMapping("/feignclient/order/applet/v5/summaryOrderByShopIdAndProductId")
//    int summaryOrderByShopIdAndProductId(@RequestParam(name = "shopId") Long shopId,
//                                         @RequestParam(name = "productId") Long productId);
//
//    /**
//     * 获取 此用户在此店铺的 所有 可结算订单 -- 进行结算处理
//     *
//     * @param userId
//     * @param shopId
//     * @return
//     */
//    @GetMapping("/feignclient/order/applet/v5/queryOrderSettledByUserIdShopId")
//    List<OrderResp> queryOrderSettledByUserIdShopId(@RequestParam("userId") Long userId,
//                                                    @RequestParam("shopId") Long shopId);
//
//
//    /**
//     * 获取 用户上月消费总金额
//     *
//     * @param upperIds
//     * @return
//     */
//    @GetMapping("/feignclient/order/applet/v5/listUserSpendingAmountLastMonth")
//    Map<Long, String> listUserSpendingAmountLastMonth(@RequestParam("upperIds") List<Long> upperIds,
//                                                      @RequestParam("shopId") Long shopId);
//
//    /**
//     * 查询 用户 在此店铺得 本月的消费记录
//     *
//     * @param userId
//     * @param shopId
//     * @return
//     */
//    @GetMapping("/feignclient/order/applet/v5/queryOrderRecordThisMonth")
//    Map<String, OrderResp> queryOrderRecordThisMonth(@RequestParam("userId") Long userId,
//                                                     @RequestParam("shopId") Long shopId);
//
//    /**
//     * 通过商品id 获取商品销量
//     * @param productIds
//     * @return Map<productId, salesVolume>
//     */
//    @GetMapping("/feignclient/order/applet/v5/countGoodsNumByProductIds")
//	Map<Long, Integer> countGoodsNumByProductIds(@RequestParam("productIds") List<Long> productIds);
//}
