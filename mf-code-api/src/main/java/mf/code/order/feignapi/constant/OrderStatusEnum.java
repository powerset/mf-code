package mf.code.order.feignapi.constant;

public enum OrderStatusEnum {
    /***
     * 订单状态
     * 0:待付款|退款,退货申请;
     * 1:待发货(支付成功|退款退货);
     * 2:待收货(支付|退货);
     * 3：交易成功(确认收货|退款，退货成功);
     * 4:交易关闭(买家主动取消待付款订单|退款撤销);
     * 5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);
     * 6:交易关闭(支付超时|退款超时)
     */
    WILLPAY_OR_REFUNDAPPLY(0, "待付款|退款,退货申请"),
    WILLSEND_OR_REFUND(1, "待发货(支付成功|退款退货)"),
    WILLRECEIVE_OR_REFUND(2, "待收货(支付|退货|退款同意)"),
    TRADE_SUCCESS_OR_REFUND_SUCCESS(3, "交易成功(确认收货|退款，退货成功)"),
    TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL(4, "交易关闭(买家主动取消待付款订单|退款撤销)"),
    TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL(5, "交易关闭(卖家主动取消待发货订单|退款，退货拒绝)"),
    TRADE_CLOSE_TIMEOUT(6, "交易关闭(支付超时|退款超时)"),
    TRADE_CLOSE_REFUND_SUCCESS(7, "交易关闭(退款成功)"),
    ;
    private int code;
    private String message;

    OrderStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderStatusEnum findByCode(Integer code) {
        for (OrderStatusEnum typeEnum : OrderStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }

    /**
     * 支付状态
     * @return
     */
    public static Integer[] payStatus() {
        return new Integer[]{OrderStatusEnum.WILLSEND_OR_REFUND.getCode(),
                OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode(),
                OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode(),
                OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode(),
                OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode()
        };
    }
}
