package mf.code.order.feignapi.constant;

/**
 * mf.code.order.feignclient.constant
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 10:16
 */
public enum OrderReturnsApplyTypeEnum {
    //0 仅退款 1退货退款
    REFUNDMONEY(0, "商品退款-仅退款"),
    REFUNDMONEYANDREFUNDGOODS(1, "商品退款-退款退货"),
    ;
    private int code;
    private String message;

    OrderReturnsApplyTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderReturnsApplyTypeEnum findByCode(Integer code) {
        for (OrderReturnsApplyTypeEnum typeEnum : OrderReturnsApplyTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }

    public static OrderReturnsApplyTypeEnum findByCode(OrderTypeEnum orderTypeEnum) {
        if (orderTypeEnum == OrderTypeEnum.REFUNDMONEY) {
            return OrderReturnsApplyTypeEnum.REFUNDMONEY;
        } else if (orderTypeEnum == OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS) {
            return OrderReturnsApplyTypeEnum.REFUNDMONEYANDREFUNDGOODS;
        }
        return null;
    }

    public static OrderTypeEnum findByCodeOrderType(Integer code) {
        for (OrderReturnsApplyTypeEnum typeEnum : OrderReturnsApplyTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return OrderTypeEnum.REFUNDMONEY;
            } else {
                return OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS;
            }
        }
        return null;
    }


    /***
     * 返回前端的业务状态
     * @return
     */
    public static int findByReturn(int code) {
        if (code == OrderReturnsApplyTypeEnum.REFUNDMONEY.getCode()) {
            return OrderTypeEnum.REFUNDMONEY.getCode();
        } else {
            return OrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode();
        }
    }
}
