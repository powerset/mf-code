package mf.code.order.feignapi.constant;

public enum OrderTypeEnum {
    //1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货',
    PAY(1, "商品购买-付款"),
    REFUNDMONEY(2, "仅退款"),
    REFUNDMONEYANDREFUNDGOODS(3, "退货退款"),
    PLATFORM_SUBSIDY(4, "平台补贴"),
    ;
    private int code;
    private String message;

    OrderTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderTypeEnum findByCode(Integer code) {
        for (OrderTypeEnum typeEnum : OrderTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
