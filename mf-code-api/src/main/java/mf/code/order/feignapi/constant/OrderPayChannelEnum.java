package mf.code.order.feignapi.constant;

public enum OrderPayChannelEnum {
    //1：余额 2：微信 3：支付宝',
    CASH(1, "余额支付"),
    WX(2, "微信支付"),
    ALIPAY(3, "抖音-支付宝"),
    ;
    private int code;
    private String message;

    OrderPayChannelEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static OrderPayChannelEnum findByCode(Integer code) {
        for (OrderPayChannelEnum typeEnum : OrderPayChannelEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
