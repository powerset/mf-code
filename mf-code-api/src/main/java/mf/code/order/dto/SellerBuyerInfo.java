package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:59
 */
@Data
public class SellerBuyerInfo {
    private String nick;
    private String avatarUrl;
    private String phone;
    private String fromShopName;
    private String buyerMsg;
}
