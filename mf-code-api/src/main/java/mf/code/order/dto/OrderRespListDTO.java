package mf.code.order.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-01 17:10
 */
@Data
public class OrderRespListDTO {
	List<OrderResp> orderRespList;
}
