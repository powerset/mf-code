package mf.code.order.dto;

import lombok.Data;

@Data
public class SellerOrderUpdateMsgReqDTO {
    private String merchantId;
    private String shopId;
    private String orderId;
    private String orderNo;
    private String sellerMsg;
}
