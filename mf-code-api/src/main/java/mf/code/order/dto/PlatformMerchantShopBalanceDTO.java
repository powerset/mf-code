package mf.code.order.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.order.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月07日 16:59
 */
@Data
public class PlatformMerchantShopBalanceDTO {
    private List<PlatformMerchantShopBalance> shops;

    @Data
    public static class PlatformMerchantShopBalance {
        /**
         * 店铺id
         */
        private Long shopId;
        /**
         * 店铺名
         */
        private String shopName;
        /**
         * 店铺余额
         */
        private String balance;

        private Long merchantId;
    }
}
