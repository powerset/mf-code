package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:02
 */
@Data
public class SellerRefundInfoItem {
    /**
     * 卖家退库时间
     */
    private String refundTime;
    /**
     * 退款类型
     */
    private Integer refundType;
    /**
     * 退款状态
     */
    private Integer refundStatus;
    /**
     * 退款商品数量
     */
    private Integer refundNum;
    /**
     * 退款金额
     */
    private BigDecimal refundAmount;
    /**
     * 商品标题
     */
    private String productTitle;
    /**
     * 商品主图
     */
    private String productMainPics;
    /**
     * 商品id
     */
    private String productId;
    /**
     * 退款原因
     */
    private String refundReason;
    /**
     * 买家发货时间
     */
    private String buyerSendTime;
    /**
     * 买家上传图片
     */
    private List<String> buyerVoucher;
    /**
     * 买家退款说明
     */
    private String refundDesc;
    /**
     * 买家结果，这个不需要吧
     */
    private String buyerResult;
    /**
     * 商家审核时间
     */
    private String auditTime;
    /**
     * 商家审核原因
     */
    private String auditReason;
    /**
     * 商家收货时间
     */
    private String sellerDealTime;
    /**
     * 商家凭证
     */
    private List<String> sellerVoucher;
    /**
     * 商家处理结果
     */
    private String sellerResult;
    /**
     * 退款结果
     */
    private String refundResult;
}
