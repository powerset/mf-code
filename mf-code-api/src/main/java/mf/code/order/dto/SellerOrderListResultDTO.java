package mf.code.order.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:06
 */
@Data
public class SellerOrderListResultDTO {

    private List<SellerOrderInfoDTO> orderList;
    private Integer page;
    private Integer size;
    private Long count;
}
