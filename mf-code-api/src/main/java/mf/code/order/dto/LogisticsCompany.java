package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:25
 */
@Data
public class LogisticsCompany {
    private Integer id;
    private String code;
    private String name;
}
