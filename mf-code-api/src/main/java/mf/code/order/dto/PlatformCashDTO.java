package mf.code.order.dto;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.order.dto
 *
 * @description: 运营平台-运营人员添加线下提现记录
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月07日 18:15
 */
@Data
public class PlatformCashDTO {
    private Long merchantId;
    private Long shopId;
    private String balance;
    private String platformUser;

    private String pics;


    public boolean checkValid() {
        if (merchantId != null && merchantId > 0 && shopId != null && shopId > 0 &&
                StringUtils.isNotBlank(balance) && StringUtils.isNotBlank(platformUser)
                && StringUtils.isNotBlank(pics)) {
            return true;
        }
        return false;
    }
}
