package mf.code.order.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet.dto
 *
 * @description: 物流快递列表
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 10:05
 */
@Data
public class LogisticsListDto {
    private List<Map> logistics;
}
