package mf.code.order.dto;

import lombok.Data;

@Data
public class SellerOrderCancelReqDTO {
    private String merchantId;
    private String shopId;
    private String orderId;
    private String orderNo;
    /**
     * 取消订单理由
     */
    private String reason;
}
