package mf.code.order.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:01
 */
@Data
public class SellerRefundInfo {
    private List<SellerRefundInfoItem> refundList;
}
