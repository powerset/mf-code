package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:07
 */
@Data
public class SellerOrderInfoDTO {

    private Long id;
    private Long orderId;
    private String orderNo;
    private String productTitle;
    private String productMainPics;
    private String skuSpecs;
    private BigDecimal productPrice;
    private Integer productNum;
    private String wxNick;
    private String buyerNick;
    private String buyerPhone;
    private String buyerAddress;
    private String createTime;
    private Integer orderType;
    private Integer type;
    private Integer status;
    private Integer refundType;
    private Integer refundStatus;
    private BigDecimal fee;
    private String buyerMsg;
    private String sellerMsg;

    /**
     * 评论状态： -1初始值,0买家已评论，1审核通过，2不通过
     */
    private String commentStatus;
    /**
     * 能否操作，0否，1能
     */
    private Integer operate;

}
