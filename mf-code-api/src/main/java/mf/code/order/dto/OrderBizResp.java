package mf.code.order.dto;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 13:31
 */
@Data
public class OrderBizResp {
	/**
	 * 主键id
	 */
	private Long id;

	/**
	 * 订单主键
	 */
	private Long orderId;

	/**
	 * 业务类型，0："分销场景"
	 */
	private Integer bizType;

	/**
	 * 业务信息
	 */
	private String bizValue;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 更新时间
	 */
	private Date utime;

	/**
	 * 删除标记 . 0:正常 1:删除
	 */
	private Integer del;
}
