package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:00
 */
@Data
public class SellerLogisticsInfo {
    private String buyerAddress;
    private String buyerNick;
    private String buyerPhone;
    private String logisticsName;
    private String logisticsNum;
}
