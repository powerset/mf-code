package mf.code.order.dto;

import lombok.Data;

@Data
public class SellerOrderListReqDTO {
    private String merchantId;
    /**
     * 店铺id
     */
    private String shopId;
    private String orderId;
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 商品id
     */
    private String productId;
    /**
     * 商品名
     */
    private String productName;
    /**
     * 买家名称
     */
    private String buyerName;
    /**
     * 买家手机号
     */
    private String buyerPhone;
    /**
     * 下单开始时间（格式："yyyy-MM-dd HH:mm:ss"）
     */
    private String cStartTime;
    /**
     * 下单结束时间（格式："yyyy-MM-dd HH:mm:ss"）
     */
    private String cEndTime;

    /**
     * 评价状态：0全部，1待评价，2已评价
     */
    private String checkStatus;
    /**
     * 订单状态
     * 0:待付款
     * 1:待发货
     * 2:待收货
     * 3:交易成功(确认收货);
     * 4:交易关闭-买家主动取消待付款订单(4、5合并为交易取消)
     * 5:交易关闭-卖家主动取消待发货订单(4、5合并为交易取消)
     * 6:支付超时(交易失败)
     * 9:全部状态
     */
    private Integer orderStatus;
    /**
     * tab页查询条件
     * 0:全部
     * 1:待发货
     * 2:退换/维权记录
     */
    private Integer tabType;
    /**
     * 订单类型：
     * 0全部
     * 1分销商品订单
     * 2供应商品订单
     * 3自营商品订单
     */
    private Integer orderType;
    /**
     * 页码
     */
    private Integer page;
    /**
     * 每页条数
     */
    private Integer size;
}
