package mf.code.order.dto;

import lombok.Data;

@Data
public class SellerAuditRefundReqDTO {
    private String merchantId;
    private String shopId;
    private String orderId;
    private String orderNo;
    private String auditStatus;
    private String auditReason;
    private String auditPic;
}
