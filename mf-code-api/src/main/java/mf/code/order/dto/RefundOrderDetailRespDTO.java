package mf.code.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 14:04
 */
@Data
public class RefundOrderDetailRespDTO {
    private Long merchantId;
    private Long shopId;
    private Long userId;
    private Long orderId;

    /***
     * 协商历史记录
     */
    private List<Map> chatHistory;
    /***
     * 商品信息
     */
    private Map goodsInfo;

    //退款原因
    private String reason;

    //退款价格
    private String price;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ctime;

    /**
     * 订单状态
     */
    private Integer status;

    private String number;

    /***
     * 返现金额
     */
    private String cashback;
}
