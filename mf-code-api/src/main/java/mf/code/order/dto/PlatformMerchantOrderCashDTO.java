package mf.code.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * mf.code.order.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月06日 21:06
 */
@Data
public class PlatformMerchantOrderCashDTO {

    /***
     * 提现时间
     */
    @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
    private Date ctime;

    /***
     * 提现金额（元）
     */
    private String fee;

    /***
     * 提现商家账号
     */
    private String phone;

    /***
     * 提现店铺
     */
    private String shopName;

    /***
     * 处理人
     */
    private String dealUser;

    /***
     * 查看凭证
     */
    private List<String> pics;

    /***
     * 提现编号
     */
    private Long id;
}
