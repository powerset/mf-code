package mf.code.order.dto;

import lombok.Data;

@Data
public class SellerRefundConfirmReqDTO {
    private String merchantId;
    private String shopId;
    private String orderId;
    private String orderNo;
}
