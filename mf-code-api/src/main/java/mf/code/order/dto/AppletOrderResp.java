package mf.code.order.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-08 14:06
 */
@Data
public class AppletOrderResp {
	/**
	 * 订单id
	 */
	private Long id;
	/**
	 * 订单详情
	 */
	private OrderResp orderInfo;
	/**
	 * 订单相关业务属性
	 */
	private List<OrderBizResp> orderBizList;
}
