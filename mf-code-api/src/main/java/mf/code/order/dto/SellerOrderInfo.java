package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:54
 */
@Data
public class SellerOrderInfo {

    private String orderId;
    private String orderNo;
    private Integer orderType;
    private Integer type;
    private Integer status;
    private Integer orderStatus;
    private Integer settleStatus;
    private BigDecimal settleAmount;
    private String createTime;
    private String paymentTime;
    private String sendTime;
    private String dealTime;
    private String settleTime;
    private String sellerMsg;
    private String buyerMsg;
    private List<SellerOrderInfoProduct> productList;
}
