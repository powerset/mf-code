package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-07 10:45
 */
@Data
public class AppletUserOrderHistoryDTO {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 联合标识：开始时间
	 */
	private Date beginTime;
	/**
	 * 联合标识：截止时间
	 */
	private Date endTime;
	/**
	 * 现在日期的上月消费金额 FIXME 按用户维度计算，应总和所有店铺消费的订单
	 */
	private BigDecimal lastSpendAmount;
	/**
	 * TODO 2.1 迭代 按用户维度计算，应总和 指定时间周期的 所有店铺消费的订单
	 */
	private BigDecimal spendAmount;
	/**
	 * 已支付的订单 -- 含分销属性
	 */
	private List<OrderResp> paidOrder = new ArrayList<>();
	/**
	 * 售后处理中的订单，此订单时间周期为 beginTime 至 now
	 */
	private List<OrderResp> afterSaleProcessing = new ArrayList<>();
}
