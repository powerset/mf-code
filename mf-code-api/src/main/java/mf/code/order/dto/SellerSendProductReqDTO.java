package mf.code.order.dto;

import lombok.Data;

@Data
public class SellerSendProductReqDTO {

    private String merchantId;
    private String shopId;
    private String orderId;
    private String orderNo;
    private String logisticsCode;
    private String logisticsNum;

}
