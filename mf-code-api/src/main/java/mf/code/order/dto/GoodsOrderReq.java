package mf.code.order.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.order.api.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 20:23
 */
@Slf4j
@Data
public class GoodsOrderReq {
    /***
     * 商户号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;
    /***
     * 用户编号
     */
    private Long userId;
    /***
     * 商品编号
     */
    private Long goodsId;

    /***
     * 收货地址编号
     */
    private Long addressId;

    /***
     * sku库快照编号
     */
    private Long skuId;

    /***
     * 是否展示sku 0 显示(默认显示)； 1 不显示；2秒杀sku类型；3平台免单抽奖
     */
    private Integer notShow;
    /***
     * 件数
     */
    private String number;
    /***
     * 买家留言信息
     */
    private String buyerMsg;

    /***
     * 1：分销商品，2：自营商品
     */
    private int bizType;

    /***
     * 支付方式 1：余额 2：微信 3：支付宝
     */
    private int payType;

    /**
     * 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     */
    private Integer discountType;

    /**
     * 优惠id（优惠券类型为优惠券id，其他为相应的活动id）
     */
    private Long discountId;

    public String getRedisKey() {
        if (discountType == null) {
            discountType = 0;
        }
        if (discountId == null) {
            discountId = 0L;
        }
        String redisKey = "" + merchantId + shopId + userId + goodsId + skuId + number + bizType + payType + discountType + discountId;
        log.info("支付订单redis缓存，jkmf:order:create:" + redisKey);
        return redisKey;
    }

    public boolean checkValid() {
        if (merchantId != null && merchantId > 0 && shopId != null && shopId > 0 && userId != null && userId > 0 &&
                goodsId != null && goodsId > 0 && skuId != null && skuId > 0
                && StringUtils.isNotBlank(number) && payType > 0) {
            return true;
        }
        return false;
    }
}
