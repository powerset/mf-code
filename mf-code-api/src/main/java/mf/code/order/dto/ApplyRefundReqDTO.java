package mf.code.order.dto;

import lombok.Data;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.order.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月11日 14:06
 */
@Data
public class    ApplyRefundReqDTO {
    /***
     * 商户编号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;
    /***
     * 用户编号
     */
    private Long userId;
    /***
     * 订单编号
     */
    private Long orderId;

    /***
     * 退款类型 1：仅退款 2：退货退款
     */
    private int type;

    /***
     * 退款原因
     */
    private Integer reason;

    /***
     * 退款/退货数量
     */
    private int number;
    /***
     * 备注说明(选填)
     */
    private String remark;
    /***
     * 上传凭证(选填)
     */
    private String pic;
    /***
     * 退款金额
     */
    private String price;

    public boolean checkValid() {
        if (this.merchantId == null || this.merchantId == 0 || this.shopId == null || this.shopId == 0 || this.userId == null || this.userId == 0 ||
                this.orderId == null || this.orderId == 0 || this.type == 0 || this.reason == null || this.number == 0 || StringUtils.isBlank(this.price) ||
                !RegexUtils.StringIsNumber(this.price)) {
            return false;
        }
        return true;
    }

}
