package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:05
 */
@Data
public class SellerOtherInfo {
    private String typeChangeLog;
    private String cancelTime;
    private String cancelName;
    private String cancelReason;
}
