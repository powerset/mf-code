package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 11:14
 */
@Data
public class OrderResp {

	/**
	 * 商品订单主键id
	 */
	private Long id;

	/**
	 * 订单编号 本系统的订单编号
	 */
	private String orderNo;

	/**
	 * 订单名称
	 */
	private String orderName;

	/**
	 * 收货地址编号
	 */
	private Long addressId;

	/**
	 * 订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
	 */
	private String logistics;

	/**
	 * 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货
	 */
	private Integer type;

	/**
	 * 支付渠道：1:余额 2:微信 3：支付宝
	 */
	private Integer payChannel;

	/**
	 * 订单状态 0:待付款|退款,退货申请;1:待发货(支付成功|退款退货);2:待收货(支付|退货);3：交易成功(确认收货|退款，退货成功);4:交易关闭(买家主动取消待付款订单|退款撤销);5:交易关闭(卖家主动取消待发货订单|退款，退货拒绝);6:交易关闭(支付超时|退款超时)
	 */
	private Integer status;

	/**
	 * 前端使用的业务状态 -- 冗余
	 */
	private Integer bizStatus;

	/**
	 * 原金额,单位元
	 */
	private BigDecimal originalfee;

	/**
	 * 实付金额,单位元
	 */
	private BigDecimal fee;

	/**
	 * 商品件数
	 */
	private Integer number;

	/**
	 * 流水号 第三方流水号
	 */
	private String tradeId;

	/**
	 * 备注信息
	 */
	private String remark;

	/**
	 * 商户号-主账号
	 */
	private Long merchantId;

	/**
	 * 店铺号
	 */
	private Long shopId;

	/**
	 * 用户编号
	 */
	private Long userId;

	/**
	 * 商品所属的店铺编号
	 */
	private Long goodsShopId;

	/**
	 * 商品编号
	 */
	private Long goodsId;

	/**
	 * 商品sku编号
	 */
	private Long skuId;

	/**
	 * 订单入参，json对象
	 */
	private String reqJson;

	/**
	 * 微信订单返回，json对象
	 */
	private String respJson;

	/**
	 * ip地址
	 */
	private String ipAddress;

	/**
	 * 回调处理系统时间
	 */
	private Date notifyTime;

	/**
	 * 响应数据中的支付时间
	 */
	private Date paymentTime;

	/**
	 * 发货时间
	 */
	private Date sendTime;

	/**
	 * 成交时间|收货时间
	 */
	private Date dealTime;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 更新时间
	 */
	private Date utime;

	/**
	 * 业务类型，1：分销商品，2：自营商品
	 */
	private Integer bizType;

	/**
	 * 业务类型针对的值1:TODO;2:TODO;
	 */
	private Long bizValue;

	/**
	 * 退款时，需要退款的订单主键编号(冗余)
	 */
	private Long refundOrderId;

	/**
	 * 买家留言
	 */
	private String buyerMsg;

	/**
	 * 卖家留言
	 */
	private String sellerMsg;

	/**
	 * 删除标记 . 0:正常 1:删除
	 */
	private Integer del;

	/**
	 * 订单来源 0:集客名品 1:抖带带 2:抖小铺
	 */
	private Integer source;

	//-----------orderBiz表 字段--------------------
	/**
	 * 业务类型，0："分销场景"
	 */
	private Integer orderBizBizType;

	/**
	 * 业务信息
	 */
	private String orderBizBizValue;

}
