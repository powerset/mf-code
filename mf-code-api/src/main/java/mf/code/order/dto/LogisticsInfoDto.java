package mf.code.order.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.applet.dto
 *
 * @description: 物流信息查询返回
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 13:48
 */
@Data
public class LogisticsInfoDto {

    private List<LogisticsInfo> logistics;
    private Integer status = 0;
    //快递单号
    private String num;
    //快递名
    private String name;

    @Data
    public static class LogisticsInfo {
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss", timezone = "GMT+8")
        private Date time;
        private String context;
        private String location;
        private String phone;
    }

    public void from(String resultLogisticsInfo) {
        if (StringUtils.isNotBlank(resultLogisticsInfo)) {
            Map<String, Object> map = JSON.parseObject(resultLogisticsInfo, Map.class);
            if (map != null && map.get("data") != null) {
                List<Object> logisticsInfoDetailObjects = JSONObject.parseArray(map.get("data").toString(), Object.class);
                this.logistics = new ArrayList<>();
                if (map.get("state") != null) {
                    this.status = NumberUtils.toInt(map.get("state").toString());
                }
                for (Object obj : logisticsInfoDetailObjects) {
                    LogisticsInfo logisticsInfo = JSONObject.parseObject(obj.toString(), LogisticsInfo.class);
                    if (logisticsInfo != null) {
                        if (StringUtils.isNotBlank(logisticsInfo.getContext())) {
                            String phone = RegexUtils.getStringContainPhone(logisticsInfo.getContext());
                            if (StringUtils.isNotBlank(phone)) {
                                logisticsInfo.setPhone(phone);
                            }
                        }
                    }
                    this.getLogistics().add(logisticsInfo);
                }
            }
        }
    }
}
