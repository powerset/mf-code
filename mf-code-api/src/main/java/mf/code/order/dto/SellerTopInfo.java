package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:51
 */
@Data
public class SellerTopInfo {
    private String orderId;
    private String orderNo;
    private Integer orderStatus;
    private Integer type;
    private Integer status;
    private String createTime;
    private BigDecimal originalFee;
    private Integer orderType;
    private BigDecimal settleAmount;
    private Integer settleStatus;
    private String settleTime;
    private String sellerMsg;
    private String buyerMsg;
}
