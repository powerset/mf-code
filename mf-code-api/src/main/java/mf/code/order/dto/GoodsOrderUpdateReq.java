package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.feignclient.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月08日 20:13
 */
@Data
public class GoodsOrderUpdateReq {
    /***
     * 订单编号
     */
    private Long orderId;
    /***
     * 用户编号
     */
    private Long userId;

    /***
     * 商户编号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;
    /**
     * 订单状态 0:待付款;1:待发货(支付成功);2:待收货;3：交易成功(确认收货);4:交易关闭-(买家主动取消待付款订单;卖家主动取消待发货订单;买家对待发货订单申请退款成功的)5:支付超时(交易关闭-订单已取消);6:退款中;7:退款成功;8:退款/退货失败;9:售后撤销-退款/退货交易关闭
     */
    private Integer status;
}
