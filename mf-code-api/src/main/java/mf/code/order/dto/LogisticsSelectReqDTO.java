package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 20:19
 */
@Data
public class LogisticsSelectReqDTO {
    private Long merchantId;
    private Long shopId;
    private Long orderId;
    private String orderNo;
    private String logisticsCode;
    private String logisticsNum;
}
