package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:56
 */
@Data
public class SellerOrderInfoProduct {

    /**
     * 商品信息
     */
    private String productTitle;
    private String productMainPics;
    private String productSrc;
    private String skuSpecs;
    private Integer orderStatus;
    private BigDecimal unitAmount;
    private Integer number;
    private BigDecimal fee;
    private String dealShopName;
}
