package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 15:33
 */
@Data
public class SellerOrderCommonReqDTO {
    private Long merchantId;
    private Long shopId;
    private Long orderId;
    private String orderNo;
}
