package mf.code.order.dto;

import lombok.Data;

/**
 * mf.code.order.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-11 14:45
 */
@Data
public class SellerOrderDetailResultDTO {


    private String orderId;
    private String orderNo;
    private Integer type;
    private Integer status;
    private Integer refundType;
    private Integer refundStatus;
    private Integer orderType;
    private SellerTopInfo topInfo;
    private SellerOrderInfo orderInfo;
    private SellerBuyerInfo buyerInfo;
    private SellerLogisticsInfo logisticsInfo;
    private SellerRefundInfo refundInfo;
    private SellerOtherInfo otherInfo;

}
