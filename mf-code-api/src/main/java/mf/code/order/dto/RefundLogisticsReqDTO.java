package mf.code.order.dto;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.order.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 16:21
 */
@Data
public class RefundLogisticsReqDTO {
    private Long merchantId;
    private Long shopId;
    private Long userId;
    private Long orderId;

    //订单物流对象:{"name":"快递名","num":"快递单号","code":"快递类型名"}
    private String name;
    private String num;
    private String code;

    public boolean checkValid() {
        if (this.merchantId == null || this.merchantId == 0 || this.shopId == null || this.shopId == 0 || this.userId == null || this.userId == 0 ||
                this.orderId == null || this.orderId == 0 || StringUtils.isBlank(this.name) || StringUtils.isBlank(this.num) || StringUtils.isBlank(this.code)) {
            return false;
        }
        return true;
    }
}
