package mf.code.order.dto;/**
 * create by qc on 2019/8/9 0009
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author gbf
 * 2019/8/9 0009、13:56
 */
@Data
public class DoudaidaiMypageResp {
    /**
     * 累计佣金
     */
    private BigDecimal commissionTotal;
    /**
     * 可提现余额
     */
    private BigDecimal commissionBalance;
    /**
     * 待结算佣金
     */
    private BigDecimal commissionWaitClose;

    /**
     * 累计销售
     */
    private BigDecimal orderTotalFee;
    /**
     * 累计成交订单数
     */
    private Integer orderTotalNum;
    /**
     * 昨日销售
     */
    private BigDecimal orderYtdTotalFee;
    /**
     * 昨日销售订单数
     */
    private Integer orderYtdTotalNum;
    /**
     * 昨日佣金
     */
    private BigDecimal orderYtdCommission;

}
