package mf.code.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.order.api.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月02日 20:23
 */
@Data
public class GoodsOrderResp {
    /***
     * 订单编号
     */
    private Long orderId;
    /***
     * 商户号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;
    /***
     * 用户编号
     */
    private Long userId;
    /***
     * 商品编号
     */
    private Long goodsId;

    /***
     * 收货地址编号
     */
    private Long addressId;

    /***
     * sku库快照编号
     */
    private Long skuId;
    /***
     * 金额
     */
    private String price;
    /***
     * 原始金额
     */
    private String originalPrice;
    /***
     * 件数
     */
    private String number;
    /***
     * 买家留言信息
     */
    private String buyerMsg;

    /***
     * 1：分销商品，2：自营商品
     */
    private int bizType;

    /***
     * 商品信息
     */
    private Map goodsInfo;

    /**
     * 订单状态 0:待付款;1:待发货(支付成功);2:待收货;3：交易成功(确认收货);4:交易关闭-(买家主动取消待付款订单;卖家主动取消待发货订单;买家对待发货订单申请退款成功的)5:支付超时(交易关闭-订单已取消);6:退款中;7:退款成功;8:退款/退货失败;9:售后撤销-退款/退货交易关闭
     */
    private Integer status;
    /**
     * 订单类型 1：商品购买-付款 2：商品退款-仅退款 3：商品退款-退款退货
     */
    private Integer type;
    /***
     * 返现金额
     */
    private String cashback;

    /**
     * 订单编号
     */
    private String orderNo;

    /***
     * 待付款订单的倒计时
     */
    private Long leftTime;

    /**
     * 支付渠道：1:余额 2:微信 3：支付宝
     */
    private Integer payChannel;

    /**
     * 优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）
     */
    private Integer discountType;

    /**
     * 优惠id（优惠券类型为优惠券id，其他为相应的用户任务id）
     */
    private Long discountId;

    /**
     * 优惠金额
     */
    private BigDecimal discountAmount;

    /**
     * 评价状态
     */
    private String commentStatus;

}
