package mf.code.teacher.constants;

/**
 * mf.code.teacher.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午5:29
 */
public enum TeacherOrderBizTypeEnum {
	/**
	 * 招募奖金
	 */
	RECRUIT_MERCHANT_INCOME(1, "招募奖金"),
	/**
	 * 推荐奖金
	 */
	RECOMMAND_TEACHER_INCOME(2, "推荐奖金"),
	QUARTERLY_DIVIDEND(3, "季度分红"),
	ANNUAL_DIVIDEND(4, "年度分红"),
	PROMOTION_BONUS(5, "晋升分红"),
	TRADING_COMMISSION(6, "交易分佣"),
	;

	/**
	 * code
	 */
	private Integer code;
	/**
	 * 描述
	 */
	private String desc;

	TeacherOrderBizTypeEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public static String findByDesc(Integer code){
		for (TeacherOrderBizTypeEnum typeEnum : TeacherOrderBizTypeEnum.values()) {
			if (typeEnum.getCode().equals(code)) {
				return typeEnum.getDesc();
			}
		}
		return "";
	}
}
