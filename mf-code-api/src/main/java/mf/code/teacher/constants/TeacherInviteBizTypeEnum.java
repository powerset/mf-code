package mf.code.teacher.constants;

/**
 * mf.code.teacher.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午4:23
 */
public enum TeacherInviteBizTypeEnum {
	/**
	 * 普通访客
	 */
	RECRUIT_MERCHANT(1, "招募商户"),
	/**
	 * 集客师
	 */
	RECOMMAND_TEACHER(2, "推荐集客师")
	;

	/**
	 * code
	 */
	private Integer code;
	/**
	 * 描述
	 */
	private String desc;

	TeacherInviteBizTypeEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
}
