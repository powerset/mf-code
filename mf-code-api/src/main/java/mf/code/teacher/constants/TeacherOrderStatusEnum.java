package mf.code.teacher.constants;

/**
 * mf.code.teacher.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午5:29
 */
public enum TeacherOrderStatusEnum {
    //订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）',
    ORDERING(0, "下单中"),
    ORDERED(1, "下单成功"),
    FAILED(2, "订单失效"),
    TIMEOUT(3, "订单超时（只存在逻辑）"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 描述
     */
    private String desc;

    TeacherOrderStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
