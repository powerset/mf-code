package mf.code.teacher.constants;

/**
 * mf.code.teacher.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午5:29
 */
public enum TeacherOrderTypeEnum {
    //0：用户提现 1：平台金额充值 2:平台退款3:用户支付',
    USERCASH(0, "用户提现"),
    PALTFORMRECHARGE(1, "平台充值"),
    PALTFORMREFUND(2, "平台退款"),
    USERPAY(3, "用户支付"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 描述
     */
    private String desc;

    TeacherOrderTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
