package mf.code.teacher.constants;

/**
 * mf.code.teacher.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午5:29
 */
public enum TeacherOrderPayTypeEnum {
    //0：用户提现 1：平台金额充值 2:平台退款3:用户支付',
    CASH(1, "余额"),
    WEIXIN(2, "微信"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 描述
     */
    private String desc;

    TeacherOrderPayTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
