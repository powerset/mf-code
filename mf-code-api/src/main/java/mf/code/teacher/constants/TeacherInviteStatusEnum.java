package mf.code.teacher.constants;

/**
 * mf.code.teacher.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 下午4:05
 */
public enum TeacherInviteStatusEnum {
	/**
	 * 邀请未生效
	 */
	INEFFECTIVE(0, "邀请未生效"),
	/**
	 * 邀请已生效
	 */
	EFFECTIVE(1, "邀请已生效")
	;

	/**
	 * code
	 */
	private Integer code;
	/**
	 * 描述
	 */
	private String desc;

	TeacherInviteStatusEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
}
