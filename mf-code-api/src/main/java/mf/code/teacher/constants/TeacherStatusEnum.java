package mf.code.teacher.constants;

/**
 * mf.code.teacher.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月28日 18:11
 */
public enum TeacherStatusEnum {
    //申请集客师的审核状态：-2认证失败，0未提交，1认证中，2认证成功
    /**
     * 认证失败
     */
    FAILED(-2, "认证失败"),
    /**
     * 认证失败
     */
    BLACK(-1, "认证失败"),
    /**
     * 未提交
     */
    INIT(0, "未提交"),
    /**
     * 认证中
     */
    ING(1, "认证中"),
    /**
     * 认证成功
     */
    SUCCESS(2, "认证成功"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 描述
     */
    private String desc;

    TeacherStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
