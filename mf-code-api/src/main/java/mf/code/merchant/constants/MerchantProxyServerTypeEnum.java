package mf.code.merchant.constants;

/**
 * mf.code.merchant.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月20日 15:40
 */
public enum MerchantProxyServerTypeEnum {
    FENDUODUO(1, "粉多多"),
    ;

    private int code;
    private String message;

    MerchantProxyServerTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static MerchantProxyServerTypeEnum findByCode(int code) {
        for (MerchantProxyServerTypeEnum typeEnum : MerchantProxyServerTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
