package mf.code.merchant.constants;

public enum OrderTypeEnum {
    USERCASH(0, "商户提现"),
    PALTFORMRECHARGE_PAY(1, "商户-付款"),
    PALTFORMRECHARGE_PRESTORE(2, "商户-存钱账户"),
        PALTFORMREFUND(3, "商户退款"),
        ;
        private int code;
        private String message;

        OrderTypeEnum(int code, String message) {
            this.code = code;
            this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
