package mf.code.merchant.constants;

public enum MfTradeTypeEnum {
    //:1支付保证金 2保证金结算退回 3补库存 4商品售出待结算 5商品售出已结算
    PAY_ORDER(1, "支付保证金"),
    ORDER_REFUND(2, "保证金结算退回"),
    ADD_STOCK(3, "补库存"),
    GOODS_SALE_WILL_CLEANING(4, "商品售出待结算"),
    GOODS_SALE_CLEANINGED(5, "商品售出已结算"),
    GOODS_SALE_REFUND(6, "仅退款"),
    GOODS_SALE_REFUND_AND_GOODS(7, "退货退款"),
    CASH(8, "余额提现"),
    GOODS_TRADE(9, "商品交易"),
    SHOP_MANAGER_PAY(10, "店长缴纳金分配收益"),
    OPEN_SHOP(11, "开通店铺"),
    GOODS_SALE_CLEANING_INTO_BALANCE(12, "商品售出已结算-转到余额(存在结算周期的)"),
    ;

    private int code;
    private String message;

    MfTradeTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String findByDesc(Integer code) {
        for (MfTradeTypeEnum typeEnum : MfTradeTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return "";
    }
}
