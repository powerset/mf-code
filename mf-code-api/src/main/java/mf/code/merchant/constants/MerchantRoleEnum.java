package mf.code.merchant.constants;

/**
 * mf.code.merchant.constants
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 17:19
 */
public enum MerchantRoleEnum {
    JKMF(0, "集客魔方商户"),
    DOU_DAI_DAI(1, "抖带带主播"),
    ;
    private int code;
    private String message;

    MerchantRoleEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
