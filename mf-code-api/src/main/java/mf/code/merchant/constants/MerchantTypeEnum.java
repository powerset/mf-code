package mf.code.merchant.constants;

/**
 * mf.code.merchant.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月20日 17:20
 */
public enum MerchantTypeEnum {
    //账号类型（0:父账号，1:子账号）
    PARERNT_MERCHANT(0, "父账号"),
    CHILD_OPERATE(1, "运营子账号"),
    CHILD_ALL(2, "全职子账号"),
    ;

    private int code;
    private String message;

    MerchantTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static MerchantTypeEnum findByCode(int code){
        for (MerchantTypeEnum typeEnum : MerchantTypeEnum.values()) {
            if(typeEnum.getCode() == code){
                return typeEnum;
            }
        }
        return null;
    }
}
