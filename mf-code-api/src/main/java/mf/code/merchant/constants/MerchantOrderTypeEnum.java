package mf.code.merchant.constants;

/**
 * create by qc on 2019/8/9 0009
 */
public enum MerchantOrderTypeEnum {


    DRAW(0, "商户提现"),
    MERCHANT_PAY(1, "商户-付款"),
    MERCHANT_INCOME(2, "商户-存钱账"),
    MERCHANT_REFUND(3, "商户退款"),
    ;
    private int code;
    private String message;

    MerchantOrderTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
