package mf.code.merchant.constants;

public enum WxTradeTypeEnum {
    JSAPI(1,"公众号支付"),
    NATIVE(2,"原生扫码支付"),
    APP(3,"app支付"),
    MKTTRANSFER(4,"企业付款");

    private int code;
    private String  message;

    WxTradeTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
