package mf.code.merchant.constants;

public enum OrderPayTypeEnum {
    CASH(1, "余额(预存金余额)"),
    WEIXIN(2, "微信"),
    ALIPAY(3, "支付宝"),
    ;
    private int code;
    private String message;

    OrderPayTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
