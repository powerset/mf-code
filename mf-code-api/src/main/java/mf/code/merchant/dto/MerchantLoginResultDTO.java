package mf.code.merchant.dto;

import lombok.Data;

/**
 * mf.code.merchant.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 19:39
 */
@Data
public class MerchantLoginResultDTO {
    /**
     * 商户id
     */
    private Long merchantId;
    /**
     * 商户手机号
     */
    private String phone;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户昵称
     */
    private String name;
    /**
     * 商户类型（0:父账号，1:运营子账号，2:全职能子账号）
     */
    private Integer type;
    /**
     * 是否弹窗(0:否，1:是)
     */
    private Integer firstDialog;
    /**
     * 是否订购(0:否，1:是)
     */
    private Integer isPurchase;
    /**
     * 是否创建店铺(0:否，1:是)
     */
    private Integer isCreateShop;

    /**
     * 商户创建时间
     */
    private Long create;

}
