package mf.code.merchant.dto;

import lombok.Data;

/**
 * mf.code.api.seller.dto
 * Description:
 *
 * @author: gel
 * @date: 2018-10-26 10:56
 */
@Data
public class RegisterReq {
    private String phone;
    private String smsCode;
    private String password;
}
