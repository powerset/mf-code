package mf.code.merchant.dto;

import lombok.Data;

/**
 * mf.code.merchant.dto
 * Description: 登录请求参数
 *
 * @author gel
 * @date 2019-04-02 19:16
 */
@Data
public class MerchantRegisterDTO {

    /**
     * 手机号
     */
    private String phone;
    /**
     * password
     */
    private String password;
    /**
     * 短信验证码
     */
    private String smsCode;

}
