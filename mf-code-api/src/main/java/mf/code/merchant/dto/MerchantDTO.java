package mf.code.merchant.dto;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.merchant.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-07 16:20
 */
@Data
public class MerchantDTO {
    /**
     * 商户id
     */
    private Long id;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 状态: 0: 创建 1：喜销宝服务调用成功 -1：喜销宝服务调用失败
     */
    private Integer status;

    /**
     * 最后登录IP
     */
    private String lastLoginIp;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 登录错误次数
     */
    private Integer loginErrorCount;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 账号类型（0:父账号，1:运营子账号，2:全职能子账号）
     */
    private Integer type;

    /**
     * 主账号id
     */
    private Long parentId;

    /**
     * 商户帐号名称
     */
    private String name;

    /**
     * 用户授权状态，0:未授权，1:授权
     */
    private Integer grantStatus;

    /**
     * 用户授权时间
     */
    private Date grantTime;

    /**
     * 用户角色：默认0，集客魔方商户；1 抖带带主播
     */
    private Integer role;

    /**
     * 头像图片地址
     */
    private String avatarUrl;

    /**
     * 权限设置（待定）
     */
    private String permission;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer del;

    /**
     * 微信openId
     */
    private String openId;

    /**
     * 子账号启用状态（子账号才会使用，0：未启用，1：启用）
     */
    private Integer subStatus;

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版[{"version":1},{"version":2}]
     */
    private String purchaseVersionJson;
}
