package mf.code.merchant.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import mf.code.common.dto.AppletMybatisPageDto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.merchant.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月14日 10:53
 */
@Data
public class AppletMerchantOrderPageDTO {
    /***
     * 分页信息
     */
    private AppletMybatisPageDto detail;
    /***
     * 累计佣金
     */
    private String totalCommission = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    /***
     * 总销售金额
     */
    private String totalSale = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    /***
     * 成交笔数
     */
    private int dealNum;
    /***
     * 可提现余额
     */
    private String balance = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    /***
     * 待结算佣金
     */
    private String willCleaning = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

    /***
     * 本月
     */
    private String thisMonth;
    /***
     * 下月
     */
    private String nextMonth;

    @Data
    public static class AppletMerchantOrderDTO {
        /***
         * 时间
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date ctime;
        /***
         * 订单号
         */
        private String orderNo;
        /***
         * 销售额
         */
        private String sale;
        /***
         * 金额
         */
        private String money;
        /***
         * 类型
         */
        private int type;


    }
}
