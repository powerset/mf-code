package mf.code.merchant.dto;

import lombok.Data;

/**
 * mf.code.api.seller.dto
 * Description:
 *
 * @author: gel
 * @date: 2018-10-25 19:48
 */
@Data
public class SmsReq {

    private String picCode;
    private String phone;
}
