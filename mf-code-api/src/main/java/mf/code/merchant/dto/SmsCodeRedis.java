package mf.code.merchant.dto;

import lombok.Data;

/**
 * mf.code.api.seller.dto
 * Description:
 *
 * @author: gel
 * @date: 2018-10-26 9:10
 */
@Data
public class SmsCodeRedis {
    private String smsCode;
    private String now;

}
