package mf.code.merchant.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.merchant.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-13 11:45
 */
@Data
public class MerchantOrderReq {
	/**
	 *
	 */
	private Long id;

	/**
	 * 商户号-主账号
	 */
	private Long merchantId;

	/**
	 * 店铺号
	 */
	private Long shopId;

	/**
	 * 店铺名
	 */
	private String shopName;

	/**
	 * 各员工之间的金额分摊json数据，格式：[{"customer_id":1,"deposit_cash":0.12,"deposit_advance":0.13},{"customer_id":1,"deposit_cash":0.12,"deposit_advance":0.13}]
	 */
	private String customerJson;

	/**
	 * 订单类型 0：商户提现 1：商户-付款 2：商户-存钱账户 3:商户退款
	 */
	private Integer type;

	/**
	 * 支付方式：1:余额(预存金余额) 2:微信 3：支付宝
	 */
	private Integer payType;

	/**
	 * 订单编号 本系统的订单编号
	 */
	private String orderNo;

	/**
	 * 订单名称
	 */
	private String orderName;

	/**
	 * 公众号appid 冗余字段
	 */
	private String pubAppId;

	/**
	 * 订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）
	 */
	private Integer status;

	/**
	 * 总金额,单位元
	 */
	private BigDecimal totalFee;

	/**
	 * 交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
	 */
	private Integer tradeType;

	/**
	 * ip地址
	 */
	private String ipAddress;

	/**
	 * 流水号 微信流水号唯一标识
	 */
	private String tradeId;

	/**
	 *
	 */
	private String remark;

	/**
	 * 回调处理系统时间
	 */
	private Date notifyTime;

	/**
	 * 响应数据中的支付时间
	 */
	private Date paymentTime;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 更新时间
	 */
	private Date utime;

	/**
	 * 订单入参，json对象
	 */
	private String reqJson;

	/**
	 * 业务类型，1：计划内活动，2：赠品活动，3:轮盘抽奖，4:订单抽奖活动，5:红包活动，6.广告,7.活动定义审核,8:"拆红包",9:粉多多
	 */
	private Integer bizType;

	/**
	 * 业务类型针对的值1:活动计划编号，2:活动定义编号，3:活动定义编号，4：活动定义编号，5:活动定义编号，6：广告位的主键编号,7:活动定义审核主键编号,8:"活动定义编号",9:0数字
	 */
	private Long bizValue;

	/**
	 * 退款时，需要退款的订单主键编号
	 */
	private Long refundOrderId;

	/**
	 * 魔方交易方式:1支付保证金 2保证金结算退回 3补库存
	 */
	private Integer mfTradeType;

	/**
	 * 支付人的openid
	 */
	private String payOpenId;
}
