package mf.code.merchant.dto;

import lombok.Data;

/**
 * mf.code.merchant.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-12 17:25
 */
@Data
public class JkmfPurchaseDTO {
    /**
     * "bizVersion":1,"money":"0.01","initMoney":"884.00","purchaseCycle":12,"name":"购买公有版小程序1年","rate":100
     */
    private Integer bizVersion;
    private String money;
    private String initMoney;
    private Integer purchaseCycle;
    private String name;
    private Integer rate;
}
