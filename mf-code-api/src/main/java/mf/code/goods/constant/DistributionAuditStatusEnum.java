package mf.code.goods.constant;

/**
 * mf.code.order.constant
 * Description:
 *
 * @author gel
 * @date 2019-04-13 17:08
 */
public enum DistributionAuditStatusEnum {
    /**
     * 未审核
     */
    INIT(0, "未审核"),
    /**
     * 审核通过
     */
    SUCCESS(1, "审核通过"),
    /**
     * 审核不通过
     */
    FAIL(-1, "审核不通过"),
    /**
     * 废弃
     */
    DISCARD(-2, "废弃"),
    ;
    private int code;
    private String message;

    DistributionAuditStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static DistributionAuditStatusEnum findByCode(Integer code) {
        for (DistributionAuditStatusEnum typeEnum : DistributionAuditStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
