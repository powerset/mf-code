package mf.code.goods.constant;


/**
 * create by qc on 2019/4/9 0009
 */
public class ProductConstant {
    public static class StatusFrontShow {
        /**
         * 上架 在售
         */
        public static final int ON_SALE = 1;
        /**
         * 上架审核中
         */
        public static final int CHECKING = 2;

        /**
         * 仓库中
         */
        public static final int IN_STORE = 3;

        /**
         * 审核失败
         */
        public static final int CHECK_FAILD = 4;

        /**
         * 供应商下架
         */
        public static final int PLAT_OFF_SHELF = -1;

    }

    public static class DistributionMode {
        /**
         * 1 店铺分销、2 店铺+平台、3 平台分销、4 无
         */
        public static final int SHOP = 1;

        public static final int SHOP_AND_PLAT = 2;

        public static final int PLAT = 3;

        public static final int NONE = 4;
    }

    public static class Status {
        /**
         * 上架 在售
         */
        public static final int ON_SALE = 1;
        /**
         * 下架 仓库中
         */
        public static final int NOT_ON_SALE = 0;
        /**
         * 抖带带上架中商品
         */
        public static final int DDD_ON_SALE = 10;
        /**
         * 下架 供应商下架
         */
        public static final int PLAT_OFF_SHELF = -1;
        /**
         * 售罄
         */
        public static final int SELL_OUT = -2;
    }

    public static class CheckStatus {
        /**
         * 默认状态 未提交审核
         */
        public static final int NOT_COMMIT = -1;
        public static final int NOT_PASS = -1;
        /**
         * 审核通过
         */
        public static final int PASS = 1;
        /**
         * 提交审核 但 未审审核
         */
        public static final int CHECKING = 0;


    }

    public static class DelStatus {
        /**
         * 已经被删除
         */
        public static final int HAS_DELETED = 1;

        /**
         * 正常
         */
        public static final int NOT_DELETED = 0;
    }

    /**
     * sku静态属性
     */
    public static class ProductSku {
        /**
         * 库存相关
         */
        public static class Stock {
            /**
             * 库存变更相关
             */
            public static class ProductSkuStockLog {
                /**
                 * 商户
                 */
                public static final Integer MERCHANT = 1;
                /**
                 * c端用户 消费
                 */
                public static final Integer CONSUMER = 2;
                /**
                 * c端用户 退库存
                 */
                public static final Integer GIVE_BACK = 3;
            }
        }

        /**
         * 快照
         */
        public static class Snapshoot {
            public static final Integer IS_SNAPSHOOT = 1;
            public static final Integer NOT_SNAPSHOOT = 0;
        }

        /**
         * 是否展示sku 0 显示(默认显示)； 1 不显示；2秒杀sku类型
         * （注意秒杀sku因为业务需要，不能展示在前端，并且要区分统计，所以直接添加一个类型）
         */
        public static class NotShow {
            public static final int SHOW = 0;
            public static final int NOT_SHOW = 1;
            public static final int FLASH_SALE = 2;
            public static final int PLANTFORM_ORDER_BACK = 3;
        }

    }

    /**
     * 分销
     */
    public static class Distribution {
        /**
         * 分销审核
         */
        public static final class Audit {
            /**
             * 审核状态
             */
            public static final class Status {
                //待审核
                public static final int WAITING_FOR_AUDIT = 0;
                //通过
                public static final int PASS = 1;
                //不通过
                public static final int NOT_PASS = -1;
                //分期的分销设置
                public static final int ABANDON = -2;
            }
        }
    }

}
