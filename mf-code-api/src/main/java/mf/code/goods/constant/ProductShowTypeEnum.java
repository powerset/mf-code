package mf.code.goods.constant;

/**
 * mf.code.goods.constant
 * Description:
 *
 * @author gel
 * @date 2019-05-15 15:52
 */
public enum ProductShowTypeEnum {

    /**
     * 无属性
     */
    NONE(0, "无"),
    /**
     * 新手专区
     */
    NEWBIE(1, "新手专区"),
    ;

    private int code;
    private String message;

    ProductShowTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ProductShowTypeEnum findByCode(Integer code) {
        for (ProductShowTypeEnum typeEnum : ProductShowTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
