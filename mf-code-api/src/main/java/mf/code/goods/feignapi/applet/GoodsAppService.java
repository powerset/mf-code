package mf.code.goods.feignapi.applet;//package mf.code.goods.feignclient.applet;
//
//import mf.code.common.dto.AppletMybatisPageDto;
//import mf.code.common.simpleresp.SimpleResponse;
//import mf.code.distribution.feignclient.dto.DistributionDTO;
//import mf.code.goods.dto.GoodProductDTO;
//import mf.code.goods.dto.GoodsSkuDTO;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import java.util.List;
//
///**
// * mf.code.goods.feignclient.applet
// * Description:
// *
// * @author: 百川
// * @date: 2019-04-03 14:10
// */
//@FeignClient(name = "mf-code-goods"//, fallbackFactory = GoodsAppFeignFallbackFactory.class
//)
//public interface GoodsAppService {
//    @GetMapping("/feignclient/goods/applet/v5/queryProductSku")
//    GoodsSkuDTO queryProductSku(@RequestParam("merchantId") Long merchantId,
//                                @RequestParam("shopId") Long shopId,
//                                @RequestParam("skuId") Long skuId,
//                                @RequestParam("userId") Long userId);
//
//    @GetMapping("/feignclient/goods/applet/v5/queryProduct")
//    GoodProductDTO queryProduct(@RequestParam("merchantId") Long merchantId,
//                                @RequestParam("shopId") Long shopId,
//                                @RequestParam("goodsId") Long goodsId);
//
//    /**
//     * 分页展示  所有可售商品
//     *
//     * @param shopId
//     * @return
//     */
//    @GetMapping("/feignclient/goods/applet/v5/pageGoodsSale")
//    AppletMybatisPageDto pageGoodsSale(@RequestParam("shopId") Long shopId,
//                                       @RequestParam(value = "offset", defaultValue = "0") Long offset,
//                                       @RequestParam(value = "size", defaultValue = "16") Long size);
//
//    /**
//     * 获取 商品分销属性
//     *
//     * @param goodsId
//     * @return
//     */
//    @GetMapping("/feignclient/goods/applet/v5/queryGoodsDistribution")
//    DistributionDTO queryGoodsDistribution(@RequestParam("goodsId") String goodsId);
//
//    /**
//     * 商户根据skuId列表查询商品信息
//     *
//     * @param skuIdList
//     * @return
//     */
//    @GetMapping("/feignclient/goods/seller/v5/queryProductSkuMapBySkuIdList")
//    SimpleResponse queryProductSkuMapBySkuIdList(@RequestParam("skuIdList") List<Long> skuIdList);
//
//    /**
//     * 库存消费
//     *
//     * @param productSkuId 产品的skuid
//     * @param stockNumber  消耗库存的数量
//     * @return SimpleResponse
//     */
//    @PostMapping("/feignclient/goods/seller/v5/consumeStock")
//    SimpleResponse consumeStock(@RequestParam("productSkuId") Long productSkuId,
//                                @RequestParam("stockNumber") int stockNumber);
//
//    /**
//     * 还库存
//     *
//     * @param productSkuId 产品的skuid
//     * @param stockNumber  消耗库存的数量
//     * @return SimpleResponse
//     */
//    @PostMapping("/feignclient/goods/seller/v5/giveBackStock")
//    SimpleResponse giveBackStock(@RequestParam("productSkuId") Long productSkuId, @RequestParam("stockNumber") int stockNumber);
//}
