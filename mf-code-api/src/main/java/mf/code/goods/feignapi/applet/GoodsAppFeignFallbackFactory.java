package mf.code.goods.feignapi.applet;//package mf.code.goods.feignclient.applet;
//
//import feign.hystrix.FallbackFactory;
//import lombok.extern.slf4j.Slf4j;
//import mf.code.common.dto.AppletMybatisPageDto;
//import mf.code.common.simpleresp.SimpleResponse;
//import mf.code.distribution.feignclient.dto.DistributionDTO;
//import mf.code.goods.dto.GoodProductDTO;
//import mf.code.goods.dto.GoodsSkuDTO;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
///**
// * mf.code.goods.feignclient.applet
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年04月13日 11:10
// */
//@Component
//@Slf4j
//public class GoodsAppFeignFallbackFactory implements FallbackFactory<GoodsAppService> {
//    @Override
//    public GoodsAppService create(Throwable cause) {
//        return new GoodsAppService() {
//            @Override
//            public GoodsSkuDTO queryProductSku(Long merchantId, Long shopId, Long skuId, Long userId) {
//                return new GoodsSkuDTO();
//            }
//
//            @Override
//            public GoodProductDTO queryProduct(Long merchantId, Long shopId, Long goodsId) {
//                return new GoodProductDTO();
//            }
//
//            @Override
//            public AppletMybatisPageDto pageGoodsSale(String shopId, Long offset, Long size) {
//                return new AppletMybatisPageDto();
//            }
//
//            @Override
//            public DistributionDTO queryGoodsDistribution(String goodsId) {
//                return new DistributionDTO();
//            }
//
//            @Override
//            public SimpleResponse queryProductSkuMapBySkuIdList(List<Long> skuIdList) {
//                return null;
//            }
//        };
//    }
//}
