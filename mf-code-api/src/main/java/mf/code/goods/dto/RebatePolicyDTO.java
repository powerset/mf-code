package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.feignclient.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-04 14:27
 */
@Data
public class RebatePolicyDTO {
	/**
	 * 返利 比率 依据条件
	 */
	private Integer amountCondition;
	/**
	 * 返利比率 0-100
	 */
	private Integer rebateRate;
	/**
	 * 贡献比率 0-100
	 */
	private Integer commissionRate;
	/**
	 * 贡献层级
	 */
	private Integer scale;
}
