package mf.code.goods.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-28 10:15
 */
@Data
public class ProductEntity {
    /**
     *
     */
    private Long id;

    /**
     * 上级商品id（从哪个商品派生的）
     * 情况1 id=parent_id 自营创建
     * 情况2 id!=parent_id 来自分销市场
     */
    private Long parentId;

    /**
     * 商户id（所属商户）
     */
    private Long merchantId;

    /**
     * 店铺id（所属店铺）
     */
    private Long shopId;

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 商品类目 为category_taobao.id
     */
    private Long productSpecs;

    /**
     * 商品描述(冗余 暂不用)
     */
    private String productDetail;

    /**
     * 商品图（主图级轮播图） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    private String mainPics;

    /**
     * 详情图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    private String detailPics;

    /**
     * 价格区间最小值 (冗余 暂不用)
     */
    private BigDecimal productPriceMin;

    /**
     * 价格区间 最大值  (冗余 暂不用)
     */
    private BigDecimal productPriceMax;

    /**
     * 淘宝价格区间 最小值 冗余(暂不用)
     */
    private BigDecimal thirdPriceMin;

    /**
     * 淘宝价格区间 最大值  冗余(暂不用)
     */
    private BigDecimal thirdPriceMax;

    /**
     * 展示类型（0：无，1：新人）
     */
    private Integer showType;

    /**
     * 状态 1:上架,0:未上架(默认值) -1:供应商下架
     */
    private Integer status;

    /**
     * 审核状态 -1: 初始状态(默认值) 0: 待审核 1:审核通过
     */
    private Integer checkStatus;

    /**
     * 审核原因
     */
    private String checkReason;

    /**
     * 审核不通过,上传图片,最多三张 非json格式,逗号隔开
     */
    private String checkReasonPic;

    /**
     * 审核时间
     */
    private Date checkTime;

    /**
     * 提交审核时间
     */
    private Date submitTime;

    /**
     * 顺序
     */
    private Long sort;

    /**
     * 自营分销佣金比例 例: 1% 此字段值为:1
     */
    private BigDecimal selfSupportRatio;

    /**
     * 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     */
    private BigDecimal platSupportRatio;

    /**
     * 上架时间
     */
    private Date putawayTime;

    /**
     * 商户填入初始销量
     */
    private Integer initSales;

    /**
     * 淘宝商品id
     */
    private String numIid;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 标记删除 0 :正常 1:删除
     */
    private Integer del;

    /**
     * 佣金（抽取固定抽佣比后的佣金）
     */
    private BigDecimal commissionNum;

    /**
     * 来源标识（0：无，1：自营，2:分销。判断条件是id是否等于parent_id,等于是自营，不等于是分销）
     */
    private Integer fromflag;


    /**
     * sku列表
     */
    private List<ProductSkuEntity> productSkuList;
    /**
     * 冗余 以skuId反查商品信息时的 skuId
     */
    private Long skuId;
}
