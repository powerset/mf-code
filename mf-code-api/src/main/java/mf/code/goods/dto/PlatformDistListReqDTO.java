package mf.code.goods.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-07 20:04
 */
@Data
public class PlatformDistListReqDTO {
    private String merchant;
    private String shopName;
    /**
     * 开始时间 格式 yyyy-MM-dd
     */
    private String submitTimeBegin;
    /**
     * 结束时间 格式 yyyy-MM-dd
     */
    private String submitTimeEnd;
    private Integer checkResult;
    private Integer type;
    private Integer page;
    private Integer size;
}
