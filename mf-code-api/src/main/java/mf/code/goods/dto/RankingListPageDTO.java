package mf.code.goods.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 通过排行榜进行 分页展示信息 的工具类
 * <per>
 *     使用场景1：小程序分页展示 在售商品信息：按销量倒序，销量相等按上架时间倒序
 *     商品信息包含：
 *          1. productId
 *          2. parentId
 *          3. productTitle
 *          4. mainPics
 *          5. selfSupportRatio
 *          6. platSupportRatio
 *          7. minPrice,
 *          8. minThirdPrice
 *          9. salesVolume
 *          10. putawayTime
 * </per>
 *
 * mf.code.goods.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-03 22:51
 */
@Data
public class RankingListPageDTO<T> {
	/**
	 * 排行榜信息 Map<Long, Integer> -- 对应排行榜的 key 和 scroe
	 */
	private Map<Long, Integer> rankingList = new LinkedHashMap<>();
	/**
	 * 内容信息 Map<Long, Object> -- 对应排行榜的 key 和 scroe
	 */
	private T content;
	/**
	 * 在售商品数
	 */
	private Integer total = 0;
	/**
	 * 偏移量
	 */
	private Long offset = 0L;
	/**
	 * 商品数
	 */
	private Long size = 0L;

	public RankingListPageDTO() {

	}

	public RankingListPageDTO(Long offset, Long size) {
		this.offset = offset;
		this.size = size;
	}
}
