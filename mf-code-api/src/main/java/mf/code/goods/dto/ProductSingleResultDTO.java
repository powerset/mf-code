package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-16 16:51
 */
@Data
public class ProductSingleResultDTO {

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 商品主图
     */
    private String productPic;

    /**
     * 商品id
     */
    private String productId;

    /**
     * 商品最大价格
     */
    private String productMax;

    /**
     * 商品最小价格
     */
    private String productMin;
}
