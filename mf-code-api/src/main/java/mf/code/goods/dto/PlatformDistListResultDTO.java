package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-08 17:11
 */
@Data
public class PlatformDistListResultDTO {
    private Long id;
    private Long merchantId;
    private Long shopId;
    /**
     * 提交时间
     */
    private String commitTime;
    /**
     * 商品主图
     */
    private String productPic;
    /**
     *
     * 商品名称
     */
    private String productName;
    /**
     * 商品id
     */
    private Long productId;
    /**
     * 价格 最高价
     */
    private String priceMin;
    /**
     * 价格 最低价
     */
    private String priceMax;
    /**
     * 平台分销比例
     */
    private String proportion;
    /**
     * 销量
     */
    private String salesVolume;
    /**
     * 剩余库存
     */
    private String surplusStock;
    /**
     * 商家账号
     */
    private String merchant;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 店长姓名
     */
    private String shopowner;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 经营地址
     */
    private String address;
    /**
     * 审核时间
     */
    private String checkTime;
    /**
     * 审核状态 1:通过 -1:不通过
     */
    private String checkStatus;
}
