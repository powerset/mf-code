package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-16 16:04
 */
@Data
public class ProductGroupReqDTO {

    private Long merchantId;
    private Long shopId;
    private Long productId;
    private Long parentProductId;
    /**
     * 分组id
     */
    private Long groupId;
    /**
     * 分组名
     */
    private String groupName;
    /**
     * 分组名
     */
    private Integer groupOrder;
}
