package mf.code.goods.dto;

import lombok.Data;

@Data
public class PlatBannerPicDTO {
    private String minPics;
    private String sid;
    private String type;
}
