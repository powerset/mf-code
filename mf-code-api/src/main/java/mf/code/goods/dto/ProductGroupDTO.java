package mf.code.goods.dto;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.goods.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月17日 14:53
 */
@Data
public class ProductGroupDTO {
    /**
     * 商品分组id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 分组名
     */
    private String name;

    /**
     * 分组排序商家编辑，默认为0。正序排列
     */
    private Integer sort;

    /**
     * 冗余商品数量，方便查询
     */
    private Integer goodsNum;

    /**
     * 删除标识
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;
}
