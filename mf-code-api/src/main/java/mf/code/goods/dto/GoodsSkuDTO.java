package mf.code.goods.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.feignclient.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月08日 17:42
 */
@Data
public class GoodsSkuDTO {
    /***
     * sku商品名
     */
    private String title;
    private String pic;
    /***
     * 商品价格
     */
    private String price;
    /***
     * 商品原价
     */
    private String originalPrice;
    /***
     * 返现金额
     */
    private String cashback;
    /***
     * 商品编号
     */
    private Long goodsId;

    /***
     * sku商品信息
     */
    private List<Map> skus;

    /***
     * 商户编号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;

    /***
     * 库存数
     */
    private Integer stock;

    /***
     * sku编号
     */
    private Long skuId;

    /***
     * 是否展示sku 0 显示(默认显示)； 1 不显示；2秒杀sku类型
     */
    private Integer notShow;
}
