package mf.code.goods.dto;

import lombok.Data;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.feignclient.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 13:56
 */
@Data
public class GoodProductSkuDTO {
    /***
     * 商品主图
     */
    private List<String> goodsPics;
    /***
     *商品名
     */
    private String title;
    /***
     * 商品价格区间
     */
    private String price;
    /***
     * 商品原价区间
     */
    private String originalPrice;
    /***
     * 商品最小价格
     */
    private String minPrice;
    /***
     * 商品最小原价
     */
    private String minOriginalPrice;

    /***
     * 商品已售件数
     */
    private String saleQuantity;
    /***
     * 返现金额
     */
    private String cashback;
    /***
     * 商品类型：0, "无分销属性" 1, "自卖分销" 2, "平台分销" 3, "自营+平台分销" 4, "倒卖--平台获取分销"
     */
    private int goodsType;
    /***
     * 商品详细图片
     */
    private List<String> picDetail;
    /***
     * 商品编号
     */
    private Long goodsId;

    /***
     * 规格信息(sku规格列表)
     */
    private List<Map> items;

    /***
     * sku商品信息
     */
    private List<Map> skus;

    /***
     * 客服微信号
     */
    private String serviceWx;

    /***
     * 预估收益对象
     */
    private UserDistributionInfoDTO profit;

    /***
     * 商品上下架状态 0:已下架 非0：正常
     */
    private Integer status;

    private int stock;

    /**
     * 佣金信息
     */
    private BigDecimal commissionNum;
    /***
     * 活动信息
     */
    private Map activityInfo;
    /***
     * 折扣信息
     */
    private Map discountInfo;
    /**
     * 评价信息
     */
    private Map commentInfo;
}
