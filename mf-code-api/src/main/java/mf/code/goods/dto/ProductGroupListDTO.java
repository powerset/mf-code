package mf.code.goods.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.goods.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月17日 15:06
 */
@Data
public class ProductGroupListDTO {
    private List<ProductGroupDTO> groups;
}
