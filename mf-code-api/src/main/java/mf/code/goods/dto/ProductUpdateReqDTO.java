package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-15 15:37
 */
@Data
public class ProductUpdateReqDTO {

    private Long productId;
    private Long merchantId;
    private Long shopId;
    private Integer showType;
}
