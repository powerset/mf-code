package mf.code.goods.dto;/**
 * create by qc on 2019/4/8 0008
 */

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author gbf
 * 2019/4/8 0008、17:53
 */
@Data
public class ProductSkuSalePropDTO implements Serializable {
    private Long productSpecsId;
    private String productSpecsValue;
    //创建的业务dto传值使用
    private Long categoryId;
    //类目的名称id
    private Long specsNameId;

    public Long getProductSpecsId() {
        return productSpecsId;
    }

    public ProductSkuSalePropDTO setProductSpecsId(Long productSpecsId) {
        this.productSpecsId = productSpecsId;
        return this;
    }

    public String getProductSpecsValue() {
        return productSpecsValue;
    }

    public ProductSkuSalePropDTO setProductSpecsValue(String productSpecsValue) {
        this.productSpecsValue = productSpecsValue;
        return this;
    }
}
