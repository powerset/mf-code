package mf.code.goods.dto;/**
 * create by qc on 2019/8/26 0026
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品分销属性信息
 * <p>
 * 从分销服务中计算分销佣金具体数额和分销比例的
 *
 * @author gbf
 * 2019/8/26 0026、13:46
 */
@Data
public class ProductDistributCommissionEntity {
    /**
     * 分销市场 展示 平台分佣比例  百分比
     */
    private BigDecimal platDistributRadio;
    /**
     * 分销市场 展示 分销佣金所得
     */
    private BigDecimal platDistributCommission;
}
