package mf.code.goods.dto;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductSkuDTO implements Serializable {
    //修改需要此id
    private Long skuId;
    private Long productId;
    private String propsJson;
    private String skuName;
    private String skuPic;
    private BigDecimal skuSellingPrice;
    private BigDecimal skuThirdPrice;
    private Integer stock;
    private Integer snapshoot;
    private Integer notShow;
    /**
     * 经过解析的销售属性对象数组
     */
    private List<ProductSkuSalePropDTO> productSkuSalePropDTOList;

    public List<ProductSkuSalePropDTO> parsePropListFromString(String propsJson) {
        if (this.propsJson == null) return null;
        List<ProductSkuSalePropDTO> propList = null;
        try {
            propList = JSON.parseArray(propsJson, ProductSkuSalePropDTO.class);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return propList;
    }

    public Long getSkuId() {
        return skuId;
    }

    public ProductSkuDTO setSkuId(Long skuId) {
        this.skuId = skuId;
        return this;
    }

    public Long getProductId() {
        return productId;
    }

    public ProductSkuDTO setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    public String getPropsJson() {
        return propsJson;
    }

    public ProductSkuDTO setPropsJson(String propsJson) {
        this.propsJson = propsJson;
        return this;
    }

    public String getSkuName() {
        return skuName;
    }

    public ProductSkuDTO setSkuName(String skuName) {
        this.skuName = skuName;
        return this;
    }

    public String getSkuPic() {
        return skuPic;
    }

    public ProductSkuDTO setSkuPic(String skuPic) {
        this.skuPic = skuPic;
        return this;
    }

    public BigDecimal getSkuSellingPrice() {
        return skuSellingPrice;
    }

    public ProductSkuDTO setSkuSellingPrice(BigDecimal skuSellingPrice) {
        this.skuSellingPrice = skuSellingPrice;
        return this;
    }

    public BigDecimal getSkuThirdPrice() {
        return skuThirdPrice;
    }

    public ProductSkuDTO setSkuThirdPrice(BigDecimal skuThirdPrice) {
        this.skuThirdPrice = skuThirdPrice;
        return this;
    }

    public Integer getStock() {
        return stock;
    }

    public ProductSkuDTO setStock(Integer stock) {
        this.stock = stock;
        return this;
    }

    public Integer getSnapshoot() {
        return snapshoot;
    }

    public ProductSkuDTO setSnapshoot(Integer snapshoot) {
        this.snapshoot = snapshoot;
        return this;
    }

    public List<ProductSkuSalePropDTO> getProductSkuSalePropDTOList() {
        return productSkuSalePropDTOList;
    }

    public ProductSkuDTO setProductSkuSalePropDTOList(List<ProductSkuSalePropDTO> productSkuSalePropDTOList) {
        this.productSkuSalePropDTOList = productSkuSalePropDTOList;
        return this;
    }
}
