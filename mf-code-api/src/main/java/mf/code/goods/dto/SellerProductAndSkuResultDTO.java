package mf.code.goods.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-13 16:11
 */
@Data
public class SellerProductAndSkuResultDTO {

    private Long productId;
    private Long parentProductId;
    private Long skuId;
    /**
     * 商品来源:自营商品；分销商品
     */
    private String productSrc;
    /**
     * 商品标题
     */
    private String productTitle;
    /**
     * 商品主图
     */
    private String productSrcMainPics;
    /**
     * 商品属性
     */
    private String productSpecs;
    /**
     * 商品属性描述
     */
    private String productSpecsDesc;
    /**
     * 单价
     */
    private BigDecimal productPrice;
    /**
     * 商品显示属性，作用区分商品类型（秒杀，活动sku）
     */
    private Integer notShow;


}
