package mf.code.goods.dto;/**
 * create by qc on 2019/4/11 0011
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author gbf
 * 2019/4/11 0011、17:41
 */
@Data
public class ProductDTO implements Serializable {

    private Long productId;
    private Long merchantId;
    private Long shopId;
    /***
     * 商品主图 1<=size<=5
     */
    private String mainPics;
    private List<String> mainPicsList;
    /***
     * 详情图片 1<=size<10
     */
    private String detailPics;
    private List<String> detailPicsList;
    /***
     *商品名
     */
    private String productTitle;
    /***
     * 商品类目
     */
    private Long productSpecs;
    /***
     * 状态 1:上架 0:未上架
     */
    private Integer status;

    /**
     * 分销市场上点击按钮  1:上架 2:仓库 . 分销设置接口必穿参数
     */
    private int statInMarketWannaOnSale;
    /***
     * 审核状态
     */
    private Integer checkStatus;
    /***
     * 审核不通过原因
     */
    private String checkReason;
    /***
     * 上架时间
     */
    private Date putawayTime;
    /***
     * 自营分佣比例
     */
    private BigDecimal selfSupportRatio;
    /***
     * 平台分佣比例
     */
    private BigDecimal platSupportRatio;
    private Date ctime;
    private Date utime;
    private Integer del;
    /**
     * 经过解析的sku对象数组
     */
    private List<ProductSkuDTO> productSkuDTOList;
    /**
     * 前端给的json格式的字符串
     */
    private String sku;
    /**
     * 1:提交上架请求,平台审核上架 0:保存到仓库
     */
    private Integer putaway;
    /**
     * 审核不通过需要的图片
     */
    private String checkReasonPicsStr;

    /**
     * 商户自定义初始销量
     */
    private Integer initSales;

    /**
     * 商户自定义初始销量
     */
    private String numIid;
    /**
     * appType=0 || null 集客魔方
     * appType=1 抖带带
     */
    private Integer appType;
    /**
     * 商品最低售价
     */
    private BigDecimal minPrice;

    public List<ProductSkuDTO> parseSkuListFromString() {
        if (sku == null) {
            return null;
        }
        List<ProductSkuDTO> skuList = null;
        try {
            skuList = JSON.parseArray(sku, ProductSkuDTO.class);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return skuList;
    }

    public List<String> parsePicsList(String picStr) {
        if (picStr == null) {
            return null;
        }
        try {
            return JSON.parseArray(mainPics, String.class);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> parseCheckReasonPicsStr() {
        if (StringUtils.isBlank(checkReasonPicsStr)) {
            return null;
        }
        String[] picArr = checkReasonPicsStr.split(",");
        return Arrays.asList(picArr);
    }

}
