package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-07 19:23
 */
@Data
public class PlatformDistAuditReqDTO {
    private Long productId;
    private Long auditId;
    /**
     * 1:通过  2:不通过
     */
    private Integer checkStatus;
    /**
     * 审核不通过理由
     */
    private String checkReason;
    /**
     * 图片(最多三张) 字符串类型 ,逗号分隔
     */
    private String checkReasonPicsStr;
}
