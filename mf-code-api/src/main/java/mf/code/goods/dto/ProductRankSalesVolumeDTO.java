package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-13 14:11
 */
@Data
public class ProductRankSalesVolumeDTO {

    private Long merchantId;
    /**
     * 执行创建订单的店铺
     */
    private Long shopId;
    private Long userId;
    /**
     * 必填，goodsId是productId，而不是parentId
     */
    private Long goodsId;
    private Long skuId;
    /**
     * 商品数量
     */
    private Integer number;
    /**
     * 订单id不一定有
     */
    private Long orderId;
}
