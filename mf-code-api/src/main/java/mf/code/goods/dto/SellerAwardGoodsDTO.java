package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 *
 * @description: 奖品的商品信息
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年05月27日 16:38
 */
@Data
public class SellerAwardGoodsDTO {
    private Long id;
    private Long parentId;
    private String displayGoodsName;
    private String displayPrice;
    private String displayMinPrice;
    private String displayMaxPrice;
    private String displayBanner;
}
