package mf.code.goods.dto;

import lombok.Data;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-16 16:24
 */
@Data
public class ProductGroupListResultDTO {
    /**
     * 分组id
     */
    private String groupId;

    /**
     * 分组名
     */
    private String groupName;

    /**
     * 分组排序
     */
    private String groupOrder;

    /**
     * 分组商品数
     */
    private String groupGoodNum;
}
