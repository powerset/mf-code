package mf.code.goods.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-27 20:04
 */
@Data
public class CategoryDistributionDTO {

    /**
     * 名称
     */
    private String name;

    /**
     * 类目ID
     */
    private String sid;

    /**
     * 类目ID
     */
    private String parentId;

    /**
     * 类目层级
     */
    private Integer level;

    /**
     * 类目佣金比例
     */
    private BigDecimal distributionRatio;

    /**
     * 分组排序运营编辑，默认为0。正序排列
     */
    private Integer sort;

    /**
     * 零级类目需要展示的图片（目前小程序端使用）
     */
    private List<String> pics;
}
