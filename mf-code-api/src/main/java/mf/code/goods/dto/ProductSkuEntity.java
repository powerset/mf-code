package mf.code.goods.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-28 10:15
 */
@Data
public class ProductSkuEntity {
    /**
     *
     */
    private Long id;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 冗余 赞不写 如需商品名称,在product中获取
     */
    private String skuName;

    /**
     * sku图片 可以为空, 如果为空,获取商品主图的第一张
     */
    private String skuPic;

    /**
     * 售价
     */
    private BigDecimal skuSellingPrice;

    /**
     * 淘宝价
     */
    private BigDecimal skuThirdPrice;

    /**
     * 成本价格
     */
    private BigDecimal skuCostPrice;

    /**
     * 定义库存
     */
    private Integer stockDef;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 0 显示(默认显示)/ 1 不显示
     */
    private Integer notShow;

    /**
     * 是否在售 1:快照 0:非快照(默认)
     */
    private Integer snapshoot;

    /**
     * 计算生成的sku的展示顺序, 如果有2个属性就组成xxxyyy的数字,以此保证用户新增了一个属性的值,依然有序.
     * 例:
     * 原为:
     * 红 1  此sort值为: 100101
     * 红 2  此sort值为: 100102
     * 黑 1  此sort值为: 200101
     * 黑 2  此sort值为: 200102
     * 改为:
     * 红 1  此sort值为: 100101
     * 红 2  此sort值为: 100102
     * 红 3  此sort值为: 100103
     * 黑 1  此sort值为: 200101
     */
    private String skuSort;

    /**
     * 类目横向排序值 例:
     * 如果product_spec
     * 颜色id=1
     * 红色id=2
     * 黑色id=3
     * 尺寸=10
     * 1cm的id =11
     * 2cm的id =12
     * 红 1cm : 此值为  1:2,10:11
     */
    private String specSort;

    /**
     * 冗余 辅助校验
     */
    private String propsJson;

    /**
     * 生成快照后,会生成新的skuId,再次保留生成快照前的skuId
     */
    private Long oldSkuId;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 标记删除 0:正常 1:删除
     */
    private Integer del;
}
