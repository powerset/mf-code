package mf.code.goods.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.goods.feignclient.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 13:56
 */
@Data
public class GoodProductDTO {
    /***
     * 商品主图
     */
    private List<String> goodsPics;
    /***
     *商品名
     */
    private String title;
    /***
     * 商品价格
     */
    private String price;
    /***
     * 商品原价
     */
    private String originalPrice;
    /***
     * 商品已售件数
     */
    private int saleQuantity;
    /***
     * 返现金额
     */
    private String cashback;
    /***
     * 商品类型：
     * 0, "无分销属性"
     * 1, "自卖分销"
     * 2, "平台分销"
     * 3, "自营+平台分销"
     * 4, "倒卖--平台获取分销"
     */
    private int goodsType;
    /***
     * 商品详细图片
     */
    private List<String> picDetail;
    /***
     * 商品编号
     */
    private Long goodsId;

    /***
     * 商品的来源上级
     */
    private Long parentId;
    /***
     * 商品供货商的商户编号
     */
    private Long parentMerchantId;
    /***
     * 商品供货商的店铺编号
     */
    private Long parentShopId;

    /***
     * 商户编号
     */
    private Long merchantId;
    /***
     * 店铺编号
     */
    private Long shopId;

    /**
     * 商品类目 为category_taobao.sid
     */
    private Long productSpecs;

    /**
     * 自营分销佣金比例
     */
    private BigDecimal selfSupportRatio;

    /**
     * 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     */
    private BigDecimal platSupportRatio;

    /**
     * 展示类型（0：无，1：新人）
     */
    private Integer showType;

    /**
     * 佣金（抽取固定抽佣比后的佣金）
     */
    private BigDecimal commissionNum;
}
