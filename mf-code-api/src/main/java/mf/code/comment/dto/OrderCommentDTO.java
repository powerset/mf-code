package mf.code.comment.dto;

import lombok.Data;

import java.util.Date;

@Data
public class OrderCommentDTO {
    /**
     * 主键id
     */
    private Long id;

    /**
     *
     */
    private Long parentId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 商品的parentid
     */
    private Long goodsParentId;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 商品skuid
     */
    private Long skuId;

    /**
     * -1初始值,0买家已评论，1审核通过，2不通过
     */
    private Integer status;

    /**
     * 审核不通过的理由
     */
    private String reason;

    /**
     * 评论信息
     */
    private String contents;

    /**
     * 评论上传的图片信息，json字符串
     */
    private String pics;

    /**
     * 总评分
     */
    private Long totalStars;

    /**
     * 评论等级详情，json格式
     */
    private String commentStarsDetail;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 是否删除，0未删除，1已删除，默认是0
     */
    private Integer del;

    /**
     * 角色id 1用户，2商户，3平台
     */
    private Integer role;

    /*****************下面是附加信息*****************************************/

    /**
     * 商品名
     */
    private String goodsName;
    /**
     * 商品主图
     */
    private String goodsPic;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 用户名
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatarUrl;

    /**
     * 商家是否已回复：0未回复，1已回复
     */
    private Integer sellerReply;
}
