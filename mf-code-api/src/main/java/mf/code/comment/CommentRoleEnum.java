package mf.code.comment;

/**
 * 评论角色
 * yunshan
 */
public enum CommentRoleEnum {
    USER(1,"用户"),
    SELLER(2,"商户"),
    PLATE(3,"平台"),;

    private int code;
    private String  message;

    CommentRoleEnum(int code,String message){
        this.code=code;
        this.message=message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
