package mf.code.comment;

public enum CommentStepEnum {

    NOT_FINISH(0,"交易未完成"),
    USER_UNREPLY(1,"用户未评论"),
    USER_REPLY(2,"用户已评论"),
    SELLER_REPLY(3,"商家已评论"),
    FORBID_COMMENT(4,"禁止评论"),;

    private int code;
    private String  message;

    CommentStepEnum(int code,String message){
        this.code=code;
        this.message=message;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
