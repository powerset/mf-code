package mf.code.comment;

/**
 * mf.code.comment
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-31 15:13
 */
public enum CommentStatusEnum {

    /**
     * 初始状态
     */
    INIT(-1, "初始值"),
    /**
     * 0买家已评论
     */
    COMMENT(0, "0买家已评论"),
    /**
     * 审核通过
     */
    PASS(1, "审核通过"),
    /**
     * 不通过
     */
    FAIL(2, "不通过"),
    ;

    private int code;
    private String message;

    CommentStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
