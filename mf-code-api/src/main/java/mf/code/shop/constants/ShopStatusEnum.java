package mf.code.shop.constants;


/**
 * mf.code.merchant.constants
 * Description:
 *
 * @author gel
 * @date 2019-04-12 9:10
 */
public enum ShopStatusEnum {

    /**
     * 店铺状态（0：未创建，1：营业中，2：即将过期，3：已过期）
     */
    INIT(0, "未创建"),
    USED(1, "营业中"),
    EXPIRE_SOON(2, "即将过期"),
    EXPIRED(3, "已过期"),
    ;

    private int code;
    private String message;

    ShopStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ShopStatusEnum findByCode(int code) {
        for (ShopStatusEnum statusEnum : ShopStatusEnum.values()) {
            if (statusEnum.getCode() == code) {
                return statusEnum;
            }
        }
        return null;
    }
}
