package mf.code.shop.constants;

/**
 * mf.code.shop.constants
 * Description: 地区级别
 *
 * @author gel
 * @date 2019-04-22 11:57
 */
public enum AreaLevelEnum {
    /**
     *
     */
    province(1, "省"),
    /**
     * 京东
     */
    city(2, "地级市"),
    /**
     * 拼多多
     */
    area(3, "区/县"),
    /**
     * 拼多多
     */
    town(4, "乡镇"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    AreaLevelEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
