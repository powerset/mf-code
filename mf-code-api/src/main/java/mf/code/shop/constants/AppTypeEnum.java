package mf.code.shop.constants;/**
 * create by qc on 2019/8/13 0013
 */

/**
 * @author gbf
 * 2019/8/13 0013、14:27
 */
public enum  AppTypeEnum {
    JKMF(0, "集客魔方"),
    /**
     * 抖带带
     */
    DDD(1, "抖带带"),
    /***
     * 抖小铺
     */
    DXP(2, "抖小铺"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    AppTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

}
