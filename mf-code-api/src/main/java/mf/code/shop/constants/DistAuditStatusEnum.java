package mf.code.shop.constants;

public enum DistAuditStatusEnum {

    /**
     * 未审核
     */
    INIT(0, "未审核"),
    /**
     * 审核通过
     */
    SUCCESS(1, "审核通过"),
    /**
     * 审核失败
     */
    ERROR(-1, "审核失败"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    DistAuditStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
