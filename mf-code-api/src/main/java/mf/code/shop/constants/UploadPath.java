package mf.code.shop.constants;

/**
 * mf.code.shop.constants
 * Description:
 *
 * @author gel
 * @date 2019-04-03 14:25
 */
public class UploadPath {

    /**
     * 自定义店铺存储目录
     */
    public static final String SHOP_LOGO_PATH = "shop/logo/";
    /**
     * 二维码存储地址
     */
    public static final String SHOP_CSPIC_PATH = "shopqrcode/shop/wechat/";
    /**
     * 店铺banner存储地址
     */
    public static final String SHOP_BANNER_PATH = "shopbanner/shop/";
    /**
     * 活码地址-商户端
     */
    public static final String LIVE_CODE_MERCHANT_PATH = "livecode/merchant/";
    /**
     * 活码地址-小程序端
     */
    public static final String LIVE_CODE_APPLET_PATH = "livecode/applet/";
}
