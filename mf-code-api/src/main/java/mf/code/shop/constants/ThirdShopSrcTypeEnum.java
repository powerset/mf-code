package mf.code.shop.constants;

/**
 * mf.code.shop.constants
 * Description:
 *
 * @author gel
 * @date 2019-04-03 16:55
 */
public enum ThirdShopSrcTypeEnum {

    /**
     * 淘宝
     */
    TAOBAO(1, "淘宝"),
    /**
     * 京东
     */
    JD(2, "京东"),
    /**
     * 拼多多
     */
    PDD(3, "拼多多"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ThirdShopSrcTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
