package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:42
 */
@Data
public class ShopDetail {
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 自定义店标地址
     */
    private String customPicPath;
    /**
     * 自定义店铺名
     */
    private String customShopName;
    /**
     * 订购版本：0:不可用 1试用版 2公开版 3私有版
     */
    private String purchaseVersion;
    /**
     * 店铺状态（0：未创建，1：营业中，2：即将过期，3：已过期）
     */
    private Integer status;
    /**
     * 过期时间
     */
    private Long expireTime;
    /**
     * 是否是否需要弹窗（1: 需要弹窗，0:不需要弹窗）
     */
    private Integer firstDialog;
    /**
     * 类目id
     */
    private Long categoryId;
    /**
     * 类目名称
     */
    private String categoryName;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 店长名称
     */
    private String username;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 地址编码（省编码_市编码_区编码）
     */
    private String addressCode;
    /**
     * 地址文本（省名_市名_区名）
     */
    private String addressText;
    /**
     * 地址详情
     */
    private String addressDetail;
    /**
     * 第三方店铺列表
     */
    private List<ThirdShop> thirdShopList;

}
