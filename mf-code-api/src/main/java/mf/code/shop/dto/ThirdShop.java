package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:17
 */
@Data
public class ThirdShop {
    /**
     * 商户id
     */
    private Long merchantId;
    /**
     * 店铺名
     */
    private String shopName;
    /**
     * 店铺昵称
     */
    private String nick;
    /**
     * 店铺url
     */
    private String shopUrl;
    /**
     * 第三方数据服务（喜销宝）来源原始店标
     */
    private String thirdPicPath;
    /**
     * 店铺来源类型 1淘宝 2京东
     */
    private Integer srcType;
    /**
     * 第三方数据服务（喜销宝）截止时间
     */
    private Long thirdDeadline;
    /**
     * 第三方店铺id（目前仅有淘宝店铺，用于绑定淘宝店铺）
     */
    private Long thirdShopId;
    /**
     * 是否绑定喜销宝（1：是，0：否）
     */
    private Integer isBindTb;
}
