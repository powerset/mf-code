package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 19:56
 */
@Data
public class StarTeacher {
    /**
     * 头像地址
     */
    private String avatarUrl;
    /**
     * 讲师昵称
     */
    private String nickname;
    /**
     * 讲师头衔
     */
    private String title;
    /**
     * 粉丝商家数量
     */
    private String mMum;
}
