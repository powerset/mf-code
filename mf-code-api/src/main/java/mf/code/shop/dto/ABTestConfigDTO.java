package mf.code.shop.dto;/**
 * create by qc on 2019/7/1 0001
 */

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * 对前端apollo配置的支持
 *
 * @author gbf
 * 2019/7/1 0001、15:06
 */
@Data
public class ABTestConfigDTO {
    private String merchantId;
    private String featureStr;
    private String featureArrStr;

    /**
     * 获取配置了AbTest开关的功能
     *
     * @return String[]
     */
    public String[] getFeatureList() {
        if (StringUtils.isBlank(this.featureArrStr)) {
            return new String[0];
        }
        return featureArrStr.split(",");
    }
}
