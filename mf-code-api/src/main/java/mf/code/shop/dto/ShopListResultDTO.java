package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:40
 */
@Data
public class ShopListResultDTO {
    /**
     * 店铺列表
     */
    private List<ShopDetail> shopList;
}
