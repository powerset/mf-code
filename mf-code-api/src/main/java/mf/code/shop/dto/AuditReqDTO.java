package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-03 10:49
 */
@Data
public class AuditReqDTO {

    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 店铺Id
     */
    private String shopId;
    /**
     * 审核状态（0：不通过， 1：通过）
     */
    private String auditStatus;
    /**
     * 审核不通过原因
     */
    private String auditReason;
}
