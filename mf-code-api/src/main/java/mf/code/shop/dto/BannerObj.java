package mf.code.shop.dto;

import lombok.Data;

@Data
public class BannerObj {

    /**
     * 店铺banner图地址
     */
    private String bannerUrl;
    /**
     * banner图跳转路径
     */
    private String jumpUrl;
}
