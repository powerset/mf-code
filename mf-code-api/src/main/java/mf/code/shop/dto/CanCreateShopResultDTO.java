package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-12 20:32
 */
@Data
public class CanCreateShopResultDTO {
    private Integer canCreate;
    private ShopDetail shopDetail;
}
