package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-08 16:16
 */
@Data
public class AuditShopListResultDTO {

    private List<ShopDetail> shopList;
    private Integer page;
    private Integer size;
    private Long count;

}
