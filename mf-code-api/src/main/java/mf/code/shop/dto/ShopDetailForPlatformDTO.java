package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-05-08 16:19
 */
@Data
public class ShopDetailForPlatformDTO {
    /**
     * 商家店铺id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 卖家昵称
     */
    private String nick;

    /**
     * 商铺名
     */
    private String shopName;

    /**
     * 联系方式-店铺手机号
     */
    private String phone;

    /**
     * 店长姓名
     */
    private String username;

    /**
     * 地址详情
     */
    private String addressDetail;

    /**
     * 省市区
     */
    private String addressText;

    /**
     * 商家手机号，即商家登录帐号
     */
    private String merchantPhone;

    /**
     * 商家手机号，即商家登录帐号，方便兼顾客户端
     */
    private String merchant;

    /**
     * 开店地址addressText+addressDetail
     */
    private String address;

    /**
     * 店铺拥有者
     */
    private String shopowner;
    /**
     * 主图
     */
    private String picPath;
}
