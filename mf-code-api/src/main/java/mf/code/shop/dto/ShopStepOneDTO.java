package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:16
 */
@Data
public class ShopStepOneDTO {
    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 店铺id
     */
    private String shopId;
    /**
     * 店铺名
     */
    private String customShopName;
    /**
     * 类目
     */
    private String categoryId;
    /**
     * logo地址
     */
    private String customPicPath;
    /**
     * 第三方店铺列表（当前淘宝），如果没有不传，或者空列表
     */
//    private List<ThirdShop> thirdShopList;
    private String thirdShopList;

}
