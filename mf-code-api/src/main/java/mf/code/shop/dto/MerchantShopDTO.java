package mf.code.shop.dto;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-01 20:01
 */
@Data
public class MerchantShopDTO {

	/**
	 * 商家店铺id
	 */
	private Long id;

	/**
	 * 商户id
	 */
	private Long merchantId;

	/**
	 * 卖家昵称
	 */
	private String nick;

	/**
	 * 商铺名
	 */
	private String shopName;

	/**
	 * 类目id，品类表的sid
	 */
	private Long categoryId;

	/**
	 * 开店审核
	 */
	private Integer openstatus;

	/**
	 * 开店审核审核原因
	 */
	private String openreason;

	/**
	 * 分销资格审核
	 */
	private Integer diststatus;

	/**
	 * 分销资格审核原因
	 */
	private String distreason;

	/**
	 * 联系方式-微信号
	 */
	private String wechat;

	/**
	 * 联系方式-QQ
	 */
	private String qq;

	/**
	 * 联系方式-店铺手机号
	 */
	private String phone;

	/**
	 * 店长姓名
	 */
	private String username;

	/**
	 * 喜销宝服务接口响应数据
	 */
	private String xxbtopRes;

	/**
	 * 淘宝店标地址。返回相对路径，可以用"http://logo.taobao.com/shop-logo"来拼接成绝对路径
	 */
	private String xxbtopPicPath;

	/**
	 * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
	 */
	private String picPath;

	/**
	 * 地址详情
	 */
	private String addressDetail;

	/**
	 * 省市区
	 */
	private String addressText;

	/**
	 * 省市区
	 */
	private String addressCode;

	/**
	 * 店铺参数小程序码图片相对路径，可以用"TODO"来拼接成绝对路径
	 */
	private String wxacodePath;

	/**
	 * 包裹参数小程序码绝对路径
	 */
	private String packcodePath;

	/**
	 * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
	 */
	private String csJson;

	/**
	 * 店家群二维码
	 */
	private String groupCode;

	/**
	 * 店铺标题
	 */
	private String title;

	/**
	 * 删除标识
	 */
	private Integer del;

	/**
	 * 创建时间
	 */
	private Date ctime;

	/**
	 * 更新时间
	 */
	private Date utime;

	/**
	 * 喜销宝截止时间
	 */
	private Date xxbDeadline;

	/**
	 * 店铺banner图json格式：[{"bannerUrl":"http://sss","jumpUrl":"http://bbb"},{"bannerUrl":"http://sss","jumpUrl":"http://bbb"}]
	 */
	private String bannerJson;

	/**
	 * 是否展示客服号
	 */
	private Integer showFlag;

	/**
	 * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
	 */
	private Integer purchaseVersion;

	/**
	 * 内有订购周期：{"purchaseTime":2018-11-21 18:00,"expireTime":2018-11-21 18:00}
	 */
	private String purchaseJson;
}
