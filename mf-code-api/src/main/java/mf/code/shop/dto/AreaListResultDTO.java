package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-22 11:54
 */
@Data
public class AreaListResultDTO {
    /**
     * 地址列表
     */
    List<AreaDTO> areaList;
}
