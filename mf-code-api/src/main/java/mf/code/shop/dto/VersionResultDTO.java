package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:51
 */
@Data
public class VersionResultDTO {
    /**
     * 订购版本：0无 1试用版 2公开版 3私有版
     */
    private String purchaseVersion;
    /**
     * 订购时间
     */
    private String purchaseTime;
    /**
     * 订购过期时间
     */
    private String expireTime;
    /**
     * 小程序名称
     */
    private String widgetName;
    /**
     * 版本号
     */
    private String versionNumber;
    /**
     * 版本更新日志
     */
    private String versionText;
    /**
     * 店铺logo
     */
    private String shopLogo;
    /**
     * 小程序二维码
     */
    private String wxacodePath;
}
