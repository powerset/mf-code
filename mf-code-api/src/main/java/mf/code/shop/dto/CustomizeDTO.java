package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-03 10:28
 */
@Data
public class CustomizeDTO {
    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 店铺id
     */
    private String shopId;
    /**
     * 客服微信号
     */
    private String csWechat;
    /**
     * 客服二维码
     */
    private String csPic;
    /**
     * 店铺banner
     */
    private String banner;

    /***
     * 店铺群码
     */
    private String groupCode;

    /**
     * 店铺描述
     */
    private String title;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 店铺logo
     */
    private String picPath;

}
