package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 19:55
 */
@Data
public class StarTeacherResultDTO {

    /**
     * 明星讲师列表
     */
    private List<StarTeacher> starList;

}
