package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

@Data
public class CsJsonDTO {

    List<CsObj> csObjList;

}
