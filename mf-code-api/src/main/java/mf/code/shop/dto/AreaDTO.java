package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description: 地区数据
 *
 * @author gel
 * @date 2019-04-22 11:02
 */
@Data
public class AreaDTO {
    /**
     * 地区编号
     */
    private Long id;
    /**
     * 父级地区编码
     */
    private Long pid;
    /**
     * 地区名称
     */
    private String name;
    /**
     * 级别
     */
    private Integer level;
}
