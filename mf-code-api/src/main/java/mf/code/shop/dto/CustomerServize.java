package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 19:51
 */
@Data
public class CustomerServize {
    /**
     * 微信客服二维码图片地址
     */
    private String pic;
    /**
     * 微信号
     */
    private String wechat;
}
