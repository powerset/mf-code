package mf.code.shop.dto;

import lombok.Data;

@Data
public class CsObj {
    /**
     * 微信二维码图片
     */
    private String pic;
    /**
     * 微信号
     */
    private String wechat;
}
