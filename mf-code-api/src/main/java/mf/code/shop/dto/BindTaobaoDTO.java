package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-02 21:12
 */
@Data
public class BindTaobaoDTO {

    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 第三方店铺id
     */
    private String thirdShopId;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 昵称
     */
    private String nick;
}
