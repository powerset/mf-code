package mf.code.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.shop.dto
 * Description: 客服结构
 *
 * @author gel
 * @date 2019-04-02 19:48
 */
@Data
public class CustomerServiceResultDTO {

    /**
     * 客服热线
     */
    private String csHotline;
    /**
     * 客服列表（目前三个）
     */
    private List<CustomerServize> csList;
}
