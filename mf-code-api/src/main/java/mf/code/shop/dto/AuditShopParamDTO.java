package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-08 16:36
 */
@Data
public class AuditShopParamDTO {
    /**
     * 手机号
     */
    private String phone;
    /**
     * 店铺名
     */
    private String shopName;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 开店审核状态（1：审核通过， -1：审核不通过）
     */
    private Integer openStatus;
    /**
     * 页码
     */
    private Integer page;
    /**
     * 每页条数
     */
    private Integer size;
}
