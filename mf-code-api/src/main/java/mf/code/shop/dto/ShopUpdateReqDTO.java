package mf.code.shop.dto;

import lombok.Data;

/**
 * mf.code.shop.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-03 10:43
 */
@Data
public class ShopUpdateReqDTO {

    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 店铺id
     */
    private String shopId;
    /**
     * 店铺名
     */
    private String customShopName;
    /**
     * 类目
     */
    private String categoryId;
    /**
     * logo地址
     */
    private String customPicPath;
    /**
     * 店长名称
     */
    private String username;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 地址编码（省编码_市编码_区编码）
     */
    private String addressCode;
    /**
     * 地址文本（省名_市名_区名）
     */
    private String addressText;
    /**
     * 地址详情
     */
    private String addressDetail;
}
