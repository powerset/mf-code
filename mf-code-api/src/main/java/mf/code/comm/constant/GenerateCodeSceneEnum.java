package mf.code.comm.constant;

/**
 * mf.code.comm.constants
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月11日 11:44
 */
public enum GenerateCodeSceneEnum {
    /**
     *
     */
    GENERATE(1, "product"),

    SHAREPOSTER(2, "shareposter"),
    ;


    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    GenerateCodeSceneEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String findByDesc(Integer code) {
        for (GenerateCodeSceneEnum typeEnum : GenerateCodeSceneEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getDesc();
            }
        }
        return null;
    }
}
