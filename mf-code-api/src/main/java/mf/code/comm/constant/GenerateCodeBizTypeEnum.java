package mf.code.comm.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.comm.constants
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月11日 11:43
 */
public enum GenerateCodeBizTypeEnum {
    /**
     *
     */
    WEEK_EXTENSION(1, "周推"),
    SHOPMANAGER_SHARE(2, "店长分享"),
    ACTIVITY_SHARE(3, "活动分享"),
    MARKET_ENTER(4, "会场入口"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    GenerateCodeBizTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }


    public static boolean productScene(int bizType) {
        List<Integer> bizTypes = new ArrayList<>();
        bizTypes.add(WEEK_EXTENSION.getCode());
        bizTypes.add(ACTIVITY_SHARE.getCode());
        if (bizTypes.contains(bizType)) {
            return true;
        }
        return false;
    }

    public static String findByName(int bizType) {
        for (GenerateCodeBizTypeEnum typeEnum : GenerateCodeBizTypeEnum.values()) {
            if (typeEnum.getCode() == bizType) {
                return typeEnum.name().toLowerCase();
            }
        }
        return "";
    }
}
