package mf.code.comm.dto;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.comm.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-24 22:50
 */
@Data
public class CommonDictDTO {
    /**
     *
     */
    private Long id;

    /**
     * 类型（字符串）
     */
    private String type;

    /**
     * 字段名
     */
    private String key;

    /**
     *
     */
    private String value;

    /**
     * 字段意义1
     */
    private String value1;

    /**
     * 字段意义2
     */
    private String value2;

    /**
     * 字段意义3
     */
    private String value3;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 备注名
     */
    private String remark;

    /**
     * 排序字段
     */
    private Integer sort;
}
