package mf.code.comm.dto;

import lombok.Data;
import mf.code.user.dto.UserResp;

import java.util.Map;

/**
 * mf.code.comm.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:48
 */
@Data
public class TemplateMsgDTO {
    private String templateId;
    private UserResp user;
    private Map<String, Object> data;
}
