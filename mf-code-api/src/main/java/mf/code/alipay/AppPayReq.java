package mf.code.alipay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月09日 16:47
 */
@Data
public class AppPayReq {
    /***
     * 商品的标题/交易标题/订单标题/订单关键字等。
     * 最大长度：256
     * <必选>
     */
    private String subject;
    /***
     * 商户网站唯一订单号
     * 最大长度：64
     * <必选>
     */
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    /***
     * 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
     * 最大长度：9
     * <必选>
     */
    @JSONField(name = "total_amount")
    private String totalAmount;

    /***回调，跳转***/
    @JSONField(name = "return_url")
    @JsonIgnore
    private String returnUrl;

    /****-----------------------------------------------------------------------------***/
    /***
     * 公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。
     * 支付宝只会在同步返回（包括跳转回商户网站）和异步通知时将该参数原样返回。
     * 本参数必须进行UrlEncode之后才可以发送给支付宝。
     * 最大长度：512
     * <可选>
     */
    @JSONField(name = "passback_params")
    private String passbackParams;

    /***
     * 对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加传给body。
     * 最大长度：128
     * <可选>
     */
    private String body;
    /***
     * 商品主类型 :0-虚拟类商品,1-实物类商品 默认：0
     * 注：虚拟类商品不支持使用花呗渠道
     * 最大长度：2
     * <可选>
     */
    @JSONField(name = "goods_type")
    private String goodsType;
    /***
     * 该笔订单允许的最晚付款时间，逾期将关闭交易。
     * 取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。
     * 该参数数值不接受小数点， 如 1.5h，可转换为 90m。
     * 最大长度：6
     * <可选>
     */
    @JSONField(name = "timeout_express")
    private String timeoutExpress;
    /***
     * 销售产品码，商家和支付宝签约的产品码
     * QUICK_MSECURITY_PAY
     * 最大长度：64
     * <可选>
     */
    @JSONField(name = "product_code")
    private String productCode;
    /***
     * 绝对超时时间，格式为yyyy-MM-dd HH:mm。
     * 最大长度：32
     * <可选>
     */
    @JSONField(name = "time_expire")
    private String timeExpire;
    /***
     * 优惠参数
     * 注：仅与支付宝协商后可用
     * 最大长度：512
     * <可选>
     */
    @JSONField(name = "promo_params")
    private String promoParams;

    /***
     * 商户原始订单号，最大长度限制32位
     * 最大长度：32
     * <可选>
     */
    @JSONField(name = "merchant_order_no")
    private String merchantOrderNo;
}
