package mf.code.alipay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年09月02日 10:01
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlipayQueryDTO {
    /***
     * 订单支付时传入的商户订单号,和支付宝交易号不能同时为空。
     * trade_no,out_trade_no如果同时存在优先取trade_no
     */
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    /***
     * 支付宝交易号，和商户订单号不能同时为空
     */
    @JSONField(name = "trade_no")
    private String tradeNo;
}
