package mf.code.alipay;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 17:12
 */
@Data
public class ResponseRefundDTO extends AlipayResponse {
    /***
     * 用户的登录id
     */
    private String buyerLogonId;
    /***
     * 买家在支付宝的用户id
     */
    private String buyerUserId;
    /***
     * 本次退款是否发生了资金变化
     */
    private String fundChange;
    /***
     * 退款支付时间
     */
    private Date gmtRefundPay;
    /***
     *
     */
    private String openId;
    /***
     * 商户订单号
     */
    private String outTradeNo;
    /***
     * 本次退款金额中买家退款金额
     */
    private String presentRefundBuyerAmount;
    /***
     * 本次退款金额中平台优惠退款金额
     */
    private String presentRefundDiscountAmount;
    /***
     * 本次退款金额中商家优惠退款金额
     */
    private String presentRefundMdiscountAmount;
    private String refundChargeAmount;
    private String refundCurrency;
    /***
     * 退款总金额
     */
    private String refundFee;
    private String refundSettlementId;
    private String sendBackFee;
    /***
     * 交易在支付时候的门店名称
     */
    private String storeName;
    /***
     * 支付宝交易号
     */
    private String tradeNo;
}
