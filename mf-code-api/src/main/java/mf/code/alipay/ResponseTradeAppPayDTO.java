package mf.code.alipay;

import lombok.Data;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 17:12
 */
@Data
public class ResponseTradeAppPayDTO extends AlipayResponse {
    /***
     * 商户原始订单号，最大长度限制32位
     */
    private String merchantOrderNo;
    /***
     * 商户网站唯一订单号
     */
    private String outTradeNo;
    /***
     * 收款支付宝账号对应的支付宝唯一用户号。
     * 以2088开头的纯16位数字
     */
    private String sellerId;
    /***
     * 该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。
     */
    private String totalAmount;
    /***
     * 该交易在支付宝系统中的交易流水号。
     */
    private String tradeNo;
}
