package mf.code.alipay;

import lombok.Data;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月15日 15:28
 */
@Data
public class RequestRefundReq {
    /***
     * 订单支付时传入的商户订单号,不能和 trade_no同时为空
     * 最大长度：64
     * <<可选>>
     */
    private String outTradeNo;
    /***
     * 支付宝交易号，和商户订单号不能同时为空
     * 最大长度：64
     * <<可选>>
     */
    private String tradeNo;
    /***
     * 需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数
     * <必选>
     */
    private String refundAmount;
    /***
     * 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。
     * <可选>
     */
    private String outRequestNo;
    /***
     * 退款的原因说明
     * <可选>
     */
    private String refundReason;
}
