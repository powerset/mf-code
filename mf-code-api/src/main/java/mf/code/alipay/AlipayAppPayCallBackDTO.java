package mf.code.alipay;

import lombok.Data;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月20日 10:42
 */
@Data
public class AlipayAppPayCallBackDTO {
    /***
     * 商户订单号
     */
    private String outTradeNo;
    /***
     * 支付宝交易号
     */
    private String tradeNo;
    /***
     * 交易状态
     */
    private String tradeStatus;
    /***
     * 订单金额 本次交易支付的订单金额，单位为人民币（元）
     */
    private String totalAmount;
    /***
     * 交易付款时间 格式为yyyy-MM-dd HH:mm:ss
     */
    private String gmtPayment;
    /***
     * 公用回传参数
     */
    private String passbackParams;
    /***
     * 买家支付宝账号
     */
    private String buyerLogonId;
    /***
     * 买家支付宝用户号
     */
    private String buyerId;
    /***
     * 实收金额 商家在交易中实际收到的款项，单位为元
     */
    private String receiptAmount;
    /***
     * 付款金额 用户在交易中支付的金额
     */
    private String buyerPayAmount;
    /***
     * 交易创建时间 格式为yyyy-MM-dd HH:mm:ss
     */
    private String gmtCreate;
}
