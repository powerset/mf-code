package mf.code.alipay.constant;

/**
 * mf.code.order.constant
 * Description:
 *
 * @author gel
 * @date 2019-04-13 17:08
 */
public enum AlipayPaySceneEnum {
    /**
     * 抖小铺(卖家版)
     */
    DOU_SHOP_MERCHANT(0, "抖小铺(卖家版)"),
    /**
     * 抖小铺
     */
    DOU_SHOP(1, "抖小铺"),
    ;
    private int code;
    private String message;

    AlipayPaySceneEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static AlipayPaySceneEnum findByCode(Integer code) {
        for (AlipayPaySceneEnum typeEnum : AlipayPaySceneEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
