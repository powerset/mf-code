package mf.code.alipay;

import lombok.Data;

import java.util.Map;

/**
 * mf.code.alipay
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月07日 17:13
 */
@Data
public class AlipayResponse {
    private String code;
    private String msg;
    private String subCode;
    private String subMsg;
    private String body;
    private Map<String, String> params;
}
