package mf.code.activity.constant;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-20 下午7:23
 */
public enum ActivityDefAuditTypeEnum {
    /**
     * 计划类抽奖活动
     */
    MCH_PLAN(1, "计划类抽奖活动"),
    /**
     * 免单机会活动
     */
    ORDER_BACK(2, "免单抽奖活动"),
    /**
     * 新人有礼活动
     */
    NEW_MAN(3, "新人有礼活动"),
    /**
     * 新人大转盘
     */
    LUCKY_WHEEL(4, "幸运大转盘"),
    /**
     * 回填订单红包任务
     */
    FILL_BACK_ORDER(5, "回填订单红包任务"),

    /***
     * 助力任务
     */
    ASSIST(6, "赠品活动"),
    /**
     * 赠品活动
     */
    GIFT(7, "赠品活动"),
    /**
     * 红包活动（回填订单，收藏加购，好评晒图)
     */
    RED_PACKET(8, "红包活动（回填订单，收藏加购，好评晒图)"),
    /**
     * 拆红包
     */
    OPEN_RED_PACKET(9, "拆红包活动"),
    /***
     * 好评晒图
     */
    GOOD_COMMENT(10, "好评晒图"),

    /***
     * 收藏加购
     */
    FAV_CART(11, "收藏加购"),

    /**
     * 免单机会活动--人工审核
     */
    ORDER_BACK_V2(12, "免单抽奖活动"),

    /***
     * 助力任务--人工审核
     */
    ASSIST_V2(13, "赠品活动"),

    /**
     * 回填订单红包任务--人工审核
     */
    FILL_BACK_ORDER_V2(14, "回填订单红包任务"),

    /***
     * 好评晒图--人工审核
     */
    GOOD_COMMENT_V2(15, "好评晒图"),

    /***
     * 收藏加购--人工审核
     */
    FAV_CART_V2(16, "收藏加购"),
    /***
     * 新手任务--3个宝箱种类
     */
    NEWBIE_TASK(17, "新手任务"),
    /***
     * 新手任务-加群享福利
     */
    NEWBIE_TASK_BONUS(18, "新手任务-加群享福利"),
    /***
     * 新手任务-查看攻略获得红包
     */
    NEWBIE_TASK_REDPACK(19, "新手任务-查看攻略获得红包"),

    /***
     * 店长任务
     */
    SHOP_MANAGER_TASK(20, "店长任务"),
    /***
     * 店长任务-店长任务_设置店长wx群获得奖励
     */
    SHOP_MANAGER_TASK_ADD_WX(21, "店长任务_设置店长wx群获得奖励"),
    /***
     * 店长任务-店长任务_邀请粉丝获得奖励
     */
    SHOP_MANAGER_TASK_INVITE_FANS(22, "店长任务_邀请粉丝获得奖励"),
    /***
     * 店长任务-店长任务_推荐粉丝成为店长
     */
    SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER(23, "店长任务_推荐粉丝成为店长"),
    /***
     * 报销活动
     */
    FULL_REIMBURSEMENT(24, "全额报销"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityDefAuditTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String findByDesc(Integer code) {
        for (ActivityDefAuditTypeEnum typeEnum : ActivityDefAuditTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.desc;
            }
        }
        return "";
    }
}
