package mf.code.activity.constant;

/**
 * mf.code.common.constant
 * Description: 活动类型 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者
 *
 * @author: gel
 * @date: 2018-11-08 9:56
 */
public enum ActivityTypeEnum {
    /**
     * 计划类抽奖活动
     */
    MCH_PLAN(1, "计划类抽奖活动"),
    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START(2, "免单商品抽奖发起者"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN(3, "免单商品抽奖参与者"),
    /**
     * 新手有礼发起者
     */
    NEW_MAN_START(4, "新手有礼发起者"),
    /**
     * 新手有礼抽奖参与者
     */
    NEW_MAN_JOIN(5, "新手有礼抽奖参与者"),
    /**
     * 轮盘抽奖
     */
    LUCK_WHEEL(0, "轮盘抽奖"),
    /**
     * 订单红包
     */
    ORDER_REDPACK(6, "订单随机红包-包裹扫描"),

    /**
     * 红包任务收藏加购
     */
    REDPACK_TASK_FAV_CART(7,"红包任务-收藏加购"),

    /***
     * 助力任务
     */
    ASSIST(8, "助力活动发起者"),
    /**
     * 拆红包发起
     */
    OPEN_RED_PACKET_START(12, "拆红包发起"),
    /**
     * 拆红包参与
     */
    OPEN_RED_PACKET_JOIN(13, "拆红包参与"),

    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START_V2(16, "免单商品抽奖发起者"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN_V2(17, "免单商品抽奖参与者"),

    /**
     * 助力任务
     */
    ASSIST_V2(18, "助力活动发起者"),

    /***
     * 新手任务-拆红包
     */
    NEWBIE_TASK_OPENREDPACKAGE(19, "新手任务-拆红包"),
    /**
     * 全额报销活动
     */
    FULL_REIMBURSEMENT(20, "全额报销活动"),
    /**
     * 平台免单抽奖
     */
    PLATFORM_ORDER_BACK(21, "平台免单抽奖")
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 从活动定义类型 获取 对应的活动类型
     * @param activityDefType
     * @return
     */
    public static Integer findTypeByDefType(Integer activityDefType) {
        if (ActivityDefTypeEnum.MCH_PLAN.getCode() == activityDefType) {
            return ActivityTypeEnum.MCH_PLAN.getCode();
        }
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == activityDefType) {
            return ActivityTypeEnum.ORDER_BACK_START.getCode();
        }
        if (ActivityDefTypeEnum.NEW_MAN.getCode() == activityDefType) {
            return ActivityTypeEnum.NEW_MAN_START.getCode();
        }
        if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDefType) {
            return ActivityTypeEnum.LUCK_WHEEL.getCode();
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == activityDefType) {
            return ActivityTypeEnum.ORDER_REDPACK.getCode();
        }
        if (ActivityDefTypeEnum.ASSIST.getCode() == activityDefType) {
            return ActivityTypeEnum.ASSIST.getCode();
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDefType) {
            return ActivityTypeEnum.OPEN_RED_PACKET_START.getCode();
        }
        if (ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == activityDefType) {
            return ActivityTypeEnum.ORDER_BACK_START_V2.getCode();
        }
        if (ActivityDefTypeEnum.ASSIST_V2.getCode() == activityDefType) {
            return ActivityTypeEnum.ASSIST_V2.getCode();
        }
        if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == activityDefType) {
            return ActivityTypeEnum.FULL_REIMBURSEMENT.getCode();
        }
        if (ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode() == activityDefType) {
            return ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode();
        }
         // 异常类型
        return -1;
    }
}
