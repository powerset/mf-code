package mf.code.activity.constant;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-28 上午12:39
 */
public enum ActivityTaskTypeEnum {
    /**
     * 回填订单红包
     */
    FILL_BACK_ORDER(1, "回填订单"),
    /**
     * 好评返现红包
     */
    GOOD_COMMENT(2, "好评返现"),
    /**
     * 红包活动（回填订单，收藏加购，好评晒图)
     */
    FAVCART(3, "收藏加购"),
    /**
     * 领取优惠券
     */
    COUPON(4,"领取优惠券"),
    /**
     * 加公众号
     */
    WECHAT_SUBSCRIPTION(5,"加公众号"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityTaskTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
