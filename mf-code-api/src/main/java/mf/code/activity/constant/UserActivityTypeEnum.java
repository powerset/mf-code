package mf.code.activity.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-23 下午1:15
 */
public enum UserActivityTypeEnum {
    /**
     * 计划类抽奖
     */
    MCH_PLAN(1, "计划类抽奖"),
    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START(2, "免单商品抽奖发起者"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN(3, "免单商品抽奖参与者"),
    /**
     * 新手有礼发起者
     */
    NEW_MAN_START(4, "新手有礼发起者"),
    /**
     * 新手有礼抽奖参与者
     */
    NEW_MAN_JOIN(5, "新手有礼抽奖参与者"),

    /***
     * 助力任务发起者
     */
    ASSIST(6, "助力活动发起者"),
    /***
     * 助力任务助力者
     */
    ASSIST_JOIN(7, "助力活动助力者"),
    /**
     * 回填订单红包
     */
    FILL_BACK_ORDER(8, "回填订单"),
    /**
     * 好评返现红包
     */
    GOODS_COMMENT(9, "好评返现"),
    /**
     * 红包活动（回填订单，收藏加购，好评晒图)
     */
    FAV_CART(10, "收藏加购"),
    /**
     * 拆红包发起
     */
    OPEN_RED_PACKET_START(12, "拆红包发起"),
    /**
     * 拆红包参与
     */
    OPEN_RED_PACKET_JOIN(13, "拆红包参与"),

    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START_V2(14, "免单商品抽奖发起者"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN_V2(15, "免单商品抽奖参与者"),
    /**
     * 助力任务
     */
    ASSIST_V2(16, "助力活动发起者"),
    /***
     * 助力任务助力者
     */
    ASSIST_JOIN_V2(17, "助力活动助力者"),
    /**
     * 回填订单红包
     */
    FILL_BACK_ORDER_V2(18, "回填订单"),
    /**
     * 好评返现红包
     */
    GOODS_COMMENT_V2(19, "好评返现"),
    /**
     * 红包活动（回填订单，收藏加购，好评晒图)
     */
    FAV_CART_V2(20, "收藏加购"),
    /***
     * 新手任务-拆红包
     */
    NEWBIE_TASK_OPENREDPACKAGE(21, "新手任务-拆红包"),
    /***
     * 全额报销发起者
     */
    FULL_REIMBURSEMENT_START(22, "全额报销发起者"),
    /***
     * 全额报销助力者
     */
    FULL_REIMBURSEMENT_JOIN(23, "全额报销助力者"),
    /***
     * 平台免单抽奖 发起者
     */
    PLATFORM_ORDER_BACK_START(24, "平台免单抽奖 发起者"),
    /***
     * 平台免单抽奖 参与者
     */
    PLATFORM_ORDER_BACK_JOIN(25, "平台免单抽奖 参与者"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    UserActivityTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static UserActivityTypeEnum findByCode(Integer code) {
        for (UserActivityTypeEnum typeEnum : UserActivityTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 我的任务页面列表查询类型（历史任务）
     *
     * @return
     */
    public static List<Integer> findTypeForTaskHistory() {
        List<Integer> types = new ArrayList<>();
        types.add(MCH_PLAN.getCode());
        types.add(ORDER_BACK_START.getCode());
        types.add(ORDER_BACK_JOIN.getCode());
        types.add(NEW_MAN_START.getCode());
        types.add(NEW_MAN_JOIN.getCode());
        types.add(ASSIST.getCode());
        types.add(FILL_BACK_ORDER.getCode());
        types.add(GOODS_COMMENT.getCode());
        types.add(FAV_CART.getCode());
        types.add(OPEN_RED_PACKET_START.getCode());
        types.add(ORDER_BACK_START_V2.getCode());
        types.add(ORDER_BACK_JOIN_V2.getCode());
        types.add(ASSIST_V2.getCode());
        types.add(FILL_BACK_ORDER_V2.getCode());
        types.add(GOODS_COMMENT_V2.getCode());
        types.add(FAV_CART_V2.getCode());
        return types;
    }

    /**
     * 通过 活动类型 发起者id 参与者id，获取 userActivityType
     *
     * @param activityType 活动类型
     * @param startId      发起者id
     * @param joinId       参与者id
     * @return
     */
    public static Integer findTypeByActivityType(Integer activityType, Long startId, Long joinId) {
        // 参与者 是否 是 发起者
        boolean userIdEquals = startId.equals(joinId);

        if (ActivityTypeEnum.MCH_PLAN.getCode() == activityType) {
            return UserActivityTypeEnum.MCH_PLAN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START.getCode() == activityType && userIdEquals) {
            return UserActivityTypeEnum.ORDER_BACK_START.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_START.getCode() == activityType && userIdEquals) {
            return UserActivityTypeEnum.NEW_MAN_START.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_START.getCode() == activityType) {
            return UserActivityTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_JOIN.getCode() == activityType) {
            return UserActivityTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (ActivityTypeEnum.ASSIST.getCode() == activityType && userIdEquals) {
            return UserActivityTypeEnum.ASSIST.getCode();
        }
        if (ActivityTypeEnum.ASSIST.getCode() == activityType) {
            return UserActivityTypeEnum.ASSIST_JOIN.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == activityType && userIdEquals) {
            return UserActivityTypeEnum.OPEN_RED_PACKET_START.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == activityType) {
            return UserActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode();
        }

        if (ActivityTypeEnum.ORDER_BACK_START_V2.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_START_V2.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode();
        }
        if (ActivityTypeEnum.ASSIST_V2.getCode() == activityType) {
            return UserActivityTypeEnum.ASSIST_V2.getCode();
        }
        if (ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode() == activityType) {
            return UserActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode();
        }
        if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == activityType && userIdEquals) {
            return UserActivityTypeEnum.FULL_REIMBURSEMENT_START.getCode();
        }
        if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == activityType) {
            return UserActivityTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode();
        }
        if (ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode() == activityType  && userIdEquals) {
            return UserActivityTypeEnum.PLATFORM_ORDER_BACK_START.getCode();
        }
        if (ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode() == activityType) {
            return UserActivityTypeEnum.PLATFORM_ORDER_BACK_JOIN.getCode();
        }
        // 异常类型
        return -1;
    }

    public static Integer findUserActivityTypeByActivityType(Integer activityType) {
        if (ActivityTypeEnum.MCH_PLAN.getCode() == activityType) {
            return UserActivityTypeEnum.MCH_PLAN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_START.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_START.getCode() == activityType) {
            return UserActivityTypeEnum.NEW_MAN_START.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_JOIN.getCode() == activityType) {
            return UserActivityTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (ActivityTypeEnum.ASSIST.getCode() == activityType) {
            return UserActivityTypeEnum.ASSIST.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == activityType) {
            return UserActivityTypeEnum.OPEN_RED_PACKET_START.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode() == activityType) {
            return UserActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode();
        }

        if (ActivityTypeEnum.ORDER_BACK_START_V2.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_START_V2.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode() == activityType) {
            return UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode();
        }
        if (ActivityTypeEnum.ASSIST_V2.getCode() == activityType) {
            return UserActivityTypeEnum.ASSIST_V2.getCode();
        }
        if (ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode() == activityType) {
            return UserActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode();
        }
        if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == activityType) {
            return UserActivityTypeEnum.FULL_REIMBURSEMENT_START.getCode();
        }
        // 异常类型
        return -1;
    }


}
