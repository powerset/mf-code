package mf.code.activity.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-16 下午8:39
 */
public enum UserCouponTypeEnum {
    /**
     * 轮盘抽奖
     */
    LUCKY_WHEEL(1, "幸运大转盘"),
    /**
     * 订单红包
     */
    ORDER_RED_PACKET(2, "回填订单"),
    /**
     * 优惠券
     */
    COUPON(3, "优惠券"),
    /**
     * 好评晒图
     */
    GOODS_COMMENT(4, "好评晒图"),
    /**
     * 收藏加购
     */
    FAV_CART(5, "收藏加购"),
    /**
     * 计划类抽奖活动
     */
    MCH_PLAN(6, "计划类抽奖活动"),
    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START(7, "抽奖免单任务"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN(8, "免单商品抽奖参与者"),
    /**
     * 新手有礼发起者
     */
    NEW_MAN_START(9, "新手有礼发起者"),
    /**
     * 新手有礼抽奖参与者
     */
    NEW_MAN_JOIN(10, "新手有礼抽奖参与者"),
    /***
     * 助力任务
     */
    ASSIST(11, "赠品任务"),
    /***
     * 拆红包活动发起者
     */
    OPEN_RED_PACKAGE_START(12, "拆红包活动"),
    /***
     * 拆红包活动助力者
     */
    OPEN_RED_PACKAGE_ASSIST(13, "拆红包活动助力者"),

    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START_V2(14, "抽奖免单任务"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN_V2(15, "免单商品抽奖参与者"),
    /***
     * 助力任务
     */
    ASSIST_V2(16, "赠品任务"),
    /**
     * 订单红包
     */
    ORDER_RED_PACKET_V2(17, "回填订单"),
    /**
     * 好评晒图
     */
    GOODS_COMMENT_V2(18, "好评晒图"),
    /**
     * 收藏加购
     */
    FAV_CART_V2(19, "收藏加购"),

    /***
     * 新手任务-加群享福利
     */
    NEWBIE_TASK_BONUS(20, "加群享福利，扫一扫"),
    /***
     * 新手任务-查看攻略获得红包
     */
    NEWBIE_TASK_REDPACK(21, "查看攻略，拿红包"),
    /***
     * 新手任务-新人红包，奖励到
     */
    NEWBIE_TASK_OPENREDPACK(25, "新人红包，奖励到"),
    /***
     * 新手任务-新人红包，拆红包记录-全金额
     */
    NEWBIE_TASK_OPENREDPACK_AMOUNT(26, "新人红包，奖励到"),
    /***
     * 店长任务-店长任务_设置店长wx群获得奖励
     */
    SHOP_MANAGER_TASK_ADD_WX(22, "设置店长微信群"),
    /***
     * 店长任务-店长任务_邀请粉丝获得奖励
     */
    SHOP_MANAGER_TASK_INVITE_FANS(23, "邀请粉丝"),
    /***
     * 店长任务-店长任务_推荐粉丝成为店长 最多10条记录
     */
    SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER(24, "推荐粉丝成为店长"),
    /**
     * 全额报销活动 发起者
     */
    FULL_REIMBURSEMENT_START(27, "全额报销活动 -- 发起者"),
    /**
     * 全额报销活动 -- 助力者
     */
    FULL_REIMBURSEMENT_JOIN(28, "全额报销活动 -- 助力者"),
    /**
     * 集客优惠券
     */
    JKMF_COUPON(29, "集客优惠券"),
    ;
    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String message;

    UserCouponTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static String findByDesc(Integer code) {
        for (UserCouponTypeEnum typeEnum : UserCouponTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return "";
    }

    public static UserCouponTypeEnum findByCode(Integer code) {
        for (UserCouponTypeEnum typeEnum : UserCouponTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }

    /**
     * 从 活动类型 获取 对应用户奖品类型
     *
     * @param type
     * @return
     */
    public static Integer findByActivityType(Integer type) {
        if (type == ActivityTypeEnum.MCH_PLAN.getCode()) {
            return UserCouponTypeEnum.MCH_PLAN.getCode();
        }
        if (type == ActivityTypeEnum.LUCK_WHEEL.getCode()) {
            return UserCouponTypeEnum.LUCKY_WHEEL.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_START.getCode() || type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            return UserCouponTypeEnum.ORDER_BACK_START.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_JOIN.getCode() || type == ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode()) {
            return UserCouponTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            return UserCouponTypeEnum.NEW_MAN_START.getCode();
        }
        if (type == ActivityTypeEnum.NEW_MAN_JOIN.getCode()) {
            return UserCouponTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_REDPACK.getCode()) {
            return UserCouponTypeEnum.ORDER_RED_PACKET.getCode();
        }
        if (type == ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
            return UserCouponTypeEnum.FAV_CART.getCode();
        }
        if (type == ActivityTypeEnum.ASSIST.getCode() || type == ActivityTypeEnum.ASSIST_V2.getCode()) {
            return UserCouponTypeEnum.ASSIST.getCode();
        }
        if (type == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()) {
            return UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode();
        }
        if (type == ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode()) {
            return UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode();
        }
        if (type == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode()) {
            return UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode();
        }
        if (type == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            return UserCouponTypeEnum.FULL_REIMBURSEMENT_START.getCode();
        }
        return 0;
    }

    /**
     * 从 活动类型 获取 对应用户奖品类型
     *
     * @param type
     * @return
     */
    public static Integer findByActivityTypeStarter(Integer type, boolean starter) {
        if (type == ActivityTypeEnum.MCH_PLAN.getCode()) {
            return UserCouponTypeEnum.MCH_PLAN.getCode();
        }
        if (type == ActivityTypeEnum.LUCK_WHEEL.getCode()) {
            return UserCouponTypeEnum.LUCKY_WHEEL.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_START.getCode() || type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            return UserCouponTypeEnum.ORDER_BACK_START.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_JOIN.getCode() || type == ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode()) {
            return UserCouponTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            return UserCouponTypeEnum.NEW_MAN_START.getCode();
        }
        if (type == ActivityTypeEnum.NEW_MAN_JOIN.getCode()) {
            return UserCouponTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_REDPACK.getCode()) {
            return UserCouponTypeEnum.ORDER_RED_PACKET.getCode();
        }
        if (type == ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
            return UserCouponTypeEnum.FAV_CART.getCode();
        }
        if (type == ActivityTypeEnum.ASSIST.getCode() || type == ActivityTypeEnum.ASSIST_V2.getCode()) {
            return UserCouponTypeEnum.ASSIST.getCode();
        }
        if (type == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()) {
            return UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode();
        }
        if (type == ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode()) {
            return UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode();
        }
        if (type == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode() && starter) {
            return UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode();
        }
        if (type == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode()) {
            return UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode();
        }
        if (type == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() && starter) {
            return UserCouponTypeEnum.FULL_REIMBURSEMENT_START.getCode();
        }
        if (type == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            return UserCouponTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode();
        }
        return 0;
    }

    /**
     * 从 活动定义类型 获取 对应用户奖品类型
     *
     * @param activityDefType
     * @return
     */
    public static List<Integer> findByActivityDefType(Integer activityDefType) {
        List<Integer> userCouponTypeList = new ArrayList<>();

        if (ActivityDefTypeEnum.MCH_PLAN.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.MCH_PLAN.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.ORDER_BACK_START.getCode());
            userCouponTypeList.add(UserCouponTypeEnum.ORDER_BACK_JOIN.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.NEW_MAN.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.NEW_MAN_START.getCode());
            userCouponTypeList.add(UserCouponTypeEnum.NEW_MAN_JOIN.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.LUCKY_WHEEL.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.ASSIST.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.ASSIST.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.GIFT.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.ASSIST.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.RED_PACKET.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.FAV_CART.getCode());
            userCouponTypeList.add(UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
            userCouponTypeList.add(UserCouponTypeEnum.GOODS_COMMENT.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.GOODS_COMMENT.getCode());
            return userCouponTypeList;
        }
        if (ActivityDefTypeEnum.FAV_CART.getCode() == activityDefType) {
            userCouponTypeList.add(UserCouponTypeEnum.FAV_CART.getCode());
            return userCouponTypeList;
        }

        // 异常类型
        userCouponTypeList.add(-1);
        return userCouponTypeList;
    }
}
