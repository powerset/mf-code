package mf.code.activity.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: gel
 * @date: 2018-11-07 21:08
 */
public enum ActivityDefTypeEnum {
    /**
     * 计划类抽奖活动
     */
    MCH_PLAN(1, "计划类抽奖活动"),
    /**
     * 免单机会活动
     */
    ORDER_BACK(2, "免单抽奖活动"),
    /**
     * 新人有礼活动
     */
    NEW_MAN(3, "新人有礼活动"),
    /**
     * 新人大转盘
     */
    LUCKY_WHEEL(4, "幸运大转盘"),
    /**
     * 回填订单红包任务
     */
    FILL_BACK_ORDER(5, "回填订单"),

    /***
     * 助力任务
     */
    ASSIST(6, "赠品活动"),
    /***
     * 助力
     */
    GIFT(7, "助力"),
    /**
     * 红包活动（回填订单，收藏加购，好评晒图)
     */
    RED_PACKET(8, "红包活动（回填订单，收藏加购，好评晒图)"),
    /**
     * 拆红包
     */
    OPEN_RED_PACKET(9, "拆红包"),
    /***
     * 好评晒图
     */
    GOOD_COMMENT(10, "好评晒图"),

    /***
     * 收藏加购
     */
    FAV_CART(11, "收藏加购"),

    /**
     * 免单机会活动--人工审核
     */
    ORDER_BACK_V2(12, "免单抽奖活动"),

    /***
     * 助力任务--人工审核
     */
    ASSIST_V2(13, "赠品活动"),

    /**
     * 回填订单红包任务--人工审核
     */
    FILL_BACK_ORDER_V2(14, "回填订单"),

    /***
     * 好评晒图--人工审核
     */
    GOOD_COMMENT_V2(15, "好评晒图"),

    /***
     * 收藏加购--人工审核
     */
    FAV_CART_V2(16, "收藏加购"),

    /***
     * 新手任务--3个宝箱种类
     */
    NEWBIE_TASK(17, "新手任务"),
    /***
     * 新手任务-加群享福利
     */
    NEWBIE_TASK_BONUS(18, "新手任务-加群享福利"),
    /***
     * 新手任务-查看攻略获得红包
     */
    NEWBIE_TASK_REDPACK(19, "新手任务-查看攻略获得红包"),

    /***
     * 店长任务
     */
    SHOP_MANAGER_TASK(20, "店长任务"),
    /***
     * 店长任务-店长任务_设置店长wx群获得奖励
     */
    SHOP_MANAGER_TASK_ADD_WX(21, "店长任务_设置店长wx群获得奖励"),
    /***
     * 店长任务-店长任务_邀请粉丝获得奖励
     */
    SHOP_MANAGER_TASK_INVITE_FANS(22, "店长任务_邀请粉丝获得奖励"),
    /***
     * 店长任务-店长任务_推荐粉丝成为店长
     */
    SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER(23, "店长任务_推荐粉丝成为店长"),
    /**
     * 全额报销活动
     */
    FULL_REIMBURSEMENT(24, "全额报销活动"),
    /**
     * 平台免单抽奖活动
     */
    PLATFORM_ORDER_BACK(25, "平台免单抽奖活动"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityDefTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ActivityDefTypeEnum findByCode(int code) {
        for (ActivityDefTypeEnum typeEnum : ActivityDefTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }

    public static String findByDesc(int code) {
        for (ActivityDefTypeEnum typeEnum : ActivityDefTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getDesc();
            }
        }
        return null;
    }

    /**
     * 从 活动类型 获取 活动定义类型
     *
     * @param type
     * @return
     */
    public static Integer findByActivityType(int type) {
        if (ActivityTypeEnum.MCH_PLAN.getCode() == type) {
            return ActivityDefTypeEnum.MCH_PLAN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START.getCode() == type) {
            return ActivityDefTypeEnum.ORDER_BACK.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN.getCode() == type) {
            return ActivityDefTypeEnum.ORDER_BACK.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_START.getCode() == type) {
            return ActivityDefTypeEnum.NEW_MAN.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_JOIN.getCode() == type) {
            return ActivityDefTypeEnum.NEW_MAN.getCode();
        }
        if (ActivityTypeEnum.ORDER_REDPACK.getCode() == type) {
            return ActivityDefTypeEnum.FILL_BACK_ORDER.getCode();
        }
        if (ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
            return ActivityDefTypeEnum.FAV_CART.getCode();
        }
        if (ActivityTypeEnum.ASSIST.getCode() == type) {
            return ActivityDefTypeEnum.ASSIST.getCode();
        }
        if (ActivityTypeEnum.LUCK_WHEEL.getCode() == type) {
            return ActivityDefTypeEnum.LUCKY_WHEEL.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == type) {
            return ActivityDefTypeEnum.OPEN_RED_PACKET.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode() == type) {
            return ActivityDefTypeEnum.OPEN_RED_PACKET.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START_V2.getCode() == type) {
            return ActivityDefTypeEnum.ORDER_BACK_V2.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode() == type) {
            return ActivityDefTypeEnum.ORDER_BACK_V2.getCode();
        }
        if (ActivityTypeEnum.ASSIST_V2.getCode() == type) {
            return ActivityDefTypeEnum.ASSIST_V2.getCode();
        }
        if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == type) {
            return ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode();
        }

        // 类型异常
        return -1;
    }


    public static boolean checkCaculateCommission(Integer activityDefType) {
        List<Integer> list = new ArrayList<>();
        list.add(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
        list.add(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode());
        list.add(ActivityDefTypeEnum.FAV_CART.getCode());
        list.add(ActivityDefTypeEnum.FAV_CART_V2.getCode());
        list.add(ActivityDefTypeEnum.GOOD_COMMENT.getCode());
        list.add(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode());
        list.add(ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode());
        list.add(ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        if (list.contains(activityDefType)) {
            return true;
        }
        return false;
    }

    public static boolean directEnterWallet(Integer activityDefType){
        List<Integer> list = new ArrayList<>();
        list.add(ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
        if (list.contains(activityDefType)) {
            return true;
        }
        return false;
    }

    /***
     * 计算非 任务活动，计算出的佣金 给平台--店长，新手任务迭代之后衍生出来的概念定义<助力，免单>
     * @param activityDefType
     * @return
     */
    public static boolean checkNotTaskDefType(Integer activityDefType) {
        List<Integer> list = new ArrayList<>();
        list.add(ActivityDefTypeEnum.ASSIST.getCode());
        list.add(ActivityDefTypeEnum.ASSIST_V2.getCode());
        list.add(ActivityDefTypeEnum.ORDER_BACK.getCode());
        list.add(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
        if (list.contains(activityDefType)) {
            return true;
        }
        return false;
    }
}
