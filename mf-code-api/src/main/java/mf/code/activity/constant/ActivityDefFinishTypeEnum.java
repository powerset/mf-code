package mf.code.activity.constant;

/**
 * mf.code.activity.constant
 * Description: 任务完成下单方式
 *
 * @author gel
 * @date 2019-05-24 11:23
 */
public enum ActivityDefFinishTypeEnum {
    /**
     * 淘宝下单
     */
    TAOBAO(0, "淘宝下单"),
    /**
     * 微信下单
     */
    WEICHAT(1, "微信下单"),
    ;

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    ActivityDefFinishTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ActivityDefFinishTypeEnum findByCode(Integer code) {
        for (ActivityDefFinishTypeEnum finishTypeEnum : ActivityDefFinishTypeEnum.values()) {
            if (finishTypeEnum.getCode() == code) {
                return finishTypeEnum;
            }
        }
        return null;
    }
}
