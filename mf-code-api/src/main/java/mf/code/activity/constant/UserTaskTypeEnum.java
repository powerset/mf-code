package mf.code.activity.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.common.constant
 * Description: 活动类型 0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包(包裹)7:红包任务-收藏加购
 *
 * @author: gel
 * @date: 2018-11-21 17:04
 */
public enum UserTaskTypeEnum {
    /**
     * 幸运大转盘
     */
    LUCK_WHEEL(0, "幸运大转盘"),
    /**
     * 计划类抽奖
     */
    MCH_PLAN(1, "计划类抽奖"),
    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START(2, "免单商品抽奖发起者"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN(3, "免单商品抽奖参与者"),
    /**
     * 新手有礼发起者
     */
    NEW_MAN_START(4, "新手有礼发起者"),
    /**
     * 新手有礼抽奖参与者
     */
    NEW_MAN_JOIN(5, "新手有礼抽奖参与者"),
    /**
     * 订单随机红包(包裹)
     */
    ORDER_REDPACK(6, "订单随机红包-包裹扫描"),

    /**
     * 红包任务收藏加购
     */
    REDPACK_TASK_FAV_CART(7, "红包任务-收藏加购"),
    /**
     * 助力活动发起者
     */
    ASSIST_START(8, "助力活动发起者"),
    /**
     * 好评返现
     */
    GOOD_COMMENT(9, "好评返现"),
    /**
     * 拆红包发起
     */
    OPEN_RED_PACKET_START(12, "拆红包发起"),
    /**
     * 拆红包参与
     */
    OPEN_RED_PACKET_JOIN(13, "拆红包参与"),
    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK_START_V2(14, "免单商品抽奖发起者"),
    /**
     * 免单商品抽奖参与者
     */
    ORDER_BACK_JOIN_V2(15, "免单商品抽奖参与者"),
    /**
     * 助力活动发起者
     */
    ASSIST_START_V2(16, "助力活动发起者"),
    /**
     * 订单随机红包(包裹)
     */
    ORDER_REDPACK_V2(17, "订单随机红包-包裹扫描"),
    /**
     * 红包任务收藏加购
     */
    REDPACK_TASK_FAV_CART_V2(18, "红包任务-收藏加购"),
    /**
     * 好评返现
     */
    GOOD_COMMENT_V2(19, "好评返现"),
    /**
     * 店长任务
     */
    SHOP_MANAGER_TASK(20, "店长任务"),
    /**
     * 新手任务
     */
    NEWBIE_TASK(21, "新手任务"),

    /**
     * 全额报销活动
     */
    FULL_REIMBURSEMENT(22, "全额报销活动"),
    /**
     * 平台免单抽奖
     */
    PLATFORM_ORDER_BACK(23, "平台免单抽奖"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    UserTaskTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static UserTaskTypeEnum findByCode(int code) {
        for (UserTaskTypeEnum typeEnum : UserTaskTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }

    /**
     * 从活动定义类型 获取 对应的 用户任务类型
     *
     * @param activityDefType
     * @return
     */
    public static List<Integer> findByActivityDefType(Integer activityDefType) {
        List<Integer> userTaskTypeList = new ArrayList<>();

        if (ActivityDefTypeEnum.MCH_PLAN.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.MCH_PLAN.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            userTaskTypeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.NEW_MAN.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            userTaskTypeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.LUCK_WHEEL.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.ASSIST.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.GIFT.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.RED_PACKET.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            userTaskTypeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            userTaskTypeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.OPEN_RED_PACKET_START.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.FAV_CART.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            return userTaskTypeList;
        }
        if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == activityDefType) {
            userTaskTypeList.add(UserTaskTypeEnum.FULL_REIMBURSEMENT.getCode());
            return userTaskTypeList;
        }
        // 没有对应的活动类型
        userTaskTypeList.add(-1);
        return userTaskTypeList;
    }


    /**
     * 从活动类型 获取 相应的任务类型
     *
     * @param type
     * @return
     */
    public static Integer findByActivityType(Integer type) {
        if (ActivityTypeEnum.MCH_PLAN.getCode() == type) {
            return UserTaskTypeEnum.MCH_PLAN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_START.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_START.getCode() == type) {
            return UserTaskTypeEnum.NEW_MAN_START.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_JOIN.getCode() == type) {
            return UserTaskTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (ActivityTypeEnum.ORDER_REDPACK.getCode() == type) {
            return UserTaskTypeEnum.ORDER_REDPACK.getCode();
        }
        if (ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
            return UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode();
        }
        if (ActivityTypeEnum.ASSIST.getCode() == type) {
            return UserTaskTypeEnum.ASSIST_START.getCode();
        }
        if (ActivityTypeEnum.LUCK_WHEEL.getCode() == type) {
            return UserTaskTypeEnum.LUCK_WHEEL.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == type) {
            return UserTaskTypeEnum.OPEN_RED_PACKET_START.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode() == type) {
            return UserTaskTypeEnum.OPEN_RED_PACKET_JOIN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START_V2.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_START_V2.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode();
        }
        if (ActivityTypeEnum.ASSIST_V2.getCode() == type) {
            return UserTaskTypeEnum.ASSIST_START_V2.getCode();
        }
        if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == type) {
            return UserTaskTypeEnum.FULL_REIMBURSEMENT.getCode();
        }
        if (ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode() == type) {
            return UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode();
        }

        // 异常类型
        return -1;
    }

    /**
     * 从用户活动类型 获取 相应的任务类型
     *
     * @param type
     * @return
     */
    public static Integer findByUserActivityType(Integer type) {
        if (UserActivityTypeEnum.MCH_PLAN.getCode() == type) {
            return UserTaskTypeEnum.MCH_PLAN.getCode();
        }
        if (UserActivityTypeEnum.ORDER_BACK_START.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_START.getCode();
        }
        if (UserActivityTypeEnum.ORDER_BACK_JOIN.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (UserActivityTypeEnum.NEW_MAN_START.getCode() == type) {
            return UserTaskTypeEnum.NEW_MAN_START.getCode();
        }
        if (UserActivityTypeEnum.NEW_MAN_JOIN.getCode() == type) {
            return UserTaskTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (UserActivityTypeEnum.ASSIST.getCode() == type) {
            return UserTaskTypeEnum.ASSIST_START.getCode();
        }
        if (UserActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == type) {
            return UserTaskTypeEnum.OPEN_RED_PACKET_START.getCode();
        }
        if (UserActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode() == type) {
            return UserTaskTypeEnum.OPEN_RED_PACKET_JOIN.getCode();
        }
        if (UserActivityTypeEnum.ORDER_BACK_START_V2.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_START_V2.getCode();
        }
        if (UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode() == type) {
            return UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode();
        }
        if (UserActivityTypeEnum.ASSIST_V2.getCode() == type) {
            return UserTaskTypeEnum.ASSIST_START_V2.getCode();
        }
        if (UserActivityTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode() == type) {
            return UserTaskTypeEnum.FULL_REIMBURSEMENT.getCode();
        }

        // 异常类型
        return -1;
    }
}
