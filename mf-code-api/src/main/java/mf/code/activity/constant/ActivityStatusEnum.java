package mf.code.activity.constant;

/**
 * mf.code.common.constant
 * Description: 活动状态
 *
 * @author: gel
 * @date: 2018-11-06 20:09
 */
public enum ActivityStatusEnum {
    /**
     * 待发布
     */
    UNPUBLISHED(0, "待发布"),
    /**
     * 已发布
     */
    PUBLISHED(1, "已发布"),
    /**
     * 已结束
     */
    END(2, "已结束"),
    /**
     * 抽奖未成功
     */
    FAILURE(-1, "抽奖未成功"),
    /**
     * 强制下线
     */
    DEL(-2, "强制下线"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    ActivityStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ActivityStatusEnum findByCode(int code){
        for (ActivityStatusEnum statusEnum : ActivityStatusEnum.values()) {
            if(statusEnum.getCode() == code){
                return statusEnum;
            }
        }
        return null;
    }
}
