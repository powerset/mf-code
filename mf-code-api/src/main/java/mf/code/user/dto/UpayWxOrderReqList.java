package mf.code.user.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.user.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-26 15:05
 */
@Data
public class UpayWxOrderReqList {
    private List<UpayWxOrderReq> upayWxOrderReqList;
}
