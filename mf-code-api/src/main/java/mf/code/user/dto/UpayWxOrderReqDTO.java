package mf.code.user.dto;

import lombok.Data;

/**
 * mf.code.user.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月12日 19:54
 */
@Data
public class UpayWxOrderReqDTO {
    /**
     * 商户号 冗余字段
     */
    private Long merchantId;

    /**
     * 店铺编号
     */
    private Long shopId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单类型 0：用户提现 1：平台金额充值 2:平台退款3:用户支付
     */
    private Integer type;

    /**
     * 支付方式：1:余额 2:微信
     */
    private Integer payType;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 小程序appid 冗余字段
     */
    private String appletAppId;

    /**
     * 订单状态  0：进行中  1：成功   2：失效  3：超时（只存在逻辑）
     */
    private Integer status;

    /**
     * 总金额,单位元
     */
    private String totalFee;

    /**
     * 交易方式：1：JSAPI--公众号支付|小程序支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    private Integer tradeType;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 流水号
     */
    private String tradeId;

    /**
     * 备注说明
     */
    private String remark;

    /**
     * 订单入参，json对象
     */
    private String reqJson;

    /**
     * 业务类型，1:"活动计划类"),2:"轮盘抽奖"),3:"回填订单"),4:"收藏加购"),5:"好评返现"),6:"拆红包活动"),7:"助力活动"),8:"免单抽奖"),9:"新人有礼"),
     */
    private Integer bizType;

    /**
     * 业务类型针对的值1:活动编号，2:活动定义编号3:活动任务编号4活动任务编号5活动任务编号6活动编号7活动编号8活动编号9活动编号
     */
    private Long bizValue;

    /**
     * 当交易方式为JSAPI的时候，1:为公众号支付 2:小程序支付
     */
    private Integer jsapiScene;

    /**
     * 退款时，需要退款的订单主键编号
     */
    private Long refundOrderId;
}
