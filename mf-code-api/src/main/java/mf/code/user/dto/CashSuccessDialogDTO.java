package mf.code.user.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.user.dto
 *
 * @description: 提现成功后的弹窗
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月19日 14:45
 */
@Data
public class CashSuccessDialogDTO {

    /***
     * 弹窗类型
     */
    private int type;
    /***
     * 团队人数带来的预估收益
     */
    private String estimateProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    /***
     * 团队人数
     */
    private int teamSize;
}
