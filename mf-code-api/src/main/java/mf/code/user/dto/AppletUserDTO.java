package mf.code.user.dto;

import lombok.Data;

/**
 * mf.code.user.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-11 00:46
 */
@Data
public class AppletUserDTO {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 用户信息
	 */
	private UserResp userResp;
	/**
	 * 用户收入信息
	 */
	private UserIncomeResp userIncomeResp;
}
