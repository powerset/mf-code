package mf.code.user.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户收入信息
 * mf.code.user.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-11 00:10
 */
@Data
public class UserIncomeResp {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 最近一次商城推广收益结算时间
	 */
	private Date settledTime;
	/**
	 * 从推广到现在 商城购物总返利金额 -- 已结算
	 */
	private BigDecimal totalRebateIncome;
	/**
	 * 从推广到现在 商城团队总贡献给我的金额 -- 已结算
	 */
	private BigDecimal contributionIncome;
}
