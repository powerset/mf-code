package mf.code.user.dto;

import lombok.Data;

/**
 * mf.code.user.dto
 * Description:
 *
 * @author gel
 * @date 2019-04-13 11:56
 */
@Data
public class SellerUserAddressResultDTO {
    private Long userId;
    /**
     * 用户所属店铺
     */
    private Long vipShopId;
    private Long addressId;
    private String avatarUrl;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 国家
     */
    private String country;
    /**
     * 省份
     */
    private String province;
    /**
     * 城市
     */
    private String city;
    /**
     * 街区
     */
    private String street;
    /**
     * 详细
     */
    private String detail;
    /***
     * 收货人
     */
    private String userName;
    /***
     * 微信昵称
     */
    private String nick;
}
