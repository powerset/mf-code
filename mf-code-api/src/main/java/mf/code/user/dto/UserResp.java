package mf.code.user.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.user.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月11日 15:26
 */
@Data
public class UserResp {
    /**
     * 用户ID
     */
    private Long id;

    /**
     * openid
     */
    private String openId;

    /**
     * 商户Id，merchant表的主键
     */
    private Long merchantId;

    /**
     * 对应商户id下的店铺id
     */
    private Long shopId;

    /**
     * 登录场景
     */
    private Integer scene;

    /**
     * sessionKey
     */
    private String sessionKey;

    /**
     * 公众平台openid
     */
    private String wxopenid;

    /**
     * unionid
     */
    private String unionId;

    /**
     * 用户手机号码
     */
    private String mobile;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像图片地址
     */
    private String avatarUrl;

    /**
     * 性别，0:未知；1:男；2:女
     */
    private Integer gender;

    /**
     * 城市
     */
    private String city;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 用户授权状态，0:未授权，1:授权，-1机器人，-2平台
     */
    private Integer grantStatus;

    /**
     * 角色：0:普通用户；1:店长
     */
    private Integer role;

    /**
     * 成为角色的时间
     */
    private Date roleTime;

    /**
     * 微信个人二维码，微信群二维码 {“personalQR”:”url1”,”groupQR”:”url2”}
     */
    private String wxCode;

    /**
     * 用户授权时间
     */
    private Date grantTime;

    /**
     * 此用户属于为哪个店铺的粉丝
     */
    private Long vipShopId;

    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;
}
