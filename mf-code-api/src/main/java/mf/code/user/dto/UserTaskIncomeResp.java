package mf.code.user.dto;

import lombok.Data;
import mf.code.one.dto.UserCouponDTO;
import mf.code.one.dto.UserTaskDTO;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.user.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-17 14:32
 */
@Data
public class UserTaskIncomeResp {
	/**
	 * userId
	 */
	private Long userId;
	/**
	 * 今日任务奖金
	 */
	private BigDecimal todayAward;
	/**
	 * 今日任务佣金
	 */
	private BigDecimal todayCommission;
	/**
	 * 累计任务奖金
	 */
	private BigDecimal totalAward;
	/**
	 * 累计任务佣金
	 */
	private BigDecimal totalCommission;
	/**
	 * 店长任务资格
	 */
	private Map<Long, UserTaskDTO> userTaskMap;
	/**
	 * 店长任务 奖励情况
	 */
	private Map<Long, UserCouponDTO> userCouponMap;
}
