package mf.code.user.vo;

import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.user.repo.vo
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 11:01
 */
public class UpayWxOrderAboutVO {

    /***
     * 自购省-类型
     * @return
     */
    public static List<Integer> addSelfPurchaseRebate() {
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderBizTypeEnum.REBATE.getCode());
        return list;
    }

    /***
     * 分享赚-类型
     * @return
     */
    public static List<Integer> addShareCommission() {
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderBizTypeEnum.CONTRIBUTION.getCode());
        return list;
    }

    /***
     * 任务奖金-类型
     * @return
     */
    public static List<Integer> addTaskAward() {
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderBizTypeEnum.ACTIVITY.getCode());
        list.add(UpayWxOrderBizTypeEnum.LUCKY_WHEEL.getCode());
        list.add(UpayWxOrderBizTypeEnum.RANDOMREDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.REDPACKTASK.getCode());
        list.add(UpayWxOrderBizTypeEnum.GOODSCOMMENT.getCode());
        list.add(UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode());
        list.add(UpayWxOrderBizTypeEnum.ASSIST_ACTIVITY.getCode());
        list.add(UpayWxOrderBizTypeEnum.ORDER_BACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.CHECKPOINT.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWMAN_START.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        list.add(UpayWxOrderBizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode());
        list.add(UpayWxOrderBizTypeEnum.FULL_REIMBURSEMENT.getCode());
        return list;
    }

    /***
     * 任务佣金-类型
     * @return
     */
    public static List<Integer> addTaskCommission() {
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderBizTypeEnum.ACTIVITY.getCode());
        list.add(UpayWxOrderBizTypeEnum.LUCKY_WHEEL.getCode());
        list.add(UpayWxOrderBizTypeEnum.RANDOMREDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.REDPACKTASK.getCode());
        list.add(UpayWxOrderBizTypeEnum.GOODSCOMMENT.getCode());
        list.add(UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode());
        list.add(UpayWxOrderBizTypeEnum.ASSIST_ACTIVITY.getCode());
        list.add(UpayWxOrderBizTypeEnum.ORDER_BACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.CHECKPOINT.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWMAN_START.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        list.add(UpayWxOrderBizTypeEnum.LEVLEUP_SHOP_MANAGER.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_PAY.getCode());
        return list;
    }

    /***
     * 赚收益页面已赚收益的具体类型界定()
     * @return
     */
    public static List<Integer> finishProfitUpayWxOrderTypes() {
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode());
        list.add(UpayWxOrderTypeEnum.REBATE.getCode());
        return list;
    }

    /***
     * 赚收益页面已赚收益的具体业务类型界定()
     * @return
     */
    public static List<Integer> finishProfitUpayWxOrderBizTypes() {
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderBizTypeEnum.RANDOMREDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.REDPACKTASK.getCode());
        list.add(UpayWxOrderBizTypeEnum.GOODSCOMMENT.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(UpayWxOrderBizTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        return list;
    }
}
