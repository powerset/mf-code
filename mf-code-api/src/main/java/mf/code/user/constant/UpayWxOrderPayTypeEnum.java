package mf.code.user.constant;

public enum UpayWxOrderPayTypeEnum {
    CASH(1, "余额"),
    WEIXIN(2, "微信");
    private int code;
    private String message;

    UpayWxOrderPayTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
