package mf.code.user.constant;

/**
 * mf.code.upay.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 18:14
 */
public enum JsapiSceneEnum {

    PUBJSAPI(1, "公众号支付"),
    APPLETJSAPI(2, "小程序支付");
    private int code;
    private String message;

    JsapiSceneEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
