package mf.code.user.constant;

public enum UpayWxOrderPendTypeEnum {
    USERCASH(0, "用户提现"),
    PALTFORMRECHARGE(1, "平台充值"),
    PALTFORMREFUND(2, "平台退款"),
    USERPAY(3, "用户支付"),
    REBATE(4, "任务分佣"),
    ;
    private int code;
    private String message;

    UpayWxOrderPendTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
