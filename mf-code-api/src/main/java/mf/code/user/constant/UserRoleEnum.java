package mf.code.user.constant;

/**
 * mf.code.user.constant
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-15 08:42
 */
public enum UserRoleEnum {
	NORMAL(0, "普通用户"),
	SHOP_MANAGER(1, "店长"),
	DOU_DAI_DAI(2, "抖带带"),
	DOU_XIAO_PU(3, "抖小铺"),
	;
	private int code;
	private String message;

	UserRoleEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
