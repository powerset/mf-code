package mf.code.user.constant;

public enum UpayWxOrderPendStatusEnum {
    ORDERING(0, "下单中"),
    ORDERED_PENDING(1, "下单成功(待结算)"),
    FAILED(2, "订单失效"),
    TIMEOUT(3, "订单超时（只存在逻辑）"),
    ;
    private int code;
    private String message;

    UpayWxOrderPendStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
