package mf.code.user.constant;

/**
 * mf.code.upay.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 18:14
 */
public enum UpayWxOrderBizTypeEnum {

    /**
     * 免单抽奖
     */
    ACTIVITY(1, "活动计划类"),
    /**
     * 轮盘抽奖
     */
    LUCKY_WHEEL(2, "轮盘抽奖"),
    /**
     * 回填订单
     */
    RANDOMREDPACK(3, "回填订单"),
    /**
     * 收藏加购
     */
    REDPACKTASK(4, "收藏加购"),
    /**
     * 好评返现
     */
    GOODSCOMMENT(5, "好评返现"),
    /**
     * 拆红包活动
     */
    OPEN_RED_PACKAGE(6, "拆红包活动"),
    /***
     * 助力活动
     */
    ASSIST_ACTIVITY(7, "助力活动"),
    /***
     * 免单抽奖
     */
    ORDER_BACK(8, "免单抽奖"),
    /***
     * 新手有礼发起
     */
    NEWMAN_START(9, "新人有礼"),

    /***
     * 财富大闯关
     */
    CHECKPOINT(10, "财富大闯关"),
    /***
     * 用户返利
     */
    REBATE(11, "购买返利"),
    /***
     * 团队贡献
     */
    CONTRIBUTION(12, "团队贡献"),
    /**
     * 购物
     */
    SHOPPING(14, "购物"),
    /***
     * 新手任务-加群享福利
     */
    NEWBIE_TASK_BONUS(15, "新手任务-加群享福利"),
    /***
     * 新手任务-查看攻略获得红包
     */
    NEWBIE_TASK_REDPACK(16, "新手任务-查看攻略获得红包"),
    /***
     * 店长任务-店长任务_设置店长wx群获得奖励
     */
    SHOP_MANAGER_TASK_ADD_WX(17, "店长任务_设置店长wx群获得奖励"),
    /***
     * 店长任务-店长任务_邀请粉丝获得奖励
     */
    SHOP_MANAGER_TASK_INVITE_FANS(18, "店长任务_邀请粉丝获得奖励"),
    /***
     * 店长任务-店长任务_推荐粉丝成为店长
     */
    SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER(19, "店长任务_推荐粉丝成为店长"),
    /***
     * 新手任务-新人红包，奖励到
     */
    NEWBIE_TASK_OPENREDPACK(20, "新手任务-新人红包，奖励到"),
    /***
     * 老版本闯关金额结算
     */
    CHECKPOINT_OLD_DATA_CLEANED(21, "老版本闯关金额结算"),
    /***
     * 升级成为店长粉丝佣金到账
     */
    LEVLEUP_SHOP_MANAGER(22, "升级成为店长粉丝佣金到账"),
    /***
     * 报销活动
     */
    FULL_REIMBURSEMENT(23, "报销活动"),
    /***
     * 付费升级店长
     */
    SHOP_MANAGER_PAY(24, "付费升级店长"),
    ;

    private int code;
    private String message;

    UpayWxOrderBizTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String findByDesc(int code) {
        for (UpayWxOrderBizTypeEnum typeEnum : UpayWxOrderBizTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return null;
    }

    public static UpayWxOrderBizTypeEnum findByCode(int code) {
        for (UpayWxOrderBizTypeEnum typeEnum : UpayWxOrderBizTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return null;
    }
}
