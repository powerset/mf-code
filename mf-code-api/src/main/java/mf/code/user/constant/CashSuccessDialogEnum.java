package mf.code.user.constant;

/**
 * mf.code.user.constant
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月19日 14:54
 */
public enum CashSuccessDialogEnum {
    /***
     * 正常
     */
    NOMAL(0, "正常弹窗-成功"),
    /***
     * 拆红包下单后提现：已升级店长&红包还有库存
     */
    ONEMORE_OPENREDPACK(1, "拆红包-已成为店长"),
    /***
     * 拆红包下单后提现：未升级店长
     */
    LEVELUP_SHOPMANAGER(2, "升级成为店长"),
    /***
     * 完成2个新手任务后提现
     */
    FINISH_NEWBIETASK(3, "完成2个新手任务"),
    /***
     * 报销活动
     */
    FULL_REIMBURSEMENT(4, "报销活动"),
    ;
    private int code;
    private String message;

    CashSuccessDialogEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
