package mf.code.user.constant;

import java.util.List;

/**
 * mf.code.upay.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 18:14
 */
public enum UpayWxOrderPendBizTypeEnum {

    /**
     * 免单抽奖
     */
    ACTIVITY(1, "活动计划类"),
    /**
     * 轮盘抽奖
     */
    LUCKY_WHEEL(2, "轮盘抽奖"),
    /**
     * 回填订单
     */
    RANDOMREDPACK(3, "回填订单"),
    /**
     * 收藏加购
     */
    REDPACKTASK(4, "收藏加购"),
    /**
     * 好评返现
     */
    GOODSCOMMENT(5, "好评返现"),
    /**
     * 拆红包活动
     */
    OPEN_RED_PACKAGE(6, "拆红包活动"),
    /***
     * 助力活动
     */
    ASSIST_ACTIVITY(7, "助力活动"),
    /***
     * 免单抽奖
     */
    ORDER_BACK(8, "免单抽奖"),
    /***
     * 新手有礼发起
     */
    NEWMAN_START(9, "新人有礼"),

    /***
     * 财富大闯关
     */
    CHECKPOINT(10,"财富大闯关"),
    /***
     * 用户返利
     */
    REBATE(11, "购买返利"),
    /***
     * 团队贡献
     */
    CONTRIBUTION(12, "团队贡献"),
    /**
     * 商家所得
     */
    SHOP_INCOME(13, "商家所得"),
    /**
     * 购物
     */
    SHOPPING(14, "购物"),

    TRANSACTION(15, "订单成交-商户交易税"),
    SPECS_COMMISSION(16, "订单成交-平台商品类目抽佣"),
    USER_COMMISSION_RECYCLING(17, "订单成交-上级缺失 用户分佣回收"),
    USER_COMMISSION_TAX(18, "订单成交-用户返利分佣税务部分回收"),
    USER_TEAM_REWARD(19, "订单成交-用户返利分佣团队奖励部分回收"),
    /***
     * 付费升级店长
     */
    SHOP_MANAGER_PAY(24, "付费升级店长"),
    ;

    private int code;
    private String message;

    UpayWxOrderPendBizTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String findByDesc(int code){
        for (UpayWxOrderPendBizTypeEnum typeEnum : UpayWxOrderPendBizTypeEnum.values()) {
            if(typeEnum.getCode() == code){
                return typeEnum.getMessage();
            }
        }
        return null;
    }
}
