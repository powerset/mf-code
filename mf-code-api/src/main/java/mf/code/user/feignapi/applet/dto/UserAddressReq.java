package mf.code.user.feignapi.applet.dto;

import lombok.Data;

import java.util.Date;

/**
 * mf.code.user.feignclient.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月01日 09:04
 */
@Data
public class UserAddressReq {
    /**
     * 用户收货地址主键id
     */
    private Long id;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 地址类型 1：普通 2：默认
     */
    private Integer type;

    /**
     * 国家编码
     */
    private String countryCode;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份编码
     */
    private String provinceCode;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 城市
     */
    private String city;

    /**
     * 街区
     */
    private String street;

    /**
     * 详细
     */
    private String detail;

    /**
     * 邮政编码
     */
    private String postalCode;

    /**
     * 删除标记 . 0:正常 1:删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;
    /***
     * 收货人
     */
    private String nick;
}
