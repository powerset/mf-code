package mf.code.user.feignapi.applet.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.user.feignclient.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月01日 15:15
 */
@Data
public class UserAddressList {
    private List<UserAddressReq> address;
}
