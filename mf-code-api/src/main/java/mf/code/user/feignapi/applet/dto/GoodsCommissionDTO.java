package mf.code.user.feignapi.applet.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.user.feignclient.applet.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-09 14:58
 */
@Data
public class GoodsCommissionDTO {
	/**
	 * 支付订单者
	 */
	private Long userId;
	/**
	 * 订单id
	 */
	private Long GoodsOrderId;
	/**
	 * 分销商商户id
	 */
	private Long merchantId;
	/**
	 * 分销商店铺id
	 */
	private Long shopId;
	/**
	 * 供应商商户id
	 */
	private Long parentMerchantId;
	/**
	 * 供应商店铺id
	 */
	private Long parentShopId;
	/**
	 * 订单编号
	 */
	private String orderNo;
	/**
	 * 交易税
	 */
	private BigDecimal transactionCommission;
	/**
	 * 供货商佣金
	 */
	private BigDecimal supplierCommission;
	/**
	 * 供货商所得
	 */
	private BigDecimal supplierIncome;
	/**
	 * 平台抽佣
	 */
	private BigDecimal platformCommission;
	/**
	 * 分销广场佣金
	 */
	private BigDecimal squareCommission;
	/**
	 * 分销商抽佣
	 */
	private BigDecimal distributorCommission;
	/**
	 * 用户可分配佣金
	 */
	private BigDecimal customerCommission;
	/**
	 * 返利比率 0-100
	 */
	private Integer rebateRate;
	/**
	 * 贡献比率 0-100
	 */
	private Integer commissionRate;
	/**
	 * 贡献层级
	 */
	private Integer scale;
}
