package mf.code.user.feignapi.applet;//package mf.code.user.feignclient.applet;
//
//import feign.hystrix.FallbackFactory;
//import mf.code.common.simpleresp.SimpleResponse;
//import mf.code.user.dto.UpayWxOrderReq;
//import mf.code.user.dto.UpayWxOrderReqDTO;
//import mf.code.user.dto.UserRespDTO;
//import mf.code.user.feignclient.applet.dto.GoodsCommissionDTO;
//import mf.code.user.feignclient.applet.dto.IncomeOverviewTomonthDTO;
//import mf.code.user.feignclient.applet.dto.UserAddressReq;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
///**
// * mf.code.user.feignclient.applet
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年04月13日 17:23
// */
//@Component
//public class UserAppFeignFallbackFactory implements FallbackFactory<UserAppService> {
//    @Override
//    public UserAppService create(Throwable throwable) {
//        return new UserAppService() {
//            @Override
//            public IncomeOverviewTomonthDTO queryUserBillThisMonth(String userId, String ShopId) {
//                return null;
//            }
//
//            @Override
//            public SimpleResponse saveDistributionCommission(GoodsCommissionDTO goodsCommissionDTO) {
//                return null;
//            }
//
//            @Override
//            public SimpleResponse cancelDistributionCommission(String orderNo, String shopId) {
//                return null;
//            }
//
//            @Override
//            public UserAddressReq queryAdddress(Long addressId) {
//                return null;
//            }
//
//            @Override
//            public UserRespDTO queryUser(Long userId) {
//                return null;
//            }
//
//            @Override
//            public List<Long> selectAllIds() {
//                return null;
//            }
//
//            @Override
//            public Integer updateUserBalance(UpayWxOrderReq upayWxOrderReq) {
//                return null;
//            }
//
//            @Override
//            public List<Long> queryUpperIds(Long userId, Long shopId, Integer scale) {
//                return null;
//            }
//
//            @Override
//            public int saveUpayWxOrder(UpayWxOrderReqDTO reqDTO) {
//                return 0;
//            }
//
//            @Override
//            public SimpleResponse queryUserAndAddressForSeller(Long userId, Long addressId) {
//                return null;
//            }
//
//            @Override
//            public SimpleResponse queryAddressListForSeller(List<Long> addressIdList) {
//                return null;
//            }
//
//            @Override
//            public SimpleResponse queryAddressMapForSeller(List<Long> addressIdList) {
//                return null;
//            }
//
//            @Override
//            public SimpleResponse queryUserIdByAddressNick(String nick) {
//                return null;
//            }
//        };
//    }
//}
