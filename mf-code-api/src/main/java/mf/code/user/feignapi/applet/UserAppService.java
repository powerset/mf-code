package mf.code.user.feignapi.applet;//package mf.code.user.feignclient.applet;
//
//import mf.code.common.simpleresp.SimpleResponse;
//import mf.code.user.dto.UpayWxOrderReq;
//import mf.code.user.dto.UpayWxOrderReqDTO;
//import mf.code.user.dto.UserRespDTO;
//import mf.code.user.feignclient.applet.dto.GoodsCommissionDTO;
//import mf.code.user.feignclient.applet.dto.UserBillDTO;
//import mf.code.user.feignclient.applet.dto.UserAddressReq;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
///**
// * mf.code.user.feignclient.applet
// * Description:
// *
// * @author: 百川
// * @date: 2019-04-01 19:38
// */
//@FeignClient(value = "mf-code-user"//,fallbackFactory = UserAppFeignFallbackFactory.class
//)
//public interface UserAppService {
//
//    /**
//     * 查询 在此店铺内， 用户本月消费和收入的概况
//     *
//     * @param userId
//     * @param shopId
//     * @return statusCode = 0: 用户本月 预估收益，消费条件，消费金额，达标差额
//     * statusCode = 1: 本月累计收益，最新团员贡献佣金
//     */
//    @GetMapping("/feignclient/user/applet/v5/queryUserBillThisMonth")
//    UserBillDTO queryUserBillThisMonth(@RequestParam("userId") String userId,
//                                       @RequestParam("shopId") String shopId);
//
//    /**
//     * 对商品的佣金进行存储
//     *
//     * @param goodsCommissionDTO
//     * @return
//     */
//    @PostMapping("/feignclient/user/applet/v5/saveDistributionCommission")
//    SimpleResponse saveDistributionCommission(@RequestBody GoodsCommissionDTO goodsCommissionDTO);
//
//    /**
//     * 订单取消，对商品返利及分佣的记录 设置失效
//     */
//    @PostMapping("/feignclient/user/applet/v5/cancelDistributionCommission")
//    SimpleResponse cancelDistributionCommission(@RequestParam("orderNo") String orderNo,
//                                                @RequestParam("shopId") String shopId);
//
//    /***
//     * 查询收货地址
//     * @param addressId
//     * @return
//     */
//    @GetMapping("/feignclient/user/applet/v5/queryAdddress")
//    UserAddressReq queryAdddress(@RequestParam("addressId") Long addressId);
//
//	/***
//	 * 获取用户信息
//	 * @param userId
//	 * @return
//	 */
//	@GetMapping("/feignclient/user/applet/v5/queryUser")
//	UserRespDTO queryUser(@RequestParam("userId") Long userId);
//
//	/**
//	 * 获取所有授权的用户ids
//	 * @return
//	 */
//	@GetMapping("/feignclient/user/applet/v5/selectAllIds")
//	List<Long> selectAllIds();
//
//	/**
//	 * 插入记录 并 更新余额
//	 * @param upayWxOrderReq
//	 * @return
//	 */
//	@PostMapping("/feignclient/user/applet/v5/updateUserBalance")
//	Integer updateUserBalance(@RequestBody UpayWxOrderReq upayWxOrderReq);
//
//	/**
//	 * 获取此用户的上级ids
//	 * @param userId
//	 * @param shopId
//	 * @param scale 层级数
//	 * @return
//	 */
//	@GetMapping("/feignclient/user/applet/v5/queryUpperIds")
//	List<Long> queryUpperIds(@RequestParam("userId") Long userId,
//	                         @RequestParam("shopId") Long shopId,
//	                         @RequestParam("scale")Integer scale);
//
//    @PostMapping("/feignclient/user/applet/v5/saveUpayWxOrder")
//    int saveUpayWxOrder(@RequestBody UpayWxOrderReqDTO reqDTO);
//
//    /**
//     * 获取用户地址信息和用户信息
//     *
//     * @param userId
//     * @param addressId
//     * @return
//     */
//    @GetMapping("/feignclient/user/seller/v5/queryUserAddress")
//    SimpleResponse queryUserAndAddressForSeller(@RequestParam(value = "userId") Long userId,
//                                                @RequestParam(value = "addressId") Long addressId);
//
//    /**
//     * 获取用户地址信息List
//     *
//     * @param addressIdList
//     * @return
//     */
//    @GetMapping("/feignclient/user/seller/v5/queryAddressList")
//    SimpleResponse queryAddressListForSeller(@RequestParam(value = "addressIdList") List<Long> addressIdList);
//
//    /**
//     * 获取用户地址信息map
//     *
//     * @param addressIdList
//     * @return
//     */
//    @GetMapping("/feignclient/user/seller/v5/queryAddressMap")
//    SimpleResponse queryAddressMapForSeller(@RequestParam(value = "addressIdList") List<Long> addressIdList);
//
///**
// * 通过买家名模糊查询用户Id列表
// *
// * @param nick
// * @return
// */
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@GetMapping("/feignclient/user/seller/v5/queryAddressIdByAddressNickOrPhoneForSeller")
//	SimpleResponse queryAddressIdByAddressNickOrPhoneForSeller(@RequestParam(value = "nick") String nick,
//@RequestParam(value = "phone") String phone);
//
//	/**
//	 * 更新用户余额结算时间
//	 */
//	@RequestMapping("/feignclient/user/applet/v5/updateUserBalanceSettledTime")
//	void updateUserBalanceSettledTime(@RequestParam("shopId") Long shopId);
//
//
//	/**
//	 * 订单结算
//	 * @param upayWxOrderReqs
//	 */
//	@RequestMapping("/feignclient/user/seller/v5/saveUpayWxOrderPend")
//	void saveUpayWxOrderPend(@RequestParam("upayWxOrderReqs") List<UpayWxOrderReq> upayWxOrderReqs);
//}
