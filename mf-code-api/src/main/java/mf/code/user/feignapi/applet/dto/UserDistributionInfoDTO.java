package mf.code.user.feignapi.applet.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.user.feignclient.applet.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 10:53
 */
@Data
public class UserDistributionInfoDTO {
	/**
	 * 用户本月消费金额
	 */
	private BigDecimal spendingAmountTomonth;
	/**
	 * 本月预估收益 (仅本月团队贡献)
	 */
	private BigDecimal estimateIncome;
	/**
	 * 消费达标额度
	 */
	private BigDecimal commissionStandard;
	/**
	 * 剩余达标差额
	 */
	private BigDecimal leftAmount;
}
