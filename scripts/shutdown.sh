#!/usr/bin/env bash
echo "------------------- shutdown.sh java progress start --------------"

server_list=(
mf-code-one
mf-code-shop
mf-code-goods
mf-code-user
mf-code-order
mf-code-distribution
mf-code-teacher
mf-code-statistics
mf-code-web
mf-code-comm
mf-code-comment
)

for var in ${server_list[@]}
do
   echo "工程：" $var
done
echo "请输入待打包服务工程名:"

read aliasname
case ${aliasname} in
    mf-code-one)  echo '你选择了'${aliasname}
    ;;
    mf-code-shop)  echo '你选择了'${aliasname}
    ;;
    mf-code-goods)  echo '你选择了'${aliasname}
    ;;
    mf-code-user)  echo '你选择了'${aliasname}
    ;;
    mf-code-order)  echo '你选择了'${aliasname}
    ;;
    mf-code-distribution)  echo '你选择了'${aliasname}
    ;;
    mf-code-teacher)  echo '你选择了'${aliasname}
    ;;
    mf-code-statistics)  echo '你选择了'${aliasname}
    ;;
    mf-code-web)  echo '你选择了'${aliasname}
    ;;
    mf-code-comm)  echo '你选择了'${aliasname}
    ;;
    mf-code-comment)  echo '你选择了'${aliasname}
    ;;
    *)  echo "输入的工程名不包含在脚本中，无法执行"
    exit 1
    ;;
esac

echo "请输入服务端口号:"
read port
if [[ ${port} -gt 8099 || ${port} -lt 8001 ]]; then
    echo "输入的端口号不符合规则，无法执行"
    exit 1
fi

#私网IP,通过寻找eth0网卡的方式获取
priIp=$(ifconfig eth0| grep 'inet'|awk '{print $2}')

# 获取公网ip，通过调用外部接口查询的方式获取 curl ifconfig.me | curl icanhazip.com
# curl --connect-timeout SECONDS  Maximum time allowed for connection
# curl -m, --max-time SECONDS  Maximum time allowed for the transfer
pubIp=$(curl --connect-timeout 1 -m 2 ifconfig.me)
if [ -z "$pubIp" ]; then
    pubIp=$(curl --connect-timeout 1 -m 2 icanhazip.com)
fi

echo "公网ip:" ${pubIp}
echo "私网ip:" ${priIp}
echo "当前服务端口"${aliasname} ${port}

## 根据端口号获取java进程号ps aux|grep ${port}|grep -v grep|awk '{print $2}'|xargs kill

echo "------------------- 通知eureka下线服务节点开始 --------------"
curl -X DELETE http://code:password123@129.211.133.28:8001/eureka/apps/${aliasname}/${priIp}:${aliasname}:${port}
curl -X DELETE http://code:password123@129.211.101.141:8001/eureka/apps/${aliasname}/${priIp}:${aliasname}:${port}
echo "------------------- 通知eureka下线服务节点结束 --------------"


# 根据服务工程名查询pid
pid=$(ps ax | grep ${aliasname} | grep java | head -1 | awk '{print $1}')
echo "=================== 通知服务shutdown开始 ====================="
curl -X POST http://${priIp}:${port}/shutdown?token=shutdownByToken20190531
echo "=================== 通知服务shutdown结束 ====================="

## 遍历30秒，如果服务pid为空，则终止循环
MAX_TIMEOUT=30
while (( ${MAX_TIMEOUT}>=0 )); do
    let MAX_TIMEOUT--
    echo '服务停止中...'
    sleep 1
    pid=$(ps ax | grep ${aliasname} | grep java | head -1 | awk '{print $1}')
    echo "------------------- 防止shutdown过程中，服务依然会去eureka注册，所以执行下线操作 --------------"
    curl -X DELETE http://code:password123@129.211.133.28:8001/eureka/apps/${aliasname}/${priIp}:${aliasname}:${port}
    curl -X DELETE http://code:password123@129.211.101.141:8001/eureka/apps/${aliasname}/${priIp}:${aliasname}:${port}
    if [ -z ${pid} ]; then
        echo '服务停止成功...........'
        echo "------------------- shutdown.sh java progress end --------------"
        # 服务停止成功后，直接退出
        exit 1
    fi
done

echo "################ 服务30秒内没有停止 ####################"
read -p "强制停止服务请输入y, 其他字符退出：" forceShutdown
if [ ${forceShutdown}=="y" ]; then
    echo "kill -9" ${pid}
    kill -9 ${pid}
fi

echo "------------------- shutdown.sh java progress end --------------"
