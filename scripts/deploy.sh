#!/usr/bin/env bash
echo "------------------- kill java progress start --------------"

server_list=(
mf-code-one
mf-code-shop
mf-code-goods
mf-code-user
mf-code-order
mf-code-distribution
mf-code-teacher
mf-code-statistics
mf-code-web
mf-code-comm
mf-code-comment
)

for var in ${server_list[@]}
do
   echo "工程：" $var
done
echo "请输入待打包服务工程名:"

read aliasname
case ${aliasname} in
    mf-code-one)  echo '你选择了'${aliasname}
    ;;
    mf-code-shop)  echo '你选择了'${aliasname}
    ;;
    mf-code-goods)  echo '你选择了'${aliasname}
    ;;
    mf-code-user)  echo '你选择了'${aliasname}
    ;;
    mf-code-order)  echo '你选择了'${aliasname}
    ;;
    mf-code-distribution)  echo '你选择了'${aliasname}
    ;;
    mf-code-teacher)  echo '你选择了'${aliasname}
    ;;
    mf-code-statistics)  echo '你选择了'${aliasname}
    ;;
    mf-code-web)  echo '你选择了'${aliasname}
    ;;
    mf-code-comm)  echo '你选择了'${aliasname}
    ;;
    mf-code-comment)  echo '你选择了'${aliasname}
    ;;
    *)  echo "输入的工程名不包含在脚本中，无法执行"
    exit 1
    ;;
esac

echo "请输入服务端口号:"
read port
if [[ ${port} -gt 8099 || ${port} -lt 8001 ]]; then
    echo "输入的端口号不符合规则，无法执行"
    exit 1
fi

# 获取公网ip，通过调用外部接口查询的方式获取 curl ifconfig.me | curl icanhazip.com
pubIp=$(curl --connect-timeout 1 -m 3 ifconfig.me)
if [ -z "$pubIp" ]; then
    pubIp=$(curl --connect-timeout 1 -m 3 icanhazip.com)
fi
if [ -z "$pubIp" ]; then
    read -p "未能获取到公网ip，请手动输入：" pubIp
fi
echo "公网ip:" ${pubIp}
echo "当前服务端口"${aliasname} ${port}
pid=$(ps ax | grep ${aliasname} | grep java | head -1 | awk '{print $1}')
if [ ${pid} ]; then
    echo "存在相同进程，部署程序终止, ${pid}"
    exit 1
fi

echo "------------------- kill java progress end --------------"

echo "******************** deploy start ********************"

nohup java -Dserver.port=${port} -jar /home/code/git-data/mf-code-parent-${port}/${aliasname}/target/${aliasname}-1.0-SNAPSHOT.jar --spring.profiles.active=prod --eureka.instance.ip-address=${pubIp}> /home/code/git-data/mf-code-parent-${port}/${aliasname}/nohup.out 2>&1 &

echo "******************** deploy end ********************"
