#!/usr/bin/env bash

echo "拷贝启动脚本（build.sh,shutdown.sh,deploy.sh）到当前目录指定工程下"
# 返回上级目录
cd ..
# 保存当前目录
parentPwd=$(pwd)
echo "当前目录为：" ${parentPwd}
read -p "请输入服务工程名" aliasname
read -p "请输入服务端口号" port

echo "拷贝build.sh到服务工程名目录下"
if [ ! -d "/data/" ];then
    echo ${parentPwd}/${aliasname} "目录不存在！！退出执行"
    exit
fi
filePath=${parentPwd}/${aliasname}"/build.sh"
if [ ! -f "$filePath" ];then
    cp scripts/build.sh ${parentPwd}/${aliasname}
else
    echo "shutdown.sh已经存在"
fi


echo "拷贝shutdown.sh到服务工程名目录下"
filePath=${parentPwd}/${aliasname}"/shutdown.sh"
if [ ! -f "$filePath" ];then
    cp scripts/shutdown.sh ${parentPwd}/${aliasname}
else
    echo "shutdown.sh已经存在"
fi

echo "拷贝deploy.sh到服务工程名目录下"
filePath=${parentPwd}/${aliasname}"/deploy.sh"
if [ ! -f "$filePath" ];then


else
    echo "shutdown.sh已经存在"
fi

cp src/main/resources/shell/*.sh .
echo "授予权限"
chmod 755 *.sh




