#!/usr/bin/env bash

echo "******************** mf-code-parent build.sh start ********************"

# 跳出到mf-code-parent项目下，执行mvn install
cd ..
pwd

git pull

mvn clean install -Dmaven.test.skip=true

echo "******************** mf-code-parent build.sh end ********************"

