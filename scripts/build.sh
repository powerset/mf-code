#!/usr/bin/env bash

server_list=(
mf-code-one
mf-code-shop
mf-code-goods
mf-code-user
mf-code-order
mf-code-distribution
mf-code-teacher
mf-code-statistics
mf-code-web
mf-code-comm
mf-code-comment
)

for var in ${server_list[@]}
do
   echo "工程：" $var
done
echo "请输入待打包服务工程名:"

read aliasname
case ${aliasname} in
    mf-code-one)  echo '你选择了'${aliasname}
    ;;
    mf-code-shop)  echo '你选择了'${aliasname}
    ;;
    mf-code-goods)  echo '你选择了'${aliasname}
    ;;
    mf-code-user)  echo '你选择了'${aliasname}
    ;;
    mf-code-order)  echo '你选择了'${aliasname}
    ;;
    mf-code-distribution)  echo '你选择了'${aliasname}
    ;;
    mf-code-teacher)  echo '你选择了'${aliasname}
    ;;
    mf-code-statistics)  echo '你选择了'${aliasname}
    ;;
    mf-code-web)  echo '你选择了'${aliasname}
    ;;
    mf-code-comm)  echo '你选择了'${aliasname}
    ;;
    mf-code-comment)  echo '你选择了'${aliasname}
    ;;
    *)  echo "输入的工程名不包含在脚本中，无法执行"
    exit 1
    ;;
esac

# 跳到相应的打包工程目录
cd ../${aliasname}
echo "执行打包目录：$(pwd)"

echo "******************** build.sh start ********************"

git pull

mvn clean package -DskipTests

echo "******************** build.sh end ********************"
