package mf.code.shop.api.seller;

import mf.code.annotation.Upload;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.merchant.service.MerchantCustomService;
import mf.code.shop.constants.AreaLevelEnum;
import mf.code.shop.constants.UploadPath;
import mf.code.shop.dto.*;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.shop.api.seller
 * Description:
 *
 * @author gel
 * @date 2019-04-02 20:02
 */
@RestController
@RequestMapping(value = {"/api/shop/seller/v5/shop/","/api/shop/applet/v5/shop/"})
public class SellerShopApi {

    @Autowired
    private ShopUploadService shopUploadService;
    @Autowired
    private ShopCreateService shopCreateService;
    @Autowired
    private ShopUpdateService shopUpdateService;
    @Autowired
    private ShopInfoService shopInfoService;
    @Autowired
    private ShopThirdService shopThirdService;
    @Autowired
    private MerchantCustomService merchantCustomService;

    /**
     * 店铺logo限制500k以内 500*1024
     */
    private final static Long LOGO_MAX_SIZE = 512000L;
    /**
     * 店铺banner限制2M以内 2*1024*1024
     */
    private final static Long BANNER_MAX_SIZE = 2097152L;

    /**
     * 店铺-店标图片上传
     *
     * @param image 图片
     * @return 地址
     */
    @Upload
    @PostMapping(value = "/logo")
    public SimpleResponse logo(@RequestParam(value = "image") MultipartFile image) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultMap = new HashMap<>();
        if (image.getSize() > LOGO_MAX_SIZE) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("上传店铺logo图片，请不要超过500KB");
            return simpleResponse;
        }
        // 上传自定义logo
        String imageUrl = shopUploadService.uploadWithoutShopId(UploadPath.SHOP_LOGO_PATH, image);
        resultMap.put("customPicPath", imageUrl);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 店铺-创建第一步
     *
     * @param shopStepOneDTO
     * @return
     */
    @PostMapping(value = "/stepone")
    public SimpleResponse stepone(@RequestBody ShopStepOneDTO shopStepOneDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultMap = new HashMap<>();
        // 创建店铺第一步：判断是够成功购买了店铺，并尚未创建
        Long stepOne = shopCreateService.createStepOne(shopStepOneDTO);
        resultMap.put("shopId", stepOne);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 店铺-创建第二步
     *
     * @param shopStepTwoDTO
     * @return
     */
    @PostMapping(value = "/steptwo")
    public SimpleResponse steptwo(@RequestBody ShopStepTwoDTO shopStepTwoDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 创建店铺第二步
        Integer count = shopCreateService.createStepTwo(shopStepTwoDTO);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("店铺创建失败");
            return simpleResponse;
        }
        CustomerServiceResultDTO resultDTO = merchantCustomService.getCustomService(Long.valueOf(shopStepTwoDTO.getMerchantId()));
        simpleResponse.setData(resultDTO);
        return simpleResponse;
    }

    /**
     * 店铺-列表
     *
     * @param merchantId
     * @return
     */
    @GetMapping(value = "/list")
    public SimpleResponse list(@RequestParam(value = "merchantId") Long merchantId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ShopListResultDTO resultDTO = shopInfoService.getList(merchantId);
        simpleResponse.setData(resultDTO);
        return simpleResponse;
    }

    /**
     * 店铺-小程序码查询
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping(value = "/wxacode")
    public SimpleResponse wxacode(@RequestParam(value = "merchantId") Long merchantId,
                                  @RequestParam(value = "shopId") Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultMap = new HashMap<>();
        String wxacode = shopInfoService.getWxacode(merchantId, shopId);
        resultMap.put("wxacodePath", wxacode);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 店铺-小程序版本信息
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping(value = "/versioninfo")
    public SimpleResponse versioninfo(@RequestParam(value = "merchantId") Long merchantId,
                                      @RequestParam(value = "shopId") Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        VersionResultDTO resultDTO = shopInfoService.getVersioninfo(merchantId, shopId);
        simpleResponse.setData(resultDTO);
        return simpleResponse;
    }

    /**
     * 店铺-店铺修改
     *
     * @param shopUpdateReqDTO
     * @return
     */
    @PostMapping(value = "/update")
    public SimpleResponse update(@RequestBody ShopUpdateReqDTO shopUpdateReqDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Integer count = shopUpdateService.updateShop(shopUpdateReqDTO);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("店铺更新失败");
            return simpleResponse;
        }
        // 防腐层
        return simpleResponse;
    }

    /**
     * 店铺-绑定淘宝店铺
     *
     * @param bindTaobaoDTO
     * @return
     */
    @PostMapping(value = "/bindtb")
    public SimpleResponse bindtb(@RequestBody BindTaobaoDTO bindTaobaoDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Integer count = shopThirdService.bindtb(bindTaobaoDTO);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("店铺绑定失败");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 店铺-店铺详情
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping(value = "/detail")
    public SimpleResponse detail(@RequestParam(value = "merchantId") Long merchantId,
                                 @RequestParam(value = "shopId") Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 查询店铺详情
        ShopDetail shopDetail = shopInfoService.detail(merchantId, shopId);
        simpleResponse.setData(shopDetail);
        return simpleResponse;
    }

    /**
     * 店铺-查询店铺装修
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @GetMapping(value = "/customize")
    public SimpleResponse getCustomize(@RequestParam(value = "merchantId") Long merchantId,
                                       @RequestParam(value = "shopId") Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 查询店铺装修
        CustomizeDTO customizeDTO = shopInfoService.getCustomize(merchantId, shopId);
        simpleResponse.setData(customizeDTO);
        return simpleResponse;
    }

    /**
     * 店铺-店铺装修
     *
     * @param customizeDTO
     * @return
     */
    @PostMapping(value = "/customize")
    public SimpleResponse updateCustomize(@RequestBody CustomizeDTO customizeDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 更新店铺装修
        Integer count = shopUpdateService.updateCustomize(customizeDTO);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("店铺装修更新失败");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 店铺-客服图片上传
     *
     * @param shopId
     * @param image
     * @return
     */
    @Upload
    @PostMapping(value = "/csPic")
    public SimpleResponse csPic(@RequestParam(value = "shopId") String shopId,
                                @RequestParam(value = "image") MultipartFile image) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 上传店铺客服图片
        String imgUrl = shopUploadService.uploadWithShopId(Long.valueOf(shopId), UploadPath.SHOP_CSPIC_PATH, image);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("csPic", imgUrl);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 店铺-banner上传
     *
     * @param shopId 店铺id
     * @param image  图片
     * @return 图片地址
     */
    @Upload
    @PostMapping(value = "/banner")
    public SimpleResponse banner(@RequestParam(value = "shopId") String shopId,
                                 @RequestParam(value = "image") MultipartFile image) {
        Assert.isInteger(shopId, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "店铺编号错误");
        SimpleResponse simpleResponse = new SimpleResponse();
        if(image.getSize() > BANNER_MAX_SIZE){
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("上传店铺banner图，请选择小于2MB的图片");
            return simpleResponse;
        }
        // 上传店铺banner
        String imgUrl = shopUploadService.uploadWithShopId(Long.valueOf(shopId), UploadPath.SHOP_BANNER_PATH, image);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("banner", imgUrl);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 创建第三方店铺
     *
     * @param thirdShop
     * @return
     */
    @PostMapping(value = "/createThirdShop")
    public SimpleResponse createThirdShop(@RequestBody ThirdShop thirdShop) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Integer count = shopThirdService.createThirdShop(thirdShop);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("保存淘宝店铺失败,重复旺旺号");
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("thirdShopId", thirdShop.getThirdShopId());
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 创建第三方店铺
     *
     * @param thirdShop
     * @return
     */
    @PostMapping(value = "/updateThirdShop")
    public SimpleResponse updateThirdShop(@RequestBody ThirdShop thirdShop) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Integer count = shopThirdService.updateThirdShop(thirdShop);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("保存淘宝店铺失败");
        }
        return simpleResponse;
    }

    /**
     * 判断是否能创建店铺
     *
     * @param merchantId
     * @return
     */
    @GetMapping(value = "/canCreateShop")
    public SimpleResponse canCreateShop(@RequestParam(value = "merchantId") Long merchantId,
                                        @RequestParam(value = "shopId", required = false) Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        CanCreateShopResultDTO resultDTO = shopInfoService.canCreateShop(merchantId, shopId);
        simpleResponse.setData(resultDTO);
        return simpleResponse;
    }

    /**
     * 查询三级地址
     *
     * @param pid 父级地区id
     * @return
     */
    @GetMapping(value = "/areaList")
    public SimpleResponse getAreaList(@RequestParam(value = "merchantId") Long merchantId,
                                      @RequestParam(value = "pid") Long pid,
                                      @RequestParam(value = "level") Integer level) {
        SimpleResponse simpleResponse = new SimpleResponse();
        AreaListResultDTO areaListResultDTO = shopInfoService.getAreaList(merchantId, pid, level);
        simpleResponse.setData(areaListResultDTO);
        return simpleResponse;
    }
}
