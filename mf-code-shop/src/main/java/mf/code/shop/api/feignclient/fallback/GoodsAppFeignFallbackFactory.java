package mf.code.shop.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.shop.api.feignclient.GoodsAppService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.goods.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 11:10
 */
@Component
@Slf4j
public class GoodsAppFeignFallbackFactory implements FallbackFactory<GoodsAppService> {
    @Override
    public GoodsAppService create(Throwable cause) {
        return new GoodsAppService() {

            @Override
            public AppletUserProductRebateDTO pageGoodsSaleWithSalesVolumeDesc(Long shopId, Integer type, Long offset, Long size) {
                return new AppletUserProductRebateDTO();
            }

            @Override
            public List<Long> getProductIds(Long shopId, Integer type) {
                return new ArrayList<>();
            }
        };
    }
}
