package mf.code.shop.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.shop.api.feignclient.OneAppService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.user.api.feignclient.fallback
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:07
 */
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable cause) {
        return new OneAppService() {
            @Override
            public Map<String, Object> queryOpenRedPackageStatus(Long shopId, Long userId) {
                return new HashMap<>();
            }

            @Override
            public Map<String, Object> queryHomePageNewbieTask(Long shopId, Long userId) {
                return null;
            }
        };
    }
}
