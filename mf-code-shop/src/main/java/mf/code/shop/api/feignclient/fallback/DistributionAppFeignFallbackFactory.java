package mf.code.shop.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.shop.api.feignclient.DistributionAppService;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class DistributionAppFeignFallbackFactory implements FallbackFactory<DistributionAppService> {
    @Override
    public DistributionAppService create(Throwable throwable) {
        return new DistributionAppService() {
            @Override
            public UserDistributionInfoDTO queryDistributionInfo(Long userId, Long shopId) {
                return null;
            }

            @Override
            public String getRebateCommission(ProductDistributionDTO productDTO) {
                return null;
            }

            @Override
            public SimpleResponse distributionCommission(Long userId, Long shopId, Long orderId) {
                return null;
            }

            @Override
            public AppletUserProductRebateDTO queryProductRebateByUserIdShopId(AppletUserProductRebateDTO appletUserProductRebate) {
                return null;
            }
        };
    }
}
