package mf.code.shop.api.applet;/**
 * create by qc on 2019/8/13 0013
 */

import lombok.RequiredArgsConstructor;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.shop.repo.po.ReportUrl;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 抖带带相关新做接口
 *
 * @author gbf
 * 2019/8/13 0013、11:09
 */
@RestController
@RequestMapping("/api/applet/shop/ddd/v1")
@RequiredArgsConstructor
public class AppletDoudaidaiApi {
    public final StringRedisTemplate stringRedisTemplate;


    /**
     * 上报视频地址, 存在redis中
     *
     * @param urlDto
     * @return
     */
    @PostMapping("uploadurl")
    public SimpleResponse uploadurl(@RequestBody ReportUrl urlDto) {
        if (urlDto.getUrl() == null) {
            return new SimpleResponse(1, "视频地址不合法");
        }
        if (urlDto.getProductId() == null) {
            return new SimpleResponse(2, "产品id不合法");
        }
        stringRedisTemplate.opsForValue().set(urlDto.getUrl(), urlDto.getProductId().toString());
        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    /**
     * 通过url获取商品id
     *
     * @param urlDto
     * @return
     */
    @PostMapping("getPidByUrl")
    public SimpleResponse getPidByUrl(@RequestBody ReportUrl urlDto) {
        if (urlDto.getUrl() == null) {
            return new SimpleResponse(1, "视频地址不合法");
        }
        String productId = stringRedisTemplate.opsForValue().get(urlDto.getUrl());
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("productId", productId);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultMap);
    }
}
