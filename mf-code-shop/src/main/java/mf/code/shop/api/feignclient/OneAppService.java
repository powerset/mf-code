package mf.code.shop.api.feignclient;

import mf.code.shop.api.feignclient.fallback.OneAppFeignFallbackFactory;
import mf.code.shop.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.user.api.feignclient
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 20:06
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {

    @GetMapping("/feignapi/one/applet/v6/queryOpenRedPackageStatus")
    Map<String, Object> queryOpenRedPackageStatus(@RequestParam("shopId") Long shopId,
                                                  @RequestParam("userId") Long userId);

    /***
     * 首页获取新手模块数据展现
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/queryHomePageNewbieTask")
    Map<String, Object> queryHomePageNewbieTask(@RequestParam("shopId") Long shopId,
                                                @RequestParam("userId") Long userId);
}
