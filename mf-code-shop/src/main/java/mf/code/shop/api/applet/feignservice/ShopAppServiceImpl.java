package mf.code.shop.api.applet.feignservice;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.dto.MerchantOrderReq;
import mf.code.merchant.repo.dao.MerchantOrderMapper;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.repository.MerchantLoginRepository;
import mf.code.shop.common.property.ShopProperty;
import mf.code.shop.domain.aggregateroot.ShoppingMall;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.shop.dto.ShopDetail;
import mf.code.shop.dto.ShopDetailForPlatformDTO;
import mf.code.shop.dto.ShopListResultDTO;
import mf.code.shop.repo.dao.MerchantShopMapper;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.po.ShopFootprint;
import mf.code.shop.repo.repository.ShopRepostory;
import mf.code.shop.repo.repository.ShoppingMallRepository;
import mf.code.shop.service.MerchantBalanceService;
import mf.code.shop.service.ShopFootprintService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * mf.code.shop.api.applet.feignservice
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 20:56
 */
@Slf4j
@RestController
public class ShopAppServiceImpl {
    @Autowired
    private ShoppingMallRepository shoppingMallRepository;
    @Autowired
    private ShopProperty shopProperty;
    @Autowired
    private ShopRepostory shopRepostory;
    @Autowired
    private MerchantShopMapper merchantShopMapper;
    @Autowired
    private MerchantOrderMapper merchantOrderMapper;
    @Autowired
    private MerchantBalanceService merchantBalanceService;
    @Autowired
    private MerchantLoginRepository merchantLoginRepository;
    @Autowired
    private ShopFootprintService shopFootprintService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 删除足迹
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/deleteShopFootprint")
    public boolean deleteShopFootprint(@RequestParam("userId") Long userId,
                                       @RequestParam("shopId") Long shopId) {
        QueryWrapper<ShopFootprint> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ShopFootprint::getUserId, userId)
                .eq(ShopFootprint::getShopId, shopId);
        ShopFootprint shopFootprint = shopFootprintService.getOne(wrapper);
        if (shopFootprint == null) {
            return true;
        }
        shopFootprint.setDel(DelEnum.YES.getCode());
        boolean success = shopFootprintService.updateById(shopFootprint);
        if (!success) {
            return false;
        }
        return true;
    }

    /**
     * 根据店铺id获取商品id
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/getMerchantIdByShopId")
    public Long getMerchantIdByShopId(@RequestParam("shopId") Long shopId) {
        MerchantShop shop = shopRepostory.getByShopId(shopId);
        if (shop != null) {
            return shop.getMerchantId();
        }
        return null;
    }

    /***
     * 根据商户id查询旗下的所有未删除的店铺
     * @param merchantId
     * @return 店铺集合
     */
    @GetMapping("/feignapi/shop/platform/v5/getMerchantIdByShopList")
    public ShopListResultDTO getMerchantIdByShopList(@RequestParam("merchantId") Long merchantId) {
        List<MerchantShop> merchantShops = shopRepostory.selectUnDelShops(merchantId);
        if (CollectionUtils.isEmpty(merchantShops)) {
            return null;
        }
        ShopListResultDTO shopListResultDTO = new ShopListResultDTO();
        shopListResultDTO.setShopList(new ArrayList<>());

        for (MerchantShop merchantShop : merchantShops) {
            ShopDetail shopDetail = new ShopDetail();
            BeanUtils.copyProperties(merchantShop, shopDetail);
            shopDetail.setShopId(merchantShop.getId());
            shopDetail.setCustomShopName(merchantShop.getShopName());
            shopListResultDTO.getShopList().add(shopDetail);
        }
        return shopListResultDTO;
    }

    /**
     * 获取所有有效店铺的ids
     *
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/selectAllIds")
    public List<Long> selectAllIds() {
        return merchantShopMapper.selectAllIds();
    }

    /**
     * 插入记录 并 更新余额
     *
     * @param merchantOrderReq
     * @return
     */
    @PostMapping("/feignapi/shop/applet/v5/updateMerchantBalance")
    public Integer updateMerchantBalance(@RequestBody MerchantOrderReq merchantOrderReq) {
        if (merchantOrderReq == null) {
            log.error("参数异常");
            return 0;
        }
        // 创建商户余额流水
        MerchantOrder merchantOrder = new MerchantOrder();
        BeanUtils.copyProperties(merchantOrderReq, merchantOrder);
        int insert = merchantOrderMapper.insertSelective(merchantOrder);
        if (insert == 0) {
            log.info("触发 订单防重机制 order_no = {}", merchantOrder.getOrderNo());
            return insert;
        } else {
            Integer rows = merchantBalanceService.updateMerchantBalance(merchantOrder.getMerchantId(), merchantOrder.getShopId(), merchantOrder.getTotalFee());
            if (rows == 0) {
                log.error("商户金额更新失败：merchantId = {}, shopId = {}", merchantOrder.getMerchantId(), merchantOrder.getShopId());
            }
            return rows;
        }
    }

    /**
     * 微信用户 登录小程序 返回相关店铺信息
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/queryShopInfoByLogin")
    public Map<String, Object> queryShopInfoByLogin(@RequestParam("shopId") String shopId) {
        if (StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            shopId = shopProperty.getShopId();
        }
        ShoppingMall shoppingMall = shoppingMallRepository.findById(Long.valueOf(shopId));
        if (shoppingMall == null) {
            log.error("店铺已被删除，shopId = {}", shopId);
            shopId = shopProperty.getShopId();
            // 容错处理，跳转到 测试店铺
            shoppingMall = shoppingMallRepository.findById(Long.valueOf(shopId));

        }
        MerchantShop shoppingMallInfo = shoppingMall.getShoppingMallInfo();
        log.error(JSON.toJSONString(shoppingMallInfo));
        Map<String, Object> shopInfo = new HashMap<>();
        shopInfo.put("serviceWx", JSON.parseArray(shoppingMallInfo.getCsJson(), Object.class));
        shopInfo.put("shopId", shopId);
        shopInfo.put("picPath", shoppingMallInfo.getPicPath());
        shopInfo.put("shopName", shoppingMallInfo.getShopName());
        shopInfo.put("merchantId", shoppingMallInfo.getMerchantId());
        shopInfo.put("purchaseVersion", shoppingMallInfo.getPurchaseVersion());
        log.error(JSON.toJSONString(shopInfo));
        Map<String, Object> homePage = new HashMap<>();
        homePage.put("shopInfo", shopInfo);
        return homePage;
    }


    /**
     * 商户查询 返回相关店铺信息
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/queryShopInfoForSeller")
    public Map<String, Object> queryShopInfoForSeller(@RequestParam("shopId") String shopId) {
        if (StringUtils.isBlank(shopId)) {
            return new HashMap<>();
        }
        MerchantShop byShopId = shopRepostory.getByShopId(Long.valueOf(shopId));
        return BeanMapUtil.beanToMap(byShopId);
    }

    /**
     * 根据商家帐号即手机号获取商户id
     *
     * @param phone
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/getMerchantIdByPhone")
    public Long getMerchantIdByPhone(@RequestParam("phone") String phone) {
        Merchant merchant = merchantLoginRepository.getMerchantByPhone(phone);
        if (merchant != null) {
            return merchant.getId();
        }
        return null;
    }

    /**
     * 根据店铺名称模糊查询店铺id列表
     *
     * @param shopName
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/getShopIdListByShopName")
    public List<Long> getShopIdListByShopName(@RequestParam("shopName") String shopName) {
        return shopRepostory.selectByShopName(shopName);
    }

    /**
     * 根据店铺id列表，查询商品列表信息
     *
     * @param shopIdList
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v5/getShopListByShopIdList")
    public List<ShopDetailForPlatformDTO> getShopListByShopIdList(@RequestParam("shopIdList") List<Long> shopIdList) {
        List<MerchantShop> merchantShopList = shopRepostory.getMerchantShopListByShopIdList(shopIdList);
        if (CollectionUtils.isEmpty(merchantShopList)) {
            return new ArrayList<>();
        }
        Set<Long> merchantIdSet = new HashSet<>();
        for (MerchantShop merchantShop : merchantShopList) {
            merchantIdSet.add(merchantShop.getMerchantId());
        }
        List<Merchant> merchantList = merchantLoginRepository.getMerchantListByIdList(new ArrayList<>(merchantIdSet));
        Map<Long, Merchant> merchantMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(merchantList)) {
            for (Merchant merchant : merchantList) {
                merchantMap.put(merchant.getId(), merchant);
            }
        }
        List<ShopDetailForPlatformDTO> shopDetailForPlatformDTOList = new ArrayList<>();
        for (MerchantShop merchantShop : merchantShopList) {
            ShopDetailForPlatformDTO shopDetailForPlatformDTO = new ShopDetailForPlatformDTO();
            BeanUtils.copyProperties(merchantShop, shopDetailForPlatformDTO);
            Merchant merchant = merchantMap.get(merchantShop.getMerchantId());
            if (merchant != null) {
                shopDetailForPlatformDTO.setMerchantPhone(merchant.getPhone());
            }
            shopDetailForPlatformDTOList.add(shopDetailForPlatformDTO);
        }
        return shopDetailForPlatformDTOList;
    }

    /**
     * 保存用户浏览店铺的足迹
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v7/saveViewShop")
    public boolean saveViewShop(@RequestParam("userId") Long userId,
                                @RequestParam("shopId") Long shopId) {
        if (userId == null || userId < 1 || shopId == null || shopId < 1) {
            log.error("参数异常");
            return false;
        }
        // 校验店铺
        ShoppingMall shoppingMall = shoppingMallRepository.findById(shopId);
        if (shoppingMall == null) {
            log.error("查无此店铺， shopId = {}", shopId);
            return false;
        }
        // 查询 店铺足迹
        QueryWrapper<ShopFootprint> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ShopFootprint::getUserId, userId)
                .eq(ShopFootprint::getShopId, shopId)
                .eq(ShopFootprint::getDel, DelEnum.NO.getCode());
        ShopFootprint shopFootprint = shopFootprintService.getOne(wrapper);
        Date now = new Date();

        MerchantShop merchantShop = shoppingMall.getShoppingMallInfo();
        if (shopFootprint == null) {
            // 不存在 则创建
            shopFootprint = new ShopFootprint();
            shopFootprint.setUserId(userId);
            shopFootprint.setMerchantId(merchantShop.getMerchantId());
            shopFootprint.setShopId(merchantShop.getId());
            shopFootprint.setShopName(merchantShop.getShopName());
            shopFootprint.setPicPath(merchantShop.getPicPath());
            shopFootprint.setCsJson(merchantShop.getCsJson());
            shopFootprint.setDel(DelEnum.NO.getCode());
            shopFootprint.setCtime(now);
            shopFootprint.setUtime(now);
        } else {
            // 存在则更新访问时间
            shopFootprint.setShopName(merchantShop.getShopName());
            shopFootprint.setCsJson(merchantShop.getCsJson());
            shopFootprint.setPicPath(merchantShop.getPicPath());
            shopFootprint.setDel(merchantShop.getDel());
            shopFootprint.setUtime(now);
        }
        try {
            return shopFootprintService.saveOrUpdate(shopFootprint);
        } catch (Exception e) {
            log.error("用户店铺足迹插入失败，可能已存在一条记录, userId = {}, shopId = {}", userId, shopId);
            return false;
        }
    }

    /**
     * 通过id查店铺
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v8/findShopById")
    public MerchantShopDTO findShopById(@RequestParam("shopId") Long shopId) {
        if (shopId == null || shopId < 1) {
            log.error("参数错误");
            return null;
        }
        MerchantShop merchantShop = merchantShopMapper.selectById(shopId);
        MerchantShopDTO merchantShopDTO = new MerchantShopDTO();
        BeanUtils.copyProperties(merchantShop, merchantShopDTO);
        return merchantShopDTO;
    }

    /**
     * 通过merchantId查店铺
     *
     * @param merchantId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/findShopByMerchantId")
    public MerchantShopDTO findShopByMerchantId(@RequestParam("merchantId") Long merchantId) {
        if (merchantId == null || merchantId < 1) {
            log.error("参数错误");
            return null;
        }
        QueryWrapper<MerchantShop> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantShop::getMerchantId, merchantId);
        MerchantShop merchantShop = merchantShopMapper.selectOne(wrapper);
        MerchantShopDTO merchantShopDTO = new MerchantShopDTO();
        BeanUtils.copyProperties(merchantShop, merchantShopDTO);
        return merchantShopDTO;
    }

    /**
     * 通过 openId 获取 抖带带主播信息
     *
     * @param openId
     * @return
     */
    @GetMapping("feignapi/shop/applet/queryMerchantByOpenId")
    public MerchantDTO queryMerchantByOpenId(@RequestParam("openId") String openId) {
        if (StringUtils.isBlank(openId)) {
            log.error("openId 为空");
            return null;
        }
        Merchant merchant = merchantLoginRepository.getMerchantByOpenId(openId);
        if (merchant == null) {
            log.info("新用户");
            return null;
        }
        MerchantDTO merchantDTO = new MerchantDTO();
        BeanUtils.copyProperties(merchant, merchantDTO);
        return merchantDTO;
    }

    /**
     * 通过 merchantId 获取 抖带带主播信息
     *
     * @param merchantId
     * @return
     */
    @GetMapping("feignapi/shop/applet/queryMerchantById")
    public MerchantDTO queryMerchantById(@RequestParam("merchantId") Long merchantId) {
        if (merchantId == null || merchantId < 1) {
            log.error("参数异常");
            return null;
        }
        Merchant merchant = merchantLoginRepository.getMerchantById(merchantId);
        if (merchant == null) {
            log.info("新用户");
            return null;
        }
        MerchantDTO merchantDTO = new MerchantDTO();
        BeanUtils.copyProperties(merchant, merchantDTO);
        return merchantDTO;
    }

    /**
     * 保存或更新merchant
     *
     * @param merchantDTO
     * @return
     */
    @PostMapping("feignapi/shop/applet/saveOrUpdateMerchant")
    public Long saveOrUpdateMerchant(@RequestBody MerchantDTO merchantDTO) {
        if (merchantDTO == null) {
            log.error("参数异常");
            return null;
        }
        Merchant merchant = new Merchant();
        BeanUtils.copyProperties(merchantDTO, merchant);
        if (merchant.getId() == null) {
            int rows = merchantLoginRepository.createMerchant(merchant);
            if (rows == 0) {
                log.error("数据库异常，保存失败");
                return null;
            }
        } else {
            boolean update = merchantLoginRepository.updateById(merchant);
            if (!update) {
                log.error("数据库异常，更新失败");
                return null;
            }
        }
        return merchant.getId();
    }

    /**
     * 保存或更新merchantShopInfo
     *
     * @param merchantShopDTO
     * @return
     */
    @PostMapping("feignapi/shop/applet/saveOrUpdateMerchantShop")
    public Long saveOrUpdateMerchantShop(@RequestBody MerchantShopDTO merchantShopDTO) {
        if (merchantShopDTO == null) {
            log.error("参数异常");
            return null;
        }
        MerchantShop merchantShop = new MerchantShop();
        BeanUtils.copyProperties(merchantShopDTO, merchantShop);
        if (merchantShop.getId() == null) {
            int rows = merchantShopMapper.insertSelective(merchantShop);
            if (rows == 0) {
                log.error("数据库异常，保存失败");
                return null;
            }
        } else {
            int rows = merchantShopMapper.updateByPrimaryKeySelective(merchantShop);
            if (rows == 0) {
                log.error("数据库异常，更新失败");
                return null;
            }
        }
        return merchantShop.getId();
    }

    /**
     * 根据店铺id查询店铺
     *
     * @param shopId
     * @return
     */
    @GetMapping("feignapi/shop/applet/findShopByShopId")
    public MerchantShopDTO findShopByShopId(@RequestParam("shopId") Long shopId) {
        MerchantShop shop = merchantShopMapper.selectByPrimaryKey(shopId);
        MerchantShopDTO merchantShopDTO = new MerchantShopDTO();
        merchantShopDTO.setId(shop.getId());
        merchantShopDTO.setMerchantId(shop.getMerchantId());
        merchantShopDTO.setPhone(shop.getPhone());
        merchantShopDTO.setPurchaseVersion(shop.getPurchaseVersion());
        return merchantShopDTO;
    }
}
