package mf.code.shop.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.order.dto.OrderResp;
import mf.code.shop.api.feignclient.OrderAppService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.order.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月03日 14:40
 */
@Component
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public int countGoodsNumByOrder(Long goodsId) {
                return 0;
            }

            @Override
            public OrderResp queryOrder(Long orderId) {
                return new OrderResp();
            }

            @Override
            public int summaryOrderByShopIdAndProductId(Long shopId, Long productId) {
                return 0;
            }

            @Override
            public List<OrderResp> queryOrderSettledByUserIdShopId(Long userId, Long shopId) {
                return new ArrayList<>();
            }

            @Override
            public Map<String, OrderResp> queryOrderRecordThisMonth(Long userId, Long shopId) {
                return new HashMap<>();
            }

            @Override
            public Map<String, String> countGoodsNumByProductIds(List<Long> productIds) {
                return new HashMap<>();
            }

            @Override
            public Integer queryOrderNumByProductIds(Long shopId, Long userId, List<Long> productIds) {
                return null;
            }
        };
    }
}
