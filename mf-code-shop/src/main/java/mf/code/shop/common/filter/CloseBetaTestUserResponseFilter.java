package mf.code.shop.common.filter;/**
 * create by qc on 2019/6/28 0028
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netflix.ribbon.proxy.annotation.Http;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.cms.PasswordRecipientId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author gbf
 * 2019/6/28 0028、09:33
 */
//@Slf4j
//@Component
//public class CloseBetaTestUserResponseFilter implements Filter {
//
//    /**
//     * 配置打开功能标识
//     */
//    @Value("${switch.overview.isOn}")
//    private int isOn;
//    /**
//     * 概览的内测功能是否打开
//     */
//    @Value("${switch.overview.enabled}")
//    private int enabled;
//
//    /**
//     * 内测商户id
//     */
//    @Value("${switch.overview.closeBetaTestUserIds}")
//    private String closeBetaTestUserIds;
//    /**
//     * 内测商户id分隔符号
//     */
//    @Value("${switch.overview.separator}")
//    private String separator;
//
//    @Value("${switch.loginURI}")
//    private String loginURI;
//
//    /**
//     * 响应头key
//     */
//    private static final String RESPONSE_HEADER_KEY = "isCloseBetaTestUser";
//    /**
//     * 响应头的值
//     */
//    private static final String IS_CLOSE_BETA_TEST_USER = "true";
//
//    private static final String SUCCESS = "0";
//
//    /**
//     * @param request
//     * @param response
//     * @param filterChain
//     * @throws IOException
//     * @throws ServletException
//     */
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
//        // 是否开始内测开关
//        if (enabled == isOn) {
//            // 复制请求流
//            BodyReaderRequestWrapper bodyReaderRequestWrapper = new BodyReaderRequestWrapper((HttpServletRequest) request);
//            // 过滤登陆请求
//            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//            String requestURI = httpServletRequest.getRequestURI();
//            // 非登陆请求直接返回
//            if (!loginURI.equals(requestURI)) {
//                filterChain.doFilter(bodyReaderRequestWrapper, response);
//                return;
//            }
//            log.info("进入被处理的请求,URI为 : {}", ((HttpServletRequest) request).getRequestURI());
//            // 复制响应流
//            ResponseBodyReaderWrapper wrapperResponse = new ResponseBodyReaderWrapper((HttpServletResponse) response);
//
//            filterChain.doFilter(bodyReaderRequestWrapper, wrapperResponse);
//
//            byte[] content = wrapperResponse.getContent();//获取返回值
//            //判断是否有值
//            if (content.length > 0) {
//                String str = new String(content, "UTF-8");
//                try {
//                    JSONObject jsonObject = JSON.parseObject(str);
//                    String code = jsonObject.getString("code");
//                    if (SUCCESS.equals(code)) {
//                        String data = jsonObject.getString("data");
//                        JSONObject jsonObjectData = JSON.parseObject(data);
//                        String merchantId = jsonObjectData.getString("merchantId");
//
//                        String[] ids = closeBetaTestUserIds.split(separator);
//                        List<String> idList = Arrays.asList(ids);
//                        log.info("内测商户为:{},登陆商户为:{}", idList, merchantId);
//                        if (idList.contains(merchantId)) {
//                            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
//                            httpServletResponse.setHeader(RESPONSE_HEADER_KEY, IS_CLOSE_BETA_TEST_USER);
//                        }
//                    }
//                } catch (Exception e) {
//                    log.error(">>>>>>>>>>>>>>>>>>>>> 解析响应体出错!");
//                    e.printStackTrace();
//                } finally {
//                    //把返回值输出到客户端
//                    ServletOutputStream out = null;
//                    try {
//                        out = response.getOutputStream();
//                        out.write(str.getBytes());
//                    } finally {
//                        if (out != null) {
//                            out.flush();
//                        }
//                    }
//                }
//            }
//        } else {
//            filterChain.doFilter(request, response);
//            return;
//        }
//    }
//}