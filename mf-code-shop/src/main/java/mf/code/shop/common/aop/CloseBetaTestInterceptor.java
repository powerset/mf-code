package mf.code.shop.common.aop;/**
 * create by qc on 2019/6/27 0027
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * 内测功能拦截器
 *
 * @author gbf
 * 2019/6/27 0027、16:52
 */
@Slf4j
public class CloseBetaTestInterceptor extends HandlerInterceptorAdapter {

    /**
     * 配置打开功能标识
     */
    @Value("${switch.overview.isOn}")
    private int isOn;
    /**
     * 概览的内测功能是否打开
     */
    @Value("${switch.overview.enabled}")
    private int enabled;
    /**
     * 内测商户id
     */
    @Value("${switch.overview.closeBetaTestUserIds}")
    private String closeBetaTestUserIds;
    /**
     * 内测商户id分隔符号
     */
    @Value("${switch.overview.separator}")
    private String separator;

    /**
     * 响应头key
     */
    private static final String RESPONSE_HEADER_KEY = "isCloseBetaTestUser";
    /**
     * 响应头的值
     */
    private static final String IS_CLOSE_BETA_TEST_USER = "true";

    /**
     * 登陆商户是内测用户,就写入响应头 isCloseBetaTestUser
     *
     * @param request  请求
     * @param response 响应
     * @param handler  handler
     * @param ex       异常
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        // 主业务中在请求中设置了merchantId
        if (enabled == isOn) {
            String[] ids = closeBetaTestUserIds.split(separator);
            List<String> idList = Arrays.asList(ids);
            if (idList.contains("8")) {
                response.setHeader(RESPONSE_HEADER_KEY, IS_CLOSE_BETA_TEST_USER);
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }
}
