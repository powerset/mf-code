package mf.code.shop.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.shop.repo.dao.CategoryAreaMapper;
import mf.code.shop.repo.po.CategoryArea;
import mf.code.shop.repo.repository.CategoryAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * mf.code.shop.repo.repository.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-22 10:54
 */
@Service
public class CategoryAreaRepositoryImpl implements CategoryAreaRepository {

    @Autowired
    private CategoryAreaMapper categoryAreaMapper;

    /**
     * 根据父级地区id和地区级别查询地区列表
     *
     * @param pid   父级地区id
     * @param level 地区级别
     * @return 地区列表
     */
    @Override
    public List<CategoryArea> selectByPidAndLevel(Long pid, Integer level) {

        QueryWrapper<CategoryArea> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CategoryArea::getPid, pid)
                .eq(CategoryArea::getLevel, level);
        return categoryAreaMapper.selectList(queryWrapper);
    }
}
