package mf.code.shop.repo.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.shop.dto.AuditShopParamDTO;
import mf.code.shop.repo.po.MerchantShop;

import java.util.List;

/**
 * mf.code.shop.repo.repository
 * Description:
 *
 * @author gel
 * @date 2019-04-03 13:59
 */
public interface ShopRepostory {

    /**
     * 根据主键查询
     *
     * @param shopId 店铺id
     * @return 店铺信息
     */
    MerchantShop getByShopId(Long shopId);

    /**
     * 按字段更新
     *
     * @param merchantShop 店铺入参
     * @return 更新条数
     */
    Integer updateByPrimaryKeySelective(MerchantShop merchantShop);

    /**
     * 按字段创建
     *
     * @param merchantShop 店铺入参
     * @return 创建条数
     */
    Integer createOrUpdateBySelective(MerchantShop merchantShop);

    /**
     * 商户查询所属店铺
     *
     * @param id 商户id
     * @return 店铺列表
     */
    List<MerchantShop> getByMerchantId(Long id);

    /**
     * 商户查询所属店铺
     *
     * @param id 商户id
     * @return 店铺数量
     */
    Integer countByMerchantIdForPurchaseShop(Long id);

    /**
     * 商户查询所属店铺
     *
     * @param merchantId 商户id
     * @return 店铺数量
     */
    Integer countByMerchantIdForPurchaseShopWithoutTime(Long merchantId);

    /***
     * 根据商户id查询旗下的所有未删除的店铺
     * @param merchantId
     * @return 店铺集合
     */
    List<MerchantShop> selectUnDelShops(Long merchantId);

    /**
     * 商户查询未完成创建的店铺
     *
     * @param id 商户id
     * @return 店铺数量
     */
    List<MerchantShop> selectUnfinishCreateShop(Long id);

    /**
     * 运营平台多条件查询店铺列表
     *
     * @param paramDTO 条件
     * @return 店铺列表
     */
    IPage<MerchantShop> pageListForPlatform(AuditShopParamDTO paramDTO);

    /**
     * 根据店铺id列表查询店铺信息
     * @param shopIdList
     * @return
     */
    List<MerchantShop> getMerchantShopListByShopIdList(List<Long> shopIdList);

    /**
     *
     * @param shopName
     * @return
     */
    List<Long> selectByShopName(String shopName);
}
