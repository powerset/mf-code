package mf.code.shop.repo.repository.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.shop.domain.aggregateroot.ShoppingMall;
import mf.code.shop.repo.dao.MerchantShopMapper;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.repository.ShoppingMallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.repo.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-29 10:08
 */
@Slf4j
@Service
public class ShoppingMallRepositroyImpl implements ShoppingMallRepository {
	@Autowired
	private MerchantShopMapper merchantShopMapper;

	/**
	 * 通过id获取商城
	 * @param id id
	 * @return
	 */
	@Override
	public ShoppingMall findById(Long id) {

		MerchantShop merchantShop  = this.merchantShopMapper.selectById(id);
		if (merchantShop == null) {
			log.error("查无数据, shopId = {}", id);
			return null;
		}

		// 装配ShoppingMall
		ShoppingMall shoppingMall = new ShoppingMall();
		shoppingMall.setId(merchantShop.getId());
		shoppingMall.setShoppingMallInfo(merchantShop);

		return shoppingMall;
	}
}
