package mf.code.shop.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * shop_footprint
 * 用户店铺足迹表-日志表
 */
public class ShopFootprint implements Serializable {
    /**
     * 用户店铺足迹主键id
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 商铺名
     */
    private String shopName;

    /**
     * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    private String picPath;

    /**
     * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     */
    private String csJson;

    /**
     * 删除标识
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * shop_footprint
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户店铺足迹主键id
     * @return id 用户店铺足迹主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户店铺足迹主键id
     * @param id 用户店铺足迹主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 商铺名
     * @return shop_name 商铺名
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 商铺名
     * @param shopName 商铺名
     */
    public void setShopName(String shopName) {
        this.shopName = shopName == null ? null : shopName.trim();
    }

    /**
     * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     * @return pic_path 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    public String getPicPath() {
        return picPath;
    }

    /**
     * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     * @param picPath 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    public void setPicPath(String picPath) {
        this.picPath = picPath == null ? null : picPath.trim();
    }

    /**
     * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     * @return cs_json 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     */
    public String getCsJson() {
        return csJson;
    }

    /**
     * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     * @param csJson 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     */
    public void setCsJson(String csJson) {
        this.csJson = csJson == null ? null : csJson.trim();
    }

    /**
     * 删除标识
     * @return del 删除标识
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识
     * @param del 删除标识
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", shopName=").append(shopName);
        sb.append(", picPath=").append(picPath);
        sb.append(", csJson=").append(csJson);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}