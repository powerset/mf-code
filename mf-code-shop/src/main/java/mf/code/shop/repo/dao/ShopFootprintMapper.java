package mf.code.shop.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.shop.repo.po.ShopFootprint;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopFootprintMapper extends BaseMapper<ShopFootprint> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ShopFootprint record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ShopFootprint record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ShopFootprint selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ShopFootprint record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ShopFootprint record);
}
