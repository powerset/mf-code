package mf.code.shop.repo.po;/**
 * create by qc on 2019/8/13 0013
 */

import lombok.Data;

/**
 * @author gbf
 * 2019/8/13 0013、11:15
 */
@Data
public class ReportUrl {
    private Long merchantId;
    private Long shopId;
    private Long productId;
    private String url;
}
