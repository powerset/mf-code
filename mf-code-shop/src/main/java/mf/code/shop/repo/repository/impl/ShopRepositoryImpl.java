package mf.code.shop.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.utils.DateUtil;
import mf.code.shop.dto.AuditShopParamDTO;
import mf.code.shop.dto.ShopDetail;
import mf.code.shop.dto.ShopDetailForPlatformDTO;
import mf.code.shop.repo.dao.MerchantShopMapper;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.repository.ShopRepostory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.shop.repo.repository.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-03 13:59
 */
@Slf4j
@Service
public class ShopRepositoryImpl implements ShopRepostory {

    @Value("${seller.purchaseTime}")
    private String merchantPurchaseTime;

    @Autowired
    private MerchantShopMapper merchantShopMapper;


    /**
     * 根据主键查询
     *
     * @param shopId
     * @return
     */
    @Override
    public MerchantShop getByShopId(Long shopId) {
        if (shopId == null) {
            return null;
        }
        return merchantShopMapper.selectByPrimaryKey(shopId);
    }

    /**
     * 按字段更新
     *
     * @param merchantShop
     * @return
     */
    @Override
    public Integer updateByPrimaryKeySelective(MerchantShop merchantShop) {
        if (merchantShop == null || merchantShop.getId() == null) {
            return 0;
        }
        merchantShop.setUtime(new Date());
        return merchantShopMapper.updateByPrimaryKeySelective(merchantShop);
    }

    /**
     * 按字段创建
     *
     * @param merchantShop
     * @return
     */
    @Override
    public Integer createOrUpdateBySelective(MerchantShop merchantShop) {
        if (merchantShop == null) {
            return 0;
        }
        Date now = new Date();
        merchantShop.setUtime(now);
        if (merchantShop.getId() != null && merchantShop.getId() > 0) {
            return merchantShopMapper.updateByPrimaryKeySelective(merchantShop);
        }
        merchantShop.setCtime(now);
        return merchantShopMapper.insertSelective(merchantShop);
    }

    /**
     * 商户查询所属店铺
     *
     * @param merchantId 商户id
     * @return 店铺列表
     */
    @Override
    public List<MerchantShop> getByMerchantId(Long merchantId) {

        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(MerchantShop::getMerchantId, merchantId).eq(MerchantShop::getDel, DelEnum.NO.getCode());
        return merchantShopMapper.selectList(queryWrapper);
    }

    /**
     * 商户查询订购店铺数量 todo bug:如何抖音小铺做区分
     *
     * @param merchantId 商户id
     * @return 店铺数量
     */
    @Override
    public Integer countByMerchantIdForPurchaseShop(Long merchantId) {
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantShop::getMerchantId, merchantId)
                .ge(MerchantShop::getCtime, merchantPurchaseTime);
        return merchantShopMapper.selectCount(queryWrapper);
    }

    /**
     * 商户查询订购店铺数量
     *
     * @param merchantId 商户id
     * @return 店铺数量
     */
    @Override
    public Integer countByMerchantIdForPurchaseShopWithoutTime(Long merchantId) {
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantShop::getMerchantId, merchantId);
        return merchantShopMapper.selectCount(queryWrapper);
    }

    /***
     * 根据商户id查询旗下的所有未删除的店铺
     * @param merchantId
     * @return 店铺集合
     */
    @Override
    public List<MerchantShop> selectUnDelShops(Long merchantId) {
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantShop::getMerchantId, merchantId)
                .eq(MerchantShop::getDel, DelEnum.NO.getCode());
        return merchantShopMapper.selectList(queryWrapper);
    }

    /**
     * 商户查询未完成创建的店铺
     *
     * @param merchantId 商户id
     * @return 店铺数量
     */
    @Override
    public List<MerchantShop> selectUnfinishCreateShop(Long merchantId) {
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantShop::getMerchantId, merchantId)
                .eq(MerchantShop::getPurchaseVersion, 0)
                .isNull(MerchantShop::getPurchaseJson)
                .ge(MerchantShop::getCtime, merchantPurchaseTime);
        return merchantShopMapper.selectList(queryWrapper);
    }

    /**
     * 多条件查询
     *
     * @param paramDTO 条件
     * @return 店铺列表
     */
    @Override
    public IPage<MerchantShop> pageListForPlatform(AuditShopParamDTO paramDTO) {
        Page<MerchantShop> ipage = new Page<>(paramDTO.getPage(), paramDTO.getSize());
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(paramDTO.getPhone())) {
            queryWrapper.lambda().eq(MerchantShop::getPhone, paramDTO.getPhone());
        }
        if (StringUtils.isNotBlank(paramDTO.getShopName())) {
            queryWrapper.lambda().like(MerchantShop::getShopName, paramDTO.getShopName());
        }
        if (StringUtils.isNotBlank(paramDTO.getStartTime()) && StringUtils.isNotBlank(paramDTO.getEndTime())) {
            queryWrapper.lambda().between(MerchantShop::getCtime, DateUtil.stringtoDate(paramDTO.getStartTime(), DateUtil.FORMAT_TWO)
                    , DateUtil.stringtoDate(paramDTO.getEndTime(), DateUtil.FORMAT_TWO));
        }
        return merchantShopMapper.selectPage(ipage, queryWrapper);
    }

    /**
     * 多条件查询
     *
     * @param shopIdList 条件
     * @return 店铺列表
     */
    @Override
    public List<MerchantShop> getMerchantShopListByShopIdList(List<Long> shopIdList) {
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        if (CollectionUtils.isEmpty(shopIdList)) {
            return new ArrayList<>();
        }
        queryWrapper.select("id", "merchant_id","pic_path", "nick", "shop_name", "phone", "username", "address_detail", "address_text");
        queryWrapper.lambda().in(MerchantShop::getId, shopIdList);
        return merchantShopMapper.selectList(queryWrapper);
    }

    /**
     * @param shopName
     * @return
     */
    @Override
    public List<Long> selectByShopName(String shopName) {
        if (StringUtils.isBlank(shopName)) {
            return new ArrayList<>();
        }
        QueryWrapper<MerchantShop> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().select(MerchantShop::getId).like(MerchantShop::getShopName, shopName);
        Integer count = merchantShopMapper.selectCount(queryWrapper);
        if (count <= 0) {
            return new ArrayList<>();
        }
        if (count > 100) {
            queryWrapper.lambda().last("limit 100");
        }
        List<MerchantShop> merchantShops = merchantShopMapper.selectList(queryWrapper);
        List<Long> shopIdList = new ArrayList<>();
        for (MerchantShop merchantShop : merchantShops) {
            shopIdList.add(merchantShop.getId());
        }
        return shopIdList;
    }
}
