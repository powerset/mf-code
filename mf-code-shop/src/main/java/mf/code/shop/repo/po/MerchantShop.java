package mf.code.shop.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * merchant_shop
 * 商家绑定的店铺表
 */
public class MerchantShop implements Serializable {
    /**
     * 商家店铺id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 卖家昵称
     */
    private String nick;

    /**
     * 商铺名
     */
    private String shopName;

    /**
     * 类目id，品类表的sid
     */
    private Long categoryId;

    /**
     * 开店审核
     */
    private Integer openstatus;

    /**
     * 开店审核审核原因
     */
    private String openreason;

    /**
     * 分销资格审核
     */
    private Integer diststatus;

    /**
     * 分销资格审核原因
     */
    private String distreason;

    /**
     * 联系方式-微信号
     */
    private String wechat;

    /**
     * 联系方式-QQ
     */
    private String qq;

    /**
     * 联系方式-店铺手机号
     */
    private String phone;

    /**
     * 店长姓名
     */
    private String username;

    /**
     * 喜销宝服务接口响应数据
     */
    private String xxbtopRes;

    /**
     * 淘宝店标地址。返回相对路径，可以用"http://logo.taobao.com/shop-logo"来拼接成绝对路径
     */
    private String xxbtopPicPath;

    /**
     * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    private String picPath;

    /**
     * 地址详情
     */
    private String addressDetail;

    /**
     * 省市区
     */
    private String addressText;

    /**
     * 省市区
     */
    private String addressCode;

    /**
     * 店铺参数小程序码图片相对路径，可以用"TODO"来拼接成绝对路径
     */
    private String wxacodePath;

    /**
     * 包裹参数小程序码绝对路径
     */
    private String packcodePath;

    /**
     * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     */
    private String csJson;

    /**
     * 店家群二维码
     */
    private String groupCode;

    /**
     * 店铺标题
     */
    private String title;

    /**
     * 删除标识
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 喜销宝截止时间
     */
    private Date xxbDeadline;

    /**
     * 店铺banner图json格式：[{"bannerUrl":"http://sss","jumpUrl":"http://bbb"},{"bannerUrl":"http://sss","jumpUrl":"http://bbb"}]
     */
    private String bannerJson;

    /**
     * 是否展示客服号
     */
    private Integer showFlag;

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
     */
    private Integer purchaseVersion;

    /**
     * 内有订购周期：{"purchaseTime":2018-11-21 18:00,"expireTime":2018-11-21 18:00}
     */
    private String purchaseJson;

    /**
     * merchant_shop
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商家店铺id
     * @return id 商家店铺id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商家店铺id
     * @param id 商家店铺id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 卖家昵称
     * @return nick 卖家昵称
     */
    public String getNick() {
        return nick;
    }

    /**
     * 卖家昵称
     * @param nick 卖家昵称
     */
    public void setNick(String nick) {
        this.nick = nick == null ? null : nick.trim();
    }

    /**
     * 商铺名
     * @return shop_name 商铺名
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 商铺名
     * @param shopName 商铺名
     */
    public void setShopName(String shopName) {
        this.shopName = shopName == null ? null : shopName.trim();
    }

    /**
     * 类目id，品类表的sid
     * @return category_id 类目id，品类表的sid
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * 类目id，品类表的sid
     * @param categoryId 类目id，品类表的sid
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 开店审核
     * @return openStatus 开店审核
     */
    public Integer getOpenstatus() {
        return openstatus;
    }

    /**
     * 开店审核
     * @param openstatus 开店审核
     */
    public void setOpenstatus(Integer openstatus) {
        this.openstatus = openstatus;
    }

    /**
     * 开店审核审核原因
     * @return openReason 开店审核审核原因
     */
    public String getOpenreason() {
        return openreason;
    }

    /**
     * 开店审核审核原因
     * @param openreason 开店审核审核原因
     */
    public void setOpenreason(String openreason) {
        this.openreason = openreason == null ? null : openreason.trim();
    }

    /**
     * 分销资格审核
     * @return distStatus 分销资格审核
     */
    public Integer getDiststatus() {
        return diststatus;
    }

    /**
     * 分销资格审核
     * @param diststatus 分销资格审核
     */
    public void setDiststatus(Integer diststatus) {
        this.diststatus = diststatus;
    }

    /**
     * 分销资格审核原因
     * @return distReason 分销资格审核原因
     */
    public String getDistreason() {
        return distreason;
    }

    /**
     * 分销资格审核原因
     * @param distreason 分销资格审核原因
     */
    public void setDistreason(String distreason) {
        this.distreason = distreason == null ? null : distreason.trim();
    }

    /**
     * 联系方式-微信号
     * @return wechat 联系方式-微信号
     */
    public String getWechat() {
        return wechat;
    }

    /**
     * 联系方式-微信号
     * @param wechat 联系方式-微信号
     */
    public void setWechat(String wechat) {
        this.wechat = wechat == null ? null : wechat.trim();
    }

    /**
     * 联系方式-QQ
     * @return qq 联系方式-QQ
     */
    public String getQq() {
        return qq;
    }

    /**
     * 联系方式-QQ
     * @param qq 联系方式-QQ
     */
    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    /**
     * 联系方式-店铺手机号
     * @return phone 联系方式-店铺手机号
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 联系方式-店铺手机号
     * @param phone 联系方式-店铺手机号
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 店长姓名
     * @return username 店长姓名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 店长姓名
     * @param username 店长姓名
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * 喜销宝服务接口响应数据
     * @return xxbtop_res 喜销宝服务接口响应数据
     */
    public String getXxbtopRes() {
        return xxbtopRes;
    }

    /**
     * 喜销宝服务接口响应数据
     * @param xxbtopRes 喜销宝服务接口响应数据
     */
    public void setXxbtopRes(String xxbtopRes) {
        this.xxbtopRes = xxbtopRes == null ? null : xxbtopRes.trim();
    }

    /**
     * 淘宝店标地址。返回相对路径，可以用"http://logo.taobao.com/shop-logo"来拼接成绝对路径
     * @return xxbtop_pic_path 淘宝店标地址。返回相对路径，可以用"http://logo.taobao.com/shop-logo"来拼接成绝对路径
     */
    public String getXxbtopPicPath() {
        return xxbtopPicPath;
    }

    /**
     * 淘宝店标地址。返回相对路径，可以用"http://logo.taobao.com/shop-logo"来拼接成绝对路径
     * @param xxbtopPicPath 淘宝店标地址。返回相对路径，可以用"http://logo.taobao.com/shop-logo"来拼接成绝对路径
     */
    public void setXxbtopPicPath(String xxbtopPicPath) {
        this.xxbtopPicPath = xxbtopPicPath == null ? null : xxbtopPicPath.trim();
    }

    /**
     * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     * @return pic_path 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    public String getPicPath() {
        return picPath;
    }

    /**
     * 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     * @param picPath 小时光OSS店标地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    public void setPicPath(String picPath) {
        this.picPath = picPath == null ? null : picPath.trim();
    }

    /**
     * 地址详情
     * @return address_detail 地址详情
     */
    public String getAddressDetail() {
        return addressDetail;
    }

    /**
     * 地址详情
     * @param addressDetail 地址详情
     */
    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail == null ? null : addressDetail.trim();
    }

    /**
     * 省市区
     * @return address_text 省市区
     */
    public String getAddressText() {
        return addressText;
    }

    /**
     * 省市区
     * @param addressText 省市区
     */
    public void setAddressText(String addressText) {
        this.addressText = addressText == null ? null : addressText.trim();
    }

    /**
     * 省市区
     * @return address_code 省市区
     */
    public String getAddressCode() {
        return addressCode;
    }

    /**
     * 省市区
     * @param addressCode 省市区
     */
    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode == null ? null : addressCode.trim();
    }

    /**
     * 店铺参数小程序码图片相对路径，可以用"TODO"来拼接成绝对路径
     * @return wxacode_path 店铺参数小程序码图片相对路径，可以用"TODO"来拼接成绝对路径
     */
    public String getWxacodePath() {
        return wxacodePath;
    }

    /**
     * 店铺参数小程序码图片相对路径，可以用"TODO"来拼接成绝对路径
     * @param wxacodePath 店铺参数小程序码图片相对路径，可以用"TODO"来拼接成绝对路径
     */
    public void setWxacodePath(String wxacodePath) {
        this.wxacodePath = wxacodePath == null ? null : wxacodePath.trim();
    }

    /**
     * 包裹参数小程序码绝对路径
     * @return packcode_path 包裹参数小程序码绝对路径
     */
    public String getPackcodePath() {
        return packcodePath;
    }

    /**
     * 包裹参数小程序码绝对路径
     * @param packcodePath 包裹参数小程序码绝对路径
     */
    public void setPackcodePath(String packcodePath) {
        this.packcodePath = packcodePath == null ? null : packcodePath.trim();
    }

    /**
     * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     * @return cs_json 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     */
    public String getCsJson() {
        return csJson;
    }

    /**
     * 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     * @param csJson 客服联系方式json数据，格式：[{"pic":"http://sss","wechat":"gel-ink"},{"pic":"http://aaa","wechat":"gel_ink"}]
     */
    public void setCsJson(String csJson) {
        this.csJson = csJson == null ? null : csJson.trim();
    }

    /**
     * 店家群二维码
     * @return group_code 店家群二维码
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * 店家群二维码
     * @param groupCode 店家群二维码
     */
    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode == null ? null : groupCode.trim();
    }

    /**
     * 店铺标题
     * @return title 店铺标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 店铺标题
     * @param title 店铺标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 删除标识
     * @return del 删除标识
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识
     * @param del 删除标识
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 喜销宝截止时间
     * @return xxb_deadline 喜销宝截止时间
     */
    public Date getXxbDeadline() {
        return xxbDeadline;
    }

    /**
     * 喜销宝截止时间
     * @param xxbDeadline 喜销宝截止时间
     */
    public void setXxbDeadline(Date xxbDeadline) {
        this.xxbDeadline = xxbDeadline;
    }

    /**
     * 店铺banner图json格式：[{"bannerUrl":"http://sss","jumpUrl":"http://bbb"},{"bannerUrl":"http://sss","jumpUrl":"http://bbb"}]
     * @return banner_json 店铺banner图json格式：[{"bannerUrl":"http://sss","jumpUrl":"http://bbb"},{"bannerUrl":"http://sss","jumpUrl":"http://bbb"}]
     */
    public String getBannerJson() {
        return bannerJson;
    }

    /**
     * 店铺banner图json格式：[{"bannerUrl":"http://sss","jumpUrl":"http://bbb"},{"bannerUrl":"http://sss","jumpUrl":"http://bbb"}]
     * @param bannerJson 店铺banner图json格式：[{"bannerUrl":"http://sss","jumpUrl":"http://bbb"},{"bannerUrl":"http://sss","jumpUrl":"http://bbb"}]
     */
    public void setBannerJson(String bannerJson) {
        this.bannerJson = bannerJson == null ? null : bannerJson.trim();
    }

    /**
     * 是否展示客服号
     * @return show_flag 是否展示客服号
     */
    public Integer getShowFlag() {
        return showFlag;
    }

    /**
     * 是否展示客服号
     * @param showFlag 是否展示客服号
     */
    public void setShowFlag(Integer showFlag) {
        this.showFlag = showFlag;
    }

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
     * @return purchase_version 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
     */
    public Integer getPurchaseVersion() {
        return purchaseVersion;
    }

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
     * @param purchaseVersion 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
     */
    public void setPurchaseVersion(Integer purchaseVersion) {
        this.purchaseVersion = purchaseVersion;
    }

    /**
     * 内有订购周期：{"purchaseTime":2018-11-21 18:00,"expireTime":2018-11-21 18:00}
     * @return purchase_json 内有订购周期：{"purchaseTime":2018-11-21 18:00,"expireTime":2018-11-21 18:00}
     */
    public String getPurchaseJson() {
        return purchaseJson;
    }

    /**
     * 内有订购周期：{"purchaseTime":2018-11-21 18:00,"expireTime":2018-11-21 18:00}
     * @param purchaseJson 内有订购周期：{"purchaseTime":2018-11-21 18:00,"expireTime":2018-11-21 18:00}
     */
    public void setPurchaseJson(String purchaseJson) {
        this.purchaseJson = purchaseJson == null ? null : purchaseJson.trim();
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", nick=").append(nick);
        sb.append(", shopName=").append(shopName);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", openstatus=").append(openstatus);
        sb.append(", openreason=").append(openreason);
        sb.append(", diststatus=").append(diststatus);
        sb.append(", distreason=").append(distreason);
        sb.append(", wechat=").append(wechat);
        sb.append(", qq=").append(qq);
        sb.append(", phone=").append(phone);
        sb.append(", username=").append(username);
        sb.append(", xxbtopRes=").append(xxbtopRes);
        sb.append(", xxbtopPicPath=").append(xxbtopPicPath);
        sb.append(", picPath=").append(picPath);
        sb.append(", addressDetail=").append(addressDetail);
        sb.append(", addressText=").append(addressText);
        sb.append(", addressCode=").append(addressCode);
        sb.append(", wxacodePath=").append(wxacodePath);
        sb.append(", packcodePath=").append(packcodePath);
        sb.append(", csJson=").append(csJson);
        sb.append(", groupCode=").append(groupCode);
        sb.append(", title=").append(title);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", xxbDeadline=").append(xxbDeadline);
        sb.append(", bannerJson=").append(bannerJson);
        sb.append(", showFlag=").append(showFlag);
        sb.append(", purchaseVersion=").append(purchaseVersion);
        sb.append(", purchaseJson=").append(purchaseJson);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}