package mf.code.shop.service.impl;

import com.alibaba.fastjson.JSONObject;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.Assert;
import mf.code.shop.constants.OpenAuditStatusEnum;
import mf.code.shop.dto.*;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.repository.ShopRepostory;
import mf.code.shop.service.ShopUpdateService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author gel
 */
@Service
public class ShopUpdateServiceImpl implements ShopUpdateService {
    @Autowired
    private ShopRepostory shopRepostory;

    /**
     * 更新店铺信息
     *
     * @param shopUpdateReqDTO 更新内容
     * @return 更新条数
     */
    @Override
    public Integer updateShop(ShopUpdateReqDTO shopUpdateReqDTO) {
        Assert.isInteger(shopUpdateReqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        MerchantShop merchantShop = shopRepostory.getByShopId(Long.valueOf(shopUpdateReqDTO.getShopId()));
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(Long.valueOf(shopUpdateReqDTO.getMerchantId())),
                ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        MerchantShop merchantShopUpdate = new MerchantShop();
        merchantShopUpdate.setId(merchantShop.getId());
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getAddressCode())) {
            merchantShopUpdate.setAddressCode(shopUpdateReqDTO.getAddressCode());
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getAddressDetail())) {
            merchantShopUpdate.setAddressDetail(shopUpdateReqDTO.getAddressDetail());
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getAddressText())) {
            merchantShopUpdate.setAddressText(shopUpdateReqDTO.getAddressText());
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getCustomShopName())) {
            merchantShopUpdate.setShopName(shopUpdateReqDTO.getCustomShopName());
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getCustomPicPath())) {
            merchantShopUpdate.setPicPath(shopUpdateReqDTO.getCustomPicPath());
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getCategoryId())) {
            merchantShopUpdate.setCategoryId(Long.valueOf(shopUpdateReqDTO.getCategoryId()));
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getPhone())) {
            merchantShopUpdate.setPhone(shopUpdateReqDTO.getPhone());
        }
        if (StringUtils.isNotBlank(shopUpdateReqDTO.getUsername())) {
            merchantShopUpdate.setUsername(shopUpdateReqDTO.getUsername());
        }
        return shopRepostory.updateByPrimaryKeySelective(merchantShopUpdate);
    }

    /**
     * 店铺装修
     *
     * @param customizeDTO 装修内容
     * @return 更新条数
     */
    @Override
    public Integer updateCustomize(CustomizeDTO customizeDTO) {
        Assert.isInteger(customizeDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        MerchantShop merchantShop = shopRepostory.getByShopId(Long.valueOf(customizeDTO.getShopId()));
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(Long.valueOf(customizeDTO.getMerchantId())),
                ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        if (StringUtils.isNotBlank(customizeDTO.getBanner())) {
            List<BannerObj> bannerObjList = new ArrayList<>();
            BannerObj bannerObj = new BannerObj();
            bannerObj.setBannerUrl(customizeDTO.getBanner());
            bannerObjList.add(bannerObj);
            merchantShop.setBannerJson(JSONObject.toJSONString(bannerObjList));
        }
        if (StringUtils.isNotBlank(customizeDTO.getCsPic()) || StringUtils.isNotBlank(customizeDTO.getCsWechat())) {
            List<CsObj> csObjList = new ArrayList<>();
            CsObj csObj = new CsObj();
            csObj.setPic(customizeDTO.getCsPic());
            csObj.setWechat(customizeDTO.getCsWechat());
            csObjList.add(csObj);
            merchantShop.setCsJson(JSONObject.toJSONString(csObjList));
        }
        if (StringUtils.isNotBlank(customizeDTO.getGroupCode())) {
            merchantShop.setGroupCode(customizeDTO.getGroupCode());
        }
        merchantShop.setShopName(customizeDTO.getShopName());
        merchantShop.setPicPath(customizeDTO.getPicPath());
        merchantShop.setTitle(customizeDTO.getTitle());
        merchantShop.setUtime(new Date());
        return shopRepostory.updateByPrimaryKeySelective(merchantShop);
    }

    /**
     * 开店审核
     *
     * @param auditReqDTO 审核内容
     * @return 更新条数
     */
    @Override
    public Integer openAudit(AuditReqDTO auditReqDTO) {
        Assert.isInteger(auditReqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        MerchantShop merchantShop = shopRepostory.getByShopId(Long.valueOf(auditReqDTO.getShopId()));
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(Long.valueOf(auditReqDTO.getMerchantId())),
                ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        Assert.isTrue(merchantShop.getOpenstatus() == 0, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "该店铺已审核");
        int openStatus = Integer.valueOf(auditReqDTO.getAuditStatus());
        if (openStatus == OpenAuditStatusEnum.ERROR.getCode()) {
            Assert.isTrue(StringUtils.isNotBlank(auditReqDTO.getAuditReason()), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请写明原因");
        }
        merchantShop.setOpenstatus(openStatus);
        merchantShop.setOpenreason(auditReqDTO.getAuditReason());
        merchantShop.setUtime(new Date());
        return shopRepostory.updateByPrimaryKeySelective(merchantShop);
    }

    /**
     * 分销资格审核
     *
     * @param auditReqDTO 审核内容
     * @return 更新条数
     */
    @Override
    public Integer distAudit(AuditReqDTO auditReqDTO) {
        Assert.isInteger(auditReqDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        MerchantShop merchantShop = shopRepostory.getByShopId(Long.valueOf(auditReqDTO.getShopId()));
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(Long.valueOf(auditReqDTO.getMerchantId())),
                ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        Assert.isTrue(merchantShop.getDiststatus() == 0, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "该店铺已审核");
        int distStatus = Integer.valueOf(auditReqDTO.getAuditStatus());
        if (distStatus == OpenAuditStatusEnum.ERROR.getCode()) {
            Assert.isTrue(StringUtils.isNotBlank(auditReqDTO.getAuditReason()), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请写明原因");
        }
        merchantShop.setDiststatus(distStatus);
        merchantShop.setDistreason(auditReqDTO.getAuditReason());
        merchantShop.setUtime(new Date());
        return shopRepostory.updateByPrimaryKeySelective(merchantShop);
    }

}
