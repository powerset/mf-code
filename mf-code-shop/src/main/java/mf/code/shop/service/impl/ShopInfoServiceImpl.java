package mf.code.shop.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.common.RedisKeyConstant;
import mf.code.common.WeixinMpConstants;
import mf.code.common.repo.repository.CommonDictRepository;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.merchant.repo.repository.MerchantOrderRepository;
import mf.code.shop.common.caller.wxmp.WeixinMpService;
import mf.code.shop.common.caller.wxpay.WxpayProperty;
import mf.code.shop.constants.AreaLevelEnum;
import mf.code.shop.constants.ShopStatusEnum;
import mf.code.shop.dto.*;
import mf.code.shop.repo.po.CategoryArea;
import mf.code.shop.repo.po.CategoryShop;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.po.MerchantThirdShop;
import mf.code.shop.repo.repository.CategoryAreaRepository;
import mf.code.shop.repo.repository.CategoryShopRepository;
import mf.code.shop.repo.repository.ShopRepostory;
import mf.code.shop.repo.repository.ThirdShopRepostory;
import mf.code.shop.service.ShopInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author gel
 */
@Service
public class ShopInfoServiceImpl implements ShopInfoService {
    @Autowired
    private ShopRepostory shopRepostory;
    @Autowired
    private ThirdShopRepostory thirdShopRepostory;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private CategoryShopRepository categoryShopRepository;
    @Autowired
    private CategoryAreaRepository categoryAreaRepository;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CommonDictRepository commonDictRepository;

    /**
     * 获取店铺列表
     *
     * @param merchantId 商户id
     * @return 店铺列表
     */
    @Override
    public ShopListResultDTO getList(Long merchantId) {
        List<MerchantShop> merchantShops = shopRepostory.getByMerchantId(merchantId);
        ShopListResultDTO shopListResultDTO = new ShopListResultDTO();
        if (CollectionUtils.isEmpty(merchantShops)) {
            return shopListResultDTO;
        }
        List<ShopDetail> shopDetails = new ArrayList<>();
        for (MerchantShop merchantShop : merchantShops) {
            ShopDetail shopDetail = new ShopDetail();
            shopDetail.setShopId(merchantShop.getId());
            shopDetail.setCategoryId(merchantShop.getCategoryId());
            shopDetail.setCustomPicPath(merchantShop.getPicPath());
            shopDetail.setCustomShopName(merchantShop.getShopName());
            shopDetail.setPurchaseVersion(merchantShop.getPurchaseVersion() == null ? "" : merchantShop.getPurchaseVersion().toString());
            shopDetail.setStatus(ShopStatusEnum.INIT.getCode());
            if (StringUtils.isNotBlank(merchantShop.getPurchaseJson())) {
                getPurchaseStatusAndExpireTime(merchantShop, shopDetail);
            }
            if (StringUtils.isBlank(merchantShop.getPhone()) || StringUtils.isBlank(merchantShop.getAddressText())) {
                shopDetail.setStatus(ShopStatusEnum.INIT.getCode());
            }
            shopDetail.setFirstDialog(0);
            shopDetails.add(shopDetail);
        }
        // 判断是否包含带创建店铺
        Integer orderCount = merchantOrderRepository.countMerchantOrderForPurchaseShop(merchantId);
        Integer shopCount = shopRepostory.countByMerchantIdForPurchaseShop(merchantId);
        if (orderCount > shopCount) {
            ShopDetail shopDetail = new ShopDetail();
            shopDetail.setStatus(ShopStatusEnum.INIT.getCode());
            shopDetail.setPurchaseVersion("0");
            shopDetails.add(shopDetail);
        }
        shopListResultDTO.setShopList(shopDetails);
        return shopListResultDTO;
    }

    /**
     * 获取购买店铺的过期时间和状态
     *
     * @param merchantShop 店铺信息
     * @param shopDetail   店铺详情
     */
    private void getPurchaseStatusAndExpireTime(MerchantShop merchantShop, ShopDetail shopDetail) {
        JSONObject merchantShopJson = JSONObject.parseObject(merchantShop.getPurchaseJson());
        if (merchantShopJson == null) {
            return;
        }
        String expireTime = merchantShopJson.getString("expireTime");
        Date date = DateUtil.stringtoDate(expireTime, DateUtil.FORMAT_ONE);
        shopDetail.setExpireTime(date.getTime());
        shopDetail.setStatus(ShopStatusEnum.USED.getCode());
        // 已过期
        if (date.before(new Date())) {
            shopDetail.setStatus(ShopStatusEnum.EXPIRED.getCode());
        } else if (date.before(DateUtil.addDay(new Date(), 7))) {
            // 还有七天即将过期
            shopDetail.setStatus(ShopStatusEnum.EXPIRE_SOON.getCode());
        }
    }

    /**
     * 获取店铺小程序码
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return 小程序码地址
     */
    @Override
    public String getWxacode(Long merchantId, Long shopId) {
        MerchantShop merchantShop = shopRepostory.getByShopId(shopId);
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(merchantId), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        // 如果为空则生成微信小程序码
        if (StringUtils.isBlank(merchantShop.getWxacodePath())) {
            return createWXACode(merchantId, shopId, WeixinMpConstants.SCENE_SHOP);
        }
        return merchantShop.getWxacodePath();
    }

    /**
     * 根据店铺id获取商户id
     *
     * @param shopId 店铺id
     * @return 小程序码地址
     */
    @Override
    public Long getMerchantIdByShopId(Long shopId) {
        MerchantShop shop = shopRepostory.getByShopId(shopId);
        if (shop != null) {
            return shop.getMerchantId();
        }
        return null;
    }

    /**
     * 创建根据scene小程序码
     *
     * @param merchantId
     * @param shopId
     * @param scene
     * @return
     */
    public String createWXACode(Long merchantId, Long shopId, Integer scene) {
        // 查询是否存在
        MerchantShop merchantShop = shopRepostory.getByShopId(shopId);
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        String path = "";
        if (WeixinMpConstants.SCENE_SHOP.equals(scene)) {
            if (merchantShop.getWxacodePath() != null) {
                return merchantShop.getWxacodePath();
            }
            path = WeixinMpConstants.WXCODESHOPPATH;
        }
        if (WeixinMpConstants.SCENE_PACKAGE.equals(scene)) {
            if (merchantShop.getPackcodePath() != null) {
                return merchantShop.getPackcodePath();
            }
            path = WeixinMpConstants.WXCODESHOPPACKAGEPATH;
        }
        // 拼接字符串
        String picUrl = weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(),
                String.valueOf(merchantId) + "," + shopId + "," + scene, path, null, false);
        // 保存小程序码
        MerchantShop merchantShop1 = new MerchantShop();
        if (WeixinMpConstants.SCENE_SHOP.equals(scene)) {
            merchantShop1.setWxacodePath(picUrl);
        }
        if (WeixinMpConstants.SCENE_PACKAGE.equals(scene)) {
            merchantShop1.setPackcodePath(picUrl);
        }
        merchantShop1.setId(merchantShop.getId());
        merchantShop1.setUtime(new Date());
        shopRepostory.updateByPrimaryKeySelective(merchantShop1);
        return picUrl;
    }

    /**
     * 获取小程序版本系信息
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return 小程序版本信息
     */
    @Override
    public VersionResultDTO getVersioninfo(Long merchantId, Long shopId) {
        MerchantShop merchantShop = shopRepostory.getByShopId(shopId);
        VersionResultDTO resultDTO = new VersionResultDTO();
        resultDTO.setShopLogo(merchantShop.getPicPath());
        // TODO 待添加修改存储的地方
        String jkmfName = commonDictRepository.selectValueByTypeKey("platformCs", "jkmfName");
        resultDTO.setWidgetName(StringUtils.isBlank(jkmfName) ? "集客魔方" : jkmfName);
        String jkmfVersionNum = commonDictRepository.selectValueByTypeKey("platformCs", "jkmfVersionNum");
        resultDTO.setVersionNumber(StringUtils.isBlank(jkmfVersionNum) ? "1.3.3" : jkmfVersionNum);
        String jkmfVersionText = commonDictRepository.selectValueByTypeKey("platformCs", "jkmfVersionText");
        resultDTO.setVersionText(StringUtils.isBlank(jkmfVersionText) ? "1.3.3" : jkmfVersionText);
        resultDTO.setPurchaseVersion(merchantShop.getPurchaseVersion().toString());
        if (merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode())
                || merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.VERSION_PRIVATE.getCode())
                || merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.CLEAN.getCode())
                || merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.WEIGHTING.getCode())) {
            JSONObject jsonObject = JSON.parseObject(merchantShop.getPurchaseJson());
            if (jsonObject != null) {
                resultDTO.setPurchaseTime(jsonObject.getString("purchaseTime"));
                resultDTO.setExpireTime(jsonObject.getString("expireTime"));
            }
            String wxacode = getWxacode(merchantId, shopId);
            resultDTO.setWxacodePath(wxacode);
        }
        return resultDTO;
    }

    /**
     * 店铺详情
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return 店铺详情
     */
    @Override
    public ShopDetail detail(Long merchantId, Long shopId) {
        MerchantShop merchantShop = shopRepostory.getByShopId(shopId);
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(merchantId), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        ShopDetail shopDetail = new ShopDetail();
        // 这里直接采用spring提供的bean复制的方式
        BeanUtils.copyProperties(merchantShop, shopDetail);
        shopDetail.setShopId(shopId);
        shopDetail.setPurchaseVersion(merchantShop.getPurchaseVersion() == null ? "" : merchantShop.getPurchaseVersion().toString());
        if (StringUtils.isNotBlank(merchantShop.getPurchaseJson())) {
            getPurchaseStatusAndExpireTime(merchantShop, shopDetail);
        }
        shopDetail.setCustomPicPath(merchantShop.getPicPath());
        shopDetail.setCustomShopName(merchantShop.getShopName());
        CategoryShop categoryShop = categoryShopRepository.selectCategoryShopByCid(merchantShop.getCategoryId().toString());
        shopDetail.setCategoryId(merchantShop.getCategoryId());
        shopDetail.setCategoryName(categoryShop.getName());
        shopDetail.setCtime(merchantShop.getCtime().getTime());
        shopDetail.setUsername(merchantShop.getUsername());
        shopDetail.setPhone(merchantShop.getPhone());
        shopDetail.setAddressCode(merchantShop.getAddressCode());
        shopDetail.setAddressText(merchantShop.getAddressText());
        shopDetail.setAddressDetail(merchantShop.getAddressDetail());
        shopDetail.setFirstDialog(1);
        String firstLoginDialogRedisKey = RedisKeyConstant.SHOP_FIRST_LOGIN_DIALOG + shopId;
        String firstLogin = this.stringRedisTemplate.opsForValue().get(firstLoginDialogRedisKey);
        if (StringUtils.isNotBlank(firstLogin)) {
            shopDetail.setFirstDialog(0);
            // 设置新手引导30天过期，三十天之后继续弹
            this.stringRedisTemplate.opsForValue().set(firstLoginDialogRedisKey, DateUtil.getNow(), 30L, TimeUnit.DAYS);
        }
        JSONObject jsonObject = JSONObject.parseObject(merchantShop.getPurchaseJson());
        if (jsonObject != null) {
            shopDetail.setExpireTime(DateUtil.stringtoDate(jsonObject.getString("expireTime"), DateUtil.FORMAT_ONE).getTime());
        }
        getPurchaseStatusAndExpireTime(merchantShop, shopDetail);
        shopDetail.setThirdShopList(new ArrayList<>());
        List<MerchantThirdShop> merchantThirdShops = thirdShopRepostory.queryAllThirdShops(merchantId);
        if (!CollectionUtils.isEmpty(merchantThirdShops)) {
            List<ThirdShop> thirdShops = new ArrayList<>();
            for (MerchantThirdShop merchantThirdShop : merchantThirdShops) {
                ThirdShop thirdShop = new ThirdShop();
                thirdShop.setNick(merchantThirdShop.getNick());
                thirdShop.setShopName(merchantThirdShop.getShopName());
                thirdShop.setShopUrl(merchantThirdShop.getShopUrl());
                thirdShop.setSrcType(merchantThirdShop.getSrcType());
                thirdShop.setIsBindTb(0);
                if (merchantThirdShop.getThirdDeadline() != null) {
                    thirdShop.setIsBindTb(1);
                    thirdShop.setThirdDeadline(merchantThirdShop.getThirdDeadline().getTime());
                } else {
                    // TODO 喜销宝店铺接口不可用，但需要保证能提现调用过绑定接口，这里苟且以第三方的店铺图片有无，来判断是否调用过
                    if (StringUtils.isNotBlank(merchantThirdShop.getThirdPicPath())) {
                        thirdShop.setIsBindTb(1);
                    }
                }
                thirdShop.setThirdPicPath(merchantThirdShop.getThirdPicPath());
                thirdShop.setThirdShopId(merchantThirdShop.getId());
                thirdShops.add(thirdShop);
            }
            shopDetail.setThirdShopList(thirdShops);
        }
        return shopDetail;
    }

    /**
     * 获取店铺装修内容
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return 装修内容
     */
    @Override
    public CustomizeDTO getCustomize(Long merchantId, Long shopId) {
        MerchantShop merchantShop = shopRepostory.getByShopId(shopId);
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "店铺编号错误");
        Assert.isTrue(merchantShop.getMerchantId().equals(merchantId), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "您无权访问该店铺");
        CustomizeDTO customizeDTO = new CustomizeDTO();
        if (StringUtils.isNotBlank(merchantShop.getBannerJson())) {
            List<BannerObj> bannerObjList = JSONArray.parseArray(merchantShop.getBannerJson(), BannerObj.class);
            if (!CollectionUtils.isEmpty(bannerObjList)) {
                customizeDTO.setBanner(bannerObjList.get(0).getBannerUrl());
            }
        }
        if (StringUtils.isNotBlank(merchantShop.getCsJson())) {
            List<CsObj> csJsonList = JSONArray.parseArray(merchantShop.getCsJson(), CsObj.class);
            if (!CollectionUtils.isEmpty(csJsonList)) {
                customizeDTO.setCsPic(csJsonList.get(0).getPic());
                customizeDTO.setCsWechat(csJsonList.get(0).getWechat());
            }
        }
        if (StringUtils.isNotBlank(merchantShop.getGroupCode())) {
            customizeDTO.setGroupCode(merchantShop.getGroupCode());
        }
        customizeDTO.setShopId(shopId.toString());
        customizeDTO.setShopName(merchantShop.getShopName());
        customizeDTO.setPicPath(merchantShop.getPicPath());
        customizeDTO.setTitle(merchantShop.getTitle());

        return customizeDTO;
    }

    /**
     * 根据条件查询审核页面展示内容
     *
     * @param paramDTO
     * @return
     */
    @Override
    public AuditShopListResultDTO getListForPlatform(AuditShopParamDTO paramDTO) {
        IPage<MerchantShop> merchantShopPage = shopRepostory.pageListForPlatform(paramDTO);
        List<MerchantShop> records = merchantShopPage.getRecords();
        List<ShopDetail> shopDetails = new ArrayList<>();
        if (!CollectionUtils.isEmpty(records)) {
            for (MerchantShop merchantShop : records) {
                ShopDetail shopDetail = new ShopDetail();
                shopDetail.setCtime(merchantShop.getCtime().getTime());
                shopDetail.setShopId(merchantShop.getId());
                shopDetail.setCustomPicPath(merchantShop.getPicPath());
                shopDetail.setCustomShopName(merchantShop.getShopName());
                shopDetails.add(shopDetail);
            }
        }
        AuditShopListResultDTO resultDTO = new AuditShopListResultDTO();
        resultDTO.setCount(merchantShopPage.getTotal());
        resultDTO.setPage((int) merchantShopPage.getCurrent());
        resultDTO.setSize((int) merchantShopPage.getSize());
        resultDTO.setShopList(shopDetails);
        return resultDTO;
    }

    /**
     * 检查是否能创建店铺
     *
     * @param merchantId
     * @return
     */
    @Override
    public CanCreateShopResultDTO canCreateShop(Long merchantId, Long shopId) {
        Integer orderCount = merchantOrderRepository.countMerchantOrderForPurchaseShop(merchantId);
        Integer shopCount = shopRepostory.countByMerchantIdForPurchaseShop(merchantId);
        CanCreateShopResultDTO resultDTO = new CanCreateShopResultDTO();
        resultDTO.setCanCreate(0);
        if (orderCount > shopCount) {
            resultDTO.setCanCreate(1);
        }
        // 这里兼容旧数据，判断用户自己选择没有创建完成的店铺
        MerchantShop merchantShop = null;
        if (shopId != null && shopId > 0) {
            merchantShop = shopRepostory.getByShopId(shopId);
        } else {
//            List<MerchantShop> merchantShops = shopRepostory.selectUnfinishCreateShop(merchantId);
//            if (!CollectionUtils.isEmpty(merchantShops)) {
//                merchantShop = merchantShops.get(0);
//            }
            return resultDTO;
        }
        if (merchantShop != null) {
            ShopDetail shopDetail = new ShopDetail();
            shopDetail.setShopId(merchantShop.getId());
            if (merchantShop.getCategoryId() != null) {
                CategoryShop categoryShop = categoryShopRepository.selectCategoryShopByCid(merchantShop.getCategoryId().toString());
                shopDetail.setCategoryId(merchantShop.getCategoryId());
                shopDetail.setCategoryName(categoryShop.getName());
            }
            shopDetail.setCustomShopName(merchantShop.getShopName());
            shopDetail.setCustomPicPath(merchantShop.getPicPath());
            shopDetail.setUsername(merchantShop.getUsername());
            shopDetail.setPhone(merchantShop.getPhone());
            shopDetail.setAddressCode(merchantShop.getAddressCode());
            shopDetail.setAddressText(merchantShop.getAddressText());
            shopDetail.setAddressDetail(merchantShop.getAddressDetail());
            shopDetail.setThirdShopList(new ArrayList<>());
            List<MerchantThirdShop> merchantThirdShops = thirdShopRepostory.queryAllThirdShops(merchantId);
            if (!CollectionUtils.isEmpty(merchantThirdShops)) {
                List<ThirdShop> thirdShops = new ArrayList<>();
                for (MerchantThirdShop merchantThirdShop : merchantThirdShops) {
                    ThirdShop thirdShop = new ThirdShop();
                    thirdShop.setNick(merchantThirdShop.getNick());
                    thirdShop.setShopName(merchantThirdShop.getShopName());
                    thirdShop.setShopUrl(merchantThirdShop.getShopUrl());
                    thirdShop.setSrcType(merchantThirdShop.getSrcType());
                    thirdShops.add(thirdShop);
                }
                shopDetail.setThirdShopList(thirdShops);
            }
            resultDTO.setShopDetail(shopDetail);
            resultDTO.setCanCreate(1);
        }
        return resultDTO;
    }

    /**
     * 查询地址信息
     *
     * @param merchantId 商户id
     * @param pid        父级地区id
     * @param level      地区级别
     * @return
     */
    @Override
    public AreaListResultDTO getAreaList(Long merchantId, Long pid, Integer level) {
        if (level < 0 || level > AreaLevelEnum.town.getCode()) {
            return null;
        }
        // 省级pid为0
        if (level == AreaLevelEnum.province.getCode()) {
            pid = 0L;
        }
        List<CategoryArea> categoryAreas = categoryAreaRepository.selectByPidAndLevel(pid, level);
        AreaListResultDTO areaListResultDTO = new AreaListResultDTO();
        if (!CollectionUtils.isEmpty(categoryAreas)) {
            List<AreaDTO> areaDTOList = new ArrayList<>();
            for (CategoryArea categoryArea : categoryAreas) {
                AreaDTO areaDTO = new AreaDTO();
                BeanUtils.copyProperties(categoryArea, areaDTO);
                areaDTOList.add(areaDTO);
            }
            areaListResultDTO.setAreaList(areaDTOList);
            return areaListResultDTO;
        }
        return null;
    }
}
