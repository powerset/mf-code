package mf.code.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.shop.repo.po.ShopFootprint;

/**
 * mf.code.shop.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-23 12:56
 */
public interface ShopFootprintService extends IService<ShopFootprint> {
}
