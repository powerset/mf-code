package mf.code.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.merchant.repo.dao.MerchantBalanceMapper;
import mf.code.merchant.repo.po.MerchantBalance;
import mf.code.shop.service.MerchantBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.common.job.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-12 23:00
 */
@Slf4j
@Service
public class MerchantBalanceServiceImpl extends ServiceImpl<MerchantBalanceMapper, MerchantBalance> implements MerchantBalanceService {
	@Autowired
	private MerchantBalanceMapper merchantBalanceMapper;

	/**
	 * 更新商户余额
	 * @param merchantId
	 * @param shopId
	 * @param merchantIncome
	 */
	@Override
	public Integer updateMerchantBalance(Long merchantId, Long shopId, BigDecimal merchantIncome) {
		int rows;

		Date now = new Date();
		QueryWrapper<MerchantBalance> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(MerchantBalance::getMerchantId, merchantId)
				.eq(MerchantBalance::getShopId, shopId);
		MerchantBalance merchantBalance = merchantBalanceMapper.selectOne(wrapper);
		if (merchantBalance == null) {
			// 新增一条余额记录
			merchantBalance = new MerchantBalance();
			merchantBalance.setMerchantId(merchantId);
			merchantBalance.setShopId(shopId);
			merchantBalance.setCustomerId(0L);
			merchantBalance.setDepositCash(BigDecimal.ZERO);
			merchantBalance.setDepositAdvance(merchantIncome);
			merchantBalance.setCtime(now);
			merchantBalance.setUtime(now);
			merchantBalance.setVersion(5);
			rows = merchantBalanceMapper.insertSelective(merchantBalance);
			if (rows == 0) {
				log.error("商户余额创建失败，merchantId = {}, shopId = {}",merchantId, shopId);
			}
		} else {
			// 更新商户余额
			Map<String, Object> params = new HashMap<>();
			params.put("shopId", shopId);
			params.put("depositAdvance", merchantIncome);
			params.put("utime", now);

			rows = merchantBalanceMapper.updateBalanceByParams(params);
			if (rows == 0) {
				log.error("商户余额更新失败，merchantId = {}, shopId = {}",merchantId, shopId);
				// TODO 发送邮件报警
			}
		}
		return rows;
	}
}
