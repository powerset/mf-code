package mf.code.shop.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.NumberValidationUtil;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.shop.api.feignclient.*;
import mf.code.shop.domain.aggregateroot.ShoppingMall;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.po.ShopFootprint;
import mf.code.shop.repo.repository.ShoppingMallRepository;
import mf.code.shop.service.HomePageService;
import mf.code.shop.service.ShopFootprintService;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.shop.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-16 19:53
 */
@Slf4j
@Service
public class HomePageServiceImpl implements HomePageService {
    @Autowired
    private ShoppingMallRepository shoppingMallRepository;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private ShopFootprintService shopFootprintService;
    @Autowired
    private UserAppService userAppService;
    @Autowired
    private OrderAppService orderAppService;
    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /**
     * 查询店铺首页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryHomePage(String userId, Long shopId, Long offset, Long size) {

        // 查询店铺
        ShoppingMall shoppingMall = shoppingMallRepository.findById(shopId);
        if (shoppingMall == null) {
            log.error("查无此店铺, sid = {}", shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此店铺");
        }
        //是否购买过新人专享
        boolean newManRecommandImpression = true;
        List<Long> productIds = goodsAppService.getProductIds(shopId, ProductShowTypeEnum.NEWBIE.getCode());
        if (!CollectionUtils.isEmpty(productIds) && StringUtils.isNotBlank(userId)) {
            //查询新人专享是否下过单
            Integer num = orderAppService.queryOrderNumByProductIds(shopId, Long.parseLong(userId), productIds);
            if (num != null && num > 0) {
                newManRecommandImpression = false;
            }
        }
        // 获取新人专享销售商品
        AppletUserProductRebateDTO newMansProductRebateDTO = null;
        if (newManRecommandImpression) {
            newMansProductRebateDTO = goodsAppService.pageGoodsSaleWithSalesVolumeDesc(shopId, ProductShowTypeEnum.NEWBIE.getCode(), 0L, size);
            if (newMansProductRebateDTO == null) {
                log.error("获取商品列表失败");
            }

            if (!CollectionUtils.isEmpty(newMansProductRebateDTO.getProductDistributionDTOList()) && newMansProductRebateDTO.getProductDistributionDTOList().size() > 3) {
                newMansProductRebateDTO.setProductDistributionDTOList(newMansProductRebateDTO.getProductDistributionDTOList().subList(0, 3));
            }

            if (!CollectionUtils.isEmpty(newMansProductRebateDTO.getProductDistributionDTOList())) {
                for (ProductDistributionDTO productDistributionDTO : newMansProductRebateDTO.getProductDistributionDTOList()) {
                    JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                    if (mainPics.size() == 0) {
                        continue;
                    }
                    productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);

                    String salesVolume = productDistributionDTO.getSalesVolume();
                    int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                    productDistributionDTO.setSalesVolume(NumberValidationUtil.convertNumberForString(totalSale));
                }
            }
        }

        // 获取销售商品
        AppletUserProductRebateDTO appletUserProductRebateDTO = goodsAppService.pageGoodsSaleWithSalesVolumeDesc(shopId, ProductShowTypeEnum.NONE.getCode(), offset, size);
        if (appletUserProductRebateDTO == null) {
            log.error("获取商品列表失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "获取商品列表失败");
        }

        if (StringUtils.isNotBlank(userId)) {
            appletUserProductRebateDTO.setUserId(Long.parseLong(userId));

            // 获取 对此用户 的商品返利金额
            AppletUserProductRebateDTO productRebate = distributionAppService.queryProductRebateByUserIdShopId(appletUserProductRebateDTO);
            if (productRebate == null) {
                log.error("获取用户的商品返利金额 异常, userId = {}, shopId = {}", userId, shopId);
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
            }
            appletUserProductRebateDTO = productRebate;
        }

        AppletMybatisPageDto<ProductDistributionDTO> distributionProductPage = new AppletMybatisPageDto<>(size, offset);
        distributionProductPage.setContent(appletUserProductRebateDTO.getProductDistributionDTOList());
        if (offset + size < appletUserProductRebateDTO.getTotal()) {
            distributionProductPage.setPullDown(true);
        }

        if (!CollectionUtils.isEmpty(appletUserProductRebateDTO.getProductDistributionDTOList())) {
            for (ProductDistributionDTO productDistributionDTO : appletUserProductRebateDTO.getProductDistributionDTOList()) {
                JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                if (mainPics.size() == 0) {
                    continue;
                }
                productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);

                String salesVolume = productDistributionDTO.getSalesVolume();
                int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                productDistributionDTO.setSalesVolume(NumberValidationUtil.convertNumberForString(totalSale));
            }
        }


        // 查询 在此店铺内， 用户 推广收益情况
        UserDistributionInfoDTO userDistributionInfo = null;
        if (StringUtils.isNotBlank(userId)) {
            userDistributionInfo = distributionAppService.queryDistributionInfo(Long.valueOf(userId), shopId);
        }

        // 加工首页数据
        MerchantShop shoppingMallInfo = shoppingMall.getShoppingMallInfo();
        Map<String, Object> homePage = new HashMap<>();
        homePage.put("picPath", shoppingMallInfo.getPicPath());
        homePage.put("shopName", shoppingMallInfo.getShopName());
        homePage.put("bannerJson", JSON.parseArray(shoppingMallInfo.getBannerJson()));
        homePage.put("csJson", JSON.parseArray(shoppingMallInfo.getCsJson()));
        homePage.put("goodsRecommend", distributionProductPage);
        homePage.put("newMansGoodsRecommend", newManRecommandImpression ? newMansProductRebateDTO.getProductDistributionDTOList() : new ArrayList<>());
        homePage.put("commissionStandard", 0);
        homePage.put("spendingAmountTomonth", 0);
        homePage.put("leftAmount", 0);

        if (userDistributionInfo != null) {
            homePage.putAll(BeanMapUtil.beanToMap(userDistributionInfo));
        }

        // 2.3迭代 新手任务
        if (StringUtils.isNotBlank(userId)) {
            Map<String, Object> newBie = oneAppService.queryHomePageNewbieTask(shopId, Long.parseLong(userId));
            homePage.put("newBie", newBie);
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS, homePage);
    }

    /***
     * 查询新人专享列表
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryNewManRecommends(Long userId, Long shopId, Long offset, Long size) {
        // 查询店铺
        ShoppingMall shoppingMall = shoppingMallRepository.findById(shopId);
        if (shoppingMall == null) {
            log.error("查无此店铺, sid = {}", shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此店铺");
        }

        // 获取新人专享销售商品
        AppletUserProductRebateDTO newMansProductRebateDTO = goodsAppService.pageGoodsSaleWithSalesVolumeDesc(shopId, ProductShowTypeEnum.NEWBIE.getCode(), offset, size);
        if (newMansProductRebateDTO == null) {
            log.error("获取商品列表失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "获取新人商品列表失败");
        }

        newMansProductRebateDTO.setUserId(userId);

        // 获取 对此用户 的商品返利金额
        AppletUserProductRebateDTO productRebate = distributionAppService.queryProductRebateByUserIdShopId(newMansProductRebateDTO);
        if (productRebate == null) {
            log.error("queryNewManRecommends 获取用户的商品返利金额 异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候重试");
        }

        AppletMybatisPageDto<ProductDistributionDTO> distributionProductPage = new AppletMybatisPageDto<>(size, offset);
        distributionProductPage.setContent(productRebate.getProductDistributionDTOList());
        if (offset + size < newMansProductRebateDTO.getTotal()) {
            distributionProductPage.setPullDown(true);
        }

        if (!CollectionUtils.isEmpty(productRebate.getProductDistributionDTOList())) {
            for (ProductDistributionDTO productDistributionDTO : productRebate.getProductDistributionDTOList()) {
                JSONArray mainPics = JSON.parseArray(productDistributionDTO.getMainPics());
                if (mainPics.size() == 0) {
                    continue;
                }
                productDistributionDTO.setMainPics(mainPics.getString(0) + compressionRatioPic);

                String salesVolume = productDistributionDTO.getSalesVolume();
                int totalSale = productDistributionDTO.getInitSale() + NumberUtils.toInt(salesVolume);
                productDistributionDTO.setSalesVolume(NumberValidationUtil.convertNumberForString(totalSale));
            }
        }

        Map<String, Object> homePage = new HashMap<>();
        homePage.put("newMansGoodsRecommends", distributionProductPage);
        homePage.put("openRedPack", new HashMap<>());
        //拆红包信息@百川
        Map<String, Object> openRedPackageMap = oneAppService.queryOpenRedPackageStatus(shopId, userId);
        if (openRedPackageMap != null) {
            homePage.put("openRedPack", openRedPackageMap);
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS, homePage);
    }

    /**
     * 查询 用户店铺足迹
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryFootprint(Long userId, Long shopId, Long offset, Long size) {
        // 获取用户信息
        UserResp user = userAppService.queryUser(userId);
        if (user == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
        }
        // 获取我的店铺信息
        ShoppingMall shoppingMall = this.shoppingMallRepository.findById(user.getVipShopId());
        if (shoppingMall == null) {
            log.error("查无此店铺, shopId = {}", user.getVipShopId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
        }
        MerchantShop merchantShop = shoppingMall.getShoppingMallInfo();
        // 处理 我的店铺返回数据
        Map<String, Object> myShop = new HashMap<>();
        myShop.put("merchantId", merchantShop.getMerchantId());
        myShop.put("shopId", merchantShop.getId());
        myShop.put("shopName", merchantShop.getShopName());
        myShop.put("picPath", merchantShop.getPicPath());
        myShop.put("serviceWx", merchantShop.getCsJson());
        myShop.put("status", shoppingMall.getId().equals(shopId) ? 1 : 0);

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("myShop", myShop);

        AppletMybatisPageDto<Map<String, Object>> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);
        // 按utime 时间倒序 ，第一条就是 当前访问的店铺
        Page<ShopFootprint> page = new Page<>(offset/size + 1, size);
        page.setDesc("utime");
        QueryWrapper<ShopFootprint> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ShopFootprint::getUserId, userId);
        IPage<ShopFootprint> shopFootprintIPage = shopFootprintService.page(page, wrapper);
        if (shopFootprintIPage == null || CollectionUtils.isEmpty(shopFootprintIPage.getRecords())) {
            resultVO.put("viewShop", appletMybatisPageDto);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
        }
        List<ShopFootprint> records = shopFootprintIPage.getRecords();
        List<Map<String, Object>> viewShopList = new ArrayList<>();
        for (ShopFootprint shopFootprint : records) {
            Map<String, Object> viewShop = new HashMap<>();
            viewShop.put("merchantId", shopFootprint.getMerchantId());
            viewShop.put("shopId", shopFootprint.getShopId());
            viewShop.put("shopName", shopFootprint.getShopName());
            viewShop.put("picPath", shopFootprint.getPicPath());
            viewShop.put("serviceWx", shopFootprint.getCsJson());
            viewShop.put("status", shopFootprint.getShopId().equals(shopId) ? 1 : 0);
            viewShopList.add(viewShop);
        }
        appletMybatisPageDto.setContent(viewShopList);
        if (offset + size < shopFootprintIPage.getTotal()) {
            appletMybatisPageDto.setPullDown(true);
        }
        resultVO.put("viewShop", appletMybatisPageDto);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }
}
