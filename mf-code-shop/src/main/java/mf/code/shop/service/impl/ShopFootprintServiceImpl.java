package mf.code.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.shop.repo.dao.ShopFootprintMapper;
import mf.code.shop.repo.po.ShopFootprint;
import mf.code.shop.service.ShopFootprintService;
import org.springframework.stereotype.Service;

/**
 * mf.code.shop.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-23 12:57
 */
@Slf4j
@Service
public class ShopFootprintServiceImpl extends ServiceImpl<ShopFootprintMapper, ShopFootprint> implements ShopFootprintService {
}
