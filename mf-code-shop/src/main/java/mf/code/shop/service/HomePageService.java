package mf.code.shop.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.shop.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-16 19:53
 */
public interface HomePageService {

	/**
	 * 查询店铺首页
	 *
	 * @param userId
	 * @param shopId
	 * @param offset
	 * @param size
	 * @return
	 */
	SimpleResponse queryHomePage(String userId, Long shopId, Long offset, Long size);

	/***
	 * 查询新人专享列表
	 * @param userId
	 * @param shopId
	 * @param offset
	 * @param size
	 * @return
	 */
	SimpleResponse queryNewManRecommends(Long userId, Long shopId, Long offset, Long size);

	/**
	 * 查询 用户店铺足迹
	 *
	 * @param userId
	 * @param shopId
	 * @param offset
	 * @param size
	 * @return
	 */
	SimpleResponse queryFootprint(Long userId, Long shopId, Long offset, Long size);
}
