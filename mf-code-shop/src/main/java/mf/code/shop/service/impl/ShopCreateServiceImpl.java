package mf.code.shop.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.DelEnum;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.repo.repository.CommonDictRepository;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.merchant.dto.JkmfPurchaseDTO;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.repository.MerchantOrderRepository;
import mf.code.shop.constants.ThirdShopSrcTypeEnum;
import mf.code.shop.dto.*;
import mf.code.shop.repo.po.MerchantShop;
import mf.code.shop.repo.po.MerchantThirdShop;
import mf.code.shop.repo.repository.ShopRepostory;
import mf.code.shop.repo.repository.ThirdShopRepostory;
import mf.code.shop.service.teacher.PurchaseTeacherAboutService;
import mf.code.shop.service.ShopCreateService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-29 18:05
 */
@Slf4j
@Service
public class ShopCreateServiceImpl implements ShopCreateService {

    @Autowired
    private ShopRepostory shopRepostory;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private ThirdShopRepostory thirdShopRepostory;
    @Autowired
    private CommonDictRepository commonDictRepository;
    @Autowired
    private PurchaseTeacherAboutService purchaseTeacherAboutService;

    @Value("${seller.purchaseTime}")
    private String merchantPurchaseTime;

    /**
     * 创建店铺第一步
     *
     * @param shopStepOneDTO 第一步入参
     * @return 店铺Id
     */
    @Override
    public Long createStepOne(ShopStepOneDTO shopStepOneDTO) {
        // 断言参数
        Assert.notNull(shopStepOneDTO, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参为空");
        Assert.isInteger(shopStepOneDTO.getCategoryId(), ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请选择店铺类目");
        Assert.isBlank(shopStepOneDTO.getCustomPicPath(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), "请上传店铺图标");
        Assert.isBlank(shopStepOneDTO.getCustomShopName(), ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请填写店铺名称");
        // 判断是否符合创建店铺条件，是否订单可以匹配
        Long merchantId = Long.valueOf(shopStepOneDTO.getMerchantId());
        Integer orderCount = merchantOrderRepository.countMerchantOrderForPurchaseShop(merchantId);
        Integer shopCount = shopRepostory.countByMerchantIdForPurchaseShop(merchantId);
        MerchantShop merchantShop = new MerchantShop();
        if (StringUtils.isNotBlank(shopStepOneDTO.getShopId())) {
            Long shopId = Long.valueOf(shopStepOneDTO.getShopId());
            MerchantShop shop = shopRepostory.getByShopId(shopId);
            Assert.notNull(shop, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "入参传输错误");
            merchantShop.setId(shopId);
        } else {
            Assert.isTrue(orderCount > shopCount, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请先购买创建店铺资格");
        }
        merchantShop.setCategoryId(Long.valueOf(shopStepOneDTO.getCategoryId()));
        merchantShop.setPicPath(shopStepOneDTO.getCustomPicPath());
        merchantShop.setShopName(shopStepOneDTO.getCustomShopName());
        merchantShop.setMerchantId(merchantId);
        merchantShop.setNick("");
        merchantShop.setDel(DelEnum.NO.getCode());
        // 创建店铺
        Integer count = shopRepostory.createOrUpdateBySelective(merchantShop);
        Assert.isTrue(count != 0, ApiStatusEnum.ERROR_BUS_NO5.getCode(), "保存店铺出错了!");
        // 创建第三方店铺
        if (StringUtils.isNotBlank(shopStepOneDTO.getThirdShopList())) {
            List<ThirdShop> thirdShopList = JSONArray.parseArray(shopStepOneDTO.getThirdShopList(), ThirdShop.class);
            if (!CollectionUtils.isEmpty(thirdShopList)) {
                for (ThirdShop thirdShop : thirdShopList) {
                    MerchantThirdShop merchantThirdShop = new MerchantThirdShop();
                    merchantThirdShop.setMerchantId(Long.valueOf(shopStepOneDTO.getMerchantId()));
                    merchantThirdShop.setSrcType(ThirdShopSrcTypeEnum.TAOBAO.getCode());
                    merchantThirdShop.setNick(thirdShop.getNick());
                    merchantThirdShop.setShopName(thirdShop.getShopName());
                    merchantThirdShop.setShopUrl(thirdShop.getShopUrl());
                    merchantThirdShop.setThirdService(1);
                    merchantThirdShop.setDel(DelEnum.NO.getCode());
                    try {
                        // 不再关注唯一索引异常
                        thirdShopRepostory.createBySelective(merchantThirdShop);
                    } catch (DuplicateKeyException exception) {
                        log.error(exception.getLocalizedMessage());
                    }
                }
            }
        }

        return merchantShop.getId();
    }

    /**
     * 创建店铺第二步
     *
     * @param shopStepTwoDTO 第二步入参
     * @return 店铺详情
     */
    @Override
    public Integer createStepTwo(ShopStepTwoDTO shopStepTwoDTO) {
        // 断言参数
        Assert.notNull(shopStepTwoDTO, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参为空");
        Assert.isInteger(shopStepTwoDTO.getShopId(), ApiStatusEnum.ERROR_BUS_NO2.getCode(), "店铺编号传输错误");
        Assert.isBlank(shopStepTwoDTO.getUsername(), ApiStatusEnum.ERROR_BUS_NO3.getCode(), "请填写店长姓名");
        Assert.isBlank(shopStepTwoDTO.getPhone(), ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请填写联系方式");
        Assert.isBlank(shopStepTwoDTO.getAddressCode(), ApiStatusEnum.ERROR_BUS_NO5.getCode(), "请填写经营省市区");
        Assert.isBlank(shopStepTwoDTO.getAddressText(), ApiStatusEnum.ERROR_BUS_NO6.getCode(), "请填写经营省市区");
        Assert.isBlank(shopStepTwoDTO.getAddressDetail(), ApiStatusEnum.ERROR_BUS_NO7.getCode(), "请填写经营详细地址");
        // 更新操作
        MerchantShop merchantShop = shopRepostory.getByShopId(Long.valueOf(shopStepTwoDTO.getShopId()));
        Assert.notNull(merchantShop, ApiStatusEnum.ERROR_BUS_NO8.getCode(), "请填写请先执行第一步创建");
        // 暂时不需要校验有没有重复操作
        merchantShop.setUsername(shopStepTwoDTO.getUsername());
        merchantShop.setPhone(shopStepTwoDTO.getPhone());
        merchantShop.setAddressCode(shopStepTwoDTO.getAddressCode());
        merchantShop.setAddressText(shopStepTwoDTO.getAddressText());
        merchantShop.setAddressDetail(shopStepTwoDTO.getAddressDetail());

        // 在此保存订单购买数据。兼容旧数据，如果创建时间晚于商城上线时间的话则需要判断订单;如果早于上线时间则直接保存
        if(merchantShop.getCtime().before(DateUtil.stringtoDate(merchantPurchaseTime, DateUtil.FORMAT_ONE))){
            return shopRepostory.updateByPrimaryKeySelective(merchantShop);
        }

        List<MerchantOrder> merchantOrders = merchantOrderRepository.selectMerchantOrderForPurchaseShop(Long.valueOf(shopStepTwoDTO.getMerchantId()));
        Assert.isTrue(!CollectionUtils.isEmpty(merchantOrders), ApiStatusEnum.ERROR_BUS_NO8.getCode(), "您的购买记录不匹配");
        MerchantOrder merchantOrderUpdate = null;
        for (MerchantOrder merchantOrder : merchantOrders) {
            if (merchantOrder.getRemark() == null
                    || StringUtils.isBlank(merchantOrder.getRemark())
                    || merchantOrder.getRemark().contains("支付订单")) {
                merchantOrderUpdate = merchantOrder;
                break;
            }
        }
        Assert.notNull(merchantOrderUpdate, ApiStatusEnum.ERROR_BUS_NO9.getCode(), "您的购买记录不匹配");
        String purchaseJson = changeNotifyTimeToPurchaseJson(merchantOrderUpdate);
        merchantOrderUpdate.setShopId(merchantShop.getId());
        merchantOrderUpdate.setRemark(purchaseJson);
        merchantOrderRepository.updateMerchantOrderForPurchaseShop(merchantOrderUpdate);
        // 填写购买时间
        merchantShop.setPurchaseVersion(changeMerchantOrderBizTypeToPurchaseVersion(merchantOrderUpdate.getBizType()));
        merchantShop.setPurchaseJson(purchaseJson);
        // 直接审核通过
        merchantShop.setOpenstatus(1);

        Integer integer = shopRepostory.updateByPrimaryKeySelective(merchantShop);

        //更新集客师金额
        MerchantOrder merchantOrder = this.merchantOrderRepository.purchaseFirstAppletMerchantOrder(merchantShop.getMerchantId());
        if (merchantOrder != null) {
            this.purchaseTeacherAboutService.dealTeacher(merchantOrder, merchantShop);
        }
        return integer;
    }


    /**
     * 转换订单类型-->店铺购买版本
     *
     * @param bizType
     * @return 订购版本：0:不可用 1试用版 2公开版 3私有版
     */
    private int changeMerchantOrderBizTypeToPurchaseVersion(int bizType) {
        if (bizType == BizTypeEnum.PURCHASE_PUBLIC.getCode()) {
            return MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode();
        }
        if (bizType == BizTypeEnum.PURCHASE_PRIVATE.getCode()) {
            return MerchantShopPurchaseVersionEnum.VERSION_PRIVATE.getCode();
        }
        if (bizType == BizTypeEnum.LEVELUP_PRIVATE.getCode()) {
            return MerchantShopPurchaseVersionEnum.VERSION_PRIVATE.getCode();
        }
        if(bizType == BizTypeEnum.PURCHASE_CLEARANCE.getCode()){
            return MerchantShopPurchaseVersionEnum.CLEAN.getCode();
        }
        if(bizType == BizTypeEnum.PURCHASE_WEIGHTING.getCode()){
            return MerchantShopPurchaseVersionEnum.WEIGHTING.getCode();
        }
        return MerchantShopPurchaseVersionEnum.VERSION_TEST.getCode();
    }

    /**
     * 返回json
     *
     * @return
     */
    private String changeNotifyTimeToPurchaseJson(MerchantOrder merchantOrder) {
        JkmfPurchaseDTO jkmfPurchaseDTO = null;
        //获取字典表信息
        CommonDict commonDict = this.commonDictRepository.selectByTypeKey("jkmfPurchase", "pay_menu");
        List<JkmfPurchaseDTO> jkmfPurchaseDTOList = JSONObject.parseArray(commonDict.getValue(), JkmfPurchaseDTO.class);
        if (!CollectionUtils.isEmpty(jkmfPurchaseDTOList)) {
            for (JkmfPurchaseDTO dto : jkmfPurchaseDTOList) {
                //[{"money":"588.00","name":"购买公有版","rate":100},{"money":"5888.00","name":"购买私有版","rate":100}]
                jkmfPurchaseDTO = dto;
                boolean bizVersion = jkmfPurchaseDTO != null && merchantOrder.getBizValue().toString().equals(jkmfPurchaseDTO.getBizVersion().toString());
                if (bizVersion) {
                    break;
                }
            }
        }
        //获取购买周期
        int months = 0;
        if (jkmfPurchaseDTO != null && jkmfPurchaseDTO.getPurchaseCycle() != null) {
            months = jkmfPurchaseDTO.getPurchaseCycle();
        }
        Map<String, Object> purchase = new HashMap<>();
        purchase.put("purchaseTime", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        purchase.put("expireTime", DateFormatUtils.format(DateUtils.addMonths(new Date(), months), "yyyy-MM-dd HH:mm:ss"));
        return JSONObject.toJSONString(purchase);
    }

}
