package mf.code.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.merchant.repo.po.MerchantBalance;

import java.math.BigDecimal;

/**
 * mf.code.common.job.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-12 22:59
 */
public interface MerchantBalanceService extends IService<MerchantBalance> {
    /**
     * 更新商户余额
     *
     * @param merchantId
     * @param shopId
     * @param merchantIncome
     */
    Integer updateMerchantBalance(Long merchantId, Long shopId, BigDecimal merchantIncome);

    /**
     * 通过店铺id获取店铺的可提现余额
     * @param shopId
     * @return
     */
    //BigDecimal findDepositAdvanceByShopId(Long shopId);
}
