package mf.code.shop.domain.aggregateroot;

import lombok.Data;
import mf.code.shop.repo.po.MerchantShop;

/**
 * mf.code.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-28 18:07
 */
@Data
public class ShoppingMall {
    private Long id;
    private MerchantShop shoppingMallInfo;
}
