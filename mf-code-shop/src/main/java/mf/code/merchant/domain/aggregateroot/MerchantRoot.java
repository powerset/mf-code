package mf.code.merchant.domain.aggregateroot;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.shop.repo.po.MerchantShop;

/**
 * mf.code.merchant.domain.valueobject
 * Description:
 *
 * @author gel
 * @date 2019-04-02 16:37
 */
@ToString
@EqualsAndHashCode
public class MerchantRoot {

    /**
     * 商户数据
     */
    private Merchant merchant;
    /**
     * 商户订单
     */
    private MerchantOrder merchantOrder;
    /**
     * 店铺数据
     */
    private MerchantShop merchantShop;

    /**
     * 是否购买小程序店铺
     * @return
     */
    public boolean isPurchase() {
        return false;
    }


    /**
     * 是否创建店铺
     * @return
     */
    public boolean isCreateShop() {
        return false;
    }


}
