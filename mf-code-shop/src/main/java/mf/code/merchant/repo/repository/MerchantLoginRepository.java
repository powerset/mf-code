package mf.code.merchant.repo.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.merchant.repo.po.Merchant;

import java.util.List;

public interface MerchantLoginRepository extends IService<Merchant> {

    Merchant getMerchantById(Long id);

    /**
     * 根据手机号查询商户信息
     *
     * @param phone 手机号
     * @return 商户信息
     */
    Merchant getMerchantByPhone(String phone);

    /**
     * 更新商户密码
     *
     * @param id       商户id
     * @param password 密码
     */
    Integer updateMerchantPassword(Long id, String password);

    /**
     * 计算商户密码
     *
     * @param phone    手机号
     * @param password 明文密码
     * @return 密文密码
     */
    String calcMerchantPassword(String phone, String password);

    /**
     * 创建商户信息
     *
     * @param merchant
     * @return
     */
    int createMerchant(Merchant merchant);

    /**
     * 根据商户id列表查询商户信息
     * @param idList
     * @return
     */
    List<Merchant> getMerchantListByIdList(List<Long> idList);

    /**
     * 抖带带 通过openId 获取主播信息
     * @param openId
     * @return
     */
    Merchant getMerchantByOpenId(String openId);
}
