package mf.code.merchant.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * merchant
 * 商户
 */
public class Merchant implements Serializable {
    /**
     * 商户id
     */
    private Long id;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 状态: 0: 创建 1：喜销宝服务调用成功 -1：喜销宝服务调用失败
     */
    private Integer status;

    /**
     * 最后登录IP
     */
    private String lastLoginIp;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 登录错误次数
     */
    private Integer loginErrorCount;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 账号类型（0:父账号，1:运营子账号，2:全职能子账号）
     */
    private Integer type;

    /**
     * 主账号id
     */
    private Long parentId;

    /**
     * 商户帐号名称
     */
    private String name;

    /**
     * 用户授权状态，0:未授权，1:授权
     */
    private Integer grantStatus;

    /**
     * 用户授权时间
     */
    private Date grantTime;

    /**
     * 用户角色：默认0，集客魔方商户；1 抖带带主播
     */
    private Integer role;

    /**
     * 头像图片地址
     */
    private String avatarUrl;

    /**
     * 权限设置（待定）
     */
    private String permission;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer del;

    /**
     * 微信openId
     */
    private String openId;

    /**
     * 子账号启用状态（子账号才会使用，0：未启用，1：启用）
     */
    private Integer subStatus;

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版[{"version":1},{"version":2}]
     */
    private String purchaseVersionJson;

    /**
     * merchant
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商户id
     * @return id 商户id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商户id
     * @param id 商户id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 手机号码
     * @return phone 手机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号码
     * @param phone 手机号码
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 密码
     * @return password 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 状态: 0: 创建 1：喜销宝服务调用成功 -1：喜销宝服务调用失败
     * @return status 状态: 0: 创建 1：喜销宝服务调用成功 -1：喜销宝服务调用失败
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态: 0: 创建 1：喜销宝服务调用成功 -1：喜销宝服务调用失败
     * @param status 状态: 0: 创建 1：喜销宝服务调用成功 -1：喜销宝服务调用失败
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 最后登录IP
     * @return last_login_ip 最后登录IP
     */
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    /**
     * 最后登录IP
     * @param lastLoginIp 最后登录IP
     */
    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp == null ? null : lastLoginIp.trim();
    }

    /**
     * 最后登录时间
     * @return last_login_time 最后登录时间
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 最后登录时间
     * @param lastLoginTime 最后登录时间
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * 登录错误次数
     * @return login_error_count 登录错误次数
     */
    public Integer getLoginErrorCount() {
        return loginErrorCount;
    }

    /**
     * 登录错误次数
     * @param loginErrorCount 登录错误次数
     */
    public void setLoginErrorCount(Integer loginErrorCount) {
        this.loginErrorCount = loginErrorCount;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 账号类型（0:父账号，1:运营子账号，2:全职能子账号）
     * @return type 账号类型（0:父账号，1:运营子账号，2:全职能子账号）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 账号类型（0:父账号，1:运营子账号，2:全职能子账号）
     * @param type 账号类型（0:父账号，1:运营子账号，2:全职能子账号）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 主账号id
     * @return parent_id 主账号id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 主账号id
     * @param parentId 主账号id
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 商户帐号名称
     * @return name 商户帐号名称
     */
    public String getName() {
        return name;
    }

    /**
     * 商户帐号名称
     * @param name 商户帐号名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 用户授权状态，0:未授权，1:授权
     * @return grant_status 用户授权状态，0:未授权，1:授权
     */
    public Integer getGrantStatus() {
        return grantStatus;
    }

    /**
     * 用户授权状态，0:未授权，1:授权
     * @param grantStatus 用户授权状态，0:未授权，1:授权
     */
    public void setGrantStatus(Integer grantStatus) {
        this.grantStatus = grantStatus;
    }

    /**
     * 用户授权时间
     * @return grant_time 用户授权时间
     */
    public Date getGrantTime() {
        return grantTime;
    }

    /**
     * 用户授权时间
     * @param grantTime 用户授权时间
     */
    public void setGrantTime(Date grantTime) {
        this.grantTime = grantTime;
    }

    /**
     * 用户角色：默认0，集客魔方商户；1 抖带带主播
     * @return role 用户角色：默认0，集客魔方商户；1 抖带带主播
     */
    public Integer getRole() {
        return role;
    }

    /**
     * 用户角色：默认0，集客魔方商户；1 抖带带主播
     * @param role 用户角色：默认0，集客魔方商户；1 抖带带主播
     */
    public void setRole(Integer role) {
        this.role = role;
    }

    /**
     * 头像图片地址
     * @return avatar_url 头像图片地址
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     * 头像图片地址
     * @param avatarUrl 头像图片地址
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl == null ? null : avatarUrl.trim();
    }

    /**
     * 权限设置（待定）
     * @return permission 权限设置（待定）
     */
    public String getPermission() {
        return permission;
    }

    /**
     * 权限设置（待定）
     * @param permission 权限设置（待定）
     */
    public void setPermission(String permission) {
        this.permission = permission == null ? null : permission.trim();
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @return del 删除标识 0:未删除 1:已删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @param del 删除标识 0:未删除 1:已删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 微信openId
     * @return open_id 微信openId
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 微信openId
     * @param openId 微信openId
     */
    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    /**
     * 子账号启用状态（子账号才会使用，0：未启用，1：启用）
     * @return sub_status 子账号启用状态（子账号才会使用，0：未启用，1：启用）
     */
    public Integer getSubStatus() {
        return subStatus;
    }

    /**
     * 子账号启用状态（子账号才会使用，0：未启用，1：启用）
     * @param subStatus 子账号启用状态（子账号才会使用，0：未启用，1：启用）
     */
    public void setSubStatus(Integer subStatus) {
        this.subStatus = subStatus;
    }

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版[{"version":1},{"version":2}]
     * @return purchase_version_json 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版[{"version":1},{"version":2}]
     */
    public String getPurchaseVersionJson() {
        return purchaseVersionJson;
    }

    /**
     * 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版[{"version":1},{"version":2}]
     * @param purchaseVersionJson 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版[{"version":1},{"version":2}]
     */
    public void setPurchaseVersionJson(String purchaseVersionJson) {
        this.purchaseVersionJson = purchaseVersionJson == null ? null : purchaseVersionJson.trim();
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", phone=").append(phone);
        sb.append(", password=").append(password);
        sb.append(", status=").append(status);
        sb.append(", lastLoginIp=").append(lastLoginIp);
        sb.append(", lastLoginTime=").append(lastLoginTime);
        sb.append(", loginErrorCount=").append(loginErrorCount);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", type=").append(type);
        sb.append(", parentId=").append(parentId);
        sb.append(", name=").append(name);
        sb.append(", grantStatus=").append(grantStatus);
        sb.append(", grantTime=").append(grantTime);
        sb.append(", role=").append(role);
        sb.append(", avatarUrl=").append(avatarUrl);
        sb.append(", permission=").append(permission);
        sb.append(", del=").append(del);
        sb.append(", openId=").append(openId);
        sb.append(", subStatus=").append(subStatus);
        sb.append(", purchaseVersionJson=").append(purchaseVersionJson);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}