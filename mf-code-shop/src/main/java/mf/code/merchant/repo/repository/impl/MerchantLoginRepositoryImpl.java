package mf.code.merchant.repo.repository.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.common.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.Assert;
import mf.code.common.utils.MD5Util;
import mf.code.merchant.constants.MerchantRoleEnum;
import mf.code.merchant.constants.MerchantTypeEnum;
import mf.code.merchant.repo.dao.MerchantMapper;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.repository.MerchantLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MerchantLoginRepositoryImpl extends ServiceImpl<MerchantMapper, Merchant> implements MerchantLoginRepository {

    @Autowired
    private MerchantMapper merchantMapper;


    @Override
    public Merchant getMerchantById(Long id) {
        return merchantMapper.selectByPrimaryKey(id);
    }

    @Override
    public Merchant getMerchantByPhone(String phone) {
        QueryWrapper<Merchant> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Merchant::getRole, MerchantRoleEnum.JKMF.getCode())
                .eq(Merchant::getPhone, phone)
                .eq(Merchant::getType, MerchantTypeEnum.PARERNT_MERCHANT.getCode())
                .eq(Merchant::getDel, DelEnum.NO.getCode());
        Merchant merchant = merchantMapper.selectOne(queryWrapper);
        return merchant;
    }

    /**
     * 更新商户密码
     *
     * @param id       商户id
     * @param password 密码
     */
    @Override
    public Integer updateMerchantPassword(Long id, String password) {
        Merchant merchant = merchantMapper.selectByPrimaryKey(id);
        Assert.notNull(merchant, ApiStatusEnum.ERROR_BUS_NO11.getCode(), "商户编号错误");
        merchant.setPassword(calcMerchantPassword(merchant.getPhone(), password));
        return merchantMapper.updateByPrimaryKeySelective(merchant);
    }

    /**
     * 计算商户密码
     *
     * @param phone    手机号
     * @param password 明文密码
     * @return 密文密码
     */
    @Override
    public String calcMerchantPassword(String phone, String password) {
        return MD5Util.md5(MD5Util.md5(password) + phone.substring(7));
    }

    /**
     * 创建商户信息
     *
     * @param merchant
     * @return
     */
    @Override
    public int createMerchant(Merchant merchant) {
        return merchantMapper.insertSelective(merchant);
    }

    /**
     * 根据商户id列表查询商户信息
     *
     * @param idList
     * @return
     */
    @Override
    public List<Merchant> getMerchantListByIdList(List<Long> idList) {
        return merchantMapper.selectBatchIds(idList);
    }

    /**
     * 抖带带 通过openId 获取主播信息
     * @param openId
     * @return
     */
    @Override
    public Merchant getMerchantByOpenId(String openId) {
        QueryWrapper<Merchant> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Merchant::getOpenId, openId)
                .eq(Merchant::getGrantStatus, 1);
        return merchantMapper.selectOne(wrapper);
    }
}
