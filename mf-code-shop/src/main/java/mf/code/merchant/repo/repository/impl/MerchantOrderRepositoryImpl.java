package mf.code.merchant.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.OrderPayTypeEnum;
import mf.code.merchant.constants.OrderStatusEnum;
import mf.code.merchant.constants.OrderTypeEnum;
import mf.code.merchant.repo.dao.MerchantOrderMapper;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.repository.MerchantOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * mf.code.merchant.repo.repository.impl
 * Description:
 *
 * @author gel
 * @date 2019-04-03 21:40
 */
@Service
public class MerchantOrderRepositoryImpl implements MerchantOrderRepository {

    @Value("${seller.purchaseTime}")
    private String merchantPurchaseTime;

    @Autowired
    private MerchantOrderMapper merchantOrderMapper;


    /**
     * 查询是否有订购店铺的订单
     *
     * @param merchantId 商户ID
     * @return 完成订单数量
     */
    @Override
    public Integer countMerchantOrderForPurchaseShop(Long merchantId) {
        QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantOrder::getMerchantId, merchantId)
                .in(MerchantOrder::getBizType, Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_CLEARANCE.getCode(),BizTypeEnum.PURCHASE_WEIGHTING.getCode()))
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .ge(MerchantOrder::getCtime, merchantPurchaseTime);
        return merchantOrderMapper.selectCount(queryWrapper);
    }


    /**
     * 查询订购店铺的订单
     *
     * @param merchantId 商户ID
     * @return 具体订单数据
     */
    @Override
    public MerchantOrder purchaseFirstAppletMerchantOrder(Long merchantId){
        QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantOrder::getMerchantId, merchantId)
                .in(MerchantOrder::getBizType, Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_CLEARANCE.getCode(),BizTypeEnum.PURCHASE_WEIGHTING.getCode()))
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .ge(MerchantOrder::getCtime, merchantPurchaseTime)
                .orderByAsc(MerchantOrder::getCtime)
        ;
        List<MerchantOrder> merchantOrders = this.merchantOrderMapper.selectList(queryWrapper);
        if(CollectionUtils.isEmpty(merchantOrders)){
            return null;
        }
        return merchantOrders.get(0);
    }

    /**
     * 查询是否有订购店铺的订单
     *
     * @param merchantId 商户ID
     * @return 完成订单数量
     */
    @Override
    public Integer countMerchantOrderForPurchaseShopWithoutTime(Long merchantId) {
        QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantOrder::getMerchantId, merchantId)
                .in(MerchantOrder::getBizType, Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_CLEARANCE.getCode(),BizTypeEnum.PURCHASE_WEIGHTING.getCode()))
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode());
        return merchantOrderMapper.selectCount(queryWrapper);
    }

    /**
     * 查询店铺购买的最后一笔订单
     *
     * @param merchantId 商户ID
     * @return 具体订单数据
     */
    @Override
    public List<MerchantOrder> selectMerchantOrderForPurchaseShop(Long merchantId) {
        QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantOrder::getMerchantId, merchantId)
                .in(MerchantOrder::getBizType, Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_CLEARANCE.getCode(),BizTypeEnum.PURCHASE_WEIGHTING.getCode()))
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .ge(MerchantOrder::getCtime, merchantPurchaseTime);
        return merchantOrderMapper.selectList(queryWrapper);
    }

    /**
     * 更新店铺购买订单
     *
     * @param merchantOrder 商户订单
     * @return 条数
     */
    @Override
    public Integer updateMerchantOrderForPurchaseShop(MerchantOrder merchantOrder) {
        if (merchantOrder.getId() == null) {
            return 0;
        }
        return merchantOrderMapper.updateByPrimaryKeySelective(merchantOrder);
    }

}
