package mf.code.merchant.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RedisKeyConstant;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.merchant.dto.*;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.repository.MerchantLoginRepository;
import mf.code.merchant.repo.repository.MerchantOrderRepository;
import mf.code.merchant.service.MerchantLoginService;
import mf.code.shop.common.caller.aliyundayu.DayuCaller;
import mf.code.shop.common.email.EmailService;
import mf.code.shop.repo.repository.ShopRepostory;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author gel
 */
@Slf4j
@Service
public class MerchantLoginServiceImpl implements MerchantLoginService {

    @Autowired
    private MerchantLoginRepository merchantLoginRepository;
    @Autowired
    private MerchantOrderRepository merchantOrderRepository;
    @Autowired
    private ShopRepostory shopRepostory;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DayuCaller dayuCaller;
    @Autowired
    private EmailService emailService;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Value("${seller.smsCode.debug.mode}")
    private Integer smsCodeDebugMode;
    @Value("${seller.uid.name}")
    private String uidName;
    @Value("${seller.token.name}")
    private String tokenName;

    /**
     * 商户后台登录，区别其他登录方式
     * 主要包括：是否订购，是否创建店铺，是否弹窗
     *
     * @param dto 入参
     * @return 登录参数
     */
    @Override
    public MerchantLoginResultDTO loginForBackend(MerchantLoginDTO dto) {

        Assert.isInteger(dto.getPhone(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请填写正确的登录手机号");
        // 查询redis，短信验证码不存在
        Assert.isTrue(StringUtils.isNotBlank(dto.getSmsCode()) || StringUtils.isNotBlank(dto.getPassword()),
                ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请选择正确的登录方式");
        Merchant merchant = null;
        if (StringUtils.isNotBlank(dto.getSmsCode())) {
            merchant = loginForSMSCode(dto.getPhone(), dto.getSmsCode());
        } else if (StringUtils.isNotBlank(dto.getPassword())) {
            merchant = loginForPassword(dto.getPhone(), dto.getPassword());
        }
        Assert.notNull(merchant, ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请先完成注册");
        return getMerchantLoginResult(merchant);
    }

    private MerchantLoginResultDTO getMerchantLoginResult(Merchant merchant) {
        MerchantLoginResultDTO resultDTO = new MerchantLoginResultDTO();
        resultDTO.setMerchantId(merchant.getId());
        resultDTO.setPhone(merchant.getPhone());
        resultDTO.setType(merchant.getType());
        resultDTO.setUsername(merchant.getPhone());
        resultDTO.setName(merchant.getName());
        resultDTO.setCreate(merchant.getCtime().getTime());
        resultDTO.setFirstDialog(0);

        // 商户端登陆埋点 开始
        try {
            log.info("---------------------- 商户端登陆埋点上报 开始");
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.STATISTICS_REPORT_MERCHANT_LOGIN), JSON.toJSONString(resultDTO));
            log.info("---------------------- 商户端登陆埋点上报 结束 消息体={}", JSON.toJSONString(resultDTO));
        } catch (Exception e) {
            log.error("--------------------- 商户端登陆埋点报错 ---------------------");
            e.printStackTrace();
        }
        // 商户端登陆埋点 结束

        String firstLoginDialogRedisKey = RedisKeyConstant.MERCHANT_FIRST_LOGIN_DIALOG + merchant.getId();
        String firstLogin = this.stringRedisTemplate.opsForValue().get(firstLoginDialogRedisKey);
        if (StringUtils.isBlank(firstLogin)) {
            resultDTO.setFirstDialog(1);
            // 设置新手引导30天过期，三十天之后继续弹
            this.stringRedisTemplate.opsForValue().set(firstLoginDialogRedisKey, DateUtil.getNow(), 30L, TimeUnit.DAYS);
        }
        // 查询是够购买过店铺（指第一个）
        Integer orderCount = merchantOrderRepository.countMerchantOrderForPurchaseShopWithoutTime(merchant.getId());
        resultDTO.setIsPurchase(orderCount == 0 ? 0 : 1);
        // 查询是够有尚未创建的店铺（指第一个，包括删除的店铺）
        Integer shopCount = shopRepostory.countByMerchantIdForPurchaseShopWithoutTime(merchant.getId());
        resultDTO.setIsCreateShop(shopCount == 0 ? 0 : 1);
        if (shopCount > 0) {
            resultDTO.setIsPurchase(1);
        }
        return resultDTO;
    }

    /**
     * 商户后台注册，区别其他登录方式
     * 主要包括：是否订购，是否创建店铺，是否弹窗
     *
     * @param dto 入参
     * @return 注册参数
     */
    @Override
    public MerchantLoginResultDTO registerForBackend(MerchantRegisterDTO dto) {
        return null;
    }


    @Override
    public SimpleResponse getSmsVerifyCodeForRegister(SmsReq smsReq, String pictureCode) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 校验 图形验证码，用户会话中的图形验证码 存在&匹配
        if (null == pictureCode) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("图形验证码已过期");
            return simpleResponse;
        }
        if (!StringUtils.equalsIgnoreCase(pictureCode, smsReq.getPicCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("图形验证码不匹配");
            return simpleResponse;
        }
        // redisKey
        String redisKey = RedisKeyConstant.MERCHANT_REGISTER + smsReq.getPhone();
        // 判断 手机号码 是否60秒之内重复发送
        String redisString = stringRedisTemplate.opsForValue().get(redisKey);
        SmsCodeRedis smsRedisRegister = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (smsRedisRegister != null && DateUtil.getCurrentMillis() < Long.valueOf(smsRedisRegister.getNow()) + DateUtil.ONE_MINUTE_MILLIS) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("稍后再次请求短信验证码");
            return simpleResponse;
        }
        // 判断 手机号码 是否已经注册
        Merchant merchantByPhone = merchantLoginRepository.getMerchantByPhone(smsReq.getPhone());
        if (merchantByPhone != null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("已经注册过");
            return simpleResponse;
        }
        // 测试模式：不校验短信验证码
        if (smsCodeDebugMode != null && smsCodeDebugMode == 1) {
            SmsCodeRedis register = new SmsCodeRedis();
            register.setNow(DateUtil.getCurrentMillis().toString());
            register.setSmsCode("111111");
            stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 2, TimeUnit.MINUTES);
            return simpleResponse;
        }
        // 生成短信验证码，6位
        String smsCode = RandomStrUtil.getRandomNum();
        // 短信验证码存入redis，过期时间2分钟key-->mch:reg:<phone> value-->smsCode, now
        SmsCodeRedis register = new SmsCodeRedis();
        register.setNow(DateUtil.getCurrentMillis().toString());
        register.setSmsCode(smsCode);
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 2, TimeUnit.MINUTES);
        // 调用大于短信接口(手机号，短信验证码)
        SendSmsResponse sendSmsResponse = dayuCaller.smsForRegister(smsReq.getPhone(), smsCode);
        // 处理大于短信返回状态
        if (!StringUtils.equalsIgnoreCase("ok", sendSmsResponse.getCode())) {
            log.error("请求阿里大于失败, phone" + smsReq.getPhone());
            // 短信验证码调用失败处理
            emailService.sendSimpleMail("请求阿里大于失败, phone" + smsReq.getPhone(), JSON.toJSONString(sendSmsResponse));
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("短信服务请求失败");
            return simpleResponse;
        }
        return simpleResponse;
    }


    @Override
    public SimpleResponse getSmsVerifyCodeForLogin(SmsReq smsReq, String pictureCode) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 校验图形验证码，用户会话中的图形验证码 存在&匹配
        if (null == pictureCode) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("图形验证码已过期");
            return simpleResponse;
        }
        if (!StringUtils.equalsIgnoreCase(pictureCode, smsReq.getPicCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("图形验证码不匹配");
            return simpleResponse;
        }
        if (!RegexUtils.isMobileExact(smsReq.getPhone())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("手机号输入不正确");
            return simpleResponse;
        }
        // redisKey
        String redisKey = RedisKeyConstant.MERCHANT_LOGIN + smsReq.getPhone();
        // 判断 手机号码 是否60秒之内重复发送
        String redisString = stringRedisTemplate.opsForValue().get(redisKey);
        SmsCodeRedis smsRedisRegister = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (smsRedisRegister != null && DateUtil.getCurrentMillis() < Long.valueOf(smsRedisRegister.getNow()) + DateUtil.ONE_MINUTE_MILLIS) {
            // 当前时间在预定时间之前，返回错误提示
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("稍后再次请求短信验证码");
            return simpleResponse;
        }
        // 判断 手机号码 是否已经注册
        Merchant merchantByPhone = merchantLoginRepository.getMerchantByPhone(smsReq.getPhone());
        if (merchantByPhone == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("请先注册");
            return simpleResponse;
        }
        // 测试模式：不校验短信验证码
        if (smsCodeDebugMode != null && smsCodeDebugMode == 1) {
            SmsCodeRedis register = new SmsCodeRedis();
            register.setNow(DateUtil.getCurrentMillis().toString());
            register.setSmsCode("111111");
            stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 2, TimeUnit.MINUTES);
            return simpleResponse;
        }
        // 生成短信验证码，6位
        String smsCode = RandomStrUtil.getRandomNum();
        // 短信验证码存入redis，过期时间2分钟key-->mch:reg:<phone> value-->smsCode, now
        SmsCodeRedis login = new SmsCodeRedis();
        login.setNow(DateUtil.getCurrentMillis().toString());
        login.setSmsCode(smsCode);
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(login), 2, TimeUnit.MINUTES);
        // 调用大于短信接口(手机号，短信验证码)
        SendSmsResponse sendSmsResponse = dayuCaller.smsForLogin(smsReq.getPhone(), smsCode);
        // 处理大于短信返回状态
        if (!StringUtils.equalsIgnoreCase("ok", sendSmsResponse.getCode())) {
            log.error("请求阿里大于失败, phone" + smsReq.getPhone());
            emailService.sendSimpleMail("请求阿里大于失败, phone" + smsReq.getPhone(), JSON.toJSONString(sendSmsResponse));
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("短信服务请求失败");
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    public MerchantLoginResultDTO register(RegisterReq registerReq, HttpServletRequest request, HttpServletResponse response) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Assert.isTrue(registerReq.getPhone() != null && RegexUtils.isMobileExact(registerReq.getPhone()),
                ApiStatusEnum.ERROR_BUS_NO11.getCode(), "手机输入错误");
        // 查询redis，短信验证码不存在
        Assert.isTrue(registerReq.getSmsCode() != null && StringUtils.isNotBlank(registerReq.getSmsCode()),
                ApiStatusEnum.ERROR_BUS_NO12.getCode(), "短信验证码无效");
        String redisString = stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANT_REGISTER + registerReq.getPhone());
        Assert.isBlank(redisString, ApiStatusEnum.ERROR_BUS_NO13.getCode(), "短信验证码无效");
        // 校验商户是否注册过
        Merchant merchant1 = merchantLoginRepository.getMerchantByPhone(registerReq.getPhone());
        Assert.isTrue(merchant1 == null, ApiStatusEnum.ERROR_BUS_NO14.getCode(), "商户已注册过");
        // 校验用户输入短信验证码与redis存储不符
        SmsCodeRedis smsRegisterRedis = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        Assert.isTrue(StringUtils.equalsIgnoreCase(registerReq.getSmsCode(), smsRegisterRedis.getSmsCode()),
                ApiStatusEnum.ERROR_BUS_NO5.getCode(), "短信验证码输入有误");
        // 创建 商户
        // 密码 手机号码后4位+一次MD5结果进行二次MD5
        Date now = new Date(System.currentTimeMillis() / 1000 * 1000);
        log.info(registerReq.getPhone() + "商户注册时间：" + now.getTime());
        Merchant merchant = new Merchant();
        merchant.setPhone(registerReq.getPhone());
        // 第三方接口调用状态：0: 创建未调用 1：调用成功 -1：创建调用失败 -2：欠费调用失败
        merchant.setStatus(0);
        merchant.setCtime(now);
        merchant.setUtime(now);
        int size = merchantLoginRepository.createMerchant(merchant);
        Assert.isTrue(size != 0, ApiStatusEnum.ERROR_BUS_NO16.getCode(), "创建帐号失败");
        request.getSession().setAttribute(uidName, merchant.getId());
        request.getSession().setMaxInactiveInterval(8 * 60 * 60);
        // 生成token,存入cookie
        merchant = merchantLoginRepository.getMerchantById(merchant.getId());
        String token = TokenUtil.encryptToken(merchant.getId().toString(), merchant.getPhone(), merchant.getCtime(), TokenUtil.SAAS);
        Cookie cookie = new Cookie(tokenName, token);
        cookie.setMaxAge(8 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);
        //redis相关存储
        String firstLoginDialogRedisKey = RedisKeyConstant.MERCHANT_FIRSTLOGIN_DIALOG + merchant.getId();
        this.stringRedisTemplate.opsForValue().set(firstLoginDialogRedisKey, "1", 30, TimeUnit.DAYS);
        return getMerchantLoginResult(merchant);
    }


    /**
     * 商户密码登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return 商户基本信息
     */
    private Merchant loginForPassword(String phone, String password) {
        // 校验用户密码是否匹配
        Merchant merchant = merchantLoginRepository.getMerchantByPhone(phone);
        Assert.notNull(merchant, ApiStatusEnum.ERROR_BUS_NO12.getCode(), "请先完成注册");
        // 因为前段已经做过明文md5加密所以，此处只需要做进一步加密就好
        String md5Client = MD5Util.md5(password + phone.substring(7));
        Assert.isTrue(StringUtils.equals(md5Client, merchant.getPassword()), ApiStatusEnum.ERROR_BUS_NO13.getCode(), "密码错误！");
        return merchant;
    }

    /**
     * 商户短信验证码登录
     *
     * @param phone   手机号
     * @param smsCode 短信验证码
     * @return 商户基本信息AppletGoodsServiceImpl
     */
    private Merchant loginForSMSCode(String phone, String smsCode) {
        String redisString = stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANT_LOGIN + phone);
        SmsCodeRedis smsRegisterRedis = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        Assert.notNull(smsRegisterRedis, ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请稍后重试！");
        // 校验用户输入短信验证码与redis存储不符
        Assert.isTrue(StringUtils.equalsIgnoreCase(smsCode, smsRegisterRedis.getSmsCode()), ApiStatusEnum.ERROR_BUS_NO5.getCode(), "请稍后重试！");
        // 20181217 如果没有密码则添加初始密码
        Merchant merchant = merchantLoginRepository.getMerchantByPhone(phone);
        Assert.notNull(merchant, ApiStatusEnum.ERROR_BUS_NO6.getCode(), "请先完成注册");
        if (StringUtils.isBlank(merchant.getPassword())) {
            log.info(">>>>>>>>>>>>>>>>>商户密码设置,phone:" + phone + ",密码：" + smsRegisterRedis.getSmsCode());

            Integer count = merchantLoginRepository.updateMerchantPassword(merchant.getId(), smsRegisterRedis.getSmsCode());
            if (count == 0) {
                log.error(">>>>>>>>>>>>>>>>>商户密码设置失败,phone:" + phone + ",密码：" + smsRegisterRedis.getSmsCode());
            } else {
                // 发送初始化密码通知
                dayuCaller.smsForInitPassword(phone, smsRegisterRedis.getSmsCode());
            }
        }
        return merchant;
    }
}
