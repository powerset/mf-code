package mf.base.turbine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@SpringBootApplication
@EnableEurekaClient
@EnableHystrixDashboard
@EnableTurbine
@Slf4j
public class MfBaseTurbineApplication {

    public static void main(String[] args) {
        SpringApplication.run(MfBaseTurbineApplication.class, args);
        log.info("ELK启动日志标识:MF-CODE-TURBINE");
    }
}
