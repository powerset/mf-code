package mf.code.one;

import com.alibaba.fastjson.JSON;
import mf.code.api.applet.v10.service.AppletPlatformOrderBackService;
import mf.code.common.service.CommonConfService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.ApolloLHYXDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OneServiceTest {

    @Autowired
    private CommonConfService commonConfService;

    @Autowired
    private AppletPlatformOrderBackService appletPlatformOrderBackService;

    @Test
    public void getUnionConfTest(){
        ApolloLHYXDTO apolloLHYXDTO = commonConfService.getUnionConf();
        System.out.println(apolloLHYXDTO);
    }

    @Test
    public void unionPageSeckillTest(){
        SimpleResponse simpleResponse = appletPlatformOrderBackService.unionPageSeckill(36L, 1L, "", null);
                System.out.println(JSON.toJSONString(simpleResponse));
    }

    @Test
    public void sellUnionTest(){
        SimpleResponse simpleResponse = appletPlatformOrderBackService.sellUnion(1L, 10L, 144L, 8L);
        System.out.println(JSON.toJSONString(simpleResponse));
    }
}
