package mf.code.api.msactivity;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v10.AppletPlatformMsActivityApi;
import mf.code.common.simpleresp.SimpleResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.common.caller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月06日 19:51
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MsActivityTest {
    @Autowired
    private AppletPlatformMsActivityApi appletPlatformMsActivityApi;

    /***
     * 获取秒杀活动的时间段轴
     */
    @Test
    public void getMsActivityTime() {
        SimpleResponse simpleResponse = appletPlatformMsActivityApi.getMsActivityTime(36L, null, null, 139L);
        System.out.println(JSONObject.toJSONString(simpleResponse));
    }

    /***
     * 获取秒杀活动某个时间段 商品详情信息
     */
    @Test
    public void getMsActivityOneBatch() {
        SimpleResponse simpleResponse = appletPlatformMsActivityApi.getMsActivityOneBatch(36L, 139L, null, null, 0, 6);
        System.out.println(JSONObject.toJSONString(simpleResponse));
    }
}
