package mf.code.api.applet;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * mf.code.api.applet
 * Description:
 *
 * @author gel
 * @date 2019-07-19 15:54
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AppletUnionMarketingCouponApiTest {

    @Autowired
    private AppletUnionMarketingCouponApi appletUnionMarketingCouponApi;

    @Test
    public void queryCouponsByTypeAndStatus() {
        appletUnionMarketingCouponApi.queryCouponsByTypeAndStatus(29, 1, 325L, 117L, 0, 10);
    }

    @Test
    public void queryJKMFCouponByStatusForOrder() {
//        appletUnionMarketingCouponApi.queryJKMFCouponByStatusForOrder(1, 36L, 110L, 335L, 0, 10);
    }

    @Test
    public void queryCanApplyCoupon() {
//        appletUnionMarketingCouponApi.queryCanApplyCoupon(117L, 325L, 814L, BigDecimal.ONE);
    }
}
