package mf.code.api.applet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AppletUserActivityV2ServiceTest {

    @Autowired
    GoodsService goodsService;
    @Test
    public void getGoodsInfo(){
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "display_price", "xxbtop_price", "pic_url", "display_goods_name", "xxbtop_title");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add("566518017914");
        queryWrapper.in("xxbtop_num_iid", jsonArray);
        List<Goods> goodsList = goodsService.list(queryWrapper);
        System.out.println(JSON.toJSONString(goodsList));
    }
}
