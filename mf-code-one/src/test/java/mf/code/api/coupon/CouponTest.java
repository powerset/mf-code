package mf.code.api.coupon;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CouponTest {
    @Test
    public void getCheckedGood(){
        ProductEntity productEntity = new ProductEntity();
        productEntity.setCheckReason("aaa");
        Map<String ,Object> map = new HashMap<>();
        map.put("chekcProductInfo",productEntity);
         new SimpleResponse(ApiStatusEnum.SUCCESS,map);

         System.out.println(JSON.toJSON(new SimpleResponse(ApiStatusEnum.SUCCESS,map)));
    }


    @Test
    public void getCheckedGoods(){
        ProductEntity productEntity = new ProductEntity();
        productEntity.setCheckReason("aaa");
        Map<String ,Object> map = new HashMap<>();
        map.put("chekcProductInfo",productEntity);
        map.put("productList",new ArrayList<>());
        map.put("count",0);
        map.put("page",1);
        map.put("size",1);
        new SimpleResponse(ApiStatusEnum.SUCCESS,map);

        System.out.println(JSON.toJSON(new SimpleResponse(ApiStatusEnum.SUCCESS,map)));
    }

}
