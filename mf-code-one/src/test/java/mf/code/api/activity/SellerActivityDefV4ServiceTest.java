package mf.code.api.activity;

import com.alibaba.fastjson.JSON;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.applet.v3.service.CheckpointTaskSpaceService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.service.RecallV3Service;
import mf.code.api.seller.service.SellerGoodsV2Service;
import mf.code.api.seller.service.SellerWxmpService;
import mf.code.api.seller.v4.service.SellerActivityDefV4Service;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.ucoupon.repo.po.UserCoupon;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * mf.code.api.activity
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-19 22:31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SellerActivityDefV4ServiceTest {

    @Autowired
    private SellerActivityDefV4Service sellerActivityDefV4Service;

    @Autowired
    private ActivityDefService activityDefService;

    @Autowired
    private ActivityDefCheckService activityDefCheckService;

    @Autowired
    private SellerWxmpService sellerWxmpService;

    @Autowired
    private CheckpointTaskSpaceService checkpointTaskSpaceService;

    @Autowired
    private SellerGoodsV2Service sellerGoodsV2Service;

    /**
     * 测试新手任务创建
     */
    @Test
    public void createNewBieDefTest() {
        CreateDefReq req = new CreateDefReq();
//        req.setMerchantId(8L);
//        req.setShopId(8L);
//        req.setStockDef(1);
//        req.setType(ActivityDefTypeEnum.NEWBIE_TASK.getCode());
//        Map<String, Object> extraData = new HashMap<>();
//        extraData.put("readAmount", "1");
//        extraData.put("serviceWx", "[{\"wechat\":\"\",\"pic\":\"https://asset.wxyundian.com/data-test3/activity/cspic/wechat/WechatIMG9.jpeg\"}]");
//        extraData.put("keywords", "测试");
//        extraData.put("scanAmount", "1");
//        extraData.put("openRedPackactiveId", 1140);
//        req.setExtraData(JSON.toJSONString(extraData));
        req.setAmount("1");
        req.setType(24);
        req.setShopId(110L);
        req.setMerchantId(150L);
        req.setStockDef(3);
        Map<String, Object> extraData = new HashMap<>();
        extraData.put("condDrawTime", 1);
        extraData.put("hitsPerDraw", 1);
        extraData.put("maxReimbursement", 2);
        extraData.put("maxAmount", 1);
        req.setExtraData(JSON.toJSONString(extraData));

        SimpleResponse response = sellerActivityDefV4Service.createDef(req);
        System.out.println(response);
    }

    /**
     * 测试获取保证金
     */
    @Test
    public void getDepositDefTest() {
        CreateDefReq req = new CreateDefReq();
//        req.setActivityDefId(1214L);
        Map<String, Object> extraData = new HashMap<>();
        extraData.put("setWechatAmount", 1);
        extraData.put("inviteAmount", 1);
        extraData.put("developAmount", 1);
        req.setExtraData(JSON.toJSONString(extraData));
        req.setShopId(144L);
        req.setMerchantId(8L);
        req.setStockDef(1);
        req.setType(20);

        SimpleResponse response = sellerActivityDefV4Service.getDepositDef(req);
        System.out.println(response);
    }

    /**
     * 测试补库存
     */
    @Test
    public void supplyAddStockTest() {
        CreateDefReq req = new CreateDefReq();
        req.setActivityDefId(1514L);
        req.setDepositDef("2");
        req.setMerchantId(150L);
        req.setStockDef(1);

        SimpleResponse simpleResponse = sellerActivityDefV4Service.supplyAddStock(req);
        System.out.println(simpleResponse);
    }

    /**
     * 测试查询
     */
    @Test
    public void queryDefTest() {
        SimpleResponse simpleResponse = sellerActivityDefV4Service.queryDef(1505L);
        System.out.println(simpleResponse);
    }

    /**
     * 测试插入
     */
    @Test
    public void testInsert() {
        ActivityDef activityDef = new ActivityDef();
        activityDef.setShopId(8L);
        activityDef.setMerchantId(8L);
        activityDef.setType(ActivityDefTypeEnum.NEWBIE_TASK.getCode());
        activityDef.setStockDef(10);
        activityDef.setStock(10);
        activityDef.setDepositDef(BigDecimal.TEN);
        activityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());

        activityDefService.createActivityDef(activityDef);

        System.out.println(activityDef);
    }

    /**
     * 测试店长任务创建
     */
    @Test
    public void createShopManagerDefTest() {
        CreateDefReq req = new CreateDefReq();
        req.setMerchantId(8L);
        req.setShopId(8L);
        req.setStockDef(1);
        req.setType(ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode());
        Map<String, Object> extraData = new HashMap<>();
        extraData.put("setWechatAmount", "1");
        extraData.put("inviteCount", 1);
        extraData.put("inviteAmount", "1");
        extraData.put("developCount", 1);
        extraData.put("developAmount", "1");
        req.setExtraData(JSON.toJSONString(extraData));

        SimpleResponse response = sellerActivityDefV4Service.createDef(req);
        System.out.println(response);
    }

    /**
     * 查询用户是否有拆红包活动测试
     */
    @Test
    public void getOpenRedPackActivityInfoTest() {
        SimpleResponse simpleResponse = sellerActivityDefV4Service.getOpenRedPackActivityInfo(8L, 150L);
        System.out.println(simpleResponse);
    }

    /**
     * 查询参与用户详情测试
     */
    @Test
    public void applyDetailTest() {
        SimpleResponse simpleResponse = this.sellerActivityDefV4Service.applyDetail(8L, 1394L, 10, 1);
        System.out.println(simpleResponse);
    }

    /**
     * 分页查询营销活动测试
     */
    @Test
    public void queryDefByPageTest() {
        SimpleResponse simpleResponse = this.sellerActivityDefV4Service.queryDefByPage(8L, 8L, null, 17, 10, 1);
        System.out.println(simpleResponse);
    }

    @Test
    public void createAbleTest(){
        SimpleResponse simpleResponse = activityDefCheckService.createAble(8L, 8L,  17,  null, null, null);
        System.out.println(simpleResponse);
    }

    @Test
    public void createSceneWxmpTest(){
        SimpleResponse simpleResponse = this.sellerWxmpService.createSceneWxmp(150L, 110L, 2, 10, 0l);
        System.out.println(simpleResponse);
    }

    @Test
    public void getItemListTest(){
        SimpleResponse simpleResponse = this.checkpointTaskSpaceService.getItemList(150L, 110L);
        System.out.println(simpleResponse);
    }

    @Test
    public void pageGoodsSkuTest(){
        Map<String, Object> params = new HashMap<>(10);
        params.put("shopId", 139);
        params.put("onsale", 1);
        SimpleResponse simpleResponse = sellerGoodsV2Service.pageGoodsSku(params, "1", "10");
        System.out.println(simpleResponse);
    }

    @Test
    public void queryActivityDefUserJoinByPageTest(){
        SimpleResponse response = this.sellerActivityDefV4Service.queryActivityDefUserJoinByPage(150L, 122L, 1697L, 24, 10, 1);
        System.out.println(response);
    }
}
