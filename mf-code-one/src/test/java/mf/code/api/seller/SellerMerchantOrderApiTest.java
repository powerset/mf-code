package mf.code.api.seller;

import mf.code.api.seller.service.SellerMerchantOrderService;
import mf.code.common.simpleresp.SimpleResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.api.seller.service.summary
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-25 11:40
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SellerMerchantOrderApiTest {

    @Autowired
    private SellerMerchantOrderService sellerMerchantOrderService;

    @Test
    public void queryBalanceOfClearingProgressInfoTest(){
        SimpleResponse simpleResponse = this.sellerMerchantOrderService.queryBalanceOfClearingProgressInfo(8L, 139L, 1295L);
        System.out.println(simpleResponse);
    }
}
