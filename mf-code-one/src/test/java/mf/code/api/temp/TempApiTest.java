package mf.code.api.temp;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.easyexcel.EasyExcelUtil;
import mf.code.common.easyexcel.FieldTypeAnnotation;
import mf.code.common.utils.ConvertUtil;
import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.common.caller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月06日 19:51
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TempApiTest {
    @Autowired
    private OssCaller ossCaller;

    /***
     * excel导出到oss
     */
    @Test
    public void excelTest() {
        List<ManTest> dataList = new ArrayList();
        ManTest man1 = new ManTest("张三", 20, "男", (float) 10000.8);
        ManTest man2 = new ManTest("李四", 21, "男", (float) 11000.8);
        ManTest man3 = new ManTest("王五", 22, "女", (float) 1200.8);
        ManTest man4 = new ManTest("赵六", 23, "男", (float) 13000.8);
        ManTest man5 = new ManTest("田七", 24, "男", (float) 14000.8);
        ManTest man6 = new ManTest();
        man6.setName("老八");
        dataList.add(man1);
        dataList.add(man2);
        dataList.add(man3);
        dataList.add(man4);
        dataList.add(man5);
        dataList.add(man6);
        InputStream inputStream = EasyExcelUtil.exportInputStream(dataList, ManTest.class);
        FieldTypeAnnotation fieldTypeAnnotation = dataList.get(0).getClass().getAnnotation(FieldTypeAnnotation.class);
        // 进行转码，使其支持中文文件名
        String fileName = ConvertUtil.toUtf8String(fieldTypeAnnotation.name() + DateFormatUtils.format(new Date(), "yyyyMMdd"));
        String bucket = fileName + ".xlsx";
        String ossUrl = this.ossCaller.uploadObject2OSSInputstream(inputStream, bucket);
        log.info("<<<<<<<< ossUrl:{}", ossUrl);
    }

    @Data
    @FieldTypeAnnotation(name = "test")
    public static class ManTest extends BaseRowModel {
        @ExcelProperty(value = "名称", index = 0)
        private String name;
        @ExcelProperty(value = "性别", index = 1)
        private int sex;
        @ExcelProperty(value = "年龄", index = 2)
        private String idCard;
        @ExcelProperty(value = "钞票", index = 3)
        private float salary;
        @ExcelProperty(value = "时间", index = 3)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date date;

        public ManTest() {
            super();
        }

        public ManTest(String name, int sex, String idCard, float salary) {
            super();
            this.name = name;
            this.sex = sex;
            this.idCard = idCard;
            this.salary = salary;
            this.date = new Date();
        }
    }
}
