package mf.code.api.async;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.v3.service.AppletUserActivityUV3Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.api.async
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月09日 17:22
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AsyncApiTest {
    @Autowired
    private AppletUserActivityUV3Service appletUserActivityUV3Service;


    @Test
    public void fillorder(){
        BackFillOrderReq req = new BackFillOrderReq();
        req.setShopId("110");
        req.setGoodsId("9081");
        req.setOrderId("413082561316945956");
        req.setUserId("36");

        appletUserActivityUV3Service.backfillOrderAsync(req);

    }
}
