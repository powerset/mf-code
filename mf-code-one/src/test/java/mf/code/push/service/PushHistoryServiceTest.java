package mf.code.push.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * mf.code.push.service
 * Description:
 *
 * @author: gel
 * @date: 2019-03-07 14:53
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PushHistoryServiceTest {

    /**
     * 使用RocketMq的生产者
     */
    @Autowired
    private PushHistoryService pushHistoryService;

    @Test
    public void countPushHistoryTodayByShopId() {
        System.out.println(pushHistoryService.countPushHistoryTodayByShopId(52L));
    }
}
