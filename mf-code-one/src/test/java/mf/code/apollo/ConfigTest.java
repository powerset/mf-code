package mf.code.apollo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigFile;
import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.core.enums.ConfigFileFormat;
import com.ctrip.framework.apollo.internals.YmlConfigFile;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfig;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import mf.code.common.easyexcel.EasyExcelUtil;
import mf.code.common.easyexcel.FieldTypeAnnotation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.yaml.snakeyaml.Yaml;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * mf.code.common.caller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月06日 19:51
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ConfigTest {
    @Autowired
    private ApolloPropertyService apolloPropertyService;

    @Test
    public void getApolloConfig() {
        YmlConfigFile configFile = (YmlConfigFile) ConfigService.getConfigFile("application-apollo", ConfigFileFormat.YML);
        Yaml yaml = new Yaml();
        ApolloLhyxProperty load = yaml.loadAs(configFile.getContent(), ApolloLhyxProperty.class);
        System.out.println(JSON.toJSONString(load));
    }

    @Test
    public void getApolloProperty() {
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        System.out.println(apolloLhyxProperty.getLhyx().getActivity().getStartTime());
        System.out.println(JSONObject.toJSONString(apolloLhyxProperty));
    }
}
