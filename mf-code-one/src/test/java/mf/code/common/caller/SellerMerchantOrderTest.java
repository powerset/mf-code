package mf.code.common.caller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.dto.MerchantOrderMktTransferReq;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.api.seller.service.SellerMerchantOrderService;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RandomStrUtil;
import mf.code.merchant.constants.BizTypeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.common.caller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月19日 17:24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SellerMerchantOrderTest {
    @Autowired
    private SellerMerchantOrderService sellerMerchantOrderService;

    /***
     * 扫码付款
     */
    @Test
    public void createWxOrder(){
        MerchantOrderReq req = new MerchantOrderReq();
        req.setMerchantId(2L);
        req.setShopId(50L);
        req.setAmount("0.01");
        req.setTradeType(1);
        req.setActivityAdId(1L);
        req.setPayType(MerchantOrderReq.payment_wx);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        SimpleResponse simpleResponse = this.sellerMerchantOrderService.creatMerchantOrder(req, request);
        log.info("创建返回：{}",simpleResponse.getCode());
    }

    /***
     * 扫码付款-粉多多-购买
     */
    @Test
    public void createWxOrderFenduoduo(){
        MerchantOrderReq req = new MerchantOrderReq();
        req.setMerchantId(2L);
        req.setAmount("6800");
        req.setTradeType(1);
        req.setPayType(MerchantOrderReq.payment_wx);
        req.setBizType(BizTypeEnum.FENDUODUO_PROXY.getCode());
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        SimpleResponse simpleResponse = this.sellerMerchantOrderService.creatMerchantOrder(req, request);
        log.info("创建返回：{}",simpleResponse.getCode());
    }

    /***
     * 子账号预存金付款-本身
     */
    @Test
    public void createPrestoreOrderOne(){
        MerchantOrderReq req = new MerchantOrderReq();
        req.setMerchantId(2L);
        req.setAmount("0.01");
        req.setActivityAdId(1L);
        req.setPayType(MerchantOrderReq.payment_prestore);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        SimpleResponse simpleResponse = this.sellerMerchantOrderService.creatMerchantOrder(req, request);
        log.info("创建返回：{}", JSONObject.toJSONString(simpleResponse));
    }

    /***
     * 子账号预存金付款-多个账号
     */
    @Test
    public void createPrestoreOrderMany(){
        MerchantOrderReq req = new MerchantOrderReq();
        req.setMerchantId(2L);
        req.setShopId(35L);
        req.setCustomerId(1L);
        req.setAmount("10");
        req.setActivityAdId(1L);
        req.setPayType(MerchantOrderReq.payment_prestore);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        SimpleResponse simpleResponse = this.sellerMerchantOrderService.creatMerchantOrder(req, request);
        log.info("创建返回：{}", JSONObject.toJSONString(simpleResponse));
    }

    @Test
    public void refundWxMerchantOrder(){
        Long orderId = 1L;
        BigDecimal refundFee = new BigDecimal("0.01");
        this.sellerMerchantOrderService.merchantRefund(orderId, refundFee);
    }

    @Autowired
    private WeixinPayService weixinPayService;
    @Test
    public void commitRefund(){
        String refundOrderNo =  "V2" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        String orderNo= "V2jWGxmB1546052875197";
        BigDecimal amount = new BigDecimal("10");
        Map<String, Object> param = weixinPayService.getRefundPayOrderReqParams(orderNo, refundOrderNo,
                amount, amount, WeixinPayConstants.SELLER_NOTIFY_SOURCE);
        Map<String, Object> result = weixinPayService.refundPayOrder(param);
        log.info("<<<<<<<<<< refund resp: {} req: {}", result, param);
    }

    /***
     * 提现
     */
    @Test
    public void tx(){
        MerchantOrderMktTransferReq req = new MerchantOrderMktTransferReq();
        req.setMerchantId(8L);
        req.setCustomerId(8L);
        req.setAmount("2");
        req.setShopId(36L);
        this.sellerMerchantOrderService.createMktTransfersMerchantOrder(req, null);
    }
}
