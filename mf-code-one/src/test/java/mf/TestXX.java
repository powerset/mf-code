package mf;

import lombok.extern.slf4j.Slf4j;
import mf.code.MfCodeOneApplication;
import mf.code.api.feignservice.UnionMarketingApolloDataServiceImpl;
import mf.code.api.seller.service.FansV3Service;
import mf.code.common.utils.RequestSignUtil;
import mf.code.summary.constant.ActivityTypeMapping;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MfCodeOneApplication.class)
@Slf4j
public class TestXX {


   @Autowired
   FansV3Service fansV3Service;
   @Autowired
   StringRedisTemplate stringRedisTemplate;

   @Autowired
   private UnionMarketingApolloDataServiceImpl unionMarketingApolloDataService;
    @Test
    public void queryOrder() {
        String skuIds = unionMarketingApolloDataService.getSkuIds();
        System.out.println(skuIds);
    }

}
