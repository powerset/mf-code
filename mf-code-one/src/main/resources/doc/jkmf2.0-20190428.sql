alter table `activity` add `stock_type` int(11) default '1' COMMENT '扣库存类型：1：发起活动扣除库存； 2：完成活动扣除库存' AFTER `type`;

alter table `common_dict` modify column `value` text;
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (30, 'shoppingMall', 'userRebatePolicy', '[{\"amountCondition\":0,\"rebateRate\":10,\"commissionRate\":60,\"scale\":5},{\"amountCondition\":100,\"rebateRate\":50,\"commissionRate\":20,\"scale\":5}]', '', '', '', 0, '商城配置个人返利和上级分佣', 999, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (31, 'shopCS', 'commissionStandard_1', '{\"beginTime\":\"2019-3-28\",\"condition\":50.00}', '', '', NULL, 0, '商城配置个人获得佣金的条件', 999, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (33, 'logistics', 'list', '', '[{\"id\":1,\"code\":\"yuantong\",\"name\":\"圆通速递\"},{\"id\":2,\"code\":\"shentong\",\"name\":\"申通快递\"},{\"id\":3,\"code\":\"zhongtong\",\"name\":\"中通快递\"},{\"id\":4,\"code\":\"huitongkuaidi\",\"name\":\"百世快递\"},{\"id\":5,\"code\":\"yunda\",\"name\":\"韵达快递\"},{\"id\":6,\"code\":\"shunfeng\",\"name\":\"顺丰速运\"},{\"id\":7,\"code\":\"tiantian\",\"name\":\"天天快递\"},{\"id\":8,\"code\":\"youzhengguonei\",\"name\":\"邮政快递包裹\"},{\"id\":9,\"code\":\"yzguonei\",\"name\":\"邮政国内\"},{\"id\":10,\"code\":\"youzhengbk\",\"name\":\"邮政标准快递\"},{\"id\":11,\"code\":\"zhaijisong\",\"name\":\"宅急送\"},{\"id\":12,\"code\":\"debangkuaidi\",\"name\":\"德邦快递\"},{\"id\":13,\"code\":\"kuayue\",\"name\":\"跨越速运\"},{\"id\":14,\"code\":\"baishiwuliu\",\"name\":\"百世快运\"},{\"id\":15,\"code\":\"annengwuliu\",\"name\":\"安能快运\"},{\"id\":16,\"code\":\"ane66\",\"name\":\"安能快递\"},{\"id\":17,\"code\":\"qita\",\"name\":\"其它\"}]', '', '', 0, '快递配置', 999, '2019-04-01 01:00:00', '2019-04-01 01:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (34, 'shopCS', 'commissionStandard_2', '{\"beginTime\":\"2019-4-10\",\"condition\":100.00}', '', '', NULL, 0, '', 999, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (35, 'product_order', 'refund_reason', '与卖家协商一致,买错/多买/不想要,未收到货,破损/污渍/有瑕疵,7天无理由退,其他', '', '', NULL, 0, '', 999, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (36, 'platformCs', 'customService', '{\"csHotline\":\"13296762731\",\"csList\":[{\"pic\":\"https://asset.wxyundian.com/data/seller/cspic/custom1.jpg\",\"wechat\":\"客服一号\"},{\"pic\":\"https://asset.wxyundian.com/data/seller/cspic/custom2.jpg\",\"wechat\":\"客服二号\"}]}', '', '', NULL, 0, '运营客服列表', 999, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (37, 'platformCs', 'starService', '{\"starList\":[{\"avatarUrl\":\"https://asset.wxyundian.com/data/seller/star/star1.png\",\"nickname\":\"周瑜\",\"title\":\"军团领袖\",\"mMum\":\"147233\"},{\"avatarUrl\":\"https://asset.wxyundian.com/data/seller/star/star2.png\",\"nickname\":\"简姆士\",\"title\":\"军团领袖\",\"mMum\":\"122697\"},{\"avatarUrl\":\"https://asset.wxyundian.com/data/seller/star/star3.png\",\"nickname\":\"不灭\",\"title\":\"军团领袖\",\"mMum\":\"91769\"},{\"avatarUrl\":\"https://asset.wxyundian.com/data/seller/star/star4.png\",\"nickname\":\"索隆\",\"title\":\"军团领袖\",\"mMum\":\"83248\"},{\"avatarUrl\":\"https://asset.wxyundian.com/data/seller/star/star5.png\",\"nickname\":\"云儿\",\"title\":\"军团领袖\",\"mMum\":\"51566\"}]}', '', '', NULL, 0, '明星讲师列表', 999, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (38, 'platformCs', 'jkmfName', '集客魔方', '', '', NULL, 0, '小程序店铺版本信息', 999, '2019-04-01 01:00:00', '2019-04-01 01:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (39, 'platformCs', 'jkmfVersionNum', '1.3.3', '', '', NULL, 0, '', 999, '2019-04-01 01:00:00', '2019-04-01 01:00:00');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (40, 'platformCs', 'jkmfVersionText', '', '', '', NULL, 0, '', 999, '2019-04-01 01:00:00', '2019-04-01 01:00:00');

alter table `merchant_proxy_server` modify column `pay_fee` decimal(10,2);

alter table `merchant_shop` add `openStatus` tinyint(4) DEFAULT '0'  COMMENT '开店审核' AFTER `category_id`;
alter table `merchant_shop` add `openReason` varchar(128) DEFAULT '' COMMENT '开店审核审核原因' AFTER `openStatus`;
alter table `merchant_shop` add `distStatus` tinyint(4) DEFAULT '0' COMMENT '分销资格审核' AFTER `openReason`;
alter table `merchant_shop` add `distReason` varchar(128) DEFAULT '' COMMENT '分销资格审核原因' AFTER `distStatus`;
alter table `merchant_shop` add `username` varchar(128) DEFAULT '' COMMENT '店长姓名' AFTER `phone`;
alter table `merchant_shop` add `address_code` varchar(128) DEFAULT '' COMMENT '省市区' AFTER `pic_path`;
alter table `merchant_shop` add `address_text` varchar(128) DEFAULT '' COMMENT '省市区' AFTER `address_code`;
alter table `merchant_shop` add `address_detail` varchar(128) DEFAULT '' COMMENT '地址详情' AFTER `address_text`;

alter table `upay_balance` add `shopping_balance` decimal(11,2) DEFAULT '0.00' COMMENT '商城购物，返利分佣收益余额' AFTER `balance`;
alter table `upay_balance` add `settled_time` datetime DEFAULT NULL COMMENT '返利佣金结算时间' AFTER `shopping_balance`;

alter table `user` add `cash_protocal` int(11) DEFAULT '0' COMMENT '提现协议 0 未签署，1签署' BEFORE `ctime`;

alter table `user_task` modify column `activity_def_id`  bigint(20);
