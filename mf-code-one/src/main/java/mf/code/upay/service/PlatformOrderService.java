package mf.code.upay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.upay.repo.po.PlatformOrder;

/**
 * mf.code.upay.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月02日 10:14
 */
public interface PlatformOrderService extends IService<PlatformOrder> {
}
