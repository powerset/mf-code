package mf.code.upay.service;

import mf.code.upay.repo.po.UpayBalance;

import java.math.BigDecimal;

/**
 * @Auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月24日 10:26
 * @Description:
 */
public interface UpayBalanceService {

    UpayBalance create(long merchantID, long shopID, long userID, BigDecimal balance);

    int insertSelective(UpayBalance upayBalance);

    UpayBalance updateOrCreate(long merchantID, long shopID, long userID, BigDecimal balance);

    UpayBalance query(long merchantID, long shopID, long userID);

    UpayBalance update(UpayBalance upayBalance);

    Integer updateBalanceByAmount(Long merchantId, Long shopId, Long userId, BigDecimal amount);

    int finishAddUserOpenRedPackBalance(Long merchantId, Long shopId, Long userId, BigDecimal amount);

    BigDecimal canCashMoney(Long merchantId, Long shopId, Long userId);
}
