package mf.code.upay.service.impl;

import com.alibaba.excel.util.CollectionUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.common.constant.DelEnum;
import mf.code.upay.repo.dao.OrderMapper;
import mf.code.upay.repo.dao.ProductMapper;
import mf.code.upay.repo.dao.ProductSkuMapper;
import mf.code.upay.repo.po.Order;
import mf.code.upay.repo.po.Product;
import mf.code.upay.repo.po.ProductSku;
import mf.code.upay.service.OrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.upay.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月17日 10:35
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductSkuMapper productSkuMapper;
    @Autowired
    private UserService userService;

    @Override
    public int countByStatusByShopUser(Long shopId, Long userId, List<Integer> oderTypes, List<Integer> orderStatus) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Order::getShopId, shopId)
                .eq(Order::getUserId, userId)
                .in(Order::getType, oderTypes)
                .eq(Order::getDel, DelEnum.NO.getCode())
        ;
        if (!CollectionUtils.isEmpty(orderStatus)) {
            wrapper.and(params -> params.in("status", orderStatus));
        }
        return this.orderMapper.selectCount(wrapper);
    }

    @Override
    public Map<Long, Map> getUpayOrderProductInfo(List<Long> productOrderIds) {
        if (CollectionUtils.isEmpty(productOrderIds)) {
            return new HashMap<>();
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Order::getId, productOrderIds)
        ;
        List<Order> orders = this.orderMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(orders)) {
            return new HashMap<>();
        }

        List<Long> skuIds = new ArrayList<>();
        List<Long> productIds = new ArrayList<>();
        for (Order order : orders) {
            if (skuIds.indexOf(order.getSkuId()) == -1) {
                skuIds.add(order.getSkuId());
            }
            if (productIds.indexOf(order.getGoodsId()) == -1) {
                productIds.add(order.getGoodsId());
            }
        }

        if (CollectionUtils.isEmpty(skuIds) || CollectionUtils.isEmpty(productIds)) {
            return new HashMap<>();
        }
        QueryWrapper<ProductSku> productSkuQueryWrapper = new QueryWrapper<>();
        productSkuQueryWrapper.lambda()
                .in(ProductSku::getId, skuIds);
        List<ProductSku> skus = this.productSkuMapper.selectList(productSkuQueryWrapper);
        if (CollectionUtils.isEmpty(skus)) {
            return new HashMap<>();
        }
        Map<Long, ProductSku> productSkuMap = new HashMap<>();
        for (ProductSku productSku : skus) {
            productSkuMap.put(productSku.getId(), productSku);
        }

        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.lambda()
                .in(Product::getId, productIds);
        List<Product> products = this.productMapper.selectList(productQueryWrapper);
        if (CollectionUtils.isEmpty(products)) {
            return new HashMap<>();
        }
        Map<Long, Product> productMap = new HashMap<>();
        for (Product product : products) {
            productMap.put(product.getId(), product);
        }

        //处理交易明细订单信息
        Map resp = new HashMap();
        for (Order order : orders) {
            Map map = new HashMap();
            ProductSku productSku = productSkuMap.get(order.getSkuId());
            Product product = productMap.get(order.getGoodsId());
            String title = StringUtils.isBlank(productSku.getSkuName()) ? product.getProductTitle() : productSku.getSkuName();
            map.put("fee", order.getFee());
            map.put("title", title);

            resp.put(order.getId(), map);
        }
        return resp;
    }

    @Override
    public User getUserInfoByOrderId(Long orderId) {
        Order order = this.orderMapper.selectByPrimaryKey(orderId);
        if (order == null) {
            return null;
        }

        User user = this.userService.selectByPrimaryKey(order.getUserId());
        return user;
    }

    @Override
    public Map<Long, Order> getOrderListByOrderIds(List<Long> orderIds) {
        if (CollectionUtils.isEmpty(orderIds)) {
            return new HashMap<>();
        }
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Order::getId, orderIds)
        ;
        List<Order> orders = this.orderMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(orders)) {
            return new HashMap<>();
        }
        Map<Long, Order> map = new HashMap<>();
        for (Order order : orders) {
            map.put(order.getId(), order);
        }
        return map;
    }
}
