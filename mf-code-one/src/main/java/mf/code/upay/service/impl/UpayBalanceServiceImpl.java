package mf.code.upay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.upay.repo.dao.UpayBalanceMapper;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderMfTradeTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月24日 10:29
 * @Description:
 */
@Service
@Slf4j
public class UpayBalanceServiceImpl implements UpayBalanceService {

    @Autowired
    private UpayBalanceMapper upayBalanceMapper;
    @Autowired
    private UpayWxOrderService upayWxOrderService;

    @Override
    @Transactional
    public UpayBalance create(long merchantID, long shopID, long userID, BigDecimal balance) {
        UpayBalance po = new UpayBalance();
        po.setMerchantId(merchantID);
        po.setShopId(shopID);
        po.setUserId(userID);
        po.setBalance(balance);
        po.setCreatedAt(new Date());
        po.setUpdatedAt(new Date());
        this.upayBalanceMapper.insertSelective(po);
        return po;
    }

    @Override
    public int insertSelective(UpayBalance upayBalance) {
        upayBalance.setCreatedAt(new Date());
        upayBalance.setUpdatedAt(new Date());
        return upayBalanceMapper.insertSelective(upayBalance);
    }

    @Override
    public UpayBalance updateOrCreate(long merchantID, long shopID, long userID, BigDecimal balance) {
        UpayBalance po = this.query(merchantID, shopID, userID);
        if (po == null) {
            po = this.create(merchantID, shopID, userID, balance);
            return po;
        }

        po.setUpdatedAt(new Date());
        po.setBalance(po.getBalance().add(balance));
        this.upayBalanceMapper.updateByPrimaryKeySelective(po);
        return po;
    }


    @Override
    public UpayBalance query(long merchantID, long shopID, long userID) {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("merchantId", merchantID);
        objectMap.put("shopId", shopID);
        objectMap.put("userId", userID);
        List<UpayBalance> upayBalances = this.upayBalanceMapper.listPageByParams(objectMap);
        if (upayBalances == null || upayBalances.size() == 0) {
            return null;
        }
        return upayBalances.get(0);
    }

    @Override
    public UpayBalance update(UpayBalance upayBalance) {
        upayBalance.setUpdatedAt(new Date());
        this.upayBalanceMapper.updateByPrimaryKeySelective(upayBalance);
        return upayBalance;
    }

    @Override
    public Integer updateBalanceByAmount(Long merchantId, Long shopId, Long userId, BigDecimal amount) {
        Map<String, Object> map = new HashMap<>();
        map.put("merchantId", merchantId);
        map.put("shopId", shopId);
        map.put("userId", userId);
        map.put("amount", amount);
        map.put("updateAt", new Date());
        return this.upayBalanceMapper.payLessUserBalance(map);
    }

    @Override
    public int finishAddUserOpenRedPackBalance(Long merchantId, Long shopId, Long userId, BigDecimal amount) {
        Map<String, Object> map = new HashMap<>();
        map.put("merchantId", merchantId);
        map.put("shopId", shopId);
        map.put("userId", userId);
        map.put("amount", amount);
        map.put("updateAt", new Date());
        return this.upayBalanceMapper.finishAddUserOpenRedPackBalance(map);
    }

    @Override
    public BigDecimal canCashMoney(Long merchantId, Long shopId, Long userId) {
        UpayBalance upayBalance = query(merchantId, shopId, userId);
        if (upayBalance == null) {
            return BigDecimal.ZERO;
        }
        //获取余额提现金额
        BigDecimal taskIncome = upayBalance.getBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal promotionIncome = upayBalance.getShoppingBalance().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal openRedPackBalance = upayBalance.getOpenredpackBalance().setScale(2, BigDecimal.ROUND_DOWN);
        QueryWrapper<UpayWxOrder> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.lambda()
                .eq(UpayWxOrder::getShopId, shopId)
                .eq(UpayWxOrder::getUserId, userId)
                .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
                .eq(UpayWxOrder::getBizType, UpayWxOrderBizTypeEnum.OPEN_RED_PACKAGE.getCode())
                .eq(UpayWxOrder::getMfTradeType, UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode())
        ;
        BigDecimal openRedPackLockSumMoney = BigDecimal.ZERO;
        UpayWxOrder upayWxOrder1 = upayWxOrderService.getOne(orderQueryWrapper);
        if (upayWxOrder1 != null) {
            //锁定金额
            openRedPackLockSumMoney = upayWxOrder1.getTotalFee();
        }
        BigDecimal canCashMoney = BigDecimal.ZERO;
        canCashMoney = taskIncome.add(promotionIncome).add(openRedPackBalance).subtract(openRedPackLockSumMoney);
        return canCashMoney;
    }
}
