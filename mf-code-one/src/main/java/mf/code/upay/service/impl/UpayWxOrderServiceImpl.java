package mf.code.upay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.Constants;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RandomStrUtil;
import mf.code.upay.repo.dao.UpayBalanceMapper;
import mf.code.upay.repo.dao.UpayWxOrderMapper;
import mf.code.upay.repo.enums.JsapiSceneEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.WxTradeTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月24日 14:31
 */
@Slf4j
@Service
public class UpayWxOrderServiceImpl extends ServiceImpl<UpayWxOrderMapper, UpayWxOrder> implements UpayWxOrderService {

    @Autowired
    private UpayWxOrderMapper upayWxOrderMapper;
    @Autowired
    private UpayBalanceMapper upayBalanceMapper;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private OrderTxServiceImpl orderTxService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private UpayBalanceService upayBalanceService;
    private final int REFUND_WX = 2;

    private final int REFUND_BALANCE = 1;

    @Override
    public UpayWxOrder selectByOrderNoPlus(String orderNo) {
        return upayWxOrderMapper.selectByOrderNoPlus(orderNo);
    }

    /**
     * 退款
     *
     * @param payType  1:余额退款  2:微信退款
     * @param userId   用户id
     * @param amount   金额
     * @param orderId  产生退款的订单id
     * @param bizType  业务类型，1：免单抽奖2:轮盘抽奖3:随机红包4:红包任务
     * @param bizValue 业务类型针对的值1:活动编号，2:活动定义编号3:待定4待定
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public SimpleResponse refund(int payType,
                                 Long merchantId,
                                 Long shopId,
                                 Long userId,
                                 BigDecimal amount,
                                 Long orderId,
                                 Integer bizType,
                                 Long bizValue,
                                 String orderNo) {

        SimpleResponse response = null;
        String redisFobidKey = RedisKeyConstant.FORBID_KEY_REFUND + userId + "" + orderId;
        boolean boo = RedisForbidRepeat.NX(redisTemplate, redisFobidKey);
        if (!boo) {
            return new SimpleResponse(Constants.RefundStatus.REFUND_FORBID, "防重.此用户已经对此订单退款过");
        }
        //生成订单.生成订单时,如果是微信支付,必须需要在这里插入一条记录.
        //不支持事务
        Long createOrderId = orderTxService.createRefundOrder(userId, merchantId, shopId, amount, payType, orderId, bizType, bizValue);
        //如果是-1,没有返回正确的orderId,也就是没有插入成功
        if (createOrderId == -1) {
            return new SimpleResponse(Constants.RefundStatus.REFUND_CREATE_ORDER_FAILD, "退款时,不能正常创建退款订单,直接返回.");
        }

        // 2:微信支付 ->调用微信申请退款接口,发起退款
        // 1:余额支付 ->1.改订单状态 2.退余额
        if (payType == Constants.PayType.WX_PAY) {
            log.info("<<<<<<<<<< start wx refund orderId: {}",createOrderId);
            //微信退款: 调用微信申请退款接口,发起退款
            UpayWxOrder refundWxOrder = this.upayWxOrderMapper.selectByPrimaryKey(createOrderId);
            response = wxRefund(orderNo, refundWxOrder.getOrderNo(), amount);
        } else if (payType == Constants.PayType.BALANCE_PAY) {
            // 1 改订单状态   2 退余额b
            response = balanceRefund(merchantId, userId, amount, createOrderId);
        }
        return response;
    }

    /**
     * 余额退款
     * <p>
     * 1 更新订单状态
     * 2 增加用户余额
     */
    private SimpleResponse balanceRefund(Long merchantId, Long userId, BigDecimal amount, Long orderId) {
        Date now = new Date();
        // 更新订单状态
        UpayWxOrder order = upayWxOrderMapper.selectByPrimaryKey(orderId);
        order.setUtime(now);
        order.setStatus(OrderStatusEnum.ORDERED.getCode());
        int sqlResult = upayWxOrderMapper.updateByPrimaryKeySelective(order);

        Map param = new HashMap(5);
        param.put("userId", userId);
        param.put("updateAt", now);
        param.put("amount", amount);
        param.put("merchantId", order.getMchId());
        param.put("shopId", order.getShopId());
        int sqlUpdateResult = upayBalanceMapper.refundAddUserBalance(param);
        //增加用户余额
        SimpleResponse response =
                sqlUpdateResult == 1
                        ? new SimpleResponse(0, "success")
                        : new SimpleResponse(-1, "更新user_balance表,sql执行结果是:0");
        if (sqlUpdateResult == 0) {
            log.info("退款业务:退回用户id:" + userId + "余额,merchantId:" + merchantId + ",orderId:" + orderId);
        }
        return response;
    }

    /**
     * 发起微信退款申请
     */
    private SimpleResponse wxRefund(String orderNo, String refundOrderNo, BigDecimal amount) {
        Map<String, Object> param = weixinPayService.getRefundPayOrderReqParams(orderNo, refundOrderNo, amount, amount, WeixinPayConstants.APPLET_NOTIFY_SOURCE);
        Map<String, Object> result = weixinPayService.refundPayOrder(param);
        log.info("<<<<<<<<<< refund resp: {} req: {}",result, param);
        String WX_RETURN_CODE_VAL = "FAIL";
        String WX_RETURN_CODE_KEY = "return_code";
        // 应补偿
        if (result.get(WX_RETURN_CODE_KEY) != null && WX_RETURN_CODE_VAL.equals(result.get(WX_RETURN_CODE_KEY))) {
            String errMsg = null;
            if(result.get("err_code") != null && StringUtils.isNotBlank(result.get("err_code").toString())){
                errMsg = result.get("err_code").toString();
            }else if(result.get("return_msg") != null && StringUtils.isNotBlank(result.get("return_msg").toString())){
                errMsg = result.get("return_msg").toString();
            }
            return new SimpleResponse(-1, "调用微信申请退款接口有误,微信返回代码:" + result == null ? "" : errMsg);
        }
        return new SimpleResponse(0, "success");
    }

    /**
     * 微信退款回调后 , 更新退款订单状态
     */
    @Override
    public SimpleResponse wxRefundCallback(Long orderId, Long userId) {
        UpayWxOrder order = new UpayWxOrder();
        order.setId(orderId);
        order.setUserId(userId);
        order.setUtime(new Date());
        //状态是1
        order.setStatus(OrderStatusEnum.ORDERED.getCode());
        int result = upayWxOrderMapper.updateByPrimaryKeySelective(order);
        SimpleResponse response;
        if (result == 1) {
            response = new SimpleResponse(0, "success");
        } else {
            response = new SimpleResponse(-1, "error");
        }
        return response;
    }

    @Override
    public UpayWxOrder selectByKeyId(Long orderKeyID) {
        return this.upayWxOrderMapper.selectByPrimaryKey(orderKeyID);
    }

    @Override
    public UpayWxOrder findById(Long orderId) {
        return upayWxOrderMapper.selectByPrimaryKey(orderId);
    }

    @Override
    public UpayWxOrder create(UpayWxOrder upayWxOrder) {
        upayWxOrder.setCtime(new Date());
        upayWxOrder.setUtime(new Date());
        this.upayWxOrderMapper.insertSelective(upayWxOrder);
        return upayWxOrder;
    }

    @Override
    @Transactional
    public Integer createAndUpdateBalance(UpayWxOrder upayWxOrder, BigDecimal amount) {
        int i = 0;
        // begin
        UpayWxOrder upayWxOrder1 = this.create(upayWxOrder);
        if(upayWxOrder1 != null){
            i = 1;
        }
        //余额相减
        Integer integer = this.upayBalanceService.updateBalanceByAmount(upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getUserId(), amount);
        if(integer != null && integer > 0){
            i = integer;
        }
        // over
        return i;
    }

    @Override
    public UpayWxOrder updateByPrimaryKeySelective(UpayWxOrder upayWxOrder) {
        upayWxOrder.setUtime(new Date());
        this.upayWxOrderMapper.updateByPrimaryKeySelective(upayWxOrder);
        return upayWxOrder;
    }

    @Override
    public Integer updateOrderStatusPlus(UpayWxOrder record) {
        return upayWxOrderMapper.updateOrderStatusPlus(record);
    }

    @Override
    public List<UpayWxOrder> query(Map<String, Object> map) {
        return this.upayWxOrderMapper.listPageByParams(map);
    }

    @Override
    public int countUpayWxOrder(Map<String, Object> map) {
        return this.upayWxOrderMapper.countUpayWxOrder(map);
    }

    @Override
    public BigDecimal sumUpayWxOrderTotalFee(Map<String, Object> params) {
        return this.upayWxOrderMapper.sumUpayWxOrderTotalFee(params);
    }

    @Override
    public UpayWxOrder addPo(Long userID,
                             Integer payType,
                             Integer type,
                             String orderNo,
                             String orderName,
                             Long merchantID,
                             Long shopID,
                             Integer status,
                             BigDecimal amount,
                             String ipAddress,
                             String tradeID,
                             String remark,
                             Date paymentTime,
                             Date notifyTime,
                             String reqJson,
                             Integer bizType,
                             Long bizValue) {
        UpayWxOrder po = new UpayWxOrder();
        if (userID != null) {
            po.setUserId(userID);
        }
        if (payType != null) {
            po.setPayType(payType);
            if (payType == OrderPayTypeEnum.WEIXIN.getCode()) {
                po.setAppletAppId(wxpayProperty.getMfAppId());
                po.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
                po.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
            }
        }
        if (type != null) {
            po.setType(type);
        }
        if (StringUtils.isNotBlank(orderNo)) {
            po.setOrderNo(orderNo);
        } else {
            po.setOrderNo("V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis());
        }
        if (StringUtils.isNotBlank(orderName)) {
            po.setOrderName(orderName);
        }
        if (merchantID != null) {
            po.setMchId(merchantID);
        }
        if (shopID != null) {
            po.setShopId(shopID);
        }
        if (status != null) {
            po.setStatus(status);
        }
        if (amount != null) {
            po.setTotalFee(amount);
        }
        if (StringUtils.isNotBlank(ipAddress)) {
            po.setIpAddress(ipAddress);
        }
        if (StringUtils.isNotBlank(tradeID)) {
            po.setTradeId(tradeID);
        }
        if (StringUtils.isNotBlank(remark)) {
            po.setRemark(remark);
        }
        if (paymentTime != null) {
            po.setPaymentTime(paymentTime);
        }
        if (notifyTime != null) {
            po.setNotifyTime(notifyTime);
        }
        if (StringUtils.isNotBlank(reqJson)) {
            po.setReqJson(reqJson);
        } else {
            po.setReqJson("");
        }
        if (bizType != null) {
            po.setBizType(bizType);
        }
        if (bizValue != null) {
            po.setBizValue(bizValue);
        }

        return po;
    }
}
