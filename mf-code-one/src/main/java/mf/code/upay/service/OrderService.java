package mf.code.upay.service;

import mf.code.upay.repo.po.Order;
import mf.code.user.repo.po.User;

import java.util.List;
import java.util.Map;

/**
 * mf.code.upay.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月17日 10:32
 */
public interface OrderService {
    int countByStatusByShopUser(Long shopId, Long userId, List<Integer> oderTypes, List<Integer> orderStatus);

    /***
     * 获取交易明细内的购物商品信息
     * @param productOrderIds
     * @return
     */
    Map<Long, Map>  getUpayOrderProductInfo(List<Long> productOrderIds);

    User getUserInfoByOrderId(Long orderId);

    Map<Long, Order> getOrderListByOrderIds(List<Long> orderIds);
}
