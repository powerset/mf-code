package mf.code.upay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.upay.repo.dao.PlatformOrderMapper;
import mf.code.upay.repo.po.PlatformOrder;
import mf.code.upay.service.PlatformOrderService;
import org.springframework.stereotype.Service;

/**
 * mf.code.upay.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月02日 10:15
 */
@Service
public class PlatformOrderServiceImpl extends ServiceImpl<PlatformOrderMapper, PlatformOrder> implements PlatformOrderService {
}
