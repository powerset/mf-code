package mf.code.upay.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.IpUtil;
import mf.code.upay.repo.enums.*;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayAboutService;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.activity.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 14:58
 */
@Service
@Slf4j
public class UpayAboutServiceImpl implements UpayAboutService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayBalanceService upayBalanceService;


    @Override
    public SimpleResponse openRedPackageSaveUpay(Activity activity, BigDecimal fee) {
        Long activityId = activity.getId();
        Long merchantId = activity.getMerchantId();
        Long shopId = activity.getShopId();
        Long userId = activity.getUserId();

        //存储pay_order
        UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(userId, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.PALTFORMRECHARGE.getCode(), null,
                BizTypeEnum.findBizTypeByActivityType(activity.getType()).getMessage(), merchantId, shopId, OrderStatusEnum.ORDERED.getCode(), fee, IpUtil.getIpAddress(null), null, BizTypeEnum.findBizTypeByActivityType(activity.getType()).getMessage(), null, null,
                null, BizTypeEnum.findBizTypeByActivityType(activity.getType()).getCode(), activityId);
        upayWxOrder.setMfTradeType(UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode());
        if (activity.getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            upayWxOrder.setPaymentTime(new Date());
            upayWxOrder.setMfTradeType(UpayWxOrderMfTradeTypeEnum.CLEANINGED.getCode());
        }
        this.upayWxOrderService.create(upayWxOrder);
        //更新|存储balance
        UpayBalance upayBalance = upayBalanceService.query(merchantId, shopId, userId);
        if (upayBalance == null) {
            UpayBalance po = new UpayBalance();
            po.setMerchantId(merchantId);
            po.setShopId(shopId);
            po.setUserId(userId);
            po.setOpenredpackBalance(fee);
            int i = upayBalanceService.insertSelective(po);
            log.info("<<<<<<<<创建余额-拆红包获得金额状态：{}", i);
        } else {
            int i = upayBalanceService.finishAddUserOpenRedPackBalance(merchantId, shopId, userId, fee);
            log.info("<<<<<<<<更新拆红包获得金额状态：{}", i);
        }
        return new SimpleResponse();
    }
}
