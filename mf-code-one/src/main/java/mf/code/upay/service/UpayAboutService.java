package mf.code.upay.service;

import mf.code.activity.repo.po.Activity;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;

/**
 * mf.code.activity.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月15日 14:57
 */
public interface UpayAboutService {
    SimpleResponse openRedPackageSaveUpay(Activity activity, BigDecimal fee);
}
