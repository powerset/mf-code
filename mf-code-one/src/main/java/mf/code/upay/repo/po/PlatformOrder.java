package mf.code.upay.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * platform_order
 * 平台订单表,用于平台充值,提现,查账
 */
public class PlatformOrder implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 商户号-主账号id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单类型 0：平台提现 1：平台-付款 2：平台-入账 3:平台-退款
     */
    private Integer type;

    /**
     * 支付方式：1:余额(预存金余额) 2:微信 3：支付宝
     */
    private Integer payType;

    /**
     * 订单编号 本系统的平台的订单编号 -- 特定方式生成 同种条件生成唯一值
     */
    private String orderNo;

    /**
     * 订单名称 -- 提示平台订单业务类型
     */
    private String orderName;

    /**
     * 公众号appid 冗余字段
     */
    private String pubAppId;

    /**
     * 订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）
     */
    private Integer status;

    /**
     * 总金额,单位元
     */
    private BigDecimal totalFee;

    /**
     * 交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    private Integer tradeType;

    /**
     * 流水号 微信流水号唯一标识
     */
    private String tradeId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 回调处理系统时间
     */
    private Date notifyTime;

    /**
     * 响应数据中的支付时间
     */
    private Date paymentTime;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 订单入参，json对象
     */
    private String reqJson;

    /**
     * 业务类型，1：订单成交-商户交易税，2：订单成交-平台商品类目抽佣，3:订单成交-上级缺失 用户分佣回收，4:订单成交-用户返利分佣税务部分回收，5:订单成交-用户返利分佣团队奖励部分回收，6:任务完成-上级缺失 用户闯关缴税回收
     */
    private Integer bizType;

    /**
     * 业务类型针对的值：订单成交相关(1~5)此值为orderId， 任务完成相关(6)此值为 待定
     */
    private Long bizValue;

    /**
     * platform_order
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户号-主账号id
     * @return merchant_id 商户号-主账号id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户号-主账号id
     * @param merchantId 商户号-主账号id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 订单类型 0：平台提现 1：平台-付款 2：平台-入账 3:平台-退款
     * @return type 订单类型 0：平台提现 1：平台-付款 2：平台-入账 3:平台-退款
     */
    public Integer getType() {
        return type;
    }

    /**
     * 订单类型 0：平台提现 1：平台-付款 2：平台-入账 3:平台-退款
     * @param type 订单类型 0：平台提现 1：平台-付款 2：平台-入账 3:平台-退款
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 支付方式：1:余额(预存金余额) 2:微信 3：支付宝
     * @return pay_type 支付方式：1:余额(预存金余额) 2:微信 3：支付宝
     */
    public Integer getPayType() {
        return payType;
    }

    /**
     * 支付方式：1:余额(预存金余额) 2:微信 3：支付宝
     * @param payType 支付方式：1:余额(预存金余额) 2:微信 3：支付宝
     */
    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    /**
     * 订单编号 本系统的平台的订单编号 -- 特定方式生成 同种条件生成唯一值
     * @return order_no 订单编号 本系统的平台的订单编号 -- 特定方式生成 同种条件生成唯一值
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 订单编号 本系统的平台的订单编号 -- 特定方式生成 同种条件生成唯一值
     * @param orderNo 订单编号 本系统的平台的订单编号 -- 特定方式生成 同种条件生成唯一值
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 订单名称 -- 提示平台订单业务类型
     * @return order_name 订单名称 -- 提示平台订单业务类型
     */
    public String getOrderName() {
        return orderName;
    }

    /**
     * 订单名称 -- 提示平台订单业务类型
     * @param orderName 订单名称 -- 提示平台订单业务类型
     */
    public void setOrderName(String orderName) {
        this.orderName = orderName == null ? null : orderName.trim();
    }

    /**
     * 公众号appid 冗余字段
     * @return pub_app_id 公众号appid 冗余字段
     */
    public String getPubAppId() {
        return pubAppId;
    }

    /**
     * 公众号appid 冗余字段
     * @param pubAppId 公众号appid 冗余字段
     */
    public void setPubAppId(String pubAppId) {
        this.pubAppId = pubAppId == null ? null : pubAppId.trim();
    }

    /**
     * 订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）
     * @return status 订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）
     * @param status 订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 总金额,单位元
     * @return total_fee 总金额,单位元
     */
    public BigDecimal getTotalFee() {
        return totalFee;
    }

    /**
     * 总金额,单位元
     * @param totalFee 总金额,单位元
     */
    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    /**
     * 交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     * @return trade_type 交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    public Integer getTradeType() {
        return tradeType;
    }

    /**
     * 交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     * @param tradeType 交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款
     */
    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    /**
     * 流水号 微信流水号唯一标识
     * @return trade_id 流水号 微信流水号唯一标识
     */
    public String getTradeId() {
        return tradeId;
    }

    /**
     * 流水号 微信流水号唯一标识
     * @param tradeId 流水号 微信流水号唯一标识
     */
    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    /**
     * 备注
     * @return remark 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 回调处理系统时间
     * @return notify_time 回调处理系统时间
     */
    public Date getNotifyTime() {
        return notifyTime;
    }

    /**
     * 回调处理系统时间
     * @param notifyTime 回调处理系统时间
     */
    public void setNotifyTime(Date notifyTime) {
        this.notifyTime = notifyTime;
    }

    /**
     * 响应数据中的支付时间
     * @return payment_time 响应数据中的支付时间
     */
    public Date getPaymentTime() {
        return paymentTime;
    }

    /**
     * 响应数据中的支付时间
     * @param paymentTime 响应数据中的支付时间
     */
    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 订单入参，json对象
     * @return req_json 订单入参，json对象
     */
    public String getReqJson() {
        return reqJson;
    }

    /**
     * 订单入参，json对象
     * @param reqJson 订单入参，json对象
     */
    public void setReqJson(String reqJson) {
        this.reqJson = reqJson == null ? null : reqJson.trim();
    }

    /**
     * 业务类型，1：订单成交-商户交易税，2：订单成交-平台商品类目抽佣，3:订单成交-上级缺失 用户分佣回收，4:订单成交-用户返利分佣税务部分回收，5:订单成交-用户返利分佣团队奖励部分回收，6:任务完成-上级缺失 用户闯关缴税回收
     * @return biz_type 业务类型，1：订单成交-商户交易税，2：订单成交-平台商品类目抽佣，3:订单成交-上级缺失 用户分佣回收，4:订单成交-用户返利分佣税务部分回收，5:订单成交-用户返利分佣团队奖励部分回收，6:任务完成-上级缺失 用户闯关缴税回收
     */
    public Integer getBizType() {
        return bizType;
    }

    /**
     * 业务类型，1：订单成交-商户交易税，2：订单成交-平台商品类目抽佣，3:订单成交-上级缺失 用户分佣回收，4:订单成交-用户返利分佣税务部分回收，5:订单成交-用户返利分佣团队奖励部分回收，6:任务完成-上级缺失 用户闯关缴税回收
     * @param bizType 业务类型，1：订单成交-商户交易税，2：订单成交-平台商品类目抽佣，3:订单成交-上级缺失 用户分佣回收，4:订单成交-用户返利分佣税务部分回收，5:订单成交-用户返利分佣团队奖励部分回收，6:任务完成-上级缺失 用户闯关缴税回收
     */
    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    /**
     * 业务类型针对的值：订单成交相关(1~5)此值为orderId， 任务完成相关(6)此值为 待定
     * @return biz_value 业务类型针对的值：订单成交相关(1~5)此值为orderId， 任务完成相关(6)此值为 待定
     */
    public Long getBizValue() {
        return bizValue;
    }

    /**
     * 业务类型针对的值：订单成交相关(1~5)此值为orderId， 任务完成相关(6)此值为 待定
     * @param bizValue 业务类型针对的值：订单成交相关(1~5)此值为orderId， 任务完成相关(6)此值为 待定
     */
    public void setBizValue(Long bizValue) {
        this.bizValue = bizValue;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", userId=").append(userId);
        sb.append(", type=").append(type);
        sb.append(", payType=").append(payType);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", orderName=").append(orderName);
        sb.append(", pubAppId=").append(pubAppId);
        sb.append(", status=").append(status);
        sb.append(", totalFee=").append(totalFee);
        sb.append(", tradeType=").append(tradeType);
        sb.append(", tradeId=").append(tradeId);
        sb.append(", remark=").append(remark);
        sb.append(", notifyTime=").append(notifyTime);
        sb.append(", paymentTime=").append(paymentTime);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", reqJson=").append(reqJson);
        sb.append(", bizType=").append(bizType);
        sb.append(", bizValue=").append(bizValue);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}