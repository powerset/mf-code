package mf.code.upay.repo.enums;

/**
 * mf.code.upay.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 18:14
 */
public enum UpayWxOrderMfTradeTypeEnum {

    /**
     * 免单抽奖
     */
    WILLCLEANING(1, "待结算"),
    /**
     * 轮盘抽奖
     */
    CLEANINGED(2, "已结算"),
    ;

    private int code;
    private String message;

    UpayWxOrderMfTradeTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String findByDesc(int code){
        for (UpayWxOrderMfTradeTypeEnum typeEnum : UpayWxOrderMfTradeTypeEnum.values()) {
            if(typeEnum.getCode() == code){
                return typeEnum.getMessage();
            }
        }
        return null;
    }
}
