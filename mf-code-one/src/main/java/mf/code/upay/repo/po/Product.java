package mf.code.upay.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * product
 */
public class Product implements Serializable {
    /**
     *
     */
    private Long id;

    /**
     * 上级商品id（从哪个商品派生的）
情况1 id=parent_id 自营创建
情况2 id!=parent_id 来自分销市场
     */
    private Long parentId;

    /**
     * 商户id（所属商户）
     */
    private Long merchantId;

    /**
     * 店铺id（所属店铺）
     */
    private Long shopId;

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 商品类目 为category_taobao.sid
     */
    private Long productSpecs;

    /**
     * 商品描述(冗余 暂不用)
     */
    private String productDetail;

    /**
     * 商品图（主图级轮播图） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    private String mainPics;

    /**
     * 详情图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    private String detailPics;

    /**
     * 价格区间最小值 (冗余 暂不用)
     */
    private BigDecimal productPriceMin;

    /**
     * 价格区间 最大值  (冗余 暂不用)
     */
    private BigDecimal productPriceMax;

    /**
     * 淘宝价格区间 最小值 冗余(暂不用)
     */
    private BigDecimal thirdPriceMin;

    /**
     * 淘宝价格区间 最大值  冗余(暂不用)
     */
    private BigDecimal thirdPriceMax;

    /**
     * 状态（1:上架,0:未上架(默认值)）
     */
    private Integer status;

    /**
     * 审核状态 -1: 初始状态(默认值) 0: 待审核 1:审核通过
     */
    private Integer checkStatus;

    /**
     * 审核原因
     */
    private String checkReason;

    /**
     * 审核不通过,上传图片,最多三张 非json格式,逗号隔开
     */
    private String checkReasonPic;

    /**
     * 顺序
     */
    private Long sort;

    /**
     * 自营分销佣金比例
     */
    private BigDecimal selfSupportRatio;

    /**
     * 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     */
    private BigDecimal platSupportRatio;

    /**
     * 提交审核时间
     */
    private Date submitTime;

    /**
     * 审核时间
     */
    private Date checkTime;

    /**
     * 上架时间
     */
    private Date putawayTime;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 标记删除 0 :正常 1:删除
     */
    private Integer del;

    /**
     * product
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 上级商品id（从哪个商品派生的）
 情况1 id=parent_id 自营创建
 情况2 id!=parent_id 来自分销市场
     * @return parent_id 上级商品id（从哪个商品派生的）
 情况1 id=parent_id 自营创建
 情况2 id!=parent_id 来自分销市场
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 上级商品id（从哪个商品派生的）
 情况1 id=parent_id 自营创建
 情况2 id!=parent_id 来自分销市场
     * @param parentId 上级商品id（从哪个商品派生的）
 情况1 id=parent_id 自营创建
 情况2 id!=parent_id 来自分销市场
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 商户id（所属商户）
     * @return merchant_id 商户id（所属商户）
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id（所属商户）
     * @param merchantId 商户id（所属商户）
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id（所属店铺）
     * @return shop_id 店铺id（所属店铺）
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id（所属店铺）
     * @param shopId 店铺id（所属店铺）
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 商品标题
     * @return product_title 商品标题
     */
    public String getProductTitle() {
        return productTitle;
    }

    /**
     * 商品标题
     * @param productTitle 商品标题
     */
    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle == null ? null : productTitle.trim();
    }

    /**
     * 商品类目 为category_taobao.sid
     * @return product_specs 商品类目 为category_taobao.sid
     */
    public Long getProductSpecs() {
        return productSpecs;
    }

    /**
     * 商品类目 为category_taobao.sid
     * @param productSpecs 商品类目 为category_taobao.sid
     */
    public void setProductSpecs(Long productSpecs) {
        this.productSpecs = productSpecs;
    }

    /**
     * 商品描述(冗余 暂不用)
     * @return product_detail 商品描述(冗余 暂不用)
     */
    public String getProductDetail() {
        return productDetail;
    }

    /**
     * 商品描述(冗余 暂不用)
     * @param productDetail 商品描述(冗余 暂不用)
     */
    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail == null ? null : productDetail.trim();
    }

    /**
     * 商品图（主图级轮播图） 存json数组 ["http://2.jpg","http://1.jpg"]
     * @return main_pics 商品图（主图级轮播图） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    public String getMainPics() {
        return mainPics;
    }

    /**
     * 商品图（主图级轮播图） 存json数组 ["http://2.jpg","http://1.jpg"]
     * @param mainPics 商品图（主图级轮播图） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    public void setMainPics(String mainPics) {
        this.mainPics = mainPics == null ? null : mainPics.trim();
    }

    /**
     * 详情图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     * @return detail_pics 详情图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    public String getDetailPics() {
        return detailPics;
    }

    /**
     * 详情图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     * @param detailPics 详情图（n+图片） 存json数组 ["http://2.jpg","http://1.jpg"]
     */
    public void setDetailPics(String detailPics) {
        this.detailPics = detailPics == null ? null : detailPics.trim();
    }

    /**
     * 价格区间最小值 (冗余 暂不用)
     * @return product_price_min 价格区间最小值 (冗余 暂不用)
     */
    public BigDecimal getProductPriceMin() {
        return productPriceMin;
    }

    /**
     * 价格区间最小值 (冗余 暂不用)
     * @param productPriceMin 价格区间最小值 (冗余 暂不用)
     */
    public void setProductPriceMin(BigDecimal productPriceMin) {
        this.productPriceMin = productPriceMin;
    }

    /**
     * 价格区间 最大值  (冗余 暂不用)
     * @return product_price_max 价格区间 最大值  (冗余 暂不用)
     */
    public BigDecimal getProductPriceMax() {
        return productPriceMax;
    }

    /**
     * 价格区间 最大值  (冗余 暂不用)
     * @param productPriceMax 价格区间 最大值  (冗余 暂不用)
     */
    public void setProductPriceMax(BigDecimal productPriceMax) {
        this.productPriceMax = productPriceMax;
    }

    /**
     * 淘宝价格区间 最小值 冗余(暂不用)
     * @return third_price_min 淘宝价格区间 最小值 冗余(暂不用)
     */
    public BigDecimal getThirdPriceMin() {
        return thirdPriceMin;
    }

    /**
     * 淘宝价格区间 最小值 冗余(暂不用)
     * @param thirdPriceMin 淘宝价格区间 最小值 冗余(暂不用)
     */
    public void setThirdPriceMin(BigDecimal thirdPriceMin) {
        this.thirdPriceMin = thirdPriceMin;
    }

    /**
     * 淘宝价格区间 最大值  冗余(暂不用)
     * @return third_price_max 淘宝价格区间 最大值  冗余(暂不用)
     */
    public BigDecimal getThirdPriceMax() {
        return thirdPriceMax;
    }

    /**
     * 淘宝价格区间 最大值  冗余(暂不用)
     * @param thirdPriceMax 淘宝价格区间 最大值  冗余(暂不用)
     */
    public void setThirdPriceMax(BigDecimal thirdPriceMax) {
        this.thirdPriceMax = thirdPriceMax;
    }

    /**
     * 状态（1:上架,0:未上架(默认值)）
     * @return status 状态（1:上架,0:未上架(默认值)）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（1:上架,0:未上架(默认值)）
     * @param status 状态（1:上架,0:未上架(默认值)）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 审核状态 -1: 初始状态(默认值) 0: 待审核 1:审核通过
     * @return check_status 审核状态 -1: 初始状态(默认值) 0: 待审核 1:审核通过
     */
    public Integer getCheckStatus() {
        return checkStatus;
    }

    /**
     * 审核状态 -1: 初始状态(默认值) 0: 待审核 1:审核通过
     * @param checkStatus 审核状态 -1: 初始状态(默认值) 0: 待审核 1:审核通过
     */
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 审核原因
     * @return check_reason 审核原因
     */
    public String getCheckReason() {
        return checkReason;
    }

    /**
     * 审核原因
     * @param checkReason 审核原因
     */
    public void setCheckReason(String checkReason) {
        this.checkReason = checkReason == null ? null : checkReason.trim();
    }

    /**
     * 审核不通过,上传图片,最多三张 非json格式,逗号隔开
     * @return check_reason_pic 审核不通过,上传图片,最多三张 非json格式,逗号隔开
     */
    public String getCheckReasonPic() {
        return checkReasonPic;
    }

    /**
     * 审核不通过,上传图片,最多三张 非json格式,逗号隔开
     * @param checkReasonPic 审核不通过,上传图片,最多三张 非json格式,逗号隔开
     */
    public void setCheckReasonPic(String checkReasonPic) {
        this.checkReasonPic = checkReasonPic == null ? null : checkReasonPic.trim();
    }

    /**
     * 顺序
     * @return sort 顺序
     */
    public Long getSort() {
        return sort;
    }

    /**
     * 顺序
     * @param sort 顺序
     */
    public void setSort(Long sort) {
        this.sort = sort;
    }

    /**
     * 自营分销佣金比例
     * @return self_support_ratio 自营分销佣金比例
     */
    public BigDecimal getSelfSupportRatio() {
        return selfSupportRatio;
    }

    /**
     * 自营分销佣金比例
     * @param selfSupportRatio 自营分销佣金比例
     */
    public void setSelfSupportRatio(BigDecimal selfSupportRatio) {
        this.selfSupportRatio = selfSupportRatio;
    }

    /**
     * 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     * @return plat_support_ratio 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     */
    public BigDecimal getPlatSupportRatio() {
        return platSupportRatio;
    }

    /**
     * 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     * @param platSupportRatio 平台分销佣金比例. 从分销市场获取的商品,直接复制父商品的plat_support_ratio
     */
    public void setPlatSupportRatio(BigDecimal platSupportRatio) {
        this.platSupportRatio = platSupportRatio;
    }

    /**
     * 提交审核时间
     * @return submit_time 提交审核时间
     */
    public Date getSubmitTime() {
        return submitTime;
    }

    /**
     * 提交审核时间
     * @param submitTime 提交审核时间
     */
    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    /**
     * 审核时间
     * @return check_time 审核时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 审核时间
     * @param checkTime 审核时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 上架时间
     * @return putaway_time 上架时间
     */
    public Date getPutawayTime() {
        return putawayTime;
    }

    /**
     * 上架时间
     * @param putawayTime 上架时间
     */
    public void setPutawayTime(Date putawayTime) {
        this.putawayTime = putawayTime;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 标记删除 0 :正常 1:删除
     * @return del 标记删除 0 :正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 标记删除 0 :正常 1:删除
     * @param del 标记删除 0 :正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     *
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", productTitle=").append(productTitle);
        sb.append(", productSpecs=").append(productSpecs);
        sb.append(", productDetail=").append(productDetail);
        sb.append(", mainPics=").append(mainPics);
        sb.append(", detailPics=").append(detailPics);
        sb.append(", productPriceMin=").append(productPriceMin);
        sb.append(", productPriceMax=").append(productPriceMax);
        sb.append(", thirdPriceMin=").append(thirdPriceMin);
        sb.append(", thirdPriceMax=").append(thirdPriceMax);
        sb.append(", status=").append(status);
        sb.append(", checkStatus=").append(checkStatus);
        sb.append(", checkReason=").append(checkReason);
        sb.append(", checkReasonPic=").append(checkReasonPic);
        sb.append(", sort=").append(sort);
        sb.append(", selfSupportRatio=").append(selfSupportRatio);
        sb.append(", platSupportRatio=").append(platSupportRatio);
        sb.append(", submitTime=").append(submitTime);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", putawayTime=").append(putawayTime);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
