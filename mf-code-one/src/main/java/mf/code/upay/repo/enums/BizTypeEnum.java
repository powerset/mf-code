package mf.code.upay.repo.enums;

import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.common.constant.Constants;

/**
 * mf.code.upay.repo.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 18:14
 */
public enum BizTypeEnum {

    /**
     * 免单抽奖
     */
    ACTIVITY(1, "活动计划类"),
    /**
     * 轮盘抽奖
     */
    LUCKY_WHEEL(2, "轮盘抽奖"),
    /**
     * 回填订单
     */
    RANDOMREDPACK(3, "日常任务-回填订单任务"),
    /**
     * 收藏加购
     */
    REDPACKTASK(4, "日常任务-收藏加购任务"),
    /**
     * 好评返现
     */
    GOODSCOMMENT(5, "日常任务-好评返现任务"),
    /**
     * 拆红包活动
     */
    OPEN_RED_PACKAGE(6, "拆红包活动"),
    /***
     * 助力活动
     */
    ASSIST_ACTIVITY(7, "助力活动"),
    /***
     * 免单抽奖
     */
    ORDER_BACK(8, "免单抽奖"),
    /***
     * 新手有礼发起
     */
    NEWMAN_START(9, "新人有礼"),

    /***
     * 财富大闯关
     */
    CHECKPOINT(10, "财富大闯关"),
    /***
     * 用户返利
     */
    REBATE(11, "购买返利"),
    /***
     * 团队贡献
     */
    CONTRIBUTION(12, "团队贡献"),
    /**
     * 购物
     */
    SHOPPING(14, "购物"),

    /***
     * 新手任务-加群享福利
     */
    NEWBIE_TASK_BONUS(15, "新手任务-加群享福利任务"),
    /***
     * 新手任务-查看攻略获得红包
     */
    NEWBIE_TASK_REDPACK(16, "新手任务-查看攻略获得红包任务"),
    /***
     * 店长任务-店长任务_设置店长wx群获得奖励
     */
    SHOP_MANAGER_TASK_ADD_WX(17, "店长任务_设置店长wx群获得奖励任务"),
    /***
     * 店长任务-店长任务_邀请粉丝获得奖励
     */
    SHOP_MANAGER_TASK_INVITE_FANS(18, "店长任务_邀请粉丝获得奖励任务"),
    /***
     * 店长任务-店长任务_推荐粉丝成为店长
     */
    SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER(19, "店长任务_推荐粉丝成为店长任务"),
    /***
     * 新手任务-新人红包奖励到
     */
    NEWBIE_TASK_OPENREDPACK(20, "新手任务-新人红包奖励到任务"),
    /***
     * 老版本闯关金额结算
     */
    CHECKPOINT_OLD_DATA_CLEANED(21, "老版本闯关金额结算"),

    /***
     * 升级成为店长粉丝佣金到账
     */
    LEVLEUP_SHOP_MANAGER(22, "升级成为店长粉丝佣金到账"),
    /***
     * 报销活动
     */
    FULL_REIMBURSEMENT(23, "报销活动"),
    /***
     * 升级成为店长粉丝佣金到账
     */
    SHOP_MANAGER_PAY(24, "成功引导粉丝成为店长"),
    ;

    private int code;
    private String message;

    BizTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String findByDesc(int code) {
        for (BizTypeEnum typeEnum : BizTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return null;
    }

    public static BizTypeEnum findBizTypeByDefType(Integer activityDefType) {
        if (ActivityDefTypeEnum.MCH_PLAN.getCode() == activityDefType) {
            return BizTypeEnum.ACTIVITY;
        }
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == activityDefType) {
            return BizTypeEnum.ORDER_BACK;
        }
        if (ActivityDefTypeEnum.NEW_MAN.getCode() == activityDefType) {
            return BizTypeEnum.NEWMAN_START;
        }
        if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDefType) {
            return BizTypeEnum.LUCKY_WHEEL;
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == activityDefType) {
            return BizTypeEnum.RANDOMREDPACK;
        }
        if (ActivityDefTypeEnum.ASSIST.getCode() == activityDefType) {
            return BizTypeEnum.ASSIST_ACTIVITY;
        }
        if (ActivityDefTypeEnum.GIFT.getCode() == activityDefType) {
            return BizTypeEnum.ASSIST_ACTIVITY;
        }
        if (ActivityDefTypeEnum.RED_PACKET.getCode() == activityDefType) {
            return null;
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDefType) {
            return BizTypeEnum.OPEN_RED_PACKAGE;
        }
        if (ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDefType) {
            return BizTypeEnum.GOODSCOMMENT;
        }
        if (ActivityDefTypeEnum.FAV_CART.getCode() == activityDefType) {
            return BizTypeEnum.REDPACKTASK;
        }
        if (ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == activityDefType) {
            return BizTypeEnum.ORDER_BACK;
        }
        if (ActivityDefTypeEnum.ASSIST_V2.getCode() == activityDefType) {
            return BizTypeEnum.ASSIST_ACTIVITY;
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode() == activityDefType) {
            return BizTypeEnum.RANDOMREDPACK;
        }
        if (ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() == activityDefType) {
            return BizTypeEnum.GOODSCOMMENT;
        }
        if (ActivityDefTypeEnum.FAV_CART_V2.getCode() == activityDefType) {
            return BizTypeEnum.REDPACKTASK;
        }
        if (ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode() == activityDefType) {
            return BizTypeEnum.NEWBIE_TASK_BONUS;
        }
        if (ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode() == activityDefType) {
            return BizTypeEnum.NEWBIE_TASK_REDPACK;
        }
        if (ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode() == activityDefType) {
            return BizTypeEnum.SHOP_MANAGER_TASK_ADD_WX;
        }
        if (ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode() == activityDefType) {
            return BizTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS;
        }
        if (ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode() == activityDefType) {
            return BizTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER;
        }
        if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == activityDefType) {
            return BizTypeEnum.FULL_REIMBURSEMENT;
        }
        return null;
    }

    public static BizTypeEnum findBizTypeByActivityType(Integer activityType) {
        if (ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode() == activityType) {
            return BizTypeEnum.NEWBIE_TASK_OPENREDPACK;
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == activityType) {
            return BizTypeEnum.OPEN_RED_PACKAGE;
        }
        if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == activityType) {
            return BizTypeEnum.FULL_REIMBURSEMENT;
        }
        return null;
    }
}
