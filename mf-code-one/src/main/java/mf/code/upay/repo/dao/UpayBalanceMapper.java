package mf.code.upay.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.upay.repo.po.UpayBalance;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface UpayBalanceMapper extends BaseMapper<UpayBalance> {
    /***
     * 根据条件查询
     */
    List<UpayBalance> listPageByParams(Map<String, Object> params);
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(UpayBalance record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(UpayBalance record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    UpayBalance selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UpayBalance record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UpayBalance record);

    /**
     * 退款后更新用户余额
     * @param param
     * @return
     */
    int refundAddUserBalance(Map param);
    /**
     * 支付更新用户余额
     * @param param
     * @return
     */
    int payLessUserBalance(Map param);

    int finishAddUserOpenRedPackBalance(Map param);
}