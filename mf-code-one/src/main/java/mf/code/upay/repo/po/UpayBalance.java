package mf.code.upay.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * upay_balance
 * 用户余额表，jkmfv1.0创建
 */
public class UpayBalance implements Serializable {
    /**
     * 用户余额表主键
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 商户编号
     */
    private Long merchantId;

    /**
     * 店铺编号
     */
    private Long shopId;

    /**
     * 余额,单位元 — 任务余额
     */
    private BigDecimal balance;

    /**
     * 商城购物，返利分佣收益余额
     */
    private BigDecimal shoppingBalance;

    /**
     * 拆红包收益余额
     */
    private BigDecimal openredpackBalance;

    /**
     * 返利佣金结算时间
     */
    private Date settledTime;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新时间
     */
    private Date updatedAt;

    /**
     * upay_balance
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户余额表主键
     * @return id 用户余额表主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户余额表主键
     * @param id 用户余额表主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 商户编号
     * @return merchant_id 商户编号
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户编号
     * @param merchantId 商户编号
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺编号
     * @return shop_id 店铺编号
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺编号
     * @param shopId 店铺编号
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 余额,单位元 — 任务余额
     * @return balance 余额,单位元 — 任务余额
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * 余额,单位元 — 任务余额
     * @param balance 余额,单位元 — 任务余额
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * 商城购物，返利分佣收益余额
     * @return shopping_balance 商城购物，返利分佣收益余额
     */
    public BigDecimal getShoppingBalance() {
        return shoppingBalance;
    }

    /**
     * 商城购物，返利分佣收益余额
     * @param shoppingBalance 商城购物，返利分佣收益余额
     */
    public void setShoppingBalance(BigDecimal shoppingBalance) {
        this.shoppingBalance = shoppingBalance;
    }

    /**
     * 拆红包收益余额
     * @return openRedPack_balance 拆红包收益余额
     */
    public BigDecimal getOpenredpackBalance() {
        return openredpackBalance;
    }

    /**
     * 拆红包收益余额
     * @param openredpackBalance 拆红包收益余额
     */
    public void setOpenredpackBalance(BigDecimal openredpackBalance) {
        this.openredpackBalance = openredpackBalance;
    }

    /**
     * 返利佣金结算时间
     * @return settled_time 返利佣金结算时间
     */
    public Date getSettledTime() {
        return settledTime;
    }

    /**
     * 返利佣金结算时间
     * @param settledTime 返利佣金结算时间
     */
    public void setSettledTime(Date settledTime) {
        this.settledTime = settledTime;
    }

    /**
     * 创建时间
     * @return created_at 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 创建时间
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 更新时间
     * @return updated_at 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 更新时间
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", balance=").append(balance);
        sb.append(", shoppingBalance=").append(shoppingBalance);
        sb.append(", openredpackBalance=").append(openredpackBalance);
        sb.append(", settledTime=").append(settledTime);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}