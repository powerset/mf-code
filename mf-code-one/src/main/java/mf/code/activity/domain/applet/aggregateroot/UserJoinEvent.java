package mf.code.activity.domain.applet.aggregateroot;

import lombok.Data;
import mf.code.uactivity.repo.po.UserActivity;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 17:42
 */
@Data
public class UserJoinEvent {
	/**
	 * 从属活动id
	 */
	private Long activityId;
	/**
	 * 从属用户id
	 */
	private Long userId;
	/**
	 * 参与事件信息
	 */
	private UserActivity userActivityInfo;
}
