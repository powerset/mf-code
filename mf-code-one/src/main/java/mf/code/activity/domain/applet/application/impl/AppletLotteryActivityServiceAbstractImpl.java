package mf.code.activity.domain.applet.application.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.*;
import mf.code.activity.domain.applet.application.AppletLotteryActivityServiceAbstract;
import mf.code.activity.domain.applet.valueobject.ActivityPopupStatusEnum;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.domain.applet.valueobject.UserBoard;
import mf.code.activity.domain.applet.valueobject.UserBoardConfig;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.activity.domain.applet.application.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-13 21:19
 */
@Slf4j
@Service
public abstract class AppletLotteryActivityServiceAbstractImpl extends AppletActivityService implements AppletLotteryActivityServiceAbstract {
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    /**
     * 创建抽奖活动
     *
     * @param params
     * @return
     */
    @Override
    public SimpleResponse create(Map<String, Object> params) {
        Long activityDefId = Long.valueOf(params.get("activityDefId").toString());
        Long userId = Long.valueOf(params.get("userId").toString());

        AppletActivity appletActivity = this.createAppletActivity(activityDefId, userId);
        if (appletActivity == null) {
            log.info("activityStatus:3, 库存不足");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "库存不足");
        }
        if (appletActivity.getId() != null) {
            log.info("activityStatus:5, 已有任务，无法发起");
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("activityStatus", ActivityPopupStatusEnum.HAVE_ONE.getCode());
            resultVO.put("message", "已有任务，无法发起");
            resultVO.put("activityId", appletActivity.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5, resultVO);
        }

        AppletActivity appletAssistActivity = this.assembleLotteryActivity(appletActivity, params);
        if (appletAssistActivity == null) {
            log.info("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "参数异常");
        }

        // 生成用户参与事件
        appletAssistActivity.newUserJoinEvent(userId);

        // 持久化活动状态 -- 事务
        boolean save = appletActivityRepository.saveCreateByStockType(appletAssistActivity);
        if (!save) {
            log.info("activityStatus:3, 库存不足");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "库存不足");
        }
        return activityPageVO(appletActivity, params);
    }




    @Override
    public SimpleResponse join(Map<String, Object> params) {
        Long shopId = Long.valueOf(params.get("shopId").toString());
        Long activityId = Long.valueOf(params.get("activityId").toString());
        Long userId = Long.valueOf(params.get("userId").toString());
        Long pubUserId = null;
        if (params.get("pubUserId") != null && RegexUtils.StringIsNumber(params.get("pubUserId").toString())) {
            pubUserId = Long.valueOf(params.get("pubUserId").toString());
        }

        AppletLotteryActivity appletLotteryActivity = this.findAppletLotteryActivityById(activityId, userId, pubUserId);
        if (appletLotteryActivity == null) {
            log.error("查无此活动，activityId = {}", activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "查无此活动");
        }

        // 当前活动是否可过期
        if (appletLotteryActivity.isExpired()) {
            // TODO 活动过期 处理 -- 不影响平台免单抽奖

        }

        // 判断用户是否已经参与了此活动，如果参与了，返回response
        if (stringRedisTemplate.opsForHash().hasKey(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userId.toString())) {
            log.info("用户已参与活动");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "已参与活动");
        }

        // 当前活动是否可参与 -- 校验是不是发起人，并且活动在进行中的状态，未开奖
        if (!appletLotteryActivity.canJoinByUserId(userId)) {
            log.info("活动已无法参与");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "活动已无法参与");
        }

        // 活动计数
        Long trigger = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);
        if (trigger == null) {
            log.error("活动计数异常，redis异常");
            stringRedisTemplate.opsForValue().increment(mf.code.uactivity.repo.redis.RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
            // 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "网络异常，请稍候重试");
        }

        // 是否参与成功
        if (!appletLotteryActivity.isJoinSuccess(trigger)) {
            log.info("活动已开奖，activityId = {}", activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "活动已开奖");
        }
        // 获取用户信息
        AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
        if (appletUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "无效用户");
        }
        // 通过访问者 补全 活动邀请访问者的记录
        appletActivityRepository.addActivityInviteEventByVisitor(appletLotteryActivity, userId, pubUserId);
        appletLotteryActivity.inviteSuccess();

        // 将用户成为参与者 -- 获取抽奖码
        appletLotteryActivity.becomeMemberByUser(appletUser.getUserinfo());
        // 生成用户参与事件
        appletLotteryActivity.newUserJoinEvent(userId);
        // 持久化活动状态 -- 事务
        boolean save = appletActivityRepository.saveJoinLotteryActivity(appletLotteryActivity, userId, pubUserId);
        if (!save) {
            log.error("参加后更新数据异常");
            stringRedisTemplate.opsForValue().increment(mf.code.uactivity.repo.redis.RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
            // 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "网络异常，请稍候重试");
        }

        // 是否开奖
        if (appletLotteryActivity.isLottery(trigger)) {
            if (appletLotteryActivity.getActivityInfo().getCondPersionCount() != null) {
                // 由用户触发的开奖活动

                // 补全抽奖用户信息
                appletActivityRepository.addUserWinningProbability(appletLotteryActivity);
                // 抽奖
                List<Long> winnerIds = appletLotteryActivity.drawLottery();
                // 补全 用户参与事件 -- 只针对中奖者
                appletActivityRepository.findUserJoinEventByAppletActivity(appletLotteryActivity, winnerIds);
                // 发奖 并发起相应的中奖任务
                appletLotteryActivity.publishWinnerTask();

                // 持久化活动状态
                save = appletActivityRepository.saveLottery(appletLotteryActivity);
                if (!save) {
                    log.error("开奖存储异常 activity = {}", activityId);
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "网络异常，请稍候重试");
                }
                //开奖推送消息
                this.sendTemplateMessageService.sendTemplateMsg2Award(shopId, activityId, null);
            }
            // 由时间来触发开奖
        }
        // 处理返回数据
        return activityPageVO(appletLotteryActivity, params);
    }

    @Override
    public SimpleResponse queryActivityByParams(Map<String, Object> params) {
        Long activityId = Long.valueOf(params.get("activityId").toString());
        Long userId = Long.valueOf(params.get("userId").toString());
        Long shopId = Long.valueOf(params.get("shopId").toString());
        Long offset = Long.valueOf(params.get("offset").toString());
        Long size = Long.valueOf(params.get("size").toString());

        AppletActivity appletActivity = appletActivityRepository.findById(activityId);

        // 装配活动 -- 访问他人活动
        AppletLotteryActivity appletLotteryActivity = appletActivityRepository.assemblyAppletLotteryActivity(appletActivity, userId);
        // TODO 待补充
        boolean success = appletActivityRepository.addLotteryActivityInfo(appletLotteryActivity, userId, offset, size);
        if (!success) {
            log.error("活动数据回载防重");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "点击太快，请稍候再试");
        }

        if (appletLotteryActivity.isExpired()) {
            // 活动过期
            // 补全 用户参与事件
            if (appletLotteryActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
                // TODO 处理过期流程 -- 平台免单抽奖无需考虑
            }
        }
        return activityPageVO(appletLotteryActivity, params);
    }

    private AppletLotteryActivity findAppletLotteryActivityById(Long activityId, Long userId, Long pubUserId) {
        AppletActivity appletActivity = appletActivityRepository.findById(activityId);
        if (null == appletActivity) {
            log.error("查无此活动，activityId = {}", activityId);
            return null;
        }
        Activity activity = appletActivity.getActivityInfo();
        AppletLotteryActivity appletLotteryActivity = new AppletLotteryActivity();
        appletLotteryActivity.setId(activity.getId());
        appletLotteryActivity.setActivityDefId(activity.getActivityDefId());
        appletLotteryActivity.setActivityInfo(activity);
        appletLotteryActivity.setUserInfo(appletActivity.getUserInfo());
        appletLotteryActivity.setJoinRule(new JoinRule(activity));
        appletLotteryActivity.setCreateRule(new CreateRule(activity));
        appletLotteryActivity.setVisitUserId(userId);

        UserBoard userBoard = new UserBoard();
        userBoard.setActivityDefId(activity.getActivityDefId());
        appletLotteryActivity.setUserBoardConfig(new UserBoardConfig());

        boolean success = appletActivityRepository.addLotteryActivityInfo(appletLotteryActivity, userId, 0L, 16L);
        if (!success) {
            log.error("活动数据回载防重");
            return null;
        }

        return appletLotteryActivity;
    }

    protected abstract AppletActivity assembleLotteryActivity(AppletActivity appletActivity, Map<String, Object> params);

    protected abstract SimpleResponse activityPageVO(AppletActivity appletActivity, Map<String, Object> params);

}
