package mf.code.activity.domain.applet.repository;

import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;

/**
 * mf.code.activity.domain.applet.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 16:00
 */
public interface AppletActivityDefRepository {
	/**
	 * 通过id 获取 活动定义
	 * @param activityDefId
	 * @return
	 */
	AppletActivityDef findById(Long activityDefId);

	/**
	 * 通过shopId 活动定义类型, 获取 进行中的相应的活动定义
	 * @param shopId
	 * @param type
	 * @return
	 */
	AppletActivityDef findByShopIdType(Long shopId, Integer type);

	/**
	 * 通过id 获取 带有 看板 的 活动定义
	 * @param activityDefId
	 * @return
	 */
	AppletActivityDef findActivityDefBoardById(Long activityDefId);

	/**
	 * 通过id 获取 带有用户看板的 活动定义
	 * 用户看板信息
	 * <per>
	 *     1.已领奖人数
	 *     2.快成团人信息
	 *     3.进行中的活动集合
	 * </per>
	 * @param activityDefId
	 * @return
	 */
	AppletActivityDef findUserBoardById(Long activityDefId);

	/**
	 * 通过shopId 活动定义类型, 获取 最新下线-已结束的相应的活动定义
	 * @param shopId
	 * @param type
	 * @return
	 */
	AppletActivityDef findByShopIdTypeEndStatus(Long shopId, Integer type);
}
