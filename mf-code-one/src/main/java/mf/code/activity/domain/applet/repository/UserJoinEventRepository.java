package mf.code.activity.domain.applet.repository;

import mf.code.activity.domain.applet.aggregateroot.UserJoinEvent;

/**
 * mf.code.activity.domain.applet.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 19:13
 */
public interface UserJoinEventRepository {
	/**
	 * 保存 userActivity
	 * @param userJoinEvent
	 * @return
	 */
	boolean save(UserJoinEvent userJoinEvent);
}
