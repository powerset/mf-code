package mf.code.activity.domain.applet.valueobject;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-24 11:59
 */
public enum UserCompleteRecordStatusEnum {
	UNRECEIVED(0, "未领奖"),
	RECEIVIED(1, "已领奖"),
	;

	/**
	 * code
	 */
	private Integer code;

	/**
	 * 描述
	 */
	private String desc;

	UserCompleteRecordStatusEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public static Integer findByUserCouponStatus(Integer status) {
		return null;
	}
}
