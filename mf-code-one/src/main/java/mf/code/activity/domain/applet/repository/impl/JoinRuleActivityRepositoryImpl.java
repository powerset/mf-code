package mf.code.activity.domain.applet.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.JoinRuleActivity;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.domain.applet.valueobject.ActivityCompleteDimensionEnum;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.domain.applet.valueobject.UserCompleteRecord;
import mf.code.activity.domain.applet.valueobject.UserCompleteRecordStatusEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.common.constant.*;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.domain.applet.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-24 00:27
 */
@Slf4j
@Service
public class JoinRuleActivityRepositoryImpl implements JoinRuleActivityRepository {
	@Autowired
	private UserTaskService userTaskService;
	@Autowired
	private UserCouponService userCouponService;
	@Autowired
	private ActivityService activityService;

	/**
	 * 通过参与规则 获取 所有用户 符合参与规则的 参与规则活动
	 * @param joinRule
	 * @return
	 */
	@Override
	public JoinRuleActivity findByJoinRule(JoinRule joinRule) {
		return this.findByJoinRuleUserId(joinRule, null);
	}

	/**
	 * 通过参与规则 和用户id 获取 符合参与规则的此用户活动
	 * @param joinRule
	 * @param userId
	 * @return
	 */
	@Override
	public JoinRuleActivity findByJoinRuleUserId(JoinRule joinRule, Long userId) {
		// 按参与规则 完成的用户任务记录
		List<UserCompleteRecord> userCompleteRecordList = this.listUserCompleteRecordByJoinRule(joinRule, userId);
		Map<Long, List<UserCompleteRecord>> userCompleteRecordMap = new HashMap<>();
		for (UserCompleteRecord userCompleteRecord : userCompleteRecordList) {
			Long recordUserId = userCompleteRecord.getUserId();
			// 一个用户有多个已完成任务记录 先按用户分组
			List<UserCompleteRecord> userCompleteRecordListByUserId;
			if (userCompleteRecordMap.containsKey(recordUserId)) {
				userCompleteRecordListByUserId = userCompleteRecordMap.get(recordUserId);
			} else {
				userCompleteRecordListByUserId = new ArrayList<>();
			}
			userCompleteRecordListByUserId.add(userCompleteRecord);
			userCompleteRecordMap.put(recordUserId, userCompleteRecordListByUserId);
		}

		// 按参与规则 获取进行中的活动
		List<Activity> publishedActivityList = listPublishedByJoinRule(joinRule, userId);
		Map<Long, List<Activity>> publishedActivityMap = new HashMap<>();
		for (Activity activity : publishedActivityList) {
			Long activityUserId = activity.getUserId();
			// 一个用户有多个已完成任务记录 先按用户分组
			List<Activity> activityListByUserId;
			if (publishedActivityMap.containsKey(activityUserId)) {
				activityListByUserId = publishedActivityMap.get(activityUserId);
			} else {
				activityListByUserId = new ArrayList<>();
			}
			activityListByUserId.add(activity);
			publishedActivityMap.put(activityUserId, activityListByUserId);
		}

		JoinRuleActivity joinRuleActivity = new JoinRuleActivity();
		joinRuleActivity.setJoinRule(joinRule);
		joinRuleActivity.setCompleteRecordMap(userCompleteRecordMap);
		joinRuleActivity.setPublishedActivityMap(publishedActivityMap);

		return joinRuleActivity;
	}

	/**
	 * 增装用户最新一条活动记录
	 * @param joinRuleActivity
	 * @param userId
	 */
	@Override
	public void addLastUserActivity(JoinRuleActivity joinRuleActivity, Long userId) {
		JoinRule joinRule = joinRuleActivity.getJoinRule();

		ActivityCompleteDimensionEnum completeDimensionEnum = joinRule.getCompleteDimensionEnum();
		Integer activityDefType = joinRule.getActivityDefType();

		QueryWrapper<Activity> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Activity::getUserId, userId)
				.eq(Activity::getType, ActivityTypeEnum.findTypeByDefType(activityDefType));

		if (completeDimensionEnum == ActivityCompleteDimensionEnum.SHOP) {
			wrapper.lambda()
					.eq(Activity::getShopId, joinRule.getDimensionId());
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.ACTIVITYDEF) {
			wrapper.lambda()
					.eq(Activity::getActivityDefId, joinRule.getDimensionId());
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.GOODS) {
			wrapper.lambda()
					.eq(Activity::getGoodsId, joinRule.getDimensionId());
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.ORDER) {
			wrapper.lambda()
					.eq(Activity::getOrderId, joinRule.getDimensionId());
		}
		Page<Activity> page = new Page<>(0, 1);
		page.setDesc("id");
		IPage<Activity> activityIPage = activityService.page(page, wrapper);
		if (activityIPage == null) {
			joinRuleActivity.setLastUserActivityMap(new HashMap<>());
			return ;
		}
		List<Activity> records = activityIPage.getRecords();
		Map<Long, Activity> lastUserActivity = new HashMap<>();
		if (!CollectionUtils.isEmpty(records)) {
			lastUserActivity.put(userId, records.get(0));
		}
		joinRuleActivity.setLastUserActivityMap(lastUserActivity);
	}

	/**
	 * 按参与规则 获取进行中的活动
	 * @param joinRule
	 * @return
	 */
	private List<Activity> listPublishedByJoinRule(JoinRule joinRule, Long userId) {
		ActivityCompleteDimensionEnum completeDimensionEnum = joinRule.getCompleteDimensionEnum();
		Integer activityDefType = joinRule.getActivityDefType();

		QueryWrapper<Activity> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Activity::getType, ActivityTypeEnum.findTypeByDefType(activityDefType))
				.eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode());
		if (userId != null) {
			wrapper.lambda()
					.eq(Activity::getUserId, userId);
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.SHOP) {
			wrapper.lambda()
					.eq(Activity::getShopId, joinRule.getDimensionId());
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.ACTIVITYDEF) {
			wrapper.lambda()
					.eq(Activity::getActivityDefId, joinRule.getDimensionId());
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.GOODS) {
			wrapper.lambda()
					.eq(Activity::getGoodsId, joinRule.getDimensionId());
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.ORDER) {
			wrapper.lambda()
					.eq(Activity::getOrderId, joinRule.getDimensionId());
		}
		List<Activity> activityList = activityService.list(wrapper);

		return activityList == null? new ArrayList<>(): activityList;
	}

	/**
	 * 按参与规则，获取用户完成记录
	 * @param joinRule
	 * @return
	 */
	private List<UserCompleteRecord> listUserCompleteRecordByJoinRule(JoinRule joinRule, Long userId) {
		ActivityCompleteDimensionEnum completeDimensionEnum = joinRule.getCompleteDimensionEnum();

		Integer activityDefType = joinRule.getActivityDefType();
		boolean joinCount = joinRule.isJoinCount();

		List<UserCompleteRecord> userCompleteRecordList = new ArrayList<>();

		if (completeDimensionEnum == ActivityCompleteDimensionEnum.SHOP) {
			QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
			wrapper.lambda()
					.in(UserCoupon::getType, UserCouponTypeEnum.findByActivityDefType(activityDefType))
					.eq(UserCoupon::getShopId, joinRule.getDimensionId());
			if (!joinCount) {
				wrapper.lambda()
						.eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode());
			}
			if (userId != null) {
				wrapper.lambda()
						.eq(UserCoupon::getUserId, userId);
			}
			List<UserCoupon> userCouponList = userCouponService.list(wrapper);
			if (!CollectionUtils.isEmpty(userCouponList)) {
				for (UserCoupon userCoupon : userCouponList) {
					UserCompleteRecord userCompleteRecord = new UserCompleteRecord();
					userCompleteRecord.setUserId(userCoupon.getUserId());
					userCompleteRecord.setActivityDefId(userCoupon.getActivityDefId());
					userCompleteRecord.setActivityId(userCoupon.getActivityId());
					userCompleteRecord.setShopId(userCoupon.getShopId());
					userCompleteRecord.setStatus(userCoupon.getStatus());
					userCompleteRecord.setAmount(userCoupon.getAmount());
					userCompleteRecordList.add(userCompleteRecord);
				}
			}
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.ACTIVITYDEF) {
			QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
			wrapper.lambda()
					.eq(UserCoupon::getActivityDefId, joinRule.getDimensionId());
			if (!joinCount) {
				wrapper.lambda()
						.eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode());
			}
			if (userId != null) {
				wrapper.lambda()
						.eq(UserCoupon::getUserId, userId);
			}
			List<UserCoupon> userCouponList = userCouponService.list(wrapper);
			if (!CollectionUtils.isEmpty(userCouponList)) {
				for (UserCoupon userCoupon : userCouponList) {
					UserCompleteRecord userCompleteRecord = new UserCompleteRecord();
					userCompleteRecord.setUserId(userCoupon.getUserId());
					userCompleteRecord.setActivityDefId(userCoupon.getActivityDefId());
					userCompleteRecord.setActivityId(userCoupon.getActivityId());
					userCompleteRecord.setShopId(userCoupon.getShopId());
					userCompleteRecord.setStatus(userCoupon.getStatus());
					userCompleteRecord.setAmount(userCoupon.getAmount());
					userCompleteRecordList.add(userCompleteRecord);
				}
			}
		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.GOODS) {
			QueryWrapper<UserTask> wrapper = new QueryWrapper<>();
			wrapper.lambda()
					.in(UserTask::getType, UserTaskTypeEnum.findByActivityDefType(activityDefType))
					.eq(UserTask::getGoodsId, joinRule.getDimensionId());
			if (!joinCount) {
				wrapper.lambda()
						.eq(UserTask::getStatus, UserTaskStatusEnum.AWARD_SUCCESS.getCode());
			}
			if (userId != null) {
				wrapper.lambda()
						.eq(UserTask::getUserId, userId);
			}
			List<UserTask> userTaskList = userTaskService.list(wrapper);
			if (!CollectionUtils.isEmpty(userTaskList)) {
				for (UserTask userTask : userTaskList) {
					UserCompleteRecord userCompleteRecord = new UserCompleteRecord();
					userCompleteRecord.setUserId(userTask.getUserId());
					userCompleteRecord.setActivityDefId(userTask.getActivityDefId());
					userCompleteRecord.setActivityId(userTask.getActivityId());
					userCompleteRecord.setShopId(userTask.getShopId());
					userCompleteRecord.setGoodsId(userTask.getGoodsId());
					BigDecimal rpAmount = userTask.getRpAmount();
					userCompleteRecord.setAmount(rpAmount == null ? BigDecimal.ZERO : rpAmount);
					if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
						userCompleteRecord.setStatus(UserCompleteRecordStatusEnum.RECEIVIED.getCode());
					} else {
						userCompleteRecord.setStatus(UserCompleteRecordStatusEnum.UNRECEIVED.getCode());
					}
					userCompleteRecordList.add(userCompleteRecord);
				}
			}

		}
		if (completeDimensionEnum == ActivityCompleteDimensionEnum.ORDER) {
			QueryWrapper<UserTask> wrapper = new QueryWrapper<>();
			wrapper.lambda()
					.in(UserTask::getType, UserTaskTypeEnum.findByActivityDefType(activityDefType))
					.eq(UserTask::getOrderId, joinRule.getDimensionId());
			if (!joinCount) {
				wrapper.lambda()
						.eq(UserTask::getStatus, UserTaskStatusEnum.AWARD_SUCCESS.getCode());
			}
			if (userId != null) {
				wrapper.lambda()
						.eq(UserTask::getUserId, userId);
			}
			List<UserTask> userTaskList = userTaskService.list(wrapper);
			if (!CollectionUtils.isEmpty(userTaskList)) {
				for (UserTask userTask : userTaskList) {
					UserCompleteRecord userCompleteRecord = new UserCompleteRecord();
					userCompleteRecord.setUserId(userTask.getUserId());
					userCompleteRecord.setActivityDefId(userTask.getActivityDefId());
					userCompleteRecord.setActivityId(userTask.getActivityId());
					userCompleteRecord.setShopId(userTask.getShopId());
					userCompleteRecord.setOrderId(userTask.getOrderId());
					BigDecimal rpAmount = userTask.getRpAmount();
					userCompleteRecord.setAmount(rpAmount == null ? BigDecimal.ZERO : rpAmount);
					if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
						userCompleteRecord.setStatus(UserCompleteRecordStatusEnum.RECEIVIED.getCode());
					} else {
						userCompleteRecord.setStatus(UserCompleteRecordStatusEnum.UNRECEIVED.getCode());
					}
					userCompleteRecordList.add(userCompleteRecord);
				}
			}
		}

		return userCompleteRecordList;
	}
}
