package mf.code.activity.domain.applet.application;

import mf.code.common.simpleresp.SimpleResponse;

import java.util.Map;

/**
 * mf.code.activity.domain.applet.application
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-13 21:18
 */
public interface AppletLotteryActivityServiceAbstract {

	/**
	 * 发起 活动
	 *
	 * @param params
	 * @return
	 */
	SimpleResponse create(Map<String, Object> params);

	/**
	 * 参与 活动 主流程 -- 已拆红包活动为模板
	 * @param params
	 * @return
	 */
	SimpleResponse join(Map<String, Object> params);

	/**
	 * 获取活动详情
	 * @param params
	 * @return
	 */
	SimpleResponse queryActivityByParams(Map<String, Object> params);
}
