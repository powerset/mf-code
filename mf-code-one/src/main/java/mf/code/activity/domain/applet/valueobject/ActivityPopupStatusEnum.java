package mf.code.activity.domain.applet.valueobject;

/**
 * 用户进入首页或详情页 获取 拆红包弹框类型
 * <per>
 *     0. 什么都不弹
 *     1. 弹出 帮好友拆了1.2元，快去找好友帮拆你的红包吧 -- 活动详情页
 *     2. 弹出 您有一个红包待拆开 -- 活动列表
 *     3. 弹出 红包已被抢光了，下次早点来！ -- 活动详情页
 *     4. 弹出 任务失败，再来挑战一次 -- 活动详情页
 *     5. 弹出 你已有一个红包啦，不要贪心哦 -- 活动详情页
 *     6. 弹出 恭喜您！已拆得所有现金红包 -- 活动详情页
 *     7. 弹出 恭喜您！好友帮你拆出了红包 -- 活动详情页
 *     8. 弹出 分享给你一个5元红包，立即来拆开吧 -- 活动详情页
 *     9. 弹出 完成下单即可提现现金 -- 提现
 *     10.弹出 完成支付即可提现现金 -- 提现
 *     11.弹出 您的钱包中有5元可提现 还有10元待解锁 -- 提现
 *     12.弹出 你有10元红包待拆开 还差2个好友就能拆开啦 -- 首页和商品详情页
 *     13.弹出 你有10元红包待提现 -- 首页和商品详情页
 * </per>
 *
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-14 17:20
 */
public enum ActivityPopupStatusEnum {
	NONE(0, "什么都不弹"),
	HELP_OPEN(1, "帮好友拆了1.2元，快去找好友帮拆你的红包吧"),
	OPEN(2, "您有一个红包待拆开"),
	STOCK_ZERO(3, "红包已被抢光了，下次早点来！"),
	FAILED(4, "任务失败，再来挑战一次"),
	HAVE_ONE(5, "你已有一个红包啦，不要贪心哦"),
	AWARD(6, "恭喜您！已拆得所有现金红包"),
	GET_HELP(7, "恭喜您！好友帮你拆出了红包"),
	SHARE_OPEN(8, "分享给你一个5元红包，立即来拆开吧"),
	ORDER(9, "完成下单即可提现现金"),
	PAY(10, "完成支付即可提现现金"),
	UNLOCKED(11, "您的钱包中有5元可提现 还有10元待解锁"),
	FEW_FRIENDS(12, "你有10元红包待拆开 还差2个好友就能拆开啦"),
	PENDING_CASH(13, "你有10元红包待提现"),
	PENDING_STATE(14, "9,10,11 待定状态 -- 须进一步校验"),
	FAILED_OTHER(15, "非活动本人，进入到过期的活动中时 的弹窗"),
	ACTIVITY_ING(16, "进行中的活动"),
	;

	/**
	 * code
	 */
	private Integer code;

	/**
	 * 描述
	 */
	private String desc;

	ActivityPopupStatusEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	/**
	 * 此活动弹窗状态 是否需要活动详情信息
	 * @param openRedPackageActivityStatusEnum
	 * @return
	 */
	public static boolean isNeedActivityInfoByActivityStatusEnum(ActivityPopupStatusEnum openRedPackageActivityStatusEnum) {
		if (ActivityPopupStatusEnum.HELP_OPEN == openRedPackageActivityStatusEnum
				|| ActivityPopupStatusEnum.STOCK_ZERO == openRedPackageActivityStatusEnum
				|| ActivityPopupStatusEnum.FAILED == openRedPackageActivityStatusEnum
				|| ActivityPopupStatusEnum.HAVE_ONE == openRedPackageActivityStatusEnum
				|| ActivityPopupStatusEnum.AWARD == openRedPackageActivityStatusEnum
				|| ActivityPopupStatusEnum.GET_HELP == openRedPackageActivityStatusEnum
				|| ActivityPopupStatusEnum.SHARE_OPEN == openRedPackageActivityStatusEnum) {
			return true;
		}
		return false;
	}
}
