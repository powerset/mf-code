package mf.code.activity.domain.applet.aggregateroot;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.domain.applet.valueobject.ActivityDefBoard;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.domain.applet.valueobject.UserBoard;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.common.constant.*;
import mf.code.user.repo.po.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 15:48
 */
@Data
public class AppletActivityDef {
	/**
	 * 唯一标识
	 */
	private Long id;
	/**
	 * 活动定义信息
	 */
	private ActivityDef activityDefInfo;
	/**
	 * 活动定义看板 -- 记录活动定义进行的状态
	 */
	private ActivityDefBoard activityDefBoard;
	/**
	 * 活动定义 用户看板
	 */
	private UserBoard userBoard;
	/**
	 * 活动定义 用户参与规则
	 */
	private JoinRule joinRule;
	/**
	 * 活动定义 本身发起规则
	 */
	private CreateRule createRule;



	/**
	 * 能否从活动定义中创建活动
	 * @return true 充足， false 不足
	 *
	 * 检验条件
	 * <per>
	 *     1. 是否发布
	 *     2. 是否删除
	 *     3. 库存是否充足
	 * </per>
	 */
	public boolean canCreate() {
		if (ActivityDefStatusEnum.PUBLISHED.getCode() != this.activityDefInfo.getStatus()) {
			return false;
		}
		if (DelEnum.YES.getCode() == this.activityDefInfo.getDel()) {
			return false;
		}
		if (this.createRule.isCheckStock()) {
			if (this.activityDefInfo.getStock() <= 0) {
				return false;
			}
		}
		if (this.createRule.isCheckDeposit()) {
			if (this.createRule.isUnitGoodsPrice()) {
				BigDecimal goodsPrice = this.activityDefInfo.getGoodsPrice();
				BigDecimal needDeposit = goodsPrice.multiply(new BigDecimal("3"));
				return needDeposit.compareTo(this.activityDefInfo.getDeposit()) <= 0;
			} else {
				BigDecimal goodsPrice = this.activityDefInfo.getGoodsPrice();
				goodsPrice = goodsPrice == null ? BigDecimal.ZERO : goodsPrice;
				BigDecimal commission = this.activityDefInfo.getCommission();
				commission = commission == null ? BigDecimal.ZERO : commission;
				// 此处容错处理，因为活动定义创建时，拆红包活动会同时在commission和goodsPrice中设置相同的值
				if (this.activityDefInfo.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
					commission = commission.compareTo(goodsPrice) == 0 ? BigDecimal.ZERO : commission;
				}
				BigDecimal needDeposit = goodsPrice.add(commission);
				return needDeposit.compareTo(this.activityDefInfo.getDeposit()) < 0;
			}
		}
		return true;
	}

	/**
	 * 通用概念
	 * 1. 用户看板 查询 此活动定义 已经领取奖品的人数
	 * 2. 此活动定义下 已完成人数
	 * @return
	 */
	public Integer queryAlreadyCompleteNum() {
		return this.activityDefBoard.getCompleteRecordMap().size();
	}

	/**
	 * 查询 快完成任务的人数
	 * @return
	 */
	public Integer queryAlmostCompleteNum() {
		return this.userBoard.getAlmostUserInfoMap().values().size();
	}

	/**
	 * 按看板配置的展示限制 返回 用户信息
	 * @return
	 */
	public List<Object> queryAlmostCompleteUserInfo() {
		Object[] userInfos = this.userBoard.getAlmostUserInfoMap().values().toArray();
		// 按业务要求  进行时间倒序
		Arrays.sort(userInfos, (o1, o2) -> {
			JSONObject jo1 = JSON.parseObject(o1.toString());
			JSONObject jo2 = JSON.parseObject(o2.toString());
			Long to1 = jo1.getLong("time");
			Long to2 = jo2.getLong("time");
			if (to1-to2 > 0) {
				return 1;
			} else if (to1-to2 < 0) {
				return -1;
			} else {
				return 0;
			}
		});
		Integer size = this.userBoard.getConfig().getSize();
		size = userInfos.length <= size? userInfos.length: size;
		List<Object> userInfoList = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			userInfoList.add(userInfos[i]);
		}
		return userInfoList;
	}

	/**
	 * 通用概念
	 * 1. 用户看板 查询 正在追赶的人数
	 * 2. 进行中还未成团的数量
	 *
	 * @return
	 */
	public Integer queryPrusueNum() {
		int prusueNum = 0;
		for (List<Activity> activityList : this.activityDefBoard.getPublishedActivityMap().values()) {
			for (Activity activity : activityList) {
				if (null == activity.getAwardStartTime()) {
					prusueNum ++;
				}
			}
		}
		return prusueNum;
	}

	/**
	 * 从活动定义中创建用户活动
	 * @return
	 */
	public AppletActivity newActivity(User userInfo) {
		Date now = new Date();
		Activity activity = new Activity();
		activity.setMerchantId(this.activityDefInfo.getMerchantId());
		activity.setShopId(this.activityDefInfo.getShopId());
		activity.setActivityDefId(this.activityDefInfo.getId());
		activity.setUserId(userInfo.getId());
		activity.setPayPrice(this.activityDefInfo.getGoodsPrice());
		activity.setType(ActivityTypeEnum.findTypeByDefType(this.activityDefInfo.getType()));
		activity.setStockType(this.activityDefInfo.getStockType());
		activity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
		activity.setStartTime(now);
		if (this.activityDefInfo.getCondDrawTime() != null) {
			activity.setEndTime(new Date(now.getTime() + this.activityDefInfo.getCondDrawTime() * 60 * 1000L));
		}
		activity.setGoodsId(this.activityDefInfo.getGoodsId());
		activity.setHitsPerDraw(this.activityDefInfo.getHitsPerDraw());
		activity.setCondPersionCount(this.activityDefInfo.getCondPersionCount());
		activity.setMissionNeedTime(this.activityDefInfo.getMissionNeedTime());
		activity.setMerchantCouponJson(this.activityDefInfo.getMerchantCouponJson());
		// 关键词暂定一个，所以直接存储
		activity.setKeyWordsJson(this.activityDefInfo.getKeyWord());
		activity.setDel(DelEnum.NO.getCode());
		activity.setVersion(6);
		activity.setCtime(now);
		activity.setUtime(now);
		activity.setWeighting(this.activityDefInfo.getWeighting());

		// 对特定活动 额外处理
		if (this.activityDefInfo.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
			BigDecimal commission = this.activityDefInfo.getCommission();
			BigDecimal goodsPrice = this.activityDefInfo.getGoodsPrice();
			BigDecimal payPrice = commission.compareTo(BigDecimal.ZERO) == 0? goodsPrice: commission;
			activity.setPayPrice(payPrice);
		}

		if (this.activityDefInfo.getType() == ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
			activity.setStartTime(this.activityDefInfo.getStartTime());
			activity.setEndTime(this.activityDefInfo.getEndTime());
		}

		// 装配用户活动根
		AppletActivity appletActivity = new AppletActivity();
		appletActivity.setActivityDefId(this.activityDefInfo.getId());
		appletActivity.setActivityInfo(activity);
		appletActivity.setJoinRule(this.joinRule);
		appletActivity.setCreateRule(this.createRule);
		appletActivity.setUserInfo(userInfo);
		appletActivity.setVisitUserId(userInfo.getId());
		return appletActivity;
	}
}
