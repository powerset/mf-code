package mf.code.activity.domain.applet.valueobject;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 活动定义 用户看板 --- 助力活动 -- 如果新增其他活动的看板，则进行迭代抽象
 * 主要便于用户查看活动进行的状态
 * <per>
 *     1.活动总库存
 *     2.剩余库存
 *     3.剩余库存比率
 *     4.已领奖人数
 *     5.快成团人数
 *     6.快成团人信息
 *     7.进行中的活动集合
 * </per>
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 23:17
 */
@Data
public class UserBoard {
	/**
	 * 所属活动定义id
	 */
	private Long activityDefId;
	/**
	 * 配置
	 */
	private UserBoardConfig config = new UserBoardConfig();
	/**
	 * 快成团人信息集合Map<userId, userInfo>
	 */
	private Map<Long, Object> almostUserInfoMap = new HashMap<>();
}
