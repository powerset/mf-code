package mf.code.activity.domain.applet.valueobject;

import lombok.Data;
import mf.code.uactivity.repo.po.UserPubJoin;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-16 10:59
 */
@Data
public class ActivityInviteEvent {
	/**
	 * 活动id
	 */
	private Long activityId;
	/**
	 * 邀请者id
	 */
	private Long pubUserId;
	/**
	 * 被邀请者id
	 */
	private Long subUserId;
	/**
	 * 邀请记录信息
	 */
	private UserPubJoin userPubJoinInfo;
}
