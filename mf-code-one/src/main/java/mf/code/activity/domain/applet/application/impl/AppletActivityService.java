package mf.code.activity.domain.applet.application.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;
import mf.code.activity.domain.applet.aggregateroot.JoinRuleActivity;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * mf.code.activity.domain.applet.application
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-13 23:02
 */
@Slf4j
@Service
public class AppletActivityService {
    @Autowired
    protected AppletActivityDefRepository appletActivityDefRepository;
    @Autowired
    protected AppletActivityRepository appletActivityRepository;
    @Autowired
    protected JoinRuleActivityRepository joinRuleActivityRepository;
    @Autowired
    protected AppletUserRepository appletUserRepository;
    @Autowired
    protected StringRedisTemplate stringRedisTemplate;

    /**
     * 创建活动
     * 亦可 查询 用户进行中的活动或上次已完成的活动 (需建立在用户已无法发起新的活动的前提下)
     *
     * 因活动定义无法创建时 返回null
     * 无法创建的场景：
     * 1.无活动定义
     * 2.活动定义 库存不足、挂起、删除
     *
     * 因活动参与规则 无法创建时，返回用户进行中的活动或上次已完成的活动
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    protected AppletActivity createAppletActivity(Long activityDefId, Long userId) {
        AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(activityDefId);
        if (null == appletActivityDef) {
            log.error("查无此活动定义，activityDefId = {}", activityDefId);
            return null;
        }
        // 获取符合参与规则的进行中活动及已完成记录
        JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRuleUserId(appletActivityDef.getJoinRule(), userId);

        if (!joinRuleActivity.canJoinByUserId(userId)) {
            // 说明 已有进行中活动或不可再发起新活动了 处理返回参数 -- 各种情况判断

            // 获取用户进行中的活动列表
            List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
            Activity activity;
            if (CollectionUtils.isEmpty(activityList)) {
                // 无进行中活动，获取用户已完成的活动
                Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
                if (completedActivityId == null) {
                    log.error("逻辑异常，须排查");
                    return null;
                }
                AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
                if (completedActivity == null) {
                    log.error("查无此活动，activityId = {}", completedActivityId);
                    return null;
                }
                activity = completedActivity.getActivityInfo();
            } else {
                activity = activityList.get(0);
            }
            // 返回进行中活动 或 已完成的活动
            return appletActivityRepository.assemblyAppletActivity(activity);
        }

        // 活动是否可以发起 --- 会针对每个不同的活动类型 处理相应的发起活动规则
        if (!appletActivityDef.canCreate()) {
            log.info("库存不足");
            return null;
        }
        // 获取用户信息
        AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
        if (appletUser == null) {
            log.error("无效用户 userId = {}", userId);
            return null;
        }
        // 从活动定义中创建用户活动
        return appletActivityDef.newActivity(appletUser.getUserinfo());
    }

    /**
     * 查询用户进行中的活动或最近一次已完成的活动
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    protected AppletActivity queryCurrentUserActivity(Long activityDefId, Long userId) {

        AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(activityDefId);
        if (null == appletActivityDef) {
            log.error("查无此活动定义，activityDefId = {}", activityDefId);
            return null;
        }
        // 获取符合参与规则的进行中活动及已完成记录
        JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRuleUserId(appletActivityDef.getJoinRule(), userId);
        // 获取用户进行中的活动列表
        List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
        Activity activity;
        if (CollectionUtils.isEmpty(activityList)) {
            // 查询 用户 进行中的活动 或 已完成的活动-- 优先返回进行中的活动
            Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
            if (completedActivityId == null) {
                log.info("用户无进行中的活动 且 无最近一次已完成的活动");
                return null;
            }
            AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
            if (completedActivity == null) {
                log.error("查无此活动，activityId = {}", completedActivityId);
                return null;
            }
            activity = completedActivity.getActivityInfo();
        } else {
            activity = activityList.get(0);
        }
        // 返回进行中活动 或 已完成的活动
        return appletActivityRepository.assemblyAppletActivity(activity);
    }
}
