package mf.code.activity.domain.applet.valueobject.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.valueobject.ActivityCompleteDimensionEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.constant.ActivityDefTypeEnum;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * 用户参与活动的规则
 * 活动类型不同，参与维度不同，完成条件不同，则用户的参与要求不同
 * <p>
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-23 下午3:03
 */
@Slf4j
@Data
public class JoinRule {
    /**
     * 维度标识, 根据不同维度枚举，传入对应主键id
     */
    private String dimensionId;
    /**
     * 活动定义类型
     */
    private Integer activityDefType;
    /**
     * 可重复参与维度枚举 1店铺维度 2活动维度 3平台维度 4商品维度 5活动定义维度
     */
    private ActivityCompleteDimensionEnum completeDimensionEnum;
    /**
     * 可完成次数: 默认 1次
     */
    private Integer totalCount = 1;
    /**
     * 是否允许有多个进行中的活动: 默认 false
     */
    private boolean multiple = false;
    /**
     * 是否按参与次数 作为 完成次数
     */
    private boolean joinCount = false;
    /**
     * 可获得总金额 为null不校验，不为null 校验是否达到最大金额
     */
    private BigDecimal personalMaxAmount;
    /**
     * 本次活动 最小奖金
     */
    private BigDecimal perMinAmount;
    /**
     * 本次活动 最大奖金
     */
    private BigDecimal perMaxAmount;

    // 如果 是免单抽奖活动 请重写CompleteRule
    public JoinRule(ActivityDef activityDef) {
        this.activityDefType = activityDef.getType();
        this.completeDimensionEnum = ActivityCompleteDimensionEnum.findByActivityDefType(this.activityDefType);
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == this.activityDefType
                || ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == this.activityDefType) {
            // 免单抽奖活动，默认 0.1元发起，总发起次数3次
            this.totalCount = 3;
            this.multiple = false;
            this.joinCount = true;
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == this.activityDefType) {
            // 拆红包活动:不限制可完成次数
            this.totalCount = Integer.MAX_VALUE;
        }
        if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == this.activityDefType) {
            this.totalCount = Integer.MAX_VALUE;
            String amountJson = activityDef.getAmountJson();
            if (StringUtils.isBlank(amountJson)) {
                this.personalMaxAmount = BigDecimal.ZERO;
                this.perMinAmount = BigDecimal.ZERO;
                this.perMaxAmount = BigDecimal.ZERO;
            } else {
                JSONObject amountJo = JSON.parseObject(amountJson);
                JSONObject perAmount = amountJo.getJSONObject("perAmount");
                this.personalMaxAmount = new BigDecimal(amountJo.getString("personalMaxAmount"));

                this.perMinAmount = new BigDecimal(perAmount.getString("min"));
                this.perMaxAmount = new BigDecimal(perAmount.getString("max"));
            }
        }
        if (ActivityCompleteDimensionEnum.SHOP == this.completeDimensionEnum) {
            this.dimensionId = activityDef.getShopId().toString();
        }
        if (ActivityCompleteDimensionEnum.GOODS == this.completeDimensionEnum) {
            this.dimensionId = activityDef.getGoodsId().toString();
        }
        if (ActivityCompleteDimensionEnum.ACTIVITYDEF == this.completeDimensionEnum) {
            this.dimensionId = activityDef.getId().toString();
        }
    }

    // 主动定义参与规则以orderId为维度 -- 免单抽奖活动 如果是商品订单发起，则无限制发起次数，并允许多个进行中的活动
    public JoinRule(ActivityDef activityDef, String orderId) {
        this(activityDef.getType(), orderId);
    }

    // 通过活动 获取 参与规则
    public JoinRule(Activity activity) {
        this.activityDefType = ActivityDefTypeEnum.findByActivityType(activity.getType());
        this.completeDimensionEnum = ActivityCompleteDimensionEnum.findByActivityDefType(this.activityDefType);
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == this.activityDefType
                || ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == this.activityDefType) {
            // 免单抽奖活动，默认 0.1元发起，总发起次数3次
            this.totalCount = 3;
            this.multiple = false;
            this.joinCount = true;
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == this.activityDefType) {
            // 拆红包活动:不限制可完成次数
            this.totalCount = Integer.MAX_VALUE;
        }
        if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == this.activityDefType) {
            this.totalCount = Integer.MAX_VALUE;
        }
        if (ActivityCompleteDimensionEnum.SHOP == this.completeDimensionEnum) {
            this.dimensionId = activity.getShopId().toString();
        }
        if (ActivityCompleteDimensionEnum.GOODS == this.completeDimensionEnum) {
            this.dimensionId = activity.getGoodsId().toString();
        }
        if (ActivityCompleteDimensionEnum.ACTIVITYDEF == this.completeDimensionEnum) {
            this.dimensionId = activity.getId().toString();
        }
        if (ActivityCompleteDimensionEnum.ORDER == this.completeDimensionEnum) {
            this.dimensionId = activity.getOrderId();
            this.totalCount = 100;
            this.multiple = true;
            this.joinCount = true;
        }
    }

    // 主动定义参与规则以orderId为维度 并通过活动定义类型 获取参与规则 -- 适用 回填订单红包任务
    public JoinRule(Integer activityDefType, String orderId) {
        this.activityDefType = activityDefType;
        this.completeDimensionEnum = ActivityCompleteDimensionEnum.ORDER;
        this.dimensionId = orderId;
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == this.activityDefType
                || ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == this.activityDefType) {
            this.totalCount = 100;
            this.multiple = true;
            this.joinCount = true;
        }
    }

    // 主动定义参与规则以goodsId为维度 并通过活动定义类型 获取参与规则 -- 适用 好评返现
    public JoinRule(Integer activityDefType, Long goodsId) {
        this.activityDefType = activityDefType;
        this.completeDimensionEnum = ActivityCompleteDimensionEnum.GOODS;
        this.dimensionId = goodsId.toString();
    }

}
