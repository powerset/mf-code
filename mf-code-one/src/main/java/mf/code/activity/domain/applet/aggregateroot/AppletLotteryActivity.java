package mf.code.activity.domain.applet.aggregateroot;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.valueobject.AwardInfo;
import mf.code.api.AppletMybatisPageDto;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.goods.dto.ProductEntity;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.activity.domain.applet.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-14 17:51
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class AppletLotteryActivity extends AppletActivity{
    /**
     * 用户中奖概率 Map<userId, 中奖码数量>
     */
    private Map<Object, Object> userWinningProbability;
    /**
     * 中奖者 用户id
     */
    private List<Long> winnerIds;
    /**
     * 奖品池 -- 商品
     */
    private ProductEntity productEntity;
    /**
     * 活动客服
     */
    private Map<String, Object> serviceWx;
    /**
     * 活动参与人列表 -- 已支持分页获取
     */
    private AppletMybatisPageDto<Map> joinUserInfoPage;
    /**
     * 用户邀请人的活动参与人列表
     */
    private AppletMybatisPageDto<Map> pubJoinUserInfoPage;
    /**
     * 中奖者列表
     */
    private AppletMybatisPageDto<Map> winnerInfoPage;
    /**
     * 参与奖
     */
    private AwardInfo awardInfo;
    /**
     * 中奖者任务id
     */
    private UserTask userTask;

    /**
     * 获取所有参与者id object(string)
     */
    public List<Long> queryTotalJoinUserIdList() {
        if (CollectionUtils.isEmpty(this.userWinningProbability)) {
            return new ArrayList<>();
        }
        List<Long> userIdList = new ArrayList<>();
        for (Object obj : this.userWinningProbability.keySet()) {
            userIdList.add(Long.valueOf(obj.toString()));
        }
        return userIdList;
    }

    /**
     * 从参与事件中 -- 抽出中奖事件, 并发起中奖者任务
     *
     * @return 中奖者userid
     */
    public List<Long> drawLottery() {
        if (CollectionUtils.isEmpty(this.userWinningProbability)) {
            this.winnerIds = new ArrayList<>();
            return this.winnerIds;
        }

        int totalCount = 0;
        for (Object value : this.userWinningProbability.values()) {
            totalCount = totalCount + Integer.parseInt((String) value);
        }
        int times = this.activityInfo.getHitsPerDraw() <= this.userWinningProbability.size()? this.activityInfo.getHitsPerDraw() : this.userWinningProbability.size();

        this.winnerIds = new ArrayList<>();
        for (int i = 0; i < times; i++) {
            int start = 0;
            int end = 0;
            int count;
            int random = new Random().nextInt(totalCount);
            for (Map.Entry<Object, Object> entry : this.userWinningProbability.entrySet()) {
                Long userId = Long.parseLong((String) entry.getKey());
                count = Integer.parseInt((String) entry.getValue());
                end = end + count;
                if (random >= start && random < end) {
                    //中奖

                    // 已中奖过滤
                    if (this.winnerIds.contains(userId)) {
                        i--;
                        break;
                    }
                    // 新增中奖人
                    this.winnerIds.add(userId);
                    break;
                } else {
                    start = start + count;
                }
            }
        }

        return winnerIds;
    }

    public void publishWinnerTask() {
        Date now = new Date();
        for (Long userId : this.winnerIds) {
            UserJoinEvent userJoinEvent = this.userJoinEventMap.get(userId);
            if (userJoinEvent == null) {
                continue;
            }
            UserActivity userActivityInfo = userJoinEvent.getUserActivityInfo();
            userActivityInfo.setStatus(UserActivityStatusEnum.WINNER.getCode());
            userActivityInfo.setUtime(now);
            userJoinEvent.setUserActivityInfo(userActivityInfo);
            if (CollectionUtils.isEmpty(winEventMap)) {
                this.winEventMap = new HashMap<>();
            }
            this.winEventMap.put(userId, userJoinEvent);

            // 发起中奖者任务
            UserTask userTask = this.newUserTask(userActivityInfo);
            if (CollectionUtils.isEmpty(this.winnerTask)) {
                this.winnerTask = new HashMap<>();
            }
            this.winnerTask.put(userActivityInfo.getUserId(), userTask);
        }
    }

    /**
     * 获取访问者的中奖概率 -- 百分比
     * @return
     */
    public double queryUserWinningProbability() {
        if (CollectionUtils.isEmpty(this.userWinningProbability)) {
            return 0.00;
        }
        Collection<Object> values = this.userWinningProbability.values();
        int total = 0;
        for (Object obj : values) {
            int value = Integer.parseInt(obj.toString());
            total = total + value;
        }
        int size = this.activityInfo.getHitsPerDraw();

        if (this.activityInfo.getBatchNum() != null && this.activityInfo.getBatchNum() > this.activityInfo.getHitsPerDraw()) {
            total = total + (int)(activityInfo.getBatchNum() - activityInfo.getHitsPerDraw());
            size = this.activityInfo.getBatchNum().intValue();
        }
        Object visitorProbability = this.userWinningProbability.get(visitUserId.toString());
        if (visitorProbability == null) {
            return 0.00;
        }
        int visitorValue = Integer.parseInt(visitorProbability.toString());


        int probability = (visitorValue * 10000) / total * size;
        Double probabilityDouble = probability / 100d;

        return probabilityDouble.compareTo(0d) == 0 ? 0.01 : probabilityDouble;
    }

    /**
     * 是否是中奖人
     * @return
     */
    public boolean isWinner() {
        return this.winnerIds.contains(this.visitUserId);
    }
}
