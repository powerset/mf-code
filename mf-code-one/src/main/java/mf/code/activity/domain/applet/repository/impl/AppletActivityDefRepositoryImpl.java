package mf.code.activity.domain.applet.repository.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.valueobject.*;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.activity.domain.applet.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 15:54
 */
@Slf4j
@Service
public class AppletActivityDefRepositoryImpl implements AppletActivityDefRepository {
	@Autowired
	private ActivityDefService activityDefService;
	@Autowired
	private UserCouponService userCouponService;
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private ActivityService activityService;

	/**
	 * 通过id 获取 活动定义
	 * @param activityDefId
	 * @return
	 */
	@Override
	public AppletActivityDef findById(Long activityDefId) {
		ActivityDef activityDef = activityDefService.getById(activityDefId);
		if (activityDef == null) {
			log.error("活动定义不存在，activityDefId = {}", activityDefId);
			return null;
		}
		// 装配活动定义根
		return assemblyAppletActivityDef(activityDef);
	}

	/**
	 * 通过shopId 活动定义类型, 获取 进行中的相应的活动定义
	 * @param shopId
	 * @param type
	 * @return
	 * <per>
	 *     1. OpenRedPackageActivityDef
	 * </per>
	 */
	@Override
	public AppletActivityDef findByShopIdType(Long shopId, Integer type) {
		QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityDef::getShopId, shopId)
				.eq(ActivityDef::getType, type)
				.eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
				.eq(ActivityDef::getDel, DelEnum.NO.getCode());
		List<ActivityDef> activityDefList = activityDefService.list(wrapper);
		if (CollectionUtils.isEmpty(activityDefList)) {
			log.info("没有对应类型的活动定义在发布中，shopId = {}, type = {}", shopId, type);
			return null;
		}
		ActivityDef activityDef = activityDefList.get(0);
		// 装配活动定义根
		return assemblyAppletActivityDef(activityDef);
	}

	/**
	 * 通过id 获取 带有 看板 的 活动定义
	 * @param activityDefId
	 * @return
	 */
	@Override
	public AppletActivityDef findActivityDefBoardById(Long activityDefId) {
		// 获取 活动定义
		AppletActivityDef appletActivityDef = this.findById(activityDefId);
		if (appletActivityDef == null) {
			return null;
		}
		// 活动定义下 完成的用户任务记录
		List<UserCompleteRecord> userCompleteRecordList = this.listUserCompleteRecordById(activityDefId);
		Map<Long, List<UserCompleteRecord>> userCompleteRecordMap = new HashMap<>();
		for (UserCompleteRecord userCompleteRecord : userCompleteRecordList) {
			Long userId = userCompleteRecord.getUserId();
			// 一个用户有多个已完成任务记录 先按用户分组
			List<UserCompleteRecord> userCompleteRecordListByUserId;
			if (userCompleteRecordMap.containsKey(userId)) {
				userCompleteRecordListByUserId = userCompleteRecordMap.get(userId);
			} else {
				userCompleteRecordListByUserId = new ArrayList<>();
			}
			userCompleteRecordListByUserId.add(userCompleteRecord);
			userCompleteRecordMap.put(userId, userCompleteRecordListByUserId);
		}

		// 活动定义下 进行中的活动集合
		List<Activity> publishedActivityList = listPublishedActivityById(activityDefId);
		Map<Long, List<Activity>> publishedActivityMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(publishedActivityList)) {
			for (Activity activity : publishedActivityList) {
				Long userId = activity.getUserId();
				// 一个用户有多个已完成任务记录 先按用户分组
				List<Activity> activityListByUserId;
				if (publishedActivityMap.containsKey(userId)) {
					activityListByUserId = publishedActivityMap.get(userId);
				} else {
					activityListByUserId = new ArrayList<>();
				}
				activityListByUserId.add(activity);
				publishedActivityMap.put(userId, activityListByUserId);
			}
		}

		ActivityDefBoard activityDefBoard = new ActivityDefBoard();
		activityDefBoard.setActivityDefId(activityDefId);
		activityDefBoard.setPublishedActivityMap(publishedActivityMap);
		activityDefBoard.setCompleteRecordMap(userCompleteRecordMap);

		appletActivityDef.setActivityDefBoard(activityDefBoard);

		return appletActivityDef;
	}

	/**
	 * 通过id 获取 带有用户看板的 活动定义
	 * 用户看板信息
	 * <per>
	 *     1.已领奖人数
	 *     2.快成团人信息
	 *     3.进行中的活动集合
	 * </per>
	 * @param activityDefId
	 * @return
	 */
	@Override
	public AppletActivityDef findUserBoardById(Long activityDefId) {
		// 获取 活动定义
		AppletActivityDef appletActivityDef = this.findActivityDefBoardById(activityDefId);
		if (appletActivityDef == null) {
			return null;
		}

		// 快成团人信息集合
		List<Object> userInfoList = stringRedisTemplate.opsForHash().values(RedisKeyConstant.MF_ACTIVITY_ALMOST_COMPLETE + activityDefId);
		Map<Long, Object> userInfoMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(userInfoList)) {
			for (Object obj : userInfoList) {
				JSONObject userInfo = JSON.parseObject(obj.toString());
				Long userId = userInfo.getLong("uid");
				userInfoMap.put(userId, userInfo);
			}
		}

		// 装配用户看板
		UserBoard userBoard = new UserBoard();
		userBoard.setActivityDefId(activityDefId);
		userBoard.setAlmostUserInfoMap(userInfoMap);

		appletActivityDef.setUserBoard(userBoard);

		return appletActivityDef;
	}

	/**
	 * 通过shopId 活动定义类型, 获取 最新下线-已结束的相应的活动定义
	 * @param shopId
	 * @param type
	 * @return
	 */
	@Override
	public AppletActivityDef findByShopIdTypeEndStatus(Long shopId, Integer type) {
		QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityDef::getShopId, shopId)
				.eq(ActivityDef::getType, type)
				.in(ActivityDef::getStatus, Arrays.asList(ActivityDefStatusEnum.CLOSE.getCode()),
						ActivityDefStatusEnum.REFUNDED.getCode(),
						ActivityDefStatusEnum.END.getCode())
				.eq(ActivityDef::getDel, DelEnum.NO.getCode());

		Page<ActivityDef> page = new Page<>(0, 1);
		page.setDesc("id");
		IPage<ActivityDef> activityDefIPage = activityDefService.page(page, wrapper);
		if (activityDefIPage == null || CollectionUtils.isEmpty(activityDefIPage.getRecords())) {
			return null;
		}
		List<ActivityDef> activityDefList = activityDefIPage.getRecords();
		if (CollectionUtils.isEmpty(activityDefList)) {
			log.info("没有对应类型的活动定义发布过，shopId = {}, type = {}", shopId, type);
			return null;
		}
		ActivityDef activityDef = activityDefList.get(0);
		// 装配活动定义根
		return assemblyAppletActivityDef(activityDef);
	}

	/**
	 * 活动定义下 完成的用户任务记录
	 * @param activityDefId
	 * @return
	 */
	private List<UserCompleteRecord> listUserCompleteRecordById(Long activityDefId) {
		List<UserCompleteRecord> userCompleteRecordList = new ArrayList<>();

		QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(UserCoupon::getActivityDefId, activityDefId)
				.eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode());
		List<UserCoupon> completeUserCoupons = userCouponService.list(wrapper);

		if (!CollectionUtils.isEmpty(completeUserCoupons)) {
			for (UserCoupon userCoupon : completeUserCoupons) {
				UserCompleteRecord userCompleteRecord = new UserCompleteRecord();
				userCompleteRecord.setUserId(userCoupon.getUserId());
				userCompleteRecord.setActivityDefId(userCoupon.getActivityDefId());
				userCompleteRecord.setActivityId(userCoupon.getActivityId());
				userCompleteRecord.setShopId(userCoupon.getShopId());
				userCompleteRecord.setStatus(UserCompleteRecordStatusEnum.findByUserCouponStatus(userCoupon.getStatus()));
			}
		}

		return userCompleteRecordList;
	}

	/**
	 * 活动定义下 进行中的活动
	 * @param activityDefId
	 * @return
	 */
	private List<Activity> listPublishedActivityById(Long activityDefId) {
		QueryWrapper<Activity> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Activity::getActivityDefId, activityDefId)
				.eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode());
		return activityService.list(wrapper);
	}

	// 装配活动定义根
	private AppletActivityDef assemblyAppletActivityDef(ActivityDef activityDef) {
		AppletActivityDef appletActivityDef = new AppletActivityDef();
		appletActivityDef.setId(activityDef.getId());
		appletActivityDef.setActivityDefInfo(activityDef);
		appletActivityDef.setJoinRule(new JoinRule(activityDef));
		appletActivityDef.setCreateRule(new CreateRule(activityDef));
		return appletActivityDef;
	}
}
