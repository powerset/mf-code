package mf.code.activity.domain.applet.repository;

import mf.code.activity.domain.applet.aggregateroot.JoinRuleActivity;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;

/**
 * mf.code.activity.domain.applet.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-24 00:27
 */
public interface JoinRuleActivityRepository {

	/**
	 * 通过参与规则 获取 所有用户 符合参与规则的 参与规则活动
	 * @param joinRule
	 * @return
	 */
	JoinRuleActivity findByJoinRule(JoinRule joinRule);

	/**
	 * 通过参与规则 和用户id 获取 符合参与规则的此用户活动
	 * @param joinRule
	 * @param userId
	 * @return
	 */
	JoinRuleActivity findByJoinRuleUserId(JoinRule joinRule, Long userId);


	/**
	 * 增装用户最新一条活动记录
	 * @param joinRuleActivity
	 * @param userId
	 */
	void addLastUserActivity(JoinRuleActivity joinRuleActivity, Long userId);

}
