package mf.code.activity.domain.applet.valueobject;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-14 19:57
 */
public enum ScenesEnum {
	/**
	 * 首页和商品详情页
	 */
	HOME_PAGE(1,"首页和商品详情页"),

	/**
	 * 活动详情页
	 */
	ACTIVITY_PAGE(2,"活动详情页"),

	/**
	 * 提现页
	 */
	CASH_PAGE(3,"提现页"),
	;

	/**
	 * code
	 */
	private Integer code;

	/**
	 * 描述
	 */
	private String desc;

	ScenesEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public static ScenesEnum findById(Integer scenes) {
		for (ScenesEnum scenesEnum : ScenesEnum.values()) {
			if (scenesEnum.getCode().equals(scenes)) {
				return scenesEnum;
			}
		}
		// 默认活动详情页
		return ScenesEnum.ACTIVITY_PAGE;
	}
}
