package mf.code.activity.domain.applet.valueobject;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 用户活动完成记录
 *
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-24 11:32
 */
@Data
public class UserCompleteRecord {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 活动id
	 */
	private Long activityId;
	/**
	 * 活动定义id
	 */
	private Long activityDefId;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 奖品id
	 */
	private Long goodsId;
	/**
	 * 订单id
	 */
	private String orderId;
	/**
	 * 完成状态
	 */
	private Integer status;
	/**
	 * 完成金额
	 */
	private BigDecimal amount;
}
