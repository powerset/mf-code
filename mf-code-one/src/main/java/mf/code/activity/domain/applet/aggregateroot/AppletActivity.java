package mf.code.activity.domain.applet.aggregateroot;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.valueobject.*;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.common.constant.*;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.user.repo.po.User;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 15:09
 */
@Slf4j
@Data
public class AppletActivity {
	/**
	 * 活动唯一标识
	 */
	protected Long id;
	/**
	 * 活动定义id
	 */
	protected Long activityDefId;
	/**
	 * 本次查看活动的用户ID
	 */
	protected Long visitUserId;
	/**
	 * 活动信息
	 */
	protected Activity activityInfo;
	/**
	 * 发起者信息
	 */
	protected User userInfo;
	/**
	 * 发起人信息
	 */
	protected Map<String, Object> startUserInfo = new HashMap<>();
	/**
	 * 参与人信息
	 */
	protected Map<String, Object> joinUserInfo = new HashMap<>();
	/**
	 * 用户参与后的 参与人数
	 */
	protected Long trigger;
	/**
	 * 用户看板
	 */
	protected UserBoardConfig userBoardConfig;
	/**
	 * 活动 邀请 访问者的记录
	 */
	protected ActivityInviteEvent activityInviteEvent;
	/**
	 * 参与规则
	 */
	protected JoinRule joinRule;
	/**
	 * 活动定义 本身发起规则
	 */
	protected CreateRule createRule;
	/**
	 * 参与事件集合 -- Map<UserId, UserJoinEvent>
	 */
	protected Map<Long, UserJoinEvent> userJoinEventMap;
	/**
	 * 中奖事件集合 -- Map<UserId, UserJoinEvent>
	 */
	protected Map<Long, UserJoinEvent> winEventMap;
	/**
	 * 中奖者任务
	 */
	protected Map<Long, UserTask> winnerTask;

	/**
	 * 活动是否还在进行中
	 *
	 * @return true 进行中，false 已结束
	 */
	public boolean isPublished() {
		// 是否在发布中
		return ActivityStatusEnum.PUBLISHED.getCode() == this.activityInfo.getStatus();
	}

	/**
	 * 是否已开奖
	 * @return true 已开奖， false 未开奖
	 */
	public boolean isAwarded() {
		return null != this.activityInfo.getAwardStartTime();
	}

	/**
	 * 进行中的活动 是否已过期
	 * @return true 已过期，false 未过期
	 */
	public boolean isExpired() {
		// this.activityInfo.getCondPersionCount() == null 则按开奖时间 进行开奖 -- 不存在剩余时间的过期状态

		if (this.activityInfo.getCondPersionCount() != null && queryLeftTime() <= 0L) {
			this.activityInfo.setStatus(ActivityStatusEnum.END.getCode());
			this.activityInfo.setUtime(new Date());
			return true;
		}
		return ActivityStatusEnum.FAILURE.getCode() == this.activityInfo.getStatus();
	}

	/**
	 * 获取 活动 与当前时间的 过期时间差 -- 获取活动剩余时间
	 */
	public Long queryLeftTime() {
		return this.activityInfo.getEndTime().getTime() - System.currentTimeMillis();
	}

	/**
	 * 活动是否可参与
	 * <per>
	 * 1. 活动是否过期
	 * 2. 活动是否开奖
	 * </per>
	 *
	 * @return
	 */
	public boolean isValid() {
		if (this.activityInfo.getStatus() != ActivityStatusEnum.PUBLISHED.getCode()) {
			return false;
		}
		return this.activityInfo.getAwardStartTime() == null;
	}

	/**
	 * 用户是否可以参与活动
	 *
	 * @param userId
	 * @return
	 */
	public boolean canJoinByUserId(Long userId) {
		if (this.activityInfo.getUserId().equals(userId)) {
			return false;
		}
		return this.isValid();
	}

	/**
	 * 将用户成为参与者
	 * <per>
	 * userid：用户id
	 * nickName：用户呢称
	 * avatarUrl：用户头像
	 * awardNo：抽奖码
	 * time：参与时间
	 * </per>
	 *
	 * @param user
	 * @return
	 */
	public Map<String, Object> becomeMemberByUser(User user) {
		// 生成抽奖码
		long currentTime = System.currentTimeMillis();
		long awardNo = currentTime % 100000000;
		List<Long> awardNoList = new ArrayList<>();
		awardNoList.add(awardNo);

		Map<String, Object> member = new HashMap<>();
		member.put("uid", user.getId());
		member.put("nickName", user.getNickName());
		member.put("avatarUrl", user.getAvatarUrl());
		member.put("awardNo", awardNoList);
		member.put("time", currentTime);

		this.joinUserInfo = member;
		return member;
	}

	/**
	 * 活动的生命周期 -- 分，可用作redis缓存时长 -- 非活动剩余时间
	 *
	 * @return
	 */
	public int queryLifeCycle() {
		Date startTime = this.activityInfo.getStartTime();
		Date endTime = this.activityInfo.getEndTime();
		int condDrawTime = (int) ((endTime.getTime() - startTime.getTime()) / 1000 / 60);
		// 1440分 = 1天
		return condDrawTime < 1440 ? 1440 : condDrawTime;
	}

	/**
	 * 是否参与成功
	 *
	 * @param trigger 参与人数
	 * @return
	 */
	public boolean isJoinSuccess(Long trigger) {
		if (this.activityInfo.getCondPersionCount() == null) {
			this.trigger = trigger;
			return true;
		}
		if (this.activityInfo.getCondPersionCount() >= trigger) {
			this.trigger = trigger;
			return true;
		}
		return false;
	}

	/**
	 * 邀请成功
	 */
	public void inviteSuccess() {

		if (this.activityInviteEvent == null) {
			return ;
		}
		Date now = new Date();

		UserPubJoin userPubJoinInfo = this.activityInviteEvent.getUserPubJoinInfo();
		userPubJoinInfo.setSubActionStatus(UserPubJoinStatusEnum.JOINED.getCode());
		userPubJoinInfo.setSubActionTime(now);
		userPubJoinInfo.setUtime(now);
		this.activityInviteEvent.setUserPubJoinInfo(userPubJoinInfo);
	}

	/**
	 * 是否可以添加到 用户看板 的即将完成排行榜
	 *
	 * @param trigger 参与人数
	 * @return
	 */
	public boolean isBoardSoonCompleteRanking(Long trigger) {
		return trigger.intValue() == (this.activityInfo.getCondPersionCount() - this.userBoardConfig.getLeftNum());
	}

	/**
	 * 是否开奖
	 *
	 * @param trigger 参与人数
	 * @return
	 */
	public boolean isLottery(Long trigger) {
		// 则按开奖时间 开奖
		if (this.activityInfo.getCondPersionCount() == null) {
			if (this.activityInfo.getEndTime().getTime() <= System.currentTimeMillis()
					&& this.activityInfo.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
				Date now = new Date();

				this.activityInfo.setAwardStartTime(now);
				this.activityInfo.setUtime(now);
				if (!this.createRule.isActivityTask()) {
					this.activityInfo.setStatus(ActivityStatusEnum.END.getCode());
				}
				return true;
			}
			return false;
		}

		// 按成团人数 开奖
		if (trigger.intValue() == this.activityInfo.getCondPersionCount()) {
			Date now = new Date();

			this.activityInfo.setAwardStartTime(now);
			this.activityInfo.setUtime(now);
			if (!this.createRule.isActivityTask()) {
				this.activityInfo.setStatus(ActivityStatusEnum.END.getCode());
			}
			return true;
		}
		return false;
	}

	/**
	 * 从参与事件中 -- 抽出中奖事件, 并发起中奖者任务
	 *
	 * @return 中奖者userid
	 */
	public List<Long> drawLottery() {
		this.winEventMap = new HashMap<>();
		this.winnerTask = new HashMap<>();
		List<Long> winnerIds = new ArrayList<>();

		Date now = new Date();

		Integer type = this.activityInfo.getType();
		if (type == ActivityTypeEnum.ASSIST.getCode() || type == ActivityTypeEnum.ASSIST_V2.getCode()) {
			// 助力活动，中奖者 就是 活动发起者
			Long userId = activityInfo.getUserId();

			winnerIds.add(userId);
			UserJoinEvent userJoinEvent = this.userJoinEventMap.get(userId);
			UserActivity userActivityInfo = userJoinEvent.getUserActivityInfo();
			userActivityInfo.setStatus(UserActivityStatusEnum.WINNER.getCode());
			userActivityInfo.setUtime(now);
			userJoinEvent.setUserActivityInfo(userActivityInfo);
			this.winEventMap.put(userId, userJoinEvent);
			// 发起中奖者任务
			UserTask userTask = this.newUserTask(userActivityInfo);
			this.winnerTask.put(userActivityInfo.getUserId(), userTask);
			return winnerIds;
		}
		log.error("其他活动的抽奖逻辑，未完善 -- 目前只支持 助力活动");
		return null;
	}

	/**
	 * 从用户活动中创建用户参与活动记录
	 *
	 * @param userId
	 * @return
	 */
	public UserJoinEvent newUserJoinEvent(Long userId) {
		Date now = new Date();

		UserActivity userActivity = new UserActivity();
		userActivity.setUserId(userId);
		userActivity.setMerchantId(this.activityInfo.getMerchantId());
		userActivity.setShopId(this.activityInfo.getShopId());
		userActivity.setActivityId(this.activityInfo.getId());
		userActivity.setActivityDefId(this.activityInfo.getActivityDefId());
		userActivity.setType(UserActivityTypeEnum.findTypeByActivityType(this.activityInfo.getType(), this.activityInfo.getUserId(), userId));
		userActivity.setGoodsId(this.activityInfo.getGoodsId());
		userActivity.setStatus(UserActivityStatusEnum.NO_WIN.getCode());
		userActivity.setMerchantShopCouponId(0L);
		userActivity.setUserCouponId(0L);
		userActivity.setCtime(now);
		userActivity.setUtime(now);

		// 装配用户参与事件
		UserJoinEvent userJoinEvent = new UserJoinEvent();
		userJoinEvent.setUserId(userId);
		userJoinEvent.setActivityId(this.id);
		userJoinEvent.setUserActivityInfo(userActivity);

		if (CollectionUtils.isEmpty(this.userJoinEventMap)) {
			this.userJoinEventMap = new HashMap<>();
		}
		this.userJoinEventMap.put(userId, userJoinEvent);
		return userJoinEvent;
	}

	/**
	 * 发起中奖者任务
	 *
	 * @param userActivityInfo
	 */
	protected UserTask newUserTask(UserActivity userActivityInfo) {
		Date now = new Date();

		UserTask userTask = new UserTask();
		userTask.setType(UserTaskTypeEnum.findByActivityType(this.activityInfo.getType()));
		userTask.setUserId(userActivityInfo.getUserId());
		userTask.setKeyWords(getKeywordRandom());
		userTask.setMerchantId(this.activityInfo.getMerchantId());
		userTask.setShopId(this.activityInfo.getShopId());
		userTask.setActivityDefId(this.activityInfo.getActivityDefId());
		userTask.setActivityId(this.id);
		userTask.setGoodsId(this.activityInfo.getGoodsId());
		userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
		userTask.setUserActivityId(userActivityInfo.getId());
		userTask.setRpAmount(BigDecimal.ZERO);
		userTask.setRead(0);
		userTask.setStockFlag(0);
		userTask.setCtime(now);
		userTask.setUtime(now);
		return userTask;
	}

	/**
	 * 随机获取任务关键词
	 */
	private String getKeywordRandom() {
		// 获取关键字列表
		List<String> keyWordsList = JSONObject.parseArray(this.activityInfo.getKeyWordsJson(), String.class);
		// 当没有提供关键词列表，则自己初始化
		if (CollectionUtils.isEmpty(keyWordsList)) {
			keyWordsList = new ArrayList<>();
			keyWordsList.add("等待商家设置");
		}
		int keyWordsListSize = keyWordsList.size();

		return keyWordsList.get((int) (Math.random() * keyWordsListSize));
	}

	/**
	 * 通过此活动为原型 创建 新的拆红包活动
	 * @param user 访问者
	 * @return
	 */
	public AppletAssistActivity newOpenRedPackageActivity(User user) {
		Date now = new Date();

		Activity fromActivity = this.getActivityInfo();

		AppletAssistActivity openRedPackageActivity = new AppletAssistActivity();

		openRedPackageActivity.setScenesEnum(ScenesEnum.ACTIVITY_PAGE);
		openRedPackageActivity.setVisitUserId(user.getId());

		Activity toActivity = new Activity();

		// 从 appletActivity.copyPrototype()
		BeanUtils.copyProperties(this.activityInfo, toActivity);
		toActivity.setId(null);
		toActivity.setUserId(user.getId());
		toActivity.setAwardStartTime(null);
		toActivity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
		toActivity.setStartTime(now);
		toActivity.setEndTime(new Date(now.getTime() + (this.activityInfo.getEndTime().getTime() - this.activityInfo.getStartTime().getTime())));
		toActivity.setOrderId(null);
		toActivity.setVersion(6);
		toActivity.setUtime(now);
		toActivity.setCtime(now);
		toActivity.setDel(DelEnum.NO.getCode());
		toActivity.setType(ActivityTypeEnum.OPEN_RED_PACKET_START.getCode());

		openRedPackageActivity.setActivityDefId(toActivity.getActivityDefId());
		openRedPackageActivity.setActivityInfo(toActivity);
		openRedPackageActivity.setJoinRule(new JoinRule(toActivity));
		openRedPackageActivity.setCreateRule(new CreateRule(toActivity));

		UserBoard userBoard = new UserBoard();
		userBoard.setActivityDefId(toActivity.getActivityDefId());
		openRedPackageActivity.setUserBoardConfig(new UserBoardConfig());

		openRedPackageActivity.setTrigger(1L);
		openRedPackageActivity.calculationAssistAmountList();
		Map<String, Object> startUserInfo = openRedPackageActivity.becomeMemberByUser(user);
		if (!this.getActivityInfo().getUserId().equals(user.getId())) {
			// 建立邀请关系 -- 没有邀请关系
			startUserInfo.put("pubUserId", fromActivity.getUserId());
		}
		startUserInfo.put("awardPool", openRedPackageActivity.getOpenAmount());

		openRedPackageActivity.setStartUserInfo(startUserInfo);

		return openRedPackageActivity;
	}

	/**
	 * 获取发起活动时，应扣的保证金
	 *
	 * @return
	 */
	public BigDecimal queryReduceDeposit() {
		if (!this.createRule.isReduceDeposit()) {
			return BigDecimal.ZERO;
		}
		if (!this.createRule.isUnitGoodsPrice()) {
			return this.activityInfo.getPayPrice();
		}
		if (this.activityInfo.getDeposit() != null && this.activityInfo.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
			return this.activityInfo.getDeposit();
		}
		return this.activityInfo.getPayPrice().multiply(new BigDecimal(this.activityInfo.getCondPersionCount()));
	}

	/**
	 * 获取发起活动时，应扣的库存数
	 * @return
	 */
	public Integer queryReduceStock() {
		if (!this.createRule.isReduceStock()) {
			return 0;
		}
		if (!this.createRule.isStockByCondPersionCount() && !this.createRule.isStockByHitsPerDraw()) {
			return 1;
		} else if (this.createRule.isStockByHitsPerDraw()) {
			return this.activityInfo.getHitsPerDraw();
		} else {
			return this.activityInfo.getCondPersionCount();
		}
	}

	/**
	 * 开奖后 给到用户余额钱包的钱
	 * @return
	 */
	public BigDecimal queryPayAmount() {
		return this.activityInfo.getPayPrice();
	}
}
