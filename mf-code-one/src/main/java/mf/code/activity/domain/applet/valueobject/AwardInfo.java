package mf.code.activity.domain.applet.valueobject;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-16 11:15
 */
@Data
public class AwardInfo {
    /**
     * 奖品id
     */
    private Long id;
    /**
     * 奖品标题
     */
    private String title;
    /**
     * 奖品说明
     */
    private String desc;
    /**
     * 奖品图片
     */
    private String pic;
    /**
     * 奖品有效期
     */
    private String effectiveTime;
    /**
     * 奖品价值
     */
    private BigDecimal price;
}
