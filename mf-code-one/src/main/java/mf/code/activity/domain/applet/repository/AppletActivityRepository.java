package mf.code.activity.domain.applet.repository;

import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletLotteryActivity;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;

import java.util.List;

/**
 * mf.code.activity.domain.applet.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 17:10
 */
public interface AppletActivityRepository {

	/**
	 * 保存创建的活动和用户参与信息 -- 含扣库存逻辑 -- 事务支持
	 * @param appletActivity
	 * @return
	 */
	boolean saveCreateByStockType(AppletActivity appletActivity);

	/**
	 * 保存开奖的活动和用户参与信息以及用户任务信息 -- 含扣库存逻辑 -- 事务支持
	 * @param appletActivity
	 * @return
	 */
	boolean saveLottery(AppletActivity appletActivity);

	/**
	 * 通过id 获取
	 * @param activityId
	 * @return
	 */
	AppletActivity findById(Long activityId);

	/**
	 * 补全 用户参与事件
	 * @param appletActivity
	 */
	void findUserJoinEventByAppletActivity(AppletActivity appletActivity, List<Long> userIds);

	/**
	 * 通过 活动信息 装配 活动
	 * @param activity
	 * @return
	 */
	AppletActivity assemblyAppletActivity(Activity activity);

	/**
	 * 通过 活动信息 装配 拆红包活动
	 * @param activity 活动
	 * @param visitorId 访问者
	 * @param scenesEnum 页面场景
	 * @return
	 */
	AppletAssistActivity assemblyOpenRedPackageActivity(Activity activity, Long visitorId, ScenesEnum scenesEnum);

	/**
	 * 增装 拆红包用户信息
	 */
	void addAssistActivityInfo(AppletAssistActivity openRedPackageActivity);

	/**
	 * 拆红包活动帮拆后， 活动数据更新
	 * @param openRedPackageActivity
	 * @return
	 */
	boolean saveAssistAppletAssistActivity(AppletAssistActivity openRedPackageActivity);

	/**
	 * 保存 开奖的拆红包活动
	 * @param openRedPackageActivity
	 * @return
	 */
	boolean saveLotteryAppletAssistActivity(AppletAssistActivity openRedPackageActivity);

	/**
	 * 拆红包活动 过期更新
	 * @param openRedPackageActivity
	 * @return
	 */
	boolean saveExpiredOrCompleteOpenRedPackageActivity(AppletAssistActivity openRedPackageActivity);

	/**
	 * 通过访问者 增装 活动邀请访问者的记录
	 * @param appletActivity
	 * @param userId  邀请者
	 */
	void addActivityInviteEventByVisitor(AppletActivity appletActivity, Long userId, Long pubUserId);

	/**
	 * 增装 抽奖用户信息
	 * @param appletLotteryActivity
	 */
	boolean addLotteryActivityInfo(AppletLotteryActivity appletLotteryActivity, Long userId,  Long offset, Long size);

	/**
	 * 参与抽奖后， 活动数据更新 -- 含邀请逻辑
	 * @param appletLotteryActivity
	 * @param userId
	 * @param pubUserId
	 * @return
	 */
	boolean saveJoinLotteryActivity(AppletLotteryActivity appletLotteryActivity, Long userId, Long pubUserId);

	/**
	 * 抽奖活动 开奖后，增装 用户中奖概率信息
	 * @param appletLotteryActivity
	 */
	void addUserWinningProbability(AppletLotteryActivity appletLotteryActivity);

	/**
	 * 装配 抽奖活动
	 * @param appletActivity
	 * @param userId
	 * @return
	 */
	AppletLotteryActivity assemblyAppletLotteryActivity(AppletActivity appletActivity, Long userId);

	/**
	 * 装配 抽奖活动
	 * @param activity
	 * @return
	 */
	AppletLotteryActivity assemblyAppletLotteryActivity(Activity activity);
}
