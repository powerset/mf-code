package mf.code.activity.domain.applet.aggregateroot;

import lombok.Data;
import mf.code.activity.domain.applet.valueobject.*;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.common.constant.ActivityStatusEnum;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 符合 参与规则 的 所有进行中活动和已完成用户记录
 * TODO redis缓存
 *
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-24 00:15
 */
@Data
public class JoinRuleActivity {
	/**
	 * 参与规则
	 */
	private JoinRule joinRule;

	/**
	 * 按参与规则 完成的用户任务记录 Map<userId, List<UserCompleteRecord>>
	 */
	private Map<Long, List<UserCompleteRecord>> completeRecordMap;

	/**
	 * 按 参与规则 进行中的活动集合 Map<userId, List<Activity>>
	 */
	private Map<Long, List<Activity>> publishedActivityMap;

	/**
	 * 增装 用户最新一条活动记录 Map<UserId, Activity>
	 */
	private Map<Long, Activity> lastUserActivityMap;

	public JoinRuleActivity(){}

	/**
	 * 用户最新一条活动记录，是否 过期 -- 此方法需要先调用 joinRuleActivity.addLastUserActivity()方法
	 */
	public boolean isExpiredLastUserActivity(Long userId) {

		if (CollectionUtils.isEmpty(lastUserActivityMap)) {
			return false;
		}
		Activity activity = lastUserActivityMap.get(userId);
		if (activity == null) {
			return false;
		}
		return activity.getStatus().equals(ActivityStatusEnum.FAILURE.getCode());
	}

	/**
	 * 如果 参与规则 本身是按 活动定义为维度的，则可以直接从activityDefBoard中复制
	 */
	public JoinRuleActivity(JoinRule joinRule, ActivityDefBoard activityDefBoard) {
		this.joinRule = joinRule;
		this.completeRecordMap = activityDefBoard.getCompleteRecordMap();
		this.publishedActivityMap = activityDefBoard.getPublishedActivityMap();
	}

	/**
	 * 通过 用户id 查询用户进行中的活动 -- 按 参与规则 校验用户是否有进行中的活动
	 * @param userId
	 * @return
	 */
	public List<Activity> queryPublishedActivityByUserId(Long userId) {
		return this.publishedActivityMap.get(userId);
	}

	/**
	 * 通过 用户id 查询 已完成的任务 -- 按 参与规则 校验用户是否完成
	 * @return
	 */
	public List<UserCompleteRecord> queryUserCompleteRecordByUserId(Long userId) {
		return this.completeRecordMap.get(userId);
	}

	/**
	 * 按参与规则，用户是否可以参与活动
	 * @param userId
	 * @return true 可以参与 -- 说明用户可以发起新活动，false 不可参与 -- 说明已有进行中活动，或参与次数已满, 或 个人最大总金额已达标
	 */
	public boolean canJoinByUserId(Long userId) {
		List<Activity> activityList = this.queryPublishedActivityByUserId(userId);
		List<UserCompleteRecord> userCompleteRecordList = this.queryUserCompleteRecordByUserId(userId);

		if (this.joinRule.getPersonalMaxAmount() != null) {
			// 校验是否达到个人最大金额
			BigDecimal currentAmount = BigDecimal.ZERO;
			if (!CollectionUtils.isEmpty(activityList)) {
				for (Activity activity : activityList) {
					currentAmount = currentAmount.add(activity.getPayPrice());
				}
			}
			if (!CollectionUtils.isEmpty(userCompleteRecordList)) {
				for (UserCompleteRecord userCompleteRecord : userCompleteRecordList) {
					currentAmount = currentAmount.add(userCompleteRecord.getAmount());
				}
			}
			if (this.joinRule.getPersonalMaxAmount().compareTo(currentAmount) <= 0) {
				return false;
			}
		}


		int activitySize = activityList == null? 0: activityList.size();
		int completeRecordSize = userCompleteRecordList == null? 0: userCompleteRecordList.size();

		int joinCount = activitySize + completeRecordSize;
		Integer totalCount = this.joinRule.getTotalCount();
		boolean multiple = this.joinRule.isMultiple();

		if (joinCount < totalCount) {
			// 已存在进行中活动 并且 允许 多条进行中活动 -- 可参加
			if (activitySize != 0 && multiple) {
				return true;
			}
			// 已存在进行中活动 但 不允许 多条进行中的活动 -- 不可参加，返回进行中的活动id
			// 不存在进行中的活动 -- 可参加
			return activitySize == 0;
		}

		// 已参与次数 > 可参与次数，则如果存在进行中，返回进行中活动的id，如果不存在，返回已完成活动的id
		return false;
	}

	/**
	 * 查询 用户 进行中的活动 或 已完成的活动-- 优先返回进行中的活动
	 * @param userId
	 * @return
	 */
	public Long queryPublishedOrCompletedActivityIdByUserId(Long userId) {
		if (this.canJoinByUserId(userId)) {
			// 用户还可参与活动
			return null;
		}
		List<Activity> activityList = this.queryPublishedActivityByUserId(userId);
		List<UserCompleteRecord> userCompleteRecordList = this.queryUserCompleteRecordByUserId(userId);

		return CollectionUtils.isEmpty(activityList)? userCompleteRecordList.get(0).getActivityId(): activityList.get(0).getId();
	}
}
