package mf.code.activity.domain.applet.valueobject.config;

import lombok.Data;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;

/**
 * 活动定义本身 发起规则配置
 * 不同的活动定义类型 又不同的发起规则：
 * 如：有的活动定义发起要校验库存是否充足，有的活动定义发起要校验保证金是否充足，还有的活动定义即要校验库存是否充足又要校验保证金是否充足
 * 另外 还存在未知的活动发起校验规则 --- 可扩展
 *
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-02 22:49
 */
@Data
public class CreateRule {
	/**
	 * 发起是否校验库存 true 校验， false 不校验
	 */
	private boolean checkStock;
	/**
	 * 发起是否校验保证金额 true 校验， false 不校验
	 */
	private boolean checkDeposit;
	/**
	 * 活动定义配置的时间 cond_draw_time 是否是 每个助力人的单位时间
	 * true: 则活动总时间 = cond_draw_time * 总需助力人数
	 */
	private boolean unitCondDrawTime;
	/**
	 * 活动定义配置的奖励 goods_price 是否是 单位金额 -- 用于活动的发起库存校验
	 * true: 则最小奖励金额 = goods_price * 发奖人数 hits_per_draw
	 */
	private boolean unitGoodsPrice;
	// ------------------ 活动入库相关 ----------------------------
	/**
	 * 活动入库时，是否扣保证金 -- 用于活动的发起库存的实际扣减
	 * 当reduceDeposit为true时
	 * unitGoodsPrice 为false 时，所扣保证金额额度为 goodsPrice
	 * 且unitGoodsPrice为true时则，，所扣保证金额额度为 最大奖励金额 = goods_price * condPersionCount
	 */
	private boolean reduceDeposit;
	/**
	 * 活动入库时，是否扣库存
	 */
	private boolean reduceStock;
	/**
	 * 是否按活动定义的 hits_per_draw 来进行扣减库存数，  此与 stockByCondPersionCount 互斥
	 * 前提 reduceStock 为 true
	 * stockByHitsPerDraw为 true 时，按 hitsPerDraw 扣减库存
	 * stockByHitsPerDraw为 false 时，stockByCondPersionCount false 时，则会扣减 1 个库存
	 */
	private boolean stockByHitsPerDraw;
	/**
	 * 是否按 活动开团人数扣减库存数 此与 stockByHitsPerDraw 互斥
	 * 前提 reduceStock 为 true
	 * stockByCondPersionCount true 时，按 condPersionCount 扣减库存
	 * stockByCondPersionCount false 时，stockByHitsPerDraw为 false 时，则会扣减 1 个库存
	 */
	private boolean stockByCondPersionCount;
	/**
	 * 回退保证金或库存时，是否减去 已助力的金额 和 库存
	 */
	private boolean subtractAssist;
	// ------ 助力 ----
	/**
	 * 是否助力后直接发起
	 */
	private boolean assistCreate;
	/**
	 * 发起者 是否 可参与（助力）自己的活动
	 */
	private boolean joinSelf;
	/**
	 * 是否 活动开奖后的任务
	 */
	private boolean activityTask;

	/**
	 * 通过活动定义 生成 创建规则
	 * @param activityDef
	 */
	public CreateRule(ActivityDef activityDef) {
		// 默认配置：活动发起时 只校验库存; 活动入库时 库存减1
		this.defaultConfig();

		Integer activityDefType = activityDef.getType();
		if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == activityDefType) {
			// 此配置含义为：活动发起时 只校验保证金(剩余保证金 > 最小奖励金额)； 活动入库时 扣除 最大奖励金额
			this.checkStock = false;
			this.checkDeposit = true;
			this.unitCondDrawTime = true;
			this.unitGoodsPrice = true;
			this.reduceDeposit = true;
			this.stockByCondPersionCount = true;
			this.subtractAssist = true;
		}
		if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDefType) {
			// 此配置含义为：活动发起时 只校验库存；活动入库时 扣库存数1 且 扣保证金额额度为 payPrice；
			this.assistCreate = true;
			this.joinSelf = true;
			this.reduceDeposit = true;
			this.activityTask = true;
		}
		if (ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode() == activityDefType) {
			// 此配置含义为：活动发起时 只校验库存；活动入库时 扣库存数1 且 扣保证金额额度为 payPrice；
			this.stockByHitsPerDraw = true;
		}
	}

	public CreateRule(Activity activity) {
		// 默认配置：活动发起时 只校验库存; 活动入库时 库存减1
		this.defaultConfig();

		Integer activityType = activity.getType();
		if (ActivityTypeEnum.FULL_REIMBURSEMENT.getCode() == activityType) {
			// 此配置含义为：活动发起时 只校验保证金(剩余保证金 > 最小奖励金额)； 活动入库时 扣除 最大奖励金额
			this.checkStock = false;
			this.checkDeposit = true;
			this.unitCondDrawTime = true;
			this.unitGoodsPrice = true;
			this.reduceDeposit = true;
			this.stockByCondPersionCount = true;
			this.subtractAssist = true;
		}
		if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == activityType
				|| ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode() == activityType) {
			// 此配置含义为：活动发起时 只校验库存；活动入库时 扣库存数1 且 扣保证金额额度为 payPrice；
			this.assistCreate = true;
			this.joinSelf = true;
			this.reduceDeposit = true;
			this.activityTask = true;
		}
		if (ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode() == activityType) {
			// 此配置含义为：活动发起时 只校验库存；活动入库时 扣库存数1 且 扣保证金额额度为 payPrice；
			this.stockByHitsPerDraw = true;
		}
	}

	private void defaultConfig() {
		// 默认配置：活动发起时 只校验库存; 活动入库时 库存减1
		this.checkStock = true;
		this.checkDeposit = false;
		this.unitCondDrawTime = false;
		this.unitGoodsPrice = false;
		this.reduceDeposit = false;
		this.reduceStock = true;
		this.stockByHitsPerDraw = false;
		this.stockByCondPersionCount = false;
		this.subtractAssist = false;
		this.assistCreate = false;
		this.joinSelf = false;
		this.activityTask = false;
	}
}
