package mf.code.activity.domain.applet.application.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.*;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.domain.applet.application.AppletAssistActivityServiceAbstract;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.domain.applet.valueobject.ActivityPopupStatusEnum;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.domain.applet.valueobject.UserBoard;
import mf.code.activity.domain.applet.valueobject.UserBoardConfig;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.one.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.activity.domain.applet.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-04 14:09
 */
@Slf4j
@Service
public abstract class AppletAssistActivityServiceAbstractImpl extends AppletActivityService implements AppletAssistActivityServiceAbstract {
	@Autowired
	private AppletActivityDefRepository appletActivityDefRepository;
	@Autowired
	private AppletActivityRepository appletActivityRepository;
	@Autowired
	private JoinRuleActivityRepository joinRuleActivityRepository;
	@Autowired
	private AppletUserRepository appletUserRepository;
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	/**
	 * 获取活动详情
	 * @param params
	 * @return
	 */
	public SimpleResponse queryActivityByParams(Map<String, Object> params) {
		Object activityIdParam = params.get("activityId");
		Long userId = Long.valueOf(params.get("userId").toString());
		Long shopId = Long.valueOf(params.get("shopId").toString());
		Integer scenes = Integer.valueOf(params.get("scenes").toString());
		Integer activityDefType = Integer.valueOf(params.get("activityDefType").toString());
		ScenesEnum scenesEnum = ScenesEnum.findById(scenes);
		Long activityId = null;

		if (activityIdParam != null && RegexUtils.StringIsNumber(activityIdParam.toString())) {
			activityId = Long.valueOf(activityIdParam.toString());
		}

		// 以下是获取 自己的活动详情
		AppletActivityDef appletActivityDef = appletActivityDefRepository.findByShopIdType(shopId, activityDefType);
		if (appletActivityDef == null) {
			// 如果访问的是别人的活动，则展示别人的活动详情
			if (activityId != null) {
				AppletActivity appletActivity = appletActivityRepository.findById(activityId);
				if (appletActivity != null) {
					// 装配活动 -- 访问他人活动
					AppletAssistActivity appletAssistActivity = appletActivityRepository.assemblyOpenRedPackageActivity(appletActivity.getActivityInfo(), userId, scenesEnum);
					appletActivityRepository.addAssistActivityInfo(appletAssistActivity);

					if (appletAssistActivity.isExpired()) {
						// 活动过期
						// 补全 用户参与事件
						if (appletAssistActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
							appletActivityRepository.findUserJoinEventByAppletActivity(appletAssistActivity, null);
							appletAssistActivity.setExpired();
							appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(appletAssistActivity);
						}
					}
					return activityPageVO(appletAssistActivity, params);
				}
			}
			log.info("没有对应类型的活动定义在发布中");
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "来晚了，活动已经结束啦");
		}
		// 按活动参与规则，获取用户进行中或已完成活动
		JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRule(appletActivityDef.getJoinRule());
		// 判断用户是否能参与活动定义
		if (joinRuleActivity.canJoinByUserId(userId)) {
			// 如果访问的是别人的活动，则展示别人的活动详情
			if (activityId != null) {
				AppletActivity appletActivity = appletActivityRepository.findById(activityId);
				if (appletActivity != null) {
					// 装配活动 -- 访问他人活动
					AppletAssistActivity appletAssistActivity = appletActivityRepository.assemblyOpenRedPackageActivity(appletActivity.getActivityInfo(), userId, scenesEnum);
					appletActivityRepository.addAssistActivityInfo(appletAssistActivity);

					if (appletAssistActivity.isExpired()) {
						// 活动过期
						// 补全 用户参与事件
						if (appletAssistActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
							appletActivityRepository.findUserJoinEventByAppletActivity(appletAssistActivity, null);
							appletAssistActivity.setExpired();
							appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(appletAssistActivity);
						}
					}
					return activityPageVO(appletAssistActivity, params);
				}
			}

			// 没有进行中活动且可发起新活动 -- 且不是进入别人的活动
			if (appletActivityDef.canCreate()) {
				// 增装用户 最新一条活动记录
				joinRuleActivityRepository.addLastUserActivity(joinRuleActivity, userId);

				// 可以参与 -- 说明，可以发起新活动
				ActivityDef activityDef = appletActivityDef.getActivityDefInfo();
				BigDecimal commission = activityDef.getCommission();
				BigDecimal goodsPrice = activityDef.getGoodsPrice();
				BigDecimal activityAmount = commission.compareTo(BigDecimal.ZERO) == 0 ? goodsPrice : commission;

				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityDefId", appletActivityDef.getId());
				resultVO.put("activityStatus", ActivityPopupStatusEnum.OPEN.getCode());

				if (joinRuleActivity.isExpiredLastUserActivity(userId) && scenes.equals(ScenesEnum.ACTIVITY_PAGE.getCode())) {
					resultVO.put("activityStatus", ActivityPopupStatusEnum.FAILED.getCode());
				}
				resultVO.put("money", activityAmount);
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
			log.info("库存不足");
			Map<String, Object> resultVO = new HashMap<>();
			resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		// 说明 已有进行中活动或不可再发起新活动了 处理返回参数 -- 各种情况判断


		// 获取用户进行中的活动列表
		List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
		Activity activity;
		if (CollectionUtils.isEmpty(activityList)) {
			// 无进行中活动，获取用户已完成的活动
			Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
			if (completedActivityId == null) {
				log.error("openRedPackagePopup逻辑异常，须排查");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "openRedPackagePopup逻辑异常，须排查");
			}
			AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
			Activity activityInfo = completedActivity.getActivityInfo();
			if (activityInfo == null) {
				log.error("openRedPackagePopup 查无此活动，activityId = {}", completedActivityId);
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
			}
			activity = activityInfo;
		} else {
			activity = activityList.get(0);
		}
		// 装配活动
		AppletAssistActivity appletAssistActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, userId, scenesEnum);
		appletActivityRepository.addAssistActivityInfo(appletAssistActivity);

		if (appletAssistActivity.isExpired()) {
			// 活动过期
			// 补全 用户参与事件
			if (appletAssistActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
				appletActivityRepository.findUserJoinEventByAppletActivity(appletAssistActivity, null);
				appletAssistActivity.setExpired();
				appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(appletAssistActivity);
			}
		}
		return activityPageVO(appletAssistActivity, params);
	}

	/**
	 * 发起 活动 主流程
	 *
	 * @param params
	 * @return
	 */
	@Override
	public SimpleResponse create(Map<String, Object> params) {
		Long activityDefId = Long.valueOf(params.get("activityDefId").toString());
		Long userId = Long.valueOf(params.get("userId").toString());

		AppletActivity appletActivity = this.createAppletActivity(activityDefId, userId);
		if (appletActivity == null) {
			log.info("activityStatus:3, 库存不足");
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "库存不足");
		}
		if (appletActivity.getId() != null) {
			log.info("activityStatus:5, 已有任务，无法发起");
			Map<String, Object> resultVO = new HashMap<>();
			resultVO.put("activityStatus", ActivityPopupStatusEnum.HAVE_ONE.getCode());
			resultVO.put("message", "已有任务，无法发起");
			resultVO.put("activityId", appletActivity.getId());
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2, resultVO);
		}
		AppletActivity appletAssistActivity = this.assembleNewActivity(appletActivity, params);
		if (appletAssistActivity == null) {
			log.info("参数异常");
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "参数异常");
		}
		// 生成用户参与事件
		appletAssistActivity.newUserJoinEvent(userId);

		// 持久化活动状态 -- 事务
		boolean save = appletActivityRepository.saveCreateByStockType(appletAssistActivity);
		if (!save) {
			log.info("activityStatus:3, 库存不足");
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "库存不足");
		}
		return activityPageVO(appletAssistActivity, params);
	}

	/**
	 * 助力 活动 主流程 -- 已拆红包活动为模板
	 * @param params
	 * @return
	 */
	@Override
	public SimpleResponse assist(Map<String, Object> params) {
		Long userId = Long.valueOf(params.get("userId").toString());
		Long activityId = Long.valueOf(params.get("activityId").toString());
		AppletAssistActivity appletAssistActivity = this.findByActivityIdVisitorId(activityId, userId);
		if (appletAssistActivity == null) {
			log.error("查无此活动，activityId = {}", activityId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此活动");
		}

		// 要展示活动详情的活动
		AppletAssistActivity showAppletAssistActivity = appletAssistActivity;
		if (!this.canAssistByVisitorId(appletAssistActivity, userId)) {
			// 此活动已不可助力
			if (appletAssistActivity.getCreateRule().isAssistCreate()) {
				// TODO 助力并创建
				log.error("此方法待支持");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
			} else {
				// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
				AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
				if (visitorActivity != null) {
					showAppletAssistActivity = visitorActivity;
				}
			}
			// 自己有活动 -- 则返回自己的活动 ，没有则返回 访问的活动
			return activityPageVO(showAppletAssistActivity, params);
		}
		// TODO: 按帮拆规则 对用户帮拆资格进行 校验 -- 封装成 选取 助力规则
		if (appletAssistActivity.getActivityInfo().getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
			String redisValue = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_ASSISTABLE_KEY + activityId + ":" + userId);
			if (StringUtils.isBlank(redisValue)) {
				log.info("无助力资格");
				if (appletAssistActivity.getCreateRule().isAssistCreate()) {
					// TODO 助力并创建
					log.error("此方法待支持");
					return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
				} else {
					// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
					AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
					if (visitorActivity != null) {
						showAppletAssistActivity = visitorActivity;
					}
				}
				return activityPageVO(showAppletAssistActivity, params);
			}
			stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_ASSISTABLE_KEY + userId);
			// 自己有活动 -- 则返回自己的活动 ，没有则返回 访问的活动

		} else {
			// 按帮拆规则 对用户帮拆资格进行 校验
			boolean success = RedisForbidRepeat.NX(stringRedisTemplate, mf.code.uactivity.repo.redis.RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + appletAssistActivity.getActivityInfo().getShopId() + ":" + userId, 365, TimeUnit.DAYS);
			if (!success) {
				log.info("此店铺已帮拆，无法再帮拆， shopId= {}, userId = {}", appletAssistActivity.getActivityInfo().getShopId(), userId);
				if (appletAssistActivity.getCreateRule().isAssistCreate()) {
					// TODO 助力并创建
					log.error("此方法待支持");
					return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
				} else {
					// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
					AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
					if (visitorActivity != null) {
						showAppletAssistActivity = visitorActivity;
					}
				}
				return activityPageVO(showAppletAssistActivity, params);
			}
		}

		// 活动计数
		Long trigger = stringRedisTemplate.opsForValue().increment(mf.code.uactivity.repo.redis.RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
		stringRedisTemplate.expire(mf.code.uactivity.repo.redis.RedisKeyConstant.ACTIVITY_TRIGGER + activityId, appletAssistActivity.queryLifeCycle(), TimeUnit.MINUTES);
		if (trigger == null) {
			log.error("活动计数异常，redis异常");
			stringRedisTemplate.opsForValue().increment(mf.code.uactivity.repo.redis.RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
			// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
			if (appletAssistActivity.getCreateRule().isAssistCreate()) {
				// TODO 助力并创建
				log.error("此方法待支持");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
			} else {
				// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
				AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
				if (visitorActivity != null) {
					showAppletAssistActivity = visitorActivity;
				}
			}
			return activityPageVO(showAppletAssistActivity, params);
		}
		// 是否参与成功
		if (!appletAssistActivity.isJoinSuccess(trigger)) {
			log.info("活动已完成，activityId = {}", activityId);
			// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
			if (appletAssistActivity.getCreateRule().isAssistCreate()) {
				// TODO 助力并创建
				log.error("此方法待支持");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
			} else {
				// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
				AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
				if (visitorActivity != null) {
					showAppletAssistActivity = visitorActivity;
				}
			}
			return activityPageVO(showAppletAssistActivity, params);
		}
		// 获取用户信息
		AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
		if (appletUser == null) {
			log.error("无效用户, userId = {}", userId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "无效用户");
		}
		// 通过访问者 补全 活动邀请访问者的记录
		appletActivityRepository.addActivityInviteEventByVisitor(appletAssistActivity, appletUser.getUserId(), appletAssistActivity.getActivityInfo().getUserId());
		appletAssistActivity.inviteSuccess();

		// 将用户成为参与者 -- 获取抽奖码
		Map<String, Object> member = appletAssistActivity.becomeMemberByUser(appletUser.getUserinfo());
		// 生成用户参与事件
		appletAssistActivity.newUserJoinEvent(userId);

		// 持久化活动状态 -- 事务 -- TODO: 对用户参与进行记录
		boolean save = appletActivityRepository.saveAssistAppletAssistActivity(appletAssistActivity);
		if (!save) {
			log.error("帮拆后更新数据异常");
			// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
			if (appletAssistActivity.getCreateRule().isAssistCreate()) {
				// TODO 助力并创建
				log.error("此方法待支持");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
			} else {
				// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
				AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
				if (visitorActivity != null) {
					showAppletAssistActivity = visitorActivity;
				}
			}
			return activityPageVO(showAppletAssistActivity, params);
		}

		// 是否开奖
		if (appletAssistActivity.isLottery(trigger)) {
			// 补全 用户参与事件
			appletActivityRepository.findUserJoinEventByAppletActivity(appletAssistActivity, null);
			// 抽奖，并发起相应的中奖任务
			List<Long> winnerIds = appletAssistActivity.drawLottery();
			// 持久化活动状态
			save = appletActivityRepository.saveLottery(appletAssistActivity);
			if (!save) {
				// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
				if (appletAssistActivity.getCreateRule().isAssistCreate()) {
					// TODO 助力并创建
					log.error("此方法待支持");
					return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "方法 未支持");
				} else {
					// 判断访问者 是否有自己的活动 -- 如果有，则返回助力着自己的活动，如果没有就返回访问的活动
					AppletAssistActivity visitorActivity = showVisitorAppletAssistActivity(appletAssistActivity.getActivityDefId(), userId);
					if (visitorActivity != null) {
						showAppletAssistActivity = visitorActivity;
					}
				}
				return activityPageVO(showAppletAssistActivity, params);
			}
		}
		// 处理返回数据
		return activityPageVO(showAppletAssistActivity, params);
	}

	/**
	 * 通过 activityId  获取 visitorId 访问的 助力类活动
	 */
	private AppletAssistActivity findByActivityIdVisitorId(Long activityId, Long userId) {
		// 获取 活动
		AppletActivity appletActivity = appletActivityRepository.findById(activityId);
		if (null == appletActivity) {
			log.error("查无此活动，activityId = {}", activityId);
			return null;
		}
		Activity activity = appletActivity.getActivityInfo();
		AppletAssistActivity appletAssistActivity = new AppletAssistActivity();
		appletAssistActivity.setId(activity.getId());
		appletAssistActivity.setActivityDefId(activity.getActivityDefId());
		appletAssistActivity.setActivityInfo(activity);
		appletAssistActivity.setUserInfo(appletActivity.getUserInfo());
		appletAssistActivity.setJoinRule(new JoinRule(activity));
		appletAssistActivity.setCreateRule(new CreateRule(activity));

		UserBoard userBoard = new UserBoard();
		userBoard.setActivityDefId(activity.getActivityDefId());
		appletAssistActivity.setUserBoardConfig(new UserBoardConfig());

		appletAssistActivity.setVisitUserId(userId);
		appletAssistActivity.setScenesEnum(ScenesEnum.ACTIVITY_PAGE);
		appletActivityRepository.addAssistActivityInfo(appletAssistActivity);

		return appletAssistActivity;
	}

	/**
	 * 是否存在自己的活动
	 *
	 * @param activityDefId
	 * @param userId
	 * @return
	 */
	private AppletAssistActivity showVisitorAppletAssistActivity(Long activityDefId, Long userId) {
		AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(activityDefId);
		if (null == appletActivityDef) {
			log.error("查无此活动定义，activityDefId = {}", activityDefId);
			return null;
		}
		// 获取符合参与规则的进行中活动及已完成记录
		JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRuleUserId(appletActivityDef.getJoinRule(), userId);

		if (joinRuleActivity.canJoinByUserId(userId)) {
			log.info("用户可以发起，说明 无进行中的活动，或没有活动，或有完成的活动但还可以发起新的活动");
			return null;
		}
		// 说明 已有进行中活动或不可再发起新活动了 处理返回参数 -- 各种情况判断

		// 获取用户进行中的活动列表
		List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
		Activity activity;
		if (CollectionUtils.isEmpty(activityList)) {
			// 无进行中活动，获取用户已完成的活动
			Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
			if (completedActivityId == null) {
				log.error("逻辑异常，须排查");
				return null;
			}
			AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
			Activity activityInfo = completedActivity.getActivityInfo();
			if (activityInfo == null) {
				log.error("查无此活动，activityId = {}", completedActivityId);
				return null;
			}
			activity = activityInfo;
		} else {
			activity = activityList.get(0);
		}
		// 返回进行中活动 或 已完成的活动
		AppletActivity appletActivity = appletActivityRepository.assemblyAppletActivity(activity);

		if (appletActivity == null) {
			log.info("无进行中的活动，或没有活动，或有完成的活动但还可以发起新的活动");
			return null;
		}
		AppletAssistActivity appletAssistActivity = this.assemblyPrimaryAppletAssistActivity(appletActivity.getActivityInfo(), userId, ScenesEnum.ACTIVITY_PAGE);
		appletActivityRepository.addAssistActivityInfo(appletAssistActivity);
		return appletAssistActivity;
	}

	/**
	 * 通过 活动信息 装配 助力类活动
	 *
	 * @param activity
	 * @return
	 */
	private AppletAssistActivity assemblyPrimaryAppletAssistActivity(Activity activity, Long visitorId, ScenesEnum scenesEnum) {
		AppletAssistActivity appletAssistActivity = new AppletAssistActivity();
		appletAssistActivity.setId(activity.getId());
		appletAssistActivity.setActivityDefId(activity.getActivityDefId());
		appletAssistActivity.setActivityInfo(activity);
		appletAssistActivity.setJoinRule(new JoinRule(activity));
		appletAssistActivity.setCreateRule(new CreateRule(activity));

		UserBoard userBoard = new UserBoard();
		userBoard.setActivityDefId(activity.getActivityDefId());
		appletAssistActivity.setUserBoardConfig(new UserBoardConfig());

		appletAssistActivity.setVisitUserId(visitorId);
		appletAssistActivity.setScenesEnum(scenesEnum);
		return appletAssistActivity;
	}

	/**
	 * 当前活动是否可助力
	 * @param appletAssistActivity
	 * @param userId
	 * @return
	 */
	private boolean canAssistByVisitorId(AppletAssistActivity appletAssistActivity, Long userId) {
		if (appletAssistActivity.isExpired()) {
			// 活动过期
			if (appletAssistActivity.getActivityInfo().getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
				// 补全 用户参与事件
				appletActivityRepository.findUserJoinEventByAppletActivity(appletAssistActivity, null);
				appletAssistActivity.setExpired();
				appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(appletAssistActivity);
			}
			return false;
		}
		// 活动是否可以助力
		return appletAssistActivity.canJoinByUserId(userId);
	}

	/**
	 * 自定义活动
	 * @param appletActivity
	 * @return
	 */
	protected abstract AppletActivity assembleNewActivity(AppletActivity appletActivity, Map<String, Object> params);

	/**
	 * 自定义活动页面交互
	 * @param appletActivity
	 * @return
	 */
	protected abstract SimpleResponse activityPageVO(AppletActivity appletActivity, Map<String, Object> params);
}
