package mf.code.activity.domain.applet.valueobject;

import lombok.Data;

/**
 * 活动定义 用户看板配置 --- 助力活动看板配置 -- 如果新增其他活动的看板配置，则进行迭代抽象
 * 此配置可读取字典表
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-23 00:24
 */
@Data
public class UserBoardConfig {
	/**
	 * 配置 进入快完成列表的条件 -- 剩余活动成团人数
	 */
	private Integer leftNum = 1;
	/**
	 * 配置 快完成用户列表展示数量
	 */
	private Integer size = 5;

}
