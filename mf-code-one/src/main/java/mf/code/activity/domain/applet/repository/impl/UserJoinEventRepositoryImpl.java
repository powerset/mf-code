package mf.code.activity.domain.applet.repository.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.aggregateroot.UserJoinEvent;
import mf.code.activity.domain.applet.repository.UserJoinEventRepository;
import mf.code.uactivity.service.UserActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.activity.domain.applet.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 19:13
 */
@Slf4j
@Service
public class UserJoinEventRepositoryImpl implements UserJoinEventRepository {
	@Autowired
	private UserActivityService userActivityService;

	/**
	 * 保存 userActivity
	 * @param userJoinEvent
	 * @return
	 */
	@Override
	public boolean save(UserJoinEvent userJoinEvent) {
		return userActivityService.save(userJoinEvent.getUserActivityInfo());
	}
}
