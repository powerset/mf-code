package mf.code.activity.domain.applet.repository.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletLotteryActivity;
import mf.code.activity.domain.applet.aggregateroot.UserJoinEvent;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.valueobject.*;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.AppletMybatisPageDto;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.api.applet.v3.service.CommissionAboutService;
import mf.code.api.applet.v6.service.NewbieOpenRedPackageService;
import mf.code.api.applet.v9.service.FullReimbursementAboutService;
import mf.code.api.feignclient.CommAppService;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.dto.ProductEntity;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.one.dto.GrantCouponDTO;
import mf.code.one.templatemessge.OneTemplateMessageData;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.service.UpayAboutService;
import mf.code.user.dto.AppletUserDTO;
import mf.code.user.dto.UserResp;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.management.AttributeList;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.activity.domain.applet.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-22 17:11
 */
@Slf4j
@Service
public class AppletActivityRepositoryImpl implements AppletActivityRepository {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserService userService;
    @Autowired
    private UpayAboutService upayAboutService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    @Autowired
    private NewbieOpenRedPackageService newcomerOpenRedPackageService;
    @Autowired
    private CommissionAboutService commissionAboutService;
    @Autowired
    private CommAppService commAppService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private FullReimbursementAboutService fullReimbursementAboutService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;

    /**
     * 保存创建活动和用户参与信息 -- 含扣库存逻辑 -- 事务支持
     *
     * @param appletActivity
     * @return
     */
    @Transactional
    @Override
    public boolean saveCreateByStockType(AppletActivity appletActivity) {
        // 扣库存逻辑
        Activity activityInfo = appletActivity.getActivityInfo();
        CreateRule createRule = appletActivity.getCreateRule();
        if (ActivityDefStockTypeEnum.CREATE_REDUCE.getCode().equals(activityInfo.getStockType())) {
            // 扣库存  还是扣保证金 -- 是否需要某一个为零就扣减失败
            // 是否必须保证库存数为正
            if (createRule.isCheckStock()) {
                // 扣减库存 或 扣减库存和保证金
                BigDecimal reduceDeposit = appletActivity.queryReduceDeposit();
                Integer reduceStock = appletActivity.queryReduceStock();
                Integer rows = activityDefService.reduceDeposit(appletActivity.getActivityDefId(), reduceDeposit, reduceStock);
                if (rows == 0) {
                    log.error("扣库存失败，库存不足");
                    return false;
                }
            } else {
                // 不扣库存，只扣保证金 -- 如果一个活动只扣保证金，且以剩余保证金为库存数时，则可以不校验 stock >= 0
                BigDecimal reduceDeposit = appletActivity.queryReduceDeposit();
                Integer reduceStock = appletActivity.queryReduceStock();
                Integer rows = activityDefService.reduceDepositWithoutCheckStock(appletActivity.getActivityDefId(), reduceDeposit, reduceStock);
                if (rows == 0) {
                    log.error("扣库存失败，库存不足");
                    return false;
                }
            }
        }

        // 是否配置了新手任务
        boolean newBieActivity = false;

        User user = userService.getById(activityInfo.getUserId());
        if (activityInfo.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() && user.getShopId().equals(user.getVipShopId())) {
            boolean completeNewbieOpenRedPackage = newcomerOpenRedPackageService.isCompleteNewbieOpenRedPackage(activityInfo.getUserId());
            if (!completeNewbieOpenRedPackage) {
                ActivityDef activityDef = newcomerOpenRedPackageService.hasExistNewbieTask(activityInfo.getShopId());
                if (activityDef != null) {
                    activityInfo.setType(ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode());
                    newBieActivity = true;
                }
            }
        }


        // 入库活动
        boolean save = activityService.saveSelective(activityInfo);
        if (!save) {
            log.error("数据插入失败，数据库异常");
            throw new RuntimeException();
        }
        Long activityId = activityInfo.getId();
        appletActivity.setId(activityId);
        // 保存用户参与事件
        saveUserJoinEvent(appletActivity, newBieActivity);

        // 针对不同活动 -- 额外处理的存储
        if (appletActivity instanceof AppletAssistActivity) {
            AppletAssistActivity appletAssistActivity = (AppletAssistActivity) appletActivity;
            return this.saveCreateAssistActivity(appletAssistActivity);
        }

        if (appletActivity instanceof AppletLotteryActivity) {
            AppletLotteryActivity appletLotteryActivity = (AppletLotteryActivity) appletActivity;
            return this.saveCreateLotteryActivity(appletLotteryActivity);
        }

        return true;
    }

    // 保存用户参与事件 -- 此方法只支持创建保存事件，无更新方法
    private void saveUserJoinEvent(AppletActivity appletActivity, boolean restActivity) {
        Map<Long, UserJoinEvent> userJoinEventMap = appletActivity.getUserJoinEventMap();
        if (!CollectionUtils.isEmpty(userJoinEventMap)) {
            // 获取所有待插入事件
            List<UserActivity> userActivityList = new ArrayList<>();
            for (UserJoinEvent userJoinEvent : userJoinEventMap.values()) {
                // 如果id 为空 则新增参与事件
                if (userJoinEvent.getUserActivityInfo().getId() == null) {
                    userJoinEvent.setActivityId(appletActivity.getId());
                    userJoinEvent.getUserActivityInfo().setActivityId(appletActivity.getId());
                    if (restActivity) {
                        userJoinEvent.getUserActivityInfo().setType(UserActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode());
                    }
                    userActivityList.add(userJoinEvent.getUserActivityInfo());
                }
                // 如果id 不为空 则更新参与事件 --- 事件 正常业务 是不变得，但本系统却是可变的 -- 待处理
            }
            if (!CollectionUtils.isEmpty(userActivityList)) {
                // 入库
                boolean save = userActivityService.saveBatch(userActivityList, 100);
                if (!save) {
                    log.error("数据插入失败，数据库异常");
                    throw new RuntimeException();
                }
            }
        }
    }

    /**
     * 保存 创建 抽奖活动
     * @param appletLotteryActivity
     * @return
     */
    private boolean saveCreateLotteryActivity(AppletLotteryActivity appletLotteryActivity) {
        Long activityId = appletLotteryActivity.getId();
        User userInfo = appletLotteryActivity.getUserInfo();
        Map<String, Object> startUserInfo = appletLotteryActivity.getStartUserInfo();
        List awardNo = (List) startUserInfo.get("awardNo");

        // 通用事件处理
        if (appletLotteryActivity.getCreateRule().isJoinSelf()) {
            // 发起者 本人 也参与抽奖
            // 活动计数
            Long rtn = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);
            if (rtn == null) {
                log.error("活动计数异常，redis异常");
                throw new RuntimeException();
            }
            // 更新redis抽奖概率 -- 发起者获得10个抽奖码
            stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userInfo.getId().toString(), awardNo.size());
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);
            // redis更新活动列表
            stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, JSON.toJSONString(appletLotteryActivity.getStartUserInfo()));
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);

            // 用户邀请列表 -- 包含自己
            this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userInfo.getId(), JSON.toJSONString(appletLotteryActivity.getStartUserInfo()));
            this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userInfo.getId(), appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);
        }

        return true;
    }

    /**
     * 保存 创建 助力活动
     *
     * @param appletAssistActivity
     * @return
     */
    private boolean saveCreateAssistActivity(AppletAssistActivity appletAssistActivity) {

        Long activityId = appletAssistActivity.getId();
        Activity activity = appletAssistActivity.getActivityInfo();
        CreateRule createRule = appletAssistActivity.getCreateRule();
        Long userId = activity.getUserId();
        Map<String, Object> startUserInfo = appletAssistActivity.getStartUserInfo();
        startUserInfo.put("activityId", activityId);
        Map<String, Map<String, Object>> assistUserInfo = appletAssistActivity.getAssistUserInfoMap();

        // 活动计数加1
        if (createRule.isJoinSelf()) {
            Long trigger = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
            if (trigger == null) {
                log.error("活动计数异常，redis异常");
                throw new RuntimeException();
            }
        }

        // 初始化奖池金额
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId, appletAssistActivity.getOpenAmount().toString());
        // 拆红包算法
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId, JSON.toJSONString(appletAssistActivity.getAssistAmountList()));
        // 发起者信息并存入redis
        stringRedisTemplate.opsForHash().put(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), userId.toString(), JSON.toJSONString(startUserInfo));
        // 更新 助力者信息 列表
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId, JSON.toJSONString(assistUserInfo.values()));

        return true;
    }

    /**
     * 保存开奖的活动和用户参与信息以及用户任务信息 -- 含扣库存逻辑 -- 事务支持
     *
     * @param appletActivity
     * @return
     */
    @Transactional
    @Override
    public boolean saveLottery(AppletActivity appletActivity) {
        boolean save = saveLotteryByStockType(appletActivity);
        if (!save) {
            log.error("扣库存失败，库存不足");
            return false;
        }
        // 针对不同活动 -- 额外处理的存储
        if (appletActivity instanceof AppletAssistActivity) {
            AppletAssistActivity openRedPackageActivity = (AppletAssistActivity) appletActivity;
            return this.saveLotteryAppletAssistActivity(openRedPackageActivity);
        }

        if (appletActivity instanceof AppletLotteryActivity) {
            AppletLotteryActivity appletLotteryActivity = (AppletLotteryActivity) appletActivity;
            return this.saveLotteryByAppletLotteryActivity(appletLotteryActivity);
        }

        return true;
    }

    private boolean saveLotteryByAppletLotteryActivity(AppletLotteryActivity appletLotteryActivity) {
        Activity activityInfo = appletLotteryActivity.getActivityInfo();
        Long activityId = appletLotteryActivity.getId();

        List<Long> winnerIds = appletLotteryActivity.getWinnerIds();
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER + activityId, JSONObject.toJSONString(winnerIds));
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);
        Map<Long, User> userMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(winnerIds)) {
            Collection<User> users = userService.listByIds(winnerIds);
            if (!CollectionUtils.isEmpty(users)) {
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }
        }
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId);
        for (Long winnerId : winnerIds) {
            User user = userMap.get(winnerId);
            if (user == null) {
                continue;
            }
            Map<String, Object> memberUserInfo = appletLotteryActivity.becomeMemberByUser(user);
            this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId, JSON.toJSONString(memberUserInfo));
        }
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);


        Map<Object, Object> userCountMap = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId);
        List<Long> loserIds = new ArrayList<>();
        for (Map.Entry<Object, Object> entry : userCountMap.entrySet()) {
            Long userId = Long.parseLong((String) entry.getKey());
            if (winnerIds.contains(userId)) {
                stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER_DIALOG + activityId + ":" + userId, "1");
                stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER_DIALOG + activityId + ":" + userId, 7, TimeUnit.DAYS);
                continue;
            }
            stringRedisTemplate.opsForSet().add(RedisKeyConstant.ACTIVITY_LOSER_SET + activityId, userId.toString());
            loserIds.add(userId);
        }
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_LOSER_SET + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);

        if (appletLotteryActivity.getCreateRule().isStockByHitsPerDraw()) {
            int returnStock = activityInfo.getHitsPerDraw() - winnerIds.size();
            activityDefService.addDeposit(appletLotteryActivity.getActivityDefId(), BigDecimal.ZERO, returnStock);
        }

        // 获取优惠券
        String merchantCouponJson = activityInfo.getMerchantCouponJson();
        if (StringUtils.isBlank(merchantCouponJson)) {
            return true;
        }
        JSONArray couponList = JSON.parseArray(merchantCouponJson);
        if (CollectionUtils.isEmpty(couponList)) {
            return true;
        }
        Long couponId = Long.valueOf(couponList.get(0).toString());

        // TODO 发放优惠券 -- 注意 非异步
        GrantCouponDTO grantCouponDTO = new GrantCouponDTO();
        grantCouponDTO.setActivityDefId(activityInfo.getActivityDefId());
        grantCouponDTO.setActivityId(activityInfo.getId());
        grantCouponDTO.setCouponId(couponId);
        grantCouponDTO.setUserIds(loserIds);
        Map<Long, Long> userCouponIdMap = merchantShopCouponService.grantCouponById(grantCouponDTO);
        if (!CollectionUtils.isEmpty(userCouponIdMap)) {
            for (Map.Entry<Long, Long> entry : userCouponIdMap.entrySet()){
                stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER_DIALOG + activityId + ":" + entry.getKey(), "2");
                stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER_DIALOG + activityId + ":" + entry.getKey(), 7, TimeUnit.DAYS);
            }
        }

        return true;
    }

    /**
     * 通过id 获取
     *
     * @param activityId
     * @return
     */
    @Override
    public AppletActivity findById(Long activityId) {
        Activity activity = activityService.findById(activityId);
        if (activity == null) {
            return null;
        }
        // 装配活动根
        return assemblyAppletActivity(activity);
    }

    /**
     * 补全 用户参与事件
     *
     * @param appletActivity
     */
    @Override
    public void findUserJoinEventByAppletActivity(AppletActivity appletActivity, List<Long> userIds) {
        Activity activityInfo = appletActivity.getActivityInfo();

        QueryWrapper<UserActivity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserActivity::getActivityId, activityInfo.getId())
                .eq(UserActivity::getStatus, UserActivityStatusEnum.NO_WIN.getCode());
        if (!CollectionUtils.isEmpty(userIds)) {
            wrapper.lambda().in(UserActivity::getUserId, userIds);
        }
        List<UserActivity> userActivityList = userActivityService.list(wrapper);

        Map<Long, UserJoinEvent> userJoinEventMap = appletActivity.getUserJoinEventMap();
        if (CollectionUtils.isEmpty(userJoinEventMap)) {
            userJoinEventMap = new HashMap<>();
        }
        for (UserActivity userActivity : userActivityList) {
            UserJoinEvent userJoinEvent = new UserJoinEvent();
            userJoinEvent.setUserId(userActivity.getUserId());
            userJoinEvent.setActivityId(userActivity.getActivityId());
            userJoinEvent.setUserActivityInfo(userActivity);
            userJoinEventMap.put(userActivity.getUserId(), userJoinEvent);
        }

        appletActivity.setUserJoinEventMap(userJoinEventMap);
    }

    /**
     * 通过 活动信息 装配 活动
     *
     * @param activity
     * @return
     */
    @Override
    public AppletActivity assemblyAppletActivity(Activity activity) {
        User user = userService.getById(activity.getUserId());
        AppletActivity appletActivity = new AppletActivity();
        appletActivity.setId(activity.getId());
        appletActivity.setActivityDefId(activity.getActivityDefId());
        appletActivity.setActivityInfo(activity);
        appletActivity.setUserInfo(user);
        appletActivity.setJoinRule(new JoinRule(activity));
        appletActivity.setCreateRule(new CreateRule(activity));

        UserBoard userBoard = new UserBoard();
        userBoard.setActivityDefId(activity.getActivityDefId());
        appletActivity.setUserBoardConfig(new UserBoardConfig());
        return appletActivity;
    }

    /**
     * 通过 活动信息 装配 拆红包活动
     *
     * @param activity
     * @return
     */
    @Override
    public AppletAssistActivity assemblyOpenRedPackageActivity(Activity activity, Long visitorId, ScenesEnum scenesEnum) {
        User user = userService.getById(activity.getUserId());
        AppletAssistActivity appletActivity = new AppletAssistActivity();
        appletActivity.setId(activity.getId());
        appletActivity.setActivityDefId(activity.getActivityDefId());
        appletActivity.setActivityInfo(activity);
        appletActivity.setUserInfo(user);
        appletActivity.setJoinRule(new JoinRule(activity));
        appletActivity.setCreateRule(new CreateRule(activity));

        UserBoard userBoard = new UserBoard();
        userBoard.setActivityDefId(activity.getActivityDefId());
        appletActivity.setUserBoardConfig(new UserBoardConfig());

        appletActivity.setVisitUserId(visitorId);
        appletActivity.setScenesEnum(scenesEnum);
        return appletActivity;
    }

    /**
     * 增装 拆红包用户信息
     */
    @Override
    public void addAssistActivityInfo(AppletAssistActivity openRedPackageActivity) {
        Long activityId = openRedPackageActivity.getId();
        Activity activity = openRedPackageActivity.getActivityInfo();
        Long activityDefId = openRedPackageActivity.getActivityDefId();
        Long userId = activity.getUserId();

        // 获取 活动的金额列表
        String amounts = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId);
        List<BigDecimal> amountList = null;
        if (StringUtils.isNotBlank(amounts)) {
            amountList = JSON.parseArray(amounts, BigDecimal.class);
        }
        // 获取 发起者信息
        Map<String, Object> startUserInfo = getStartUserInfo(activityDefId, userId);
        // 获取redis中 助力者信息
        Map<String, Map<String, Object>> assistUserInfoMap = getAssistUserInfoList(openRedPackageActivity);
        // 获取 邀请人信息
        Map<String, Object> pubUserInfo = getPubUserInfo(activityDefId, startUserInfo);
        // 获取 本次离开时间段的 助力的信息
        List<Map<String, Object>> assistDialogList = getAssistDialogList(activityId, userId);

        // 是否弹出中奖窗口
        Boolean awardDialog = false;
        // 是否弹出助力弹窗
        Boolean assistDialog = false;

        ScenesEnum scenesEnum = openRedPackageActivity.getScenesEnum();
        Long visitUserId = openRedPackageActivity.getVisitUserId();
        if (scenesEnum == ScenesEnum.ACTIVITY_PAGE && visitUserId.equals(activity.getUserId())) {
            // 进入活动页，并且进入的活动页是活动者本人
            // 是否弹出中奖窗口
            awardDialog = stringRedisTemplate.hasKey(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_DIALOG + activityId + ":" + userId);
            awardDialog = awardDialog == null ? false : awardDialog;

            // 是否弹出助力弹窗
            assistDialog = stringRedisTemplate.hasKey(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
            assistDialog = assistDialog == null ? false : assistDialog;

            // 删除 本次离开时间段的 助力的信息
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activity.getId() + ":" + activity.getUserId());
            // 删除 中奖弹框
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_DIALOG + activity.getId() + ":" + activity.getUserId());
        }

        openRedPackageActivity.setAssistAmountList(amountList);
        openRedPackageActivity.setStartUserInfo(startUserInfo);
        openRedPackageActivity.setAssistUserInfoMap(assistUserInfoMap);
        openRedPackageActivity.setPubUserInfo(pubUserInfo);
        openRedPackageActivity.setAssistDialogList(assistDialogList);
        openRedPackageActivity.setIsAwardDialog(awardDialog);
        openRedPackageActivity.setIsAssistDialog(assistDialog);

        addMemberInfo(openRedPackageActivity);
    }

    public void addMemberInfo(AppletAssistActivity appletAssistActivity) {
        Map<String, Map<String, Object>> assistUserInfoMap = appletAssistActivity.getAssistUserInfoMap();
        if (!CollectionUtils.isEmpty(assistUserInfoMap)) {
            return;
        }
        // redis已删除 -- 数据已入库
        if (appletAssistActivity.getActivityInfo().getAwardStartTime() != null) {
            List<Integer> userCouponTypes = new ArrayList<>();
            userCouponTypes.add(UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode());
            userCouponTypes.add(UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode());
            userCouponTypes.add(UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode());
            userCouponTypes.add(UserCouponTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode());

            List<Long> couponUserIds = new ArrayList<>();
            QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(UserCoupon::getActivityId, appletAssistActivity.getId())
                    .in(UserCoupon::getType, userCouponTypes);
            wrapper.orderByDesc("id");
            List<UserCoupon> userCoupons = userCouponService.list(wrapper);
            if (!CollectionUtils.isEmpty(userCoupons)) {
                for (UserCoupon userCoupon : userCoupons) {
                    couponUserIds.add(userCoupon.getUserId());
                }
            }
            Map<Long, User> userMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(couponUserIds)) {
                QueryWrapper<User> uWrapper = new QueryWrapper<>();
                uWrapper.lambda()
                        .in(User::getId, couponUserIds);
                List<User> users = userService.list(uWrapper);
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }
            if (!CollectionUtils.isEmpty(userCoupons)) {
                for (UserCoupon userCoupon : userCoupons) {
                    User user = userMap.get(userCoupon.getUserId());
                    appletAssistActivity.setMemberByUser(user, userCoupon);
                }
            }
        }
    }

    /**
     * 拆红包活动帮拆后， 活动数据更新
     *
     * @param appletAssistActivity
     * @return
     */
    @Transactional
    @Override
    public boolean saveAssistAppletAssistActivity(AppletAssistActivity appletAssistActivity) {
        // 更新用户邀请记录
        boolean save = updateActivityInviteEvent(appletAssistActivity);
        if (!save) {
            log.error("更新活动邀请记录异常, activityId = {}, pubUserId = {}, subUserId = {}",
                    appletAssistActivity.getId(),
                    appletAssistActivity.getActivityInfo().getUserId(),
                    appletAssistActivity.getVisitUserId());
            throw new RuntimeException();
        }

        // 保存用户参与事件
        saveUserJoinEvent(appletAssistActivity, false);

        // 入库助力即发奖金额
        Map<Long, UserCoupon> winnerCoupon = appletAssistActivity.queryAssistAwardPrize();
        if (!CollectionUtils.isEmpty(winnerCoupon)) {
            save = userCouponService.saveBatch(winnerCoupon.values(), 100);
            if (!save) {
                log.error("用户奖励入库失败");
                throw new RuntimeException();
            }

            //夜辰 -- 拆红包开奖完成， 入库 upayWxOrder, upayBalance
            SimpleResponse simpleResponse = upayAboutService.openRedPackageSaveUpay(appletAssistActivity.getActivityInfo(), appletAssistActivity.getOpenAmount());
            if (simpleResponse.error()) {
                log.error("拆红包-余额-订单-入库异常");
                throw new RuntimeException();
            }
            //报销活动，完成任务的推送消息--todo:百川验证下
            if (appletAssistActivity.getActivityInfo().getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                log.info("<<<<<<<<报销活动助力开始推送消息：appletAssistActivity:{}", appletAssistActivity);
                BigDecimal awardPool = fullReimbursementAboutService.getFullReimbursementMoney(
                        appletAssistActivity.getActivityInfo().getMerchantId(), appletAssistActivity.getActivityInfo().getShopId(),
                        appletAssistActivity.getActivityInfo().getUserId(), appletAssistActivity.getActivityInfo().getId(),
                        appletAssistActivity.getActivityInfo().getHitsPerDraw());

                //报销活动金额
                BigDecimal applyAmount = appletAssistActivity.getActivityInfo().getDeposit();
                //剩余金额
                BigDecimal remain = applyAmount.subtract(awardPool);

                TemplateMsgDTO templateMsgDTO = new TemplateMsgDTO();
                templateMsgDTO.setTemplateId(wxmpProperty.getOrderCashBackMsgTmpId());
                UserResp userResp = new UserResp();
                userResp.setOpenId(appletAssistActivity.getUserInfo().getOpenId());
                userResp.setId(appletAssistActivity.getUserInfo().getId());
                templateMsgDTO.setUser(userResp);
                Map<String, Object> data = OneTemplateMessageData.fullReimbursementDirectCash(
                        appletAssistActivity.getActivityInfo().getMerchantId(), appletAssistActivity.getActivityInfo().getShopId(),
                        appletAssistActivity.getOpenAmount(), remain, appletAssistActivity.getUserInfo().getNickName(),
                        appletAssistActivity.getActivityInfo().getId());
                templateMsgDTO.setData(data);
                log.info("<<<<<<<<报销活动助力开始推送消息：req:{}", templateMsgDTO);
                commAppService.sendJkmfTemplateMsg(templateMsgDTO);
            }
        }

        Long activityId = appletAssistActivity.getId();
        Activity activity = appletAssistActivity.getActivityInfo();
        Long userId = activity.getUserId();
        Map<String, Object> startUserInfo = appletAssistActivity.getStartUserInfo();
        List<Map<String, Object>> assistDialogList = appletAssistActivity.getAssistDialogList();

        Map<String, Map<String, Object>> assistUserInfoList = appletAssistActivity.getAssistUserInfoMap();
        Map<String, Object> visitorMember = assistUserInfoList.get(appletAssistActivity.getVisitUserId().toString());

        // 更新活动奖池金额
        BigDecimal openAmount = appletAssistActivity.getOpenAmount();
        Double awardPool = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId, openAmount.doubleValue());
        // 更新 发起人信息
        startUserInfo.put("awardPool", awardPool);
        // 发起者信息并存入redis
        stringRedisTemplate.opsForHash().put(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), userId.toString(), JSON.toJSONString(startUserInfo));
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, JSON.toJSONString(visitorMember));
        // 更新 助力者信息 列表
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId, JSON.toJSONString(assistUserInfoList.values()));
        // 更新助力者弹框
        if (appletAssistActivity.getIsAssistDialog()) {
            // 只是对redis的助力弹框进行更新，不影响已生成的助力弹框列表
            assistDialogList = new ArrayList<>();
            assistDialogList.add(visitorMember);
        }
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId, JSON.toJSONString(assistDialogList));
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ASSIST_SUCCESS + activityId + ":" + appletAssistActivity.getVisitUserId(), "1");
        stringRedisTemplate.expire(RedisKeyConstant.ASSIST_SUCCESS + activityId + ":" + appletAssistActivity.getVisitUserId(), appletAssistActivity.queryLifeCycle(), TimeUnit.MINUTES);
        return true;
    }

    /**
     * 更新用户邀请记录
     *
     * @param appletActivity
     */
    private boolean updateActivityInviteEvent(AppletActivity appletActivity) {
        ActivityInviteEvent activityInviteEvent = appletActivity.getActivityInviteEvent();
        if (activityInviteEvent == null) {
            return true;
        }
        UserPubJoin userPubJoinInfo = activityInviteEvent.getUserPubJoinInfo();

        return userPubJoinService.updateById(userPubJoinInfo);
    }

    /**
     * 保存 开奖的拆红包活动
     *
     * @param openRedPackageActivity
     * @return
     */
    @Override
    public boolean saveLotteryAppletAssistActivity(AppletAssistActivity openRedPackageActivity) {
        Long activityId = openRedPackageActivity.getId();
        Activity activity = openRedPackageActivity.getActivityInfo();
        Long userId = activity.getUserId();
        Map<Long, UserCoupon> winnerCoupon = openRedPackageActivity.getWinnerCoupon();

        // 存储用户奖励
        if (!CollectionUtils.isEmpty(winnerCoupon)) {
            if (activity.getType() == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode()) {
                ActivityDef activityDef = activityDefService.getById(activity.getActivityDefId());
                List<UserCoupon> userCouponList = new ArrayList<>();
                for (UserCoupon userCoupon : winnerCoupon.values()) {
                    userCoupon.setActivityTaskId(activityDef.getParentId());
                    userCouponList.add(userCoupon);
                }
                boolean save = userCouponService.saveBatch(userCouponList, 100);
                if (!save) {
                    log.error("用户奖励入库失败");
                    throw new RuntimeException();
                }
            } else {
                boolean save = userCouponService.saveBatch(winnerCoupon.values(), 100);
                if (!save) {
                    log.error("用户奖励入库失败");
                    throw new RuntimeException();
                }
            }
        }

        //夜辰 -- 拆红包开奖完成， 入库 upayWxOrder, upayBalance
        BigDecimal payAmount = openRedPackageActivity.queryPayAmount();
        SimpleResponse simpleResponse = upayAboutService.openRedPackageSaveUpay(activity, payAmount);
        if (simpleResponse.error()) {
            log.error("拆红包-余额-订单-入库异常");
            throw new RuntimeException();
        }
        //报销活动的完成推送
        if (activity.getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            log.info("<<<<<<<<报销活动完成开始推送消息：activity:{}", activity);
            BigDecimal awardPool = fullReimbursementAboutService.getFullReimbursementMoney(
                    activity.getMerchantId(), activity.getShopId(),
                    activity.getUserId(), activity.getId(), activity.getHitsPerDraw());

            User user = userService.selectByPrimaryKey(userId);
            //报销活动金额
            BigDecimal applyAmount = activity.getDeposit();
            //剩余金额
            BigDecimal remain = applyAmount.subtract(awardPool);

            TemplateMsgDTO templateMsgDTO = new TemplateMsgDTO();
            templateMsgDTO.setTemplateId(wxmpProperty.getOrderCashBackMsgTmpId());
            UserResp userResp = new UserResp();
            userResp.setOpenId(user.getOpenId());
            userResp.setId(user.getId());
            templateMsgDTO.setUser(userResp);
            Map<String, Object> data = OneTemplateMessageData.fullReimbursementSuccess(
                    activity.getMerchantId(), activity.getShopId(), remain, activity.getId());
            templateMsgDTO.setData(data);
            log.info("<<<<<<<<报销活动完成开始推送消息：req:{}", templateMsgDTO);
            commAppService.sendJkmfTemplateMsg(templateMsgDTO);

            //开奖弹窗 存入redis
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.AWARDDIALOG_ACTIVITY_UID + activity.getId() + ":" + user.getId(), "1", 24, TimeUnit.HOURS);
        }
        if (openRedPackageActivity.getActivityInfo().getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()
                || openRedPackageActivity.getActivityInfo().getType() == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode()) {
            commissionAboutService.saveTodayProfitRedis(true, userId, activity.getPayPrice(), BigDecimal.ZERO);
            // 加入 拆红包 轮播弹幕
            addOpenRedPackageBarrage(openRedPackageActivity);
        }
        commissionAboutService.canCashDialogRedisData(activity.getActivityDefId(), userId);
        // 发起者 中奖弹框 -- 是否触发开奖
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_DIALOG + activityId + ":" + userId, "1");
        // 加入 拆红包 轮播弹幕

        // 删除 本次离开时间段的 助力的信息 -- 开奖后 不弹助力者助力信息
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);

        // 清除发起信息
        stringRedisTemplate.opsForHash().delete(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId(), userId.toString());
        // 删除 redis更新活动列表
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        // 助力者信息
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId);
        // 删除奖池信息
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId);
        // 删除活动助力金集合
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId);

        //活动未成团推送消息
        this.sendTemplateMessageService.sendTemplateMsg2Award(activity.getShopId(), activity.getId(), null);

        return true;
    }

    /**
     * 加入 拆红包 轮播弹幕
     */
    private void addOpenRedPackageBarrage(AppletAssistActivity openRedPackageActivity) {
        Map<String, Object> startUserInfo = openRedPackageActivity.getStartUserInfo();
        Activity activity = openRedPackageActivity.getActivityInfo();
        Long activityDefId = openRedPackageActivity.getActivityDefId();

        String userId = startUserInfo.get("userId").toString();
        String nickName = startUserInfo.get("nickName").toString();

        // 处理中奖者名字
        char[] chars = nickName.toCharArray();
        if (chars.length <= 1) {
            nickName = "**" + nickName;
        } else {
            nickName = chars[0] + "**" + chars[chars.length - 1];
        }
        // **H 领取到5元现金
        Map<String, Object> winner = new HashMap<>();
        winner.put("userId", userId);
        winner.put("nickName", nickName);
        winner.put("amount", activity.getPayPrice());
        // 分页存入redis
        String winners = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId);
        if (StringUtils.isBlank(winners)) {
            List<Map<String, Object>> winnerList = new ArrayList<>();
            winnerList.add(winner);
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId, JSON.toJSONString(winnerList), 365, TimeUnit.DAYS);
            return;
        }
        List<Map> winnerList = JSON.parseArray(winners, Map.class);
        winnerList.add(winner);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId, JSON.toJSONString(winnerList), 365, TimeUnit.DAYS);
    }

    /**
     * 拆红包活动 过期更新
     *
     * @param openRedPackageActivity
     * @return
     */
    @Transactional
    @Override
    public boolean saveExpiredOrCompleteOpenRedPackageActivity(AppletAssistActivity openRedPackageActivity) {
        // 扣库存逻辑
        Activity activity = openRedPackageActivity.getActivityInfo();
        CreateRule createRule = openRedPackageActivity.getCreateRule();

        Long userId = activity.getUserId();
        Long activityId = activity.getId();

        if (openRedPackageActivity.isExpired() && activity.getStatus().equals(ActivityStatusEnum.FAILURE.getCode())) {
            // 退库存逻辑
            if (ActivityDefStockTypeEnum.CREATE_REDUCE.getCode().equals(activity.getStockType())) {
                // 扣库存  还是扣保证金 -- 是否需要某一个为零就扣减失败
                // 是否必须保证库存数为正
                if (createRule.isReduceStock()) {
                    // 扣减库存 或 扣减库存和保证金
                    BigDecimal addDeposit = openRedPackageActivity.queryReduceDeposit();
                    Integer addStock = openRedPackageActivity.queryReduceStock();

                    BigDecimal subtractAmount = openRedPackageActivity.querySubtractAmount();
                    Integer subtractStock = openRedPackageActivity.querySubtractStock();

                    Integer rows = activityDefService.addDeposit(activity.getActivityDefId(), addDeposit.subtract(subtractAmount), addStock - subtractStock);
                    if (rows == 0) {
                        log.error("退库存失败，库存未退, activityId = {}, activityDefId = {}", activityId, activity.getActivityDefId());
                        log.info("回滚重试中……");
                        return false;
                    }
                } else {
                    // 不扣库存，只扣保证金 -- 如果一个活动只扣保证金，且以剩余保证金为库存数时，则可以不校验 stock >= 0
                    BigDecimal addDeposit = openRedPackageActivity.queryReduceDeposit();
                    Integer addStock = openRedPackageActivity.queryReduceStock();

                    BigDecimal subtractAmount = openRedPackageActivity.querySubtractAmount();
                    Integer subtractStock = openRedPackageActivity.querySubtractStock();

                    Integer rows = activityDefService.addDepositWithoutCheckStock(activity.getActivityDefId(), addDeposit.subtract(subtractAmount), addStock - subtractStock);
                    if (rows == 0) {
                        log.error("退库存失败，库存未退, activityId = {}, activityDefId = {}", activityId, activity.getActivityDefId());
                        log.info("回滚重试中……");
                        return false;
                    }
                }
            }
            // 发起者 失败弹框 -- 是否触发过期
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_EXPIRED_DIALOG + activityId + ":" + userId, "1");
        }

        // 活动状态更新
        boolean update = activityService.updateById(activity);
        if (!update) {
            log.error("数据插入失败，数据库异常");
            throw new RuntimeException();
        }

        // 获取所有待更新 或 待插入 的中奖事件
        Map<Long, UserJoinEvent> userJoinEventMap = openRedPackageActivity.getUserJoinEventMap();
        List<UserActivity> userActivityList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(userJoinEventMap)) {
            for (UserJoinEvent userJoinEvent : userJoinEventMap.values()) {
                userActivityList.add(userJoinEvent.getUserActivityInfo());
            }
        }
        if (!CollectionUtils.isEmpty(userActivityList)) {
            update = userActivityService.updateBatchById(userActivityList, 100);
            if (!update) {
                log.error("数据更新失败，数据库异常");
                stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + openRedPackageActivity.getId(), -1);
                throw new RuntimeException();
            }
        }

        // 清除发起信息
        stringRedisTemplate.opsForHash().delete(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId(), userId.toString());
        // 助力者信息
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
        // 删除 redis更新活动列表
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        // 删除 助力者列表
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId);
        // 删除奖池信息
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId);
        // 删除活动助力金集合
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId);

        return true;
    }

    /**
     * 通过访问者 增装 活动邀请访问者的记录
     *
     * @param appletActivity
     * @param userId
     */
    @Override
    public void addActivityInviteEventByVisitor(AppletActivity appletActivity, Long userId, Long pubUserId) {
        if (userId == null || pubUserId == null) {
            return;
        }
        // 默认活动发起者为邀请者
        Long activityId = appletActivity.getId();

        QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserPubJoin::getUserId, pubUserId)
                .eq(UserPubJoin::getSubUid, userId)
                .eq(UserPubJoin::getActivityId, activityId);
        List<UserPubJoin> userPubJoins = userPubJoinService.list(wrapper);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            return;
        }

        // 装配 邀请记录
        ActivityInviteEvent activityInviteEvent = new ActivityInviteEvent();
        activityInviteEvent.setActivityId(activityId);
        activityInviteEvent.setPubUserId(pubUserId);
        activityInviteEvent.setSubUserId(userId);
        activityInviteEvent.setUserPubJoinInfo(userPubJoins.get(0));
        appletActivity.setActivityInviteEvent(activityInviteEvent);
    }

    /**
     * 增装 抽奖用户信息
     * @param appletLotteryActivity
     */
    @Override
    public boolean addLotteryActivityInfo(AppletLotteryActivity appletLotteryActivity, Long userId, Long offset, Long size) {
        Activity activity = appletLotteryActivity.getActivityInfo();

        // 获取商品信息
        Long goodsId = activity.getGoodsId();
        List<Long> productSkuIdList = new ArrayList<>();
        productSkuIdList.add(goodsId);
        List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
        if (!CollectionUtils.isEmpty(productEntities)) {
            appletLotteryActivity.setProductEntity(productEntities.get(0));
        }

        // 获取 活动客服
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
        if (activityDef != null && JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
            List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);

            Map<String, Object> serviceWx = new HashMap<>();
            //微信号
            serviceWx.put("customServiceWxNum", csMapList.get(0).get("wechat"));
            //微信二维码
            serviceWx.put("customServiceCode", csMapList.get(0).get("pic"));
            appletLotteryActivity.setServiceWx(serviceWx);
        }

        // 获取用户优惠券信息
        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getUserId, userId)
                .eq(UserCoupon::getActivityId, activity.getId())
                .eq(UserCoupon::getType, UserCouponTypeEnum.JKMF_COUPON.getCode());
        UserCoupon userCoupon = userCouponService.getOne(wrapper);
        if (userCoupon != null) {
            MerchantShopCoupon merchantShopCoupon = merchantShopCouponService.getById(userCoupon.getCouponId());
            Date displayStartTime = merchantShopCoupon.getDisplayStartTime();
            Date displayEndTime = merchantShopCoupon.getDisplayEndTime();

            AwardInfo awardInfo = new AwardInfo();
            awardInfo.setId(merchantShopCoupon.getId());
            awardInfo.setTitle("商品代金券");
            awardInfo.setDesc(merchantShopCoupon.getLimitAmount().compareTo(BigDecimal.ZERO) <= 0 ? "无金额门槛" : "满" + merchantShopCoupon.getLimitAmount() + "金额可使用");
            awardInfo.setPic(JSON.parseArray(appletLotteryActivity.getProductEntity().getMainPics()).get(0).toString());
            awardInfo.setEffectiveTime(DateUtil.dateToString(displayStartTime, DateUtil.POINT_DATE_FORMAT) + "-" + DateUtil.dateToString(displayEndTime, DateUtil.POINT_DATE_FORMAT));
            awardInfo.setPrice(merchantShopCoupon.getAmount());
            appletLotteryActivity.setAwardInfo(awardInfo);
        }

        boolean success = this.addJoinUserInfoList(appletLotteryActivity, userId, offset, size);
        if (!success) {
            log.error("活动数据回载防重, activityId = {}", activity.getId());
            return false;
        }
        this.addUserWinningProbability(appletLotteryActivity);

        return true;
    }

    /**
     * 分页添加 活动参与者信息
     * @param appletLotteryActivity
     * @param offset
     * @param size
     */
    private boolean addJoinUserInfoList(AppletLotteryActivity appletLotteryActivity, Long userId,  Long offset, Long size) {
        Activity activity = appletLotteryActivity.getActivityInfo();
        Long activityId = activity.getId();

        Long total = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        if (total == null || total == 0) {
            resetLotteryActivityInfo(appletLotteryActivity, userId, activity);
            total = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        }

        AppletMybatisPageDto<Map> joinUserInfoPage = new AppletMybatisPageDto<>(size, offset);
        appletLotteryActivity.setJoinUserInfoPage(joinUserInfoPage);

        total = total == null ? 0 : total;
        //查询redis,参与人展现,直接倒叙排列 -- 分页获取
        List<String> range = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, offset, offset + size - 1);
        //判断redis数据是否为空，为空则去数据库插入再次更新insert一波，非空直接返回
        List<Map> joinUserInfoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(range)) {
            for (String userInfo : range) {
                Map joinUserInfo = JSON.parseObject(userInfo, Map.class);
                joinUserInfoList.add(joinUserInfo);
            }
        }
        joinUserInfoPage.setContent(joinUserInfoList);
        if (offset + size < total) {
            joinUserInfoPage.setPullDown(true);
        }

        AppletMybatisPageDto<Map> pubJoinUserInfoPage = new AppletMybatisPageDto<>(size, offset);
        appletLotteryActivity.setPubJoinUserInfoPage(pubJoinUserInfoPage);

        Long pubJoinUserTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userId);
        if (pubJoinUserTotal == null || pubJoinUserTotal == 0) {
            resetLotteryActivityInfo(appletLotteryActivity, userId, activity);
            pubJoinUserTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userId);
        }
        pubJoinUserTotal = pubJoinUserTotal == null ? 0 : pubJoinUserTotal;
        List<String> pubJoinUserRange = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userId, offset, offset + size -1);
        List<Map> PubJoinUserInfoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(pubJoinUserRange)) {
            for (String userInfo : pubJoinUserRange) {
                Map joinUserInfo = JSON.parseObject(userInfo, Map.class);
                PubJoinUserInfoList.add(joinUserInfo);
            }
        }
        pubJoinUserInfoPage.setContent(PubJoinUserInfoList);
        if (offset + size < pubJoinUserTotal) {
            pubJoinUserInfoPage.setPullDown(true);
        }

        AppletMybatisPageDto<Map> winnerInfoPage = new AppletMybatisPageDto<>(size, offset);
        appletLotteryActivity.setWinnerInfoPage(winnerInfoPage);

        if (appletLotteryActivity.isAwarded()) {
            Long winnerSize = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId);
            if (winnerSize == null || winnerSize == 0) {
                resetLotteryActivityWinnerInfo(appletLotteryActivity);
                winnerSize = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId);
                winnerSize = winnerSize == null ? 0 : winnerSize;
            }

            // 获取中奖者名单
            List<Long> winnerIds = new ArrayList<>();
            String winnersResult = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_WINNER + activityId);
            if (StringUtils.isNotBlank(winnersResult)) {
                winnerIds = JSON.parseArray(winnersResult, Long.class);
            }
            appletLotteryActivity.setWinnerIds(winnerIds);

            List<String> winnerRange = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, offset, offset + size - 1);
            List<Map> winnerInfoList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(winnerRange)) {
                for (String userInfo : winnerRange) {
                    Map winnerUserInfo = JSON.parseObject(userInfo, Map.class);
                    winnerInfoList.add(winnerUserInfo);
                }
            }
            winnerInfoPage.setContent(winnerInfoList);
            if (offset + size < winnerSize) {
                winnerInfoPage.setPullDown(true);
            }

            // 获取 中奖任务id
            QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
            userTaskWrapper.lambda()
                    .eq(UserTask::getActivityId, activityId)
                    .eq(UserTask::getUserId, userId);
            UserTask userTask = userTaskService.getOne(userTaskWrapper);
            appletLotteryActivity.setUserTask(userTask);
        }
        return true;
    }

    private void resetLotteryActivityWinnerInfo(AppletLotteryActivity appletLotteryActivity) {
        Long activityId = appletLotteryActivity.getId();
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId);

        if (appletLotteryActivity.isAwarded()) {
            // 获取中奖者名单
            List<Long> winnerIds = new ArrayList<>();
            String winnersResult = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_WINNER + activityId);
            if (StringUtils.isBlank(winnersResult)) {
                resetLotteryActivityInfo(appletLotteryActivity, appletLotteryActivity.getVisitUserId(), appletLotteryActivity.getActivityInfo());
                winnersResult = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_WINNER + activityId);
            }

            if (StringUtils.isNotBlank(winnersResult)) {
                winnerIds = JSON.parseArray(winnersResult, Long.class);
            }
            appletLotteryActivity.setWinnerIds(winnerIds);

            Map<Long, User> userMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(winnerIds)) {
                Collection<User> users = userService.listByIds(winnerIds);
                if (!CollectionUtils.isEmpty(users)) {
                    for (User user : users) {
                        userMap.put(user.getId(), user);
                    }
                }
            }

            for (Long winnerId : winnerIds) {
                User user = userMap.get(winnerId);
                if (user == null) {
                    continue;
                }
                Map<String, Object> memberUserInfo = appletLotteryActivity.becomeMemberByUser(user);
                this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId, JSON.toJSONString(memberUserInfo));
            }
        }

    }

    /**
     * 重新添加 抽奖活动的信息
     *
     * @param appletLotteryActivity
     * @param userId
     * @param activity
     */
    private void resetLotteryActivityInfo(AppletLotteryActivity appletLotteryActivity, Long userId, Activity activity) {
        if (appletLotteryActivity.isPublished()) {
            return;
        }
        Long activityId = activity.getId();
        // 初始化状态
        List<Long> winnerIds = new ArrayList<>();
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId);
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_WINNER + activityId);
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_LOSER_SET + activityId);


        Map<String, Object> userActivityParams = new HashMap<>();
        userActivityParams.put("merchantId", activity.getMerchantId());
        userActivityParams.put("shopId", activity.getShopId());
        userActivityParams.put("activityId", activityId);
        userActivityParams.put("order", "ctime");
        userActivityParams.put("direct", "asc");
        //状态不为0-->意义是未参加的用户==脏数据的存储
        userActivityParams.put("statusNot", UserActivityStatusEnum.NO_QUALIFICATION.getCode());
        List<UserActivity> userActivities = this.userActivityService.pageListUserActivity(userActivityParams);
        if (CollectionUtils.isEmpty(userActivities)) {
            return;
        }

        Set<Long> userIdSet = new HashSet<>();
        for (UserActivity userActivity : userActivities) {
            userIdSet.add(userActivity.getUserId());
        }
        List<Long> userIdList = new ArrayList<>(userIdSet);

        Map<Long, User> userMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userIdList)) {
            Map<String, Object> userParams = new HashMap<>();
            userParams.put("userIds", userIdList);
            List<User> users = this.userService.query(userParams);
            if (!CollectionUtils.isEmpty(users)) {
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }
        }

        for (UserActivity userActivity : userActivities) {
            Long userActivityUserId = userActivity.getUserId();
            this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userActivityUserId);

            User user = userMap.get(userActivityUserId);
            if (user == null) {
                continue;
            }

            Map<String, Object> memberUserInfo = new HashMap<>();
            if (activity.getUserId().longValue() == userActivityUserId.longValue()) {
                if (!appletLotteryActivity.getCreateRule().isJoinSelf()) {
                    log.info("发起者本人不参与活动");
                    continue;
                }
                // TODO 发起者用户信息 -- 平台免单抽奖无需考虑
            } else {
                memberUserInfo = appletLotteryActivity.becomeMemberByUser(user);
            }

            Object value = this.stringRedisTemplate.opsForHash().get(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userActivityUserId.toString());
            if (value != null) {
                continue;
            }

            List awardNo = (List) memberUserInfo.get("awardNo");
            this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userActivityUserId.toString(), awardNo.size());
            this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, 1, TimeUnit.DAYS);

            this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, JSON.toJSONString(memberUserInfo));
            this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, 1, TimeUnit.DAYS);

            // 用户邀请列表 -- 包含自己
            this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userActivityUserId, JSON.toJSONString(memberUserInfo));
            this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userActivityUserId, 1, TimeUnit.DAYS);

            if (userActivity.getStatus() == UserActivityStatusEnum.WINNER.getCode()) {
                winnerIds.add(userActivityUserId);
            }

            // 添加邀请信息
            Map<String, Object> userPubJoinParams = new HashMap<>();
            userPubJoinParams.put("userId", userActivityUserId);
            userPubJoinParams.put("activityId", activityId);
            userPubJoinParams.put("subActionStatus", UserPubJoinStatusEnum.JOINED.getCode());
            userPubJoinParams.put("order", "ctime");
            userPubJoinParams.put("direct", "asc");
            List<UserPubJoin> userPubJoins = this.userPubJoinService.listPageByParams(userPubJoinParams);
            if (CollectionUtils.isEmpty(userPubJoins)) {
                continue;
            }

            for (UserPubJoin userPubJoin : userPubJoins) {
                User subUser = userMap.get(userPubJoin.getSubUid());
                // 加工用户邀请列表的用户信息 放入redis
                Map<String, Object> subMemberUserInfo = appletLotteryActivity.becomeMemberByUser(subUser);
                this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userPubJoin.getUserId(), JSON.toJSONString(subMemberUserInfo));
                this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userPubJoin.getUserId(), 1, TimeUnit.DAYS);

                // 更新redis抽奖概率
                this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userPubJoin.getUserId().toString(), 1);
                this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, 1, TimeUnit.DAYS);
            }
        }

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER + activityId, JSONObject.toJSONString(winnerIds));
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER + activityId,1, TimeUnit.DAYS);

        Map<Object, Object> userCountMap = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId);
        for (Map.Entry<Object, Object> entry : userCountMap.entrySet()) {
            Long joinerId = Long.parseLong((String) entry.getKey());
            if (winnerIds.contains(joinerId)) {
                continue;
            }
            stringRedisTemplate.opsForSet().add(RedisKeyConstant.ACTIVITY_LOSER_SET + activityId, userId.toString());
        }
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_LOSER_SET + activityId, 1, TimeUnit.DAYS);
    }

    @Transactional
    @Override
    public boolean saveJoinLotteryActivity(AppletLotteryActivity appletLotteryActivity, Long userId, Long pubUserId) {
        // 更新用户邀请记录
        boolean save = updateActivityInviteEvent(appletLotteryActivity);
        if (!save) {
            log.error("更新活动邀请记录异常, activityId = {}, pubUserId = {}, subUserId = {}", appletLotteryActivity.getId(), pubUserId, userId);
            throw new RuntimeException();
        }
        // 保存用户参与事件
        saveUserJoinEvent(appletLotteryActivity, false);

        Long activityId = appletLotteryActivity.getId();
        if (pubUserId != null) {
            // 该邀请人未参加此活动  处理
            Object pubUidObj = stringRedisTemplate.opsForHash().get(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, pubUserId.toString());
            if (pubUidObj == null) {
                pubUserId = null;
            }
        }
        // 判断判断pubUid是否存在
        if (pubUserId == null) {
            // 更新redis抽奖概率
            stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userId.toString(), 1);
        } else {
            // redis更新用户邀请列表 LPUSH
            stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + pubUserId, JSON.toJSONString(appletLotteryActivity.getJoinUserInfo()));
            // 更新redis抽奖概率
            stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, pubUserId.toString(), 1);
            stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, userId.toString(), 1);
        }
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, JSON.toJSONString(appletLotteryActivity.getJoinUserInfo()));
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);

        // 用户邀请列表 -- 包含自己
        this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userId, JSON.toJSONString(appletLotteryActivity.getJoinUserInfo()));
        this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityId + ":" + userId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);

        return true;
    }

    /**
     * 抽奖活动 开奖后，增装 用户中奖概率信息
     * @param appletLotteryActivity
     */
    @Override
    public void addUserWinningProbability(AppletLotteryActivity appletLotteryActivity) {
        Map<Object, Object> userWinningProbability = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + appletLotteryActivity.getId());
        appletLotteryActivity.setUserWinningProbability(userWinningProbability);
    }

    /**
     * 装配 抽奖活动
     * @param appletActivity
     * @return
     */
    @Override
    public AppletLotteryActivity assemblyAppletLotteryActivity(AppletActivity appletActivity, Long userId) {
        Activity activity = appletActivity.getActivityInfo();

        AppletLotteryActivity appletLotteryActivity = new AppletLotteryActivity();
        appletLotteryActivity.setId(activity.getId());
        appletLotteryActivity.setActivityDefId(activity.getActivityDefId());
        appletLotteryActivity.setActivityInfo(activity);
        appletLotteryActivity.setUserInfo(appletActivity.getUserInfo());
        appletLotteryActivity.setJoinRule(appletActivity.getJoinRule());
        appletLotteryActivity.setCreateRule(appletActivity.getCreateRule());
        appletLotteryActivity.setUserBoardConfig(appletActivity.getUserBoardConfig());
        appletLotteryActivity.setVisitUserId(userId);

        return appletLotteryActivity;
    }

    /**
     * 装配 抽奖活动
     * @param activity
     * @return
     */
    @Override
    public AppletLotteryActivity assemblyAppletLotteryActivity(Activity activity) {

        AppletLotteryActivity appletLotteryActivity = new AppletLotteryActivity();
        appletLotteryActivity.setId(activity.getId());
        appletLotteryActivity.setActivityDefId(activity.getActivityDefId());
        appletLotteryActivity.setActivityInfo(activity);
        appletLotteryActivity.setJoinRule(new JoinRule(activity));
        appletLotteryActivity.setCreateRule(new CreateRule(activity));
        appletLotteryActivity.setVisitUserId(activity.getUserId());

        return appletLotteryActivity;
    }

    /**
     * 获取 本次离开时间段的 助力的信息
     *
     * @param activityId
     * @param userId
     * @return
     */
    private List<Map<String, Object>> getAssistDialogList(Long activityId, Long userId) {
        List<Map<String, Object>> assistDialogList = new ArrayList<>();
        Boolean hasKey = stringRedisTemplate.hasKey(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
        hasKey = hasKey == null ? false : hasKey;
        if (hasKey) {
            String assistDialogs = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
            if (StringUtils.isNotBlank(assistDialogs)) {
                JSONArray jsonArray = JSON.parseArray(assistDialogs);
                if (!CollectionUtils.isEmpty(jsonArray)) {
                    for (Object obj : jsonArray) {
                        Map map = (Map) obj;
                        Map<String, Object> memberInfo = new HashMap<>();
                        memberInfo.put("userId", map.get("userId").toString());
                        memberInfo.put("avatarUrl", map.get("avatarUrl").toString());
                        memberInfo.put("nickName", map.get("nickName").toString());
                        memberInfo.put("time", map.get("time").toString());
                        memberInfo.put("amount", map.get("amount").toString());
                        assistDialogList.add(memberInfo);
                    }
                }
            }
        }
        return assistDialogList;
    }

    /**
     * 拆红包活动  -- 邀请者信息
     *
     * @param activityDefId
     * @param startUserInfo
     * @return
     */
    private Map<String, Object> getPubUserInfo(Long activityDefId, Map<String, Object> startUserInfo) {
        Map<String, Object> pubUserInfo = new HashMap<>();

        Object pubUserIdObj = startUserInfo.get("pubUserId");
        Long pubUserId = pubUserIdObj == null ? null : Long.valueOf(pubUserIdObj.toString());
        if (null != pubUserId) {
            Object pubUserObj = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId, pubUserId.toString());
            if (pubUserObj != null) {
                JSONObject jsonObject = JSON.parseObject(pubUserObj.toString());

                Date pubTime = DateUtil.stringtoDate(jsonObject.getString("time"), DateUtil.POINT_DATE_FORMAT_TWO);
                Date time = DateUtil.stringtoDate(startUserInfo.get("time").toString(), DateUtil.POINT_DATE_FORMAT_TWO);

                if (pubTime != null && time != null && time.getTime() >= pubTime.getTime()) {
                    JSONObject pubUserJson = JSON.parseObject(pubUserObj.toString());

                    pubUserInfo.put("userId", pubUserJson.getLong("userId"));
                    pubUserInfo.put("avatarUrl", pubUserJson.getString("avatarUrl"));
                    pubUserInfo.put("nickName", pubUserJson.getString("nickName"));
                    pubUserInfo.put("time", pubUserJson.getString("time"));
                    pubUserInfo.put("amount", pubUserJson.getBigDecimal("amount"));

                    pubUserInfo.put("pubUserId", pubUserJson.getLong("pubUserId"));
                    pubUserInfo.put("awardPool", pubUserJson.getBigDecimal("awardPool"));
                    pubUserInfo.put("activityId", pubUserJson.getLong("activityId"));
                }
            }
        }
        return pubUserInfo;
    }

    /**
     * 拆红包活动  -- 助力者信息
     *
     * @param openRedPackageActivity
     * @return
     */
    private Map<String, Map<String, Object>> getAssistUserInfoList(AppletAssistActivity openRedPackageActivity) {
        Long activityId = openRedPackageActivity.getId();

        // 获取redis中 助力者信息
        Map<String, Map<String, Object>> assistUserInfoList = new HashMap<>();
        String assistUser = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId);
        if (StringUtils.isNotBlank(assistUser)) {
            JSONArray jsonArray = JSON.parseArray(assistUser);
            for (Object obj : jsonArray) {
                Map map = (Map) obj;
                Map<String, Object> memberInfo = new HashMap<>();
                memberInfo.put("userId", map.get("userId").toString());
                memberInfo.put("avatarUrl", map.get("avatarUrl").toString());
                memberInfo.put("nickName", map.get("nickName").toString());
                memberInfo.put("time", map.get("time").toString());
                memberInfo.put("amount", map.get("amount").toString());
                assistUserInfoList.put(map.get("userId").toString(), memberInfo);
            }
        }
        return assistUserInfoList;
    }

    /**
     * 拆红包活动  -- 获取发起人信息
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    private Map<String, Object> getStartUserInfo(Long activityDefId, Long userId) {

        Map<String, Object> startUserInfo = new HashMap<>();
        Object startUserObj = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId, userId.toString());
        if (startUserObj != null) {
            JSONObject jsonObject = JSON.parseObject(startUserObj.toString());

            startUserInfo.put("userId", jsonObject.getLong("userId"));
            startUserInfo.put("avatarUrl", jsonObject.getString("avatarUrl"));
            startUserInfo.put("nickName", jsonObject.getString("nickName"));
            startUserInfo.put("time", jsonObject.getString("time"));
            startUserInfo.put("amount", jsonObject.getBigDecimal("amount"));

            startUserInfo.put("pubUserId", jsonObject.getLong("pubUserId"));
            startUserInfo.put("awardPool", jsonObject.getBigDecimal("awardPool"));
            startUserInfo.put("activityId", jsonObject.getLong("activityId"));
        }
        return startUserInfo;
    }

    /**
     * 保存开奖的活动和用户参与信息以及用户任务信息 -- 含扣库存逻辑 -- 事务支持
     *
     * @param appletActivity
     * @return
     */
    private boolean saveLotteryByStockType(AppletActivity appletActivity) {
        // 扣库存逻辑
        Activity activityInfo = appletActivity.getActivityInfo();
        if (ActivityDefStockTypeEnum.COMPLETE_REDUCE.getCode().equals(activityInfo.getStockType())) {
            if (activityInfo.getType().equals(ActivityTypeEnum.OPEN_RED_PACKET_START.getCode())) {
                Integer rows = activityDefService.reduceDeposit(appletActivity.getActivityDefId(), activityInfo.getPayPrice(), 1);
                if (rows == 0) {
                    log.error("扣库存失败，库存不足");
                    return false;
                }
            } else {
                Integer rows = activityDefService.reduceDeposit(appletActivity.getActivityDefId(), BigDecimal.ZERO, 1);
                if (rows == 0) {
                    log.error("扣库存失败，库存不足");
                    return false;
                }
            }

        }
        // 活动状态更新
        boolean update = activityService.updateById(activityInfo);
        if (!update) {
            log.error("数据插入失败，数据库异常");
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + appletActivity.getId(), -1);
            throw new RuntimeException();
        }
        // 保存或更新用户中奖记录
        Map<Long, UserJoinEvent> winEventMap = appletActivity.getWinEventMap();
        if (CollectionUtils.isEmpty(winEventMap)) {
            return true;
        }
        // 获取所有待更新 或 待插入 的中奖事件
        List<UserActivity> updateWinEventList = new ArrayList<>();
        List<UserActivity> saveWinEventList = new ArrayList<>();
        for (UserJoinEvent userJoinEvent : winEventMap.values()) {
            // 如果id 为空 则新增参与事件
            if (userJoinEvent.getUserActivityInfo().getId() == null) {
                saveWinEventList.add(userJoinEvent.getUserActivityInfo());
            } else {
                updateWinEventList.add(userJoinEvent.getUserActivityInfo());
            }
        }
        if (!CollectionUtils.isEmpty(updateWinEventList)) {
            update = userActivityService.updateBatchById(updateWinEventList, 100);
            if (!update) {
                log.error("数据更新失败，数据库异常");
                stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + appletActivity.getId(), -1);
                throw new RuntimeException();
            }
        }
        if (!CollectionUtils.isEmpty(saveWinEventList)) {
            boolean save = userActivityService.saveBatch(updateWinEventList, 100);
            if (!save) {
                log.error("数据插入失败，数据库异常");
                stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + appletActivity.getId(), -1);
                throw new RuntimeException();
            }
        }

        // 保存用户参与事件
        Map<Long, UserJoinEvent> userJoinEventMap = appletActivity.getUserJoinEventMap();
        if (!CollectionUtils.isEmpty(userJoinEventMap)) {
            // 获取所有待插入事件
            List<UserActivity> joinUserActivityList = new ArrayList<>();
            for (UserJoinEvent userJoinEvent : userJoinEventMap.values()) {
                // 如果id 为空 则新增参与事件
                if (userJoinEvent.getUserActivityInfo().getId() == null) {
                    joinUserActivityList.add(userJoinEvent.getUserActivityInfo());
                }
                // 如果id 不为空 则更新参与事件 --- 事件 正常业务 是不变得，但本系统却是可变的 -- 待处理
            }
            if (!CollectionUtils.isEmpty(joinUserActivityList)) {
                // 排除待保存的用户参与事件 已被中奖保存的情况
                List<UserActivity> saveJoinEvent = new ArrayList<>();
                for (UserActivity joinUserActivity : joinUserActivityList) {
                    boolean equals = false;
                    for (UserActivity winUserActivity : saveWinEventList) {
                        if (winUserActivity.getUserId().equals(joinUserActivity.getUserId())) {
                            equals = true;
                        }
                    }
                    if (!equals) {
                        saveJoinEvent.add(joinUserActivity);
                    }
                }
                if (!CollectionUtils.isEmpty(saveJoinEvent)) {
                    boolean save = userActivityService.saveBatch(saveJoinEvent, 100);
                    if (!save) {
                        log.error("数据插入失败，数据库异常");
                        stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + appletActivity.getId(), -1);
                        throw new RuntimeException();
                    }
                }
            }
        }
        // 获取所有中奖任务
        Map<Long, UserTask> winnerTask = appletActivity.getWinnerTask();
        if (CollectionUtils.isEmpty(winnerTask)) {
            return true;
        }
        // 获取所有待插入的中奖任务
        List<UserTask> userTaskList = new ArrayList<>();
        for (UserTask userTask : winnerTask.values()) {
            // 如果id 为空 则新增参与事件
            if (userTask.getId() == null) {
                userTaskList.add(userTask);
            }
            // 无 更新中奖任务
        }
        if (CollectionUtils.isEmpty(userTaskList)) {
            return true;
        }
        boolean save = userTaskService.saveBatch(userTaskList, 100);
        if (!save) {
            log.error("数据插入失败，数据库异常");
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + appletActivity.getId(), -1);
            throw new RuntimeException();
        }

        return true;
    }
}
