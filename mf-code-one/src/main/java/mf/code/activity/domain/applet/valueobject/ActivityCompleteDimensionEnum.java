package mf.code.activity.domain.applet.valueobject;

import mf.code.activity.constant.ActivityDefTypeEnum;

import javax.activity.ActivityCompletedException;

/**
 * 可重复参与维度枚举类
 * <p>
 * 针对不同活动，可重复参与的条件（维度）不同
 * <p>
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-20 下午2:35
 */
public enum ActivityCompleteDimensionEnum {

    /**
     * 店铺维度 -- 按店铺维度，计算用户完成活动的次数
     */
    SHOP(1, "店铺维度"),

    /**
     * 商品维度 -- 按商品维度，计算用户完成活动的次数
     */
    GOODS(2, "商品维度"),

    /**
     * 活动定义维度 -- 按活动定义维度，计算用户完成活动的次数
     */
    ACTIVITYDEF(3, "活动定义维度"),

    /**
     * 订单维度 -- 按订单维度，计算用户完成活动的次数
     */
    ORDER(4, "订单维度"),
    ;

    /**
     * code
     */
    private Integer code;

    /**
     * 描述
     */
    private String desc;

    ActivityCompleteDimensionEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ActivityCompleteDimensionEnum findByActivityDefType(Integer type) {

        if (ActivityDefTypeEnum.MCH_PLAN.getCode() == type) {
            return ActivityCompleteDimensionEnum.ACTIVITYDEF;
        }
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == type) {
            return ActivityCompleteDimensionEnum.ACTIVITYDEF;
        }
        if (ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == type) {
            return ActivityCompleteDimensionEnum.ACTIVITYDEF;
        }
        if (ActivityDefTypeEnum.NEW_MAN.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == type) {
            return ActivityCompleteDimensionEnum.SHOP;
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == type) {
            return ActivityCompleteDimensionEnum.ORDER;
        }
        if (ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode() == type) {
            return ActivityCompleteDimensionEnum.ORDER;
        }
        if (ActivityDefTypeEnum.ASSIST.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.ASSIST_V2.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.GIFT.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.RED_PACKET.getCode() == type) {
            return ActivityCompleteDimensionEnum.ACTIVITYDEF;
        }
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == type) {
            // 拆红包提升为店铺维度，一个拆红包活动定义同一时间只能有在一个进行中
            // 已店铺维度获取，用户在进行中的拆红包活动，以避免店铺切换活动定义时，用户出现两个进行中的拆红包活动
            return ActivityCompleteDimensionEnum.SHOP;
        }
        if (ActivityDefTypeEnum.GOOD_COMMENT.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.FAV_CART.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.FAV_CART_V2.getCode() == type) {
            return ActivityCompleteDimensionEnum.GOODS;
        }
        if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == type) {
            return ActivityCompleteDimensionEnum.ACTIVITYDEF;
        }
        if (ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode() == type) {
            return ActivityCompleteDimensionEnum.ACTIVITYDEF;
        }
        // 异常类型
        return null;
    }
}
