package mf.code.activity.domain.applet.aggregateroot;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.domain.applet.valueobject.*;
import mf.code.activity.domain.applet.valueobject.config.CreateRule;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.common.constant.*;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.utils.DateUtil;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.activitydetail.AssertUserDto;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 由 OpenRedPackage 提为 AppletAssistActivity
 * <p>
 * mf.code.activity.domain.applet.aggregateroot
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-14 13:35
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class AppletAssistActivity extends AppletActivity {
    /**
     * 查看活动的场景
     */
    protected ScenesEnum scenesEnum;
    /**
     * 邀请人 邀请的活动信息
     */
    protected Map<String, Object> pubUserInfo = new HashMap<>();
    /**
     * 助力者们的信息  Map<UserId, Map<String, Object>>
     */
    protected Map<String, Map<String, Object>> assistUserInfoMap = new HashMap<>();
    /**
     * 助力金额集合 -- 从中拆得助力金额
     */
    protected List<BigDecimal> assistAmountList;
    /**
     * 本次拆得的助力金额
     */
    protected BigDecimal openAmount;
    /**
     * 帮好友拆得的资金
     */
    protected BigDecimal helpAmount;
    /**
     * 本次离开时间段的 助力的信息
     */
    protected List<Map<String, Object>> assistDialogList = new ArrayList<>();
    /**
     * 是否有中奖弹框
     */
    protected Boolean isAwardDialog = false;
    /**
     * 是否有助力弹框
     */
    protected Boolean isAssistDialog = false;
    /**
     * 中奖者奖品
     */
    protected Map<Long, UserCoupon> winnerCoupon;
    /***
     * 用户的助力情况-时间倒叙
     */
    private AppletMybatisPageDto<AssertUserDto> assertListPage = new AppletMybatisPageDto<>(6, 0);

    /**
     * 用户是否可以参与活动
     *
     * @param userId
     * @return
     */
    public boolean canJoinByUserId(Long userId) {
        if (this.activityInfo.getUserId().equals(userId)) {
            return false;
        }
        if (!CollectionUtils.isEmpty(this.assistUserInfoMap)) {
            Map<String, Object> stringObjectMap = this.assistUserInfoMap.get(userId.toString());
            if (stringObjectMap != null) {
                // 已助力
                return false;
            }
        }
        return this.isValid();
    }

    /**
     * 进行中的活动 是否已过期
     *
     * @return true 已过期，false 未过期
     */
    @Override
    public boolean isExpired() {
        if (isAwarded()) {
            // 已开奖，不处理过期
            return false;
        }
        if (queryLeftTime() <= 0L) {
            return true;
        }
        return ActivityStatusEnum.FAILURE.getCode() == this.activityInfo.getStatus();
    }

    /**
     * 获取 拆开红包 还差好友数 = 开团人数 - 助力人数 - 自己
     *
     * @return
     */
    public Integer queryLiftFriends() {
        return this.getActivityInfo().getCondPersionCount() - this.assistUserInfoMap.size() - 1;
    }

    /**
     * 通过 访问页面场景， 获取 相应 活动弹窗状态
     *
     * @return
     */
    public ActivityPopupStatusEnum queryActivityPopupStatus() {
        if (!this.isPublished()) {
            // 活动主页场景 不在进行中，已过期 活动的状态
            if (ScenesEnum.ACTIVITY_PAGE == this.scenesEnum) {
                if (this.isExpired() && this.activityInfo.getUserId().equals(this.visitUserId)) {
                    return ActivityPopupStatusEnum.FAILED;
                }
                // 不在进行中，已完成 活动的状态
                return ActivityPopupStatusEnum.HAVE_ONE;
            }
            // 其他场景 不在进行中的 活动的状态
            return ActivityPopupStatusEnum.NONE;
        }

        // 进行中 -- 是否已开奖
        if (this.isAwarded()) {
            if (ScenesEnum.HOME_PAGE == this.scenesEnum) {
                // 首页和商品详情页 进行中但已开奖 -- 13.弹出 你有10元红包待提现 -- 首页和商品详情页
                return ActivityPopupStatusEnum.PENDING_CASH;
            }
            if (ScenesEnum.ACTIVITY_PAGE == this.scenesEnum) {
                // 活动页 -- 是否 需要开奖提醒 并且 访问者 是活动发起者
                if (this.isAwardDialog && this.activityInfo.getUserId().equals(this.visitUserId)) {
                    // 中奖提醒 弹窗
                    return ActivityPopupStatusEnum.AWARD;
                }
                // 已开奖，不弹助力信息
                // if (this.isAssistDialog && this.activityInfo.getUserId().equals(this.visitUserId)) {
                // 	// 助力提醒 弹窗
                // 	return OpenRedPackageActivityPopupStatusEnum.GET_HELP;
                // }
                if (!this.activityInfo.getUserId().equals(this.visitUserId)) {
                    // 无需提醒 -- 进行中但已开奖：帮拆不成功，但给访问者拆得弹框 ：分享给你一个5元红包，立即来拆开吧
                    return ActivityPopupStatusEnum.SHARE_OPEN;
                }

                return ActivityPopupStatusEnum.NONE;
            }
            if (ScenesEnum.CASH_PAGE == this.scenesEnum) {
                // 提现页 --  9.完成下单即可提现现金 10.完成支付即可提现现金 11.您的钱包中有5元可提现,还有10元待解锁
                return ActivityPopupStatusEnum.PENDING_STATE;
            }

            log.error("场景值异常， scenes = {}", this.scenesEnum.getCode());
            return ActivityPopupStatusEnum.NONE;
        }
        // 进行中 -- 未开奖
        // 活动是否过期
        if (this.isExpired() && this.activityInfo.getUserId().equals(this.visitUserId)) {
            // 过期 -- 所有场景
            return ActivityPopupStatusEnum.FAILED;
        }

        // 未过期
        if (ScenesEnum.HOME_PAGE == this.scenesEnum) {
            // 首页和商品详情页
            // 12.弹出 你有10元红包待拆开 还差2个好友就能拆开啦 -- 首页和商品详情页
            return ActivityPopupStatusEnum.FEW_FRIENDS;

        }
        if (ScenesEnum.ACTIVITY_PAGE == this.scenesEnum) {
            // 活动页
            if (this.helpAmount != null && this.activityInfo.getUserId().equals(this.visitUserId)) {
                return ActivityPopupStatusEnum.HELP_OPEN;
            }
            // 助力者列表弹窗 并且 访问者 是 活动发起者
            if (this.isAssistDialog && this.activityInfo.getUserId().equals(this.visitUserId)) {
                return ActivityPopupStatusEnum.GET_HELP;
            }
            // 访问者 不是 活动发起者，帮拆
            if (!this.activityInfo.getUserId().equals(this.visitUserId)) {
                return ActivityPopupStatusEnum.SHARE_OPEN;
            }
            return ActivityPopupStatusEnum.NONE;
        }
        if (ScenesEnum.CASH_PAGE == this.scenesEnum) {
            return ActivityPopupStatusEnum.NONE;
        }

        log.error("场景值异常， scenes = {}", this.scenesEnum.getCode());
        return ActivityPopupStatusEnum.NONE;
    }

    /**
     * 将用户成为参与者
     * <per>
     * userId：用户id
     * nickName：用户呢称
     * avatarUrl：用户头像
     * amount：拆得的金额
     * time：参与时间
     * </per>
     *
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> becomeMemberByUser(User user) {
        this.openAmount = this.assistAmountList.get(trigger.intValue() - 1);

        Map<String, Object> memberInfo = new HashMap<>();
        memberInfo.put("userId", user.getId());
        memberInfo.put("avatarUrl", user.getAvatarUrl());
        memberInfo.put("nickName", user.getNickName());
        memberInfo.put("time", DateUtil.dateToString(new Date(), DateUtil.POINT_DATE_FORMAT_TWO));
        memberInfo.put("amount", this.openAmount);

        if (!this.activityInfo.getUserId().equals(user.getId())) {
            if (CollectionUtils.isEmpty(this.assistUserInfoMap)) {
                this.assistUserInfoMap = new HashMap<>();
            }
            this.assistUserInfoMap.put(user.getId().toString(), memberInfo);

            if (CollectionUtils.isEmpty(this.assistDialogList)) {
                this.assistDialogList = new ArrayList<>();
            }
            this.assistDialogList.add(memberInfo);
        }

        return memberInfo;
    }

    /**
     * 从数据库重新 装配 助力成员
     * <per>
     * userId：用户id
     * nickName：用户呢称
     * avatarUrl：用户头像
     * amount：拆得的金额
     * time：参与时间
     * </per>
     *
     * @param user
     * @return
     */
    public void setMemberByUser(User user, UserCoupon userCoupon) {
        Map<String, Object> memberInfo = new HashMap<>();
        memberInfo.put("userId", user.getId());
        memberInfo.put("avatarUrl", user.getAvatarUrl());
        memberInfo.put("nickName", user.getNickName());
        memberInfo.put("time", DateUtil.dateToString(userCoupon.getCtime(), DateUtil.POINT_DATE_FORMAT_TWO));
        memberInfo.put("amount", userCoupon.getAmount());

        if (!this.activityInfo.getUserId().equals(user.getId())) {
            if (CollectionUtils.isEmpty(this.assistUserInfoMap)) {
                this.assistUserInfoMap = new HashMap<>();
            }
            this.assistUserInfoMap.put(user.getId().toString(), memberInfo);
        } else {
            this.startUserInfo = new HashMap<>(memberInfo);
            this.startUserInfo.put("awardPool", this.activityInfo.getPayPrice());
            this.startUserInfo.put("activityId", this.id);
        }
    }


    /**
     * 从参与事件中 -- 抽出中奖事件, 并发起中奖者任务
     *
     * @return 中奖者userid
     */
    @Override
    public List<Long> drawLottery() {
        Date now = new Date();

        this.winEventMap = new HashMap<>();
        this.winnerTask = new HashMap<>();
        List<Long> winnerIds = new ArrayList<>();
        // 中奖者 就是 活动发起者
        Long userId = activityInfo.getUserId();
        winnerIds.add(userId);

        UserJoinEvent userJoinEvent = this.userJoinEventMap.get(userId);
        UserActivity userActivityInfo = userJoinEvent.getUserActivityInfo();
        userActivityInfo.setStatus(UserActivityStatusEnum.WINNER.getCode());
        userActivityInfo.setUtime(now);
        userJoinEvent.setUserActivityInfo(userActivityInfo);
        this.winEventMap.put(userId, userJoinEvent);

        // 助力着 颁发奖品
        if (CollectionUtils.isEmpty(this.assistUserInfoMap)) {
            return winnerIds;
        }
        for (Map<String, Object> assistUserInfo : this.assistUserInfoMap.values()) {
            if (CollectionUtils.isEmpty(this.winnerCoupon)) {
                this.winnerCoupon = new HashMap<>();
            }
            UserCoupon userCoupon = awardPrize(assistUserInfo);
            this.winnerCoupon.put(userCoupon.getUserId(), userCoupon);
        }
        if (this.createRule.isJoinSelf()) {
            // 发起者 作为助力着，颁发奖品
            UserCoupon userCoupon = awardPrize(startUserInfo);
            userCoupon.setType(UserCouponTypeEnum.findByActivityTypeStarter(activityInfo.getType(), false));
            userCoupon.setStatus(UserCouponStatusEnum.UNRECEIVED.getCode());
            if (CollectionUtils.isEmpty(this.winnerCoupon)) {
                this.winnerCoupon = new HashMap<>();
            }
            this.winnerCoupon.put(userCoupon.getUserId(), userCoupon);
        }

        // 给发起者 颁奖 -- 最终实际金额
        UserCoupon userCoupon = awardPrize(startUserInfo);
        userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());

        userCoupon.setAmount(this.queryPayAmount());
        if (CollectionUtils.isEmpty(this.winnerCoupon)) {
            this.winnerCoupon = new HashMap<>();
        }
        this.winnerCoupon.put(0L, userCoupon);
        return winnerIds;
    }

    private UserCoupon awardPrize(Map<String, Object> assistUserInfo) {
        Long helper = Long.valueOf(assistUserInfo.get("userId").toString());
        boolean starter = helper.equals(this.activityInfo.getUserId());

        Date now = new Date();

        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setShopId(this.activityInfo.getShopId());
        userCoupon.setMerchantId(this.activityInfo.getMerchantId());
        userCoupon.setUserId(helper);
        userCoupon.setType(UserCouponTypeEnum.findByActivityTypeStarter(this.activityInfo.getType(), starter));
        userCoupon.setActivityId(this.getId());
        userCoupon.setActivityDefId(this.activityInfo.getActivityDefId());
        userCoupon.setActivityTaskId(null);
        userCoupon.setAmount(new BigDecimal(assistUserInfo.get("amount").toString()));
        userCoupon.setAwardTime(DateUtil.stringtoDate(assistUserInfo.get("time").toString(), DateUtil.POINT_DATE_FORMAT_TWO));
        userCoupon.setStatus(UserCouponStatusEnum.UNRECEIVED.getCode());
        userCoupon.setCommission(BigDecimal.ZERO);
        userCoupon.setUtime(now);
        userCoupon.setCtime(now);

        return userCoupon;
    }

    /**
     * 拆红包核心算法
     *
     * @return
     */
    public void calculationAssistAmountList() {
        if (!CollectionUtils.isEmpty(this.assistAmountList)) {
            return;
        }

        Integer condPersionCount = this.activityInfo.getCondPersionCount();
        BigDecimal amount = this.activityInfo.getPayPrice();

        if (1 == condPersionCount) {
            this.assistAmountList = new ArrayList<>();
            this.assistAmountList.add(amount);
            return;
        }

        // 后部分人数
        int n = condPersionCount / 2;
        // 每人平均金额
        BigDecimal averAmount = amount.divide(new BigDecimal(condPersionCount), 2, RoundingMode.DOWN);
        // 对下一人抽取时，相对上一人金额刻度的 偏移量
        int vi = 1;
        // 抽取金额的随机范围（金额刻度）
        int size = 4;
        // 金额的随机取值范围和偏移的金额刻度之间的重合部分大小（金额刻度）
        int overSize = size - vi;
        //金额刻度长度为：
        int len = (n - 1) * vi + size;
        // 每人平均金额下再按人数划分的金额刻度
        BigDecimal scaleAmount = averAmount.divide(new BigDecimal(len), 2, RoundingMode.DOWN);

        LinkedList<BigDecimal> amountList = new LinkedList<>();
        // 计算 金额列表 并存入redis,List类型
        // 从中间开始计算随机金额，前一半使用leftPush, 后一半使用rightPush
        if (condPersionCount % 2 != 0) {
            amountList.addFirst(averAmount);
        }
        for (int i = 0; i < n; i++) {
            BigDecimal stepAmount = scaleAmount.multiply(new BigDecimal(Math.random() * size + vi * i));
            stepAmount = stepAmount.setScale(2, RoundingMode.DOWN);
            amountList.addLast(averAmount.subtract(stepAmount));
            amountList.addFirst(averAmount.add(stepAmount));
        }
        // 矫正 资金总额
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (BigDecimal unitAmount : amountList) {
            totalAmount = totalAmount.add(unitAmount);
        }
        BigDecimal leftAmount = amount.subtract(totalAmount);
        BigDecimal firstAmount = amountList.pollFirst();
        firstAmount = firstAmount.add(leftAmount);
        amountList.addFirst(firstAmount);
        this.assistAmountList = new ArrayList<>(amountList);
    }

    /**
     * 处理前端展示
     * -- 首页和商品详情页
     * -- 活动主页
     *
     * @return
     */
    public Map<String, Object> queryActivityPageVO() {
        // 处理页面返回值
        // 通过 访问页面场景， 获取 相应 活动弹窗状态
        ActivityPopupStatusEnum openRedPackageActivityPopupStatusEnum = this.queryActivityPopupStatus();
        if (ActivityPopupStatusEnum.PENDING_STATE == openRedPackageActivityPopupStatusEnum) {
            // "9,10,11 待定状态 -- 须进一步校验"
            // 提现页 --  9.完成下单即可提现现金 10.完成支付即可提现现金 11.您的钱包中有5元可提现,还有10元待解锁
            log.error("此接口不支持 提现页， to 夜辰Api");
            return null;
        }
        Object awardPoolObj = startUserInfo.get("awardPool");

        BigDecimal awardPool = awardPoolObj == null ? BigDecimal.ZERO : new BigDecimal(awardPoolObj.toString()).setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal activityAmount = activityInfo.getPayPrice().setScale(2, BigDecimal.ROUND_DOWN);
        if (this.activityInfo.getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            activityAmount = new BigDecimal(activityInfo.getKeyWordsJson());
        }

        // 需要展示活动信息 -- 加工前端页面展示的数据结构
        Map<String, Object> resultVO = new HashMap<>();
        if (ActivityPopupStatusEnum.FEW_FRIENDS == openRedPackageActivityPopupStatusEnum) {
            Integer integer = this.queryLiftFriends();
            resultVO.put("surplusFriend", integer);
        }
        resultVO.put("activityStatus", openRedPackageActivityPopupStatusEnum.getCode());
        resultVO.put("activityDefId", activityInfo.getActivityDefId());
        resultVO.put("activityId", activityInfo.getId());
        resultVO.put("money", activityAmount);
        resultVO.put("leftAmount", activityAmount.subtract(awardPool));
        resultVO.put("leftTime", this.queryLeftTime());

        if (ScenesEnum.ACTIVITY_PAGE == scenesEnum) {

            resultVO.put("activityAmount", activityAmount);
            resultVO.put("endTime", activityInfo.getEndTime());
            resultVO.put("awardPool", awardPool);
            resultVO.put("buttonStatus", activityInfo.getCondPersionCount() > this.getAssistUserInfoMap().size() + 1 ? 0 : 1);
            resultVO.put("startUserVO", startUserInfo);
            resultVO.put("assistVOList", assistUserInfoMap.values());
            resultVO.put("assistDialogList", assistDialogList);
            resultVO.put("pubUserVO", pubUserInfo);
            if (this.helpAmount != null) {
                Map<String, Object> helpFriendAmount = new HashMap<>();
                helpFriendAmount.put("amount", this.helpAmount);
                resultVO.put("helpFriendDialog", helpFriendAmount);
            }
        }
        return resultVO;
    }

    /**
     * 通过此活动为原型 创建 新的拆红包活动
     *
     * @param user 访问者
     * @return
     */
    @Override
    public AppletAssistActivity newOpenRedPackageActivity(User user) {
        Date now = new Date();

        AppletAssistActivity openRedPackageActivity = new AppletAssistActivity();
        openRedPackageActivity.setScenesEnum(ScenesEnum.ACTIVITY_PAGE);
        openRedPackageActivity.setVisitUserId(user.getId());

        Activity toActivity = new Activity();
        if (!this.getActivityInfo().getUserId().equals(user.getId())) {
            // 活动参与者 创建活动
            // 处理帮拆弹框
            openRedPackageActivity.setHelpAmount(this.openAmount);
        }

        // 从 appletActivity.copyPrototype()
        BeanUtils.copyProperties(this.activityInfo, toActivity);
        toActivity.setId(null);
        toActivity.setUserId(user.getId());
        toActivity.setAwardStartTime(null);
        toActivity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
        toActivity.setStartTime(now);
        toActivity.setEndTime(new Date(now.getTime() + (this.activityInfo.getEndTime().getTime() - this.activityInfo.getStartTime().getTime())));
        toActivity.setOrderId(null);
        toActivity.setVersion(6);
        toActivity.setUtime(now);
        toActivity.setCtime(now);
        toActivity.setDel(DelEnum.NO.getCode());
        toActivity.setType(ActivityTypeEnum.OPEN_RED_PACKET_START.getCode());

        openRedPackageActivity.setActivityDefId(toActivity.getActivityDefId());
        openRedPackageActivity.setActivityInfo(toActivity);
        openRedPackageActivity.setJoinRule(new JoinRule(toActivity));
        openRedPackageActivity.setCreateRule(new CreateRule(toActivity));

        UserBoard userBoard = new UserBoard();
        userBoard.setActivityDefId(toActivity.getActivityDefId());
        openRedPackageActivity.setUserBoardConfig(new UserBoardConfig());

        openRedPackageActivity.setTrigger(1L);
        openRedPackageActivity.calculationAssistAmountList();
        Map<String, Object> startUserInfo = openRedPackageActivity.becomeMemberByUser(user);
        if (!this.getActivityInfo().getUserId().equals(user.getId())) {
            // 建立邀请关系
            startUserInfo.put("pubUserId", this.activityInfo.getUserId());
        }
        startUserInfo.put("awardPool", openRedPackageActivity.getOpenAmount());

        openRedPackageActivity.setStartUserInfo(startUserInfo);

        return openRedPackageActivity;
    }

    /**
     * 设置活动过期
     */
    public void setExpired() {
        this.activityInfo.setStatus(ActivityStatusEnum.FAILURE.getCode());
        this.activityInfo.setUtime(new Date());
        this.expiredUserJoinEvent();
    }

    /**
     * 参与事件 过期处理
     */
    public void expiredUserJoinEvent() {
        Date now = new Date();
        if (CollectionUtils.isEmpty(this.userJoinEventMap)) {
            return;
        }
        for (UserJoinEvent userJoinEvent : this.userJoinEventMap.values()) {
            UserActivity userActivityInfo = userJoinEvent.getUserActivityInfo();
            userActivityInfo.setUtime(now);
            userActivityInfo.setStatus(UserActivityStatusEnum.AWARD_EXPIRE.getCode());
        }
    }

    /**
     * 完成支付
     */
    public void completePayTask() {
        this.activityInfo.setStatus(ActivityStatusEnum.END.getCode());
    }

    /**
     * 获取 应扣掉的保证金 -- 此方法最多扣掉 中奖人数对应的保证金
     *
     * @return
     */
    public BigDecimal querySubtractAmount() {
        if (!this.createRule.isSubtractAssist()) {
            return BigDecimal.ZERO;
        }
        if (CollectionUtils.isEmpty(assistUserInfoMap)) {
            return BigDecimal.ZERO;
        }
        Integer stock = this.querySubtractStock();

        BigDecimal totalAmount = BigDecimal.ZERO;
        for (int i = 0; i < stock; i++) {
            BigDecimal assistAmount = assistAmountList.get(i);
            totalAmount = totalAmount.add(assistAmount);
        }
        return totalAmount;
    }

    /**
     * 获取 应扣掉的库存 -- 此方法最多扣掉 中奖人数
     *
     * @return
     */
    public Integer querySubtractStock() {
        if (!this.createRule.isSubtractAssist()) {
            return 0;
        }
        int hitsPerDraw = this.activityInfo.getHitsPerDraw();
        int assistCount = assistUserInfoMap.values().size();
        return hitsPerDraw > assistCount ? assistCount : hitsPerDraw;
    }

    /**
     * 是否 发放助力奖金
     *
     * @return
     */
    public boolean canProvideAssistAmount() {
        if (!this.createRule.isSubtractAssist()) {
            return false;
        }
        int assistCount = this.assistUserInfoMap.values().size();
        int hitsPerDraw = this.activityInfo.getHitsPerDraw();
        return assistCount <= hitsPerDraw;
    }

    /**
     * 获取 助力的发奖记录
     *
     * @return
     */
    public Map<Long, UserCoupon> queryAssistAwardPrize() {
        if (!this.canProvideAssistAmount()) {
            return null;
        }
        // 发起者 颁发奖品
        UserCoupon userCoupon = awardPrize(startUserInfo);
        userCoupon.setAmount(this.openAmount);
        userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
        if (CollectionUtils.isEmpty(this.winnerCoupon)) {
            this.winnerCoupon = new HashMap<>();
        }
        this.winnerCoupon.put(userCoupon.getUserId(), userCoupon);

        return this.winnerCoupon;
    }

    /**
     * 开奖后 给到用户余额钱包的钱
     *
     * @return
     */
    public BigDecimal queryPayAmount() {
        BigDecimal subtractAmount = this.querySubtractAmount();
        BigDecimal deposit = this.queryReduceDeposit();
        return deposit.subtract(subtractAmount);
    }
}
