package mf.code.activity.domain.applet.valueobject;

import lombok.Data;
import mf.code.activity.repo.po.Activity;

import java.util.List;
import java.util.Map;

/**
 * 活动定义本身的进行状态，活动定义的进行中活动完成记录
 *
 * mf.code.activity.domain.applet.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-23 20:50
 */
@Data
public class ActivityDefBoard {
	/**
	 * 所属活动定义id
	 */
	private Long activityDefId;
	/**
	 * 活动定义下的 进行中的活动集合 Map<activityId, activity>
	 */
	private Map<Long, List<Activity>> publishedActivityMap;
	/**
	 * 活动定义下的 完成的用户任务记录 Map<userId, userTask>
	 */
	private Map<Long, List<UserCompleteRecord>> completeRecordMap;
}
