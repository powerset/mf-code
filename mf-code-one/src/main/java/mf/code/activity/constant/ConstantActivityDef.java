package mf.code.activity.constant;/**
 * create by gbf on 2018/11/8 0008
 */

/**
 * @author gbf
 * @description :
 */
public class ConstantActivityDef {
    public static class Type {
        //计划发起
        public static final Integer PLAN = 1;
        //回填订单
        public static final Integer ORDER = 2;
        //新人有礼
        public static final Integer NEWBIE = 3;
    }

    public static class Status {
        //待审核
        public static final Integer WAITING_AUDIT = 0;
        //审核通过
        public static final Integer AUDIT_PASS = 1;
        //审核不通过
        public static final Integer AUDIT_FAILD = -1;
        //已发布
        public static final Integer PUBLISHED = 2;
        //已结束
        public static final Integer FINISHED = 3;
        //已下线
        public static final Integer OFF_LINE = -2;
    }
}
