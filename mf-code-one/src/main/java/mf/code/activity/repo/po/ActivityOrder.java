package mf.code.activity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity_order
 * 活动订单表，用户回填订单表
 */
public class ActivityOrder implements Serializable {
    /**
     * 回填订单主键id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 商户店铺id
     */
    private Long shopId;

    /**
     * 活动定义id
     */
    private Long activityDefId;

    /**
     * 活动id，占用
     */
    private Long activityId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 商户后台定义商品id
     */
    private Long goodsId;

    /**
     * 商户后台定义商品名
     */
    private String displayGoodsName;

    /**
     * 商户后台定义商品展示图片
     */
    private String displayGoodsBanner;

    /**
     * 来源类型 1淘宝 2京东
     */
    private Integer srcType;

    /**
     * 商品数字id
     */
    private String numIid;

    /**
     * 喜销宝返回商品主图
     */
    private String picUrl;

    /**
     * 喜销宝返回商品标题
     */
    private String title;

    /**
     * 喜销宝返回订单数据
     */
    private String xxbtopRes;

    /**
     * 订单交易状态
     */
    private String tradeStatus;

    /**
     * 审核状态  0待审核 1审核通过
     */
    private Integer status;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 商户定义商品价格
     */
    private BigDecimal displayGoodsPrice;

    /**
     * 订单实付价格
     */
    private BigDecimal payment;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * activity_order
     */
    private static final long serialVersionUID = 1L;

    /**
     * 回填订单主键id
     * @return id 回填订单主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 回填订单主键id
     * @param id 回填订单主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 商户店铺id
     * @return shop_id 商户店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 商户店铺id
     * @param shopId 商户店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 活动定义id
     * @return activity_def_id 活动定义id
     */
    public Long getActivityDefId() {
        return activityDefId;
    }

    /**
     * 活动定义id
     * @param activityDefId 活动定义id
     */
    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * 活动id，占用
     * @return activity_id 活动id，占用
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * 活动id，占用
     * @param activityId 活动id，占用
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 订单id
     * @return order_id 订单id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 订单id
     * @param orderId 订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * 商户后台定义商品id
     * @return goods_id 商户后台定义商品id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商户后台定义商品id
     * @param goodsId 商户后台定义商品id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商户后台定义商品名
     * @return display_goods_name 商户后台定义商品名
     */
    public String getDisplayGoodsName() {
        return displayGoodsName;
    }

    /**
     * 商户后台定义商品名
     * @param displayGoodsName 商户后台定义商品名
     */
    public void setDisplayGoodsName(String displayGoodsName) {
        this.displayGoodsName = displayGoodsName == null ? null : displayGoodsName.trim();
    }

    /**
     * 商户后台定义商品展示图片
     * @return display_goods_banner 商户后台定义商品展示图片
     */
    public String getDisplayGoodsBanner() {
        return displayGoodsBanner;
    }

    /**
     * 商户后台定义商品展示图片
     * @param displayGoodsBanner 商户后台定义商品展示图片
     */
    public void setDisplayGoodsBanner(String displayGoodsBanner) {
        this.displayGoodsBanner = displayGoodsBanner == null ? null : displayGoodsBanner.trim();
    }

    /**
     * 来源类型 1淘宝 2京东
     * @return src_type 来源类型 1淘宝 2京东
     */
    public Integer getSrcType() {
        return srcType;
    }

    /**
     * 来源类型 1淘宝 2京东
     * @param srcType 来源类型 1淘宝 2京东
     */
    public void setSrcType(Integer srcType) {
        this.srcType = srcType;
    }

    /**
     * 商品数字id
     * @return num_iid 商品数字id
     */
    public String getNumIid() {
        return numIid;
    }

    /**
     * 商品数字id
     * @param numIid 商品数字id
     */
    public void setNumIid(String numIid) {
        this.numIid = numIid == null ? null : numIid.trim();
    }

    /**
     * 喜销宝返回商品主图
     * @return pic_url 喜销宝返回商品主图
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 喜销宝返回商品主图
     * @param picUrl 喜销宝返回商品主图
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl == null ? null : picUrl.trim();
    }

    /**
     * 喜销宝返回商品标题
     * @return title 喜销宝返回商品标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 喜销宝返回商品标题
     * @param title 喜销宝返回商品标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 喜销宝返回订单数据
     * @return xxbtop_res 喜销宝返回订单数据
     */
    public String getXxbtopRes() {
        return xxbtopRes;
    }

    /**
     * 喜销宝返回订单数据
     * @param xxbtopRes 喜销宝返回订单数据
     */
    public void setXxbtopRes(String xxbtopRes) {
        this.xxbtopRes = xxbtopRes == null ? null : xxbtopRes.trim();
    }

    /**
     * 订单交易状态
     * @return trade_status 订单交易状态
     */
    public String getTradeStatus() {
        return tradeStatus;
    }

    /**
     * 订单交易状态
     * @param tradeStatus 订单交易状态
     */
    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus == null ? null : tradeStatus.trim();
    }

    /**
     * 审核状态  0待审核 1审核通过
     * @return status 审核状态  0待审核 1审核通过
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 审核状态  0待审核 1审核通过
     * @param status 审核状态  0待审核 1审核通过
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 开始时间
     * @return start_time 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 开始时间
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 支付时间
     * @return pay_time 支付时间
     */
    public Date getPayTime() {
        return payTime;
    }

    /**
     * 支付时间
     * @param payTime 支付时间
     */
    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    /**
     * 商户定义商品价格
     * @return display_goods_price 商户定义商品价格
     */
    public BigDecimal getDisplayGoodsPrice() {
        return displayGoodsPrice;
    }

    /**
     * 商户定义商品价格
     * @param displayGoodsPrice 商户定义商品价格
     */
    public void setDisplayGoodsPrice(BigDecimal displayGoodsPrice) {
        this.displayGoodsPrice = displayGoodsPrice;
    }

    /**
     * 订单实付价格
     * @return payment 订单实付价格
     */
    public BigDecimal getPayment() {
        return payment;
    }

    /**
     * 订单实付价格
     * @param payment 订单实付价格
     */
    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", activityDefId=").append(activityDefId);
        sb.append(", activityId=").append(activityId);
        sb.append(", userId=").append(userId);
        sb.append(", orderId=").append(orderId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", displayGoodsName=").append(displayGoodsName);
        sb.append(", displayGoodsBanner=").append(displayGoodsBanner);
        sb.append(", srcType=").append(srcType);
        sb.append(", numIid=").append(numIid);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", title=").append(title);
        sb.append(", xxbtopRes=").append(xxbtopRes);
        sb.append(", tradeStatus=").append(tradeStatus);
        sb.append(", status=").append(status);
        sb.append(", startTime=").append(startTime);
        sb.append(", payTime=").append(payTime);
        sb.append(", displayGoodsPrice=").append(displayGoodsPrice);
        sb.append(", payment=").append(payment);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}