package mf.code.activity.repo.bo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.activity.repo.bo
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-07 下午12:14
 */
@Data
public class AmountBO {
	// 回填订单金额fillBackOrderAmount，收藏加购金额favCartAmount，好评晒图金额goodCommentAmount
	private BigDecimal fillBackOrderAmount;
	private BigDecimal favCartAmount;
	private BigDecimal goodCommentAmount;
}
