package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.ActivityPlanItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ActivityPlanItemMapper extends BaseMapper<ActivityPlanItem> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ActivityPlanItem record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ActivityPlanItem record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ActivityPlanItem selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ActivityPlanItem record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ActivityPlanItem record);

    void deleteByActivityPlanId(Long id);

    int addActivityPlanItemBatch(List<ActivityPlanItem> param);

    List<ActivityPlanItem> findAllItemByPlanId(Map param);

    List<ActivityPlanItem> findAllItemByPlanIdPlus(Map param);

    int publish(Map map);

    Integer updateByPlanIdAndDayNum(ActivityPlanItem item);

    ActivityPlanItem findItemByPlanIdAndDayNumLimit(Long activityPlanId);
}