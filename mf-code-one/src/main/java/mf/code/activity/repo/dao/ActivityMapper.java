package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.Activity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ActivityMapper extends BaseMapper<Activity> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(Activity record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(Activity record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    Activity selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Activity record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Activity record);

    List<Activity> listPageByParams(Map<String, Object> params);

    List<Map> activityListByPlanIdPlus(Map<String, Object> params);

    int countActivity(Map<String, Object> params);

    List<Activity> listActivityByStatus(int status);

    List<Activity> listActivityByStatusAndAwardStartTimeINN(int status);

    int publish(Map m);

    List<Activity> selectForCommonPlus(Map param);

    Integer updateDeposit(@Param(value = "id") Long id,
                          @Param(value = "amount") BigDecimal amount,
                          @Param(value = "uTime") Date utime);

    /**
     * find activity by plan id
     * @param planId
     * @return
     */
    List<Activity> findByPlanId(Long planId);

    Activity findItemByPlanIdAndDayNumLimit(Long activityPlanId);

    int updateByActivityDefIdStatus(@Param(value = "activityDefId") Long activityDefId, @Param("status") int status, @Param(value = "uTime") Date utime);

    int deleteActivityByActivityDefId(Long activityDefId);

    List<Activity> getActivityArrBySku(Map param);

    List<Map> findByGoodsId(Map map);

    Long countGoodsStock(Long activityDefId);

    List<Map> findByDefId(Long activityDefId);

    List<Activity> findByDefIdInActivity(long defId);

    List<Activity> selectByMerchantIdShopId(Map m);

    List<Activity> selectByMerchantIdShopId4Common(Map m);

    List<Activity> findByDefId4HomePage(Long activityDefId);

    Activity findByDefIdUserId(Map m);

    Activity findByDefId4Summary(Long id);

    BigDecimal calcOpenRedPacketMoney(Map<String, Object> param);

    BigDecimal sumByParams(Map<String, Object> param);
}
