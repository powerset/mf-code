package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.ActivityDefAudit;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ActivityDefAuditMapper extends BaseMapper<ActivityDefAudit> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ActivityDefAudit record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ActivityDefAudit record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ActivityDefAudit selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ActivityDefAudit record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ActivityDefAudit record);

    /**
     * 查询满足 条件参数 的 总记录数
     * @param param 查询条件
     * @return 总记录数
     */
    Integer countByParam(Map<String, Object> param);

    /**
     * 查询满足 条件参数 的 记录 ---可分页查询
     * @param param 查询条件
     * @return list数据
     */
    List<ActivityDefAudit> listPageByParam(Map<String, Object> param);
}