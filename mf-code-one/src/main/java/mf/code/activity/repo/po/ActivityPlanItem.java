package mf.code.activity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity_plan_item
 * 活动计划实例
 */
public class ActivityPlanItem implements Serializable {
    /**
     * 活动计划实例id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 活动计划id
     */
    private Long activityPlanId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 日期 格式yyyyMMdd
     */
    private String datetimeStr;

    /**
     * 第几天
     */
    private Integer dateNum;

    /**
     * 商品id 
     */
    private Long goodsId;

    /**
     * 冗余--商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 关键字及其刷单量 [{key:a,total:3},{key:b,total:2}]
     */
    private String totalPerKeyJson;

    /**
     * 
     */
    private Date ctime;

    /**
     * 
     */
    private Date utime;

    /**
     * 删除标记 0:正常 1:删除
     */
    private Integer del;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * activity_plan_item
     */
    private static final long serialVersionUID = 1L;

    /**
     * 活动计划实例id
     * @return id 活动计划实例id
     */
    public Long getId() {
        return id;
    }

    /**
     * 活动计划实例id
     * @param id 活动计划实例id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 活动计划id
     * @return activity_plan_id 活动计划id
     */
    public Long getActivityPlanId() {
        return activityPlanId;
    }

    /**
     * 活动计划id
     * @param activityPlanId 活动计划id
     */
    public void setActivityPlanId(Long activityPlanId) {
        this.activityPlanId = activityPlanId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 日期 格式yyyyMMdd
     * @return datetime_str 日期 格式yyyyMMdd
     */
    public String getDatetimeStr() {
        return datetimeStr;
    }

    /**
     * 日期 格式yyyyMMdd
     * @param datetimeStr 日期 格式yyyyMMdd
     */
    public void setDatetimeStr(String datetimeStr) {
        this.datetimeStr = datetimeStr == null ? null : datetimeStr.trim();
    }

    /**
     * 第几天
     * @return date_num 第几天
     */
    public Integer getDateNum() {
        return dateNum;
    }

    /**
     * 第几天
     * @param dateNum 第几天
     */
    public void setDateNum(Integer dateNum) {
        this.dateNum = dateNum;
    }

    /**
     * 商品id 
     * @return goods_id 商品id 
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商品id 
     * @param goodsId 商品id 
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 冗余--商品价格
     * @return goods_price 冗余--商品价格
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 冗余--商品价格
     * @param goodsPrice 冗余--商品价格
     */
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 关键字及其刷单量 [{key:a,total:3},{key:b,total:2}]
     * @return total_per_key_json 关键字及其刷单量 [{key:a,total:3},{key:b,total:2}]
     */
    public String getTotalPerKeyJson() {
        return totalPerKeyJson;
    }

    /**
     * 关键字及其刷单量 [{key:a,total:3},{key:b,total:2}]
     * @param totalPerKeyJson 关键字及其刷单量 [{key:a,total:3},{key:b,total:2}]
     */
    public void setTotalPerKeyJson(String totalPerKeyJson) {
        this.totalPerKeyJson = totalPerKeyJson == null ? null : totalPerKeyJson.trim();
    }

    /**
     * 
     * @return ctime 
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 
     * @param ctime 
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 
     * @return utime 
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 
     * @param utime 
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 删除标记 0:正常 1:删除
     * @return del 删除标记 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 0:正常 1:删除
     * @param del 删除标记 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 版本号
     * @return version 版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 版本号
     * @param version 版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", activityPlanId=").append(activityPlanId);
        sb.append(", shopId=").append(shopId);
        sb.append(", datetimeStr=").append(datetimeStr);
        sb.append(", dateNum=").append(dateNum);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodsPrice=").append(goodsPrice);
        sb.append(", totalPerKeyJson=").append(totalPerKeyJson);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", version=").append(version);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}