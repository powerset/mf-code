package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.ActivityTask;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ActivityTaskMapper extends BaseMapper<ActivityTask> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ActivityTask record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ActivityTask record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ActivityTask selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ActivityTask record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ActivityTask record);

    int reduceStock(@Param("activityTaskId") Long activityTaskId, @Param("utime") Date date);

    int addStock(@Param("activityTaskId") Long activityTaskId, @Param("utime") Date date);

    List<ActivityTask> selectByParams(Map<String, Object> activityTaskParams);

    Integer supplyStock(@Param("activityDefId") Long activityDefId, @Param("stockApply") Integer stockApply, @Param("utime") Date utime);

    List<ActivityTask> selectByMerchantIdShopIdFromActivityTask(Map m);

    Long getStockByGoodsId(Long goodsId);
}
