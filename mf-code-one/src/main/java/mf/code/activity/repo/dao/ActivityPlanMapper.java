package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.ActivityPlan;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface ActivityPlanMapper extends BaseMapper<ActivityPlan> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ActivityPlan record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ActivityPlan record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ActivityPlan selectByPrimaryKeyPlus(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ActivityPlan record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ActivityPlan record);

    int updateByPrimaryKeySelectivePlus(ActivityPlan record);

    int updateByPrimaryKeySelectivePlusWithoutMerchantId(ActivityPlan record);

    /**
     * 分页查找,并筛选状态
     * @param param
     * @return
     */
    List<Map> selectAll(HashMap<String, Object> param);

    /**
     * 按条件查询
     * @param params
     * @return
     */
    List<ActivityPlan> selectByParams(HashMap<String, Object> params);

    int updatePlus(Map m);

    Map findByIdPlus(Long id);

    int confirm(Map map);

    int publish(Map map);

    /**
     * 更新红包任务红包库存
     * @param id
     * @param redpackStockValue 库存红包更新数量数量
     * @return
     */
    Integer updateRedpackStock(@Param(value = "id") Long id,
                               @Param(value = "redpackStockValue") Integer redpackStockValue,
                               @Param(value = "utime") Date utime);

    int subtractTotalRedpackStockByPrimaryKey(Long activityPlanId, @Param(value = "utime") Date utime);

    /**
     * 通过ctime获取ids
     * @param condDate
     * @return
     */
    List<ActivityPlan> findByCtimePlus(String condDate);
}
