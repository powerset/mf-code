package mf.code.activity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity_def_audit
 * 活动审核记录表，商户提交活动定义，运营审核活动定义，通过则直接发布活动
 */
public class ActivityDefAudit implements Serializable {
    /**
     * 审核记录id
     */
    private Long id;

    /**
     * 平台运营用户id--审核人员
     */
    private Long platUserId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 商户店铺id
     */
    private Long shopId;

    /**
     * 活动定义id
     */
    private Long activityDefId;

    /**
     * 活动类型 1:计划内活动  2:免单商品抽奖活动 3:新人有礼活动 4:新人大转盘 5:回填订单红包活动 6:助力活动 7:收藏加购 8:红包活动（回填订单，收藏加购，好评晒图) 9:拆红包10：好评晒图
     */
    private Integer type;

    /**
     * 1:增加库存 2：库存退回
     */
    private Integer bizType;

    /**
     * 申请时间
     */
    private Date applyTime;

    /**
     * Json格式，商品id列表[“1”,”2”]
     */
    private String goodsIds;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 初始的库存数量
     */
    private Integer stockDef;

    /**
     * 申请的库存数量
     */
    private Integer stockApply;

    /**
     * 实际增加的库存数量
     */
    private Integer stockActual;

    /**
     * 最终的库存数量
     */
    private Integer stockFinal;

    /**
     * 初始的保证金定义额度
     */
    private BigDecimal depositDef;

    /**
     * 申请的保证金定义额度
     */
    private BigDecimal depositApply;

    /**
     * 实际缴纳的保证金额度
     */
    private BigDecimal depositActual;

    /**
     * 最终的保证金额度
     */
    private BigDecimal depositFinal;

    /**
     * 初始的成长任务红包个数
     */
    private Integer taskRpNumDef;

    /**
     * 申请的成长任务红包个数
     */
    private Integer taskRpNumApply;

    /**
     * 实际增加的成人任务红包个数
     */
    private Integer taskRpNumActual;

    /**
     * 最终的成长任务红包个数
     */
    private Integer taskRpNumFinal;

    /**
     * 初始的成长任务红包保证金
     */
    private BigDecimal taskRpDepositDef;

    /**
     * 申请的成长任务红包保证金
     */
    private BigDecimal taskRpDepositApply;

    /**
     * 实际缴纳的保证金额度
     */
    private BigDecimal taskRpDepositActual;

    /**
     * 最终的成长任务红包保证金
     */
    private BigDecimal taskRpDepositFinal;

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    private Integer depositStatus;

    /**
     * 保证金支付凭证地址
     */
    private String voucher;

    /**
     * 关键词 
     */
    private String keyWord;

    /**
     * 发布时间
     */
    private Date publishTime;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 审核状态  1待审核（待付款） 2审核通过
     */
    private Integer status;

    /**
     * 删除状态，0 正常， 1删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * activity_def_audit
     */
    private static final long serialVersionUID = 1L;

    /**
     * 审核记录id
     * @return id 审核记录id
     */
    public Long getId() {
        return id;
    }

    /**
     * 审核记录id
     * @param id 审核记录id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 平台运营用户id--审核人员
     * @return plat_user_id 平台运营用户id--审核人员
     */
    public Long getPlatUserId() {
        return platUserId;
    }

    /**
     * 平台运营用户id--审核人员
     * @param platUserId 平台运营用户id--审核人员
     */
    public void setPlatUserId(Long platUserId) {
        this.platUserId = platUserId;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 商户店铺id
     * @return shop_id 商户店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 商户店铺id
     * @param shopId 商户店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 活动定义id
     * @return activity_def_id 活动定义id
     */
    public Long getActivityDefId() {
        return activityDefId;
    }

    /**
     * 活动定义id
     * @param activityDefId 活动定义id
     */
    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * 活动类型 1:计划内活动  2:免单商品抽奖活动 3:新人有礼活动 4:新人大转盘 5:回填订单红包活动 6:助力活动 7:收藏加购 8:红包活动（回填订单，收藏加购，好评晒图) 9:拆红包10：好评晒图
     * @return type 活动类型 1:计划内活动  2:免单商品抽奖活动 3:新人有礼活动 4:新人大转盘 5:回填订单红包活动 6:助力活动 7:收藏加购 8:红包活动（回填订单，收藏加购，好评晒图) 9:拆红包10：好评晒图
     */
    public Integer getType() {
        return type;
    }

    /**
     * 活动类型 1:计划内活动  2:免单商品抽奖活动 3:新人有礼活动 4:新人大转盘 5:回填订单红包活动 6:助力活动 7:收藏加购 8:红包活动（回填订单，收藏加购，好评晒图) 9:拆红包10：好评晒图
     * @param type 活动类型 1:计划内活动  2:免单商品抽奖活动 3:新人有礼活动 4:新人大转盘 5:回填订单红包活动 6:助力活动 7:收藏加购 8:红包活动（回填订单，收藏加购，好评晒图) 9:拆红包10：好评晒图
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 1:增加库存 2：库存退回
     * @return biz_type 1:增加库存 2：库存退回
     */
    public Integer getBizType() {
        return bizType;
    }

    /**
     * 1:增加库存 2：库存退回
     * @param bizType 1:增加库存 2：库存退回
     */
    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    /**
     * 申请时间
     * @return apply_time 申请时间
     */
    public Date getApplyTime() {
        return applyTime;
    }

    /**
     * 申请时间
     * @param applyTime 申请时间
     */
    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    /**
     * Json格式，商品id列表[“1”,”2”]
     * @return goods_ids Json格式，商品id列表[“1”,”2”]
     */
    public String getGoodsIds() {
        return goodsIds;
    }

    /**
     * Json格式，商品id列表[“1”,”2”]
     * @param goodsIds Json格式，商品id列表[“1”,”2”]
     */
    public void setGoodsIds(String goodsIds) {
        this.goodsIds = goodsIds == null ? null : goodsIds.trim();
    }

    /**
     * 商品id
     * @return goods_id 商品id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商品id
     * @param goodsId 商品id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 初始的库存数量
     * @return stock_def 初始的库存数量
     */
    public Integer getStockDef() {
        return stockDef;
    }

    /**
     * 初始的库存数量
     * @param stockDef 初始的库存数量
     */
    public void setStockDef(Integer stockDef) {
        this.stockDef = stockDef;
    }

    /**
     * 申请的库存数量
     * @return stock_apply 申请的库存数量
     */
    public Integer getStockApply() {
        return stockApply;
    }

    /**
     * 申请的库存数量
     * @param stockApply 申请的库存数量
     */
    public void setStockApply(Integer stockApply) {
        this.stockApply = stockApply;
    }

    /**
     * 实际增加的库存数量
     * @return stock_actual 实际增加的库存数量
     */
    public Integer getStockActual() {
        return stockActual;
    }

    /**
     * 实际增加的库存数量
     * @param stockActual 实际增加的库存数量
     */
    public void setStockActual(Integer stockActual) {
        this.stockActual = stockActual;
    }

    /**
     * 最终的库存数量
     * @return stock_final 最终的库存数量
     */
    public Integer getStockFinal() {
        return stockFinal;
    }

    /**
     * 最终的库存数量
     * @param stockFinal 最终的库存数量
     */
    public void setStockFinal(Integer stockFinal) {
        this.stockFinal = stockFinal;
    }

    /**
     * 初始的保证金定义额度
     * @return deposit_def 初始的保证金定义额度
     */
    public BigDecimal getDepositDef() {
        return depositDef;
    }

    /**
     * 初始的保证金定义额度
     * @param depositDef 初始的保证金定义额度
     */
    public void setDepositDef(BigDecimal depositDef) {
        this.depositDef = depositDef;
    }

    /**
     * 申请的保证金定义额度
     * @return deposit_apply 申请的保证金定义额度
     */
    public BigDecimal getDepositApply() {
        return depositApply;
    }

    /**
     * 申请的保证金定义额度
     * @param depositApply 申请的保证金定义额度
     */
    public void setDepositApply(BigDecimal depositApply) {
        this.depositApply = depositApply;
    }

    /**
     * 实际缴纳的保证金额度
     * @return deposit_actual 实际缴纳的保证金额度
     */
    public BigDecimal getDepositActual() {
        return depositActual;
    }

    /**
     * 实际缴纳的保证金额度
     * @param depositActual 实际缴纳的保证金额度
     */
    public void setDepositActual(BigDecimal depositActual) {
        this.depositActual = depositActual;
    }

    /**
     * 最终的保证金额度
     * @return deposit_final 最终的保证金额度
     */
    public BigDecimal getDepositFinal() {
        return depositFinal;
    }

    /**
     * 最终的保证金额度
     * @param depositFinal 最终的保证金额度
     */
    public void setDepositFinal(BigDecimal depositFinal) {
        this.depositFinal = depositFinal;
    }

    /**
     * 初始的成长任务红包个数
     * @return task_rp_num_def 初始的成长任务红包个数
     */
    public Integer getTaskRpNumDef() {
        return taskRpNumDef;
    }

    /**
     * 初始的成长任务红包个数
     * @param taskRpNumDef 初始的成长任务红包个数
     */
    public void setTaskRpNumDef(Integer taskRpNumDef) {
        this.taskRpNumDef = taskRpNumDef;
    }

    /**
     * 申请的成长任务红包个数
     * @return task_rp_num_apply 申请的成长任务红包个数
     */
    public Integer getTaskRpNumApply() {
        return taskRpNumApply;
    }

    /**
     * 申请的成长任务红包个数
     * @param taskRpNumApply 申请的成长任务红包个数
     */
    public void setTaskRpNumApply(Integer taskRpNumApply) {
        this.taskRpNumApply = taskRpNumApply;
    }

    /**
     * 实际增加的成人任务红包个数
     * @return task_rp_num_actual 实际增加的成人任务红包个数
     */
    public Integer getTaskRpNumActual() {
        return taskRpNumActual;
    }

    /**
     * 实际增加的成人任务红包个数
     * @param taskRpNumActual 实际增加的成人任务红包个数
     */
    public void setTaskRpNumActual(Integer taskRpNumActual) {
        this.taskRpNumActual = taskRpNumActual;
    }

    /**
     * 最终的成长任务红包个数
     * @return task_rp_num_final 最终的成长任务红包个数
     */
    public Integer getTaskRpNumFinal() {
        return taskRpNumFinal;
    }

    /**
     * 最终的成长任务红包个数
     * @param taskRpNumFinal 最终的成长任务红包个数
     */
    public void setTaskRpNumFinal(Integer taskRpNumFinal) {
        this.taskRpNumFinal = taskRpNumFinal;
    }

    /**
     * 初始的成长任务红包保证金
     * @return task_rp_deposit_def 初始的成长任务红包保证金
     */
    public BigDecimal getTaskRpDepositDef() {
        return taskRpDepositDef;
    }

    /**
     * 初始的成长任务红包保证金
     * @param taskRpDepositDef 初始的成长任务红包保证金
     */
    public void setTaskRpDepositDef(BigDecimal taskRpDepositDef) {
        this.taskRpDepositDef = taskRpDepositDef;
    }

    /**
     * 申请的成长任务红包保证金
     * @return task_rp_deposit_apply 申请的成长任务红包保证金
     */
    public BigDecimal getTaskRpDepositApply() {
        return taskRpDepositApply;
    }

    /**
     * 申请的成长任务红包保证金
     * @param taskRpDepositApply 申请的成长任务红包保证金
     */
    public void setTaskRpDepositApply(BigDecimal taskRpDepositApply) {
        this.taskRpDepositApply = taskRpDepositApply;
    }

    /**
     * 实际缴纳的保证金额度
     * @return task_rp_deposit_actual 实际缴纳的保证金额度
     */
    public BigDecimal getTaskRpDepositActual() {
        return taskRpDepositActual;
    }

    /**
     * 实际缴纳的保证金额度
     * @param taskRpDepositActual 实际缴纳的保证金额度
     */
    public void setTaskRpDepositActual(BigDecimal taskRpDepositActual) {
        this.taskRpDepositActual = taskRpDepositActual;
    }

    /**
     * 最终的成长任务红包保证金
     * @return task_rp_deposit_final 最终的成长任务红包保证金
     */
    public BigDecimal getTaskRpDepositFinal() {
        return taskRpDepositFinal;
    }

    /**
     * 最终的成长任务红包保证金
     * @param taskRpDepositFinal 最终的成长任务红包保证金
     */
    public void setTaskRpDepositFinal(BigDecimal taskRpDepositFinal) {
        this.taskRpDepositFinal = taskRpDepositFinal;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @return deposit_status 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public Integer getDepositStatus() {
        return depositStatus;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @param depositStatus 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public void setDepositStatus(Integer depositStatus) {
        this.depositStatus = depositStatus;
    }

    /**
     * 保证金支付凭证地址
     * @return voucher 保证金支付凭证地址
     */
    public String getVoucher() {
        return voucher;
    }

    /**
     * 保证金支付凭证地址
     * @param voucher 保证金支付凭证地址
     */
    public void setVoucher(String voucher) {
        this.voucher = voucher == null ? null : voucher.trim();
    }

    /**
     * 关键词 
     * @return key_word 关键词 
     */
    public String getKeyWord() {
        return keyWord;
    }

    /**
     * 关键词 
     * @param keyWord 关键词 
     */
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord == null ? null : keyWord.trim();
    }

    /**
     * 发布时间
     * @return publish_time 发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 发布时间
     * @param publishTime 发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * 审核时间
     * @return audit_time 审核时间
     */
    public Date getAuditTime() {
        return auditTime;
    }

    /**
     * 审核时间
     * @param auditTime 审核时间
     */
    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    /**
     * 审核状态  1待审核（待付款） 2审核通过
     * @return status 审核状态  1待审核（待付款） 2审核通过
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 审核状态  1待审核（待付款） 2审核通过
     * @param status 审核状态  1待审核（待付款） 2审核通过
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 删除状态，0 正常， 1删除
     * @return del 删除状态，0 正常， 1删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除状态，0 正常， 1删除
     * @param del 删除状态，0 正常， 1删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", platUserId=").append(platUserId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", activityDefId=").append(activityDefId);
        sb.append(", type=").append(type);
        sb.append(", bizType=").append(bizType);
        sb.append(", applyTime=").append(applyTime);
        sb.append(", goodsIds=").append(goodsIds);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", stockDef=").append(stockDef);
        sb.append(", stockApply=").append(stockApply);
        sb.append(", stockActual=").append(stockActual);
        sb.append(", stockFinal=").append(stockFinal);
        sb.append(", depositDef=").append(depositDef);
        sb.append(", depositApply=").append(depositApply);
        sb.append(", depositActual=").append(depositActual);
        sb.append(", depositFinal=").append(depositFinal);
        sb.append(", taskRpNumDef=").append(taskRpNumDef);
        sb.append(", taskRpNumApply=").append(taskRpNumApply);
        sb.append(", taskRpNumActual=").append(taskRpNumActual);
        sb.append(", taskRpNumFinal=").append(taskRpNumFinal);
        sb.append(", taskRpDepositDef=").append(taskRpDepositDef);
        sb.append(", taskRpDepositApply=").append(taskRpDepositApply);
        sb.append(", taskRpDepositActual=").append(taskRpDepositActual);
        sb.append(", taskRpDepositFinal=").append(taskRpDepositFinal);
        sb.append(", depositStatus=").append(depositStatus);
        sb.append(", voucher=").append(voucher);
        sb.append(", keyWord=").append(keyWord);
        sb.append(", publishTime=").append(publishTime);
        sb.append(", auditTime=").append(auditTime);
        sb.append(", status=").append(status);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}