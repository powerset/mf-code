package mf.code.activity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.activity.repo.po.ActivityOrder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ActivityOrderMapper extends BaseMapper<ActivityOrder> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(ActivityOrder record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(ActivityOrder record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    ActivityOrder selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ActivityOrder record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ActivityOrder record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param params
     */
    List<ActivityOrder> selectByParams(Map<String, Object> params);

    /**
     *  根据条件来查询符合条件的数据库数目
     *
     * @param params
     */
    int countActivityOrder(Map<String, Object> params);
}