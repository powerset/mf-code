package mf.code.activity.repo.po;

import java.io.Serializable;

/**
 * activity_plan
 * 活动计划
 */
public class ActivityPlanKey implements Serializable {
    /**
     * 活动计划id
     */
    private Long id;

    /**
     *  每个计划内活动中奖人数
     */
    private Integer hitsPerDraw;

    /**
     * activity_plan
     */
    private static final long serialVersionUID = 1L;

    /**
     * 活动计划id
     * @return id 活动计划id
     */
    public Long getId() {
        return id;
    }

    /**
     * 活动计划id
     * @param id 活动计划id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *  每个计划内活动中奖人数
     * @return hits_per_draw  每个计划内活动中奖人数
     */
    public Integer getHitsPerDraw() {
        return hitsPerDraw;
    }

    /**
     *  每个计划内活动中奖人数
     * @param hitsPerDraw  每个计划内活动中奖人数
     */
    public void setHitsPerDraw(Integer hitsPerDraw) {
        this.hitsPerDraw = hitsPerDraw;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", hitsPerDraw=").append(hitsPerDraw);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}