package mf.code.activity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity_task
 * 活动任务定义表  用于商户定义红包任务
 */
public class ActivityTask implements Serializable {
    /**
     * 任务定义id
     */
    private Long id;

    /**
     * 商户id,merchant表主键
     */
    private Long merchantId;

    /**
     * 店铺id, merchant_shop表的主键
     */
    private Long shopId;

    /**
     * 商品ID, goods表的主键，便于反查
     */
    private Long goodsId;

    /**
     * 活动定义id, 活动定义表主键，保存在支付订单，便于反查
     */
    private Long activityDefId;

    /**
     * 任务类型（1：订单核对，2：好评返现，3：收藏加购，4：领取优惠券，5：加载公众号）
     */
    private Integer type;

    /**
     * 任务名称
     */
    private String taskTitle;

    /**
     * 任务简介
     */
    private String taskDesc;

    /**
     * 任务开始时间
     */
    private Date startTime;

    /**
     * 任务结束时间
     */
    private Date endTime;

    /**
     * 任务实行时间
     */
    private Integer missionNeedTime;

    /**
     * 单人红包库存定义
     */
    private Integer rpStockDef;

    /**
     * 单人红包库存
     */
    private Integer rpStock;

    /**
     * 红包金额 单位元
     */
    private BigDecimal rpAmount;

    /**
     * 淘口令 商品淘口令或优惠券淘口令
     */
    private String tpwd;

    /**
     * 发布时间
     */
    private Date publishTime;

    /**
     * 微信客服 昵称
     */
    private String wechat;

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    private Integer depositStatus;

    /**
     * 任务状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束
     */
    private Integer status;

    /**
     * 删除标记 . 0:正常 1:删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * activity_task
     */
    private static final long serialVersionUID = 1L;

    /**
     * 任务定义id
     * @return id 任务定义id
     */
    public Long getId() {
        return id;
    }

    /**
     * 任务定义id
     * @param id 任务定义id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id,merchant表主键
     * @return merchant_id 商户id,merchant表主键
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id,merchant表主键
     * @param merchantId 商户id,merchant表主键
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id, merchant_shop表的主键
     * @return shop_id 店铺id, merchant_shop表的主键
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id, merchant_shop表的主键
     * @param shopId 店铺id, merchant_shop表的主键
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 商品ID, goods表的主键，便于反查
     * @return goods_id 商品ID, goods表的主键，便于反查
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商品ID, goods表的主键，便于反查
     * @param goodsId 商品ID, goods表的主键，便于反查
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 活动定义id, 活动定义表主键，保存在支付订单，便于反查
     * @return activity_def_id 活动定义id, 活动定义表主键，保存在支付订单，便于反查
     */
    public Long getActivityDefId() {
        return activityDefId;
    }

    /**
     * 活动定义id, 活动定义表主键，保存在支付订单，便于反查
     * @param activityDefId 活动定义id, 活动定义表主键，保存在支付订单，便于反查
     */
    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * 任务类型（1：订单核对，2：好评返现，3：收藏加购，4：领取优惠券，5：加载公众号）
     * @return type 任务类型（1：订单核对，2：好评返现，3：收藏加购，4：领取优惠券，5：加载公众号）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 任务类型（1：订单核对，2：好评返现，3：收藏加购，4：领取优惠券，5：加载公众号）
     * @param type 任务类型（1：订单核对，2：好评返现，3：收藏加购，4：领取优惠券，5：加载公众号）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 任务名称
     * @return task_title 任务名称
     */
    public String getTaskTitle() {
        return taskTitle;
    }

    /**
     * 任务名称
     * @param taskTitle 任务名称
     */
    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle == null ? null : taskTitle.trim();
    }

    /**
     * 任务简介
     * @return task_desc 任务简介
     */
    public String getTaskDesc() {
        return taskDesc;
    }

    /**
     * 任务简介
     * @param taskDesc 任务简介
     */
    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc == null ? null : taskDesc.trim();
    }

    /**
     * 任务开始时间
     * @return start_time 任务开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 任务开始时间
     * @param startTime 任务开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 任务结束时间
     * @return end_time 任务结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 任务结束时间
     * @param endTime 任务结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 任务实行时间
     * @return mission_need_time 任务实行时间
     */
    public Integer getMissionNeedTime() {
        return missionNeedTime;
    }

    /**
     * 任务实行时间
     * @param missionNeedTime 任务实行时间
     */
    public void setMissionNeedTime(Integer missionNeedTime) {
        this.missionNeedTime = missionNeedTime;
    }

    /**
     * 单人红包库存定义
     * @return rp_stock_def 单人红包库存定义
     */
    public Integer getRpStockDef() {
        return rpStockDef;
    }

    /**
     * 单人红包库存定义
     * @param rpStockDef 单人红包库存定义
     */
    public void setRpStockDef(Integer rpStockDef) {
        this.rpStockDef = rpStockDef;
    }

    /**
     * 单人红包库存
     * @return rp_stock 单人红包库存
     */
    public Integer getRpStock() {
        return rpStock;
    }

    /**
     * 单人红包库存
     * @param rpStock 单人红包库存
     */
    public void setRpStock(Integer rpStock) {
        this.rpStock = rpStock;
    }

    /**
     * 红包金额 单位元
     * @return rp_amount 红包金额 单位元
     */
    public BigDecimal getRpAmount() {
        return rpAmount;
    }

    /**
     * 红包金额 单位元
     * @param rpAmount 红包金额 单位元
     */
    public void setRpAmount(BigDecimal rpAmount) {
        this.rpAmount = rpAmount;
    }

    /**
     * 淘口令 商品淘口令或优惠券淘口令
     * @return tpwd 淘口令 商品淘口令或优惠券淘口令
     */
    public String getTpwd() {
        return tpwd;
    }

    /**
     * 淘口令 商品淘口令或优惠券淘口令
     * @param tpwd 淘口令 商品淘口令或优惠券淘口令
     */
    public void setTpwd(String tpwd) {
        this.tpwd = tpwd == null ? null : tpwd.trim();
    }

    /**
     * 发布时间
     * @return publish_time 发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 发布时间
     * @param publishTime 发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * 微信客服 昵称
     * @return wechat 微信客服 昵称
     */
    public String getWechat() {
        return wechat;
    }

    /**
     * 微信客服 昵称
     * @param wechat 微信客服 昵称
     */
    public void setWechat(String wechat) {
        this.wechat = wechat == null ? null : wechat.trim();
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @return deposit_status 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public Integer getDepositStatus() {
        return depositStatus;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @param depositStatus 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public void setDepositStatus(Integer depositStatus) {
        this.depositStatus = depositStatus;
    }

    /**
     * 任务状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束
     * @return status 任务状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 任务状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束
     * @param status 任务状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @return del 删除标记 . 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @param del 删除标记 . 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", activityDefId=").append(activityDefId);
        sb.append(", type=").append(type);
        sb.append(", taskTitle=").append(taskTitle);
        sb.append(", taskDesc=").append(taskDesc);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", missionNeedTime=").append(missionNeedTime);
        sb.append(", rpStockDef=").append(rpStockDef);
        sb.append(", rpStock=").append(rpStock);
        sb.append(", rpAmount=").append(rpAmount);
        sb.append(", tpwd=").append(tpwd);
        sb.append(", publishTime=").append(publishTime);
        sb.append(", wechat=").append(wechat);
        sb.append(", depositStatus=").append(depositStatus);
        sb.append(", status=").append(status);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}