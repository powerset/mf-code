package mf.code.activity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity_plan
 * 活动计划
 */
public class ActivityPlan implements Serializable {
    /**
     * 活动计划id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 商户店铺id
     */
    private Long shopId;

    /**
     * 商品id列表 [110,222]
     */
    private String goodsIdsJson;

    /**
     * 微信号
     */
    private String serviceWx;

    /**
     * 客服二维码
     */
    private String serviceQr;

    /**
     * 活动计划描述
     */
    private String detail;

    /**
     * 1: 商家填已完: 待设计  
2: 运营设计完: 待提交
3: 商家付款后: 已提交
4: 商家确认后: 已确认 
5: 商家发布后: 已发布
-1: 结束
-2: 非正常状态
     */
    private Integer status;

    /**
     * 计划开始时间
     */
    private Date startTime;

    /**
     * 计划结束时间
     */
    private Date endTime;

    /**
     * 计划总体关键字
     */
    private String keys;

    /**
     * 计划总体刷单量
     */
    private Integer total;

    /**
     * 刷单剩余库存
     */
    private Integer totalResidueStock;

    /**
     * 整个计划时间天数
     */
    private Integer totalDay;

    /**
     * 保证金
     */
    private BigDecimal deposit;

    /**
     * 商品价格json定义 [2.30,6.88]
     */
    private String amountJson;

    /**
     * 开奖条件-满足人数开奖 单位 人数
     */
    private Integer condPersionCount;

    /**
     * 开奖条件-上线后满足时间开奖 单位 分钟
     */
    private Integer condDrawTime;

    /**
     *  每个计划内活动中奖人数
     */
    private Integer hitsPerDraw;

    /**
     * 中奖任务-需要完成时间 单位 分钟
     */
    private Integer missionNeedTime;

    /**
     * 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 {优惠券id,过期时间}
     */
    private String merchantCouponJson;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 删除标记 0:正常 1:删除
     */
    private Integer del;

    /**
     * 版本
     */
    private Integer verison;

    /**
     * 定义红包个数
     */
    private Integer totalRedpack;

    /**
     * 红包库存
     */
    private Integer totalRedpackStock;

    /**
     * 单个红包金额
     */
    private BigDecimal redpackUnitPrice;

    /**
     * 凭证url
     */
    private String voucherUrl;

    /**
     * activity_plan
     */
    private static final long serialVersionUID = 1L;

    /**
     * 活动计划id
     * @return id 活动计划id
     */
    public Long getId() {
        return id;
    }

    /**
     * 活动计划id
     * @param id 活动计划id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 商户店铺id
     * @return shop_id 商户店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 商户店铺id
     * @param shopId 商户店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 商品id列表 [110,222]
     * @return goods_ids_json 商品id列表 [110,222]
     */
    public String getGoodsIdsJson() {
        return goodsIdsJson;
    }

    /**
     * 商品id列表 [110,222]
     * @param goodsIdsJson 商品id列表 [110,222]
     */
    public void setGoodsIdsJson(String goodsIdsJson) {
        this.goodsIdsJson = goodsIdsJson == null ? null : goodsIdsJson.trim();
    }

    /**
     * 微信号
     * @return service_wx 微信号
     */
    public String getServiceWx() {
        return serviceWx;
    }

    /**
     * 微信号
     * @param serviceWx 微信号
     */
    public void setServiceWx(String serviceWx) {
        this.serviceWx = serviceWx == null ? null : serviceWx.trim();
    }

    /**
     * 客服二维码
     * @return service_qr 客服二维码
     */
    public String getServiceQr() {
        return serviceQr;
    }

    /**
     * 客服二维码
     * @param serviceQr 客服二维码
     */
    public void setServiceQr(String serviceQr) {
        this.serviceQr = serviceQr == null ? null : serviceQr.trim();
    }

    /**
     * 活动计划描述
     * @return detail 活动计划描述
     */
    public String getDetail() {
        return detail;
    }

    /**
     * 活动计划描述
     * @param detail 活动计划描述
     */
    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    /**
     * 1: 商家填已完: 待设计   2: 运营设计完: 待提交 3: 商家付款后: 已提交 4: 商家确认后: 已确认  5: 商家发布后: 已发布 -1: 结束 -2: 非正常状态
     * @return status 1: 商家填已完: 待设计   2: 运营设计完: 待提交 3: 商家付款后: 已提交 4: 商家确认后: 已确认  5: 商家发布后: 已发布 -1: 结束 -2: 非正常状态
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 1: 商家填已完: 待设计   2: 运营设计完: 待提交 3: 商家付款后: 已提交 4: 商家确认后: 已确认  5: 商家发布后: 已发布 -1: 结束 -2: 非正常状态
     * @param status 1: 商家填已完: 待设计   2: 运营设计完: 待提交 3: 商家付款后: 已提交 4: 商家确认后: 已确认  5: 商家发布后: 已发布 -1: 结束 -2: 非正常状态
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 计划开始时间
     * @return start_time 计划开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 计划开始时间
     * @param startTime 计划开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 计划结束时间
     * @return end_time 计划结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 计划结束时间
     * @param endTime 计划结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 计划总体关键字
     * @return keys 计划总体关键字
     */
    public String getKeys() {
        return keys;
    }

    /**
     * 计划总体关键字
     * @param keys 计划总体关键字
     */
    public void setKeys(String keys) {
        this.keys = keys == null ? null : keys.trim();
    }

    /**
     * 计划总体刷单量
     * @return total 计划总体刷单量
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 计划总体刷单量
     * @param total 计划总体刷单量
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 刷单剩余库存
     * @return total_residue_stock 刷单剩余库存
     */
    public Integer getTotalResidueStock() {
        return totalResidueStock;
    }

    /**
     * 刷单剩余库存
     * @param totalResidueStock 刷单剩余库存
     */
    public void setTotalResidueStock(Integer totalResidueStock) {
        this.totalResidueStock = totalResidueStock;
    }

    /**
     * 整个计划时间天数
     * @return total_day 整个计划时间天数
     */
    public Integer getTotalDay() {
        return totalDay;
    }

    /**
     * 整个计划时间天数
     * @param totalDay 整个计划时间天数
     */
    public void setTotalDay(Integer totalDay) {
        this.totalDay = totalDay;
    }

    /**
     * 保证金
     * @return deposit 保证金
     */
    public BigDecimal getDeposit() {
        return deposit;
    }

    /**
     * 保证金
     * @param deposit 保证金
     */
    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    /**
     * 商品价格json定义 [2.30,6.88]
     * @return amount_json 商品价格json定义 [2.30,6.88]
     */
    public String getAmountJson() {
        return amountJson;
    }

    /**
     * 商品价格json定义 [2.30,6.88]
     * @param amountJson 商品价格json定义 [2.30,6.88]
     */
    public void setAmountJson(String amountJson) {
        this.amountJson = amountJson == null ? null : amountJson.trim();
    }

    /**
     * 开奖条件-满足人数开奖 单位 人数
     * @return cond_persion_count 开奖条件-满足人数开奖 单位 人数
     */
    public Integer getCondPersionCount() {
        return condPersionCount;
    }

    /**
     * 开奖条件-满足人数开奖 单位 人数
     * @param condPersionCount 开奖条件-满足人数开奖 单位 人数
     */
    public void setCondPersionCount(Integer condPersionCount) {
        this.condPersionCount = condPersionCount;
    }

    /**
     * 开奖条件-上线后满足时间开奖 单位 分钟
     * @return cond_draw_time 开奖条件-上线后满足时间开奖 单位 分钟
     */
    public Integer getCondDrawTime() {
        return condDrawTime;
    }

    /**
     * 开奖条件-上线后满足时间开奖 单位 分钟
     * @param condDrawTime 开奖条件-上线后满足时间开奖 单位 分钟
     */
    public void setCondDrawTime(Integer condDrawTime) {
        this.condDrawTime = condDrawTime;
    }

    /**
     *  每个计划内活动中奖人数
     * @return hits_per_draw  每个计划内活动中奖人数
     */
    public Integer getHitsPerDraw() {
        return hitsPerDraw;
    }

    /**
     *  每个计划内活动中奖人数
     * @param hitsPerDraw  每个计划内活动中奖人数
     */
    public void setHitsPerDraw(Integer hitsPerDraw) {
        this.hitsPerDraw = hitsPerDraw;
    }

    /**
     * 中奖任务-需要完成时间 单位 分钟
     * @return mission_need_time 中奖任务-需要完成时间 单位 分钟
     */
    public Integer getMissionNeedTime() {
        return missionNeedTime;
    }

    /**
     * 中奖任务-需要完成时间 单位 分钟
     * @param missionNeedTime 中奖任务-需要完成时间 单位 分钟
     */
    public void setMissionNeedTime(Integer missionNeedTime) {
        this.missionNeedTime = missionNeedTime;
    }

    /**
     * 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 {优惠券id,过期时间}
     * @return merchant_coupon_json 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 {优惠券id,过期时间}
     */
    public String getMerchantCouponJson() {
        return merchantCouponJson;
    }

    /**
     * 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 {优惠券id,过期时间}
     * @param merchantCouponJson 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 {优惠券id,过期时间}
     */
    public void setMerchantCouponJson(String merchantCouponJson) {
        this.merchantCouponJson = merchantCouponJson == null ? null : merchantCouponJson.trim();
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 删除标记 0:正常 1:删除
     * @return del 删除标记 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 0:正常 1:删除
     * @param del 删除标记 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 版本
     * @return verison 版本
     */
    public Integer getVerison() {
        return verison;
    }

    /**
     * 版本
     * @param verison 版本
     */
    public void setVerison(Integer verison) {
        this.verison = verison;
    }

    /**
     * 定义红包个数
     * @return total_redpack 定义红包个数
     */
    public Integer getTotalRedpack() {
        return totalRedpack;
    }

    /**
     * 定义红包个数
     * @param totalRedpack 定义红包个数
     */
    public void setTotalRedpack(Integer totalRedpack) {
        this.totalRedpack = totalRedpack;
    }

    /**
     * 红包库存
     * @return total_redpack_stock 红包库存
     */
    public Integer getTotalRedpackStock() {
        return totalRedpackStock;
    }

    /**
     * 红包库存
     * @param totalRedpackStock 红包库存
     */
    public void setTotalRedpackStock(Integer totalRedpackStock) {
        this.totalRedpackStock = totalRedpackStock;
    }

    /**
     * 单个红包金额
     * @return redpack_unit_price 单个红包金额
     */
    public BigDecimal getRedpackUnitPrice() {
        return redpackUnitPrice;
    }

    /**
     * 单个红包金额
     * @param redpackUnitPrice 单个红包金额
     */
    public void setRedpackUnitPrice(BigDecimal redpackUnitPrice) {
        this.redpackUnitPrice = redpackUnitPrice;
    }

    /**
     * 凭证url
     * @return voucher_url 凭证url
     */
    public String getVoucherUrl() {
        return voucherUrl;
    }

    /**
     * 凭证url
     * @param voucherUrl 凭证url
     */
    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl == null ? null : voucherUrl.trim();
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", goodsIdsJson=").append(goodsIdsJson);
        sb.append(", serviceWx=").append(serviceWx);
        sb.append(", serviceQr=").append(serviceQr);
        sb.append(", detail=").append(detail);
        sb.append(", status=").append(status);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", keys=").append(keys);
        sb.append(", total=").append(total);
        sb.append(", totalResidueStock=").append(totalResidueStock);
        sb.append(", totalDay=").append(totalDay);
        sb.append(", deposit=").append(deposit);
        sb.append(", amountJson=").append(amountJson);
        sb.append(", condPersionCount=").append(condPersionCount);
        sb.append(", condDrawTime=").append(condDrawTime);
        sb.append(", hitsPerDraw=").append(hitsPerDraw);
        sb.append(", missionNeedTime=").append(missionNeedTime);
        sb.append(", merchantCouponJson=").append(merchantCouponJson);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", del=").append(del);
        sb.append(", verison=").append(verison);
        sb.append(", totalRedpack=").append(totalRedpack);
        sb.append(", totalRedpackStock=").append(totalRedpackStock);
        sb.append(", redpackUnitPrice=").append(redpackUnitPrice);
        sb.append(", voucherUrl=").append(voucherUrl);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}