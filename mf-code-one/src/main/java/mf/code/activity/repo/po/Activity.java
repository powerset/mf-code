package mf.code.activity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity
 * 活动表
 */
public class Activity implements Serializable {
    /**
     * 活动id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 商户店铺id
     */
    private Long shopId;

    /**
     * 计划内活动 活动计划id
     */
    private Long activityPlanId;

    /**
     * 计划内活动 活动计划实例id
     */
    private Long activityItemId;

    /**
     * 计划内活动 第几个批次
     */
    private Long batchNum;

    /**
     * 活动定义id
     */
    private Long activityDefId;

    /**
     * 活动发起的用户id
     */
    private Long userId;

    /**
     * 支付价格-非商品价格
     */
    private BigDecimal payPrice;

    /**
     * 活动发起的订单id
     */
    private String orderId;

    /**
     * 活动类型 0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包-包裹扫码 7:红包任务-收藏加购 8:助力活动 12:拆红包发起 13:拆红包参与
     */
    private Integer type;

    /**
     * 扣库存类型：0：发起活动扣除库存； 1：完成活动扣除库存
     */
    private Integer stockType;

    /**
     * 活动状态  0待发布 1已发布 -1抽奖未成功 -2强制下线 2已结束
     */
    private Integer status;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 冗余字段: 商品id
     */
    private Long goodsId;

    /**
     * 冗余字段: 每个计划内活动中奖人数
     */
    private Integer hitsPerDraw;

    /**
     * 冗余字段: 开奖条件-满足人数开奖 单位 人数
     */
    private Integer condPersionCount;

    /**
     * 冗余字段: 中奖任务-需要完成时间 单位 分钟
     */
    private Integer missionNeedTime;

    /**
     * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表json 优惠券id
     */
    private String merchantCouponJson;

    /**
     * 冗余字段: 回填订单红包，单个商品保证金定义金额
     */
    private BigDecimal depositDef;

    /**
     * 冗余字段: 回填订单红包，单个商品保证金当前额度
     */
    private BigDecimal deposit;

    /**
     * 开奖时间
     */
    private Date awardStartTime;

    /**
     * 关键词 数组json格式 ["a","b","c"]
     */
    private String keyWordsJson;

    /**
     * 本活动不需要用户提交领奖任务截图: 0 不提交, 1需提交
     */
    private Integer weighting;

    /**
     * 版本
     */
    private Integer version;

    /**
     * 删除标记 . 0:正常 1:删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * activity
     */
    private static final long serialVersionUID = 1L;

    /**
     * 活动id
     * @return id 活动id
     */
    public Long getId() {
        return id;
    }

    /**
     * 活动id
     * @param id 活动id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 商户店铺id
     * @return shop_id 商户店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 商户店铺id
     * @param shopId 商户店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 计划内活动 活动计划id
     * @return activity_plan_id 计划内活动 活动计划id
     */
    public Long getActivityPlanId() {
        return activityPlanId;
    }

    /**
     * 计划内活动 活动计划id
     * @param activityPlanId 计划内活动 活动计划id
     */
    public void setActivityPlanId(Long activityPlanId) {
        this.activityPlanId = activityPlanId;
    }

    /**
     * 计划内活动 活动计划实例id
     * @return activity_item_id 计划内活动 活动计划实例id
     */
    public Long getActivityItemId() {
        return activityItemId;
    }

    /**
     * 计划内活动 活动计划实例id
     * @param activityItemId 计划内活动 活动计划实例id
     */
    public void setActivityItemId(Long activityItemId) {
        this.activityItemId = activityItemId;
    }

    /**
     * 计划内活动 第几个批次
     * @return batch_num 计划内活动 第几个批次
     */
    public Long getBatchNum() {
        return batchNum;
    }

    /**
     * 计划内活动 第几个批次
     * @param batchNum 计划内活动 第几个批次
     */
    public void setBatchNum(Long batchNum) {
        this.batchNum = batchNum;
    }

    /**
     * 活动定义id
     * @return activity_def_id 活动定义id
     */
    public Long getActivityDefId() {
        return activityDefId;
    }

    /**
     * 活动定义id
     * @param activityDefId 活动定义id
     */
    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * 活动发起的用户id
     * @return user_id 活动发起的用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 活动发起的用户id
     * @param userId 活动发起的用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 支付价格-非商品价格
     * @return pay_price 支付价格-非商品价格
     */
    public BigDecimal getPayPrice() {
        return payPrice;
    }

    /**
     * 支付价格-非商品价格
     * @param payPrice 支付价格-非商品价格
     */
    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    /**
     * 活动发起的订单id
     * @return order_id 活动发起的订单id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 活动发起的订单id
     * @param orderId 活动发起的订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * 活动类型 0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包-包裹扫码 7:红包任务-收藏加购 8:助力活动 12:拆红包发起 13:拆红包参与
     * @return type 活动类型 0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包-包裹扫码 7:红包任务-收藏加购 8:助力活动 12:拆红包发起 13:拆红包参与
     */
    public Integer getType() {
        return type;
    }

    /**
     * 活动类型 0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包-包裹扫码 7:红包任务-收藏加购 8:助力活动 12:拆红包发起 13:拆红包参与
     * @param type 活动类型 0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者 6:订单随机红包-包裹扫码 7:红包任务-收藏加购 8:助力活动 12:拆红包发起 13:拆红包参与
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 扣库存类型：0：发起活动扣除库存； 1：完成活动扣除库存
     * @return stock_type 扣库存类型：0：发起活动扣除库存； 1：完成活动扣除库存
     */
    public Integer getStockType() {
        return stockType;
    }

    /**
     * 扣库存类型：0：发起活动扣除库存； 1：完成活动扣除库存
     * @param stockType 扣库存类型：0：发起活动扣除库存； 1：完成活动扣除库存
     */
    public void setStockType(Integer stockType) {
        this.stockType = stockType;
    }

    /**
     * 活动状态  0待发布 1已发布 -1抽奖未成功 -2强制下线 2已结束
     * @return status 活动状态  0待发布 1已发布 -1抽奖未成功 -2强制下线 2已结束
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 活动状态  0待发布 1已发布 -1抽奖未成功 -2强制下线 2已结束
     * @param status 活动状态  0待发布 1已发布 -1抽奖未成功 -2强制下线 2已结束
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 开始时间
     * @return start_time 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 开始时间
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 活动结束时间
     * @return end_time 活动结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 活动结束时间
     * @param endTime 活动结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 冗余字段: 商品id
     * @return goods_id 冗余字段: 商品id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 冗余字段: 商品id
     * @param goodsId 冗余字段: 商品id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 冗余字段: 每个计划内活动中奖人数
     * @return hits_per_draw 冗余字段: 每个计划内活动中奖人数
     */
    public Integer getHitsPerDraw() {
        return hitsPerDraw;
    }

    /**
     * 冗余字段: 每个计划内活动中奖人数
     * @param hitsPerDraw 冗余字段: 每个计划内活动中奖人数
     */
    public void setHitsPerDraw(Integer hitsPerDraw) {
        this.hitsPerDraw = hitsPerDraw;
    }

    /**
     * 冗余字段: 开奖条件-满足人数开奖 单位 人数
     * @return cond_persion_count 冗余字段: 开奖条件-满足人数开奖 单位 人数
     */
    public Integer getCondPersionCount() {
        return condPersionCount;
    }

    /**
     * 冗余字段: 开奖条件-满足人数开奖 单位 人数
     * @param condPersionCount 冗余字段: 开奖条件-满足人数开奖 单位 人数
     */
    public void setCondPersionCount(Integer condPersionCount) {
        this.condPersionCount = condPersionCount;
    }

    /**
     * 冗余字段: 中奖任务-需要完成时间 单位 分钟
     * @return mission_need_time 冗余字段: 中奖任务-需要完成时间 单位 分钟
     */
    public Integer getMissionNeedTime() {
        return missionNeedTime;
    }

    /**
     * 冗余字段: 中奖任务-需要完成时间 单位 分钟
     * @param missionNeedTime 冗余字段: 中奖任务-需要完成时间 单位 分钟
     */
    public void setMissionNeedTime(Integer missionNeedTime) {
        this.missionNeedTime = missionNeedTime;
    }

    /**
     * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表json 优惠券id
     * @return merchant_coupon_json 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表json 优惠券id
     */
    public String getMerchantCouponJson() {
        return merchantCouponJson;
    }

    /**
     * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表json 优惠券id
     * @param merchantCouponJson 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表json 优惠券id
     */
    public void setMerchantCouponJson(String merchantCouponJson) {
        this.merchantCouponJson = merchantCouponJson == null ? null : merchantCouponJson.trim();
    }

    /**
     * 冗余字段: 回填订单红包，单个商品保证金定义金额
     * @return deposit_def 冗余字段: 回填订单红包，单个商品保证金定义金额
     */
    public BigDecimal getDepositDef() {
        return depositDef;
    }

    /**
     * 冗余字段: 回填订单红包，单个商品保证金定义金额
     * @param depositDef 冗余字段: 回填订单红包，单个商品保证金定义金额
     */
    public void setDepositDef(BigDecimal depositDef) {
        this.depositDef = depositDef;
    }

    /**
     * 冗余字段: 回填订单红包，单个商品保证金当前额度
     * @return deposit 冗余字段: 回填订单红包，单个商品保证金当前额度
     */
    public BigDecimal getDeposit() {
        return deposit;
    }

    /**
     * 冗余字段: 回填订单红包，单个商品保证金当前额度
     * @param deposit 冗余字段: 回填订单红包，单个商品保证金当前额度
     */
    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    /**
     * 开奖时间
     * @return award_start_time 开奖时间
     */
    public Date getAwardStartTime() {
        return awardStartTime;
    }

    /**
     * 开奖时间
     * @param awardStartTime 开奖时间
     */
    public void setAwardStartTime(Date awardStartTime) {
        this.awardStartTime = awardStartTime;
    }

    /**
     * 关键词 数组json格式 ["a","b","c"]
     * @return key_words_json 关键词 数组json格式 ["a","b","c"]
     */
    public String getKeyWordsJson() {
        return keyWordsJson;
    }

    /**
     * 关键词 数组json格式 ["a","b","c"]
     * @param keyWordsJson 关键词 数组json格式 ["a","b","c"]
     */
    public void setKeyWordsJson(String keyWordsJson) {
        this.keyWordsJson = keyWordsJson == null ? null : keyWordsJson.trim();
    }

    /**
     * 本活动不需要用户提交领奖任务截图: 0 不提交, 1需提交
     * @return weighting 本活动不需要用户提交领奖任务截图: 0 不提交, 1需提交
     */
    public Integer getWeighting() {
        return weighting;
    }

    /**
     * 本活动不需要用户提交领奖任务截图: 0 不提交, 1需提交
     * @param weighting 本活动不需要用户提交领奖任务截图: 0 不提交, 1需提交
     */
    public void setWeighting(Integer weighting) {
        this.weighting = weighting;
    }

    /**
     * 版本
     * @return version 版本
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 版本
     * @param version 版本
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @return del 删除标记 . 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @param del 删除标记 . 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", activityPlanId=").append(activityPlanId);
        sb.append(", activityItemId=").append(activityItemId);
        sb.append(", batchNum=").append(batchNum);
        sb.append(", activityDefId=").append(activityDefId);
        sb.append(", userId=").append(userId);
        sb.append(", payPrice=").append(payPrice);
        sb.append(", orderId=").append(orderId);
        sb.append(", type=").append(type);
        sb.append(", stockType=").append(stockType);
        sb.append(", status=").append(status);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", hitsPerDraw=").append(hitsPerDraw);
        sb.append(", condPersionCount=").append(condPersionCount);
        sb.append(", missionNeedTime=").append(missionNeedTime);
        sb.append(", merchantCouponJson=").append(merchantCouponJson);
        sb.append(", depositDef=").append(depositDef);
        sb.append(", deposit=").append(deposit);
        sb.append(", awardStartTime=").append(awardStartTime);
        sb.append(", keyWordsJson=").append(keyWordsJson);
        sb.append(", weighting=").append(weighting);
        sb.append(", version=").append(version);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}