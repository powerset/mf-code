package mf.code.activity.repo.po;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * activity_def
 * 活动定义表，定义用户自发活动的表
 */
public class ActivityDef implements Serializable {
    /**
     * 活动id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 定义活动的父级编号
     */
    private Long parentId;

    /**
     * 商户店铺id
     */
    private Long shopId;

    /**
     * 活动类型
1:计划内活动 (废弃)
2:用户订单活动=免单抽奖 v1
3:新人有礼活动 (废弃)
4:新人大转盘
5:回填订单红包活动 v1
6:助力活动=7:赠品活动
7:赠品活动=6:助力活动
8:红包活动（回填订单，收藏加购，好评晒图)  (废弃)
9:拆红包
10好评晒图 v1
11收藏加购 v1
12免单抽奖活动 v2
13赠品活动 v2
14,回填订单红包任务 v2
15,好评晒图 v2
16,收藏加购 v2
     */
    private Integer type;

    /**
     * 活动状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束 5已结算
     */
    private Integer status;

    /**
     * 活动标题
     */
    private String title;

    /**
     * 活动描述
     */
    @TableField("`desc`")
    private String desc;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * Json格式，商品id列表[1,2]
     */
    private String goodsIds;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 扣库存方式：1提前占用库存法 2先到先得减库存法
     */
    private Integer stockType;

    /**
     * 库存定义数量
     */
    private Integer stockDef;

    /**
     * 库存数量
     */
    private Integer stock;

    /**
     * 保证金定义额度
     */
    private BigDecimal depositDef;

    /**
     * 保证金额度
     */
    private BigDecimal deposit;

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    private Integer depositStatus;

    /**
     * 保证金支付凭证地址
     */
    private String voucher;

    /**
     *
     */
    private String keyWord;

    /**
     * 同一活动定义单个用户可发起的总次数
     */
    private Integer startNum;

    /**
     * 发布时间
     */
    private Date publishTime;

    /**
     * 活动时间（从发起抽奖到抽奖结束的时间段） 单位 分钟
     */
    private Integer condDrawTime;

    /**
     * 开奖条件-满足人数开奖 单位 人数
     */
    private Integer condPersionCount;

    /**
     * 每个计划内活动中奖人数
     */
    private Integer hitsPerDraw;

    /**
     * 成长任务单红包金额
     */
    private BigDecimal taskRpAmount;

    /**
     * 成长任务红包定义个数
     */
    private Integer taskRpNumDef;

    /**
     * 成长任务红包个数
     */
    private Integer taskRpNum;

    /**
     * 成长任务红包定义保证金
     */
    private BigDecimal taskRpDepositDef;

    /**
     * 成长任务红包保证金
     */
    private BigDecimal taskRpDeposit;

    /**
     * 总保证金额度（活动保证金和任务保证金和）
     */
    private BigDecimal totalDeposit;

    /**
     * 中奖任务-需要完成时间 单位 分钟
     */
    private Integer missionNeedTime;

    /**
     * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 优惠券id
     */
    private String merchantCouponJson;

    /**
     * 本活动不需要用户提交领奖任务截图: 0不提交, 1需提交
     */
    private Integer weighting;

    /**
     * 提交申请时间
     */
    private Date applyTime;

    /**
     * 审核时间
     */
    private Date auditingTime;

    /**
     * 审核不通过原因
     */
    private String auditingReason;

    /**
     * 删除标记 . 0:正常 1:删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 修改时间
     */
    private Date utime;

    /**
     * 客服微信号
     */
    private String serviceWx;

    /**
     * 发起活动支付金额
     */
    private BigDecimal jeton;

    /**
     * 额外任务佣金（免单抽奖和免单赠品额外使用）
     */
    private BigDecimal commission;

    /**
     * 报销活动时存单人单次报销范围和单人最大 报销金额，格式：{"perAmount":{"min":"6","max":"100"},"personalMaxAmount":"200"}
     */
    private String amountJson;

    /**
     * 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
     */
    private Integer finishType;

    /**
     * 审核方式（1:人工审核 2:自动审核）
     */
    private Integer auditType;

    /**
     * activity_def
     */
    private static final long serialVersionUID = 1L;

    /**
     * 活动id
     * @return id 活动id
     */
    public Long getId() {
        return id;
    }

    /**
     * 活动id
     * @param id 活动id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 定义活动的父级编号
     * @return parent_id 定义活动的父级编号
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 定义活动的父级编号
     * @param parentId 定义活动的父级编号
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 商户店铺id
     * @return shop_id 商户店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 商户店铺id
     * @param shopId 商户店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 活动类型
 1:计划内活动 (废弃)
 2:用户订单活动=免单抽奖 v1
 3:新人有礼活动 (废弃)
 4:新人大转盘
 5:回填订单红包活动 v1
 6:助力活动=7:赠品活动
 7:赠品活动=6:助力活动
 8:红包活动（回填订单，收藏加购，好评晒图)  (废弃)
 9:拆红包
 10好评晒图 v1
 11收藏加购 v1
 12免单抽奖活动 v2
 13赠品活动 v2
 14,回填订单红包任务 v2
 15,好评晒图 v2
 16,收藏加购 v2
     * @return type 活动类型
 1:计划内活动 (废弃)
 2:用户订单活动=免单抽奖 v1
 3:新人有礼活动 (废弃)
 4:新人大转盘
 5:回填订单红包活动 v1
 6:助力活动=7:赠品活动
 7:赠品活动=6:助力活动
 8:红包活动（回填订单，收藏加购，好评晒图)  (废弃)
 9:拆红包
 10好评晒图 v1
 11收藏加购 v1
 12免单抽奖活动 v2
 13赠品活动 v2
 14,回填订单红包任务 v2
 15,好评晒图 v2
 16,收藏加购 v2
     */
    public Integer getType() {
        return type;
    }

    /**
     * 活动类型
 1:计划内活动 (废弃)
 2:用户订单活动=免单抽奖 v1
 3:新人有礼活动 (废弃)
 4:新人大转盘
 5:回填订单红包活动 v1
 6:助力活动=7:赠品活动
 7:赠品活动=6:助力活动
 8:红包活动（回填订单，收藏加购，好评晒图)  (废弃)
 9:拆红包
 10好评晒图 v1
 11收藏加购 v1
 12免单抽奖活动 v2
 13赠品活动 v2
 14,回填订单红包任务 v2
 15,好评晒图 v2
 16,收藏加购 v2
     * @param type 活动类型
 1:计划内活动 (废弃)
 2:用户订单活动=免单抽奖 v1
 3:新人有礼活动 (废弃)
 4:新人大转盘
 5:回填订单红包活动 v1
 6:助力活动=7:赠品活动
 7:赠品活动=6:助力活动
 8:红包活动（回填订单，收藏加购，好评晒图)  (废弃)
 9:拆红包
 10好评晒图 v1
 11收藏加购 v1
 12免单抽奖活动 v2
 13赠品活动 v2
 14,回填订单红包任务 v2
 15,好评晒图 v2
 16,收藏加购 v2
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 活动状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束 5已结算
     * @return status 活动状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束 5已结算
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 活动状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束 5已结算
     * @param status 活动状态  -2强制下线 -1审核失败 0保存草稿 1待审核 2审核通过 3已发布 4已结束 5已结算
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 活动标题
     * @return title 活动标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 活动标题
     * @param title 活动标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 活动描述
     * @return desc 活动描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 活动描述
     * @param desc 活动描述
     */
    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }

    /**
     * 开始时间
     * @return start_time 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 开始时间
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 活动结束时间
     * @return end_time 活动结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 活动结束时间
     * @param endTime 活动结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Json格式，商品id列表[1,2]
     * @return goods_ids Json格式，商品id列表[1,2]
     */
    public String getGoodsIds() {
        return goodsIds;
    }

    /**
     * Json格式，商品id列表[1,2]
     * @param goodsIds Json格式，商品id列表[1,2]
     */
    public void setGoodsIds(String goodsIds) {
        this.goodsIds = goodsIds == null ? null : goodsIds.trim();
    }

    /**
     * 商品id
     * @return goods_id 商品id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 商品id
     * @param goodsId 商品id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商品价格
     * @return goods_price 商品价格
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 商品价格
     * @param goodsPrice 商品价格
     */
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 扣库存方式：1提前占用库存法 2先到先得减库存法
     * @return stock_type 扣库存方式：1提前占用库存法 2先到先得减库存法
     */
    public Integer getStockType() {
        return stockType;
    }

    /**
     * 扣库存方式：1提前占用库存法 2先到先得减库存法
     * @param stockType 扣库存方式：1提前占用库存法 2先到先得减库存法
     */
    public void setStockType(Integer stockType) {
        this.stockType = stockType;
    }

    /**
     * 库存定义数量
     * @return stock_def 库存定义数量
     */
    public Integer getStockDef() {
        return stockDef;
    }

    /**
     * 库存定义数量
     * @param stockDef 库存定义数量
     */
    public void setStockDef(Integer stockDef) {
        this.stockDef = stockDef;
    }

    /**
     * 库存数量
     * @return stock 库存数量
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * 库存数量
     * @param stock 库存数量
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 保证金定义额度
     * @return deposit_def 保证金定义额度
     */
    public BigDecimal getDepositDef() {
        return depositDef;
    }

    /**
     * 保证金定义额度
     * @param depositDef 保证金定义额度
     */
    public void setDepositDef(BigDecimal depositDef) {
        this.depositDef = depositDef;
    }

    /**
     * 保证金额度
     * @return deposit 保证金额度
     */
    public BigDecimal getDeposit() {
        return deposit;
    }

    /**
     * 保证金额度
     * @param deposit 保证金额度
     */
    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @return deposit_status 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public Integer getDepositStatus() {
        return depositStatus;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @param depositStatus 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public void setDepositStatus(Integer depositStatus) {
        this.depositStatus = depositStatus;
    }

    /**
     * 保证金支付凭证地址
     * @return voucher 保证金支付凭证地址
     */
    public String getVoucher() {
        return voucher;
    }

    /**
     * 保证金支付凭证地址
     * @param voucher 保证金支付凭证地址
     */
    public void setVoucher(String voucher) {
        this.voucher = voucher == null ? null : voucher.trim();
    }

    /**
     *
     * @return key_word
     */
    public String getKeyWord() {
        return keyWord;
    }

    /**
     *
     * @param keyWord
     */
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord == null ? null : keyWord.trim();
    }

    /**
     * 同一活动定义单个用户可发起的总次数
     * @return start_num 同一活动定义单个用户可发起的总次数
     */
    public Integer getStartNum() {
        return startNum;
    }

    /**
     * 同一活动定义单个用户可发起的总次数
     * @param startNum 同一活动定义单个用户可发起的总次数
     */
    public void setStartNum(Integer startNum) {
        this.startNum = startNum;
    }

    /**
     * 发布时间
     * @return publish_time 发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 发布时间
     * @param publishTime 发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * 活动时间（从发起抽奖到抽奖结束的时间段） 单位 分钟
     * @return cond_draw_time 活动时间（从发起抽奖到抽奖结束的时间段） 单位 分钟
     */
    public Integer getCondDrawTime() {
        return condDrawTime;
    }

    /**
     * 活动时间（从发起抽奖到抽奖结束的时间段） 单位 分钟
     * @param condDrawTime 活动时间（从发起抽奖到抽奖结束的时间段） 单位 分钟
     */
    public void setCondDrawTime(Integer condDrawTime) {
        this.condDrawTime = condDrawTime;
    }

    /**
     * 开奖条件-满足人数开奖 单位 人数
     * @return cond_persion_count 开奖条件-满足人数开奖 单位 人数
     */
    public Integer getCondPersionCount() {
        return condPersionCount;
    }

    /**
     * 开奖条件-满足人数开奖 单位 人数
     * @param condPersionCount 开奖条件-满足人数开奖 单位 人数
     */
    public void setCondPersionCount(Integer condPersionCount) {
        this.condPersionCount = condPersionCount;
    }

    /**
     * 每个计划内活动中奖人数
     * @return hits_per_draw 每个计划内活动中奖人数
     */
    public Integer getHitsPerDraw() {
        return hitsPerDraw;
    }

    /**
     * 每个计划内活动中奖人数
     * @param hitsPerDraw 每个计划内活动中奖人数
     */
    public void setHitsPerDraw(Integer hitsPerDraw) {
        this.hitsPerDraw = hitsPerDraw;
    }

    /**
     * 成长任务单红包金额
     * @return task_rp_amount 成长任务单红包金额
     */
    public BigDecimal getTaskRpAmount() {
        return taskRpAmount;
    }

    /**
     * 成长任务单红包金额
     * @param taskRpAmount 成长任务单红包金额
     */
    public void setTaskRpAmount(BigDecimal taskRpAmount) {
        this.taskRpAmount = taskRpAmount;
    }

    /**
     * 成长任务红包定义个数
     * @return task_rp_num_def 成长任务红包定义个数
     */
    public Integer getTaskRpNumDef() {
        return taskRpNumDef;
    }

    /**
     * 成长任务红包定义个数
     * @param taskRpNumDef 成长任务红包定义个数
     */
    public void setTaskRpNumDef(Integer taskRpNumDef) {
        this.taskRpNumDef = taskRpNumDef;
    }

    /**
     * 成长任务红包个数
     * @return task_rp_num 成长任务红包个数
     */
    public Integer getTaskRpNum() {
        return taskRpNum;
    }

    /**
     * 成长任务红包个数
     * @param taskRpNum 成长任务红包个数
     */
    public void setTaskRpNum(Integer taskRpNum) {
        this.taskRpNum = taskRpNum;
    }

    /**
     * 成长任务红包定义保证金
     * @return task_rp_deposit_def 成长任务红包定义保证金
     */
    public BigDecimal getTaskRpDepositDef() {
        return taskRpDepositDef;
    }

    /**
     * 成长任务红包定义保证金
     * @param taskRpDepositDef 成长任务红包定义保证金
     */
    public void setTaskRpDepositDef(BigDecimal taskRpDepositDef) {
        this.taskRpDepositDef = taskRpDepositDef;
    }

    /**
     * 成长任务红包保证金
     * @return task_rp_deposit 成长任务红包保证金
     */
    public BigDecimal getTaskRpDeposit() {
        return taskRpDeposit;
    }

    /**
     * 成长任务红包保证金
     * @param taskRpDeposit 成长任务红包保证金
     */
    public void setTaskRpDeposit(BigDecimal taskRpDeposit) {
        this.taskRpDeposit = taskRpDeposit;
    }

    /**
     * 总保证金额度（活动保证金和任务保证金和）
     * @return total_deposit 总保证金额度（活动保证金和任务保证金和）
     */
    public BigDecimal getTotalDeposit() {
        return totalDeposit;
    }

    /**
     * 总保证金额度（活动保证金和任务保证金和）
     * @param totalDeposit 总保证金额度（活动保证金和任务保证金和）
     */
    public void setTotalDeposit(BigDecimal totalDeposit) {
        this.totalDeposit = totalDeposit;
    }

    /**
     * 中奖任务-需要完成时间 单位 分钟
     * @return mission_need_time 中奖任务-需要完成时间 单位 分钟
     */
    public Integer getMissionNeedTime() {
        return missionNeedTime;
    }

    /**
     * 中奖任务-需要完成时间 单位 分钟
     * @param missionNeedTime 中奖任务-需要完成时间 单位 分钟
     */
    public void setMissionNeedTime(Integer missionNeedTime) {
        this.missionNeedTime = missionNeedTime;
    }

    /**
     * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 优惠券id
     * @return merchant_coupon_json 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 优惠券id
     */
    public String getMerchantCouponJson() {
        return merchantCouponJson;
    }

    /**
     * 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 优惠券id
     * @param merchantCouponJson 冗余字段: 商户优惠券 不中奖发放商户优惠券id列表 逗号分隔 优惠券id
     */
    public void setMerchantCouponJson(String merchantCouponJson) {
        this.merchantCouponJson = merchantCouponJson == null ? null : merchantCouponJson.trim();
    }

    /**
     * 本活动不需要用户提交领奖任务截图: 0不提交, 1需提交
     * @return weighting 本活动不需要用户提交领奖任务截图: 0不提交, 1需提交
     */
    public Integer getWeighting() {
        return weighting;
    }

    /**
     * 本活动不需要用户提交领奖任务截图: 0不提交, 1需提交
     * @param weighting 本活动不需要用户提交领奖任务截图: 0不提交, 1需提交
     */
    public void setWeighting(Integer weighting) {
        this.weighting = weighting;
    }

    /**
     * 提交申请时间
     * @return apply_time 提交申请时间
     */
    public Date getApplyTime() {
        return applyTime;
    }

    /**
     * 提交申请时间
     * @param applyTime 提交申请时间
     */
    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    /**
     * 审核时间
     * @return auditing_time 审核时间
     */
    public Date getAuditingTime() {
        return auditingTime;
    }

    /**
     * 审核时间
     * @param auditingTime 审核时间
     */
    public void setAuditingTime(Date auditingTime) {
        this.auditingTime = auditingTime;
    }

    /**
     * 审核不通过原因
     * @return auditing_reason 审核不通过原因
     */
    public String getAuditingReason() {
        return auditingReason;
    }

    /**
     * 审核不通过原因
     * @param auditingReason 审核不通过原因
     */
    public void setAuditingReason(String auditingReason) {
        this.auditingReason = auditingReason == null ? null : auditingReason.trim();
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @return del 删除标记 . 0:正常 1:删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标记 . 0:正常 1:删除
     * @param del 删除标记 . 0:正常 1:删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 修改时间
     * @return utime 修改时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 修改时间
     * @param utime 修改时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 客服微信号
     * @return service_wx 客服微信号
     */
    public String getServiceWx() {
        return serviceWx;
    }

    /**
     * 客服微信号
     * @param serviceWx 客服微信号
     */
    public void setServiceWx(String serviceWx) {
        this.serviceWx = serviceWx == null ? null : serviceWx.trim();
    }

    /**
     * 发起活动支付金额
     * @return jeton 发起活动支付金额
     */
    public BigDecimal getJeton() {
        return jeton;
    }

    /**
     * 发起活动支付金额
     * @param jeton 发起活动支付金额
     */
    public void setJeton(BigDecimal jeton) {
        this.jeton = jeton;
    }

    /**
     * 额外任务佣金（免单抽奖和免单赠品额外使用）
     * @return commission 额外任务佣金（免单抽奖和免单赠品额外使用）
     */
    public BigDecimal getCommission() {
        return commission;
    }

    /**
     * 额外任务佣金（免单抽奖和免单赠品额外使用）
     * @param commission 额外任务佣金（免单抽奖和免单赠品额外使用）
     */
    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    /**
     *
     * @return amount_json
     */
    public String getAmountJson() {
        return amountJson;
    }

    /**
     *
     * @param amountJson
     */
    public void setAmountJson(String amountJson) {
        this.amountJson = amountJson == null ? null : amountJson.trim();
    }

    /**
     * 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
     * @return finish_type 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
     */
    public Integer getFinishType() {
        return finishType;
    }

    /**
     * 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
     * @param finishType 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
     */
    public void setFinishType(Integer finishType) {
        this.finishType = finishType;
    }

    /**
     * 审核方式（1:人工审核 2:自动审核）
     * @return audit_type 审核方式（1:人工审核 2:自动审核）
     */
    public Integer getAuditType() {
        return auditType;
    }

    /**
     * 审核方式（1:人工审核 2:自动审核）
     * @param auditType 审核方式（1:人工审核 2:自动审核）
     */
    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    /**
     *
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", parentId=").append(parentId);
        sb.append(", shopId=").append(shopId);
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append(", title=").append(title);
        sb.append(", desc=").append(desc);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", goodsIds=").append(goodsIds);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodsPrice=").append(goodsPrice);
        sb.append(", stockType=").append(stockType);
        sb.append(", stockDef=").append(stockDef);
        sb.append(", stock=").append(stock);
        sb.append(", depositDef=").append(depositDef);
        sb.append(", deposit=").append(deposit);
        sb.append(", depositStatus=").append(depositStatus);
        sb.append(", voucher=").append(voucher);
        sb.append(", keyWord=").append(keyWord);
        sb.append(", startNum=").append(startNum);
        sb.append(", publishTime=").append(publishTime);
        sb.append(", condDrawTime=").append(condDrawTime);
        sb.append(", condPersionCount=").append(condPersionCount);
        sb.append(", hitsPerDraw=").append(hitsPerDraw);
        sb.append(", taskRpAmount=").append(taskRpAmount);
        sb.append(", taskRpNumDef=").append(taskRpNumDef);
        sb.append(", taskRpNum=").append(taskRpNum);
        sb.append(", taskRpDepositDef=").append(taskRpDepositDef);
        sb.append(", taskRpDeposit=").append(taskRpDeposit);
        sb.append(", totalDeposit=").append(totalDeposit);
        sb.append(", missionNeedTime=").append(missionNeedTime);
        sb.append(", merchantCouponJson=").append(merchantCouponJson);
        sb.append(", weighting=").append(weighting);
        sb.append(", applyTime=").append(applyTime);
        sb.append(", auditingTime=").append(auditingTime);
        sb.append(", auditingReason=").append(auditingReason);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serviceWx=").append(serviceWx);
        sb.append(", jeton=").append(jeton);
        sb.append(", commission=").append(commission);
        sb.append(", amountJson=").append(amountJson);
        sb.append(", finishType=").append(finishType);
        sb.append(", auditType=").append(auditType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
