package mf.code.activity.repo.redis;

/**
 * @Author yechen
 * @Date
 **/
public class RedisKeyConstant {

    /**
     * 数据库 MerchantShop 的一条记录
     * 存储要求：
     *  1.merchantShop.getPicPath()：不为空
     *  2.有效期：1个自然日 00:00:00 过期
     */
    public static final String MERCHANT_SHOP = "mf:code:merchant:shopid:";//<shopid>

    /**
     * 首页店铺活动的reids
     */
    public static final String HOMEPAGE_ACTIVITY_MERCHANT = "activity:activity:homepage:merchant:";//<shopid>
    /**
     * 首页用户发起(走订单)活动的reids
     */
    public static final String HOMEPAGE_ACTIVITY_ORDER = "activity:activity:homepage:order:";//<shopid>
    /**
     * 首页用户发起(走订单)活动的reids
     */
    public static final String HOMEPAGE_ACTIVITY_SHOP = "activity:activity:homepage:shop:";//<shopid>
    /**
     * 首页用户发起(走订单)活动的reids
     */
    public static final String HOMEPAGE_TURNTABLE_SHOP = "activity:activity:homepage:shop:turntable:";//<shopid>:<uid>
    /**
     * 每天活动开始时间
     */
    public static final String ACTIVITY_START_TIME_PER_DAY = "mf:activity:activityService:createActivity:startTimeEachDay";
    /**
     * 每天活动结束时间
     */
    public static final String ACTIVITY_END_TIME_PER_DAY = "mf:activity:activityService:createActivity:endTimeEachDay";
    /**
     * 商户后台 活动管理 创建活动 的防重key
     */
    public static final String ACTIVITY_DEF_CREATE_FORBID_REPEAT = "activity:activitydef:create:forbidrepeat:";//<mid>

    /**
     * 商户后台 活动管理 修改关联拆红包活动的防重key
     */
    public static final String ACTIVITY_DEF_OPEN_RED_PACK = "activity:activitydef:openRedPack:";

}
