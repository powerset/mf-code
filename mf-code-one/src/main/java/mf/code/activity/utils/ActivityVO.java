package mf.code.activity.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.utils
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-29 下午5:10
 */
public class ActivityVO {

    public static Map<String, Object> initResultVO2AuditRecord() {
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("supplyCount", 0);
        List<Map<String, Object>> activityDefAuditVOList = new ArrayList<>();
        Map<String, Object> activityDefAuditVO = new HashMap<>();
        activityDefAuditVO.put("applyTime", "");
        activityDefAuditVO.put("applyStock", "");
        activityDefAuditVO.put("applyDeposit", "");
        activityDefAuditVO.put("initStock", "");
        activityDefAuditVO.put("finalStock", "");
        activityDefAuditVOList.add(activityDefAuditVO);
        resultVO.put("activityDefAuditVOList", activityDefAuditVOList);
        return resultVO;
    }
}
