package mf.code.activity.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author gel
 * @date 2019-05-23 19:14
 */
public interface ActivityDefCheckService {

    ActivityDef updateFromActivityDefV2Req(ActivityDefV2Req activityDefV2Req);

    /**
     * 校验 是否 有创建活动 的资格
     * 在创建活动时，会对 同个店铺下且未删除的 商品或商品组 进行 状态的校验
     * 若已存在 保存，待审核，审核通过，发布中 的商品，活动均无法创建；否则 可创建。
     * 在修改活动时（通过是否存在activityDefId判断是否是修改操作），若修改传入的
     * 商品或商品组 与保存在数据库里的 商品或商品组 不一致，则按创建活动时的标准校验；
     * 一致更，则返回 可修改状态
     *
     * @param merchantId  商户id
     * @param shopId      店铺id
     * @param type        活动类型
     * @param goodsId     商品id
     * @param goodsIdList 商品组ids
     * @return SimpleResponse<Object>
     */
    SimpleResponse<Object> createAble(Long merchantId, Long shopId, Integer type, Long goodsId, List<Long> goodsIdList, Long activityDefId);

    /**
     * 通过activityDefV2Req创建活动定义
     *
     * @param activityDefV2Req 请求参数
     * @return ActivityDef
     */
    ActivityDef saveOrUpdateActivityDefFromActivityDefV2Req(ActivityDefV2Req activityDefV2Req);

    boolean pay2start(Long activityDefId);

    /**
     * 通过支付结算活动定义
     **/
    boolean refundDef(Long activityDefId, BigDecimal refundMoney);

    /**
     * 通过补库存结算活动
     **/
    boolean refundAddStockAudit(Long activityDefAuditId, BigDecimal refundMoney);

    SimpleResponse removeActivityDefAtSaveStatus(Long activityDefId);

    SimpleResponse suspendOrResumeActivityDef(Long activityDefId, Integer status);

    /**
     * 活动定义 是否 已发布
     *
     * @param activityDefV2Req
     * @return
     */
    boolean checkPublished(ActivityDefV2Req activityDefV2Req);


    /**
     * 校验活动定义 是否能正常发起活动，isReduce = true 在校验的同时减库存，isReduce = false在校验的时候 不减库存
     *
     * @param activityDefId
     * @param activityDef
     * @param isReduce
     * @return 有库存 返回true
     */
    boolean checkValidity(Long activityDefId, ActivityDef activityDef, boolean isReduce);

    /**
     * 对已发布中的活动 的修改 进行校验：
     * 赠品和免单商品 活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
     * 领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
     * 无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
     * 幸运大转盘 活动，支持修改的选项有：无。
     * 无法修改的选项有：类型type, 奖励人数stockDef
     * 红包任务 活动，支持修改的选项有：收藏加购任务时间(分)favCartMissionNeedTime	，好评返现任务时间(分)	goodCommentMissionNeedTime
     * 无法修改的选项有：活动商品组goodsIdList，初始红包库存stockDef，回填订单金额fillBackOrderAmount，收藏加购金额favCartAmount，好评晒图金额goodCommentAmount
     *
     * @param activityDefV2Req activityDefV2Req
     * @return 响应消息：
     */
    SimpleResponse<Object> checkUnUpateParamWhenPublished(ActivityDefV2Req activityDefV2Req);
}
