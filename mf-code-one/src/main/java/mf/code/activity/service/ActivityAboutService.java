package mf.code.activity.service;

import mf.code.activity.repo.po.Activity;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;

import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月25日 09:36
 */
public interface ActivityAboutService {

    /***
     * 获取活动内容对应的客服信息
     * @param activity
     * @return
     */
    Map<String, Object> getServiceWx(Activity activity);

    Map<String, Object> changeCustomServiceToMap(String wechat, String pic);

    /***
     * 查询活动信息的详情
     * @param activity
     * @param merchantShop
     * @param activityPersonList
     * @param customServiceMap
     * @return
     */
    Map<String, Object> queryActivityAndGoods(Activity activity,
                                              MerchantShop merchantShop,
                                              List<String> activityPersonList,
                                              Map<String, Object> customServiceMap);

    /***
     * 查询商品信息
     * @param goodsID
     * @return
     */
    Map<String, Object> queryGoodsAbout(Long goodsID);


    /***
     * 根据用户id查询是否参加
     * joinStatus:0:未参与  1:参与,但没中获  2:中奖  3:参与,有优惠券(其他) 4:活动未开始 5：活动活动结束 6 中奖失效状态
     * 其中状态优先级：中奖>结束>参与
     * @param userID
     * @param activity
     * @param activityPersonList
     * @param activityPersonInviteList
     * @param merchantShop
     * @param customServiceMap
     * @return
     */
    Map<String, Object> queryUserActivityJoin(Long userID, Activity activity, List<String> activityPersonList, List<String> activityPersonInviteList,
                                              MerchantShop merchantShop, Map<String, Object> customServiceMap);

     /***
     * 处理用户此时的状态
     * @param userActivity
     * @param activity
     * @param activtityStatus
     * @param userID
     * @return -1: 已结束;1:待开始;2:待开奖;3:已开奖;
     *          4:待开奖,未参与(出现参与按钮),5：待开奖,参与(出现召唤好友);
     *          6：已开奖,没中;7：已开奖,中优惠券;8：已开奖,规定时间没完成任务;9：已开奖,并且获奖;
     */
    Integer getJoinStatus(UserTask userTask, UserActivity userActivity, Activity activity, int activtityStatus, Long userID);

    /***
     * 获取中奖人信息
     * @param activity
     * @param activityPersonList
     * @return
     */
    List<Object> queryRedisWinner(Activity activity, List<String> activityPersonList);

    /***
     * 参与人展现,直接倒叙排列,获取参与人的小圆圈数目
     * @param activity
     * @param activityID
     * @param activityPersonList
     * @return
     */
    List<Object> queryRedisInviteUser(Activity activity, Long activityID, List<String> activityPersonList);

    /***
     * 查询自己的活动
     * @param activity
     * @param userId
     * @return
     */
    Activity getMySelfActivity(Activity activity, Long userId);

    /***
     * 查询是否开团
     * @param activity
     * @param validAward 是否有效开奖状态(排除过期的)
     * @return
     */
    Boolean checkAwardOrAwarded(Activity activity, boolean validAward);
}
