package mf.code.activity.service.activitydef.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.common.DelEnum;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.dto.ProductSkuEntity;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.repo.po.GoodsPlus;
import mf.code.goods.service.GoodsPlusService;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.activity.service.activitydef.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-23 16:32
 */
@Service
public class ActivityDefFavCartV2Service extends AbstractActivityDefService {
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsPlusService goodsPlusService;

    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Merchant merchant = this.merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(req.getShopId());
        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if (req.getExtraDataObject() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少必要参数");
        }
        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        if (!success) {
            return RedisForbidRepeat.NXError();
        }
        // 收藏加购多个商品id
        if (StringUtils.isBlank(req.getGoodsIds())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "传参异常，请选择商品");
        }
        Set<Long> goodsIdSet = new HashSet<>();
        String[] split = req.getGoodsIds().split(",");
        for (String str : split) {
            if (StringUtils.isNotBlank(str)) {
                goodsIdSet.add(Long.valueOf(str));
            }
        }
        if (goodsIdSet.size() <= 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "传参异常，请选择商品");
        }
        // 此时goodsId为productid，需要查询到goodsId
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Goods::getMerchantId, req.getMerchantId())
                .eq(Goods::getShopId, req.getShopId())
                .in(Goods::getProductId, goodsIdSet)
                .orderByDesc(Goods::getId);
        List<Goods> list = goodsService.list(queryWrapper);
        goodsIdSet.clear();
        if (CollectionUtils.isEmpty(list)) {
            goodsIdSet.add(0L);
        } else {
            for (Goods goods : list) {
                goodsIdSet.add(goods.getId());
            }
        }
        // 校验是否有创建活动的资格
        simpleResponse = activityDefCheckService.createAble(req.getMerchantId(), req.getShopId(), req.getType(), null, new ArrayList<>(goodsIdSet), null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        if (req.getActivityDefId() != null && req.getActivityDefId() > 0) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
            org.springframework.util.Assert.isTrue(activityDef != null, "该编号的活动不存在哦！defId=" + req.getActivityDefId());
            req.from(activityDef);
            req.setExtraDataObject(new CreateDefReq.ExtraData());
            req.getExtraDataObject().from(activityDef);
        } else {
            req.init();
        }
        //若此时没有穿库存数
        if (req.getStockDef() == null || req.getStockDef() == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请输入正确的库存");
        }
        return this.activityDefAboutService.getDepositByProduct(req);
    }

    @Override
    public SimpleResponse createDef(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Set<Long> goodsIdSet = new HashSet<>();
        String[] split = req.getGoodsIds().split(",");
        for (String str : split) {
            if (StringUtils.isNotBlank(str)) {
                goodsIdSet.add(Long.valueOf(str));
            }
        }
        if (goodsIdSet.size() <= 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "传参异常，请选择商品");
        }
        // 此时goodsId为productid，需要查询到goodsId
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Goods::getMerchantId, req.getMerchantId())
                .eq(Goods::getShopId, req.getShopId())
                .in(Goods::getProductId, goodsIdSet)
                .orderByDesc(Goods::getId);
        List<Goods> list = goodsService.list(queryWrapper);
        // 为保证与之前活动逻辑复用，暂时添加
        StringBuilder sb = new StringBuilder();
        // 未创建的话需要创建goods和goods_plus
        if (CollectionUtils.isEmpty(list)) {
            List<ProductEntity> productEntityList = goodsAppService.queryProductDetailList(new ArrayList<>(goodsIdSet));
            if (CollectionUtils.isEmpty(productEntityList)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "网络异常，请稍后重试");
            }
            List<Goods> goodsList = new ArrayList<>();
            for (ProductEntity productEntity : productEntityList) {
                if (productEntity.getShowType() == ProductShowTypeEnum.NEWBIE.getCode()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "新手专区商品不能参与活动");
                }
                if (!productEntity.getParentId().equals(productEntity.getId())) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "分销商品不能参与活动");
                }
                List<ProductSkuEntity> productSkuEntityList = productEntity.getProductSkuList();
                if (CollectionUtils.isEmpty(productSkuEntityList)) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "网络异常，请稍后重试");
                }
                BigDecimal minPrice = new BigDecimal("100000");
                BigDecimal maxPrice = BigDecimal.ZERO;
                for (ProductSkuEntity productSkuEntity : productSkuEntityList) {
                    if (productSkuEntity.getSkuSellingPrice().compareTo(minPrice) < 0) {
                        minPrice = productSkuEntity.getSkuSellingPrice();
                    }
                    if (productSkuEntity.getSkuSellingPrice().compareTo(maxPrice) > 0) {
                        maxPrice = productSkuEntity.getSkuSellingPrice();
                    }
                }
                // 先存储goods表
                Goods goods = new Goods();
                goods.setMerchantId(req.getMerchantId());
                goods.setShopId(req.getShopId());
                goods.setSrcType(GoodsSrcTypeEnum.JKMF.getCode());
                goods.setCategoryId(0L);
                goods.setDisplayGoodsName(productEntity.getProductTitle());
                goods.setDisplayPrice(minPrice);
                if (StringUtils.isNotBlank(productEntity.getMainPics())) {
                    List<String> mainPicList = JSONArray.parseArray(productEntity.getMainPics(), String.class);
                    goods.setDisplayBanner(mainPicList.get(0));
                    goods.setPicUrl(mainPicList.get(0));
                }
                // 使用用户自定义主图替换掉
                if (StringUtils.isNotBlank(req.getExtraDataObject().getGoodsMainPic())) {
                    goods.setDisplayBanner(req.getExtraDataObject().getGoodsMainPic());
                    goods.setPicUrl(req.getExtraDataObject().getGoodsMainPic());
                }
                goods.setDescPath(productEntity.getDetailPics());
                goods.setXxbtopNumIid(productEntity.getNumIid());
                goods.setXxbtopPrice(minPrice + "-" + maxPrice);
                goods.setDel(DelEnum.NO.getCode());
                goods.setCtime(new Date());
                goods.setUtime(new Date());
                goods.setOnsale(GoodsOnsaleEnum.ONSALE.getCode());
                goods.setTop(GoodsTopEnum.INIT.getCode());
                goods.setProductId(productEntity.getId());
                goods.setSkuId(req.getExtraDataObject().getSkuId());
                goodsList.add(goods);
            }
            boolean saveSuccess = goodsService.saveBatch(goodsList);
            if (!saveSuccess) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "服务异常，请稍后重试");
            }
            List<GoodsPlus> goodsPlusList = new ArrayList<>();
            String keyJson = "";
            if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
                String keyWord = req.getExtraDataObject().getKeywords();
                if (StringUtils.isNotBlank(keyWord)) {
                    String[] keys = keyWord.split(",");
                    keyJson = JSONArray.toJSONString(Arrays.asList(keys));
                }
            }
            for (Goods goods : goodsList) {
                sb.append(goods.getId()).append(",");
                GoodsPlus goodsPlus = new GoodsPlus();
                goodsPlus.setGoodsId(goods.getId());
                goodsPlus.setMerchantId(goods.getMerchantId());
                goodsPlus.setType(GoodsPlusTypeEnum.KEY_WORDS.getCode());
                goodsPlus.setValue(keyJson);
                goodsPlus.setCtime(new Date());
                goodsPlus.setUtime(new Date());
                goodsPlusList.add(goodsPlus);
            }
            if (!CollectionUtils.isEmpty(goodsPlusList)) {
                goodsPlusService.saveBatch(goodsPlusList);
            }
        } else {
            for (Goods goods : list) {
                goodsIdSet.add(goods.getId());
                sb.append(goods.getId()).append(",");
            }
        }
        req.setGoodsIds(sb.toString().substring(0, sb.length() - 1));
        ActivityDef activityDef = this.activityDefAboutService.addActivityDef(req);
        boolean isSuccess = this.activityDefService.saveOrUpdate(activityDef);
        if (!isSuccess) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "存储活动异常 To @夜辰");
        }
        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }
        // 记录到 activity_task表中
        List<ActivityTask> activityTasks = this.activityDefAboutService.addActivityTasks(activityDef, req);
        boolean success = this.activityTaskService.saveBatch(activityTasks);
        if (!success) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityTaskService.saveError();
        }
        // 处理返回数据
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }

    @Override
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());

        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }
        req.init();
        String keyJson = "";
        if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
            String keyWord = req.getExtraDataObject().getKeywords();
            if (StringUtils.isNotBlank(keyWord)) {
                String[] split = keyWord.split(",");
                keyJson = JSON.toJSONString(Arrays.asList(split));
            }
        }
        if (!StringUtils.equals(activityDef.getKeyWord(), keyJson)) {
            Set<Long> goodsIdSet = new HashSet<>();
            String[] split = req.getGoodsIds().split(",");
            for (String str : split) {
                goodsIdSet.add(Long.valueOf(str));
            }
            if (!CollectionUtils.isEmpty(goodsIdSet)) {
                QueryWrapper<GoodsPlus> queryWrapper = new QueryWrapper<>();
                queryWrapper.lambda()
                        .eq(GoodsPlus::getMerchantId, req.getMerchantId())
                        .in(GoodsPlus::getGoodsId, goodsIdSet);
                GoodsPlus goodsPlus = new GoodsPlus();
                goodsPlus.setValue(keyJson);
                goodsPlus.setUtime(new Date());
                goodsPlusService.update(goodsPlus, queryWrapper);
            }
        }
        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            return this.activityDefUpdateService.activityDefUpdateSave(req, activityDef);
        } else {
            //非草稿修改
            return this.activityDefUpdateService.activityDefUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.FAV_CART_V2;
    }
}
