package mf.code.activity.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.goods.repo.po.Goods;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author: gel
 * @date: 2018-11-07 17:20
 */
public interface ActivityDefService extends IService<ActivityDef> {

    /**
     * 还库存
     *
     * @param activityDefId
     * @param stockNum      之前消费了几个库存
     * @return
     */
    Integer addStock(Long activityDefId, int stockNum);


    /**
     * 扣库存
     *
     * @param activityDefId
     * @param stockNum
     * @param stock
     * @return
     */
    Integer reduceStockAfterCallback(Long activityDefId, int stockNum, Integer stock);

    /**
     * 商户创建活动定义
     *
     * @param activityDef
     * @return
     */
    Integer createActivityDef(ActivityDef activityDef);

    /**
     * 修改活动定义
     *
     * @param activityDef
     * @return
     */
    Integer updateActivityDef(ActivityDef activityDef);

    /**
     * 删除活动定义
     *
     * @param id
     * @return
     */
    Integer deleteActivityDef(Long id);

    /**
     * 按照主键查询活动定义
     *
     * @param id
     * @return
     */
    ActivityDef selectByPrimaryKey(Long id);

    List<ActivityDef>  selectByParentId(Long activityDefId);

    /**
     * 动态条件查询
     *
     * @param params
     * @return
     */
    List<ActivityDef> selectByParams(Map<String, Object> params);

    /**
     * 扣除保证金余额
     *
     * @param id
     * @param amount
     * @param num
     * @return
     */
    Integer reduceDeposit(Long id, BigDecimal amount, Integer num);

    Integer reduceDepositWithoutCheckStock(Long id, BigDecimal amount, Integer num);

    Integer addDepositWithoutCheckStock(Long id, BigDecimal amount, Integer num);

    /**
     * 更新保证金余额
     *
     * @param id
     * @param amount
     * @param num
     * @return
     */
    Integer addDeposit(Long id, BigDecimal amount, Integer num);

    /**
     * 通过参数 查询 数据
     *
     * @param param
     * @return
     */
    List<ActivityDef> listPageByParams(Map<String, Object> param);

    /**
     * 查询符合参数条件的总记录数
     *
     * @param param 查询条件
     * @return Integer 总记录数
     */
    Integer countForPageList(Map<String, Object> param);

    /**
     * 查询符合参数条件的总记录数
     *
     * @param param 查询条件
     * @return Integer 总记录数
     */
    Integer countByParams(Map<String, Object> param);

    /**
     * 挂起
     *
     * @param activityDefId
     * @return
     */
    Integer suspendActivityDef(Long activityDefId);

    /**
     * 增加保证金余额 和 库存余额
     *
     * @param activityDefId
     * @param depositActual
     * @param stockActual
     * @return
     */
    Integer updateDepositDefAndStockDef(Long activityDefId, BigDecimal depositActual, Integer stockActual);


    /**
     * 发放红包任务红包
     *
     * @param id
     * @param amount
     * @return
     */
    Integer reduceRedpackStock(Long id, BigDecimal amount);


    /**
     * 回退红包任务红包
     *
     * @param id
     * @param amount
     * @return
     */
    Integer addRedpackStock(Long id, BigDecimal amount);

    Map statisticsGoods(Long merchantId, Long shopId);

    List<ActivityDef> selectByMerchantIdShopId(Long merchantId, Long shopId);

    /**
     * 如果是新人有礼物 , 也就是助力活动 , 也就是赠品活动
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    List<ActivityDef> selectByMerchantIdShopId4Newbie(Long merchantId, Long shopId);

    /**
     * 获取某个活动定义的所有进行中的活动
     * 获取进行中的免单商品活动：活动状态为：1已发布， 删除标记为：0正常
     * 获取进行中的赠品（助力）活动：同上
     * 获取进行中的红包任务（）：不进入活动表，从user_task表入手
     *
     * @param activityDefId
     * @return
     */
    List<Activity> listOngoindActivity(Long activityDefId);

    ActivityDef findByGoodsId(Long goodsId);

    ActivityDef selectOpenRPDefByShopId(Long shopId);


    List<Goods> selectGoodsByDef(Long shopId, List<Integer> activityType);

    List<Goods> selectGoodsByTask(Long shopId, int activityType);

    Integer addStockAndDeposit(Long activityDefId, BigDecimal deposit, int stock);

    List<ActivityDef> queryByQueryWrapper(Long merchantId, Long shopId, int type);

    List<ActivityDef> queryByQueryWrapper(Long merchantId, Long shopId, List<Integer> types);

    /**
     * 根据父活动id和类型查询活动
     *
     * @param parentId
     * @param type
     * @return
     */
    ActivityDef selectByParentIdAndType(Long parentId, int type);

    /**
     * 根据口令查询扫一扫活动
     *
     * @param keywords
     * @return
     */
    ActivityDef selectByKeywords(String keywords);

    /**
     * 查询最新一条进行中的拆红包活动
     * @param merchantId
     * @param shopId
     * @return
     */
    ActivityDef selectLastOpenRedPackActiyityDef(Long merchantId, Long shopId);

    Integer reduceShopManagerDeposit(Long activityDefId, int stock);
}
