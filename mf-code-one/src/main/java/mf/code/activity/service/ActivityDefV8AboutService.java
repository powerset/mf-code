package mf.code.activity.service;

import mf.code.api.applet.v8.dto.CheckpointTaskSpaceDTO;

/**
 * mf.code.activity.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月21日 16:54
 */
public interface ActivityDefV8AboutService {

    CheckpointTaskSpaceDTO.TaskDTO queryNewbieTaskDetail(Long userId, boolean first);

    CheckpointTaskSpaceDTO.TaskDTO queryShopManagerTaskDetail(Long userId, boolean first);

    CheckpointTaskSpaceDTO.TaskDTO queryDailyTaskDetail(Long merchantId, Long shopId, Long userId, boolean first);
}
