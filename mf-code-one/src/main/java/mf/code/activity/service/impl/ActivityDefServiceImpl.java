package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.dao.ActivityDefMapper;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.ReduceOrAddEnum;
import mf.code.common.email.EmailService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2018-11-07 17:20
 */
@Slf4j
@Service
public class ActivityDefServiceImpl extends ServiceImpl<ActivityDefMapper, ActivityDef> implements ActivityDefService {

    @Autowired
    private ActivityDefMapper activityDefMapper;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private GoodsService goodsService;

    @Override
    public Integer addStock(Long activityDefId, int stockNum) {
        Map<String, Object> map = new HashMap<>(2);
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("回退库存日志开始：回退数量：" + stockNum + ",回退前数据" + JSON.toJSONString(activityDef));
        if (activityDef.getStock() + stockNum > activityDef.getStockDef()) {
            log.error("回退库存异常，当前库存与待回退库存的和超过总库存！");
            emailService.sendSimpleMail("回退库存异常，当前库存与待回退库存的和超过总库存！",
                    "回退数量：" + stockNum + ",回退前数据" + JSON.toJSONString(activityDef));
        }
        map.put("activityDefId", activityDefId);
        map.put("stockNum", stockNum);
        int count = activityDefMapper.addStock(map);
        ActivityDef activityDef1 = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("回退库存日志开始：回退执行条数：" + count + ",回退后数据" + JSON.toJSONString(activityDef1));
        return count;
    }

    /**
     * 扣库存
     *
     * @param activityDefId activityDefId
     * @param stockNum      stockNum
     * @param stock         stock
     * @return Integer
     */
    @Override
    public Integer reduceStockAfterCallback(Long activityDefId, int stockNum, Integer stock) {
        Map<String, Object> map = new HashMap<>(2);
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("扣除库存日志开始：扣除数量：" + stockNum + ",扣除前数据" + JSON.toJSONString(activityDef));
        if (activityDef.getStock() > activityDef.getStockDef()) {
            log.error("扣除库存异常，当前库存超过总库存！");
            emailService.sendSimpleMail("扣除库存异常，当前库存超过总库存！",
                    "扣除数量：" + stockNum + ",扣除前数据" + JSON.toJSONString(activityDef));
        }
        map.put("id", activityDefId);
        map.put("stockNum", stockNum);
        map.put("originalStock", stock);
        int count = activityDefMapper.updateStockAfterCallback(map);
        ActivityDef activityDef1 = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("扣除库存日志开始：扣除执行条数：" + count + ",扣除后数据" + JSON.toJSONString(activityDef1));
        return count;
    }

    /**
     * 商户创建活动定义
     *
     * @param activityDef
     * @return
     */
    @Override
    public Integer createActivityDef(ActivityDef activityDef) {
        if (activityDef == null) {
            return 0;
        }
        Date now = new Date();
        activityDef.setCtime(now);
        activityDef.setUtime(now);
        return activityDefMapper.insertSelective(activityDef);
    }

    /**
     * 审核活动定义
     *
     * @param activityDef
     * @ * @return
     */
    @Override
    public Integer updateActivityDef(ActivityDef activityDef) {
        return activityDefMapper.updateByPrimaryKeySelective(activityDef);
    }

    /**
     * 删除活动定义
     *
     * @param id
     * @return
     */
    @Override
    public Integer deleteActivityDef(Long id) {
        if (id == null) {
            return 0;
        }
        ActivityDef activityDef = new ActivityDef();
        activityDef.setId(id);
        activityDef.setDel(DelEnum.YES.getCode());
        activityDef.setUtime(new Date());
        return activityDefMapper.updateByPrimaryKeySelective(activityDef);
    }

    /**
     * 按照主键查询活动定义
     *
     * @param id
     * @return
     */
    @Override
    public ActivityDef selectByPrimaryKey(Long id) {
        return activityDefMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ActivityDef> selectByParentId(Long activityDefId) {
        QueryWrapper<ActivityDef> activityDefQueryWrapper = new QueryWrapper<>();
        activityDefQueryWrapper.lambda()
                .eq(ActivityDef::getParentId, activityDefId)
        ;
        List<ActivityDef> activityDefs = activityDefMapper.selectList(activityDefQueryWrapper);
        return activityDefs;
    }

    /**
     * 动态条件查询
     *
     * @param params
     * @return
     */
    @Override
    public List<ActivityDef> selectByParams(Map<String, Object> params) {
        return activityDefMapper.selectByParams(params);
    }


    /**
     * 扣除保证金余额
     *
     * @param id
     * @param amount
     * @param num
     * @return
     */
    @Override
    public Integer reduceDeposit(Long id, BigDecimal amount, Integer num) {
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(id);
        log.warn("扣除库存保证金日志开始：扣除数量：" + num + ",扣除金额：" + amount + ",扣除前数据" + JSON.toJSONString(activityDef));
        amount = amount.negate();
        num = num * ReduceOrAddEnum.REDUCE.getCode();
        Integer count = activityDefMapper.updateDeposit(id, amount, num, new Date());
        ActivityDef activityDef1 = activityDefMapper.selectByPrimaryKey(id);
        log.warn("扣除库存保证金日志结束：扣除数量：" + num + ",扣除金额：" + amount + ",扣除后数据" + JSON.toJSONString(activityDef1));
        return count;
    }

    /**
     * 扣除保证金余额 -- 不校验库存数
     *
     * @param id
     * @param amount
     * @param num
     * @return
     */
    @Override
    public Integer reduceDepositWithoutCheckStock(Long id, BigDecimal amount, Integer num) {
        amount = amount.negate();
        num = num * ReduceOrAddEnum.REDUCE.getCode();
        return activityDefMapper.reduceDepositWithoutCheckStock(id, amount, num, new Date());
    }

    /**
     * 增加保证金余额 -- 不校验库存数
     *
     * @param id
     * @param amount
     * @param num
     * @return
     */
    @Override
    public Integer addDepositWithoutCheckStock(Long id, BigDecimal amount, Integer num) {
        return activityDefMapper.addDepositWithoutCheckStock(id, amount, num, new Date());
    }

    /**
     * 更新保证金余额
     *
     * @param id
     * @param amount
     * @return
     */
    @Override
    public Integer addDeposit(Long id, BigDecimal amount, Integer num) {
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(id);
        if (activityDef == null) {
            log.error("未查询到此活动定义, activityDefId = {}", id);
            return 0;
        }
        log.warn("返还库存保证金日志开始：返还数量：" + num + ",返还金额：" + amount + ",返还前数据" + JSON.toJSONString(activityDef));
        if (activityDef.getStock() + num > activityDef.getStockDef() && activityDef.getType() != ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            log.error("返还库存异常，当前库存与待回退库存的和超过总库存！");
            emailService.sendSimpleMail("返还库存异常，当前库存与待回退库存的和超过总库存！",
                    "返还库存数量：" + num + ",返还前数据" + JSON.toJSONString(activityDef));
        }
        if (activityDef.getDeposit().add(amount).compareTo(activityDef.getDepositDef()) > 0 && amount.intValue() != 0) {
            log.error("返还保证金异常，当前保证金与待回退保证金的和超过总保证金！");
            emailService.sendSimpleMail("返还保证金异常，当前保证金与待回退保证金的和超过总保证金！",
                    "返还保证金数量：" + amount + ",返还前数据" + JSON.toJSONString(activityDef));
        }
        Integer count = activityDefMapper.updateDeposit(id, amount, num, new Date());
        ActivityDef activityDef1 = activityDefMapper.selectByPrimaryKey(id);
        log.warn("返还库存保证金日志结束：返还数量：" + num + ",返还金额：" + amount + ",返还后数据" + JSON.toJSONString(activityDef1));
        return count;
    }

    @Override
    public List<ActivityDef> listPageByParams(Map param) {
        return activityDefMapper.listPageByParams(param);
    }

    @Override
    public Integer countForPageList(Map<String, Object> param) {
        return activityDefMapper.countForPageList(param);
    }

    @Override
    public Integer countByParams(Map<String, Object> param) {
        return activityDefMapper.countByParams(param);
    }

    @Override
    public Integer suspendActivityDef(Long activityDefId) {
        if (activityDefId == null) {
            return 0;
        }
        ActivityDef activityDef = new ActivityDef();
        activityDef.setId(activityDefId);
        activityDef.setStatus(ActivityDefStatusEnum.CLOSE.getCode());
        activityDef.setUtime(new Date());
        return activityDefMapper.updateByPrimaryKeySelective(activityDef);
    }

    /**
     * 补库存专用
     *
     * @param activityDefId
     * @param depositActual
     * @param stockActual
     * @return
     */
    @Override
    public Integer updateDepositDefAndStockDef(Long activityDefId, BigDecimal depositActual, Integer stockActual) {
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("补充库存保证金日志开始：更改数量：" + stockActual + ",更改金额：" + depositActual + ",更改前数据" + JSON.toJSONString(activityDef));
        Integer count = activityDefMapper.updateDepositDefAndStockDef(activityDefId, depositActual, stockActual, new Date());
        ActivityDef activityDef1 = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("更改库存保证金日志结束：更改数量：" + stockActual + ",更改金额：" + depositActual + ",更改后数据" + JSON.toJSONString(activityDef1));
        return count;
    }

    /**
     * 发放红包任务红包
     *
     * @param id
     * @param amount
     * @return
     */
    @Override
    public Integer reduceRedpackStock(Long id, BigDecimal amount) {
        BigDecimal realAmount = amount.negate();
        return activityDefMapper.updateRedPackStock(id, ReduceOrAddEnum.REDUCE.getCode(), realAmount, new Date());
    }

    /**
     * 回退红包任务红包
     *
     * @param id
     * @param amount
     * @return
     */
    @Override
    public Integer addRedpackStock(Long id, BigDecimal amount) {
        return activityDefMapper.updateRedPackStock(id, ReduceOrAddEnum.ADD.getCode(), amount, new Date());
    }

    @Override
    public Map statisticsGoods(Long merchantId, Long shopId) {
        Map<String, Object> m = new HashMap<>();
        m.put("merchantId", merchantId);
        m.put("shopId", shopId);
        return goodsService.statisticsGoods(m);
    }

    @Override
    public List<ActivityDef> selectByMerchantIdShopId(Long merchantId, Long shopId) {
        Map<String, Object> m = new HashMap<>();
        m.put("merchantId", merchantId);
        m.put("shopId", shopId);
        return activityDefMapper.selectByMerchantIdShopId(m);
    }

    /**
     * 如果是新人有礼物 , 也就是助力活动 , 也就是赠品活动
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public List<ActivityDef> selectByMerchantIdShopId4Newbie(Long merchantId, Long shopId) {
        Map<String, Object> m = new HashMap<>();
        m.put("merchantId", merchantId);
        m.put("shopId", shopId);
        return activityDefMapper.selectByMerchantIdShopId4Newbie(m);
    }


    @Override
    public List<Activity> listOngoindActivity(Long activityDefId) {
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.lambda()
                .eq(Activity::getActivityDefId, activityDefId)
                .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
                .eq(Activity::getDel, DelEnum.NO.getCode());
        return activityService.list(aWrapper);
    }

    @Override
    public ActivityDef findByGoodsId(Long goodsId) {
        return activityDefMapper.findByGoodsId(goodsId);
    }


    @Override
    public ActivityDef selectOpenRPDefByShopId(Long shopId) {
        return activityDefMapper.selectOpenRPDefByShopId(shopId);
    }

    @Override
    public List<Goods> selectGoodsByDef(Long shopId, List<Integer> activityType) {
        Map<String, Object> m = new HashMap<>();
        m.put("shopId", shopId);
        m.put("typeList", activityType);
        return activityDefMapper.selectGoodsByDef(m);
    }

    @Override
    public List<Goods> selectGoodsByTask(Long shopId, int activityType) {
        Map<String, Object> m = new HashMap<>();
        m.put("shopId", shopId);
        m.put("type", activityType);
        return activityDefMapper.selectGoodsByTask(m);
    }

    @Override
    public Integer addStockAndDeposit(Long activityDefId, BigDecimal deposit, int stock) {
        return activityDefMapper.addStockAndDeposit(activityDefId, deposit, stock, new Date());
    }

    @Override
    public List<ActivityDef> queryByQueryWrapper(Long merchantId, Long shopId, int type) {
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getType, type)
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
        ;
        wrapper.orderByDesc("id");
        List<ActivityDef> activityDefs = this.activityDefMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            return null;
        }
        ActivityDef activityDef = activityDefs.get(0);
        wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getParentId, activityDef.getId())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
        ;
        activityDefs = this.activityDefMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            return null;
        }

        return activityDefs;
    }

    @Override
    public List<ActivityDef> queryByQueryWrapper(Long merchantId, Long shopId, List<Integer> types) {
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .in(ActivityDef::getType, types)
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
        ;
        wrapper.orderByDesc("id");
        List<ActivityDef> activityDefs = this.activityDefMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            return null;
        }
        return activityDefs;
    }

    @Override
    public ActivityDef selectByParentIdAndType(Long parentId, int type) {
        if (parentId == null) {
            return null;
        }
        return baseMapper.selectOne(new QueryWrapper<ActivityDef>()
                .lambda()
                .eq(ActivityDef::getParentId, parentId)
                .eq(ActivityDef::getType, type));
    }

    @Override
    public ActivityDef selectByKeywords(String keywords) {

        return baseMapper.selectOne(new QueryWrapper<ActivityDef>()
                .lambda()
                .eq(ActivityDef::getKeyWord, keywords)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
        );
    }

    @Override
    public ActivityDef selectLastOpenRedPackActiyityDef(Long merchantId, Long shopId) {
        if (merchantId == null || shopId == null) {
            return null;
        }
        ActivityDef activityDef = baseMapper.selectOne(new QueryWrapper<ActivityDef>()
                .lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getType, ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())
                .orderByDesc(ActivityDef::getId));

        return activityDef;
    }

    @Override
    public Integer reduceShopManagerDeposit(Long activityDefId, int stock) {
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("扣除库存保证金日志开始：扣除数量：" + stock + ",扣除前数据" + JSON.toJSONString(activityDef));
        Integer count = activityDefMapper.reduceShopManagerDeposit(activityDefId, stock, new Date());
        ActivityDef activityDef1 = activityDefMapper.selectByPrimaryKey(activityDefId);
        log.warn("扣除库存保证金日志结束：扣除数量：" + stock + ",扣除后数据" + JSON.toJSONString(activityDef1));
        return count;
    }
}
