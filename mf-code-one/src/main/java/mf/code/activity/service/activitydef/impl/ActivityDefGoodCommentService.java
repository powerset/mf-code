package mf.code.activity.service.activitydef.impl;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * mf.code.api.seller.service.createDef.impl
 *
 * @description: 好评晒图活动 好评返现当前不包含商品
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月18日 18:23
 */
@Service
public class ActivityDefGoodCommentService extends AbstractActivityDefService {
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;

    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Merchant merchant = this.merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(req.getShopId());
        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if (req.getExtraDataObject() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少必要参数");
        }
        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        if (!success) {
            return RedisForbidRepeat.NXError();
        }
        // 校验是否有创建活动的资格
        simpleResponse = activityDefCheckService.createAble(req.getMerchantId(), req.getShopId(), req.getType(), null, null, null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        if (req.getActivityDefId() != null && req.getActivityDefId() > 0) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
            org.springframework.util.Assert.isTrue(activityDef != null, "该编号的活动不存在哦！defId=" + req.getActivityDefId());
            req.from(activityDef);
            req.setExtraDataObject(new CreateDefReq.ExtraData());
            req.getExtraDataObject().from(activityDef);
        } else {
            req.init();
        }
        //若此时没有穿库存数
        if (req.getStockDef() == null || req.getStockDef() == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请输入正确的库存");
        }
        //若是拆红包，则初始化
        this.activityDefAboutService.openRedpacketInit(req);
        return this.activityDefAboutService.getDeposit(req);
    }

    @Override
    public SimpleResponse createDef(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ActivityDef activityDef = this.activityDefAboutService.addActivityDef(req);
        boolean isSuccess = this.activityDefService.saveOrUpdate(activityDef);
        if (!isSuccess) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "存储活动异常 To @夜辰");
        }
        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }

        // 记录到 activity_task表中
        if (StringUtils.isNotBlank(req.getGoodsIds()) && !req.getGoodsIds().equals("-1")) {
            List<ActivityTask> activityTasks = this.activityDefAboutService.addActivityTasks(activityDef, req);
            boolean success = this.activityTaskService.saveBatch(activityTasks);
            if (!success) {
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
                return activityTaskService.saveError();
            }
        }
        // 处理返回数据
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }


    @Override
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());

        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }
        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            return this.activityDefUpdateService.activityDefUpdateSave(req, activityDef);
        } else {
            //非草稿修改
            return this.activityDefUpdateService.activityDefUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.GOOD_COMMENT;
    }
}
