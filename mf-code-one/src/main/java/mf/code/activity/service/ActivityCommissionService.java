package mf.code.activity.service;

import mf.code.api.applet.dto.CommissionBO;

import java.math.BigDecimal;

/**
 * mf.code.activity.service
 * Description: 活动佣金操作类，包括佣金税率查询，佣金计算，分配佣金
 *
 * @author gel
 * @date 2019-02-14 11:11
 */
public interface ActivityCommissionService {

    /**
     * 查询佣金税率
     * @return BigDecimal 税率
     */
    BigDecimal getCommissionRate();

    /**
     * 计算佣金
     */
    CommissionBO calculateCommission(Long userId, BigDecimal commissionDef);
}
