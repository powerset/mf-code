package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.dao.*;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.repo.po.ActivityPlanItem;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements ActivityService {
    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivityPlanMapper activityPlanMapper;
    @Autowired
    private ActivityPlanItemMapper activityPlanItemMapper;
    @Autowired
    private ActivityDefMapper activityDefMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityTaskMapper activityTaskMapper;

    @Override
    public boolean saveSelective(Activity activity) {
        return this.activityMapper.insertSelective(activity) != 0;
    }

    /**
     * 商户确认后,新增活动记录
     */
    @Override
    public SimpleResponse addActivity(Map map) {
        Long activityPlanId = Long.parseLong(map.get("id").toString());
        Long merchantId = Long.parseLong(map.get("merchantId").toString());

        Date now = new Date();
        ActivityPlan activityPlan = activityPlanMapper.selectByPrimaryKeyPlus(activityPlanId);

        Map param = new HashMap(2);
        param.put("activityPlanId", activityPlanId);
        param.put("merchantId", merchantId);
        List<ActivityPlanItem> listPlanItem = activityPlanItemMapper.findAllItemByPlanIdPlus(param);
        BigDecimal deposit = new BigDecimal(0);
        if (listPlanItem.size() == 0) {
            return new SimpleResponse(2, "没有有效的计划实例(计划日历)");
        }

        for (int i = 0; i < listPlanItem.size(); i++) {
            //一天 , 一个店铺 , 只刷一个商品
            // 计划实例
            ActivityPlanItem item = listPlanItem.get(i);
            // 单批次中奖人数
            int hitsPersonPerDraw = activityPlan.getHitsPerDraw();
            // 每个关键字--对应人数
            String totalPerKeyJson = item.getTotalPerKeyJson();
            //  k-v为  关键字-单量   的  Map :
            Map<String, Integer> mapKeyNum = new HashMap<>();
            List<Map<String, Object>> list = (List<Map<String, Object>>) JSON.parse(totalPerKeyJson);

            //计算 中奖  总人数 和   关键字
            int totalPerson = 0;
            String[] keysPlatformInsert = new String[list.size()];

            for (int j = 0; j < list.size(); j++) {
                String key = (String) list.get(j).get("key");
                int num = Integer.parseInt(list.get(j).get("total").toString());
                totalPerson = totalPerson + num;
                keysPlatformInsert[j] = key;
                mapKeyNum.put(key, num);
            }

            //计算item保证金
            //计算新增item所需要的保证金
            double temp = totalPerson * Double.parseDouble(item.getGoodsPrice() == null ? "0" : item.getGoodsPrice().toString());
            BigDecimal tempBigDec = new BigDecimal(temp);
            // 计算 保证金
            deposit = deposit.add(tempBigDec);

            //计算 队列
            String[] keysQueue = new String[totalPerson];
            for (Map.Entry<String, Integer> entry : mapKeyNum.entrySet()) {
                String key = entry.getKey();
                int num = entry.getValue();
                for (int m = 0; m < num; m++) {
                    recursion(totalPerson, keysQueue, key);
                }
            }

            // 计算多少批次
            int batchTotal = totalPerson % hitsPersonPerDraw == 0 ? totalPerson / hitsPersonPerDraw : totalPerson / hitsPersonPerDraw + 1;
            //每个批次创建一个activity
            for (int j = 0; j < batchTotal; j++) {
                List<String> keysQueuePerActivity = new ArrayList<>();
                // 计算一个活动的关键字队列
                for (int p = 0; p < hitsPersonPerDraw; p++) {
                    if (totalPerson > 0) {
                        keysQueuePerActivity.add(keysQueue[totalPerson - 1]);
                        totalPerson--;
                    }
                }
                // 计算开始结束时间
                Date[] dates = getDateTimes(item, activityPlan, j);

                //创建一个活动
                Activity activity = newActivity(now,
                        //计划
                        activityPlan,
                        // 计划实例
                        item,
                        //刷单关键字
                        JSON.toJSONString(keysQueuePerActivity),
                        //批次号 : 从1开始
                        Long.valueOf(j) + 1,
                        //开始时间
                        dates[START_DATE_TIME],
                        // 结束时间
                        dates[END_DATE_TIME]);
                // :gbf 优化 : 改批量
                activityMapper.insertSelective(activity);
            }
            ActivityPlan ap = new ActivityPlan();
            ap.setDeposit(deposit);
            ap.setId(activityPlan.getId());
            int updateDeposit = activityPlanMapper.updateByPrimaryKeySelective(ap);
            //: 优化 .
            if (updateDeposit == 0) {
                // 即使保证金计算错误,但是不回滚,依然继续
                log.error("ActivityServiceImpl#addActivity就算保证金后,修改activityPlan表中deposit有误.");
            }
        }
        return new SimpleResponse(0, "success");
    }

    private static final int START_DATE_TIME = 0;
    private static final int END_DATE_TIME = 1;


    /**
     * 计算开始结束时间
     */
    private Date[] getDateTimes(ActivityPlanItem item, ActivityPlan activityPlan, int j) {
        String startTimeEachDayStr = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_START_TIME_PER_DAY);
        //默认九点开始 , 如果无配置redis
        int startTimeEachDayInt = Constants.DEFAULT_START_TIME;
        if (startTimeEachDayStr != null) {
            startTimeEachDayInt = Integer.parseInt(startTimeEachDayStr);
        }
        //        String datetimeStr = item.getDatetimeStr(); 需求变更前
        // 需求变更后 要求: 发布以后再生成活动的具体日期
        //需求变更后: 这里生成的时间并不是最终的时间,只是对每个不同item的时间标记,  在发布的时候, 还会重新生成活动时间
        String datetimeStr = DateUtil.dateToString(DateUtil.addDay(new Date(), item.getDateNum()), "yyyyMMdd");
        String time = "";
        Integer condDrawTime = activityPlan.getCondDrawTime();
        if (startTimeEachDayInt + j > 23) {
            // todo: gbf 这里的异常应在输入时候限制 , 这里不处理
        }

        // 并不是固定一个小时一批
        int hh = startTimeEachDayInt + (j * condDrawTime) / 60;
        int mm = (j * condDrawTime) % 60;
        time = hh >= 10 ? hh + "" + getMm(mm) + "00"
                : "0" + hh + "" + getMm(mm) + "00";

        Date startTime = DateUtil.stringtoDate(datetimeStr + time, DateUtil.FORMAT_TOKEN);

        if ((condDrawTime / 60) < 1) {
            time = startTimeEachDayInt + j > startTimeEachDayInt ? (startTimeEachDayInt + j) + "" : "09";
            time = time + condDrawTime + "00";
        } else {
            time = (startTimeEachDayInt + j) + (condDrawTime / 60) + "" + (condDrawTime % 60) + "00";
        }

        hh = startTimeEachDayInt + ((j + 1) * condDrawTime) / 60;
        mm = ((j + 1) * condDrawTime) % 60;

        time = hh >= 10 ? hh + "" + getMm(mm) + "00" : "0" + hh + "" + getMm(mm) + "00";


        Date endTime = DateUtil.stringtoDate(datetimeStr + "" + time, DateUtil.FORMAT_TOKEN);
        Date[] dates = new Date[2];
        dates[0] = startTime;
        dates[1] = endTime;
        return dates;
    }

    String getMm(Integer mm) {
        if (mm < 10) {
            return 0 + "" + mm;
        } else {
            return mm + "";
        }
    }

    /**
     * 组装Activity实体
     */
    private Activity newActivity(Date now, ActivityPlan plan, ActivityPlanItem item,
                                 String keyWordJson, Long batchNum,
                                 Date startTime, Date endTime
    ) {
        Activity activity = new Activity();
        activity.setMerchantId(plan.getMerchantId());
        activity.setShopId(plan.getShopId());
        activity.setActivityPlanId(plan.getId());
        activity.setHitsPerDraw(plan.getHitsPerDraw());
        activity.setCondPersionCount(plan.getCondPersionCount());
        activity.setMerchantCouponJson(plan.getMerchantCouponJson());
        activity.setType(1);
        activity.setStatus(0);
        activity.setMissionNeedTime(plan.getMissionNeedTime());
        activity.setCtime(now);
        activity.setUtime(now);
        activity.setBatchNum(batchNum);
        activity.setActivityItemId(item.getId());
        activity.setGoodsId(item.getGoodsId());

        JSONArray array = JSON.parseArray(plan.getAmountJson());

        activity.setPayPrice(new BigDecimal(array.get(0) == null ? "-1" : array.get(0).toString()));

        activity.setKeyWordsJson(keyWordJson);
        activity.setBatchNum(batchNum);
        activity.setStartTime(startTime);
        activity.setEndTime(endTime);
        //        activity.setAwardStartTime(awardTime);// 前段不需要
        return activity;
    }

    /**
     * 递归 随机数生成关键字队列
     */
    private void recursion(int totalPerson, String[] keysQueue, String key) {
        int ran = new Random().nextInt(totalPerson);
        if (keysQueue[ran] == null) {
            keysQueue[ran] = key;
        } else {
            recursion(totalPerson, keysQueue, key);
        }
    }


    @Override
    public int add(Activity record) {
        return activityMapper.insertSelective(record);
    }

    @Override
    public int del(Activity record) {
        return 0;
    }

    @Override
    public int update(Activity record) {
        return activityMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Activity findById(Long id) {
        return this.activityMapper.selectByPrimaryKey(id);
    }


    @Override
    public List<Activity> findPage(Map<String, Object> params) {
        return this.activityMapper.listPageByParams(params);
    }

    @Override
    public int countActivity(Map<String, Object> params) {
        return this.activityMapper.countActivity(params);
    }

    @Override
    public List<Activity> listActivityByStatus(int status) {
        return this.activityMapper.listActivityByStatus(status);
    }

    @Override
    public int updateActivityStatus(Long aid, int status) {
        Activity activity = activityMapper.selectByPrimaryKey(aid);
        Long activityDefId = activity.getActivityDefId();
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        Integer condDrawTime = activityDef.getCondDrawTime();

        long nowLong = System.currentTimeMillis();
        Date nowDate = new Date(nowLong);
        Date endDate = new Date(nowLong + condDrawTime * 60 * 1000);

        //计算开始结束时间
        activity.setStatus(status);
        activity.setUtime(nowDate);
        activity.setStartTime(nowDate);
        activity.setEndTime(endDate);

        return activityMapper.updateByPrimaryKeySelective(activity);
    }

    @Override
    public List<Activity> listActivityByStatusAndAwardStartTimeINN(int status) {
        return activityMapper.listActivityByStatusAndAwardStartTimeINN(status);
    }

    @Override
    public int countByShopGoodsForDel(Long shopId, Long goodsId) {
        Map<String, Object> params = new HashMap<>(4);
        List<Integer> activityStatuses = new ArrayList<>(2);
        activityStatuses.add(ActivityStatusEnum.PUBLISHED.getCode());
        activityStatuses.add(ActivityStatusEnum.UNPUBLISHED.getCode());
        params.put("activityStatuses", activityStatuses);
        params.put("del", DelEnum.NO.getCode());
        if (shopId != null) {
            params.put("shopId", shopId);
        }
        if (goodsId != null) {
            params.put("goodsId", goodsId);
        }
        return activityMapper.countActivity(params);
    }

    /***
     * 表内注解：0待发布 1已发布 -1抽奖未成功 -2强制下线 2已结束',
     * @param activityStatus
     * @param activityStartTime
     * @param activityAwardTime
     * @param activityEndTime
     * @return -1 已结束 1待开始 2待开奖 3已开奖 另外不做小程序展现的状态：0:待发布 -2：强制下线
     */
    @Override
    public int getActivityStatus(int activityStatus, Date activityStartTime, Date activityAwardTime, Date activityEndTime) {
        Integer respStatus = null;//默认状态
        if (activityStatus == -1) {//-1抽奖未成功
            respStatus = -1;//已结束
        } else if (activityStatus == 2) {
            if (activityAwardTime != null) {
                respStatus = 3;//已开奖
            } else {
                respStatus = 2;
            }
        } else if (activityStatus == 1) {
            Date startTime = DateUtils.addSeconds(activityStartTime, -5);
            //待开始 //防止跳转太快
            boolean willStart = activityStatus == 1 && startTime.getTime() > System.currentTimeMillis();
            //待开奖
            boolean willAward = activityStatus == 1 && System.currentTimeMillis() > startTime.getTime()
                    && activityEndTime.getTime() > System.currentTimeMillis() && activityAwardTime == null;
            //已开奖
            boolean awarded = activityStatus == 1 && System.currentTimeMillis() > startTime.getTime() && activityAwardTime != null;
            //抽奖未成功(已结束)
            boolean end = activityStatus == 1 && System.currentTimeMillis() > activityEndTime.getTime() && activityAwardTime == null;
            if (willStart) {
                respStatus = 1;
            } else if (willAward) {
                respStatus = 2;
            } else if (awarded) {
                respStatus = 3;
            } else if (end) {
                respStatus = -1;
            } else {
                respStatus = 1;
            }
        } else {
            respStatus = activityStatus;
        }
        if (respStatus == null) {
            respStatus = 0;
        }
        return respStatus;
    }

    /***
     * 获取红包状态
     * @param activity
     * @return
     */
    @Override
    public int getRedPackStatus(Activity activity, Long userID) {
        Map<String, Object> userTaskParams = new HashMap<String, Object>();
        userTaskParams.put("merchantId", activity.getMerchantId());
        userTaskParams.put("shopId", activity.getShopId());
        userTaskParams.put("userId", userID);
        userTaskParams.put("activityId", activity.getId());
        userTaskParams.put("type", ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode());
        List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
        if (userTasks != null && userTasks.size() > 0) {
            return this.userTaskService.getUserTaskStatus(userTasks.get(0));
        }
        return 0;
    }

    /***
     * 活动结束/没开成团 的activityid,根据goodsid和status=1的，查询到就更新zset的goodsid对应的时间,如果没有就删除该goodsid
     * @param activityID 结束的活动编号id
     * @return 1
     */
    @Override
    public int checkActivityEnd2Zset(Long activityID) {
        Activity activity = this.activityMapper.selectByPrimaryKey(activityID);
        Map<String, Object> activityParams = new HashMap<String, Object>();
        activityParams.put("merchantId", activity.getMerchantId());
        activityParams.put("shopId", activity.getShopId());
        activityParams.put("goodsId", activity.getGoodsId());
        activityParams.put("types", Arrays.asList(2, 4));
        activityParams.put("status", 1);
        activityParams.put("order", "end_time");
        activityParams.put("direct", "asc");
        List<Activity> activities = this.activityMapper.listPageByParams(activityParams);
        if (activities == null || activities.size() == 0) {//不存在，则进行删除key
            this.stringRedisTemplate.opsForZSet().remove(
                    mf.code.uactivity.repo.redis.RedisKeyConstant.GOODS_ID_RANKING_LIST + activity.getShopId(), activity.getGoodsId().toString());
        } else {
            //更新
            this.stringRedisTemplate.opsForZSet().add(
                    mf.code.uactivity.repo.redis.RedisKeyConstant.GOODS_ID_RANKING_LIST + activity.getShopId(),
                    activity.getGoodsId().toString(), activities.get(0).getEndTime().getTime());
        }
        return 1;
    }

    @Override
    public SimpleResponse createActivityFromDef(Long userId, String orderId, Long activityDefId, Integer status) {
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse = checkPayForActivity(activityDefId, userId);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        // 妥协处理
        if (simpleResponse.getData() != null) {
            if (simpleResponse.getData() instanceof Activity) {
                return simpleResponse;
            }
        }
        // 创建activity
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        Activity activity = copyToActivity(userId, orderId, activityDef, status);
        int size = this.add(activity);
        if (size == 0) {
            simpleResponse.setCode(1);
            simpleResponse.setMessage("更新错误");
            return simpleResponse;
        }
        simpleResponse.setData(activity);
        return simpleResponse;
    }

    /**
     * 校验是否能创建支付
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse checkPayForActivity(Long activityDefId, Long userId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 20181213 添加机器人的校验，如果grant_status为-1则说明是机器人，不做次数校验
        User user = userService.selectByPrimaryKey(userId);
        if (user.getGrantStatus() == -1) {
            return simpleResponse;
        }
        ActivityDef activityDef = activityDefMapper.selectByPrimaryKey(activityDefId);
        if (activityDef == null) {
            simpleResponse.setCode(1);
            simpleResponse.setMessage("数据错误");
            return simpleResponse;
        }
        // 新人有礼活动，库存必须大于1
        if (activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode()) && activityDef.getStock() <= 1) {
            simpleResponse.setCode(13);
            simpleResponse.setMessage("库存不足");
            return simpleResponse;
        }
        if (activityDef.getStock() <= 0 || activityDef.getStock() < activityDef.getHitsPerDraw()) {
            simpleResponse.setCode(13);
            simpleResponse.setMessage("库存不足");
            return simpleResponse;
        }
        Map<String, Object> params = new HashMap<>(8);
        params.put("activityDefId", activityDefId);
        params.put("merchantId", activityDef.getMerchantId());
        params.put("shopId", activityDef.getShopId());
        params.put("userId", userId);
        params.put("del", DelEnum.NO.getCode());
        // 判断同一用户同一活动定义，总共只能发起三次
        // 如果存在未发布的活动，则返回给上层接口
        boolean orderBack = activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode()) ||
                activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
        if (orderBack) {
            List<Activity> list = activityMapper.listPageByParams(params);
            int size = 0;
            if (!CollectionUtils.isEmpty(list)) {
                for (Activity activity : list) {
                    // 如果查到 免单任务，有待发布活动，直接返回该活动
                    if (activity.getStatus() == ActivityStatusEnum.UNPUBLISHED.getCode()) {
                        simpleResponse.setData(activity);
                        return simpleResponse;
                    }
                    if (activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()
                            && activity.getEndTime() != null
                            && activity.getEndTime().getTime() <= DateUtil.getCurrentMillis()) {
                        continue;
                    }
                    if (activity.getAwardStartTime() != null && activity.getEndTime().getTime() > 0) {
                        continue;
                    }
                    // 此方法用于创建，所以判断有初始数据则直接返回，否则报错
                    if (activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
                        simpleResponse.setCode(12);
                        simpleResponse.setMessage("还有未结束活动");
                        simpleResponse.setData(activity);
                        return simpleResponse;
                    }
                    size++;
                }
            }
            if (size >= 3) {
                simpleResponse.setCode(11);
                simpleResponse.setMessage("活动发起次数已满");
                return simpleResponse;
            }
        } else if (activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode()) || activityDef.getType().equals(ActivityDefTypeEnum.ASSIST.getCode())) {
            // 一个店铺可能存在多个新人有礼活动，但是需要校验，用户是否完成了一个
            List<Activity> list = activityMapper.listPageByParams(params);
            if (!CollectionUtils.isEmpty(list)) {
                List<Long> activityIds = new ArrayList<>();
                for (Activity activity : list) {
                    // 如果查到 有待发布活动，直接返回该活动
                    if (activity.getStatus() == ActivityStatusEnum.UNPUBLISHED.getCode()) {
                        simpleResponse.setData(activity);
                        return simpleResponse;
                    }
                    // 此方法用于创建，所以判断有初始数据则直接返回，否则报错
                    if (activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
                        simpleResponse.setCode(12);
                        simpleResponse.setMessage("还有未结束活动");
                        simpleResponse.setData(activity);
                        return simpleResponse;
                    }
                    activityIds.add(activity.getId());
                }
                Map<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("userId", userId);
                paramsMap.put("type", UserTaskTypeEnum.NEW_MAN_START.getCode());
                paramsMap.put("merchantId", activityDef.getMerchantId());
                paramsMap.put("shop_id", activityDef.getShopId());
                paramsMap.put("activityIds", activityIds);
                paramsMap.put("status", UserTaskStatusEnum.AWARD_SUCCESS.getCode());
                int countUserTask = userTaskService.countUserTask(paramsMap);
                if (countUserTask != 0) {
                    simpleResponse.setCode(11);
                    simpleResponse.setMessage("活动发起次数已满");
                    return simpleResponse;
                }
            }
        } else {
            simpleResponse.setCode(100);
            simpleResponse.setMessage("活动类型错误");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 更新金额
     *
     * @param id
     * @param amount
     * @return
     */
    @Override
    public Integer updateDeposit(Long id, BigDecimal amount) {
        return activityMapper.updateDeposit(id, amount, new Date());
    }

    @Override
    public int updateStatus(Long aid, int status) {
        Activity activity = new Activity();
        activity.setId(aid);
        activity.setStatus(status);
        return activityMapper.updateByPrimaryKeySelective(activity);
    }

    @Override
    public List<Activity> findByPlanId(Long planId) {
        return activityMapper.findByPlanId(planId);
    }

    @Override
    public Activity findByPlanIdLimit(Long activityPlanId) {
        return activityMapper.findItemByPlanIdAndDayNumLimit(activityPlanId);
    }

    @Override
    public int updateByActivityDefIdStatus(Long activityDefId, int status) {
        return activityMapper.updateByActivityDefIdStatus(activityDefId, status, new Date());
    }

    @Override
    public int deleteActivityByActivityDefId(Long activityDefId) {
        return activityMapper.deleteActivityByActivityDefId(activityDefId);
    }

    @Override
    public List<Activity> getActivityArrBySku(Map param) {
        return activityMapper.getActivityArrBySku(param);
    }

    @Override
    public List<Map> findByGoodsId(Long goodsId) {
        Map map = new HashMap();
        map.put("goodsId", goodsId);

        return activityMapper.findByGoodsId(map);
    }

    @Override
    public Long countGoodsStock(Long activityDefId) {
        return activityMapper.countGoodsStock(activityDefId);
    }


    @Override
    public List<Map> findByDefId(Long activityDefId) {
        return activityMapper.findByDefId(activityDefId);
    }

    @Override
    public List<Activity> findByDefIdInActivity(long defId) {
        return activityMapper.findByDefIdInActivity(defId);
    }

    @Override
    public List<Activity> selectByMerchantIdShopId(Long merchantId, Long shopId) {
        Map m = new HashMap();
        m.put("merchantId", merchantId);
        m.put("shopId", shopId);

        return activityMapper.selectByMerchantIdShopId(m);
    }

    @Override
    public List<Activity> findByDefId4HomePage(Long activityDefId) {
        return activityMapper.findByDefId4HomePage(activityDefId);
    }

    @Override
    public Activity findByDefIdUserId(String defId, Long userId) {

        Map m = new HashMap();
        m.put("defId", defId);
        m.put("userId", userId);
        return activityMapper.findByDefIdUserId(m);
    }

    @Override
    public boolean checkExpired(Activity activity, Long userId, Long activityDefId) {
        if (activity != null) {
            Integer status = activity.getStatus();
            return status == ActivityStatusEnum.FAILURE.getCode()
                    && activity.getDel() == DelEnum.NO.getCode();
        }
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getUserId, userId)
                .eq(Activity::getActivityDefId, activityDefId);
        wrapper.orderByDesc("id");
        List<Activity> activityList = this.list(wrapper);
        for (Activity act : activityList) {
            if (act.getStatus() == ActivityStatusEnum.FAILURE.getCode()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAwarded(Activity activity) {
        return activity.getAwardStartTime() != null;
    }

    @Override
    public Activity saveFromActivityDef(Long userId, ActivityDef activityDef) {
        Activity activity = new Activity();
        activity.setMerchantId(activityDef.getMerchantId());
        activity.setShopId(activityDef.getShopId());
        activity.setActivityDefId(activityDef.getId());
        activity.setUserId(userId);
        if (activityDef.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
            BigDecimal commission = activityDef.getCommission();
            BigDecimal goodsPrice = activityDef.getGoodsPrice();
            BigDecimal payPrice = commission.compareTo(BigDecimal.ZERO) == 0 ? goodsPrice : commission;
            activity.setPayPrice(payPrice);
        }
        activity.setType(this.activityDefType2ActivityType(activityDef.getType()));
        activity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
        Date now = new Date();
        activity.setStartTime(now);
        activity.setEndTime(new Date(now.getTime() + activityDef.getCondDrawTime() * 60000L));
        activity.setGoodsId(activityDef.getGoodsId());
        activity.setHitsPerDraw(activityDef.getHitsPerDraw());
        activity.setCondPersionCount(activityDef.getCondPersionCount());
        activity.setMissionNeedTime(activityDef.getMissionNeedTime());
        activity.setMerchantCouponJson(activityDef.getMerchantCouponJson());
        activity.setKeyWordsJson(activityDef.getKeyWord());
        activity.setDel(DelEnum.NO.getCode());
        activity.setVersion(0);
        activity.setUtime(now);
        activity.setCtime(now);
        boolean save = this.save(activity);
        if (!save) {
            return null;
        }
        return activity;
    }

    @Override
    public Integer activityDefType2ActivityType(Integer activityDefType) {
        if (activityDefType.equals(ActivityDefTypeEnum.MCH_PLAN.getCode())) {
            return ActivityTypeEnum.MCH_PLAN.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.ORDER_BACK.getCode())) {
            return ActivityTypeEnum.ORDER_BACK_START.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())) {
            return ActivityTypeEnum.ORDER_BACK_START_V2.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.NEW_MAN.getCode())) {
            return ActivityTypeEnum.NEW_MAN_START.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
            return ActivityTypeEnum.LUCK_WHEEL.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())) {
            return ActivityTypeEnum.ORDER_REDPACK.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.ASSIST.getCode())) {
            return ActivityTypeEnum.ASSIST.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.ASSIST_V2.getCode())) {
            return ActivityTypeEnum.ASSIST_V2.getCode();
        }
        if (activityDefType.equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
            return ActivityTypeEnum.OPEN_RED_PACKET_START.getCode();
        }
        return null;
    }

    @Override
    public boolean checkValidity(Activity activity) {
        // 活动是否已开奖
        boolean awarded = this.checkAwarded(activity);
        if (awarded) {
            return false;
        }
        // 活动是否已过期
        boolean expired = this.checkExpired(activity, null, null);
        if (expired) {
            return false;
        }
        return true;
    }

    @Override
    public boolean updateAwardStartTime(Activity activity) {
        Date now = new Date();
        activity.setAwardStartTime(now);
        activity.setUtime(now);
        return this.updateById(activity);
    }

    @Override
    public Activity findByDefId4Summary(Long id) {
        return activityMapper.findByDefId4Summary(id);
    }

    /**
     * 拆红包的 待返现/已返现.
     * hasPay==1 已返现
     * hasPay==0 待返现
     * @param param
     * @return
     */
    @Override
    public BigDecimal calcOpenRedPacketMoney(Map<String, Object> param) {
        return activityMapper.calcOpenRedPacketMoney(param);
    }

    /**
     * 根据条件汇总金额
     *
     * @param param
     * @return
     */
    @Override
    public BigDecimal sumByParams(Map<String, Object> param) {
        return activityMapper.sumByParams(param);
    }

    /**
     * 组装activity数据
     *
     * @param userId
     * @param orderId
     * @param activityDef
     * @return
     */
    private Activity copyToActivity(Long userId, String orderId, ActivityDef activityDef, Integer status) {
        Activity activity = new Activity();
        activity.setMerchantId(activityDef.getMerchantId());
        activity.setShopId(activityDef.getShopId());
        activity.setActivityDefId(activityDef.getId());
        activity.setUserId(userId);
        activity.setPayPrice(activityDef.getGoodsPrice());
        activity.setOrderId(orderId);
        ActivityDefTypeEnum activityDefTypeEnum = ActivityDefTypeEnum.findByCode(activityDef.getType());
        ActivityTypeEnum activityTypeEnum = getActivityTypeEnum(activityDefTypeEnum);
        activity.setType(activityTypeEnum.getCode());
        activity.setStatus(status);
        activity.setGoodsId(activityDef.getGoodsId());
        activity.setHitsPerDraw(activityDef.getHitsPerDraw());
        activity.setCondPersionCount(activityDef.getCondPersionCount());
        activity.setMissionNeedTime(activityDef.getMissionNeedTime());
        activity.setMerchantCouponJson(activityDef.getMerchantCouponJson());
        // 关键词暂定一个，所以直接存储
        activity.setKeyWordsJson(activityDef.getKeyWord());
        activity.setDel(DelEnum.NO.getCode());
        Date now = new Date();
        activity.setCtime(now);
        activity.setUtime(now);
        activity.setWeighting(activityDef.getWeighting());
        return activity;
    }

    private ActivityTypeEnum getActivityTypeEnum(ActivityDefTypeEnum activityDefTypeEnum) {
        ActivityTypeEnum activityTypeEnum;
        switch (activityDefTypeEnum) {
            case ORDER_BACK:
                activityTypeEnum = ActivityTypeEnum.ORDER_BACK_START;
                break;
            case NEW_MAN:
                activityTypeEnum = ActivityTypeEnum.NEW_MAN_START;
                break;
            case LUCKY_WHEEL:
                activityTypeEnum = ActivityTypeEnum.LUCK_WHEEL;
                break;
            case FILL_BACK_ORDER:
                activityTypeEnum = ActivityTypeEnum.ORDER_REDPACK;
                break;
            case ASSIST:
            case GIFT:
                activityTypeEnum = ActivityTypeEnum.ASSIST;
                break;
            case OPEN_RED_PACKET:
                activityTypeEnum = ActivityTypeEnum.OPEN_RED_PACKET_START;
                break;
            case ORDER_BACK_V2:
                activityTypeEnum = ActivityTypeEnum.ORDER_BACK_START_V2;
                break;
            case ASSIST_V2:
                activityTypeEnum = ActivityTypeEnum.ASSIST_V2;
                break;
            default:
                activityTypeEnum = ActivityTypeEnum.MCH_PLAN;
                break;
        }
        return activityTypeEnum;
    }
}
