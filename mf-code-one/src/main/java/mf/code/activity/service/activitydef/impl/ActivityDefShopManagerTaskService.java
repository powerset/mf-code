package mf.code.activity.service.activitydef.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.feignclient.CommAppService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.activity.service.activitydef.impl
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-19 14:44
 */
@Service
public class ActivityDefShopManagerTaskService extends AbstractActivityDefService {
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private CommAppService commAppService;


    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        Merchant merchant = merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(req.getShopId());
        CommonDictDTO commonDictDTO = commAppService.selectByTypeKey("shopManager", "salePolicy");
        BigDecimal salePrice = BigDecimal.ZERO;
        if (commonDictDTO != null && StringUtils.isNotBlank(commonDictDTO.getValue())) {
            String value = commonDictDTO.getValue();
            JSONObject jsonObject = JSON.parseObject(value);
            salePrice = jsonObject.getBigDecimal("salePrice");
        }


        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if (req.getExtraDataObject() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少必要参数");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getSetWechatAmount())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少设置微信群任务佣金");
        }

        if (req.getExtraDataObject().getInviteCount() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少邀请下级粉丝人数");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getInviteAmount())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少完成邀请下级粉丝任务佣金");
        }

        if (req.getExtraDataObject().getDevelopCount() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少发展粉丝成为店长人数");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getDevelopAmount())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少完成发展粉丝为店长任务佣金");
        }

        if (req.getExtraDataObject().getSetWechatAmount() != null) {
            Assert.isNumber(req.getExtraDataObject().getSetWechatAmount(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请输入正确的设置微信群任务金额");
        }

        if (req.getExtraDataObject().getInviteAmount() != null) {
            Assert.isNumber(req.getExtraDataObject().getInviteAmount(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请输入正确的邀请下级粉丝任务金额");
        }

        if (req.getExtraDataObject().getDevelopAmount() != null) {
            Assert.isNumber(req.getExtraDataObject().getDevelopAmount(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请输入正确的发展粉丝为店长任务金额");
        }

        // 校验店长人数
        if (req.getStockDef().intValue() < 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，店长人数数必须≥1");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getSetWechatAmount()) || new BigDecimal(req.getExtraDataObject().getSetWechatAmount()).floatValue() < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，设置微信群任务不能小于0");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getInviteAmount()) || new BigDecimal(req.getExtraDataObject().getInviteAmount()).floatValue() < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，邀请下级粉丝任务不能小于0");
        }

        if (salePrice.compareTo(BigDecimal.ZERO) == 0) {
            if (StringUtils.isBlank(req.getExtraDataObject().getDevelopAmount()) || new BigDecimal(req.getExtraDataObject().getDevelopAmount()).floatValue() < 0) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，发展粉丝为店长任务不能小于0");
            }
        }

        // redis防重拦截
        String key = new StringBuilder(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT).append(req.getMerchantId()).toString();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, key);
        if (!success) {
            return RedisForbidRepeat.NXError();
        }

        // 校验是否有创建活动的资格
        SimpleResponse simpleResponse = activityDefCheckService.createAble(req.getMerchantId(),
                req.getShopId(), req.getType(), null, null, null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(key);
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse createDef(CreateDefReq req) {
        // 拆分活动
        ActivityDef mainActivityDef = new ActivityDef();
        ActivityDef setWechatActivityDef = new ActivityDef();
        ActivityDef inviteActivityDef = new ActivityDef();
        ActivityDef developActivityDef = new ActivityDef();

        boolean result = activityDefAboutService.splitShopManagerTaskDefReq(req, mainActivityDef,
                setWechatActivityDef, inviteActivityDef, developActivityDef);
        if (!result) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "活动拆分异常，@to大妖怪");
        }

        activityDefService.createActivityDef(mainActivityDef);

        // 更新主记录的parentId为当前id
        mainActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.updateActivityDef(mainActivityDef);

        setWechatActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.createActivityDef(setWechatActivityDef);

        inviteActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.createActivityDef(inviteActivityDef);

        developActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.createActivityDef(developActivityDef);

        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(mainActivityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }

        Map<String, Object> map = new HashMap<>(1);
        map.put("activityDefId", mainActivityDef.getId());
        SimpleResponse response = new SimpleResponse();
        response.setData(map);
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());

        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }

        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            return this.activityDefUpdateService.activityDefShopManagerUpdateSave(req, activityDef);
        } else {
            //非草稿修改,
            return this.activityDefUpdateService.activityDefShopManagerUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.SHOP_MANAGER_TASK;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (req.getActivityDefId() == null) {
            if (req == null || req.getExtraDataObject() == null || StringUtils.isBlank(req.getExtraDataObject().getInviteAmount())
                    || StringUtils.isBlank(req.getExtraDataObject().getDevelopAmount())
                    || req.getStockDef() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数，@to木子");
            }
            BigDecimal deposit = activityDefAboutService.calcShopManagerTastDeposit(req.getExtraDataObject().getSetWechatAmount(),
                    req.getExtraDataObject().getInviteAmount(), req.getExtraDataObject().getDevelopAmount(),
                    req.getExtraDataObject().getDevelopTaskTimes(), req.getStockDef());

            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("depositDef", deposit);
            simpleResponse.setData(resultVO);
        } else {
            if (req == null || req.getStockDef() == null || req.getStockDef() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数，@to木子");
            }

            // 查询主活动
            ActivityDef mainActivityDef = activityDefService.selectByPrimaryKey(req.getActivityDefId());
            if (mainActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动不存在");
            }

            // 查询设置微信群子活动
            ActivityDef setWechatAcitvityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
            if (setWechatAcitvityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "设置微信群子活动不存在");
            }

            // 查询邀请粉丝任务子活动
            ActivityDef inviteAcitvityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
            if (inviteAcitvityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "邀请粉丝任务子活动不存在");
            }

            // 查询发展粉丝为店长活动
            ActivityDef developAcitvityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
            if (developAcitvityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "发展粉丝为店长活动不存在");
            }

            BigDecimal deposit = activityDefAboutService.calcShopManagerTastDeposit(setWechatAcitvityDef.getCommission().toString(),
                    inviteAcitvityDef.getCommission().toString(), developAcitvityDef.getCommission().toString(),
                    developAcitvityDef.getCondPersionCount(), req.getStockDef());

            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("depositDef", deposit);
            simpleResponse.setData(resultVO);
        }
        return simpleResponse;
    }
}
