package mf.code.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.api.seller.dto.SupplyStockReq;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-17 下午2:33
 */
public interface ActivityDefAuditService extends IService<ActivityDefAudit> {
    /**
     * 查询满足 条件参数 的 总记录数
     *
     * @param param 查询条件
     * @return 总记录数
     */
    Integer countByParam(Map<String, Object> param);

    /**
     * 查询满足 条件参数 的 记录 ---可分页查询
     *
     * @param param 查询条件
     * @return list数据
     */
    List<ActivityDefAudit> listPageByParams(Map<String, Object> param);

    ActivityDefAudit selectByPrimaryKey(Long id);

    Integer updateByPrimaryKeySelective(ActivityDefAudit activityDefAudit);

    Integer deleteActivityDefAudit(Long id);

    Integer insertSelective(ActivityDefAudit activityDefAudit);

    /**
     * 提交（支付） 定义活动时，在审核表中记录一条数据
     *
     * @param activityDef
     * @return
     */
    ActivityDefAudit saveOrUpdateAudit4ActivityDef(ActivityDef activityDef);

    ActivityDefAudit refundMoneyAddAudit4ActivityDef(ActivityDefAudit activityDefAudit, BigDecimal unitPrice, BigDecimal refundMoney, int totalStock);

    SimpleResponse<Object> saveError();

    SimpleResponse<Object> listPageRecord4ActivityDefAudit(long current, long size, Long activityDefId);

    ActivityDefAudit supplyStock(SupplyStockReq supplyStockReq);

    boolean paySupplyStock(Long activityDefAuditId);
}
