package mf.code.activity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefAuditTypeEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.dao.ActivityDefAuditMapper;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.activity.utils.ActivityVO;
import mf.code.api.seller.dto.SupplyStockReq;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-17 下午2:34
 */
@Slf4j
@Service
public class ActivityDefAuditServiceImpl extends ServiceImpl<ActivityDefAuditMapper, ActivityDefAudit> implements ActivityDefAuditService {
    private final ActivityDefAuditMapper activityDefAuditMapper;
    private final ActivityDefService activityDefService;
    private final ActivityTaskService activityTaskService;
    private final ActivityService activityService;

    @Autowired
    public ActivityDefAuditServiceImpl(ActivityDefAuditMapper activityDefAuditMapper, ActivityDefService activityDefService, ActivityTaskService activityTaskService, ActivityService activityService) {
        this.activityDefAuditMapper = activityDefAuditMapper;
        this.activityDefService = activityDefService;
        this.activityTaskService = activityTaskService;
        this.activityService = activityService;
    }

    @Override
    public Integer countByParam(Map<String, Object> param) {
        return activityDefAuditMapper.countByParam(param);
    }

    @Override
    public List<ActivityDefAudit> listPageByParams(Map<String, Object> param) {
        return activityDefAuditMapper.listPageByParam(param);
    }

    @Override
    public ActivityDefAudit selectByPrimaryKey(Long id) {
        return activityDefAuditMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer updateByPrimaryKeySelective(ActivityDefAudit activityDefAudit) {
        return activityDefAuditMapper.updateByPrimaryKeySelective(activityDefAudit);
    }

    @Override
    public Integer deleteActivityDefAudit(Long id) {
        if (id == null) {
            return 0;
        }
        ActivityDefAudit activityDefAudit = new ActivityDefAudit();
        activityDefAudit.setId(id);
        activityDefAudit.setDel(DelEnum.YES.getCode());
        activityDefAudit.setUtime(new Date());
        return activityDefAuditMapper.updateByPrimaryKeySelective(activityDefAudit);
    }

    @Override
    public Integer insertSelective(ActivityDefAudit activityDefAudit) {
        return activityDefAuditMapper.insertSelective(activityDefAudit);
    }

    @Override
    public ActivityDefAudit saveOrUpdateAudit4ActivityDef(ActivityDef activityDef) {

        Date now = new Date();
        QueryWrapper<ActivityDefAudit> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDefAudit::getActivityDefId, activityDef.getId());
        ActivityDefAudit activityDefAudit = this.getOne(wrapper);
        if (null == activityDefAudit) {
            activityDefAudit = new ActivityDefAudit();
        }
        activityDefAudit.setGoodsIds(activityDef.getGoodsIds());
        activityDefAudit.setGoodsId(activityDef.getGoodsId());
        activityDefAudit.setStockApply(activityDef.getStockDef());
        activityDefAudit.setDepositApply(activityDef.getDepositDef());
        activityDefAudit.setTaskRpNumApply(activityDef.getTaskRpNumDef());
        activityDefAudit.setTaskRpDepositApply(activityDef.getTaskRpDepositDef());
        activityDefAudit.setKeyWord(activityDef.getKeyWord());

        activityDefAudit.setTaskRpDepositDef(BigDecimal.ZERO);
        activityDefAudit.setMerchantId(activityDef.getMerchantId());
        activityDefAudit.setShopId(activityDef.getShopId());
        activityDefAudit.setActivityDefId(activityDef.getId());
        activityDefAudit.setType(activityDef.getType());
        activityDefAudit.setDepositDef(BigDecimal.ZERO);
        activityDefAudit.setStockDef(0);
        activityDefAudit.setStatus(ActivityDefAuditStatusEnum.WAIT.getCode());
        activityDefAudit.setTaskRpNumDef(0);
        activityDefAudit.setDepositStatus(0);
        activityDefAudit.setDel(DelEnum.NO.getCode());
        activityDefAudit.setCtime(now);
        activityDefAudit.setUtime(now);
        activityDefAudit.setApplyTime(now);
        activityDefAudit.setBizType(ActivityDefAuditBizTypeEnum.STOCK_ADD.getCode());
        boolean success = this.saveOrUpdate(activityDefAudit);
        if (!success) {
            return null;
        }
        return activityDefAudit;
    }

    @Override
    public ActivityDefAudit refundMoneyAddAudit4ActivityDef(ActivityDefAudit activityDefAudit, BigDecimal unitPrice, BigDecimal refundMoney, int totalStock) {
        int applyRefundStock = 0;
        if (unitPrice != null && unitPrice.compareTo(BigDecimal.ZERO) > 0) {
            applyRefundStock = refundMoney.divide(unitPrice, 2, BigDecimal.ROUND_DOWN).intValue();
        }
        ActivityDefAudit newActivityDefAudit = new ActivityDefAudit();
        if (totalStock > 0) {
            newActivityDefAudit.setStockFinal(totalStock - applyRefundStock);
            newActivityDefAudit.setStockDef(totalStock);
        }
        newActivityDefAudit.setMerchantId(activityDefAudit.getMerchantId());
        newActivityDefAudit.setShopId(activityDefAudit.getShopId());
        newActivityDefAudit.setActivityDefId(activityDefAudit.getActivityDefId());
        newActivityDefAudit.setType(activityDefAudit.getType());
        newActivityDefAudit.setApplyTime(new Date());
        newActivityDefAudit.setGoodsIds(activityDefAudit.getGoodsIds());
        newActivityDefAudit.setGoodsId(activityDefAudit.getGoodsId());
        newActivityDefAudit.setStockDef(activityDefAudit.getStockDef());
        newActivityDefAudit.setStockApply(applyRefundStock);
        newActivityDefAudit.setDepositDef(activityDefAudit.getDepositDef());
        newActivityDefAudit.setDepositApply(refundMoney);
        newActivityDefAudit.setDepositStatus(1);
        newActivityDefAudit.setKeyWord(activityDefAudit.getKeyWord());
        newActivityDefAudit.setStatus(ActivityDefAuditStatusEnum.PASS.getCode());
        newActivityDefAudit.setDel(DelEnum.NO.getCode());
        newActivityDefAudit.setCtime(new Date());
        newActivityDefAudit.setUtime(new Date());
        newActivityDefAudit.setBizType(ActivityDefAuditBizTypeEnum.STOCK_REFUND.getCode());
        Integer integer = this.insertSelective(newActivityDefAudit);
        if (integer == 0) {
            return null;
        }
        return newActivityDefAudit;
    }

    @Override
    public SimpleResponse<Object> saveError() {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, String> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "创建活动审核记录出错，活动审核记录未创建，to 百川");
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        response.setMessage("网络异常，请稍候重试");
        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse<Object> listPageRecord4ActivityDefAudit(long current, long size, Long activityDefId) {
        // Controler 请做好参数校验
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, Object> resultVO = ActivityVO.initResultVO2AuditRecord();

        Page<ActivityDefAudit> page = new Page<>();
        page.setAsc("id");
        QueryWrapper<ActivityDefAudit> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDefAudit::getActivityDefId, activityDefId)
                .eq(ActivityDefAudit::getDepositStatus, DepositStatusEnum.PAYMENT.getCode());
        IPage<ActivityDefAudit> activityDefAuditIPage = this.page(page, wrapper);
        if (null == activityDefAuditIPage) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        List<ActivityDefAudit> activityDefAudits = activityDefAuditIPage.getRecords();
        if (CollectionUtils.isEmpty(activityDefAudits)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        resultVO.put("supplyCount", activityDefAuditIPage.getTotal());
        ArrayList<Object> activityDefAuditVOList = new ArrayList<>();
        for (ActivityDefAudit activityDefAudit : activityDefAudits) {
            HashMap<String, Object> activityDefAuditVO = new HashMap<>(16);
            activityDefAuditVO.put("applyTime", DateUtil.dateToString(activityDefAudit.getApplyTime(), DateUtil.FORMAT_TWO));
            String indexApplyStock = "+";
            String indexApplyDeposit = "-";
            if (activityDefAudit.getBizType().equals(ActivityDefAuditBizTypeEnum.STOCK_REFUND.getCode())) {
                indexApplyStock = "-";
                indexApplyDeposit = "+";
            }
            activityDefAuditVO.put("applyStock", indexApplyStock + activityDefAudit.getStockApply());
            activityDefAuditVO.put("applyDeposit", indexApplyDeposit + activityDefAudit.getDepositApply());
            activityDefAuditVO.put("initStock", activityDefAudit.getStockDef());
            activityDefAuditVO.put("finalStock", activityDefAudit.getStockFinal());
            activityDefAuditVO.put("type", ActivityDefAuditBizTypeEnum.findByDesc(activityDefAudit.getBizType()));
            activityDefAuditVOList.add(activityDefAuditVO);
        }
        resultVO.put("activityDefAuditVOList", activityDefAuditVOList);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    @Override
    public ActivityDefAudit supplyStock(SupplyStockReq supplyStockReq) {
        Long activityDefId = Long.valueOf(supplyStockReq.getActivityDefId());
        Date now = new Date();
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        ActivityDefAudit activityDefAudit = new ActivityDefAudit();
        activityDefAudit.setMerchantId(activityDef.getMerchantId());
        activityDefAudit.setShopId(activityDef.getShopId());
        activityDefAudit.setActivityDefId(activityDef.getId());
        activityDefAudit.setType(activityDef.getType());
        activityDefAudit.setBizType(ActivityDefAuditBizTypeEnum.STOCK_ADD.getCode());
        activityDefAudit.setApplyTime(now);
        activityDefAudit.setGoodsIds(activityDef.getGoodsIds());
        activityDefAudit.setGoodsId(activityDef.getGoodsId());
        activityDefAudit.setStockDef(activityDef.getStockDef());
        activityDefAudit.setStockApply(0);
        activityDefAudit.setStockFinal(0);
        if (StringUtils.isNotBlank(supplyStockReq.getStockDef())) {
            activityDefAudit.setStockApply(Integer.valueOf(supplyStockReq.getStockDef()));
        }
        if (StringUtils.isNotBlank(supplyStockReq.getDepositDef())) {
            activityDefAudit.setDepositApply(new BigDecimal(supplyStockReq.getDepositDef()));
        }
        activityDefAudit.setDepositDef(activityDef.getDepositDef());
        activityDefAudit.setDepositStatus(0);
        activityDefAudit.setKeyWord(activityDef.getKeyWord());
        activityDefAudit.setStatus(1);
        activityDefAudit.setDel(DelEnum.NO.getCode());
        activityDefAudit.setCtime(now);
        activityDefAudit.setUtime(now);
        boolean save = this.save(activityDefAudit);
        if (!save) {
            return null;
        }
        return activityDefAudit;
    }

    /***
     * 补库存支付成功后
     * @param activityDefAuditId
     * @return
     */
    @Override
    public boolean paySupplyStock(Long activityDefAuditId) {
        Date now = new Date();
        // 内部回调 不做参数校验
        ActivityDefAudit activityDefAudit = this.getById(activityDefAuditId);
        if (null == activityDefAudit) {
            log.error("activity_def_audit 表 没有此数据记录，activityDefAuditId = " + activityDefAuditId);
            return false;
        }
        // 获取信息
        BigDecimal depositDef = BigDecimal.ZERO;
        if (activityDefAudit.getDepositDef() != null && activityDefAudit.getDepositDef().compareTo(BigDecimal.ZERO) > 0) {
            depositDef = activityDefAudit.getDepositDef();
        }
        BigDecimal depositApply = BigDecimal.ZERO;
        if (activityDefAudit.getDepositApply() != null && activityDefAudit.getDepositApply().compareTo(BigDecimal.ZERO) > 0) {
            depositApply = activityDefAudit.getDepositApply();
        }
        BigDecimal depositFinal = depositDef.add(depositApply);

        Integer stockDef = 0;
        if (activityDefAudit.getStockDef() != null && activityDefAudit.getStockDef() > 0) {
            stockDef = activityDefAudit.getStockDef();
        }
        Integer stockApply = 0;
        if (activityDefAudit.getStockApply() != null && activityDefAudit.getStockApply() > 0) {
            stockApply = activityDefAudit.getStockApply();
        }
        int stockFinal = stockDef + stockApply;
        // 更新付款记录
        activityDefAudit.setDepositActual(depositApply);
        activityDefAudit.setDepositFinal(depositFinal);

        activityDefAudit.setStockActual(stockApply);
        activityDefAudit.setStockFinal(stockFinal);

        activityDefAudit.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        activityDefAudit.setStatus(ActivityDefAuditStatusEnum.PASS.getCode());
        activityDefAudit.setPublishTime(now);
        activityDefAudit.setAuditTime(now);
        activityDefAudit.setUtime(now);
        boolean success = this.updateById(activityDefAudit);
        if (!success) {
            log.error("更新审核记录表 activity_def_audit 失败， activityDefAuditId = " + activityDefAuditId);
            return false;
        }

        // 更新 activityDef表
        Long activityDefId = activityDefAudit.getActivityDefId();
        Integer activityDefRows = activityDefService.updateDepositDefAndStockDef(activityDefId, depositApply, stockApply);
        if (activityDefRows == 0) {
            log.error("更新活动定义表 activity_def 失败， activityDefAuditId = " + activityDefAuditId);
            return false;
        }

        /***
         * 红包任务20190318活动拆分迭代后，将1拆3个活动形式存在
         */
        // 如果是红包活动，更新 红包任务状态 activity_task
        if (activityDefAudit.getType() == ActivityDefAuditTypeEnum.RED_PACKET.getCode()) {
            success = activityTaskService.supplyStock(activityDefId, stockApply);
            if (!success) {
                log.error("更新活动定义表 activity_def 失败， activityDefAuditId = " + activityDefAuditId);
                return false;
            }
        }

        boolean favCart_GOODCOMMENT = ActivityDefTypeEnum.FAV_CART.getCode() == activityDefAudit.getType() || ActivityDefTypeEnum.FAV_CART_V2.getCode() == activityDefAudit.getType()
                || ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDefAudit.getType() || ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() == activityDefAudit.getType();
        if (favCart_GOODCOMMENT) {
            success = activityTaskService.supplyStock(activityDefId, stockApply);
            if (!success) {
                log.error("更新活动定义表 activity_def 失败， activityDefAuditId = " + activityDefAuditId);
                return false;
            }
        }

        // 如果是幸运大转盘，更新 activity表 ，把活动重新启动
        if (activityDefAudit.getType() == ActivityDefAuditTypeEnum.LUCKY_WHEEL.getCode()) {
            QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
            aWrapper.lambda()
                    .eq(Activity::getActivityDefId, activityDefId);
            Activity activity = activityService.getOne(aWrapper);
            activity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
            activity.setDel(DelEnum.NO.getCode());
            activity.setUtime(now);
            success = activityService.updateById(activity);
            if (!success) {
                log.error("更新活动表 activity 失败， activityDefAuditId = " + activityDefAuditId);
                return false;
            }

        }
        return true;
    }

}
