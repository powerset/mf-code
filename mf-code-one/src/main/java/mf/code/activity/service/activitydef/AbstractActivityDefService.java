package mf.code.activity.service.activitydef;

import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * mf.code.api.seller.service.createDef.impl
 *
 * @author yechen
 * @date 2019年03月18日 20:18
 */
@Component
public abstract class AbstractActivityDefService {
    @Autowired
    private GoodsService goodsService;
    /**
     * 检查入参
     *
     * @param req
     * @return
     */
    protected SimpleResponse assertReq(CreateDefReq req) {
        if (req == null || req.getType() == null || req.getMerchantId() == null || req.getShopId() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        req.init();
        if (req.getAmount() != null) {
            Assert.isNumber(req.getAmount(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请输入正确的金额");
        }

        return new SimpleResponse();
    }

    /**
     * 校验活动数据是否可以创建
     *
     * @param req
     * @return
     */
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        return new SimpleResponse();
    }

    /**
     * 校验活动数据是否可以创建
     *
     * @param req
     * @return
     */
    protected SimpleResponse checkDefForUpdate(CreateDefReq req) {
        return new SimpleResponse();
    }

    /**
     * 校验活动数据是否可以创建
     *
     * @param req
     * @return
     */
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        return new SimpleResponse();
    }

    /**
     * 创建活动
     *
     * @param req
     * @return
     */
    protected SimpleResponse createDef(CreateDefReq req) {
        return new SimpleResponse();
    }

    /**
     * 创建活动
     *
     * @param req
     * @return
     */
    protected SimpleResponse updateDef(CreateDefReq req) {
        return new SimpleResponse();
    }

    /**
     * 骨架方法 -- 执行活动定义创建
     *
     * @param req
     * @return
     */
    final SimpleResponse doCreate(CreateDefReq req) {
        SimpleResponse simpleResponse;
        simpleResponse = assertReq(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        simpleResponse = checkDefForCreate(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        simpleResponse = createDef(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 骨架方法 -- 执行活动定义更新
     *
     * @param req
     * @return
     */
    final SimpleResponse doUpdate(CreateDefReq req) {
        SimpleResponse simpleResponse;
        simpleResponse = assertReq(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        simpleResponse = checkDefForUpdate(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        simpleResponse = updateDef(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 骨架方法 -- 执行保证金查询
     *
     * @param req
     * @return
     */
    final SimpleResponse doDeposit(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse = assertReq(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        simpleResponse = calcDeposit(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        return simpleResponse;
    }


    public abstract ActivityDefTypeEnum getType();
}
