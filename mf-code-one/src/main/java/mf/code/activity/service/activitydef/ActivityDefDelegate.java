package mf.code.activity.service.activitydef;

import mf.code.api.seller.dto.CreateDefReq;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.exception.ArgumentException;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * mf.code.api.seller.service.createDef
 *
 * @author yechen
 * @date 2019年03月18日 17:47
 */
@Component
public class ActivityDefDelegate {

    private static Map<ActivityDefTypeEnum, AbstractActivityDefService> registerService = new ConcurrentHashMap<>(128);

    @Autowired
    private List<AbstractActivityDefService> activityDefServices;

    @PostConstruct
    public void init() {
        for (AbstractActivityDefService activityDefService : activityDefServices) {
            registerService.put(activityDefService.getType(), activityDefService);
        }
    }
    private AbstractActivityDefService getActivityDefService(int type){
        AbstractActivityDefService activityDefService = registerService.get(ActivityDefTypeEnum.findByCode(type));
        if (activityDefService == null) {
            throw new ArgumentException("因版本问题，暂未提供！");
        }
        return activityDefService;
    }
    /**
     * 创建活动定义
     *
     * @param req
     * @return
     */
    public SimpleResponse create(CreateDefReq req) {
        AbstractActivityDefService converterService = getActivityDefService(req.getType());
        return converterService.doCreate(req);
    }

    /**
     * 更新活动定义
     *
     * @param req
     * @return
     */
    public SimpleResponse update(CreateDefReq req) {
        AbstractActivityDefService converterService = registerService.get(ActivityDefTypeEnum.findByCode(req.getType()));
        return converterService.doUpdate(req);
    }

    /**
     * 查询活动保证金
     *
     * @param req
     * @return
     */
    public SimpleResponse deposit(CreateDefReq req) {
        AbstractActivityDefService converterService = registerService.get(ActivityDefTypeEnum.findByCode(req.getType()));
        return converterService.doDeposit(req);
    }
}
