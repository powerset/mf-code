package mf.code.activity.service.activitydef.impl;

import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.activity.service.activitydef.impl
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-07-02 16:07
 */
@Service
public class ActivityDefReimbursementService extends AbstractActivityDefService {

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantShopService merchantShopService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ActivityDefCheckService activityDefCheckService;

    @Autowired
    private ActivityDefAboutService activityDefAboutService;

    @Autowired
    private ActivityDefService activityDefService;

    @Autowired
    private ActivityDefAuditService activityDefAuditService;

    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;

    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        Merchant merchant = merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(req.getShopId());
        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if (req.getExtraDataObject() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少必要参数");
        }
        if (req.getStockDef() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少计划参与人数");
        }

        if (StringUtils.isBlank(req.getAmount())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少拉新成本");
        }
        if (req.getType() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少邀请类型");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getMaxAmount())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少单人报销的最大值");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getMaxReimbursement())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少单人单次报销的最大值");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getCondDrawTime())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少邀请单个新人时间");
        }

        if (req.getExtraDataObject().getHitsPerDraw() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "传参异常，缺少提现条件");
        }

        // redis防重拦截
        String key = new StringBuilder(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT).append(req.getMerchantId()).toString();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, key);
        if (!success) {
            return RedisForbidRepeat.NXError();
        }

        // 校验是否有创建活动的资格
        SimpleResponse simpleResponse = activityDefCheckService.createAble(req.getMerchantId(), req.getShopId(), req.getType(), null, null, null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(key);
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse createDef(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ActivityDef activityDef = activityDefAboutService.addActivityDefReimbursement(req);
        int isSuccess = this.activityDefService.createActivityDef(activityDef);
        if (isSuccess == 0) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "存储活动异常 to @大妖怪");
        }

        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }

        // 处理返回数据
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }

    @Override
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());
        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }
        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            return this.activityDefUpdateService.activityDefReimbursementUpdateSave(req, activityDef);
        } else {
            //非草稿修改
            return this.activityDefUpdateService.activityDefReimbursementUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.FULL_REIMBURSEMENT;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        if (req == null || req.getStockDef() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数异常，to @大妖怪");
        }

        BigDecimal deposit = BigDecimal.ZERO;
        if (req.getActivityDefId() == null) {
            if (StringUtils.isNotBlank(req.getAmount())) {
                deposit = activityDefAboutService.calcReimbursementDeposit(req.getStockDef(), req.getAmount());
            } else {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数异常，to @大妖怪");
            }
        } else {
            if (req.getStockDef() != null) {
                ActivityDef activityDef = activityDefService.selectByPrimaryKey(req.getActivityDefId());
                if (activityDef == null) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动不存在！");
                }
                deposit = activityDefAboutService.calcReimbursementDeposit(req.getStockDef(), activityDef.getGoodsPrice().toString());
            } else {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数异常，to @大妖怪");
            }
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> result = new HashMap<>(2);
        result.put("depositDef", deposit);
        simpleResponse.setData(result);
        return simpleResponse;
    }
}
