package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.service.ActivityAboutService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityPlanService;
import mf.code.activity.service.ActivityService;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.constant.*;
import mf.code.common.utils.JsonParseUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.activity.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月25日 09:36
 */
@Slf4j
@Service
public class ActivityAboutServiceImpl implements ActivityAboutService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityPlanService activityPlanService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;
    @Autowired
    private UserService userService;

    /***
     * 获取活动对应的客服信息
     * @param activity
     * @return
     */
    @Override
    public Map<String, Object> getServiceWx(Activity activity) {
        Map<String, Object> resp = new HashMap<>();
        if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
            if (activityDef != null && StringUtils.isBlank(activityDef.getServiceWx())) {
                return resp;
            }
            if(JsonParseUtil.booJsonArr(activityDef.getServiceWx())){
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if(CollectionUtils.isEmpty(csMapList)){
                    return resp;
                }
                //微信号
                resp.put("customServiceWxNum", csMapList.get(0).get("wechat"));
                //微信二维码
                resp.put("customServiceCode", csMapList.get(0).get("pic"));
            }else{

                resp.put("customServiceWxNum", activityDef.getServiceWx());
                resp.put("customServiceCode", "");
            }

        } else if (activity.getActivityPlanId() != null && activity.getActivityPlanId() > 0) {
            ActivityPlan activityPlan = this.activityPlanService.findById(activity.getActivityPlanId());
            if (activityPlan != null && StringUtils.isNotBlank(activityPlan.getServiceWx())) {
                //微信号
                resp.put("customServiceWxNum", activityPlan.getServiceWx());
                //微信二维码
                resp.put("customServiceCode", "");
            }
        }
        return resp;
    }

    /***
     * 设置活动对应的客服信息
     * [{"wechat":"微信客服8881","pic":"https://asset.wxyundian.com/data-test3/shopqrcode/shop/wechat/公众号二维码.png"}]
     * @param wechat
     * @param pic
     * @return
     */
    @Override
    public Map<String, Object> changeCustomServiceToMap(String wechat, String pic) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("wechat", wechat);
        resp.put("pic", pic);
        return resp;
    }

    /***
     * 查询活动信息的详情
     * @param activity
     * @param merchantShop
     * @param activityPersonList
     * @param customServiceMap
     * @return
     */
    @Override
    public Map<String, Object> queryActivityAndGoods(Activity activity, MerchantShop merchantShop, List<String> activityPersonList, Map<String, Object> customServiceMap) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("merchantId", activity.getMerchantId());
        map.put("activityId", activity.getId());
        map.put("shopId", activity.getShopId());
        map.put("activityType", activity.getType());//0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者
        map.put("startTime", activity.getStartTime() != null ? DateFormatUtils.format(activity.getStartTime(), "yyyy-MM-dd HH:mm:ss") : "");//活动开始
        map.put("endTime", activity.getEndTime() != null ? DateFormatUtils.format(activity.getEndTime(), "yyyy-MM-dd HH:mm:ss") : "");//活动结束
        map.put("cutoffTime", activity.getEndTime() != null ? activity.getEndTime().getTime() : 0L);
        Map<String, Object> codeMap = this.merchantShopService.getCustomServiceAbout(merchantShop);
        if (customServiceMap != null && customServiceMap.get("customServiceCode") != null && StringUtils.isNotBlank(customServiceMap.get("customServiceCode").toString())) {
            map.put("code", customServiceMap.get("customServiceCode"));
        } else {
            map.put("code", codeMap != null && codeMap.get("customServiceCode") != null &&
                    StringUtils.isNotBlank(codeMap.get("customServiceCode").toString()) ? this.ossCaller.cdnReplace(codeMap.get("customServiceCode").toString()) : "");//客服二维码
        }
        map.put("condPersionCount", activity.getCondPersionCount());//满人开奖
        //新人有礼活动发起者编号
        map.put("newManStartId", activity.getUserId() == null ? "" : String.valueOf(activity.getUserId()));
        //满时间开奖
        map.put("condDrawTime", "");
        map.put("hitsPerDraw", activity.getHitsPerDraw());//开奖份数
        map.put("joinCount", activityPersonList.size());//活动参加人数
        //根据商品列表填充数据
        //JSONObject.parseArray(activityPlan.getGoodsIdsJson(),Long.class)==List<Long.class-->Long>
        map.put("goods", this.queryGoodsAbout(activity.getGoodsId()));
        return map;
    }

    /***
     * 查询目的：获取 商品列表信息
     * @param goodsID
     * @return
     */
    @Override
    public Map<String, Object> queryGoodsAbout(Long goodsID) {
        //先从redis获取
        String goodsStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_GOODS + goodsID);
        if (StringUtils.isNotBlank(goodsStr)) {
            JSONObject itemJSONObj = JSONObject.parseObject(goodsStr);
            Map<String, Object> itemMap = JSONObject.toJavaObject(itemJSONObj, Map.class);
            return itemMap;
        }

        Map<String, Object> map = new HashMap<String, Object>();
        Goods goods = this.goodsService.selectById(goodsID);
        if (goods != null) {
            map.put("title", StringUtils.isNotBlank(goods.getDisplayGoodsName()) ? goods.getDisplayGoodsName() : "");
            map.put("picUrl", StringUtils.isNotBlank(goods.getPicUrl()) ? this.ossCaller.cdnReplace(goods.getPicUrl()) : "");
            map.put("price", goods.getDisplayPrice());
            map.put("goodsId", goods.getId());
        }
        //存入redis
        if (StringUtils.isNotBlank(goods.getPicUrl())) {
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_GOODS + goodsID, JSON.toJSONString(map), 1, TimeUnit.DAYS);
        }
        return map;
    }

    /***
     * 根据用户id查询是否参加
     * joinStatus:0:未参与  1:参与,但没中获  2:中奖  3:参与,有优惠券(其他) 4:活动未开始 5：活动活动结束 6 中奖失效状态
     * 其中状态优先级：中奖>结束>参与
     * @param userID
     * @param activity
     * @param activityPersonList
     * @param activityPersonInviteList
     * @param merchantShop
     * @param customServiceMap
     * @return
     */
    @Override
    public Map<String, Object> queryUserActivityJoin(Long userID, Activity activity, List<String> activityPersonList, List<String> activityPersonInviteList, MerchantShop merchantShop, Map<String, Object> customServiceMap) {
        Map<String, Object> map = this.merchantShopService.getCustomServiceAbout(merchantShop);
        if (customServiceMap != null && customServiceMap.get("customServiceWxNum") != null && StringUtils.isNotBlank(customServiceMap.get("customServiceWxNum").toString())) {
//            map.put("customServiceCode",customServiceMap.get("customServiceCode"));
            map.put("customServiceWxNum", customServiceMap.get("customServiceWxNum"));
        }
        Map<String, Object> userActivityParams = new HashMap<String, Object>();
        userActivityParams.put("userId", userID);
        userActivityParams.put("activityId", activity.getId());
        userActivityParams.put("order", "ctime");
        userActivityParams.put("direct", "desc");
        int activtityStatus = this.activityService.getActivityStatus(activity.getStatus(), activity.getStartTime(), activity.getAwardStartTime(), activity.getEndTime());
        List<UserActivity> userActivitys = this.userActivityService.pageListUserActivity(userActivityParams);
        map.put("awardNo", new ArrayList<Object>());
        map.put("activityDefId", 0);
        map.put("joinAwardCode", 0);
        if (userActivitys == null || userActivitys.size() == 0) {
            map.put("joinStatus", activtityStatus == 2 ? 4 : activtityStatus);
            return map;
        }
        List<UserTask> userTasks = this.userTaskService.queryAwardTask(
                activity.getMerchantId(),
                activity.getShopId(),
                userID,
                activity.getId());
        UserTask userTask = null;
        if(!CollectionUtils.isEmpty(userTasks)){
            userTask = userTasks.get(0);
        }
        UserActivity userActivity = userActivitys.get(0);
        Integer status = this.getJoinStatus(userTask, userActivity, activity, activtityStatus, userID);
        map.put("joinStatus", status);
        //若是未中奖,包括 啥都没有+优惠券 状态6：未中奖 状态7：优惠券
        map.put("redPackStatus", 0);//领奖失效:-1 领奖成功:3 领奖中:1 没意义：0
        if (Arrays.asList(6, 7).contains(status)) {
            map.put("redPackStatus", this.activityService.getRedPackStatus(activity, userID));
        }
        //用户自己参加的个数
        int joinNum = 0;
        if (activityPersonList != null) {
            for (String str : activityPersonList) {
                JSONObject jsonObject = JSON.parseObject(str);
                if (NumberUtils.toLong(jsonObject.get("uid").toString()) == userID) {
                    List<Object> userJoinList = new ArrayList<Object>();
                    List<Long> awardNos = JSONObject.parseArray(jsonObject.get("awardNo").toString(), Long.class);
                    for (long awardNo : awardNos) {
                        Map<String, Object> joinUserMap = new HashMap<>();
                        joinUserMap.put("avatarUrl", StringUtils.isNotBlank(jsonObject.getString("avatarUrl")) ? jsonObject.getString("avatarUrl") : "");
                        joinUserMap.put("uid", jsonObject.getLong("uid") == 0 ? 0 : jsonObject.getLong("uid"));
                        joinUserMap.put("awardNo", awardNo);
                        joinUserMap.put("isYourself", true);
                        userJoinList.add(joinUserMap);
                    }
                    map.put("awardNo", userJoinList);
                    joinNum = awardNos.size();
                }
            }
        }
        if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
            map.put("activityDefId", activity.getActivityDefId());
        }
        int lastInviteNum = this.redisService.queryRedisJoinInviteLastNum(activity.getId(), userID);
        //增加的中奖码个数
        map.put("addAwardNum", 0);
        //用户邀请的总人数
        map.put("totalInviteNum", activityPersonInviteList.size());
        //中奖弹窗
        map.put("awardDialog", false);
        if (status == 10) {//中奖状态等于10
            boolean userTaskStatusDialog = userTask != null && userTask.getStatus() == UserTaskStatusEnum.INIT.getCode();
            String awardDialogValue = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.AWARDDIALOG + activity.getId() + ":" + userID);
            if (StringUtils.isNotBlank(awardDialogValue) && NumberUtils.toInt(awardDialogValue) == 0) {
                if(userTaskStatusDialog){
                    map.put("awardDialog", true);
                }
                this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.AWARDDIALOG + activity.getId() + ":" + userID, "1", 1, TimeUnit.DAYS);
            }
        }
        //未中奖弹窗
        String noAwardDialogValue = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.NOAWARDDIALOG + activity.getUserId());
        if (StringUtils.isNotBlank(noAwardDialogValue) && NumberUtils.toInt(noAwardDialogValue) == 0) {
            map.put("noAwardDialog", true);
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.NOAWARDDIALOG + activity.getUserId(), "1", 1, TimeUnit.DAYS);
        }

        if (activityPersonInviteList.size() > lastInviteNum) {
            map.put("addAwardNum", activityPersonInviteList.size() - lastInviteNum);
            this.redisService.updateRedisJoinInviteLastNum(activity.getId(), userID, activityPersonInviteList.size());
        }

        map.put("joinAwardCode", activityPersonInviteList.size() + joinNum);
        //优惠券的处理
        Map<String, Object> shopCouponMap = this.queryShopCoupon(userActivity);
        map.put("shopCoupon", shopCouponMap);

        //判断该活动是否是新人有礼发起者，若是新人有礼，则对其joinStatus重新返回。新人有礼状态：10：成功 -1：失败  其余是过程状态
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode() && activity.getUserId() != null && userID.equals(activity.getUserId())) {
            if (userActivity.getStatus() != 2 && userActivity.getStatus() != UserActivityStatusEnum.NO_WIN.getCode()) {//2是中奖状态,1是待开奖，其余都是未中奖
                map.put("joinStatus", -1);//发起失败，需重新发起
            }
        }
        return map;
    }

    /***
     * 处理用户此时的状态
     * @param userActivity
     * @param activity
     * @param activtityStatus
     * @param userID
     * @return -1: 已结束;1:待开始;2:待开奖;3:已开奖;
     *          4:待开奖,未参与(出现参与按钮),5：待开奖,参与(出现召唤好友);
     *          6：已开奖,没中;7：已开奖,中优惠券;8：已开奖,规定时间没完成任务;9：已开奖,并且获奖;
     */
    @Override
    public Integer getJoinStatus(UserTask userTask, UserActivity userActivity, Activity activity, int activtityStatus, Long userID) {
        //待开奖
        if (activtityStatus == 2) {
            //出现召唤好友
            if (userActivity.getStatus() == 1) {
                return 5;
            }
            //未参与
            if (userActivity.getStatus() == 0) {
                return 4;
            }
        }
        //已开奖
        if (activtityStatus == 3) {
            //获奖发放成功,不区分活动类型
            if (userTask != null && userTask.getStatus() == 3) {
                return 9;
            }
            //中奖
            if ((activity.getType() == 4 && activity.getUserId() != null && activity.getUserId().equals(userID))
                    || (userTask != null && userTask.getStatus() != 3 && userTask.getStatus() != -1)
                    || userActivity.getStatus() == 2) {
                //中奖时，存储redis
                String awardDialogValue = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.AWARDDIALOG + activity.getId() + ":" + userID);
                if (StringUtils.isBlank(awardDialogValue)) {
                    this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.AWARDDIALOG + activity.getId() + ":" + userID, "0", 1, TimeUnit.DAYS);
                }
                return 10;
            }
            //没中奖
            if (userActivity.getStatus() == 1) {
                if (activity.getType() != 2 && activity.getType() != 4) {
                    this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.NOAWARDDIALOG + activity.getUserId(), "0", 1, TimeUnit.DAYS);
                }
                return 6;
            }
            //中奖任务过期
            if (userActivity.getStatus() == -2 || (userTask != null && userTask.getStatus() == -1)) {
                return 8;
            }
            //优惠券
            if (userActivity.getStatus() == 3) {
                return 7;
            }
        }
        return activtityStatus;
    }

    @Override
    public List<Object> queryRedisWinner(Activity activity, List<String> activityPersonList) {
        //获取中奖人的用户id
        List<Long> winnerUserIDs = this.redisService.getRedisWinnerUserIDs(activity.getId());
        //获取中奖人任务信息
        List<UserTask> userTasks = this.userTaskService.queryAwardTask(
                activity.getMerchantId(),
                activity.getShopId(),
                null,
                activity.getId());
        //根据user_task来判断要展现几条数据，和状态
        Map<Long, UserTask> userTaskMap = new HashMap<Long, UserTask>();
        if (userTasks != null && userTasks.size() > 0) {
            for (UserTask userTask : userTasks) {
                userTaskMap.put(userTask.getUserId(), userTask);
            }
        }
        //活动参与人数
        Map<Long, String> joinUserMap = new HashMap<>();
        if (activityPersonList != null && activityPersonList.size() > 0) {
            for (int i = 0; i < activityPersonList.size(); i++) {
                JSONObject jsonObject = JSON.parseObject(activityPersonList.get(i).toString());
                joinUserMap.put(jsonObject.getLong("uid"), jsonObject.getString("avatarUrl"));
            }
        }
        List<Object> list = new ArrayList<Object>();
        if (winnerUserIDs == null || winnerUserIDs.size() == 0) {
            return list;
        }
        for (Long winnerUserID : winnerUserIDs) {
            Map<String, Object> map = new HashMap<>();
            UserTask userTask = userTaskMap.get(winnerUserID);
            if (StringUtils.isBlank(joinUserMap.get(winnerUserID)) || userTask == null) {
                continue;
            }

            map.put("awardStatus", this.userTaskService.getUserTaskStatus(userTask));
            map.put("avatarUrl", joinUserMap.get(winnerUserID));
            map.put("userId", winnerUserID);
            list.add(map);
        }
        return list;
    }

    @Override
    public List<Object> queryRedisInviteUser(Activity activity, Long activityID, List<String> activityPersonList) {
        List<Object> list = new ArrayList<Object>();

        //查询redis，获取参与人的小圆圈数目
        if (activityPersonList != null && activityPersonList.size() > 0) {
            for (int i = 0; i < activityPersonList.size(); i++) {
                JSONObject jsonObject = JSON.parseObject(activityPersonList.get(i).toString());
                //获取小圆圈数目
                List<String> activityUserJoinList = this.redisService.queryRedisJoinInviteUser(activityID, jsonObject.getLong("uid"));
                //activityUserJoinList.size()就是小圆圈的值
                //判断该用户是否机器人用户
                Map<String, Object> map = this.userService.robotUserInfo(jsonObject.getLong("uid"));
                if (map == null || map.size() == 0) {
                    map.put("avatarUrl", jsonObject.getString("avatarUrl"));
                }
                map.put("joinNum", activityUserJoinList.size());
                if (list == null || list.size() == 0) {
                    list = new ArrayList<>();
                }
                list.add(map);
            }
        }

        return list;
    }

    @Override
    public Activity getMySelfActivity(Activity activity, Long userId) {
        QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
        activityWrapper.lambda()
                .eq(Activity::getMerchantId, activity.getMerchantId())
                .eq(Activity::getShopId, activity.getShopId())
                .eq(Activity::getGoodsId, activity.getGoodsId())
                .eq(Activity::getUserId, userId)
                .in(Activity::getType, Arrays.asList(ActivityTypeEnum.ASSIST.getCode(), ActivityTypeEnum.ASSIST_V2.getCode()))
                .orderByDesc(Activity::getCtime)
        ;
        List<Activity> activities = this.activityService.list(activityWrapper);
        Long activityId = activity.getId();
        if (!CollectionUtils.isEmpty(activities)) {
            for (Activity activity1 : activities) {
                if (activities.size() == 1) {
                    activity = activities.get(0);
                }
                if (activity1.getStatus() == ActivityStatusEnum.PUBLISHED.getCode() || this.checkAwardOrAwarded(activity1,true)) {
                    activity = activity1;
                }
            }
            if (activityId.equals(activity.getId())) {
                activity = activities.get(0);
            }
        }
        return activity;
    }

    /***
     * 查询是否开团
     * @param activity
     * @return
     */
    @Override
    public Boolean checkAwardOrAwarded(Activity activity, boolean validAward) {
        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", activity.getMerchantId());
        userTaskParams.put("shopId", activity.getShopId());
        userTaskParams.put("activityId", activity.getId());
        userTaskParams.put("type", UserTaskTypeEnum.ASSIST_START.getCode());
        List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
        if (!CollectionUtils.isEmpty(userTasks)) {
            if(validAward){
                for (UserTask userTask : userTasks) {
                    if (userTask.getStatus() != UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                        return true;
                    }
                }
            }else {
                return true;
            }
        }
        return false;
    }


    /***
     * 优惠券的处理
     */
    private Map<String, Object> queryShopCoupon(UserActivity userActivity) {
        MerchantShopCoupon merchantShopCoupon = this.merchantShopCouponService.selectByPriKey(userActivity.getMerchantShopCouponId());
        Map<String, Object> map = new HashMap<>();
        map.put("couponStartTime", merchantShopCoupon == null || merchantShopCoupon.getDisplayStartTime() == null ? "" : DateFormatUtils.format(merchantShopCoupon.getDisplayStartTime(), "yyyy.MM.dd"));
        map.put("couponEndTime", merchantShopCoupon == null || merchantShopCoupon.getDisplayEndTime() == null ? "" : DateFormatUtils.format(merchantShopCoupon.getDisplayEndTime(), "yyyy.MM.dd"));
        map.put("amount", merchantShopCoupon == null || merchantShopCoupon.getAmount() == null ? BigDecimal.ZERO : merchantShopCoupon.getAmount());
        map.put("name", merchantShopCoupon == null || StringUtils.isBlank(merchantShopCoupon.getName()) ? "" : merchantShopCoupon.getName());
        map.put("tpwd", merchantShopCoupon == null || StringUtils.isBlank(merchantShopCoupon.getTpwd()) ? "" : merchantShopCoupon.getTpwd());
        return map;
    }
}
