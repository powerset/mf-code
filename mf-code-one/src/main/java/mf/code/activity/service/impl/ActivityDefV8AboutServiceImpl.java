package mf.code.activity.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityDefV8AboutService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v8.dto.CheckpointTaskSpaceDTO;
import mf.code.api.applet.v8.dto.NewbieTask;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.CheckpointTaskSpaceTypeEnum;
import mf.code.one.vo.UserCouponAboutVO;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.activity.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月21日 16:54
 */
@Service
@Slf4j
public class ActivityDefV8AboutServiceImpl implements ActivityDefV8AboutService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserService userService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private CommissionService commissionService;

    @Override
    public CheckpointTaskSpaceDTO.TaskDTO queryNewbieTaskDetail(Long userId, boolean first) {
        User user = userService.selectByPrimaryKey(userId);
        Long shopId = user.getVipShopId();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            log.error("<<<<<<<<获取店铺信息异常： shopId:{}", shopId);
        }
        Long merchantId = merchantShop.getMerchantId();

        List<ActivityDef> activityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId, ActivityDefTypeEnum.NEWBIE_TASK.getCode());
        if (CollectionUtils.isEmpty(activityDefs)) {
            return null;
        }
        List<Long> activityDefIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            if (!activityDef.getId().equals(activityDef.getParentId()) && activityDefIds.indexOf(activityDef.getId()) == -1) {
                activityDefIds.add(activityDef.getId());
            }
        }
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = new CheckpointTaskSpaceDTO.TaskDTO();
        taskDTO.setType(CheckpointTaskSpaceTypeEnum.NEWBIE_TASK.getCode());
        taskDTO.setTaskDetails(new ArrayList<NewbieTask>());
        //该用户是否做过
        Map<Long, List<UserCoupon>> existUserCouponsMap = queryActivityDefInfo(merchantId, shopId, userId, null,
                UserCouponAboutVO.addNewbieTaskType());
        boolean isDone = false;
        if(!CollectionUtils.isEmpty(existUserCouponsMap)){
            for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                List<UserCoupon> coupons = entry.getValue();
                UserCoupon userCoupon = coupons.get(0);
                if(userCoupon.getType() != UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode()){
                    isDone = true;
                }
            }
        }

        Map<Long, List<UserCoupon>> userCouponsMap = queryActivityDefInfo(merchantId, shopId, userId, activityDefIds,
                UserCouponAboutVO.addNewbieTaskType());
        for (ActivityDef activityDef : activityDefs) {
            //父级定义活动基本数据
            if (activityDef.getId().equals(activityDef.getParentId())) {
                //判断是否有库存，并且未做过
                if (activityDef.getStock() <= 0 && !isDone) {
                    return null;
                }
            }
        }
        //子级所有各活动情况
        for (ActivityDef activityDef : activityDefs) {
            //父级定义活动基本数据
            if (activityDef.getId().equals(activityDef.getParentId())) {
                taskDTO.setRemainStock(activityDef.getStock());
                taskDTO.setStock(activityDef.getStockDef());
                continue;
            }
            if (!activityDef.getId().equals(activityDef.getParentId())) {
                NewbieTask newbieTask = new NewbieTask();
                newbieTask.setType(activityDef.getType());

                for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                    List<UserCoupon> coupons = entry.getValue();
                    isDoneUserCoupon(newbieTask, coupons, 1, null, activityDef.getType());
                }

                List<UserCoupon> userCoupons = userCouponsMap.get(activityDef.getId());
                if (!CollectionUtils.isEmpty(userCoupons)) {
                    isDoneUserCoupon(newbieTask, userCoupons, 1, null, activityDef.getType());
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode()) {
                    addTask(userId, taskDTO, newbieTask, activityDef);
                    if (first) {
                        break;
                    }
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode()) {
                    addTask(userId, taskDTO, newbieTask, activityDef);
                    if (first) {
                        break;
                    }
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                    addTask(userId, taskDTO, newbieTask, activityDef);
                    if (first) {
                        break;
                    }
                }
            }
        }
        if (CollectionUtils.isEmpty(taskDTO.getTaskDetails())) {
            return null;
        }
        return taskDTO;
    }

    @Override
    public CheckpointTaskSpaceDTO.TaskDTO queryShopManagerTaskDetail(Long userId, boolean first) {
        User user = userService.selectByPrimaryKey(userId);
        Long shopId = user.getVipShopId();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            log.error("<<<<<<<<获取店铺信息异常： shopId:{}", shopId);
        }
        Long merchantId = merchantShop.getMerchantId();
        List<ActivityDef> activityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId, ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode());
        if (CollectionUtils.isEmpty(activityDefs)) {
            return null;
        }

        boolean shopManager = user.getRole() == 1 ? true : false;

        List<Long> activityDefIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            if (!activityDef.getId().equals(activityDef.getParentId()) && activityDefIds.indexOf(activityDef.getId()) == -1) {
                activityDefIds.add(activityDef.getId());
            }
        }
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = new CheckpointTaskSpaceDTO.TaskDTO();
        taskDTO.setType(CheckpointTaskSpaceTypeEnum.SHOP_MANAGER_TASK.getCode());
        taskDTO.setTaskDetails(new ArrayList<NewbieTask>());
        taskDTO.setMerchantId(merchantId);
        taskDTO.setShopId(shopId);

        //该用户是否做过
        Map<Long, List<UserCoupon>> existUserCouponsMap = queryActivityDefInfo(merchantId, shopId, userId, null,
                UserCouponAboutVO.addShopManagerTaskType());

        Map<Long, List<UserCoupon>> userCouponsMap = queryActivityDefInfo(merchantId, shopId, userId, activityDefIds,
                UserCouponAboutVO.addShopManagerTaskType());
        for (ActivityDef activityDef : activityDefs) {
            //父级定义活动基本数据
            if (activityDef.getId().equals(activityDef.getParentId())) {
                taskDTO.setRemainStock(activityDef.getStock());
                taskDTO.setStock(activityDef.getStockDef());
            }
            //子级所有各活动情况
            if (!activityDef.getId().equals(activityDef.getParentId())) {
                NewbieTask newbieTask = new NewbieTask();
                newbieTask.setType(activityDef.getType());

                for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                    List<UserCoupon> coupons = entry.getValue();
                    if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                        isDoneUserCoupon(newbieTask, coupons, 2, activityDef.getStartNum(), activityDef.getType());
                    } else {
                        isDoneUserCoupon(newbieTask, coupons, 2, null, activityDef.getType());
                    }
                }

                List<UserCoupon> userCoupons = userCouponsMap.get(activityDef.getId());
                if (!CollectionUtils.isEmpty(userCoupons)) {
                    if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                        isDoneUserCoupon(newbieTask, userCoupons, 2, activityDef.getStartNum(), activityDef.getType());
                    } else {
                        isDoneUserCoupon(newbieTask, userCoupons, 2, null, activityDef.getType());
                    }
                }
                if (shopManager && !newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                    addTask(userId, taskDTO, newbieTask, activityDef);
                    if (first) {
                        break;
                    }
                }
                if (shopManager && !newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode()) {
                    addTask(userId, taskDTO, newbieTask, activityDef);
                    if (first) {
                        break;
                    }
                }
                if (shopManager && !newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode()) {
                    addTask(userId, taskDTO, newbieTask, activityDef);
                    if (first) {
                        break;
                    }
                }
            }
        }
        if (CollectionUtils.isEmpty(taskDTO.getTaskDetails())) {
            return null;
        }
        return taskDTO;
    }

    @Override
    public CheckpointTaskSpaceDTO.TaskDTO queryDailyTaskDetail(Long merchantId, Long shopId, Long userId, boolean first) {
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = new CheckpointTaskSpaceDTO.TaskDTO();
        taskDTO.setMerchantId(merchantId);
        taskDTO.setShopId(shopId);
        taskDTO.setType(CheckpointTaskSpaceTypeEnum.DAILY_TASK.getCode());
        //查询店铺下的回填订单任务
        List<ActivityDef> fillBackOrderActivityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId,
                Arrays.asList(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(), ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()));
        if (!CollectionUtils.isEmpty(fillBackOrderActivityDefs)) {
            ActivityDef activityDef = fillBackOrderActivityDefs.get(0);
            if (activityDef.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
                addTaskDetail(userId, taskDTO, activityDef);
                return taskDTO;
            }
        }
        //查询店铺下的好评晒图任务
        List<ActivityDef> goodCommentActivityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId,
                Arrays.asList(ActivityDefTypeEnum.GOOD_COMMENT.getCode(), ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()));
        if (!CollectionUtils.isEmpty(goodCommentActivityDefs)) {
            ActivityDef activityDef = goodCommentActivityDefs.get(0);
            if (activityDef.getStock() > 0) {
                addTaskDetail(userId, taskDTO, activityDef);
                return taskDTO;
            }
        }
        //查询店铺下的收藏加购任务
        List<ActivityDef> favcartActivityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId,
                Arrays.asList(ActivityDefTypeEnum.FAV_CART.getCode(), ActivityDefTypeEnum.FAV_CART_V2.getCode()));
        if (!CollectionUtils.isEmpty(favcartActivityDefs)) {
            ActivityDef activityDef = favcartActivityDefs.get(0);
            if (activityDef.getStock() > 0) {
                addTaskDetail(userId, taskDTO, activityDef);
                return taskDTO;
            }
        }
        return null;
    }

    /***
     * 拼接任务详细，剩余任务数
     * @param taskDTO
     * @param activityDef
     */
    private void addTaskDetail(Long userId, CheckpointTaskSpaceDTO.TaskDTO taskDTO, ActivityDef activityDef) {
        NewbieTask newbieTask = new NewbieTask();
        newbieTask.setType(activityDef.getType());
        BigDecimal commission = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
        //扣除税率的佣金
        CommissionBO commissionBO = commissionService.commissionCalculate(userId, commission);
        newbieTask.setRebate(commissionBO.getCommission().toString());
        newbieTask.setActivityDefId(activityDef.getId());
        taskDTO.getTaskDetails().add(newbieTask);
    }

    private void addTask(Long userId, CheckpointTaskSpaceDTO.TaskDTO taskDTO, NewbieTask newbieTask, ActivityDef activityDef) {
        newbieTask.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = commissionService.commissionCalculate(userId, activityDef.getCommission());
        newbieTask.setRebate(commissionBO.getCommission().toString());
        if (activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            newbieTask.setRebate(activityDef.getCommission().toString());
        }
        taskDTO.getTaskDetails().add(newbieTask);
    }


    /***
     * 查询活动定义的已中奖信息
     * @param merchantId
     * @param shopId
     * @param defIds
     * @return
     */
    public Map<Long, List<UserCoupon>> queryActivityDefInfo(Long merchantId, Long shopId, Long userId, List<Long> defIds, List<Integer> types) {
        List<UserCoupon> userCoupons = userCouponService.queryByDefIds(merchantId, shopId, userId, defIds, types);
        if (CollectionUtils.isEmpty(userCoupons)) {
            return new HashMap<>();
        }
        Map<Long, List<UserCoupon>> resp = new HashMap<>();
        for (UserCoupon userCoupon : userCoupons) {
            List<UserCoupon> userCouponList = resp.get(userCoupon.getActivityDefId());
            if (CollectionUtils.isEmpty(userCouponList)) {
                userCouponList = new ArrayList<>();
            }
            userCouponList.add(userCoupon);
            resp.put(userCoupon.getActivityDefId(), userCouponList);
        }
        return resp;
    }

    /***
     * 判断是否做过
     * @param newbieTask
     * @param userCoupons
     * @param scene 1新手 2店长
     * @param type  活动定义类型
     */
    private void isDoneUserCoupon(NewbieTask newbieTask, List<UserCoupon> userCoupons, int scene, Integer startNum, Integer type) {
        if (scene == 1) {
            UserCoupon userCoupon = userCoupons.get(0);
            if (UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode() == userCoupon.getType()
                    && type == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode()) {
                newbieTask.setFinish(true);
            }
            if (UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode() == userCoupon.getType()
                    && type == ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode()) {
                newbieTask.setFinish(true);
            }
            if (UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode() == userCoupon.getType()
                    && type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                newbieTask.setFinish(true);
            }
        }
        if (scene == 2) {
            if (startNum != null) {
                List<UserCoupon> recommend_shopmanagers = new ArrayList<>();
                for (UserCoupon userCoupon : userCoupons) {
                    if (UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode() == userCoupon.getType()) {
                        recommend_shopmanagers.add(userCoupon);
                    }
                }
                if (recommend_shopmanagers.size() >= startNum
                        && type == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                    newbieTask.setFinish(true);
                }
            } else {
                UserCoupon userCoupon = userCoupons.get(0);
                if (UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode() == userCoupon.getType()
                        && type == ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode()) {
                    newbieTask.setFinish(true);
                }
                if (UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode() == userCoupon.getType()
                        && type == ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode()) {
                    newbieTask.setFinish(true);
                }
            }
        }
    }
}
