package mf.code.activity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.dao.ActivityTaskMapper;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityTaskUService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-04 下午1:43
 */
@Slf4j
@Service
public class ActivityTaskUServiceImpl extends ServiceImpl<ActivityTaskMapper, ActivityTask> implements ActivityTaskUService {
	private final ActivityTaskMapper activityTaskMapper;

	@Autowired
	public ActivityTaskUServiceImpl(ActivityTaskMapper activityTaskMapper) {
		this.activityTaskMapper = activityTaskMapper;
	}

	@Override
	public boolean reduceStock(Long activityTaskId) {
		int update = activityTaskMapper.reduceStock(activityTaskId, new Date());
		if (update == 0) {
			return false;
		}
		return true;
	}

	@Override
	public boolean addStock(Long activityTaskId) {
		int update = activityTaskMapper.addStock(activityTaskId, new Date());
		if (update == 0) {
			return false;
		}
		return true;
	}
}
