package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import mf.code.activity.repo.dao.ActivityMapper;
import mf.code.activity.repo.dao.ActivityPlanItemMapper;
import mf.code.activity.repo.dao.ActivityPlanMapper;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.repo.po.ActivityPlanItem;
import mf.code.activity.service.ActivityPlanService;
import mf.code.activity.service.ActivityService;
import mf.code.common.constant.ReduceOrAddEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ActivityPlanServiceImpl implements ActivityPlanService {

    @Autowired
    private ActivityPlanMapper activityPlanMapper;

    @Autowired
    private ActivityMapper activityMapper;

    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityPlanItemMapper activityPlanItemMapper;

    private static final String formatTime = "yyyyMMdd HH:mm:ss";
    private static final String formatDay = "yyyyMMdd";

    /**
     * 商户确认
     */
    @Override
    @Transactional
    public SimpleResponse confirm(Map map) {

        int sqlResult = activityPlanMapper.confirm(map);
        if (sqlResult == 1) {
            SimpleResponse response = activityService.addActivity(map);
            return response;
        } else {
            String msg = "更新plan状态=4执行返回0,plan=" + map.get("id") + ";merchant=" + map.get("merchantId");
            return new SimpleResponse(1, msg);
        }
    }

    /**
     * 商户发布
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int publish(Map map) {
        // 更新 status=5, utime=now FROM 活动计划 WHERE id=? AND mid=? AND status=4
        // 更新 status=1, utime=now FROM 活动 WHERE id IN ? AND mid=? AND status=0
        int i = activityPlanMapper.publish(map);
        if (i > 0) {
            i = activityMapper.publish(map);
            if (i > 0) {
                updateDays((Long) map.get("id"));
            }
        }
        return i;
    }

    /**
     * 排日期
     *
     * @param activityPlanId
     * @return
     */
    private int updateDays(Long activityPlanId) {
        Map param = new HashMap();
        param.put("activityPlanId", activityPlanId);
        List<ActivityPlanItem> items = activityPlanItemMapper.findAllItemByPlanIdPlus(param);

        Date now = new Date();
        for (int i = 0; i < items.size(); i++) {
            ActivityPlanItem item = items.get(i);
            Date date = DateUtil.addDay(now, i + 1);
            String dateStr = DateUtil.dateToString(date, "yyyyMMdd");
            item.setDatetimeStr(dateStr);
            //更新plan_item表中的显示日期
            activityPlanItemMapper.updateByPrimaryKeySelective(item);
        }

        //更新activity表
        //通过activityPlanId查询activity
        //截取时间后拼接 , 更新activity表的时间
        param.put("order", "id");
        param.put("direct", "asc");
        param.put("activity_plan_id", activityPlanId);
        List<Activity> activities = activityMapper.selectForCommonPlus(param);
        String temp = "";
        int counter = 0;
        for (Activity activity : activities) {
            String temp2 = DateUtil.dateToString(activity.getStartTime(), formatDay);
            if (!temp.equals(temp2)) {
                temp = temp2;
                counter++;
            }
            String dayStr = DateUtil.dateToString(DateUtil.addDay(new Date(), counter), formatDay);
            String startTimeStr = DateUtil.dateToString(activity.getStartTime(), formatTime);
            String endTimeStr = DateUtil.dateToString(activity.getEndTime(), formatTime);
            String startJoinStr = dayStr + " " + startTimeStr.split(" ")[1];
            String endJoinStr = dayStr + " " + endTimeStr.split(" ")[1];
            Date startTime = DateUtil.stringtoDate(startJoinStr, formatTime);
            Date endTime = DateUtil.stringtoDate(endJoinStr, formatTime);
            activity.setStartTime(startTime);
            activity.setEndTime(endTime);
            activityMapper.updateByPrimaryKeySelective(activity);
        }
        return 0;
    }

    /**
     * 更新计划表中刷单总数和保证金额度
     *
     * @param plan
     * @param goodsPrice
     * @param totalPerKeyJson
     * @return
     */
    @Override
    public int updateDepositByid(ActivityPlan plan, BigDecimal goodsPrice, String totalPerKeyJson, Long itemId) {
        double price = Double.parseDouble(goodsPrice.toString());

        //根据activityPlanId查询item , 重新计算total和deposit , 保证save和update通用
        Map param = new HashMap();
        param.put("activityPlanId", plan.getId());
        List<ActivityPlanItem> items = activityPlanItemMapper.findAllItemByPlanIdPlus(param);
        int activityPlanItemTotal = 0;
        for (ActivityPlanItem i : items) {
            activityPlanItemTotal = activityPlanItemTotal + getTotal(i.getTotalPerKeyJson());
        }
        double deposit = price * (activityPlanItemTotal);
        plan.setTotal(activityPlanItemTotal);
        plan.setDeposit(BigDecimal.valueOf(deposit));

        //插入plan表,更新total和保证金额度
        return activityPlanMapper.updateByPrimaryKeySelective(plan);
    }

    private int getTotal(String json) {
        List<Map> list = (List<Map>) JSON.parse(json);
        int total = 0;
        for (Map m : list) {
            total = total + Integer.parseInt(m.get("total").toString());
        }
        return total;
    }

    /**
     * 添加计划
     */
    @Override
    public int add(ActivityPlan record) {
        Date now = new Date();
        record.setCtime(now);
        record.setUtime(now);
        record.setStatus(1);
        record.setHitsPerDraw(0);
        return activityPlanMapper.insertSelective(record);
    }

    /**
     * 显示分页列表
     */
    @Override
    public PageInfo selectAll(String merchantId, int pageSize, int currentPage, Long shopId, Integer stat) {
        HashMap<String, Object> param = new HashMap<>(2);
        param.put("merchantId", merchantId);
        param.put("shopId", shopId == null ? "" : shopId);
        param.put("stat", stat);
        Page<Object> page = PageHelper.startPage(currentPage, pageSize);
        List<Map> list = activityPlanMapper.selectAll(param);
        PageInfo info = page.toPageInfo();
        info.setList(list);
        return info;
    }

    @Override
    public PageInfo selectAll(String merchantId, int pageSize, int currentPage, Integer stat) {
        return selectAll(merchantId, pageSize, currentPage, null, stat);
    }

    /**
     * 按条件查询
     *
     * @param params
     * @return
     */
    @Override
    public List<ActivityPlan> selectByParams(HashMap<String, Object> params) {
        return activityPlanMapper.selectByParams(params);
    }

    @Override
    public int updateByPrimaryKeySelectivePlus(ActivityPlan record) {
        return activityPlanMapper.updateByPrimaryKeySelectivePlus(record);
    }

    @Override
    public int updateByPrimaryKeySelectivePlusWithoutMerchantId(ActivityPlan record) {
        return activityPlanMapper.updateByPrimaryKeySelectivePlusWithoutMerchantId(record);
    }

    /**
     * real delete . not mark del
     */
    @Override
    public int del(Long id) {
        return activityPlanMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(ActivityPlan record) {
        return activityPlanMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updatePlus(ActivityPlan record) {
        Map m = new HashMap();
        m.put("status", record.getStatus());
        m.put("id", record.getId());
        m.put("merchantId", record.getMerchantId());
        return activityPlanMapper.updatePlus(m);
    }

    @Override
    public ActivityPlan findById(Long id) {
        return this.activityPlanMapper.selectByPrimaryKeyPlus(id);
    }

    @Override
    public Map findByIdPlus(Long id) {
        return activityPlanMapper.findByIdPlus(id);
    }

    @Override
    public List<ActivityPlan> findList() {
        return null;
    }

    public static final int CONDITION_PERSION_TOTAL = 1;
    public static final int CONDITION_DRAW_TIME = 3;
    public static final int CONDITION_MISSION_TIME = 5;

    /**
     * 校验 状态
     * 校验 活动计划.相关字段
     * |----开奖条件-上线后满足时间开奖 单位 分钟 > 10
     * |----活动中奖人数 > 0
     * |----中奖任务-需要完成时间 单位 分钟 > 5
     * |----开奖条件-满足人数开奖 单位 人数 > 0
     *
     * @param condPersionCount
     * @param condDrawTime
     * @param hitsPerDraw
     * @param missionNeedTime
     * @return
     */
    @Override
    public String assertSaveParam(int status,
                                  int condPersionCount,
                                  int condDrawTime,
                                  int hitsPerDraw,
                                  int missionNeedTime,
                                  Integer totalDay) {
        String msg = null;
        if (status != 1 && status != 2) {

            msg = "非法状态操作";
        } else if (condPersionCount < CONDITION_PERSION_TOTAL) {

            msg = "成团人数不能<" + CONDITION_PERSION_TOTAL + "人";
        } else if (condDrawTime < CONDITION_DRAW_TIME) {

            msg = "上线后开奖时间必须>" + CONDITION_DRAW_TIME + "分钟";
        } else if (hitsPerDraw < 0) {

            msg = "中奖人数必须>0";
        } else if (missionNeedTime < CONDITION_MISSION_TIME) {

            msg = "任务开奖时间必须>" + CONDITION_MISSION_TIME + "分钟";
        } else if (totalDay < 1) {
            msg = "计划天数必须>1";
        }
        return msg;
    }


    /**
     * 扣除红包任务库存
     *
     * @param id
     * @return
     */
    @Override
    public Integer reduceRedpackStock(Long id) {
        return activityPlanMapper.updateRedpackStock(id, ReduceOrAddEnum.REDUCE.getCode(), new Date());
    }

    /**
     * 返还红包任务库存
     *
     * @param id
     * @return
     */
    @Override
    public Integer addRedpackStock(Long id) {
        return activityPlanMapper.updateRedpackStock(id, ReduceOrAddEnum.ADD.getCode(), new Date());
    }

    @Override
    public int subtractTotalRedpackStockByPrimaryKey(Long activityPlanId) {
        return activityPlanMapper.subtractTotalRedpackStockByPrimaryKey(activityPlanId, new Date());
    }

    @Override
    public List<ActivityPlan> findByCtimePlus(String condDate) {
        List<ActivityPlan> byCtimePlus = activityPlanMapper.findByCtimePlus(condDate);
        return byCtimePlus;

    }

    @Override
    public SimpleResponse subBillResidue(Long activityPlanId) {
        SimpleResponse response = new SimpleResponse();



        return response;
    }

}
