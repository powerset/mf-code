package mf.code.activity.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.service.ActivityCommissionService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.common.service.CommonDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2019-02-14 14:36
 */
@Slf4j
@Service
public class ActivityCommissionServiceImpl implements ActivityCommissionService {

    @Autowired
    private CommonDictService commonDictService;
    /**
     * 查询佣金税率
     *
     * @return BigDecimal 税率
     */
    @Override
    public BigDecimal getCommissionRate() {
        String value = commonDictService.selectValueByTypeKey("checkpoint", "rate");
        // 如果上述查不到则采用默认10%，并打印错误
        if(null == value){
            log.error("财富大闯关佣金税率查询为空，目前采用10%默认税率");
            return new BigDecimal("10");
        }
        return new BigDecimal(value);
    }

    /**
     * 计算佣金
     */
    @Override
    public CommissionBO calculateCommission(Long userId, BigDecimal commissionDef) {
        BigDecimal commissionRate = getCommissionRate();
        CommissionBO commissionBO = new CommissionBO(userId, commissionDef);
        // 计算税费向下取整，自己获得佣金=总佣金-税费
        BigDecimal tax = commissionDef.multiply(commissionRate).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_DOWN);
        BigDecimal commission = commissionDef.subtract(tax);
        commissionBO.setRate(commissionRate);
        commissionBO.setCommission(commission);
        commissionBO.setTax(tax);
        // 查询需要向谁缴纳税费
        return commissionBO;
    }

}
