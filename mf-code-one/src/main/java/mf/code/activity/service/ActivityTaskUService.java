package mf.code.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.ActivityTask;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-04 下午1:42
 */
public interface ActivityTaskUService extends IService<ActivityTask> {
	/**
	 * 领取，减库存
	 * @param activityTaskId
	 * @return
	 */
	boolean reduceStock(Long activityTaskId);

	/**
	 * 超时退库存
	 */
	boolean addStock(Long activityTaskId);
}
