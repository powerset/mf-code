package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.bo.AmountBO;
import mf.code.activity.repo.dao.ActivityTaskMapper;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.TaskSpaceNodeDto;
import mf.code.api.seller.dto.ActivityTaskBO;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.JsonParseUtil;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-27 下午3:16
 */
@Slf4j
@Service
public class ActivityTaskServiceImpl extends ServiceImpl<ActivityTaskMapper, ActivityTask> implements ActivityTaskService {
	private final ActivityTaskMapper activityTaskMapper;

	@Autowired
	public ActivityTaskServiceImpl(ActivityTaskMapper activityTaskMapper) {
		this.activityTaskMapper = activityTaskMapper;
	}

	@Autowired
    private MerchantShopService merchantShopService;
	@Autowired
    private UserCouponService userCouponService;

	@Override
    public boolean saveOrUpdateFromActivityDef(ActivityTaskBO activityTaskBO) {
        // 修改商品
        // 1. 在原有商品上，另外再增加一个商品
        // 2. 在原有商品上，修改其中某个商品
        // 3. 在原有商品上，即有修改商品，又有新增商品
        // 修改思路 ：由23两点看不出是要新增还是要修改，所以直接简单粗爆的处理是，把所有原有纪录删除，重新插入新的数据

        ActivityDef activityDef = activityTaskBO.getActivityDef();
        BigDecimal fillBackOrderAmount = activityTaskBO.getFillbackOrderAmount();
        Integer fillBackOrderMissionNeedTime = 51840000;
        BigDecimal favCartAmount = activityTaskBO.getFavCartAmount();
        Integer favCartMissionNeedTime = activityTaskBO.getFavCartMissionNeedTime();
        BigDecimal goodCommentAmount = activityTaskBO.getGoodCommentAmout();
        Integer goodCommentMissionNeedTime = activityTaskBO.getGoodCommentMissionNeedTime();
        List<Long> goodsIdList = JSON.parseArray(activityDef.getGoodsIds(), Long.class);
        Long merchantId = activityDef.getMerchantId();
        Long shopId = activityDef.getShopId();
        Long activityDefId = activityDef.getId();
        Integer stockDef = activityDef.getStockDef();
        String serviceWx = activityDef.getServiceWx();
        // 20190119 更新客服微信号
        if(StringUtils.isNotBlank(activityDef.getServiceWx())){
            if(JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if (!org.springframework.util.CollectionUtils.isEmpty(csMapList)) {
                    //微信号
                    serviceWx = (String) csMapList.get(0).get("wechat");
                }
            }
        }

        Map<Integer, BigDecimal> typeAmount = new HashMap<>();
        typeAmount.put(ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode(), fillBackOrderAmount);
        typeAmount.put(ActivityTaskTypeEnum.FAVCART.getCode(), favCartAmount);
        typeAmount.put(ActivityTaskTypeEnum.GOOD_COMMENT.getCode(), goodCommentAmount);

        Map<Integer, List<Object>> typeAmountTime = new HashMap<>();
        List<Object> fillBackAmountTime = new ArrayList<>();
		fillBackAmountTime.add(fillBackOrderAmount);
		fillBackAmountTime.add(fillBackOrderMissionNeedTime);
		typeAmountTime.put(ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode(), fillBackAmountTime);

		List<Object> favCartAmountTime = new ArrayList<>();
		favCartAmountTime.add(favCartAmount);
		favCartAmountTime.add(favCartMissionNeedTime);
		typeAmountTime.put(ActivityTaskTypeEnum.FAVCART.getCode(), favCartAmountTime);

		List<Object> goodCommentAmountTime = new ArrayList<>();
		goodCommentAmountTime.add(goodCommentAmount);
		goodCommentAmountTime.add(goodCommentMissionNeedTime);
		typeAmountTime.put(ActivityTaskTypeEnum.GOOD_COMMENT.getCode(), goodCommentAmountTime);


        List<ActivityTask> activityTaskList = new ArrayList<>();
        QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityTask::getActivityDefId, activityDefId)
                .eq(ActivityTask::getDel, DelEnum.NO.getCode());
        List<ActivityTask> activityTasks = this.list(wrapper);

        if (CollectionUtils.isNotEmpty(activityTasks)) {
            boolean remove = this.remove(wrapper);
            if (!remove) {
                return false;
            }
        }

        for (Map.Entry<Integer, List<Object>> entry : typeAmountTime.entrySet()) {
            for (Long goodsId : goodsIdList) {
                ActivityTask activityTask = newActivityTask(merchantId, shopId, activityDefId, stockDef, serviceWx, entry.getKey(), entry.getValue(), goodsId);
                activityTaskList.add(activityTask);
            }
        }
        return this.saveBatch(activityTaskList);
    }

    private ActivityTask newActivityTask(Long merchantId, Long shopId, Long activityDefId, Integer stockDef, String serviceWx, Integer type, List<Object> amountTimeList, Long goodsId) {
        Date now = new Date();
        ActivityTask activityTask = new ActivityTask();
        activityTask.setMerchantId(merchantId);
        activityTask.setShopId(shopId);
        activityTask.setGoodsId(goodsId);
        activityTask.setActivityDefId(activityDefId);
        activityTask.setType(type);
        activityTask.setStartTime(now);
        activityTask.setEndTime(DateUtils.addYears(now, 100));
        activityTask.setMissionNeedTime((Integer) amountTimeList.get(1));
	    BigDecimal rpAmount = (BigDecimal) amountTimeList.get(0);
	    activityTask.setRpAmount(rpAmount);
        activityTask.setRpStockDef(stockDef);
        activityTask.setRpStock(stockDef);
        activityTask.setWechat(serviceWx);
        activityTask.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        activityTask.setStatus(ActivityTaskStatusEnum.SAVE.getCode());
	    activityTask.setDel(DelEnum.NO.getCode());
	    if (rpAmount.equals(BigDecimal.ZERO)) {
		    activityTask.setDel(DelEnum.YES.getCode());
	    }
        activityTask.setCtime(now);
        activityTask.setUtime(now);
        return activityTask;
    }

    @Override
    public SimpleResponse saveError() {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, String> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "activity_task表创建出错，相关未创建，to 百川");
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        response.setMessage("网络异常，请稍候重试");
        response.setData(resultVO);
        return response;
    }

    /**
     * 查询用户待完成红包任务列表
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public List<TaskSpaceNodeDto> missionList(Long merchantId, Long shopId, Long userId, Long goodsId) {
        List<TaskSpaceNodeDto> taskSpaceList = new ArrayList<>();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        // 判断红包任务列表(回填订单，好评返现，收藏加购)
        Map<String, Object> activityTaskParams = new HashMap<>();
        activityTaskParams.put("merchantId", merchantShop.getMerchantId());
        activityTaskParams.put("shopId", shopId);
        activityTaskParams.put("goodsId", goodsId);
        activityTaskParams.put("typeList", Arrays.asList(ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode(),
                ActivityTaskTypeEnum.FAVCART.getCode(), ActivityTaskTypeEnum.GOOD_COMMENT.getCode()));
        activityTaskParams.put("status", ActivityTaskStatusEnum.PUBLISHED.getCode());
        activityTaskParams.put("depositStatus", DepositStatusEnum.PAYMENT.getCode());
        activityTaskParams.put("del", DelEnum.NO.getCode());
        List<ActivityTask> activityTasks = activityTaskMapper.selectByParams(activityTaskParams);
        if(CollectionUtils.isEmpty(activityTasks)){
            return taskSpaceList;
        }
        Map<String, Object> goodsCommentParams = new HashMap<>();
        goodsCommentParams.put("merchantId", merchantShop.getMerchantId());
        goodsCommentParams.put("shopId", shopId);
        goodsCommentParams.put("userId", userId);
        goodsCommentParams.put("typeList", Arrays.asList(UserCouponTypeEnum.GOODS_COMMENT.getCode(),
                UserCouponTypeEnum.ORDER_RED_PACKET.getCode(), UserCouponTypeEnum.FAV_CART.getCode()));
        goodsCommentParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
        goodsCommentParams.put("order", "id");
        goodsCommentParams.put("direct", "desc");
        // 会不会存在很多条成功的记录
        List<UserCoupon> userCouponList = userCouponService.listPageByparams(goodsCommentParams);
        for (ActivityTask activityTask : activityTasks) {
            if(activityTask.getRpStock() <= 0){
                continue;
            }
            TaskSpaceNodeDto taskSpaceComment = new TaskSpaceNodeDto();
            changeTypeAndTag(activityTask, taskSpaceComment);
            taskSpaceComment.setAmount(activityTask.getRpAmount());
            if(org.springframework.util.CollectionUtils.isEmpty(userCouponList)){
                taskSpaceComment.setTotalAmount(BigDecimal.ZERO);
                taskSpaceComment.setLastTime(0L);
            }else{
                BigDecimal bigDecimal = BigDecimal.ZERO;
                for (UserCoupon userCoupon : userCouponList) {
                    if(activityTask.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()
                            && userCoupon.getType() == UserCouponTypeEnum.ORDER_RED_PACKET.getCode()){
                        bigDecimal = bigDecimal.add(userCoupon.getAmount());
                    }
                }
                taskSpaceComment.setTotalAmount(bigDecimal.setScale(2, RoundingMode.HALF_UP));
                taskSpaceComment.setLastTime(userCouponList.get(0).getCtime().getTime());
            }
            taskSpaceList.add(taskSpaceComment);
        }
        return taskSpaceList;
    }

    private void changeTypeAndTag(ActivityTask activityTask, TaskSpaceNodeDto taskSpaceComment) {
        if(activityTask == null || taskSpaceComment == null){
            return;
        }
        if(activityTask.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()){
            taskSpaceComment.setType(TaskSpaceTypeEnum.GOOD_COMMENT.getType());
            taskSpaceComment.setTag(TaskSpaceTypeEnum.GOOD_COMMENT.getTag());
        }else if(activityTask.getType() == ActivityTaskTypeEnum.FAVCART.getCode()){
            taskSpaceComment.setType(TaskSpaceTypeEnum.FAV_CART.getType());
            taskSpaceComment.setTag(TaskSpaceTypeEnum.FAV_CART.getTag());
        }else if(activityTask.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()){
            taskSpaceComment.setType(TaskSpaceTypeEnum.ORDER_BACK.getType());
            taskSpaceComment.setTag(TaskSpaceTypeEnum.ORDER_BACK.getTag());
        }
    }

    @Override
    public Map<String, Object> queryRpAmountByActivityDefId(Long activityDefId) {
        Map<String, Object> resultVO = new HashMap<>();
        QueryWrapper<ActivityTask> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ActivityTask::getActivityDefId, activityDefId)
		        .eq(ActivityTask::getDel, DelEnum.NO.getCode());
        List<ActivityTask> activityTasks = this.list(queryWrapper);

        BigDecimal fillBackOrderAmount = BigDecimal.ZERO;
        BigDecimal favCartAmount = BigDecimal.ZERO;
        BigDecimal goodCommentAmount = BigDecimal.ZERO;

        for (ActivityTask activityTask : activityTasks) {
            if (activityTask.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()) {
	            fillBackOrderAmount = activityTask.getRpAmount();
                break;
            }
        }

        for (ActivityTask activityTask : activityTasks) {
            if (activityTask.getType() == ActivityTaskTypeEnum.FAVCART.getCode()) {
				favCartAmount = activityTask.getRpAmount();
                break;
            }
        }
        for (ActivityTask activityTask : activityTasks) {
            if (activityTask.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()) {
	            goodCommentAmount = activityTask.getRpAmount();
                break;
            }
        }
	    resultVO.put("fillBackOrderAmount", fillBackOrderAmount);
	    resultVO.put("favCartAmount", favCartAmount);
	    resultVO.put("goodCommentAmount", goodCommentAmount);
        return resultVO;
    }

	@Override
	public SimpleResponse selectErrorByActivityDefId(Long activityDefId) {
		SimpleResponse<Object> response = new SimpleResponse<>();
		Map<String, String> resultVO = new HashMap<>();
		resultVO.put("errorMessage", "通过activityDefId查询activity_task时出错，to 百川 activityDefId = "+activityDefId);
		response.setStatusEnum(ApiStatusEnum.ERROR_DB);
		response.setMessage("网络异常，请稍候重试");
		response.setData(resultVO);
		return response;
	}

	@Override
	public boolean supplyStock(Long activityDefId, Integer stockApply) {
    	Integer update = activityTaskMapper.supplyStock(activityDefId, stockApply, new Date());
		return update != 0;
	}

	@Override
	public BigDecimal getTotalAmount(Long activityDefId) {
		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityTask::getActivityDefId, activityDefId);
		List<ActivityTask> activityTasks = this.list(wrapper);
		BigDecimal fillBackOrderAmount = BigDecimal.ZERO;
		BigDecimal favCartAmount = BigDecimal.ZERO;
		BigDecimal goodCommentAmount = BigDecimal.ZERO;
		for (ActivityTask activityTask : activityTasks) {
			if (activityTask.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()) {
				fillBackOrderAmount = activityTask.getRpAmount();
			}
			if (activityTask.getType() == ActivityTaskTypeEnum.FAVCART.getCode()) {
				favCartAmount = activityTask.getRpAmount();
			}
			if (activityTask.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()) {
				goodCommentAmount = activityTask.getRpAmount();
			}
		}
		return fillBackOrderAmount.add(favCartAmount).add(goodCommentAmount);
	}

    /**
     * 根据条件查询列表
     *
     * @param params
     * @return
     */
    @Override
    public List<ActivityTask> selectByParams(Map<String, Object> params) {
        return activityTaskMapper.selectByParams(params);
    }

	@Override
	public BigDecimal getDeposit(Long activityDefId) {
		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityTask::getActivityDefId, activityDefId);
		List<ActivityTask> activityTasks = this.list(wrapper);

		BigDecimal deposit = BigDecimal.ZERO;
		for (ActivityTask activityTask : activityTasks) {
			deposit = deposit.add(activityTask.getRpAmount().multiply(new BigDecimal(activityTask.getRpStock())));
		}
		return deposit;
	}

	@Override
	public Integer getStock(Long activityDefId) {
		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityTask::getActivityDefId, activityDefId);
		List<ActivityTask> activityTasks = this.list(wrapper);

		Integer stock  = 0;
		for (ActivityTask activityTask : activityTasks) {
			stock = stock + activityTask.getRpStock();
		}
		return stock;
	}

	@Override
	public AmountBO getAmountBO(Long activityDefId) {
		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityTask::getActivityDefId, activityDefId);
		List<ActivityTask> activityTasks = this.list(wrapper);
		BigDecimal fillBackOrderAmount = BigDecimal.ZERO;
		BigDecimal favCartAmount = BigDecimal.ZERO;
		BigDecimal goodCommentAmount = BigDecimal.ZERO;
		for (ActivityTask activityTask : activityTasks) {
			if (activityTask.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()) {
				fillBackOrderAmount = activityTask.getRpAmount();
			}
			if (activityTask.getType() == ActivityTaskTypeEnum.FAVCART.getCode()) {
				favCartAmount = activityTask.getRpAmount();
			}
			if (activityTask.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()) {
				goodCommentAmount = activityTask.getRpAmount();
			}
		}
		AmountBO amountBO = new AmountBO();
		amountBO.setFillBackOrderAmount(fillBackOrderAmount);
		amountBO.setGoodCommentAmount(goodCommentAmount);
		amountBO.setFavCartAmount(favCartAmount);
		return amountBO;
	}

    @Override
    public List<ActivityTask> selectByMerchantIdShopIdFromActivityTask(Long merchantId, Long shopId) {
        Map m = new HashMap();
        m.put("merchantId", merchantId);
        m.put("shopId", shopId);
        return activityTaskMapper.selectByMerchantIdShopIdFromActivityTask(m);
    }

    @Override
    public Long getStockByGoodsId(Long goodsId) {
        return activityTaskMapper.getStockByGoodsId(goodsId);
    }

	@Override
	public boolean updatePublished(ActivityTaskBO activityTaskBO) {
		ActivityDef activityDef = activityTaskBO.getActivityDef();
		Long activityDefId = activityDef.getId();
		Integer goodCommentMissionNeedTime = activityTaskBO.getGoodCommentMissionNeedTime();


		List<ActivityTask> activityTaskList = new ArrayList<>();
		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityTask::getActivityDefId, activityDefId)
				.in(ActivityTask::getType, Arrays.asList(ActivityTaskTypeEnum.GOOD_COMMENT.getCode(), ActivityTaskTypeEnum.FAVCART.getCode()))
				.eq(ActivityTask::getDel, DelEnum.NO.getCode());
		List<ActivityTask> goodCommentActivityTasks = this.list(wrapper);

		for (ActivityTask activityTask : goodCommentActivityTasks) {
			activityTask.setMissionNeedTime(goodCommentMissionNeedTime);
			activityTaskList.add(activityTask);
		}

		return this.updateBatchById(activityTaskList);
	}
}
