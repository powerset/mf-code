package mf.code.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.bo.AmountBO;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.seller.dto.ActivityTaskBO;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-27 下午3:15
 */
public interface ActivityTaskService extends IService<ActivityTask> {
    /**
     * 通过 活动定义 创建 红包任务 （目前 只有当 定义红包活动时 才会记录）
     *
     * @param activityTaskBO
     * @return
     */
    boolean saveOrUpdateFromActivityDef(ActivityTaskBO activityTaskBO);

    SimpleResponse saveError();

    /**
     * 在商品详情页面,展示的该用户的可以做的任务
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    List missionList(Long merchantId, Long shopId, Long userId, Long goodsId);



    Map<String, Object> queryRpAmountByActivityDefId(Long activityDefId);

    SimpleResponse selectErrorByActivityDefId(Long activityDefId);

    boolean supplyStock(Long activityDefId, Integer stockApply);

    BigDecimal getTotalAmount(Long adid);

    /**
     * 根据条件查询列表
     * @param params
     * @return
     */
    List<ActivityTask> selectByParams(Map<String, Object> params);

    /**
     * 查询剩余保证金额
     * @param activityDefId
     * @return
     */
    BigDecimal getDeposit(Long activityDefId);

    /**
     * 查询红包剩余库存
     * @param activityDefId
     * @return
     */
    Integer getStock(Long activityDefId);

	AmountBO getAmountBO(Long activityDefId);

    List<ActivityTask> selectByMerchantIdShopIdFromActivityTask(Long merchantId, Long shopId);

    Long getStockByGoodsId(Long goodsId);

    boolean updatePublished(ActivityTaskBO activityTaskBO);
}
