package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.bo.AmountBO;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.*;
import mf.code.api.feignclient.CommAppService;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.SellerActivityDefV4Service;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-23 19:15
 */
@Slf4j
@Service
public class ActivityDefCheckServiceImpl implements ActivityDefCheckService {

    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private SellerActivityDefV4Service sellerActivityDefV4Service;
    @Autowired
    private CommAppService commAppService;

    @Override
    public ActivityDef updateFromActivityDefV2Req(ActivityDefV2Req activityDefV2Req) {
        // 获取参数
        Long adid = Long.valueOf(activityDefV2Req.getActivityDefId());
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(adid);

        activityDef.setMerchantId(Long.valueOf(activityDefV2Req.getMerchantId()));
        activityDef.setShopId(Long.valueOf(activityDefV2Req.getShopId()));
        activityDef.setType(Integer.valueOf(activityDefV2Req.getType()));

        String goodsIds = activityDefV2Req.getGoodsIdList();
        BigDecimal goodsPrice = activityDefV2Req.getGoodsPrice();
        BigDecimal depositDef = activityDefV2Req.getDepositDef();
        if (StringUtils.isNotBlank(goodsIds)) {
            List<Long> goodsIdList = new ArrayList<>();
            String[] split = goodsIds.split(",");
            for (String goodsId : split) {
                goodsIdList.add(Long.valueOf(goodsId));
            }
            activityDef.setGoodsIds(JSON.toJSONString(goodsIdList));
        }
        String goodsId = activityDefV2Req.getGoodsId();
        if (StringUtils.isNotBlank(goodsId)) {
            activityDef.setGoodsId(Long.valueOf(goodsId));
        }
        if (null != goodsPrice) {
            activityDef.setGoodsPrice(goodsPrice);
        }
        String stockDef = activityDefV2Req.getStockDef();
        if (StringUtils.isNotBlank(stockDef)) {
            activityDef.setStockDef(Integer.valueOf(stockDef));
            activityDef.setStock(Integer.valueOf(stockDef));
        }
        if (null != depositDef) {
            activityDef.setDepositDef(depositDef);
            activityDef.setDeposit(depositDef);
        }
        String keyWord = activityDefV2Req.getKeyWord();
        if (StringUtils.isNotBlank(keyWord)) {
            String[] split = keyWord.split(",");
            activityDef.setKeyWord(JSON.toJSONString(Arrays.asList(split)));
        }
        String condDrawTime = activityDefV2Req.getCondDrawTime();
        if (StringUtils.isNotBlank(condDrawTime)) {
            // 分
            activityDef.setCondDrawTime(Integer.valueOf(condDrawTime));
        }
        condDrawTime = activityDefV2Req.getGoodCommentMissionNeedTime();
        if (StringUtils.isNotBlank(condDrawTime)) {
            // 分
            activityDef.setCondDrawTime(Integer.valueOf(condDrawTime));
        }
        String condPersionCount = activityDefV2Req.getCondPersionCount();
        if (StringUtils.isNotBlank(condPersionCount)) {
            activityDef.setCondPersionCount(Integer.valueOf(condPersionCount));
        }
        String hitsPerDraw = activityDefV2Req.getHitsPerDraw();
        if (StringUtils.isNotBlank(hitsPerDraw)) {
            activityDef.setHitsPerDraw(Integer.valueOf(hitsPerDraw));
        }
        String missionNeedTime = activityDefV2Req.getMissionNeedTime();
        if (StringUtils.isNotBlank(missionNeedTime)) {
            activityDef.setMissionNeedTime(Integer.valueOf(missionNeedTime));
        }
        String goodCommentMissionNeedTime = activityDefV2Req.getGoodCommentMissionNeedTime();
        if (StringUtils.isNotBlank(goodCommentMissionNeedTime)) {
            activityDef.setMissionNeedTime(Integer.valueOf(goodCommentMissionNeedTime));
        }
        String merchantCouponJson = activityDefV2Req.getMerchantCouponJson();
        if (StringUtils.isNotBlank(merchantCouponJson)) {
            List<Long> merchantCouponList = new ArrayList<>();
            String[] split = merchantCouponJson.split(",");
            for (String merchantCoupon : split) {
                if (!RegexUtils.StringIsNumber(merchantCoupon)) {
                    continue;
                }
                merchantCouponList.add(Long.valueOf(merchantCoupon));
            }
            if (!CollectionUtils.isEmpty(merchantCouponList)) {
                activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
            }
        }
        String serviceWx = activityDefV2Req.getServiceWx();
        String serviceWxPic = activityDefV2Req.getServiceWxPic();
        if (StringUtils.isNotBlank(serviceWx) || StringUtils.isNotBlank(serviceWxPic)) {
            List<Map<String, String>> csMapList = new ArrayList<>();
            Map<String, String> csMap = new HashMap<>(2);
            csMap.put("wechat", serviceWx);
            csMap.put("pic", serviceWxPic);
            csMapList.add(csMap);
            activityDef.setServiceWx(JSONArray.toJSONString(csMapList));
        }

        String weighting = activityDefV2Req.getWeighting();
        if (StringUtils.isNotBlank(weighting)) {
            activityDef.setWeighting(Integer.valueOf(weighting));
        }
        // 设置佣金
        BigDecimal commission = activityDefV2Req.getCommission();
        if (commission != null && commission.compareTo(BigDecimal.ONE) >= 0) {
            activityDef.setCommission(commission);
        }

        boolean isSuccess = activityDefService.saveOrUpdate(activityDef);
        if (!isSuccess) {
            return null;
        }

        return activityDef;
    }

    /**
     * 能否创建活动
     *
     * @param merchantId    商户id
     * @param shopId        店铺id
     * @param type          活动类型
     * @param goodsId       商品id
     * @param goodsIdList   商品组ids
     * @param activityDefId
     * @return
     */
    @Override
    public SimpleResponse<Object> createAble(Long merchantId, Long shopId, Integer type, Long goodsId, List<Long> goodsIdList, Long activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        // 如果是修改，在一下情况下 可以不用校验，直接返回 校验通过
        // 是否需要校验 创建肯定校验， 更新时如果不涉及到商品的修改，就可以不用校验
        // 如何判断是否是更新操作，即 adid != Null 。
        // 如何判断商品没有被修改 传入的goodsIds 或者 goodsId 和 数据库中的值 是否 一样, 一样则不用校验
        if (activityDefId != null) {
            ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
            Long goodsIdDB = activityDef.getGoodsId();
            String goodsIdsDB = activityDef.getGoodsIds();
            List<Long> goodsIdListDB = null;
            if (StringUtils.isNotBlank(goodsIdsDB) && JsonParseUtil.booJsonArr(goodsIdsDB)) {
                goodsIdListDB = JSON.parseArray(goodsIdsDB, Long.class);
            }

            if (null != goodsId && goodsId.equals(goodsIdDB)) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            if (null != goodsIdList && null != goodsIdListDB) {
                Collection<Long> intersection = CollectionUtils.intersection(goodsIdListDB, goodsIdList);
                if (intersection.size() == goodsIdList.size()) {
                    response.setStatusEnum(ApiStatusEnum.SUCCESS);
                    return response;
                }
            }
            if (type.equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            if (type.equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
        }

        if (type.equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
            // 校验是否有创建 幸运大转盘 的资格
            return existActivityDef(merchantId, shopId, Arrays.asList(type));
        }
        if (type.equals(ActivityDefTypeEnum.NEW_MAN.getCode())) {
            // 校验是否有创建 原 新人有礼活动 的资格
            return createAble4Gift(shopId, Arrays.asList(type), goodsId);
        }
        if (type.equals(ActivityDefTypeEnum.ASSIST.getCode())) {
            // 校验是否有创建 助力裂变活动 的资格
            return createAble4Gift(shopId, Arrays.asList(ActivityDefTypeEnum.ASSIST.getCode(), ActivityDefTypeEnum.ASSIST_V2.getCode()), goodsId);
        }
        // 助力赠品活动校验条件
        if (type.equals(ActivityDefTypeEnum.ASSIST_V2.getCode())) {
            // 校验是否有创建 助力裂变活动 的资格
            return createAble4Gift(shopId, Arrays.asList(ActivityDefTypeEnum.ASSIST.getCode(), ActivityDefTypeEnum.ASSIST_V2.getCode()), goodsId);
        }
        if (type.equals(ActivityDefTypeEnum.ORDER_BACK.getCode())) {
            // 校验是否有创建 免单商品 的资格
            return createAble4Gift(shopId, Arrays.asList(ActivityDefTypeEnum.ORDER_BACK.getCode(), ActivityDefTypeEnum.ORDER_BACK_V2.getCode()), goodsId);
        }
        if (type.equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())) {
            // 校验是否有创建 免单商品 的资格
            return createAble4Gift(shopId, Arrays.asList(ActivityDefTypeEnum.ORDER_BACK.getCode(), ActivityDefTypeEnum.ORDER_BACK_V2.getCode()), goodsId);
        }
        if (type.equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            // 校验是否有创建 红包活动 的资格
            List<Long> activityDefIds = new ArrayList<>();
            activityDefIds.add(activityDefId);
            return createAble4RedPacket(shopId, goodsIdList, activityDefIds, null);
        }
        if (type.equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())) {
            // 校验是否有创建 回填订单 的资格(存在就不能再创建)
            return this.existActivityDef(merchantId, shopId, Arrays.asList(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(), ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()));
        }
        // 校验是否有创建 回填订单 的资格(存在就不能再创建)
        if (type.equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
            return this.existActivityDef(merchantId, shopId, Arrays.asList(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(), ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()));
        }
        if (type.equals(ActivityDefTypeEnum.FAV_CART.getCode())) {
            // 校验是否有创建 收藏加购 的资格
            QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityDef::getShopId, shopId)
                    .in(ActivityDef::getType, Arrays.asList(ActivityDefTypeEnum.FAV_CART.getCode(), ActivityDefTypeEnum.FAV_CART_V2.getCode()))
                    .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                    .in(ActivityDef::getStatus, Arrays.asList(ActivityDefStatusEnum.SAVE.getCode(),
                            ActivityDefStatusEnum.AUDIT_WAIT.getCode(),
                            ActivityDefStatusEnum.AUDIT_SUCCESS.getCode(),
                            ActivityDefStatusEnum.PUBLISHED.getCode()))
                    .orderByDesc(ActivityDef::getId)
            ;
            List<ActivityDef> activityDefs = activityDefService.list(wrapper);
            // 空， 有资格
            if (CollectionUtils.isEmpty(activityDefs)) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            List<Long> activityDefIds = new ArrayList<>();
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getGoodsId() != null && activityDef.getGoodsId() == -1) {
                    return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1, "警告：您已经创建了全店的活动！");
                }
                if (activityDefIds.indexOf(activityDef.getId()) == -1) {
                    activityDefIds.add(activityDef.getId());
                }
            }
            if (CollectionUtils.isEmpty(goodsIdList) && goodsId != null && goodsId > 0) {
                if (goodsIdList == null) {
                    goodsIdList = new ArrayList<>();
                }
                goodsIdList.add(goodsId);
            }
            return createAble4RedPacket(shopId, goodsIdList, activityDefIds, ActivityTaskTypeEnum.FAVCART);
        }
        // 校验是否有创建 收藏加购 的资格
        if (type.equals(ActivityDefTypeEnum.FAV_CART_V2.getCode())) {
            QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityDef::getShopId, shopId)
                    .in(ActivityDef::getType, Arrays.asList(ActivityDefTypeEnum.FAV_CART.getCode(), ActivityDefTypeEnum.FAV_CART_V2.getCode()))
                    .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                    .in(ActivityDef::getStatus, Arrays.asList(ActivityDefStatusEnum.SAVE.getCode(),
                            ActivityDefStatusEnum.AUDIT_WAIT.getCode(),
                            ActivityDefStatusEnum.AUDIT_SUCCESS.getCode(),
                            ActivityDefStatusEnum.PUBLISHED.getCode()))
                    .orderByDesc(ActivityDef::getId)
            ;
            List<ActivityDef> activityDefs = activityDefService.list(wrapper);
            // 空， 有资格
            if (CollectionUtils.isEmpty(activityDefs)) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            List<Long> activityDefIds = new ArrayList<>();
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getGoodsId() != null && activityDef.getGoodsId() == -1) {
                    return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1, "警告：您已经创建了全店的活动！");
                }
                if (activityDefIds.indexOf(activityDef.getId()) == -1) {
                    activityDefIds.add(activityDef.getId());
                }
            }
            if (CollectionUtils.isEmpty(goodsIdList) && goodsId != null && goodsId > 0) {
                if (goodsIdList == null) {
                    goodsIdList = new ArrayList<>();
                }
                goodsIdList.add(goodsId);
            }
            return createAble4RedPacket(shopId, goodsIdList, activityDefIds, ActivityTaskTypeEnum.FAVCART);
        }
        if (type.equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode())) {
            // 校验是否有创建 好评晒图 的资格
            QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityDef::getShopId, shopId)
                    .eq(ActivityDef::getType, type)
                    .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                    .in(ActivityDef::getStatus, Arrays.asList(ActivityDefStatusEnum.SAVE.getCode(),
                            ActivityDefStatusEnum.AUDIT_WAIT.getCode(),
                            ActivityDefStatusEnum.AUDIT_SUCCESS.getCode(),
                            ActivityDefStatusEnum.PUBLISHED.getCode()))
                    .orderByDesc(ActivityDef::getId)
            ;
            List<ActivityDef> activityDefs = activityDefService.list(wrapper);
            // 空， 有资格
            if (CollectionUtils.isEmpty(activityDefs)) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            List<Long> activityDefIds = new ArrayList<>();
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getGoodsId() != null && activityDef.getGoodsId() == -1) {
                    return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1, "警告：您已经创建了全店的活动！");
                }
                if (activityDefIds.indexOf(activityDef.getId()) == -1) {
                    activityDefIds.add(activityDef.getId());
                }
            }
            if (CollectionUtils.isEmpty(goodsIdList) && goodsId != null && goodsId > 0) {
                if (goodsIdList == null) {
                    goodsIdList = new ArrayList<>();
                }
                goodsIdList.add(goodsId);
            }
            return createAble4RedPacket(shopId, goodsIdList, activityDefIds, ActivityTaskTypeEnum.GOOD_COMMENT);
        }
        /*
         * 校验是否有创建 好评晒图 的资格
         * 去除喜销宝人工审核阶段，没有商品信息
         */
        if (type.equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode())) {
            return this.existActivityDef(merchantId, shopId, Arrays.asList(ActivityDefTypeEnum.GOOD_COMMENT.getCode(), ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()));
        }

        if (type.equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
            // 校验是否有创建 拆红包 的资格
            return this.existActivityDef(merchantId, shopId, Arrays.asList(type));
        }

        // 校验是否有创建 新手任务 的资格
        if (type.equals(ActivityDefTypeEnum.NEWBIE_TASK.getCode())) {

//            SimpleResponse simpleResponse = sellerActivityDefV4Service.getOpenRedPackActivityInfo(merchantId, shopId);
//            if (simpleResponse.error() || simpleResponse.getData() == null) {
//                return simpleResponse;
//            }
            return this.existActivityDef(merchantId, shopId, Arrays.asList(type));
        }

        // 校验是否有创建 店长任务 的资格
        if (type.equals(ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode())) {
            return this.existActivityDef(merchantId, shopId, Arrays.asList(type));
        }

        // 校验是否有创建 全额报销 的资格
        if (type.equals(ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode())) {
            return this.existActivityDef(merchantId, shopId, Arrays.asList(type));
        }

        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
        response.setMessage("所传的活动类型type和校验的活动类型type无法对应，须沟通解决 to 木子 and 百川");
        return response;
    }

    private SimpleResponse<Object> existActivityDef(Long merchantId, Long shopId, List<Integer> typeList) {
        // 校验是否有创建 回填订单 的资格
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .in(ActivityDef::getType, typeList)
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                .in(ActivityDef::getStatus, Arrays.asList(ActivityDefStatusEnum.SAVE.getCode(),
                        ActivityDefStatusEnum.AUDIT_WAIT.getCode(),
                        ActivityDefStatusEnum.AUDIT_SUCCESS.getCode(),
                        ActivityDefStatusEnum.PUBLISHED.getCode()))
        ;
        List<ActivityDef> activityDefs = activityDefService.list(wrapper);
        // 空， 有资格
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (CollectionUtils.isEmpty(activityDefs)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        // 存在则 判断存在的活动中，是否有待审核 和 已发布的状态，有则无法创建
        return checkActivityDefStatus4createAble(activityDefs);
    }

    private SimpleResponse<Object> createAble4RedPacket(Long shopId, List<Long> goodsIdList, List<Long> activityDefIds, ActivityTaskTypeEnum taskTypeEnum) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验是否有创建 红包活动 的资格
        // goodsIdList 将要创建的商品组ids,
        if (CollectionUtils.isEmpty(goodsIdList)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        wrapper.in("activity_def_id", activityDefIds);
        if (!CollectionUtils.isEmpty(goodsIdList)) {
            wrapper.in("goods_id", goodsIdList);
        }
        if (taskTypeEnum != null) {
            wrapper.eq("type", taskTypeEnum.getCode());
        }
        wrapper.eq("del", DelEnum.NO.getCode());
        List<ActivityTask> activityTasks = activityTaskService.list(wrapper);
        // 空， 有资格
        if (CollectionUtils.isEmpty(activityTasks)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        // 所有活动 对应的 goodsId， 现在要排除其本身的商品id
        for (ActivityTask activityTask : activityTasks) {
            Long gid = activityTask.getGoodsId();
            if (activityTask.getStatus() == ActivityTaskStatusEnum.AUDIT_WAIT.getCode()) {
                String info = "您已有此商品的红包活动正在审核中，无法再次创建";
                String error = "您已有此商品的红包活动正在审核中，无法再次创建:" + gid;
                return sendErrorMessage(error, info);
            }
            if (activityTask.getStatus() == ActivityTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                String info = "您已有此商品的红包活动已审核通过，无法再次创建";
                String error = "您已有此商品的红包活动已审核通过，无法再次创建：" + gid;
                return sendErrorMessage(error, info);
            }
            if (activityTask.getStatus() == ActivityTaskStatusEnum.PUBLISHED.getCode()) {
                String info = "您已有此商品的红包活动正在进行中，无法再次创建";
                String error = "您已有此商品的红包活动正在进行中，无法再次创建：" + gid;
                return sendErrorMessage(error, info);
            }
            if (activityTask.getStatus() == ActivityTaskStatusEnum.SAVE.getCode()) {
                String info = "您已有此商品的红包活动 待支付，无法再次创建";
                String error = "您已有此商品的红包活动 待支付，无法再次创建：" + gid;
                return sendErrorMessage(error, info);
            }
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private SimpleResponse<Object> createAble4Gift(Long shopId, List<Integer> typeList, Long goodsId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 能否创建 和 商品相关
        // 查看此商品id是否已经创建了活动：先定位店铺，再定位要创建的活动类型，此类型下是否有商品id的活动存在
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        wrapper.in("type", typeList);
        wrapper.eq("goods_id", goodsId);
        wrapper.eq("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.list(wrapper);

        // 空， 有资格
        if (CollectionUtils.isEmpty(activityDefs)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        // 存在则 判断存在的活动中，是否有待审核 和 已发布的状态，有则无法创建
        return checkActivityDefStatus4createAble(activityDefs);
    }

    private SimpleResponse<Object> createAble4LuckyWheel(Long shopId, Integer type) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        wrapper.eq("type", type);
        wrapper.eq("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.list(wrapper);
        // 空， 有资格
        if (CollectionUtils.isEmpty(activityDefs)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        // 非空校验
        return checkActivityDefStatus4createAble(activityDefs);
    }

    private SimpleResponse<Object> createAble4OpenRedPacket(Long shopId, Integer type) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        wrapper.eq("type", type);
        wrapper.eq("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.list(wrapper);
        // 空， 有资格
        if (CollectionUtils.isEmpty(activityDefs)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        // 非空校验
        return checkActivityDefStatus4createAble(activityDefs);
    }

    private SimpleResponse<Object> checkActivityDefStatus4createAble(List<ActivityDef> activityDefs) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        for (ActivityDef activityDef : activityDefs) {
            if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
                log.info("您已有此活动 待支付，无法再次创建");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                response.setMessage("您已有此活动 待支付，无法再次创建");
                return response;
            }
            if (activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
                log.info("您已有活动正在审核中，无法再次创建");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                response.setMessage("您已有活动正在审核中，无法再次创建");
                return response;
            }
            if (activityDef.getStatus() == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                log.info("您目前还有未完成的活动，不能创建新的活动，如需对现有活动增加库存 ，请通过补库存操作完成。");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                response.setMessage("您目前还有未完成的活动，不能创建新的活动，如需对现有活动增加库存 ，请通过补库存操作完成。");
                return response;
            }
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> sendErrorMessage(String errorMessage, String infoMessage) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        log.error(errorMessage);
        Map<String, String> resultVO = new HashMap<>();
        resultVO.put("errorMessage", errorMessage);
        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
        response.setMessage(infoMessage);
        response.setData(resultVO);
        return response;
    }

    @Override
    public ActivityDef saveOrUpdateActivityDefFromActivityDefV2Req(ActivityDefV2Req activityDefV2Req) {


        Date now = new Date();
        ActivityDef activityDef = new ActivityDef();
        String activityDefId = activityDefV2Req.getActivityDefId();
        if (StringUtils.isNotBlank(activityDefId) && RegexUtils.StringIsNumber(activityDefId)) {
            activityDef = activityDefService.getById(activityDefId);
        }
        // 初始化数据（非商户填写）
        activityDef.setMerchantId(Long.valueOf(activityDefV2Req.getMerchantId()));
        activityDef.setShopId(Long.valueOf(activityDefV2Req.getShopId()));
        activityDef.setType(Integer.valueOf(activityDefV2Req.getType()));

        activityDef.setStartTime(now);
        activityDef.setEndTime(DateUtils.addYears(now, 100));
        activityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        activityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        activityDef.setPublishTime(new Date(0));
        activityDef.setCondDrawTime(10080);
        activityDef.setWeighting(WeightingEnum.YES.getCode());
        activityDef.setDel(DelEnum.NO.getCode());
        activityDef.setApplyTime(now);
        activityDef.setJeton(new BigDecimal(0.00));
        activityDef.setUtime(now);
        activityDef.setCtime(now);
        // 商户填写的数据
        String goodsIds = activityDefV2Req.getGoodsIdList();
        BigDecimal goodsPrice = activityDefV2Req.getGoodsPrice();
        BigDecimal depositDef = activityDefV2Req.getDepositDef();
        if (StringUtils.isNotBlank(goodsIds)) {
            List<Long> goodsIdList = new ArrayList<>();
            String[] split = goodsIds.split(",");
            for (String goodsId : split) {
                goodsIdList.add(Long.valueOf(goodsId));
            }
            activityDef.setGoodsIds(JSON.toJSONString(goodsIdList));
        }
        String goodsId = activityDefV2Req.getGoodsId();
        if (StringUtils.isNotBlank(goodsId)) {
            activityDef.setGoodsId(Long.valueOf(goodsId));
        }
        if (null != goodsPrice) {
            activityDef.setGoodsPrice(goodsPrice);
        }
        String stockDef = activityDefV2Req.getStockDef();
        if (StringUtils.isNotBlank(stockDef)) {
            activityDef.setStockDef(Integer.valueOf(stockDef));
            activityDef.setStock(Integer.valueOf(stockDef));
        }
        if (null != depositDef) {
            activityDef.setDepositDef(depositDef);
            activityDef.setDeposit(depositDef);
        }
        String keyWord = activityDefV2Req.getKeyWord();
        if (StringUtils.isNotBlank(keyWord)) {
            String[] split = keyWord.split(",");
            activityDef.setKeyWord(JSON.toJSONString(Arrays.asList(split)));
        }
        String condDrawTime = activityDefV2Req.getCondDrawTime();
        if (StringUtils.isNotBlank(condDrawTime)) {
            // 分
            activityDef.setCondDrawTime(Integer.valueOf(condDrawTime));
        }
        condDrawTime = activityDefV2Req.getGoodCommentMissionNeedTime();
        if (StringUtils.isNotBlank(condDrawTime)) {
            // 分
            activityDef.setCondDrawTime(Integer.valueOf(condDrawTime));
        }
        String condPersionCount = activityDefV2Req.getCondPersionCount();
        if (StringUtils.isNotBlank(condPersionCount)) {
            activityDef.setCondPersionCount(Integer.valueOf(condPersionCount));
        }
        String hitsPerDraw = activityDefV2Req.getHitsPerDraw();
        if (StringUtils.isNotBlank(hitsPerDraw)) {
            activityDef.setHitsPerDraw(Integer.valueOf(hitsPerDraw));
        }
        String missionNeedTime = activityDefV2Req.getMissionNeedTime();
        if (StringUtils.isNotBlank(missionNeedTime)) {
            activityDef.setMissionNeedTime(Integer.valueOf(missionNeedTime));
        }
        String goodCommentMissionNeedTime = activityDefV2Req.getGoodCommentMissionNeedTime();
        if (StringUtils.isNotBlank(goodCommentMissionNeedTime)) {
            activityDef.setMissionNeedTime(Integer.valueOf(goodCommentMissionNeedTime));
        }
        String merchantCouponJson = activityDefV2Req.getMerchantCouponJson();
        if (StringUtils.isNotBlank(merchantCouponJson)) {
            List<Long> merchantCouponList = new ArrayList<>();
            String[] split = merchantCouponJson.split(",");
            for (String merchantCoupon : split) {
                if (!RegexUtils.StringIsNumber(merchantCoupon)) {
                    continue;
                }
                merchantCouponList.add(Long.valueOf(merchantCoupon));
            }
            if (!CollectionUtils.isEmpty(merchantCouponList)) {
                activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
            }
        }
        String serviceWx = activityDefV2Req.getServiceWx();
        String serviceWxPic = activityDefV2Req.getServiceWxPic();
        if (StringUtils.isNotBlank(serviceWx) || StringUtils.isNotBlank(serviceWxPic)) {
            List<Map<String, String>> csMapList = new ArrayList<>();
            Map<String, String> csMap = new HashMap<>(2);
            csMap.put("wechat", serviceWx);
            csMap.put("pic", serviceWxPic);
            csMapList.add(csMap);
            activityDef.setServiceWx(JSONArray.toJSONString(csMapList));
        }

        String weighting = activityDefV2Req.getWeighting();
        if (StringUtils.isNotBlank(weighting)) {
            activityDef.setWeighting(Integer.valueOf(weighting));
        }
        // 设置佣金
        activityDef.setCommission(BigDecimal.ZERO);
        BigDecimal commission = activityDefV2Req.getCommission();
        if (commission != null && commission.compareTo(BigDecimal.ONE) >= 0) {
            activityDef.setCommission(commission);
        }

        boolean isSuccess = activityDefService.saveOrUpdate(activityDef);
        if (!isSuccess) {
            return null;
        }

        return activityDef;
    }

    @Override
    public SimpleResponse<Object> checkUnUpateParamWhenPublished(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        // 获取传入的参数
        Long activityDefId = Long.valueOf(activityDefV2Req.getActivityDefId());
        Integer typeReq = Integer.valueOf(activityDefV2Req.getType());
        Long goodsIdReq = null;
        if (StringUtils.isNotBlank(activityDefV2Req.getGoodsId())) {
            goodsIdReq = Long.valueOf(activityDefV2Req.getGoodsId());
        }
        Integer stockDefReq = Integer.valueOf(activityDefV2Req.getStockDef());
        List<Long> goodsIdListReq = new ArrayList<>();


        ActivityDef activityDef = activityDefService.getById(activityDefId);

        Integer type = activityDef.getType();
        Long goodsId = activityDef.getGoodsId();
        String goodsIds = activityDef.getGoodsIds();
        List<Long> goodsIdList = null;
        if (JsonParseUtil.booJsonArr(goodsIds)) {
            goodsIdList = JSON.parseArray(goodsIds, Long.class);
        }
        Integer stockDef = activityDef.getStockDef();
        BigDecimal goodsPrice = activityDef.getGoodsPrice();
        Integer stockType = activityDef.getStockType();

        if (!type.equals(typeReq)) {
            log.info("活动类型无法修改");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("活动类型无法修改");
            return response;
        }
        if (!stockDef.equals(stockDefReq)) {
            log.info("活动库存无法修改");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("活动库存无法修改");
            return response;
        }
        // 赠品和免单商品 活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
        // 领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
        // 无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
        if (type.equals(ActivityDefTypeEnum.ASSIST.getCode())
                || type.equals(ActivityDefTypeEnum.NEW_MAN.getCode())
                || type.equals(ActivityDefTypeEnum.ORDER_BACK.getCode())) {
            // 比较数据库中的数据，和新提交的数据 是否一致
            if (!goodsId.equals(goodsIdReq)) {
                log.info("活动商品无法修改");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                response.setMessage("活动商品无法修改");
                return response;
            }

        }

        if (type.equals(ActivityDefTypeEnum.ASSIST.getCode())) {
            if (StringUtils.equals(stockType.toString(), activityDefV2Req.getStockType())) {
                log.info("扣库存方式不可修改");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                response.setMessage("扣库存方式不可修改");
                return response;
            }
        }
        // 幸运大转盘 活动，支持修改的选项有：无。
        // 无法修改的选项有：类型type, 奖励人数stockDef

        // 红包任务 活动，支持修改的选项有：收藏加购任务时间(分)favCartMissionNeedTime ，好评返现任务时间(分) goodCommentMissionNeedTime
        // 无法修改的选项有：活动商品组goodsIdList，初始红包库存stockDef，回填订单金额fillBackOrderAmount，收藏加购金额favCartAmount，好评晒图金额goodCommentAmount
        if (type.equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            if (!CollectionUtils.containsAll(goodsIdList, goodsIdListReq)) {
                log.info("活动商品组无法修改");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                response.setMessage("活动商品组无法修改");
                return response;
            }
            BigDecimal goodCommentAmountReq = new BigDecimal(activityDefV2Req.getGoodCommentAmount());
            BigDecimal favCartAmountReq = new BigDecimal(activityDefV2Req.getFavCartAmount());
            BigDecimal fillBackOrderAmountReq = new BigDecimal(activityDefV2Req.getFillBackOrderAmount());
            goodCommentAmountReq = goodCommentAmountReq.setScale(2, RoundingMode.DOWN);
            favCartAmountReq = favCartAmountReq.setScale(2, RoundingMode.DOWN);
            fillBackOrderAmountReq = fillBackOrderAmountReq.setScale(2, RoundingMode.DOWN);

            AmountBO amountBO = activityTaskService.getAmountBO(activityDefId);
            BigDecimal favCartAmount = amountBO.getFavCartAmount();
            BigDecimal goodCommentAmount = amountBO.getGoodCommentAmount();
            BigDecimal fillBackOrderAmount = amountBO.getFillBackOrderAmount();

            favCartAmount = favCartAmount.setScale(2, RoundingMode.DOWN);
            goodCommentAmount = goodCommentAmount.setScale(2, RoundingMode.DOWN);
            fillBackOrderAmount = fillBackOrderAmount.setScale(2, RoundingMode.DOWN);

            if (!favCartAmount.equals(favCartAmountReq)
                    || !goodCommentAmount.equals(goodCommentAmountReq)
                    || !fillBackOrderAmount.equals(fillBackOrderAmountReq)) {
                log.info("红包单价无法修改");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
                response.setMessage("红包单价无法修改");
                return response;
            }
        }
        // 拆红包任务 活动
        if (type.equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
            BigDecimal openRedpacketAmount = new BigDecimal(activityDefV2Req.getOpenRedpacketAmount());
            openRedpacketAmount = openRedpacketAmount.setScale(2, RoundingMode.DOWN);
            if (!goodsPrice.equals(openRedpacketAmount)) {
                log.info("单个红包金额无法修改");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
                response.setMessage("单个红包金额无法修改");
                return response;
            }
        }

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public boolean checkValidity(Long activityDefId, ActivityDef def, boolean isReduce) {
        ActivityDef activityDef = def;
        if (activityDef == null) {
            activityDef = activityDefService.getById(activityDefId);
        }

        if (activityDef.getStatus() != ActivityDefStatusEnum.PUBLISHED.getCode()
                || activityDef.getDel() == DelEnum.YES.getCode()) {
            return false;
        }
        if (isReduce) {
            Integer updateRows = activityDefService.reduceDeposit(activityDef.getId(), BigDecimal.ZERO, 1);
            return updateRows != 0;
        }
        return activityDef.getStock() >= 1
                && activityDef.getDeposit().compareTo(activityDef.getGoodsPrice()) >= 0;
    }

    @Override
    public SimpleResponse suspendOrResumeActivityDef(Long activityDefId, Integer status) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验是否有挂起的资格， 只有在活动进行中的状态下 才有挂起资格
        ActivityDef activityDef = activityDefService.getById(activityDefId);
        if (null == activityDef) {
            return this.selectError(activityDefId);
        }
        if (activityDef.getDel() == DelEnum.YES.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("该活动已被删除");
            return response;
        }

        Integer statusDB = activityDef.getStatus();
        if (status == ActivityDefStatusEnum.PUBLISHED.getCode()) {
            // 是想要 启动 活动， 则判断活动是否处于 非启动状态，并且 0保存 和 1待审核 无法直接操作启动
            if (statusDB == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                response.setMessage("此活动 已在进行中，无需 再次启动");
                return response;
            }
            if (statusDB == ActivityDefStatusEnum.AUDIT_ERROR.getCode()) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                response.setMessage("此活动 审核失败，无法启动");
                return response;
            }
            if (statusDB == ActivityDefStatusEnum.SAVE.getCode()) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                response.setMessage("此活动 未支付，无法启动");
                return response;
            }
            if (statusDB == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
                response.setMessage("此活动 待审核，无法启动");
                return response;
            }

            // 对店长任务的重新启动处理
            if (activityDef.getType().equals(ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode())) {
                // 获取任务三属性
                QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
                wrapper.lambda()
                        .eq(ActivityDef::getParentId, activityDef.getId())
                        .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
                ActivityDef recommendShopManagerTaskDef = activityDefService.getOne(wrapper);
                if (recommendShopManagerTaskDef == null) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
                    response.setMessage("活动任务三异常，无法启动");
                    return response;
                }
                BigDecimal commission = recommendShopManagerTaskDef.getCommission();

                if (commission.compareTo(BigDecimal.ZERO) <= 0) {
                    BigDecimal salePrice = BigDecimal.ZERO;
                    CommonDictDTO commonDictDTO = commAppService.selectByTypeKey("shopManager", "salePolicy");
                    if (commonDictDTO != null && StringUtils.isNotBlank(commonDictDTO.getValue())) {
                        JSONObject jsonObject = JSON.parseObject(commonDictDTO.getValue());
                        salePrice = jsonObject.getBigDecimal("salePrice");
                    }
                    if (salePrice.compareTo(BigDecimal.ZERO) <= 0) {
                        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "平台限时补贴已取消，您的店长任务的任务三佣金为0，活动已结束");
                    }
                }

            }

            // 对幸运大转盘启动处理
            if (activityDef.getType().equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
                // 判断店铺中是否有其他定义的大转盘活动在进行中
                QueryWrapper<ActivityDef> adWrapper = new QueryWrapper<>();
                adWrapper.lambda()
                        .eq(ActivityDef::getShopId, activityDef.getShopId())
                        .eq(ActivityDef::getType, ActivityDefTypeEnum.LUCKY_WHEEL.getCode())
                        .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                        .eq(ActivityDef::getDel, DelEnum.NO.getCode());
                List<ActivityDef> list = activityDefService.list(adWrapper);
                if (CollectionUtils.isNotEmpty(list)) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                    response.setMessage("此活动 已在进行中，无需 再次启动");
                    return response;
                }
                // 判断下库存是否充足
                if (activityDef.getStock() <= 0 || activityDef.getDeposit().compareTo(new BigDecimal(1)) <= 0) {
                    log.info("库存不足，无法启动");
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
                    response.setMessage("库存不足，无法启动");
                    return response;
                }
            }

            // 店长任务处理
            if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
                // 修改子任务状态
                List<ActivityDef> activityDefList = activityDefService.list(
                        new QueryWrapper<ActivityDef>()
                                .lambda()
                                .eq(ActivityDef::getParentId, activityDef.getId())
                                .ne(ActivityDef::getId, activityDef.getId())
                );

                for (ActivityDef item:activityDefList) {
                    item.setStatus(status);
                    activityDefService.updateActivityDef(item);
                }
            }

            //查找是否同类型的活动是否有进行中的
            QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityDef::getMerchantId, activityDef.getMerchantId())
                    .eq(ActivityDef::getShopId, activityDef.getShopId())
                    .eq(ActivityDef::getType, activityDef.getType())
                    .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                    .eq(ActivityDef::getDel, DelEnum.NO.getCode())
            ;
            wrapper.orderByDesc("id");
            List<ActivityDef> activityDefs = activityDefService.list(wrapper);
            if (!CollectionUtils.isEmpty(activityDefs)) {
                log.info("已有进行中的活动");
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
                response.setMessage("已有进行中的活动");
                return response;
            }
        }
        if (status == ActivityDefStatusEnum.CLOSE.getCode()) {
            if (statusDB != ActivityDefStatusEnum.PUBLISHED.getCode()) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                response.setMessage("该活动 没在进行中，无需挂起");
                return response;
            }


            //对拆红包的处理
            if (activityDef.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
                //查看是否有正在进行的新手任务
                QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
                wrapper.lambda()
                        .eq(ActivityDef::getMerchantId, activityDef.getMerchantId())
                        .eq(ActivityDef::getShopId, activityDef.getShopId())
                        .eq(ActivityDef::getType, ActivityDefTypeEnum.NEWBIE_TASK.getCode())
                        .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                        .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                ;
                wrapper.orderByDesc("id");
                List<ActivityDef> activityDefs = activityDefService.list(wrapper);
                if (!CollectionUtils.isEmpty(activityDefs)) {
                    log.info("新手任务正在进行中，拆红包活动无法操作");
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
                    response.setMessage("新手任务正在进行中，拆红包活动无法操作");
                    return response;
                }
            }
        }
        Date now = new Date();

        activityDef.setStatus(status);
        activityDef.setUtime(now);
        boolean update = activityDefService.updateById(activityDef);
        if (!update) {
            return saveError(response);
        }
        if (activityDef.getType().equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityTask::getActivityDefId, activityDefId);
            List<ActivityTask> activityTaskDBs = activityTaskService.list(wrapper);
            if (CollectionUtils.isEmpty(activityTaskDBs)) {
                return activityTaskService.selectErrorByActivityDefId(activityDefId);
            }
            List<ActivityTask> activityTaskList = new ArrayList<>();
            for (ActivityTask activityTask : activityTaskDBs) {
                activityTask.setStatus(status);
                activityTask.setUtime(now);
                activityTaskList.add(activityTask);
            }
            update = activityTaskService.updateBatchById(activityTaskList);
            if (!update) {
                return saveError(response);
            }
        }
        if (activityDef.getType().equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
            QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
            aWrapper.lambda().eq(Activity::getActivityDefId, activityDefId);
            Activity activity = activityService.getOne(aWrapper);
            if (status == ActivityDefStatusEnum.CLOSE.getCode()) {
                activity.setStatus(ActivityStatusEnum.DEL.getCode());
            }
            if (status == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                activity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
                activity.setEndTime(DateUtils.addYears(new Date(), 100));
            }
            activity.setUtime(now);
            boolean activityUpdate = activityService.updateById(activity);
            if (!activityUpdate) {
                return saveError(response);
            }
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private SimpleResponse<Object> selectError(Long adid) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        response.setMessage("网络异常，请稍候重试");
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "数据库未查到ActivityDef活动的信息，id = " + adid);
        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse removeActivityDefAtSaveStatus(Long activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验是否有删除的资格， 只有在保存的状态 才有删除资格
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        if (null == activityDef) {
            return this.selectError(activityDefId);
        }
        if (activityDef.getDel() == DelEnum.YES.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("该活动已被删除");
            return response;
        }
        if (activityDef.getStatus() != ActivityDefStatusEnum.SAVE.getCode() && activityDef.getStatus() != ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("只能删除 待支付的活动");
            return response;
        }
        Date now = new Date();

        activityDef.setDel(DelEnum.YES.getCode());
        activityDef.setUtime(now);
        boolean update = activityDefService.updateById(activityDef);
        if (!update) {
            return this.saveError(response);
        }
        if (activityDef.getType().equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityTask::getActivityDefId, activityDefId);
            List<ActivityTask> activityTaskDBs = activityTaskService.list(wrapper);
            if (CollectionUtils.isEmpty(activityTaskDBs)) {
                return activityTaskService.selectErrorByActivityDefId(activityDefId);
            }
            List<ActivityTask> activityTaskList = new ArrayList<>();
            for (ActivityTask activityTask : activityTaskDBs) {
                activityTask.setDel(DelEnum.YES.getCode());
                activityTask.setUtime(now);
                activityTaskList.add(activityTask);
            }
            update = activityTaskService.updateBatchById(activityTaskList);
            if (!update) {
                return saveError(response);
            }
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private SimpleResponse saveError(SimpleResponse<Object> response) {
        Map<String, String> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "创建活动定义出错，活动未创建");
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        response.setMessage("网络异常，请稍候重试");
        response.setData(resultVO);
        return response;
    }


    /***
     * 通过补库存结算活动 清算金额
     * @param activityDefAuditId
     * @return
     */
    @Override
    public boolean refundAddStockAudit(Long activityDefAuditId, BigDecimal refundMoney) {
        ActivityDefAudit activityDefAudit = activityDefAuditService.selectByPrimaryKey(activityDefAuditId);
        if (activityDefAudit == null) {
            log.error("activity_def_audit 表 没有此数据记录，activityDefAuditId = {}", activityDefAuditId);
            return false;
        }
        ActivityDef activityDef = activityDefService.getById(activityDefAudit.getActivityDefId());
        if (null == activityDef) {
            log.error("activity_def 表 没有此数据记录，activityDefId = {}", activityDefAudit.getActivityDefId());
            return false;
        }
        activityDef.setStatus(ActivityDefStatusEnum.REFUNDED.getCode());
        activityDef.setUtime(new Date());
        activityDefService.updateById(activityDef);

        //若是回填订单清算，则直接算金额，回填订单没有库存概念
        if (activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
            //增加清算记录
            this.activityDefAuditService.refundMoneyAddAudit4ActivityDef(activityDefAudit, null, refundMoney, 0);
        } else {
            //总库存
            int totalStock = getTotalStock(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId());
            //消耗库存
            int costStock = activityDef.getStockDef() - activityDef.getStock();
            //退库存= 总库存-消耗库存
            int backStock = totalStock - costStock;
            //计算单价
            BigDecimal unitPrice = activityDefAboutService.getDefAuditUnitPrice(activityDef, activityDefAudit);
            //增加清算记录
            this.activityDefAuditService.refundMoneyAddAudit4ActivityDef(activityDefAudit, unitPrice, refundMoney, backStock);
        }
        //若是收藏加购,则更新activityTask
        if (activityDef.getType() == ActivityDefTypeEnum.FAV_CART_V2.getCode()
                || activityDef.getType() == ActivityDefTypeEnum.FAV_CART.getCode()) {
            updateEndActivityTask(activityDefAudit.getActivityDefId());
        }
        return true;
    }


    /***
     * 结算活动定义，第一次创建时 可清算金额
     * @param activityDefId
     * @return
     */
    @Override
    public boolean refundDef(Long activityDefId, BigDecimal refundMoney) {
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        if (null == activityDef) {
            log.error("activity_def 表 没有此数据记录，activityDefId = " + activityDefId);
            return false;
        }
        activityDef.setStatus(ActivityDefStatusEnum.REFUNDED.getCode());
        activityDef.setUtime(new Date());
        activityDefService.updateById(activityDef);

        //给第一次加入audit表增加一条数据
        QueryWrapper<ActivityDefAudit> activityDefAuditQueryWrapper = new QueryWrapper<>();
        activityDefAuditQueryWrapper.lambda()
                .eq(ActivityDefAudit::getMerchantId, activityDef.getMerchantId())
                .eq(ActivityDefAudit::getShopId, activityDef.getShopId())
                .eq(ActivityDefAudit::getActivityDefId, activityDefId)
                .eq(ActivityDefAudit::getStatus, ActivityDefAuditStatusEnum.PASS.getCode())
                .eq(ActivityDefAudit::getDel, DelEnum.NO.getCode())
                .eq(ActivityDefAudit::getBizType, ActivityDefAuditBizTypeEnum.STOCK_ADD.getCode())
                .orderByAsc(ActivityDefAudit::getId)
        ;
        List<ActivityDefAudit> activityDefAudits = this.activityDefAuditService.list(activityDefAuditQueryWrapper);
        if (CollectionUtils.isEmpty(activityDefAudits)) {
            return true;
        }
        ActivityDefAudit activityDefAudit = activityDefAudits.get(0);
        //若是回填订单清算，则直接算金额，回填订单没有库存概念
        if (activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
            //增加清算记录
            this.activityDefAuditService.refundMoneyAddAudit4ActivityDef(activityDefAudit, null, refundMoney, 0);
        } else {
            //总库存
            int totalStock = getTotalStock(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId());
            //消耗库存
            int costStock = activityDef.getStockDef() - activityDef.getStock();
            //退库存= 总库存-消耗库存
            int backStock = totalStock - costStock;
            //计算单价
            BigDecimal unitPrice = this.activityDefAboutService.getDefAuditUnitPrice(activityDef, activityDefAudit);
            //增加清算记录
            this.activityDefAuditService.refundMoneyAddAudit4ActivityDef(activityDefAudit, unitPrice, refundMoney, backStock);
        }

        //若是收藏加购,则更新activityTask
        if (activityDef.getType() == ActivityDefTypeEnum.FAV_CART_V2.getCode()
                || activityDef.getType() == ActivityDefTypeEnum.FAV_CART.getCode()) {
            updateEndActivityTask(activityDefId);
        }
        return true;
    }

    private boolean updateEndActivityTask(Long activityDefId) {
        Date now = new Date();
        QueryWrapper<ActivityTask> atwrapper = new QueryWrapper<>();
        atwrapper.in("activity_def_id", activityDefId);
        List<ActivityTask> activityTasks = activityTaskService.list(atwrapper);
        List<ActivityTask> updates = new ArrayList<>();
        for (ActivityTask activityTask : activityTasks) {
            activityTask.setStatus(ActivityTaskStatusEnum.END.getCode());
            activityTask.setUtime(now);
            updates.add(activityTask);
        }
        boolean success = activityTaskService.updateBatchById(updates);
        if (!success) {
            return false;
        }
        return success;
    }

    /***
     * 获取总库存
     * @param mechantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    private int getTotalStock(Long mechantId, Long shopId, Long activityDefId) {
        int totalStock = 0;
        QueryWrapper<ActivityDefAudit> activityDefAuditQueryWrapper = new QueryWrapper<>();
        activityDefAuditQueryWrapper.lambda()
                .eq(ActivityDefAudit::getMerchantId, mechantId)
                .eq(ActivityDefAudit::getShopId, shopId)
                .eq(ActivityDefAudit::getActivityDefId, activityDefId)
                .eq(ActivityDefAudit::getStatus, ActivityDefAuditStatusEnum.PASS.getCode())
                .eq(ActivityDefAudit::getDel, DelEnum.NO.getCode())
                .eq(ActivityDefAudit::getBizType, ActivityDefAuditBizTypeEnum.STOCK_ADD.getCode())
                .orderByAsc(ActivityDefAudit::getId)
        ;
        List<ActivityDefAudit> activityDefAudits = this.activityDefAuditService.list(activityDefAuditQueryWrapper);
        if (CollectionUtils.isEmpty(activityDefAudits)) {
            return totalStock;
        }
        for (ActivityDefAudit activityDefAudit : activityDefAudits) {
            totalStock = totalStock + activityDefAudit.getStockApply();
        }
        return totalStock;
    }

    /***
     * 创建支付成功后
     * @param activityDefId
     * @return
     */
    @Override
    public boolean pay2start(Long activityDefId) {
        // 不做校验 内部程序自调
        Date now = new Date();
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        if (null == activityDef) {
            log.error("activity_def 表 没有此数据记录，activityDefId = " + activityDefId);
        }
        activityDef.setStatus(ActivityDefStatusEnum.PUBLISHED.getCode());
        activityDef.setStartTime(now);
        activityDef.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        activityDef.setPublishTime(now);
        activityDef.setAuditingTime(now);
        activityDef.setUtime(now);
        boolean success = activityDefService.updateById(activityDef);
        if (!success) {
            return false;
        }
        //判断是否是新手|店长任务
        if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode() ||
                activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
            List<ActivityDef> activityDefs = activityDefService.selectByParentId(activityDefId);
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef1 : activityDefs) {
                    activityDef1.setStatus(ActivityDefStatusEnum.PUBLISHED.getCode());
                    activityDef1.setStartTime(now);
                    activityDef1.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
                    activityDef1.setPublishTime(now);
                    activityDef1.setAuditingTime(now);
                    activityDef1.setUtime(now);
                    success = activityDefService.updateById(activityDef1);
                    if (!success) {
                        log.error("<<<<<<<<支付回调，更新新手|店长定义 异常");
                    }
                }
            }
        }
        // 更新 库存审核记录 表
        QueryWrapper<ActivityDefAudit> adawrapper = new QueryWrapper<>();
        adawrapper.eq("activity_def_id", activityDefId);
        ActivityDefAudit activityDefAudit = activityDefAuditService.getOne(adawrapper);
        if (activityDefAudit != null) {
            activityDefAudit.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
            activityDefAudit.setStockFinal(activityDefAudit.getStockApply());
            activityDefAudit.setDepositFinal(activityDefAudit.getDepositApply());
            activityDefAudit.setPublishTime(now);
            activityDefAudit.setAuditTime(now);
            activityDefAudit.setStatus(ActivityDefAuditStatusEnum.PASS.getCode());
            activityDefAudit.setUtime(now);
            success = activityDefAuditService.updateById(activityDefAudit);
            if (!success) {
                return false;
            }
        }
        /***
         * 红包任务20190318活动拆分迭代后，将1拆3个活动形式存在
         */
        // 如果是红包活动，更新 红包任务状态 activity_task
        if (ActivityDefTypeEnum.RED_PACKET.getCode() == activityDef.getType()) {
            QueryWrapper<ActivityTask> atwrapper = new QueryWrapper<>();
            atwrapper.in("activity_def_id", activityDefId);
            List<ActivityTask> activityTasks = activityTaskService.list(atwrapper);
            List<ActivityTask> updates = new ArrayList<>();
            for (ActivityTask activityTask : activityTasks) {
                activityTask.setStartTime(now);
                activityTask.setPublishTime(now);
                activityTask.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
                activityTask.setStatus(ActivityTaskStatusEnum.PUBLISHED.getCode());
                activityTask.setUtime(now);
                if (activityTask.getRpAmount().equals(BigDecimal.ZERO)) {
                    activityTask.setStatus(ActivityTaskStatusEnum.END.getCode());
                }
                updates.add(activityTask);
            }
            success = activityTaskService.updateBatchById(updates);
            if (!success) {
                return false;
            }
        }

        //如果是收藏加购or好评晒图的话，则更新 activity_task
        boolean favCart_GOODCOMMENT = ActivityDefTypeEnum.FAV_CART.getCode() == activityDef.getType() || ActivityDefTypeEnum.FAV_CART_V2.getCode() == activityDef.getType()
                || ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDef.getType() || ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() == activityDef.getType();
        if (favCart_GOODCOMMENT) {
            QueryWrapper<ActivityTask> atwrapper = new QueryWrapper<>();
            atwrapper.in("activity_def_id", activityDefId);
            List<ActivityTask> activityTasks = activityTaskService.list(atwrapper);
            List<ActivityTask> updates = new ArrayList<>();
            if (CollectionUtils.isEmpty(activityTasks)) {
                return false;
            }
            for (ActivityTask activityTask : activityTasks) {
                activityTask.setStartTime(now);
                activityTask.setPublishTime(now);
                activityTask.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
                activityTask.setStatus(ActivityTaskStatusEnum.PUBLISHED.getCode());
                activityTask.setUtime(now);
                if (activityTask.getRpAmount().equals(BigDecimal.ZERO)) {
                    activityTask.setStatus(ActivityTaskStatusEnum.END.getCode());
                }
                updates.add(activityTask);
            }
            if (!CollectionUtils.isEmpty(updates)) {
                success = activityTaskService.updateBatchById(updates);
                if (!success) {
                    return false;
                }
            }
        }
        if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDef.getType()) {
            ActivityDef activityDefDB = activityDefService.selectByPrimaryKey(activityDefId);
            Activity activity = new Activity();
            activity.setMerchantId(activityDefDB.getMerchantId());
            activity.setShopId(activityDefDB.getShopId());
            activity.setActivityDefId(activityDefDB.getId());
            activity.setType(ActivityTypeEnum.LUCK_WHEEL.getCode());
            activity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
            activity.setStartTime(now);
            activity.setEndTime(DateUtils.addYears(now, 100));
            activity.setCtime(now);
            activity.setUtime(now);
            activity.setWeighting(activityDef.getWeighting());
            int activityRows = activityService.add(activity);
            return activityRows != 0;
        }
        return true;
    }


    @Override
    public boolean checkPublished(ActivityDefV2Req activityDefV2Req) {
        // 校验参数是否存在
        String activityDefId = activityDefV2Req.getActivityDefId();
        if (null == activityDefId) {
            return false;
        }
        // 获取传入的参数
        Long adid = Long.valueOf(activityDefId);

        ActivityDef activityDef = activityDefService.getById(adid);
        // 判断是否是 已发布中的活动。bug： 已结束，或 已暂停的就能修改？ 我们的修改都是针对 支付过后的 一系列状态
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            return false;
        }

        return true;
    }
}
