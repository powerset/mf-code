package mf.code.activity.service;


import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ActivityService extends IService<Activity> {

    int add(Activity record);

    int del(Activity record);

    int  update(Activity record);

    Activity findById(Long id);


    List<Activity> findPage(Map<String, Object> params);

    int countActivity(Map<String, Object> params);

    List<Activity> listActivityByStatus(int status);

    int updateActivityStatus(Long aid, int status);

    boolean saveSelective(Activity activity);

    SimpleResponse addActivity(Map map);

    List<Activity> listActivityByStatusAndAwardStartTimeINN(int status);

    int countByShopGoodsForDel(Long shopId, Long goodsId);

    /***
     * 获取首页商户计划内活动的状态
     * @param activityStatus
     * @param activityStartTime
     * @param activityAwardTime
     * @param activityEndTime
     * @return
     */
    int getActivityStatus(int activityStatus, Date activityStartTime, Date activityAwardTime, Date activityEndTime);

    /***
     * 获取领取红包状态
     * @param activity
     * @return
     */
    int getRedPackStatus(Activity activity, Long userID);

    /***
     * 根据结束的活动编号，判断其goodid的活动是否还存在，存在则更新zset的score，不存在则删除zset
     * @param activityID
     * @return
     */
    int checkActivityEnd2Zset(Long activityID);

    /**
     * 根据订单号、活动定义生成具体活动数据
     * @param userId
     * @param orderId
     * @param activityDefId
     * @param status
     * @return
     */
    SimpleResponse createActivityFromDef(Long userId, String orderId, Long activityDefId, Integer status);

    /**
     * 校验是否能支付
     * @param activityDefId
     * @param userId
     * @return
     */
    SimpleResponse checkPayForActivity(Long activityDefId, Long userId);

    /**
     * 更新金额
     * @param id
     * @param amount
     * @return
     */
    Integer updateDeposit(Long id, BigDecimal amount);

    int updateStatus(Long aid, int status);

    /**
     * find by plan id
     * @param planId
     * @return
     */
    List<Activity> findByPlanId(Long planId);

    Activity findByPlanIdLimit(Long activityPlanId);

    int updateByActivityDefIdStatus(Long activityDefId, int status);

    int deleteActivityByActivityDefId(Long activityDefId);

    List<Activity> getActivityArrBySku(Map param);

    List<Map> findByGoodsId(Long goodsId);

    Long countGoodsStock(Long activityDefId);

    List<Map> findByDefId(Long activityDefId);

    List<Activity> findByDefIdInActivity(long parseLong);

    List<Activity> selectByMerchantIdShopId(Long merchantId, Long shopId);

    List<Activity> findByDefId4HomePage(Long activityDefId);

    Activity findByDefIdUserId(String defId, Long userId);

    /**
     * 校验活动 是否 已过期
     * true已过期，false未过期
     * @param activity
     * @return
     */
	boolean checkExpired(Activity activity, Long userId, Long activityDefId);

    /**
     * 校验活动 是否 已开奖
     * @param activity
     * @return
     */
    boolean checkAwarded(Activity activity);

    /**
     * 从活动定义中 创建用户活动
     * @param userId
     * @param activityDef
     * @return
     */
    Activity saveFromActivityDef(Long userId, ActivityDef activityDef);

    /**
     * 从活动定义的类型转化到活动的类型
     * @return
     */
    Integer activityDefType2ActivityType(Integer activityDefType);

    /**
     * 校验活动是否 可助力
     * 即 同时校验 活动是否 已开奖 和 判断活动是否已过期
     * @param activity
     * @return
     */
    boolean checkValidity(Activity activity);

    /**
     * 更新活动至开奖
     * @param activity
     * @return
     */
    boolean updateAwardStartTime(Activity activity);

    Activity findByDefId4Summary(Long id);

    /**
     * 计算拆红包的已经返现金额
     * @param param
     * @return
     */
    BigDecimal calcOpenRedPacketMoney(Map<String, Object> param);

    /**
     * 根据条件汇总金额
     *
     * @param param
     * @return
     */
    BigDecimal sumByParams(Map<String, Object> param);
}
