package mf.code.activity.service.activitydef.impl;

import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.activity.service.activitydef.impl
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-18 09:07
 */
@Service
public class ActivityDefNewBieTaskService extends AbstractActivityDefService {

    @Autowired
    private ActivityDefService activityDefService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantShopService merchantShopService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ActivityDefCheckService activityDefCheckService;

    @Autowired
    private ActivityDefAboutService activityDefAboutService;

    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;

    @Autowired
    private ActivityDefAuditService activityDefAuditService;

    protected SimpleResponse createDef(CreateDefReq req) {
        // 拆分活动
        ActivityDef mainActivityDef = new ActivityDef();
        ActivityDef readActivityDef = new ActivityDef();
        ActivityDef scanActivityDef = new ActivityDef();

        boolean splitResult = activityDefAboutService.splitNewBieTaskDefReq(req, mainActivityDef, readActivityDef, scanActivityDef);
        if (!splitResult) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "活动拆分异常，@to大妖怪");
        }
        activityDefService.createActivityDef(mainActivityDef);

        // 更新主记录的parentId为当前id
        mainActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.updateActivityDef(mainActivityDef);

        readActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.createActivityDef(readActivityDef);

        scanActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.createActivityDef(scanActivityDef);

        ActivityDef openRedPackActivityDef = activityDefService.selectByPrimaryKey(req.getExtraDataObject().getOpenRedPackactiveId());
        openRedPackActivityDef.setParentId(mainActivityDef.getId());
        activityDefService.updateActivityDef(openRedPackActivityDef);

        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(mainActivityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }
        Map<String, Object> map = new HashMap<>(1);
        map.put("activityDefId", mainActivityDef.getId());
        SimpleResponse response = new SimpleResponse();
        response.setData(map);
        return response;
    }

    @Override
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());

        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }

        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            return this.activityDefUpdateService.activityDefNewBieUpdateSave(req, activityDef);
        } else {
            //非草稿修改, 只能修改任务二（扫一扫任务）的微信二维码和加群口令
            return this.activityDefUpdateService.activityDefNewBieUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.NEWBIE_TASK;
    }

    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        Merchant merchant = merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(req.getShopId());
        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if (req.getExtraDataObject() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少必要参数");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getKeywords())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少微信群口令");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getServiceWx())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少微信二维码");
        }

        if (req.getExtraDataObject().getOpenRedPackactiveId() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，拆红包活动id不能为空，@to木子");
        }

        ActivityDef activityDef = activityDefService.selectByPrimaryKey(req.getExtraDataObject().getOpenRedPackactiveId());
        if (activityDef == null || activityDef.getDel() != DelEnum.NO.getCode() || activityDef.getType() != ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            boolean exist = false;
            ActivityDef parentActivityDef = activityDefService.selectByPrimaryKey(activityDef.getId());

            if (activityDef.getParentId() == null) {
                exist = true;
            }

            if (parentActivityDef == null) {
                exist =  true;
            }

            if (parentActivityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
                exist =  true;
            }

            if (!exist) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，拆红包活动不存在或未开启，@to木子");
            }
        }

        // 校验是否可创建活动（拆红包活动没被占用）
        if (activityDef.getId() != activityDef.getId()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，拆红包活动不可用");
        }

        if (req.getStockDef().intValue() < 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，计划拉新人数，必须≥1");
        }

        // 校验活动人数
        if (activityDef.getStock().intValue() < req.getStockDef().intValue()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，计划新拉人数大于拆红包活动库存数");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getReadAmount()) || new BigDecimal(req.getExtraDataObject().getReadAmount()).floatValue() < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，新手阅读任务佣金不能小于0");
        }

        if (StringUtils.isBlank(req.getExtraDataObject().getScanAmount()) || new BigDecimal(req.getExtraDataObject().getScanAmount()).floatValue() < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，扫一扫任务佣金不能小于0");
        }

        if (req.getExtraDataObject().getReadAmount() != null) {
            Assert.isNumber(req.getExtraDataObject().getReadAmount(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请输入正确的阅读任务金额");
        }

        if (req.getExtraDataObject().getScanAmount() != null) {
            Assert.isNumber(req.getExtraDataObject().getScanAmount(), ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请输入正确的扫一扫任务金额");
        }

        // 查询口令是否重复
//        ActivityDef scanActivityDef = activityDefService.selectByKeywords(req.getExtraDataObject().getKeywords());
//        if (scanActivityDef != null) {
//            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "传参异常，口令已存在");
//        }

        // redis防重拦截
        String key = new StringBuilder(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT).append(req.getMerchantId()).toString();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, key);
        if (!success) {
            return RedisForbidRepeat.NXError();
        }

        // 校验是否有创建活动的资格
        SimpleResponse simpleResponse = activityDefCheckService.createAble(req.getMerchantId(), req.getShopId(), req.getType(), null, null, null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(key);
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (req.getActivityDefId() == null) {
            if (req == null || req.getExtraDataObject() == null || StringUtils.isBlank(req.getExtraDataObject().getReadAmount())
                    || StringUtils.isBlank(req.getExtraDataObject().getScanAmount()) || req.getStockDef() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数，@to木子");
            }

            BigDecimal depositDef = activityDefAboutService.calcNewBieTastDeposit(req.getExtraDataObject().getReadAmount(), req.getExtraDataObject().getScanAmount(), req.getStockDef());

            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("depositDef", depositDef);
            simpleResponse.setData(resultVO);

            return simpleResponse;
        } else {
            if (req == null || req.getStockDef() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数，@to木子");
            }
            // 查询活动
            ActivityDef mainActivityDef = activityDefService.selectByPrimaryKey(req.getActivityDefId());
            if (mainActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动不存在");
            }

            if (!mainActivityDef.getMerchantId().equals(req.getMerchantId()) || !mainActivityDef.getShopId().equals(req.getShopId())) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "没有操作权限");
            }

            // 查询阅读任务子活动
            ActivityDef readActivityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
            if (readActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询阅读任务子活动失败");
            }

            // 查询扫一扫任务子活动
            ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
            if (readActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询扫一扫任务子子活动失败");
            }

            BigDecimal depositDef = activityDefAboutService.calcNewBieTastDeposit(readActivityDef.getCommission().toString(), scanActivityDef.getCommission().toString(), req.getStockDef());

            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("depositDef", depositDef);
            simpleResponse.setData(resultVO);
        }
        return simpleResponse;
    }
}
