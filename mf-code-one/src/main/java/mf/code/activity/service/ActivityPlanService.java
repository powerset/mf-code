package mf.code.activity.service;

import com.github.pagehelper.PageInfo;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ActivityPlanService {


    int updateDepositByid(ActivityPlan plan, BigDecimal goodsPrice, String totalPerKeyJson, Long itemId);

    int add(ActivityPlan record);

    int del(Long id);

    int update(ActivityPlan record);

    int updatePlus(ActivityPlan record);

    ActivityPlan findById(Long id);

    Map findByIdPlus(Long id);

    List<ActivityPlan> findList();

    PageInfo selectAll(String merchantId, int pageSize, int currentPage, Long shopId, Integer stat);

    PageInfo selectAll(String merchantId, int pageSize, int currentPage, Integer stat);

    /**
     * 按条件查询
     * @param params
     * @return
     */
    List<ActivityPlan> selectByParams(HashMap<String, Object> params);

    int updateByPrimaryKeySelectivePlus(ActivityPlan record);

    int updateByPrimaryKeySelectivePlusWithoutMerchantId(ActivityPlan record);

    String assertSaveParam(int status, int condPersionCount, int condDrawTime, int hitsPerDraw, int missionNeedTime, Integer totalDay);

    SimpleResponse confirm(Map map);

    int publish(Map map);

    Integer reduceRedpackStock(Long id);

    Integer addRedpackStock(Long id);

    int subtractTotalRedpackStockByPrimaryKey(Long activityPlanId);

    /**
     * 通过ctime获取列表
     * @param condDate
     * @return
     */
    List<ActivityPlan> findByCtimePlus(String condDate);

    /**
     * 减少加权计划的订单库存量
     */
    SimpleResponse subBillResidue(Long activityPlanId);

}
