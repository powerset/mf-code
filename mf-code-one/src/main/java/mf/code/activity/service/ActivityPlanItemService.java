package mf.code.activity.service;

import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.repo.po.ActivityPlanItem;

import java.util.List;


public interface ActivityPlanItemService {

    int createCalendarBatch(ActivityPlan record);

    int add(ActivityPlanItem record);

    int del(ActivityPlanItem record);

    int update(ActivityPlanItem record);

    ActivityPlanItem findById(Long id);

    List<ActivityPlanItem> findList();

    List<ActivityPlanItem> findAllItemByPlanId(Long activityPlanId);

    int updateByIdSelective(ActivityPlanItem record);

    boolean verifyDateTime(String dateStr);

    boolean verifyBatchAndTime(ActivityPlan plan, ActivityPlanItem item, boolean isAdd);

    Integer updateByPlanIdAndDayNum(ActivityPlanItem item);

    ActivityPlanItem findItemByPlanIdAndDayNumLimit(Long activityPlanId);
}
