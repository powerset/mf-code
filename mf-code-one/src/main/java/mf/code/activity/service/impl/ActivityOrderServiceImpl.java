package mf.code.activity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.activity.repo.dao.ActivityOrderMapper;
import mf.code.activity.repo.po.ActivityOrder;
import mf.code.activity.service.ActivityOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author: gel
 * @date: 2018-11-08 17:34
 */
@Service
public class ActivityOrderServiceImpl extends ServiceImpl<ActivityOrderMapper, ActivityOrder> implements ActivityOrderService {
    @Autowired
    private ActivityOrderMapper activityOrderMapper;
    /**
     * 创建
     *
     * @param activityOrder
     * @return
     */
    @Override
    public Integer createActivityOrder(ActivityOrder activityOrder) {
        return activityOrderMapper.insertSelective(activityOrder);
    }

    /**
     * 根据商户，店铺，用户，订单号查询回填订单
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @Override
    public List<ActivityOrder> selectByOrderId(Long merchantId, Long shopId, Long userId, String orderId) {
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("userId", userId);
        params.put("orderId", orderId);
        return activityOrderMapper.selectByParams(params);
    }

    @Override
    public List<ActivityOrder> selectByOrderIdOne(Long merchantId, Long shopId, Long userId, Long goodsId, String orderId){
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("userId", userId);
        params.put("goodsId", goodsId);
        params.put("orderId", orderId);
        return activityOrderMapper.selectByParams(params);
    }

    @Override
    public List<ActivityOrder> selectByParams(Map<String, Object> params) {
        return this.activityOrderMapper.selectByParams(params);
    }

    @Override
    public int countActivityOrder(Map<String, Object> params) {
        return this.activityOrderMapper.countActivityOrder(params);
    }

    /**
     * 更新数据
     *
     * @param activityOrder
     */
    @Override
    public int updateActivityOrder(ActivityOrder activityOrder) {
        return activityOrderMapper.updateByPrimaryKeySelective(activityOrder);
    }
}
