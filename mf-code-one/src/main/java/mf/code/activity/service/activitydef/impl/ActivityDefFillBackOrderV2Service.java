package mf.code.activity.service.activitydef.impl;

import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * mf.code.activity.service.activitydef.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-23 16:34
 */
@Service
public class ActivityDefFillBackOrderV2Service extends AbstractActivityDefService {
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;
    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;

    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Merchant merchant = this.merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(req.getShopId());
        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if(req.getAmount() != null){
            if(new BigDecimal("0.01").compareTo(new BigDecimal(req.getAmount())) > 0){
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "请检查单个红包金额，必须大于0.01元");
            }
        }
        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        if (!success) {
            return RedisForbidRepeat.NXError();
        }
        // 校验是否有创建活动的资格
        simpleResponse = activityDefCheckService.createAble(req.getMerchantId(), req.getShopId(), req.getType(), null, null, null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        if (req.getActivityDefId() != null && req.getActivityDefId() > 0) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
            org.springframework.util.Assert.isTrue(activityDef != null, "该编号的活动不存在哦！defId=" + req.getActivityDefId());
            req.from(activityDef);
            req.setExtraDataObject(new CreateDefReq.ExtraData());
            req.getExtraDataObject().from(activityDef);
        } else {
            req.init();
        }
        if (req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) && StringUtils.isBlank(req.getAmount()) && StringUtils.isBlank(req.getAmountJson())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "请输入正确的保证金");
        }
        //若此时没有穿库存数
        if (req.getStockDef() == null || req.getStockDef() == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请输入正确的库存");
        }
        return this.activityDefAboutService.getDepositByProduct(req);
    }

    @Override
    public SimpleResponse createDef(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ActivityDef activityDef = this.activityDefAboutService.addActivityDef(req);
        activityDef.setStock(0);
        activityDef.setStockDef(0);
        // 这一步暂时没有用到，待后续产品更改
        simpleResponse = this.activityDefAboutService.checkAmountJson(req.getAmountJson(), null);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        boolean isSuccess = this.activityDefService.saveOrUpdate(activityDef);
        if (!isSuccess) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "存储活动异常 To @夜辰");
        }
        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }

        // 处理返回数据
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }

    @Override
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());

        // 添加类型，当前需要修改回填订单的校验
        if (req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) && StringUtils.isNotBlank(req.getAmount())
                && (StringUtils.isNotBlank(req.getAmountJson()) && !"[]".equals(req.getAmountJson()))) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "回填订单金额设置只能2选1");
        }
        if (req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) && StringUtils.isNotBlank(req.getAmountJson())) {
            SimpleResponse simpleResponse = this.activityDefAboutService.checkAmountJson(req.getAmountJson(), activityDef);
            if (simpleResponse.error()) {
                return simpleResponse;
            }
        }
        if (req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) && StringUtils.isNotBlank(req.getAmount())
                && (StringUtils.isNotBlank(req.getAmountJson()) && !"[]".equals(req.getAmountJson()))) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "回填订单金额设置只能2选1");
        }
        if (req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) && StringUtils.isNotBlank(req.getAmountJson())) {
            SimpleResponse simpleResponse = this.activityDefAboutService.checkAmountJson(req.getAmountJson(), activityDef);
            if (simpleResponse.error()) {
                return simpleResponse;
            }
        }
        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }
        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            return this.activityDefUpdateService.activityDefUpdateSave(req, activityDef);
        } else {
            //非草稿修改
            return this.activityDefUpdateService.activityDefUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.FILL_BACK_ORDER_V2;
    }
}
