package mf.code.activity.service.impl;

import com.alibaba.fastjson.JSON;
import mf.code.activity.repo.dao.ActivityPlanItemMapper;
import mf.code.activity.repo.dao.ActivityPlanMapper;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.repo.po.ActivityPlanItem;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityPlanItemService;
import mf.code.common.constant.Constants;
import mf.code.common.utils.DateUtil;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ActivityPlanItemServiceImpl implements ActivityPlanItemService {

    @Autowired
    private ActivityPlanItemMapper activityPlanItemMapper;

    @Autowired
    private ActivityPlanMapper activityPlanMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 批量添加计划实例(日历)
     *
     * @param record
     * @return
     */
    @Override
    public int createCalendarBatch(ActivityPlan record) {

        Object[] goodsArr = JSON.parseArray(record.getGoodsIdsJson()).toArray();
        Object[] keysArr = JSON.parseArray(record.getKeys()).toArray();
        List<Date> dates = DateUtil.findDates(record.getStartTime(), record.getEndTime());
        List<String> dateList = new ArrayList<>();
        for (Date d : dates) {
            dateList.add(DateFormatUtils.format(d, "yyyyMMdd"));
        }

        Date cTime = new Date();
        int total = record.getTotal();
        int keySize = keysArr.length;
        int numPerKey = total / keySize;
        int numLastKey = total % keySize == 0 ? numPerKey : total % keySize;

        int i = 0;
        int j = dateList.size() * goodsArr.length;

        List<ActivityPlanItem> param = new ArrayList<>();
        for (String dateStr : dateList) {
            for (Object goods : goodsArr) {
                i++;
                param.add(createItem(record, dateStr, goods, keysArr, cTime, i == j ? numLastKey : numPerKey));
            }
        }
        activityPlanItemMapper.deleteByActivityPlanId(record.getId());
        return activityPlanItemMapper.addActivityPlanItemBatch(param);
    }

    /**
     * 1.验证平台运营填写的计划在生成批次后,是否超出22:00
     * 2.验证平台填写的单数是否 >0 的整数
     * <p>
     * true:合法. false:不合法
     *
     * @param plan
     * @param item
     * @return
     */
    @Override
    public boolean verifyBatchAndTime(ActivityPlan plan, ActivityPlanItem item, boolean isAdd) {
        // 每次几人中奖
        int hitsPerDraw = plan.getHitsPerDraw();
        int batchTotal = 0;
        // 一共刷多少单
        int total = 0;
        //总单数计算
        String totalPerKeyJsonStr = item.getTotalPerKeyJson();
        List<Map<String, Object>> jsonList = (List<Map<String, Object>>) JSON.parse(totalPerKeyJsonStr);
        for (int i = 0; i < jsonList.size(); i++) {
            int num = 0;
            try {
                num = Integer.parseInt(jsonList.get(i).get("total").toString());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            if (num < 1) {
                return false;
            }
            total = total + num;
        }
        // 一共多少批次
        batchTotal = total % hitsPerDraw == 0 ? total / hitsPerDraw : total / hitsPerDraw + 1;

        Integer condDrawTime = plan.getCondDrawTime();
        String startTimeStr = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_START_TIME_PER_DAY);
        String endTimeStr = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_END_TIME_PER_DAY);

        int startTime = startTimeStr == null ? Constants.DEFAULT_START_TIME : Integer.parseInt(startTimeStr);
        int endTime = endTimeStr == null ? Constants.DEFAULT_END_TIME : Integer.parseInt(endTimeStr);

        int booOutOfTime = (endTime - startTime) * 60 - (batchTotal * condDrawTime);
        if (booOutOfTime < 0) {
            return false;
        }

        ActivityPlan param = new ActivityPlan();
        param.setId(plan.getId());
        if (isAdd) {
            Integer totalPerson = plan.getTotal();
            param.setTotal(plan.getTotal() == null ? 0 + total : plan.getTotal() + total);
        } else {
            Map<String, Long> paramPlanMap = new HashMap();
            paramPlanMap.put("activityPlanId", plan.getId());
            List<ActivityPlanItem> list = activityPlanItemMapper.findAllItemByPlanIdPlus(paramPlanMap);
            for (ActivityPlanItem planItem : list) {
                if (!planItem.getId().equals(item.getId())) {
                    String jsonStr = planItem.getTotalPerKeyJson();
                    List<Map> totalPersonMap = (List<Map>) JSON.parse(jsonStr);
                    for (Map m : totalPersonMap) {
                        total = total + Integer.parseInt(m.get("total").toString());
                    }
                }
            }
            param.setTotal(total);
        }
        activityPlanMapper.updateByPrimaryKeySelective(param);
        return true;
    }

    @Override
    public Integer updateByPlanIdAndDayNum(ActivityPlanItem item) {

        return activityPlanItemMapper.updateByPlanIdAndDayNum(item);
    }

    @Override
    public ActivityPlanItem findItemByPlanIdAndDayNumLimit(Long activityPlanId) {

        return activityPlanItemMapper.findItemByPlanIdAndDayNumLimit(activityPlanId);
    }

    /**
     * 辅助 : 创建 ActivityPlanItem 对象
     */
    private ActivityPlanItem createItem(ActivityPlan record, String dateStr, Object goods, Object[] keysArr, Date cTime, int numPerKey) {
        ActivityPlanItem item = new ActivityPlanItem();
        int total = record.getTotal();
        item.setMerchantId(record.getMerchantId());
        item.setActivityPlanId(record.getId());
        item.setShopId(record.getShopId());
        item.setDatetimeStr(dateStr);
        item.setGoodsId(Long.parseLong(goods.toString()));
        item.setTotalPerKeyJson(getJsonString(keysArr, numPerKey));
        item.setCtime(cTime);
        item.setUtime(cTime);
        return item;
    }

    /**
     * 判断日期是否合法
     *
     * @param dateStr
     * @return
     */
    @Override
    public boolean verifyDateTime(String dateStr) {
        Date date = DateUtil.stringtoDate(dateStr, "yyyyMMdd");
        Date dateBeforeZero = DateUtil.getDateBeforeZero(date);
        if (dateBeforeZero.getTime() > System.currentTimeMillis()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 辅助 计算关键字对应的单量 ,并组成json字符串
     */
    private String getJsonString(Object[] keysArr, int numPerKey) {
        List<Map> list = new ArrayList<>();
        for (Object key : keysArr) {
            Map m = new HashMap();
            m.put("key", key.toString());
            m.put("total", numPerKey);
            list.add(m);
        }
        return JSON.toJSONString(list);
    }


    /**
     * 生成计划实例
     */
    @Override
    public int add(ActivityPlanItem record) {
        record.setCtime(new Date());
        return activityPlanItemMapper.insertSelective(record);
    }

    /**
     * 修改计划
     */
    @Override
    public int update(ActivityPlanItem record) {
        return activityPlanItemMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByIdSelective(ActivityPlanItem record) {
        return activityPlanItemMapper.updateByPrimaryKeySelective(record);
    }


    @Override
    public List<ActivityPlanItem> findAllItemByPlanId(Long activityPlanId) {
        Map param = new HashMap();
        param.put("activityPlanId", activityPlanId);
        return activityPlanItemMapper.findAllItemByPlanId(param);
    }

    @Override
    public ActivityPlanItem findById(Long id) {
        return null;
    }

    @Override
    public List<ActivityPlanItem> findList() {
        return null;
    }

    @Override
    public int del(ActivityPlanItem record) {
        return 0;
    }
}
