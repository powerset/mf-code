package mf.code.activity.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;

import java.util.List;

/**
 * mf.code.activity.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月12日 11:22
 */
public interface ActivityDefTypeService {
    /***
     * 拆红包
     * @return
     */
    ActivityDef getOpenRedPack(List<ActivityDef> activityDefs);

    /***
     * 幸运大转盘
     * @return
     */
    ActivityDef getLuckyWheel(List<ActivityDef> activityDefs);

    /***
     * 收藏加购
     * @return
     */
    ActivityDef getFavCart(List<ActivityDef> activityDefs);

    /***
     * 好评返现
     * @return
     */
    ActivityDef getGoodsComment(List<ActivityDef> activityDefs);

    ActivityDef getGoodsCommentNew(List<ActivityDef> activityDefs);

    /***
     * 免单抽奖
     * @return
     */
    ActivityDef getOrderBack(List<ActivityDef> activityDefs);

    ActivityDef getOrderBack(List<ActivityDef> activityDefs, Long goodsId);

    ActivityDef getOrderBackNew(List<ActivityDef> activityDefs);

    /***
     * 赠品
     * @return
     */
    ActivityDef getAssist(List<ActivityDef> activityDefs);

    ActivityDef getAssistNew(List<ActivityDef> activityDefs);

    ActivityDef getAssist(List<ActivityDef> activityDefs, Long goodsId);

    /***
     * 回填订单
     * @return
     */
    ActivityDef getFillOrder(List<ActivityDef> activityDefs);

    ActivityDef getGoodsCommentWithoutGoodsId(List<ActivityDef> activityDefs);

    /***
     * 收藏加购
     * @return
     */
    ActivityTask getFavCartPo(List<ActivityTask> activityTasks);

    /***
     * 好评返现
     * @return
     */
    ActivityTask getGoodsCommentPo(List<ActivityTask> activityTasks);

    /**
     * 全额报销
     * @param activityDefList
     * @return
     */
    ActivityDef getReimbursement(List<ActivityDef> activityDefList);
}
