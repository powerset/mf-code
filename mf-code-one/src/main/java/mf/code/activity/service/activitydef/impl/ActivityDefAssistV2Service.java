package mf.code.activity.service.activitydef.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefFinishTypeEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.activitydef.AbstractActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.repo.po.GoodsPlus;
import mf.code.goods.service.GoodsPlusService;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.activity.service.activitydef.impl
 * Description:
 *
 * @author gel
 * @date 2019-05-23 16:29
 */
@Slf4j
@Service
public class ActivityDefAssistV2Service extends AbstractActivityDefService {

    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;
    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsPlusService goodsPlusService;
    @Autowired
    private GoodsAppService goodsAppService;

    @Override
    protected SimpleResponse checkDefForCreate(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Merchant merchant = this.merchantService.getMerchant(req.getMerchantId());
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(req.getShopId());
        if (merchant == null || merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常，商户不存在or店铺不存在 to木子");
        }
        if (req.getExtraDataObject() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少必要参数");
        }
        /*
        finishType 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
        auditType 审核方式（1:人工审核 2:自动审核）
        skuId 任务关联的sku（仅微信小程序下单存在）
        goodsMainPic 商户自定义商品主图（仅淘宝下单存在）
         */
        Integer finishType = req.getExtraDataObject().getFinishType();
        if (finishType == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少下单方式选择");
        }
        // 如果是微信下单则需要 skuId 任务关联的sku（仅微信小程序下单存在）
        if (finishType == ActivityDefFinishTypeEnum.WEICHAT.getCode()) {
            if (req.getExtraDataObject().getSkuId() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少选择sku信息");
            }
        }
        // 如果是淘宝下单则需要 goodsMainPic 商户自定义商品主图（仅淘宝下单存在）
        if (finishType == ActivityDefFinishTypeEnum.WEICHAT.getCode()) {
            // 这里可能没有
            if (req.getExtraDataObject().getGoodsMainPic() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少商品主图");
            }
        }
        if (req.getExtraDataObject().getAuditType() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "传参异常,缺少审核方式");
        }
        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        if (!success) {
            return RedisForbidRepeat.NXError();
        }
        // 赠品活动只有一个商品
        if (StringUtils.isBlank(req.getGoodsIds())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "传参异常，请选择商品");
        }
        Set<Long> goodsIdSet = new HashSet<>();
        String[] split = req.getGoodsIds().split(",");
        //非收藏，好评，回填时，一个商品存储goodsId
        for (String str : split) {
            goodsIdSet.add(Long.valueOf(str));
        }
        if (goodsIdSet.size() <= 0 || goodsIdSet.size() > 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "传参异常，只能选择一个商品");
        }
        // 此时goodsId为productid，需要查询到goodsId
        Long goodsId = new ArrayList<>(goodsIdSet).get(0);
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Goods::getMerchantId, req.getMerchantId())
                .eq(Goods::getShopId, req.getShopId())
                .eq(Goods::getProductId, goodsId)
                .orderByDesc(Goods::getId);
        if (req.getExtraDataObject().getSkuId() == null) {
            queryWrapper.lambda().isNull(Goods::getSkuId);
        } else {
            queryWrapper.lambda().eq(Goods::getSkuId, req.getExtraDataObject().getSkuId());
        }
        List<Goods> list = goodsService.list(queryWrapper);
        if (CollectionUtils.isEmpty(list)) {
            goodsId = 0L;
        } else {
            goodsId = list.get(0).getId();
        }
        // 校验是否有创建活动的资格
        simpleResponse = activityDefCheckService.createAble(req.getMerchantId(), req.getShopId(), req.getType(), goodsId, null, null);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    protected SimpleResponse calcDeposit(CreateDefReq req) {
        if (req.getActivityDefId() != null && req.getActivityDefId() > 0) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
            Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该编号的活动不存在哦！defId=" + req.getActivityDefId());
            req.from(activityDef);
            req.setExtraDataObject(new CreateDefReq.ExtraData());
            req.getExtraDataObject().from(activityDef);
        } else {
            req.init();
        }
        //若此时没有穿库存数
        if (req.getStockDef() == null || req.getStockDef() == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请输入正确的库存");
        }
        return this.activityDefAboutService.getDepositByProduct(req);
    }

    @Override
    public SimpleResponse createDef(CreateDefReq req) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Set<Long> goodsIdSet = new HashSet<>();
        String[] split = req.getGoodsIds().split(",");
        //非收藏，好评，回填时，一个商品存储goodsId
        for (String str : split) {
            goodsIdSet.add(Long.valueOf(str));
        }
        if (goodsIdSet.size() <= 0 || goodsIdSet.size() > 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "传参异常，只能选择一个商品");
        }
        List<ProductEntity> productEntityList = goodsAppService.queryProductDetailList(new ArrayList<>(goodsIdSet));
        if (CollectionUtils.isEmpty(productEntityList)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "网络异常，请稍后重试");
        }
        ProductEntity productEntity = productEntityList.get(0);
        if (productEntity.getShowType() == ProductShowTypeEnum.NEWBIE.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "新手专区商品不能参与活动");
        }
        if (!productEntity.getParentId().equals(productEntity.getId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "分销商品不能参与活动");
        }
        Goods goods = goodsService.createOrUpdateGoodsByActivityDef(req, productEntity);

        // 为保证与之前活动逻辑复用，暂时添加
        req.setGoodsIds(goods.getId() + "");
        ActivityDef activityDef = this.activityDefAboutService.addActivityDef(req);
        if (activityDef.getHitsPerDraw() == null || activityDef.getHitsPerDraw() == 0) {
            activityDef.setHitsPerDraw(1);
        }
        boolean isSuccess = this.activityDefService.saveOrUpdate(activityDef);
        if (!isSuccess) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "存储活动异常 To @夜辰");
        }
        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }
        // 处理返回数据
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }

    @Override
    protected SimpleResponse updateDef(CreateDefReq req) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDef, ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动编号的活动不存在：defId=" + req.getActivityDefId());

        if (activityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已清算状态，无法修改");
        }
        req.init();
        String keyJson = "";
        if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
            String keyWord = req.getExtraDataObject().getKeywords();
            if (StringUtils.isNotBlank(keyWord)) {
                String[] split = keyWord.split(",");
                keyJson = JSON.toJSONString(Arrays.asList(split));
            }
        }
        if (!StringUtils.equals(activityDef.getKeyWord(), keyJson)) {
            goodsPlusService.saveGoodskeys(req.getMerchantId(), activityDef.getGoodsId(), keyJson);
        }
        //非草稿修改，支持部分修改,草稿修改，任意修改
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            //草稿修改
            // 如果需要修改价格和主图
            if (activityDef.getGoodsId() != null) {
                Goods goods = goodsService.selectById(activityDef.getGoodsId());
                boolean needUpdate = false;
                if (StringUtils.isNotBlank(req.getExtraDataObject().getGoodsMainPic())
                        && !StringUtils.equals(goods.getPicUrl(), req.getExtraDataObject().getGoodsMainPic())) {
                    goods.setDisplayBanner(req.getExtraDataObject().getGoodsMainPic());
                    goods.setPicUrl(req.getExtraDataObject().getGoodsMainPic());
                    needUpdate = true;
                }
                BigDecimal displayPrice = new BigDecimal(req.getExtraDataObject().getGoodsPrice());
                if (req.getExtraDataObject().getGoodsPrice() != null
                        && goods.getDisplayPrice().compareTo(displayPrice) != 0) {
                    goods.setDisplayPrice(displayPrice);
                    needUpdate = true;
                }
                if (needUpdate) {
                    goodsService.updateGoods(goods);
                }
            }
            return this.activityDefUpdateService.activityDefUpdateSave(req, activityDef);
        } else {
            //非草稿修改
            return this.activityDefUpdateService.activityDefUpdateNotSave(req, activityDef);
        }
    }

    @Override
    public ActivityDefTypeEnum getType() {
        return ActivityDefTypeEnum.ASSIST_V2;
    }
}
