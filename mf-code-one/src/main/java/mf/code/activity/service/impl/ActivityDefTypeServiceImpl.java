package mf.code.activity.service.impl;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefTypeService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.activity.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月12日 11:23
 */
@Service
public class ActivityDefTypeServiceImpl implements ActivityDefTypeService {

    /***
     * 拆红包
     * @return
     */
    @Override
    public ActivityDef getOpenRedPack(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean openRedPack = activityDef.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
            if (openRedPack && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 幸运大转盘
     * @return
     */
    @Override
    public ActivityDef getLuckyWheel(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean luckyWheel = activityDef.getType().equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
            if (luckyWheel && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 收藏加购
     * @return
     */
    @Override
    public ActivityDef getFavCart(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean favCart = activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode()) ||
                    activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode());
            if (favCart && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 好评晒图
     * @return
     */
    @Override
    public ActivityDef getGoodsComment(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean openRedPack = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode());
            if (openRedPack && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 好评晒图
     * @return
     */
    @Override
    public ActivityDef getGoodsCommentWithoutGoodsId(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean openRedPack = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode());
            if (!openRedPack) {
                openRedPack = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode());
            }
            if (openRedPack && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 好评晒图
     * @return
     */
    @Override
    public ActivityDef getGoodsCommentNew(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean coodsComment = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode());
            if (coodsComment && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 回填订单
     * @return
     */
    @Override
    public ActivityDef getFillOrder(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean fillOrder = activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) ||
                    activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode());
            if (fillOrder && activityDef.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 收藏加购
     * @return
     */
    @Override
    public ActivityTask getFavCartPo(List<ActivityTask> activityTasks) {
        if (CollectionUtils.isEmpty(activityTasks)) {
            return null;
        }
        for (ActivityTask activityTask : activityTasks) {
            if (activityTask.getType() == ActivityTaskTypeEnum.FAVCART.getCode() && activityTask.getRpStock() != null && activityTask.getRpStock() > 0) {
                return activityTask;
            }
        }
        return null;
    }
//

    /***
     * 好评晒图
     * @return
     */
    @Override
    public ActivityTask getGoodsCommentPo(List<ActivityTask> activityTasks) {
        if (CollectionUtils.isEmpty(activityTasks)) {
            return null;
        }
        for (ActivityTask activityTask : activityTasks) {
            if (activityTask.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()
                    && activityTask.getRpStock() != null
                    && activityTask.getRpStock() > 0) {
                return activityTask;
            }
        }
        return null;
    }

    @Override
    public ActivityDef getReimbursement(List<ActivityDef> activityDefList) {
        for (ActivityDef activityDef : activityDefList) {
            boolean coodsComment = activityDef.getType().equals(ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode());
            if (coodsComment && activityDef.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 赠品
     * @return
     */
    @Override
    public ActivityDef getAssist(List<ActivityDef> activityDefs) {
        ActivityDef activityDef1 = null;
        for (ActivityDef activityDef : activityDefs) {
            boolean newMan = activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode());
            boolean assist = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST.getCode());
            boolean assist2 = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST_V2.getCode());
            if ((newMan || assist || assist2) && activityDef.getStock() > 0) {
                activityDef1 = activityDef;
            }
        }
        return activityDef1;
    }

    /***
     * 赠品
     * @return
     */
    @Override
    public ActivityDef getAssist(List<ActivityDef> activityDefs, Long goodsId) {
        ActivityDef activityDef1 = null;
        for (ActivityDef activityDef : activityDefs) {
            boolean newMan = activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode());
            boolean assist = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST.getCode());
            boolean assist2 = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST_V2.getCode());
            if ((newMan || assist || assist2) && activityDef.getStock() > 0 && goodsId.equals(activityDef.getGoodsId())) {
                activityDef1 = activityDef;
            }
        }
        return activityDef1;
    }

    /***
     * 赠品
     * @return
     */
    @Override
    public ActivityDef getAssistNew(List<ActivityDef> activityDefs) {
        ActivityDef activityDef1 = null;
        for (ActivityDef activityDef : activityDefs) {
            boolean assist = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST_V2.getCode());
            if (assist && activityDef.getStock() > 0) {
                activityDef1 = activityDef;
            }
        }
        return activityDef1;
    }

    /***
     * 免单抽奖
     * @return
     */
    @Override
    public ActivityDef getOrderBack(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean orderBack = activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode());
            if (!orderBack) {
                orderBack = activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
            }
            if (orderBack && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 免单抽奖
     * @return
     */
    @Override
    public ActivityDef getOrderBack(List<ActivityDef> activityDefs, Long goodsId) {
        for (ActivityDef activityDef : activityDefs) {
            boolean orderBack = activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode());
            if (!orderBack) {
                orderBack = activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
            }
            if (orderBack && activityDef.getStock() > 0 && goodsId.equals(activityDef.getGoodsId())) {
                return activityDef;
            }
        }
        return null;
    }

    /***
     * 免单抽奖
     * @return
     */
    @Override
    public ActivityDef getOrderBackNew(List<ActivityDef> activityDefs) {
        for (ActivityDef activityDef : activityDefs) {
            boolean orderBack = activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
            if (orderBack && activityDef.getStock() > 0) {
                return activityDef;
            }
        }
        return null;
    }
}
