package mf.code.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.ActivityOrder;

import java.util.List;
import java.util.Map;

/**
 * mf.code.activity.service
 * Description:
 *
 * @author: gel
 * @date: 2018-11-08 17:34
 */
public interface ActivityOrderService extends IService<ActivityOrder> {


    /**
     * 创建
     * @param activityOrder
     * @return
     */
    Integer createActivityOrder(ActivityOrder activityOrder);

    /**
     * 根据商户，店铺，用户，订单号查询回填订单
     * @param merchantId
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    List<ActivityOrder> selectByOrderId(Long merchantId, Long shopId, Long userId, String orderId);

    List<ActivityOrder> selectByOrderIdOne(Long merchantId, Long shopId, Long userId, Long goodsId, String orderId);

    /***
     * 根据条件查询记录
     * @param params
     * @return
     */
    List<ActivityOrder> selectByParams(Map<String, Object> params);

    /**
     *  根据条件来查询符合条件的数据库数目
     *
     * @param params
     */
    int countActivityOrder(Map<String, Object> params);

    /**
     *  更新数据
     *
     * @param activityOrder
     */
    int updateActivityOrder(ActivityOrder activityOrder);

}
