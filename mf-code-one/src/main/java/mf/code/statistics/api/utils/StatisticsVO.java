package mf.code.statistics.api.utils;

import mf.code.user.repo.po.User;

import java.util.*;

/**
 * mf.code.api.statistics.utils
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-29 上午10:40
 */
public class StatisticsVO {

    public static Map<String, Object> initResultVO2queryActivityDefUserJoin() {
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("startTime", "");
        resultVO.put("userJoinCount", 0);
        resultVO.put("userAwardCount", 0);
        resultVO.put("userCashbackCount", 0);
        resultVO.put("totalCashbackAmount", 0);
        resultVO.put("stock", 0);
        List<Map<String, Object>> userInfoList = new ArrayList<>();
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("nick", "");
        userInfo.put("phone", "");
        userInfo.put("awardStatus", "");
        userInfo.put("cashbackStatus", "");
        userInfoList.add(userInfo);
        resultVO.put("userInfoList", userInfoList);
        return resultVO;
    }

    public static List<Map> getUserInfoList(List userIdList, List winnerUserIdList, Map<Long, User> userMap, Map cashbackUserMap) {
        ArrayList<Map> userInfoList = new ArrayList<>();
        int index = 0;
        for (Object userId : userIdList) {
            Long uid = (Long)userId;
            User user = userMap.get(uid);
            if (null == user) {
                continue;
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            String mobile = user.getMobile();
            userInfo.put("mobile", mobile == null ? "未绑定" : mobile);
            userInfo.put("awardStatus", "未中奖");
            userInfo.put("cashbackStatus", "-");
            userInfo.put("sort", 1000000+index);
            if (winnerUserIdList.contains(user.getId())) {
                userInfo.put("awardStatus", "中奖");
                userInfo.put("cashbackStatus", "未完成返现");
                userInfo.put("sort", 100000+index);
                winnerUserIdList.remove(user.getId());
            }
            if (cashbackUserMap.containsKey(user.getId())) {
                userInfo.put("cashbackStatus", cashbackUserMap.get(user.getId()));
                userInfo.put("sort", index);
                cashbackUserMap.remove(user.getId());
            }
            index ++;
            userInfoList.add(userInfo);
        }
        userInfoList.sort(Comparator.comparingInt(o -> (int) o.get("sort")));
        return userInfoList;
    }
}
