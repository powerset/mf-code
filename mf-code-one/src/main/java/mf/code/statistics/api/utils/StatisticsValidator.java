package mf.code.statistics.api.utils;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BaseValidator;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.api.statistics.utils
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-28 下午9:14
 */
@Slf4j
public class StatisticsValidator extends BaseValidator {

    public static SimpleResponse<Object> checkParam4queryActivityDefUserJoin(String current, String size, String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(current) || !RegexUtils.StringIsNumber(current)) {
            log.error("页码current设置不正确，to 木子");
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("页码current设置不正确，to 木子");
            return response;
        }
        if (StringUtils.isBlank(size) || !RegexUtils.StringIsNumber(size)) {
            log.error("页面展示条数size设置不正确，to 木子");
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("页面展示条数size设置不正确，to 木子");
            return response;
        }
        if (StringUtils.isBlank(activityDefId) || !RegexUtils.StringIsNumber(activityDefId)) {
            log.error("活动定义activityDefId设置不正确，to 木子");
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("活动定义activityDefId设置不正确，to 木子");
            return response;
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }
}
