package mf.code.statistics.api.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.statistics.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-28 下午8:45
 */
public interface SellerStatisticsService {

    SimpleResponse<Object> queryActivityDefUserJoin(String current, String size, String activityDefId, boolean all, String rpType);

    /**
     * 获取某个活动定义的占用库存数
     * 对免单商品的占用库存数计算：进行中的活动，未开团的活动 占用库存数为 中奖人数；已开团的人数 占用库存数为 中奖人数 - 中奖任务完成人数
     * 对赠品（助力活动）的占用库存数计算：进行中的活动 数量 就是 占用库存数
     * 对轮盘抽奖的占用库存数计算：0 无占用库存数概念
     * 对红包活动的占用库存数计算：进行中的红包任务 数量 就是 占用库存数
     *
     * @param activityDefId
     * @param activityDefTypeVO 对应前端所传活动类型与数据库的活动类型
     * @param activityDeftype
     * @return
     */
    int getOccupyStock(Long activityDefId, String activityDefTypeVO, Integer activityDeftype);
}
