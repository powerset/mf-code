package mf.code.statistics.api.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.utils.SellerTransformParam;
import mf.code.statistics.api.service.SellerStatisticsService;
import mf.code.statistics.api.utils.StatisticsVO;
import mf.code.statistics.api.utils.StatisticsValidator;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.PageUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.statistics.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-28 下午8:52
 */
@Slf4j
@Service
public class SellerStatisticsServiceImpl implements SellerStatisticsService {
    private final ActivityDefService activityDefService;
    private final ActivityService activityService;
    private final UserActivityService userActivityService;
    private final UserCouponService userCouponService;
    private final UserService userService;
    private final UserTaskService userTaskService;
    private final ActivityTaskService activityTaskService;
    private final StringRedisTemplate stringRedisTemplate;

    @Autowired
    public SellerStatisticsServiceImpl(ActivityDefService activityDefService,
                                       ActivityService activityService,
                                       UserActivityService userActivityService,
                                       UserCouponService userCouponService,
                                       UserService userService,
                                       StringRedisTemplate stringRedisTemplate,
                                       UserTaskService userTaskService,
                                       ActivityTaskService activityTaskService) {
        this.activityDefService = activityDefService;
        this.activityService = activityService;
        this.userActivityService = userActivityService;
        this.userCouponService = userCouponService;
        this.userService = userService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.userTaskService = userTaskService;
        this.activityTaskService = activityTaskService;
    }

    @Override
    public SimpleResponse<Object> queryActivityDefUserJoin(String current, String size, String activityDefId, boolean all, String rpType) {
        // 参数校验
        SimpleResponse<Object> response = StatisticsValidator.checkParam4queryActivityDefUserJoin(current, size, activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }

        // 转化参数
        Integer crt = Integer.valueOf(current);
        Integer sz = Integer.valueOf(size);
        Long adid = Long.valueOf(activityDefId);

        // 查询redis缓存
        String userInfoPageJSON = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITYDEF_USER_JOIN_INFO + activityDefId);
        String resultJSON = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITYDEF_JOIN_INFO + activityDefId);
        if (JsonParseUtil.booJsonObj(userInfoPageJSON) && JsonParseUtil.booJsonObj(resultJSON)) {
            // Map<Integer, List<Map<String, Object>>> pageMap = new HashMap<>();
            JSONObject userInfoPage = JSON.parseObject(userInfoPageJSON);
            Object pageUserList = userInfoPage.get(crt);
            // Map<String, Object>
            JSONObject result = JSON.parseObject(resultJSON);
            result.put("current", crt);
            result.put("userInfoList", pageUserList);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(result);
            return response;
        }

        // 获取活动定义信息
        ActivityDef activityDef = activityDefService.getById(adid);
        if (null == activityDef) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("errorMessage", "数据库未查到ActivityDef活动的信息，id = " + adid);
            response.setData(resultVO);
            return response;
        }
        String type = SellerTransformParam.PO2VOActivityDefType(activityDef.getType());
        // 获取机器人
        List<User> robots = userService.listRobot();
        // 初始化展示数据


        if (StringUtils.equals(type, SellerTransformParam.GIFT_TYPE)) {
            return giftUserInfo(crt, sz, activityDef, all);
        }
        if (StringUtils.equals(type, SellerTransformParam.LUCKY_WHEEL_TYPE)) {
            return luckyWheelUserInfo(crt, sz, activityDef, all);
        }
        if (StringUtils.equals(type, SellerTransformParam.FREE_GOODS_TYPE)) {
            return freeOrderUserInfo(crt, sz, activityDef, all, robots);
        }
        if (StringUtils.equals(type, SellerTransformParam.RED_PACKET_TYPE)) {
            return redPacketUserInfo(all, rpType, crt, sz, activityDef);
        }

        response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
        response.setMessage("所传的活动类型type和校验的活动类型type无法对应，须沟通解决 to 木子 and 百川");
        return response;
    }

    private SimpleResponse<Object> redPacketUserInfo(boolean all, String rpType, Integer crt, Integer sz, ActivityDef activityDef) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, Object> resultVO = StatisticsVO.initResultVO2queryActivityDefUserJoin();
        // 红包任务 统计页
        QueryWrapper<ActivityTask> atWrapper = new QueryWrapper<>();
        Long activityDefId = activityDef.getId();
        atWrapper.lambda()
                .eq(ActivityTask::getActivityDefId, activityDefId);
        List<ActivityTask> activityTasks = activityTaskService.list(atWrapper);
        if (CollectionUtils.isEmpty(activityTasks)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        // 获取 红包任务 的所有对应id
        List<Long> activityTaskIdList = new ArrayList<>();
        // 所有类型的剩余库存和
        int allStock = 0;
        // 回填订单的剩余库存
        int orderStock = 0;
        // 收藏加购的剩余库存
        int favcartStock = 0;
        // 好评晒图的剩余库存
        int goodcommentStock = 0;
        for (ActivityTask activityTask : activityTasks) {
            activityTaskIdList.add(activityTask.getId());
            Integer rpStock = activityTask.getRpStock();
            allStock = allStock + rpStock;
            if (activityTask.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()) {
                orderStock = orderStock + rpStock;
            }
            if (activityTask.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()) {
                goodcommentStock = goodcommentStock + rpStock;
            }
            if (activityTask.getType() == ActivityTaskTypeEnum.FAVCART.getCode()) {
                favcartStock = favcartStock + rpStock;
            }
        }

        // 获取 对应 类型的 剩余库存
        List<Integer> utTypeList = new ArrayList<>();
        if (StringUtils.isBlank(rpType) || !RegexUtils.StringIsNumber(rpType)) {
            utTypeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            utTypeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            utTypeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            resultVO.put("stock", allStock);
        }
        if (StringUtils.equals(SellerTransformParam.ORDER_RPTYPE, rpType)) {
            utTypeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            resultVO.put("stock", orderStock);
        }
        if (StringUtils.equals(SellerTransformParam.FAVCART_RPTYPE, rpType)) {
            utTypeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            resultVO.put("stock", favcartStock);
        }
        if (StringUtils.equals(SellerTransformParam.GOOD_COMMENT_RPTYPE, rpType)) {
            utTypeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            resultVO.put("stock", goodcommentStock);
        }

        QueryWrapper<UserTask> utWrapper = new QueryWrapper<>();
        utWrapper.lambda()
                .in(UserTask::getType, utTypeList)
                .in(UserTask::getActivityTaskId, activityTaskIdList);
        List<UserTask> userTasks = userTaskService.list(utWrapper);
        if (CollectionUtils.isEmpty(userTasks)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        resultVO.put("userJoinCount", userTasks.size());
        resultVO.put("userAwardCount", userTasks.size());
        if (!all) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        resultVO.put("startTime", DateUtil.dateToString(activityDef.getStartTime(), DateUtil.FORMAT_TWO));
        resultVO.put("total", userTasks.size());

        List<Long> allUserIdList = new ArrayList<>();
        List<Long> cashbackUserIdList = new ArrayList<>();
        BigDecimal totalCashbackAmount = BigDecimal.ZERO;
        for (UserTask userTask : userTasks) {
            Long userId = userTask.getUserId();
            allUserIdList.add(userId);
            if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode() && userTask.getRpAmount() != null) {
                cashbackUserIdList.add(userId);
                totalCashbackAmount = totalCashbackAmount.add(userTask.getRpAmount());
            }
        }
        resultVO.put("userCashbackCount", cashbackUserIdList.size());
        resultVO.put("totalCashbackAmount", totalCashbackAmount);

        // 获取用户信息；
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.in("id", allUserIdList);
        List<User> users = userService.list(userWrapper);
        Map<Long, User> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.getId(), user);
        }

        // 加工信息
        List<Map> userInfoList = new ArrayList<>();
        int index = 0;
        for (UserTask userTask : userTasks) {
            Long userId = userTask.getUserId();
            User user = userMap.get(userId);
            if (null == user) {
                continue;
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            String mobile = user.getMobile();
            userInfo.put("mobile", mobile == null ? "未绑定" : mobile);
            userInfo.put("awardStatus", "请忽略此字段");
            userInfo.put("cashbackStatus", "未完成");
            userInfo.put("sort", 1000000 + index);
            if (cashbackUserIdList.contains(userId)) {
                userInfo.put("cashbackStatus", userTask.getRpAmount());
            }
            index++;
            userInfoList.add(userInfo);
        }
        userInfoList.sort(Comparator.comparingInt(o -> (int) o.get("sort")));
        Map<Integer, List<Map>> pageMap = PageUtil.VO2PageVO(userInfoList, sz);
        List<Map> pageUserList = pageMap.get(crt);
        resultVO.put("pages", pageMap.size());
        resultVO.put("current", crt);
        resultVO.put("size", sz);
        resultVO.put("userInfoList", pageUserList);

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_USER_JOIN_INFO + activityDefId, JSON.toJSONString(pageMap), 5, TimeUnit.MINUTES);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_JOIN_INFO + activityDefId, JSON.toJSONString(resultVO), 5, TimeUnit.MINUTES);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    private SimpleResponse<Object> freeOrderUserInfo(Integer crt, Integer sz, ActivityDef activityDef, boolean all, List<User> robots) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, Object> resultVO = StatisticsVO.initResultVO2queryActivityDefUserJoin();
        // 免单抽奖 统计页
        Map<String, Object> userJoinMap = userActivityService.queryActivityDefUserJoinInfo(activityDef.getId(), robots);

        resultVO.put("stock", activityDef.getStock());
        if (null == userJoinMap) {
            response.setData(resultVO);
            return response;
        }
        List allUserIdList = (List) userJoinMap.get("allUserIdList");
        List winnerUserIdList = (List) userJoinMap.get("winnerUserIdList");
        if (null == winnerUserIdList) {
            winnerUserIdList = new ArrayList();
        }
        Map cashbackUserMap = (Map) userJoinMap.get("cashbackUserMap");
        if (null == cashbackUserMap) {
            cashbackUserMap = new HashMap();
        }

        // 获取此任务的所有用户信息
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.in("id", allUserIdList);
        List<User> users = userService.list(userWrapper);

        Map<Long, User> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.getId(), user);
        }

        resultVO.put("userAwardCount", winnerUserIdList.size());
        int userCashbackCount = cashbackUserMap.size();
        resultVO.put("userCashbackCount", userCashbackCount);
        BigDecimal totalCashbackAmount = BigDecimal.ZERO;

        for (Object value : cashbackUserMap.values()) {
            totalCashbackAmount = totalCashbackAmount.add((BigDecimal) value);
        }
        resultVO.put("totalCashbackAmount", totalCashbackAmount);

        // 获取所有用户的参与情况
        List<Map> userInfoList = StatisticsVO.getUserInfoList(allUserIdList, winnerUserIdList, userMap, cashbackUserMap);
        resultVO.put("userJoinCount", userInfoList.size());
        if (!all) {
            response.setData(resultVO);
            return response;
        }

        resultVO.put("startTime", DateUtil.dateToString(activityDef.getStartTime(), DateUtil.FORMAT_TWO));
        // 分页展示
        Map<Integer, List<Map>> pageMap = PageUtil.VO2PageVO(userInfoList, sz);

        List<Map> pageUserList = pageMap.get(crt);
        resultVO.put("total", userInfoList.size());
        resultVO.put("pages", pageMap.size());
        resultVO.put("current", crt);
        resultVO.put("size", sz);
        resultVO.put("userInfoList", pageUserList);

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_USER_JOIN_INFO + activityDef.getId(), JSON.toJSONString(pageMap), 5, TimeUnit.MINUTES);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_JOIN_INFO + activityDef.getId(), JSON.toJSONString(resultVO), 5, TimeUnit.MINUTES);
        response.setData(resultVO);
        return response;
    }

    private SimpleResponse<Object> luckyWheelUserInfo(Integer crt, Integer sz, ActivityDef activityDef, boolean all) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, Object> resultVO = StatisticsVO.initResultVO2queryActivityDefUserJoin();
        // 幸运大转盘 统计页
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        Long activityDefId = activityDef.getId();
        aWrapper.eq("activity_def_id", activityDefId);
        List<Activity> activities = activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            response.setData(resultVO);
            return response;
        }

        // 获取对应活动id的集合
        List<Long> activityIdList = new ArrayList<>();
        for (Activity activity : activities) {
            activityIdList.add(activity.getId());
        }
        QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
        ucWrapper.lambda()
                .in(UserCoupon::getActivityId, activityIdList);
        List<UserCoupon> userCoupons = userCouponService.list(ucWrapper);
        if (CollectionUtils.isEmpty(userCoupons)) {
            response.setData(resultVO);
            return response;
        }

        resultVO.put("userJoinCount", userCoupons.size());
        if (!all) {
            response.setData(resultVO);
            return response;
        }
        resultVO.put("startTime", DateUtil.dateToString(activityDef.getStartTime(), DateUtil.FORMAT_TWO));
        resultVO.put("total", userCoupons.size());

        List<Long> allUserIdList = new ArrayList<>();
        List<Long> winnerUserIdList = new ArrayList<>();
        BigDecimal totalCashbackAmount = BigDecimal.ZERO;
        for (UserCoupon userCoupon : userCoupons) {
            Long uid = userCoupon.getUserId();
            allUserIdList.add(uid);
            totalCashbackAmount = totalCashbackAmount.add(userCoupon.getAmount());
            if (!new BigDecimal(0.00).equals(userCoupon.getAmount())) {
                winnerUserIdList.add(uid);
            }
        }

        // 获取此任务的所有用户信息
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.in("id", allUserIdList);
        List<User> users = userService.list(userWrapper);

        Map<Long, User> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.getId(), user);
        }

        resultVO.put("userAwardCount", winnerUserIdList.size());
        resultVO.put("userCashbackCount", winnerUserIdList.size());
        resultVO.put("totalCashbackAmount", totalCashbackAmount);
        resultVO.put("stock", activityDef.getStock());

        List<Map> userInfoList = new ArrayList<>();
        int index = 0;
        for (UserCoupon userCoupon : userCoupons) {
            User user = userMap.get(userCoupon.getUserId());
            if (null == user) {
                continue;
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            String mobile = user.getMobile();
            userInfo.put("mobile", mobile == null ? "未绑定" : mobile);
            userInfo.put("awardStatus", "未中奖");
            userInfo.put("cashbackStatus", "-");
            userInfo.put("sort", 1000000 + index);
            if (!new BigDecimal(0.00).equals(userCoupon.getAmount())) {
                userInfo.put("awardStatus", "中奖");
                userInfo.put("cashbackStatus", userCoupon.getAmount());
                userInfo.put("sort", index);
            }
            index++;
            userInfoList.add(userInfo);
        }
        userInfoList.sort(Comparator.comparingInt(o -> (int) o.get("sort")));
        Map<Integer, List<Map>> pageMap = PageUtil.VO2PageVO(userInfoList, sz);
        List<Map> pageUserList = pageMap.get(crt);
        resultVO.put("pages", userInfoList.size());
        resultVO.put("current", crt);
        resultVO.put("size", sz);
        resultVO.put("userInfoList", pageUserList);

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_USER_JOIN_INFO + activityDefId, JSON.toJSONString(pageMap), 5, TimeUnit.MINUTES);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_JOIN_INFO + activityDefId, JSON.toJSONString(resultVO), 5, TimeUnit.MINUTES);
        response.setData(resultVO);
        return response;
    }

    private SimpleResponse<Object> giftUserInfo(Integer crt, Integer sz, ActivityDef activityDef, boolean all) {
        Map<String, Object> resultVO = StatisticsVO.initResultVO2queryActivityDefUserJoin();
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 赠品 统计页
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        Long activityDefId = activityDef.getId();
        aWrapper.lambda().eq(Activity::getActivityDefId, activityDefId);
        List<Activity> activities = activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        resultVO.put("userJoinCount", activities.size());
        if (!all) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        resultVO.put("startTime", DateUtil.dateToString(activityDef.getStartTime(), DateUtil.FORMAT_TWO));
        resultVO.put("total", activities.size());

        List<Long> activityIdList = new ArrayList<>();
        List<Long> allUserIdList = new ArrayList<>();
        List<Long> winnerUserIdList = new ArrayList<>();
        BigDecimal totalCashbackAmount = BigDecimal.ZERO;
        int userCashbackCount = 0;
        for (Activity activity : activities) {
            Long uid = activity.getUserId();
            allUserIdList.add(uid);
            activityIdList.add(activity.getId());
            if (activity.getAwardStartTime() != null) {
                winnerUserIdList.add(uid);
            }
        }

        // 是否完成 中奖任务
        List<Long> cashbackUserIdList = new ArrayList<>();
        QueryWrapper<UserTask> utWrapper = new QueryWrapper<>();
        List<Integer> typeList = new ArrayList<>();
        typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
        typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
        utWrapper.lambda()
                .in(UserTask::getUserId, winnerUserIdList)
                .in(UserTask::getActivityId, activityIdList)
                .in(UserTask::getType, typeList)
                .eq(UserTask::getStatus, UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        List<UserTask> userTasks = userTaskService.list(utWrapper);
        if (CollectionUtils.isNotEmpty(userTasks)) {
            for (UserTask userTask : userTasks) {
                if (userTask.getRpAmount() != null) {
                    cashbackUserIdList.add(userTask.getUserId());
                    totalCashbackAmount = totalCashbackAmount.add(userTask.getRpAmount());
                    userCashbackCount++;
                }
            }
        }

        resultVO.put("userAwardCount", winnerUserIdList.size());
        resultVO.put("userCashbackCount", userCashbackCount);
        resultVO.put("totalCashbackAmount", totalCashbackAmount);
        resultVO.put("stock", activityDef.getStock());

        // 获取此任务的所有用户信息
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.in("id", allUserIdList);
        List<User> users = userService.list(userWrapper);

        Map<Long, User> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.getId(), user);
        }

        List<Map> userInfoList = new ArrayList<>();
        int index = 0;
        for (Activity activity : activities) {
            Long userId = activity.getUserId();
            User user = userMap.get(userId);
            if (null == user) {
                continue;
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            String mobile = user.getMobile();
            userInfo.put("mobile", mobile == null ? "未绑定" : mobile);
            userInfo.put("awardStatus", "未成团");
            userInfo.put("cashbackStatus", "-");
            userInfo.put("sort", 1000000 + index);
            if (activity.getAwardStartTime() != null) {
                userInfo.put("awardStatus", "已成团");
                userInfo.put("cashbackStatus", "未完成");
                userInfo.put("sort", 100000 + index);
                if (cashbackUserIdList.contains(userId)) {
                    userInfo.put("cashbackStatus", activity.getPayPrice());
                    userInfo.put("sort", index);
                }
            }
            index++;
            userInfoList.add(userInfo);
        }
        userInfoList.sort(Comparator.comparingInt(o -> (int) o.get("sort")));
        Map<Integer, List<Map>> pageMap = PageUtil.VO2PageVO(userInfoList, sz);
        List<Map> pageUserList = pageMap.get(crt);
        resultVO.put("pages", pageMap.size());
        resultVO.put("current", crt);
        resultVO.put("size", sz);
        resultVO.put("userInfoList", pageUserList);

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_USER_JOIN_INFO + activityDefId, JSON.toJSONString(pageMap), 5, TimeUnit.MINUTES);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITYDEF_JOIN_INFO + activityDefId, JSON.toJSONString(resultVO), 5, TimeUnit.MINUTES);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    @Override
    public int getOccupyStock(Long activityDefId, String activityDefTypeVO, Integer activityDefType) {
        // TODO 列表数据查询待修改
        // 获取此活动定义下 的进行中活动
        List<Activity> activityList = activityDefService.listOngoindActivity(activityDefId);
        activityDefTypeVO = activityDefType != null ?
                SellerTransformParam.PO2VOActivityDefType(activityDefType)
                : activityDefTypeVO != null ?
                activityDefTypeVO : SellerTransformParam.PO2VOActivityDefType(activityDefService.getById(activityDefId).getType());

        if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.GIFT.getCode()))
                || activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.ASSIST.getCode()))
                || activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.ASSIST_V2.getCode()))) {
            // 助力裂变逻辑 down
            return activityList.size();
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.LUCKY_WHEEL.getCode()))) {
            // 幸运大转盘逻辑 down
            return 0;
        } else if (StringUtils.equals(activityDefTypeVO, SellerTransformParam.RED_PACKET_TYPE)) {
            // 红包活动逻辑 down

            // 先获取 List<ActivityTask> def对应的所有红包活动
            QueryWrapper<ActivityTask> atWrapper = new QueryWrapper<>();
            atWrapper.lambda()
                    .eq(ActivityTask::getActivityDefId, activityDefId);
            List<ActivityTask> activityTaskList = activityTaskService.list(atWrapper);

            List<Long> activityTaskIdList = new ArrayList<>();
            for (ActivityTask activityTask : activityTaskList) {
                activityTaskIdList.add(activityTask.getId());
            }
            // 获取所有进行中的红包任务
            List<UserTask> userTaskList = userTaskService.listOngoingUserTask(activityTaskIdList);
            return userTaskList.size();
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.ORDER_BACK.getCode()))
                || activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.ORDER_BACK_V2.getCode()))) {
            // 免单商品逻辑
            // 先获取 此活动定义下的所有活动的id
            int hitsPerDraw = 1;
            List<Long> activityIdList = new ArrayList<>();
            for (Activity activity : activityList) {
                activityIdList.add(activity.getId());
                hitsPerDraw = activity.getHitsPerDraw();
            }
            if(CollectionUtils.isEmpty(activityIdList)){
                return 0;
            }
            // 在所有活动id下的 完成任务的总人数
            int finishedCount = userTaskService.queryTaskFinishedCount(activityIdList);
            return hitsPerDraw * activityList.size() - finishedCount;
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()))
                || activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()))) {
            return 0;
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.GOOD_COMMENT.getCode()))) {
            QueryWrapper<UserTask> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(UserTask::getActivityDefId, activityDefId)
                    .eq(UserTask::getType, UserTaskTypeEnum.GOOD_COMMENT.getCode())
                    .eq(UserTask::getStatus, UserTaskStatusEnum.AUDIT_WAIT.getCode())
            ;
            return this.userTaskService.count(queryWrapper);
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()))) {
            QueryWrapper<UserTask> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(UserTask::getActivityDefId, activityDefId)
                    .eq(UserTask::getType, UserTaskTypeEnum.GOOD_COMMENT_V2.getCode())
                    .eq(UserTask::getStatus, UserTaskStatusEnum.AUDIT_WAIT.getCode())
            ;
            return this.userTaskService.count(queryWrapper);
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.FAV_CART.getCode()))) {
            QueryWrapper<UserTask> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(UserTask::getActivityDefId, activityDefId)
                    .eq(UserTask::getType, UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode())
                    .eq(UserTask::getStatus, UserTaskStatusEnum.AUDIT_WAIT.getCode())
            ;
            return this.userTaskService.count(queryWrapper);
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.FAV_CART_V2.getCode()))) {
            QueryWrapper<UserTask> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(UserTask::getActivityDefId, activityDefId)
                    .eq(UserTask::getType, UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode())
                    .eq(UserTask::getStatus, UserTaskStatusEnum.AUDIT_WAIT.getCode())
            ;
            return this.userTaskService.count(queryWrapper);
        } else if (activityDefTypeVO.equals(String.valueOf(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()))) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityDefId);
            Integer costStock = 0;
            if (activityDef != null) {
                costStock = activityDef.getStockDef() - activityDef.getStock();
            }
            QueryWrapper<Activity> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(Activity::getActivityDefId, activityDefId)
                    .eq(Activity::getStatus, ActivityStatusEnum.END.getCode())
                    .isNotNull(Activity::getAwardStartTime)
            ;
            int awardSum = this.activityService.count(queryWrapper);
            if (costStock > awardSum) {
                return costStock - awardSum;
            }
        }
        return 0;
    }
}
