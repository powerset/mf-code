package mf.code.statistics.api;

import lombok.extern.slf4j.Slf4j;
import mf.code.statistics.api.service.SellerStatisticsService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.statistics
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-28 下午7:53
 */
@Slf4j
@RestController
@RequestMapping("/api/statistics/v2/seller")
public class SellerStatisticsApi {
    private final SellerStatisticsService sellerStatisticsService;

    @Autowired
    public SellerStatisticsApi(SellerStatisticsService sellerStatisticsService) {
        this.sellerStatisticsService = sellerStatisticsService;
    }

    @GetMapping("/queryActivityDefUserJoin")
    public SimpleResponse queryActivityDefUserJoin(@RequestParam(value = "current", defaultValue = "1") String current,
                                                   @RequestParam(value = "size", defaultValue = "10") String size,
                                                   @RequestParam("activityDefId") String activityDefId,
                                                   @RequestParam(value = "rpType", required = false) String rpType) {
        return sellerStatisticsService.queryActivityDefUserJoin(current, size, activityDefId, true, rpType);
    }
}
