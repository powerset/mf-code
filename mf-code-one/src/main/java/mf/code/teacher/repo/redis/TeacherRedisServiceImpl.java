package mf.code.teacher.repo.redis;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RobotUserUtil;
import mf.code.teacher.constant.TeacherInviteBizTypeEnum;
import mf.code.teacher.constant.TeacherInviteStatusEnum;
import mf.code.teacher.constant.TeacherStatusEnum;
import mf.code.teacher.repo.po.Teacher;
import mf.code.teacher.repo.po.TeacherInvite;
import mf.code.teacher.service.TeacherInviteService;
import mf.code.teacher.service.TeacherOrderService;
import mf.code.teacher.service.TeacherService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.teacher.repo.redis
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月01日 15:07
 */
@Service
public class TeacherRedisServiceImpl implements TeacherRedisService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private TeacherInviteService teacherInviteService;
    @Autowired
    private TeacherOrderService teacherOrderService;

    private final int max_sum = 40;

    /***
     * 获取首页的弹幕消息
     * @return
     */
    @Override
    public List<String> getHomePageBarrage() {
        /***
         * ①欢迎xxx入驻成为集客师、 type =1
         * ②恭喜xxx成功招募一位商家、type =2
         */
        //定义弹幕只要40条
        String redisKey = TeacherRedisKeyConstant.TEACHER_BARRAGE_HOMEPAGE;
        BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
        List<String> strs1 = activityPersons.range(0, -1);
        if (!CollectionUtils.isEmpty(strs1)) {
            return strs1;
        }
        //先查找数据库有木有值
        //成为讲师数目
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.lambda()
                .eq(Teacher::getStatus, TeacherStatusEnum.SUCCESS.getCode())
                .orderByDesc(Teacher::getId)
        ;
        int countTeacher = this.teacherService.count(teacherQueryWrapper);

        //招募商家数目
        QueryWrapper<TeacherInvite> teacherInviteQueryWrapper = new QueryWrapper<>();
        teacherInviteQueryWrapper.lambda()
                .eq(TeacherInvite::getBizType, TeacherInviteBizTypeEnum.RECRUIT_MERCHANT.getCode())
                .eq(TeacherInvite::getStatus, TeacherInviteStatusEnum.EFFECTIVE.getCode())
                .orderByDesc(TeacherInvite::getId)
        ;
        int countTeacherInvite = this.teacherInviteService.count(teacherInviteQueryWrapper);
        int sum = countTeacher + countTeacherInvite;
        int robotNum = 0;
        if (sum >= max_sum) {
            if (countTeacher >= max_sum && countTeacherInvite >= max_sum) {
                //若直接大于40，则取一半
                countTeacher = 20;
                countTeacherInvite = 20;
            } else {
                if (countTeacherInvite < 40) {
                    countTeacher = max_sum - countTeacherInvite;
                }
            }
        } else {
            //此时假数据数量，40-sum
            robotNum = max_sum - sum;
        }
        Page<Teacher> teacherPage = new Page<>(1, countTeacher);
        Page<TeacherInvite> teacherInvitePage = new Page<>(1, countTeacherInvite);

        //认证成为讲师
        IPage<Teacher> teacherIPage = teacherService.page(teacherPage, teacherQueryWrapper);
        List<Teacher> teachers = teacherIPage.getRecords();
        //招募商家讲师
        IPage<TeacherInvite> teacherInviteIPage = teacherInviteService.page(teacherInvitePage, teacherInviteQueryWrapper);
        List<TeacherInvite> teacherInvites = teacherInviteIPage.getRecords();

        List<Long> tids = new ArrayList<>();
        if (!CollectionUtils.isEmpty(teacherInvites)) {
            for (TeacherInvite teacherInvite : teacherInvites) {
                if (tids.indexOf(teacherInvite.getTid()) == -1) {
                    tids.add(teacherInvite.getTid());
                }
            }
        }
        List<Teacher> teacherRoleNames = null;
        if (!CollectionUtils.isEmpty(tids)) {
            teacherRoleNames = (List<Teacher>) this.teacherService.listByIds(tids);
        }
        Map<Long, Teacher> teacherRoleNameMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(teacherRoleNames)) {
            for (Teacher teacher : teacherRoleNames) {
                teacherRoleNameMap.put(teacher.getId(), teacher);
            }
        }
        List<Map<String, Object>> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(teachers)) {
            for (Teacher teacher : teachers) {
                Map<String, Object> map = getMapBarrage(teacher.getRoleName(), teacher.getAvatarUrl(), 1);
                list.add(map);
            }
        }
        if (!CollectionUtils.isEmpty(teacherInvites)) {
            for (TeacherInvite teacherInvite : teacherInvites) {
                Teacher teacher = teacherRoleNameMap.get(teacherInvite.getTid());
                if (teacher != null) {
                    Map<String, Object> map = getMapBarrage(teacher.getRoleName(), teacher.getAvatarUrl(), 2);
                    list.add(map);
                }
            }
        }

        //顺序打乱
        Collections.shuffle(list);

        List<Map<String, Object>> robotUsers = null;
        //假数据
        if (robotNum > 0) {
            robotUsers = RobotUserUtil.getRobotTeacher(robotNum);
        }

        List<Map<String, Object>> finalList = new ArrayList();
        if(!CollectionUtils.isEmpty(robotUsers)){
            finalList.addAll(robotUsers);
        }
        finalList.addAll(list);
        if (!CollectionUtils.isEmpty(finalList)) {
            for (Map<String, Object> map : finalList) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("roleName", map.get("roleName").toString());
                jsonObject.put("avatarUrl", map.get("avatarUrl").toString());
                jsonObject.put("type", map.get("type"));
                this.stringRedisTemplate.opsForList().leftPush(redisKey, jsonObject.toJSONString());
            }
            this.stringRedisTemplate.expire(redisKey, DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);
        }
        BoundListOperations homepageBarrageStrs = this.stringRedisTemplate.boundListOps(redisKey);
        return homepageBarrageStrs.range(0, -1);
    }

    /***
     * 存储弹幕
     * @param roleName
     * @param type 1:入驻 2：招募
     */
    @Override
    public void saveHomePageBarrage(String roleName, String avatarUrl, int type) {
        String redisKey = TeacherRedisKeyConstant.TEACHER_BARRAGE_HOMEPAGE;
        BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
        List<String> strs1 = activityPersons.range(0, -1);
        if (CollectionUtils.isEmpty(strs1)) {
            this.getHomePageBarrage();
        }
        BoundListOperations homepageBarrage = this.stringRedisTemplate.boundListOps(redisKey);
        List<String> homepageBarragestrs = homepageBarrage.range(0, -1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("roleName", roleName);
        jsonObject.put("avatarUrl", avatarUrl);
        jsonObject.put("type", type);
        //将最右边的一个删除
        int index = homepageBarragestrs.size() - 1;
        String value = this.stringRedisTemplate.opsForList().index(redisKey, index);
        this.stringRedisTemplate.opsForList().remove(redisKey, index, value);
        this.stringRedisTemplate.opsForList().leftPush(redisKey, jsonObject.toJSONString());
    }

    /***
     * 存储今日收益信息
     * @param tid
     * @param addProfit
     * @param merchantNum
     */
    @Override
    public void saveTodayInfo(Long tid, BigDecimal addProfit, int merchantNum) {
        Teacher teacher = this.teacherService.getById(tid);
        if (teacher == null) {
            return;
        }
        String redisKey = RedisKeyConstant.TEACHER_TODY_INFO + tid;
        String todayInfo = this.stringRedisTemplate.opsForValue().get(redisKey);
        JSONObject valObj = JSON.parseObject(todayInfo);
        if (CollectionUtils.isEmpty(valObj)) {
            valObj = new JSONObject();
            valObj.put("todayIncome", new BigDecimal("0.00"));
            valObj.put("todayMerchants", 0);
        }
        if (addProfit != null) {
            valObj.put("todayIncome", new BigDecimal(valObj.getString("todayIncome")).add(addProfit).setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        valObj.put("todayMerchants", valObj.getInteger("todayMerchants") + merchantNum);
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(valObj), DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);
    }

    /***
     * 存储平台发出的总奖金
     * @param bonus 增加的奖金
     */
    @Override
    public void saveSendTotalBonus(BigDecimal bonus) {
        if (bonus == null) {
            return;
        }
        String redisKey = RedisKeyConstant.TEACHER_PLATFORM_ACCUMULATED_BONUS;
        String s = this.stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isBlank(s)) {
            BigDecimal sum = this.teacherOrderService.sumAllBonus();
            BigDecimal addMount = bonus;
            if (sum.compareTo(BigDecimal.ZERO) > 0) {
                addMount = bonus.add(sum);
            }
            this.stringRedisTemplate.opsForValue().increment(redisKey, addMount.doubleValue());
        } else {
            this.stringRedisTemplate.opsForValue().increment(redisKey, bonus.doubleValue());
        }
    }

    /***
     * 排行榜更新
     * @param tid
     * @param addProfit
     */
    @Override
    public void saveRankingList(String tid, BigDecimal addProfit) {
        String redisKey = RedisKeyConstant.TEACHER_ACCUMULATED_INCOME_LIST;
        Set<String> zset = stringRedisTemplate.opsForZSet().range(redisKey, 0L, -1L);
        boolean boo = zset.contains(tid);
        if (!boo) {
            stringRedisTemplate.opsForZSet().add(redisKey, tid, addProfit.doubleValue());
        } else {
            stringRedisTemplate.opsForZSet().incrementScore(redisKey, tid, addProfit.doubleValue());
        }
    }

    /**
     * 集客师成功招募将领 增加收益弹窗 40%
     * @param tid
     * @param bonus
     */
    @Override
    public void saveIncomeDialog40(Long tid, BigDecimal bonus) {
        if (tid == null || bonus == null) {
            return ;
        }
        String redisKey = RedisKeyConstant.TEACHER_INCOME_DIALOG40 + tid;
        stringRedisTemplate.opsForList().rightPush(redisKey, bonus.toString());
    }

    /**
     * 推荐的集客师 成功招募将领 增加收益弹窗 5%
     * @param tid
     * @param bonus
     */
    @Override
    public void saveIncomeDialog5(Long tid, BigDecimal bonus) {
        if (tid == null || bonus == null) {
            return ;
        }
        String redisKey = RedisKeyConstant.TEACHER_INCOME_DIALOG5 + tid;
        stringRedisTemplate.opsForList().rightPush(redisKey, bonus.toString());
    }

    private Map<String, Object> getMapBarrage(String roleName, String avatarUrl, int type) {
        Map<String, Object> map = new HashMap<>();
        map.put("roleName", roleName);
        map.put("avatarUrl", avatarUrl);
        map.put("type", type);
        return map;
    }
}
