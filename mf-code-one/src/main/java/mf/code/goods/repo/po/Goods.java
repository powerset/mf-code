package mf.code.goods.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * goods
 * 商品
 */
public class Goods implements Serializable {
    /**
     * 商品id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 商铺id
     */
    private Long shopId;

    /**
     * 来源类型 1淘宝 2京东
     */
    private Integer srcType;

    /**
     * 类目ID，品类表的sid
     */
    private Long categoryId;

    /**
     * 展示商品名（供页面显示）
     */
    private String displayGoodsName;

    /**
     * 展示商品价格
     */
    private BigDecimal displayPrice;

    /**
     * 展示商品banner图
     */
    private String displayBanner;

    /**
     * 商品url
     */
    private String xxbtopReq;

    /**
     * 喜销宝服务接口响应数据
     */
    private String xxbtopRes;

    /**
     * 商品描述, 字数要大于5个字节，小于25000个字节
     */
    private String xxbtopDesc;

    /**
     * 小时光OSS商铺详情地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    private String descPath;

    /**
     * 喜销宝返回商品数字id
     */
    private String xxbtopNumIid;

    /**
     * 商品主图片地址
     */
    private String xxbtopPicUrl;

    /**
     * 小时光OSS 商品主图片地址
     */
    private String picUrl;

    /**
     * 商品价格，格式：5.00；单位：元；精确到：分
     */
    private String xxbtopPrice;

    /**
     * 商品标题,不能超过60字节
     */
    private String xxbtopTitle;

    /**
     * product主键，2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     */
    private Long productId;

    /**
     * product_sku表主键 2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     */
    private Long skuId;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * goods_parent表id
     */
    private Long parentId;

    /**
     * 置顶（0:普通，非0置顶）
     */
    private Integer top;

    /**
     * 置顶时间
     */
    private Date topTime;

    /**
     * sku属性描述
     */
    private String propertiesName;

    /**
     * 是否上架（0:下架，1:上架）
     */
    private Integer onsale;

    /**
     * goods
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     * @return id 商品id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商品id
     * @param id 商品id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 商铺id
     * @return shop_id 商铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 商铺id
     * @param shopId 商铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 来源类型 1淘宝 2京东
     * @return src_type 来源类型 1淘宝 2京东
     */
    public Integer getSrcType() {
        return srcType;
    }

    /**
     * 来源类型 1淘宝 2京东
     * @param srcType 来源类型 1淘宝 2京东
     */
    public void setSrcType(Integer srcType) {
        this.srcType = srcType;
    }

    /**
     * 类目ID，品类表的sid
     * @return category_id 类目ID，品类表的sid
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * 类目ID，品类表的sid
     * @param categoryId 类目ID，品类表的sid
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 展示商品名（供页面显示）
     * @return display_goods_name 展示商品名（供页面显示）
     */
    public String getDisplayGoodsName() {
        return displayGoodsName;
    }

    /**
     * 展示商品名（供页面显示）
     * @param displayGoodsName 展示商品名（供页面显示）
     */
    public void setDisplayGoodsName(String displayGoodsName) {
        this.displayGoodsName = displayGoodsName == null ? null : displayGoodsName.trim();
    }

    /**
     * 展示商品价格
     * @return display_price 展示商品价格
     */
    public BigDecimal getDisplayPrice() {
        return displayPrice;
    }

    /**
     * 展示商品价格
     * @param displayPrice 展示商品价格
     */
    public void setDisplayPrice(BigDecimal displayPrice) {
        this.displayPrice = displayPrice;
    }

    /**
     * 展示商品banner图
     * @return display_banner 展示商品banner图
     */
    public String getDisplayBanner() {
        return displayBanner;
    }

    /**
     * 展示商品banner图
     * @param displayBanner 展示商品banner图
     */
    public void setDisplayBanner(String displayBanner) {
        this.displayBanner = displayBanner == null ? null : displayBanner.trim();
    }

    /**
     * 商品url
     * @return xxbtop_req 商品url
     */
    public String getXxbtopReq() {
        return xxbtopReq;
    }

    /**
     * 商品url
     * @param xxbtopReq 商品url
     */
    public void setXxbtopReq(String xxbtopReq) {
        this.xxbtopReq = xxbtopReq == null ? null : xxbtopReq.trim();
    }

    /**
     * 喜销宝服务接口响应数据
     * @return xxbtop_res 喜销宝服务接口响应数据
     */
    public String getXxbtopRes() {
        return xxbtopRes;
    }

    /**
     * 喜销宝服务接口响应数据
     * @param xxbtopRes 喜销宝服务接口响应数据
     */
    public void setXxbtopRes(String xxbtopRes) {
        this.xxbtopRes = xxbtopRes == null ? null : xxbtopRes.trim();
    }

    /**
     * 商品描述, 字数要大于5个字节，小于25000个字节
     * @return xxbtop_desc 商品描述, 字数要大于5个字节，小于25000个字节
     */
    public String getXxbtopDesc() {
        return xxbtopDesc;
    }

    /**
     * 商品描述, 字数要大于5个字节，小于25000个字节
     * @param xxbtopDesc 商品描述, 字数要大于5个字节，小于25000个字节
     */
    public void setXxbtopDesc(String xxbtopDesc) {
        this.xxbtopDesc = xxbtopDesc == null ? null : xxbtopDesc.trim();
    }

    /**
     * 小时光OSS商铺详情地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     * @return desc_path 小时光OSS商铺详情地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    public String getDescPath() {
        return descPath;
    }

    /**
     * 小时光OSS商铺详情地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     * @param descPath 小时光OSS商铺详情地址。返回相对路径，可以用"TODO"来拼接成绝对路径
     */
    public void setDescPath(String descPath) {
        this.descPath = descPath == null ? null : descPath.trim();
    }

    /**
     * 喜销宝返回商品数字id
     * @return xxbtop_num_iid 喜销宝返回商品数字id
     */
    public String getXxbtopNumIid() {
        return xxbtopNumIid;
    }

    /**
     * 喜销宝返回商品数字id
     * @param xxbtopNumIid 喜销宝返回商品数字id
     */
    public void setXxbtopNumIid(String xxbtopNumIid) {
        this.xxbtopNumIid = xxbtopNumIid == null ? null : xxbtopNumIid.trim();
    }

    /**
     * 商品主图片地址
     * @return xxbtop_pic_url 商品主图片地址
     */
    public String getXxbtopPicUrl() {
        return xxbtopPicUrl;
    }

    /**
     * 商品主图片地址
     * @param xxbtopPicUrl 商品主图片地址
     */
    public void setXxbtopPicUrl(String xxbtopPicUrl) {
        this.xxbtopPicUrl = xxbtopPicUrl == null ? null : xxbtopPicUrl.trim();
    }

    /**
     * 小时光OSS 商品主图片地址
     * @return pic_url 小时光OSS 商品主图片地址
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 小时光OSS 商品主图片地址
     * @param picUrl 小时光OSS 商品主图片地址
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl == null ? null : picUrl.trim();
    }

    /**
     * 商品价格，格式：5.00；单位：元；精确到：分
     * @return xxbtop_price 商品价格，格式：5.00；单位：元；精确到：分
     */
    public String getXxbtopPrice() {
        return xxbtopPrice;
    }

    /**
     * 商品价格，格式：5.00；单位：元；精确到：分
     * @param xxbtopPrice 商品价格，格式：5.00；单位：元；精确到：分
     */
    public void setXxbtopPrice(String xxbtopPrice) {
        this.xxbtopPrice = xxbtopPrice == null ? null : xxbtopPrice.trim();
    }

    /**
     * 商品标题,不能超过60字节
     * @return xxbtop_title 商品标题,不能超过60字节
     */
    public String getXxbtopTitle() {
        return xxbtopTitle;
    }

    /**
     * 商品标题,不能超过60字节
     * @param xxbtopTitle 商品标题,不能超过60字节
     */
    public void setXxbtopTitle(String xxbtopTitle) {
        this.xxbtopTitle = xxbtopTitle == null ? null : xxbtopTitle.trim();
    }

    /**
     * product主键，2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     * @return product_id product主键，2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * product主键，2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     * @param productId product主键，2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * product_sku表主键 2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     * @return sku_id product_sku表主键 2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     */
    public Long getSkuId() {
        return skuId;
    }

    /**
     * product_sku表主键 2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     * @param skuId product_sku表主键 2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余
     */
    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @return del 删除标识 0:未删除 1:已删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @param del 删除标识 0:未删除 1:已删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * goods_parent表id
     * @return parent_id goods_parent表id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * goods_parent表id
     * @param parentId goods_parent表id
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 置顶（0:普通，非0置顶）
     * @return top 置顶（0:普通，非0置顶）
     */
    public Integer getTop() {
        return top;
    }

    /**
     * 置顶（0:普通，非0置顶）
     * @param top 置顶（0:普通，非0置顶）
     */
    public void setTop(Integer top) {
        this.top = top;
    }

    /**
     * 置顶时间
     * @return top_time 置顶时间
     */
    public Date getTopTime() {
        return topTime;
    }

    /**
     * 置顶时间
     * @param topTime 置顶时间
     */
    public void setTopTime(Date topTime) {
        this.topTime = topTime;
    }

    /**
     * sku属性描述
     * @return properties_name sku属性描述
     */
    public String getPropertiesName() {
        return propertiesName;
    }

    /**
     * sku属性描述
     * @param propertiesName sku属性描述
     */
    public void setPropertiesName(String propertiesName) {
        this.propertiesName = propertiesName == null ? null : propertiesName.trim();
    }

    /**
     * 是否上架（0:下架，1:上架）
     * @return onsale 是否上架（0:下架，1:上架）
     */
    public Integer getOnsale() {
        return onsale;
    }

    /**
     * 是否上架（0:下架，1:上架）
     * @param onsale 是否上架（0:下架，1:上架）
     */
    public void setOnsale(Integer onsale) {
        this.onsale = onsale;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", srcType=").append(srcType);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", displayGoodsName=").append(displayGoodsName);
        sb.append(", displayPrice=").append(displayPrice);
        sb.append(", displayBanner=").append(displayBanner);
        sb.append(", xxbtopReq=").append(xxbtopReq);
        sb.append(", xxbtopRes=").append(xxbtopRes);
        sb.append(", xxbtopDesc=").append(xxbtopDesc);
        sb.append(", descPath=").append(descPath);
        sb.append(", xxbtopNumIid=").append(xxbtopNumIid);
        sb.append(", xxbtopPicUrl=").append(xxbtopPicUrl);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", xxbtopPrice=").append(xxbtopPrice);
        sb.append(", xxbtopTitle=").append(xxbtopTitle);
        sb.append(", productId=").append(productId);
        sb.append(", skuId=").append(skuId);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", parentId=").append(parentId);
        sb.append(", top=").append(top);
        sb.append(", topTime=").append(topTime);
        sb.append(", propertiesName=").append(propertiesName);
        sb.append(", onsale=").append(onsale);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}