package mf.code.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.goods.repo.po.GoodsParent;

import java.util.List;

/**
 * mf.code.goods.service
 * Description: 商品sku服务
 *
 * @author: gel
 * @date: 2018-12-19 16:20
 */
public interface GoodsParentService extends IService<GoodsParent> {

    /**
     * 创建
     * @param goodsSku
     * @return
     */
    int create(GoodsParent goodsSku);

    /**
     * 修改
     * @param goodsSku
     * @return
     */
    int update(GoodsParent goodsSku);

    /**
     * 删除
     * @param skuId
     * @return
     */
    int delete(Long skuId);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    GoodsParent selectById(Long id);

    /**
     * 根据numiid
     * @param shopId
     * @param numIid
     * @return
     */
    GoodsParent selectByNumiid(String shopId, String numIid);

    /**
     * 返回父类列表
     * @param goodsParentIdList
     * @return
     */
    List<GoodsParent> selectByIdList(List<Long> goodsParentIdList);
}
