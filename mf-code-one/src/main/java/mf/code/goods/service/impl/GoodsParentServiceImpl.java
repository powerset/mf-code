package mf.code.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.common.constant.DelEnum;
import mf.code.goods.repo.dao.GoodsParentMapper;
import mf.code.goods.repo.po.GoodsParent;
import mf.code.goods.service.GoodsParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2018-12-19 16:20
 */
@Service
public class GoodsParentServiceImpl extends ServiceImpl<GoodsParentMapper, GoodsParent> implements GoodsParentService {

    @Autowired
    private GoodsParentMapper goodsParentMapper;

    /**
     * 创建
     *
     * @param goodsSku
     * @return
     */
    @Override
    public int create(GoodsParent goodsSku) {
        goodsSku.setCtime(new Date());
        goodsSku.setUtime(new Date());
        return goodsParentMapper.insertSelective(goodsSku);
    }

    /**
     * 修改
     *
     * @param goodsSku
     * @return
     */
    @Override
    public int update(GoodsParent goodsSku) {
        return goodsParentMapper.updateById(goodsSku);
    }

    /**
     * 删除
     *
     * @param skuId
     * @return
     */
    @Override
    public int delete(Long skuId) {
        GoodsParent goodsSku = new GoodsParent();
        goodsSku.setId(skuId);
        goodsSku.setDel(DelEnum.NO.getCode());
        goodsSku.setUtime(new Date());
        return goodsParentMapper.updateById(goodsSku);
    }

    /**
     * 根据主键查询
     *
     * @param skuId
     * @return
     */
    @Override
    public GoodsParent selectById(Long skuId) {
        return goodsParentMapper.selectById(skuId);
    }

    /**
     * 根据numiid淘宝商品id
     *
     * @param shopId
     * @param numIid
     * @return
     */
    @Override
    public GoodsParent selectByNumiid(String shopId, String numIid) {
        Map<String, Object> params = new HashMap<>();
        params.put("shop_id", shopId);
        params.put("xxbtop_num_iid", numIid);
        params.put("del", DelEnum.NO.getCode());
        List<GoodsParent> goodsParentList = goodsParentMapper.selectByMap(params);
        if(CollectionUtils.isEmpty(goodsParentList)){
            return null;
        }
        return goodsParentList.get(0);
    }

    /**
     * 返回父类列表
     *
     * @param goodsParentIdList
     * @return
     */
    @Override
    public List<GoodsParent> selectByIdList(List<Long> goodsParentIdList) {
        return goodsParentMapper.selectBatchIds(goodsParentIdList);
    }

}
