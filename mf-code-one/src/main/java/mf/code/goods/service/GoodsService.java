package mf.code.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.dto.DepositBO;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.repo.po.Goods;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.goods.service
 * Description: 商品接口
 *
 * @author: gel
 * @date: 2018-10-29 11:00
 */
public interface GoodsService extends IService<Goods> {

    /**
     * 创建商品
     * @param goods
     * @return
     */
    Integer createGoods(Goods goods);

    /**
     * 修改商品
     * @param goods
     * @return
     */
    Integer updateGoods(Goods goods);

    /**
     * 批量修改商品
     * @param goodsIdList
     * @return
     */
    Integer updateGoodsListByParentId(Long goodsIdList, Goods goods);

    /**
     * 删除商品
     * @param id
     * @return
     */
    Integer deleteGoods(Long id);

    /**
     * 根据主键查询商品
     * @param id
     * @return
     */
    Goods selectById(Long id);

    /**
     * 分页查询商品信息
     * @param mapParams
     * @param page
     * @param size
     * @return
     */
    Map<String, Object> pageListGoods(Map<String, Object> mapParams, Integer page, Integer size);

    /**
     * count
     * @param mapParams
     * @return
     */
    Integer countForPageList(Map<String, Object> mapParams);

    /**
     * 按照店铺查询商品信息
     * @param shopId
     * @return
     */
    List<Goods> selectGoodsByShop(Long shopId);

    List<Goods> pageList(Map<String, Object> mapParams);

    /**
     * 按照主键批量查询
     * @param goodsIds
     * @return
     */
    List<Goods> selectByIdList(List<Long> goodsIds);

    /***
     * 根据主键批量查询返回Map
     * @param goodsIDs
     * @return
     */
    Map<Long, Goods> queryGoodsInfo(List<Long> goodsIDs);

    /**
     * 定时任务分页查询
     *
     * @param params
     * @param page
     * @param size
     * @return
     */
    List<Goods> pageListGoodsForJob(Map<String, Object> params, Integer page, Integer size);

    List<Goods> selectByParentId(Long parentId);

    /**
     * 首页列表
     * @param shopId
     * @return
     */
    List<Map> getHomePageDataV2(Long merchantId, Long shopId, Long user);


    SimpleResponse<Object> getDepost4Gift(Long goodsId, int stockDef);

    SimpleResponse<Object> getDepost4GiftAddCommission(Long goodsId, int stockDef, BigDecimal commission);

    SimpleResponse<Object> getDeposit4LuckyWheel(int stockDef);

    SimpleResponse<Object> getDeposit4RedPacket(List<Long> goodsIdList, int stockDef, BigDecimal totalAmount);

    Map statisticsGoods(Map m);

    SimpleResponse<Object> selectError(Long goodsId);

    Map<String, Object> getGoodsVOListByIds(List<Long> goodsIdList);

    SimpleResponse<Object> getDeposit(ActivityDefV2Req activityDefV2Req);

    SimpleResponse<Object> getDeposit(DepositBO depositBO);

    Map getTpwd(Long goodsId);

    /**
     * V2版活动根据product创建goods
     *
     * @param req
     * @param productEntity
     * @return
     */
    Goods createOrUpdateGoodsByActivityDef(CreateDefReq req, ProductEntity productEntity);
}
