package mf.code.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityAboutService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.HomePageService;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.dto.DepositBO;
import mf.code.api.seller.utils.SellerValidator;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.PageUtil;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.dto.ProductSkuEntity;
import mf.code.goods.repo.dao.GoodsMapper;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.repo.po.GoodsPlus;
import mf.code.goods.service.GoodsPlusService;
import mf.code.goods.service.GoodsService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.goods.service.impl
 * Description: 商品基本接口
 *
 * @author: gel
 * @date: 2018-10-29 11:01
 */
@Slf4j
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private HomePageService homePageService;
    @Autowired
    private GoodsPlusService goodsPlusService;

    @Override
    public Integer createGoods(Goods goods) {
        return goodsMapper.insertSelective(goods);
    }

    @Override
    public Integer updateGoods(Goods goods) {
        return goodsMapper.updateByPrimaryKeySelective(goods);
    }

    /**
     * 批量修改商品
     *
     * @param parentId
     * @param goods
     * @return
     */
    @Override
    public Integer updateGoodsListByParentId(Long parentId, Goods goods) {
        return goodsMapper.updateListByParentIdSelective(parentId, goods);
    }

    /**
     * 删除商品
     *
     * @param id
     * @return
     */
    @Override
    public Integer deleteGoods(Long id) {
        Goods goods = new Goods();
        goods.setId(id);
        goods.setDel(DelEnum.YES.getCode());
        goods.setUtime(new Date());
        return goodsMapper.updateByPrimaryKeySelective(goods);
    }

    @Override
    public Goods selectById(Long id) {
        return goodsMapper.selectByPrimaryKey(id);
    }

    @Override
    public Map<String, Object> pageListGoods(Map<String, Object> mapParams, Integer page, Integer size) {
        int count = goodsMapper.countForPageList(mapParams);
        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("page", page);
        result.put("size", size);
        if (count == 0) {
            result.put("list", new ArrayList<>());
            return result;
        }
        mapParams.putAll(PageUtil.pageSizeTolimit(page, size, count));
        List<Goods> goodsList = goodsMapper.pageListGoods(mapParams);
        List<Map<String, Object>> goodsMapList = new ArrayList<>();
        for (int i = 0; i < goodsList.size(); i++) {
            Goods goods = goodsList.get(i);
            Map<String, Object> stringObjectMap = BeanMapUtil.beanToMapIgnore(goods, "ctime", "utime", "del",
                    "xxbtopRes", "xxbtopDesc", "xxbtopNumIid");
            goodsMapList.add(stringObjectMap);
        }
        result.put("list", goodsMapList);
        return result;
    }

    /**
     * count
     *
     * @param mapParams
     * @return
     */
    @Override
    public Integer countForPageList(Map<String, Object> mapParams) {
        return goodsMapper.countForPageList(mapParams);
    }

    /**
     * 按照店铺查询商品信息
     *
     * @param shopId
     * @return
     */
    @Override
    public List<Goods> selectGoodsByShop(Long shopId) {
        Map<String, Object> params = new HashMap<>(1);
        params.put("shopId", shopId);
        params.put("del", DelEnum.NO.getCode());
        return goodsMapper.pageListGoods(params);
    }

    @Override
    public List<Goods> pageList(Map<String, Object> mapParams) {
        return goodsMapper.pageListGoods(mapParams);
    }

    /**
     * 按照主键批量查询
     *
     * @param goodsIds
     * @return
     */
    @Override
    public List<Goods> selectByIdList(List<Long> goodsIds) {
        Map<String, Object> params = new HashMap<>();
        params.put("goodsIds", goodsIds);
        return goodsMapper.pageListGoods(params);
    }

    /***
     * 根据主键批量查询返回Map
     * @param goodsIDs
     * @return
     */
    @Override
    public Map<Long, Goods> queryGoodsInfo(List<Long> goodsIDs) {
        Map<Long, Goods> goodsMap = new HashMap<>();
        if (CollectionUtils.isEmpty(goodsIDs)) {
            return new HashMap<>();
        }

        Map<String, Object> goodsParams = new HashMap<>();
        goodsParams.put("goodsIds", goodsIDs);
        List<Goods> goodsList = this.pageList(goodsParams);
        if (goodsList != null && goodsList.size() > 0) {
            for (Goods goods : goodsList) {
                goodsMap.put(goods.getId(), goods);
            }
        }
        return goodsMap;
    }


    /**
     * 定时任务分页查询
     *
     * @param params
     * @param page
     * @param size
     * @return
     */
    @Override
    public List<Goods> pageListGoodsForJob(Map<String, Object> params, Integer page, Integer size) {
        int count = goodsMapper.countForPageList(params);
        if (count == 0) {
            return null;
        }
        params.putAll(PageUtil.pageSizeTolimit(page, size, count));
        return goodsMapper.pageListGoods(params);
    }

    /**
     * 定时任务分页查询
     *
     * @param parentId
     * @return
     */
    @Override
    public List<Goods> selectByParentId(Long parentId) {
        Map<String, Object> params = new HashMap<>();
        params.put("parentId", parentId);
        params.put("del", DelEnum.NO.getCode());
        return goodsMapper.pageListGoods(params);
    }

    /**
     * 去重
     *
     * @param list
     * @return
     */
    private List distinct(List<Map> list) {
        List<Map> listShow = new ArrayList<>();
        List<Map> listDrop = new ArrayList<>();

        for (Map m : list) {
            Object stock = m.get("stock");
            // 没库存
            if (stock == null || "0".equals(stock.toString())) {
                Object defType = m.get("activityDefType");
                Object hasJoin = m.get("hasJoin") == null ? "" : m.get("hasJoin").toString();
                String canJoin = m.get("canJoin") == null ? "" : m.get("canJoin").toString();

                if (defType == null) {
                    //没 defType 剔除
                    listDrop.add(m);

                } else {
                    if (hasJoin.equals("true")) {
                        listShow.add(m);

                    } else if (canJoin.equals("true")) {
                        listShow.add(m);

                    } else {
                        //有defType 但 为不可参与状态 剔除
                        listDrop.add(m);
                    }
                }
            } else {
                //有库存 , 保留
                listShow.add(m);
            }
        }
        Set<String> goodsIdSet = new HashSet<>();
        for (int j = 0; j < listDrop.size(); j++) {
            Map m = listDrop.get(j);
            for (Map showMap : listShow) {
                // 如果listDrop中的对象  aaa 跟showList中的重复 , 就在list中删除 listDrop的 aaa
                if (m.get("goodsId").toString().equals(showMap.get("goodsId").toString())) {
                    list.remove(m);
                }
            }
            // 如果 listDrop 中有重复 对象 ,但是 showList中没有, 就用下方法剔除
            int size = goodsIdSet.size();
            goodsIdSet.add(m.get("goodsId").toString());
            if (size == goodsIdSet.size()) {
                list.remove(m);
            }
        }
        return list;
    }

    /**
     * 获取数据
     *
     * @param shopId
     * @return
     */
    private List<Map> getDataFromRedisOrDB(Long shopId) {
        String key = "mf:homePage:v2:findGoods:shopId:" + shopId;
        String val = stringRedisTemplate.opsForValue().get(key);

        List<Map> list = new ArrayList<>();
        if (val == null) {
            list = goodsMapper.getHomePageDataV2(shopId);

            stringRedisTemplate.opsForValue().set(key, JSON.toJSONString(list));
            stringRedisTemplate.expire(key, 10, TimeUnit.SECONDS);
            return list;
        }
        return JSON.parseArray(val, Map.class);
    }

    /**
     * 首页列表
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public List<Map> getHomePageDataV2(Long merchantId, Long shopId, Long userId) {
        // 如果redis中没用, 才查库. redis有效时间1分钟
        List<Map> list = this.getDataFromRedisOrDB(shopId);
        /**
         * 添加附加数据:
         */
        for (Map m : list) {
            Long goodsId = Long.parseLong(m.get("goodsId").toString());
            String statusDB = m.get("status") == null ? "" : m.get("status").toString();
            //活动定义下无活动,就将库存设为0 , 前端需要用库存做逻辑处理
            if (m.get("activityDefId") == null) {
                m.put("stock", 0);
                continue;
            }

            if (!statusDB.equals("3")) { // 3:已发布状态
                m.put("stock", 0);
            } else {
                // 参与人数 假数据
                String fakeIncr = "home:fake:joinManItem:shopId:" + shopId + ":goodsId:" + goodsId;
                Long increment = stringRedisTemplate.opsForValue().increment(fakeIncr, 1L);
                m.put("joinManTotal", increment + homePageService.buildOrGetData4Item(new Random().nextInt(5) + 1, merchantId, shopId, goodsId));
                String defType = m.get("activityDefType").toString();

                //抽奖  type=2: 抽奖活动
                if ("2".equals(defType)) {
                    Integer stock = m.get("stock") == null ? 0 : Integer.parseInt(m.get("stock").toString());
                    if (stock == 0) {
                        ActivityDef activityDef = activityDefService.findByGoodsId(goodsId);
                        if (activityDef != null) {
                            List<Activity> activityListByDefId = activityService.findByDefId4HomePage(activityDef.getId());
                            if (activityListByDefId != null && activityListByDefId.size() > 0) {
                                m.put("canJoin", true);
                            } else {
                                m.put("canJoin", false);
                                m.remove("activityDefType");
                            }
                        }
                    }
                } else if ("6".equals(defType) || "7".equals(defType) || "3".equals(defType)) {
                    this.setButtonStat2(m, goodsId, userId);
                }
            }
        }
        //去重
        list = this.distinct(list);
        // 前端去除defType
        list = this.rmType(list);
        return list;
    }

    private List rmType(List<Map> list) {
        for (Map m : list) {
            Object defType = m.get("activityDefType");
            if (defType != null) {
                String defTypeStr = defType.toString();
                if ("6".equals(defTypeStr) || "7".equals(defTypeStr) || "3".equals(defTypeStr)) {
                    String hasJoin = m.get("hasJoin") == null ? "" : m.get("hasJoin").toString();
                    Integer stock = m.get("stock") == null ? 0 : Integer.parseInt(m.get("stock").toString());
                    if (stock == 0 && !"true".equals(hasJoin)) {
                        m.remove("activityDefType");
                    }
                }
            }
        }

        return list;
    }


    @Autowired
    private ActivityAboutService activityAboutService;


    private Map setButtonStat2(Map m, Long goodsId, Long userId) {
        String defId = m.get("activityDefId").toString();
        Activity activity = activityService.findByDefIdUserId(defId, userId);

        // 去领取中
        if (activity == null) {
            m.remove("activityId");
            m.put("hasJoin", false);
            m.put("btnStatus", HomepageGiftBtnStatusEnum.GET.getCode());
            return m;
        }

        m.put("activityId", activity.getId());
        m.put("hasJoin", true);

        // 平团中
        if (activity.getAwardStartTime() == null && activity.getStatus() == 1) {
            m.put("btnStatus", HomepageGiftBtnStatusEnum.ACTIVITY_ING.getCode());
            return m;
        }
        UserTask userTask = userTaskService.findByUserIdActivityId(activity.getId(), userId);

        if (userTask != null) {
            // 领取中
            if (userTask.getStatus() != 3 && userTask.getStatus() != -1 && userTask.getStatus() != -2) {
                m.put("btnStatus", HomepageGiftBtnStatusEnum.MISSION_ING.getCode());
                return m;
            }

            //已领取
            if (userTask.getStatus() == 3) {
                m.put("btnStatus", HomepageGiftBtnStatusEnum.OVER.getCode());
                return m;
            }

            // 去领取(-2是一个中间状态,最终变为-1)
            if (userTask.getStatus() == -2) {
                m.put("btnStatus", HomepageGiftBtnStatusEnum.MISSION_ING.getCode());
                return m;
            }
        }

        m.remove("activityId");
        m.put("hasJoin", false);
        m.put("btnStatus", HomepageGiftBtnStatusEnum.GET.getCode());
        return m;
    }

    /**
     * 设置按钮状态----废弃
     * 用此:private Map setButtonStat2(Map m, Long goodsId, Long userId);
     *
     * @param m
     * @param goodsId
     * @return
     */
    private Map setButtonStat(Map m, Long goodsId, Long userId) {
        // 附加: 2:  参与状态 和 是否参与该活动
        UserActivity userActivity = userActivityService.findByGoodsId(goodsId);

        long activityId = Long.parseLong(m.get("activityId").toString());
        m.put("activityId", activityId);
        Activity activityTemp = activityService.findById(activityId);
        Activity mySelfActivity = activityAboutService.getMySelfActivity(activityTemp, userId);

        if (userActivity == null) {
            m.put("hasJoin", false);
            //按钮状态:领取赠品
            m.put("btnStatus", HomepageGiftBtnStatusEnum.GET.getCode());
        } else {

            m.put("hasJoin", true);
            Integer userActivityStatus = mySelfActivity.getStatus();
            if (userActivityStatus == UserActivityStatusEnum.NO_WIN.getCode()) {
                // 如果数据库中状态是 1:有资格,但没中获  ,就设置为进行中(拼团中)
                //按钮状态: 拼团中
                m.put("btnStatus", HomepageGiftBtnStatusEnum.ACTIVITY_ING.getCode());

                // 如果中奖和优惠券 数据库中status=2,3
            } else if (userActivityStatus == UserActivityStatusEnum.WINNER.getCode() || userActivityStatus == UserActivityStatusEnum.COUPON.getCode()) {
                UserTask userTask = userTaskService.findByGoodsId(goodsId);

                if (userTask == null) {
                    //中奖了,但是没做任务
                    //按钮状态: 领取中
                    m.put("btnStatus", HomepageGiftBtnStatusEnum.MISSION_ING.getCode());
                } else {
                    // 如果user_task 有记录.
                    Integer userTaskStatus = userTask.getStatus();

                    if (userTaskStatus == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                        //已领取 status=3
                        m.put("btnStatus", HomepageGiftBtnStatusEnum.OVER.getCode());

                    } else if (userTaskStatus == UserTaskStatusEnum.INIT.getCode() || userTaskStatus == UserTaskStatusEnum.AUDIT_WAIT.getCode()
                            || userTaskStatus == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                        //领取中 status in (1,2)
                        m.put("btnStatus", HomepageGiftBtnStatusEnum.MISSION_ING.getCode());
                    } else {
                        m.put("hasJoin", false);
                        m.put("btnStatus", HomepageGiftBtnStatusEnum.GET.getCode());
                    }
                }
            }

        }
        return m;
    }

    @Override
    public SimpleResponse<Object> getDepost4Gift(Long goodsId, int stockDef) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Goods goods = this.getById(goodsId);
        if (null == goods) {
            return this.selectError(goodsId);
        }
        BigDecimal goodsPrice = goods.getDisplayPrice();
        BigDecimal depositDef = goodsPrice.multiply(new BigDecimal(stockDef));
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("depositDef", depositDef);
        resultVO.put("goodsPrice", goodsPrice);
        response.setData(resultVO);
        return response;
    }

    /**
     * 添加佣金后的计算方式
     *
     * @param goodsId
     * @param stockDef
     * @param commission
     * @return
     */
    @Override
    public SimpleResponse<Object> getDepost4GiftAddCommission(Long goodsId, int stockDef, BigDecimal commission) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Goods goods = this.getById(goodsId);
        if (null == goods) {
            return this.selectError(goodsId);
        }
        BigDecimal goodsPrice = goods.getDisplayPrice();
        BigDecimal depositDef = goodsPrice.multiply(new BigDecimal(stockDef));
        // 添加额外佣金
        depositDef = depositDef.add(commission.multiply(new BigDecimal(stockDef)));
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("depositDef", depositDef);
        resultVO.put("goodsPrice", goodsPrice);
        resultVO.put("commission", commission);
        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse<Object> getDeposit4LuckyWheel(int stockDef) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 新人平均成本价 (0 * 44 + 20 * 50 + 40 * 5 + 75 * 1)/10000(每次的花费) * 4(每人抽奖次数)
        BigDecimal averageCost = new BigDecimal(0.51);
        // 所需保证金
        BigDecimal depositDef = averageCost.multiply(new BigDecimal(stockDef));
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("averageCost", averageCost);
        resultVO.put("depositDef", depositDef);
        response.setData(resultVO);
        return response;
    }

    /**
     * 获取红包库存
     *
     * @param goodsIdList
     * @param stockDef
     * @param totalAmount
     * @return
     */
    @Override
    public SimpleResponse<Object> getDeposit4RedPacket(List<Long> goodsIdList, int stockDef, BigDecimal totalAmount) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        int size = goodsIdList.size();
        BigDecimal depositDef = totalAmount.multiply(new BigDecimal(size)).multiply(new BigDecimal(stockDef));
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("depositDef", depositDef);
        response.setData(resultVO);
        return response;
    }

    @Override
    public Map statisticsGoods(Map m) {
        return goodsMapper.statisticsGoods(m);
    }

    @Override
    public SimpleResponse<Object> selectError(Long goodsId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        Map<String, String> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "goods表未查到此商品，to 测试妹子 goodsId = " + goodsId);
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        response.setMessage("请先创建此商品");
        response.setData(resultVO);
        return response;
    }

    @Override
    public Map<String, Object> getGoodsVOListByIds(List<Long> goodsIdList) {
        Map<String, Object> resultVO = new HashMap<>();
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(Goods::getId, goodsIdList);
        List<Goods> goodsList = this.list(wrapper);
        if (CollectionUtils.isEmpty(goodsList)) {
            return resultVO;
        }

        List<Map<String, Object>> goodsVOList = new ArrayList<>();
        for (Goods goods : goodsList) {
            Map<String, Object> goodsVO = new HashMap<>();
            goodsVO.put("goodsId", goods.getId());
            goodsVO.put("displayGoodsName", goods.getDisplayGoodsName());
            goodsVO.put("displayPrice", goods.getDisplayPrice());
            goodsVO.put("displayBanner", goods.getDisplayBanner());
            List<GoodsPlus> goodsPluses = goodsPlusService.selectByGoodsIdAndType(goods.getMerchantId(), goods.getId(), GoodsPlusTypeEnum.KEY_WORDS.getCode());
            if (CollectionUtils.isNotEmpty(goodsPluses)) {
                GoodsPlus goodsPlus = goodsPluses.get(0);
                if (JsonParseUtil.booJsonArr(goodsPlus.getValue())) {
                    goodsVO.put("goodsKeywords", JSON.parseArray(goodsPlus.getValue()));
                }

            }
            goodsVOList.add(goodsVO);
        }

        resultVO.put("goodsVOList", goodsVOList);
        return resultVO;
    }

    @Override
    public SimpleResponse<Object> getDeposit(ActivityDefV2Req activityDefV2Req) {
        return null;
    }

    @Override
    public SimpleResponse<Object> getDeposit(DepositBO depositBO) {
        // 计算保证金
        int type = depositBO.getType();
        Integer stockDef = depositBO.getStockDef();
        BigDecimal totalAmount = depositBO.getTotalAmount();
        BigDecimal commission = depositBO.getCommission();
        log.error("totalAmount = " + totalAmount);
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
            Long goodsId = depositBO.getGoodsIdList().get(0);
            return this.getDepost4GiftAddCommission(goodsId, stockDef, commission);
        }
        if (type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            Long goodsId = depositBO.getGoodsIdList().get(0);
            return this.getDepost4GiftAddCommission(goodsId, stockDef, commission);
        }
        if (type == ActivityDefTypeEnum.ASSIST.getCode()) {
            Long goodsId = depositBO.getGoodsIdList().get(0);
            return this.getDepost4GiftAddCommission(goodsId, stockDef, commission);
        }
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            return this.getDeposit4LuckyWheel(stockDef);
        }
        if (type == ActivityDefTypeEnum.RED_PACKET.getCode()) {
            return this.getDeposit4RedPacket(depositBO.getGoodsIdList(), stockDef, totalAmount);
        }
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            return this.getDeposit4OpenRedPacket(totalAmount);
        }
        String errorMessage = "所传的活动类型type和校验的活动类型type无法对应，须沟通解决 to 木子 and 百川";
        return SellerValidator.sendErrorMessage(errorMessage);
    }

    private SimpleResponse<Object> getDeposit4OpenRedPacket(BigDecimal totalAmount) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultVO = new HashMap<>();
        // 总金额，这里拆红包的总保证金。
        resultVO.put("depositDef", totalAmount);
        response.setData(resultVO);
        return response;
    }

    @Override
    public Map getTpwd(Long goodsId) {
        Map m = goodsMapper.getTpwd(goodsId);
        return m;
    }

    /**
     * V2版活动根据product创建goods
     *
     * @param req
     * @param productEntity
     * @return
     */
    @Override
    public Goods createOrUpdateGoodsByActivityDef(CreateDefReq req, ProductEntity productEntity) {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Goods::getMerchantId, req.getMerchantId())
                .eq(Goods::getShopId, req.getShopId())
                .eq(Goods::getProductId, productEntity.getId());
        if (req.getExtraDataObject().getSkuId() == null) {
            queryWrapper.lambda().isNull(Goods::getSkuId);
        } else {
            queryWrapper.lambda().eq(Goods::getSkuId, req.getExtraDataObject().getSkuId());
        }
        List<Goods> list = this.list(queryWrapper);
        Goods goods = new Goods();
        if (!CollectionUtils.isEmpty(list)) {
            goods.setId(list.get(0).getId());
        }
        List<ProductSkuEntity> productSkuEntityList = productEntity.getProductSkuList();
        Assert.isTrue(!CollectionUtils.isEmpty(productSkuEntityList), ApiStatusEnum.ERROR_BUS_NO12.getCode(), "网络异常，请稍后重试");
        BigDecimal minPrice = new BigDecimal("100000");
        BigDecimal maxPrice = BigDecimal.ZERO;
        for (ProductSkuEntity productSkuEntity : productSkuEntityList) {
            if (productSkuEntity.getSkuSellingPrice().compareTo(minPrice) < 0) {
                minPrice = productSkuEntity.getSkuSellingPrice();
            }
            if (productSkuEntity.getSkuSellingPrice().compareTo(maxPrice) > 0) {
                maxPrice = productSkuEntity.getSkuSellingPrice();
            }
        }
        // 先存储goods表
        goods.setMerchantId(req.getMerchantId());
        goods.setShopId(req.getShopId());
        goods.setSrcType(GoodsSrcTypeEnum.JKMF.getCode());
        goods.setCategoryId(0L);
        goods.setDisplayGoodsName(productEntity.getProductTitle());
        goods.setDisplayPrice(new BigDecimal(req.getExtraDataObject().getGoodsPrice()));
        if (StringUtils.isNotBlank(productEntity.getMainPics())) {
            List<String> mainPicList = JSONArray.parseArray(productEntity.getMainPics(), String.class);
            goods.setDisplayBanner(mainPicList.get(0));
            goods.setPicUrl(mainPicList.get(0));
        }
        // 使用用户自定义主图替换掉
        if (StringUtils.isNotBlank(req.getExtraDataObject().getGoodsMainPic())) {
            goods.setDisplayBanner(req.getExtraDataObject().getGoodsMainPic());
            goods.setPicUrl(req.getExtraDataObject().getGoodsMainPic());
        }
        goods.setDescPath(productEntity.getDetailPics());
        goods.setXxbtopNumIid(productEntity.getNumIid());
        goods.setXxbtopPrice(minPrice + "-" + maxPrice);
        goods.setDel(mf.code.common.DelEnum.NO.getCode());
        goods.setCtime(new Date());
        goods.setUtime(new Date());
        goods.setOnsale(GoodsOnsaleEnum.ONSALE.getCode());
        goods.setProductId(productEntity.getId());
        goods.setSkuId(req.getExtraDataObject().getSkuId());
        goods.setTop(GoodsTopEnum.INIT.getCode());
        boolean count = this.saveOrUpdate(goods);
        Assert.isTrue(count, ApiStatusEnum.ERROR_BUS_NO12.getCode(), "网络异常，请稍后重试");
        if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
            String keyWord = req.getExtraDataObject().getKeywords();
            if (StringUtils.isNotBlank(keyWord)) {
                String[] keys = keyWord.split(",");
                goodsPlusService.saveGoodskeys(req.getMerchantId(), goods.getId(), JSON.toJSONString(Arrays.asList(keys)));
            }
        }
        return goods;
    }
}
