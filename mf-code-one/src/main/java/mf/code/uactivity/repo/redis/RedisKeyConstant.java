package mf.code.uactivity.repo.redis;

/**
 * @Author yechen
 * @Date
 **/
public class RedisKeyConstant {
    /**
     * 活动剩余库存信息，多少人正在追赶 完成活动
     */
    public static final String MF_ACTIVITY_PRUSUE = "mf:activity:prusue:";// <activityDefId>
    /**
     * 活动剩余库存信息，多少人即将达成条件 的用户列表
     */
    public static final String MF_ACTIVITY_ALMOST_COMPLETE = "mf:activity:almost:complete:";// <activityDefId>

    /**
     * 用户邀请列表
     */
    public static final String ACTIVITY_USER_JOINLIST = "uactivity:activity:user:joinlist:";//<aid>:<pub_uid>
    /**
     * 用户邀请列表增加个数
     */
    public static final String ACTIVITY_USER_JOINLIST_UPSIZE = "uactivity:activity:user:joinlist:upsize:";//<aid>:<pub_uid>
    /**
     * 用户概率
     */
    public static final String ACTIVITY_USER_COUNT = "uactivity:activity:user:count:";//<aid>
    /**
     * 用户参加活动列表
     */
    public static final String ACTIVITY_JOINLIST = "uactivity:activity:joinlist:";//<aid>

    /**
     * 用户参加活动列表(含真假数据)
     */
    public static final String ACTIVITY_JOINLIST_SHOW = "jkmf:one:activity:joinlist:show:";//<aid>

    /**
     * 用户参加活动列表(假数据)
     */
    public static final String ACTIVITY_JOINLIST_INVENTED = "jkmf:one:activity:joinlist:invented:";//<aid>

    /**
     * 用户参加活动列表(假数据)
     */
    public static final String ACTIVITY_WINNER_LIST = "jkmf:one:activity:winnerlist:";//<aid>

    public static final String ACTIVITY_WINNER_LIST_SHOW = "jkmf:one:activity:winnerlist:show:";//<aid>;


    /***
     * 任务审核对应的formid
     */
    public static final String USERTASK_FORM_ID = "uactivity:userTask:formId:";//<userTaskid>

    /**
     * 活动未中奖着名单
     */
    public static final String ACTIVITY_LOSER_SET = "uactivity:activity:loserset:";//<aid>
    /**
     * 店铺已发优惠券的用户
     */
    public static final String SHOP_COUPON_USER_OWNER = "uactivity:shop_coupon:user:owner:";//<shopId>
    /**
     * 某计划类活动或定义类活动 已发过 成长任务红包的用户
     */
    public static final String ACTIVITY_PLAN_DEF_TASK_OWNER = "uactivity:activity:plan:def:owner";//<activityPlanId>:<activityDefId>
    /**
     * 拆红包活动发起者信息
     */
    public static final String OPEN_RED_PACKAGE_STARTER = "jkmf:one:uactivity:activity:openredpackage:starter:";//<activityDefId>:<userId>
    /**
     * 拆红包活动助力者信息
     */
    public static final String OPEN_RED_PACKAGE_ASSISTER = "jkmf:one:uactivity:activity:openredpackage:assist:";//<activityId>
    /**
     * 拆红包活动中奖信息弹窗
     */
    public static final String OPEN_RED_PACKAGE_AWARD_DIALOG = "jkmf:one:uactivity:activity:openredpackage:awarddialog:";//<activityId>:<userId>;
    /**
     * 拆红包活动助力信息弹窗
     */
    public static final String OPEN_RED_PACKAGE_ASSIST_DIALOG = "jkmf:one:uactivity:activity:openredpackage:assistdialog:";//<activityId>:<userId>;
    /**
     * 拆红包活动金额列表
     */
    public static final String OPEN_RED_PACKAGE_AMOUNT_LIST = "jkmf:one:uactivity:activity:openredpackage:amountlist:";//<activityId>;
    /**
     * 拆红包奖池金额
     */
    public static final String OPEN_RED_PACKAGE_AWARD_POOL = "jkmf:one:uactivity:activity:openredpackage:awardpool:";//<activityId>;
    /**
	public static final String OPEN_RED_PACKAGE_AWARD_POOL = "uactivity:activity:openRedPackage:awardpool:";//<activityId>;
    /**
     * 拆红包校验用户是否有助力资格
     */
	public static final String OPEN_RED_PACKAGE_ASSISTABLE = "jkmf:one:uactivity:activity:openredpackage:assistable:";//<shopId>:<userId>;
    /**
     * 拆红包 完成者（中奖者）列表
     */
	public static final String OPEN_RED_PACKAGE_WINNER_LIST = "jkmf:one:uactivity:activity:openredpackage:winnerlist:";//<activityDefId>
    /**
     * 拆红包 任务过期 前端二次请求容错处理 6-5
     */
	public static final String OPEN_RED_PACKAGE_AWARD_FORBID = "jkmf:one:uactivity:activity:openredpackage:awardforbid:";//<userid>;
    /**
     * 首页回填订单红包弹框
     */
	public static final String HOME_PAGE_GOODCOMMENT_POP = "homepage:goodcomment:pop:"; //<userid>

    /**
     * 拆红包 活动过期弹窗
     */
	public static final String OPEN_RED_PACKAGE_EXPIRED_DIALOG = "jkmf:one:uactivity:activity:openredpackage:expireddialog:";//<activityId>:<userId>;;
    /**
     * 活动帮拆金额
     */
    public static final String OPEN_RED_PACKAGE_HELP = "jkmf:one:uactivity:activity:openredpackage:helpamount:";//<shopId>:<userId>;;;
    /**
     * 活动助力成功
     */
    public static final String ASSIST_SUCCESS = "jkmf:one:activity:assist:success:"; //<activity> : <uid>
    /**
     * 活动定义创建防重
     */
    public static final String CREATE_ACTIVITY_DEF_FORBID_REPEAT = "jkmf:one:forbidrepeat:activitydef:create:";// <goodsId>:<uid> 平台=0

    public static final String ACTIVITY_WINNER_DIALOG = "jkmf:one:activity:winner:dialog:"; // <aid>:<uid>
    /**
     * 存储哪个商品参加了活动
     */
    public static final String ACTIVITY_GOODS_JOIN = "jkmf:one:activitydef:goods:"; // <gid>
    /**
     * 平台免单抽奖活动开奖 防重
     */
    public static final String PLATFORM_ORDER_BACK_LOTTERY = "jkmf:one:job:forbidrepeat:platformorderback:lottery";

    /**
     * applet用户活动详情 点击按钮 防重处理
     */
    public static String ACTIVITY_SUBMIT_FORBID_REPEAT = "uactivity:activity:submit:forbid:repeat:";//<aid>:<uid>
    /**
     * applet用户中奖任务 截图提交 防重处理
     */
    public static String ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT = "uactivity:activity:awardtask:commit:pics:forbid:repeat:";//<aid>:<uid>
    /**
     * applet用户中奖任务 订单提交 防重处理
     */
    public static String ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT = "uactivity:activity:awardtask:commit:order:forbid:repeat:";//<aid>:<uid>
    /**
     * applet用户中奖任务 商品URL提交 防重处理
     */
    public static String ACTIVITY_TASK_COMMIT_GOODSURL_FORBID_REPEAT = "uactivity:activity:awardtask:commit:goodsurl:forbid:repeat:";//<uid>
    /**
     * applet活动中奖任务触发值
     */
    public static String ACTIVITY_TRIGGER = "uactivity:activity:trigger:";//<aid>
    /**
     * applet活动已中奖用户
     */
    public static String ACTIVITY_WINNER = "uactivity:activity:winner:";//<aid>
    /**
     * applet活动已中奖用户
     */
    public static final String ACTIVITY_COUPONER = "uactivity:activity:couponer:";//<aid>;

    /***
     * 活动商品
     */
    public static String ACTIVITY_GOODS = "uactivity:activity:goods:";//<goods_id>

    /**
     * 首页以商品id折叠活动,并asc分页
     */
    public static String GOODS_ID_RANKING_LIST = "uactivity:activity:goods:publishTimeSort:shopId:";
    /**
     * 幸运大转盘 详情页缓存
     */
    public static String LUCKY_WHEEL_PAGE_CACHE = "uactivity:activity:luckywheel:pagecache:";// <aid>:<uid>
    /**
     * 幸运大转盘 邀请列表缓存
     */
    public static String LUCKY_WHEEL_PUBJOIN_CACHE = "uactivity:activity:luckywheel:pubjoin_cache:";// <aid>:<uid>
    /**
     * applet幸运大轮盘 抽奖 防重处理
     */
    public static String LUCKY_WHEEL_FORBID_REPEAT = "uactivity:activity:luckywheel:forbid:repeat:";//<aid>:<uid>

    /***
     * 用户中奖-弹窗
     */
    public static String AWARDDIALOG = "uactivity:activity:awardDialog:uid:";//<uid>

    /***
     * 用户中奖-弹窗
     * jkmf:one:activity:awardDialog:<aid>:<uid>
     */
    public static String AWARDDIALOG_ACTIVITY_UID = "jkmf:one:activity:awardDialog:";

    /***
     * 用户未中奖-弹窗
     */
    public static String NOAWARDDIALOG = "uactivity:activity:noawardDialog:uid:";//<uid>;
    /**
     * 没有中奖资格者 重复点击 防刷
     */
    public static String NO_QUALIFICATION = "uactivity:activity:noqualification:";//<aid>:<uid>

    public static String FAV_CART_REDPACK_DIALOG = "uactivity:userTask:redpackDialog:";//<usertaskId>

    public static String ASSIST_ABILITY = "uactivity:activity:assist:ability:";//<userId>

    /**
     * 用户任务审核时间差
     */
    public static final String ACTIVITY_USERTASK_MISSION_TIME = "uactivity:activity:usertask:missiontime:";//<userId><activityId>
    /**
     * 用户任务审核时间差之和
     */
    public static final String ACTIVITY_USERTASK_MISSION_TIME_TOTAL = "uactivity:activity:usertask:missiontime:total:";//<userId><activityId>

    /**
     * 用户任务审核时间差
     * jkmf:one:activity:assist:cancash:<userId>
     */
    public static final String ACTIVITY_ASSIST_CAN_CASH = "jkmf:one:activity:assist:cancash:";

    //审核任务用户提交时间
    public static String USERTASK_COMMIT = "userTask:commit:";//<userTaskId>
    //审核任务更新时间
    public static String USERTASK_AUDIT = "userTask:audit:";//<userTaskId>
    //审核任务  审核更新时间-提交时间的时间差
    public static String USERTASK_COST = "userTask:cost:";//<userTaskId>

    //微信提交审核阶段
    public static String applet_commit_audit = "applet_commit_audit";
}
