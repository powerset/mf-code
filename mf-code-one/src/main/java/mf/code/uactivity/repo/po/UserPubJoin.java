package mf.code.uactivity.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * user_pub_join
 * 用户邀请记录表  用户记录用户邀请好友参与的情况
 */
public class UserPubJoin implements Serializable {
    /**
     * 用户邀请记录的主键
     */
    private Long id;

    /**
     * 邀请者的用户ID, user表的主键
     */
    private Long userId;

    /**
     * 活动ID, activity表的主键
     */
    private Long activityId;

    /**
     * 活动定义ID
     */
    private Long activityDefId;

    /**
     * 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动, 14 财富大闯关（平台用户上下级关系）
     */
    private Integer type;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 邀请时间
     */
    private Date pubTime;

    /**
     * 被邀请者的用户ID, user表的主键
     */
    private Long subUid;

    /**
     * 被邀请者 昵称
     */
    private String subNick;

    /**
     * 被邀请者 头像图片地址
     */
    private String subAvatar;

    /**
     * 被邀请者 登录时间(即用户授权成功时间)
     */
    private Date subLoginTime;

    /**
     * 被邀请者 参与活动状态, 0:未参与  1:参与
     */
    private Integer subActionStatus;

    /**
     * 被邀请者 参与活动时间
     */
    private Date subActionTime;

    /**
     * 自己（sub_uid）累计缴税给老板的金额。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     */
    private BigDecimal totalScottare;

    /**
     * 自己（sub_uid）累计的额外任务佣金。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     */
    private BigDecimal totalCommission;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * user_pub_join
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户邀请记录的主键
     * @return id 用户邀请记录的主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户邀请记录的主键
     * @param id 用户邀请记录的主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 邀请者的用户ID, user表的主键
     * @return user_id 邀请者的用户ID, user表的主键
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 邀请者的用户ID, user表的主键
     * @param userId 邀请者的用户ID, user表的主键
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 活动ID, activity表的主键
     * @return activity_id 活动ID, activity表的主键
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * 活动ID, activity表的主键
     * @param activityId 活动ID, activity表的主键
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getActivityDefId() {
        return activityDefId;
    }

    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动, 14 财富大闯关（平台用户上下级关系）
     * @return type 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动, 14 财富大闯关（平台用户上下级关系）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动, 14 财富大闯关（平台用户上下级关系）
     * @param type 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动, 14 财富大闯关（平台用户上下级关系）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 邀请时间
     * @return pub_time 邀请时间
     */
    public Date getPubTime() {
        return pubTime;
    }

    /**
     * 邀请时间
     * @param pubTime 邀请时间
     */
    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    /**
     * 被邀请者的用户ID, user表的主键
     * @return sub_uid 被邀请者的用户ID, user表的主键
     */
    public Long getSubUid() {
        return subUid;
    }

    /**
     * 被邀请者的用户ID, user表的主键
     * @param subUid 被邀请者的用户ID, user表的主键
     */
    public void setSubUid(Long subUid) {
        this.subUid = subUid;
    }

    /**
     * 被邀请者 昵称
     * @return sub_nick 被邀请者 昵称
     */
    public String getSubNick() {
        return subNick;
    }

    /**
     * 被邀请者 昵称
     * @param subNick 被邀请者 昵称
     */
    public void setSubNick(String subNick) {
        this.subNick = subNick == null ? null : subNick.trim();
    }

    /**
     * 被邀请者 头像图片地址
     * @return sub_avatar 被邀请者 头像图片地址
     */
    public String getSubAvatar() {
        return subAvatar;
    }

    /**
     * 被邀请者 头像图片地址
     * @param subAvatar 被邀请者 头像图片地址
     */
    public void setSubAvatar(String subAvatar) {
        this.subAvatar = subAvatar == null ? null : subAvatar.trim();
    }

    /**
     * 被邀请者 登录时间(即用户授权成功时间)
     * @return sub_login_time 被邀请者 登录时间(即用户授权成功时间)
     */
    public Date getSubLoginTime() {
        return subLoginTime;
    }

    /**
     * 被邀请者 登录时间(即用户授权成功时间)
     * @param subLoginTime 被邀请者 登录时间(即用户授权成功时间)
     */
    public void setSubLoginTime(Date subLoginTime) {
        this.subLoginTime = subLoginTime;
    }

    /**
     * 被邀请者 参与活动状态, 0:未参与  1:参与
     * @return sub_action_status 被邀请者 参与活动状态, 0:未参与  1:参与
     */
    public Integer getSubActionStatus() {
        return subActionStatus;
    }

    /**
     * 被邀请者 参与活动状态, 0:未参与  1:参与
     * @param subActionStatus 被邀请者 参与活动状态, 0:未参与  1:参与
     */
    public void setSubActionStatus(Integer subActionStatus) {
        this.subActionStatus = subActionStatus;
    }

    /**
     * 被邀请者 参与活动时间
     * @return sub_action_time 被邀请者 参与活动时间
     */
    public Date getSubActionTime() {
        return subActionTime;
    }

    /**
     * 被邀请者 参与活动时间
     * @param subActionTime 被邀请者 参与活动时间
     */
    public void setSubActionTime(Date subActionTime) {
        this.subActionTime = subActionTime;
    }

    /**
     * 自己（sub_uid）累计缴税给老板的金额。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     * @return total_scottare 自己（sub_uid）累计缴税给老板的金额。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     */
    public BigDecimal getTotalScottare() {
        return totalScottare;
    }

    /**
     * 自己（sub_uid）累计缴税给老板的金额。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     * @param totalScottare 自己（sub_uid）累计缴税给老板的金额。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     */
    public void setTotalScottare(BigDecimal totalScottare) {
        this.totalScottare = totalScottare;
    }

    /**
     * 自己（sub_uid）累计的额外任务佣金。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     * @return total_commission 自己（sub_uid）累计的额外任务佣金。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     */
    public BigDecimal getTotalCommission() {
        return totalCommission;
    }

    /**
     * 自己（sub_uid）累计的额外任务佣金。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     * @param totalCommission 自己（sub_uid）累计的额外任务佣金。在财富大闯关中，自己就是sub_uid的角色，sub_uid是唯一的一条记录
     */
    public void setTotalCommission(BigDecimal totalCommission) {
        this.totalCommission = totalCommission;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", activityId=").append(activityId);
        sb.append(", activityDefId=").append(activityDefId);
        sb.append(", type=").append(type);
        sb.append(", shopId=").append(shopId);
        sb.append(", pubTime=").append(pubTime);
        sb.append(", subUid=").append(subUid);
        sb.append(", subNick=").append(subNick);
        sb.append(", subAvatar=").append(subAvatar);
        sb.append(", subLoginTime=").append(subLoginTime);
        sb.append(", subActionStatus=").append(subActionStatus);
        sb.append(", subActionTime=").append(subActionTime);
        sb.append(", totalScottare=").append(totalScottare);
        sb.append(", totalCommission=").append(totalCommission);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}