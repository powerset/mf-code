package mf.code.uactivity.repo.redis;

import com.alibaba.fastjson.JSONObject;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.impl.AppletUserActivityServiceImpl;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.common.constant.UserPubJoinStatusEnum;
import mf.code.common.utils.DateUtil;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.uactivity.repo.redis
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月30日 17:08
 */
@Service
public class RedisServiceImpl implements RedisService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private AppletUserActivityServiceImpl appletUserActivityServiceImpl;
    @Autowired
    private UserService userService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private UserTaskService userTaskService;

    /***
     * 过滤当天创建活动
     * @param activityCtime
     * @return
     */
    private boolean filterTodayActivity(Date activityCtime) {
        //过滤当天活动的查询
        String activityCtimeDate = DateFormatUtils.format(activityCtime, "yyyy-MM-dd");
        if (DateUtil.getCurrDate("yyyy-MM-dd").equals(activityCtimeDate)) {
            return true;
        }
        return false;
    }

    /***
     * 获取活动参与人数
     * @param activityID
     * @return
     */
    @Override
    public List<String> queryRedisJoinUser(Long activityID) {
        //查询redis,参与人展现,直接倒叙排列
        BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(RedisKeyConstant.ACTIVITY_JOINLIST + activityID);
        List<String> strs = activityPersons.range(0, -1);
        //判断redis数据是否为空，为空则去数据库插入再次更新insert一波，非空直接返回
        if (strs != null && strs.size() > 0) {
            return strs;
        }

        Activity activity = this.activityService.findById(activityID);
        if (activity == null) {
            return new ArrayList<String>();
        }
        if (this.filterTodayActivity(activity.getCtime())) {
            return new ArrayList<String>();
        }
        boolean ORDER_BACK_START = false;
        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            ORDER_BACK_START = true;
        }
        Map<String, Object> userActivityParams = new HashMap<String, Object>();
        userActivityParams.put("merchantId", activity.getMerchantId());
        userActivityParams.put("shopId", activity.getShopId());
        userActivityParams.put("activityId", activity.getId());
        userActivityParams.put("order", "ctime");
        userActivityParams.put("direct", "asc");
        //状态不为0-->意义是未参加的用户==脏数据的存储
        userActivityParams.put("statusNot", UserActivityStatusEnum.NO_QUALIFICATION.getCode());
        List<UserActivity> userActivities = this.userActivityService.pageListUserActivity(userActivityParams);
        if (userActivities == null || userActivities.size() == 0) {
            return new ArrayList<String>();
        }
        List<Long> userIDs = new ArrayList<Long>();
        for (UserActivity userActivity : userActivities) {
            if (userIDs.indexOf(userActivity.getUserId()) == -1) {
                userIDs.add(userActivity.getUserId());
            }
        }
        Map<Long, User> userMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userIDs)) {
            Map<String, Object> userParams = new HashMap<>();
            userParams.put("userIds", userIDs);
            List<User> users = this.userService.query(userParams);
            if (users != null && users.size() > 0) {
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }

        }
        for (UserActivity userActivity : userActivities) {
            User user = userMap.get(userActivity.getUserId());
            if (user == null) {
                continue;
            }
            //若该活动是新人有礼发起|助力，则本人不需要放进redis
            boolean newManStart = activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode() && activity.getUserId() != null && activity.getUserId().longValue() == user.getId().longValue();
            boolean assist = (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode())
                    && activity.getUserId() != null && activity.getUserId().longValue() == user.getId().longValue();
            if (assist || newManStart) {
                continue;
            }

            // 更新redis抽奖概率
            if (ORDER_BACK_START && activity.getUserId() != null && activity.getUserId().longValue() == userActivity.getUserId().longValue()) {
                JSONObject userJsonObject = this.appletUserActivityServiceImpl.processActivityfreeOrderLeaderInfo(user);
                this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + userActivity.getActivityId(), userJsonObject.toJSONString());
                this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + userActivity.getActivityId(), 1, TimeUnit.DAYS);

                this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, userActivity.getUserId().toString(), 10);
                this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, 1, TimeUnit.DAYS);
            } else {
                JSONObject userJsonObject = this.appletUserActivityServiceImpl.processActivityJoinUserInfo(user);
                this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + userActivity.getActivityId(), userJsonObject.toJSONString());
                this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + userActivity.getActivityId(), 1, TimeUnit.DAYS);

                Map<String, Object> userPubJoinParams = new HashMap<String, Object>();
                userPubJoinParams.put("activityId", activityID);
                userPubJoinParams.put("subUid", userActivity.getUserId());
                List<UserPubJoin> userPubJoins = this.userPubJoinService.listPageByParams(userPubJoinParams);
                if (userPubJoins == null || userPubJoins.size() == 0) {//为空，说明该用户是自发的参与。不是被邀请参与
                    this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, userActivity.getUserId().toString(), 1);
                    this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, 1, TimeUnit.DAYS);
                } else {
                    if (!stringRedisTemplate.opsForHash().hasKey(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, userActivity.getUserId().toString())) {
                        this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, userActivity.getUserId().toString(), 1);
                        this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, 1, TimeUnit.DAYS);
                    }
                }
            }
        }
        BoundListOperations activityPersonsNew = this.stringRedisTemplate.boundListOps(RedisKeyConstant.ACTIVITY_JOINLIST + activityID);
        return activityPersonsNew.range(0, -1);
    }

    public JSONObject processActivityJoinUserFormId(Long userID, String formID) {
        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("userId", userID);
        userJsonObject.put("formId", formID);
        return userJsonObject;
    }

    /***
     * 获取用户邀请人信息
     * @param activityID
     * @param userID
     * @return
     */
    @Override
    public List<String> queryRedisJoinInviteUser(Long activityID, Long userID) {
        List<String> activityUserJoinList = this.stringRedisTemplate.
                boundListOps(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityID + ":" + userID).range(0, -1);
        if (activityUserJoinList != null && activityUserJoinList.size() > 0) {
            return activityUserJoinList;
        }
        User user = this.userService.selectByPrimaryKey(userID);
        if (user == null) {
            return new ArrayList<String>();
        }
        Activity activity = this.activityService.findById(activityID);
        if (activity == null) {
            return new ArrayList<String>();
        }
        if (this.filterTodayActivity(activity.getCtime())) {
            return new ArrayList<String>();
        }

        Map<String, Object> userPubJoinParams = new HashMap<String, Object>();
        userPubJoinParams.put("userId", userID);
        userPubJoinParams.put("activityId", activityID);
        userPubJoinParams.put("subActionStatus", UserPubJoinStatusEnum.JOINED.getCode());
        userPubJoinParams.put("order", "ctime");
        userPubJoinParams.put("direct", "asc");
        List<UserPubJoin> userPubJoins = this.userPubJoinService.listPageByParams(userPubJoinParams);
        if (userPubJoins == null || userPubJoins.size() == 0) {
            return new ArrayList<String>();
        }
        for (UserPubJoin userPubJoin : userPubJoins) {
            // 加工用户邀请列表的用户信息 放入redis
            JSONObject userJsonObject = this.appletUserActivityServiceImpl.processActivityJoinUserInfo(user);
            this.stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityID + ":" + userID, userJsonObject.toJSONString());
            this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityID, 1, TimeUnit.DAYS);

            // 更新redis抽奖概率
            this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, userID.toString(), 1);
            this.stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, userPubJoin.getSubUid(), 1);

            this.stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + activityID, 1, TimeUnit.DAYS);
        }
        BoundListOperations activityUserJoinNew = this.stringRedisTemplate.
                boundListOps(RedisKeyConstant.ACTIVITY_USER_JOINLIST + activityID + ":" + userID);
        return activityUserJoinNew.range(0, -1);
    }

    /***
     * 获取活动的中奖人信息编号
     * @param activityID
     * @return
     */
    @Override
    public List<Long> getRedisWinnerUserIDs(Long activityID) {
        String str = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_WINNER + activityID);
        //获取中奖人的用户id
        List<Long> winnerUserIDs = JSONObject.parseArray(str, Long.class);
        if (winnerUserIDs != null && winnerUserIDs.size() > 0) {
            return winnerUserIDs;
        }
        Activity activity = this.activityService.findById(activityID);
        if (activity == null) {
            return new ArrayList<Long>();
        }
        //获取中奖人任务信息
        List<UserTask> userTasks = this.userTaskService.queryAwardTask(
                activity.getMerchantId(),
                activity.getShopId(),
                null,
                activity.getId());
        if (userTasks == null || userTasks.size() == 0) {
            return new ArrayList<Long>();
        }
        List<Long> winnerIDs = new ArrayList<>();
        for (UserTask userTask : userTasks) {
            if (winnerIDs.indexOf(userTask.getUserId()) == -1) {
                winnerIDs.add(userTask.getUserId());
            }
        }
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER + activityID, JSONObject.toJSONString(winnerIDs), 1, TimeUnit.DAYS);
        return winnerIDs;
    }

    /***
     * 查询红包任务弹窗
     * @param userTaskID
     * @return
     */
    @Override
    public boolean getRedisUserTaskRedPack(Long userTaskID) {
        // 删除直接返回成功与否
        Boolean b = this.stringRedisTemplate.delete(RedisKeyConstant.FAV_CART_REDPACK_DIALOG + userTaskID);
        return b == null ? false : b;
    }

    /***
     * 存储红包任务弹窗
     * @param userTaskID
     */
    @Override
    public void saveRedisUserTaskRedPack(Long userTaskID) {
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.FAV_CART_REDPACK_DIALOG + userTaskID, "1", 1, TimeUnit.DAYS);
    }

    /***
     * 查询活动 用户邀请的上次进来的邀请个数
     * @param activityID
     * @param userID
     * @return
     */
    @Override
    public Integer queryRedisJoinInviteLastNum(Long activityID, Long userID) {
        return NumberUtils.toInt(this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_USER_JOINLIST_UPSIZE + activityID + ":" + userID));
    }

    /***
     * 更新活动 用户邀请的上次进来的邀请个数
     * @param activityID
     * @param userID
     * @return
     */
    @Override
    public Integer updateRedisJoinInviteLastNum(Long activityID, Long userID, Integer newJoinInviteNum) {
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_USER_JOINLIST_UPSIZE + activityID + ":" + userID, newJoinInviteNum.toString());
        return 1;
    }

    /**
     * key 以店铺id区分
     *
     * @param shopId  zset的Key
     * @param goodsId
     * @param score   分数->时间戳
     * @return
     */
    @Override
    public boolean addRankingList(String shopId, String goodsId, Long score) {
        return stringRedisTemplate.opsForZSet().add(RedisKeyConstant.GOODS_ID_RANKING_LIST + shopId, goodsId, score);
    }

    /***
     * 删除任务提交+审核 时间
     * @param userTaskId
     * @return
     */
    @Override
    public boolean deleteUserTaskCommitAndAudit(Long userTaskId) {
        this.stringRedisTemplate.delete(RedisKeyConstant.USERTASK_COMMIT + userTaskId);
        this.stringRedisTemplate.delete(RedisKeyConstant.USERTASK_AUDIT + userTaskId);
        this.stringRedisTemplate.delete(RedisKeyConstant.USERTASK_COST + userTaskId);
        return true;
    }

    /***
     *
     * @param userTaskId
     * @return
     */
    @Override
    public void updateUserTaskSupplyAuditCost(Long userTaskId, long costTime) {
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.USERTASK_COST + userTaskId, String.valueOf(costTime));
    }

    /***
     *
     * @param userTaskId
     * @return
     */
    @Override
    public String getUserTaskSupplyAuditCost(Long userTaskId) {
        return this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.USERTASK_COST + userTaskId);
    }

    /***
     * 小程序提交审核过程中，对有些事物进行处理 true:即是小程序审核阶段，false:过审上线了
     * @return
     */
    @Override
    public boolean selectCommitAuditWx(String version) {
        String s = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.applet_commit_audit);
        if (StringUtils.isNotBlank(s) && s.equals(version)) {
            return true;
        } else {
            return false;
        }
    }

    /***
     * 小程序审核阶段调用
     */
    @Override
    public void saveCommitAuditWx(String version) {
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.applet_commit_audit, version);
    }

    /***
     * 小程序过审阶段调用
     * @return
     */
    @Override
    public boolean deleteCommitAuditWx() {
        Boolean b = this.stringRedisTemplate.delete(RedisKeyConstant.applet_commit_audit);
        return b == null ? false : b;
    }
}
