package mf.code.uactivity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.uactivity.repo.po.UserActivity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface UserActivityMapper extends BaseMapper<UserActivity> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(UserActivity record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(UserActivity record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    UserActivity selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UserActivity record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UserActivity record);

    List<UserActivity> listPageByParams(Map<String, Object> mapParams);

    int updateWinnerStatusByUserIdAndActivityId(UserActivity userActivity);

    int batchUpdateUserActivitiesByUserIdsAndActivityId(@Param("userActivities") List<UserActivity> userActivities);

    int batchUpdateCouponUserStatus(@Param("userIds") List<Long> userIds, @Param("activityId") Long aid, @Param("couponId") Long couponId, @Param("couponStartTime") Date couponStartTime, @Param("couponEndTime") Date couponEndTime, @Param("utime") Date utime);

    List<UserActivity> findUserActivityByShopIdAndUserId(@Param("shopId") Long shopId, @Param("userId") Long userId);

    int countUserActivity(Map<String, Object> mapParams);

    int batchUpdateTaskCollectCartUserStatus(@Param("userIds") List<Long> userIds, @Param("activityId") Long aid, @Param("utime") Date utime);

    int updateStatusByUserIdAndActivityId(@Param("userId") Long userId, @Param("activityId") Long activityId, @Param("status") Integer status, @Param("utime") Date utime);

    UserActivity findByActivityIdAndUserId(Map map);

    UserActivity findByGoodsId(Long goodsId);

    Long sumUserJoinActivity(Map m);

    Long countUserAll(Map<String, Object> param);

    List<UserActivity> pageListUserActivityShopAndPlatform(Map<String, Object> userActivityParams);

    int countUserActivityShopAndPlatform(Map<String, Object> userActivityParams);
}
