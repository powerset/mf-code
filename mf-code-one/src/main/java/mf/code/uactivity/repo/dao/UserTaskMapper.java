package mf.code.uactivity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.uactivity.repo.po.UserTask;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface UserTaskMapper extends BaseMapper<UserTask> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(UserTask record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(UserTask record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    UserTask selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UserTask record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UserTask record);

    List<UserTask> listPageByParams(Map<String, Object> map);

    List<UserTask> selectByActivityId(@Param("activityId") Long aid);

    List<UserTask> listByAidAndStatus(@Param("activityId") Long aid, @Param("statusList") List<Integer> statusList);

    int updatePicsByUidAndAid(UserTask record);

    List<UserTask> selectByOrderId(String orderId);

    List<UserTask> selectByUidAndAid(@Param("userId") Long uid, @Param("activityId") Long aid);

    int countUserTask(Map<String, Object> map);

    BigDecimal sumUserTaskAmount(Map<String, Object> map);

    Integer batchInsertUserTasks(@Param("userTasks") List<UserTask> userTasks);

    int countUserTaskByCtimePlus(Map map);

    Integer updateStockFlagByIdArr(Map ids);

    UserTask findByGoodsId(Long goodsId);

    UserTask findByUserIdActivityId(Map m);

    Long sumUserTask(Map m);

    int countUserTaskShopAndPlatform(Map<String, Object> userTaskParams);

    List<UserTask> queryShopAndPlatform(Map<String, Object> userTaskParams);
}
