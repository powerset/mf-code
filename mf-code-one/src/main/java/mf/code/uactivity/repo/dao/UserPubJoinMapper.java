package mf.code.uactivity.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.uactivity.repo.po.UserPubJoin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface UserPubJoinMapper extends BaseMapper<UserPubJoin> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(UserPubJoin record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(UserPubJoin record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    UserPubJoin selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UserPubJoin record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UserPubJoin record);

    /**
     * 通过被邀请者id 和 活动 id 查询 数据是否存在 判断 该用户是否 已被其他用户邀请
     * @param subUid
     * @param activityId
     * @return
     */
    List<UserPubJoin> selectBySubUidAndAid(@Param("subUid") Long subUid, @Param("activityId") Long activityId);

    int updateActivityJoinBySubUidAndAid(@Param("subUid") Long subUid, @Param("activityId") Long activityId, @Param("subActionTime") Date subActionTime);

    List<UserPubJoin> listByUidAidStatusLimit(@Param("userId") Long userId, @Param("activityId") Long activityId, @Param("status") Integer status, @Param("offset") int offset, @Param("size") int size);

    UserPubJoin selectByUserAndSubUidAndAid(@Param("pubUid") Long pubUid, @Param("subUid") Long subUid, @Param("activityId") Long aid);

    List<UserPubJoin> listByUidAidStatus(@Param("userId") Long userId, @Param("activityId") Long activityId, @Param("status") Integer status);

    /**
     * 根据条件参数 查询总数
     * @param param 查询条件
     * @return 总条数
     */
    int countByParams(Map<String, Object> param);

    /***
     * 根据条件获取下级用户所有的缴税汇总金额
     * @param param
     * @return
     */
    BigDecimal sumByTotalScottare(Map<String, Object> param);

    /**
     * 通过 条件参数 可 分页查询 数据
     * @param param 查询条件参数
     * @return list对象
     */
    List<UserPubJoin> listPageByParams(Map<String, Object> param);

    Integer updatePubJoinCommission(@Param("id") Long id, @Param("commission") BigDecimal commission, @Param("tax") BigDecimal tax, @Param("utime") Date date);

    Long sumFans(Map<String, Object> m);

    UserPubJoin findParent(Map<String, Object> m);

    List<UserPubJoin> selectAllMyMiner(Map<String, Object> params);

    Integer countAllMyMiner(Map<String, Object> params);
}
