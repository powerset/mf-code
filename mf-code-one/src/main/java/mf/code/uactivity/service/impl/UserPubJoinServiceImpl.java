package mf.code.uactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.common.constant.UserPubJoinStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.constant.UserPubJoinConstant;
import mf.code.uactivity.repo.dao.UserPubJoinMapper;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.user.constant.UserConstant;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.uactivity.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-08 11:22
 */
@Slf4j
@Service
public class UserPubJoinServiceImpl extends ServiceImpl<UserPubJoinMapper, UserPubJoin> implements UserPubJoinService {
    @Autowired
    private UserPubJoinMapper userPubJoinMapper;


    @Override
    public UserPubJoin insertUserPubJoin(Long aid, Long pubUid, User user, Integer type) {
        UserPubJoin userPubJoin = new UserPubJoin();
        userPubJoin.setUserId(pubUid);
        userPubJoin.setActivityId(aid);
        userPubJoin.setType(type);
        userPubJoin.setShopId(user.getShopId());
        userPubJoin.setPubTime(new Date());
        userPubJoin.setSubUid(user.getId());
        userPubJoin.setSubAvatar(user.getAvatarUrl());
        userPubJoin.setSubNick(user.getNickName());
        userPubJoin.setSubActionStatus(UserPubJoinConstant.SUB_ACTION_STATUS_NO);
        userPubJoin.setTotalScottare(BigDecimal.ZERO);
        userPubJoin.setTotalCommission(BigDecimal.ZERO);
        userPubJoin.setCtime(new Date());
        userPubJoin.setUtime(new Date());
        int rows = userPubJoinMapper.insertSelective(userPubJoin);
        if (rows == 0) {
            return null;
        }
        return userPubJoin;
    }

    @Override
    public List<UserPubJoin> selectBySubUidAndAid(Long uid, Long aid) {
        return userPubJoinMapper.selectBySubUidAndAid(uid, aid);
    }

    @Override
    public int updateActivityJoinBySubUidAndAid(Long subUid, Long aid) {
        return userPubJoinMapper.updateActivityJoinBySubUidAndAid(subUid, aid, new Date());
    }

    @Override
    public List<UserPubJoin> listByUidAidStatusLimit(Long userId, Long activityId, Integer status, int offset, int size) {
        return userPubJoinMapper.listByUidAidStatusLimit(userId, activityId, status, offset, size);
    }

    @Override
    public int updateByPrimaryKeySelective(UserPubJoin userPubJoin) {
        return userPubJoinMapper.updateByPrimaryKeySelective(userPubJoin);
    }

    @Override
    public UserPubJoin selectByUserAndSubUidAndAid(Long pubUid, Long uid, Long aid) {
        return userPubJoinMapper.selectByUserAndSubUidAndAid(pubUid, uid, aid);
    }

    @Override
    public SimpleResponse userPubJoin(Long pubUid, Long aid, User user, Integer type) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 判断此用户是否是 被邀请进来的
        if (pubUid == null) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setMessage("此用户不是 被邀请进来的");
            return response;
        }

        boolean pubUidExist = false;
        UserPubJoin userPubJoin = null;

        List<UserPubJoin> userPubJoins;
        if (type != null && type == UserPubJoinTypeEnum.LUCK_WHEEL.getCode()) {
            Map<String, Object> param = new HashMap<>();
            param.put("shopId", user.getShopId());
            param.put("subUid", user.getId());
            userPubJoins = userPubJoinMapper.listPageByParams(param);
        } else {
            userPubJoins = userPubJoinMapper.selectBySubUidAndAid(user.getId(), aid);
        }
        if (userPubJoins != null && userPubJoins.size() > 0) {
            for (UserPubJoin userPubJoinDB : userPubJoins) {
                // 此用户已被其他人邀请过
               /* if (userPubJoinDB.getSubActionStatus() == 1) {
                    response.setStatusEnum(ApiStatusEnum.SUCCESS);
                    response.setMessage("此用户已被其他人邀请过");
                    return response;
                }*/
                // 判断 是否 已存在邀请人的记录
                if (type != null
                        && type == UserPubJoinTypeEnum.LUCK_WHEEL.getCode()
                        && userPubJoinDB.getType() != UserPubJoinTypeEnum.LUCK_WHEEL.getCode()) {
                    continue;
                }
                if (pubUid.equals(userPubJoinDB.getUserId())) {
                    pubUidExist = true;
                    userPubJoin = userPubJoinDB;
                }
            }
        }


        // 用户邀请记录不存在 或者 用户邀请记录 非 本邀请人的记录
        if (!pubUidExist) {
            userPubJoin = insertUserPubJoin(aid, pubUid, user, type);
        }
        // 判断 用户是否授权
        if (user.getGrantStatus().equals(UserConstant.GRANT_STATUS_NO)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setMessage("此用户没有接受授权");
            return response;
        }
        // 更新用户邀请记录
        userPubJoin.setSubLoginTime(user.getGrantTime());
        userPubJoin.setActivityId(aid);
        userPubJoin.setUtime(new Date());
        userPubJoinMapper.updateByPrimaryKeySelective(userPubJoin);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setMessage("更新用户邀请记录成功");
        response.setData(userPubJoin);
        return response;
    }

    @Override
    public SimpleResponse userPubJoin(Long pubUid, Activity activity, User user) {
        Integer type = getUserPubJoinType(activity);
        Long activityId = 0L;
        if (activity != null) {
            activityId = activity.getId();
        }
        return userPubJoin(pubUid, activityId, user, type);
    }

    @Override
    public List<UserPubJoin> listByUidAidStatus(Long userId, Long activityId, int status) {
        return userPubJoinMapper.listByUidAidStatus(userId, activityId, status);
    }

    @Override
    public int countByParams(Map<String, Object> param) {
        return userPubJoinMapper.countByParams(param);
    }

    @Override
    public List<UserPubJoin> listPageByParams(Map<String, Object> param) {
        return userPubJoinMapper.listPageByParams(param);
    }

    @Override
    public void updatePubJoinSuccess(Long pubUserId, Long userId, Long activityId) {
        QueryWrapper<UserPubJoin> upjWrapper = new QueryWrapper<>();
        upjWrapper.lambda()
                .eq(UserPubJoin::getUserId, pubUserId)
                .eq(UserPubJoin::getSubUid, userId)
                .eq(UserPubJoin::getActivityId, activityId);
        UserPubJoin userPubJoin = this.getOne(upjWrapper);
        if (userPubJoin == null) {
            return;
        }
        userPubJoin.setSubActionStatus(UserPubJoinStatusEnum.JOINED.getCode());
        Date now = new Date();
        userPubJoin.setSubActionTime(now);
        userPubJoin.setUtime(now);
        this.updateById(userPubJoin);
    }

    @Override
    public void userPubJoin4Checkpoints(User user, User pubUser, Long platformUserId) {

        QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .eq(UserPubJoin::getSubUid, user.getId());
        List<UserPubJoin> userPubJoins = this.list(wrapper);

        if (!CollectionUtils.isEmpty(userPubJoins) && !userPubJoins.get(0).getUserId().equals(platformUserId)) {
            // 存在 并且 上级不是平台，则不能被抓
            return;
        }

        // 判断pubUser是不是在自己的粉丝店铺抓矿工
        if (!user.getShopId().equals(pubUser.getVipShopId())) {
            // 被邀请进来的用户更新店铺后的shopId，与邀请者的粉丝店铺Id不匹配，说明邀请者是在非他粉丝店铺中邀请的，不能成为矿工
            return;
        }

        // 判断user和pubUser是不是同一个店铺的粉丝
        if (!user.getVipShopId().equals(pubUser.getVipShopId())) {
            // 不是同一个店铺的粉丝，抓不了矿工
            return;
        }

        // 被邀请者 未授权，不能成为矿工
        if (user.getGrantStatus().equals(UserConstant.GRANT_STATUS_NO)) {
            return;
        }

        // 到这步 以上 准入标准 已通过，进行 入库
        if (CollectionUtils.isEmpty(userPubJoins)) {
            saveUserPubJoin4Checkpoints(pubUser.getId(), user);
        } else {
            UserPubJoin userPubJoin = userPubJoins.get(0);
            userPubJoin.setUserId(pubUser.getId());
            userPubJoin.setTotalScottare(BigDecimal.ZERO);
            userPubJoin.setUtime(new Date());
            this.updateById(userPubJoin);
        }


    }

    @Override
    public void bePlatformMiner(User user, User platformUser) {
        saveUserPubJoin4Checkpoints(platformUser.getId(), user);
    }

    @Override
    public BigDecimal sumByTotalScottare(Map<String, Object> param) {
        return this.userPubJoinMapper.sumByTotalScottare(param);
    }

    /**
     * 根据主键更新分配佣金
     *
     * @param id         主键
     * @param commission 自己获得佣金
     * @param tax        缴纳佣金
     * @return 更新条数
     */
    @Override
    public Integer updatePubJoinCommission(@NonNull Long id, @NonNull BigDecimal commission, @NonNull BigDecimal tax) {
        return userPubJoinMapper.updatePubJoinCommission(id, commission, tax, new Date());
    }

    @Override
    public Long sumFans(Long shopId, Long userId) {
        Map<String,Object> m  = new HashMap<>(2);
        m.put("shopId",shopId);
        m.put("userId",userId);
        return userPubJoinMapper.sumFans(m);
    }

    @Override
    public UserPubJoin findParent(Long shopId, Long userId) {
        Map<String,Object> m = new HashMap<>(2);
        m.put("shopId",shopId);
        m.put("userId",userId);
        return userPubJoinMapper.findParent(m);
    }

    /**
     * 查询我的所有矿工
     * @param params
     * @return
     */
    @Override
    public List<UserPubJoin> findAllMyMiner(Map<String, Object> params) {
        return userPubJoinMapper.selectAllMyMiner(params);
    }

    /**
     * 统计所有矿工数
     * @param params
     * @return
     */
    @Override
    public Integer countAllMyMiner(Map<String, Object> params) {
        return userPubJoinMapper.countAllMyMiner(params);
    }


    private UserPubJoin saveUserPubJoin4Checkpoints(Long pubUserId, User user) {
        UserPubJoin userPubJoin = new UserPubJoin();
        userPubJoin.setUserId(pubUserId);
        userPubJoin.setActivityId(-1L);
        userPubJoin.setType(UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        userPubJoin.setShopId(user.getShopId());
        userPubJoin.setPubTime(new Date());
        userPubJoin.setSubUid(user.getId());
        userPubJoin.setSubAvatar(user.getAvatarUrl());
        userPubJoin.setSubNick(user.getNickName());
        userPubJoin.setSubLoginTime(user.getGrantTime());
        userPubJoin.setSubActionStatus(UserPubJoinConstant.SUB_ACTION_STATUS_YES);
        userPubJoin.setSubActionTime(new Date());
        userPubJoin.setCtime(new Date());
        userPubJoin.setUtime(new Date());
        int rows = userPubJoinMapper.insertSelective(userPubJoin);
        if (rows == 0) {
            return null;
        }
        return userPubJoin;
    }

    private Integer getUserPubJoinType(Activity activity) {
        Integer type = null;
        if (activity != null && activity.getType() == ActivityTypeEnum.LUCK_WHEEL.getCode()) {
            type = UserPubJoinTypeEnum.LUCK_WHEEL.getCode();
        }
        if (activity != null && activity.getType() == ActivityTypeEnum.MCH_PLAN.getCode()) {
            type = UserPubJoinTypeEnum.MCH_PLAN.getCode();
        }
        if (activity != null
                && (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode()
                || activity.getType() == ActivityTypeEnum.ORDER_BACK_JOIN.getCode()
                || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()
                || activity.getType() == ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode())) {
            type = UserPubJoinTypeEnum.ORDER_BACK.getCode();
        }
        if (activity != null
                && (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                || activity.getType() == ActivityTypeEnum.NEW_MAN_JOIN.getCode())) {
            type = UserPubJoinTypeEnum.NEW_MAN.getCode();
        }
        if (activity != null && activity.getType() == ActivityTypeEnum.ORDER_REDPACK.getCode()) {
            type = UserPubJoinTypeEnum.ORDER_REDPACK.getCode();
        }
        if ((activity != null && (activity.getType() == ActivityTypeEnum.ASSIST.getCode())
                ||activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode())) {
            type = UserPubJoinTypeEnum.ASSIST.getCode();
        }
        return type;
    }
}
