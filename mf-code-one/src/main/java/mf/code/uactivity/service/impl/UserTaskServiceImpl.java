package mf.code.uactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityTaskService;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.uactivity.repo.dao.UserTaskMapper;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.uactivity.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月30日 15:10
 */
@Service
public class UserTaskServiceImpl extends ServiceImpl<UserTaskMapper, UserTask> implements UserTaskService {
    @Autowired
    private UserTaskMapper userTaskMapper;
    @Autowired
    private ActivityTaskService activityTaskService;

    @Override
    public int update(UserTask usertask) {
        return userTaskMapper.updateByPrimaryKeySelective(usertask);
    }

    @Override
    public UserTask selectByPrimaryKey(Long id) {
        return this.userTaskMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<UserTask> query(Map<String, Object> map) {
        return this.userTaskMapper.listPageByParams(map);
    }

    @Override
    public int insertUserTask(UserTask userTask) {
        return userTaskMapper.insertSelective(userTask);
    }

    @Override
    public List<UserTask> selectByActivityId(Long aid) {
        return userTaskMapper.selectByActivityId(aid);
    }

    @Override
    public List<UserTask> listByAidAndStatus(Long aid, List<Integer> statusList) {
        return userTaskMapper.listByAidAndStatus(aid, statusList);
    }

    @Override
    public UserTask insertUserTask(Long mid, Long shopId, Long aid, Long userId, Integer status, Integer type, String keyWords, Long goodsId, String orderId) {
        UserTask userTask = new UserTask();
        this.fromUserTask(userTask, mid, shopId, aid, userId, status, type, keyWords, goodsId, orderId, null, null);
        int rows = userTaskMapper.insertSelective(userTask);
        if (rows == 0) {
            return null;
        }
        return userTask;
    }

    private UserTask fromUserTask(UserTask userTask, Long mid, Long shopId, Long aid, Long userId, Integer status, Integer type, String keyWords, Long goodsId, String orderId, Long activityTaskId, Long userActivityId) {
        userTask.setType(type);
        userTask.setUserId(userId);
        userTask.setKeyWords(keyWords);
        userTask.setMerchantId(mid);
        userTask.setShopId(shopId);
        userTask.setActivityId(aid);
        userTask.setGoodsId(goodsId);
        userTask.setStatus(status);
        userTask.setCtime(new Date());
        userTask.setUtime(new Date());
        if (org.apache.commons.lang3.StringUtils.isNotBlank(orderId)) {
            userTask.setOrderId(orderId);
        }
        if (activityTaskId != null && activityTaskId > 0) {
            userTask.setActivityTaskId(activityTaskId);
            ActivityTask activityTask = this.activityTaskService.getById(activityTaskId);
            if (activityTask != null) {
                userTask.setActivityDefId(activityTask.getActivityDefId());
            }
        }
        if (userActivityId != null && userActivityId > 0) {
            userTask.setUserActivityId(userActivityId);
        }
        return userTask;
    }

    @Override
    public UserTask insertUserTaskByRedPackTask(Long mid, Long shopId, Long aid, Long userId, Integer status, Integer type, String keyWords, Long goodsId, String orderId, Long activityTaskId, Long userActivityId, BigDecimal rpAmount) {
        UserTask userTask = new UserTask();
        this.fromUserTask(userTask, mid, shopId, aid, userId, status, type, keyWords, goodsId, orderId, activityTaskId, userActivityId);
        userTask.setStartTime(new Date());
        userTask.setRpAmount(rpAmount);
        int rows = userTaskMapper.insertSelective(userTask);
        if (rows == 0) {
            return null;
        }
        return userTask;
    }

    @Override
    public UserTask insertUserTaskByActivityDef(Long mid, Long shopId, Long userId, Integer status, Integer type, String keyWords, Long goodsId, Long activityDefId, Long userActivityId, BigDecimal rpAmount) {
        UserTask userTask = new UserTask();
        userTask.setMerchantId(mid);
        userTask.setShopId(shopId);
        userTask.setUserId(userId);
        userTask.setActivityId(0L);
        userTask.setType(type);
        userTask.setStatus(status);
        userTask.setRpAmount(rpAmount);
        userTask.setKeyWords(keyWords);
        userTask.setActivityDefId(activityDefId);
        userTask.setGoodsId(goodsId);
        if (userActivityId != null && userActivityId > 0) {
            userTask.setUserActivityId(userActivityId);
        }
        userTask.setCtime(new Date());
        userTask.setUtime(new Date());
        userTask.setStartTime(new Date());
        int rows = userTaskMapper.insertSelective(userTask);
        if (rows == 0) {
            return null;
        }
        return userTask;
    }

    @Override
    public int updateByUidAndAid(UserTask record) {

        return userTaskMapper.updatePicsByUidAndAid(record);
    }

    @Override
    public List<UserTask> selectByOrderId(String orderId) {
        return userTaskMapper.selectByOrderId(orderId);
    }

    @Override
    public int countUserTask(Map<String, Object> map) {
        return this.userTaskMapper.countUserTask(map);
    }

    @Override
    public List<UserTask> selectByUidAndAid(Long uid, Long aid) {
        return userTaskMapper.selectByUidAndAid(uid, aid);
    }

    @Override
    public BigDecimal sumTaskAmount(Map<String, Object> map) {
        return this.userTaskMapper.sumUserTaskAmount(map);
    }

    /***
     * 获取用户任务的状态 状态 -1: 中奖任务过期 0：未提交  1待审核，-2：审核未通过，2：审核通过/未发奖， 3: 发奖成功
     * @param userTask
     * @return 领奖失效:-1 领奖成功:3 领奖中:1
     */
    @Override
    public int getUserTaskStatus(UserTask userTask) {
        if (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {//领奖失效
            return userTask.getStatus();
        } else if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {//领奖成功
            return userTask.getStatus();
        } else {
            return 1;//领奖中
        }
    }

    @Override
    public List<UserTask> queryAwardTask(Long merchantID, Long shopID, Long userID, Long activityID) {
        Map<String, Object> userTaskParams = new HashMap<String, Object>();
        userTaskParams.put("merchantId", merchantID);
        userTaskParams.put("shopId", shopID);
        if (userID != null) {
            userTaskParams.put("userId", userID);
        }
        userTaskParams.put("activityId", activityID);
        List<Integer> planAwardTypes = Arrays.asList(
                ActivityTypeEnum.MCH_PLAN.getCode(),
                ActivityTypeEnum.ORDER_BACK_START.getCode(),
                ActivityTypeEnum.ORDER_BACK_JOIN.getCode(),
                ActivityTypeEnum.NEW_MAN_START.getCode(),
                ActivityTypeEnum.NEW_MAN_JOIN.getCode());
        userTaskParams.put("types", planAwardTypes);
        List<UserTask> userTasks = this.query(userTaskParams);
        return userTasks;
    }

    @Override
    public Integer batchInsertUserTasks(List<UserTask> userTasks) {
        return userTaskMapper.batchInsertUserTasks(userTasks);
    }

    @Override
    public int countUserTaskByCtimePlus(Map map) {
        return userTaskMapper.countUserTaskByCtimePlus(map);

    }

    @Override
    public List<UserTask> listPageByParams(Map<String, Object> param) {
        return userTaskMapper.listPageByParams(param);
    }

    @Override
    public Integer updateStockFlag(Map ids) {

        Integer integer = userTaskMapper.updateStockFlagByIdArr(ids);
        return integer;
    }

    /**
     * 更新时间倒序，查询最后修改的一条记录
     *
     * @param params
     * @return
     */
    @Override
    public UserTask selectOne(Map<String, Object> params) {
        Map<String, Object> finalParams = new HashMap<>();
        if (!CollectionUtils.isEmpty(params)) {
            for (Map.Entry entry : params.entrySet()) {
                finalParams.put(StringUtils.camelToUnderline(entry.getKey().toString()), entry.getValue());
            }
        }
        QueryWrapper<UserTask> taskWrapper = new QueryWrapper<>();
        taskWrapper = taskWrapper.allEq(finalParams);
        taskWrapper = taskWrapper.orderByDesc("utime");
        return userTaskMapper.selectOne(taskWrapper);
    }

    @Override
    public Map<String, Object> queryActivityDefUserAwardInfo(List<Long> userIdsAll, List<Long> activityIdList) {

        List<Integer> typeList = new ArrayList<>();
        typeList.add(UserTaskTypeEnum.MCH_PLAN.getCode());
        typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
        typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
        typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
        typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
        typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
        QueryWrapper<UserTask> utWrapper = new QueryWrapper<>();
        utWrapper.in("activity_id", activityIdList);
        utWrapper.in("user_id", userIdsAll);
        utWrapper.in("type", typeList);
        List<UserTask> userTasks = this.list(utWrapper);
        if (CollectionUtils.isEmpty(userTasks)) {
            return null;
        }

        // 获取所有中奖用户
        List<Long> userIdsWinner = new ArrayList<>();
        // 获取所有中奖任务过期的用户
        List<Long> userIdsExpire = new ArrayList<>();
        // 获取所有返现成功用户
        Map<Long, BigDecimal> cashbackUserMap = new HashMap<>();

        // 获取所有中奖用户的集合
        for (UserTask userTask : userTasks) {
            if (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                userIdsExpire.add(userTask.getUserId());
            } else {
                userIdsWinner.add(userTask.getUserId());
            }
            if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                cashbackUserMap.put(userTask.getUserId(), userTask.getRpAmount());
            }
        }

        // 返回数据
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("winnerUserIdList", userIdsWinner);
        resultVO.put("expireUserIdList", userIdsExpire);
        resultVO.put("cashbackUserMap", cashbackUserMap);
        return resultVO;
    }

    @Override
    public UserTask findByGoodsId(Long goodsId) {
        return userTaskMapper.findByGoodsId(goodsId);
    }

    @Override
    public List<UserTask> listOngoingUserTask(List<Long> activityTaskIdList) {
        List<Integer> statusList = new ArrayList<>();
        statusList.add(UserTaskStatusEnum.INIT.getCode());
        statusList.add(UserTaskStatusEnum.AUDIT_WAIT.getCode());
        statusList.add(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
        statusList.add(UserTaskStatusEnum.AUDIT_ERROR.getCode());
        QueryWrapper<UserTask> utWrapper = new QueryWrapper<>();
        utWrapper.lambda()
                .in(UserTask::getActivityTaskId, activityTaskIdList)
                .in(UserTask::getStatus, statusList);
        return this.list(utWrapper);
    }

    @Override
    public int queryTaskFinishedCount(List<Long> activityIdList) {
        QueryWrapper<UserTask> utWrapper = new QueryWrapper<>();
        utWrapper.lambda()
                .in(UserTask::getActivityId, activityIdList)
                .eq(UserTask::getStatus, UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        List<UserTask> userTaskList = this.list(utWrapper);
        return userTaskList.size();
    }

    @Override
    public UserTask findByUserIdActivityId(Long activityId, Long userId) {
        Map m = new HashMap();
        m.put("activityId", activityId);
        m.put("userId", userId);

        return userTaskMapper.findByUserIdActivityId(m);
    }

    @Override
    public Long sumUserTask(Long shopId, Long userId) {
        Map m = new HashMap(2);
        m.put("userId", userId);
        m.put("shopId", shopId);
        return userTaskMapper.sumUserTask(m);
    }

    @Override
    public int countUserTaskShopAndPlatform(Map<String, Object> userTaskParams) {
        return userTaskMapper.countUserTaskShopAndPlatform(userTaskParams);
    }

    @Override
    public List<UserTask> queryShopAndPlatform(Map<String, Object> userTaskParams) {
        return userTaskMapper.queryShopAndPlatform(userTaskParams);
    }
}
