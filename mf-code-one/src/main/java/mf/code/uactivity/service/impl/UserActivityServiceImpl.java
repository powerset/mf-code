package mf.code.uactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.uactivity.repo.dao.UserActivityMapper;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.uactivity.service.impl;
 * Description:
 *
 * @author 百川
 * @date 2018/10/26 20:17
 */
@Service
public class UserActivityServiceImpl extends ServiceImpl<UserActivityMapper, UserActivity> implements UserActivityService {
    @Autowired
    private UserActivityMapper userActivityMapper;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityTaskService activityTaskService;

    @Override
    public int updateByPrimaryKey(UserActivity userActivity) {
        int rows = userActivityMapper.updateByPrimaryKeySelective(userActivity);
        return rows;
    }

    @Override
    public List<UserActivity> pageListUserActivity(Map<String, Object> mapParams) {
        List<UserActivity> userActivitys = this.userActivityMapper.listPageByParams(mapParams);
        return userActivitys;
    }

    /**
     * 用户参与活动时，插入一条用户活动记录，状态为0:没有中奖资格
     *
     * @param uid    参与活动的用户id
     * @param mid    商户id
     * @param shopId 淘宝店铺id
     * @param aid    活动id
     * @param type   活动类型
     * @return UserActivity 用户活动记录对象
     */
    @Override
    public UserActivity insertUserActivity(Long uid, Long mid, Long shopId, Long aid, Integer type, Integer status) {
        UserActivity userActivity = new UserActivity();
        this.fromUserActivity(userActivity, uid, mid, shopId, aid, type, status, null);
        int rows = userActivityMapper.insertSelective(userActivity);
        if (rows == 0) {
            return null;
        }
        return userActivity;
    }

    private UserActivity fromUserActivity(UserActivity userActivity, Long uid, Long mid, Long shopId, Long aid, Integer type, Integer status, Long activityTaskId) {
        Activity activity = this.activityService.findById(aid);
        if (activity != null) {
            if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
                userActivity.setActivityDefId(activity.getActivityDefId());
            }
        }
        if (activityTaskId != null && activityTaskId > 0) {
            ActivityTask activityTask = this.activityTaskService.getById(activityTaskId);
            if (activityTask != null) {
                userActivity.setActivityDefId(activityTask.getActivityDefId());
            }
        }
        userActivity.setUserId(uid);
        userActivity.setMerchantId(mid);
        userActivity.setShopId(shopId);
        userActivity.setActivityId(aid);
        userActivity.setType(type);
        userActivity.setStatus(status);
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        if (activityTaskId != null && activityTaskId > 0) {
            userActivity.setActivityTaskId(activityTaskId);
        }
        return userActivity;
    }

    @Override
    public UserActivity insertUserActivityByRedPack(Long uid, Long mid, Long shopId, Long aid, Integer type, Integer status, Long activityTaskId) {
        UserActivity userActivity = new UserActivity();
        this.fromUserActivity(userActivity, uid, mid, shopId, aid, type, status, activityTaskId);
        int rows = userActivityMapper.insertSelective(userActivity);
        if (rows == 0) {
            return null;
        }
        return userActivity;
    }

    @Override
    public UserActivity insertUserActivityByActivityDef(Long mid, Long shopId, Long uid, Long activityDefId, Integer type, Integer status) {
        UserActivity userActivity = new UserActivity();
        userActivity.setMerchantId(mid);
        userActivity.setShopId(shopId);
        userActivity.setUserId(uid);
        userActivity.setActivityDefId(activityDefId);
        userActivity.setActivityId(0L);
        userActivity.setType(type);
        userActivity.setStatus(status);
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        int rows = userActivityMapper.insertSelective(userActivity);
        if (rows == 0) {
            return null;
        }
        return userActivity;
    }

    /**
     * 用户参与活动时，插入一条用户活动记录，状态为0:没有中奖资格
     *
     * @param userActivity 参与活动的用户id
     * @return UserActivity 用户活动记录对象
     */
    @Override
    public Integer insertUserActivity(UserActivity userActivity) {
        return userActivityMapper.insertSelective(userActivity);
    }

    @Override
    public int updateWinnerStatusByUserIdAndActivityId(Long userId, Long aid, Integer status) {
        UserActivity userActivity = new UserActivity();
        userActivity.setUserId(userId);
        userActivity.setActivityId(aid);
        userActivity.setStatus(status);
        userActivity.setUtime(new Date());
        int rows = userActivityMapper.updateWinnerStatusByUserIdAndActivityId(userActivity);
        return rows;
    }

    @Override
    public int batchUpdateUserActivitiesByUserIdsAndActivityId(List<UserActivity> userActivities) {
        return userActivityMapper.batchUpdateUserActivitiesByUserIdsAndActivityId(userActivities);
    }

    @Override
    public int batchUpdateCouponUserStatus(List<Long> userIds, Long aid, Long couponId, Date couponStartTime, Date couponEndTime) {
        return userActivityMapper.batchUpdateCouponUserStatus(userIds, aid, couponId, couponStartTime, couponEndTime, new Date());
    }

    @Override
    public List<UserActivity> findUserActivityByShopIdAndUserId(Long shopId, Long userId) {
        return userActivityMapper.findUserActivityByShopIdAndUserId(shopId, userId);
    }

    @Override
    public int countUserActivity(Map<String, Object> mapParams) {
        return this.userActivityMapper.countUserActivity(mapParams);
    }

    @Override
    public int batchUpdateTaskCollectCartUserStatus(List<Long> userIds, Long aid) {
        return userActivityMapper.batchUpdateTaskCollectCartUserStatus(userIds, aid, new Date());
    }

    @Override
    public int updateStatusByUserIdAndActivityId(Long userId, Long activityId, Integer status) {
        return userActivityMapper.updateStatusByUserIdAndActivityId(userId, activityId, status, new Date());
    }

    @Override
    public UserActivity findByActivityIdAndUserId(Long activityId, Long userId) {
        Map map = new HashMap();
        map.put("activityId", activityId);
        map.put("userId", userId);

        UserActivity userActivity = userActivityMapper.findByActivityIdAndUserId(map);
        return userActivity;
    }

    @Override
    public UserActivity saveFromActivity(Long userId, Activity activity, boolean bool) {
        UserActivity userActivity = new UserActivity();
        userActivity.setUserId(userId);
        userActivity.setMerchantId(activity.getMerchantId());
        userActivity.setShopId(activity.getShopId());
        userActivity.setActivityId(activity.getId());
        userActivity.setActivityDefId(activity.getActivityDefId());
        if (activity.getType() == ActivityTypeEnum.LUCK_WHEEL.getCode()) {
            return null;
        }
        if (activity.getType() == ActivityTypeEnum.MCH_PLAN.getCode()) {
            userActivity.setType(UserActivityTypeEnum.MCH_PLAN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode()
                && bool) {
            userActivity.setType(UserActivityTypeEnum.ORDER_BACK_START.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode()
                && !bool) {
            userActivity.setType(UserActivityTypeEnum.ORDER_BACK_JOIN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_JOIN.getCode()) {
            userActivity.setType(UserActivityTypeEnum.ORDER_BACK_JOIN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                && bool) {
            userActivity.setType(UserActivityTypeEnum.NEW_MAN_START.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                && !bool) {
            userActivity.setType(UserActivityTypeEnum.NEW_MAN_JOIN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_JOIN.getCode()) {
            userActivity.setType(UserActivityTypeEnum.NEW_MAN_JOIN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ORDER_REDPACK.getCode()) {
            return null;
        }
        if (activity.getType() == ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
            return null;
        }
        if ((activity.getType() == ActivityTypeEnum.ASSIST.getCode())
                && bool) {
            userActivity.setType(UserActivityTypeEnum.ASSIST.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ASSIST.getCode()
                && !bool) {
            userActivity.setType(UserActivityTypeEnum.ASSIST_JOIN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()
                && bool) {
            userActivity.setType(UserActivityTypeEnum.OPEN_RED_PACKET_START.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()
                && !bool) {
            userActivity.setType(UserActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()
                && bool) {
            userActivity.setType(UserActivityTypeEnum.ORDER_BACK_START_V2.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()
                && !bool) {
            userActivity.setType(UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()
                && bool) {
            userActivity.setType(UserActivityTypeEnum.ASSIST_V2.getCode());
        }
        if (activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()
                && !bool) {
            userActivity.setType(UserActivityTypeEnum.ASSIST_JOIN_V2.getCode());
        }
        userActivity.setGoodsId(activity.getGoodsId());
        userActivity.setStatus(UserActivityStatusEnum.NO_WIN.getCode());
        userActivity.setMerchantShopCouponId(0L);
        userActivity.setUserCouponId(0L);
        Date now = new Date();
        userActivity.setCtime(now);
        userActivity.setUtime(now);
        boolean isSuccess = this.save(userActivity);
        if (!isSuccess) {
            return null;
        }
        return userActivity;
    }

    @Override
    public UserActivity findByGoodsId(Long goodsId) {
        return userActivityMapper.findByGoodsId(goodsId);
    }

    @Override
    public UserActivity selectById(Long userActivityId) {
        return this.userActivityMapper.selectByPrimaryKey(userActivityId);
    }

    @Override
    public boolean update2WinnerFromActivity(Activity activity) {
        QueryWrapper<UserActivity> uaWrapper = new QueryWrapper<>();
        uaWrapper.lambda()
                .eq(UserActivity::getUserId, activity.getUserId())
                .eq(UserActivity::getActivityId, activity.getId())
                .eq(UserActivity::getStatus, UserActivityStatusEnum.NO_WIN.getCode());
        UserActivity userActivity = this.getOne(uaWrapper);
        userActivity.setStatus(UserActivityStatusEnum.WINNER.getCode());
        userActivity.setUtime(new Date());
        return this.updateById(userActivity);
    }

    @Override
    public Long sumUserJoinActivity(Long shopId, Long userId) {
        Map m = new HashMap(2);
        m.put("shopId", shopId);
        m.put("userId", userId);

        return userActivityMapper.sumUserJoinActivity(m);
    }
    /**
     * 查询一个店铺下,某活动类型的参与人数
     * @param param
     * @return
     */
    @Override
    public Long countUserAll(Map<String, Object> param) {
        return userActivityMapper.countUserAll(param);
    }

    @Override
    public List<UserActivity> pageListUserActivityShopAndPlatform(Map<String, Object> userActivityParams) {
        return this.userActivityMapper.pageListUserActivityShopAndPlatform(userActivityParams);
    }

    @Override
    public int countUserActivityShopAndPlatform(Map<String, Object> userActivityParams) {
        return this.userActivityMapper.countUserActivityShopAndPlatform(userActivityParams);
    }


    @Override
    public Map<String, Object> queryActivityDefUserJoinInfo(Long activityDefId, List<User> robots) {

        List<Long> robotIds = new ArrayList<>();
        for (User user : robots) {
            robotIds.add(user.getId());
        }

        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.eq("activity_def_id", activityDefId);
        List<Activity> activities = activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            return null;
        }

        // 获取对应活动id的集合
        List<Long> activityIdList = new ArrayList<>();
        for (Activity activity : activities) {
            activityIdList.add(activity.getId());
        }

        List<Integer> statusList = new ArrayList<>();
        statusList.add(UserActivityStatusEnum.AWARD_EXPIRE.getCode());
        statusList.add(UserActivityStatusEnum.NO_WIN.getCode());
        statusList.add(UserActivityStatusEnum.WINNER.getCode());
        statusList.add(UserActivityStatusEnum.COUPON.getCode());

        // 通过activityIdList查询 user_activity表 取得所有用户id
        QueryWrapper<UserActivity> uaWrapper = new QueryWrapper<>();
        uaWrapper.in("activity_id", activityIdList);
        uaWrapper.in("status", statusList);
        List<UserActivity> userActivities = this.list(uaWrapper);
        if (CollectionUtils.isEmpty(userActivities)) {
            return null;
        }

        // 所有用户id的集合
        List<Long> userIdsAll = new ArrayList<>();
        // 获取所有优惠券用户的集合
        List<Long> userIdsCoupon = new ArrayList<>();

        for (UserActivity userActivity : userActivities) {
            Long uid = userActivity.getUserId();
            if (robotIds.contains(uid)) {
                continue;
            }
            userIdsAll.add(uid);
            if (userActivity.getStatus() == UserActivityStatusEnum.COUPON.getCode()) {
                userIdsCoupon.add(uid);
            }
        }

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("allUserIdList", userIdsAll);
        resultVO.put("couponUserIdList", userIdsCoupon);

        // 通过所有参与用户userid的查询 user_task表 获取信息
        Map<String, Object> utaskResult = userTaskService.queryActivityDefUserAwardInfo(userIdsAll, activityIdList);
        if (null == utaskResult) {
            return resultVO;
        }
        resultVO.putAll(utaskResult);

        return resultVO;
    }
}
