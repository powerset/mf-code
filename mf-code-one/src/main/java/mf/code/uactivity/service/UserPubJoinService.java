package mf.code.uactivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import lombok.NonNull;
import mf.code.activity.repo.po.Activity;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.user.repo.po.User;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.uactivity.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-08 11:21
 */
public interface UserPubJoinService extends IService<UserPubJoin> {
    UserPubJoin insertUserPubJoin(Long aid, Long pubUid, User user, Integer type);

    /**
     * 通过uid 和 aid 统计 数据数量
     * @param uid
     * @param aid
     * @return
     */
    List<UserPubJoin> selectBySubUidAndAid(Long uid, Long aid);

    int updateActivityJoinBySubUidAndAid(Long subUid, Long aid);

    List<UserPubJoin> listByUidAidStatusLimit(Long userId, Long activityId, Integer status, int offset, int size);

    int updateByPrimaryKeySelective(UserPubJoin userPubJoin);

    UserPubJoin selectByUserAndSubUidAndAid(Long pubUid, Long uid, Long aid);

    SimpleResponse userPubJoin(Long pubUid, Long aid, User user, Integer type);

    SimpleResponse userPubJoin(Long pubUid, Activity activity, User user);

    List<UserPubJoin> listByUidAidStatus(Long userId, Long activityId, int status);

    /**
     * 根据条件参数 查询总数
     * @param param 查询条件
     * @return 总条数
     */
    int countByParams(Map<String, Object> param);

    /**
     * 通过 条件参数 可 分页查询 数据
     * @param param 查询条件参数
     * @return list对象
     */
    List<UserPubJoin> listPageByParams(Map<String, Object> param);

    /**
     * 更新邀请成功状态
     * @param pubUserId
     * @param userId
     * @param activityId
     */
	void updatePubJoinSuccess(Long pubUserId, Long userId, Long activityId);

    void userPubJoin4Checkpoints(User user, User pubUser, Long platformUserId);

    void bePlatformMiner(User user, User platformUser);

    /***
     * 根据条件获取下级用户所有的缴税汇总金额
     * @param param
     * @return
     */
    BigDecimal sumByTotalScottare(Map<String, Object> param);

    /**
     * 根据主键更新分配佣金
     * @param id 主键
     * @param commission 自己获得佣金
     * @param tax 缴纳佣金
     * @return 更新条数
     */
    Integer updatePubJoinCommission(@NonNull Long id, @NonNull BigDecimal commission, @NonNull BigDecimal tax);

    /**
     * 计算粉丝数
     * @param shopId
     * @param parentId
     * @return
     */
    Long sumFans(Long shopId, Long parentId);

    UserPubJoin findParent(Long shopId, Long userId);

    /**
     * 查询我的所有矿工
     * @param params
     * @return
     */
    List<UserPubJoin> findAllMyMiner(Map<String, Object> params);

    /**
     * 统计所有矿工数
     * @param params
     * @return
     */
    Integer countAllMyMiner(Map<String, Object> params);
}
