package mf.code.uactivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.Activity;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.user.repo.po.User;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface UserActivityService extends IService<UserActivity> {
    /**
     * 通过主键更新
     *
     * @param userActivity 用户
     * @return
     */
    int updateByPrimaryKey(UserActivity userActivity);

    /***
     * 可分页查询
     * @param mapParams
     * @return
     */
    List<UserActivity> pageListUserActivity(Map<String, Object> mapParams);

    /**
     * 用户参与活动时，插入一条用户活动记录，状态为0:没有中奖资格
     *
     * @param uid    参与活动的用户id
     * @param mid    商户id
     * @param shopId 淘宝店铺id
     * @param aid    活动id
     * @param type   活动类型
     * @return UserActivity 用户活动记录对象
     */
    UserActivity insertUserActivity(Long uid, Long mid, Long shopId, Long aid, Integer type, Integer status);

    UserActivity insertUserActivityByRedPack(Long uid, Long mid, Long shopId, Long aid, Integer type, Integer status, Long activityTaskId);

    UserActivity insertUserActivityByActivityDef(Long mid, Long shopId, Long uid, Long activityDefId, Integer type, Integer status);

    /**
     * 用户参与活动时，插入一条用户活动记录，状态为0:没有中奖资格
     *
     * @param userActivity 参与活动的用户id
     * @return UserActivity 用户活动记录对象
     */
    Integer insertUserActivity(UserActivity userActivity);

    int updateWinnerStatusByUserIdAndActivityId(Long userId, Long aid, Integer status);

    int batchUpdateUserActivitiesByUserIdsAndActivityId(List<UserActivity> userActivities);

    int batchUpdateCouponUserStatus(List<Long> userIds, Long aid, Long couponId, Date couponStartTime, Date couponEndTime);

    List<UserActivity> findUserActivityByShopIdAndUserId(Long shopId, Long userId);

    int countUserActivity(Map<String, Object> mapParams);

    int batchUpdateTaskCollectCartUserStatus(List<Long> userIds, Long aid);

    int updateStatusByUserIdAndActivityId(Long userId, Long activityId, Integer status);

    UserActivity findByActivityIdAndUserId(Long activityId, Long userId);

    Map<String, Object> queryActivityDefUserJoinInfo(Long activityDefId, List<User> robots);

    /**
     * @param userId
     * @param activity
     * @param bool bool说明：当数据从activity表中读取到user_activity表时，用来区别userId是以发起者的类型处理 还是 参与者的类型处理
     *             若userId是发起者 则设置true
     *             若userId是参与者 则设置false
     * @return
     */
    UserActivity saveFromActivity(Long userId, Activity activity, boolean bool);

    UserActivity findByGoodsId(Long goodsId);

    UserActivity selectById(Long userActivityId);

    boolean update2WinnerFromActivity(Activity activity);

    /**
     * 该用户在某店铺中参数的活动数量统计
     * @param shopId
     * @param userId
     * @return
     */
    Long sumUserJoinActivity(Long shopId, Long userId);

    /**
     * 查询一个店铺下,某活动类型的参与人数
     * @param param
     * @return
     */
    Long countUserAll(Map<String, Object> param);

    /**
     * 分页获取参与记录（分店铺和全平台类型）
     * @param userActivityParams
     * @return
     */
    List<UserActivity> pageListUserActivityShopAndPlatform(Map<String, Object> userActivityParams);

    int countUserActivityShopAndPlatform(Map<String, Object> userActivityParams);
}
