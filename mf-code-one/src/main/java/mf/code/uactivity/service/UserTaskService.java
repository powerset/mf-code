package mf.code.uactivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.uactivity.repo.po.UserTask;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.uactivity.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月30日 15:09
 */
public interface UserTaskService extends IService<UserTask> {

    int update(UserTask userTask);

    UserTask selectByPrimaryKey(Long id);

    List<UserTask> query(Map<String, Object> map);

    int insertUserTask(UserTask userTask);

    List<UserTask> selectByActivityId(Long aid);

    List<UserTask> listByAidAndStatus(Long aid, List<Integer> statusList);

    UserTask insertUserTask(Long mid, Long shopId, Long aid, Long userId, Integer status, Integer type, String keyWords, Long goodsId, String orderId);

    UserTask insertUserTaskByRedPackTask(Long mid, Long shopId, Long aid, Long userId, Integer status, Integer type, String keyWords, Long goodsId, String orderId, Long activityTaskId, Long userActivityId, BigDecimal rpAmount);

    UserTask insertUserTaskByActivityDef(Long mid, Long shopId, Long userId, Integer status, Integer type, String keyWords, Long goodsId, Long activityDefId, Long userActivityId, BigDecimal rpAmount);

    int updateByUidAndAid(UserTask record);

    List<UserTask> selectByOrderId(String orderId);

    int countUserTask(Map<String, Object> map);

    List<UserTask> selectByUidAndAid(Long uid, Long aid);

    BigDecimal sumTaskAmount(Map<String, Object> map);

    /***
     * 获取用户任务的状态
     * @param userTask
     * @return
     */
    int getUserTaskStatus(UserTask userTask);

    List<UserTask> queryAwardTask(Long merchantID, Long shopID, Long userID, Long activityID);

    Integer batchInsertUserTasks(List<UserTask> userTasks);

    int countUserTaskByCtimePlus(Map map);

    List<UserTask> listPageByParams(Map<String, Object> param);

    Integer updateStockFlag(Map ids);

    /**
     * 更新时间倒序，查询最后修改的一条记录
     *
     * @param params
     * @return
     */
    UserTask selectOne(Map<String, Object> params);

    Map<String, Object> queryActivityDefUserAwardInfo(List<Long> userIdsAll, List<Long> activityIdList);

    UserTask findByGoodsId(Long goodsId);

    /**
     * 获取activity_task_id的所有进行中的活动
     * 获取进行中的红包任务：用户任务状态为：0未提交，1审核中，-2审核失败，2审核通过
     *
     * @param activityTaskIdList 活动任务id集合
     * @return
     */
    List<UserTask> listOngoingUserTask(List<Long> activityTaskIdList);

    /**
     * 获取 活动id 下 所有完成任务的人数
     * 完成任务的状态为 3发奖成功
     *
     * @param activityIdList
     * @return
     */
    int queryTaskFinishedCount(List<Long> activityIdList);

    UserTask findByUserIdActivityId(Long activityId, Long userId);

    /**
     * @param shopId
     * @param userId
     * @return
     */
    Long sumUserTask(Long shopId, Long userId);

    int countUserTaskShopAndPlatform(Map<String, Object> userTaskParams);

    List<UserTask> queryShopAndPlatform(Map<String, Object> userTaskParams);
}
