package mf.code.user.service.impl;/**
 * create by qc on 2019/6/11 0011
 */

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserRepostory;
import org.springframework.stereotype.Service;

/**
 * @author gbf
 * 2019/6/11 0011、17:06
 */
@Service
@Slf4j
public class UserRepostoryImpl extends ServiceImpl<UserMapper, User> implements UserRepostory {
}
