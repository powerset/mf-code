package mf.code.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RobotUserUtil;
import mf.code.user.constant.UserConstant;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.po.User;
import mf.code.user.repo.redis.RedisKeyConstant;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public User findByOpenid(String openid) {
        return userMapper.selectByOpenId(openid);
    }

    @Override
    public User insertUser(User user) {
        int rows = userMapper.insertSelective(user);
        if (rows == 0) {
            return null;
        }
        return user;
    }

    @Override
    public User selectByPrimaryKey(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Override
    public List<User> query(Map<String, Object> map) {
        return this.userMapper.pageListUser(map);
    }

    @Override
    public int updateByPrimaryKeySelective(User userDB) {

        return userMapper.updateByPrimaryKeySelective(userDB);
    }

    @Override
    public SimpleResponse<Object> registerOrUpdateMobile(Long userId, String mobile) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        User user = new User();
        user.setId(userId);
        user.setMobile(mobile);
        user.setUtime(new Date());
        int rows = userMapper.updateByPrimaryKeySelective(user);
        if (rows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("user表出错，手机号更新失败");
            return response;
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public List<User> listPageByParams(List<Long> idList) {
        return userMapper.selectBatchIds(idList);
    }

    /**
     * 手机号计数
     *
     * @param mobile
     * @return
     */
    @Override
    public Integer countByMoblie(String mobile) {
        Map<String, Object> params = new HashMap<>();
        params.put("mobile", mobile);
        return userMapper.countbyParams(params);
    }

    @Override
    public List<User> listPageByParams(Map<String, Object> userParam) {
        return userMapper.pageListUser(userParam);
    }

    @Override
    public boolean robotUser(Long userID) {
        Map<String, Object> userParams = new HashMap<>();
        userParams.put("merchantId", 0);
        userParams.put("shopId", 0);
        userParams.put("grantStatus", -1);
        List<User> users = this.listPageByParams(userParams);
        if (users != null && users.size() > 0) {
            for (User user : users) {
                if (user.getId().equals(userID)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> robotUserInfo(Long userID) {
        Map<String, Object> map = new HashMap<>();
        if (this.robotUser(userID)) {
            Map<String, Object> robotMap = RobotUserUtil.getRobotUserAvatarUrlAndNickFromOss();
            map.put("avatarUrl", robotMap.get("url"));
            map.put("nickName", robotMap.get("name"));
        }
        return map;
    }

    @Override
    public SimpleResponse timeLimit(final Long merchantId, final Long shopId, final Long userId, final Long bizType, final Long bizId) {
        SimpleResponse response = new SimpleResponse();
        // todo
        return response;
    }

    @Override
    public SimpleResponse timeLimit(Long userId, String hasShared) {

        String dateKey = DateUtil.getYYMMDD();

        SimpleResponse response = new SimpleResponse();
        Map respMap = new HashMap(1);

        String timeRecordKey = RedisKeyConstant.USER_ATTENT_TIME_LIMIT + dateKey + ":" + userId;

        List<String> list = new ArrayList<>();
        list.add(RedisKeyConstant.SETTING_ATTENT_TIME_LIMIT);
        list.add(timeRecordKey);

        // 获取   1:设置的可参与次数  2: 改用户参与了几次
        List<String> redisResultList = stringRedisTemplate.opsForValue().multiGet(list);

        // 初始化可参与次数 : 默认 3
        Long timeLimit = 3L;
        Long timesJoin = 0L;

        if (redisResultList.size() == 2) {
            if (redisResultList.get(0) != null) {
                timeLimit = Long.parseLong(redisResultList.get(0));
            }
            if (redisResultList.get(1) != null) {
                timesJoin = Long.parseLong(redisResultList.get(1));
            }
        }

        //相应信息设置次数
        respMap.put(UserConstant.responseTimesKey, timesJoin);
        response.setData(respMap);

        //如果是已经达到了限制次数,就返回不能参与成功
        if (timesJoin >= timeLimit) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7000);
            return response;
        }
        // 如果是没用分享
        if (timesJoin == (timeLimit - 1) && !"shared".equals(hasShared)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7001);
            return response;
        }
        // 成功原子自增,返回
        timesJoin = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.USER_ATTENT_TIME_LIMIT + dateKey + ":" + userId, 1L);
        stringRedisTemplate.expire(RedisKeyConstant.USER_ATTENT_TIME_LIMIT + dateKey + ":" + userId, 1, TimeUnit.DAYS);
        //相应信息设置次数
        respMap.put(UserConstant.responseTimesKey, timesJoin);
        response.setData(respMap);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public List<User> listRobot() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(User::getMerchantId, 0)
                .eq(User::getShopId, 0)
                .eq(User::getGrantStatus, -1);
        return this.list(wrapper);
    }

    @Override
    public Long countHasPhone(Long shopId) {
        return userMapper.countHasPhone(shopId);
    }

    @Override
    public Long countAll(Long shopId) {
        return userMapper.countAll(shopId);
    }

    @Override
    public Long countActive(Long shopId, Date begin, Date end) {
        Map m = new HashMap(3);
        m.put("shopId", shopId);
        m.put("begin", begin);
        m.put("end", end);
        return userMapper.countActive(m);
    }

    @Override
    public List<Map> countNotActive(Long shopId, Date begin, Date end) {
        Map m = new HashMap(3);
        m.put("shopId", shopId);
        m.put("begin", begin);
        m.put("end", end);
        return userMapper.countNotActive(m);
    }

    @Override
    public Long sleeping(Long shopId, Date begin, Date end) {
        Map m = new HashMap(3);
        m.put("shopId", shopId);
        m.put("begin", begin);
        m.put("end", end);
        return userMapper.sleeping(m);
    }

    @Override
    public List<User> fansList(Map param) {
        return userMapper.fansList(param);
    }

    @Override
    public Integer incrFansTotal(Long id) {
        return userMapper.incrFansTotal(id);
    }

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public List<Long> queryFansInEachShop(List<Long> shopIds) {
        return userMapper.queryFansInEachShop(shopIds);
    }

    @Override
    public Map<Long, User> queryByUserIdsToMap(List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return new HashMap<>();
        }
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(User::getId, userIds);
        List<User> users = userMapper.selectList(wrapper);

        if (CollectionUtils.isEmpty(users)) {
            return new HashMap<>();
        }
        Map<Long, User> map = new HashMap<>();
        for (User user : users) {
            map.put(user.getId(), user);
        }
        return map;
    }
}
