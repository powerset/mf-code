package mf.code.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.user.repo.po.User;

/**
 * create by qc on 2019/6/11 0011
 */
public interface UserRepostory extends IService<User> {
}
