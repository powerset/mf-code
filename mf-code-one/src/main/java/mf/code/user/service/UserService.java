package mf.code.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.repo.po.User;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface UserService extends IService<User> {
    /**
     * 根据openId查询User信息
     *
     * @param openid
     * @return
     */
    User findByOpenid(String openid);

    /**
     * 插入数据
     *
     * @param user
     * @return
     */
    User insertUser(User user);

    User selectByPrimaryKey(Long userId);

    List<User> query(Map<String, Object> map);

    /**
     * 通过主键更新用户信息
     *
     * @param userDB
     * @return
     */
    int updateByPrimaryKeySelective(User userDB);

    /**
     * 注册或修改手机号
     *
     * @param userId 用户id
     * @param mobile 手机号
     * @return SimpleResponse
     */
    SimpleResponse<Object> registerOrUpdateMobile(Long userId, String mobile);

    List<User> listPageByParams(List<Long> idList);

    /**
     * 手机号计数
     *
     * @param mobile
     * @return
     */
    Integer countByMoblie(String mobile);

    List<User> listPageByParams(Map<String, Object> userParam);


    boolean robotUser(Long userID);

    Map<String, Object> robotUserInfo(Long userID);

    /**
     * 用户参与活动次数限制
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @param userId     用户id
     * @param bizType    参与的类型.当前只是参与的活动. 1:用户分享活动 2:.....
     * @param bizId      参与项目的id.如果是活动,就是活动id
     * @return
     */
    SimpleResponse timeLimit(Long merchantId, Long shopId, Long userId, Long bizType, Long bizId);

    /**
     * 用户参与活动次数限制
     */
    SimpleResponse timeLimit(Long userId, String hasShared);

    List<User> listRobot();

    Long countHasPhone(Long shopId);

    Long countAll(Long shopId);

    Long countActive(Long shopId, Date begin, Date end);

    List<Map> countNotActive(Long shopId, Date begin, Date end);

    Long sleeping(Long shopId, Date begin, Date end);

    List<User> fansList(Map param);

    Integer incrFansTotal(Long id);

    List<User> findAll();

    /**
     * 查询每个店铺的粉丝数
     */
    List<Long> queryFansInEachShop(List<Long> shopIds);

    Map<Long, User> queryByUserIdsToMap(List<Long> userIds);
}
