package mf.code.user.repo.redis;

/**
 * @Author yechen
 * @Date
 **/
public class RedisKeyConstant {

    /**
     * 用户提现key
     */
    public static final String USERPRESENTMONEY = "user:presentMoney:";//<uid>

    /**商户申请清算退款余额匹配**/
    public static final String MERCHANTORDER_REFUND = "merchant:merchantOrder:applyRefund:";//<activityDefId>
    /**是否结算清，可更新def表**/
    public static final String MERCHANTORDER_REFUND_END = "merchant:merchantOrder:applyRefundEnd:";//<activityDefId>


    /**
     * 用户参与次数限制key
     * USER_ATTENT_TIME_LIMIT:<20181201>:<userId>
     */
    public static final String USER_ATTENT_TIME_LIMIT = "user:attentTimeLimit:";
    /**
     * 参与次数限制
     */
    public static final String SETTING_ATTENT_TIME_LIMIT = "settings:attila:attentTimeLimit";

    public static final String USER_ATTACK_ASS_LIMIT = "user:times:attack:limit:";

}
