package mf.code.merchant.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.merchant.repo.po.MerchantOrder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.merchant.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月17日 17:59
 */
public interface MerchantOrderService extends IService<MerchantOrder> {

    /***
     * 填充订单基础数据
     * @param merchantId
     * @param shopId
     * @param customerJson
     * @param orderNo
     * @param orderName
     * @param status
     * @param totalFee
     * @param tradeId
     * @param remark
     * @param bizType
     * @param bizValue
     * @param type
     * @param payType
     * @param ipAddress
     * @param refundOrderId
     * @param reqJson
     * @return
     */
    MerchantOrder addMerchantOrderPo(Long merchantId,
                                     Long shopId,
                                     String shopName,
                                     String customerJson,
                                     String orderNo,
                                     String orderName,
                                     Integer status,
                                     BigDecimal totalFee,
                                     String tradeId,
                                     String remark,
                                     Integer mfTradeType,
                                     Integer bizType,
                                     Long bizValue,
                                     Integer type,
                                     Integer payType,
                                     String ipAddress,
                                     Long refundOrderId,
                                     String reqJson);


    /***
     * 根据订单编号查询订单
     * @param orderNo
     * @return
     */
    MerchantOrder queryByOrderNo(String orderNo);

    /***
     * 查询要退款的订单
     * @param bizValue 业务编号
     * @param bizType 业务类型 BizTypeEnum //1：计划内活动，2：新人活动，订单活动，3:轮盘抽奖，4:随机红包(扫码包裹)，5:红包任务(收藏加购)，6.广告',
     * @return
     */
    MerchantOrder queryRefundOrder(Long bizValue, int bizType);

    /**
     * 公众号列表活动订单活动
     *
     * @param bizValue
     * @return
     */
    List<MerchantOrder> queryForPublicFission(Long bizValue);

    List<MerchantOrder> queryPageByParams(Map<String, Object> params);

    int countByParams(Map<String, Object> params);
}
