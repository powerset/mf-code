package mf.code.merchant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.dao.MerchantOrderMapper;
import mf.code.merchant.repo.enums.*;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.merchant.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月17日 17:59
 */
@Service
public class MerchantOrderServiceImpl extends ServiceImpl<MerchantOrderMapper, MerchantOrder> implements MerchantOrderService {
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private MerchantOrderMapper merchantOrderMapper;

    /***
     * 填充基础数据
     * @param merchantId
     * @param shopId
     * @param customerJson
     * @param orderNo
     * @param orderName
     * @param status
     * @param totalFee
     * @param tradeId
     * @param remark
     * @param bizType
     * @param bizValue
     * @param type
     * @param payType
     * @param ipAddress
     * @param refundOrderId
     * @param reqJson
     * @return
     */
    @Override
    public MerchantOrder addMerchantOrderPo(Long merchantId,
                                            Long shopId,
                                            String shopName,
                                            String customerJson,
                                            String orderNo,
                                            String orderName,
                                            Integer status,
                                            BigDecimal totalFee,
                                            String tradeId,
                                            String remark,
                                            Integer mfTradeType,
                                            Integer bizType,
                                            Long bizValue,
                                            Integer type,
                                            Integer payType,
                                            String ipAddress,
                                            Long refundOrderId,
                                            String reqJson) {
        MerchantOrder merchantOrder = new MerchantOrder();
        merchantOrder.setMerchantId(merchantId);
        merchantOrder.setShopId(shopId);
        if (StringUtils.isNotBlank(shopName)) {
            merchantOrder.setShopName(shopName);
        } else {
            merchantOrder.setShopName("");
        }
        merchantOrder.setCustomerJson(customerJson);
        merchantOrder.setType(type);
        merchantOrder.setPayType(payType);
        merchantOrder.setOrderNo(orderNo);
        merchantOrder.setOrderName(orderName);
        merchantOrder.setPubAppId(wxpayProperty.getPubAppId());
        merchantOrder.setStatus(status);
        merchantOrder.setTotalFee(totalFee);
        merchantOrder.setTradeType(WxTradeTypeEnum.NATIVE.getCode());
        merchantOrder.setIpAddress(ipAddress);
        if (StringUtils.isNotBlank(tradeId)) {
            merchantOrder.setTradeId(tradeId);
        } else {
            merchantOrder.setTradeId("");
        }
        merchantOrder.setRemark(remark);
        if (mfTradeType != null) {
            merchantOrder.setMfTradeType(mfTradeType);
        } else {
            merchantOrder.setMfTradeType(MerchantOrderReq.trade_type_pay);
        }
        merchantOrder.setNotifyTime(new Date());
        merchantOrder.setPaymentTime(new Date());
        merchantOrder.setCtime(new Date());
        merchantOrder.setUtime(new Date());
        merchantOrder.setBizType(0);
        merchantOrder.setBizValue(0L);
        if (bizType != null && bizType > 0) {
            merchantOrder.setBizType(bizType);
        }
        if (bizValue != null && bizValue > 0) {
            merchantOrder.setBizValue(bizValue);
        }
        if (refundOrderId != null && refundOrderId > 0) {
            merchantOrder.setRefundOrderId(refundOrderId);
        } else {
            merchantOrder.setRefundOrderId(0L);
        }
        merchantOrder.setReqJson(reqJson);
        merchantOrder.setPayOpenId("");
        return merchantOrder;
    }

    /***
     * 根据订单号查询订单
     * @param orderNo
     * @return
     */
    @Override
    public MerchantOrder queryByOrderNo(String orderNo) {
        Map<String, Object> params = new HashMap<>();
        params.put("order_no", orderNo);
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.allEq(params);
        return this.getOne(wrapper);
    }

    /***
     * 查询要退款的订单
     * @param bizValue 业务编号
     * @param bizType 业务类型 BizTypeEnum //1：计划内活动，2：新人活动，订单活动，3:轮盘抽奖，4:随机红包(扫码包裹)，5:红包任务(收藏加购)，6.广告',
     * @return
     */
    @Override
    public MerchantOrder queryRefundOrder(Long bizValue, int bizType) {
        Map<String, Object> merchantOrderParams = new HashMap<String, Object>();
        merchantOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        merchantOrderParams.put("bizType", bizType);
        merchantOrderParams.put("bizValue", bizValue);

        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.allEq(merchantOrderParams);
        return this.getOne(wrapper);
    }

    /**
     * 公众号列表活动订单活动
     *
     * @param bizValue
     * @return
     */
    @Override
    public List<MerchantOrder> queryForPublicFission(Long bizValue) {
        QueryWrapper<MerchantOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(MerchantOrder::getBizType, BizTypeEnum.PUBLIC_BUY_COURSE.getCode())
                .eq(MerchantOrder::getBizValue, bizValue)
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getTradeType, WxTradeTypeEnum.JSAPI.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode());
        return this.list(queryWrapper);
    }

    /***
     * 根据条件查询信息
     *
     * @param params
     * @return
     */
    @Override
    public List<MerchantOrder> queryPageByParams(Map<String, Object> params) {
        return merchantOrderMapper.queryPageByParamsThisMonth(params);
    }

    /***
     * 根据条件获取条数
     *
     * @param params
     * @return
     */
    @Override
    public int countByParams(Map<String, Object> params) {
        return merchantOrderMapper.countByParamsThisMonth(params);
    }
}
