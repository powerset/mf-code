package mf.code.merchant.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.one.dto.GrantCouponDTO;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.one.dto.UserCouponDTO;
import mf.code.ucoupon.repo.po.UserCoupon;

import java.util.List;
import java.util.Map;

/**
 * mf.code.merchant.service
 * Description:
 *
 * @author: gel
 * @date: 2018-10-29 20:25
 */
public interface MerchantShopCouponService extends IService<MerchantShopCoupon> {

    /**
     * 创建优惠券
     *
     * @param merchantShopCoupon
     * @return
     */
    Integer createMerchantShopCoupon(MerchantShopCoupon merchantShopCoupon);

    /**
     * 更新优惠券
     *
     * @param merchantShopCoupon
     * @return
     */
    Integer updateMerchantShopCoupon(MerchantShopCoupon merchantShopCoupon);

    /**
     * 删除优惠券
     *
     * @param id
     * @return
     */
    Integer deleteMerchantShopCoupon(Long id);

    /**
     * 查询店铺优惠券
     *
     * @param id
     * @return
     */
    MerchantShopCoupon selectByPriKey(Long id);

    /**
     * 查询店铺优惠券
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    List<MerchantShopCoupon> selectByShopId(Long merchantId, Long shopId);

    /**
     * 分页查询优惠券
     *
     * @param params
     * @param page
     * @param size
     * @return
     */
    Map<String, Object> pageListMerchantShopCoupon(Map<String, Object> params, Integer page, Integer size);

    /***
     * 根据条件查询优惠券集合
     */
    List<MerchantShopCoupon> queryParams(Map<String, Object> map);

    /**
     * 根据主键查询优惠券
     *
     * @param id
     * @return
     */
    MerchantShopCoupon selectById(Long id);

    /**
     * 随机筛选一条优惠券
     *
     * @return
     */
    MerchantShopCoupon randomOneCoupom(Long merchantId, Long shopId);

    Map<String, Object> getMerchantShopCouponVOListByIds(List<Long> merchantCouponIdList);

    /**
     * 通过商品id查询优惠卷
     *
     * @param productId
     * @return
     */
    List<MerchantShopCouponDTO> selectByProductId(Long productId);

    /**
     * 发放优惠卷
     *
     * @param grantCouponDTO
     * @return
     */
    Map<Long, Long> grantCouponById(GrantCouponDTO grantCouponDTO);

    /**
     * 通过id查询优惠劵
     *
     * @param couponId
     * @return
     */
    UserCouponDTO findCouponById(Long couponId);

    /**
     *
     * @param userId
     * @param productId
     * @return
     */
    List<UserCouponDTO> findUserMerchantShopCouponByProductId(Long userId, Long productId);
}
