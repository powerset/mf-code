package mf.code.merchant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.PageUtil;
import mf.code.merchant.repo.dao.MerchantShopCouponMapper;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.one.constant.MerchantShopCouponStatusEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.GrantCouponDTO;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.one.dto.UserCouponDTO;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * mf.code.merchant.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2018-10-29 20:31
 */
@Service
@Slf4j
public class MerchantShopCouponServiceImpl extends ServiceImpl<MerchantShopCouponMapper, MerchantShopCoupon> implements MerchantShopCouponService {

    @Autowired
    private MerchantShopCouponMapper merchantShopCouponMapper;
    @Autowired
    private UserCouponService userCouponService;

    /**
     * 创建优惠券
     *
     * @param merchantShopCoupon
     * @return
     */
    @Override
    public Integer createMerchantShopCoupon(MerchantShopCoupon merchantShopCoupon) {
        merchantShopCoupon.setCtime(new Date());
        merchantShopCoupon.setUtime(new Date());
        return merchantShopCouponMapper.insertSelective(merchantShopCoupon);
    }

    /**
     * 更新优惠券
     *
     * @param merchantShopCoupon
     * @return
     */
    @Override
    public Integer updateMerchantShopCoupon(MerchantShopCoupon merchantShopCoupon) {
        merchantShopCoupon.setUtime(new Date());
        return merchantShopCouponMapper.updateByPrimaryKeySelective(merchantShopCoupon);
    }

    /**
     * 删除优惠券
     *
     * @param id
     * @return
     */
    @Override
    public Integer deleteMerchantShopCoupon(Long id) {
        MerchantShopCoupon merchantShopCoupon = new MerchantShopCoupon();
        merchantShopCoupon.setId(id);
        merchantShopCoupon.setDel(DelEnum.YES.getCode());
        merchantShopCoupon.setUtime(new Date());
        return merchantShopCouponMapper.updateByPrimaryKeySelective(merchantShopCoupon);
    }

    /**
     * 查询店铺优惠券
     *
     * @param id
     * @return
     */
    @Override
    public MerchantShopCoupon selectByPriKey(Long id) {
        return merchantShopCouponMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询店铺优惠券
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public List<MerchantShopCoupon> selectByShopId(Long merchantId, Long shopId) {
        Map<String, Object> params = new HashMap<>(4);
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("now", new Date());
        params.put("del", DelEnum.NO.getCode());
        return merchantShopCouponMapper.pageListCoupon(params);
    }

    /**
     * 分页查询优惠券
     *
     * @param params
     * @param page
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> pageListMerchantShopCoupon(Map<String, Object> params, Integer page, Integer size) {
        int count = merchantShopCouponMapper.countForPageList(params);
        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("page", page);
        result.put("size", size);
        if (count == 0) {
            result.put("list", new ArrayList<>());
            return result;
        }
        params.putAll(PageUtil.pageSizeTolimit(page, size, count));
        List<MerchantShopCoupon> couponList = merchantShopCouponMapper.pageListCoupon(params);
        List<Map<String, Object>> shopMapList = new ArrayList<>();
        for (int i = 0; i < couponList.size(); i++) {
            shopMapList.add(BeanMapUtil.beanToMapIgnore(couponList.get(i), "ctime", "utime", "del"));
        }
        result.put("list", shopMapList);
        return result;
    }

    @Override
    public List<MerchantShopCoupon> queryParams(Map<String, Object> map) {
        return this.merchantShopCouponMapper.pageListCoupon(map);
    }

    /**
     * 根据主键查询优惠券
     *
     * @param id
     * @return
     */
    @Override
    public MerchantShopCoupon selectById(Long id) {
        return merchantShopCouponMapper.selectByPrimaryKey(id);
    }

    /**
     * 随机筛选一条优惠券
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public MerchantShopCoupon randomOneCoupom(Long merchantId, Long shopId) {

        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        // 如果数量偏多，可以先查总数，随机，然后limit查询准确的一条
        // 数量少，可以直接查出来，再取一条
        int count = merchantShopCouponMapper.countForPageList(params);
        if (count == 0) {
            return null;
        }
        Random rand = new Random();
        int i = rand.nextInt(count);
        params.put("limit", 1);
        params.put("offset", i - 1);
        List<MerchantShopCoupon> merchantShopCouponList = merchantShopCouponMapper.pageListCoupon(params);
        return merchantShopCouponList.get(0);
    }

    @Override
    public Map<String, Object> getMerchantShopCouponVOListByIds(List<Long> merchantCouponIdList) {
        Map<String, Object> resultVO = new HashMap<>();
        QueryWrapper<MerchantShopCoupon> mscWrapper = new QueryWrapper<>();
        mscWrapper.lambda()
                .in(MerchantShopCoupon::getId, merchantCouponIdList);
        List<MerchantShopCoupon> merchantShopCoupons = this.list(mscWrapper);
        if (CollectionUtils.isEmpty(merchantCouponIdList)) {
            return resultVO;
        }
        List<Map<String, Object>> merchantShopCouponVOList = new ArrayList<>();
        for (MerchantShopCoupon merchantShopCoupon : merchantShopCoupons) {
            Map<String, Object> merchantShopCouponVO = new HashMap<>();
            merchantShopCouponVO.put("amount",merchantShopCoupon.getAmount());
            merchantShopCouponVO.put("displayStartTime", DateUtil.dateToString(merchantShopCoupon.getDisplayStartTime(), DateUtil.FORMAT_TWO));
            merchantShopCouponVO.put("desc",merchantShopCoupon.getDesc());
            merchantShopCouponVO.put("name",merchantShopCoupon.getName());
            merchantShopCouponVO.put("displayEndTime", DateUtil.dateToString(merchantShopCoupon.getDisplayEndTime(), DateUtil.FORMAT_TWO));
            merchantShopCouponVO.put("merchantShopCouponId",merchantShopCoupon.getId());
            merchantShopCouponVO.put("tpwd",merchantShopCoupon.getTpwd());
            merchantShopCouponVOList.add(merchantShopCouponVO);
        }

        resultVO.put("merchantShopCouponVOList", merchantShopCouponVOList);
        return resultVO;
    }

    @Override
    public List<MerchantShopCouponDTO> selectByProductId(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }
        List<MerchantShopCoupon> merchantShopCoupons = baseMapper.selectList(
                new QueryWrapper<MerchantShopCoupon>()
                    .lambda()
                    .eq(MerchantShopCoupon::getProductId, productId)
                .eq(MerchantShopCoupon::getCouponStatus, MerchantShopCouponStatusEnum.PUBLISH.getCode())
                .eq(MerchantShopCoupon::getDel, DelEnum.NO.getCode())
                .gt(MerchantShopCoupon::getDisplayEndTime, new Date())
        );

        if (CollectionUtils.isEmpty(merchantShopCoupons)) {
            return new ArrayList<>();
        }

        List<MerchantShopCouponDTO> merchantShopCouponDTOS = new ArrayList<>();
        for (MerchantShopCoupon merchantShopCoupon:merchantShopCoupons) {
            MerchantShopCouponDTO merchantShopCouponDTO = new MerchantShopCouponDTO();
            BeanUtils.copyProperties(merchantShopCoupon, merchantShopCouponDTO);
            merchantShopCouponDTO.setAmount(merchantShopCoupon.getAmount().setScale(2, BigDecimal.ROUND_DOWN));
            merchantShopCouponDTOS.add(merchantShopCouponDTO);
        }

        return merchantShopCouponDTOS;
    }

    @Override
    public Map<Long, Long> grantCouponById(GrantCouponDTO grantCouponDTO) {
        if (grantCouponDTO == null ||
                grantCouponDTO.getActivityId() == null ||
                grantCouponDTO.getCouponId() == null ||
                CollectionUtils.isEmpty(grantCouponDTO.getUserIds())) {
            log.error("参数有误");
            return null;
        }

        MerchantShopCoupon merchantShopCoupon = baseMapper.selectOne(
                new QueryWrapper<MerchantShopCoupon>()
                    .lambda()
                    .eq(MerchantShopCoupon::getId, grantCouponDTO.getCouponId())
                    .eq(MerchantShopCoupon::getDel, DelEnum.NO.getCode())
                    .eq(MerchantShopCoupon::getCouponStatus, MerchantShopCouponStatusEnum.PUBLISH.getCode())
        );

        if (merchantShopCoupon == null) {
            log.error("商户没有发行优惠卷");
            return null;
        }


        Map<Long, Long> result = new HashMap<>(grantCouponDTO.getUserIds().size());
        for (Long userId:grantCouponDTO.getUserIds()) {
            // 查询用户领卷数量
            int countCoupon = userCouponService.count(
                    new QueryWrapper<UserCoupon>()
                            .lambda()
                            .eq(UserCoupon::getUserId, userId)
                            .eq(UserCoupon::getType, UserCouponTypeEnum.JKMF_COUPON.getCode())
                            .eq(UserCoupon::getCouponId, grantCouponDTO.getCouponId())
            );

            if (countCoupon >= merchantShopCoupon.getLimitPerNum()) {
                continue;
            }

            UserCoupon userCoupon = new UserCoupon();
            userCoupon.setActivityId(grantCouponDTO.getActivityId());
            userCoupon.setShopId(merchantShopCoupon.getShopId());
            userCoupon.setMerchantId(merchantShopCoupon.getMerchantId());
            userCoupon.setActivityDefId(grantCouponDTO.getActivityDefId());
            userCoupon.setType(UserCouponTypeEnum.JKMF_COUPON.getCode());
            userCoupon.setAmount(merchantShopCoupon.getAmount());
            userCoupon.setCommission(BigDecimal.ZERO);
            userCoupon.setUserId(userId);
            userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
            userCoupon.setCouponId(grantCouponDTO.getCouponId());
            userCoupon.setAwardTime(new Date());
            userCoupon.setCtime(new Date());
            userCoupon.setUtime(new Date());
            boolean save = userCouponService.saveSelective(userCoupon);
            if (!save) {
                log.error("数据库入库异常");
                continue;
            }
            result.put(userId, userCoupon.getId());
        }

        return result;
    }

    @Override
    public UserCouponDTO findCouponById(Long couponId) {

        UserCoupon userCoupon = userCouponService.findById(couponId);
        UserCouponDTO userCouponDTO = new UserCouponDTO();
        BeanUtils.copyProperties(userCoupon, userCouponDTO);
        return userCouponDTO;
    }

    @Override
    public List<UserCouponDTO> findUserMerchantShopCouponByProductId(Long userId, Long productId) {
        if (productId == null || userId == null) {
            return new ArrayList<>();
        }
        List<MerchantShopCoupon> merchantShopCoupons = baseMapper.selectList(
                new QueryWrapper<MerchantShopCoupon>()
                        .lambda()
                        .eq(MerchantShopCoupon::getProductId, productId)
                        .eq(MerchantShopCoupon::getCouponStatus, MerchantShopCouponStatusEnum.PUBLISH.getCode())
                        .eq(MerchantShopCoupon::getDel, DelEnum.NO.getCode())
                        .gt(MerchantShopCoupon::getDisplayEndTime, new Date())
        );

        if (CollectionUtils.isEmpty(merchantShopCoupons)) {
            return new ArrayList<>();
        }

        List<Long> MerchantShopCouponIds = merchantShopCoupons
                .stream()
                .map(MerchantShopCoupon::getId)
                .collect(Collectors.toList());

        List<UserCoupon> userCoupons = userCouponService.list(
                new QueryWrapper<UserCoupon>()
                    .lambda()
                    .eq(UserCoupon::getUserId, userId)
                    .in(UserCoupon::getCouponId, MerchantShopCouponIds)
        );

        List<UserCouponDTO> userCouponDTOS = new ArrayList<>();
        for (UserCoupon userCoupon:userCoupons) {
            UserCouponDTO userCouponDTO = new UserCouponDTO();
            BeanUtils.copyProperties(userCoupon, userCouponDTO);
            userCouponDTO.setId(userCoupon.getCouponId());
            userCouponDTOS.add(userCouponDTO);
        }

        return userCouponDTOS;
    }
}
