//package mf.code.merchant.repo.enums;
//
///**
// * mf.code.upay.repo.enums
// *
// * @description:
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2018年11月09日 18:14
// */
//public enum BizTypeEnum {
//    //1：计划内活动，2：赠品活动，3:轮盘抽奖，4:订单抽奖活动，5:红包活动，6.广告,7.活动定义审核， 8.拆红包
//    PLAN_ACTIVITY(1, "计划内活动"),
//    NEWMAN_ORDER_ACTIVITY(2, "赠品活动"),
//    LUCKY_WHEEL(3, "幸运大转盘"),
//    REDPACKTASK(4, "免单抽奖活动"),
//    FAVTASK(5, "红包任务"),
//    AD(6, "广告"),
//    ACTIVITY_AUDIT(7, "活动定义审核"),
//    OPEN_RED_PACKET(8, "拆红包"),
//    FENDUODUO_PROXY(9, "粉多多代理"),
//    PURCHASE_PUBLIC(10, "购买集客魔方公有版"),
//    PURCHASE_PRIVATE(11, "购买集客魔方私有版"),
//    LEVELUP_PRIVATE(12, "升级集客魔方私有版"),
//    //14：收藏加购 15：好评晒图 16：回填订单
//    PUBLIC_BUY_COURSE(13, "购买千万级讲师讲解社交电商课程"),
//    FAVCART(14, "收藏加购"),
//    GOODCOMMENT(15, "好评晒图"),
//    FILLORDERBACK(16, "回填订单"),
//    SHOPPING(17, "商城用户购物，商户所得"),
//    NEWBIETASK(18, "新手任务"),
//    SHOPMANAGERTASK(19, "店长任务"),
//    /***
//     * 报销活动
//     */
//    FULL_REIMBURSEMENT(20, "报销活动"),
//    ;
//
//
//    private int code;
//    private String message;
//
//    BizTypeEnum(int code, String message) {
//        this.code = code;
//        this.message = message;
//    }
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//
//    public static Integer findByCode(Integer code) {
//        for (BizTypeEnum typeEnum : BizTypeEnum.values()) {
//            if (typeEnum.getCode() == code) {
//                return typeEnum.getCode();
//            }
//        }
//        return null;
//    }
//
//
//    public static String findByDesc(Integer code) {
//        for (BizTypeEnum typeEnum : BizTypeEnum.values()) {
//            if (typeEnum.getCode() == code) {
//                return typeEnum.getMessage();
//            }
//        }
//        return "";
//    }
//
//
//    public static boolean isCreateDefMerchantOrder(Integer bizType) {
//        boolean isDefMerchantOrder = bizType != null
//                && (bizType == BizTypeEnum.NEWMAN_ORDER_ACTIVITY.getCode()
//                || bizType == BizTypeEnum.LUCKY_WHEEL.getCode()
//                || bizType == BizTypeEnum.REDPACKTASK.getCode()
//                || bizType == BizTypeEnum.FAVTASK.getCode()
//                || bizType == BizTypeEnum.OPEN_RED_PACKET.getCode()
//                || bizType == BizTypeEnum.FAVCART.getCode()
//                || bizType == BizTypeEnum.GOODCOMMENT.getCode()
//                || bizType == BizTypeEnum.FILLORDERBACK.getCode()
//                || bizType == BizTypeEnum.NEWBIETASK.getCode()
//                || bizType == BizTypeEnum.SHOPMANAGERTASK.getCode()
//                || bizType == BizTypeEnum.FULL_REIMBURSEMENT.getCode());
//        if (isDefMerchantOrder) {
//            return true;
//        }
//        return false;
//    }
//}
