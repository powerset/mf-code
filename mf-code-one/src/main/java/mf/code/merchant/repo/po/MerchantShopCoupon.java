package mf.code.merchant.repo.po;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * merchant_shop_coupon
 * 商家优惠券表
 */
public class MerchantShopCoupon implements Serializable {
    /**
     * 商品店铺优惠券id
     */
    private Long id;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 优惠券名称
     */
    private String name;

    /**
     * 优惠券描述
     */
    @TableField("`desc`")
    private String desc;

    /**
     * 优惠券金额
     */
    private BigDecimal amount;

    /**
     * 淘口令
     */
    private String tpwd;

    /**
     * 优惠券展示时间，开始时间
     */
    private Date displayStartTime;

    /**
     * 优惠券展示时间，结束时间
     */
    private Date displayEndTime;

    /**
     * 优惠券有效期，天数
     */
    private Integer validDay;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 使用门槛
     */
    private BigDecimal limitAmount;

    /**
     * 优惠券类型：1淘宝优惠券，2商品优惠券
     */
    @TableField("`type`")
    private Integer type;

    /**
     * 发行总量
     */
    private Integer stockDef;

    /**
     * 已发发行量
     */
    private Integer stock;

    /**
     * 每人限领取张数
     */
    private Integer limitPerNum;

    /**
     * 优惠券状态: 1:发行中，2已结束
     */
    private Integer couponStatus;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * merchant_shop_coupon
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商品店铺优惠券id
     * @return id 商品店铺优惠券id
     */
    public Long getId() {
        return id;
    }

    /**
     * 商品店铺优惠券id
     * @param id 商品店铺优惠券id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 优惠券名称
     * @return name 优惠券名称
     */
    public String getName() {
        return name;
    }

    /**
     * 优惠券名称
     * @param name 优惠券名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 优惠券描述
     * @return desc 优惠券描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 优惠券描述
     * @param desc 优惠券描述
     */
    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }

    /**
     * 优惠券金额
     * @return amount 优惠券金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 优惠券金额
     * @param amount 优惠券金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 淘口令
     * @return tpwd 淘口令
     */
    public String getTpwd() {
        return tpwd;
    }

    /**
     * 淘口令
     * @param tpwd 淘口令
     */
    public void setTpwd(String tpwd) {
        this.tpwd = tpwd == null ? null : tpwd.trim();
    }

    /**
     * 优惠券展示时间，开始时间
     * @return display_start_time 优惠券展示时间，开始时间
     */
    public Date getDisplayStartTime() {
        return displayStartTime;
    }

    /**
     * 优惠券展示时间，开始时间
     * @param displayStartTime 优惠券展示时间，开始时间
     */
    public void setDisplayStartTime(Date displayStartTime) {
        this.displayStartTime = displayStartTime;
    }

    /**
     * 优惠券展示时间，结束时间
     * @return display_end_time 优惠券展示时间，结束时间
     */
    public Date getDisplayEndTime() {
        return displayEndTime;
    }

    /**
     * 优惠券展示时间，结束时间
     * @param displayEndTime 优惠券展示时间，结束时间
     */
    public void setDisplayEndTime(Date displayEndTime) {
        this.displayEndTime = displayEndTime;
    }

    /**
     * 优惠券有效期，天数
     * @return valid_day 优惠券有效期，天数
     */
    public Integer getValidDay() {
        return validDay;
    }

    /**
     * 优惠券有效期，天数
     * @param validDay 优惠券有效期，天数
     */
    public void setValidDay(Integer validDay) {
        this.validDay = validDay;
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @return del 删除标识 0:未删除 1:已删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @param del 删除标识 0:未删除 1:已删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 使用门槛
     * @return limit_amount 使用门槛
     */
    public BigDecimal getLimitAmount() {
        return limitAmount;
    }

    /**
     * 使用门槛
     * @param limitAmount 使用门槛
     */
    public void setLimitAmount(BigDecimal limitAmount) {
        this.limitAmount = limitAmount;
    }

    /**
     * 优惠券类型：1淘宝优惠券，2商品优惠券
     * @return type 优惠券类型：1淘宝优惠券，2商品优惠券
     */
    public Integer getType() {
        return type;
    }

    /**
     * 优惠券类型：1淘宝优惠券，2商品优惠券
     * @param type 优惠券类型：1淘宝优惠券，2商品优惠券
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 发行总量
     * @return stock_def 发行总量
     */
    public Integer getStockDef() {
        return stockDef;
    }

    /**
     * 发行总量
     * @param stockDef 发行总量
     */
    public void setStockDef(Integer stockDef) {
        this.stockDef = stockDef;
    }

    /**
     * 已发发行量
     * @return stock 已发发行量
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * 已发发行量
     * @param stock 已发发行量
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 每人限领取张数
     * @return limit_per_num 每人限领取张数
     */
    public Integer getLimitPerNum() {
        return limitPerNum;
    }

    /**
     * 每人限领取张数
     * @param limitPerNum 每人限领取张数
     */
    public void setLimitPerNum(Integer limitPerNum) {
        this.limitPerNum = limitPerNum;
    }

    /**
     * 优惠券状态: 1:发行中，2已结束
     * @return coupon_status 优惠券状态: 1:发行中，2已结束
     */
    public Integer getCouponStatus() {
        return couponStatus;
    }

    /**
     * 优惠券状态: 1:发行中，2已结束
     * @param couponStatus 优惠券状态: 1:发行中，2已结束
     */
    public void setCouponStatus(Integer couponStatus) {
        this.couponStatus = couponStatus;
    }

    /**
     * 商品ID
     * @return product_id 商品ID
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 商品ID
     * @param productId 商品ID
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     *
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", name=").append(name);
        sb.append(", desc=").append(desc);
        sb.append(", amount=").append(amount);
        sb.append(", tpwd=").append(tpwd);
        sb.append(", displayStartTime=").append(displayStartTime);
        sb.append(", displayEndTime=").append(displayEndTime);
        sb.append(", validDay=").append(validDay);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", limitAmount=").append(limitAmount);
        sb.append(", type=").append(type);
        sb.append(", stockDef=").append(stockDef);
        sb.append(", stock=").append(stock);
        sb.append(", limitPerNum=").append(limitPerNum);
        sb.append(", couponStatus=").append(couponStatus);
        sb.append(", productId=").append(productId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
