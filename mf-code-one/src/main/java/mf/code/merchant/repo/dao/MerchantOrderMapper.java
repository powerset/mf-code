package mf.code.merchant.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MerchantOrderMapper extends BaseMapper<MerchantOrder> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(MerchantOrder record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(MerchantOrder record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    MerchantOrder selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(MerchantOrder record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     * @return
     */
    int updateByPrimaryKey(MerchantOrder record);

    List<MerchantOrder> queryPageByParamsThisMonth(Map<String, Object> params);

    int countByParamsThisMonth(Map<String, Object> params);
}
