package mf.code.merchant.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MerchantShopCouponMapper extends BaseMapper<MerchantShopCoupon> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(MerchantShopCoupon record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(MerchantShopCoupon record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    MerchantShopCoupon selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(MerchantShopCoupon record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(MerchantShopCoupon record);

    /**
     * 分页查询总条数
     * @param params
     * @return
     */
    int countForPageList(Map<String, Object> params);

    /**
     * 分页查询优惠券
     * @param params
     * @return
     */
    List<MerchantShopCoupon> pageListCoupon(Map<String, Object> params);

}