package mf.code.merchant.domain.aggregateroot;

import lombok.Data;
import mf.code.merchant.domain.valueobject.ShopTask;
import mf.code.merchant.repo.po.MerchantShop;

/**
 * mf-code-one 针对活动相关的 店铺信息
 *
 * mf.code.merchant.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-05 16:24
 */
@Data
public class ActivityShop {
	/**
	 * merchantShopId
	 */
	private Long id;
	/**
	 * 店铺信息
	 */
	private MerchantShop merchantShop;
	/**
	 * 1. 所属店铺任务
	 * 2. 所属喜销宝返回商品数字xxbtop_num_iid的任务 -- 关联至对应奖品的任务
	 */
	private ShopTask shopTask;

}
