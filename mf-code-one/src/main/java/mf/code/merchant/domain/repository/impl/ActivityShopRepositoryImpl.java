package mf.code.merchant.domain.repository.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.common.constant.*;
import mf.code.common.verify.TaobaoVerifyService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.domain.aggregateroot.ActivityShop;
import mf.code.merchant.domain.repository.ActivityShopRepository;
import mf.code.merchant.domain.valueobject.ShopTask;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.merchant.domain.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-05 17:01
 */
@Slf4j
@Service
public class ActivityShopRepositoryImpl implements ActivityShopRepository {
	@Autowired
	private MerchantShopService merchantShopService;
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private TaobaoVerifyService taobaoVerifyService;
	@Autowired
	private ActivityDefService activityDefService;
	@Autowired
	private ActivityTaskService activityTaskService;

	/**
	 * 通过 shopId 获取 店铺信息
	 *
	 * @param shopId
	 * @return
	 */
	@Override
	public ActivityShop findById(Long shopId) {
		MerchantShop merchantShop = merchantShopService.getById(shopId);
		if (merchantShop == null) {
			return null;
		}
		// 装配ActivityShop
		return assemblyMerchantShop(merchantShop);
	}

	/**
	 * 通过 所属喜销宝返回商品数字的xxbtop_num_iids, 增装 shopTask相关店铺任务
	 * @param activityShop
	 * @param xxbtopNumIids
	 * @return
	 */
	@Override
	public ActivityShop addShopTaskByXxbtopNumIid(ActivityShop activityShop, List<String> xxbtopNumIids) {
		Long shopId = activityShop.getId();

		// 获取 fillBackOrderMap
		Map<Long, ActivityDef> fillBackOrderMap = this.getFillBackOrderMap(shopId);
		// 获取 goodsMap
		Map<String, List<Goods>> goodsMap = this.getGoodsMap(shopId, activityShop.getMerchantShop().getMerchantId(), xxbtopNumIids);

		ShopTask shopTask = new ShopTask();
		shopTask.setShopId(shopId);
		shopTask.setXxbtopNumIids(xxbtopNumIids);
		shopTask.setFillBackOrderMap(fillBackOrderMap);
		shopTask.setGoodsMap(goodsMap);
		if (CollectionUtils.isEmpty(goodsMap)) {
			// 不存在商品列表信息，则返回
			activityShop.setShopTask(shopTask);
			return activityShop;
		}

		Set<Long> goodsIds = new HashSet<>();
		for (List<Goods> goodsList : goodsMap.values()){
			for (Goods goods : goodsList) {
				// goods表没有商品信息时，会重新调用喜销宝查询商品信息，此时没有goodsId
				if (goods.getId() != null) {
					goodsIds.add(goods.getId());
				}
			}
		}
		// 当没有goodsId时，不存在好评返现任务和收藏加购任务
		if (CollectionUtils.isEmpty(goodsIds)) {
			activityShop.setShopTask(shopTask);
			return activityShop;
		}
		// 获取对应奖品的好评返现任务
		Map<Long, ActivityTask> goodsCommentMap = this.getGoodsCommentMap(goodsIds);
		// 获取对应奖品的收藏加购任务
		Map<Long, ActivityTask> favcartMap = this.getFavcartMap(goodsIds);

		shopTask.setGoodsCommentMap(goodsCommentMap);
		shopTask.setFavcartMap(favcartMap);
		activityShop.setShopTask(shopTask);

		return activityShop;
	}

	/**
	 * 收藏加购 -- 以对应商品收藏加购 -- 一个商品只能有一条对应的发布中的任务
	 *
	 * @param goodsIds
	 * @return Map<GoodsId, ActivityDef>
	 */
	private Map<Long, ActivityTask> getFavcartMap(Set<Long> goodsIds) {
		Map<Long, ActivityTask> favcartMap = new HashMap<>();

		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.in(ActivityTask::getGoodsId, goodsIds)
				.eq(ActivityTask::getType, ActivityTaskTypeEnum.FAVCART.getCode())
				.eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
				.eq(ActivityTask::getDel, DelEnum.NO.getCode());
		List<ActivityTask> activityTasks = activityTaskService.list(wrapper);
		if (CollectionUtils.isEmpty(activityTasks)) {
			log.info("没有收藏加购红包任务, goodsIds = {}", goodsIds);
			return favcartMap;
		}
		for (ActivityTask activityTask : activityTasks) {
			favcartMap.put(activityTask.getGoodsId(), activityTask);
		}
		return favcartMap;
	}

	/**
	 * 好评返现 -- 以对应商品返现 -- 一个商品只能有一条对应的发布中的任务
	 *
	 * @param goodsIds
	 * @return Map<GoodsId, ActivityDef>
	 */
	private Map<Long, ActivityTask> getGoodsCommentMap(Set<Long> goodsIds) {
		Map<Long, ActivityTask> goodsCommentMap = new HashMap<>();

		QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.in(ActivityTask::getGoodsId, goodsIds)
				.eq(ActivityTask::getType, ActivityTaskTypeEnum.GOOD_COMMENT.getCode())
				.eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
				.eq(ActivityTask::getDel, DelEnum.NO.getCode());
		List<ActivityTask> activityTasks = activityTaskService.list(wrapper);
		if (CollectionUtils.isEmpty(activityTasks)) {
			log.info("没有好评晒图红包任务, goodsIds = {}", goodsIds);
			return goodsCommentMap;
		}
		for (ActivityTask activityTask : activityTasks) {
			goodsCommentMap.put(activityTask.getGoodsId(), activityTask);
		}
		return goodsCommentMap;
	}

	/**
	 * 	回填订单红包任务: 已没有商品的概念。 -- 一个店铺只能有一条对应的发布中的任务
	 * 	所属店铺订单给固定红包，或 给区间随机红包
	 *
	 * @param shopId
	 * @return Map<shopId, ActivityDef>
	 */
	private Map<Long, ActivityDef> getFillBackOrderMap(Long shopId) {
		Map<Long, ActivityDef> fillBackOrderMap = new HashMap<>();
		// 查看该商品 有无 回填订单红包 任务
		QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityDef::getShopId, shopId)
				.eq(ActivityDef::getType, ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
				.eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
				.eq(ActivityDef::getDel, DelEnum.NO.getCode());
		List<ActivityDef> activityDefList = activityDefService.list(wrapper);
		if (CollectionUtils.isEmpty(activityDefList)) {
			log.info("没有回填订单红包任务, shopId = {}", shopId);
			return fillBackOrderMap;
		}
		fillBackOrderMap.put(shopId, activityDefList.get(0));
		return fillBackOrderMap;
	}

	/**
	 * xxbtop_num_iid所属奖品列表信息
	 *
	 * @param shopId
	 * @param merchantId
	 * @param xxbtopNumIids
	 * @return Map<xxbtopNumIid, List<Goods>>
	 */
	private Map<String, List<Goods>> getGoodsMap(Long shopId, Long merchantId, List<String> xxbtopNumIids) {
		Map<String, List<Goods>> goodsMap = new HashMap<>();
		// 获取xxbtopNumIid创建的奖品列表
		Map<String, Object> params = new HashMap<>();
		params.put("shopId", shopId);
		params.put("numIids", xxbtopNumIids);
		params.put("del", DelEnum.NO.getCode());
		List<Goods> goodsList = goodsService.pageList(params);
		if (CollectionUtils.isEmpty(goodsList)) {
			// 数据库无xxbtopNumIids对应的奖品列表 ，则向喜销宝查询
			for (String xxbtopNumIid : xxbtopNumIids) {
				String xxbResult = taobaoVerifyService.getGoodsInfo(xxbtopNumIid, merchantId);
				if (taobaoVerifyService.isError(xxbResult)) {
					continue;
				}
				Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
				if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() <= 1) {
					continue;
				}
				// 组装参数 名称-主图-详情图-价格
				Goods goods = new Goods();
				goods.setXxbtopTitle(xxbMap.get("title").toString());
				goods.setDisplayGoodsName(xxbMap.get("title").toString());
				goods.setPicUrl(xxbMap.get("pic_url").toString());
				goods.setXxbtopPrice(xxbMap.get("price").toString());
				goods.setDisplayPrice(new BigDecimal(xxbMap.get("price").toString()));
				goods.setXxbtopNumIid(xxbtopNumIid);
				List<Goods> xxbtopNumIidGoodsList = goodsMap.get(xxbtopNumIid);
				if (CollectionUtils.isEmpty(xxbtopNumIidGoodsList)) {
					xxbtopNumIidGoodsList = new ArrayList<>();
				}
				xxbtopNumIidGoodsList.add(goods);
				goodsMap.put(xxbtopNumIid, xxbtopNumIidGoodsList);
			}
		} else {
			for (Goods goods : goodsList) {
				String xxbtopNumIid = goods.getXxbtopNumIid();
				List<Goods> xxbtopNumIidGoodsList = goodsMap.get(xxbtopNumIid);
				if (CollectionUtils.isEmpty(xxbtopNumIidGoodsList)) {
					xxbtopNumIidGoodsList = new ArrayList<>();
				}
				xxbtopNumIidGoodsList.add(goods);
				goodsMap.put(xxbtopNumIid, xxbtopNumIidGoodsList);
			}
		}
		return goodsMap;
	}

	/**
	 * 装配 activityShop
	 * @param merchantShop
	 * @return
	 */
	private ActivityShop assemblyMerchantShop(MerchantShop merchantShop) {
		ActivityShop activityShop = new ActivityShop();
		activityShop.setId(merchantShop.getId());
		activityShop.setMerchantShop(merchantShop);
		return activityShop;
	}
}
