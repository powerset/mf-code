package mf.code.summary.repo.dao;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * 统计概览
 *
 * @author gbf
 */
@Repository
public interface SummarySellerMapper {

    /**
     * 用户总数
     *
     * @param shopId
     * @return
     */
    Long countTotalUserByShopId(Long shopId);

    /**
     * 新人
     *
     * @param param
     * @return
     */
    Long countNewBieUserByShopId(Map param);

    /**
     * 参与人数
     *
     * @param m
     * @return
     */
    Long getActivityJoinManTotalByShopId(Map m);

    /**
     * 中奖人数 / 领奖人数
     *
     * @param m
     * @return
     */
    Long countHitManAndAwardManFromUserTask(Map m);


    /**
     * 中奖人数-拆红包专用
     *
     * @param param param
     * @return
     */
    Long findHitManTotalByShopIdForOpenRedpack(Map<String, Object> param);

    /**
     * 领奖人数-拆红包专用
     *
     * @param param
     * @return
     */
    Long findAwardManTotalByShopIdForOpenRedpack(Map<String, Object> param);

    /**
     * 裂变
     *
     * @param m param
     * @return result
     */
    List<Map<String, Long>> fissionSummaryByShopIdAndDate(Map m);

    /**
     * 邀请失败的 v1
     *
     * @param m param
     * @return result
     */
    List<Map<String, Long>> fissionFaildSummaryByShopIdAndDate(Map<String, Object> m);

    /**
     * 返现金额
     *
     * @param shopId param
     * @return long
     */
    BigDecimal sumCost(Map<String, Object> shopId);


    /**
     * 保证金余额
     *
     * @param m 参数
     * @return list
     */
    BigDecimal sumDepositBalance(Map<String, Object> m);


    /**
     * 新增用户统计-柱状图-按时间周期
     *
     * @param param 参数
     * @return list
     */
    List<Map<String, Object>> newbieHistogramByDate(Map<String, Object> param);

    /**
     * 参与
     *
     * @param param Map
     * @return Long
     */
    Long getUserTaskJoinByType(Map<String, Object> param);

    /**
     * 登陆
     *
     * @param param
     * @return
     */
    Long loginUserByDate(Map<String, Object> param);


    /**
     * 充值金额
     *
     * @param sqlParam
     * @return
     */
    BigDecimal sumDepositTotal(Map sqlParam);

    /**
     * 红包充值金额
     *
     * @param sqlParam
     * @return
     */
    BigDecimal sumDepositTotalFromActivityTask(Map sqlParam);

    /**
     * 红包保证金余额
     *
     * @param sqlParam
     * @return
     */
    BigDecimal sumDepositBalanceFromActivityTask(Map<String, Object> sqlParam);

    /**
     * 已返现金额
     *
     * @param sqlParam
     * @return
     */
    BigDecimal sumCostFromActivityDef(Map<String, Object> sqlParam);

    /**
     * 红包任务的待返现金额
     *
     * @param param
     * @return
     */
    BigDecimal getRetainFromUserTask(Map<String, Object> param);

    /**
     * 根据type类型count参与人数和领奖人数
     *
     * @param param
     * @return
     */
    Long countUserTaskJoinManAndAwardMan(Map<String, Object> param);

    /**
     * 拆红包的参与人数
     * @param param
     * @return
     */
    Long countJoinManFromUserActivity(Map<String, Object> param);

    /**
     * 拆红包发奖人数
     * @param param
     * @return
     */
    Long countUserCouponAwardMan(Map<String, Object> param);

    /**
     * 助力赠品 / 抽奖 的参数人数
     * @param param
     * @return
     */
    Long countJoinManFromActivity(Map<String, Object> param);

    BigDecimal getRetainFromActivityDefByTypeForAssitAndDraw(Map<String, Object> param);

    BigDecimal getRetainFromActivityDefByTypeForCommentAndCart(Map<String, Object> param);

    /**
     * 用户清算的钱
     * @param param
     * @return
     */
    BigDecimal totalCleanMoney(Map<String, Object> param);
}
