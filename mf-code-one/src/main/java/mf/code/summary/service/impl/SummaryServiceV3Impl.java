package mf.code.summary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.summary.constant.*;
import mf.code.summary.repo.dao.SummarySellerMapper;
import mf.code.summary.service.SummaryV3Service;
import mf.code.uactivity.service.UserActivityService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserRepostory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * 首页 统计概览
 *
 * 准备重构
 *
 * @author gbf
 */
@Slf4j
@Service
public class SummaryServiceV3Impl implements SummaryV3Service {
    @Autowired
    private SummarySellerMapper summarySellerMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserRepostory userRepostory;

    private static final int DAY = 1;
    private static final int WEEK = 2;
    private static final int MONTH = 3;

    /**
     * 用户统计
     *
     * @param shopId   店铺
     * @param typeEnum 类型
     * @return -->/api/seller/statistic/summaryUser
     */
    @Override
    public SimpleResponse summaryUser(Long shopId, BizTypeEnum typeEnum) {
        log.info("###################### summaryServiceV2Impl.userSummary() typeList=" + typeEnum);
        Map<String, Object> result = new HashMap<>(4);
        // 用户总数
        QueryWrapper<User> sqlWrapper = new QueryWrapper<>();
        sqlWrapper.lambda()
                .eq(User::getVipShopId, shopId)
                .eq(User::getGrantStatus, 1)
        ;
        result.put("userTotal", userRepostory.count(sqlWrapper));
        Map<String, Object> param = new HashMap<>(2);
        // 今日新用户数
        param.put("shopId", shopId);
        String today = DateUtil.dateToString(new Date(), DateUtil.LONG_DATE_FORMAT);
        param.put("date", today);

        result.put("newbieToday", summarySellerMapper.countNewBieUserByShopId(param));
        result.put("loginToday", summarySellerMapper.loginUserByDate(param));
        // 昨日新用户数
        String yesterday = DateUtil.getYesterday();
        param.put("date", yesterday);
        result.put("newBieYesterday", summarySellerMapper.countNewBieUserByShopId(param));
        Map<Object, Object> redisMapUsers = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.STATISTICS_SUMMARY_LOGIN + shopId + ":" + yesterday);
        result.put("loginYesterday", redisMapUsers == null ? 0 : redisMapUsers.keySet().size());
        //活动维度的用户
        Map<String, Long> respMap = new HashMap<>();
        if (typeEnum == null) {
            return new SimpleResponse(1, "活动类型不能为空");
        } else {
            respMap = this.activityUserSummary(shopId, typeEnum);
            result.put("activity", respMap);
        }
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(result);
        return response;
    }

    /**
     * 用户统计-活动维度
     *
     * @param shopId      店铺
     * @param bizTypeEnum 类型
     * @return Map
     */
    private Map<String, Long> activityUserSummary(Long shopId, BizTypeEnum bizTypeEnum) {
        Map<String, Long> result = new HashMap<>(6);
        Map<String, Object> param = new HashMap<>(2);
        param.put("shopId", shopId);

        // 回填订单 / 好评晒图 / 收藏加购
        if (BizTypeEnum.FILL_ORDER == bizTypeEnum ||
                BizTypeEnum.GOOD_COMMENT == bizTypeEnum ||
                BizTypeEnum.CART == bizTypeEnum) {

            param.put("typeList", UserTaskTypeMapping.getTypeList(bizTypeEnum.getCode()));
            // 参与人数 user_activity表
            Long joinCount = summarySellerMapper.countUserTaskJoinManAndAwardMan(param);
            result.put("joinCount", joinCount);
            // 领奖人数 user_task表
            param.put("status", "3");
            Long awardCount = summarySellerMapper.countUserTaskJoinManAndAwardMan(param);
            result.put("awardCount", awardCount);

            // 昨日的
            param.put("date", DateUtil.getYesterday());
            //  昨日的 参与人数
            param.remove("status");
            Long joinCountYesterday = summarySellerMapper.countUserTaskJoinManAndAwardMan(param);
            result.put("joinCountYesterday", joinCountYesterday);
            //  昨日的 领奖人数
            param.put("status", "3");
            Long awardCountYesterday = summarySellerMapper.countUserTaskJoinManAndAwardMan(param);
            result.put("awardCountYesterday", awardCountYesterday);

        } else if (BizTypeEnum.OPEN_RED_PACKET == bizTypeEnum) {
            // 参与人数 user_activity表
            param.put("typeList", UserActivityTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long joinCount = summarySellerMapper.countJoinManFromUserActivity(param);
            result.put("joinCount", joinCount);
            // 领奖人数 activity表
            param.put("typeList", UserCouponTypeMapping.getTypeList(bizTypeEnum.getCode()));
            param.put("status", 1);
            Long awardCount = summarySellerMapper.countUserCouponAwardMan(param);
            result.put("awardCount", awardCount);

            // 昨日的
            param.put("date", DateUtil.getYesterday());
            param.remove("status");
            //  昨日的 参与人数
            param.put("typeList", UserActivityTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long joinCountYesterday = summarySellerMapper.countJoinManFromUserActivity(param);
            result.put("joinCountYesterday", joinCountYesterday);
            //  昨日的 领奖人数
            param.put("typeList", UserCouponTypeMapping.getTypeList(bizTypeEnum.getCode()));
            param.put("status", 1);
            Long awardCountYesterday = summarySellerMapper.countUserCouponAwardMan(param);
            result.put("awardCountYesterday", awardCountYesterday);

        } else if (BizTypeEnum.LUCK_WHEEL == bizTypeEnum) {
            // 参与人数
            param.put("typeList", UserCouponTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long joinCount = summarySellerMapper.countUserCouponAwardMan(param);
            result.put("joinCount", joinCount);
            // 领奖人数
            param.put("status", 1);
            Long awardCount = summarySellerMapper.countUserCouponAwardMan(param);
            result.put("awardCount", awardCount);

            // 昨日的
            param.put("date", DateUtil.getYesterday());
            param.remove("status");
            Long joinCountYesterday = summarySellerMapper.countUserCouponAwardMan(param);
            result.put("joinCountYesterday", joinCountYesterday);
            param.put("status", 1);
            Long awardCountYesterday = summarySellerMapper.countUserCouponAwardMan(param);
            result.put("awardCountYesterday", awardCountYesterday);
        } else {
            // 参与人数 user_activity表
            param.put("typeList", ActivityTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long joinCount = summarySellerMapper.countJoinManFromActivity(param);
            result.put("joinCount", joinCount);
            // 中奖人数 user_task表
            param.put("typeList", UserTaskTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long hitCount = summarySellerMapper.countHitManAndAwardManFromUserTask(param);
            result.put("hitCount", hitCount);
            // 领奖人数 user_task表
            param.put("status", 1);
            param.put("typeList", UserTaskTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long awardCount = summarySellerMapper.countHitManAndAwardManFromUserTask(param);
            result.put("awardCount", awardCount);
            // 昨日的
            param.put("date", DateUtil.getYesterday());
            //  昨日的 参与人数
            param.remove("status");
            param.put("typeList", ActivityTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long joinCountYesterday = summarySellerMapper.countJoinManFromActivity(param);
            result.put("joinCountYesterday", joinCountYesterday);
            //  昨日的 中奖人数
            param.put("typeList", UserTaskTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long hitCountYesterday = summarySellerMapper.countHitManAndAwardManFromUserTask(param);
            result.put("hitCountYesterday", hitCountYesterday);
            //  昨日的 领奖人数
            param.put("status", 1);
            param.put("typeList", UserTaskTypeMapping.getTypeList(bizTypeEnum.getCode()));
            Long awardCountYesterday = summarySellerMapper.countHitManAndAwardManFromUserTask(param);
            result.put("awardCountYesterday", awardCountYesterday);
        }
        return result;
    }

    /**
     * 柱图
     *
     * @param shopId 店铺
     * @param type   类型 1:日 2:周 3:月
     * @return -->/api/seller/statistic/summaryNewbie
     */
    @Override
    public SimpleResponse summaryNewbie(Long shopId, Integer type) {
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        Map<String, Object> param = new HashMap<>(2);
        param.put("shopId", shopId);
        // 结束时间为昨日 23:59:59
        param.put("end", DateUtil.dateToString(DateUtil.getDateBeforeZero(DateUtil.addDay(new Date(), -1)), DateUtil.FORMAT_ONE));
        if (type == WEEK) {
            param.put("begin", DateUtil.dateToString(DateUtil.getDateAfterZero(DateUtil.addDay(new Date(), -7)), DateUtil.FORMAT_ONE));
        } else {
            param.put("begin", DateUtil.dateToString(DateUtil.getDateAfterZero(DateUtil.addDay(new Date(), -30)), DateUtil.FORMAT_ONE));
        }
        List<Map<String, Object>> list = summarySellerMapper.newbieHistogramByDate(param);

        List<String> queues = this.getQueue(type);
        list = this.getResultList(list, queues);
        int max = 0;
        for (Map<String, Object> m : list) {
            int temp = Integer.parseInt(m.get("count").toString());
            if (temp > max) {
                max = temp;
            }
        }
        Map result = new HashMap(2);
        result.put("max", max);
        result.put("data", list);
        response.setData(result);
        return response;
    }

    /**
     * 柱图 补全日期
     *
     * @param list   list
     * @param queues 队列
     * @return list
     */
    private List<Map<String, Object>> getResultList(List<Map<String, Object>> list, List<String> queues) {

        List<Map<String, Object>> result = new ArrayList<>();

        for (String str : queues) {
            boolean hasItem = true;
            for (Map<String, Object> m : list) {
                String date = m.get("date").toString();
                if (str.equals(date)) {
                    m.put("date", date);
                    result.add(m);
                    hasItem = false;
                    break;
                }
            }
            if (hasItem) {
                Map<String, Object> addMap = new HashMap<>(2);
                addMap.put("count", 0);
                addMap.put("date", str);
                result.add(addMap);
            }
        }
        return result;
    }

    /**
     * 柱图 补全 队列
     *
     * @param type 统计类型
     * @return list
     */
    private List<String> getQueue(Integer type) {
        List<String> list = new ArrayList<>();
        int limit = 1;
        if (type == WEEK) {
            limit = 7;
        } else if (type == MONTH) {
            limit = 30;
        }
        Date now = new Date();
        for (int i = 0; i < limit; i++) {
            list.add(DateUtil.dateToString(DateUtil.addDay(now, (i + 1) * -1), DateUtil.LONG_DATE_FORMAT));
        }
        return list;
    }


    /**
     * 裂变情况
     *
     * @param shopId       店铺
     * @param dateType     日期类型 1:日 2:周 3:月
     * @param activityType 活动类型
     * @return -->/api/seller/statistic/summaryActivity
     */
    @Override
    public SimpleResponse summaryActivity(Long shopId, Integer dateType, BizTypeEnum activityType) {
        //新人  时间默认为前七天的数据
        Date yesterday = DateUtil.addDay(new Date(), -1);
        String beginTimeStr;
        if (dateType == DAY) {
            //开始时间 : 昨天
            beginTimeStr = DateUtil.dateToString(DateUtil.getDateAfterZero(yesterday), DateUtil.FORMAT_ONE);
        } else if (dateType == WEEK) {
            // 开始时间 : 近7天
            Date time = DateUtil.addDay(new Date(), -7);
            beginTimeStr = DateUtil.dateToString(DateUtil.getDateAfterZero(time), DateUtil.FORMAT_ONE);
        } else {
            // 开始时间 : 近30天
            Date time = DateUtil.addDay(new Date(), -30);
            beginTimeStr = DateUtil.dateToString(DateUtil.getDateAfterZero(time), DateUtil.FORMAT_ONE);
        }
        // 结束时间 : 昨天
        String endTimeStr = DateUtil.dateToString(DateUtil.getDateBeforeZero(yesterday), DateUtil.FORMAT_ONE);
        Map<String, Object> resultMap = this.fissionSummary(shopId, activityType, beginTimeStr, endTimeStr);
        SimpleResponse response = new SimpleResponse();
        response.setData(resultMap);
        return response;
    }

    /**
     * 裂变情况 处理核心业务
     *
     * @param shopId      店铺
     * @param bizTypeEnum 类型
     * @param beginDate   开始时间
     * @param endDate     结束时间
     * @return Map
     */
    private Map<String, Object> fissionSummary(Long shopId, BizTypeEnum bizTypeEnum, String beginDate, String endDate) {
        Map<String, Object> param = new HashMap<>(4);
        param.put("shopId", shopId);
        //时间 要格式化成 yyyy:MM:dd hh:mm:ss才可在sql中查
        param.put("begin", beginDate);
        param.put("end", endDate);
        param.put("typeList", UserPubJoinTypeMapping.getTypeList(bizTypeEnum.getCode()));
        List<Map<String, Long>> listInvite = summarySellerMapper.fissionSummaryByShopIdAndDate(param);

        // 用户表的用户总数
        Long userTotal = 0L;
        if (BizTypeEnum.OPEN_RED_PACKET == bizTypeEnum || BizTypeEnum.ASSIST == bizTypeEnum || BizTypeEnum.LUCK_DRAW == bizTypeEnum) {
            // 拆红包 / 助力 / 抽奖 todo 调整与用户统计的参与人数相同方式
            param.put("typeList", UserActivityTypeMapping.getTypeList(bizTypeEnum.getCode()));
            userTotal = userActivityService.countUserAll(param);
        } else if (BizTypeEnum.LUCK_WHEEL == bizTypeEnum) {
            // 转盘 todo 调整与用户统计的参与人数相同方式
            param.put("typeList", UserCouponTypeMapping.getTypeList(bizTypeEnum.getCode()));
            userTotal = userCouponService.countUserAll(param);
        }

        long less = 0;
        long more = 0;
        for (Map m : listInvite) {
            long count = Long.parseLong(m.get("count") == null ? "0" : m.get("count").toString());
            if (count > 3) {
                more++;
            } else {
                less++;
            }
        }
        Map<String, Object> result = new HashMap<>(3);
        // 文案: 未邀请 意思是: 已经邀请,但成功为 0
        // 产品改了,改为没有发起邀请动作的
        result.put("total", userTotal);
        result.put("lessThan3", less);
        result.put("moreThan3", more);
        result.put("none", userTotal - less - more);
        return result;
    }

    /**
     * 财务
     *
     * @param shopId 店铺
     * @param type   类型
     * @param types
     * @return -->/api/seller/statistic/summaryFission
     */
    @Deprecated
    @Override
    public SimpleResponse summaryFission(Long shopId, Integer type, List<Integer> types) {
        log.info("#################### type=" + type + "types=" + types);
        // 产品需要:   2赠品活动、3免单抽奖 4:轮盘
        // api :1:全部 2:助力 3:抽奖 4:轮盘 5:红包 0:拆红包 对应数据库=>
        // 数据库: 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动 9拆红包',
        if (type == null && types.size() > 0) {
            type = types.get(0);
        }
        if (type == null && types.size() == 0) {
            type = 2;
        }
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        Map<String, Object> result = new HashMap<>(5);
        // sql条件
        Map<String, Object> param = new HashMap<>(2);
        param.put("shopId", shopId);
        //查询总数部分
        param.put("type", null);
        // 总金额
        BigDecimal total = this.depositTotal(param);
        result.put("total", total.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 保证金余额
        BigDecimal balance = depositBalance(param, null);
        result.put("balance", balance.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 已经返现
        BigDecimal cost = depositCost(param, null);
        result.put("cost", cost.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 待返现
        BigDecimal retain = (total.subtract(cost).subtract(balance)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        retain = retain.compareTo(BigDecimal.ZERO) >= 0 ? retain : new BigDecimal(0);
        result.put("retain", retain);

        log.info("#####################以下是分类查询###################");
        // 查询 分类部分
        param.put("type", type);

        // 总金额
        BigDecimal totalByType = this.depositTotal(param);
        result.put("totalByType", totalByType.setScale(2, BigDecimal.ROUND_HALF_DOWN));
        // 已经返现
        BigDecimal costByType = depositCost(param, type);
        result.put("costByType", costByType.setScale(2, BigDecimal.ROUND_HALF_DOWN));
        // 保证金余额
        BigDecimal balanceByType = depositBalance(param, type);
        result.put("balanceByType", balanceByType.setScale(2, BigDecimal.ROUND_HALF_DOWN));
        // 待返现
        BigDecimal retainByType = (totalByType.subtract(costByType).subtract(balanceByType)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        retainByType = retainByType.compareTo(BigDecimal.ZERO) >= 0 ? retainByType : new BigDecimal(0);
        result.put("retainByType", retainByType);

        result.put("type", type);
        if (types != null && types.size() > 1) {
            if (totalByType.compareTo(new BigDecimal(0)) == 0
                    && costByType.compareTo(new BigDecimal(0)) == 0
                    && balanceByType.compareTo(new BigDecimal(0)) == 0
                    && retainByType.compareTo(new BigDecimal(0)) == 0) {

                if (types.size() > 0) {
                    types.remove(0);
                    summaryFission(shopId, null, types);
                } else {
                    //type=5:红包任务
                    summaryFission(shopId, 5, null);
                }
            }
        }

        response.setData(result);
        return response;
    }

    /**
     * 财务统计
     * // 产品需要:   2赠品活动、3免单抽奖 4:轮盘
     * // api :1:全部 2:助力 3:抽奖 4:轮盘 5:红包 0:拆红包 对应数据库=>
     * // 数据库: 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动 9拆红包',
     *
     * @param shopId
     * @param type
     * @param bizTypeEnum
     * @return
     */
    @Override
    public SimpleResponse summaryFinanceV2(Long shopId, Integer type, BizTypeEnum bizTypeEnum) {

        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        Map<String, Object> result = new HashMap<>();
        // sql条件
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        // 总金额 = sum(activity_def.deposit_def)
        BigDecimal total = this.depositTotal(param);
        result.put("total", total.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 已经返现 = sum(activity_def.deposit_def - deposit) by typeList
        BigDecimal cost = this.depositCost(param);
        result.put("cost", cost.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 待返现 =
        BigDecimal retain = new BigDecimal(0);
        // 轮盘没用待返现
        List<Integer> typeList = this.getActivityDefTypeListByTypes(BizTypeEnum.FILL_ORDER, BizTypeEnum.GOOD_COMMENT, BizTypeEnum.CART, BizTypeEnum.LUCK_DRAW, BizTypeEnum.ASSIST);
        log.info("保证金统计:待返现金额:类型:{}", typeList);
        param.put("typeList", typeList);
        retain = summarySellerMapper.getRetainFromUserTask(param);
        BigDecimal retainOpenRedpack = this.getOpenRedpackRetain(param);
        retainOpenRedpack = (retainOpenRedpack == null) ? new BigDecimal(0) : retainOpenRedpack;
        retain = (retain == null) ? new BigDecimal(0) : retain;
        result.put("retain", (retain.add(retainOpenRedpack)).setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 保证金余额 = 总金额 - 已经返现 - 待返现
        BigDecimal balance = total.subtract(cost).subtract(retain);
        result.put("balance", balance.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        log.info("#####################以下是分类查询###################");
        // 查询 分类部分
        // 总金额
        param.put("typeList", ActivityDefTypeMapping.getTypeList(bizTypeEnum.getCode()));
        BigDecimal totalByType = this.depositTotal(param);
        result.put("totalByType", totalByType.setScale(2, BigDecimal.ROUND_HALF_DOWN));
        // 已经返现
        param.put("typeList", UpayWxOrderTypeMapping.getTypeList(bizTypeEnum.getCode()));
        BigDecimal costByType;
        if (BizTypeEnum.OPEN_RED_PACKET == bizTypeEnum) {
            param.put("typeList", ActivityTypeMapping.getTypeList(BizTypeEnum.OPEN_RED_PACKET.getCode()));
            param.put("hasPay", 1);
            costByType = activityService.calcOpenRedPacketMoney(param);
        } else {
            costByType = this.depositCost(param);
        }
        result.put("costByType", costByType.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        // 待返现
        BigDecimal retainByType = new BigDecimal(0);
        // 拆红包
        if (BizTypeEnum.OPEN_RED_PACKET == bizTypeEnum) {
            //在activity表中查
            param.put("hasPay", 0);
            param.put("typeList", ActivityTypeMapping.getTypeList(BizTypeEnum.OPEN_RED_PACKET.getCode()));
            retainByType = this.getOpenRedpackRetain(param);

        } else if (BizTypeEnum.LUCK_WHEEL == bizTypeEnum) {
            // 轮盘返现金额 = 0
            retainByType = new BigDecimal(0);

        } else if ((BizTypeEnum.FILL_ORDER == bizTypeEnum) ||
                (BizTypeEnum.GOOD_COMMENT == bizTypeEnum) ||
                (BizTypeEnum.CART == bizTypeEnum) ||
                (BizTypeEnum.LUCK_DRAW == bizTypeEnum) ||
                (BizTypeEnum.ASSIST == bizTypeEnum)) {
            // 在activity_def表中查
            param.put("typeList", ActivityDefTypeMapping.getTypeList(bizTypeEnum.getCode()));
            retainByType = summarySellerMapper.getRetainFromUserTask(param);
        }
        retainByType = (retainByType == null) ? new BigDecimal(0) : retainByType;
        result.put("retainByType", retainByType);

        // 保证金余额
        BigDecimal balanceByType = totalByType.subtract(costByType).subtract(retainByType);
        result.put("balanceByType", balanceByType.setScale(2, BigDecimal.ROUND_HALF_DOWN));

        response.setData(result);
        return response;
    }

    /**
     * 获取拆红包的待返现
     */
    private BigDecimal getOpenRedpackRetain(Map param) {
        param.put("typeList", ActivityTypeMapping.getTypeList(BizTypeEnum.OPEN_RED_PACKET.getCode()));
        param.put("hasPay", 0);
        BigDecimal retain = activityService.calcOpenRedPacketMoney(param);
        if (retain == null) {
            retain = new BigDecimal(0);
        }
        return retain;
    }


    /**
     * 通过类型数组获取类型的List
     *
     * @param types
     * @return
     */
    private List<Integer> getActivityDefTypeListByTypes(BizTypeEnum... types) {
        List<Integer> typeList = new ArrayList<>();
        for (BizTypeEnum e : types) {
            typeList.addAll(ActivityDefTypeMapping.getTypeList(e.getCode()));
        }
        return typeList;
    }

    /**
     * 总保证金 = 已交保证金 = 已经返现+待返现+保证金余额
     *
     * @param sqlParam param
     * @return
     */
    private BigDecimal depositTotal(Map<String, Object> sqlParam) {
        BigDecimal total = summarySellerMapper.sumDepositTotal(sqlParam);
        return total == null ? new BigDecimal(0) : total;
    }

    /**
     * 已经返现
     *
     * @param sqlParam param
     * @param type     param
     * @return Long
     */
    private BigDecimal depositCost(Map<String, Object> sqlParam, Integer type) {
        BigDecimal cost = summarySellerMapper.sumCost(sqlParam);
        return cost == null ? new BigDecimal(0) : cost;
    }

    /**
     * 已经返现
     *
     * @param sqlParam param
     * @return Long
     */
    private BigDecimal depositCost(Map<String, Object> sqlParam) {
        BigDecimal cost = summarySellerMapper.sumCostFromActivityDef(sqlParam);
        return cost == null ? new BigDecimal(0) : cost;
    }

    /**
     * 保证金余额
     *
     * @param sqlParam param
     * @param type     param
     * @return Long
     */
    @Deprecated
    private BigDecimal depositBalance(Map<String, Object> sqlParam, Integer type) {
        BigDecimal result;
        if (type != null && type == 5) { //fixme
            result = summarySellerMapper.sumDepositBalanceFromActivityTask(sqlParam);
        } else {
            result = summarySellerMapper.sumDepositBalance(sqlParam);
        }
        return result == null ? new BigDecimal(0) : result;
    }


    /**
     * 保证金余额
     *
     * @param sqlParam param
     * @param type     param
     * @return Long
     */
    @Deprecated
    private BigDecimal depositBalance(Map<String, Object> sqlParam, Integer type, BizTypeEnum bizTypeEnum) {
        BigDecimal result;
        if (type != null && type == 5) {
            result = summarySellerMapper.sumDepositBalanceFromActivityTask(sqlParam);
        } else {
            sqlParam.put("typeList", ActivityDefTypeMapping.getTypeList(bizTypeEnum.getCode()));
            result = summarySellerMapper.sumDepositBalance(sqlParam);
        }
        return result == null ? new BigDecimal(0) : result;
    }

    /**
     * 保证金余额
     *
     * @param sqlParam param
     * @return BigDecimal
     */
    private BigDecimal depositBalanceV2(Map<String, Object> sqlParam) {
        BigDecimal result = summarySellerMapper.sumDepositBalance(sqlParam);
        return result == null ? new BigDecimal(0) : result;
    }
}
