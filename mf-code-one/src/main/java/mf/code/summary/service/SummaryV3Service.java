package mf.code.summary.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.summary.constant.BizTypeEnum;

import java.util.List;

/**
 * create by qc on 2019/1/16 0016
 *
 * @author gbf
 */
public interface SummaryV3Service {

    /**
     * 用户统计
     *
     * @param shopId 店铺
     * @param typeEnum   类型
     * @return SimpleResponse
     */
    SimpleResponse summaryUser(Long shopId, BizTypeEnum typeEnum);

    /**
     * 柱图
     *
     * @param shopId 店铺
     * @param type   类型 1:日 2:周 3:月
     * @return SimpleResponse
     */
    SimpleResponse summaryNewbie(Long shopId, Integer type);

    /**
     * 饼图
     *
     * @param shopId        店铺
     * @param dateType      日期类型 1:日 2:周 3:月
     * @param activityTypeEnum 活动类型
     * @return SimpleResponse
     */
    SimpleResponse summaryActivity(Long shopId, Integer dateType, BizTypeEnum activityTypeEnum);

    /**
     * 财务
     *
     * @param shopId 店铺
     * @param type   类型
     * @param types   类型
     * @return SimpleResponse
     */
    SimpleResponse summaryFission(Long shopId, Integer type, List<Integer> types);

    SimpleResponse summaryFinanceV2(Long shopId, Integer type, BizTypeEnum bizTypeEnum);
}
