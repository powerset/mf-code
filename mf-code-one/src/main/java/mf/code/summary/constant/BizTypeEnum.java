package mf.code.summary.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动定义表(activity_def)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum BizTypeEnum {

    FILL_ORDER(1, "回填订单"),
    GOOD_COMMENT(2, "好评晒图"),
    CART(3, "收藏加购"),
    LUCK_WHEEL(4, "大转盘"),
    LUCK_DRAW(5, "抽奖"),
    ASSIST(6, "助力赠品"),
    OPEN_RED_PACKET(7, "拆红包"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    /**
     */
    BizTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
