package mf.code.summary.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户奖品表 用户各种奖品扩充 比如：积分，优惠券，小礼品(user_coupon)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum UserCouponTypeMapping {

    FILL_ORDER(2, "回填订单", 1),
    GOOD_COMMENT(4, "好评晒图", 2),
    CART(5, "收藏加购", 3),
    LUCK_WHEEL(1, "大转盘", 4),
    LUCK_DRAW_1(7, "抽奖",5),
    LUCK_DRAW_2(8, "抽奖",5),
    ASSIST(11, "助力赠品", 6),

    OPEN_RED_PACKET_1(12, "拆红包", 7),
    OPEN_RED_PACKET_2(13, "拆红包", 7),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (UserCouponTypeMapping e : UserCouponTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }


    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    UserCouponTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
