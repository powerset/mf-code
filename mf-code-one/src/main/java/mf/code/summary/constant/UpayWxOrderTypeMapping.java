package mf.code.summary.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动定义表(upay_wx_order) -- (-1=>没有对应活动类型)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum UpayWxOrderTypeMapping {

    PLAN_DRAW(1,"计划类抽奖",5),
    LUCK_WHEEL_1(1, "大转盘", 4),
    FILL_ORDER_1(3, "回填订单", 1),
    CART_1(4, "收藏加购", 3),
    GOOD_COMMENT_1(5, "好评晒图", 2),
    OPEN_RED_PACKET_1(6, "拆红包", 7),
    ASSIST(7, "助力赠品", 6),
    LUCK_DRAW(8, "抽奖", 5),
    NEW_MAN_GIFT(9,"新人有礼",6),
    ASSIST_3(10, "财富闯关", -1),
    FILL_ORDER_2(11, "购买返利", -1),
    CART_2(12, "团队贡献", -1),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (UpayWxOrderTypeMapping e : UpayWxOrderTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }


    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    UpayWxOrderTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
