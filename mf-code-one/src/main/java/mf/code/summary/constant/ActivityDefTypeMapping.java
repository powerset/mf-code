package mf.code.summary.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动定义表(activity_def)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum ActivityDefTypeMapping {

    LUCK_DRAW_1(2, "抽奖", 5),
    LUCK_WHEEL_1(4, "大转盘", 4),
    FILL_ORDER_1(5, "回填订单", 1),
    ASSIST_1(6, "助力赠品", 6),
    ASSIST_2(7, "助力赠品", 6),
    OPEN_RED_PACKET_1(9, "拆红包", 7),
    GOOD_COMMENT_1(10, "好评晒图", 2),
    CART_1(11, "收藏加购", 3),
    LUCK_DRAW_2(12, "抽奖", 5),
    ASSIST_3(13, "助力赠品", 6),
    FILL_ORDER_2(14, "回填订单", 1),
    GOOD_COMMENT_2(15, "好评晒图", 2),
    CART_2(16, "收藏加购", 3),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (ActivityDefTypeMapping e : ActivityDefTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }

    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    ActivityDefTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
