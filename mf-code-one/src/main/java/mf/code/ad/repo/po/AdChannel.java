package mf.code.ad.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * ad_channel
 * 流量主表，记录集客二维码广告 流量提供者的基本信息
 */
public class AdChannel implements Serializable {
    /**
     * 流量主Id
     */
    private Long id;

    /**
     * 流量主名称
     */
    private String name;

    /**
     * 渠道品牌介绍
     */
    private String channelDesc;

    /**
     * 渠道品牌介绍的图片url
     */
    private String channelPic;

    /**
     * 渠道广告位介绍
     */
    private String adDesc;

    /**
     * 渠道广告位介绍的图片url
     */
    private String adPic;

    /**
     * 限制广告主最低购买数量
     */
    private Integer unitStock;

    /**
     * 广告二维码总展现次数
     */
    private Integer stockDef;

    /**
     * 剩余展现次数
     */
    private Integer stock;

    /**
     * 展示一次的单价
     */
    private BigDecimal unitPrice;

    /**
     * 二维码url：域名?id=id
     */
    private String qrCode;

    /**
     * 平台用户id
     */
    private Long platUserId;

    /**
     * 流量主服务状态 -1已停止 1已启动
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * ad_channel
     */
    private static final long serialVersionUID = 1L;

    /**
     * 流量主Id
     * @return id 流量主Id
     */
    public Long getId() {
        return id;
    }

    /**
     * 流量主Id
     * @param id 流量主Id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 流量主名称
     * @return name 流量主名称
     */
    public String getName() {
        return name;
    }

    /**
     * 流量主名称
     * @param name 流量主名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 渠道品牌介绍
     * @return channel_desc 渠道品牌介绍
     */
    public String getChannelDesc() {
        return channelDesc;
    }

    /**
     * 渠道品牌介绍
     * @param channelDesc 渠道品牌介绍
     */
    public void setChannelDesc(String channelDesc) {
        this.channelDesc = channelDesc == null ? null : channelDesc.trim();
    }

    /**
     * 渠道品牌介绍的图片url
     * @return channel_pic 渠道品牌介绍的图片url
     */
    public String getChannelPic() {
        return channelPic;
    }

    /**
     * 渠道品牌介绍的图片url
     * @param channelPic 渠道品牌介绍的图片url
     */
    public void setChannelPic(String channelPic) {
        this.channelPic = channelPic == null ? null : channelPic.trim();
    }

    /**
     * 渠道广告位介绍
     * @return ad_desc 渠道广告位介绍
     */
    public String getAdDesc() {
        return adDesc;
    }

    /**
     * 渠道广告位介绍
     * @param adDesc 渠道广告位介绍
     */
    public void setAdDesc(String adDesc) {
        this.adDesc = adDesc == null ? null : adDesc.trim();
    }

    /**
     * 渠道广告位介绍的图片url
     * @return ad_pic 渠道广告位介绍的图片url
     */
    public String getAdPic() {
        return adPic;
    }

    /**
     * 渠道广告位介绍的图片url
     * @param adPic 渠道广告位介绍的图片url
     */
    public void setAdPic(String adPic) {
        this.adPic = adPic == null ? null : adPic.trim();
    }

    /**
     * 限制广告主最低购买数量
     * @return unit_stock 限制广告主最低购买数量
     */
    public Integer getUnitStock() {
        return unitStock;
    }

    /**
     * 限制广告主最低购买数量
     * @param unitStock 限制广告主最低购买数量
     */
    public void setUnitStock(Integer unitStock) {
        this.unitStock = unitStock;
    }

    /**
     * 广告二维码总展现次数
     * @return stock_def 广告二维码总展现次数
     */
    public Integer getStockDef() {
        return stockDef;
    }

    /**
     * 广告二维码总展现次数
     * @param stockDef 广告二维码总展现次数
     */
    public void setStockDef(Integer stockDef) {
        this.stockDef = stockDef;
    }

    /**
     * 剩余展现次数
     * @return stock 剩余展现次数
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * 剩余展现次数
     * @param stock 剩余展现次数
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 展示一次的单价
     * @return unit_price 展示一次的单价
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * 展示一次的单价
     * @param unitPrice 展示一次的单价
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * 二维码url：域名?id=id
     * @return qr_code 二维码url：域名?id=id
     */
    public String getQrCode() {
        return qrCode;
    }

    /**
     * 二维码url：域名?id=id
     * @param qrCode 二维码url：域名?id=id
     */
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode == null ? null : qrCode.trim();
    }

    /**
     * 平台用户id
     * @return plat_user_id 平台用户id
     */
    public Long getPlatUserId() {
        return platUserId;
    }

    /**
     * 平台用户id
     * @param platUserId 平台用户id
     */
    public void setPlatUserId(Long platUserId) {
        this.platUserId = platUserId;
    }

    /**
     * 流量主服务状态 -1已停止 1已启动
     * @return status 流量主服务状态 -1已停止 1已启动
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 流量主服务状态 -1已停止 1已启动
     * @param status 流量主服务状态 -1已停止 1已启动
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", channelDesc=").append(channelDesc);
        sb.append(", channelPic=").append(channelPic);
        sb.append(", adDesc=").append(adDesc);
        sb.append(", adPic=").append(adPic);
        sb.append(", unitStock=").append(unitStock);
        sb.append(", stockDef=").append(stockDef);
        sb.append(", stock=").append(stock);
        sb.append(", unitPrice=").append(unitPrice);
        sb.append(", qrCode=").append(qrCode);
        sb.append(", platUserId=").append(platUserId);
        sb.append(", status=").append(status);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}