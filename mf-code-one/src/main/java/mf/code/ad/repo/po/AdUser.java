package mf.code.ad.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * ad_user
 * 用户查看广告的信息记录
 */
public class AdUser implements Serializable {
    /**
     * 用户浏览记录id
     */
    private Long id;

    /**
     * 流量主id
     */
    private Long adChannelId;

    /**
     * 商户id（广告主id）
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 查看的商户广告批次编号
     */
    private Long adPurchaseId;

    /**
     * 用户id地址
     */
    private String ip;

    /**
     * 浏览器信息
     */
    private String userAgent;

    /**
     * 省份
     */
    private String province;

    /**
     * 用户手机机型
     */
    private String deviceModel;

    /**
     * 用户操作系统版本
     */
    private String osVersion;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * ad_user
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户浏览记录id
     * @return id 用户浏览记录id
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户浏览记录id
     * @param id 用户浏览记录id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 流量主id
     * @return ad_channel_id 流量主id
     */
    public Long getAdChannelId() {
        return adChannelId;
    }

    /**
     * 流量主id
     * @param adChannelId 流量主id
     */
    public void setAdChannelId(Long adChannelId) {
        this.adChannelId = adChannelId;
    }

    /**
     * 商户id（广告主id）
     * @return merchant_id 商户id（广告主id）
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id（广告主id）
     * @param merchantId 商户id（广告主id）
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 查看的商户广告批次编号
     * @return ad_purchase_id 查看的商户广告批次编号
     */
    public Long getAdPurchaseId() {
        return adPurchaseId;
    }

    /**
     * 查看的商户广告批次编号
     * @param adPurchaseId 查看的商户广告批次编号
     */
    public void setAdPurchaseId(Long adPurchaseId) {
        this.adPurchaseId = adPurchaseId;
    }

    /**
     * 用户id地址
     * @return ip 用户id地址
     */
    public String getIp() {
        return ip;
    }

    /**
     * 用户id地址
     * @param ip 用户id地址
     */
    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    /**
     * 浏览器信息
     * @return user_agent 浏览器信息
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * 浏览器信息
     * @param userAgent 浏览器信息
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent == null ? null : userAgent.trim();
    }

    /**
     * 省份
     * @return province 省份
     */
    public String getProvince() {
        return province;
    }

    /**
     * 省份
     * @param province 省份
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * 用户手机机型
     * @return device_model 用户手机机型
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * 用户手机机型
     * @param deviceModel 用户手机机型
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel == null ? null : deviceModel.trim();
    }

    /**
     * 用户操作系统版本
     * @return os_version 用户操作系统版本
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * 用户操作系统版本
     * @param osVersion 用户操作系统版本
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion == null ? null : osVersion.trim();
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", adChannelId=").append(adChannelId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", adPurchaseId=").append(adPurchaseId);
        sb.append(", ip=").append(ip);
        sb.append(", userAgent=").append(userAgent);
        sb.append(", province=").append(province);
        sb.append(", deviceModel=").append(deviceModel);
        sb.append(", osVersion=").append(osVersion);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}