package mf.code.ad.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.ad.repo.po.AdPurchase;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AdPurchaseMapper extends BaseMapper<AdPurchase> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(AdPurchase record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(AdPurchase record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    AdPurchase selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(AdPurchase record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(AdPurchase record);

    /**
     * 原子性 减库存
     * @param adPurchaseId
     * @param stockNum
     * @return
     */
    int reduceStock(@Param("adPurchaseId") Long adPurchaseId, @Param("stockNum") int stockNum, @Param("utime") Date utime);
}
