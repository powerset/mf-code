package mf.code.ad.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * ad_purchase
 * 集客二维码广告购买 广告主（商户）购买广告位的购买记录
 */
public class AdPurchase implements Serializable {
    /**
     * 购买操作序号(批次编号)
     */
    private Long id;

    /**
     * 商户id（广告主id）
     */
    private Long merchantId;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 流量主id
     */
    private Long adChannelId;

    /**
     * 平台运营账户id
     */
    private Long platUserId;

    /**
     * 广告状态 -2暂停 1待启动 2启动 3已结束
     */
    private Integer status;

    /**
     * 下单时间
     */
    private Date orderTime;

    /**
     * 购买时间
     */
    private Date purchaseTime;

    /**
     * 购买展示数量
     */
    private Integer stockDef;

    /**
     * 剩余展示数量
     */
    private Integer stock;

    /**
     * 缴纳的保证金
     */
    private BigDecimal depositDef;

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    private Integer depositStatus;

    /**
     * 商户H5的宣传图（商户自己生成）
     */
    private String adPic;

    /**
     * 审核状态  -2审核不通过 1待审核 2审核通过
     */
    private Integer auditStatus;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 审核不通过原因
     */
    private String auditReason;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * ad_purchase
     */
    private static final long serialVersionUID = 1L;

    /**
     * 购买操作序号(批次编号)
     * @return id 购买操作序号(批次编号)
     */
    public Long getId() {
        return id;
    }

    /**
     * 购买操作序号(批次编号)
     * @param id 购买操作序号(批次编号)
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商户id（广告主id）
     * @return merchant_id 商户id（广告主id）
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id（广告主id）
     * @param merchantId 商户id（广告主id）
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 流量主id
     * @return ad_channel_id 流量主id
     */
    public Long getAdChannelId() {
        return adChannelId;
    }

    /**
     * 流量主id
     * @param adChannelId 流量主id
     */
    public void setAdChannelId(Long adChannelId) {
        this.adChannelId = adChannelId;
    }

    /**
     * 平台运营账户id
     * @return plat_user_id 平台运营账户id
     */
    public Long getPlatUserId() {
        return platUserId;
    }

    /**
     * 平台运营账户id
     * @param platUserId 平台运营账户id
     */
    public void setPlatUserId(Long platUserId) {
        this.platUserId = platUserId;
    }

    /**
     * 广告状态 -2暂停 1待启动 2启动 3已结束
     * @return status 广告状态 -2暂停 1待启动 2启动 3已结束
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 广告状态 -2暂停 1待启动 2启动 3已结束
     * @param status 广告状态 -2暂停 1待启动 2启动 3已结束
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 下单时间
     * @return order_time 下单时间
     */
    public Date getOrderTime() {
        return orderTime;
    }

    /**
     * 下单时间
     * @param orderTime 下单时间
     */
    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    /**
     * 购买时间
     * @return purchase_time 购买时间
     */
    public Date getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * 购买时间
     * @param purchaseTime 购买时间
     */
    public void setPurchaseTime(Date purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    /**
     * 购买展示数量
     * @return stock_def 购买展示数量
     */
    public Integer getStockDef() {
        return stockDef;
    }

    /**
     * 购买展示数量
     * @param stockDef 购买展示数量
     */
    public void setStockDef(Integer stockDef) {
        this.stockDef = stockDef;
    }

    /**
     * 剩余展示数量
     * @return stock 剩余展示数量
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * 剩余展示数量
     * @param stock 剩余展示数量
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 缴纳的保证金
     * @return deposit_def 缴纳的保证金
     */
    public BigDecimal getDepositDef() {
        return depositDef;
    }

    /**
     * 缴纳的保证金
     * @param depositDef 缴纳的保证金
     */
    public void setDepositDef(BigDecimal depositDef) {
        this.depositDef = depositDef;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @return deposit_status 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public Integer getDepositStatus() {
        return depositStatus;
    }

    /**
     * 保证金支付状态（-1已失效，0待付款，1已付款）
     * @param depositStatus 保证金支付状态（-1已失效，0待付款，1已付款）
     */
    public void setDepositStatus(Integer depositStatus) {
        this.depositStatus = depositStatus;
    }

    /**
     * 商户H5的宣传图（商户自己生成）
     * @return ad_pic 商户H5的宣传图（商户自己生成）
     */
    public String getAdPic() {
        return adPic;
    }

    /**
     * 商户H5的宣传图（商户自己生成）
     * @param adPic 商户H5的宣传图（商户自己生成）
     */
    public void setAdPic(String adPic) {
        this.adPic = adPic == null ? null : adPic.trim();
    }

    /**
     * 审核状态  -2审核不通过 1待审核 2审核通过
     * @return audit_status 审核状态  -2审核不通过 1待审核 2审核通过
     */
    public Integer getAuditStatus() {
        return auditStatus;
    }

    /**
     * 审核状态  -2审核不通过 1待审核 2审核通过
     * @param auditStatus 审核状态  -2审核不通过 1待审核 2审核通过
     */
    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    /**
     * 审核时间
     * @return audit_time 审核时间
     */
    public Date getAuditTime() {
        return auditTime;
    }

    /**
     * 审核时间
     * @param auditTime 审核时间
     */
    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    /**
     * 审核不通过原因
     * @return audit_reason 审核不通过原因
     */
    public String getAuditReason() {
        return auditReason;
    }

    /**
     * 审核不通过原因
     * @param auditReason 审核不通过原因
     */
    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason == null ? null : auditReason.trim();
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", shopId=").append(shopId);
        sb.append(", adChannelId=").append(adChannelId);
        sb.append(", platUserId=").append(platUserId);
        sb.append(", status=").append(status);
        sb.append(", orderTime=").append(orderTime);
        sb.append(", purchaseTime=").append(purchaseTime);
        sb.append(", stockDef=").append(stockDef);
        sb.append(", stock=").append(stock);
        sb.append(", depositDef=").append(depositDef);
        sb.append(", depositStatus=").append(depositStatus);
        sb.append(", adPic=").append(adPic);
        sb.append(", auditStatus=").append(auditStatus);
        sb.append(", auditTime=").append(auditTime);
        sb.append(", auditReason=").append(auditReason);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}