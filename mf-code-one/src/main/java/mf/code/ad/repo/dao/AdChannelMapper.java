package mf.code.ad.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.ad.repo.po.AdChannel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AdChannelMapper extends BaseMapper<AdChannel> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(AdChannel record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(AdChannel record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    AdChannel selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(AdChannel record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(AdChannel record);

    int reduceStock(@Param("adChannelId") Long adChannelId, @Param("stockNum") Integer stockNum, @Param("utime") Date utime);
}
