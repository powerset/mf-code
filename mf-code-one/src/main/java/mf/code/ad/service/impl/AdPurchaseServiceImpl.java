package mf.code.ad.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.ad.repo.dao.AdPurchaseMapper;
import mf.code.ad.repo.po.AdPurchase;
import mf.code.ad.service.AdPurchaseService;
import mf.code.common.constant.DepositStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * mf.code.ad.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 下午5:32
 */
@Slf4j
@Service
public class
AdPurchaseServiceImpl extends ServiceImpl<AdPurchaseMapper, AdPurchase> implements AdPurchaseService {
    private final AdPurchaseMapper adPurchaseMapper;

    @Autowired
    public AdPurchaseServiceImpl(AdPurchaseMapper adPurchaseMapper) {
        this.adPurchaseMapper = adPurchaseMapper;
    }

    @Override
    public int reduceStock(Long adPurchaseId, int stockNum) {
        return adPurchaseMapper.reduceStock(adPurchaseId, stockNum, new Date());
    }

    @Override
    public boolean updatePurchaseStatus(Long adPurchaseId) {
        Date now = new Date();
        AdPurchase adPurchase = new AdPurchase();
        adPurchase.setId(adPurchaseId);
        adPurchase.setPurchaseTime(now);
        adPurchase.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        adPurchase.setUtime(now);
        return adPurchaseMapper.updateById(adPurchase) == 1;
    }
}
