package mf.code.ad.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import mf.code.ad.repo.dao.AdChannelMapper;
import mf.code.ad.repo.po.AdChannel;
import mf.code.ad.service.AdChannelService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.ad.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-15 上午9:30
 */
@Slf4j
@Service
public class AdChannelServiceImpl extends ServiceImpl<AdChannelMapper, AdChannel> implements AdChannelService {
    @Autowired
    private AdChannelMapper adChannelMapper;

    @Override
    public int reduceStock(Long adChannelId, Integer stockNum) {
        return adChannelMapper.reduceStock(adChannelId, stockNum, new Date());
    }

    @Override
    public SimpleResponse selectError(Long adChannelId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        response.setMessage("网络异常，请稍候重试");
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("errorMessage", "未查到此流量主数据，to 测试妹子 adChannelId = "+adChannelId);
        response.setData(resultVO);
        return response;
    }
}
