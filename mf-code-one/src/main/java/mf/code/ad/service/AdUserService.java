package mf.code.ad.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.ad.repo.po.AdUser;

/**
 * mf.code.ad.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-20 下午7:10
 */
public interface AdUserService extends IService<AdUser> {

    void saveAdUser(String userAgent, String ip, Long adChannelId, Long merchantId, Long shopId, Long adPurchaseId);
}
