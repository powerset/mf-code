package mf.code.ad.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.ad.repo.po.AdChannel;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.ad.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-15 上午9:29
 */
public interface AdChannelService extends IService<AdChannel> {
    /**
     * 原子性减流量主库存
     * @param adChannelId 流量主id
     * @param stockNum 被购买的数量
     * @return 影响条数
     */
    int reduceStock(Long adChannelId, Integer stockNum);

    SimpleResponse selectError(Long adChannelId);
}
