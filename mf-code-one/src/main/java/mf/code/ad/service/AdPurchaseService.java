package mf.code.ad.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.ad.repo.po.AdPurchase;

/**
 * mf.code.ad.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 下午5:31
 */
public interface AdPurchaseService extends IService<AdPurchase> {
    int reduceStock(Long adPurchaseId, int stockNum);

    /**
     * 广告主付款成功后 更新调用
     * @return true 付款更新成功，false 付款更新失败
     */
    boolean updatePurchaseStatus(Long adPurchaseId);
}
