package mf.code.ad.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cz.mallat.uasparser.OnlineUpdater;
import cz.mallat.uasparser.UASparser;
import cz.mallat.uasparser.UserAgentInfo;
import lombok.extern.slf4j.Slf4j;
import mf.code.ad.repo.dao.AdUserMapper;
import mf.code.ad.repo.po.AdUser;
import mf.code.ad.service.AdUserService;
import mf.code.common.utils.IpUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * mf.code.ad.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-20 下午7:10
 */
@Slf4j
@Service
public class AdUserServiceImpl extends ServiceImpl<AdUserMapper, AdUser> implements AdUserService {
    private final AdUserMapper adUserMapper;

    @Autowired
    public AdUserServiceImpl(AdUserMapper adUserMapper) {
        this.adUserMapper = adUserMapper;
    }

    @Async
    @Override
    public void saveAdUser(String userAgent, String ip, Long adChannelId, Long merchantId, Long shopId, Long adPurchaseId) {
        // Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36
        if (StringUtils.isBlank(userAgent)) {
            log.info("User-Agent为空");
            return;
        }

        String provice = IpUtil.Ip2Provice(ip);

        String deviceModel;
        String osVersion;
        try {
            UASparser uaSparser = new UASparser(OnlineUpdater.getVendoredInputStream());
            UserAgentInfo userAgentInfo = uaSparser.parse(userAgent);
            deviceModel = userAgentInfo.getDeviceType();
            osVersion = userAgentInfo.getOsName();
        } catch (Exception e) {
            log.info("user-agent解析异常");
            return;
        }


        Date now = new Date();
        AdUser adUser = new AdUser();
        adUser.setAdChannelId(adChannelId);
        adUser.setMerchantId(merchantId);
        adUser.setShopId(shopId);
        adUser.setAdPurchaseId(adPurchaseId);
        adUser.setIp(ip);
        adUser.setUserAgent(userAgent);
        adUser.setProvince(provice);
        adUser.setDeviceModel(deviceModel);
        adUser.setOsVersion(osVersion);
        adUser.setCtime(now);
        adUser.setUtime(now);
        int rows = adUserMapper.insertSelective(adUser);
        if (rows == 0) {
            log.info("插入ad_user表数据失败");
        }

    }
}
