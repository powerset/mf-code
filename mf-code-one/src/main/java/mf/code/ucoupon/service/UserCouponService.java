package mf.code.ucoupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import mf.code.activity.repo.po.Activity;
import mf.code.ucoupon.repo.po.UserCoupon;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.ucoupon.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-09 8:31
 */
public interface UserCouponService extends IService<UserCoupon> {
    boolean saveSelective(UserCoupon userCoupon);

    /**
     * 通过uid 和 aid 查询 用户领奖的次数
     *
     * @param userId
     * @param activityId
     * @return
     */
    int countByUidAid(Long userId, Long activityId);

    UserCoupon insertSelective(UserCoupon userCoupon);

    int updateByPrimaryKeySelective(UserCoupon userCoupon);


    Integer countByParams(Map<String, Object> param);

    List<UserCoupon> listPageByparams(Map<String, Object> userCouponParam);

    /**
     * 计算用户轮盘累计获得金额
     *
     * @param userId
     * @param shopId
     * @return
     */
    BigDecimal sumLuckyWheelAmount(Long userId, Long shopId);

    Long cumWheelMoney(Long merchantId, Long shopId, Long userId);

    Map<String, Object> queryActivityDefUserJoinInfo(Long current, Long size, List<Long> activityIdList);

    boolean saveCouponUsersFromActivity(Activity activity, List<Long> couponUserList);

    boolean saveAmountUsersFromActivity(Activity activityDB, List<Long> amountUserList);

    /**
     * 用户奖金入库（未领奖状态），若不指定userCouponType的类型，默认存入activity转化之后对应的类型
     *
     * @param activity
     * @param userCouponType
     * @return
     */
    UserCoupon saveFromActivity(Activity activity, Integer userCouponType);

    /**
     * 拆红包 助力者信息入库
     *
     * @param activity
     * @param userCouponType
     * @param assistInfo
     */
    UserCoupon saveFromOpenRedPackageAssist(Activity activity, Integer userCouponType, Map assistInfo);

    /**
     * 更新 发奖状态 至 已领奖
     *
     * @param userCoupon
     */
    UserCoupon updateAward(UserCoupon userCoupon);

    Integer getUserCouponTypeFromActivityType(Integer type);

    /***
     * 根据条件获取下级用户所有的缴税汇总金额
     * @param param
     * @return
     */
    BigDecimal sumByTypeTotalCommission(Map<String, Object> param);

    int countByTypeTotalCommission(Map<String, Object> param);

    /**
     * 参与总人数
     *
     * @param param
     * @return
     */
    Long countUserAll(Map<String, Object> param);

    List<UserCoupon> queryByDefId(Long merchantId, Long shopId, Long activityDefId);

    List<UserCoupon> queryByDefIds(Long merchantId, Long shopId, Long userId, List<Long> activityDefIds, List<Integer> types);

    /**
     * 通过id查询优惠劵
     *
     * @param couponId
     * @return
     */
    UserCoupon findById(Long couponId);
}
