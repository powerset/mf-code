package mf.code.ucoupon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.utils.DateUtil;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.ucoupon.repo.dao.UserCouponMapper;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.ucoupon.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-09 8:31
 */
@Service
public class UserCouponServiceImpl extends ServiceImpl<UserCouponMapper, UserCoupon> implements UserCouponService {
    @Autowired
    private UserCouponMapper userCouponMapper;

    @Override
    public boolean saveSelective(UserCoupon userCoupon) {
        int i = userCouponMapper.insertSelective(userCoupon);
        return i != 0;
    }

    /**
     * 通过uid 和 aid 查询 用户领奖的次数 ，获奖状态为 1
     *
     * @param userId
     * @param activityId
     * @return
     */
    @Override
    public int countByUidAid(Long userId, Long activityId) {
        return userCouponMapper.countByUidAid(userId, activityId);
    }

    @Override
    public UserCoupon insertSelective(UserCoupon userCoupon) {
        int rows = userCouponMapper.insertSelective(userCoupon);
        if (rows == 0) {
            return null;
        }
        return userCoupon;
    }

    @Override
    public int updateByPrimaryKeySelective(UserCoupon userCoupon) {
        return userCouponMapper.updateByPrimaryKeySelective(userCoupon);
    }

    @Override
    public Integer countByParams(Map<String, Object> param) {
        return userCouponMapper.countByParams(param);
    }

    @Override
    public List<UserCoupon> listPageByparams(Map<String, Object> userCouponParam) {
        return userCouponMapper.listPageByparams(userCouponParam);
    }

    /**
     * 计算用户轮盘累计获得金额
     *
     * @param userId
     * @param shopId
     * @return
     */
    @Override
    public BigDecimal sumLuckyWheelAmount(Long userId, Long shopId) {

        Map<String, Object> params = new HashMap<>(4);
        params.put("type", UserCouponTypeEnum.LUCKY_WHEEL.getCode());
        params.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
        params.put("userId", userId);
        params.put("shopId", shopId);
        return userCouponMapper.sumLuckyWheelAmount(params);
    }

    @Override
    public Long cumWheelMoney(Long merchantId, Long shopId, Long userId) {
        Map m = new HashMap();
        m.put("merchantId", merchantId);
        m.put("shopId", shopId);
        m.put("userId", userId);
        return userCouponMapper.cumWheelMoney(m);
    }

    @Override
    public Map<String, Object> queryActivityDefUserJoinInfo(Long current, Long size, List<Long> activityIdList) {
        // 获取 此定义轮盘的所有参与者
        Page<UserCoupon> page = new Page<>(current, size);
        page.setDesc("amount");
        QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
        ucWrapper.in("activity_id", activityIdList);
        IPage<UserCoupon> userCouponIPage = this.page(page, ucWrapper);
        if (null == userCouponIPage) {
            return null;
        }
        List<UserCoupon> userCoupons = userCouponIPage.getRecords();
        if (CollectionUtils.isEmpty(userCoupons)) {
            return null;
        }
        // 所有用户id的集合

        List<Long> winnerUserIdList = new ArrayList<>();
        // 获取所有返现成功用户
        Map<Long, BigDecimal> cashbackUserMap = new HashMap<>();
        List<Long> allUserIdList = new ArrayList<>();
        for (UserCoupon userCoupon : userCoupons) {
            Long uid = userCoupon.getUserId();
            allUserIdList.add(uid);
            if (!new BigDecimal(0.00).equals(userCoupon.getAmount())) {
                cashbackUserMap.put(userCoupon.getUserId(), userCoupon.getAmount());
                winnerUserIdList.add(uid);
            }
        }

        // 返回数据
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("ipage", userCouponIPage);
        resultVO.put("allUserIdList", allUserIdList);
        resultVO.put("winnerUserIdList", winnerUserIdList);
        resultVO.put("cashbackUserMap", cashbackUserMap);
        return resultVO;
    }

    @Override
    public boolean saveCouponUsersFromActivity(Activity activity, List<Long> couponUserList) {
        List<UserCoupon> userCouponList = new ArrayList<>();
        Date now = new Date();
        for (Long userId : couponUserList) {
            UserCoupon userCoupon = new UserCoupon();
            userCoupon.setShopId(activity.getShopId());
            userCoupon.setMerchantId(activity.getMerchantId());
            userCoupon.setUserId(userId);
            userCoupon.setType(UserCouponTypeEnum.COUPON.getCode());
            userCoupon.setActivityId(activity.getId());
            userCoupon.setActivityDefId(activity.getActivityDefId());
            userCoupon.setAwardTime(now);
            userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
            userCoupon.setCtime(now);
            userCoupon.setUtime(now);
            userCouponList.add(userCoupon);
        }
        return this.saveBatch(userCouponList);
    }

    @Override
    public boolean saveAmountUsersFromActivity(Activity activity, List<Long> amountUserList) {
        List<UserCoupon> userCouponList = new ArrayList<>();
        Date now = new Date();
        for (Long userId : amountUserList) {
            UserCoupon userCoupon = new UserCoupon();
            userCoupon.setShopId(activity.getShopId());
            userCoupon.setMerchantId(activity.getMerchantId());
            userCoupon.setUserId(userId);
            userCoupon.setType(getUserCouponTypeFromActivityType(activity.getType()));
            userCoupon.setActivityId(activity.getId());
            userCoupon.setActivityDefId(activity.getActivityDefId());
            userCoupon.setAmount(activity.getPayPrice());
            userCoupon.setAwardTime(now);
            userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
            userCoupon.setCtime(now);
            userCoupon.setUtime(now);
            userCouponList.add(userCoupon);
        }
        return this.saveBatch(userCouponList);
    }

    @Override
    public UserCoupon saveFromActivity(Activity activity, Integer userCouponType) {
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setShopId(activity.getShopId());
        userCoupon.setMerchantId(activity.getMerchantId());
        userCoupon.setUserId(activity.getUserId());
        userCoupon.setType(getUserCouponTypeFromActivityType(activity.getType()));
        if (userCouponType != null) {
            userCoupon.setType(userCouponType);
        }
        userCoupon.setActivityId(activity.getId());
        userCoupon.setActivityDefId(activity.getActivityDefId());
        userCoupon.setActivityTaskId(null);
        userCoupon.setAmount(activity.getPayPrice());
        Date now = new Date();
        userCoupon.setAwardTime(now);
        userCoupon.setStatus(UserCouponStatusEnum.UNRECEIVED.getCode());
        userCoupon.setUtime(now);
        userCoupon.setCtime(now);
        boolean save = this.save(userCoupon);
        if (!save) {
            return null;
        }
        return userCoupon;
    }

    @Override
    public UserCoupon saveFromOpenRedPackageAssist(Activity activity, Integer userCouponType, Map assistInfo) {
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setShopId(activity.getShopId());
        userCoupon.setMerchantId(activity.getMerchantId());
        userCoupon.setUserId(Long.valueOf(assistInfo.get("userId").toString()));
        userCoupon.setType(getUserCouponTypeFromActivityType(activity.getType()));
        if (userCouponType != null) {
            userCoupon.setType(userCouponType);
        }
        userCoupon.setActivityId(activity.getId());
        userCoupon.setActivityDefId(activity.getActivityDefId());
        userCoupon.setActivityTaskId(null);
        userCoupon.setAmount((BigDecimal) assistInfo.get("amount"));
        Date now = new Date();
        userCoupon.setAwardTime(DateUtil.stringtoDate(assistInfo.get("time").toString(), DateUtil.POINT_DATE_FORMAT_TWO));
        userCoupon.setStatus(UserCouponStatusEnum.UNRECEIVED.getCode());
        userCoupon.setUtime(now);
        userCoupon.setCtime(now);
        boolean save = this.save(userCoupon);
        if (!save) {
            return null;
        }
        return userCoupon;

    }

    @Override
    public UserCoupon updateAward(UserCoupon userCoupon) {
        userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
        userCoupon.setUtime(new Date());
        boolean update = this.updateById(userCoupon);
        if (!update) {
            return null;
        }
        return userCoupon;

    }

    @Override
    public Integer getUserCouponTypeFromActivityType(Integer type) {
        if (type == ActivityTypeEnum.MCH_PLAN.getCode()) {
            return UserCouponTypeEnum.MCH_PLAN.getCode();
        }
        if (type == ActivityTypeEnum.LUCK_WHEEL.getCode()) {
            return UserCouponTypeEnum.LUCKY_WHEEL.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_START.getCode() || type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            return UserCouponTypeEnum.ORDER_BACK_START.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_JOIN.getCode() || type == ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode()) {
            return UserCouponTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            return UserCouponTypeEnum.NEW_MAN_START.getCode();
        }
        if (type == ActivityTypeEnum.NEW_MAN_JOIN.getCode()) {
            return UserCouponTypeEnum.NEW_MAN_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_REDPACK.getCode()) {
            return UserCouponTypeEnum.ORDER_RED_PACKET.getCode();
        }
        if (type == ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
            return UserCouponTypeEnum.FAV_CART.getCode();
        }
        if (type == ActivityTypeEnum.ASSIST.getCode() || type == ActivityTypeEnum.ASSIST_V2.getCode()) {
            return UserCouponTypeEnum.ASSIST.getCode();
        }
        if (type == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()) {
            return UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode();
        }
        if (type == ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode()) {
            return UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode();
        }
        return 0;
    }

    @Override
    public BigDecimal sumByTypeTotalCommission(Map<String, Object> param) {
        return this.userCouponMapper.sumByTypeTotalCommission(param);
    }

    @Override
    public int countByTypeTotalCommission(Map<String, Object> param) {
        return this.userCouponMapper.countByTypeTotalCommission(param);
    }

    /**
     * 参与总人数
     *
     * @param param
     * @return
     */
    @Override
    public Long countUserAll(Map<String, Object> param) {
        return userCouponMapper.countUserAll(param);
    }

    @Override
    public List<UserCoupon> queryByDefId(Long merchantId, Long shopId, Long activityDefId) {
        QueryWrapper<UserCoupon> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(UserCoupon::getMerchantId, merchantId)
                .eq(UserCoupon::getShopId, shopId)
                .eq(UserCoupon::getActivityDefId, activityDefId)
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
        ;

        return userCouponMapper.selectList(queryWrapper);
    }

    @Override
    public List<UserCoupon> queryByDefIds(Long merchantId, Long shopId, Long userId, List<Long> activityDefIds, List<Integer> types) {
        QueryWrapper<UserCoupon> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(UserCoupon::getMerchantId, merchantId)
                .eq(UserCoupon::getShopId, shopId)
                .eq(UserCoupon::getUserId, userId)
                .in(UserCoupon::getType, types)
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
        ;
        if (!CollectionUtils.isEmpty(activityDefIds)) {
            queryWrapper.and(params -> params.in("activity_def_id", activityDefIds));
        }
        queryWrapper.orderByDesc("id");

        return userCouponMapper.selectList(queryWrapper);
    }

    @Override
    public UserCoupon findById(Long couponId) {
        if (couponId == null) {
            return null;
        }
        return userCouponMapper.selectById(couponId);
    }
}
