package mf.code.ucoupon.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.ucoupon.repo.po.UserCoupon;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface UserCouponMapper extends BaseMapper<UserCoupon> {
    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录
     *
     * @param record
     */
    int insert(UserCoupon record);

    /**
     * 动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(UserCoupon record);

    /**
     * 根据指定主键获取一条数据库记录
     *
     * @param id
     */
    UserCoupon selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UserCoupon record);

    /**
     * 根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UserCoupon record);

    /**
     * 通过uid 和 aid 查询 用户领奖的次数 ，获奖状态为 1
     *
     * @param userId
     * @param activityId
     * @return
     */
    int countByUidAid(@Param("userId") Long userId, @Param("activityId") Long activityId);

    Integer countByParams(Map<String, Object> param);

    List<UserCoupon> listPageByparams(Map<String, Object> userCouponParam);

    BigDecimal sumLuckyWheelAmount(Map<String, Object> userCouponParam);

    Long cumWheelMoney(Map m);

    /***
     * 根据条件获取下级用户所有的缴税汇总金额
     * @param param
     * @return
     */
    BigDecimal sumByTypeTotalCommission(Map<String, Object> param);

    /***
     * 根据条件获取自己的收益
     * @param param
     * @return
     */
    int countByTypeTotalCommission(Map<String, Object> param);

    /**
     * 参与总人数
     * @param param
     * @return
     */
    Long countUserAll(Map<String, Object> param);

    int finishIssued(@Param("couponId") Long couponId);
}
