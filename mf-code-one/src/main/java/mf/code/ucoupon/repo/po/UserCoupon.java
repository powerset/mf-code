package mf.code.ucoupon.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * user_coupon
 * 用户奖品表 用户各种奖品扩充 比如：积分，优惠券，小礼品
 */
public class UserCoupon implements Serializable {
    /**
     * 用户奖品记录表ID主键
     */
    private Long id;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 用户id，user表的主键
     */
    private Long userId;

    /**
     * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6计划类抽奖活动 7抽奖免单任务 8免单商品抽奖参与者 9新手有礼发起者 10新手有礼抽奖参与者 11助力任务 12拆红包活动 13拆红包活动助力者 25:新手任务-拆红包 29:集客优惠券
     */
    private Integer type;

    /**
     * 活动ID, activity表的主键
     */
    private Long activityId;

    /**
     * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     */
    private Long activityDefId;

    /**
     * activity_task表id
     */
    private Long activityTaskId;

    /**
     * 奖金（元）
     */
    private BigDecimal amount;

    /**
     * 活动定义佣金和当时税率，{"commissionDef":10, "rate": 10}
     */
    private String commissionDef;

    /**
     * 所得的额外任务佣金
     */
    private BigDecimal commission;

    /**
     * 获奖时间
     */
    private Date awardTime;

    /**
     * 0：未领奖，1：已领奖，2已使用 3已过期
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 优惠券ID ,merchant_shop_coupon 的主键ID
     */
    private Long couponId;

    /**
     * user_coupon
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户奖品记录表ID主键
     * @return id 用户奖品记录表ID主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户奖品记录表ID主键
     * @param id 用户奖品记录表ID主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 用户id，user表的主键
     * @return user_id 用户id，user表的主键
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id，user表的主键
     * @param userId 用户id，user表的主键
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6计划类抽奖活动 7抽奖免单任务 8免单商品抽奖参与者 9新手有礼发起者 10新手有礼抽奖参与者 11助力任务 12拆红包活动 13拆红包活动助力者 25:新手任务-拆红包 29:集客优惠券
     * @return type 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6计划类抽奖活动 7抽奖免单任务 8免单商品抽奖参与者 9新手有礼发起者 10新手有礼抽奖参与者 11助力任务 12拆红包活动 13拆红包活动助力者 25:新手任务-拆红包 29:集客优惠券
     */
    public Integer getType() {
        return type;
    }

    /**
     * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6计划类抽奖活动 7抽奖免单任务 8免单商品抽奖参与者 9新手有礼发起者 10新手有礼抽奖参与者 11助力任务 12拆红包活动 13拆红包活动助力者 25:新手任务-拆红包 29:集客优惠券
     * @param type 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6计划类抽奖活动 7抽奖免单任务 8免单商品抽奖参与者 9新手有礼发起者 10新手有礼抽奖参与者 11助力任务 12拆红包活动 13拆红包活动助力者 25:新手任务-拆红包 29:集客优惠券
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 活动ID, activity表的主键
     * @return activity_id 活动ID, activity表的主键
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * 活动ID, activity表的主键
     * @param activityId 活动ID, activity表的主键
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     * @return activity_def_id 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     */
    public Long getActivityDefId() {
        return activityDefId;
    }

    /**
     * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     * @param activityDefId 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     */
    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * activity_task表id
     * @return activity_task_id activity_task表id
     */
    public Long getActivityTaskId() {
        return activityTaskId;
    }

    /**
     * activity_task表id
     * @param activityTaskId activity_task表id
     */
    public void setActivityTaskId(Long activityTaskId) {
        this.activityTaskId = activityTaskId;
    }

    /**
     * 奖金（元）
     * @return amount 奖金（元）
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 奖金（元）
     * @param amount 奖金（元）
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 活动定义佣金和当时税率，{"commissionDef":10, "rate": 10}
     * @return commission_def 活动定义佣金和当时税率，{"commissionDef":10, "rate": 10}
     */
    public String getCommissionDef() {
        return commissionDef;
    }

    /**
     * 活动定义佣金和当时税率，{"commissionDef":10, "rate": 10}
     * @param commissionDef 活动定义佣金和当时税率，{"commissionDef":10, "rate": 10}
     */
    public void setCommissionDef(String commissionDef) {
        this.commissionDef = commissionDef == null ? null : commissionDef.trim();
    }

    /**
     * 所得的额外任务佣金
     * @return commission 所得的额外任务佣金
     */
    public BigDecimal getCommission() {
        return commission;
    }

    /**
     * 所得的额外任务佣金
     * @param commission 所得的额外任务佣金
     */
    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    /**
     * 获奖时间
     * @return award_time 获奖时间
     */
    public Date getAwardTime() {
        return awardTime;
    }

    /**
     * 获奖时间
     * @param awardTime 获奖时间
     */
    public void setAwardTime(Date awardTime) {
        this.awardTime = awardTime;
    }

    /**
     * 0：未领奖，1：已领奖，2已使用 3已过期
     * @return status 0：未领奖，1：已领奖，2已使用 3已过期
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 0：未领奖，1：已领奖，2已使用 3已过期
     * @param status 0：未领奖，1：已领奖，2已使用 3已过期
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 优惠券ID ,merchant_shop_coupon 的主键ID
     * @return coupon_id 优惠券ID ,merchant_shop_coupon 的主键ID
     */
    public Long getCouponId() {
        return couponId;
    }

    /**
     * 优惠券ID ,merchant_shop_coupon 的主键ID
     * @param couponId 优惠券ID ,merchant_shop_coupon 的主键ID
     */
    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", shopId=").append(shopId);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", userId=").append(userId);
        sb.append(", type=").append(type);
        sb.append(", activityId=").append(activityId);
        sb.append(", activityDefId=").append(activityDefId);
        sb.append(", activityTaskId=").append(activityTaskId);
        sb.append(", amount=").append(amount);
        sb.append(", commissionDef=").append(commissionDef);
        sb.append(", commission=").append(commission);
        sb.append(", awardTime=").append(awardTime);
        sb.append(", status=").append(status);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", couponId=").append(couponId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}