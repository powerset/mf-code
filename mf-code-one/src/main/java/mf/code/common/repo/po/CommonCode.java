package mf.code.common.repo.po;

import java.io.Serializable;
import java.util.Date;

/**
 * common_code
 * 二维码表，活码管理与扫码计数
 */
public class CommonCode implements Serializable {
    /**
     * 自增主键id
     */
    private Long id;

    /**
     * 父级记录id
     */
    private Long parentId;

    /**
     * 用户id（user或者merchant）
     */
    private Long userId;

    /**
     * 用户类型（1：用户，2：商户）
     */
    private Integer userType;

    /**
     * 二维码编号
     */
    private String codeNo;

    /**
     * 二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）
     */
    private Integer codeType;

    /**
     * 二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）
     */
    private Integer codeStatus;

    /**
     * 二维码码文本地址
     */
    private String codeUrl;

    /**
     * 二维码码图片地址
     */
    private String codePath;

    /**
     * 可展现次数
     */
    private Integer scanLimit;

    /**
     * 剩余展现次数
     */
    private Integer scanNum;

    /**
     * 二维码生效时间
     */
    private Date startTime;

    /**
     * 二维码失效时间
     */
    private Date endTime;

    /**
     * 个性化信息（logo图，替换图等 logo_path中间图片，reserve_path替代图片）
     */
    private String extra;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer del;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * common_code
     */
    private static final long serialVersionUID = 1L;

    /**
     * 自增主键id
     * @return id 自增主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 自增主键id
     * @param id 自增主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 父级记录id
     * @return parent_id 父级记录id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 父级记录id
     * @param parentId 父级记录id
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 用户id（user或者merchant）
     * @return user_id 用户id（user或者merchant）
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id（user或者merchant）
     * @param userId 用户id（user或者merchant）
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 用户类型（1：用户，2：商户）
     * @return user_type 用户类型（1：用户，2：商户）
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * 用户类型（1：用户，2：商户）
     * @param userType 用户类型（1：用户，2：商户）
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * 二维码编号
     * @return code_no 二维码编号
     */
    public String getCodeNo() {
        return codeNo;
    }

    /**
     * 二维码编号
     * @param codeNo 二维码编号
     */
    public void setCodeNo(String codeNo) {
        this.codeNo = codeNo == null ? null : codeNo.trim();
    }

    /**
     * 二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）
     * @return code_type 二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）
     */
    public Integer getCodeType() {
        return codeType;
    }

    /**
     * 二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）
     * @param codeType 二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）
     */
    public void setCodeType(Integer codeType) {
        this.codeType = codeType;
    }

    /**
     * 二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）
     * @return code_status 二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）
     */
    public Integer getCodeStatus() {
        return codeStatus;
    }

    /**
     * 二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）
     * @param codeStatus 二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）
     */
    public void setCodeStatus(Integer codeStatus) {
        this.codeStatus = codeStatus;
    }

    /**
     * 二维码码文本地址
     * @return code_url 二维码码文本地址
     */
    public String getCodeUrl() {
        return codeUrl;
    }

    /**
     * 二维码码文本地址
     * @param codeUrl 二维码码文本地址
     */
    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl == null ? null : codeUrl.trim();
    }

    /**
     * 二维码码图片地址
     * @return code_path 二维码码图片地址
     */
    public String getCodePath() {
        return codePath;
    }

    /**
     * 二维码码图片地址
     * @param codePath 二维码码图片地址
     */
    public void setCodePath(String codePath) {
        this.codePath = codePath == null ? null : codePath.trim();
    }

    /**
     * 可展现次数
     * @return scan_limit 可展现次数
     */
    public Integer getScanLimit() {
        return scanLimit;
    }

    /**
     * 可展现次数
     * @param scanLimit 可展现次数
     */
    public void setScanLimit(Integer scanLimit) {
        this.scanLimit = scanLimit;
    }

    /**
     * 剩余展现次数
     * @return scan_num 剩余展现次数
     */
    public Integer getScanNum() {
        return scanNum;
    }

    /**
     * 剩余展现次数
     * @param scanNum 剩余展现次数
     */
    public void setScanNum(Integer scanNum) {
        this.scanNum = scanNum;
    }

    /**
     * 二维码生效时间
     * @return start_time 二维码生效时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 二维码生效时间
     * @param startTime 二维码生效时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 二维码失效时间
     * @return end_time 二维码失效时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 二维码失效时间
     * @param endTime 二维码失效时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 个性化信息（logo图，替换图等 logo_path中间图片，reserve_path替代图片）
     * @return extra 个性化信息（logo图，替换图等 logo_path中间图片，reserve_path替代图片）
     */
    public String getExtra() {
        return extra;
    }

    /**
     * 个性化信息（logo图，替换图等 logo_path中间图片，reserve_path替代图片）
     * @param extra 个性化信息（logo图，替换图等 logo_path中间图片，reserve_path替代图片）
     */
    public void setExtra(String extra) {
        this.extra = extra == null ? null : extra.trim();
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @return del 删除标识 0:未删除 1:已删除
     */
    public Integer getDel() {
        return del;
    }

    /**
     * 删除标识 0:未删除 1:已删除
     * @param del 删除标识 0:未删除 1:已删除
     */
    public void setDel(Integer del) {
        this.del = del;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", userId=").append(userId);
        sb.append(", userType=").append(userType);
        sb.append(", codeNo=").append(codeNo);
        sb.append(", codeType=").append(codeType);
        sb.append(", codeStatus=").append(codeStatus);
        sb.append(", codeUrl=").append(codeUrl);
        sb.append(", codePath=").append(codePath);
        sb.append(", scanLimit=").append(scanLimit);
        sb.append(", scanNum=").append(scanNum);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", extra=").append(extra);
        sb.append(", del=").append(del);
        sb.append(", ctime=").append(ctime);
        sb.append(", utime=").append(utime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}