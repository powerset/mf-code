package mf.code.common.repo.dto;

import lombok.Data;

/**
 * mf.code.common.repo.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-25 17:30
 */
@Data
public class RedirectDTO {
    private String originUrl;
    private String targetUrl;
    private Boolean canRedirect;
}
