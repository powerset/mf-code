package mf.code.common.repo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mf.code.common.repo.po.CommonCode;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonCodeMapper extends BaseMapper<CommonCode> {
    /**
     *  根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录
     *
     * @param record
     */
    int insert(CommonCode record);

    /**
     *  动态字段,写入数据库记录
     *
     * @param record
     */
    int insertSelective(CommonCode record);

    /**
     *  根据指定主键获取一条数据库记录
     *
     * @param id
     */
    CommonCode selectByPrimaryKey(Long id);

    /**
     *  动态字段,根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(CommonCode record);

    /**
     *  根据主键来更新符合条件的数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(CommonCode record);
}
