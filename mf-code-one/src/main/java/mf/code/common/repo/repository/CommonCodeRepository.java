package mf.code.common.repo.repository;

import mf.code.common.repo.po.CommonCode;

import java.util.List;

/**
 * mf.code.common.repo.repository
 * Description:
 *
 * @author gel
 * @date 2019-06-21 15:48
 */
public interface CommonCodeRepository {

    /**
     * 创建或修改二维码记录
     * @return
     */
    Integer createOrUpdate(CommonCode commonCode);

    List<CommonCode> queryCommonCodeList(Long userId, Integer userType, Integer codeType);

    CommonCode queryCommonCodeByCodeNo(String codeNo);

    CommonCode queryCommonCodeById(Long id);

    List<CommonCode> queryCommonCodeByParentId(Long parentId);
}
