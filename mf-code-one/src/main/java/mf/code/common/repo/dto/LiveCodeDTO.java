package mf.code.common.repo.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.common.repo.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-21 16:26
 */
@Data
public class LiveCodeDTO {
    /**
     * 二维码id
     */
    private Long codeId;
    /**
     * 二维码编号
     */
    private Long codeNo;
    /**
     * 生成人
     */
    private Long userId;
    /**
     * 生成人类型
     */
    private Integer userType;
    /**
     * 活码名称
     */
    private String liveName;
    /**
     * 活码可扫次数，也可直接从liveCodeList直接遍历累加出来
     */
    private Integer scanLimit;
    /**
     * 生效时间，可以服务器直接赋值
     */
    private String startTime;
    /**
     * 失效时间，应该是从liveCodeList直接遍历比较出来的。
     */
    private String endTime;
    /**
     * 活码中间小图片
     */
    private String logoPath;
    /**
     * liveCodeList如果都失效了则用来替换展示的图片。
     */
    private String reservePath;
    /**
     * 微信群二维码列表
     */
    private List<WeChatCodeDTO> liveCodeList;

}
