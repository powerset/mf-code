package mf.code.common.repo.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.common.repo.dao.CommonCodeMapper;
import mf.code.common.repo.po.CommonCode;
import mf.code.common.repo.repository.CommonCodeRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mf.code.common.repo.repository.impl
 * Description:
 *
 * @author gel
 * @date 2019-06-21 15:59
 */
@Service
public class CommonCodeRepositoryImpl implements CommonCodeRepository {

    @Autowired
    private CommonCodeMapper commonCodeMapper;

    /**
     * 创建或修改二维码记录
     *
     * @param commonCode
     * @return
     */
    @Override
    public Integer createOrUpdate(CommonCode commonCode) {
        if (commonCode == null) {
            return 0;
        }
        commonCode.setUtime(new Date());
        if (commonCode.getId() == null) {
            return commonCodeMapper.insert(commonCode);
        } else {
            return commonCodeMapper.updateByPrimaryKeySelective(commonCode);
        }
    }

    /**
     * 根据生成者id，类型，码类型
     *
     * @return
     */
    @Override
    public List<CommonCode> queryCommonCodeList(Long userId, Integer userType, Integer codeType) {
        QueryWrapper<CommonCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CommonCode::getUserId, userId)
                .eq(CommonCode::getUserType, userType)
                .eq(CommonCode::getCodeType, codeType);
        return commonCodeMapper.selectList(queryWrapper);
    }

    /**
     * 根据codeNo查询
     *
     * @param codeNo
     * @return
     */
    @Override
    public CommonCode queryCommonCodeByCodeNo(String codeNo) {
        if (StringUtils.isNotBlank(codeNo)) {
            return null;
        }
        QueryWrapper<CommonCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(CommonCode::getCodeNo, codeNo);
        return commonCodeMapper.selectOne(queryWrapper);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Override
    public CommonCode queryCommonCodeById(Long id) {
        if (id == null) {
            return null;
        }
        return commonCodeMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据parentId查询
     *
     * @param parentId
     * @return
     */
    @Override
    public List<CommonCode> queryCommonCodeByParentId(Long parentId) {
        if (parentId == null) {
            return null;
        }
        QueryWrapper<CommonCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(CommonCode::getParentId, parentId);
        return commonCodeMapper.selectList(queryWrapper);
    }


}
