package mf.code.common.repo.dto;

import lombok.Data;

/**
 * mf.code.common.repo.dto
 * Description:
 *
 * @author gel
 * @date 2019-06-21 16:40
 */
@Data
public class WeChatCodeDTO {
    /**
     * 排序，读取顺序
     */
    private Integer sort;
    /**
     * 微信图片地址
     */
    private String weChatPath;
    /**
     * 微信图片类型
     */
    private Integer weChatType;
    /**
     * 失效时间，默认7天
     */
    private String endTime;
    /**
     * 微信图片url
     */
    private String weChatUrl;
    /**
     * 可扫描次数
     */
    private Integer scanNum;

}
