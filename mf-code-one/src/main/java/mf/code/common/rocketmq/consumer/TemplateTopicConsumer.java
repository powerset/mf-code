package mf.code.common.rocketmq.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.common.constant.RocketMqTopicTagEnum;
import mf.code.common.repo.po.CommonDict;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

/**
 * mf.code.common.rocketmq.consumer
 * Description:
 *
 * @author gel
 * @date 2019-05-06 19:04
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "template", consumerGroup = "${spring.application.name}")
public class TemplateTopicConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;

    @Override
    public void onMessage(MessageExt message) {
        String bizVale = new String(message.getBody(), Charset.forName("UTF-8"));
        String msgId = message.getMsgId();
        String tags = message.getTags();
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTag(), tags)) {
            checkpoint(msgId, bizVale);
        }
        if (StringUtils.equalsIgnoreCase(RocketMqTopicTagEnum.TEMPLATE_PUSH_RECALL.getTag(), tags)) {
            pushRecall(msgId, bizVale);
        }
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        // set consumer consume message from now
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

    private void checkpoint(String msgId, String bizVale) {
        log.info("财富闯关消息ID：" + msgId + "，消息体：" + bizVale);
        JSONObject jsonObject = JSON.parseObject(bizVale);
        sendTemplateMessageService.sendTemplateMsg2CheckpointFinish(jsonObject.getLong("merchantId"),
                jsonObject.getLong("shopId"),
                jsonObject.getLong("userId"),
                jsonObject.getIntValue("finishNum"),
                JSONObject.parseObject(jsonObject.get("commonDict").toString(), CommonDict.class));
        log.info("财富闯关消息ID：" + msgId + "消费成功");
    }

    private void pushRecall(String msgId, String bizVale) {
        log.info("不活跃粉丝召回ID：" + msgId + "，消息体：" + bizVale);
        JSONObject jsonObject = JSON.parseObject(bizVale);
        /**
         * 推送行为
         */
        sendTemplateMessageService.sendWxPushRecall(
                jsonObject.getLong("merchantId"),
                jsonObject.getLong("shopId"),
                jsonObject.getLong("userId"),
                jsonObject.getLong("pushId"),
                jsonObject.getIntValue("activityType"));
        log.info("不活跃粉丝召回ID：" + msgId + "消费成功");
    }
}
