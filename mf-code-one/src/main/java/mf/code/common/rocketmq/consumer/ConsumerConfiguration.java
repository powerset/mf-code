//package mf.code.common.rocketmq.consumer;
//
//import lombok.extern.slf4j.Slf4j;
//import mf.code.common.rocketmq.consumer.processor.ConsumerListenerProcessor;
//import mf.code.common.rocketmq.exception.RocketMQException;
//import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
//import org.apache.rocketmq.client.exception.MQClientException;
//import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
//import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.SpringBootConfiguration;
//import org.springframework.context.annotation.Bean;
//
///**
// * mf.code.common.rocketmq.consumer
// * Description:
// *
// * @author: gel
// * @date: 2019-02-21 15:36
// */
//@Slf4j
//@SpringBootConfiguration
//public class ConsumerConfiguration {
//    @Value("${rocketmq.consumer.open}")
//    private String open;
//    @Value("${rocketmq.consumer.namesrvAddr}")
//    private String namesrvAddr;
//    @Value("${rocketmq.consumer.groupName}")
//    private String groupName;
//    @Value("${rocketmq.consumer.consumeThreadMin}")
//    private int consumeThreadMin;
//    @Value("${rocketmq.consumer.consumeThreadMax}")
//    private int consumeThreadMax;
//    @Value("${rocketmq.consumer.topics}")
//    private String topics;
//    @Value("${rocketmq.consumer.consumeMessageBatchMaxSize}")
//    private int consumeMessageBatchMaxSize;
//    @Autowired
//    private ConsumerListenerProcessor consumeMsgListenerProcessor;
//
//    @Bean
//    public DefaultMQPushConsumer getRocketMQConsumer() throws RocketMQException {
//        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(groupName);
//        consumer.setNamesrvAddr(namesrvAddr);
//        consumer.setConsumeThreadMin(consumeThreadMin);
//        consumer.setConsumeThreadMax(consumeThreadMax);
//        consumer.registerMessageListener(consumeMsgListenerProcessor);
//        /**
//         * 设置Consumer第一次启动是从队列头部开始消费还是队列尾部开始消费
//         * 如果非第一次启动，那么按照上次消费的位置继续消费
//         */
//        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
//        /**
//         * 设置消费模型，集群还是广播，默认为集群
//         */
//        consumer.setMessageModel(MessageModel.CLUSTERING);
//        /**
//         * 设置一次消费消息的条数，默认为1条
//         */
//        consumer.setConsumeMessageBatchMaxSize(consumeMessageBatchMaxSize);
//        try {
//            /**
//             * 设置该消费者订阅的主题和tag，如果是订阅该主题下的所有tag，则tag使用*；如果需要指定订阅该主题下的某些tag，则使用||分割，例如tag1||tag2||tag3
//             */
//            String[] topicTagsArr = topics.split(";");
//            for (String topicTags : topicTagsArr) {
//                String[] topicTag = topicTags.split("~");
//                consumer.subscribe(topicTag[0], topicTag[1]);
//            }
//            if("1".equals(open)){
//                consumer.start();
//                log.info(String.format("consumer is start !!! groupName:[%s],namesrvAddr:[%s],topics:[%s]", groupName, namesrvAddr, topics));
//            }
//        } catch (MQClientException e) {
//            log.error(String.format("consumer start error !!! groupName:[%s],namesrvAddr:[%s],topics:[%s]", groupName, namesrvAddr, topics), e);
//            throw new RocketMQException(e);
//        }
//        return consumer;
//    }
//}
