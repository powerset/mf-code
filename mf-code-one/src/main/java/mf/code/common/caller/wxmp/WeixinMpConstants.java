package mf.code.common.caller.wxmp;

/**
 * @description: 常量
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月25日 16:33
 */
public class WeixinMpConstants {
    /**
     * 获取小程序的token
     */
    public static final String DOMAIN_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";
    /**
     * 获取没限制的小程序二维码
     */
    public static final String DOMAIN_WX_ACODE_UNLIMIT = "https://api.weixin.qq.com/wxa/getwxacodeunlimit";

    /***
     * 发送模板消息
     */
    public static final String DOMAIN_WX_SENDTEMPLATEMESSAGE = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send";

    /***
     * 发送客服消息给用户
     */
    public static final String DOMAIN_WX_SENDCUSTOMERMESSAGE = "https://api.weixin.qq.com/cgi-bin/message/custom/send";

    /**
     * 必须是已经发布的小程序存在的页面（否则报错），例如 pages/index/index, 根路径前不要填加 /,不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
     */
    /***
     * 集客魔方落地页
     */
    public static final String PAGE = "pages/index/login";

    /***
     * 集客师业务落地页
     */
    public static final String PAGE_TEACHER_INIT = "pages/login/login";

    /**
     * 默认为 430px，最小 280px，最大 1280px
     */
    public static final Integer WIDTH = 430;


    /**
     * 小程序相关二维码目录地址
     */
    public static final String WXCODEMERCHANTPATH = "wxcode/merchant/";//商户
    public static final String WXCODESHOPPATH = "wxcode/shop/";//店铺
    public static final String WXCODESHOPPACKAGEPATH = "wxcode/shop/package";//包裹
    public static final String WXCODESHOPPOSTERPATH = "wxcode/shop/poster";//海报用途的小程序码
    public static final String APPLETACTIVITYSHAREPOSTERPATH = "poster/shop/";//小程序的活动海报 poter/shop/shopid/uid/aid


    public static final String WXCODE_GOODS_PATH = "wxcode/shop/goods/";//商品用途的小程序码
    public static final String WXCODE_ACTIVITY_FILLORDER_PATH = "wxcode/shop/activity/fillorder/";//回填订单用途的小程序码
    public static final String WXCODE_ACTIVITY_GOODCOMMENT_PATH = "wxcode/shop/activity/goodcomment/";//好评晒图用途的小程序码
    public static final String WXCODE_ACTIVITY_OPENREDPACK_PATH = "wxcode/shop/activity/openredpack/";//拆红包用途的小程序码
    public static final String WXCODE_ACTIVITY_ASSIST_PATH = "wxcode/shop/activity/assist/";//助力用途的小程序码
    public static final String WXCODE_ACTIVITY_LUCKYWHEEL_PATH = "wxcode/shop/activity/luckywheel/";//幸运大转盘用途的小程序码
    public static final String WXCODE_ACTIVITY_FAVCART_PATH = "wxcode/shop/activity/favcart/";//收藏加购用途的小程序码
    public static final String WXCODE_ACTIVITY_ORDERBACK_PATH = "wxcode/shop/activity/orderback/";//免单抽奖用途的小程序码
    public static final String WXCODE_ACTIVITY_FULL_REIMBURSEMENT_PATH = "wxcode/shop/activity/fullReimbursement/";//全额报销用途的小程序码


    /***
     * 小程序码场景编号
     */
    public static final Integer SCENE_MERCHANT = 1;//商户场景生成的小程序码
    public static final Integer SCENE_SHOP = 2;//店铺场景生成的小程序码
    public static final Integer SCENE_PACKAGE = 3;//包裹场景生成的小程序码
    public static final Integer SCENE_POSTER = 4;//海报场景生成的小程序码

    public static final Integer SCENE_GOODS = 5;//商品场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_FILLORDER = 6;//回填订单活动场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_FULL_REIMBURSEMENT = 32;//全额报销活动场景生成的小程序码

    public static final Integer SCENE_ACTIVITY_GOODCOMMENT = 7;//好评晒图活动场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_GOODCOMMENT20 = 20;//好评晒图活动场景生成的小程序码 前端解析定义 去除商品id

    public static final Integer SCENE_ACTIVITY_OPENREDPACK = 8;//拆红包活动场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_ASSIST = 9;//助力活动场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_LUCKYWHEEL = 10;//幸运大转盘活动场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_FAVCART = 11;//收藏加购活动场景生成的小程序码
    public static final Integer SCENE_ACTIVITY_ORDERBACK = 12;//免单抽奖活动场景生成的小程序码




    /***
     * 集客师小程序目录相关
     */
    public static final String WXCODETEACHERPOSTERPATH = "wxcode/teacher/poster/";//海报用途的小程序码
    public static final String WXCODETEACHERH5POSTERPATH = "wxcode/teacher/h5poster/";//海报用途的小程序码

    /***
     * 集客师小程序场景相关
     */
    public static final Integer SCENE_TEANCHER_INVITETEACHER = 1;//邀请集客师场景的小程序码


}
