package mf.code.common.caller.wxpay;

/**
 * @description: 常量
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月25日 16:33
 */
public class WeixinPayConstants {
    public static final String JSAPI = "JSAPI";
    public static final String NATIVE = "NATIVE";
    public static final String APP = "APP";
    public static final String MKTTRANSFER = "MKTTRANSFER";

    public static final Integer PAYSCENE_PUB_APPID = 1;//使用公众号appid的支付
    public static final Integer PAYSCENE_APPLET_APPID = 2;//使用小程序appid的支付
    public static final Integer PAYSCENE_APPLET_TEACHER_APPID = 3;//使用集客师小程序appid的支付

    public static final String APPLET_NOTIFY_SOURCE = "applet";
    public static final String SELLER_NOTIFY_SOURCE = "seller";

    public static final String APPLET_NOTIFY_SOURCE_NEW = "applet_new";


    /********集客魔方***********/
    public static final String APPLETE_BODY = "集客魔方-活动充值";
    public static final String BODY = "集客魔方";
    public static final String MERCHANTSELLER_BODY_CREATEPAY = "创建活动付款";
    public static final String MERCHANTSELLER_BODY_ADDSTOCKPAY = "补库存付款";

    public static final String MERCHANTSELLER_BODY_BUY = "购买";
    public static final String MERCHANTSELLER_BODY_RENEW = "续费";
    public static final String MERCHANTSELLER_BODY_LEVELUP = "升级";
}
