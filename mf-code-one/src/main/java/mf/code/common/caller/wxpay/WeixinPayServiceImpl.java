package mf.code.common.caller.wxpay;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class WeixinPayServiceImpl implements WeixinPayService {
    @Autowired
    private WxpayProperty wxpayProperty;

    /***
     * 封装企业提现支付入参
     * @param openID(用户openid)
     * @param userName 用户名(可传项)
     * @param amount 金额
     * @param appletName 小程序名
     * @return
     */
    @Override
    public Map<String, Object> getTransferReqParams(String openID, String userName, BigDecimal amount, String appletName, Integer payScene) {
        //拼接微信接口参数
        Map<String, Object> map = new HashMap<String, Object>();
        if (payScene == WeixinPayConstants.PAYSCENE_PUB_APPID.intValue()) {
            map.put("mch_appid", wxpayProperty.getPubAppId()); //商户账号appid==公众号appid
        } else if (payScene == WeixinPayConstants.PAYSCENE_APPLET_APPID.intValue()) {
            map.put("mch_appid", wxpayProperty.getMfAppId()); //商户账号appid==集客魔方小程序appid
        } else if (payScene == WeixinPayConstants.PAYSCENE_APPLET_TEACHER_APPID.intValue()) {
            map.put("mch_appid", wxpayProperty.getTeacherAppId()); //商户账号appid==集客师小程序appid
        }

        map.put("mchid", wxpayProperty.getMchId());//商户号
        //选填 设备号	device_info  String(32) 微信支付分配的终端设备号
        String nonceStr = RandomStrUtil.randomStr(28);
        map.put("nonce_str", nonceStr);//随机字符串
        String partnerTradeNo = "V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        map.put("partner_trade_no", partnerTradeNo);//商户订单号
        map.put("openid", openID);//用户openid
        if (StringUtils.isNotBlank(userName)) {
            map.put("check_name", "FORCE_CHECK");//校验用户姓名选项
            map.put("re_user_name", userName);//选填 收款用户姓名
        } else {
            map.put("check_name", "NO_CHECK");
        }
        BigDecimal transAmt = amount.multiply(new BigDecimal(100));//元转化为分
        map.put("amount", transAmt.intValue());//金额 单位分
        map.put("desc", appletName);//企业付款描述信息
        map.put("spbill_create_ip", IpUtil.getServerIp());//Ip地址

        String signTemp = SortUtil.buildSignStr(map);
        String sign = signTemp + "&key=" + wxpayProperty.getMchSecretKey();
        sign = MD5Util.md5(sign).toUpperCase();//注：MD5签名方式
        map.put("sign", sign);//签名	sign String(32)

        return map;
    }

    /**
     * 企业提现支付
     *
     * @param reqParams 入参
     * @return
     */
    @Override
    public Map<String, Object> transfer(Map<String, Object> reqParams) {
        //将map入参按照微信接口的xml格式传入
        String bodyXml = WXPayUtil.mapToXml(reqParams, false);
        //调用xml参数，post请求调用微信接口
        String certPath = wxpayProperty.getCertPath() + "/apiclient_cert.p12";
        String resp = CertHttpUtil.ssl(wxpayProperty.getPayTransfersUrl(), bodyXml, wxpayProperty.getMchId(), certPath);
        //对返回的结果进行处理
        Map<String, Object> xmlMap = WXPayUtil.xmlToMap(resp);

        //用于验证支付接口的参数
        xmlMap.put("nonce_str", reqParams.get("nonce_str"));
        xmlMap.put("sign", reqParams.get("sign"));
        xmlMap.put("partner_trade_no", reqParams.get("partner_trade_no"));

        log.debug("提现过程调用微信接口的返回：{}", xmlMap);
        return xmlMap;
    }

    /***
     * 封装统一下单的入参
     * @param price 价格 单位元
     * @param outTradeNo 订单编号
     * @param ip ip
     * @param appid appid or 小程序(根据哪个appid入参)
     * @return
     */
    @Override
    public Map<String, Object> getUnifiedorderReqParams(BigDecimal price,
                                                        String outTradeNo,
                                                        String ip,
                                                        String appid,
                                                        String tradeType,
                                                        String attach,
                                                        String openID,
                                                        String notifySource,
                                                        String body) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("appid", appid);
        //商品描述	body
        map.put("body", body);
        //交易类型	trade_type
        map.put("trade_type", tradeType);
        //商户号	mch_id
        map.put("mch_id", wxpayProperty.getMchId());
        //随机字符串	nonce_str
        String nonceStr = RandomStrUtil.randomStr(28);
        map.put("nonce_str", nonceStr);
        //商户订单号	out_trade_no
        map.put("out_trade_no", outTradeNo);
        //标价币种	fee_type
        map.put("fee_type", "CNY");
        BigDecimal transTotalFee = price.multiply(new BigDecimal(100));//元转化为分
        //标价金额	total_fee  单位为分
        map.put("total_fee", transTotalFee.intValue());
        //终端IP	spbill_create_ip
        map.put("spbill_create_ip", ip);
        if (StringUtils.isNotBlank(attach)) {//非必填
            map.put("attach", ConvertUtil.toUtf8String(attach));
            log.info(">>>>>>>>>> attach: {}", attach);
        }

        /**起始时间，结束时间 是随机选填字段，可省去。建议：最短失效时间间隔大于1分钟 */
        //交易起始时间
        map.put("time_start", DateFormatUtils.format(new Date(), "yyyyMMddHHmmss"));
        //订单失效时间
        map.put("time_expire", DateFormatUtils.format(DateUtils.addHours(new Date(), 2), "yyyyMMddHHmmss"));

        if (WeixinPayConstants.SELLER_NOTIFY_SOURCE.equals(notifySource)) {
            map.put("notify_url", wxpayProperty.getSellerPayUnifiedNotifyUrl());//通知地址	notify_url
        } else if (WeixinPayConstants.APPLET_NOTIFY_SOURCE_NEW.equals(notifySource)) {
            map.put("notify_url", wxpayProperty.getPayUnifiedNotifyUrlNew());//新版通知地址	notify_url
        } else {
            map.put("notify_url", wxpayProperty.getPayUnifiedNotifyUrl());//通知地址	notify_url
        }
        if (StringUtils.isNotBlank(openID)) {
            map.put("openid", openID);//用户唯一标识
        }

        String sortSign = SortUtil.buildSignStr(map);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        map.put("sign", sign);//签名	sign

        return map;
    }

    /***
     * 封装统一下单的参数(h5)
     * @param price 价格 单位元
     * @param outTradeNo 订单编号
     * @param ip ip
     * @param appid 支付场景  公众号 or 小程序(根据哪个appid入参)
     * @param body 内容
     * @param platform 支付平台 1：pc 2：安卓 3：ios 4:wap网站
     * @return
     */
    @Override
    public Map<String, Object> getUnifiedorderReqParamsH5(BigDecimal price,
                                                          String outTradeNo,
                                                          String ip,
                                                          String appid,
                                                          String attach,
                                                          String openID,
                                                          String notifySource,
                                                          String body,
                                                          Integer platform) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("appid", appid);
        //商户号	mch_id
        map.put("mch_id", wxpayProperty.getMchId());
        //随机字符串	nonce_str
        String nonceStr = RandomStrUtil.randomStr(28);
        map.put("nonce_str", nonceStr);
        //商品描述	body
        map.put("body", body);
        //非必填
        if (StringUtils.isNotBlank(attach)) {
            map.put("attach", ConvertUtil.toUtf8String(attach));
        }
        //商户订单号	out_trade_no
        map.put("out_trade_no", outTradeNo);
        //标价币种	fee_type
        map.put("fee_type", "CNY");
        BigDecimal transTotalFee = price.multiply(new BigDecimal(100));//元转化为分
        //标价金额	total_fee  单位为分
        map.put("total_fee", transTotalFee.intValue());
        //终端IP	spbill_create_ip
        map.put("spbill_create_ip", ip);
        /**起始时间，结束时间 是随机选填字段，可省去。建议：最短失效时间间隔大于5分钟 */
        //交易起始时间
        map.put("time_start", DateFormatUtils.format(new Date(), "yyyyMMddHHmmss"));
        //订单失效时间
        map.put("time_expire", DateFormatUtils.format(DateUtils.addHours(new Date(), 2), "yyyyMMddHHmmss"));
        if (WeixinPayConstants.SELLER_NOTIFY_SOURCE.equals(notifySource)) {
            map.put("notify_url", wxpayProperty.getSellerPayUnifiedNotifyUrl());//通知地址	notify_url
        } else {
            map.put("notify_url", wxpayProperty.getPayUnifiedNotifyUrl());//通知地址	notify_url
        }
        if (StringUtils.isNotBlank(openID)) {
            map.put("openid", openID);//用户唯一标识
        }
        //交易类型	trade_type
        map.put("trade_type", "MWEB");

        if (WeixinPayConstants.SELLER_NOTIFY_SOURCE.equals(notifySource)) {
            map.put("notify_url", wxpayProperty.getSellerPayUnifiedNotifyUrl());//通知地址	notify_url
        } else {
            map.put("notify_url", wxpayProperty.getPayUnifiedNotifyUrl());//通知地址	notify_url
        }
        if (StringUtils.isNotBlank(openID)) {
            map.put("openid", openID);//用户唯一标识
        }

        map.put("scene_info", this.getH5SceneInfo(platform, body));

        String sortSign = SortUtil.buildSignStr(map);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        map.put("sign", sign);//签名	sign
        return map;
    }

    private Map getH5SceneInfo(Integer platform, String body) {
        Map h5 = new HashMap();

        Map map = new HashMap();
        if (platform == 2) {
            //TODO:
        } else if (platform == 3) {
            //TODO:
        } else if (platform == 4) {
            map.put("type", "Wap");
            map.put("wap_url", "https://applet.wxyundian.com");
            map.put("wap_name", body);
        }
        h5.put("h5_info", map);

        return h5;
    }

    /**
     * 功能描述: 统一下单支付 请求不需要证书
     *
     * @param: [price] 支付金额 单位元
     * @return: java.util.Map<java.lang.String                                                                                                                               ,                                                                                                                               java.lang.Object>
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/11 0011 15:48
     */
    @Override
    public Map<String, Object> unifiedorder(Map<String, Object> mapReqParams) {
        //将map入参按照微信接口的xml格式传入
        log.info("<<<<<<<<<< unifiedorder req: {}", mapReqParams);
        String bodyXml = WXPayUtil.mapToXml(mapReqParams, false);

        String resp = CertHttpUtil.withoutSSL(wxpayProperty.getPayUnifiedorderUrl(), bodyXml);
        log.info("统一下单 resp:{}", resp);
        //对返回的结果进行处理
        Map<String, Object> xmlMap = WXPayUtil.xmlToMap(resp);
        log.info("统一下单美化后的结果 xmlMap:{}", xmlMap);
        return xmlMap;
    }

    @Override
    public Map<String, Object> unifiedorderNotifyUrl(HttpServletRequest request) {
        //将通知情况解析出来
        log.info(">>>>>>>>>>> notify request: header:{} contentType:{} ip:{}", request.getHeaderNames(), request.getContentType(), IpUtil.getIpAddress(request));
        Map<String, Object> map = new HashMap<>();
        if (!request.getContentType().toLowerCase().contains("xml")) {
            Map<String, String[]> reqMap = request.getParameterMap();
            Set<String> strings = reqMap.keySet();
            for (String key : strings) {
                log.error("invalid key: {} invalid resp: {}", key, reqMap.get(key));
            }
            return map;
        }
        BufferedReader streamReader = null;
        InputStream inputStream = null;
        try {
            inputStream = request.getInputStream();
            streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            log.info(">>>>>>>>>>> notify StringBuffer: {}", responseStrBuilder.toString());
            map = WXPayUtil.xmlToMap(responseStrBuilder.toString());
        } catch (IOException e) {
            log.error("IOException: {}", e);
        } finally {
            try {
                if (streamReader != null) {
                    streamReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                log.error("IOException: {}", e);
            }
        }
        return map;
    }

    /***
     * 查询订单 请求不需要证书
     * @param mapReqParams
     * @return
     */
    @Override
    public Map<String, Object> queryOrder(Map<String, Object> mapReqParams) {
        //将map入参按照微信接口的xml格式传入
        String bodyXml = WXPayUtil.mapToXml(mapReqParams, false);

        String resp = CertHttpUtil.withoutSSL(wxpayProperty.getPayQueryOrderUrl(), bodyXml);
        log.info("查询订单 resp:{}", resp);
        //对返回的结果进行处理
        Map<String, Object> xmlMap = WXPayUtil.xmlToMap(resp);
        log.info("查询订单美化后的结果 xmlMap:{}", xmlMap);
        /***
         * 以下字段在return_code为SUCCESS的时候有返回
         *    小程序ID	appid
         *    商户号	mch_id
         *    随机字符串	nonce_str
         *    签名	sign
         *    业务结果	result_code SUCCESS/FAIL
         *    错误代码	err_code
         *    错误代码描述	err_code_des  结果信息描述
         * 以下字段在return_code 、result_code、trade_state都为SUCCESS时有返回
         *    设备号	device_info
         *    用户标识	openid
         *    是否关注公众账号	is_subscribe  用户是否关注公众账号，Y-关注，N-未关注
         *    交易类型	trade_type  调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP，MICROPAY，详细说明见参数规定
         *    交易状态	trade_state SUCCESS—支付成功 REFUND—转入退款 NOTPAY—未支付 CLOSED—已关闭
         *              REVOKED—已撤销（刷卡支付）USERPAYING--用户支付中 PAYERROR--支付失败(其他原因，如银行返回失败)
         *    付款银行	bank_type   银行类型，采用字符串类型的银行标识
         *    标价金额	total_fee   订单总金额，单位为分
         *    应结订单金额	settlement_total_fee	当订单使用了免充值型优惠券后返回该参数，应结订单金额=订单金额-免充值优惠券金额。
         *    标价币种	fee_type
         *    现金支付金额	cash_fee
         *    现金支付币种	cash_fee_type
         *    代金券金额	coupon_fee
         *    代金券使用数量	coupon_count
         *    代金券类型	coupon_type_$n  CASH--充值代金券 NO_CASH---非充值优惠券    开通免充值券功能，并且订单使用了优惠券后有返回（取值：CASH、NO_CASH）。$n为下标,从0开始编号，举例：coupon_type_$0
         *    代金券ID	coupon_id_$n    代金券ID, $n为下标，从0开始编号
         *    微信支付订单号	transaction_id
         *    商户订单号	out_trade_no
         *    附加数据	attach  附加数据，原样返回
         *    支付完成时间	time_end
         *    交易状态描述	trade_state_desc    对当前查询订单状态的描述和下一步操作的指引
         */
        return xmlMap;
    }

    /***
     * 拼接查询订单的参数
     * @param outOrderNo 本系统订单号
     * @return
     */
    @Override
    public Map<String, Object> getQueryOrderParams(String outOrderNo, Integer payScene) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (payScene.intValue() == WeixinPayConstants.PAYSCENE_PUB_APPID.intValue()) {
            //公众账号ID
            map.put("appid", wxpayProperty.getPubAppId());
        } else if (payScene.intValue() == WeixinPayConstants.PAYSCENE_APPLET_APPID.intValue()) {
            //小程序ID
            map.put("appid", wxpayProperty.getMfAppId());
        }
        //商户号	mch_id
        map.put("mch_id", wxpayProperty.getMchId());
        //随机字符串	nonce_str
        String nonceStr = RandomStrUtil.randomStr(28);
        map.put("nonce_str", nonceStr);
        //商户订单号	out_trade_no
        map.put("out_trade_no", outOrderNo);

        String sortSign = SortUtil.buildSignStr(map);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        map.put("sign", sign);//签名	sign

        return map;
    }

    /***
     * 申请退款 请求需要双向证书
     * @param reqRefundParams
     * @return
     */
    @Override
    public Map<String, Object> refundPayOrder(Map<String, Object> reqRefundParams) {
        log.info("<<<<<<<<<< refund req: {}", reqRefundParams);
        //将map入参按照微信接口的xml格式传入
        String bodyXml = WXPayUtil.mapToXml(reqRefundParams, false);
        String certPath = wxpayProperty.getCertPath() + "/apiclient_cert.p12";
        String resp = CertHttpUtil.ssl(wxpayProperty.getPayRefundOrderUrl(), bodyXml, wxpayProperty.getMchId(), certPath);
        log.info("<<<<<<<<<< refund resp: {}, xmlBody: {}", resp, bodyXml);
        //对返回的结果进行处理
        Map<String, Object> xmlMap = WXPayUtil.xmlToMap(resp);
        log.info("<<<<<<<<<< refund resp deal with: {}", xmlMap);
        /***
         * 以下字段在return_code为SUCCESS的时候有返回
         *          业务结果	result_code	是	String(16)  SUCCESS/FAIL SUCCESS退款申请接收成功，结果通过退款查询接口查询 FAIL 提交业务失败
         *          错误代码	err_code	否	String(32)
         *          错误代码描述	err_code_des	否	String(128)	系统超时
         *          小程序ID	appid	是	String(32)	微信分配的小程序ID
         *          商户号	mch_id	是	String(32)	微信支付分配的商户号
         *          随机字符串	nonce_str	是	String(32)  随机字符串，不长于32位
         *          签名	sign	是	String(32)
         *          微信订单号	transaction_id	是	String(32)
         *          商户订单号	out_trade_no	是	String(32)	商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
         *          商户退款单号	out_refund_no	是	String(64)  商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
         *          微信退款单号	refund_id	是	String(32)      微信退款单号
         *          退款金额	refund_fee	是	Int                 退款总金额,单位为分,可以做部分退款
         *          应结退款金额	settlement_refund_fee   否	Int 去掉非充值代金券退款金额后的退款金额，退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
         *          标价金额	total_fee	是	Int                 订单总金额，单位为分，只能为整数，详见支付金额
         *          应结订单金额	settlement_total_fee	否	Int 去掉非充值代金券金额后的订单总金额，应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
         *          标价币种	fee_type	否	String(8)
         *          现金支付金额	cash_fee	是	Int
         *          现金支付币种	cash_fee_type	否	String(16)
         *          现金退款金额	cash_refund_fee	否	Int
         *          代金券类型	coupon_type_$n	否	String(8)
         *          代金券退款总金额	coupon_refund_fee	否	Int
         *          单个代金券退款金额	coupon_refund_fee_$n	否	Int
         *          退款代金券使用数量	coupon_refund_count	否	Int
         *          退款代金券ID	coupon_refund_id_$n	否	String(20)
         */

        return xmlMap;
    }

    /***
     * 申请退款流程的参数封装
     * @param oldOutOrderNo
     * @param refundOutOrderNo
     * @param totalFee
     * @param refundFee
     * @return
     */
    @Override
    public Map<String, Object> getRefundPayOrderReqParams(String oldOutOrderNo,
                                                          String refundOutOrderNo,
                                                          BigDecimal totalFee,
                                                          BigDecimal refundFee,
                                                          String notifySource) {
        Map<String, Object> map = new HashMap<String, Object>();
        //商户号 String(32)
        map.put("mch_id", wxpayProperty.getMchId());
        //随机字符串 String(32)
        map.put("nonce_str", RandomStrUtil.randomStr(28));
        //商户订单号 String(32)
        map.put("out_trade_no", oldOutOrderNo);
        //商户退款单号 String(64)
        map.put("out_refund_no", refundOutOrderNo);
        //订单金额 Int单位为分
        map.put("total_fee", totalFee.multiply(new BigDecimal(100)).intValue());
        //退款金额 Int单位为分
        map.put("refund_fee", refundFee.multiply(new BigDecimal(100)).intValue());
        map.put("refund_fee_type", "CNY");
        //退款资金来源 否 String(30) 仅针对老资金流商户使用 REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
//        map.put("refund_account","");
        //退款结果通知url 否 String(256) 通知URL必须为外网可访问的url，不允许带参数
        if (WeixinPayConstants.SELLER_NOTIFY_SOURCE.equals(notifySource)) {
            map.put("notify_url", wxpayProperty.getSellerPayRefundNotifyUrl());
            //公众号ID String(32)
            map.put("appid", wxpayProperty.getPubAppId());
            //退款原因 否 String(80) 若商户传入，会在下发给用户的退款消息中体现退款原因
            map.put("refund_desc", "集客魔方-活动退款");
        } else if (WeixinPayConstants.APPLET_NOTIFY_SOURCE.equals(notifySource)) {
            map.put("notify_url", wxpayProperty.getPayRefundNotifyUrl());
            //小程序ID String(32)
            map.put("appid", wxpayProperty.getMfAppId());
            //退款原因 否 String(80) 若商户传入，会在下发给用户的退款消息中体现退款原因
            map.put("refund_desc", "集客魔方-活动发起库存不足退款");
        }

        String sortSign = SortUtil.buildSignStr(map);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        map.put("sign", sign);//签名	sign

        return map;
    }

    /**
     * 查询企业付款状态
     *
     * @param nonceStr
     * @param sign
     * @param partnerTradeNo
     * @return
     */
    @Override
    public boolean checkTransfers(String nonceStr, String sign, String partnerTradeNo) {
        String checkUrl = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo";
        //拼接参数
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("appid", wxpayProperty.getMchAppId()); //商户账号appid==公众号appid
        map.put("mch_id", wxpayProperty.getMchId());//商户号
        map.put("nonce_str", nonceStr);//随机字符串
        map.put("partner_trade_no", partnerTradeNo);//商户订单号
        map.put("sign", sign);//签名	sign String(32)

        //将map入参按照微信接口的xml格式传入
        String bodyXml = WXPayUtil.mapToXml(map, false);
        //调用xml参数，post请求调用微信接口
        String resp = HttpUtil.sendPostXml(checkUrl, bodyXml);
        //对返回的结果进行处理
        Map<String, Object> xmlMap = WXPayUtil.xmlToMap(resp);
        if ("SUCCESS".equals(xmlMap.get("return_code").toString().toUpperCase())) {
            if ("SUCCESS".equals(xmlMap.get("result_code").toString().toUpperCase())) {
                if ("SUCCESS".equals(xmlMap.get("status").toString().toUpperCase())) {
                    return true;
                } else if ("FAILED".equals(xmlMap.get("status").toString().toUpperCase())) {
                    //TODO:是否做mongodb存储日志
                    return false;
                } else if ("PROCESSING".equals(xmlMap.get("status").toString().toUpperCase())) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 红包发放 未实现+未测试
     *
     * @param reOpenID
     * @param price
     * @param totalNum
     * @return
     */
    @Override
    public Map<String, Object> redPack(String reOpenID, int price, int totalNum) {
        Map<String, Object> map = new HashMap<String, Object>();
        String nonceStr = RandomStrUtil.randomStr(28);
        map.put("nonce_str", nonceStr);//随机字符串	nonce_str String(32)
        String mchBillno = RandomStrUtil.randomStr(6) + System.currentTimeMillis() + new AtomicInteger(1);
        map.put("mch_billno", mchBillno);//商户订单号	mch_billno  String(28)
        map.put("mch_id", wxpayProperty.getMchId()); //商户号	mch_id  String(32)
        map.put("wxappid", wxpayProperty.getPubAppId()); //公众账号appid	wxappid  String(32)
        map.put("send_name", wxpayProperty.getMchSendName());//商户名称	send_name  String(32) 红包发送者名称
//        map.put("re_openid",reOpenID);//用户openid	re_openid  String(32)
//        map.put("total_amount",price);//付款金额	total_amount  int  单位分
//        map.put("total_num",totalNum);//红包发放总人数	total_num  int
        map.put("wishing", wxpayProperty.getWishing());//红包祝福语	wishing  String(128)
        map.put("client_ip", IpUtil.getServerIp()); //Ip地址	client_ip  String(15) 127.0.0.1
        map.put("act_name", wxpayProperty.getActName());//活动名称	act_name  String(32)
        map.put("remark", wxpayProperty.getRemark());//备注	remark  String(256)
//        if(price < 100 || price > 20000){
//            //非必需(32) 场景id	scene_id:(发放红包使用场景，红包金额大于200或者小于1元时必传PRODUCT_1:商品促销 PRODUCT_2:抽奖 PRODUCT_3:虚拟物品兑奖  PRODUCT_4:企业内部福利 PRODUCT_5:渠道分润 PRODUCT_6:保险回馈 PRODUCT_7:彩票派奖 PRODUCT_8:税务刮奖)
//            map.put("scene_id","PRODUCT_3");//备注	remark  String(256)
//        }
        //否 活动信息(128)	risk_info
        //否 资金授权商户号(32)	consume_mch_id

        String signTemp = SortUtil.buildSignStr(map);
        String sign = signTemp + "&key=" + wxpayProperty.getMchSecretKey();
        sign = MD5Util.md5(sign).toUpperCase();//注：MD5签名方式
        map.put("sign", sign);//签名	sign String(32)

        String bodyXml = WXPayUtil.mapToXml(map, false);
        String resp = HttpUtil.sendPostXml(wxpayProperty.getPayRedPackageUrl(), bodyXml);//是否需要证书支付
        //对返回的结果进行处理
        Map<String, Object> xmlMap = WXPayUtil.xmlToMap(resp);
        return null;
    }
}
