package mf.code.common.easyexcel;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.utils.ConvertUtil;
import mf.code.excel.ExcelListener;
import org.apache.commons.lang.time.DateFormatUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * mf.code.common.easyexcel
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月07日 11:07
 */
@Slf4j
public class EasyExcelUtil {
    private static Sheet initSheet;
    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private final static DateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);

    /***
     * 浏览器直接下载
     *
     * @param list
     * @param response
     * @param clazz
     */
    public static void export(List<? extends BaseRowModel> list, HttpServletResponse response, Class<? extends BaseRowModel> clazz) {
        ServletOutputStream out = null;
        try {
            out = response.getOutputStream();
        } catch (IOException e) {
            log.error("IOException {}", e);
        }
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        try {
            FieldTypeAnnotation fieldTypeAnnotation = list.get(0).getClass().getAnnotation(FieldTypeAnnotation.class);
            // 进行转码，使其支持中文文件名
            String fileName = ConvertUtil.toUtf8String(fieldTypeAnnotation.name() + DateFormatUtils.format(new Date(), "yyyy.MM.dd.HH.mm"));

            Sheet sheet2 = new Sheet(2, 3, clazz, "sheet", null);
            EasyExcelUtil.setResponseHeader(response, fileName);
            writer.write(list, sheet2);
            out.flush();
        } catch (Exception e) {
            log.error("IOException {}", e);
        } finally {
            writer.finish();
            try {
                out.close();
            } catch (IOException e) {
                log.error("IOException {}", e);
            }
        }
    }

    public static InputStream exportInputStream(List<? extends BaseRowModel> list, Class<? extends BaseRowModel> clazz) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        try {
            Sheet sheet2 = new Sheet(2, 3, clazz, "sheet", null);
            writer.write(list, sheet2);
            out.flush();
        } catch (Exception e) {
            log.error("IOException {}", e);
        } finally {
            writer.finish();
            try {
                out.close();
            } catch (IOException e) {
                log.error("IOException {}", e);
            }
        }
        InputStream swapStream = new ByteArrayInputStream(out.toByteArray());
        return swapStream;
    }

    /**
     * 读大于1000行数据, 带样式
     *
     * @param filePath 文件觉得路径
     * @return
     */
    public static List<Object> readMoreThan1000RowBySheet(String filePath, Sheet sheet) {
        if (!StringUtils.hasText(filePath)) {
            return null;
        }

        sheet = sheet != null ? sheet : initSheet;

        InputStream fileStream = null;
        try {
            fileStream = new FileInputStream(filePath);
            ExcelListener excelListener = new ExcelListener();
            EasyExcelFactory.readBySax(fileStream, sheet, excelListener);
            return excelListener.getDatas();
        } catch (FileNotFoundException e) {
            log.error("找不到文件或文件路径错误, 文件：{}", filePath);
        } finally {
            try {
                if (fileStream != null) {
                    fileStream.close();
                }
            } catch (IOException e) {
                log.error("excel文件读取失败, 失败原因：{}", e);
            }
        }
        return null;
    }

    public static void setResponseHeader(HttpServletResponse response, String codedFileName) {
        try {
            response.reset();
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setHeader("content-disposition", "attachment;filename=" + codedFileName + ".xlsx");
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            log.error("Exception:", ex);
        }
    }
}

