package mf.code.common.redis;

/**
 * redis键
 *
 * @author gel
 */
public class RedisKeyConstant {

    /**
     * 指导一个商家导入至少1商品
     */
    public static final String TEACHER_TRAINING_IOP = "teacher:training:iop:";// <tid>

    /**
     * 指导一个商家创建至少3个活动
     */
    public static final String TEACHER_TRAINING_CTA = "teacher:training:cta:";// <tid>

    /**
     * 协助一个商家提升粉丝数
     */
    public static final String TEACHER_TRAINING_PF = "teacher:training:pf:"; // <tid>

    /**
     * redis 收入弹框防重加锁
     */
    public static final String TEACHER_INCOME_DIALOG_FORBID_REPEAT = "teacher:income:dialog:forbidrepeat:";//<tid>

    /**
     * 集客师成功招募将领 增加收益弹窗 40%
     */
    public static final String TEACHER_INCOME_DIALOG40 = "teacher:income:dialog40:";//<tid>
    /**
     * 推荐的集客师 成功招募将领 增加收益弹窗 5%
     */
    public static final String TEACHER_INCOME_DIALOG5 = "teacher:income:dialog5:";//<tid>

    /**
     * 各军团武力值
     */
    public static final String TEACHER_ARMY_FORCE = "teacher:army:force";

    /**
     * 数据库 teacher数据缓存
     */
    public static final String TEACHER_CACHE = "teacher:cache:id:";//<tid>
    /**
     * 数据库 teacherInvite缓存
     */
    public static final String TEACHER_INVITE_CACHE = "teacher:invite:cache:id:";//<tid>
    //申请结果
    public static final String TEACHER_APPLY_RESULT = "teacher:apply:result:";//<tid>

    public static final String TEACHER_RECOMMAND_COUNT_LIST = "teacher:recommand:count:list:";//<tid>
    /**
     * 集客师平台累计发放分红
     */
    public static final String TEACHER_PLATFORM_ACCUMULATED_BONUS = "teacher:platform:accumulated:bonus";

    /**
     * 个人累计收益排行榜(真实数据) key, roleName, scroe
     */
    public static final String TEACHER_ACCUMULATED_INCOME_LIST = "teacher:accumulated:income:list";
    /**
     * 个人累计收益排行榜(带有假数据)
     */
    public static final String TEACHER_ACCUMULATED_INCOME_LIST_WITH_FALSE = "teacher:accumulated:income:list:with:false";
    /**
     * 集客师 商户注册用短信验证码键
     */
    public final static String TEACHER_REGISTER = "teacher:register:";

    /**
     * 集客师今日信息
     * String todayInfo = stringRedisTemplate.opsForValue().get(RedisKeyConstant.TEACHER_TODY_INFO + teacherAggregateRoot.getId());
     * if (StringUtils.isNotBlank(todayInfo)) {
     * JSONObject jsonObject = JSON.parseObject(todayInfo);
     * jsonObject.getBigDecimal("todayIncome");
     * jsonObject.getInteger("todayMerchants");
     * }
     */
    public final static String TEACHER_TODY_INFO = "teacher:tody:info:";//<tid>

    /**
     * 推送点击率统计
     */
    public static final String RECALL_PUSH_STATISTICS = "mf:merchant:recall:push:";//<shopId>:<pushId>:{userId1,userId2......}

    /**
     * 商户注册用短信验证码键
     */
    public final static String MERCHANT_REGISTER = "merchant:register:";
    /***
     * 注册成功时，第一次的弹窗reids存储
     */
    public final static String MERCHANT_FIRSTLOGIN_DIALOG = "merchant:firstlogin:dialog:";//<mid>

    /**
     * 商户注册用短信验证码键
     */
    public final static String MERCHANT_LOGIN = "merchant:login:";
    /**
     * 商户后台 活动定义 此活动用户参加情况 缓存
     */
    public static final String ACTIVITYDEF_USER_JOIN_INFO = "merchant:activitydef:userjoininfo";//<activityDefId>

    public static final String ACTIVITYDEF_JOIN_INFO = "merchant:activitydef:activitydefjoininfo";//<activityDefId>
    /**
     * 记录用户日活量
     */
    public static final String STATISTICS_SUMMARY_LOGIN = "mf:statistics:summary:login:";//<shopId>:<20190122>;uid
    /**
     * 数据库 用户数据缓存
     */
    public static final String MF_CODE_USER_CACHE = "mf:code:user:id:";//<userId>

    /**
     * applet登录token
     */
    public static String USER_LOGIN = "user:login:uid:openid:";

    public static String USER_LOGIN_FORBID_REPEAT = "user:login:forbid:repeat:";//<openId>

    public static final String FORBID_KEY = "mf:code:common:";

    public static final String FORBID_KEY_REFUND = "mf:code:common:refund:";

    public static final String REDIS_LOCK_KEY = "mf:code:common:lock:";

    /**
     * 用户手机验证码 user:mobilecode:+ <userId> + ":" + <mobile>
     */
    public static final String MOBILE_CODE = "user:mobilecode:";
    /**
     * 用户手机发送验证码次数限制
     * key--> user:mobilenum:+ <userId> + ":" + <mobile>
     * value--> num
     */
    public static final String MOBILE_CODE_NUM = "user:mobilenum:";
    /**
     * 添加任务空间缓存
     * key--> taskspace:list:+ <userId>
     * value--> json
     */
    public static final String TASK_SPACE_LIST = "taskspace:list:";
    /**
     * 添加任务空间防重
     * key--> taskspace:repeat:+ <userId>
     * value--> 1
     */
    public static final String TASK_SPACE_REPEAT = "taskspace:repeat:";

    /***
     * 导出excel时间防重
     */
    public static final String EXCEL_REPEAT = "excel:repeat:";
    /***
     * 导出excel成功后的数据返回
     * key:shopId+All+type_+yyyy-MM-dd_yyyy-MM-dd
     * value:excel地址
     * expire:30day
     */
    public static final String EXCEL_EXIST = "excel:exist:";

    public static final String SAVE_ADCHANNEL = "ad:save:";//<platUserId>

    public static final String PURCHASE_AD = "ad:purchase:ad:";//<merchantId>

    // user:newbie:<userId>
    public static final String NEWBIE = "user:homepage:newbie:";
    /**
     *
     */
    public static final String ERROR_EMAILSENDER = "log:email:send:";//<title>

    /**
     * 公众号accessToken
     */
    public static final String PUBLIC_ACCESS_TOKEN = "public:accesstoken";
    /**
     * 公众号accessToken
     */
    public static final String PUBLIC_JSAPI_TICKET = "public:jsapiticket";

    /***
     * 闯关今日信息(value=json字符串{"todayTaskProfit":"0.1","todayScottareProfit":"0.1","todayScottare":"0.1"})
     * key--> checkpoint:todayTaskProfit:<shopId>:<userId>
     */
    public static final String CHECKPOINT_TODAYINFO = "checkpoint:todayTaskProfit:";

    /***
     * 闯关今日信息(value=json字符串{"todayTaskProfit":"0.1","todayScottareProfit":"0.1","todayScottare":"0.1"})
     * key--> checkpoint:todayTaskProfit:<userId>
     */
    public static final String CHECKPOINT_TODAYINFO_TOTAL = "checkpoint:todayTaskProfit:total:";//<userId>

    /***
     * 矿工们为上级产生的收益
     * key--> checkpoint:minerProvideProfit:<shopId>:<userId>//上级用户编号
     */
    public static final String CHECKPOINT_MINERPROVIDE_PROFIT = "checkpoint:minerProvideProfit:";

    /***
     * 矿工们为上级产生的收益
     * key--> checkpoint:minerProvideProfittotal:<userId>//上级用户编号
     */
    public static final String CHECKPOINT_MINERPROVIDE_PROFIT_TOTAL = "checkpoint:minerProvideProfit:total:";

    /***
     * 用户完成任务的具体类型+佣金金额(value=json字符串[{"type":1,"amount":"0.1"}]),其中 type 从CheckpointTaskSpaceEnum枚举里取
     * key--> checkpoint:userTask:finish:<shopId>:<userId>
     */
    public static final String CHECKPOINT_USERTASK_FINISH = "checkpoint:userTask:finish:";
    /***
     * 用户完成任务的具体类型+佣金金额(value=json字符串[{"type":1,"amount":"0.1"}]),其中 type 从CheckpointTaskSpaceEnum枚举里取
     * key--> checkpoint:userTask:finish:<userId>
     */
    public static final String CHECKPOINT_USERTASK_FINISH_TOTAL = "checkpoint:userTask:finish:total:";

    /***
     * 用户是否通关(value=json字符串{"value":"不屈黑铁","money":0,"name":0})
     * key--> checkpoint:finish:<shopId>:<userId>
     */
    public static final String CHECKPOINT_FINISH = "checkpoint:finish:";

    /***
     * 用户是否通关(value=json字符串{"value":"不屈黑铁","money":0,"name":0})
     * key--> checkpoint:finish:<shopId>:<userId>
     */
    public static final String CHECKPOINT_FINISH_PLATFORM = "checkpoint:finish:platform:";

    /***
     * 判断总收益是否是否占当前关卡的50%或者90%。若是，弹窗
     * key--> checkpoint:differenceAmount:<shopId>:<userId>
     */
    public static final String CHECKPOINT_DIFFERENCE_AMOUNT = "checkpoint:differenceAmount:";
    /**
     * userPubJoin更新锁
     * key--> checkpoint:upj:repeat:<userId>
     * value--> 1
     */
    public static final String CHECKPOINT_UPJ_REPEAT = "checkpoint:upj:repeat:";
    /**
     * 数据字典键
     * key--> common:dict:<type>:<key>
     * value--> commonDict json字符串
     */
    public static final String COMMON_DICT = "common:dict:";
    /**
     * 回填订单防重锁
     * key--> jkmf:one:backfillOrder:repeat:<orderId>
     * value--> 1
     */
    public static final String BACKFILLORDER_REPEAT = "jkmf:one:backfillOrder:repeat:";
    /**
     * 商户常用的淘宝店铺
     * key--> jkmf:one:merchant:usualNick:<merchantId>
     * value--> nick
     */
    public static final String MERCHANT_USUAL_NICK = "jkmf:one:merchant:usualNick:";


    /**
     * 回填订单异步处理防重锁
     * key--> jkmf:one:backfillOrder:Asyncrepeat:<orderId>
     * value--> 1
     */
    public static final String BACKFILLORDER_ASYNC_REPEAT = "jkmf:one:backfillOrder:Asyncrepeat:";

    /**
     * 回填订单异步处理返回结果
     * key--> jkmf:one:backfillOrder:Asyncresult:<orderId>
     * value--> 返回结果信息
     */
    public static final String BACKFILLORDER_ASYNC_RESULT = "jkmf:one:backfillOrder:Asyncresult:";

    /**
     * 活码生成同步锁
     * key--> jkmf:one:livecode:lock:<userType>:<userId>
     * value--> 1
     */
    public static final String LIVECODE_LOCK = "jkmf:one:livecode:lock:";
}
