package mf.code.common.apollo.impl;

import com.alibaba.fastjson.JSONObject;
import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.core.enums.ConfigFileFormat;
import com.ctrip.framework.apollo.internals.YmlConfigFile;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

/**
 * mf.code.common.apollo.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月15日 20:23
 */
@Service
public class ApolloPropertyServiceImpl implements ApolloPropertyService {
    @Override
    public ApolloLhyxProperty getApolloLhyxProperty() {
        YmlConfigFile configFile = (YmlConfigFile) ConfigService.getConfigFile("application-apollo", ConfigFileFormat.YML);
        if (configFile == null) {
            return new ApolloLhyxProperty();
        }
        Yaml yaml = new Yaml();
        Object load = yaml.load(configFile.getContent());
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(load));
        ApolloLhyxProperty apolloLhyxProperty = JSONObject.toJavaObject(jsonObject, ApolloLhyxProperty.class);
        return apolloLhyxProperty;
    }
}
