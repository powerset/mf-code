package mf.code.common.apollo;

/**
 * mf.code.common.apollo
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月15日 20:23
 */
public interface ApolloPropertyService {

    ApolloLhyxProperty getApolloLhyxProperty();
}
