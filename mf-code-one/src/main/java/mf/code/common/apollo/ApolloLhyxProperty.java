package mf.code.common.apollo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.common.apollo
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月15日 19:35
 */
@Data
public class ApolloLhyxProperty {

    private Lhyx lhyx;
    private Lhyx total;

    @Data
    public static class Lhyx {
        @JsonProperty("switchs")
        private Switchs switchs;
        @JsonProperty("ms")
        private List<Ms> ms;
        @JsonProperty("pre-activity")
        private PreActivity preActivity;
        @JsonProperty("activity")
        private Activity activity;
        @JsonProperty("zstj")
        private List<Zstj> zstj;
    }

    @Data
    public static class Zstj {
        @JsonProperty("gid")
        private Long gid;
    }

    @Data
    public static class Switchs {
        @JsonProperty("youhuiquan")
        private int youhuiquan;
    }

    @Data
    public static class Ms {
        @JsonProperty("id")
        private Long id;
        @JsonProperty("activity-day")
        private String activityDay;
        @JsonProperty("batch")
        private List<Batch> batch;
    }

    @Data
    public static class Batch {
        private Long id;
        @JsonProperty("start-time")
        private String startTime;
        @JsonProperty("end-time")
        private String endTime;
        @JsonProperty("skus")
        private List<Skus> skus;

        /**时间戳*/
        private Long startTimeStamp;
        private Long endTimeStamp;
    }

    @Data
    public static class Skus {
        @JsonProperty("sku")
        private String sku;
    }

    @Data
    public static class PreActivity {
        @JsonProperty("startTime")
        private String startTime;
        @JsonProperty("endTime")
        private String endTime;
        @JsonProperty("count")
        private Long count;
        @JsonProperty("money")
        private BigDecimal money;
        @JsonProperty("kf")
        private String kf;
    }

    @Data
    public static class Activity {
        @JsonProperty("startTime")
        private String startTime;
        @JsonProperty("endTime")
        private String endTime;

        @JsonProperty("baokuan-gids")
        private String baokuanGids;
        @JsonProperty("group1-gids")
        private String group1Gids;
        @JsonProperty("group2-gids")
        private String group2Gids;
        @JsonProperty("banner")
        private List<Banner> banner;
        @JsonProperty("times")
        private List<Time> times;
    }

    @Data
    public static class Banner {
        @JsonProperty("url")
        private String url;
        @JsonProperty("gid")
        private Long gid;
    }

    @Data
    public static class Time {
        @JsonProperty("time")
        private String time;
    }
}
