package mf.code.common.handler;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.exception.ArgumentException;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理器
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 捕获异常
     *
     * @param request
     * @param exception
     * @return
     * @throws Exception
     */
    @ExceptionHandler(Exception.class)
    public SimpleResponse ExceptionHandler(HttpServletRequest request, Exception exception) {
        return handleErrorInfo(request, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "系统异常，请联系管理员", exception);
    }

    /**
     * 处理前端传参错误异常
     *
     * @param request
     * @param exception
     * @return
     * @throws Exception
     */
    @ExceptionHandler(ArgumentException.class)
    public SimpleResponse sessionNotFoundExceptionHandler(HttpServletRequest request, ArgumentException exception) {
        return handleErrorInfo(request, exception.getCode(), exception.getErrorMessage(), exception);
    }

    private SimpleResponse handleErrorInfo(HttpServletRequest request, int code, String msg, Exception e) {
        log.error("系统异常", e);
        return new SimpleResponse(code, msg);
    }
}