//package mf.code.common.aop;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import lombok.extern.slf4j.Slf4j;
//import mf.code.common.caller.wxmp.WxmpProperty;
//import mf.code.common.utils.BeanMapUtil;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.lang.math.NumberUtils;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.BoundListOperations;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.stereotype.Component;
//import org.springframework.util.CollectionUtils;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.*;
//
///**
// * mf.code.common.aop
// *
// * @description: 对前端的formid进行拦截存储
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2018年11月28日 10:50
// */
//@Component
//@Aspect
//@Slf4j
//public class AspectJ4TemplateMessageFormId {
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;
//    @Autowired
//    private WxmpProperty wxmpProperty;
//
//    private static final String MOCK_FORM = "the formId is a mock one";
//
//    @Around("execution(public * mf.code.api.applet..*.*Api.*(..))")
//    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//        getUrlMethodParam(proceedingJoinPoint);
//        //延续去调用具体的api方法
//        Object result = proceedingJoinPoint.proceed();
//        return result;
//    }
//
//    private void getUrlMethodParam(ProceedingJoinPoint proceedingJoinPoint) {
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        String method = request.getMethod();
//        String paramJson;
//        //只对get方法判断处理
//        Map<String, Object> requestParams = new HashMap<>();
//        if ("GET".equalsIgnoreCase(method)) {
//            Map<String, String[]> parameterMap = request.getParameterMap();
//            paramJson = JSON.toJSONString(parameterMap);
//            requestParams = JSONObject.parseObject(paramJson, Map.class);
//        } else {
//            String contentType = request.getContentType();
//            if (contentType.toLowerCase().contains("application/json")) {
//                List<Object> strs = handleObjectArray(proceedingJoinPoint);
//                if (strs != null && strs.size() > 0) {
//                    if (strs.get(0) instanceof Map) {
//                        requestParams = (Map) strs.get(0);
//                    } else {
//                        requestParams = BeanMapUtil.beanToMap(strs.get(0));
//                    }
//                }
//            }
//        }
//        boolean isExistUserId = requestParams.get("userId") != null
//                && StringUtils.isNotBlank(requestParams.get("userId").toString());
//        boolean isExistUid = requestParams.get("uid") != null
//                && StringUtils.isNotBlank(requestParams.get("uid").toString());
//        boolean isExistFormIds = requestParams.get("formIds") != null && StringUtils.isNotBlank(requestParams.get("formIds").toString());
//        Long userId;
//        List<String> formIds = new ArrayList<>();
//        if ((isExistUserId || isExistUid) && isExistFormIds) {
//            if (isExistUserId) {
//                if ("GET".equalsIgnoreCase(method)) {
//                    List<Long> arrayUserIds = JSONObject.parseArray(requestParams.get("userId").toString(), Long.class);
//                    userId = arrayUserIds.get(0);
//                } else {
//                    userId = NumberUtils.toLong(requestParams.get("userId").toString());
//                }
//            } else {
//                if ("GET".equalsIgnoreCase(method)) {
//                    List<Long> arrayUids = JSONObject.parseArray(requestParams.get("uid").toString(), Long.class);
//                    userId = arrayUids.get(0);
//                } else {
//                    userId = NumberUtils.toLong(requestParams.get("uid").toString());
//                }
//            }
//            //formIds是以字符串传入
//            if ("GET".equalsIgnoreCase(method)) {
//                List<String> arrayFormIdsStr = JSONObject.parseArray(requestParams.get("formIds").toString(), String.class);
//                formIds = Arrays.asList(arrayFormIdsStr.get(0).split(","));
//            } else {
//                formIds = Arrays.asList(requestParams.get("formIds").toString().split(","));
//            }
//
//            //查询redis,参与人展现,直接倒叙排列
//            Map<String, Object> formIdMap = new HashMap<>();
//            BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(wxmpProperty.getRedisKey(userId));
//            List<Object> strs = activityPersons.range(0, -1);
//            if (!CollectionUtils.isEmpty(strs)) {
//                for (Object object : strs) {
//                    JSONObject jsonObject = JSON.parseObject(object.toString());
//                    String formID = jsonObject.getString("formId");
//                    formIdMap.put(formID, jsonObject.toString());
//                }
//            }
//            //刚进来的formId自我去重
//            List<String> needFormIds = new ArrayList<>();
//            if (formIds != null && formIds.size() > 0 && !formIds.contains(MOCK_FORM)) {
//                for (String formId : formIds) {
//                    if (!MOCK_FORM.equals(formId) && needFormIds.indexOf(formId) == -1) {
//                        needFormIds.add(formId);
//                    }
//                }
//            }
//            //formId进行存储redis
//            if (!CollectionUtils.isEmpty(needFormIds)) {
//                for (String formId : needFormIds) {
//                    //去重
//                    Object object = formIdMap.get(formId);
//                    if (object == null) {
//                        JSONObject userJsonObject = this.processTemplateMessageUserInfo(formId);
//                        this.stringRedisTemplate.opsForList().rightPush(wxmpProperty.getRedisKey(userId), userJsonObject.toJSONString());
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * post请求解析--处理数组
//     *
//     * @param proceedingJoinPoint
//     * @return
//     */
//    private List<Object> handleObjectArray(ProceedingJoinPoint proceedingJoinPoint) {
//        Object[] os = proceedingJoinPoint.getArgs();
//        List args = new ArrayList();
//        for (int i = 0; i < os.length; i++) {
//            if (os[i] instanceof HttpServletRequest || os[i] instanceof HttpServletResponse) {
//                continue;
//            }
//            args.add(os[i]);
//        }
//        return args;
//    }
//
//    // 私有方法：加工用户邀请列表的用户信息
//    private JSONObject processTemplateMessageUserInfo(String formId) {
//        JSONObject userJsonObject = new JSONObject();
//        userJsonObject.put("formId", formId);
//        userJsonObject.put("ctime", System.currentTimeMillis());
//        return userJsonObject;
//    }
//}
