package mf.code.common.aop;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.teacher.domain.aggregateroot.TeacherAggregateRoot;
import mf.code.api.teacher.domain.repository.TeacherRepository;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RequestSignUtil;
import mf.code.common.utils.TokenUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * mf.code.common.aop
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-04 下午6:01
 */
@Slf4j
public class TeacherTokenInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	private TeacherRepository teacherRepository;

	@Value("${applet.token.debug.mode}")
	private Integer debugMode;
	@Value("${mf.sign.key}")
	private String signKey;
	@Value("${mf.sign.name}")
	private String signName;
	@Value("${mf.sign.date}")
	private String signDateName;
	@Value("${mf.sign.debugMode}")
	private Integer signDebugMode;
	private final String TOKEN_NAME = "token";
	private final Long tenMinute = 10L * 60 * 1000;

	private ThreadLocal<Map<String, Object>> objectThreadLocal = new ThreadLocal<>();

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		// 添加校验sign
		try {
			String sign = request.getHeader(signName);
			String signDateStr = request.getParameter(signDateName);
			if (StringUtils.isBlank(sign) || !StringUtils.isNumeric(signDateStr)) {
				log.error(request.getRequestURI() + "，请求没有mfsign字段,或者没有signdate");
				// 签名debug模式
				if (signDebugMode != null && signDebugMode != 1) {
					return signErrorReturn(response);
				}
			} else {
				// 判断签名时间，是否有效
				Long signDate = Long.valueOf(signDateStr);
				if(Math.abs(DateUtil.getCurrentMillis() - signDate) > tenMinute){
					log.error(request.getRequestURI() + "，签名不匹配,入参:" + JSONObject.toJSONString(request.getParameterMap())
							+ ", 加密时间：" + signDate + ", 当前时间：" + DateUtil.getCurrentMillis());
					// 签名debug模式
					if (signDebugMode != null && signDebugMode != 1) {
						return signErrorReturn(response);
					}
				}
				String signature = RequestSignUtil.generateSignatureForReq(request.getParameterMap(), signKey);
				if (!StringUtils.equals(signature, sign)) {
					log.error(request.getRequestURI() + "，签名不匹配,入参:" + JSONObject.toJSONString(request.getParameterMap())
							+ ", client:" + sign + "，server:" + signature);
					// 签名debug模式
					if (signDebugMode != null && signDebugMode != 1) {
						return signErrorReturn(response);
					}
				}
			}
		} catch (Exception e) {
			log.error("校验签名异常", e);
		}

		String tid = request.getParameter("tid");

		// 如果关掉校验直接通过
		if (debugMode != null && debugMode == 1) {
			return true;
		}

		// 正常验签
		String token = request.getHeader(TOKEN_NAME);
		if (StringUtils.isBlank(token)) {
			log.error("token不存在, 需前端静默请求TeacherLoginApi，验证用户是否授权或生成新token");
			return tokenErrorReturn(response);
		}
		// 问题：新人大转盘 试试手气 请求参数中 uid获取不到……从token中 获取uid 容错
		if (StringUtils.isBlank(tid)) {
			log.error("请求参数中，不存在tid参数");
			return tokenErrorReturn(response);
		}

		TeacherAggregateRoot teacher = teacherRepository.findByTid(tid);

		/**
		 *   uid 用户id，商户id，平台运营用户id
		 *   user 小程序openid，微信公众号openid，商户手机号，平台运营用户手机号
		 *   ctime 用户，商户，平台运营用户的数据记录的创建时间，yyyyMMddHHmmss格式
		 *   now 系统时间，yyyyMMddHHmmss格式
		 *   digest
		 *
		 *   XXX 固定值，场景不同，结尾语不同
		 *   YYY 固定值，场景不同，结尾语不同
		 *   ZZZ 固定值，场景不同，结尾语不同
		 */
		Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.TEACHER);
		if (stringObjectMap == null) {
			log.error("token解密数据为空");
			return tokenErrorReturn(response);
		}
		if (!StringUtils.equals(tid, stringObjectMap.get("uid"))) {
			log.error("token解密tid数据 与 请求参数中的tid不匹配");
			return tokenErrorReturn(response);
		}
		if (teacher == null || !StringUtils.equals(teacher.getOpenid(), stringObjectMap.get("user"))) {
			log.error("token解密user数据 与 数据库中的openid不匹配");
			return tokenErrorReturn(response);
		}
		String md5Str = TokenUtil.md5forDigest(teacher.getId().toString(), teacher.getOpenid(), teacher.getTeacherInfo().getCtime(), TokenUtil.TEACHER);
		if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
			log.error("token解密后验签失败,token = " + token);
			return tokenErrorReturn(response);
		}

		String popularizeCode = teacher.getTeacherInfo().getPopularizeCode();
		if (StringUtils.isNotBlank(popularizeCode)) {
			response.setHeader("popularizeCode", popularizeCode);
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
	}

	/**
	 * token 异常
	 *
	 * @param response 入参HttpServletResponse
	 * @return 返回boolean
	 */
	private boolean tokenErrorReturn(HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		try (PrintWriter out = response.getWriter()) {
			JSONObject res = new JSONObject();
			res.put("code", "-2");
			res.put("message", "需要重新登录");
			res.put("data", "");
			out.append(res.toString());
			out.flush();
		} catch (IOException e) {
			log.error("response发送失败:" + e);
			try {
				response.sendError(500);
			} catch (IOException e1) {
				log.error("这里不应该出错:" + e);
			}
		}
		return false;
	}

	/**
	 * 签名异常
	 *
	 * @param response 入参HttpServletResponse
	 * @return 返回boolean
	 */
	private boolean signErrorReturn(HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		try (PrintWriter out = response.getWriter()) {
			JSONObject res = new JSONObject();
			res.put("code", "-3");
			res.put("message", "签名错误，请使用微信小程序登录");
			res.put("data", "");
			out.append(res.toString());
			out.flush();
		} catch (IOException e) {
			log.error("response发送失败:" + e);
			try {
				response.sendError(500);
			} catch (IOException e1) {
				log.error("这里不应该出错:" + e);
			}
		}
		return false;
	}
}
