package mf.code.common.aop;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.feignclient.ShopAppService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.common.utils.TokenUtil;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.merchant.dto.MerchantDTO;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * mf.code.common.aop
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 15:28
 */
@Slf4j
public class SellerTokenInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private ShopAppService shopAppService;
    private final String TOKEN_NAME = "token";

    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;

    @Value("${seller.token.debug.mode}")
    private Integer debugMode;

    @Value("${seller.token.name}")
    private String tokenName;
    @Value("${seller.uid.name}")
    private String uidName;

    private List<String> urlForbidden = new ArrayList<>();

    {
        urlForbidden.add("/api/seller/activity");
        urlForbidden.add("/api/seller/V2/activity");
        urlForbidden.add("/api/seller/v2/activity");
        urlForbidden.add("/api/seller/v3/activity");
        urlForbidden.add("/api/seller/task");
    }

    private final String SHOP_PARAM = "shopId";

    /**
     * 不能区分小程序端和商户端平台端TokenInterceptor
     *
     * @param request
     * @param response
     * @param handler
     * @return
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String project = request.getHeader("project");
        if ("ddd".equals(project)) {
            return this.checkTokenForDDD(request, response, handler);
        }

        HttpSession session = request.getSession();
        // 如果关掉校验直接通过
        if (debugMode != null && debugMode == 1) {
//            Object debugToken = request.getHeader("debugToken");
//            if (debugToken == null) {
//                return tokenErrorReturn(response, "debugToken不存在");
//            }
//            // 这里不用放入session,因为参数在前面的filter已经填充了
//            session.setAttribute(uidName, debugToken);
            return true;
        }

        Object merchant = session.getAttribute(uidName);
        if (null == merchant) {
            log.error("会话不存在merchant数据");
            return tokenErrorReturn(response, "会话不存在merchant数据");
        }
        Cookie[] cookies = request.getCookies();
        if (cookies == null || cookies.length == 0) {
            log.error("cookie数据不存在");
            return tokenErrorReturn(response, "cookie数据不存在");
        }
        String token = "";
        for (Cookie cookie : cookies) {
            if (tokenName.equals(cookie.getName())) {
                token = cookie.getValue();
                break;
            }
        }
        if (StringUtils.isBlank(token)) {
            log.error("指定cookie数据不存在");
            return tokenErrorReturn(response, "指定cookie数据不存在");
        }
        /**
         *   uid 用户id，商户id，平台运营用户id
         *   user 小程序openid，微信公众号openid，商户手机号，平台运营用户手机号
         *   ctime 用户，商户，平台运营用户的数据记录的创建时间，yyyyMMddHHmmss格式
         *   now 系统时间，yyyyMMddHHmmss格式
         *   digest
         *
         *   XXX 固定值，场景不同，结尾语不同
         *   YYY 固定值，场景不同，结尾语不同
         *   ZZZ 固定值，场景不同，结尾语不同
         */
        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.SAAS);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(response, "token解密数据为空");
        }
        if (!StringUtils.equals(merchant.toString(), stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 会话中merchant数据不匹配");
            return tokenErrorReturn(response, "token解密uid数据 与 会话中merchant数据不匹配");
        }
        Merchant merchant1 = merchantService.getMerchant(Long.valueOf(stringObjectMap.get("uid")));
        if (merchant1 == null || !StringUtils.equals(merchant1.getPhone(), stringObjectMap.get("user"))) {
            log.error("token解密user数据 与 数据库中的phone数据不匹配");
            return tokenErrorReturn(response, "token解密user数据 与 数据库中的phone数据不匹配");
        }
        String md5Str = TokenUtil.md5forDigest(merchant1.getId().toString(), merchant1.getPhone(), merchant1.getCtime(), TokenUtil.SAAS);
        if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
            log.info(merchant1.getPhone() + "商户消息解密时间：" + merchant1.getCtime().getTime());
            log.error("token解密后验签失败");
            return tokenErrorReturn(response, "token解密后验签失败");
        }
        // 校验店铺权限,因为TokenConfig中定义消息头
        if (!checkPermission(request, merchant1)) {
            return tokenPermissionReturn(response, "该店铺没有权限");
        }
        return true;
    }

    private boolean checkTokenForDDD(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // TODO: 此处 可对指定API再放行处理

        Map<String, String> reqParams = getRequestParams(request);
        String merchantId = reqParams.get("merchantId");

        // 如果关掉校验直接通过
        if (debugMode != null && debugMode == 1) {
            log.warn("appletToken校验已关闭");
            return true;
        }
        // 正常验签
        String token = request.getHeader(TOKEN_NAME);
        if (StringUtils.isBlank(token)) {
            log.error("token不存在, 需前端静默请求AppletLoginApi，验证用户是否授权或生成新token");
            return tokenErrorReturn(response, "token不存在, 需前端静默请求AppletLoginApi，验证用户是否授权或生成新token");
        }
        if (StringUtils.isBlank(merchantId)) {
            log.error("请求参数中，不存在merchantId参数");
            return tokenErrorReturn(response, "请求参数中，不存在merchantId参数");
        }
        MerchantDTO merchant = shopAppService.queryMerchantById(Long.valueOf(merchantId));

        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.DOU_DAI_DAI);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(response, "token解密数据为空");
        }
        if (!StringUtils.equals(merchantId, stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 请求参数中的uid不匹配");
            return tokenErrorReturn(response, "token解密uid数据 与 请求参数中的uid不匹配");
        }
        if (merchant == null || !StringUtils.equals(merchant.getOpenId(), stringObjectMap.get("user"))) {
            log.error("token解密user数据 与 数据库中的openId不匹配");
            return tokenErrorReturn(response, "token解密user数据 与 数据库中的openId不匹配");
        }
        String md5Str = TokenUtil.md5forDigest(merchant.getId().toString(), merchant.getOpenId(), merchant.getCtime(), TokenUtil.DOU_DAI_DAI);
        if (!StringUtils.equalsIgnoreCase(stringObjectMap.get("digest"), md5Str)) {
            log.error("token解密后验签失败,token = " + token);
            return tokenErrorReturn(response, "token解密后验签失败");
        }
        return true;
    }

    private Map<String, String> getRequestParams(HttpServletRequest request) {
        String uid = request.getParameter("uid");
        String userId = request.getParameter("userId");
        String pubUid = request.getParameter("pub_uid");
        String aid = request.getParameter("aid");
        String activityId = request.getParameter("activityId");
        String pubType = request.getParameter("type");
        String merchantId = request.getParameter("merchantId");

        Map<String, String> reqParams = new HashMap<>();

        if (StringUtils.isNotBlank(uid) && RegexUtils.StringIsNumber(uid)) {
            reqParams.put("userId", uid);
        }
        if (StringUtils.isNotBlank(userId) && RegexUtils.StringIsNumber(userId)) {
            reqParams.put("userId", userId);
        }
        if (StringUtils.isNotBlank(pubUid) && RegexUtils.StringIsNumber(pubUid)) {
            reqParams.put("pubUserId", pubUid);
        }
        if (StringUtils.isNotBlank(aid) && RegexUtils.StringIsNumber(aid)) {
            reqParams.put("activityId", aid);
        }
        if (StringUtils.isNotBlank(activityId) && RegexUtils.StringIsNumber(activityId)) {
            reqParams.put("activityId", activityId);
        }
        if (StringUtils.isNotBlank(pubType) && RegexUtils.StringIsNumber(pubType)) {
            reqParams.put("pubType", pubType);
        }
        if (StringUtils.isNotBlank(merchantId) && RegexUtils.StringIsNumber(merchantId)) {
            reqParams.put("merchantId", merchantId);
        }

        return reqParams;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    }

    private boolean tokenErrorReturn(HttpServletResponse response, String message) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            JSONObject res = new JSONObject();
            res.put("code", "-2");
            res.put("message", "invalid request");
            if (message != null) {
                res.put("message", message);
            }
            res.put("data", "");
            out.append(res.toString());
            out.flush();
        } catch (IOException e) {
            try {
                response.sendError(500);
            } catch (IOException e1) {
            }
        }
        return false;
    }


    /**
     * @param response 返回参数
     * @param message  提示信息
     * @return 返回与否
     */
    private boolean tokenPermissionReturn(HttpServletResponse response, String message) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            JSONObject res = new JSONObject();
            res.put("code", ApiStatusEnum.ERROR_PERMISSION.getCode());
            res.put("message", ApiStatusEnum.ERROR_PERMISSION.getMessage());
            if (message != null) {
                res.put("message", message);
            }
            res.put("data", "");
            out.append(res.toString());
            out.flush();
        } catch (IOException e) {
            try {
                response.sendError(500);
            } catch (IOException e1) {
            }
        }
        return false;
    }

    /**
     * 检查店铺权限，当前仅活动创建相关
     *
     * @param request   请求
     * @param merchant1 商户信息
     * @return 是否通过
     */
    private boolean checkPermission(HttpServletRequest request, Merchant merchant1) {
        log.info(request.getRequestURI());
        // header中取得数据
        String shopId = request.getHeader(SHOP_PARAM);
        if (StringUtils.isBlank(shopId)) {
            // TODO 容错：后台页面刷新错误
            return true;
        }
        if (StringUtils.equals(shopId, "0")) {
            return true;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(Long.valueOf(shopId));
        if (merchantShop == null) {
            return false;
        }
        // 判断如果是未购买则不允许请求
        String requestUri = request.getRequestURI();
        for (String url : urlForbidden) {
            if (requestUri.startsWith(url)) {
                int version = merchantShop.getPurchaseVersion();
                MerchantShopPurchaseVersionEnum versionEnum = MerchantShopPurchaseVersionEnum.findByCode(version);
                JSONObject jsonObject = JSONObject.parseObject(merchantShop.getPurchaseJson());
                switch (versionEnum) {
                    case VERSION_TEST:
                        return false;
                    case VERSION_TRYOUT:
                    case VERSION_PUBLIC:
                    case VERSION_PRIVATE:
                    case CLEAN:
                    case WEIGHTING:
                        Date expireTime = DateUtil.stringtoDate(jsonObject.getString("expireTime"), DateUtil.FORMAT_ONE);
                        if (expireTime.before(new Date())) {
                            return false;
                        }
                        return true;
                    default:
                        break;
                }
            }
        }
        return true;
    }
}
