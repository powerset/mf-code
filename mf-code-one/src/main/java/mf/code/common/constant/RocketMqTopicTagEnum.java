package mf.code.common.constant;

import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: gel
 * @date: 2019-02-25 10:17
 */
@ToString
public enum RocketMqTopicTagEnum {


    /**
     * 测试模版
     */
    DEMOTOPIC_ALL(1, "demoTopic", "all", "测试"),
    /**
     * 财富大闯关模版消息
     */
    TEMPLATE_CHECKPOINT(2, "template", "checkpoint", "财富大闯关模版消息"),

    TEMPLATE_PUSH_RECALL(2, "template", "push_recall", "粉丝召回"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * topic
     */
    private String topic;
    /**
     * tag
     */
    private String tag;
    /**
     * 描述
     */
    private String desc;

    RocketMqTopicTagEnum(int code, String topic, String tag, String desc) {
        this.code = code;
        this.desc = desc;
        this.topic = topic;
        this.tag = tag;
    }

    public static RocketMqTopicTagEnum findByTopicAndTag(String topic, String tag) {
        for (RocketMqTopicTagEnum topicTagEnum : RocketMqTopicTagEnum.values()) {
            if (StringUtils.equals(topicTagEnum.getTopic(), topic) && StringUtils.equals(topicTagEnum.getTag(), tag)) {
                return topicTagEnum;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getTopic() {
        return topic;
    }

    public String getTag() {
        return tag;
    }

    public String getDesc() {
        return desc;
    }


}
