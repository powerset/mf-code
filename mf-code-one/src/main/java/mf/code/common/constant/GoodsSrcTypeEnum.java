package mf.code.common.constant;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: gel
 * @date: 2018-11-03 10:28
 */
public enum GoodsSrcTypeEnum {
    /**
     * 集客平台
     */
    JKMF(0, "集客魔方"),
    /**
     * 淘宝
     */
    TAOBAO(1, "淘宝"),
    /**
     * 京东
     */
    JD(2, "京东"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    GoodsSrcTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
