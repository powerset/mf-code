package mf.code.common.constant;

/**
 * mf.code.common.constant
 * Description: 任务空间类型枚举
 *
 * @author: gel
 * @date: 2018-12-27 12:02
 */
public enum TaskSpaceTypeEnum {

    /**
     * i.回填订单拿红包 说明：回填已购商品订单号即可拿红包 奖励：神秘额度红包        1 红包
     * b.玩一次幸运大转盘 说明：转动大转盘有机会领取10000元奖金 奖励：0-10000元红包  2 幸运大转盘
     * j.绑定手机号 绑定手机号可以解锁提现等功能 奖励：解绑提现和抽奖功能            9 绑定手机号
     * a.领取一次店铺赠品 说明：完成一定数量好友邀请即可免费领取 奖励：商品全额返现  10 商品免单
     * c.发起一次抽奖 说明：对已购商品发起抽奖可以获得免单机会 奖励：10倍中奖机会 11 10倍中奖机 会
     * d.参加一次抽奖 说明：邀请越多好友抽中免单大奖几率越高 奖励：商品全额返现    12 商品免单
     * f.商品好评晒图 说明：对本店已购商品好评晒图即可拿红包 奖励：神秘额度红包    13 红包
     * g.商品收藏加购 说明：对本店商品进行收藏加购即可拿红包 奖励：神秘额度红包      14 红包
     * 试用                                                                        15
     */

    /**
     * 为了提升随机算法效率 , 不要在改变这个枚举类的顺序 .
     */


    /**
     * 绑定手机号 #################################  Phone(9)
     */
    BIND_PHONE(9, 1, "绑定手机号"),
    /**
     * 领取赠品
     */
    RECEIVE_GIFT(10, 1, "领取赠品"),

    /**
     * 转盘活动 #################################   B(2)
     */
    LUCK_WHEEL(2, 2, "转盘抽奖"),
    /**
     * 发起抽奖活动
     */
    LAUNCH_LUCKY(11, 2, "发起抽奖活动"),
    /**
     * 参与商品抽奖活动
     */
    JOIN_LUCKY(12, 2, "参与商品抽奖活动"),
    /**
     * 参与试用活动
     */
    JOIN_TRIAL(15, 2, "参与试用活动"),
    /**
     * 好评晒图
     */
    GOOD_COMMENT(13, 3, "好评晒图"),
    /**
     * 回填订单
     */
    ORDER_BACK(1, 3, "回填订单"),
    /**
     * 收藏加购
     */
    FAV_CART(14, 3, "收藏加购"),
    /**
     * 拆红包
     */
    OPEN_RED_PACKET(3, 3, "拆红包"),

    /**
     * 领取赠品-v2
     */
    RECEIVE_GIFT_V2(16, 1, "领取赠品"),
    /**
     * 发起抽奖活动-v2-暂无
     */
    LAUNCH_LUCKY_V2(17, 2, "发起抽奖活动"),
    /**
     * 参与商品抽奖活动
     */
    JOIN_LUCKY_V2(18, 2, "参与商品抽奖活动"),
    /**
     * 好评晒图-V2
     */
    GOOD_COMMENT_V2(19, 3, "好评晒图"),
    /**
     * 红包活动（回填订单，收藏加购，好评晒图)
     */
    FAV_CART_V2(20, 3, "收藏加购"),
    ;

    /**
     * code
     */
    private int type;
    /**
     * 类型(1：新手任务，2：活动任务，3：红包任务)
     */
    private int tag;
    /**
     * 描述
     */
    private String desc;

    TaskSpaceTypeEnum(int type, int tag, String desc) {
        this.type = type;
        this.tag = tag;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public int getTag() {
        return tag;
    }

    public String getDesc() {
        return desc;
    }
}
