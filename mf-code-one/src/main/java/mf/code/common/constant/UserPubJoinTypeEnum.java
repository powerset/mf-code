package mf.code.common.constant;

import mf.code.activity.constant.ActivityTypeEnum;

/**
 * mf.code.common.constant
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-01 下午5:36
 */
public enum UserPubJoinTypeEnum {
    /**
     * 未知类型
     */
    UNKNOW(0, "未知类型"),
    /**
     * 幸运大转盘
     */
    LUCK_WHEEL(1, "幸运大转盘"),
    /**
     * 计划类抽奖
     */
    MCH_PLAN(2, "计划类抽奖"),
    /**
     * 新手有礼活动
     */
    NEW_MAN(3, "新手有礼活动"),
    /**
     * 免单商品抽奖发起者
     */
    ORDER_BACK(4, "免单商品抽奖活动"),
    /**
     * 订单随机红包(包裹)
     */
    ORDER_REDPACK(5, "订单随机红包-包裹扫描"),

    /***
     * 助力任务
     */
    ASSIST(6, "助力活动"),

    /**
     * 拆红包发起
     */
    OPEN_RED_PACKET_START(12, "拆红包发起"),
    /**
     * 拆红包参与
     */
    OPEN_RED_PACKET_JOIN(13, "拆红包参与"),
    /***
     * 财富大闯关（平台用户上下级关系 查询此类型）
     */
    CHECKPOINTS(14, "财富大闯关"),
    /**
     * 商城用户层级关系
     */
    SHOPPINGMALL(15, "商城用户层级关系"),

    /**
     * 商城用户层级关系
     */
    FULL_REIMBURSEMENT(16, "全额报销"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;

    UserPubJoinTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static UserPubJoinTypeEnum getTypeEnumByCode(Integer typeCode) {
        for (UserPubJoinTypeEnum userPubJoinTypeEnum: UserPubJoinTypeEnum.values()) {
            if (userPubJoinTypeEnum.getCode() == typeCode) {
                return userPubJoinTypeEnum;
            }
        }
        return null;
    }
    public static UserPubJoinTypeEnum getTypeEnumByCode(String typeCode) {
        return getTypeEnumByCode(Integer.valueOf(typeCode));
    }

    public static int getCodeByActivityType(Integer type) {

        if (ActivityTypeEnum.LUCK_WHEEL.getCode() == type) {
            return LUCK_WHEEL.getCode();
        }
        if (ActivityTypeEnum.MCH_PLAN.getCode() == type) {
            return MCH_PLAN.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_START.getCode() == type || ActivityTypeEnum.ORDER_BACK_START_V2.getCode() == type) {
            return ORDER_BACK.getCode();
        }
        if (ActivityTypeEnum.ORDER_BACK_JOIN.getCode() == type || ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode() == type) {
            return ORDER_BACK.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_START.getCode() == type) {
            return NEW_MAN.getCode();
        }
        if (ActivityTypeEnum.NEW_MAN_JOIN.getCode() == type) {
            return NEW_MAN.getCode();
        }
        if (ActivityTypeEnum.ASSIST.getCode() == type || ActivityTypeEnum.ASSIST_V2.getCode() == type) {
            return ASSIST.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() == type) {
            return OPEN_RED_PACKET_START.getCode();
        }
        if (ActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode() == type) {
            return OPEN_RED_PACKET_JOIN.getCode();
        }

        return UNKNOW.getCode();
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
