package mf.code.common.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @Auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月17日 14:37
 * @Description:
 */
public class DateUtil {
    // 格式：年月日时分秒
    public static final String FORMAT_TOKEN = "yyyyMMddHHmmss";

    // 格式：年－月－日 小时：分钟：秒
    public static final String FORMAT_ONE = "yyyy-MM-dd HH:mm:ss";

    // 格式：年－月－日 小时：分钟
    public static final String FORMAT_TWO = "yyyy-MM-dd HH:mm";

    // 格式：年月日 小时分钟秒
    public static final String FORMAT_THREE = "yyyyMMdd-HHmmss";

    // 格式：小时：分钟
    public static final String FORMAT_FOUR = "yyyyMMdd";
    // 格式：年月日
    public static final String FORMAT_FIVE = "HH:mm";

    // 格式：年－月－日
    public static final String LONG_DATE_FORMAT = "yyyy-MM-dd";

    // 格式：年.月.日
    public static final String POINT_DATE_FORMAT = "yyyy.MM.dd";

    // 格式：年.月.日 时.分
    public static final String POINT_DATE_FORMAT_TWO = "yyyy.MM.dd HH.mm";

    // 格式：月－日
    public static final String SHORT_DATE_FORMAT = "MM-dd";

    // 格式：小时：分钟：秒
    public static final String LONG_TIME_FORMAT = "HH:mm:ss";

    //格式：年-月
    public static final String MONTG_DATE_FORMAT = "yyyy-MM";

    // 年的加减
    public static final int SUB_YEAR = Calendar.YEAR;

    // 月加减
    public static final int SUB_MONTH = Calendar.MONTH;

    // 天的加减
    public static final int SUB_DAY = Calendar.DATE;

    // 小时的加减
    public static final int SUB_HOUR = Calendar.HOUR;

    // 分钟的加减
    public static final int SUB_MINUTE = Calendar.MINUTE;

    // 秒的加减
    public static final int SUB_SECOND = Calendar.SECOND;

    static final String dayNames[] = {"星期日", "星期一", "星期二", "星期三", "星期四",
            "星期五", "星期六"};
    /**
     * 一月三十天的毫秒数
     */
    public static final Long ONE_MONTH_MILLIS = 30L * 24 * 60 * 60 * 1000;
    /**
     * 一天的毫秒数
     */
    public static final Long ONE_DAY_MILLIS = 24L * 60 * 60 * 1000;
    /**
     * 一小时的毫秒数
     */
    public static final Long ONE_HOUR_MILLIS = 60L * 60 * 1000;
    /**
     * 一分钟的毫秒数
     */
    public static final Long ONE_MINUTE_MILLIS = 60L * 1000;

    public DateUtil() {
    }

    /**
     * 今日剩余时间
     *
     * @return
     */
    public static Long getTimeoutSecond() {
        Date now = new Date();
        Date dateBeforeZero = DateUtil.getDateBeforeZero(now);
        return DateUtil.secondDiff(now, dateBeforeZero);
    }

    /**
     * 获取某天0点
     *
     * @param date
     * @return
     */
    public static Date getDateAfterZero(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取一天的开始结束时间 ,返回数组 . 第一个是这天的开始.第二个是这天的结束
     *
     * @param date
     * @return
     */
    public static Date[] getBeginAndEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Date[] dates = new Date[2];
        dates[0] = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        dates[1] = calendar.getTime();

        return dates;
    }

    /**
     * 获取某天23:59:59
     */
    public static Date getDateBeforeZero(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }


    /**
     * 获取当前毫秒数
     *
     * @return
     */
    public static Long getCurrentMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 获取当前秒数
     *
     * @return
     */
    public static Integer getCurrentSeconds() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    /**
     * 获取两个时间相差毫秒数
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static Long dateMillsDiff(Date startTime, Date endTime) {
        if (startTime.after(endTime)) {
            return 0L;
        }
        return endTime.getTime() - startTime.getTime();
    }

    /**
     * 添加N天
     *
     * @param now
     * @param day
     * @return
     */
    public static Date addDay(Date now, int day) {
        if (now == null) {
            return now;
        }
        return new Date(now.getTime() + day * DateUtil.ONE_DAY_MILLIS);
    }


    public static Date parseDate(String str, Date defaultValue, String pattern) {
        if (StringUtils.isNotBlank(str)) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            try {
                defaultValue = sdf.parse(str);
            } catch (ParseException ignored) {
            }
        }
        return defaultValue;
    }

    /**
     * 把符合日期格式的字符串转换为日期类型
     */
    public static Date stringtoDate(String dateStr, String format) {
        Date d = null;
        SimpleDateFormat formater = new SimpleDateFormat(format);
        try {
            formater.setLenient(false);
            d = formater.parse(dateStr);
        } catch (Exception e) {
            // log.error(e);
            d = null;
        }
        return d;
    }

    /**
     * 把符合日期格式的字符串转换为日期类型
     */
    public static Date stringtoDate(String dateStr, String format, ParsePosition pos) {
        Date d = null;
        SimpleDateFormat formater = new SimpleDateFormat(format);
        try {
            formater.setLenient(false);
            d = formater.parse(dateStr, pos);
        } catch (Exception e) {
            d = null;
        }
        return d;
    }

    /**
     * 把日期转换为字符串
     */
    public static String dateToString(Date date, String format) {
        String result = "";
        SimpleDateFormat formater = new SimpleDateFormat(format);
        try {
            result = formater.format(date);
        } catch (Exception e) {
            // log.error(e);
        }
        return result;
    }

    /**
     * 获取当前第几周
     *
     * @return int
     */
    public static Integer getThisWeek() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        //设置周一为一周的第一天
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        int num = cal.get(Calendar.WEEK_OF_YEAR);
        return num;
    }

    /**
     * 获取昨天 yyyy-MM-dd格式 ->数据库
     */
    public static String getYesterday() {
        return DateUtil.dateToString(DateUtil.addDay(new Date(), -1), DateUtil.LONG_DATE_FORMAT);
    }

    /**
     * 获取当前时间的指定格式
     */
    public static String getCurrDate(String format) {
        return dateToString(new Date(), format);
    }

    /**
     * @Title: dateSub
     * @Date 2014-1-9 上午10:44:02
     * @Description: 得到指定日期前(后)的日期
     * @param: @param dateKind  例：Calendar.DAY_OF_MONTH
     * @param: @param dateStr  指定日期
     * @param: @param amount   增加(减去)的时间量
     * @param: @return
     * @return: String
     * @author mtf
     */
    public static String dateSub(int dateKind, String dateStr, int amount) {
        Date date = stringtoDate(dateStr, MONTG_DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(dateKind, amount);
        return dateToString(calendar.getTime(), FORMAT_ONE);
    }

    /**
     * 昨日日期
     *
     * @return
     */
    public static String yearthDate(String dateStr) {
        Date date = stringtoDate(dateStr, LONG_DATE_FORMAT);//取时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);//把日期往后增加一天.整数往后推,负数往前移动
        //date=calendar.getTime();   //这个时间就是日期往后推一天的结果
        return dateToString(calendar.getTime(), LONG_DATE_FORMAT);
    }

    /**
     * 两个日期相减
     *
     * @return 相减得到的毫秒数
     */
    public static long timeSub(String firstTime, String secTime, String pattern) {
        long first = stringtoDate(firstTime, pattern).getTime();
        long second = stringtoDate(secTime, pattern).getTime();
        return (second - first);
    }

    /**
     * 两个日期相减
     * 参数地DATE
     * second 两个日期相差的秒
     *
     * @return 相减得到的秒数
     * 后面时间减去前面时间  再减去 相差秒数    如果大于0 返回 FASLE
     */
    public static boolean timeSub(Date firstTime, Date secTime, long secs) {
        long first = firstTime.getTime();
        long second = secTime.getTime();
        // 判断两个时间 是否间隔那么长 secs。
        return (second - first - secs) <= 0;
    }

    /**
     * 两个日期相减
     * 参数地DATE
     *
     * @return 相减得到的秒数
     * 后面时间减去前面时间  如果大于0 返回 false
     */
    public static boolean timeSub(Date firstTime, Date secTime) {
        long first = firstTime.getTime();
        long second = secTime.getTime();
        return (second - first) <= 0;
    }

    /**
     * 时间计算 今天/昨天/前天
     *
     * @param date /
     * @return /
     */
    public static String todayYesterdayBeforYesterday(Date date) {
        // 比今天 0:00:00 大
        if (timeSub(date,getBeginAndEnd(new Date())[0])) {
            return "今天";
        } else if (timeSub(date,getBeginAndEnd(DateUtil.addDay(new Date(), -1))[0])) {
            return "昨天";
        } else if (timeSub(date,getBeginAndEnd(DateUtil.addDay(new Date(), -2))[0])) {
            return "前天";
        } else {
            return DateUtil.dateToString(date, FORMAT_TWO);
        }
    }

    /**
     * 获得某月的天数
     */
    public static int getDaysOfMonth(String year, String month) {
        int days = 0;
        if ("1".equals(month) || "3".equals(month) || "5".equals(month)
                || "7".equals(month) || "8".equals(month) || "10".equals(month)
                || "12".equals(month)) {
            days = 31;
        } else if ("4".equals(month) || "6".equals(month) || "9".equals(month)
                || "11".equals(month)) {
            days = 30;
        } else {
            if ((Integer.parseInt(year) % 4 == 0 && Integer.parseInt(year) % 100 != 0)
                    || Integer.parseInt(year) % 400 == 0) {
                days = 29;
            } else {
                days = 28;
            }
        }

        return days;
    }

    /**
     * 获取某年某月的天数
     */
    public static int getDaysOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获得当前日期
     */
    public static int getToday() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DATE);
    }

    /**
     * 获得当前月份
     */
    public static int getToMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获得当前年份
     */
    public static int getToYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 返回日期的天
     */
    public static int getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DATE);
    }

    /**
     * 返回日期的年
     */
    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 返回日期的月份，1-12
     */
    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 计算两个日期相差的天数，如果date2 > date1 返回正数，否则返回负数
     */
    public static long dayDiff(Date date1, Date date2) {
        return (date2.getTime() - date1.getTime()) / 86400000;
    }


    /**
     * 计算两个日期相差的分钟，如果date2 > date1 返回正数，否则返回负数
     */
    public static long minuteDiff(Date date1, Date date2) {
        return (date2.getTime() - date1.getTime()) / 60000;
    }

    /**
     * 计算两日期的秒差值,如果date2 > date1 返回正数，否则返回负数
     */
    public static long secondDiff(Date d1, Date d2) {
        return (d2.getTime() - d1.getTime()) / 1000;
    }

    /**
     * 比较两个日期的年差
     */
    public static int yearDiff(String before, String after) {
        Date beforeDay = stringtoDate(before, LONG_DATE_FORMAT);
        Date afterDay = stringtoDate(after, LONG_DATE_FORMAT);
        return getYear(afterDay) - getYear(beforeDay);
    }

    /**
     * 比较指定日期与当前日期的差
     */
    public static int yearDiffCurr(String after) {
        Date beforeDay = new Date();
        Date afterDay = stringtoDate(after, LONG_DATE_FORMAT);
        return getYear(beforeDay) - getYear(afterDay);
    }

    /**
     * 获取每月的第一周
     */
    public static int getFirstWeekdayOfMonth(int year, int month) {
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.SATURDAY); // 星期天为第一天
        c.set(year, month - 1, 1);
        return c.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 获取每月的最后一周
     */
    public static int getLastWeekdayOfMonth(int year, int month) {
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.SATURDAY); // 星期天为第一天
        c.set(year, month - 1, getDaysOfMonth(year, month));
        return c.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 获得当前日期字符串，格式"yyyy-MM-dd HH:mm:ss"
     *
     * @return
     */
    public static String getNow() {
        Calendar today = Calendar.getInstance();
        return dateToString(today.getTime(), FORMAT_ONE);
    }


    /**
     * 判断日期是否有效,包括闰年的情况
     *
     * @param date YYYY-mm-dd
     * @return
     */
    public static boolean isDate(String date) {
        StringBuffer reg = new StringBuffer(
                "^((\\d{2}(([02468][048])|([13579][26]))-?((((0?");
        reg.append("[13578])|(1[02]))-?((0?[1-9])|([1-2][0-9])|(3[01])))");
        reg.append("|(((0?[469])|(11))-?((0?[1-9])|([1-2][0-9])|(30)))|");
        reg.append("(0?2-?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][12");
        reg.append("35679])|([13579][01345789]))-?((((0?[13578])|(1[02]))");
        reg.append("-?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))");
        reg.append("-?((0?[1-9])|([1-2][0-9])|(30)))|(0?2-?((0?[");
        reg.append("1-9])|(1[0-9])|(2[0-8]))))))");
        Pattern p = Pattern.compile(reg.toString());
        return p.matcher(date).matches();
    }


    /*****
     * 时间 增加、减少 n个小时以后时间
     * @param d
     *          YYYY-mm-dd HH:mm:ss
     * @param num>0  小时
     * @param type  增加和减少标志
     * **/
    public static Date adjustDateByHour(Date d, Integer num, int type) {
        Calendar Cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Cal.setTime(d);
        if (type == 0) {
            Cal.add(Calendar.MINUTE, -num);
            // System.out.println("date:"+df.format(Cal.getTime()));

        } else {
            Cal.add(Calendar.MINUTE, num);
            //System.out.println("date:"+df.format(Cal.getTime()));
        }
        return Cal.getTime();
    }

    /*****
     * 时间 增加、减少 n个分钟以后时间
     * @param d
     *          YYYY-mm-dd HH:mm:ss
     * @param num>0  分钟
     * @param type  增加和减少标志
     * **/
    public static Date adjustDateByMinutes(Date d, Integer num, int type) {
        Calendar Cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Cal.setTime(d);
        if (type == 0) {
            Cal.add(Calendar.MINUTE, -num);
            //  System.out.println("date:"+df.format(Cal.getTime()));

        } else {
            Cal.add(Calendar.MINUTE, num);
            //   System.out.println("date:"+df.format(Cal.getTime()));
        }
        return Cal.getTime();
    }

    /**
     * 区间内所有日期
     *
     * @param dBegin
     * @param dEnd
     * @return
     */
    public static List<Date> findDates(Date dBegin, Date dEnd) {
        List lDate = new ArrayList();
        lDate.add(dBegin);
        Calendar calBegin = Calendar.getInstance();
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(dEnd);
        while (dEnd.after(calBegin.getTime())) {
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            lDate.add(calBegin.getTime());
        }
        return lDate;
    }


    /**
     * 当前三个月之前的日期
     *
     * @return
     */
    public static Date threeMouthBefore() {
        Long beforeTime = DateUtil.getCurrentMillis() - ONE_MONTH_MILLIS * 3;
        return new Date(beforeTime);
    }

    public static String getYYMMDD() {
        Calendar instance = Calendar.getInstance();
        Integer yy = instance.get(Calendar.YEAR);
        Integer mm = instance.get(Calendar.MONTH) + 1;
        Integer dd = instance.get(Calendar.DATE);
        return yy.toString() + mm.toString() + dd.toString();
    }

    public static int getNaturalDay2Second() {
        // 过期时间 1天（自然日）
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date endTime = cal.getTime();
        return (int) ((endTime.getTime() - System.currentTimeMillis()) / 1000);
    }

    /**
     * 获取 上月最后一天的结束时间
     */
    public static Date getEndOfLastMonth() {
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // 本月
        int month = DateUtil.getMonth(date) - 1;
        // 上月
        int lastMonth = month - 1;
        // -1 转 12月
        if (lastMonth == -1) {
            int year = DateUtil.getYear(date);
            calendar.set(Calendar.YEAR, year - 1);
            lastMonth = 11;
        }

        calendar.set(Calendar.MONTH, lastMonth);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }
    /**
     * 获取 上月第一天的开始时间
     */
    public static Date getBeginOfLastMonth() {
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // 本月
        int month = DateUtil.getMonth(date) - 1;
        // 上月
        int lastMonth = month - 1;
        // -1 转 12月
        if (lastMonth == -1) {
            int year = DateUtil.getYear(date);
            calendar.set(Calendar.YEAR, year - 1);
            lastMonth = 11;
        }

        calendar.set(Calendar.MONTH, lastMonth);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    /**
     * 获取 上俩月第一天的开始时间
     */
    public static Date getBeginOfLastTwoMonth() {
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // 本月
        int month = DateUtil.getMonth(date) - 1;
        // 上月
        int lastTwoMonth = month - 2;
        // -1 转 12月
        if (lastTwoMonth == -1) {
            int year = DateUtil.getYear(date);
            calendar.set(Calendar.YEAR, year - 1);
            lastTwoMonth = 11;
        }
        // -2 转 11月
        if (lastTwoMonth == -2) {
            int year = DateUtil.getYear(date);
            calendar.set(Calendar.YEAR, year - 1);
            lastTwoMonth = 10;
        }

        calendar.set(Calendar.MONTH, lastTwoMonth);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    /**
     * 获取 上俩月第一天的开始时间
     */
    public static Date getEndOfLastTwoMonth() {
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // 本月
        int month = DateUtil.getMonth(date) - 1;
        // 上月
        int lastTwoMonth = month - 2;
        // -1 转 12月
        if (lastTwoMonth == -1) {
            int year = DateUtil.getYear(date);
            calendar.set(Calendar.YEAR, year - 1);
            lastTwoMonth = 11;
        }
        // -2 转 11月
        if (lastTwoMonth == -2) {
            int year = DateUtil.getYear(date);
            calendar.set(Calendar.YEAR, year - 1);
            lastTwoMonth = 10;
        }

        calendar.set(Calendar.MONTH, lastTwoMonth);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }

    /**
     * 获取 本月第一天的开始时间
     */
    public static Date getBeginOfThisMonth() {
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    /**
     * 获取 本月最后一天的结束时间
     */
    public static Date getEndOfThisMonth() {
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }

    /**
     * 获取 下月第一天的开始时间
     */
    public static Date getBeginOfNextMonth() {
        Date date = DateUtils.addMonths(new Date(), 1);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    /**
     * 获取 下月最后一天的结束时间
     */
    public static Date getEndOfNextMonth() {
        Date date = DateUtils.addMonths(new Date(), 1);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }
}
