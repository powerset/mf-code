package mf.code.common.utils;/**
 * create by gbf on 2018/11/2 0002
 */

import com.alibaba.fastjson.JSON;
import mf.code.common.exception.ArgumentException;
import org.apache.commons.lang.StringUtils;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author gbf
 * @description :
 */
public class Assert {
    /**
     * 当表达式为false,则抛出异常.
     * <pre>
     *     isTrue(true , ...)  : 通过
     *     isTrue(false , ...) : 抛异常
     * </pre>
     *
     * @param expression   表达式
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isTrue(boolean expression, Integer errorCode, String errorMessage) {
        if (!expression) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 断言不为空
     * <pre class="code">Assert.notNull(shop, "The class must not be null");</pre>
     *
     * @param object       待检查对象
     * @param errorCode    断言失败时要使用的异常编码
     * @param errorMessage 断言失败时要使用的异常消息
     * @throws IllegalArgumentException if the object is {@code null}
     */
    public static void notNull(@Nullable Object object, Integer errorCode, String errorMessage) {
        if (object == null) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 判断参数是否是空字符串
     * <pre>
     *   isBlank(null , ...)      : 抛异常
     *   isBlank("" , ...)        : 抛异常
     *   isBlank(" " , ...)       : 抛异常
     *   isBlank("bob" , ...)     : 通过
     *   isBlank("  bob  " , ...) : 通过
     * </pre>
     *
     * @param argument     校验参数
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isBlank(String argument, Integer errorCode, String errorMessage) {
        if (StringUtils.isBlank(argument)) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 是数字 ,包括:整数 / 小数 .如果不是数字,抛异常
     * <pre>
     *     isNumber("2",  ...)                         : 通过
     *     isNumber("2.1",  ...)                       : 通过
     *     isNumber("99999999999999999999999",  ...)   : 通过  (超过Long的长度也会通过)
     *     isNumber("99999999999999999999999.1",  ...) : 通过  (超过Decimal的长度也会通过)
     *     isNumber("a",  ...)                         : 抛异常
     *     isNumber("2a", ...)                         : 抛异常
     * </pre>
     *
     * @param strNummber   字符串
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isNumber(String strNummber, Integer errorCode, String errorMessage) {
        isBlank(strNummber, errorCode, errorMessage);

        boolean isNumber = NumberValidationUtil.isRealNumber(strNummber);
        if (!isNumber) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 是整数,如果不是整数,抛异常
     * <pre>
     *      isInteger("2",  ...)                         : 通过
     *      isInteger("99999999999999999999999",  ...)   : 通过  (超过Long的长度也会通过)
     * </pre>
     *
     * @param strNumber    字符串
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isInteger(String strNumber, int errorCode, String errorMessage) {
        boolean isNumber = NumberValidationUtil.isInteger(strNumber);
        if (!isNumber) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 是整数,如果不是整数,抛异常
     * <pre>
     *      isLong("2",  ...)                         : 通过
     *      isLong("99999999999999999999999",  ...)   : 通过  (超过Long的长度也会通过)
     * </pre>
     *
     * @param strNumber    字符串
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isLong(String strNumber,int errorCode,String errorMessage){
        isInteger(strNumber,errorCode,errorMessage);
    }

    /**
     * 断言前端以 regex 分割的参数,如果以 regex 分割的字符串,有非数字,抛异常
     * <pre>
     *     isIntegerArray("1,2,3" , "," , ...)   : 通过
     *     isIntegerArray("1,2,a" , "," , ...)   : 不通过
     *     isIntegerArray("1,2.1" , "," , ...)   : 不通过
     * </pre>
     *
     * @param argsStr      校验参数
     * @param regex        分隔符
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isIntegerArray(String argsStr, String regex, int errorCode, String errorMessage) {
        //是否包含字符
        hasText(argsStr, errorCode, errorMessage);
        String[] argsArr = argsStr.split(regex);

        for (int i = 0; i < argsArr.length; i++) {
            isInteger(argsArr[i], errorCode, errorMessage);
        }
    }

    /**
     * 字符串有值,如果没用值,抛异常
     *
     * @param text         字符串
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void hasText(@Nullable String text, int errorCode, String errorMessage) {
        if (!org.springframework.util.StringUtils.hasText(text)) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 可以转为Date类型日期,如果不能,抛异常
     * <pre>
     *     canConvertDate("2019-01-01","YYYY-MM-DD" ...)   : 通过
     *     canConvertDate("2019-22-01","YYYY-MM-DD" ...)   : 不通过
     *     canConvertDate("2019-AA","YYYY-MM-DD" ...)      : 不通过
     *     canConvertDate("20190301","YYYY-MM-DD" ...)     : 不通过
     * </pre>
     *
     * @param dateStr      日期字符串
     * @param dateFormat   日期格式化
     * @param errorCode    错误码
     * @param errorMessage 错误类型
     */
    public static void canConvertDate(String dateStr, String dateFormat, int errorCode, String errorMessage) {
        hasText(dateStr, errorCode, errorMessage);
        Date date = DateUtil.stringtoDate(dateStr, dateFormat);
        if (date == null) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 结束日期在开始日期之后(字符串类型日期)
     * <pre>
     *     isRightDateArgument("2019-01-01","2019-03-01","YYYY-MM-DD" , ...)  : 通过
     *     isRightDateArgument("2019-04-01","2019-03-01","YYYY-MM-DD" , ...)  : 不通过
     *     isRightDateArgument("20190101","20190301","YYYY-MM-DD" , ...)      : 不通过
     * </pre>
     *
     * @param beginDateStr 开始时间
     * @param endDateStr   结束时间
     * @param dateFormat   格式化类型
     * @param errorCode    错误码
     * @param errorMessage 错误类型
     */
    public static void isRightDateArgument(String beginDateStr, String endDateStr, String dateFormat, Integer errorCode, String errorMessage) {
        canConvertDate(beginDateStr, dateFormat, errorCode, "开始日期格式不正确");
        canConvertDate(endDateStr, dateFormat, errorCode, "结束日期格式不正确");
        isEndDateAfterBeginDate(
                DateUtil.stringtoDate(beginDateStr, dateFormat),
                DateUtil.stringtoDate(endDateStr, dateFormat),
                errorCode,
                errorMessage);
    }

    /**
     * 结束时间在开始时间之后,如果结束时间在开始时间之前, 抛异常.
     * <pre>
     *     与isRightDateArgument(...)相同
     * </pre>
     *
     * @param beginDate    开始时间
     * @param endDate      结束时间
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */

    public static void isEndDateAfterBeginDate(Date beginDate, Date endDate, Integer errorCode, String errorMessage) {
        isInstanceOf(Date.class, beginDate, errorCode, "开始时间不是日期");
        isInstanceOf(Date.class, endDate, errorCode, "结束时间不是日期");
        if (beginDate.after(endDate)) {
            if (StringUtils.isBlank(errorMessage)) {
                errorMessage = "结束时间早于开始时间";
            }
            throwException(errorCode, errorMessage);
        }
    }


    /**
     * 数字,不再规定区间 (min<arg<max) 抛出异常
     * <pre>
     *     isRightNumberSection(1,1,5,...) : 通过
     *     isRightNumberSection(5,1,5,...) : 通过
     *     isRightNumberSection(4,1,5,...) : 通过
     *     isRightNumberSection(0,1,5,...) : 不通过
     *     isRightNumberSection(7,1,5,...) : 不通过
     * </pre>
     *
     * @param arg          被校验参数
     * @param min          最小值
     * @param max          最大值
     * @param errorCode    错误信息
     * @param errorMessage 错误信息
     */
    public static void isRightNumberSection(Integer arg, Integer min, Integer max, Integer errorCode, String errorMessage) {
        if (arg > max || arg < min) {
            if (errorMessage == null) {
                errorMessage = "参数不在合法区间";
            }
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * json对象校验
     *
     * @param json         json格式字符串
     * @param clazz        格式化对象类型
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isJsonObj(String json, Class clazz, Integer errorCode, String errorMessage) {
        try {
            JSON.parseObject(json, clazz);
        } catch (RuntimeException e) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * json数组校验
     *
     * @param json         json格式字符串
     * @param clazz        格式化对象类型
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void isJsonArray(String json, Class clazz, Integer errorCode, String errorMessage) {
        try {
            JSON.parseArray(json, clazz);
        } catch (RuntimeException e) {
            throwException(errorCode, errorMessage);
        }
    }

    /**
     * 判断数字是否大于零
     *
     * @param number       数字
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void numberBiggerThanZeroNotNull(Number number, Integer errorCode, String errorMessage) {
        if (number == null) throwException(errorCode, "该数字为空");
        numberBiggerThanZeroNullable(number, errorCode, errorMessage);
    }

    /**
     * 判断数字是否大于零
     *
     * @param number       数字
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     */
    public static void numberBiggerThanZeroNullable(Number number, Integer errorCode, String errorMessage) {
        if (number == null) return;
        if (number instanceof Integer) {
            if (number.intValue() < 0) throwException(errorCode, errorMessage);
        } else if (number instanceof Long) {
            if (number.longValue() < 0) throwException(errorCode, errorMessage);
        } else if (number instanceof Float) {
            if (number.floatValue() < 0) throwException(errorCode, errorMessage);
        } else if (number instanceof BigDecimal) {
            if (((BigDecimal) number).compareTo(BigDecimal.ZERO) < 0) throwException(errorCode, errorMessage);
        } else if (number instanceof Double) {
            if (number.doubleValue() < 0) throwException(errorCode, errorMessage);
        }
    }

    /**
     * 统一处理errorCode和errorMessage的null值
     * errorCode默认值: -1
     * errorMessage默认值:参数异常
     *
     * @param errorCode    错误码(返回给用户)
     * @param errorMessage 错误信息(返回给用户)
     */
    private static void throwException(Integer errorCode, String errorMessage) {
        throw new ArgumentException(errorCode == null ? -1 : errorCode, errorMessage == null ? "参数异常" : errorMessage);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // 下面是spring的断言,因为要统一异常处理,所以复制过来. 由IllegalStateException改为ArgumentException
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private static void isInstanceOf(Class<?> type, @Nullable Object obj, Integer errorCode, String errorMessage) {
        notNull(type, errorCode, errorMessage);
        if (!type.isInstance(obj)) {
            instanceCheckFailed(type, obj, errorCode, errorMessage);
        }
    }

    private static void instanceCheckFailed(Class<?> type, @Nullable Object obj, Integer errorCode, @Nullable String msg) {
        String className = obj != null ? obj.getClass().getName() : "null";
        String result = "";
        boolean defaultMessage = true;
        if (org.springframework.util.StringUtils.hasLength(msg)) {
            if (endsWithSeparator(msg)) {
                result = msg + " ";
            } else {
                result = messageWithTypeName(msg, className);
                defaultMessage = false;
            }
        }

        if (defaultMessage) {
            result = result + "该类 [" + className + "] 不能转为 " + type;
        }

        throwException(errorCode, result);
    }


    private static boolean endsWithSeparator(String msg) {
        return msg.endsWith(":") || msg.endsWith(";") || msg.endsWith(",") || msg.endsWith(".");
    }

    private static String messageWithTypeName(String msg, @Nullable Object typeName) {
        return msg + (msg.endsWith(" ") ? "" : ": ") + typeName;
    }
}
