package mf.code.common.utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RandomStrUtil {
    public static final String REGULAR_NAME_CHINESE = "[\u4e00-\u9fa5]";

    /***
     * 计算字符串的字节数
     * @param value
     * @return
     */
    public static int stringLength(String value) {
        int valueLength = 0;
        String chinese = REGULAR_NAME_CHINESE;
        for (int i = 0; i < value.length(); i++) {
            String temp = value.substring(i, i + 1);
            if (temp.matches(chinese)) {
                valueLength += 2;
            } else {
                valueLength += 1;
            }
        }
        return valueLength;
    }

    /**
     * 随机生成六位数验证码
     *
     * @return
     */
    public static String getRandomNum() {
        Random r = new Random();
        int result = r.nextInt(900000) + 100000;
        return result + "";
    }

    /**
     * 获取随机长度的字符串
     *
     * @param length 需要的长度
     * @return
     */
    public static String randomStr(int length) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < length; j++) {
            //生成一个97-122之间的int类型整数--为了生成小写字母
            int intValL = (int) (Math.random() * 26 + 97);
            //生成一个65-90之间的int类型整数--为了生成大写字母
            int intValU = (int) (Math.random() * 26 + 65);
            //生成一个30-39之间的int类型整数--为了生成数字
            int intValN = (int) (Math.random() * 10 + 48);
            int intVal = 0;
            int r = (int) (Math.random() * 3);
            if (r == 0) {
                intVal = intValL;
            } else if (r == 1) {
                intVal = intValU;
            } else {
                intVal = intValN;
            }
            sb.append((char) intVal);
        }
        return sb.toString();
    }


    /**
     * 根据概率列表随机金额 列表格式[{"min":0.1,"max":1,"prop":80},{"min":1.1,"max":3,"prop":10},{"min":3.1,"max":5,"prop":10}]
     *
     * @param mapList
     * @param scale
     * @return
     */
    public static BigDecimal randomAmount(List<Map> mapList, Integer scale) {
        if (scale == null) {
            scale = 2;
        }
        BigDecimal redPack = BigDecimal.ZERO;
        int total = 0;
        for (Map<String, Object> map : mapList) {
            total = total + (Integer) map.get("prop");
        }
        Random random = new Random();
        int realProp = random.nextInt(total);
        Random randomAmount = new Random();
        BigDecimal hundred = new BigDecimal("100");
        for (Map<String, Object> map : mapList) {
            realProp = realProp - (Integer) map.get("prop");
            if (realProp <= 0) {
                int max = new BigDecimal(map.get("max").toString()).multiply(hundred).intValue();
                int min = new BigDecimal(map.get("min").toString()).multiply(hundred).intValue();
                int amount = randomAmount.nextInt(max - min);
                redPack = new BigDecimal(amount + min);
                break;
            }
        }
        redPack = redPack.divide(hundred, scale, BigDecimal.ROUND_HALF_UP);
        if (redPack.doubleValue() <= 0) {
            return BigDecimal.ZERO;
        }
        return redPack;
    }

    /***
     * 随机值
     * @param min 最小值
     * @param max 总和
     * @param scl 小数最大位数
     * @return
     */
    public static BigDecimal randomNum(double min, double max, int scl){
        //指定小数位
        int pow = (int) Math.pow(10, scl);
        double one = Math.floor((Math.random() * (max - min) + min) * pow) / pow;
        return new BigDecimal(String.valueOf(one));
    }
}
