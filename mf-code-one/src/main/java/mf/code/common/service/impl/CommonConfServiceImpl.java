package mf.code.common.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import mf.code.common.service.CommonConfService;
import mf.code.common.utils.DateUtil;
import mf.code.one.dto.ApolloLHYXDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service
public class CommonConfServiceImpl implements CommonConfService {


    @Autowired
    private ApolloPropertyService apolloPropertyService;

    @Override
    public ApolloLHYXDTO getUnionConf() {
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        ApolloLHYXDTO result = new ApolloLHYXDTO();

        List<Long> times = new ArrayList<>();
        Long now = System.currentTimeMillis();

        times.add(now);
        Date preStartTime = DateUtil.stringtoDate(apolloLhyxProperty.getLhyx().getPreActivity().getStartTime(), DateUtil.FORMAT_ONE);
        times.add(preStartTime.getTime());
        Date preEndTime = DateUtil.stringtoDate(apolloLhyxProperty.getLhyx().getPreActivity().getEndTime(), DateUtil.FORMAT_ONE);
        times.add(preEndTime.getTime());
        Date startTime = DateUtil.stringtoDate(apolloLhyxProperty.getLhyx().getActivity().getStartTime(), DateUtil.FORMAT_ONE);
        times.add(startTime.getTime());
        Date endTime = DateUtil.stringtoDate(apolloLhyxProperty.getLhyx().getActivity().getEndTime(), DateUtil.FORMAT_ONE);
        times.add(endTime.getTime());
        Collections.sort(times, new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                if (o1 - o2 < 0) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        // 获取状态
        int status = this.searchFromList(times, now);
        result.setStatus(status);
        switch (status) {
            case 0:
                // 预热活动未开始
                result.setTime(preStartTime.getTime() - now);
                break;
            case 1:
                // 预热活动进行中
                result.setTime(preEndTime.getTime() - now);
                break;
            case 2:
                // 预热活动已结束
                // 预热活动和正式活动重合的情况
                if (startTime.getTime() < now){
                    status = 3;
                    result.setTime(endTime.getTime() - now);
                } else {
                    result.setTime(startTime.getTime() - now);
                }
                break;
            case 3:
                // 正式活动进行中
                result.setTime(endTime.getTime() - now);
                break;
            case 4:
                // 正式活动已结束
                break;
        }

        result.setCount(apolloLhyxProperty.getLhyx().getPreActivity().getCount());
        result.setMoney(apolloLhyxProperty.getLhyx().getPreActivity().getMoney());

        // 获取banner
        List<ApolloLHYXDTO.Banner> BannerList = new ArrayList<>();
        for (ApolloLhyxProperty.Banner banner : apolloLhyxProperty.getLhyx().getActivity().getBanner()) {
            ApolloLHYXDTO.Banner banner1 = new ApolloLHYXDTO.Banner();
            banner1.setUrl(banner.getUrl());
            banner1.setGid(banner.getGid());
            BannerList.add(banner1);
        }

        // 获取爆款商品id
        result.setHotIds(apolloLhyxProperty.getLhyx().getActivity().getBaokuanGids());

        // 获取广告位商品id
        result.setGroup1Ids(apolloLhyxProperty.getLhyx().getActivity().getGroup1Gids());
        result.setGroup2Ids(apolloLhyxProperty.getLhyx().getActivity().getGroup2Gids());

        // 获取专属推荐列表商品id
        List<ApolloLHYXDTO.Recommend> recommends = new ArrayList<>();
        for (ApolloLhyxProperty.Zstj zstj:apolloLhyxProperty.getLhyx().getZstj()){
            ApolloLHYXDTO.Recommend recommend = new ApolloLHYXDTO.Recommend();
            recommend.setGid(zstj.getGid());
            recommends.add(recommend);
        }
        result.setRecommend(recommends);
        result.setBanner(BannerList);

        // 获取正式活动页卡片时间配置
        List<ApolloLhyxProperty.Time> timesConfList = apolloLhyxProperty.getLhyx().getActivity().getTimes();
        List<String> timeList = timesConfList
                .stream()
                .map(ApolloLhyxProperty.Time::getTime)
                .collect(Collectors.toList());
        result.setTimes(timeList);
        return result;
    }

    /**
     * 二分法从list中查询元素下标
     *
     * @return
     */
    private int searchFromList(List<Long> list, long item) {
        int low = 0;
        int high = list.size() - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (item < list.get(mid)) {
                high = mid - 1;
            } else if (item == list.get(mid))
                return mid;
            else
                low = mid + 1;
        }
        return -low;
    }
}
