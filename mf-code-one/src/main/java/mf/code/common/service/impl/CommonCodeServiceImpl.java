package mf.code.common.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.CommonCodeStatusEnum;
import mf.code.common.CommonCodeTypeEnum;
import mf.code.common.CommonCodeUserTypeEnum;
import mf.code.common.DelEnum;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.dao.CommonCodeMapper;
import mf.code.common.repo.dto.LiveCodeDTO;
import mf.code.common.repo.dto.RedirectDTO;
import mf.code.common.repo.dto.WeChatCodeDTO;
import mf.code.common.repo.po.CommonCode;
import mf.code.common.service.CommonCodeService;
import mf.code.common.utils.BufferedImageUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.FileUtil;
import mf.code.common.utils.QRCodeUtil;
import mf.code.shop.constants.UploadPath;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.common.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-06-21 14:04
 */
@Slf4j
@Service
public class CommonCodeServiceImpl implements CommonCodeService {

    @Autowired
    private CommonCodeMapper commonCodeMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private OssCaller ossCaller;
    /**
     * 小程序端活码重定向接口
     */
    @Value(value = "${livecode.applet}")
    private String appletRedirectUrl;
    /**
     * oss图片域名
     */
    @Value(value = "${aliyunoss.bucketUrlConsole}")
    private String bucketUrlConsole;
    /**
     * cdn域名
     */
    @Value(value = "${aliyunoss.cdnUrl}")
    private String cdnUrl;
    /**
     * 临时文件目录
     */
    private final String CODE_TEMP_PATH = "/home/code/temp/";
    /**
     * 微信公众号扫码结果
     * http://weixin.qq.com/...
     * 微信个人号
     * https://u.wechat.com/...
     * 群二维码
     * https://weixin.qq.com/...
     */
    private final String WECHAT_PUBLIC = "http://weixin.qq.com";
    private final String WECHAT_PERSONAL = "https://u.wechat.com/";
    private final String WECHAT_GROUP = "https://weixin.qq.com/";

    /**
     * 从图片中获取微信二维码
     *
     * @param wechatPath
     * @param userId
     * @return
     */
    @Override
    public String getWxQRFramPicUrl(String wechatPath, Long userId) {
        String localFilePath = CODE_TEMP_PATH + userId + "personal.jpg";
        // 此处的图片地址不能包含域名
        wechatPath = wechatPath.replace(bucketUrlConsole, "").replace(cdnUrl, "").substring(1);
        ossCaller.downloadFile(wechatPath, localFilePath);
        File file = new File(localFilePath);
        if (!file.exists()) {
            return null;
        }
        Result result = null;
        try {
            result = QRCodeUtil.anlaysisQRCode(file, localFilePath);
        } catch (NotFoundException e) {
            log.error("oss临时下载文件不存在" + localFilePath, e);
            return null;
        } finally {
            FileUtils.deleteQuietly(file);
        }
        if (result == null) {
            return null;
        }
        String wechatUrl = result.getText();
        if (!StringUtils.startsWithIgnoreCase(wechatUrl, WECHAT_PUBLIC)
                && !StringUtils.startsWithIgnoreCase(wechatUrl, WECHAT_PERSONAL)
                && !StringUtils.startsWithIgnoreCase(wechatUrl, WECHAT_GROUP)) {
            return null;
        }

        BufferedImage qrCode = BufferedImageUtil.createQrCode(wechatUrl, 250, 250);

        String codePath = "";
        String codeNo = UUID.randomUUID().toString();
        InputStream is = null;
        long start = System.currentTimeMillis();
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(qrCode, "png", os);
            is = new ByteArrayInputStream(os.toByteArray());
            //上传aliyunoss
            String bucket = UploadPath.LIVE_CODE_APPLET_PATH + 1 + "/" + userId + "/" + codeNo + ".png";
            codePath = this.ossCaller.uploadObject2OSSInputstream(is, bucket);
            log.info("活码生成：{} 耗时 {} ms", codePath, System.currentTimeMillis() - start);
        } catch (Exception e) {
            log.error("Exception", e);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                log.error("Exception", e);
            }
        }
        return codePath;
    }

    /**
     * 创建修改活码(TODO 暂时不支持修改)
     * 1.加分布式锁
     * 2.根据入参图片解析出是否是二维码，并解析出二维码字符串
     * 3.查询是否有活码记录，如果没有则创建活码记录，有则更新子码的记录
     * 4.根据二维码字符串和用户生成唯一的codeNo，并作简单的md5（或直接采用uuid）
     * 5.生成活码图片并上传
     * 6.创建上传码的记录
     * 7.删除分布式锁
     *
     * @param liveCodeDTO
     * @return
     */
    @Override
    public String createOrUpdateLiveCode(LiveCodeDTO liveCodeDTO) {
        Long userId = liveCodeDTO.getUserId();
        Integer userType = liveCodeDTO.getUserType();
        if (userId == null || userType == null) {
            return null;
        }
        // 1.加分布式锁
        RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.LIVECODE_LOCK + liveCodeDTO.getUserId(), 10, TimeUnit.SECONDS);
        // 2.根据入参图片解析出是否是二维码，并解析出二维码字符串
        List<WeChatCodeDTO> weChatCodeDTOList = liveCodeDTO.getLiveCodeList();
        if (CollectionUtils.isEmpty(weChatCodeDTOList)) {
            return null;
        }
        int rightCodeNum = 0;
        for (WeChatCodeDTO weChatCodeDTO : weChatCodeDTOList) {
            String wechatPath = weChatCodeDTO.getWeChatPath();
            String localFilePath = CODE_TEMP_PATH + liveCodeDTO.getUserId() + ".jpg";
            // 此处的图片地址不能包含域名
            wechatPath = wechatPath.replace(bucketUrlConsole, "").replace(cdnUrl, "").substring(1);
            ossCaller.downloadFile(wechatPath, localFilePath);
            File file = new File(localFilePath);
            if (!file.exists()) {
                continue;
            }
            Result result = null;
            try {
                result = QRCodeUtil.anlaysisQRCode(file, localFilePath);
            } catch (NotFoundException e) {
                log.error("oss临时下载文件不存在" + localFilePath, e);
                continue;
            } finally {
                FileUtils.deleteQuietly(file);
            }
            if (result == null) {
                continue;
            }

            String wechatUrl = result.getText();
            if (!StringUtils.startsWithIgnoreCase(wechatUrl, WECHAT_PUBLIC)
                    && !StringUtils.startsWithIgnoreCase(wechatUrl, WECHAT_PERSONAL)
                    && !StringUtils.startsWithIgnoreCase(wechatUrl, WECHAT_GROUP)) {
                continue;
            }
            weChatCodeDTO.setWeChatUrl(wechatUrl);

            BufferedImage qrCode = BufferedImageUtil.createQrCode(wechatUrl, 250, 250);
            InputStream is = null;
            long start = System.currentTimeMillis();
            String wechatCodePath = "";
            try {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(qrCode, "png", os);
                is = new ByteArrayInputStream(os.toByteArray());
                //上传aliyunoss
                String bucket = UploadPath.LIVE_CODE_APPLET_PATH + userType + "/" + userId + "/" + DateUtil.getCurrentMillis() + ".png";
                wechatCodePath = this.ossCaller.uploadObject2OSSInputstream(is, bucket);
                log.info("活码生成：{} 耗时 {} ms", wechatCodePath, System.currentTimeMillis() - start);
            } catch (Exception e) {
                log.error("Exception", e);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    log.error("Exception", e);
                }
            }
            if (StringUtils.isNotBlank(wechatCodePath)) {
                weChatCodeDTO.setWeChatPath(wechatCodePath);
            }
            rightCodeNum++;
        }
        if (rightCodeNum == 0) {
            return "";
        }
        // 3.查询是否有活码记录，如果没有则创建活码记录，有则更新子码的记录
        Long codeId = liveCodeDTO.getCodeId();
        CommonCode commonCode = null;
        if (codeId != null) {
            commonCode = commonCodeMapper.selectByPrimaryKey(codeId);
        }
        Date now = new Date();
        String codeNo = UUID.randomUUID().toString();
        String codePath = "";
        String codeUrl = "";
        if (commonCode == null) {
            codeUrl = appletRedirectUrl + codeNo;
            BufferedImage qrCode = BufferedImageUtil.createQrCode(codeUrl, 250, 250);
            InputStream is = null;
            long start = System.currentTimeMillis();
            try {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(qrCode, "png", os);
                is = new ByteArrayInputStream(os.toByteArray());
                //上传aliyunoss
                String bucket = UploadPath.LIVE_CODE_APPLET_PATH + userType + "/" + userId + "/" + codeNo + ".png";
                codePath = this.ossCaller.uploadObject2OSSInputstream(is, bucket);
                log.info("活码生成：{} 耗时 {} ms", codePath, System.currentTimeMillis() - start);
            } catch (Exception e) {
                log.error("Exception", e);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    log.error("Exception", e);
                }
            }
            commonCode = new CommonCode();
            commonCode.setParentId(0L);
            commonCode.setCodeNo(codeNo);
            commonCode.setCodePath(codePath);
            commonCode.setCodeStatus(CommonCodeStatusEnum.USED.getCode());
            commonCode.setCodeType(CommonCodeTypeEnum.LIVE_CODE.getCode());
            commonCode.setCodeUrl(codeUrl);
            commonCode.setStartTime(now);
            if (!StringUtils.isEmpty(liveCodeDTO.getStartTime())) {
                commonCode.setStartTime(DateUtil.stringtoDate(liveCodeDTO.getStartTime(), DateUtil.LONG_DATE_FORMAT));
            }
            commonCode.setEndTime(DateUtil.addDay(now, 7));
            if (!StringUtils.isEmpty(liveCodeDTO.getEndTime())) {
                commonCode.setEndTime(DateUtil.stringtoDate(liveCodeDTO.getEndTime(), DateUtil.LONG_DATE_FORMAT));
            }
            commonCode.setDel(DelEnum.NO.getCode());
            commonCode.setScanLimit(liveCodeDTO.getScanLimit());
            commonCode.setScanNum(0);
            commonCode.setUserId(userId);
            commonCode.setUserType(userType);
            // 个性化信息（logo图，替换图等 logoPath中间图片，reservePath替代图片）
            Map<String, String> extraMap = new HashMap<>();
            if (!StringUtils.isEmpty(liveCodeDTO.getLogoPath())) {
                extraMap.put("logoPath", liveCodeDTO.getLogoPath());
                extraMap.put("reservePath", liveCodeDTO.getReservePath());
            }
            commonCode.setExtra(JSONObject.toJSONString(extraMap));
            commonCode.setCtime(now);
            commonCode.setUtime(now);
            int count = commonCodeMapper.insertSelective(commonCode);
            if (count == 0) {
                return null;
            }
        }
        for (WeChatCodeDTO weChatCodeDTO : weChatCodeDTOList) {
            CommonCode commonCodeSub = new CommonCode();
            commonCodeSub.setParentId(commonCode.getId());
            commonCodeSub.setCodeNo(codeNo);
            commonCodeSub.setCodeStatus(CommonCodeStatusEnum.USED.getCode());
            commonCodeSub.setCodeType(CommonCodeTypeEnum.GROUP_CODE.getCode());
            commonCodeSub.setCodePath(weChatCodeDTO.getWeChatPath());
            commonCodeSub.setCodeUrl(weChatCodeDTO.getWeChatUrl());
            commonCodeSub.setStartTime(now);
            if (!StringUtils.isEmpty(liveCodeDTO.getStartTime())) {
                commonCodeSub.setStartTime(DateUtil.stringtoDate(liveCodeDTO.getStartTime(), DateUtil.LONG_DATE_FORMAT));
            }
            commonCodeSub.setEndTime(DateUtil.addDay(now, 7));
            if (!StringUtils.isEmpty(liveCodeDTO.getEndTime())) {
                commonCodeSub.setEndTime(DateUtil.stringtoDate(liveCodeDTO.getEndTime(), DateUtil.LONG_DATE_FORMAT));
            }
            commonCodeSub.setScanLimit(weChatCodeDTO.getScanNum());
            commonCodeSub.setScanNum(0);
            commonCodeSub.setUserId(userId);
            commonCodeSub.setUserType(userType);
            commonCodeSub.setExtra("{}");
            commonCodeSub.setDel(DelEnum.NO.getCode());
            commonCodeSub.setCtime(now);
            commonCodeSub.setUtime(now);
            commonCodeMapper.insertSelective(commonCodeSub);
        }
        stringRedisTemplate.delete(RedisKeyConstant.LIVECODE_LOCK + liveCodeDTO.getUserId());
        return codePath;
    }

    /**
     * 更新二维码图片
     *
     * @param liveCodeDTO
     * @return
     */
    @Override
    public WeChatCodeDTO uploadWeChatCode(LiveCodeDTO liveCodeDTO) {
        return null;
    }

    /**
     * 根据codeNo查询活码地址
     * 1.根据codeNO查询活码记录
     * 2.判断活码记录是否可用，可以继续，否则直接返回
     * 3.查询该活码下的所有可用群码
     * 4.遍历群码，设置跳转地址
     * 5.修改返回真实群码的可扫描次数
     *
     * @param code
     * @return
     */
    @Override
    public RedirectDTO queryLiveCodeByCodeNo(String code, Boolean needCalc) {
        QueryWrapper<CommonCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CommonCode::getCodeNo, code)
                .eq(CommonCode::getCodeType, CommonCodeTypeEnum.LIVE_CODE.getCode());
        CommonCode commonCode = commonCodeMapper.selectOne(queryWrapper);
        RedirectDTO redirectDTO = new RedirectDTO();
        redirectDTO.setCanRedirect(false);
        if (commonCode == null) {
            return redirectDTO;
        }
        redirectDTO.setCanRedirect(true);
        // 如果停止使用则返回用户默认的图片
        if (commonCode.getCodeStatus() != CommonCodeStatusEnum.USED.getCode()) {
            redirectDTO.setCanRedirect(false);
            JSONObject jsonObject = JSON.parseObject(commonCode.getExtra());
            if (jsonObject != null && jsonObject.containsKey("reservePath")) {
                redirectDTO.setCanRedirect(true);
                redirectDTO.setTargetUrl(jsonObject.get("reservePath").toString());
            }
            return redirectDTO;
        }

        QueryWrapper<CommonCode> subWrapper = new QueryWrapper<>();
        subWrapper.lambda()
                .eq(CommonCode::getParentId, commonCode.getId())
                .eq(CommonCode::getCodeStatus, CommonCodeStatusEnum.USED.getCode());
        List<CommonCode> commonCodes = commonCodeMapper.selectList(subWrapper);
        if (CollectionUtils.isEmpty(commonCodes)) {
            return redirectDTO;
        }
        for (CommonCode commonCode1 : commonCodes) {
            if (commonCode1.getScanNum() > commonCode1.getScanLimit()) {
                continue;
            }
            if (commonCode1.getEndTime().before(new Date())) {
                commonCode1.setCodeStatus(CommonCodeStatusEnum.EXPIRE.getCode());
                commonCodeMapper.updateByPrimaryKeySelective(commonCode1);
                continue;
            }
            redirectDTO.setCanRedirect(true);
            redirectDTO.setTargetUrl(commonCode1.getCodeUrl());
            if (needCalc) {
                // 此为小功能，暂不考虑并发情况
                int scanNum = commonCode1.getScanNum() + 1;
                if (scanNum >= commonCode1.getScanLimit()) {
                    commonCode1.setCodeStatus(CommonCodeStatusEnum.STOP.getCode());
                }
                commonCode1.setScanNum(commonCode1.getScanNum() + 1);
                commonCodeMapper.updateByPrimaryKeySelective(commonCode1);
                // 更新活码扫描次数
                commonCode.setScanNum(commonCode.getScanNum() + 1);
                if (commonCode.getScanNum() > commonCode.getScanLimit()) {
                    commonCode.setCodeStatus(CommonCodeStatusEnum.STOP.getCode());
                }
                commonCodeMapper.updateByPrimaryKeySelective(commonCode);
            }
            break;
        }
        return redirectDTO;
    }

    /**
     * 根据用户id查询活码路径
     * TODO 因为微信二维码无法完成跳转，所以暂时使用原始二维码
     *
     * @param userId
     * @return
     */
    @Override
    public String queryLiveCodeByUserId(Long userId) {
        QueryWrapper<CommonCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(CommonCode::getUserId, userId)
                .eq(CommonCode::getUserType, CommonCodeUserTypeEnum.USER.getCode())
                .eq(CommonCode::getCodeType, CommonCodeTypeEnum.GROUP_CODE.getCode())
                .orderByDesc(CommonCode::getId);
        Page<CommonCode> page = new Page<>(1, 1);
        IPage<CommonCode> commonCodeIPage = commonCodeMapper.selectPage(page, queryWrapper);
        if (commonCodeIPage == null || CollectionUtils.isEmpty(commonCodeIPage.getRecords())) {
            return "";
        }
        CommonCode commonCode = commonCodeIPage.getRecords().get(0);
        return commonCode.getCodePath();
    }

}
