package mf.code.common.service;

import mf.code.common.repo.dto.LiveCodeDTO;
import mf.code.common.repo.dto.RedirectDTO;
import mf.code.common.repo.dto.WeChatCodeDTO;

/**
 * mf.code.common.service
 * Description:
 *
 * @author gel
 * @date 2019-06-21 14:04
 */
public interface CommonCodeService {

	/**
	 * 从图片中获取微信二维码
	 * @param wechatPath
	 * @param userId
	 * @return
	 */
	String getWxQRFramPicUrl(String wechatPath, Long userId);

	/**
     * 创建修改活码
     *
     * @param liveCodeDTO
     * @return
     */
    String createOrUpdateLiveCode(LiveCodeDTO liveCodeDTO);

    /**
     * 上传二维码图片
     *
     * @param liveCodeDTO
     * @return
     */
    WeChatCodeDTO uploadWeChatCode(LiveCodeDTO liveCodeDTO);

    /**
     * 根据codeNo查询活码地址
     *
     * @param code
     * @return
     */
    RedirectDTO queryLiveCodeByCodeNo(String code, Boolean needCalc);

    /**
     * 根据用户id查询活码路径
     *
     * @param userId
     * @return
     */
    String queryLiveCodeByUserId(Long userId);
}
