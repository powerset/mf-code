package mf.code.common.service;

import mf.code.one.dto.ApolloLHYXDTO;

public interface CommonConfService {

    /**
     * 获取联合营销配置信息
     * @return
     */
    ApolloLHYXDTO getUnionConf();
}
