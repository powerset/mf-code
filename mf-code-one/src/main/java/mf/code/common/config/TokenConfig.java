package mf.code.common.config;

import mf.code.common.aop.AppletTokenInterceptor;
import mf.code.common.aop.PlatformTokenInterceptor;
import mf.code.common.aop.SellerTokenInterceptor;
import mf.code.common.aop.TeacherTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 17:28
 */
@Configuration
public class TokenConfig extends WebMvcConfigurationSupport {

    /**
     * 添加拦截器之前先自己创建一下这个Spring Bean
     * 将拦截器注册为bean也交给spring管理,这样拦截器内部注入其他的bean对象也就可以被spring识别了
     */
    @Bean
    SellerTokenInterceptor sellerTokenInterceptor() {
        return new SellerTokenInterceptor();
    }

    @Bean
    AppletTokenInterceptor appletTokenInterceptor() {
        return new AppletTokenInterceptor();
    }

    @Bean
    TeacherTokenInterceptor teacherTokenInterceptor() {
        return new TeacherTokenInterceptor();
    }

    @Bean
    PlatformTokenInterceptor platformTokenInterceptor() {
        return new PlatformTokenInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注意，这里添加和忽略的path，都是controller中配置的path开始，但是不包含server.servlet.context-path配置的path
        registry.addInterceptor(sellerTokenInterceptor())
                .addPathPatterns(
                        "/api/seller/**",
                        "/api/ad/seller/**")
                .excludePathPatterns(
                        "/api/seller/merchant/**",
                        "/api/seller/merchantOrder/wx/pay/callback",
                        "/api/seller/v3/recall/showPoster",
                        "/api/seller/merchantOrder/wx/refund/callback",
                        "/api/seller/merchantOrder/donateJkmf",
                        "/api/seller/code");
        registry.addInterceptor(appletTokenInterceptor()).addPathPatterns("/api/applet/**")
                .excludePathPatterns(
                        "/api/applet/login",
                        "/api/applet/v3/login",
                        "/api/applet/userOrder/wx/pay/callback",
                        "/api/applet/userOrder/v11/wx/pay/callback",
                        "/api/applet/userOrder/wx/refund/callback",
                        "/api/applet/appletCommitAudit",
                        "/api/applet/weixin/**",
                        "/api/applet/code",
                        "/api/web/applet/homepage/queryHomePage",
                        "/api/goods/applet/v5/getProductsByGroupId",
                        "/api/goods/applet/v1/goods/plate/distribution/getSpecesAndBanners",
                        "/api/goods/applet/v1/goods/plate/distribution/list",
                        "/api/applet/v3/checkpointTaskSpace/getBarrage",
                        "/api/goods/applet/v5/getGoodsDetail",
                        "/api/comment/applet/queryProductCommentPage"

        );
        registry.addInterceptor(platformTokenInterceptor())
                .addPathPatterns(
                        "/api/platform/**",
                        "/api/ad/platform/**")
                .excludePathPatterns(
                        "/api/platform/login");
        registry.addInterceptor(teacherTokenInterceptor())
                .addPathPatterns(
                        "/api/teacher/**")
                .excludePathPatterns(
                        "/api/teacher/login",
                        "/api/teacher/home/registerMerchant",
                        "/api/teacher/home/getSmsCode",
                        "/api/teacher/weixin/**");
        super.addInterceptors(registry);
    }

    /**
     * 静态资源
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }
}
