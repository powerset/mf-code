package mf.code.common.verify.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.dto.SmsCodeRedis;
import mf.code.api.teacher.dto.ApplyTeacherReq;
import mf.code.common.caller.aliyundayu.DayuCaller;
import mf.code.common.email.EmailService;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.common.verify.SmsVerifyCodeService;
import mf.code.teacher.constant.TeacherRoleEnum;
import mf.code.teacher.constant.TeacherStatusEnum;
import mf.code.teacher.repo.po.Teacher;
import mf.code.teacher.service.TeacherService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * mf.code.common.verify.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-04 下午2:17
 */
@Slf4j
@Service
public class SmsVerifyCodeServiceImpl implements SmsVerifyCodeService {
	private final TeacherService teacherService;
	private final StringRedisTemplate stringRedisTemplate;
	private final DayuCaller dayuCaller;
	private final EmailService emailService;

	@Value("${teacher.smsCode.debug.mode}")
	private Integer smsCodeDebugMode;

	@Autowired
	public SmsVerifyCodeServiceImpl(StringRedisTemplate stringRedisTemplate,
	                                TeacherService teacherService,
	                                DayuCaller dayuCaller, EmailService emailService) {
		this.stringRedisTemplate = stringRedisTemplate;
		this.teacherService = teacherService;
		this.dayuCaller = dayuCaller;
		this.emailService = emailService;
	}

	@Override
	public Boolean checkSmsVerifyCodeIsValid(ApplyTeacherReq applyTeacherReq){
		// redisKey
		String redisKey = RedisKeyConstant.TEACHER_REGISTER + applyTeacherReq.getPhone();
		// 判断 手机号码 是否有效
		String redisString = stringRedisTemplate.opsForValue().get(redisKey);
		if (StringUtils.isBlank(redisString)) {
			return false;
		}
		SmsCodeRedis smsRedisRegister = JSONObject.parseObject(redisString, SmsCodeRedis.class);
		boolean equals = StringUtils.equals(smsRedisRegister.getSmsCode(), applyTeacherReq.getSmsVerifyCode());
		if(equals){
			this.stringRedisTemplate.delete(redisKey);
		}
		return equals;
	}

	@Override
	public SimpleResponse<Object> smsVerifyCodeForApplyTeacher(ApplyTeacherReq applyTeacherReq) {
		/*
		SimpleResponse<Object> simpleResponse = new SimpleResponse<>();
		// 校验 图形验证码，用户会话中的图形验证码 存在&匹配
		if (null == pictureCode) {
			simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
			simpleResponse.setMessage("没有图形验证码");
			return simpleResponse;
		}
		if (!StringUtils.equalsIgnoreCase(pictureCode, smsReq.getPicCode())) {
			simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
			simpleResponse.setMessage("图形验证码不匹配");
			return simpleResponse;
		}
		*/
		// redisKey
		String redisKey = RedisKeyConstant.TEACHER_REGISTER + applyTeacherReq.getPhone();
		// 判断 手机号码 是否60秒之内重复发送
		String redisString = stringRedisTemplate.opsForValue().get(redisKey);
		SmsCodeRedis smsRedisRegister = JSONObject.parseObject(redisString, SmsCodeRedis.class);
		if (smsRedisRegister != null && DateUtil.getCurrentMillis() < Long.valueOf(smsRedisRegister.getNow()) + DateUtil.ONE_MINUTE_MILLIS) {
			return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "稍后再次请求短信验证码");
		}
		// 判断 手机号码 是否已经注册
		QueryWrapper<Teacher> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Teacher::getPhone, applyTeacherReq.getPhone());
		Teacher teacher = teacherService.getOne(wrapper);
		if (teacher != null && (TeacherStatusEnum.ING.getCode().equals(teacher.getStatus()) || TeacherRoleEnum.TEACHER.getCode().equals(teacher.getRole()))) {
			return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "此手机号码已经被申请，换一个试试吧");
		}
		// 测试模式：不校验短信验证码
		if (smsCodeDebugMode != null && smsCodeDebugMode == 1) {
			SmsCodeRedis register = new SmsCodeRedis();
			register.setNow(DateUtil.getCurrentMillis().toString());
			register.setSmsCode("111111");
			stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 5, TimeUnit.MINUTES);
			return new SimpleResponse<>(ApiStatusEnum.SUCCESS);
		}
		// 生成短信验证码，6位
		String smsCode = RandomStrUtil.getRandomNum();
		// 短信验证码存入redis，过期时间2分钟key-->mch:reg:<phone> value-->smsCode, now
		SmsCodeRedis register = new SmsCodeRedis();
		register.setNow(DateUtil.getCurrentMillis().toString());
		register.setSmsCode(smsCode);
		stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 5, TimeUnit.MINUTES);
		// 调用大于短信接口(手机号，短信验证码)
		SendSmsResponse sendSmsResponse = dayuCaller.smsForRegister(applyTeacherReq.getPhone(), smsCode);
		// 处理大于短信返回状态
		if (!StringUtils.equalsIgnoreCase("ok", sendSmsResponse.getCode())) {
			log.error("请求阿里大于失败, phone" + applyTeacherReq.getPhone());
			// 短信验证码调用失败处理
			emailService.sendSimpleMail("请求阿里大于失败, phone" + applyTeacherReq.getPhone(), JSON.toJSONString(sendSmsResponse));
			return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "短信服务请求失败");
		}
		return new SimpleResponse<>(ApiStatusEnum.SUCCESS);
	}

}
