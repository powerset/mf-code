package mf.code.common.job;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletLotteryActivity;
import mf.code.activity.domain.applet.application.impl.AppletLotteryActivityServiceAbstractImpl;
import mf.code.activity.domain.applet.valueobject.UserBoard;
import mf.code.activity.domain.applet.valueobject.UserBoardConfig;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.constant.ActivityApiEntrySceneEnum;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 此定时器 适用于 按活动发起时间发起活动的任务
 * <p>
 * mf.code.common.job
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 08:45
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class TimeCreateActivityScheduledService extends AppletLotteryActivityServiceAbstractImpl {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    // 免单抽奖活动，发起者获得10个抽奖码
    @Value("${activity.freeOrderGetAwardCodeNumber}")
    private Integer freeOrderGetAwardCodeNumber;

    /**
     * 检测 活动定义的活动发起时间，到达活动开始时间 则发起活动。
     */
    @Scheduled(fixedRate = 60000)
    public void timeCreateActivity() {
        // 一.是否发起校验
        // 1.获取活动定义 -- 优化条件：增加活动定义结束时间过滤
        QueryWrapper<ActivityDef> activityDefWrapper = new QueryWrapper<>();
        activityDefWrapper.lambda()
                .eq(ActivityDef::getType, ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                .gt(ActivityDef::getStock, 0)
                .lt(ActivityDef::getStartTime, new Date())
                .gt(ActivityDef::getEndTime, new Date());
        List<ActivityDef> activityDefList = activityDefService.list(activityDefWrapper);
        if (CollectionUtils.isEmpty(activityDefList)) {
            return;
        }

        // 2.获取发起活动的机器人
        Map<String, Object> userParams = new HashMap<>();
        userParams.put("merchantId", 0);
        userParams.put("shopId", 0);
        userParams.put("grantStatus", -1);
        List<User> users = this.userService.listPageByParams(userParams);
        if (CollectionUtils.isEmpty(users)) {
            log.warn("机器人失踪了~~~");
            return;
        }

        for (ActivityDef activityDef : activityDefList) {
            // 3.是否已发起活动
            QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
            activityWrapper.lambda()
                    .eq(Activity::getActivityDefId, activityDef.getId());
            int count = activityService.count(activityWrapper);
            if (count > 0) {
                continue;
            }
            // 4.发起活动
            Map<String, Object> params = new HashMap<>();
            params.put("activityDefId", activityDef.getId());
            params.put("userId", users.get(0).getId());
            params.put("type", ActivityApiEntrySceneEnum.CREATE.getCode());
            this.create(params);
        }
    }

    /**
     * 检测 活动定义的活动发起时间，到达活动开始时间 则发起活动。
     */
    @Scheduled(fixedRate = 60000)
    public void timeLotteryActivity() {
        log.info("定时开奖开始");
        // 原子开奖
        // 结算防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.PLATFORM_ORDER_BACK_LOTTERY, 1, TimeUnit.DAYS);
        if (!success) {
            log.error("上一开奖job还未处理结束");
            return;
        }
        // 定时开奖
        try {
            // 1.获取待开奖的活动
            QueryWrapper<Activity> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(Activity::getType, ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode())
                    .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode());
            List<Activity> activityList = activityService.list(wrapper);
            // 2.校验是否达到开奖标准
            for (Activity activity : activityList) {
                AppletLotteryActivity appletLotteryActivity = appletActivityRepository.assemblyAppletLotteryActivity(activity);
                if (appletLotteryActivity.isLottery(null)) {
                    // 3.开奖
                    // 补全抽奖用户信息
                    appletActivityRepository.addUserWinningProbability(appletLotteryActivity);
                    // 抽奖
                    List<Long> winnerIds = appletLotteryActivity.drawLottery();
                    // 补全 用户参与事件 -- 只针对中奖者
                    appletActivityRepository.findUserJoinEventByAppletActivity(appletLotteryActivity, winnerIds);
                    // 发奖 并发起相应的中奖任务
                    appletLotteryActivity.publishWinnerTask();

                    // 持久化活动状态
                    boolean save = appletActivityRepository.saveLottery(appletLotteryActivity);
                    if (!save) {
                        log.error("开奖存储异常 activity = {}", activity.getId());
                        continue;
                    }
                    //开奖推送消息
                    sendTemplateMessageService.sendTemplateMsg2PlatformOrderBack(activity, appletLotteryActivity.queryTotalJoinUserIdList());
                }
            }
        } finally {
            stringRedisTemplate.delete(RedisKeyConstant.PLATFORM_ORDER_BACK_LOTTERY);
        }
    }


    @Override
    protected AppletActivity assembleLotteryActivity(AppletActivity appletActivity, Map<String, Object> params) {
        User user = appletActivity.getUserInfo();

        AppletLotteryActivity appletLotteryActivity = new AppletLotteryActivity();
        appletLotteryActivity.setId(appletActivity.getId());
        appletLotteryActivity.setActivityDefId(appletActivity.getActivityDefId());
        appletLotteryActivity.setActivityInfo(appletActivity.getActivityInfo());
        appletLotteryActivity.setUserInfo(user);
        appletLotteryActivity.setJoinRule(appletActivity.getJoinRule());
        appletLotteryActivity.setCreateRule(appletActivity.getCreateRule());
        appletLotteryActivity.setTrigger(0L);

        UserBoard userBoard = new UserBoard();
        userBoard.setActivityDefId(appletLotteryActivity.getActivityDefId());
        appletLotteryActivity.setUserBoardConfig(new UserBoardConfig());

        Map<String, Object> startUserInfo = new HashMap<>();
        startUserInfo.put("uid", user.getId());
        startUserInfo.put("nickName", user.getNickName());
        startUserInfo.put("avatarUrl", user.getAvatarUrl());
        long currentTime = System.currentTimeMillis();
        long awardNo = currentTime % 100000000;
        List<Long> awardNoList = new ArrayList<>();
        for (int i = 0; i < freeOrderGetAwardCodeNumber; i++) {
            awardNo = awardNo + i;
            awardNoList.add(awardNo);
        }
        startUserInfo.put("awardNo", awardNoList);
        startUserInfo.put("time", currentTime);
        appletLotteryActivity.setStartUserInfo(startUserInfo);
        appletLotteryActivity.setVisitUserId(user.getId());

        return appletLotteryActivity;
    }

    @Override
    protected SimpleResponse activityPageVO(AppletActivity appletActivity, Map<String, Object> params) {
        int type = Integer.parseInt(params.get("type").toString());
        if (type == ActivityApiEntrySceneEnum.CREATE.getCode()) {
            String goodsId = stringRedisTemplate.opsForValue().get(RedisKeyConstant.CREATE_ACTIVITY_DEF_FORBID_REPEAT + appletActivity.getActivityInfo().getGoodsId() + ":" + ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode());

            Map<String, Object> activityVO = new HashMap<>();
            activityVO.put("activityId", appletActivity.getId());
            activityVO.put("skuId", appletActivity.getActivityInfo().getGoodsId());

            stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_GOODS_JOIN + goodsId, JSON.toJSONString(activityVO));
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_GOODS_JOIN + goodsId, appletActivity.queryLeftTime(), TimeUnit.MINUTES);

            Map<String, Object> resultDate = new HashMap<>();
            resultDate.put("joinStatus", 1);
            resultDate.put("awardNO", appletActivity.getStartUserInfo().get("awardNo"));
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultDate);
        }

        return new SimpleResponse();
    }
}
