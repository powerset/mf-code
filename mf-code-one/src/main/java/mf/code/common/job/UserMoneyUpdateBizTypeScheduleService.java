package mf.code.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.common.job
 *
 * @description: 细化掉用户订单的业务类型。结束后可移除掉
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 10:27
 */
@Slf4j
//@Component
//@Profile(value = {"test3", "prod"})
public class UserMoneyUpdateBizTypeScheduleService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;
    private final int MAX_LIMIT = 30;

    @Scheduled(fixedRate = 15000)
    public void userMoneyUpdateBizType() {
        log.info("<<<<<<<<<<机器人开始扫描定义活动了");
        Map<String, Object> userParams = new HashMap<>();
        userParams.put("merchantId", 0);
        userParams.put("shopId", 0);
        userParams.put("grantStatus", -1);
        List<User> users = this.userService.listPageByParams(userParams);
        if (CollectionUtils.isEmpty(users)) {
            log.warn("机器人失踪了~~~");
            return;
        }
        User user = users.get(0);
        int pageNum = 1;
        for (; ; pageNum++) {
            Page<UpayWxOrder> page = new Page<>(pageNum, MAX_LIMIT);
            QueryWrapper<UpayWxOrder> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(UpayWxOrder::getBizType, BizTypeEnum.ACTIVITY.getCode())
                    .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                    .notIn(UpayWxOrder::getUserId, Arrays.asList(user.getId()))
            ;
            IPage<UpayWxOrder> upayWxOrderIPage = this.upayWxOrderService.page(page, wrapper);
            if (CollectionUtils.isEmpty(upayWxOrderIPage.getRecords())) {
                return;
            }
            List<UpayWxOrder> upayWxOrderList = upayWxOrderIPage.getRecords();
            List<Long> bizValues = new ArrayList<>();
            for (UpayWxOrder upayWxOrder : upayWxOrderList) {
                if (upayWxOrder.getBizValue() != null && upayWxOrder.getBizValue() > 0 && bizValues.indexOf(upayWxOrder.getBizValue()) == -1) {
                    bizValues.add(upayWxOrder.getBizValue());
                }
            }
            if(CollectionUtils.isEmpty(bizValues)){
                return;
            }
            QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
            activityQueryWrapper.lambda()
                    .in(Activity::getId, bizValues);
            List<Activity> activities = this.activityService.list(activityQueryWrapper);
            if(CollectionUtils.isEmpty(activities)){
                return;
            }
            Map<Long, Activity> map = new HashMap<>();
            for(Activity activity : activities){
                map.put(activity.getId(), activity);
            }
            for (UpayWxOrder upayWxOrder : upayWxOrderList) {
                if (upayWxOrder.getBizValue() == null || upayWxOrder.getBizValue() <= 0) {
                    continue;
                }
                Activity activity = map.get(upayWxOrder.getBizValue());
                if (activity == null || activity.getType() == null || activity.getType() <= 0 || activity.getType() == ActivityTypeEnum.MCH_PLAN.getCode()) {
                    continue;
                }
                /**创建c端用户订单**/
                BizTypeEnum bizTypeEnum = null;
                if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
                    bizTypeEnum = BizTypeEnum.NEWMAN_START;
                } else if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
                    bizTypeEnum = BizTypeEnum.ASSIST_ACTIVITY;
                } else if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
                    bizTypeEnum = BizTypeEnum.ORDER_BACK;
                }
                if (bizTypeEnum != null) {
                    upayWxOrder.setBizType(bizTypeEnum.getCode());
                    this.upayWxOrderService.updateById(upayWxOrder);
                }
            }
        }
    }
}
