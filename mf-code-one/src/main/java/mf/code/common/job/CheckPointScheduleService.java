//package mf.code.common.job;
//
//import com.alibaba.fastjson.JSONObject;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import lombok.extern.slf4j.Slf4j;
//import mf.code.common.repo.po.CommonDict;
//import mf.code.common.service.CommonDictService;
//import mf.code.common.utils.DateUtil;
//import mf.code.common.utils.IpUtil;
//import mf.code.merchant.repo.po.MerchantShop;
//import mf.code.merchant.service.MerchantShopService;
//import mf.code.uactivity.repo.po.UserPubJoin;
//import mf.code.uactivity.service.UserPubJoinService;
//import mf.code.upay.repo.enums.BizTypeEnum;
//import mf.code.upay.repo.enums.OrderPayTypeEnum;
//import mf.code.upay.repo.enums.OrderStatusEnum;
//import mf.code.upay.repo.enums.OrderTypeEnum;
//import mf.code.upay.repo.po.UpayWxOrder;
//import mf.code.upay.service.UpayBalanceService;
//import mf.code.upay.service.UpayWxOrderService;
//import mf.code.user.constant.UserPubJoinTypeEnum;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Profile;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//import org.springframework.util.CollectionUtils;
//
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * mf.code.common.job
// *
// * @description: 增加好商户的redis。结束后可移除掉
// * @auther: yechen
// * @email: wangqingfeng@wxyundian.com
// * @Date: 2019年03月12日 14:19
// */
//@Slf4j
//@Component
//@Profile(value = {"dev", "test2", "prod"})
//public class CheckPointScheduleService {
//    @Autowired
//    private UserPubJoinService userPubJoinService;
//    @Autowired
//    private CommonDictService commonDictService;
//    @Autowired
//    private UpayWxOrderService upayWxOrderService;
//    @Autowired
//    private MerchantShopService merchantShopService;
//    @Autowired
//    private UpayBalanceService upayBalanceService;
//
//    private final int MAX_LIMIT = 30;
//
//    @Value("${checkpoint.oldDataTime}")
//    private String oldDataTime;
//
//    @Scheduled(fixedRate = 60000)
//    public void merchantDialogSchedule() {
//        int pageNum = 1;
//        Date date = DateUtil.stringtoDate(oldDataTime, DateUtil.FORMAT_ONE);
//        if (date == null) {
//            log.info("<<<<<<<<<<<< 未配置日期 <<<<<<<<<<<");
//            return;
//        }
//        for (; ; pageNum++) {
//            QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
//            wrapper.lambda()
//                    .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
//                    .le(UserPubJoin::getCtime, date)
//            ;
//            Page<UserPubJoin> page = new Page<>(pageNum, MAX_LIMIT);
//            IPage<UserPubJoin> userPubJoinIPage = userPubJoinService.page(page, wrapper);
//            List<UserPubJoin> upperUserPubJoins = userPubJoinIPage.getRecords();
//            if (CollectionUtils.isEmpty(upperUserPubJoins)) {
//                break;
//            }
//
//            for (UserPubJoin userPubJoin1 : upperUserPubJoins) {
//                Long shopId = userPubJoin1.getShopId();
//                Long userId = userPubJoin1.getSubUid();
//                MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
//                if (merchantShop == null) {
//                    continue;
//                }
//                //查找该用户是否已经处理过旧数据
//                UpayWxOrder upayWxOrderOne = getUpayWxOrderOne(shopId, userId);
//                if (upayWxOrderOne != null) {
//                    continue;
//                }
//
//                //获取收益
//                List<UserPubJoin> userPubJoins = this.getUserPubJoin(shopId, userId, null);
//                UserPubJoin userPubJoin = null;
//                if (!CollectionUtils.isEmpty(userPubJoins)) {
//                    userPubJoin = userPubJoins.get(0);
//                }
//
//                //获取字典表-税率信息and关卡信息
//                List<CommonDict> commonDicts = this.commonDictService.selectListByType("checkpoint");
//                //任务金额+缴税金额
//                Map<String, Object> totalScottareParams = new HashMap<>();
//                totalScottareParams.put("shopId", shopId);
//                totalScottareParams.put("userId", userId);
//                totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
//                BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);
//
//                //获取已通关数目
//                List<UpayWxOrder> upayWxOrders = getUpayWxOrder(shopId, userId);
//                CommonDict commonDict = null;
//                for (CommonDict commonDict1 : commonDicts) {
//                    if ("level".equals(commonDict1.getKey())) {
//                        commonDict = commonDict1;
//                    }
//                }
//                Map<Long, Map<String, Object>> checkpointMap = new HashMap<>();
//                if (commonDict != null) {
//                    List<Object> commonDictObjects = JSONObject.parseArray(commonDict.getValue(), Object.class);
//                    for (Object obj : commonDictObjects) {
//                        Map<String, Object> jsonMap = JSONObject.parseObject(obj.toString(), Map.class);
//                        checkpointMap.put(Long.valueOf(jsonMap.get("name").toString()), jsonMap);
//                    }
//                }
//                BigDecimal passTaskProfit = BigDecimal.ZERO;
//                BigDecimal passScottareProfit = BigDecimal.ZERO;
//                if (!CollectionUtils.isEmpty(upayWxOrders)) {
//                    for (UpayWxOrder upayWxOrder : upayWxOrders) {
//                        Map<String, Object> checkpoint = checkpointMap.get(upayWxOrder.getBizValue());
//                        Map<String, Object> upayWxOrderJsonMap = JSONObject.parseObject(upayWxOrder.getRemark(), Map.class);
//                        if (checkpoint != null && upayWxOrderJsonMap != null) {
//                            passTaskProfit = passTaskProfit.add(new BigDecimal(upayWxOrderJsonMap.get("commission").toString()));
//                            passScottareProfit = passScottareProfit.add(new BigDecimal(upayWxOrderJsonMap.get("scottare").toString()));
//                        }
//                    }
//                }
//
//                BigDecimal taskProfit = BigDecimal.ZERO;
//                BigDecimal scottareProfit = BigDecimal.ZERO;
//                if (userPubJoin != null) {
//                    taskProfit = userPubJoin.getTotalCommission().subtract(passTaskProfit).setScale(2, BigDecimal.ROUND_DOWN);
//                }
//                scottareProfit = allMinerTotalScottare.subtract(passScottareProfit).setScale(2, BigDecimal.ROUND_DOWN);
//
//                BigDecimal sum = taskProfit.add(scottareProfit);
//                if (sum.compareTo(BigDecimal.ZERO) > 0) {
//                    //存储
//                    UpayWxOrder upayWxOrderUpper = upayWxOrderService.addPo(userId, OrderPayTypeEnum.CASH.getCode(),
//                            OrderTypeEnum.PALTFORMRECHARGE.getCode(), null, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getMessage(),
//                            merchantShop.getMerchantId(), shopId, OrderStatusEnum.ORDERED.getCode(), sum,
//                            IpUtil.getServerIp(), null, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getMessage(), new Date(), null, null,
//                            BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode(), 0L);
//                    upayWxOrderService.create(upayWxOrderUpper);
//                    // 更新用户余额
//                    upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), userId, sum);
//                }
//            }
//        }
//    }
//
//    /***
//     * 查询是否已经处理过旧数据
//     * @param shopId
//     * @param userId
//     * @return
//     */
//    private UpayWxOrder getUpayWxOrderOne(Long shopId, Long userId) {
//        QueryWrapper<UpayWxOrder> wrapper = new QueryWrapper<>();
//        wrapper.lambda()
//                .eq(UpayWxOrder::getShopId, shopId)
//                .eq(UpayWxOrder::getUserId, userId)
//                .eq(UpayWxOrder::getType, OrderTypeEnum.PALTFORMRECHARGE.getCode())
//                .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
//                .eq(UpayWxOrder::getBizType, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode())
//                .orderByDesc(UpayWxOrder::getId)
//        ;
//        return this.upayWxOrderService.getOne(wrapper);
//    }
//
//    /***
//     * 获取已通关的订单信息
//     * @param shopId
//     * @param userId
//     * @return
//     */
//    private List<UpayWxOrder> getUpayWxOrder(Long shopId, Long userId) {
//        QueryWrapper<UpayWxOrder> wrapper = new QueryWrapper<>();
//        wrapper.lambda()
//                .eq(UpayWxOrder::getShopId, shopId)
//                .eq(UpayWxOrder::getUserId, userId)
//                .eq(UpayWxOrder::getType, OrderTypeEnum.PALTFORMRECHARGE.getCode())
//                .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
//                .eq(UpayWxOrder::getBizType, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode())
//                .orderByDesc(UpayWxOrder::getId)
//        ;
//        return this.upayWxOrderService.list(wrapper);
//    }
//
//
//    /***
//     * 获取累计缴税
//     * @param shopId
//     * @param userId
//     * @return
//     */
//    private List<UserPubJoin> getUserPubJoin(Long shopId, Long userId, Long bossUserId) {
//        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
//        userPubJoinQueryWrapper.lambda()
//                .eq(UserPubJoin::getShopId, shopId)
//                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
//        ;
//        if (userId != null && userId > 0) {
//            userPubJoinQueryWrapper.and(params -> params.eq("sub_uid", userId));
//        }
//        if (bossUserId != null && bossUserId > 0) {
//            userPubJoinQueryWrapper.and(params -> params.eq("user_id", bossUserId));
//        }
//        return this.userPubJoinService.list(userPubJoinQueryWrapper);
//    }
//}
