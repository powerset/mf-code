package mf.code.common.job;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.service.ActivityPlanService;
import mf.code.activity.service.ActivityService;
import mf.code.common.utils.DateUtil;
import mf.code.uactivity.service.UserTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.common.job
 * Description: 商品图片转储定时器
 *
 * @author: gbf
 * @date: 2018-11-10 11:26
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class RedpackStockScheduledService {
    private static final int PERIOD = 1000 * 60 * 60 * 24;

    @Autowired
    private ActivityPlanService activityPlanService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityService activityService;

    /**
     * <pre>
     *      1.每天凌晨1点执行
     * </pre>
     */
    @Scheduled(cron = "0 0 1 * * ?") // 生产环境 !!!
//    @Scheduled(fixedRate = 3000)    //test环境
    public void redpackStockScheduledService() {
        /**
         *      1.activity_plan中end_time like 20181020 , 获得所有计划id
         *      2.通过计划id查询活动
         *      3.count(1) user_task where status=0:未提交 and type=7:红包任务-收藏加购 and ctime是昨天的 activity in (活动id数组)
         *      4.activity_plan.total_redpack_stock=count(1)
         */
        doScheduledService();
    }

    /**
     * 自动任务
     */
    private void doScheduledService() {
        //fixme 尚未测试
        String date = DateUtil.dateToString(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24)), "yyyy-MM-dd");
        date="2018-11-20";
        //获取计划list
        List<ActivityPlan> plans = activityPlanService.findByCtimePlus(date);
        for (ActivityPlan plan : plans) {
            //获取活动list
            List<Activity> activities = activityService.findByPlanId(plan.getId());
            //得到activity的id数组
            Long[] ids = new Long[activities.size()];
            for (int i = 0; i < activities.size(); i++) {
                ids[i] = activities.get(i).getId();
            }
            //count user_task中未完成的用户
            Map map = new HashMap();
            map.put("ids", ids);
            //统计需要退库存的个数 select count from user_task where stock_flag =0没退的 and type=7红包任务 and status=-1统一改的 and activity_id in (...)
            int countResult = userTaskService.countUserTaskByCtimePlus(map);
            if (countResult > 0) {
                //改状态   update user_task set stock_flag=1 where activity_id in (....)
                Integer updateStockFlag = userTaskService.updateStockFlag(map);
                // 计算增加后库存
                plan.setTotalRedpackStock(countResult + plan.getTotalRedpackStock());
                //改库存
                int i = activityPlanService.updateByPrimaryKeySelectivePlus(plan);
                log.info(i + "" + updateStockFlag);
            }
        }
    }
}
