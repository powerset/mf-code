package mf.code.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.api.feignclient.CommAppService;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.one.templatemessge.OneTemplateMessageData;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.dto.UserResp;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.common.job
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月19日 09:08
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class ActivityAwardSendTemplateMessageService {
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private CommAppService commAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private final int MAX_LIMIT = 30;

    /***
     * 活动任务倒计时推送定时器
     */
    @Scheduled(fixedRate = 180000)
    public void activityAbnormalEndUserTaskScheduled() {
        int pageNum = 0;
        for (; ; pageNum++) {
            Map<String, Object> activityParams = new HashMap<String, Object>();
            activityParams.put("status", UserTaskStatusEnum.INIT.getCode());
            activityParams.put("read", 0);
            activityParams.put("direct", "asc");
            activityParams.put("size", MAX_LIMIT);
            activityParams.put("offset", pageNum * MAX_LIMIT);
            List<UserTask> userTasks = this.userTaskService.query(activityParams);
            if (CollectionUtils.isEmpty(userTasks)) {
                return;
            }
            for (UserTask userTask : userTasks) {
                Activity activity = this.activityService.findById(userTask.getActivityId());
                if (activity == null || activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
                    continue;
                }
                long timeMillis = DateUtils.addHours(userTask.getCtime(), 24).getTime() - System.currentTimeMillis();
                if (userTask.getStartTime() == null && timeMillis > 0 && timeMillis <= 0.5 * 60 * 60 * 1000) {
                    log.info("中奖任务截止半小时的推送--userTaskId:{}", userTask.getId());
                    userTask.setRead(1);
                    this.userTaskService.update(userTask);
                    //倒计时进入半小时，开启推送
                    this.sendTemplateMessageService.sendTemplateMsg2UserTaskCountDown(userTask.getGoodsId(), userTask.getUserId(), userTask.getActivityId());
                }
            }
        }
    }

    /***
     * 成团的第二次倒计时定时器推送
     */
    @Scheduled(fixedRate = 180000)
    public void activityAbnormalEndScheduled() {
        int pageNum = 0;
        for (; ; pageNum++) {
            Map<String, Object> userTaskParams = new HashMap<String, Object>();
            userTaskParams.put("read", 0);
            userTaskParams.put("status", UserTaskStatusEnum.INIT.getCode());
            userTaskParams.put("type", UserTaskTypeEnum.ASSIST_START.getCode());
            userTaskParams.put("size", MAX_LIMIT);
            userTaskParams.put("offset", pageNum * MAX_LIMIT);
            List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
            if (CollectionUtils.isEmpty(userTasks)) {
                return;
            }
            for (UserTask userTask : userTasks) {
                User user = this.userService.selectByPrimaryKey(userTask.getUserId());
                if (userTask.getStartTime() == null) {
                    long timeMillis = DateUtils.addHours(userTask.getCtime(), 24).getTime() - System.currentTimeMillis();
                    if (timeMillis > 0 && timeMillis <= 2 * 60 * 60 * 1000) {
                        log.info("成团任务截止2小时的推送--userTaskId:{}", userTask.getId());
                        userTask.setRead(1);
                        this.userTaskService.update(userTask);
                        //倒计时进入2小时，开启推送
                        this.sendTemplateMessageService.sendTemplateMsg2ActivityCountDown(userTask, user);
                    }
                }
            }
        }
    }


    @Scheduled(fixedRate = 60000)
    public void fullReimbursementWillExpireScheduled() {
        int pageNum = 1;
        for (; ; pageNum++) {
            QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
            activityQueryWrapper.lambda()
                    .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
                    .eq(Activity::getType, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode())
                    .eq(Activity::getDel, DelEnum.NO.getCode())
                    .isNull(Activity::getBatchNum)
            ;
            Page<Activity> page = new Page<>(pageNum, MAX_LIMIT);
            IPage<Activity> activityIPage = activityService.page(page, activityQueryWrapper);
            List<Activity> activities = activityIPage.getRecords();
            if (CollectionUtils.isEmpty(activities)) {
                return;
            }
            List<Long> userIds = new ArrayList<>();
            for (Activity activity : activities) {
                if (userIds.indexOf(activity.getUserId()) == -1) {
                    userIds.add(activity.getUserId());
                }
            }
            if (CollectionUtils.isEmpty(userIds)) {
                continue;
            }
            Map<Long, User> userMap = userService.queryByUserIdsToMap(userIds);
            for (Activity activity : activities) {
                User user = userMap.get(activity.getUserId());
                if (user == null) {
                    continue;
                }
                long timeMillis = activity.getEndTime().getTime();
                if (timeMillis > 0 && timeMillis <= 1 * 60 * 60 * 1000) {
                    log.info("<<<<<<<<截止1小时的推送--activityId:{}", activity.getId());

                    String redisKey = RedisKeyConstant.ACTIVITY_JOINLIST + activity.getId();
                    BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
                    List joinAssistUserInfo = activityPersons.range(0, -1);

                    //剩余几个人可完成
                    int remain = activity.getCondPersionCount() - joinAssistUserInfo.size();

                    TemplateMsgDTO templateMsgDTO = new TemplateMsgDTO();
                    templateMsgDTO.setTemplateId(wxmpProperty.getOrderProessMsgTmpId());
                    UserResp userResp = new UserResp();
                    userResp.setOpenId(user.getOpenId());
                    userResp.setId(user.getId());
                    templateMsgDTO.setUser(userResp);
                    Map<String, Object> data = OneTemplateMessageData.fullReimbursementWillExpire(
                            activity.getMerchantId(), activity.getShopId(), remain, activity.getId());
                    templateMsgDTO.setData(data);
                    log.info("<<<<<<<<报销活动助力开始推送消息：req:{}", templateMsgDTO);
                    commAppService.sendJkmfTemplateMsg(templateMsgDTO);

                    activity.setBatchNum(0L);
                    activity.setUtime(new Date());
                    activityService.save(activity);
                }
            }
        }
    }
}
