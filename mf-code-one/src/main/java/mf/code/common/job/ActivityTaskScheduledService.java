package mf.code.common.job;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WeixinPayServiceImpl;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.common.job
 * Description: 定时器
 *
 * @author: 百川
 * @date: 2018-11-01 11:26
 */
@Slf4j
//@Async
@Component
@Profile(value = {"test2", "prod"})
public class ActivityTaskScheduledService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private WeixinPayServiceImpl weixinPayServiceImpl;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private AppletActivityRepository appletActivityRepository;

    private final int MAX_LIMIT = 30;

    /***
     * 检测未成团，修改任务状态，若是用户发起的，则需退款处理  若是助力的 直接退库存
     */
    @Scheduled(fixedRate = 60000)
    public void activityAbnormalEndScheduled() {
        log.info("=========未成团定时器发车了");
        int pageNum = 0;
        for (; ; pageNum++) {
            Map<String, Object> activityParams = new HashMap<String, Object>();
            activityParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
            activityParams.put("abnormalEnd", true);
            activityParams.put("order", "end_time");
            activityParams.put("direct", "asc");
            activityParams.put("size", MAX_LIMIT);
            activityParams.put("offset", pageNum * MAX_LIMIT);
            List<Activity> abnormalEndActivitys = this.activityService.findPage(activityParams);
            if (CollectionUtils.isEmpty(abnormalEndActivitys)) {
                return;
            }
            for (Activity activity : abnormalEndActivitys) {

                if (activity.getType() == ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
                    continue;
                }

                // 拆红包活动过期 退库存 -- 有自己的过期流程
                if (activity.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode() || activity.getType() == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode()) {
                    AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, activity.getUserId(), ScenesEnum.HOME_PAGE);
                    if (openRedPackageActivity.isExpired()) {
                        // 活动过期
                        // 补全 用户参与事件
                        if (openRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
                            appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
                            openRedPackageActivity.setExpired();
                            appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
                        }
                    }
                    //活动未成团推送消息
                    this.sendTemplateMessageService.sendTemplateMsg2Award(activity.getShopId(), activity.getId(), null);
                    continue;
                }

                log.info("=========该编号为{}的活动即将失效", activity.getId());
                activity.setStatus(-1);
                activity.setUtime(new Date());
                this.activityService.update(activity);

                //活动未成团推送消息
                this.sendTemplateMessageService.sendTemplateMsg2Award(activity.getShopId(), activity.getId(), null);

                //判断该活动是否是新手有礼活动，若是，则更新user_activity对应的status
                if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                        || activity.getType() == ActivityTypeEnum.ASSIST.getCode()
                        || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
                    this.userActivityService.updateStatusByUserIdAndActivityId(activity.getUserId(), activity.getId(), UserActivityStatusEnum.AWARD_EXPIRE.getCode());
                }

                if (activity.getType() != ActivityTypeEnum.ORDER_BACK_START.getCode()
                        && activity.getType() != ActivityTypeEnum.NEW_MAN_START.getCode()
                        && activity.getType() != ActivityTypeEnum.ASSIST.getCode()
                        && activity.getType() != ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()
                        && activity.getType() != ActivityTypeEnum.ORDER_BACK_START_V2.getCode()
                        && activity.getType() != ActivityTypeEnum.ASSIST_V2.getCode()
                        && activity.getType() != ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                    continue;
                }


                //助力活动退库存
                if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
                    ActivityDef activityDef = activityDefService.getById(activity.getActivityDefId());
                    if (ActivityDefStockTypeEnum.COMPLETE_REDUCE.getCode().equals(activityDef.getStockType())) {
                        // 1.4迭代 活动剩余库存详情 多少人正在努力追赶
                        stringRedisTemplate.opsForValue().increment(RedisKeyConstant.MF_ACTIVITY_PRUSUE + activity.getActivityDefId(), -1L);
                        continue;
                    }
                    log.info("=========该编号为{}的活动为助力|拆红包，退库存走一波", activity.getId());
                    //还原库存走一波
                    this.activityDefService.addDeposit(activity.getActivityDefId(), BigDecimal.ZERO, 1);
                    continue;
                }

                // 全额报销活动退库存、退保证金
                log.info("=========该编号为{}的活动为全额报销，退库存、退保证金走一波", activity.getId());
                if (activity.getType() == ActivityTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                    Map<String, Object> upayWxOrderParams = new HashMap<String, Object>();
                    upayWxOrderParams.put("type", OrderTypeEnum.USERPAY.getCode());
                    upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
                    upayWxOrderParams.put("bizValue", activity.getId());
                    upayWxOrderParams.put("bizType", activity.getType());

                    BigDecimal totalFee = BigDecimal.ZERO;
                    List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
                    for (UpayWxOrder upayWxOrder : upayWxOrders) {
                        totalFee = totalFee.add(upayWxOrder.getTotalFee());
                    }

                    // 计算保证金
                    BigDecimal back = activity.getDeposit().subtract(totalFee);

                    // 计算库存 = 要退的保证金/单价 向下取整
                    int backStok = (int)Math.floor(back.divide(activity.getPayPrice()).floatValue());
                    this.activityDefService.addDeposit(activity.getActivityDefId(), back, backStok);
                    continue;
                }

                //当该活动是用户发起的时候,为正常结束/未成团 需要退款流程 支付方式不区分
                Map<String, Object> upayWxOrderParams = new HashMap<String, Object>();
                upayWxOrderParams.put("type", OrderTypeEnum.USERPAY.getCode());
                upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
                upayWxOrderParams.put("bizValue", activity.getId());
                List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
                if (upayWxOrders != null && upayWxOrders.size() > 0) {
                    UpayWxOrder upayWxOrder = upayWxOrders.get(0);
                    //退款流程
                    log.info("<<<<<<<<<< start refund: {}", upayWxOrder);
                    SimpleResponse refund = this.upayWxOrderService.refund(upayWxOrder.getPayType(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getUserId(), upayWxOrder.getTotalFee(),
                            upayWxOrder.getId(), upayWxOrder.getBizType(), upayWxOrder.getBizValue(), upayWxOrder.getOrderNo());
                    if (refund.getCode() == 0) {
                        //还原库存走一波
                        if (activity.getActivityDefId() == null || activity.getActivityDefId() == 0) {
                            return;
                        }
                        if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
                            this.activityDefService.addStock(activity.getActivityDefId(), activity.getHitsPerDraw());
                        } else {
                            this.activityDefService.addStock(activity.getActivityDefId(), activity.getHitsPerDraw() + 1);
                        }
                    }
                }
            }
        }
    }

    /**
     * 检测 用户中奖任务 完成情况，修改任务状态
     */
    @Scheduled(fixedRate = 60000)
    public void activityWinnerScheduled() {
        // 获取 已开奖 但 未结束的 活动: 条件status==1已发布并且award_start_time is not null
        List<Activity> activities = activityService.listActivityByStatusAndAwardStartTimeINN(1);
        if (CollectionUtils.isEmpty(activities)) {
            return;
        }
        for (Activity activity : activities) {
            // 不处理拆红包的成团进行中任务 -- 因为成团进行中的活动需要提醒用户去下单提现，等用户支付完成后 会更新为结束状态
            if (activity.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()
                    || activity.getType() == ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode()
                    || activity.getType() == ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
                continue;
            }

            // 获取每个活动所需信息
            Long aid = activity.getId(); // 活动id
            Long activityDefId = activity.getActivityDefId();
            Date awardStartTime = activity.getAwardStartTime(); //开奖时间
            Integer missionNeedTime = activity.getMissionNeedTime();// 中奖任务-需要完成时间 单位 分钟
            Integer hitsPerDraw = activity.getHitsPerDraw();// 每个计划内活动中奖人数
            Long mid = activity.getMerchantId();
            Long shopId = activity.getShopId();
            Long goodsId = activity.getGoodsId();
            Integer type = activity.getType();
            Long activityLeader = activity.getUserId();

            // 通过 aid和status 获取 未完成中奖任务 的用户信息 中奖任务包括：计划类，免单商品抽奖，新人有礼，任务成长红包
            List<Integer> statusList = new ArrayList<>();
            statusList.add(UserTaskStatusEnum.INIT.getCode()); // 未提交
            statusList.add(UserTaskStatusEnum.AUDIT_WAIT.getCode()); // 审核中
            statusList.add(UserTaskStatusEnum.AUDIT_SUCCESS.getCode()); // 审核通过/未发奖
            statusList.add(UserTaskStatusEnum.AUDIT_ERROR.getCode()); // 审核失败
            List<UserTask> userTasks = userTaskService.listByAidAndStatus(aid, statusList);

            // 本任务所有用户都已完成中奖任务，更新活动状态为 2:活动结束
            if (CollectionUtils.isEmpty(userTasks)) {
                List<Integer> statusExpireList = new ArrayList<>();
                statusExpireList.add(UserTaskStatusEnum.AWARD_EXPIRE.getCode()); // 活动过期
                List<UserTask> userTaskList = userTaskService.listByAidAndStatus(aid, statusExpireList);

                List<Long> userIdList = new ArrayList<>();
                List<Long> activityIdList = new ArrayList<>();
                for (UserTask userTask : userTaskList) {
                    userIdList.add(userTask.getUserId());
                    activityIdList.add(userTask.getActivityId());
                }

                if (!CollectionUtils.isEmpty(userIdList) && !CollectionUtils.isEmpty(activities)) {
                    QueryWrapper<UserActivity> uaWrapper = new QueryWrapper<>();
                    uaWrapper.lambda()
                            .in(UserActivity::getUserId, userIdList)
                            .in(UserActivity::getActivityId, activityIdList);
                    List<UserActivity> userActivityList = userActivityService.list(uaWrapper);
                    for (UserActivity userActivity : userActivityList) {
                        if (userActivity.getStatus() != UserActivityStatusEnum.AWARD_EXPIRE.getCode()) {
                            // 更新用户活动记录的状态
                            userActivityService.updateWinnerStatusByUserIdAndActivityId(userActivity.getUserId(), userActivity.getActivityId(), UserActivityStatusEnum.AWARD_EXPIRE.getCode()); // -2中奖任务过期
                        }
                    }
                }


                activityService.updateStatus(aid, ActivityStatusEnum.END.getCode());
                activityService.checkActivityEnd2Zset(aid);
                continue;
            }

            Integer index = hitsPerDraw; // 直接领奖后的名单位置下标
            for (UserTask userTask : userTasks) {
                if(userTask.getType() == UserTaskTypeEnum.GOOD_COMMENT.getCode() || userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()){
                    continue;
                }
                if(userTask.getStatus() == UserTaskStatusEnum.AUDIT_WAIT.getCode()){
                    continue;
                }

                Long uid = userTask.getUserId(); // 用户id
                Date startTime = userTask.getStartTime();// 用户中奖任务的开始计时时间
                Date receiveTime = userTask.getReceiveTime(); // 任务提交时间
                Date auditingTime = userTask.getAuditingTime();// 任务审核时间
                Date ctime = userTask.getCtime(); // 用户中奖任务的创建时间
                // 通过用户中奖任务的创建时间和活动完成所需时间  现在系统时间，判断用户 中奖任务是否过期
                Long now = System.currentTimeMillis(); // 当前系统时间
                Long overTime;
                Long supplyTotalTime = 0L;
                Long supplyTime = 0L;
                if (null == startTime) {
                    // 说明用户还没开始做任务
                    // 与24小时比较 是否过期
                    overTime = DateUtils.addHours(ctime, 24).getTime(); // 截止时间
                } else {
                    overTime = startTime.getTime() + missionNeedTime * 60000L; // 截止时间
                    // 补足审核时间 当 提交时间 有， 审核时间没有，则开始补足时间，
                    // 先取，做时间比较，确定是否要存入redis
                    String totalTime = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME_TOTAL + uid + aid);
                    if (StringUtils.isNotBlank(totalTime)) {
                        supplyTotalTime = Long.valueOf(totalTime);
                    }
                    // 二次提交图片审核的处理
                    // 当审核时间有，且审核不通过 的情况
                    if (receiveTime != null && auditingTime != null && receiveTime.getTime() <= auditingTime.getTime()) {
                        supplyTime = auditingTime.getTime() - receiveTime.getTime();

                        String lastTime = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME + uid + aid);
                        // 时间比较
                        if (!StringUtils.equals(lastTime, supplyTime.toString())) {
                            // 把稳定的时间 放入redis
                            stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME + uid + aid, supplyTime.toString());
                            supplyTotalTime = supplyTotalTime + supplyTime;
                            stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME_TOTAL + uid + aid, supplyTotalTime.toString());
                        }
                    }
                    overTime = overTime + supplyTotalTime;
                    stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME + uid + aid, overTime+30000, TimeUnit.MILLISECONDS);
                    stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME_TOTAL + uid + aid, overTime+30000, TimeUnit.MILLISECONDS);
                }

                // 先判断是直接中奖用户，还是重抽中奖用户(活动开奖时间后延30秒，若开奖时间大，说明是直接中奖用户，否则为重抽中奖用户)
                if (ctime.getTime() < awardStartTime.getTime() + 30000L) {
                    if (overTime >= now) {
                        continue;
                    }
                    // 直接中奖用户 任务过期 处理


                    // 更新用户中奖任务记录的状态
                    userTask.setStatus(UserTaskStatusEnum.AWARD_EXPIRE.getCode()); // -1: 中奖任务过期
                    userTask.setUtime(new Date()); // 更新时间
                    userTaskService.update(userTask);
                    // 判断是否是 红包成长任务--收藏加购，如果是，则不在重抽
                    if (userTask.getType() == ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                        continue;
                    }
                    // 更新用户活动记录的状态
                    userActivityService.updateWinnerStatusByUserIdAndActivityId(uid, aid, UserActivityStatusEnum.AWARD_EXPIRE.getCode()); // -2中奖任务过期
                    // 判断是否是新人有礼活动(裂变)，若是 则不再重抽
                    if (userTask.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
                        // TODO 退钱
                        //当该活动是用户发起的时候,为中奖任务未做 需要退款流程 支付方式不区分
                        Map<String, Object> upayWxOrderParams = new HashMap<>();
                        upayWxOrderParams.put("type", OrderTypeEnum.USERPAY.getCode());
                        upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
                        upayWxOrderParams.put("bizType", BizTypeEnum.ACTIVITY.getCode());
                        upayWxOrderParams.put("bizValue", activity.getId());
                        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
                        if (upayWxOrders != null && upayWxOrders.size() > 0) {
                            UpayWxOrder upayWxOrder = upayWxOrders.get(0);
                            //退款流程
                            SimpleResponse refund = this.upayWxOrderService.refund(upayWxOrder.getPayType(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getUserId(), upayWxOrder.getTotalFee(),
                                    upayWxOrder.getId(), upayWxOrder.getBizType(), upayWxOrder.getBizValue(), upayWxOrder.getOrderNo());
                            if (refund.getCode() == 0) {
                                //还原库存走一波
                                this.activityDefService.addStock(activity.getActivityDefId(), 1);
                            }
                        }
                        continue;
                    }
                    if (userTask.getType() == UserTaskTypeEnum.ASSIST_START.getCode() || userTask.getType() == UserTaskTypeEnum.ASSIST_START_V2.getCode()) {
                        //还原库存走一波
                        this.activityDefService.addDeposit(activity.getActivityDefId(), BigDecimal.ZERO, 1);
                        continue;
                    }
                    if (userTask.getType() == UserTaskTypeEnum.ORDER_BACK_START.getCode() || userTask.getType() == UserTaskTypeEnum.ORDER_BACK_START_V2.getCode()) {
                        //当该活动是用户发起的时候,为中奖任务未做 需要退款流程 支付方式不区分
                        Map<String, Object> upayWxOrderParams = new HashMap<>();
                        upayWxOrderParams.put("type", OrderTypeEnum.USERPAY.getCode());
                        upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
                        upayWxOrderParams.put("bizType", BizTypeEnum.ACTIVITY.getCode());
                        upayWxOrderParams.put("bizValue", activity.getId());
                        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
                        if (upayWxOrders != null && upayWxOrders.size() > 0) {
                            UpayWxOrder upayWxOrder = upayWxOrders.get(0);
                            //退款流程
                            SimpleResponse refund = this.upayWxOrderService.refund(upayWxOrder.getPayType(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getUserId(), upayWxOrder.getTotalFee(),
                                    upayWxOrder.getId(), upayWxOrder.getBizType(), upayWxOrder.getBizValue(), upayWxOrder.getOrderNo());
                            if (refund.getCode() == 0) {
                                //还原库存走一波
                                this.activityDefService.addStock(activity.getActivityDefId(), 1);
                            }
                        }
                    }

                    // 重抽中奖用户 并使信息入库
                    // 使用redis 先拿到 此任务的中奖名单
                    List<Object> winnerList = JSON.parseArray(stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_WINNER + aid));
                    if (CollectionUtils.isEmpty(winnerList)) {
                        continue;
                    }
                    Long userId = Long.parseLong(winnerList.get(index).toString()); // 获取中奖人
                    //重抽中奖人的推送消息
                    this.sendTemplateMessageService.sendTemplateMsg2Award(shopId, aid, userId);
                    String keyWords = userTask.getKeyWords();
                    // 中奖任务开始，用户信息入库
                    if (type == ActivityTypeEnum.ORDER_BACK_START.getCode() && !userId.equals(activityLeader)) {
                        type = ActivityTypeEnum.ORDER_BACK_JOIN.getCode();
                    }
                    if (type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode() && !userId.equals(activityLeader)) {
                        type = ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode();
                    }
                    UserTask newUserTask = userTaskService.insertUserTask(mid, shopId, aid, userId, UserTaskStatusEnum.INIT.getCode(), type, keyWords, goodsId,null);
                    userActivityService.updateWinnerStatusByUserIdAndActivityId(userId, aid, UserActivityStatusEnum.WINNER.getCode()); // 更新用户活动信息 2:中奖
                    index = index + 1;

                    stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME + uid + aid);
                    stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME_TOTAL + uid + aid);

                    redisService.deleteUserTaskCommitAndAudit(userTask.getId());
                } else {
                    if (overTime >= now) {
                        continue;
                    }
                    // 重抽中奖用户 任务过期 处理

                    // 更新用户中奖任务记录的状态
                    userTask.setStatus(UserTaskStatusEnum.AWARD_EXPIRE.getCode()); // -1: 中奖任务过期
                    userTask.setUtime(new Date()); // 更新时间
                    userTaskService.update(userTask);
                    // 更新用户活动记录的状态
                    userActivityService.updateWinnerStatusByUserIdAndActivityId(uid, aid, UserActivityStatusEnum.AWARD_EXPIRE.getCode()); // -2中奖任务过期
                    activityDefService.addStock(activityDefId, 1);

                    stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME + uid + aid);
                    stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_USERTASK_MISSION_TIME_TOTAL + uid + aid);

                    redisService.deleteUserTaskCommitAndAudit(userTask.getId());
                }

            }
        }
    }

}
