package mf.code.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.one.constant.MerchantShopCouponStatusEnum;
import mf.code.one.constant.MerchantShopCouponTypeEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.common.job
 * Description:
 *
 * @author gel
 * @date 2019-07-20 13:14
 */
@Slf4j
@Component
@Profile(value = {"dev", "test2", "prod"})
public class CouponScheduledService {

    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;

    /**
     * 十分钟
     */
    @Scheduled(fixedRate = 600000)
    public void checkExpireForCoupon() {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("type", MerchantShopCouponTypeEnum.GOODS.getCode());
        queryMap.put("couponStatusList", Arrays.asList(MerchantShopCouponStatusEnum.PUBLISH.getCode(),
                MerchantShopCouponStatusEnum.UNPUBLISH.getCode()));
        queryMap.put("expiredTime", new Date());
        List<MerchantShopCoupon> merchantShopCouponList = merchantShopCouponService.queryParams(queryMap);
        if (CollectionUtils.isEmpty(merchantShopCouponList)) {
            return;
        }
        if (merchantShopCouponList.size() > 1000) {
            // TODO 待分页操作
            log.error("优惠券更新过期时间job，查询优惠券数量过大，请调整逻辑");
        }
        for (MerchantShopCoupon merchantShopCoupon : merchantShopCouponList) {
            if (merchantShopCoupon.getCouponStatus() == MerchantShopCouponStatusEnum.PUBLISH.getCode()) {
                merchantShopCoupon.setCouponStatus(MerchantShopCouponStatusEnum.EXPIRE.getCode());
                merchantShopCouponService.updateMerchantShopCoupon(merchantShopCoupon);
            }
            // 查询用户优惠券，将其过期处理
            QueryWrapper<UserCoupon> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(UserCoupon::getCouponId, merchantShopCoupon.getId())
                    .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode());
            List<UserCoupon> list = userCouponService.list(queryWrapper);
            if (CollectionUtils.isEmpty(list)) {
                continue;
            }
            for (UserCoupon userCoupon : list) {
                userCoupon.setStatus(UserCouponStatusEnum.EXPIRE.getCode());
                userCoupon.setUtime(new Date());
            }
            userCouponService.updateBatchById(list);
        }
    }
}
