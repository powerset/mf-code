package mf.code.common.job;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.service.SellerMerchantOrderService;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.repo.enums.OrderPayTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantOrderService;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.common.job
 * Description: 查询订单状态-并得知是否退款 定时器
 *
 * @author: 夜辰
 * @date: 2018-11-13 11:26
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class RefundTaskScheduledService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private MerchantOrderService merchantOrderService;
    @Autowired
    private SellerMerchantOrderService sellerMerchantOrderService;


    private final int MAX_LIMIT = 30;

    /**
     * 扫出 订单状态-并得知是否退款
     */
    @Scheduled(fixedRate = 60000)
    public void appletRefundScheduled() {
        int pageNum = 0;
        for (; ; pageNum++) {
            //每次只查询20个
            Map<String, Object> reqParams = new HashMap<String, Object>();
            reqParams.put("type", OrderTypeEnum.USERPAY.getCode());//用户支付
            reqParams.put("status", OrderStatusEnum.ORDERING.getCode());//进行中
            reqParams.put("payType", OrderPayTypeEnum.WEIXIN.getCode());//微信支付的场景
            reqParams.put("order", "ctime");
            reqParams.put("direct", "asc");
            reqParams.put("size", MAX_LIMIT);
            reqParams.put("offset", pageNum * MAX_LIMIT);
            List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(reqParams);
            if (upayWxOrders == null || upayWxOrders.size() == 0) {
                break;
            }
            for (UpayWxOrder upayWxOrder : upayWxOrders) {
                //对于大于两分钟的,走退款流程
                long time = System.currentTimeMillis() - upayWxOrder.getCtime().getTime();
                if (time > 2 * 60 * 1000) {
                    //若大于10分钟，则暂定超时处理
                    if (time >= 10 * 60 * 1000) {
                        upayWxOrder.setStatus(OrderStatusEnum.TIMEOUT.getCode());
                        upayWxOrder.setUtime(new Date());
                        this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);
                        continue;
                    }
                    Map<String, Object> respQueryOrderMap = this.selectWxOrderInfo(upayWxOrder.getOrderNo(), WeixinPayConstants.PAYSCENE_APPLET_APPID);
                    if (CollectionUtils.isEmpty(respQueryOrderMap)) {
                        continue;
                    }
                    boolean wxOrderInfoStatus = this.getWxOrderStatusInfo(respQueryOrderMap);
                    if (wxOrderInfoStatus) {
                        upayWxOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
                        upayWxOrder.setTradeId(respQueryOrderMap.get("transaction_id").toString());
                        upayWxOrder.setPaymentTime(DateUtil.parseDate(respQueryOrderMap.get("time_end").toString(), null, "yyyyMMddHHmmss"));
                        upayWxOrder.setUtime(new Date());
                        this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);

                        //退款流程
                        this.upayWxOrderService.refund(upayWxOrder.getPayType(), upayWxOrder.getMchId(), upayWxOrder.getShopId(), upayWxOrder.getUserId(), upayWxOrder.getTotalFee(),
                                upayWxOrder.getId(), upayWxOrder.getBizType(), upayWxOrder.getBizValue(), upayWxOrder.getOrderNo());
                        continue;
                    }
                    //若大于120分钟，则暂定超时处理
                    if (time >= 120 * 60 * 1000) {
                        upayWxOrder.setStatus(OrderStatusEnum.TIMEOUT.getCode());
                        upayWxOrder.setUtime(new Date());
                        upayWxOrder.setRemark(JSONObject.toJSONString(respQueryOrderMap));
                        this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);
                    }
                }
            }
        }
    }

    /***
     * 扫出商户后台订单
     *
     * <return_msg><![CDATA]></return_msg>
     * <appid><![CDATA[wxb7784511b3f1d7cd]]></appid>
     * <mch_id><![CDATA[1515160051]]></mch_id>
     * <nonce_str><![CDATA[CoYkMR5vwJ1IR7Wa]]></nonce_str>
     * <sign><![CDATA[1B2E96985ADA57567E6F6D6B3896DFE2]]></sign>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <openid><![CDATA[ofPkK0kw8alJfQkF6--_rn99qN8Y]]></openid>
     * <is_subscribe><![CDATA[N]]></is_subscribe>
     * <trade_type><![CDATA[NATIVE]]></trade_type>
     * <bank_type><![CDATA[CFT]]></bank_type>
     * <total_fee>1</total_fee>
     * <fee_type><![CDATA[CNY]]></fee_type>
     * <transaction_id><![CDATA[4200000204201812194820907317]]></transaction_id>
     * <out_trade_no><![CDATA[V26yzCzK1545225200356]]></out_trade_no>
     * <attach><![CDATA[%E6%B5%8B%E8%AF%951]]></attach>
     * <time_end><![CDATA[20181219211529]]></time_end>
     * <trade_state><![CDATA[REFUND]]></trade_state>
     * <cash_fee>1</cash_fee>
     * <trade_state_desc><![CDATA[订单发生过退款，退款详情请查询退款单]]></trade_state_desc>
     * </xml>
     */

    @Scheduled(fixedRate = 60000)
    public void merchantRefundScheduled() {
        int pageNum = 1;
        for (; ; pageNum++) {
            Map<String, Object> params = new HashMap<>();
            QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
            params.put("status", mf.code.merchant.repo.enums.OrderStatusEnum.ORDERING.getCode());//进行中
            params.put("pay_type", mf.code.merchant.repo.enums.OrderPayTypeEnum.WEIXIN.getCode());//微信支付的场景
            params.put("type", mf.code.merchant.repo.enums.OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());//微信支付
            wrapper.allEq(params);
            // 分页查询 排序方式
            Page<MerchantOrder> page = new Page<>(pageNum, MAX_LIMIT);
            page.setAsc("ctime");
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);
            List<MerchantOrder> merchantOrders = merchantOrderIPage.getRecords();
            if (CollectionUtils.isEmpty(merchantOrders)) {
                break;
            }
            for (MerchantOrder merchantOrder : merchantOrders) {
                //对于大于两分钟的,走退款流程
                long time = System.currentTimeMillis() - merchantOrder.getCtime().getTime();
                if (time > 2 * 60 * 1000) {
                    Map<String, Object> respQueryOrderMap = this.selectWxOrderInfo(merchantOrder.getOrderNo(), WeixinPayConstants.PAYSCENE_PUB_APPID);
                    if (CollectionUtils.isEmpty(respQueryOrderMap)) {
                        continue;
                    }
                    boolean wxOrderInfoStatus = this.getWxOrderStatusInfo(respQueryOrderMap);
                    if (wxOrderInfoStatus) {
                        merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
                        merchantOrder.setTradeId(respQueryOrderMap.get("transaction_id").toString());
                        merchantOrder.setPaymentTime(DateUtil.parseDate(respQueryOrderMap.get("time_end").toString(), null, "yyyyMMddHHmmss"));
                        merchantOrder.setUtime(new Date());
                        this.merchantOrderService.updateById(merchantOrder);
                        //退款流程
                        this.sellerMerchantOrderService.merchantRefund(merchantOrder.getId(), merchantOrder.getTotalFee());
                        continue;
                    }

                    //若大于120分钟，则暂定超时处理
                    if (time >= 120 * 60 * 1000) {
                        merchantOrder.setStatus(OrderStatusEnum.TIMEOUT.getCode());
                        merchantOrder.setUtime(new Date());
                        merchantOrder.setRemark(JSONObject.toJSONString(respQueryOrderMap));
                        this.merchantOrderService.updateById(merchantOrder);
                    }
                }
            }
        }
    }


    private Map<String, Object> selectWxOrderInfo(String orderNo, Integer payScene) {
        Map<String, Object> queryParams = this.weixinPayService.getQueryOrderParams(orderNo, payScene);
        Map<String, Object> respQueryOrderMap = this.weixinPayService.queryOrder(queryParams);
        log.info("<<<<<<<<<< 查询微信该订单信息:{}", respQueryOrderMap);
        return respQueryOrderMap;
    }

    private Boolean getWxOrderStatusInfo(Map<String, Object> respQueryOrderMap) {
        boolean returnCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("return_code") != null &&
                StringUtils.isNotBlank(respQueryOrderMap.get("return_code").toString()) &&
                "SUCCESS".equals(respQueryOrderMap.get("return_code"));
        boolean resultCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("result_code") != null &&
                StringUtils.isNotBlank(respQueryOrderMap.get("result_code").toString()) &&
                "SUCCESS".equals(respQueryOrderMap.get("result_code"));
        boolean tradeStateSuccess = respQueryOrderMap != null && respQueryOrderMap.get("trade_state") != null &&
                StringUtils.isNotBlank(respQueryOrderMap.get("trade_state").toString()) &&
                "SUCCESS".equals(respQueryOrderMap.get("trade_state"));
        /***
         * SUCCESS—支付成功
         * REFUND—转入退款
         * NOTPAY—未支付
         * CLOSED—已关闭
         * REVOKED—已撤销（刷卡支付）
         * USERPAYING--用户支付中
         * PAYERROR--支付失败(其他原因，如银行返回失败)
         */
        if (returnCodeSuccess && resultCodeSuccess && tradeStateSuccess) {
            return true;
        } else {
            return false;
        }
    }
}
