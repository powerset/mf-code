package mf.code.common.job;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.AppletUserPayOrderService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.common.job
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-13 下午3:05
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class RobotCreateActivityScheduledService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private AppletUserPayOrderService appletUserPayOrderService;
    @Autowired
    private UserService userService;

    private final int MAX_LIMIT = 30;

    /**
     * 检测 用户中奖任务 完成情况，修改任务状态
     */
    @Scheduled(fixedRate = 120000)
    public void activityWinnerScheduled() {
        // 该免单抽奖活动 是正常发布未结束的，并且 没有任何用户在进行中， 并且判断是否有足够的库存发起。
        log.info("<<<<<<<<<<机器人开始扫描定义活动了");
        Map<String, Object> userParams = new HashMap<>();
        userParams.put("merchantId", 0);
        userParams.put("shopId", 0);
        userParams.put("grantStatus", -1);
        List<User> users = this.userService.listPageByParams(userParams);
        if (CollectionUtils.isEmpty(users)) {
            log.warn("机器人失踪了~~~");
            return;
        }
        // 所有商家店铺的所有免单抽奖活动
        int pageNum = 0;
        for (; ; pageNum++) {
            Map<String, Object> activityDefParam = new HashMap<>();
            activityDefParam.put("type", ActivityDefTypeEnum.ORDER_BACK.getCode());
            activityDefParam.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
            activityDefParam.put("del", DelEnum.NO.getCode());
            activityDefParam.put("size", MAX_LIMIT);
            activityDefParam.put("offset", pageNum * MAX_LIMIT);
            List<ActivityDef> activityDefs = activityDefService.selectByParams(activityDefParam);
            if (CollectionUtils.isEmpty(activityDefs)) {
                log.warn("没有订单类的定义活动");
                return;
            }

            // 此一个activityDefId下，获取 所有 发起的并且在进行中的活动，1，没法起成立，2发起过的，现在没有进行的了，也成立
            for (ActivityDef activityDef : activityDefs) {
                log.info("<<<<<<<<<<机器人开始着手活动定义编号为{}的活动了", activityDef.getId());
                long start = System.currentTimeMillis();
                Map<String, Object> activityParam = new HashMap<>();
                activityParam.put("merchantId", activityDef.getMerchantId());
                activityParam.put("shopId", activityDef.getShopId());
                activityParam.put("activityDefId", activityDef.getId());
                activityParam.put("status", ActivityStatusEnum.PUBLISHED.getCode());
                activityParam.put("awardStartTimeIsNull", "");
                activityParam.put("del", DelEnum.NO.getCode());
                List<Activity> activitys = activityService.findPage(activityParam);
                if (activitys == null || activitys.size() <= 1) {
                    // 判断 是否有足够的库存 发起
                    if (activityDef.getStock() < activityDef.getHitsPerDraw()) {
                        // 剩余库存数 小于 中奖人数 时 发起不了
                        log.warn("剩余库存数 小于 中奖人数 时 发起不了，活动定义编号{}", activityDef.getId());
                        continue;
                    }
                    log.info("<<<<<<<<<<机器人创建活动定义编号为{}的活动,def:{}", activityDef.getId(), activityDef);
                    // 要机器人发起
                    Map<String, Object> orderMap = new HashMap<>();
                    orderMap.put("amount", new BigDecimal(0.1).toString());
                    orderMap.put("shopId", 0);
                    orderMap.put("userId", users.get(0).getId());
                    orderMap.put("merchantId", 0);
                    orderMap.put("activityDefId", activityDef.getId());
                    SimpleResponse order = this.appletUserPayOrderService.createOrder(orderMap, null, true);
                    log.info("<<<<<<<<<< 机器人创建活动结束：{} 活动定义编号：{} cost {} ms", JSON.toJSONString(order), activityDef.getId(), System.currentTimeMillis() - start);
                }
            }
        }
    }
}
