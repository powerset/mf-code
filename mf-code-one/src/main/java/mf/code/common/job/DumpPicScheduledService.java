package mf.code.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.html2Image.Html2Image;
import mf.code.common.utils.FileUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.teacher.constant.TeacherInviteBizTypeEnum;
import mf.code.teacher.repo.po.TeacherInvite;
import mf.code.teacher.service.TeacherInviteService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.common.job
 * Description: 商品图片转储定时器
 *
 * @author: gel
 * @date: 2018-11-10 11:26
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class DumpPicScheduledService {

    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private TeacherInviteService teacherInviteService;

    /**
     * 每隔10分钟检测新建商品数据，将喜销宝返回的商品信息，转存到oss
     * 并保存
     */
    @Scheduled(fixedRate = 60000)
    public void DumpGoodsPic() {
        /**
         * 1、查询尚未转储的商品主图，商品详情图，店铺主图
         * 2、转储图片
         */
//        // 查询店铺
//        shopPicScheduled();
        // 查询商品
        goodsPicScheduled();
        // 商品详情图
        goodsDescScheduled();
    }

    /**
     * 店铺图片
     */
    private void shopPicScheduled() {
        Integer page = 1;
        for (; ; page++) {
            Map<String, Object> params = new HashMap<>();
            params.put("picPathNull", "null");
            Integer count = merchantShopService.countForPageList(params);
            if (count == 0) {
                break;
            }
            List<MerchantShop> merchantShopList = merchantShopService.pageListMerchantShopForJob(params, page, 100);
            if (CollectionUtils.isEmpty(merchantShopList)) {
                break;
            }
            for (int i = 0; i < merchantShopList.size(); i++) {
                if (StringUtils.isBlank(merchantShopList.get(i).getXxbtopPicPath())) {
                    continue;
                }
                MerchantShop merchantShop = merchantShopList.get(i);
                try {
                    if (StringUtils.equalsIgnoreCase(merchantShop.getXxbtopPicPath(), "[]")) {
                        continue;
                    }
                    //通过输入流获取图片数据
                    InputStream inStream = getInputStreamFromHttpGet("http://logo.taobao.com/shop-logo" + merchantShop.getXxbtopPicPath());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = inStream.read(buffer)) > -1) {
                        baos.write(buffer, 0, len);
                    }
                    baos.flush();
                    InputStream stream1 = new ByteArrayInputStream(baos.toByteArray());
                    String[] strs = merchantShop.getXxbtopPicPath().split("\\.");
                    String s = ossCaller.uploadObject2OSSInputstream(stream1, "shop/" + merchantShop.getId()
                            + "/" + merchantShop.getId() + "." + strs[strs.length - 1]);
                    merchantShop.setPicPath(s);
                    merchantShopService.updateMerchantShop(merchantShop);
                    if (inStream != null) {
                        inStream.close();
                    }
                    QueryWrapper<TeacherInvite> wrapper = new QueryWrapper();
                    wrapper.lambda()
                            .eq(TeacherInvite::getBizType, TeacherInviteBizTypeEnum.RECRUIT_MERCHANT.getCode())
                            .eq(TeacherInvite::getTid, merchantShop.getId())
                    ;
                    List<TeacherInvite> teacherInvites = this.teacherInviteService.list(wrapper);
                    if (!CollectionUtils.isEmpty(teacherInvites)) {
                        TeacherInvite teacherInvite = teacherInvites.get(0);
                        teacherInvite.setPicUrl(s);
                        teacherInvite.setUtime(new Date());
                        this.teacherInviteService.updateById(teacherInvite);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 商品主图
     */
    private void goodsPicScheduled() {
        Integer goodsPage = 1;
        for (; ; goodsPage++) {
            Map<String, Object> params = new HashMap<>();
            params.put("picUrlNull", "null");
            Integer count = goodsService.countForPageList(params);
            if (count == 0) {
                break;
            }
            List<Goods> goodsList = goodsService.pageListGoodsForJob(params, goodsPage, 100);
            if (CollectionUtils.isEmpty(goodsList)) {
                break;
            }
            for (int i = 0; i < goodsList.size(); i++) {
                Goods goods = goodsList.get(i);
                String xxbtopPicUrl = goods.getXxbtopPicUrl();
                if (StringUtils.isBlank(xxbtopPicUrl)) {
                    continue;
                }
                try {
                    //通过输入流获取图片数据
                    InputStream inStream = getInputStreamFromHttpGet(xxbtopPicUrl);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = inStream.read(buffer)) > -1) {
                        baos.write(buffer, 0, len);
                    }
                    baos.flush();
                    InputStream stream1 = new ByteArrayInputStream(baos.toByteArray());
                    String[] strs = xxbtopPicUrl.split("\\.");
                    String s = ossCaller.uploadObject2OSSInputstream(stream1, "goods/picUrl/" + goods.getId() + "." + strs[strs.length - 1]);
                    goods.setPicUrl(s);
                    goodsService.updateGoods(goods);
                    if (inStream != null) {
                        inStream.close();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 商品详情图
     */
    private void goodsDescScheduled() {
        Integer goodsDescPage = 1;
        for (; ; goodsDescPage++) {
            Map<String, Object> params = new HashMap<>();
            params.put("descPathNull", "null");
            Integer count = goodsService.countForPageList(params);
            if (count == 0) {
                break;
            }
            List<Goods> goodsList = goodsService.pageListGoodsForJob(params, goodsDescPage, 100);
            if (CollectionUtils.isEmpty(goodsList)) {
                break;
            }
            for (int i = 0; i < goodsList.size(); i++) {
                Goods goods = goodsList.get(i);
                // 商品详情代码段
                String xxbtopDesc = goods.getXxbtopDesc();
                if (StringUtils.isBlank(xxbtopDesc)) {
                    continue;
                }
                try {
                    String html = "<!DOCTYPE html><html><body>" + xxbtopDesc.replaceAll("< img", "<img") + "</body></html>";
                    final Html2Image html2Image = Html2Image.fromHtml(html, null);
                    html2Image.getImageRenderer().setAutoHeight(true);
                    html2Image.getImageRenderer().setWidth(750);
                    html2Image.getImageRenderer().setWriteCompressionQuality(0.5f);
                    String filePath = "/home/code/logs/image/";
                    FileUtil.createFileDir(filePath);
                    // 拼成文件路径
                    filePath = filePath + "desc.jpg";
                    html2Image.getImageRenderer().saveImage(filePath);
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    String s = ossCaller.uploadObject2OSSInputstream(fileInputStream, "goods/descPath/" + goods.getId() + "." + "jpg");
                    goods.setDescPath(s);
                    goodsService.updateGoods(goods);
                    fileInputStream.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private InputStream getInputStreamFromHttpGet(String url) throws IOException {
        HttpClient httpClient = new HttpClient();
        GetMethod g1 = new GetMethod(url);
        httpClient.executeMethod(g1);
        return g1.getResponseBodyAsStream();
    }

}
