package mf.code.common.job;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


/**
 * 为统计服务做数据清洗
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class DataRinseScheduledService {
    
}