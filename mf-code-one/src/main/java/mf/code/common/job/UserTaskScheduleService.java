package mf.code.common.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.common.job
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月07日 14:47
 */
@Slf4j
@Component
@Profile(value = {"test2", "prod"})
public class UserTaskScheduleService {
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private UserActivityService userActivityService;

    private final int MAX_LIMIT = 30;

    /***
     * 好评晒图+红包任务的过期还库存
     */
    @Scheduled(fixedRate = 15000)
    public void userTaskTimeOutGiveBackStock() {
        int pageNum = 1;
        for (; ; pageNum++) {
            Page<UserTask> page = new Page<>(pageNum, MAX_LIMIT);
            QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
            userTaskWrapper.lambda()
                    .in(UserTask::getType, Arrays.asList(
                            UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode(),
                            UserTaskTypeEnum.GOOD_COMMENT.getCode(),
                            UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode(),
                            UserTaskTypeEnum.GOOD_COMMENT_V2.getCode()
                    ))
                    .in(UserTask::getStatus, Arrays.asList(
                            UserTaskStatusEnum.INIT.getCode(),
                            UserTaskStatusEnum.AUDIT_ERROR.getCode()))
            ;
            IPage<UserTask> userTaskIPage = this.userTaskService.page(page, userTaskWrapper);
            if (CollectionUtils.isEmpty(userTaskIPage.getRecords())) {
                return;
            }
            List<Long> activityTaskIds = new ArrayList<Long>();
            List<Long> activityDefIds = new ArrayList<>();
            for (UserTask userTask : userTaskIPage.getRecords()) {
                if (userTask.getActivityTaskId() != null && userTask.getActivityTaskId() > 0 && activityTaskIds.indexOf(userTask.getActivityTaskId()) == -1) {
                    activityTaskIds.add(userTask.getActivityTaskId());
                }
                if (userTask.getActivityDefId() != null && userTask.getActivityDefId() > 0 && activityDefIds.indexOf(userTask.getActivityDefId()) == -1) {
                    activityDefIds.add(userTask.getActivityDefId());
                }
            }
            if (CollectionUtils.isEmpty(activityTaskIds) && CollectionUtils.isEmpty(activityDefIds)) {
                continue;
            }
            List<ActivityTask> activityTaskList = null;
            Map<Long, ActivityTask> map = new HashMap<>();
            if (!CollectionUtils.isEmpty(activityTaskIds)) {
                QueryWrapper<ActivityTask> activityTaskWrapper = new QueryWrapper<>();
                activityTaskWrapper.lambda()
                        .in(ActivityTask::getId, activityTaskIds);
                activityTaskList = this.activityTaskService.list(activityTaskWrapper);
                if (CollectionUtils.isEmpty(activityTaskList)) {
                    continue;
                }
                for (ActivityTask activityTask : activityTaskList) {
                    map.put(activityTask.getId(), activityTask);
                }
            }

            List<ActivityDef> activityDefs = null;
            Map<Long, ActivityDef> activityDefMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(activityDefIds)) {
                QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
                wrapper.lambda()
                        .in(ActivityDef::getId, activityDefIds)
                ;
                activityDefs = activityDefService.list(wrapper);
                if (CollectionUtils.isEmpty(activityDefs)) {
                    continue;
                }
                for (ActivityDef activityDef : activityDefs) {
                    activityDefMap.put(activityDef.getId(), activityDef);
                }
            }

            for (UserTask userTask : userTaskIPage.getRecords()) {
                boolean activiyTaskIdIsEmpty = userTask.getActivityTaskId() == null || userTask.getActivityTaskId() == 0;
                boolean activityDefIsEmpty = userTask.getActivityDefId() == null || userTask.getActivityDefId() == 0;
                if (activiyTaskIdIsEmpty && activityDefIsEmpty) {
                    continue;
                }
                ActivityTask activityTask = null;
                if (userTask.getActivityTaskId() != null && userTask.getActivityTaskId() > 0) {
                    activityTask = map.get(userTask.getActivityTaskId());
                    if (activityTask == null) {
                        continue;
                    }
                }
                ActivityDef activityDef = null;
                if (userTask.getActivityDefId() != null && userTask.getActivityDefId() > 0) {
                    activityDef = activityDefMap.get(userTask.getActivityDefId());
                    if (activityDef == null) {
                        continue;
                    }
                }

                Integer missionNeedTime = 0;
                if (activityTask != null) {
                    missionNeedTime = activityTask.getMissionNeedTime();
                }
                if (activityDef != null) {
                    Integer missionNeedTimeDef = activityDef.getMissionNeedTime() != null ? activityDef.getMissionNeedTime() : activityDef.getCondDrawTime();
                    missionNeedTime = missionNeedTimeDef;
                }

                //在规定时间范围内，是否已经提交过
                String costStr = redisService.getUserTaskSupplyAuditCost(userTask.getId());
                boolean deleteRedisKey = false;
                boolean updateUserTask = false;
                boolean addStock = false;
                if (userTask.getStatus() == UserTaskStatusEnum.INIT.getCode()) {
                    //非第一次提交
                    Date overTime = DateUtils.addMinutes(userTask.getStartTime(), missionNeedTime);
                    int costNum = 0;
                    if (StringUtils.isNotBlank(costStr)) {
                        if (StringUtils.isNotBlank(costStr)) {
                            costNum = NumberUtils.toInt(costStr);
                        }
                    }
                    overTime = DateUtils.addSeconds(overTime, costNum);
                    if (System.currentTimeMillis() > overTime.getTime()) {
                        updateUserTask = true;
                    }
                    //非第一次提交
                    if (costNum > 0 && updateUserTask) {
                        addStock = true;
                        deleteRedisKey = true;
                    }
                }

                if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_ERROR.getCode()) {
                    int costNum = 0;
                    if (StringUtils.isNotBlank(costStr)) {
                        costNum = NumberUtils.toInt(costStr);
                    }
                    //非第一次提交
                    Date overTime = DateUtils.addMinutes(userTask.getStartTime(), missionNeedTime);
                    overTime = DateUtils.addSeconds(overTime, costNum);
                    if (System.currentTimeMillis() > overTime.getTime()) {
                        deleteRedisKey = true;
                        updateUserTask = true;
                        addStock = true;
                    }
                }
                if (updateUserTask) {
                    userTask.setStatus(UserTaskStatusEnum.AWARD_EXPIRE.getCode());
                    userTask.setUtime(new Date());
                    this.userTaskService.updateById(userTask);

                    if (userTask.getUserActivityId() != null && userTask.getUserActivityId() > 0) {
                        UserActivity userActivity = this.userActivityService.selectById(userTask.getUserActivityId());
                        if (userActivity != null) {
                            userActivity.setStatus(UserActivityStatusEnum.AWARD_EXPIRE.getCode());
                            userActivity.setUtime(new Date());
                            this.userActivityService.updateById(userActivity);
                        }
                    }
                }
                if (deleteRedisKey) {
                    this.redisService.deleteUserTaskCommitAndAudit(userTask.getId());
                }
                if (addStock) {
                    //还库存--提交审核才扣库存
                    if (this.activityDefService.addDeposit(userTask.getActivityDefId(), BigDecimal.ZERO, 1) == 1) {
                        log.info("用户非活动任务还库存成功,任务编号：{}", userTask.getId());
                    } else {
                        log.info("用户非活动任务还库存失败功,任务编号：{}", userTask.getId());
                    }
                }
            }
        }
    }
}
