package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.common.constant.*;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 18:06
 */
@Service
@Slf4j
public class RedPackTaskBalanceOfClearingProgressConverterImpl extends BalanceOfClearingProgressService {
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean redPackType = activityDef.getType().equals(ActivityDefTypeEnum.RED_PACKET.getCode());
        if (redPackType) {
            QueryWrapper<ActivityTask> activityTaskWrapper = new QueryWrapper<>();
            activityTaskWrapper.lambda()
                    .eq(ActivityTask::getActivityDefId, activityDef.getId())
                    .in(ActivityTask::getStatus, Arrays.asList(
                            ActivityTaskStatusEnum.PUBLISHED.getCode(),
                            ActivityTaskStatusEnum.AUDIT_SUCCESS.getCode(),
                            ActivityTaskStatusEnum.DEL.getCode(),
                            ActivityTaskStatusEnum.END.getCode()
                    ))
                    .eq(ActivityTask::getDel, DelEnum.NO.getCode())
            ;
            List<ActivityTask> activityTasks = this.activityTaskService.list(activityTaskWrapper);
            if (CollectionUtils.isEmpty(activityTasks)) {
                return null;
            }
            List<Long> activityTaskIds = new ArrayList<Long>();
            Map<Long, ActivityTask> activityTaskMap = new HashMap<>();
            for (ActivityTask activityTask : activityTasks) {
                if (activityTaskIds.indexOf(activityTask.getId()) == -1) {
                    activityTaskIds.add(activityTask.getId());
                }
                activityTaskMap.put(activityTask.getId(), activityTask);
            }
            QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
            userTaskWrapper.lambda()
                    .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                    .eq(UserTask::getShopId, activityDef.getShopId())
                    .in(UserTask::getActivityTaskId, activityTaskIds)
                    .in(UserTask::getStatus, Arrays.asList(
                            UserTaskStatusEnum.AUDIT_ERROR.getCode(),
                            UserTaskStatusEnum.AUDIT_WAIT.getCode(),
                            UserTaskStatusEnum.AUDIT_SUCCESS.getCode(),
                            UserTaskStatusEnum.INIT.getCode(),
                            UserTaskStatusEnum.AWARD_SUCCESS.getCode()
                    ))
                    .orderByDesc(UserTask::getId)
            ;
            List<UserTask> userTasks = this.userTaskService.list(userTaskWrapper);
            //审核中数目
            int auditWaitNum = 0;
            //审核失败|审核成功数目
            int orderBackAuditFailOrAuditSuccessNum = 0;
            UserTask userTask1 = null;
            //绑定金额总计
            BigDecimal sumBindingAmount = BigDecimal.ZERO;
            //中奖金额总计
            BigDecimal sumAwardedMoneyAmount = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(userTasks)) {
                for (UserTask userTask : userTasks) {
                    if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                        ActivityTask activityTask = activityTaskMap.get(userTask.getActivityTaskId());
                        if (activityTask != null) {
                            sumAwardedMoneyAmount = sumAwardedMoneyAmount.add(activityTask.getRpAmount());
                        }
                    }
                    if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                        auditWaitNum = auditWaitNum + 1;
                        ActivityTask activityTask = activityTaskMap.get(userTask.getActivityTaskId());
                        if (activityTask != null) {
                            sumBindingAmount = sumBindingAmount.add(activityTask.getRpAmount());
                        }
                    }
                    if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_ERROR.getCode() ||
                            userTask.getStatus() == UserTaskStatusEnum.INIT.getCode() ||
                            userTask.getStatus() == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                        orderBackAuditFailOrAuditSuccessNum = orderBackAuditFailOrAuditSuccessNum + 1;
                        //最新的一个任务倒计时
                        if (userTask1 == null) {
                            userTask1 = userTask;
                        }
                        ActivityTask activityTask = activityTaskMap.get(userTask.getActivityTaskId());
                        if (activityTask != null) {
                            sumBindingAmount = sumBindingAmount.add(activityTask.getRpAmount());
                        }
                    }
                }
            }

            QueryWrapper<ActivityDefAudit> activityDefAuditWrapper = new QueryWrapper<>();
            activityDefAuditWrapper.eq("activity_def_id", activityDef.getId());
            activityDefAuditWrapper.eq("status", ActivityDefAuditStatusEnum.PASS.getCode());
            activityDefAuditWrapper.eq("del", DelEnum.NO.getCode());
            List<ActivityDefAudit> activityDefAudits = this.activityDefAuditService.list(activityDefAuditWrapper);
            if (CollectionUtils.isEmpty(activityDefAudits)) {
                return null;
            }
            int stockSum = 0;
            for (ActivityDefAudit activityDefAudit : activityDefAudits) {
                stockSum = stockSum + activityDefAudit.getStockApply();
            }

            BigDecimal sumFillBackOrderAmount = BigDecimal.ZERO;
            BigDecimal sumGoodCommentAmount = BigDecimal.ZERO;
            BigDecimal sumFavcartAmount = BigDecimal.ZERO;

            BigDecimal remainFillBackOrderAmount = BigDecimal.ZERO;
            BigDecimal remainGoodCommentAmount = BigDecimal.ZERO;
            BigDecimal remainFavcartAmount = BigDecimal.ZERO;
            for (ActivityTask activityTask : activityTasks) {
                BigDecimal stockApply = new BigDecimal(String.valueOf(stockSum));
                BigDecimal remainAmount = activityTask.getRpAmount().multiply(new BigDecimal(String.valueOf(activityTask.getRpStock())));
                if (activityTask.getRpStockDef() != null && activityTask.getRpStockDef() > 0) {
                    if (ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode() == activityTask.getType()) {
                        sumFillBackOrderAmount = sumFillBackOrderAmount.add(activityTask.getRpAmount().multiply(stockApply));
                        remainFillBackOrderAmount = remainFillBackOrderAmount.add(remainAmount);
                    } else if (ActivityTaskTypeEnum.GOOD_COMMENT.getCode() == activityTask.getType()) {
                        sumGoodCommentAmount = sumGoodCommentAmount.add(activityTask.getRpAmount().multiply(stockApply));
                        remainGoodCommentAmount = remainGoodCommentAmount.add(remainAmount);
                    } else if (ActivityTaskTypeEnum.FAVCART.getCode() == activityTask.getType()) {
                        sumFavcartAmount = sumFavcartAmount.add(activityTask.getRpAmount().multiply(stockApply));
                        remainFavcartAmount = remainFavcartAmount.add(remainAmount);
                    }
                }
            }

            BigDecimal sum = sumFillBackOrderAmount.add(sumGoodCommentAmount).add(sumFavcartAmount);
            BigDecimal remain = remainFillBackOrderAmount.add(remainGoodCommentAmount).add(remainFavcartAmount);

            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.from(activityDef, sum, sumAwardedMoneyAmount, remain, sumBindingAmount, auditWaitNum, orderBackAuditFailOrAuditSuccessNum);
            if (userTask1 != null) {
                dto.setCutoffTime(DateUtils.addHours(userTask1.getStartTime(), 24).getTime());
                dto.setCutoffDate(DateUtils.addHours(userTask1.getStartTime(), 24));
            }
            return dto;
        }
        return null;
    }
}
