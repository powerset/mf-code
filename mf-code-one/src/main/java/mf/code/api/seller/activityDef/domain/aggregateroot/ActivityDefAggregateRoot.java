package mf.code.api.seller.activityDef.domain.aggregateroot;

import lombok.Data;
import mf.code.activity.repo.po.ActivityDef;

/**
 * mf.code.api.seller.activityDef.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-27 上午10:07
 */
@Data
public class ActivityDefAggregateRoot {
	private Long activityDefId;
	private ActivityDef activityDefInfo;
}
