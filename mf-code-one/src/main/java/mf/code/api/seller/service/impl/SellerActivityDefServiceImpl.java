package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.seller.dto.ActivityDefReq;
import mf.code.api.seller.service.SellerActivityDefService;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.PageUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-15 下午4:07
 */
@Slf4j
@Service
public class SellerActivityDefServiceImpl implements SellerActivityDefService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserService userService;

    /**
     * 创建商户活动定义
     * @param activityDefReq 活动定义请求参数
     * @return 返回 SimpleResponse 响应体
     */
    @Override
    public SimpleResponse createActivityDef(ActivityDefReq activityDefReq) {
        SimpleResponse response = new SimpleResponse<>();

        String merchantId = activityDefReq.getMerchantId();
        String reqStatus = activityDefReq.getStatus();
        // 通用参数校验
        String typeStr = activityDefReq.getType();
        String shopId = activityDefReq.getShopId();
        if (StringUtils.isBlank(reqStatus)
                || StringUtils.isBlank(merchantId)
                || StringUtils.isBlank(shopId)
                || StringUtils.isBlank(typeStr)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("您的活动还未填完必填项，请补充后再提交");
            return response;
        }

        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 检查是否 有 创建 活动的资格
        response = hasQualificationToCreate(activityDefReq);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        // 检查参数 并创建活动对象
        ActivityDef activityDef = checkReqParam(activityDefReq);
        if (activityDef == null) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("您的活动还未填完必填项，请补充后再提交");
            return response;
        }
        if (activityDef.getHitsPerDraw() != null
                && activityDef.getStockDef() != null
                && activityDef.getHitsPerDraw() >= activityDef.getStockDef()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("中奖人数 必须小于 库存数量");
            return response;
        }

        Date date = new Date();
        activityDef.setCtime(date);
        activityDef.setUtime(date);
        Integer result = activityDefService.createActivityDef(activityDef);
        if (result == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：插入活动数据出错");
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        // 提交时，同时也 插入 库存记录表
        if (activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
            ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
            if (null == activityDefAudit) {
                log.error("创建活动定义时，插入activityDefAudit活动定义审核表出错");
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("创建活动定义时，插入activityDefAudit活动定义审核表出错");
                return response;
            }
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }


    @Override
    public SimpleResponse hasQualificationToCreate(ActivityDefReq activityDefReq) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String typeStr = activityDefReq.getType();
        String merchantId = activityDefReq.getMerchantId();
        String shopId = activityDefReq.getShopId();
        if (StringUtils.isBlank(typeStr)
                || StringUtils.isBlank(merchantId)
                || StringUtils.isBlank(shopId)
                || !RegexUtils.StringIsNumber(typeStr)
                || !RegexUtils.StringIsNumber(shopId)
                || !RegexUtils.StringIsNumber(merchantId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("type或merchantId或shopId未设置");
            return response;
        }

        int type = Integer.parseInt(typeStr);
        Long merchantID = Long.valueOf(merchantId);
        Long shopID = Long.valueOf(shopId);
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()
                || type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
            Map<String, Object> param = new HashMap<>();
            param.put("merchantId", merchantID);
            param.put("shopId", shopID);
            param.put("type", type);
            param.put("del", DelEnum.NO.getCode());
            List<ActivityDef> activityDefs = activityDefService.listPageByParams(param);
            if (activityDefs == null) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                    response.setMessage("您已有活动正在审核中，无法再次创建");
                    return response;
                }
                if (activityDef.getStatus() == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                    response.setMessage("您已有活动正在进行中，无法再次创建");
                    return response;
                }
            }
        }
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()
                || type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            String goodsId = activityDefReq.getGoodsId();
            if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)) {
                response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
                response.setMessage("请先设置参加活动的商品");
                return response;
            }
            // 查看此商品id是否已经创建了活动：先定位店铺，再定位要创建的活动类型，此类型下是否有商品id的活动存在
            Map<String, Object> param = new HashMap<>();
            param.put("merchanId", merchantID);
            param.put("shopId", shopID);
            param.put("type", type);
            param.put("goodsId", Long.valueOf(goodsId));
            param.put("del", DelEnum.NO.getCode());
            List<ActivityDef> activityDefs = activityDefService.listPageByParams(param);
            // 不存在，则可以创建
            if (activityDefs == null) {
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                return response;
            }
            // 存在则 判断存在的活动中，是否有待审核 和 已发布的状态，有则无法创建
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                    response.setMessage("您已有该商品的活动正在审核中，无法再次创建");
                    return response;
                }
                if (activityDef.getStatus() == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                    response.setMessage("您已有该商品的活动正在进行中，无法再次创建");
                    return response;
                }
            }
        }

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    /**
     * 商户活动定义，列表展示 已创建的活动
     * @param pageSize 列表大小
     * @param currentPage 当前页码
     * @param merchantId 商户id
     * @param shopId 店铺id
     * @param status 活动状态
     * @param depositStatus 保证金支付状态
     * @param types 活动类型
     * @return SimpleResponse 响应体
     */
    @Override
    public SimpleResponse   listPageActivityDef(String pageSize, String currentPage, String merchantId, String shopId, String status, String depositStatus, String types) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(merchantId) || StringUtils.isBlank(types)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        String[] typeStrList = types.trim().split(",");
        List<Integer> typeList = new ArrayList<>();
        for (String typeStr : typeStrList) {
            typeList.add(Integer.valueOf(typeStr));
        }

        Integer size = Integer.valueOf(pageSize);
        Integer page = Integer.valueOf(currentPage);

        // 定义查询条件
        Map<String, Object> param = new HashMap<>(16);
        param.put("merchantId", Long.valueOf(merchantId));
        param.put("types", typeList);
        param.put("del", DelEnum.NO.getCode());
        if (StringUtils.isNotBlank(shopId)) {
            param.put("shopId", Long.valueOf(shopId));
        }
        if (StringUtils.isNotBlank(status)) {
            param.put("status", Integer.valueOf(status));
        }
        if (StringUtils.isNotBlank(depositStatus)) {
            param.put("depositStatus", Integer.valueOf(depositStatus));
        }
        // 查询总记录数
        Integer count = activityDefService.countByParams(param);
        // 信息加工
        HashMap<String, Object> resultVO = new HashMap<>(16);
        resultVO.put("count", count);
        resultVO.put("page", page);
        resultVO.put("size", size);
        if (count == 0) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setMessage("您没有定义活动，赶紧去创建吧");
            return response;
        }
        // 设置分页查询条件--活动展示列表
        param.putAll(PageUtil.pageSizeTolimit(page, size, count));
        param.put("order", "id");
        param.put("direct", "desc");
        // 分页查询 活动列表
        List<ActivityDef> activityDefs = activityDefService.listPageByParams(param);
        ArrayList<Object> activityListVO = new ArrayList<>();

        for (ActivityDef activityDef : activityDefs) {
            HashMap<String, Object> activityDefVO = new HashMap<>();
            if (activityDef.getStatus() != ActivityDefStatusEnum.SAVE.getCode()
                    && activityDef.getDepositStatus() == DepositStatusEnum.AWAITING_PAYMENT.getCode()
                    && (activityDef.getApplyTime().getTime() + 7*24*3600*1000) < System.currentTimeMillis()) {
                activityDefVO.put("depositStatus", DepositStatusEnum.INVALID.getCode());
            } else {
                activityDefVO.put("depositStatus", activityDef.getDepositStatus());
            }

            // 通用 参数
            activityDefVO.put("activityId", activityDef.getId());
            activityDefVO.put("type", activityDef.getType());
            activityDefVO.put("shopId", activityDef.getShopId());
            MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityDef.getShopId());
            if (merchantShop == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("数据库异常：找不到店铺");
                return response;
            }
            activityDefVO.put("shopName", merchantShop.getShopName());
            activityDefVO.put("status", activityDef.getStatus());

            if (activityDef.getDepositStatus() == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("数据库异常：depositStatus未设置");
                return response;
            }

            activityDefVO.put("defStock", activityDef.getStockDef());
            BigDecimal depositDef = activityDef.getDepositDef();
            activityDefVO.put("defDeposit", depositDef);

            // 免单商品
            if (typeList.contains(ActivityDefTypeEnum.ORDER_BACK.getCode()) || typeList.contains(ActivityDefTypeEnum.NEW_MAN.getCode())) {
                activityDefVO.put("type", activityDef.getType());
                activityDefVO.put("goodsId", activityDef.getGoodsId());

                activityDefVO.put("totalDeposit", activityDef.getTotalDeposit());

                activityDefVO.put("defRpStock", activityDef.getTaskRpNumDef());
                activityDefVO.put("defRpDeposit", activityDef.getTaskRpDepositDef());

                activityDefVO.put("playerNum", 0);

                Map<String, Object> activityParam = new HashMap<>();
                activityParam.put("activityDefId", activityDef.getId());
                List<Activity> activities = activityService.findPage(activityParam);
                if (!CollectionUtils.isEmpty(activities)) {
                    // 获取对应活动id的集合
                    List<Long> activityIds = new ArrayList<>();
                    for (Activity activity : activities) {
                        activityIds.add(activity.getId());
                    }
                    // 通过aids查询 user_activity表 取得所有用户id

                    Map<String, Object> uActivityParam = new HashMap<>();
                    uActivityParam.put("activityIds", activityIds);
                    List<UserActivity> userActivities = userActivityService.pageListUserActivity(uActivityParam);
                    activityDefVO.put("playerNum", userActivities.size());
                }


            }

            if (typeList.contains(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
                if (activityDef.getStatus() == ActivityDefStatusEnum.CLOSE.getCode()
                        || activityDef.getStatus() == ActivityDefStatusEnum.END.getCode()
                        || activityDef.getStatus() == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                    BigDecimal leftDeposit = activityDef.getDeposit();
                    Integer stock = activityDef.getStock();
                    activityDefVO.put("leftDeposit", leftDeposit);
                    activityDefVO.put("leftStock", stock);
                    activityDefVO.put("playerNum", 0);

                    Map<String, Object> userCouponParams = new HashMap<>();
                    userCouponParams.put("merchantId", activityDef.getMerchantId());
                    userCouponParams.put("shopId", activityDef.getShopId());
                    userCouponParams.put("type", UserCouponTypeEnum.LUCKY_WHEEL.getCode());
                    userCouponParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
                    Map<String, Object> activityParam = new HashMap<>();
                    activityParam.put("activityDefId", activityDef.getId());
                    List<Activity> activities = activityService.findPage(activityParam);
                    if (!CollectionUtils.isEmpty(activities)) {
                        List<Long> activityIds = new ArrayList<>();
                        for (Activity activity : activities) {
                            activityIds.add(activity.getId());
                        }
                        userCouponParams.put("activityIds", activityIds);
                        List<UserCoupon> userCoupons = userCouponService.listPageByparams(userCouponParams);
                        activityDefVO.put("playerNum", userCoupons.size());
                    }

                }
            }

            if (typeList.contains(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())) {
                String goodsIds = activityDef.getGoodsIds();
                if (JsonParseUtil.booJsonArr(goodsIds)) {
                    List<Integer> goodsIdList = JSON.parseArray(goodsIds, Integer.class);
                    activityDefVO.put("goodsNum", goodsIdList.size());
                }
                activityDefVO.put("packcodePath", merchantShop.getPackcodePath());
                if (activityDef.getStatus() == ActivityDefStatusEnum.CLOSE.getCode()
                        || activityDef.getStatus() == ActivityDefStatusEnum.END.getCode()
                        || activityDef.getStatus() == ActivityDefStatusEnum.PUBLISHED.getCode()) {
                    BigDecimal leftDeposit = activityDef.getDeposit();
                    Integer stock = activityDef.getStock();
                    activityDefVO.put("leftDeposit", leftDeposit);
                    activityDefVO.put("leftStock", stock);
                    activityDefVO.put("paid", depositDef.subtract(leftDeposit));

                    Map<String, Object> userCouponParams = new HashMap<>();
                    userCouponParams.put("merchantId", activityDef.getMerchantId());
                    userCouponParams.put("shopId", activityDef.getShopId());
                    userCouponParams.put("type", UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
                    userCouponParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
                    userCouponParams.put("activityDefId", activityDef.getId());
                    List<UserCoupon> userCoupons = userCouponService.listPageByparams(userCouponParams);
                    activityDefVO.put("playerNum", userCoupons.size());
                }

            }
            activityListVO.add(activityDefVO);
        }
        resultVO.put("activityDefsInfo", activityListVO);


        // 返回结果
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    /**
     * 修改商户活动定义
     * @param activityDefReq 活动定义请求参数
     * @return 返回 SimpleResponse 响应体
     */
    @Override
    public SimpleResponse update(ActivityDefReq activityDefReq) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        String merchantId = activityDefReq.getMerchantId();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }

        if (StringUtils.isBlank(activityDefReq.getActivityDefId())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        // 活动入库
        ActivityDef activityDef = checkReqParam(activityDefReq);
        if (activityDef == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        if (activityDef.getHitsPerDraw() != null
                && activityDef.getStockDef() != null
                && activityDef.getHitsPerDraw() >= activityDef.getStockDef()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("中奖人数 必须小于 库存数量");
            return response;
        }

        activityDef.setUtime(new Date());
        activityDef.setId(Long.valueOf(activityDefReq.getActivityDefId()));
        Integer result = activityDefService.updateActivityDef(activityDef);
        if (result == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：更新活动数据出错");
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        // 提交时，同时也 插入 库存记录表
        if (activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {
            ActivityDefAudit activityDefAudit = new ActivityDefAudit();
            Date now = new Date();
            activityDefAudit.setMerchantId(activityDef.getMerchantId());
            activityDefAudit.setShopId(activityDef.getShopId());
            activityDefAudit.setActivityDefId(activityDef.getId());
            activityDefAudit.setType(activityDef.getType());
            activityDefAudit.setApplyTime(now);
            activityDefAudit.setGoodsIds(activityDef.getGoodsIds());
            activityDefAudit.setGoodsId(activityDef.getGoodsId());
            activityDefAudit.setStockDef(0);
            activityDefAudit.setStockApply(activityDef.getStockDef());
            activityDefAudit.setDepositDef(BigDecimal.ZERO);
            activityDefAudit.setDepositApply(activityDef.getDepositDef());
            activityDefAudit.setTaskRpNumDef(0);
            activityDefAudit.setTaskRpNumApply(activityDef.getTaskRpNumDef());
            activityDefAudit.setTaskRpDepositDef(BigDecimal.ZERO);
            activityDefAudit.setTaskRpDepositApply(activityDef.getTaskRpDepositDef());
            activityDefAudit.setDepositStatus(0);
            activityDefAudit.setKeyWord(activityDef.getKeyWord());
            activityDefAudit.setStatus(1);
            activityDefAudit.setDel(DelEnum.NO.getCode());
            activityDefAudit.setCtime(now);
            activityDefAudit.setUtime(now);
            Integer rows = activityDefAuditService.insertSelective(activityDefAudit);
            if (rows == 0) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
                return response;
            }
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public SimpleResponse suppyStock(String activityDefId, String merchantId, String applyDeposit, String applyStock) {
        SimpleResponse response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId) || StringUtils.isBlank(merchantId) || StringUtils.isBlank(applyDeposit) || StringUtils.isBlank(applyStock)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }

        response = querySuppyStockQualification(activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        Date now = new Date();
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(Long.valueOf(activityDefId));
        ActivityDefAudit activityDefAudit = new ActivityDefAudit();
        activityDefAudit.setMerchantId(activityDef.getMerchantId());
        activityDefAudit.setShopId(activityDef.getShopId());
        activityDefAudit.setActivityDefId(activityDef.getId());
        activityDefAudit.setType(activityDef.getType());
        activityDefAudit.setApplyTime(now);
        activityDefAudit.setGoodsIds(activityDef.getGoodsIds());
        activityDefAudit.setGoodsId(activityDef.getGoodsId());
        activityDefAudit.setStockDef(activityDef.getStockDef());
        activityDefAudit.setStockApply(Integer.valueOf(applyStock));
        activityDefAudit.setDepositDef(activityDef.getDepositDef());
        activityDefAudit.setDepositApply(new BigDecimal(applyDeposit));
        activityDefAudit.setDepositStatus(0);
        activityDefAudit.setKeyWord(activityDef.getKeyWord());
        activityDefAudit.setStatus(1);
        activityDefAudit.setDel(DelEnum.NO.getCode());
        activityDefAudit.setCtime(now);
        activityDefAudit.setUtime(now);
        Integer rows = activityDefAuditService.insertSelective(activityDefAudit);
        if (rows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public SimpleResponse querySuppyStockQualification(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId)
                || !RegexUtils.StringIsNumber(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("activityDefId请求参数未设置");
            return response;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("activityDefId", Long.valueOf(activityDefId));
        param.put("depositStatus", DepositStatusEnum.AWAITING_PAYMENT.getCode());
        param.put("del", DelEnum.NO.getCode());
        List<ActivityDefAudit> activityDefAudits = activityDefAuditService.listPageByParams(param);
        if (!CollectionUtils.isEmpty(activityDefAudits)) {
            for (ActivityDefAudit activityDefAudit : activityDefAudits) {
                if (activityDefAudit.getApplyTime().getTime() < DateUtils.addDays(new Date(),7).getTime()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                    response.setMessage("已有补库存记录还未审核，请联系平台运营尽快处理");
                    return response;
                }
            }
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public SimpleResponse queryGoodsInfo(String merchantId, String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(merchantId) || StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        Long activityDefID = Long.valueOf(activityDefId);
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefID);
        if (null == activityDef) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        Long goodsId = activityDef.getGoodsId();
        String goodsIds = activityDef.getGoodsIds();
        List<Map<String, Object>> listVO = new ArrayList<>();
        if (null != goodsId) {
            Goods goods = goodsService.selectById(goodsId);
            HashMap<String, Object> resultVO = new HashMap<>(16);
            resultVO.put("goodsId", goods.getId());
            resultVO.put("title", goods.getDisplayGoodsName());
            resultVO.put("price", goods.getDisplayPrice());
            resultVO.put("keyWord", activityDef.getKeyWord());
            listVO.add(resultVO);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(listVO);
            return response;
        }
        if (StringUtils.isNotBlank(goodsIds) && JsonParseUtil.booJsonArr(goodsIds)) {
            List<Long> goodsIdList = JSON.parseArray(goodsIds, Long.class);
            List<Goods> goodsList = goodsService.selectByIdList(goodsIdList);

            for (Goods goods : goodsList) {
                HashMap<String, Object> goodsVO = new HashMap<>(16);
                goodsVO.put("goodsId", goods.getId());
                goodsVO.put("title", goods.getDisplayGoodsName());
                goodsVO.put("price", goods.getDisplayPrice());
                listVO.add(goodsVO);
            }


            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(listVO);
            return  response;
        }



        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setMessage("您还未添加商品");
        return response;
    }

    @Override
    public SimpleResponse queryActivityDefStockInfo(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        Long activityDefID = Long.valueOf(activityDefId);

        Map<String, Object> activityDefAuditParam = new HashMap<>(16);
        activityDefAuditParam.put("activityDefId", activityDefID);
        activityDefAuditParam.put("status" , ActivityDefAuditStatusEnum.PASS.getCode());
        activityDefAuditParam.put("order", "apply_time");
        activityDefAuditParam.put("direct", "desc");
        List<ActivityDefAudit> activityDefAudits = activityDefAuditService.listPageByParams(activityDefAuditParam);
        List<Map<String, Object>> listVO = new ArrayList<>();
        for (ActivityDefAudit activityDefAudit : activityDefAudits) {
            HashMap<String, Object> activityDefAuditVO = new HashMap<>(16);
            activityDefAuditVO.put("applyTime", DateUtil.dateToString(activityDefAudit.getApplyTime(), DateUtil.FORMAT_TWO));
            activityDefAuditVO.put("applyStock", activityDefAudit.getStockApply());
            activityDefAuditVO.put("applyDeposit", activityDefAudit.getDepositApply());
            activityDefAuditVO.put("status", activityDefAudit.getStatus());
            activityDefAuditVO.put("actualStock", activityDefAudit.getStockActual());
            activityDefAuditVO.put("actualDeposit", activityDefAudit.getDepositActual());
            activityDefAuditVO.put("voucher", activityDefAudit.getVoucher());
            activityDefAuditVO.put("initStock", activityDefAudit.getStockDef());
            activityDefAuditVO.put("finalStock", activityDefAudit.getStockFinal());
            listVO.add(activityDefAuditVO);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(listVO);
        return response;
    }

    @Override
    public SimpleResponse getActivityInfo(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(Long.valueOf(activityDefId));
        HashMap<String, Object> activityDefVO = new HashMap<>();
        activityDefVO.put("merchantId", activityDef.getMerchantId());
        activityDefVO.put("type", activityDef.getType());
        activityDefVO.put("shopId", activityDef.getShopId());
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityDef.getShopId());
        activityDefVO.put("shopName", merchantShop.getShopName());
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()) {
            activityDefVO.put("initStock", activityDef.getStockDef());
            activityDefVO.put("depositDef", activityDef.getDepositDef());
        } else {
            HashMap<String, Object> param = new HashMap<>();
            param.put("activityDefId", activityDef.getId());
            param.put("stockDef", 0);
            List<ActivityDefAudit> activityDefAudits = activityDefAuditService.listPageByParams(param);
            if (CollectionUtils.isEmpty(activityDefAudits)) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("数据库异常：activityDefAudit没有相应的活动数据");
                return response;
            }
            activityDefVO.put("initStock", activityDefAudits.get(0).getStockApply());
            activityDefVO.put("depositDef", activityDefAudits.get(0).getDepositApply());
        }
        if (activityDef.getType() == ActivityDefTypeEnum.ORDER_BACK.getCode() || activityDef.getType() == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            if (activityDef.getGoodsId() != null) {
                Goods goods = goodsService.selectById(activityDef.getGoodsId());
                Map<String, Object> goodsVO = new HashMap<>();
                List<Map<String, Object>> goodsVOList = new ArrayList<>();
                goodsVO.put("goodsId", goods.getId());
                goodsVO.put("pic", goods.getPicUrl());
                goodsVO.put("title", goods.getDisplayGoodsName());
                goodsVO.put("price", goods.getDisplayPrice());
                goodsVOList.add(goodsVO);
                activityDefVO.put("goodsVOlist", goodsVOList);
            }
            activityDefVO.put("condPersionCount", activityDef.getCondPersionCount());
            activityDefVO.put("hitsPerDraw", activityDef.getHitsPerDraw());
            activityDefVO.put("taskRpAmount", activityDef.getTaskRpAmount());
            activityDefVO.put("taskRpNum", activityDef.getTaskRpNumDef());
            activityDefVO.put("taskRpDeposit", activityDef.getTaskRpDepositDef());
            activityDefVO.put("condDrawTime", activityDef.getCondDrawTime()/60/24);
            activityDefVO.put("missionNeedTime", activityDef.getMissionNeedTime());
            activityDefVO.put("keyword", "");
            if (StringUtils.isNotBlank(activityDef.getKeyWord())) {
                activityDefVO.put("keyword", JSON.parseArray(activityDef.getKeyWord(), String.class));
            }

            activityDefVO.put("merchantCouponJson", "");
            if (StringUtils.isNotBlank(activityDef.getMerchantCouponJson())) {
                activityDefVO.put("merchantCouponJson", JSON.parseArray(activityDef.getMerchantCouponJson(), String.class));
            }
            // 20190119 更新客服微信号和微信图片
            if(StringUtils.isNotBlank(activityDef.getServiceWx())){
                if(JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                    List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                    if (!CollectionUtils.isEmpty(csMapList)) {
                        //微信号
                        activityDefVO.put("serviceWx", csMapList.get(0).get("wechat"));
                        //微信二维码
                        activityDefVO.put("serviceWxPic", csMapList.get(0).get("pic"));
                    }
                }else {
                    activityDefVO.put("serviceWx", activityDef.getServiceWx());
                    activityDefVO.put("serviceWxPic", "");
                }
            }
        }
        if (activityDef.getType() == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
            String goodsIds = activityDef.getGoodsIds();
            List<Map<String, Object>> listVO = new ArrayList<>();
            if (StringUtils.isNotBlank(goodsIds) && JsonParseUtil.booJsonArr(goodsIds)) {
                List<Long> goodsIdList = JSON.parseArray(goodsIds, Long.class);
                List<Goods> goodsList = goodsService.selectByIdList(goodsIdList);
                for (Goods goods : goodsList) {
                    HashMap<String, Object> goodsVO = new HashMap<>(16);
                    goodsVO.put("goodsId", goods.getId());
                    goodsVO.put("pic", goods.getPicUrl());
                    goodsVO.put("title", goods.getDisplayGoodsName());
                    goodsVO.put("price", goods.getDisplayPrice());
                    listVO.add(goodsVO);
                }
            }
            activityDefVO.put("goodsVOlist", listVO);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(activityDefVO);
        return response;
    }

    @Override
    public SimpleResponse getDeposit(String typeStr, String goodsId, String stockDefStr) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        int type;
        int stockDef;
        if (StringUtils.isNotBlank(typeStr) && RegexUtils.StringIsNumber(typeStr)
                && (StringUtils.isNotBlank(stockDefStr) && RegexUtils.StringIsNumber(stockDefStr))) {
            type = Integer.valueOf(typeStr);
            stockDef = Integer.valueOf(stockDefStr);
        } else {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()
                || type == ActivityDefTypeEnum.ASSIST.getCode()
                || type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            if (StringUtils.isNotBlank(goodsId) && RegexUtils.StringIsNumber(goodsId)) {
                Goods goods = goodsService.selectById(Long.valueOf(goodsId));
                if (goods == null) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                    response.setMessage("数据库异常：没有该商品信息");
                    return response;
                }
                BigDecimal goodsPrice = goods.getDisplayPrice();
                BigDecimal depositDef = goodsPrice.multiply(new BigDecimal(stockDef));
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                Map<String, Object> resultVO = new HashMap<>(16);
                resultVO.put("depositDef", depositDef);
                resultVO.put("goodsPrice", goodsPrice);
                response.setData(resultVO);
                return response;
            }
        }
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            // 每次抽奖 平均所用金额averagePrice （ total 扩大100倍 防止丢失精度）
            int total = 0 * 44 + 20 * 50 + 40 * 5 + 75 * 1;
            BigDecimal averagePrice = new BigDecimal(total).divide(new BigDecimal(10000));
            // 预计每人抽奖的次数
            int averageTimes = 4;
            // 新人平均成本价
            BigDecimal averageCost = averagePrice.multiply(new BigDecimal(averageTimes));
            // 所需保证金
            BigDecimal depositDef = averageCost.multiply(new BigDecimal(stockDef));
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            Map<String, Object> resultVO = new HashMap<>(16);
            resultVO.put("averageCost", averageCost);
            resultVO.put("depositDef", depositDef);
            response.setData(resultVO);
            return response;
        }
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
            // 规则：获取0.1-1.0元（包含1.0）的概率80%，获取1.1-3.0元（包含3.0）随机红包的概率10%，获取3.1-5.0元（包含5.0）随机红包的概率10%
            // averageCost (0.55*80+2.05*10+4.05*10)100
            BigDecimal averageCost = new BigDecimal(1.05);
            BigDecimal depositDef = averageCost.multiply(new BigDecimal(stockDef));
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            Map<String, Object> resultVO = new HashMap<>(16);
            resultVO.put("averageCost", averageCost);
            resultVO.put("depositDef", depositDef);
            response.setData(resultVO);
            return response;
        }
        response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
        return response;
    }

    @Override
    public SimpleResponse getActivityDefData(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(Long.valueOf(activityDefId));
        if (activityDef == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：未查到此条活动");
            return response;
        }
        if (activityDef.getType() != ActivityDefTypeEnum.ORDER_BACK.getCode() || activityDef.getType() != ActivityDefTypeEnum.NEW_MAN.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：活动类型匹配");
            return response;
        }
        if (activityDef.getStatus() == ActivityDefStatusEnum.SAVE.getCode()
                || activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_WAIT.getCode()
                || activityDef.getStatus() == ActivityDefStatusEnum.AUDIT_ERROR.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：活动状态异常");
            return response;
        }
        if (activityDef.getDel() == DelEnum.YES.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：活动已删除");
            return response;
        }
        HashMap<Object, Object> activityDefDataVO = new HashMap<>(16);
        activityDefDataVO.put("usedStock", activityDef.getStockDef() - activityDef.getStock());
        activityDefDataVO.put("usedDeposit", activityDef.getDepositDef().subtract(activityDef.getDeposit()));
        activityDefDataVO.put("leftDeposit", activityDef.getDeposit());
        activityDefDataVO.put("usedTaskRpNum", activityDef.getTaskRpNumDef() - activityDef.getTaskRpNum());
        activityDefDataVO.put("usedTaskRpDeposit", activityDef.getTaskRpDepositDef().subtract(activityDef.getTaskRpDeposit()));
        activityDefDataVO.put("leftTaskRpDeposit", activityDef.getTaskRpDeposit());

        // 返回结果
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(activityDefDataVO);
        return response;
    }

    // 私有方法： 创建活动 或 修改活动 对req参数的校验
    private ActivityDef checkReqParam(ActivityDefReq activityDefReq){
        String statusStr = activityDefReq.getStatus();
        // 通用参数校验
        String typeStr = activityDefReq.getType();
        String merchantId = activityDefReq.getMerchantId();
        String shopId = activityDefReq.getShopId();
        if (StringUtils.isBlank(statusStr)
                || StringUtils.isBlank(merchantId)
                || StringUtils.isBlank(shopId)
                || StringUtils.isBlank(typeStr)) {
            return null;
        }
        if (!RegexUtils.StringIsNumber(typeStr)
                || !RegexUtils.StringIsNumber(merchantId)
                || !RegexUtils.StringIsNumber(shopId)
                || !RegexUtils.StringIsNumber(statusStr)) {
            return null;
        }
        // 把新人有礼活动 转化 为助力活动 begin
        activityDefReq = newMan2Assist(activityDefReq);
        // end

        int type = Integer.parseInt(activityDefReq.getType());
        int status = Integer.parseInt(statusStr);

        ActivityDef activityDef = new ActivityDef();
        activityDef.setMerchantId(Long.valueOf(merchantId));

        activityDef.setType(type);
        activityDef.setShopId(Long.valueOf(shopId));
        activityDef.setStatus(status);
        // 当 活动提交审核时，进行参数校验，保存时 不校验
        String goodsId = activityDefReq.getGoodsId();
        String condPersionCount = activityDefReq.getCondPersionCount();
        String stockDef = activityDefReq.getStockDef();
        String hitsPerDraw = activityDefReq.getHitsPerDraw();
        String taskRpNumStr = activityDefReq.getTaskRpNum();
        String condDrawTime = activityDefReq.getCondDrawTime();
        String missionNeedTime = activityDefReq.getMissionNeedTime();
        String taskRpAmountStr = activityDefReq.getTaskRpAmount();
        if (status == ActivityDefStatusEnum.AUDIT_WAIT.getCode()) {

            // 提交状态，根据不同活动类型 校验不同需求参数
            // 新人有礼 或者 免单商品抽奖 的参数校验      新增 助力活动的校验
            if (type == ActivityDefTypeEnum.NEW_MAN.getCode()
                    || type == ActivityDefTypeEnum.ASSIST.getCode()
                    || type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
                log.info("创建 免单商品活动");
                if (StringUtils.isBlank(goodsId)
                        || StringUtils.isBlank(condPersionCount)
                        || StringUtils.isBlank(stockDef)
                        || StringUtils.isBlank(activityDefReq.getDepositDef())
                        || StringUtils.isBlank(hitsPerDraw)
                        || StringUtils.isBlank(condDrawTime)
                        || StringUtils.isBlank(missionNeedTime)
                        || StringUtils.isBlank(activityDefReq.getKeyWord())
                        || (StringUtils.isBlank(activityDefReq.getServiceWx()) && StringUtils.isBlank(activityDefReq.getServiceWxPic()))) {
                    log.info("免单商品活动：参数缺失");
                    return null;
                }
                if (!RegexUtils.StringIsNumber(goodsId)
                        || !RegexUtils.StringIsNumber(stockDef)
                        || !RegexUtils.StringIsNumber(hitsPerDraw)
                        || !RegexUtils.StringIsNumber(taskRpNumStr)
                        || !RegexUtils.StringIsNumber(condDrawTime)
                        || !RegexUtils.StringIsNumber(missionNeedTime)
                        || !RegexUtils.StringIsNumber(condPersionCount)) {
                    return null;
                }
                // 活动入库
                activityDef.setGoodsId(Long.valueOf(goodsId));
                activityDef.setCondPersionCount(Integer.valueOf(condPersionCount));
                SimpleResponse depositResponse = getDeposit(typeStr, goodsId, stockDef);
                if (depositResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                    return null;
                }
                Map depositMap = (Map)depositResponse.getData();
                BigDecimal depositDef = new BigDecimal(depositMap.get("depositDef").toString());
                BigDecimal goodsPrice = new BigDecimal(depositMap.get("goodsPrice").toString());
                activityDef.setGoodsPrice(goodsPrice);
                activityDef.setDepositDef(depositDef);
                activityDef.setDeposit(depositDef);
                activityDef.setTotalDeposit(depositDef);
                if (StringUtils.isNotBlank(taskRpAmountStr)
                        && RegexUtils.StringIsNumber(taskRpAmountStr)
                        && StringUtils.isNotBlank(taskRpNumStr)
                        && RegexUtils.StringIsNumber(taskRpNumStr)) {
                    BigDecimal taskRpAmount = new BigDecimal(taskRpAmountStr);
                    Integer taskRpNum = Integer.valueOf(taskRpNumStr);
                    activityDef.setTaskRpAmount(taskRpAmount);
                    activityDef.setTaskRpNumDef(taskRpNum);
                    activityDef.setTaskRpNum(taskRpNum);
                    BigDecimal taskRpDeposit = taskRpAmount.multiply(new BigDecimal(taskRpNum));
                    activityDef.setTaskRpDepositDef(taskRpDeposit);
                    activityDef.setTaskRpDeposit(taskRpDeposit);
                    activityDef.setTotalDeposit(depositDef.add(taskRpDeposit));
                }
                activityDef.setStockDef(Integer.valueOf(stockDef));
                activityDef.setStock(Integer.valueOf(stockDef));
                activityDef.setHitsPerDraw(Integer.valueOf(hitsPerDraw));

                activityDef.setCondDrawTime(Integer.valueOf(condDrawTime)*24*60);
                activityDef.setMissionNeedTime(Integer.valueOf(missionNeedTime));
                if(StringUtils.isNotBlank(activityDefReq.getMerchantCouponJson())) {
                    String[] merchantCouponJson = activityDefReq.getMerchantCouponJson().trim().split(",");
                    List<Long> merchantCouponList = new ArrayList<>();
                    for (String merchantCouponId : merchantCouponJson) {
                        if (RegexUtils.StringIsNumber(merchantCouponId)) {
                            merchantCouponList.add(Long.valueOf(merchantCouponId));
                        }
                    }
                    if (!CollectionUtils.isEmpty(merchantCouponList)) {
                        activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
                    }
                }

                String[] keyWordJson = activityDefReq.getKeyWord().trim().split(",");
                activityDef.setKeyWord(JSON.toJSONString(keyWordJson));

                List<Map<String, String>> csMapList = new ArrayList<>();
                Map<String, String> csMap = new HashMap<>(2);
                csMap.put("wechat", activityDefReq.getServiceWx());
                csMap.put("pic", activityDefReq.getServiceWxPic());
                csMapList.add(csMap);
                activityDef.setServiceWx(JSONArray.toJSONString(csMapList));
                activityDef.setJeton(new BigDecimal(0.1));

            }
            if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
                if (StringUtils.isBlank(stockDef)
                        || StringUtils.isBlank(activityDefReq.getDepositDef())) {
                    return null;
                }

                if (!RegexUtils.StringIsNumber(stockDef)) {
                    return null;
                }
                // 活动入库
                activityDef.setStockDef(Integer.valueOf(stockDef));
                activityDef.setStock(Integer.valueOf(stockDef));

                SimpleResponse depositResponse = getDeposit(typeStr, goodsId, stockDef);
                if (depositResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                    return null;
                }
                Map depositMap = (Map)depositResponse.getData();
                BigDecimal depositDef = new BigDecimal(depositMap.get("depositDef").toString());
                activityDef.setDepositDef(depositDef);
                activityDef.setDeposit(depositDef);

            }
            if (type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
                if (StringUtils.isBlank(activityDefReq.getGoodsIds())
                        || StringUtils.isBlank(stockDef)
                        || StringUtils.isBlank(activityDefReq.getDepositDef())) {
                    return null;
                }

                if (!RegexUtils.StringIsNumber(stockDef)) {
                    return null;
                }

                // 活动入库
                String[] goodsIds = activityDefReq.getGoodsIds().trim().split(",");
                List<Long> goodsIdList = new ArrayList<>();
                for (String goodsIdsStr : goodsIds) {
                    if (RegexUtils.StringIsNumber(goodsIdsStr)) {
                        goodsIdList.add(Long.valueOf(goodsIdsStr));
                    }
                }
                if (CollectionUtils.isEmpty(goodsIdList)) {
                    return null;
                }
                activityDef.setGoodsIds(JSON.toJSONString(goodsIdList));
                activityDef.setStockDef(Integer.valueOf(stockDef));
                activityDef.setStock(Integer.valueOf(stockDef));
                SimpleResponse depositResponse = getDeposit(typeStr, goodsId, stockDef);
                if (depositResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                    return null;
                }
                Map depositMap = (Map)depositResponse.getData();
                BigDecimal depositDef = new BigDecimal(depositMap.get("depositDef").toString());
                activityDef.setDepositDef(depositDef);
                activityDef.setDeposit(depositDef);
            }
            activityDef.setApplyTime(new Date());
        } else {

            if (StringUtils.isNotBlank(goodsId) && RegexUtils.StringIsNumber(goodsId)) {
                if (StringUtils.isNotBlank(stockDef) && RegexUtils.StringIsNumber(stockDef)) {
                    activityDef.setGoodsId(Long.valueOf(goodsId));
                    SimpleResponse depositResponse = getDeposit(typeStr, goodsId, stockDef);
                    if (depositResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                        return null;
                    }
                    Map depositMap = (Map) depositResponse.getData();
                    BigDecimal depositDef = new BigDecimal(depositMap.get("depositDef").toString());
                    BigDecimal goodsPrice = new BigDecimal(depositMap.get("goodsPrice").toString());
                    activityDef.setGoodsPrice(goodsPrice);

                    activityDef.setDepositDef(depositDef);
                    activityDef.setDeposit(depositDef);

                    activityDef.setStockDef(Integer.valueOf(stockDef));
                    activityDef.setStock(Integer.valueOf(stockDef));
                } else {
                    Goods goods = goodsService.selectById(Long.valueOf(goodsId));
                    if (goods != null) {
                        BigDecimal goodsPrice = goods.getDisplayPrice();
                    }
                }
            }
            if (StringUtils.isNotBlank(condPersionCount)
                    && RegexUtils.StringIsNumber(condPersionCount)) {
                activityDef.setCondPersionCount(Integer.valueOf(condPersionCount));
            }

            if (StringUtils.isNotBlank(hitsPerDraw)
                    && RegexUtils.StringIsNumber(hitsPerDraw)) {
                activityDef.setHitsPerDraw(Integer.valueOf(hitsPerDraw));
            }

            if (StringUtils.isNotBlank(taskRpAmountStr)
                    && RegexUtils.StringIsNumber(taskRpAmountStr)
                    && StringUtils.isNotBlank(taskRpNumStr)
                    && RegexUtils.StringIsNumber(taskRpNumStr)) {
                BigDecimal taskRpAmount = new BigDecimal(taskRpAmountStr);
                Integer taskRpNum = Integer.valueOf(taskRpNumStr);
                activityDef.setTaskRpAmount(taskRpAmount);
                activityDef.setTaskRpNumDef(taskRpNum);
                activityDef.setTaskRpNum(taskRpNum);
                BigDecimal taskRpDeposit = taskRpAmount.multiply(new BigDecimal(taskRpNum));
                activityDef.setTaskRpDepositDef(taskRpDeposit);
                activityDef.setTaskRpDeposit(taskRpDeposit);
            } else if (StringUtils.isNotBlank(taskRpAmountStr) && RegexUtils.StringIsNumber(taskRpAmountStr)){
                BigDecimal taskRpAmount = new BigDecimal(taskRpAmountStr);
                activityDef.setTaskRpAmount(taskRpAmount);
            } else if (StringUtils.isNotBlank(taskRpNumStr) && RegexUtils.StringIsNumber(taskRpNumStr)) {
                Integer taskRpNum = Integer.valueOf(taskRpNumStr);
                activityDef.setTaskRpNumDef(taskRpNum);
                activityDef.setTaskRpNum(taskRpNum);
            }

            if (StringUtils.isNotBlank(condDrawTime)
                    && RegexUtils.StringIsNumber(condDrawTime)) {
                activityDef.setCondDrawTime(Integer.valueOf(condDrawTime)*24*60);
            }
            if (StringUtils.isNotBlank(missionNeedTime)
                    && RegexUtils.StringIsNumber(missionNeedTime)) {
                activityDef.setMissionNeedTime(Integer.valueOf(missionNeedTime));
            }
            if(StringUtils.isNotBlank(activityDefReq.getMerchantCouponJson())) {
                String[] merchantCouponJson = activityDefReq.getMerchantCouponJson().trim().split(",");
                List<Long> merchantCouponList = new ArrayList<>();
                for (String merchantCouponId : merchantCouponJson) {
                    if (RegexUtils.StringIsNumber(merchantCouponId)) {
                        merchantCouponList.add(Long.valueOf(merchantCouponId));
                    }
                }
                if (!CollectionUtils.isEmpty(merchantCouponList)) {
                    activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
                }
            }
            if (StringUtils.isNotBlank(activityDefReq.getGoodsIds())) {
                String[] goodsIds = activityDefReq.getGoodsIds().trim().split(",");
                List<Long> goodsIdList = new ArrayList<>();
                for (String goodsIdsStr : goodsIds) {
                    if (RegexUtils.StringIsNumber(goodsIdsStr)) {
                        goodsIdList.add(Long.valueOf(goodsIdsStr));
                    }
                }
                activityDef.setGoodsIds(JSON.toJSONString(goodsIdList));
            }
            if (StringUtils.isNotBlank(activityDefReq.getKeyWord())) {
                String[] keyWordJson = activityDefReq.getKeyWord().trim().split(",");
                activityDef.setKeyWord(JSON.toJSONString(keyWordJson));
            }

            String serviceWx = activityDefReq.getServiceWx();
            String serviceWxPic = activityDefReq.getServiceWxPic();
            if (StringUtils.isNotBlank(serviceWx) || StringUtils.isNotBlank(serviceWxPic)) {
                List<Map<String, String>> csMapList = new ArrayList<>();
                Map<String, String> csMap = new HashMap<>(2);
                csMap.put("wechat", serviceWx);
                csMap.put("pic", serviceWxPic);
                csMapList.add(csMap);
                activityDef.setServiceWx(JSONArray.toJSONString(csMapList));
            }
        }
        activityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        activityDef.setDel(DelEnum.NO.getCode());
        return activityDef;
    }

    /**
     * 判断此商品id，是否有进行中的活动
     * @param shopId
     * @param goodsId
     * @return 有则返回true，没有则返回FALSE
     */
    @Override
    public boolean hasRunningActivity(Long shopId, Long goodsId) {
        // 检查activity表  计划类，免单商品抽奖，新人有礼
        Date now = new Date();
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        param.put("startTimeAfterNow", now);
        // 活动已发布，未开始
        List<Activity> activities = activityService.findPage(param);
        // 清除参数
        param.clear();
        // 活动已开始，待开奖，未结束
        param.put("shopId", shopId);
        param.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        param.put("startBeforNow", now);
        param.put("endTimeAfterNow", now);
        param.put("awardStartTimeIsNull", 0);
        List<Activity> activities1 = activityService.findPage(param);
        // 清除参数
        param.clear();
        // 活动已开始，已开奖，未结束
        param.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        param.put("awardStartTimeIsNotNull", 1);
        List<Activity> activities2 = activityService.findPage(param);
        activities.addAll(activities1);
        activities.addAll(activities2);
        if(CollectionUtils.isEmpty(activities)){
            return false;
        }
        for (Activity activity : activities) {
            if(activity.getGoodsId() == null){
                continue;
            }
            if (activity.getGoodsId().equals(goodsId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public SimpleResponse delOrSuspend(String activityDefId, String del, String status) {
        SimpleResponse response = new SimpleResponse();
        if (StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        Long activityDefID = Long.valueOf(activityDefId);
        // 回收 成长任务-收藏加购 库存
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefID);

        if (StringUtils.isNotBlank(del) && Integer.valueOf(del) == DelEnum.YES.getCode()) {
            Integer rows = activityDefService.deleteActivityDef(activityDefID);
            if (rows == 0) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                return response;
            }

            // 轮盘删除
            if (activityDef.getType() == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
                activityService.deleteActivityByActivityDefId(activityDef.getId());
            }

            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }


        if (StringUtils.isNotBlank(status) && Integer.valueOf(status) == ActivityDefStatusEnum.CLOSE.getCode()) {
            Integer rows = activityDefService.suspendActivityDef(activityDefID);
            if (rows == 0) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                return response;
            }


            // 轮盘下线
            if (activityDef.getType() == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
                activityService.updateByActivityDefIdStatus(activityDef.getId(), ActivityStatusEnum.DEL.getCode());
            }
            Map<String, Object> param = new HashMap<>();
            param.put("activityDefId", activityDefId);
            List<Activity> activities = activityService.findPage(param);
            for (Activity activity : activities) {
                List<UserTask> userTasks = userTaskService.selectByActivityId(activity.getId());
                for (UserTask userTask : userTasks) {
                    if (userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()
                            && userTask.getStatus() != UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                        Integer activityDefRows = activityDefService.addRedpackStock(activityDefID, activityDef.getTaskRpAmount());
                        if (activityDefRows == 0) {
                            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                            response.setMessage("数据库异常：成长任务-收藏加购 库存 失败");
                            return response;
                        }
                    }
                }
            }
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
        return response;
    }

    @Override
    public SimpleResponse queryActivitySituation(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验参数
        if (StringUtils.isBlank(activityDefId) || !RegexUtils.StringIsNumber(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("activityDefId 请求参数未设置");
            return response;
        }
        Long aDefId = Long.valueOf(activityDefId);
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(aDefId);
        // 校验数据
        if (activityDef == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("activityDef表 获取活动失败");
            return response;
        }
        // 开始 获取所有参与此活动的用户

        Integer type = activityDef.getType();
        Long merchantId = activityDef.getMerchantId();
        Long shopId = activityDef.getShopId();
        Integer status = activityDef.getStatus();

        // 当类型为新人有礼 或者 免单抽奖时 处理 1个defId对多个aid
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()
                || type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            Map<String, Object> param = new HashMap<>();
            param.put("activityDefId", aDefId);
            List<Activity> activities = activityService.findPage(param);
            if (CollectionUtils.isEmpty(activities)) {
                List<Map<String, Object>> userVOs = initUserVOs();
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                response.setData(userVOs);
                return response;
            }
            // 获取对应活动id的集合
            List<Long> activityIds = new ArrayList<>();
            for (Activity activity : activities) {
                activityIds.add(activity.getId());
            }
            // 通过aids查询 user_activity表 取得所有用户id
            Map<String, Object> uActivityParam = new HashMap<>();
            uActivityParam.put("activityIds", activityIds);
            List<UserActivity> userActivities = userActivityService.pageListUserActivity(uActivityParam);
            // 所有用户id的集合
            List<Long> userIdsAll = new ArrayList<>();
            // 获取所有中奖用户的集合
            List<Long> userIdsWinner = new ArrayList<>();
            // 获取所有优惠券用户的集合
            List<Long> userIdsCoupon = new ArrayList<>();
            if (CollectionUtils.isEmpty(userActivities)) {
                List<Map<String, Object>> userVOs = initUserVOs();
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                response.setData(userVOs);
                return response;
            }
            for (UserActivity userActivity : userActivities) {
                Long uid = userActivity.getUserId();
                userIdsAll.add(uid);
                if (userActivity.getStatus() == UserActivityStatusEnum.WINNER.getCode()) {
                    userIdsWinner.add(uid);
                }
                if (userActivity.getStatus() == UserActivityStatusEnum.COUPON.getCode()) {
                    userIdsCoupon.add(uid);
                }
            }
            // 通过aids查询 user_task表 获取所有用户id
            Map<String, Object> uTaskParam = new HashMap<>();
            uTaskParam.put("activityIds", activityIds);
            List<UserTask> userTasks = userTaskService.listPageByParams(uTaskParam);
            // 获取 收藏加购任务 的用户集合
            List<Long> userIdsCart = new ArrayList<>();
            for (UserTask userTask : userTasks) {
                if (userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                    userIdsCart.add(userTask.getUserId());
                }
            }
            // 获取此任务的所有用户信息
            List<User> users = userService.listPageByParams(userIdsAll);
            // 加工数据

            List<Map<String, Object>> userVOs = new ArrayList<>();
            for (User user : users) {
                Map<String, Object> userVO = new HashMap<>();
                userVO.put("avatarUrl", user.getAvatarUrl());
                userVO.put("nickName", user.getNickName());
                userVO.put("mobile", user.getMobile());
                userVO.put("status", "未中奖");
                userVO.put("level", 4);
                if (userIdsWinner.contains(user.getId())) {
                    userVO.put("status", "中奖");
                    userVO.put("level", 1);
                }
                if (userIdsCart.contains(user.getId())) {
                    userVO.put("status", "成长任务红包");
                    userVO.put("level", 2);
                }
                if (userIdsCoupon.contains(user.getId())) {
                    userVO.put("status", "优惠券");
                    userVO.put("level", 3);
                }
                userVOs.add(userVO);
            }
            // 排序：中奖，成长任务红包，优惠券，未中奖
            Object[] mapArr = userVOs.toArray();
            for(int i = 0; i < mapArr.length - 1; i++) {//外层循环控制排序趟数
                for(int j = 0; j < mapArr.length - 1 - i; j++) {//内层循环控制每一趟排序多少次
                    Map map1 = (Map)mapArr[j];
                    Map map2 = (Map)mapArr[j+1];
                    if((int)map1.get("level") > (int)map2.get("level")){
                        Map temp = (Map)mapArr[j];
                        mapArr[j] = mapArr[j+1];
                        mapArr[j+1] = temp;
                    }
                }
            }
            List resultVOs = CollectionUtils.arrayToList(mapArr);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVOs);
            return response;
        }

        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode() || type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
            // 通过 mercahntId 、 shopId 、 activityId 、type 、 status(1已领取)查询 userCoupon 表  获取 userid的集合 以及 中奖金额
            // TODO 活动列表 参与人数
            // 条件 userId的集合  查询 user表 获取 用户的 avatarUrl，nickName，mobile， 加工信息 + status + level
            // 加工信息 用于前端展示

            // 获取对应活动id的集合

            List<Map<String, Object>> userVOs = new ArrayList<>();

            // 获取 新人大转盘 或 回填订单红包 的 参与人数
            List<Long> userIds = new ArrayList<>();
            Map<String, Object> userCouponParams = new HashMap<>();
            userCouponParams.put("merchantId", merchantId);
            userCouponParams.put("shopId", shopId);

            if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
                userCouponParams.put("type", UserCouponTypeEnum.LUCKY_WHEEL.getCode());
                Map<String, Object> param = new HashMap<>();
                param.put("activityDefId", aDefId);
                List<Activity> activities = activityService.findPage(param);
                if (CollectionUtils.isEmpty(activities)) {
                    userVOs = initUserVOs();
                    response.setStatusEnum(ApiStatusEnum.SUCCESS);
                    response.setData(userVOs);
                    return response;
                }
                List<Long> activityIds = new ArrayList<>();
                for (Activity activity : activities) {
                    activityIds.add(activity.getId());
                }
                userCouponParams.put("activityIds", activityIds);
            }
            if (type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
                userCouponParams.put("type", UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
                userCouponParams.put("activityDefId", aDefId);
            }
            userCouponParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
            List<UserCoupon> userCoupons = userCouponService.listPageByparams(userCouponParams);
            if (CollectionUtils.isEmpty(userCoupons)) {
                userVOs = initUserVOs();
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                response.setData(userVOs);
                return response;
            }
            for (UserCoupon userCoupon: userCoupons) {
                // 有重复id 的情况，过滤
                if (userIds.contains(userCoupon.getUserId())) {
                    continue;
                }
                userIds.add(userCoupon.getUserId());
            }
            List<User> users = userService.listPageByParams(userIds);
            // 进行加工
            for (UserCoupon userCoupon : userCoupons) {
                Map<String, Object> userVO = new HashMap<>();
                Long userId = userCoupon.getUserId();
                BigDecimal amount = userCoupon.getAmount();
                userVO.put("status", "已领取￥"+ amount);
                userVO.put("level", amount.multiply(new BigDecimal(100)).intValue());
                for (User user: users) {
                    if (userId.equals(user.getId())) {
                        userVO.put("avatarUrl", user.getAvatarUrl());
                        userVO.put("nickName", user.getNickName());
                        userVO.put("mobile", user.getMobile());
                        break;
                    }
                }
                userVOs.add(userVO);
            }

            // 排序：中奖，成长任务红包，优惠券，未中奖
            Object[] mapArr = userVOs.toArray();
            for(int i = 0; i < mapArr.length - 1; i++) {//外层循环控制排序趟数
                for(int j = mapArr.length - 1; j > i; j--) {//内层循环控制每一趟排序多少次
                    Map map1 = (Map)mapArr[j];
                    Map map2 = (Map)mapArr[j-1];
                    if((int)map1.get("level") > (int)map2.get("level")){
                        Map temp = (Map)mapArr[j];
                        mapArr[j] = mapArr[j-1];
                        mapArr[j-1] = temp;
                    }
                }
            }
            List resultVOs = CollectionUtils.arrayToList(mapArr);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVOs);
            return response;
        }

        response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
        response.setMessage("该活动的类型不匹配");
        return response;
    }

    private List<Map<String, Object>> initUserVOs() {
        List<Map<String, Object>> userVOs = new ArrayList<>();
        Map<String, Object> userVO = new HashMap<>();
        userVO.put("avatarUrl", "");
        userVO.put("nickName", "");
        userVO.put("mobile", "");
        userVO.put("status", "");
        userVO.put("level", "");
        userVOs.add(userVO);
        return userVOs;
    }

    private ActivityDefReq newMan2Assist(ActivityDefReq activityDefReq) {
        if (Integer.parseInt(activityDefReq.getType()) == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            activityDefReq.setType(ActivityDefTypeEnum.ASSIST.getCode()+"");
            activityDefReq.setHitsPerDraw(0+"");
        }
        return activityDefReq;
    }

}
