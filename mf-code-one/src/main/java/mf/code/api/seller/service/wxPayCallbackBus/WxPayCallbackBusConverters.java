package mf.code.api.seller.service.wxPayCallbackBus;

import mf.code.api.seller.service.wxPayCallbackBus.impl.*;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.service.wxPayCallbackBus
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 12:00
 */
@Service
public class WxPayCallbackBusConverters {
    @Autowired
    private List<WxPayCallbackBusConverter> wxPayCallbackBusConverters;

    static Map<Integer, WxPayCallbackBusConverter> registerService = new HashMap<>();

    static {
        registerService.put(BizTypeEnum.ACTIVITY_AUDIT.getCode(), new ActivityDefAuditCallbackConverter());
        registerService.put(BizTypeEnum.AD.getCode(), new AdCallbackConverter());
        registerService.put(-1, new CreateDefCallbackConverter());
        registerService.put(BizTypeEnum.FENDUODUO_PROXY.getCode(), new FenduoduoProxyCallbackConverter());
        registerService.put(BizTypeEnum.PURCHASE_PUBLIC.getCode(), new PubBuyCourseCallbackConverter());
        registerService.put(BizTypeEnum.PUBLIC_BUY_COURSE.getCode(), new PurchasePublicCallbackConverter());
    }

    public String convert(MerchantOrder merchantOrder) {
        if (!CollectionUtils.isEmpty(this.wxPayCallbackBusConverters)) {
            for (WxPayCallbackBusConverter converter : this.wxPayCallbackBusConverters) {
                String str = converter.convert(merchantOrder);
                if (StringUtils.isNotBlank(str)) {
                    break;
                }
            }
        }
        return "";
    }

    public void getService(Integer name, MerchantOrder merchantOrder) {
        WxPayCallbackBusConverter converterService = registerService.get(name);
        if (converterService == null) {
            return;
        }
        converterService.convert(merchantOrder);
    }

//    public void convert(MerchantOrder merchantOrder) {
//        WxPayCallbackBusConverters factory = new WxPayCallbackBusConverters();
//        if (BizTypeEnum.isCreateDefMerchantOrder(merchantOrder.getBizType())) {
//            factory.getService(-1, merchantOrder);
//        } else {
//            factory.getService(merchantOrder.getBizType(), merchantOrder);
//        }
//    }
}
