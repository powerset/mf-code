package mf.code.api.seller.service;

import mf.code.api.seller.dto.UserTaskAuditReq;
import mf.code.api.seller.dto.UserTaskV2AuditReq;
import mf.code.api.seller.dto.UserTaskV3AuditReq;
import mf.code.common.simpleresp.SimpleResponse;

import java.util.Map;

/**
 * 商户关于任务的操作
 *
 * @author gel
 */
public interface SellerActivityTaskService {

    /**
     * 商户审核用户任务
     *
     * @return
     */
    SimpleResponse auditUserTask(UserTaskAuditReq req);

    /***
     * v2版本审核用户任务
     * @param req
     * @return
     */
    SimpleResponse auditUserTaskV2(UserTaskV2AuditReq req);

    /***
     * v3版本审核用户任务
     * @param req
     * @return
     */
    SimpleResponse auditUserTaskV3(UserTaskV3AuditReq req);

    /**
     * 分页查询用户任务
     * @param params
     * @param page
     * @param size
     * @return
     */
    SimpleResponse pageListUserTask(Map<String, Object> params, Integer page, Integer size);

    /**
     * 分页查询审核任务列表
     * @param params
     * @param page
     * @param size
     * @return
     */
    SimpleResponse taskAuditList(Map<String, Object> params, Integer page, Integer size);


}
