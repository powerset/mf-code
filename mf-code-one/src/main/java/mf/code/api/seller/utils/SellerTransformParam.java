package mf.code.api.seller.utils;

import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.utils.BaseValidator;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * mf.code.api.seller.constant
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-29 下午9:09
 */
public class SellerTransformParam extends BaseValidator {
    /**
     * type 赠品
     */
    public static final String GIFT_TYPE = "1";
    /**
     * type 幸运大转盘
     */
    public static final String LUCKY_WHEEL_TYPE = "2";
    /**
     * type 红包活动
     */
    public static final String RED_PACKET_TYPE = "3";
    /**
     * type 免单商品
     */
    public static final String FREE_GOODS_TYPE = "4";
    /**
     * type 拆红包
     */
    public static final String OPEN_RED_PACKET_TYPE = "5";

    /**
     * status 待支付
     */
    public static final String PAY_WAIT_STATUS = "1";
    /**
     * status 进行中
     */
    public static final String PUBLISHED_STATUS = "2";
    /**
     * status 已结束
     */
    private static final String END_STATUS = "3";
    /**
     * status 库存警告
     */
    public static final String STOCK_WARN_STATUS = "4";
    /**
     * status 已结算
     */
    public static final String STOCK_REFUND_STATUS = "5";

    /**
     * operation 删除
     */
    public static final String DEL = "1";
    /**
     * operation 挂起
     */
    private static final String SUSPEND = "2";
    /**
     * operation 启动
     */
    private static final String RESUME = "3";


    public static final String ORDER_RPTYPE = "1";
    public static final String FAVCART_RPTYPE = "2";
    public static final String GOOD_COMMENT_RPTYPE = "3";


    public static List<Integer> VO2PO4ActivityDefType(String type) {

        if (StringUtils.isBlank(type) || !RegexUtils.StringIsNumber(type)) {
            return defaultTypeList();
        }
        if (StringUtils.equals(GIFT_TYPE, type)) {
            return giftTypeList();
        }
        if (StringUtils.equals(LUCKY_WHEEL_TYPE, type)) {
            return luckyWheelTypeList();
        }
        if (StringUtils.equals(RED_PACKET_TYPE, type)) {
            return redPacketTypeList();
        }
        if (StringUtils.equals(FREE_GOODS_TYPE, type)) {
            return freeGoodsTypeList();
        }
        if (StringUtils.equals(OPEN_RED_PACKET_TYPE, type)) {
            return openRedPacketTypeList();
        }

        return null;
    }

    public static List<Integer> VO2PO4ActivityDefStatus(String status) {

        if (StringUtils.isBlank(status) || !RegexUtils.StringIsNumber(status)) {
            return defaultStatusList();
        }
        if (StringUtils.equals(PAY_WAIT_STATUS, status)) {
            return payWaitStatusList();
        }
        if (StringUtils.equals(PUBLISHED_STATUS, status)) {
            return publishedStatusList();
        }
        if (StringUtils.equals(END_STATUS, status)) {
            return endStatusList();
        }
        if (StringUtils.equals(STOCK_WARN_STATUS, status)) {
            return stockWarnStatusList();
        }
        if (StringUtils.equals(STOCK_REFUND_STATUS, status)) {
            return Arrays.asList(ActivityDefStatusEnum.REFUNDED.getCode());
        }

        return null;
    }

    public static String PO2VO4ActivityDefStatus(Integer status) {
        if (payWaitStatusList().contains(status)) {
            return PAY_WAIT_STATUS;
        }
        if (publishedStatusList().contains(status)) {
            return PUBLISHED_STATUS;
        }
        if (endStatusList().contains(status)) {
            return END_STATUS;
        }
        if (refundStatusList().contains(status)) {
            return STOCK_REFUND_STATUS;
        }
        return status.toString();
    }

    public static String PO2VOActivityDefType(Integer type) {

        if (giftTypeList().contains(type)) {
            return GIFT_TYPE;
        }
        if (luckyWheelTypeList().contains(type)) {
            return LUCKY_WHEEL_TYPE;
        }
        if (redPacketTypeList().contains(type)) {
            return RED_PACKET_TYPE;
        }
        if (freeGoodsTypeList().contains(type)) {
            return FREE_GOODS_TYPE;
        }
        if (openRedPacketTypeList().contains(type)) {
            return OPEN_RED_PACKET_TYPE;
        }
        return "未知类型";
    }

    private static List<Integer> freeGoodsTypeList() {
        List<Integer> freeGoodsTypeList = new ArrayList<>();
        freeGoodsTypeList.add(ActivityDefTypeEnum.ORDER_BACK.getCode());
        freeGoodsTypeList.add(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
        return freeGoodsTypeList;
    }

    private static List<Integer> redPacketTypeList() {
        List<Integer> redPacketTypeList = new ArrayList<>();
        redPacketTypeList.add(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
        redPacketTypeList.add(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode());
        redPacketTypeList.add(ActivityDefTypeEnum.RED_PACKET.getCode());
        return redPacketTypeList;
    }

    private static List<Integer> luckyWheelTypeList() {
        List<Integer> luckyWheelTypeList = new ArrayList<>();
        luckyWheelTypeList.add(ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
        return luckyWheelTypeList;
    }

    private static List<Integer> giftTypeList() {
        List<Integer> giftTypeList = new ArrayList<>();
        giftTypeList.add(ActivityDefTypeEnum.NEW_MAN.getCode());
        giftTypeList.add(ActivityDefTypeEnum.ASSIST.getCode());
        giftTypeList.add(ActivityDefTypeEnum.ASSIST_V2.getCode());
        return giftTypeList;
    }

    private static List<Integer> openRedPacketTypeList() {
        List<Integer> openRedPacketTypeList = new ArrayList<>();
        openRedPacketTypeList.add(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        return openRedPacketTypeList;
    }

    private static List<Integer> defaultTypeList() {
        List<Integer> defaultTypeList = new ArrayList<>();
        defaultTypeList.add(ActivityDefTypeEnum.ORDER_BACK.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.NEW_MAN.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.ASSIST.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.ASSIST_V2.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.RED_PACKET.getCode());
        defaultTypeList.add(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        return defaultTypeList;
    }

    private static List<Integer> defaultStatusList() {
        List<Integer> defaultStatusList = new ArrayList<>();
        defaultStatusList.add(ActivityDefStatusEnum.CLOSE.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.AUDIT_ERROR.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.SAVE.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.AUDIT_WAIT.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.AUDIT_SUCCESS.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.PUBLISHED.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.END.getCode());
        defaultStatusList.add(ActivityDefStatusEnum.REFUNDED.getCode());
        return defaultStatusList;
    }

    private static List<Integer> payWaitStatusList() {
        List<Integer> payWaitStatusList = new ArrayList<>();
        payWaitStatusList.add(ActivityDefStatusEnum.SAVE.getCode());
        payWaitStatusList.add(ActivityDefStatusEnum.AUDIT_WAIT.getCode());
        return payWaitStatusList;
    }

    private static List<Integer> stockWarnStatusList() {
        List<Integer> stockWarnStatusList = new ArrayList<>();
        stockWarnStatusList.add(ActivityDefStatusEnum.PUBLISHED.getCode());
        return stockWarnStatusList;
    }

    private static List<Integer> endStatusList() {
        List<Integer> endStatusList = new ArrayList<>();
        endStatusList.add(ActivityDefStatusEnum.CLOSE.getCode());
        endStatusList.add(ActivityDefStatusEnum.AUDIT_ERROR.getCode());
        endStatusList.add(ActivityDefStatusEnum.AUDIT_SUCCESS.getCode());
        endStatusList.add(ActivityDefStatusEnum.END.getCode());
        return endStatusList;
    }

    private static List<Integer> refundStatusList() {
        List<Integer> refundStatusList = new ArrayList<>();
        refundStatusList.add(ActivityDefStatusEnum.REFUNDED.getCode());
        return refundStatusList;
    }

    private static List<Integer> publishedStatusList() {
        List<Integer> publishStatusList = new ArrayList<>();
        publishStatusList.add(ActivityDefStatusEnum.PUBLISHED.getCode());
        return publishStatusList;
    }

    public static Integer getActivityDefStatusByOperation(String operation) {
        if (StringUtils.equals(SUSPEND, operation)) {
            return ActivityDefStatusEnum.CLOSE.getCode();
        }
        if (StringUtils.equals(RESUME, operation)) {
            return ActivityDefStatusEnum.PUBLISHED.getCode();
        }
        return null;
    }
}
