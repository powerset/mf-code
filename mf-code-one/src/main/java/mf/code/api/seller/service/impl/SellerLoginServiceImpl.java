package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.dto.LoginReq;
import mf.code.api.seller.dto.RegisterReq;
import mf.code.api.seller.dto.SmsCodeRedis;
import mf.code.api.seller.dto.SmsReq;
import mf.code.api.seller.service.SellerLoginService;
import mf.code.common.caller.aliyundayu.DayuCaller;
import mf.code.common.email.EmailService;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.service.MerchantService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.seller.service
 * Description:
 *
 * @author: gel
 * @date: 2018-10-25 19:37
 */
@Slf4j
@Service
public class SellerLoginServiceImpl implements SellerLoginService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DayuCaller dayuCaller;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private EmailService emailService;

    @Value("${seller.smsCode.debug.mode}")
    private Integer smsCodeDebugMode;
    @Value("${seller.uid.name}")
    private String uidName;
    @Value("${seller.token.name}")
    private String tokenName;

    @Override
    public SimpleResponse<Object> getSmsVerifyCodeForRegister(SmsReq smsReq, String pictureCode) {
        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();
        // 校验 图形验证码，用户会话中的图形验证码 存在&匹配
        if (null == pictureCode) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("图形验证码已过期");
            return simpleResponse;
        }
        if (!StringUtils.equalsIgnoreCase(pictureCode, smsReq.getPicCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("图形验证码不匹配");
            return simpleResponse;
        }
        // redisKey
        String redisKey = RedisKeyConstant.MERCHANT_REGISTER + smsReq.getPhone();
        // 判断 手机号码 是否60秒之内重复发送
        String redisString = stringRedisTemplate.opsForValue().get(redisKey);
        SmsCodeRedis smsRedisRegister = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (smsRedisRegister != null && DateUtil.getCurrentMillis() < Long.valueOf(smsRedisRegister.getNow()) + DateUtil.ONE_MINUTE_MILLIS) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("稍后再次请求短信验证码");
            return simpleResponse;
        }
        // 判断 手机号码 是否已经注册
        Merchant merchantByPhone = merchantService.getMerchantByPhone(smsReq.getPhone());
        if (merchantByPhone != null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("已经注册过");
            return simpleResponse;
        }
        // 测试模式：不校验短信验证码
        if (smsCodeDebugMode != null && smsCodeDebugMode == 1) {
            SmsCodeRedis register = new SmsCodeRedis();
            register.setNow(DateUtil.getCurrentMillis().toString());
            register.setSmsCode("111111");
            stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 2, TimeUnit.MINUTES);
            return simpleResponse;
        }
        // 生成短信验证码，6位
        String smsCode = RandomStrUtil.getRandomNum();
        // 短信验证码存入redis，过期时间2分钟key-->mch:reg:<phone> value-->smsCode, now
        SmsCodeRedis register = new SmsCodeRedis();
        register.setNow(DateUtil.getCurrentMillis().toString());
        register.setSmsCode(smsCode);
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 2, TimeUnit.MINUTES);
        // 调用大于短信接口(手机号，短信验证码)
        SendSmsResponse sendSmsResponse = dayuCaller.smsForRegister(smsReq.getPhone(), smsCode);
        // 处理大于短信返回状态
        if (!StringUtils.equalsIgnoreCase("ok", sendSmsResponse.getCode())) {
            log.error("请求阿里大于失败, phone" + smsReq.getPhone());
            // 短信验证码调用失败处理
            emailService.sendSimpleMail("请求阿里大于失败, phone" + smsReq.getPhone(), JSON.toJSONString(sendSmsResponse));
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("短信服务请求失败");
            return simpleResponse;
        }
        return simpleResponse;
    }


    @Override
    public SimpleResponse<Object> getSmsVerifyCodeForLogin(SmsReq smsReq, String pictureCode) {
        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();
        // 校验图形验证码，用户会话中的图形验证码 存在&匹配
        if (null == pictureCode) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("图形验证码已过期");
            return simpleResponse;
        }
        if (!StringUtils.equalsIgnoreCase(pictureCode, smsReq.getPicCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("图形验证码不匹配");
            return simpleResponse;
        }
        if (!RegexUtils.isMobileExact(smsReq.getPhone())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("手机号输入不正确");
            return simpleResponse;
        }
        // redisKey
        String redisKey = RedisKeyConstant.MERCHANT_LOGIN + smsReq.getPhone();
        // 判断 手机号码 是否60秒之内重复发送
        String redisString = stringRedisTemplate.opsForValue().get(redisKey);
        SmsCodeRedis smsRedisRegister = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (smsRedisRegister != null && DateUtil.getCurrentMillis() < Long.valueOf(smsRedisRegister.getNow()) + DateUtil.ONE_MINUTE_MILLIS) {
            // 当前时间在预定时间之前，返回错误提示
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("稍后再次请求短信验证码");
            return simpleResponse;
        }
        // 判断 手机号码 是否已经注册
        Merchant merchantByPhone = merchantService.getMerchantByPhone(smsReq.getPhone());
        if (merchantByPhone == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("请先注册");
            return simpleResponse;
        }
        // 测试模式：不校验短信验证码
        if (smsCodeDebugMode != null && smsCodeDebugMode == 1) {
            SmsCodeRedis register = new SmsCodeRedis();
            register.setNow(DateUtil.getCurrentMillis().toString());
            register.setSmsCode("111111");
            stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(register), 2, TimeUnit.MINUTES);
            return simpleResponse;
        }
        // 生成短信验证码，6位
        String smsCode = RandomStrUtil.getRandomNum();
        // 短信验证码存入redis，过期时间2分钟key-->mch:reg:<phone> value-->smsCode, now
        SmsCodeRedis login = new SmsCodeRedis();
        login.setNow(DateUtil.getCurrentMillis().toString());
        login.setSmsCode(smsCode);
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(login), 2, TimeUnit.MINUTES);
        // 调用大于短信接口(手机号，短信验证码)
        SendSmsResponse sendSmsResponse = dayuCaller.smsForLogin(smsReq.getPhone(), smsCode);
        // 处理大于短信返回状态
        if (!StringUtils.equalsIgnoreCase("ok", sendSmsResponse.getCode())) {
            log.error("请求阿里大于失败, phone" + smsReq.getPhone());
            emailService.sendSimpleMail("请求阿里大于失败, phone" + smsReq.getPhone(), JSON.toJSONString(sendSmsResponse));
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("短信服务请求失败");
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    public SimpleResponse<Object> register(RegisterReq registerReq, HttpServletRequest request, HttpServletResponse response) {
        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();
        if (registerReq.getPhone() == null || !RegexUtils.isMobileExact(registerReq.getPhone())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("用户手机输入错误");
            return simpleResponse;
        }
        // 查询redis，短信验证码不存在
        if (registerReq.getSmsCode() == null || StringUtils.isBlank(registerReq.getSmsCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("用户短信验证码输入错误");
            return simpleResponse;
        }
        String redisString = stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANT_REGISTER + registerReq.getPhone());
        if (redisString == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("短信验证码失效");
            return simpleResponse;
        }
        // 校验商户是否注册过
        Merchant merchant1 = merchantService.getMerchantByPhone(registerReq.getPhone());
        if (merchant1 != null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("商户已注册过");
            return simpleResponse;
        }
        // 校验用户输入短信验证码与redis存储不符
        SmsCodeRedis smsRegisterRedis = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (!StringUtils.equalsIgnoreCase(registerReq.getSmsCode(), smsRegisterRedis.getSmsCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("短信验证码输入有误");
            return simpleResponse;
        }
        // 创建 商户
        // TODO 密码 手机号码后4位+一次MD5结果进行二次MD5
        Date now = new Date(System.currentTimeMillis() / 1000 * 1000);

        log.info(registerReq.getPhone() + "商户注册时间：" + now.getTime());
        Merchant merchant = new Merchant();
        merchant.setPhone(registerReq.getPhone());
        // 第三方接口调用状态：0: 创建未调用 1：调用成功 -1：创建调用失败 -2：欠费调用失败
        merchant.setStatus(0);
        merchant.setCtime(now);
        merchant.setUtime(now);
        int size = merchantService.createMerchant(merchant);
        request.getSession().setAttribute(uidName, merchant.getId());
        request.getSession().setMaxInactiveInterval(8 * 60 * 60);
        // 生成token,存入cookie
        merchant = merchantService.getMerchant(merchant.getId());
        String token = TokenUtil.encryptToken(merchant.getId().toString(), merchant.getPhone(), merchant.getCtime(), TokenUtil.SAAS);
        Cookie cookie = new Cookie(tokenName, token);
        cookie.setMaxAge(8 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);
        //redis相关存储
        String firstLoginDialogRedisKey = RedisKeyConstant.MERCHANT_FIRSTLOGIN_DIALOG + merchant.getId();
        this.stringRedisTemplate.opsForValue().set(firstLoginDialogRedisKey, "1", 30, TimeUnit.DAYS);
        Map<String, Object> result = getRegisterAndLoginResult(merchant);
        simpleResponse.setData(result);
        return simpleResponse;
    }

    @Override
    public SimpleResponse<Object> login(LoginReq loginReq, HttpServletRequest request, HttpServletResponse response) {
        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();
        if (StringUtils.isBlank(loginReq.getPhone()) || !RegexUtils.isMobileExact(loginReq.getPhone())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("用户手机输入错误");
            return simpleResponse;
        }
        // 查询redis，短信验证码不存在
        if (StringUtils.isBlank(loginReq.getSmsCode())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("用户短信验证码输入错误");
            return simpleResponse;
        }
        String redisString = stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANT_LOGIN + loginReq.getPhone());
        SmsCodeRedis smsRegisterRedis = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (smsRegisterRedis == null) {
            // TODO 更新 商户 登录失败次数+1
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("短信验证码失效");
            return simpleResponse;
        }
        // 校验用户输入短信验证码与redis存储不符
        if (!StringUtils.equalsIgnoreCase(loginReq.getSmsCode(), smsRegisterRedis.getSmsCode())) {
            // TODO 更新 商户 登录失败次数+1
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("短信验证码输入有误");
            return simpleResponse;
        }
        // 20181217 如果没有密码则添加初始密码
        Merchant merchant = merchantService.getMerchantByPhone(loginReq.getPhone());
        if (StringUtils.isBlank(merchant.getPassword())) {
            log.info(">>>>>>>>>>>>>>>>>商户密码设置,phone:" + loginReq.getPhone() + ",密码：" + smsRegisterRedis.getSmsCode());
            String password = MD5Util.md5(MD5Util.md5(smsRegisterRedis.getSmsCode()) + merchant.getPhone().substring(7));
            merchantService.updateMerchantPassword(merchant.getId(), password);
            // 发送初始化密码通知
            dayuCaller.smsForInitPassword(loginReq.getPhone(), smsRegisterRedis.getSmsCode());
        }
        request.getSession().setAttribute(uidName, merchant.getId());
        request.getSession().setMaxInactiveInterval(8 * 60 * 60);
        String token = TokenUtil.encryptToken(merchant.getId().toString(), merchant.getPhone(), merchant.getCtime(), TokenUtil.SAAS);
        Cookie cookie = new Cookie(tokenName, token);
        cookie.setMaxAge(8 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);
        Map<String, Object> result = getRegisterAndLoginResult(merchant);
        simpleResponse.setData(result);
        return simpleResponse;
    }

    @Override
    public SimpleResponse<Object> loginByPassword(LoginReq loginReq, HttpServletRequest request, HttpServletResponse response) {

        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();
        if (StringUtils.isBlank(loginReq.getPhone()) || !RegexUtils.isMobileExact(loginReq.getPhone())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("手机输入错误");
            return simpleResponse;
        }
        if (StringUtils.isBlank(loginReq.getPassword())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("密码不能为空");
            return simpleResponse;
        }
        Merchant merchant = merchantService.getMerchantByPhone(loginReq.getPhone());
        if (merchant == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请先注册");
            return simpleResponse;
        }
        String md5Client = MD5Util.md5(loginReq.getPassword() + loginReq.getPhone().substring(7));
        if (!StringUtils.equals(md5Client, merchant.getPassword())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("密码输入错误");
            return simpleResponse;
        }
        request.getSession().setAttribute(uidName, merchant.getId());
        request.getSession().setMaxInactiveInterval(8 * 60 * 60);
        String token = TokenUtil.encryptToken(merchant.getId().toString(), merchant.getPhone(), merchant.getCtime(), TokenUtil.SAAS);
        Cookie cookie = new Cookie(tokenName, token);
        cookie.setMaxAge(8 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);
        Map<String, Object> result = getRegisterAndLoginResult(merchant);
        simpleResponse.setData(result);
        return simpleResponse;
    }

    /**
     * 组装登录和注册返回数据
     *
     * @param merchant
     * @return
     */
    private Map<String, Object> getRegisterAndLoginResult(Merchant merchant) {
        Map<String, Object> result = new HashMap<>();
        result.put("merchantId", merchant.getId());
        result.put("username", merchant.getPhone());
        result.put("name", merchant.getName());
        result.put("type", merchant.getType());
        result.put("firstDialog", false);
        //查询是否弹窗
        String firstLoginDialogRedisKey = RedisKeyConstant.MERCHANT_FIRSTLOGIN_DIALOG + merchant.getId();
        String firstLogin = this.stringRedisTemplate.opsForValue().get(firstLoginDialogRedisKey);
        if (StringUtils.isNotBlank(firstLogin)) {
            result.put("firstDialog", true);
            this.stringRedisTemplate.delete(firstLoginDialogRedisKey);
        }
        return result;
    }
}
