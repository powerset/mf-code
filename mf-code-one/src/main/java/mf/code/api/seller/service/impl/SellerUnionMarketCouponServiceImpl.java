package mf.code.api.seller.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.sd4324530.fastweixin.util.CollectionUtil;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.api.seller.service.SellerUnionMarketCouponService;
import mf.code.common.constant.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.ProductEntity;
import mf.code.merchant.repo.dao.MerchantShopCouponMapper;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.one.constant.MerchantShopCouponStatusEnum;
import mf.code.one.constant.MerchantShopCouponTypeEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.one.dto.SellerMerchantShopCouponDTO;
import mf.code.one.dto.SellerShopCouponDTO;
import mf.code.one.dto.UserReceivedCouponDTO;
import mf.code.ucoupon.repo.dao.UserCouponMapper;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 商户端-联合营销-优惠券  impl
 * author:yunshan
 */
@Service
public class SellerUnionMarketCouponServiceImpl implements SellerUnionMarketCouponService {

    @Autowired
    private MerchantShopCouponMapper merchantShopCouponMapper;
    @Autowired
    private UserCouponMapper userCouponMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private GoodsAppService goodsAppService;

    @Override
    public List<SellerMerchantShopCouponDTO> queryAllCoupons(Long shopId, Long page, Long size) {

        QueryWrapper<MerchantShopCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantShopCoupon::getShopId, shopId)
                .eq(MerchantShopCoupon::getType, MerchantShopCouponTypeEnum.GOODS.getCode())
                .eq(MerchantShopCoupon::getDel, DelEnum.NO.getCode());

        Page<MerchantShopCoupon> iPage = new Page<>(page, size);
        IPage<MerchantShopCoupon> upayMerchantShopCouponIPage = merchantShopCouponMapper.selectPage(iPage, wrapper);
        if (upayMerchantShopCouponIPage == null) {
            return null;
        }
        List<MerchantShopCoupon> records = upayMerchantShopCouponIPage.getRecords();
        if (CollectionUtil.isEmpty(records)) {
            return null;
        }
        List<SellerMerchantShopCouponDTO> merchantShopCouponDTOList = new ArrayList<>();

        // 拼接返回参数
        for (MerchantShopCoupon m : records) {
            int userScope = 1;
            int goodsScope = 1;
            SellerMerchantShopCouponDTO dto = new SellerMerchantShopCouponDTO();
            MerchantShopCouponDTO merchantShopCouponDTO = new MerchantShopCouponDTO();
            BeanUtils.copyProperties(m, merchantShopCouponDTO);
            List<ProductEntity> productEntityList = goodsAppService.listByIds(Arrays.asList(m.getProductId()));
            if (!CollectionUtils.isEmpty(productEntityList)) {
                merchantShopCouponDTO.setProductTitle(productEntityList.get(0).getProductTitle());
            }
            merchantShopCouponDTO.setReceiveNum(merchantShopCouponDTO.getStockDef() - merchantShopCouponDTO.getStock());
            dto.setMerchantShopCoupon(merchantShopCouponDTO);
            dto.setUserScope(userScope);
            dto.setGoodsScope(goodsScope);

            merchantShopCouponDTOList.add(dto);
        }

        return merchantShopCouponDTOList;
    }

    @Override
    public int countAllCoupons(Long shopId) {

        QueryWrapper<MerchantShopCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantShopCoupon::getShopId, shopId)
                .eq(MerchantShopCoupon::getType, MerchantShopCouponTypeEnum.GOODS.getCode())
                .eq(MerchantShopCoupon::getDel, DelEnum.NO.getCode());
        return merchantShopCouponMapper.selectCount(wrapper);
    }

    /**
     * 创建优惠券
     *
     * @param sellerShopCouponDTO
     * @return
     */
    @Override
    public SimpleResponse addNewCoupons(SellerShopCouponDTO sellerShopCouponDTO) {

        SimpleResponse simpleResponse = new SimpleResponse();
        if (sellerShopCouponDTO.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            simpleResponse.setMessage("优惠券金额不能小于零");
            return simpleResponse;
        }
        if (sellerShopCouponDTO.getLimitPerNum() < 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("每人限领张数不能小于1");
            return simpleResponse;
        }
        if (sellerShopCouponDTO.getLimitAmount().compareTo(BigDecimal.ZERO) < 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            simpleResponse.setMessage("使用门槛金额不能小于零");
            return simpleResponse;
        }
        Date startTime = DateUtil.stringtoDate(sellerShopCouponDTO.getDisplayStartTime(), DateUtil.FORMAT_ONE);
        Date endTime = DateUtil.stringtoDate(sellerShopCouponDTO.getDisplayEndTime(), DateUtil.FORMAT_ONE);
        if (startTime.after(endTime)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("起始时间不能晚于结束时间");
            return simpleResponse;
        }
        if (endTime.before(new Date())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("优惠券结束时间填写错误");
            return simpleResponse;
        }
        MerchantShopCoupon merchantShopCoupon = new MerchantShopCoupon();
        BeanUtils.copyProperties(sellerShopCouponDTO, merchantShopCoupon);
        merchantShopCoupon.setName(sellerShopCouponDTO.getName());
        merchantShopCoupon.setType(MerchantShopCouponTypeEnum.GOODS.getCode());
        merchantShopCoupon.setCouponStatus(MerchantShopCouponStatusEnum.PUBLISH.getCode());
        merchantShopCoupon.setDisplayStartTime(startTime);
        merchantShopCoupon.setDisplayEndTime(endTime);
        merchantShopCoupon.setDel(DelEnum.NO.getCode());
        merchantShopCoupon.setCouponStatus(MerchantShopCouponStatusEnum.PUBLISH.getCode());
        merchantShopCoupon.setLimitPerNum(1);
        merchantShopCoupon.setType(MerchantShopCouponTypeEnum.GOODS.getCode());
        merchantShopCoupon.setStockDef(sellerShopCouponDTO.getStockDef());
        merchantShopCoupon.setStock(sellerShopCouponDTO.getStockDef());
        Date now = new Date();
        merchantShopCoupon.setCtime(now);
        merchantShopCoupon.setUtime(now);
        merchantShopCoupon.setDesc("集客优惠券");
        int count = merchantShopCouponMapper.insertSelective(merchantShopCoupon);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO15);
            simpleResponse.setMessage("创建优惠券错误");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 查询领取用户列表情况
     *
     * @param shopId
     * @param couponId
     * @param num
     * @param size
     * @return
     */
    @Override
    public List<UserReceivedCouponDTO> queryUserCouponsList(Long shopId, Long couponId, Long num, Long size) {

        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getCouponId, couponId)
                .eq(UserCoupon::getShopId, shopId);

        Page<UserCoupon> page = new Page<>(num, size);

        // 领取张数，当前是一张，以后需要按照用户分组
        IPage<UserCoupon> upayMerchantShopCouponIPage = userCouponMapper.selectPage(page, wrapper);
        if (upayMerchantShopCouponIPage == null) {
            return null;
        }

        List<UserCoupon> records = upayMerchantShopCouponIPage.getRecords();
        if (CollectionUtil.isEmpty(records)) {
            return null;
        }

        List<UserReceivedCouponDTO> userCouponInfoList = new ArrayList<>();

        for (UserCoupon coupon : records) {
            UserReceivedCouponDTO userCouponInfo = new UserReceivedCouponDTO();
            Long userId = coupon.getUserId();
            User user = userService.selectByPrimaryKey(userId);
            if (null == user) {
                continue;
            }
            String userName = user.getNickName();
            String userPic = user.getAvatarUrl();
            int status = coupon.getStatus();
            Date ctime = coupon.getCtime();

            userCouponInfo.setReceiveNum(1);
            userCouponInfo.setCtime(ctime);
            userCouponInfo.setUserName(userName);
            userCouponInfo.setUserPic(userPic);
            userCouponInfo.setStatus(status);
            userCouponInfo.setUserId(userId);
            userCouponInfo.setCouponId(coupon.getCouponId());

            userCouponInfoList.add(userCouponInfo);
        }

        return userCouponInfoList;
    }

    /**
     * 优惠券已经被领取的数量
     *
     * @param shopId
     * @param couponId
     * @return
     */
    @Override
    public int countCouponsInfo(Long shopId, Long couponId) {

        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getCouponId, couponId)
                .eq(UserCoupon::getShopId, shopId);

        return userCouponMapper.selectCount(wrapper);
    }

    /**
     * 优惠券已经被使用的数量
     *
     * @param shopId
     * @param couponId
     * @return
     */
    @Override
    public int countCouponsInfoUsed(Long shopId, Long couponId) {

        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getCouponId, couponId)
                .eq(UserCoupon::getShopId, shopId)
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.USED.getCode());
        return userCouponMapper.selectCount(wrapper);
    }

    @Override
    public SimpleResponse finishIssued(Long shopId, Long couponId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponMapper.selectByPrimaryKey(couponId);
        if (merchantShopCoupon == null || !merchantShopCoupon.getShopId().equals(shopId)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("您无权修改该优惠券");
            return simpleResponse;
        }
        // 状态不正确直接返回
        if (merchantShopCoupon.getCouponStatus() != MerchantShopCouponStatusEnum.INIT.getCode()
                && merchantShopCoupon.getCouponStatus() != MerchantShopCouponStatusEnum.PUBLISH.getCode()
                && merchantShopCoupon.getCouponStatus() != MerchantShopCouponStatusEnum.UNPUBLISH.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("该优惠券已停止发放");
            return simpleResponse;
        }
        merchantShopCoupon.setCouponStatus(MerchantShopCouponStatusEnum.UNPUBLISH.getCode());
        merchantShopCoupon.setUtime(new Date());
        int i = merchantShopCouponMapper.updateByPrimaryKeySelective(merchantShopCoupon);
        if (i < 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该优惠券发行状态修改失败");
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    public MerchantShopCoupon getCheckedGood(Long couponId) {

        return merchantShopCouponMapper.selectByPrimaryKey(couponId);
    }

    /**
     * 更新优惠券
     *
     * @param coupon
     * @return
     */
    @Override
    public SimpleResponse updateNewCouponsInfo(SellerShopCouponDTO coupon) {
        SimpleResponse simpleResponse = new SimpleResponse();
        //查询被修改的优惠券信息
        MerchantShopCoupon old = getCheckedGood(coupon.getId());
        if (null == old) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("查无此优惠券");
            return simpleResponse;
        }
        if (!old.getShopId().equals(coupon.getShopId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("您无权修改该优惠券");
            return simpleResponse;
        }
        if (old.getCouponStatus() != MerchantShopCouponStatusEnum.INIT.getCode()
                && old.getCouponStatus() != MerchantShopCouponStatusEnum.PUBLISH.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该优惠券已下线，不能修改");
            return simpleResponse;
        }

        //已创建的优惠券，只能修改时间且时间只能延长，其他的不能修改
        Date oldDisplayEndTime = old.getDisplayEndTime();

        //校验修改时间,结束时间只能延长，延长的时间不能小于1小时，且开始时间也只能后延
        Long oldEndTime = oldDisplayEndTime.getTime();
        Long newEndTime = DateUtil.stringtoDate(coupon.getDisplayEndTime(), DateUtil.FORMAT_ONE).getTime();

        Long oldStarTime = old.getDisplayStartTime().getTime();
        Long newStartTime = DateUtil.stringtoDate(coupon.getDisplayStartTime(), DateUtil.FORMAT_ONE).getTime();

        Long time = newEndTime - oldEndTime;

        Long intervalTime = 1000 * 60 * 60L;
        if (time <= intervalTime) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("结束时间延长请设置大于一小时");
            return simpleResponse;
        }

        if (newStartTime < oldStarTime) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("开始时间不能提前");
            return simpleResponse;
        }

        MerchantShopCoupon merchantShopCoupon = new MerchantShopCoupon();
        merchantShopCoupon.setId(coupon.getId());
        merchantShopCoupon.setUtime(new Date());
        merchantShopCoupon.setDisplayStartTime(DateUtil.stringtoDate(coupon.getDisplayStartTime(), DateUtil.FORMAT_ONE));
        merchantShopCoupon.setDisplayEndTime(DateUtil.stringtoDate(coupon.getDisplayEndTime(), DateUtil.FORMAT_ONE));
        int i = merchantShopCouponMapper.updateByPrimaryKeySelective(merchantShopCoupon);
        if (i < 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    public SimpleResponse deleteNewCouponById(Long couponId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        //查询需要被删除的优惠券信息，若不存在则直接返回
        MerchantShopCoupon old = getCheckedGood(couponId);
        if (null == old) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("您无权查看该优惠券");
            return simpleResponse;
        }
        if (old.getCouponStatus() != MerchantShopCouponStatusEnum.UNPUBLISH.getCode()
                && old.getCouponStatus() != MerchantShopCouponStatusEnum.BAN.getCode()
                && old.getCouponStatus() != MerchantShopCouponStatusEnum.EXPIRE.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("该优惠券不能删除，请先下线");
            return simpleResponse;
        }
        MerchantShopCoupon merchantShopCoupon = new MerchantShopCoupon();
        merchantShopCoupon.setId(old.getId());
        merchantShopCoupon.setDel(DelEnum.YES.getCode());
        merchantShopCoupon.setUtime(new Date());
        int i = merchantShopCouponMapper.updateByPrimaryKeySelective(merchantShopCoupon);
        if (i < 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该优惠券删除失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }

    @Override
    public MerchantShopCoupon findCouponById(Long shopId, Long couponId) {

        QueryWrapper<MerchantShopCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantShopCoupon::getId, couponId)
                .eq(MerchantShopCoupon::getShopId, shopId)
                .eq(MerchantShopCoupon::getDel, 0);

        return merchantShopCouponMapper.selectOne(wrapper);
    }
}
