package mf.code.api.seller.dto;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * mf.code.api.seller.dto
 * Description:
 *
 * @author: 夜辰
 * @date: 2019-03-12 11:23
 */
@Data
public class UserTaskV2AuditReq {

    private String userTaskId;
    private String pics;

    private List<Reason> photos;

    @Data
    public static class Reason {
        private String reason;
        private String pic;
        //-1, "审核失败"， 1, "审核通过"
        private int status;
        private int index;
        private String orderId;
    }

    public void form() {
        if (StringUtils.isNotBlank(this.pics)) {
            List<Reason> picsJson = JSONArray.parseArray(this.pics, Reason.class);
            if(!CollectionUtils.isEmpty(picsJson)){
                this.photos = picsJson;
            }
        }
    }
}
