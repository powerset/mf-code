package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.dto.VersionResp;
import mf.code.api.seller.service.SellerShopService;
import mf.code.api.seller.service.SellerVersionService;
import mf.code.common.caller.wxmp.WeixinMpConstants;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.seller.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2019-02-27 16:30
 */
@Slf4j
@Service
public class SellerVersionServiceImpl implements SellerVersionService {

    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private SellerShopService sellerShopService;

    /**
     * 获取小程序版本更新内容
     *
     * @param merchantId 商户id
     * @param shopId     店铺id
     * @return 返回
     *
     * purchaseVersion: '', // 订购版本：0测试版(给测试用的)1试用版 2公开版 3私有版
     * purchaseTime: '', // 订购时间
     * purchaseExpireTime: '', // 订购过期时间
     * widgetName: '', // 小程序名称
     * versionNumber: '', // 版本号
     * versionText: '', // 版本编辑内容
     * shopLogo: '', // 店铺logo
     * smallRogramQr: '' // 小程序二维码
     */
    @Override
    public SimpleResponse getNowVersion(Long merchantId, Long shopId) {

        SimpleResponse simpleResponse = new SimpleResponse();

        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        VersionResp resp = new VersionResp();
        resp.setShopLogo(merchantShop.getPicPath());
        // TODO 待添加修改存储的地方
        resp.setWidgetName("集客魔方");
        resp.setVersionNumber("1.2.9");
        resp.setVersionText("");
        resp.setPurchaseVersion(merchantShop.getPurchaseVersion().toString());
        if(merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode())
                || merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.VERSION_PRIVATE.getCode())
                || merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.CLEAN.getCode())
                || merchantShop.getPurchaseVersion().equals(MerchantShopPurchaseVersionEnum.WEIGHTING.getCode())){
            JSONObject jsonObject = JSON.parseObject(merchantShop.getPurchaseJson());
            if(jsonObject != null){
                resp.setPurchaseTime(jsonObject.getString("purchaseTime"));
                resp.setPurchaseExpireTime(jsonObject.getString("expireTime"));
            }
            SimpleResponse simpleResponse1 = sellerShopService.queryWXACode(merchantId.toString(), shopId.toString(), WeixinMpConstants.SCENE_SHOP);
            resp.setSmallRogramQr(simpleResponse1.getData().toString());
        }
        simpleResponse.setData(resp);
        return simpleResponse;
    }
}
