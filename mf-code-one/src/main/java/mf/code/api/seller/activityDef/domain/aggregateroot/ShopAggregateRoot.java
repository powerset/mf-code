package mf.code.api.seller.activityDef.domain.aggregateroot;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.api.seller.activityDef.domain.entity.ActivityDefHistory;
import mf.code.merchant.repo.po.MerchantShop;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-20 上午9:05
 */
@Data
public class ShopAggregateRoot {
	private Long shopId;
	private MerchantShop shopInfo;
	private ActivityDefHistory activityDefHistory;

	public Map<String, Object> getAppletHomePage() {
		Map<String, Object> shopInfo = getCustomServiceAbout();
		shopInfo.put("shopId", shopId);
		shopInfo.put("picPath", this.shopInfo.getPicPath());
		shopInfo.put("shopName", this.shopInfo.getShopName());
		shopInfo.put("merchantId", this.shopInfo.getMerchantId());

		Map<String, Object> homePage = new HashMap<>();
		homePage.put("shopInfo",shopInfo);

		return homePage;
	}

	private Map<String, Object> getCustomServiceAbout() {
		Map<String, Object> shopInfo = new HashMap<>();
		//客服二维码
		shopInfo.put("customServiceCode", "");
		//客服微信号
		shopInfo.put("customServiceWxNum", "");
		if (StringUtils.isNotBlank(this.shopInfo.getCsJson())) {
			List<Object> objects = JSONObject.parseArray(this.shopInfo.getCsJson(), Object.class);
			//目前取第一个
			if (objects != null && objects.size() > 0) {
				JSONObject jsonObject = JSONObject.parseObject(objects.get(0).toString());
				Map csJsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
				shopInfo.put("customServiceCode", csJsonMap.get("pic"));
				shopInfo.put("customServiceWxNum", csJsonMap.get("wechat"));
			}
		}
		return shopInfo;
	}
}
