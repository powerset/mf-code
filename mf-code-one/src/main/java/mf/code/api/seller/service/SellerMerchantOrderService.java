package mf.code.api.seller.service;

import mf.code.api.seller.dto.ApplyRefundDto;
import mf.code.api.seller.dto.MerchantOrderMktTransferReq;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.common.simpleresp.SimpleResponse;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * mf.code.api.applet.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月17日 19:51
 */
public interface SellerMerchantOrderService {

    /***
     * 创建商户订单
     * @param dto
     * @param request
     * @return
     */
    SimpleResponse creatMerchantOrder(MerchantOrderReq dto, HttpServletRequest request);

    /***
     * 查询商户订单状态
     * @param orderKeyID
     * @return
     */
    SimpleResponse queryMerchantOrderStatus(Long orderKeyID);

    /***
     * 商户订单支付回调
     * @param request
     * @return
     */
    String wxPayCallback(HttpServletRequest request);

    /***
     * 商户退款订单回调
     * @param request
     * @return
     */
    String wxRefundCallback(HttpServletRequest request);

    /***
     * 商户-提现
     * @param req
     * @return
     */
    SimpleResponse createMktTransfersMerchantOrder(MerchantOrderMktTransferReq req, HttpServletRequest request);

    /***
     * 申请退款
     * @param orderId 需要退款的订单主键编号
     * @param refundTotalAmount 退款金额
     * @return
     */
    SimpleResponse merchantRefund(Long orderId, BigDecimal refundTotalAmount);

    /***
     * 查询商户财务统计-除广告
     * @param merchantId
     * @param shopId
     * @param start
     * @param end
     * @param type
     * @return
     */
    SimpleResponse queryRecord(Long merchantId,
                               Long shopId,
                               String start,
                               String end,
                               Integer limit,
                               Integer pageNum,
                               int type);

    /***
     * 查询清算活动余额进度情况
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    SimpleResponse queryBalanceOfClearingProgressInfo(Long merchantId,
                                                      Long shopId,
                                                      Long activityDefId);

    /***
     * 查询商户余额信息
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse queryMerchantBalanceInfo(Long merchantId,
                                            Long shopId);

    /***
     * 申请结算退款
     * @param applyRefundDto
     * @return
     */
    SimpleResponse applyRefund(ApplyRefundDto applyRefundDto);

    /***
     * 查询结算退款状态
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    SimpleResponse queryApplyRefundStatus(Long merchantId,
                                          Long shopId,
                                          Long activityDefId);

    /***
     * 获取购买东西的信息
     * @param merchantId
     * @param type 1：购买小程序
     * @return
     */
    SimpleResponse queryInfo2buy(Long merchantId,
                                 int type);

    /***
     * 查询商户财务记录-流水
     *
     * @param merchantId
     * @param shopId
     * @param limit
     * @param offset
     * @param type 0交易、退款(包含待结算的) 1：提现、收入(只包含已结算的) 2:待结算佣金管理,本月待结算 3:待结算佣金管理,下月待结算
     * @return
     */
    SimpleResponse queryRecordLog(Long merchantId, Long shopId, Integer limit, Integer offset, int type);
}
