package mf.code.api.seller.service.checkMerchantOrderAmount.impl;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.api.seller.service.checkMerchantOrderAmount.CheckCreateAmountConverter;
import mf.code.merchant.constants.BizTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * mf.code.api.seller.service.checkMerchantOrderAmount.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 11:41
 */
@Service
public class ActivityDefAuditConverter implements CheckCreateAmountConverter {
    @Autowired
    private ActivityDefAuditService activityDefAuditService;

    @Override
    public boolean convert(MerchantOrderReq dto) {
        if (dto.getBizType() == BizTypeEnum.ACTIVITY_AUDIT.getCode()) {
            //def_audit编号
            ActivityDefAudit activityDefAudit = this.activityDefAuditService.selectByPrimaryKey(dto.getBizValue());
            if (activityDefAudit != null && activityDefAudit.getDepositApply().compareTo(new BigDecimal(dto.getAmount())) == 0) {
                return true;
            }
        }
        return false;
    }
}
