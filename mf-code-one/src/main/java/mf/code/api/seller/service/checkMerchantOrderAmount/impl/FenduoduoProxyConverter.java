package mf.code.api.seller.service.checkMerchantOrderAmount.impl;

import com.alibaba.fastjson.JSONObject;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.api.seller.service.checkMerchantOrderAmount.CheckCreateAmountConverter;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.merchant.constants.BizTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.service.checkMerchantOrderAmount.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 11:42
 */
@Service
public class FenduoduoProxyConverter implements CheckCreateAmountConverter {
    @Autowired
    private CommonDictService commonDictService;

    @Override
    public boolean convert(MerchantOrderReq dto) {
        if (dto.getBizType() == BizTypeEnum.FENDUODUO_PROXY.getCode()) {
            CommonDict commonDict = this.commonDictService.selectByTypeKey("fenduoduo", "pay_menu");
            List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
            if (!CollectionUtils.isEmpty(objects)) {
                for (Object obj : objects) {
                    //[{"trade_type":1,"money":"6800.00","name":"购买"},{"trade_type":3,"money":"3400.00","name":"续费"}]
                    JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                    Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                    boolean tradeType = jsonMap != null && dto.getTradeType().equals(jsonMap.get("trade_type"));
                    boolean amount = jsonMap != null && new BigDecimal(dto.getAmount()).compareTo(new BigDecimal(jsonMap.get("money").toString())) == 0;
                    if (tradeType && amount) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
