package mf.code.api.seller.v4.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.v4.dto
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-24 16:31
 */
@Data
public class QueryApplyDetailResp {

    /**
     * 总参与人数
     */
    private Integer joinCount;

    /**
     * 完成人数
     */
    private Integer finishedCount;

    /**
     * 任务1完成人数
     */
    private Integer task1Count;

    /**
     * 任务2完成人数
     */
    private Integer task2Count;

    /**
     * 任务3完成人数
     */
    private Integer task3Count;

    /**
     * 列表参数
     */
    private List<Map<String, Object>> list;

    /**
     * 总条数
     */
    private Long total;

    /**
     * 当前页码
     */
    private Long currentPage;

    /**
     * 当前页的条数
     */
    private Long size;

    /**
     * 总页码
     */
    private Long pages;

    /**
     * 类型
     */
    private Integer type;

}
