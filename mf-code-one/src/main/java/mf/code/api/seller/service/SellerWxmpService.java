package mf.code.api.seller.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.common.simpleresp.SimpleResponse;

import java.util.List;

/**
 * mf.code.api.seller.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月21日 14:05
 */
public interface SellerWxmpService {

    /***
     * 创建场景wx小程序码
     * @param merchantId 商户编号
     * @param shopId 店铺编号
     * @param wxmpType wx小程序码类型 1:店铺二维码 2：活动二维码 3：商品二维码
     * @param sceneType 场景类型 1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览
     * @param goodsId 商品编号
     * @return
     */
    SimpleResponse createSceneWxmp(Long merchantId,
                                   Long shopId,
                                   int wxmpType,
                                   int sceneType,
                                   Long goodsId);


    /**
     * 查询活动定义
     * @param merchantId
     * @param shopId
     * @param type
     * @return
     */
    List<ActivityDef> getActivityDef(Long merchantId, Long shopId, Integer type);
    /***
     * 获取场景码对应的业务值
     * @param activityDefList
     * @param merchantId
     * @param shopId
     * @param sceneType
     * @return
     */
    Long getDetailParameterValule(List<ActivityDef> activityDefList, Long merchantId, Long shopId, int sceneType, Long goodsId);
    Long getDetailParameterValule(List<ActivityDef> activityDefList, Long merchantId, Long shopId, int sceneTyp);
    /***
     * 匹配小程序码的场景
     * @param objects
     * @param merchantId
     * @param shopId
     * @param sceneType
     * @return
     */
    Object[] getActivityObjects(Object[] objects, Long merchantId, Long shopId, int sceneType, Long detailParameterValule);

}
