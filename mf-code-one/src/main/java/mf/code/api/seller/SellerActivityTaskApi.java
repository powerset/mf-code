package mf.code.api.seller;

import mf.code.api.seller.dto.UserTaskAuditReq;
import mf.code.api.seller.dto.UserTaskV2AuditReq;
import mf.code.api.seller.dto.UserTaskV3AuditReq;
import mf.code.api.seller.service.SellerActivityTaskService;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 商户关于任务的操作
 *
 * @author gel
 */
@RestController
@RequestMapping(value = "/api/seller")
public class SellerActivityTaskApi {

    @Autowired
    private SellerActivityTaskService sellerActivityTaskService;

    /**
     * 商户审核用户任务
     *
     * @return
     */
    @PostMapping(value = "/task/audit")
    public SimpleResponse auditUserTask(@RequestBody UserTaskAuditReq req) {
        return sellerActivityTaskService.auditUserTask(req);
    }

    /***
     * v2版本审核用户任务
     * @param req
     * @return
     */
    @RequestMapping(value = "/v2/task/audit", method = RequestMethod.POST)
    public SimpleResponse v2AuditUserTask(@RequestBody UserTaskV2AuditReq req) {
        return sellerActivityTaskService.auditUserTaskV2(req);
    }

    /***
     * v3版本审核用户任务
     * @param req
     * @return
     */
    @RequestMapping(value = "/v3/task/audit", method = RequestMethod.POST)
    public SimpleResponse v3AuditUserTask(@RequestBody UserTaskV3AuditReq req) {
        return sellerActivityTaskService.auditUserTaskV3(req);
    }


    /**
     * 分页查询用户任务
     * 1中奖任务，2成长任务 | 3、免单抽奖 4、赠品领取 5、收藏加购 6、好评晒图
     *
     * @param merchantId
     * @param shopId
     * @param status     1:审核中 2：已审核(成功，失败)
     * @param type
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/task/page")
    public SimpleResponse pageListUserTask(@RequestParam(value = "merchantId") Long merchantId,
                                           @RequestParam(value = "shopId", required = false) Long shopId,
                                           @RequestParam(value = "status", required = false) Integer status,
                                           @RequestParam(value = "type", required = false) Integer type,
                                           @RequestParam(value = "page", defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", defaultValue = "10") Integer size) {
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        List<Integer> statusList = new ArrayList<>();
        if (status == null) {
            statusList.add(UserTaskStatusEnum.AUDIT_WAIT.getCode());
            statusList.add(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
            statusList.add(UserTaskStatusEnum.AUDIT_ERROR.getCode());
            statusList.add(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
            params.put("statusList", statusList);
        } else {
            if (status == 1) {
                params.put("status", UserTaskStatusEnum.AUDIT_WAIT.getCode());
            }
            if (status == 2) {
                params.put("statusList", Arrays.asList(UserTaskStatusEnum.AUDIT_SUCCESS.getCode(), UserTaskStatusEnum.AWARD_SUCCESS.getCode(), UserTaskStatusEnum.AUDIT_ERROR.getCode()));
            }
            if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                params.put("order", "receive_time");
                params.put("direct", "asc");
            } else {
                params.put("order", "receive_time");
                params.put("direct", "desc");
            }
        }

        List<Integer> typeList = new ArrayList<>();
        if (type == null) {
            typeList.add(UserTaskTypeEnum.MCH_PLAN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK_V2.getCode());
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT_V2.getCode());
        } else if (type == 1) {
            typeList.add(UserTaskTypeEnum.MCH_PLAN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
        } else if (type == 2) {
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
        } else if (type == 3) {
            // 免单抽奖
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
        } else if (type == 4) {
            // 赠品领取
            typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
        } else if (type == 5) {
            // 收藏加购
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode());
        } else if (type == 6) {
            // 好评晒图
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT_V2.getCode());
        } else if (type == 7) {
            // 回填订单
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK_V2.getCode());
        }
        params.put("types", typeList);
        return sellerActivityTaskService.pageListUserTask(params, page, size);
    }

    /**
     * 分页查询审核任务列表
     * @param merchantId
     * @param shopId
     * @param status 1:审核中 2：已审核(成功，失败)
     * @param type
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/v3/task/page")
    public SimpleResponse taskAuditList(@RequestParam(value = "merchantId",required = false) Long merchantId,
                                        @RequestParam(value = "shopId", required = false) Long shopId,
                                        @RequestParam(value = "status", required = false) Integer status,
                                        @RequestParam(value = "type", required = false) Integer type,
                                        @RequestParam(value = "checkResultType", required = false) Integer checkResultType,
                                        @RequestParam(value = "page", defaultValue = "1") Integer page,
                                        @RequestParam(value = "size", defaultValue = "10") Integer size){

        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        List<Integer> statusList = new ArrayList<>();
        if (status == null) {
            statusList.add(UserTaskStatusEnum.AUDIT_WAIT.getCode());
            statusList.add(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
            statusList.add(UserTaskStatusEnum.AUDIT_ERROR.getCode());
            statusList.add(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
            params.put("statusList", statusList);
        } else {
            if (status == 1) {
                params.put("status", UserTaskStatusEnum.AUDIT_WAIT.getCode());
            }
            //已审核
            if (status == 2) {
                if(null==checkResultType){
                    params.put("statusList", Arrays.asList(UserTaskStatusEnum.AUDIT_SUCCESS.getCode(), UserTaskStatusEnum.AWARD_SUCCESS.getCode(), UserTaskStatusEnum.AUDIT_ERROR.getCode()));
                }
                //审核通过的结果
                else if (1==checkResultType){
                    params.put("statusList", Arrays.asList(UserTaskStatusEnum.AUDIT_SUCCESS.getCode(), UserTaskStatusEnum.AWARD_SUCCESS.getCode()));
                }
                else if(2==checkResultType){
                    params.put("statusList", Arrays.asList( UserTaskStatusEnum.AUDIT_ERROR.getCode()));
                }
            }
            if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                params.put("order", "receive_time");
                params.put("direct", "asc");
            } else {
                params.put("order", "auditing_time");
                params.put("direct", "desc");
            }
        }

        List<Integer> typeList = new ArrayList<>();
        if (type == null) {
            typeList.add(UserTaskTypeEnum.MCH_PLAN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK_V2.getCode());
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT_V2.getCode());
        } else if (type == 1) {
            typeList.add(UserTaskTypeEnum.MCH_PLAN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
        } else if (type == 2) {
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
        } else if (type == 3) {
            // 免单抽奖
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
        } else if (type == 4) {
            // 赠品领取
            typeList.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
            typeList.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START.getCode());
            typeList.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
        } else if (type == 5) {
            // 收藏加购
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
            typeList.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode());
        } else if (type == 6) {
            // 好评晒图
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT.getCode());
            typeList.add(UserTaskTypeEnum.GOOD_COMMENT_V2.getCode());
        } else if (type == 7) {
            // 回填订单
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK.getCode());
            typeList.add(UserTaskTypeEnum.ORDER_REDPACK_V2.getCode());
        }
        params.put("types", typeList);

        return sellerActivityTaskService.taskAuditList(params, page, size);
    }



}
