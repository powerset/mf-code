package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.service.DonateJkmfService;
import mf.code.common.caller.aliyundayu.DayuCaller;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MerchantShopPurchaseVersionEnum;
import mf.code.merchant.dto.JkmfPurchaseDTO;
import mf.code.merchant.repo.enums.OrderPayTypeEnum;
import mf.code.merchant.repo.enums.OrderStatusEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantOrderService;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.seller.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年09月23日 15:02
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DonateJkmfServiceImpl implements DonateJkmfService {
    private final MerchantService merchantService;
    private final MerchantShopService merchantShopService;
    private final DayuCaller dayuCaller;
    private final StringRedisTemplate stringRedisTemplate;
    private final MerchantOrderService merchantOrderService;
    private final CommonDictService commonDictService;

    @Value("${seller.uid.name}")
    private String uidName;
    @Value("${seller.token.name}")
    private String tokenName;

    /***
     * 购买st系列-全网通-赠送集客魔方使用
     *
     * @param phone
     * @param request
     * @return
     */
    @Override
    public SimpleResponse donateJkmf(String phone, HttpServletRequest request, HttpServletResponse response) {
        // 校验商户是否注册过
        Merchant merchant = merchantService.getMerchantByPhone(phone);
        if (merchant == null) {
            log.info(">>>>>>>>>>>>>>>>>商户密码设置,phone:" + phone + ",密码：" + phone.substring(5));
            String password = MD5Util.md5(MD5Util.md5(phone.substring(5)) + phone.substring(7));

            Date now = new Date(System.currentTimeMillis() / 1000 * 1000);
            log.info(">>>>>>>>>>>>>>>>>{} 商户注册时间:{} ", phone, now.getTime());
            merchant = new Merchant();
            merchant.setPhone(phone);
            // 第三方接口调用状态：0: 创建未调用 1：调用成功 -1：创建调用失败 -2：欠费调用失败
            merchant.setStatus(0);
            merchant.setCtime(now);
            merchant.setUtime(now);
            merchant.setPassword(password);
            int size = merchantService.createMerchant(merchant);
            log.info("<<<<<<<<create merchant num:{}", size);

            request.getSession().setAttribute(uidName, merchant.getId());
            request.getSession().setMaxInactiveInterval(8 * 60 * 60);
            // 生成token,存入cookie
            merchant = merchantService.getMerchant(merchant.getId());
            String token = TokenUtil.encryptToken(merchant.getId().toString(), merchant.getPhone(), merchant.getCtime(), TokenUtil.SAAS);
            Cookie cookie = new Cookie(tokenName, token);
            cookie.setMaxAge(8 * 60 * 60);
            cookie.setPath("/");
            response.addCookie(cookie);
            //redis相关存储
            String firstLoginDialogRedisKey = RedisKeyConstant.MERCHANT_FIRSTLOGIN_DIALOG + merchant.getId();
            this.stringRedisTemplate.opsForValue().set(firstLoginDialogRedisKey, "1", 30, TimeUnit.DAYS);
            // 发送初始化密码通知
            dayuCaller.smsForInitPassword(phone, phone.substring(5));
        }

        /**赠送环节**/
        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = IpUtil.getIpAddress(request);
        //订单号 v8赠送标识
        String outTraderNo = "V8" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        String attach = "支付订单";
        MerchantOrder merchantOrder = this.merchantOrderService.addMerchantOrderPo(
                merchant.getId(), 0L, "",
                null, outTraderNo, "购买全网通赠送", OrderStatusEnum.ORDERING.getCode(),
                new BigDecimal(0 + ""), null, attach,
                1, null, null, null, null, ipAddress, null, null);

        merchantOrder.setBizType(BizTypeEnum.PURCHASE_CLEARANCE.getCode());

        merchantOrder.setBizValue((long) getBizValueVersion(MerchantShopPurchaseVersionEnum.CLEAN.getCode()));

        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
        merchantOrder.setPayType(OrderPayTypeEnum.WEIXIN.getCode());
        merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
        merchantOrder.setReqJson(JSON.toJSONString(phone));
        merchantOrder.setPaymentTime(new Date());
        merchantOrder.setUtime(new Date());
        // 这里关于绑定店铺的需要，修改为空字符串
        merchantOrder.setRemark("");
        boolean save = this.merchantOrderService.save(merchantOrder);
        log.info("<<<<<<<<创建订单成功：{}", save);

        //判断是否存在清仓版店铺，若有则增加年限 todo:改动较大，暂时不考虑
//        List<MerchantShop> merchantShops = merchantShopService.list(
//                new QueryWrapper<MerchantShop>()
//                        .lambda()
//                        .eq(MerchantShop::getMerchantId, merchant.getId())
//                        .eq(MerchantShop::getPurchaseVersion, MerchantShopPurchaseVersionEnum.CLEAN.getCode())
//                        .orderByDesc(MerchantShop::getId)
//        );
//        log.info("<<<<<<<<该商户是否存在店铺：{}", merchantShops.size());
//        if (!CollectionUtils.isEmpty(merchantShops)) {
//            //若有，则增加年限
//            MerchantShop merchantShop = merchantShops.get(0);
//            String oldPurchaseJson = merchantShop.getPurchaseJson();
//            JSONObject jsonObject = JSON.parseObject(merchantShop.getPurchaseJson());
//            if (jsonObject != null) {
//                int month = getpurchaseCycleMonth(merchantOrder.getBizValue());
//                Date expireTime = DateUtil.parseDate(jsonObject.getString("expireTime"), null, "yyyy-MM-dd HH:mm:ss");
//                String newExpireTime = DateFormatUtils.format(DateUtils.addMonths(expireTime, month), "yyyy-MM-dd HH:mm:ss");
//
//                Map<String, Object> purchase = new HashMap<>();
//                purchase.put("purchaseTime", jsonObject.getString("purchaseTime"));
//                purchase.put("expireTime", newExpireTime);
//                merchantShop.setUtime(new Date());
//                merchantShop.setPurchaseJson(JSONObject.toJSONString(purchase));
//                merchantShopService.updateById(merchantShop);
//                log.info("<<<<<<<<更新店铺年限 shopId：{}, old time:{}, new time:{}", merchantShop.getId(), oldPurchaseJson, merchantShop.getPurchaseJson());
//            }
//        }
        return new SimpleResponse();
    }

    /***
     *
     * @param type
     * @return
     */
    private int getBizValueVersion(int type) {
        /***
         * [{"bizVersion":1,"money":"0.01","initMoney":"5888.00","purchaseCycle":12,"name":"会员电商版","rate":100},
         * {"bizVersion":2,"money":"5888.00","initMoney":"884.00","purchaseCycle":12,"name":"续费私有版小程序1年","rate":100},
         * {"bizVersion":3,"money":"0.00","initMoney":"1888.00","purchaseCycle":12,"name":"尾货清仓版","rate":100},
         * {"bizVersion":4,"money":"0.00","initMoney":"2888.00","purchaseCycle":12,"name":"电商加权版","rate":100}]
         * bizvalue == bizVersion
         */
        if (type == MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode()) {
            return 1;
        } else if (type == MerchantShopPurchaseVersionEnum.VERSION_PRIVATE.getCode()) {
            return 2;
        } else if (type == MerchantShopPurchaseVersionEnum.CLEAN.getCode()) {
            return 3;
        } else if (type == MerchantShopPurchaseVersionEnum.WEIGHTING.getCode()) {
            return 4;
        }
        return 0;
    }

    private int getpurchaseCycleMonth(Long bizValueVersion) {
        JkmfPurchaseDTO jkmfPurchaseDTO = null;
        //获取字典表信息
        CommonDict commonDict = commonDictService.selectByTypeKey("jkmfPurchase", "pay_menu");
        List<JkmfPurchaseDTO> jkmfPurchaseDTOList = JSONObject.parseArray(commonDict.getValue(), JkmfPurchaseDTO.class);
        if (!CollectionUtils.isEmpty(jkmfPurchaseDTOList)) {
            for (JkmfPurchaseDTO dto : jkmfPurchaseDTOList) {
                //[{"money":"588.00","name":"购买公有版","rate":100},{"money":"5888.00","name":"购买私有版","rate":100}]
                jkmfPurchaseDTO = dto;
                boolean bizVersion = String.valueOf(bizValueVersion).equals(jkmfPurchaseDTO.getBizVersion().toString());
                if (bizVersion) {
                    break;
                }
            }
        }
        //获取购买周期
        int months = 0;
        if (jkmfPurchaseDTO != null && jkmfPurchaseDTO.getPurchaseCycle() != null) {
            months = jkmfPurchaseDTO.getPurchaseCycle();
        }
        return months;
    }
}
