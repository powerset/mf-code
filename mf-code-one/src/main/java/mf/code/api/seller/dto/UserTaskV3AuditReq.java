package mf.code.api.seller.dto;


import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
public class UserTaskV3AuditReq {
    private String userTaskId;
    private String pics;

    private List<UserTaskV3AuditReq.Reason> photos;

    @Data
    public static class Reason {
        private String reason;
        private String pic;
        //-1, "审核失败"， 1, "审核通过"
        private int status;
        private int index;
        private String orderId;
    }

    public void form() {
        if (StringUtils.isNotBlank(this.pics)) {
            List<UserTaskV3AuditReq.Reason> picsJson = JSONArray.parseArray(this.pics, UserTaskV3AuditReq.Reason.class);
            if (!CollectionUtils.isEmpty(picsJson)) {
                this.photos = picsJson;
            }
        }
    }

}
