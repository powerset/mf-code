package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 18:06
 */
@Service
@Slf4j
public class ActivityTypeBalanceOfClearingProgressConverterImpl extends BalanceOfClearingProgressService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserCouponService userCouponService;


    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean activityType = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.ASSIST_V2.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.MCH_PLAN.getCode());
        if (activityType) {
            //查询活动-activity
            QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
            activityWrapper.lambda()
                    .eq(Activity::getMerchantId, activityDef.getMerchantId())
                    .eq(Activity::getShopId, activityDef.getShopId())
                    .eq(Activity::getActivityDefId, activityDef.getId())
                    .in(Activity::getStatus, Arrays.asList(
                            ActivityStatusEnum.PUBLISHED.getCode(),
                            ActivityStatusEnum.END.getCode()
                    ))
                    .orderByDesc(Activity::getId)
            ;
            List<Activity> activities = this.activityService.list(activityWrapper);
            List<Long> activityIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activities)) {
                for (Activity activity : activities) {
                    if (activityIds.indexOf(activity.getId()) == -1) {
                        activityIds.add(activity.getId());
                    }
                }
            }
            //查询活动占用金额-userTask
            List<UserTask> userTasks = null;
            if (!CollectionUtils.isEmpty(activityIds)) {
                QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
                userTaskWrapper.lambda()
                        .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                        .eq(UserTask::getShopId, activityDef.getShopId())
                        .in(UserTask::getActivityId, activityIds)
                        .in(UserTask::getStatus, Arrays.asList(
                                UserTaskStatusEnum.AUDIT_ERROR.getCode(),
                                UserTaskStatusEnum.AUDIT_WAIT.getCode(),
                                UserTaskStatusEnum.AUDIT_SUCCESS.getCode(),
                                UserTaskStatusEnum.INIT.getCode(),
                                UserTaskStatusEnum.AWARD_SUCCESS.getCode()
                        ))
                ;
                userTasks = this.userTaskService.list(userTaskWrapper);
            }

            //查询活动消耗金额-userCoupon
            List<UserCoupon> userCoupons = null;
            if (!CollectionUtils.isEmpty(activityIds)) {
                QueryWrapper<UserCoupon> userCouponWrapper = new QueryWrapper<>();
                userCouponWrapper.lambda()
                        .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                        .eq(UserCoupon::getShopId, activityDef.getShopId())
                        .in(UserCoupon::getActivityId, activityIds)
                        .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                ;
                userCoupons = this.userCouponService.list(userCouponWrapper);
            }

            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.from(activityDef, activities, userTasks, userCoupons);
            return dto;
        }
        return null;
    }
}
