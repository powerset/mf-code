package mf.code.api.seller.service.wxPayCallbackBus.impl;

import mf.code.api.ad.service.AdSellerService;
import mf.code.api.seller.service.wxPayCallbackBus.WxPayCallbackBusConverter;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.seller.service.wxPayCallbackBus.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 13:50
 */
@Service
public class AdCallbackConverter implements WxPayCallbackBusConverter {
    @Autowired
    private AdSellerService adSellerService;

    @Override
    public String convert(MerchantOrder merchantOrder) {
        if (merchantOrder.getBizType() == BizTypeEnum.AD.getCode()) {
            //广告位的付款
            this.adSellerService.updatePurchaseStatus(merchantOrder.getBizValue());
            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }
}
