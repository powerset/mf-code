package mf.code.api.seller.service.wxPayCallbackBus.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.api.seller.service.wxPayCallbackBus.WxPayCallbackBusConverter;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.enums.MerchantProxyServerTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.po.MerchantProxyServer;
import mf.code.merchant.service.MerchantProxyServerService;
import mf.code.merchant.service.MerchantService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * mf.code.api.seller.service.wxPayCallbackBus.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 13:50
 */
@Service
public class FenduoduoProxyCallbackConverter implements WxPayCallbackBusConverter {
    @Autowired
    private MerchantProxyServerService merchantProxyServerService;
    @Autowired
    private MerchantService merchantService;

    @Override
    public String convert(MerchantOrder merchantOrder) {
        if (merchantOrder.getBizType() == BizTypeEnum.FENDUODUO_PROXY.getCode()) {
            //代理第三方软件服务的存储-粉多多
            MerchantProxyServer merchantProxyServer = this.getMerchantProxyServer(merchantOrder.getMerchantId(), MerchantProxyServerTypeEnum.FENDUODUO.getCode());
            if (merchantProxyServer == null) {
                merchantProxyServer = new MerchantProxyServer();
                this.addMerchantProxyServer(merchantProxyServer, merchantOrder, MerchantProxyServerTypeEnum.FENDUODUO.getCode());
                this.merchantProxyServerService.insertSelective(merchantProxyServer);
            } else {
                //更新数据
                merchantProxyServer.setUtime(new Date());
                merchantProxyServer.setFee(merchantProxyServer.getFee().add(merchantOrder.getTotalFee()));
                int addDays = 0;
                if (merchantProxyServer.getExpireTime().getTime() > System.currentTimeMillis()) {
                    addDays = (int) DateUtil.dayDiff(new Date(), merchantProxyServer.getExpireTime());
                }
                merchantProxyServer.setExpireTime(DateUtils.addYears(new Date(), 1));
                merchantProxyServer.setExpireTime(DateUtils.addDays(merchantProxyServer.getExpireTime(), addDays));
                this.merchantProxyServerService.updateById(merchantProxyServer);
                return ApiStatusEnum.SUCCESS.getMessage();
            }
        }
        return null;
    }

    /***
     * 查询商户代理服务信息
     * @param merchantId
     * @param type
     * @return
     */
    private MerchantProxyServer getMerchantProxyServer(Long merchantId, int type) {
        MerchantProxyServer merchantProxyServer = null;
        QueryWrapper<MerchantProxyServer> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantProxyServer::getMerchantId, this.merchantService.getParentMerchantId(merchantId))
                .eq(MerchantProxyServer::getType, type)
        ;
        List<MerchantProxyServer> merchantProxyServers = this.merchantProxyServerService.list(wrapper);
        if (!CollectionUtils.isEmpty(merchantProxyServers)) {
            merchantProxyServer = merchantProxyServers.get(0);
        }
        return merchantProxyServer;
    }

    /***
     * 拼接代理服务信息对象
     * @param merchantProxyServer
     * @param merchantOrder
     * @param type
     * @return
     */
    private MerchantProxyServer addMerchantProxyServer(MerchantProxyServer merchantProxyServer, MerchantOrder merchantOrder, int type) {
        merchantProxyServer.setMerchantId(merchantOrder.getMerchantId());
        merchantProxyServer.setShopIds("0");
        merchantProxyServer.setFee(merchantOrder.getTotalFee());
        merchantProxyServer.setType(type);
        merchantProxyServer.setVersion(0);
        merchantProxyServer.setBuyTime(new Date());
        merchantProxyServer.setExpireTime(DateUtils.addYears(new Date(), 1));
        merchantProxyServer.setExtra(null);
        merchantProxyServer.setCtime(new Date());
        merchantProxyServer.setUtime(new Date());
        merchantProxyServer.setIncomeBillNo(merchantOrder.getOrderNo());
        merchantProxyServer.setPayBillNo("");
        return merchantProxyServer;
    }
}
