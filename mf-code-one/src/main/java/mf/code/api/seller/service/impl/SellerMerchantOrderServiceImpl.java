package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.MybatisPageDto;
import mf.code.api.ad.service.AdSellerService;
import mf.code.api.feignclient.OrderAppService;
import mf.code.api.money.ReportService;
import mf.code.api.seller.dto.*;
import mf.code.api.seller.service.SellerMerchantOrderService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressConverter;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.checkMerchantOrderAmount.CheckCreateAmountConverters;
import mf.code.api.seller.service.wxPayCallbackBus.WxPayCallbackBusConverters;
import mf.code.common.caller.wxpay.WXPayUtil;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.ActivityDefAuditStatusEnum;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.exception.SqlUpdateFaildException;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MerchantOrderTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.dto.AppletMerchantOrderPageDTO;
import mf.code.merchant.repo.enums.MerchantTypeEnum;
import mf.code.merchant.repo.enums.OrderPayTypeEnum;
import mf.code.merchant.repo.enums.OrderStatusEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantBalance;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.*;
import mf.code.upay.repo.po.Order;
import mf.code.upay.service.OrderService;
import mf.code.user.repo.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月17日 19:51
 */
@Service
@Slf4j
public class SellerMerchantOrderServiceImpl implements SellerMerchantOrderService {
    private final WxpayProperty wxpayProperty;
    private final WeixinPayService weixinPayService;
    private final StringRedisTemplate stringRedisTemplate;
    private final MerchantService merchantService;
    private final MerchantOrderService merchantOrderService;
    private final MerchantBalanceService merchantBalanceService;
    private final AdSellerService adSellerService;
    private final MerchantShopService merchantShopService;
    private final ActivityDefService activityDefService;
    private final ActivityDefAuditService activityDefAuditService;
    private final ReportService reportService;
    private final List<BalanceOfClearingProgressConverter> balanceOfClearingProgressConverters;
    private final MerchantProxyServerService merchantProxyServerService;
    private final CheckCreateAmountConverters checkCreateAmountConverters;
    private final WxPayCallbackBusConverters wxPayCallbackBusConverters;
    private final CommonDictService commonDictService;
    private final ActivityDefCheckService activityDefCheckService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderAppService orderAppService;

    @Value("${merchantPay.debug.mode}")
    private Integer debugMode;

    @Autowired
    public SellerMerchantOrderServiceImpl(CommonDictService commonDictService, WxPayCallbackBusConverters wxPayCallbackBusConverters, CheckCreateAmountConverters checkCreateAmountConverters, MerchantProxyServerService merchantProxyServerService, List<BalanceOfClearingProgressConverter> balanceOfClearingProgressConverters, ReportService reportService, ActivityDefAuditService activityDefAuditService, ActivityDefService activityDefService, MerchantShopService merchantShopService, AdSellerService adSellerService, MerchantBalanceService merchantBalanceService, MerchantOrderService merchantOrderService, MerchantService merchantService, StringRedisTemplate stringRedisTemplate, WeixinPayService weixinPayService, WxpayProperty wxpayProperty, ActivityDefCheckService activityDefCheckService) {
        this.wxpayProperty = wxpayProperty;
        this.weixinPayService = weixinPayService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.merchantService = merchantService;
        this.merchantOrderService = merchantOrderService;
        this.merchantBalanceService = merchantBalanceService;
        this.adSellerService = adSellerService;
        this.merchantShopService = merchantShopService;
        this.activityDefService = activityDefService;
        this.activityDefAuditService = activityDefAuditService;
        this.reportService = reportService;
        this.balanceOfClearingProgressConverters = balanceOfClearingProgressConverters;
        this.merchantProxyServerService = merchantProxyServerService;
        this.checkCreateAmountConverters = checkCreateAmountConverters;
        this.wxPayCallbackBusConverters = wxPayCallbackBusConverters;
        this.commonDictService = commonDictService;
        this.activityDefCheckService = activityDefCheckService;
    }

    //商户订单的redis临时存储
    public static final String MERCHANTORDERREDISKEY = "order:merchantorder:";//mid+sid+cusid+amount+pay

    private String getShopName(Long shopId) {
        String shopName = "";
        if (shopId != null && shopId > 0) {
            MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopId);
            if (merchantShop != null) {
                shopName = merchantShop.getShopName();
            }
        }
        return shopName;
    }

    /***
     * 创建商户订单
     * @param dto
     * @param request
     * @return
     */
    @Transactional
    @Override
    public SimpleResponse creatMerchantOrder(MerchantOrderReq dto, HttpServletRequest request) {
        //step1:入参安全性验证
        this.safetyVerification4pay(dto);
        //验证金额的值是否满足条件
        if (!this.checkCreateAmount(dto)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "需支付金额和统计所需金额不匹配，编号=" + dto.getBizValue() + "类型编号：" + dto.getBizType());
        }
        //step2:拼装微信统一下单的参数
        //订单号
        String outTraderNo = "V2" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = StringUtils.isNotBlank(dto.getUserIp()) ? dto.getUserIp() : IpUtil.getIpAddress(request);
        String attach = "支付订单";
        String orderName = "支付订单";

        //step3:储存db
        MerchantOrder merchantOrder = this.merchantOrderService.addMerchantOrderPo(dto.getMerchantId(), dto.getShopId(), this.getShopName(dto.getShopId()),
                null, outTraderNo, orderName, OrderStatusEnum.ORDERING.getCode(), new BigDecimal(dto.getAmount()), null, attach,
                dto.getTradeType(), null, null, null, null, ipAddress, null, null);
        this.fillMerchantOrderBizTypeAndBizValue(merchantOrder, dto);

        //判断支付方式
        if (MerchantOrderReq.payment_prestore.equals(dto.getPayType())) {//预存金付款方式
            return this.prestorePayType(merchantOrder, dto.getMerchantId(), dto.getShopId(), dto.getAmount(), dto.getCustomerId());
        } else {//扫码付款方式|充值
            if (new BigDecimal(dto.getAmount()).compareTo(BigDecimal.ZERO) == 0) {
                //0元购支付方式
                merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
                merchantOrder.setPayType(OrderPayTypeEnum.WEIXIN.getCode());
                merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
                merchantOrder.setReqJson(JSON.toJSONString(dto));
                merchantOrder.setPaymentTime(new Date());
                merchantOrder.setUtime(new Date());
                // 这里关于绑定店铺的需要，修改为空字符串
                merchantOrder.setRemark("");
                this.merchantOrderService.save(merchantOrder);
                return new SimpleResponse();
            }

            String str = this.stringRedisTemplate.opsForValue().get(this.getMerchantOrderRedisKey(dto));
            if (StringUtils.isNotBlank(str)) {
                return JSONObject.parseObject(str, SimpleResponse.class);
            }
            List<Object> customerList = new ArrayList<>();
            Map<String, Object> customerMap = this.setCustomerJson(dto.getAmount(), dto.getCustomerId(), true);
            customerList.add(customerMap);
            merchantOrder.setCustomerJson(JSON.toJSONString(customerList));
            return this.wxPayType(merchantOrder, dto, outTraderNo, ipAddress, attach, "");//TODO：YY支持openid
        }
    }

    /***
     * 查询商户订单状态
     * @param orderKeyID
     * @return
     */
    @Override
    public SimpleResponse queryMerchantOrderStatus(Long orderKeyID) {
        SimpleResponse d = new SimpleResponse();
        //入参安全性验证
        if (orderKeyID == null || orderKeyID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        MerchantOrder merchantOrder = this.merchantOrderService.getById(orderKeyID);
        if (merchantOrder == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该订单不存在");
            return d;
        }
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("status", merchantOrder.getStatus());
        //下单中时,调用微信平台接口查验
        if (merchantOrder.getStatus() == OrderStatusEnum.FAILED.getCode() || merchantOrder.getStatus() == OrderStatusEnum.TIMEOUT.getCode()) {
            respMap.put("status", OrderStatusEnum.FAILED.getCode());
        }

        boolean twoMinuteAfter = merchantOrder.getStatus() == OrderStatusEnum.ORDERING.getCode() && (System.currentTimeMillis() - merchantOrder.getCtime().getTime()) >= 30 * 60 * 1000;
        if (twoMinuteAfter) {
            respMap.put("status", OrderStatusEnum.FAILED.getCode());

            Map<String, Object> queryParams = this.weixinPayService.getQueryOrderParams(merchantOrder.getOrderNo(), WeixinPayConstants.PAYSCENE_APPLET_APPID);
            Map<String, Object> respQueryOrderMap = this.weixinPayService.queryOrder(queryParams);
            log.info("查询微信该订单信息:{}", respQueryOrderMap);
            boolean returnCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("return_code") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("return_code").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("return_code"));
            boolean resultCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("result_code") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("result_code").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("result_code"));
            boolean tradeStateSuccess = respQueryOrderMap != null && respQueryOrderMap.get("trade_state") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("trade_state").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("trade_state"));
            if (returnCodeSuccess && resultCodeSuccess && tradeStateSuccess) {
                /***
                 * SUCCESS—支付成功 REFUND—转入退款 NOTPAY—未支付 CLOSED—已关闭 REVOKED—已撤销（刷卡支付）USERPAYING--用户支付中 PAYERROR--支付失败(其他原因，如银行返回失败)
                 */
                merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
                merchantOrder.setTradeId(respQueryOrderMap.get("transaction_id").toString());
                merchantOrder.setPaymentTime(DateUtil.parseDate(respQueryOrderMap.get("time_end").toString(), null, "yyyyMMddHHmmss"));
                merchantOrder.setUtime(new Date());
                this.merchantOrderService.updateById(merchantOrder);
                //TODO:走退款流程
            }
        }
        d.setData(respMap);
        return d;
    }

    /***
     * 订单支付回调
     * @param request
     * @return
     */
    @Override
    public String wxPayCallback(HttpServletRequest request) {
        SimpleResponse simpleResponse = new SimpleResponse();
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        log.info("开始进入回调--");
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信支付回调失败!没有字段参数返回";
        }
        //验证签名
        Map<String, Object> checkSign = CertHttpUtil.paraFilter(wxXmlMap);
        String sortSign = SortUtil.buildSignStr(checkSign);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //对回调的参数进行验签
        if (!MD5Util.md5(sign).toUpperCase().equals(wxXmlMap.get("sign"))) {
            return "wxpayNotify:微信支付回调失败!签名不一致";
        }
        //获取基本数据
        String resultCode = wxXmlMap.get("result_code").toString();
        if ("FAIL".equals(resultCode)) {
            return "支付失败";
        }
        // 订单号
        String orderNo = wxXmlMap.get("out_trade_no").toString();
        //redis去重
        String redisForbidKey = mf.code.common.redis.RedisKeyConstant.FORBID_KEY + orderNo;
        Boolean aBoolean = stringRedisTemplate.opsForValue().setIfAbsent(redisForbidKey, "");
        if (aBoolean != null && !aBoolean) {
            return "微信支付回调失败!回调正在触发，请勿重复";
        }
        // 金额,分转元
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(wxXmlMap.get("total_fee").toString())).divide(new BigDecimal(100));
        // 微信支付时间
        Date paymentTime = DateUtil.parseDate(wxXmlMap.get("time_end").toString(), null, "yyyyMMddHHmmss");
        //微信支付订单号
        String transaction_id = wxXmlMap.get("transaction_id").toString();

        //微信支付openid标识
        String payOpenId = wxXmlMap.get("openid").toString();

        MerchantOrderReq req = new MerchantOrderReq();
        try {
            //插入成功，设置过期时间
            stringRedisTemplate.expire(redisForbidKey, 3, TimeUnit.SECONDS);
            MerchantOrder merchantOrder = this.merchantOrderService.queryByOrderNo(orderNo);
            if (merchantOrder == null) {
                log.error("商户回调内，订单不存在了...编号：{}", orderNo);
                return WXPayUtil.resp();
            }
            if (!(merchantOrder.getTotalFee().compareTo(totalFee) == 0)) {
                log.error("wxpayNotify:微信支付回调失败!返回的金额与系统订单的金额");
                return "微信支付回调失败!返回的金额与系统订单的金额不匹配";
            }
            merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
            merchantOrder.setTradeId(transaction_id);
            merchantOrder.setPaymentTime(paymentTime);
            merchantOrder.setTotalFee(totalFee);
            merchantOrder.setUtime(new Date());
            merchantOrder.setPayOpenId(payOpenId);
            this.merchantOrderService.updateById(merchantOrder);

            req.from(merchantOrder);
            if (merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode() &&
                    merchantOrder.getPayType() == OrderPayTypeEnum.WEIXIN.getCode()) {
                //TODO:wx充值
//                this.merchantBalanceService.updateById()
            } else {
                this.wxPayCallbackBusConverters.convert(merchantOrder);
            }
        } finally {
            this.stringRedisTemplate.delete(redisForbidKey);
            this.stringRedisTemplate.delete(this.getMerchantOrderRedisKey(req));
        }
        return WXPayUtil.resp();
    }

    /***
     * 退款订单回调
     * @param request
     * @return
     */
    @Override
    public String wxRefundCallback(HttpServletRequest request) {
        SimpleResponse simpleResponse = new SimpleResponse();
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信退款回调失败!没有字段参数返回";
        }
        String decryptDataReqInfo = null;
        if (wxXmlMap.get("return_code") != null && "SUCCESS".equals(wxXmlMap.get("return_code"))) {
            decryptDataReqInfo = DESUtil.decryptData(wxXmlMap.get("req_info").toString(), wxpayProperty.getMchSecretKey());
        }
        log.info(">>>>>>>>>>decryptDataReqInfo:{}", decryptDataReqInfo);
        Map<String, Object> xmlMapReqInfo = new HashMap<>();
        if (StringUtils.isNotBlank(decryptDataReqInfo)) {
            xmlMapReqInfo = WXPayUtil.xmlToMap(decryptDataReqInfo);
            log.info(">>>>>>>>>>xmlMapReqInfo:{}", xmlMapReqInfo);
        }
        if (xmlMapReqInfo == null || xmlMapReqInfo.size() == 0) {
            return "wxpayNotify:微信退款回调失败!req_info解密没有字段返回";
        }
        //获取基本数据 // SUCCESS-退款成功 CHANGE-退款异常 REFUNDCLOSE—退款关闭
        String refundStatus = xmlMapReqInfo.get("refund_status").toString();
        if (!"SUCCESS".equals(refundStatus)) {
            return "退款未成功";
        }
        // 原订单号
        String orderNo = xmlMapReqInfo.get("out_trade_no").toString();
        //退款订单号
        String refundOrderNo = xmlMapReqInfo.get("out_refund_no").toString();
        //redis去重
        String redisForbidKey = mf.code.common.redis.RedisKeyConstant.FORBID_KEY + refundOrderNo;
        if (!stringRedisTemplate.opsForValue().setIfAbsent(redisForbidKey, "")) {
            return "微信退款正在触发，请勿重复";
        }
        // 申请退款退款金额
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(xmlMapReqInfo.get("refund_fee").toString())).divide(new BigDecimal(100));
        //实际退款金额
        BigDecimal settlementRefundFee = BigDecimal.valueOf(NumberUtils.toInt(xmlMapReqInfo.get("settlement_refund_fee").toString())).divide(new BigDecimal(100));
        //微信订单号
        String transactionId = xmlMapReqInfo.get("transaction_id").toString();

        try {
            //插入成功，设置过期时间
            stringRedisTemplate.expire(redisForbidKey, 3, TimeUnit.SECONDS);
            MerchantOrder oldMerchantOrder = this.merchantOrderService.queryByOrderNo(orderNo);
            String customerJson = null;
            if (oldMerchantOrder != null && StringUtils.isNotBlank(oldMerchantOrder.getCustomerJson())) {
                customerJson = oldMerchantOrder.getCustomerJson();
            }
            MerchantOrder merchantOrder = this.merchantOrderService.queryByOrderNo(refundOrderNo);
            merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
            merchantOrder.setTotalFee(settlementRefundFee);
            merchantOrder.setTradeId(transactionId);
            merchantOrder.setCustomerJson(customerJson);
            merchantOrder.setUtime(new Date());
            this.merchantOrderService.updateById(merchantOrder);

            if (merchantOrder.getBizType() == BizTypeEnum.AD.getCode()) {
                //TODO: 百川退款后的操作-广告
            } else if (BizTypeEnum.isCreateDefMerchantOrder(merchantOrder.getBizType())) {
                //活动结算
                //验证redis
                String redisStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANTORDER_REFUND_END + merchantOrder.getBizValue());
                if (StringUtils.isNotBlank(redisStr)) {
                    this.activityDefCheckService.refundDef(merchantOrder.getBizValue(), merchantOrder.getTotalFee());
                    this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND + merchantOrder.getBizValue());
                    this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND_END + merchantOrder.getBizValue());
                }
            } else if (merchantOrder.getBizType() == BizTypeEnum.PLAN_ACTIVITY.getCode()) {
                //TODO:曲成退款后的操作--活动计划
            } else if (merchantOrder.getBizType() == BizTypeEnum.ACTIVITY_AUDIT.getCode()) {
                //补库存-结算
                ActivityDefAudit activityDefAudit = this.activityDefAuditService.selectByPrimaryKey(merchantOrder.getBizValue());
                if (activityDefAudit != null) {
                    String redisStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANTORDER_REFUND_END + activityDefAudit.getActivityDefId());
                    if (StringUtils.isNotBlank(redisStr)) {
                        this.activityDefCheckService.refundAddStockAudit(merchantOrder.getBizValue(), merchantOrder.getTotalFee());
                        this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND + activityDefAudit.getActivityDefId());
                        this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND_END + activityDefAudit.getActivityDefId());
                    }
                }
            }
            return WXPayUtil.resp();
        } catch (SqlUpdateFaildException e) {
            log.error("SqlUpdateFaildException: {}", e);
        } finally {
            this.stringRedisTemplate.delete(redisForbidKey);
        }
        return null;
    }

    /***
     * 商户提现订单
     * @param req
     * @return
     */
    @Override
    public SimpleResponse createMktTransfersMerchantOrder(MerchantOrderMktTransferReq req, HttpServletRequest request) {
        SimpleResponse d = new SimpleResponse();
        //step1:入参安全性验证和基础数据梳理
        this.safetyVerification4cash(req);
        //step2:redis去重
        Boolean isExist = this.stringRedisTemplate.opsForValue().setIfAbsent(RedisKeyConstant.USERPRESENTMONEY + req.getCustomerId(), "");
        if (isExist != null && !isExist) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("微信提现正在触发，请勿重复");
            return d;
        }
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("merchant_id", req.getMerchantId());
            params.put("shop_id", req.getShopId());
            params.put("customer_id", req.getCustomerId());
            QueryWrapper<MerchantBalance> wrapper = new QueryWrapper<>();
            wrapper.allEq(params);
            MerchantBalance merchantBalance = this.merchantBalanceService.getOne(wrapper);
            if (merchantBalance == null || merchantBalance.getDepositAdvance().compareTo(BigDecimal.ZERO) <= 0) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                d.setMessage("提现金额不满足，无法提现");
                return d;
            }
            //step3:调用微信企业支付接口
            Merchant merchant = this.merchantService.getMerchant(req.getCustomerId());
            //提现描述
            String remark = wxpayProperty.getMfName() + "-" + wxpayProperty.getDesc() + req.getCustomerId();
            Map<String, Object> reqMapParams = this.weixinPayService.getTransferReqParams(merchant.getOpenId(),
                    null, new BigDecimal(req.getAmount()), remark, WeixinPayConstants.PAYSCENE_PUB_APPID);
            Map<String, Object> xmlMap = this.weixinPayService.transfer(reqMapParams);
            log.info("用户提现-微信支付返回：xmlMap {}", xmlMap);
            //通信标识+交易标识  状态都成功时 做数据处理
            if (xmlMap == null) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
                return d;
            }
            if ((StringUtils.isNotBlank(xmlMap.get("return_code").toString()) && "FAIL".equals(xmlMap.get("return_code").toString().toUpperCase()))
                    || (StringUtils.isNotBlank(xmlMap.get("result_code").toString()) && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase()))) {
                if (StringUtils.isNotBlank(xmlMap.get("result_code").toString())
                        && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase())
                        && StringUtils.isNotBlank(xmlMap.get("err_code").toString().toUpperCase())
                        && "SYSTEMERROR".equals(xmlMap.get("err_code").toString().toUpperCase())) {//为这种错误时，需确认是否真的失败
                    boolean checkStatus = this.weixinPayService.checkTransfers(xmlMap.get("nonce_str").toString(), xmlMap.get("sign").toString(), xmlMap.get("partner_trade_no").toString());
                    if (!checkStatus) {
                        //TODO:提现失败，是重试，还是让用户重新走流程
                        d.setStatusEnum(ApiStatusEnum.ERROR_WXPAY);
                        d.setMessage("微信零钱出现异常，请重新发起提现请求");
                    }
                }
                d.setStatusEnum(ApiStatusEnum.ERROR_WXPAY);
                if (StringUtils.isNotBlank(xmlMap.get("return_msg").toString()) || StringUtils.isNotBlank(xmlMap.get("err_code_des").toString())) {
                    d.setMessage(StringUtils.isNotBlank(xmlMap.get("err_code_des").toString()) ? xmlMap.get("err_code_des").toString() : xmlMap.get("return_msg").toString());
                }
                return d;
            }
            //插入成功，设置过期时间
            stringRedisTemplate.expire(RedisKeyConstant.USERPRESENTMONEY + req.getCustomerId(), 4, TimeUnit.SECONDS);
            /**
             * step3:db处理  订单表的处理 订单表-的创建-用户余额的更新
             */
            //订单表的创建
            String orderNo = "V2" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
            String orderName = this.getShopName(req.getShopId()) + "发起提现";
            //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
            String ipAddress;
            if (request == null) {
                ipAddress = IpUtil.getServerIp();
            } else {
                ipAddress = request.getHeader("X-Forwarded-For") != null && StringUtils.isNotBlank(request.getHeader("X-Forwarded-For").toString()) ?
                        request.getHeader("X-Forwarded-For").toString() : IpUtil.getServerIp();
            }
            MerchantOrder merchantOrder = this.merchantOrderService.addMerchantOrderPo(req.getMerchantId(), req.getShopId(), this.getShopName(req.getShopId()),
                    null, orderNo, orderName, OrderStatusEnum.ORDERED.getCode(), new BigDecimal(req.getAmount()),
                    xmlMap.get("partner_trade_no").toString(), remark, null, null, null, OrderTypeEnum.USERCASH.getCode(),
                    OrderPayTypeEnum.CASH.getCode(), ipAddress, null, JSON.toJSONString(reqMapParams));
            merchantOrder.setPaymentTime(DateUtil.parseDate(xmlMap.get("payment_time").toString(), null, "yyyy-MM-dd HH:mm:ss"));
            this.merchantOrderService.save(merchantOrder);
            //更新余额表
            merchantBalance.setDepositAdvance(merchantBalance.getDepositAdvance().subtract(new BigDecimal(req.getAmount())));
            merchantBalance.setUtime(new Date());
            this.merchantBalanceService.updateById(merchantBalance);
        } finally {
            //结束时，对key进行删除处理
            this.stringRedisTemplate.delete(RedisKeyConstant.USERPRESENTMONEY + req.getCustomerId());
        }
        return d;
    }

    /***
     * 申请退款
     * @param orderId
     * @param refundTotalAmount
     * @return
     */
    @Override
    @Transactional
    public SimpleResponse merchantRefund(Long orderId, BigDecimal refundTotalAmount) {
        SimpleResponse d = new SimpleResponse();
        MerchantOrder merchantOrder = this.merchantOrderService.getById(orderId);
        if (merchantOrder == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("该编号对应的订单不存在");
            return d;
        }

        //创建退款订单
        String outTraderNo = "V2" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        String orderName = this.getShopName(merchantOrder.getShopId()) + "发起退款订单";
        String attach = this.getShopName(merchantOrder.getShopId()) + "发起退款订单";
        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = IpUtil.getIpAddress(null);
        MerchantOrder refundMerchantOrder = this.merchantOrderService.addMerchantOrderPo(merchantOrder.getMerchantId(), merchantOrder.getShopId(),
                merchantOrder.getShopName(), merchantOrder.getCustomerJson(), outTraderNo, orderName, OrderStatusEnum.ORDERING.getCode(), refundTotalAmount,
                null, attach, MerchantOrderReq.trade_type_refund, merchantOrder.getBizType(), merchantOrder.getBizValue(),
                OrderTypeEnum.PALTFORMREFUND.getCode(), merchantOrder.getPayType(), ipAddress, merchantOrder.getId(), null);
        this.merchantOrderService.save(refundMerchantOrder);

        if (merchantOrder.getPayType() == OrderPayTypeEnum.CASH.getCode()) {
            //预存金退款
            d = this.prestoreRefund(merchantOrder, refundTotalAmount);
        } else if (merchantOrder.getPayType() == OrderPayTypeEnum.WEIXIN.getCode()) {
            //微信退款
            d = this.wxRefund(merchantOrder, merchantOrder.getTotalFee(), refundTotalAmount, refundMerchantOrder.getOrderNo());
        }
        return d;
    }

    /***
     * 查询商户财务统计-除广告
     * @param merchantId
     * @param shopId
     * @param start
     * @param end
     * @param type
     *  <pre>
     *     (1,"支付保证金"),
     *     (2,"保证金结算退回"),
     *     (3,"补库存"),
     *     (6, "仅退款"),
     *     (7, "退货退款"),
     *     (8, "提现"),
     *     (9,商品交易((4, "商品售出待结算"),(5, "商品售出已结算")))
     *  </pre>
     * @return
     */
    @Override
    public SimpleResponse queryRecord(Long merchantId,
                                      Long shopId,
                                      String start,
                                      String end,
                                      Integer limit,
                                      Integer pageNum,
                                      int type) {
        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isBlank(start) || StringUtils.isBlank(end)) {
            Date endDay = new Date();
            startTime = DateUtils.addDays(endDay, -30);
            endTime = DateUtil.getDateBeforeZero(endDay);
        } else {
            startTime = DateUtil.parseDate(start, null, "yyyy-MM-dd");
            endTime = DateUtil.getDateBeforeZero(DateUtil.parseDate(end, null, "yyyy-MM-dd"));
        }

        SimpleResponse simpleResponse = new SimpleResponse();

        // 分页查询 排序方式
        Page<MerchantOrder> page = new Page<>(pageNum, limit);
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getMerchantId, this.merchantService.getParentMerchantId(merchantId))
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .between(MerchantOrder::getCtime, startTime, endTime)
        ;
        if (type != 0) {
            //魔方软件交易方式
            if (type != MfTradeTypeEnum.GOODS_TRADE.getCode() && type != MfTradeTypeEnum.CASH.getCode()) {
                wrapper.and(params -> params.eq("mf_trade_type", type));
                wrapper.and(params -> params.ne("type", OrderTypeEnum.USERCASH.getCode()));
            }
            //提现
            if (type == MfTradeTypeEnum.CASH.getCode()) {
                wrapper.and(params -> params.eq("type", OrderTypeEnum.USERCASH.getCode()));
            }
            //商品交易
            if (type == MfTradeTypeEnum.GOODS_TRADE.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode(),
                        MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //仅退款
            if (type == MfTradeTypeEnum.GOODS_SALE_REFUND.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_REFUND.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
            }
            //退货退款
            if (type == MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
            }
            //成为店长的收益
            if (type == MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
        }
        wrapper.orderByDesc("ctime");
        IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);

        MybatisPageDto<MerchantOrder> merchantOrderMybatisPage = new MybatisPageDto<MerchantOrder>();
        if (merchantOrderIPage.getRecords() == null || merchantOrderIPage.getRecords().size() == 0) {
            simpleResponse.setData(merchantOrderMybatisPage);
            return simpleResponse;
        }
        /***
         * 时间 2018.12.26 11:26
         * 交易类型 支付保证金 保证金结算退回 补库存 商品售出待结算 商品售出已结算 仅退款 退货退款
         * 交易金额 -10000
         * 交易明细 新人有礼活动保证金 10000元
         * 支付账户 微信号15167753095
         * 到账账户 集客魔方商户号
         * 操作人 15167753095（主账号） 15167753091（子账号）
         */
        Map<Long, String> tradeDetailMap = this.reportService.getTradeDetail(merchantOrderIPage.getRecords());
        Map<Long, String> merchantInfoMap = this.reportService.getMerchantInfo(merchantOrderIPage.getRecords());
        merchantOrderMybatisPage.setContent(new ArrayList<>());
        for (MerchantOrder merchantOrder : merchantOrderIPage.getRecords()) {
            String tradeDetailObj = tradeDetailMap.get(merchantOrder.getId());
            String merchantInfoObj = merchantInfoMap.get(merchantOrder.getId());
            if (StringUtils.isBlank(merchantInfoObj) || StringUtils.isBlank(tradeDetailObj)) {
                continue;
            }
            JSONObject tradeDetailJsonObject = JSONObject.parseObject(tradeDetailObj);
            JSONObject merchantJsonObject = JSONObject.parseObject(merchantInfoObj);
            if (merchantJsonObject == null || tradeDetailJsonObject == null) {
                continue;
            }
            merchantOrderMybatisPage.getContent().add(this.reportService.assembleSingleMerchantOrderInfo(merchantOrder, tradeDetailJsonObject, merchantJsonObject));
        }
        merchantOrderMybatisPage.from(merchantOrderIPage.getCurrent(), merchantOrderIPage.getSize(), merchantOrderIPage.getTotal(), merchantOrderIPage.getPages());
        simpleResponse.setData(merchantOrderMybatisPage);
        return simpleResponse;
    }

    /***
     * 查询清算活动金额进度情况
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    @Override
    public SimpleResponse queryBalanceOfClearingProgressInfo(Long merchantId, Long shopId, Long activityDefId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        //查询活动定义总金额
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityDefId);
        if (activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动异常了，请联系@百川");
        }
        boolean checkStatus = activityDef.getStatus() == ActivityDefStatusEnum.CLOSE.getCode() || activityDef.getStatus() == ActivityDefStatusEnum.END.getCode();
        if (!checkStatus) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "亲，该活动还在进行中哦，请先挂起");
        }
        // TODO 这里需要修改查询清算余额
        BalanceOfClearingProgressDto dto = this.covertUserMoneyTradeDetail(activityDef);
        /***
         * 自从加佣金之后，有些活动的清算方式做下修改，当为可清算状态时，直接去剩下的保证金=活动保证金+佣金
         */
//        //可结算时，做redis存储
//        if (dto.getStatus() == BalanceOfClearingProgressDto.CanCash && new BigDecimal(dto.getRemainMoney()).compareTo(BigDecimal.ZERO) > 0) {
//            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.MERCHANTORDER_REFUND + activityDefId, dto.getRemainMoney());
//        }
        if (dto == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该类型没有找到对应的活动，请联系@夜辰 defId=" + activityDefId);
        }
        if (dto.getStatus() == BalanceOfClearingProgressDto.CanCash && activityDef.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.MERCHANTORDER_REFUND + activityDefId, activityDef.getDeposit().setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        simpleResponse.setData(dto);
        return simpleResponse;
    }

    /***
     * 查询商户余额信息
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse queryMerchantBalanceInfo(Long merchantId, Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Merchant merchant = this.merchantService.getMerchant(merchantId);
        if (merchant == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        Long customerId = merchant.getId();//子账号/父账号
        Long parentId = 0L;//父账号赋值
        if (merchant.getType() == MerchantTypeEnum.PARERNT_MERCHANT.getCode()) {
            parentId = merchant.getId();
        } else {
            parentId = merchant.getParentId();
        }
        BigDecimal accountBalance = this.merchantBalanceService.sumDepositAdvance(parentId, null, shopId);
        BigDecimal customerBalance = this.merchantBalanceService.sumDepositAdvance(parentId, customerId, shopId);

        Map map = new HashMap();
        map.put("accountBalance", accountBalance.setScale(2, BigDecimal.ROUND_DOWN).toString());
        map.put("customerBalance", customerBalance.setScale(2, BigDecimal.ROUND_DOWN).toString());
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 申请结算退款
     * @param applyRefundDto
     * @return
     */
    @Override
    public SimpleResponse applyRefund(ApplyRefundDto applyRefundDto) {
        if (applyRefundDto == null && applyRefundDto.checkValid() && RegexUtils.StringIsNumber(applyRefundDto.getRefundMoney())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        BigDecimal refundMoney = new BigDecimal(applyRefundDto.getRefundMoney());
        //验证redis
        String redisStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.MERCHANTORDER_REFUND + applyRefundDto.getActivityDefId());
        if (StringUtils.isBlank(redisStr)) {
            //没存储，数据处理过程异常or还没到可结算状态
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "亲，没有可以结算的金额！");
        }
        //金额要大于0
        if (new BigDecimal(redisStr).compareTo(BigDecimal.ZERO) <= 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "亲，没有可以结算的金额！");
        }
        //金额匹配
        if (new BigDecimal(redisStr).compareTo(refundMoney) != 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "亲，退款金额跟查找的不匹配，金额处理异常！请稍等...");
        }
        //创建时的付款
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getMerchantId, this.merchantService.getParentMerchantId(applyRefundDto.getMerchantId()))
                .eq(MerchantOrder::getShopId, applyRefundDto.getShopId())
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .eq(MerchantOrder::getBizValue, applyRefundDto.getActivityDefId())
        ;
        List<MerchantOrder> merchantOrders = this.merchantOrderService.list(wrapper);
        if (CollectionUtils.isEmpty(merchantOrders)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "亲，根据活动定义未查询到订单，请联系客服进行沟通...");
        }
        //是否有补库存的付款
        List<MerchantOrder> addStockMerchantOrders = this.getAddStock(applyRefundDto.getMerchantId(), applyRefundDto.getShopId(), applyRefundDto.getActivityDefId());

        List<MerchantOrder> needDealMerchantOrders = new ArrayList<>();
        needDealMerchantOrders.addAll(addStockMerchantOrders);
        needDealMerchantOrders.addAll(merchantOrders);

        //是否有进行中的退款and退款总金额要小于订单金额
        Map<Long, List<MerchantOrder>> refundMerchantOrderMap = this.getRefundMerchantOrder(needDealMerchantOrders);
        BigDecimal refundAmount = BigDecimal.ZERO;
        log.info("<<<<<<<<结算开始 defId:{}, money:{}", applyRefundDto.getActivityDefId(), refundMoney);
        for (MerchantOrder merchantOrder : needDealMerchantOrders) {
            //查找是否存在该订单的退款-未成功的
            List<MerchantOrder> refundMerchantOrders = refundMerchantOrderMap.get(merchantOrder.getId());
            MerchantOrder refundMerchantOrder = null;
            if (!CollectionUtils.isEmpty(refundMerchantOrders)) {
                for (MerchantOrder refundMerchantOrder1 : refundMerchantOrders) {
                    if (refundMerchantOrder1.getStatus() == 0) {
                        refundMerchantOrder = refundMerchantOrder1;
                    }
                    if (refundMerchantOrder1.getStatus() == 1) {
                        refundAmount = refundAmount.add(refundMerchantOrder1.getTotalFee());
                    }
                }
            }
            if (refundMerchantOrder != null) {
                //原退款单号，再次发起退款
                SimpleResponse simpleResponse = this.wxRefund(merchantOrder, merchantOrder.getTotalFee(), refundMerchantOrder.getTotalFee(), refundMerchantOrder.getOrderNo());
                if (simpleResponse.getCode() == ApiStatusEnum.SUCCESS.getCode()) {
                    refundMoney = refundMoney.subtract(refundMerchantOrder.getTotalFee());
                    log.info("<<<<<<<<结算 defId:{}, leftMoney:{}", applyRefundDto.getActivityDefId(), refundMoney);
                    //结算了清redis
                    this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.MERCHANTORDER_REFUND_END + applyRefundDto.getActivityDefId(), "0");
                    continue;
                }
            }
            //容错，线下已经退款的账户，若手动再次点击，则更新下状态
            if (refundMerchantOrder == null && refundAmount.compareTo(merchantOrder.getTotalFee()) >= 0) {
                ActivityDefAudit activityDefAudit = this.activityDefAuditService.selectByPrimaryKey(merchantOrder.getBizValue());
                if (activityDefAudit != null) {
                    this.activityDefCheckService.refundAddStockAudit(merchantOrder.getBizValue(), refundMoney);
                    this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND + activityDefAudit.getActivityDefId());
                    this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND_END + activityDefAudit.getActivityDefId());
                    continue;
                } else {
                    this.activityDefCheckService.refundDef(merchantOrder.getBizValue(), refundMoney);
                    this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND + merchantOrder.getBizValue());
                    this.stringRedisTemplate.delete(RedisKeyConstant.MERCHANTORDER_REFUND_END + merchantOrder.getBizValue());
                }

                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "该订单已经退款...");
            }
            if (merchantOrder.getTotalFee().compareTo(refundMoney) >= 0) {
                //申请退款
                if (refundMoney.compareTo(BigDecimal.ZERO) > 0) {
                    SimpleResponse simpleResponse = this.merchantRefund(merchantOrder.getId(), refundMoney);
                    if (simpleResponse.getCode() == ApiStatusEnum.SUCCESS.getCode()) {
                        refundMoney = refundMoney.subtract(merchantOrder.getTotalFee());
                        log.info("<<<<<<<<结算 defId:{}, leftMoney:{}", applyRefundDto.getActivityDefId(), refundMoney);
                    }
                }
            } else {
                //申请退款
                SimpleResponse simpleResponse = this.merchantRefund(merchantOrder.getId(), merchantOrder.getTotalFee());
                if (simpleResponse.getCode() == ApiStatusEnum.SUCCESS.getCode()) {
                    refundMoney = refundMoney.subtract(merchantOrder.getTotalFee());
                    log.info("<<<<<<<<结算 defId:{}, leftMoney:{}", applyRefundDto.getActivityDefId(), refundMoney);
                }
            }

            //结算了清redis
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.MERCHANTORDER_REFUND_END + applyRefundDto.getActivityDefId(), "0");
            if (refundMoney.compareTo(BigDecimal.ZERO) <= 0) {
                break;
            }
        }
        return new SimpleResponse();
    }

    /***
     * 查询结算退款状态
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    @Override
    public SimpleResponse queryApplyRefundStatus(Long merchantId, Long shopId, Long activityDefId) {
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getMerchantId, this.merchantService.getParentMerchantId(merchantId))
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMREFUND.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .eq(MerchantOrder::getBizValue, activityDefId)
        ;
        List<MerchantOrder> merchantOrders = this.merchantOrderService.list(wrapper);

        boolean success = false;
        for (MerchantOrder merchantOrder : merchantOrders) {
            if (merchantOrder.getStatus() == OrderStatusEnum.ORDERED.getCode()) {
                success = true;
            }
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> map = new HashMap<>();
        map.put("isSuccess", success);
        simpleResponse.setData(map);
        return simpleResponse;
    }

    @Override
    public SimpleResponse queryInfo2buy(Long merchantId, int type) {
        SimpleResponse simpleResponse = new SimpleResponse();
        BuyInfoResp resp = new BuyInfoResp();
        if (type == 1) {
            CommonDict commonDict = this.commonDictService.selectByTypeKey("jkmfPurchase", "pay_menu");
            List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
            resp.setInfos(objects);
            simpleResponse.setData(resp);
        }
        return simpleResponse;
    }

    /***
     * 查询商户财务记录-流水
     *
     * @param merchantId
     * @param shopId
     * @param limit
     * @param offset
     * @param type 0交易(包含待结算的(包含退款待结算的)) 1：提现、收入(只包含已结算的) 2:待结算佣金管理,本月待结算 3:待结算佣金管理,下月待结算
     * @return
     */
    @Override
    public SimpleResponse queryRecordLog(Long merchantId, Long shopId,
                                         Integer limit, Integer offset, int type) {
        SimpleResponse simpleResponse = new SimpleResponse();
        AppletMerchantOrderPageDTO respDto = new AppletMerchantOrderPageDTO();
        respDto.setDetail(new AppletMybatisPageDto(limit, offset));

        List<MerchantOrder> merchantOrders = new ArrayList<>();
        long count = 0;
        // 分页查询 排序方式
        Page<MerchantOrder> page = new Page<>(offset / limit + 1, limit);
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getMerchantId, this.merchantService.getParentMerchantId(merchantId))
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getBizType, BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
        ;
        if (type == 0) {
            //累计销售额
            String sale = orderAppService.orderTotalFeeBySale(shopId, null, null);
            if (StringUtils.isNotBlank(sale)) {
                respDto.setTotalSale(sale);
            }
            //成交笔数
            Integer dealNumber = orderAppService.orderNumber(shopId, null, null);
            if (dealNumber != null) {
                respDto.setDealNum(dealNumber);
            }
            //累计佣金
            String commission = orderAppService.commissionTotal(shopId, null, null);
            if (StringUtils.isNotBlank(commission)) {
                respDto.setTotalCommission(commission);
            }

            //收益管理(待结算+已结算)
            wrapper.and(params -> params.in("type", Arrays.asList(MerchantOrderTypeEnum.MERCHANT_INCOME.getCode(),
                    MerchantOrderTypeEnum.MERCHANT_PAY.getCode())));
            wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode(),
                    MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode(), MfTradeTypeEnum.GOODS_SALE_CLEANING_INTO_BALANCE.getCode(),
                    MfTradeTypeEnum.GOODS_SALE_REFUND.getCode(), MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode())));
            wrapper.orderByDesc("ctime");
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);
            if (CollectionUtils.isEmpty(merchantOrderIPage.getRecords())) {
                simpleResponse.setData(respDto);
                return simpleResponse;
            }
            merchantOrders = merchantOrderIPage.getRecords();
            count = merchantOrderIPage.getTotal();
        } else if (type == 1) {
            Long parentMerchantId = merchantService.getParentMerchantId(merchantId);
            QueryWrapper<MerchantBalance> balanceWrapper = new QueryWrapper<>();
            balanceWrapper.lambda()
                    .eq(MerchantBalance::getMerchantId, parentMerchantId)
                    .eq(MerchantBalance::getCustomerId, merchantId)
                    .eq(MerchantBalance::getShopId, shopId)
            ;

            MerchantBalance merchantBalance = this.merchantBalanceService.getOne(balanceWrapper);
            if (merchantBalance != null) {
                respDto.setBalance(merchantBalance.getDepositAdvance().toString());
            }
            //提现+真实收入(到余额)管理
            wrapper.and(params -> params.in("type", Arrays.asList(MerchantOrderTypeEnum.MERCHANT_INCOME.getCode(),
                    MerchantOrderTypeEnum.DRAW.getCode())));
            wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.CASH.getCode(),
                    MfTradeTypeEnum.GOODS_SALE_CLEANING_INTO_BALANCE.getCode())));
            wrapper.orderByDesc("ctime");
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);
            if (CollectionUtils.isEmpty(merchantOrderIPage.getRecords())) {
                simpleResponse.setData(respDto);
                return simpleResponse;
            }
            merchantOrders = merchantOrderIPage.getRecords();
            count = merchantOrderIPage.getTotal();
        } else if (type == 2) {
            //累计佣金
            String commission = orderAppService.commissionTotal(shopId, DateUtil.getBeginOfLastMonth(), DateUtil.getEndOfThisMonth());
            if (StringUtils.isNotBlank(commission)) {
                respDto.setWillCleaning(commission);
            }
            //本月待结算，查询上个月的所有已结算订单
            Date beginTimeDate = DateUtil.getBeginOfLastMonth();
            Date endTimeDate = DateUtil.getEndOfLastMonth();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            int day = cal.get(Calendar.DATE);
            String afterIndex = "待结算";
            if (day > 20) {
                afterIndex = "已结算";
            }
            Date thisMonth = new Date();
            respDto.setThisMonth(DateFormatUtils.format(thisMonth, "MM") + "月20日" + afterIndex);
            respDto.setNextMonth(DateFormatUtils.format(DateUtils.addMonths(thisMonth, 1), "MM") + "月20日待结算");

            wrapper.and(params -> params.eq("type", MerchantOrderTypeEnum.MERCHANT_INCOME.getCode()));
            wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode(),
                    MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode())));
            wrapper.and(params -> params.between("ctime", beginTimeDate, endTimeDate));
            wrapper.orderByDesc("ctime");
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);
            if (CollectionUtils.isEmpty(merchantOrderIPage.getRecords())) {
                simpleResponse.setData(respDto);
                return simpleResponse;
            }
            merchantOrders = merchantOrderIPage.getRecords();
            count = merchantOrderIPage.getTotal();
        } else if (type == 3) {
            //累计佣金
            String commission = orderAppService.commissionTotal(shopId, null, null);
            if (StringUtils.isNotBlank(commission)) {
                respDto.setWillCleaning(commission);
            }

            //下月待结算，查询本月的所有待结算(不包含退款)&已结算订单
            Date beginTimeDate = DateUtil.getBeginOfThisMonth();
            Date endTimeDate = DateUtil.getEndOfThisMonth();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            int day = cal.get(Calendar.DATE);
            String afterIndex = "待结算";
            if (day > 20) {
                afterIndex = "已结算";
            }

            Date thisMonth = new Date();
            respDto.setThisMonth(DateFormatUtils.format(thisMonth, "MM") + "月20日" + afterIndex);
            respDto.setNextMonth(DateFormatUtils.format(DateUtils.addMonths(thisMonth, 1), "MM") + "月20日待结算");

            Map params = new HashMap();
            params.put("merchantId", merchantId);
            params.put("shopId", shopId);
            params.put("type", MerchantOrderTypeEnum.MERCHANT_INCOME.getCode());
            params.put("bizType", BizTypeEnum.DOUYIN_SHOP_SHOPPING.getCode());
            params.put("beginTimeDate", DateFormatUtils.format(beginTimeDate, "yyyy-MM-dd HH:mm:ss"));
            params.put("endTimeDate", DateFormatUtils.format(endTimeDate, "yyyy-MM-dd HH:mm:ss"));
            params.put("offset", offset);
            params.put("size", limit);
            List list = merchantOrderService.queryPageByParams(params);
            merchantOrders = list;
            if (CollectionUtils.isEmpty(merchantOrders)) {
                simpleResponse.setData(respDto);
                return simpleResponse;
            }
            count = merchantOrderService.countByParams(params);
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "暂不支持的数据查询类型....难住小抖我了哟-_-");
        }

        if (CollectionUtils.isEmpty(merchantOrders)) {
            simpleResponse.setData(respDto);
            return simpleResponse;
        }
        List<Long> orderIds = new ArrayList<>();
        for (MerchantOrder merchantOrder : merchantOrders) {
            if (orderIds.indexOf(merchantOrder.getBizValue()) == -1) {
                orderIds.add(merchantOrder.getBizValue());
            }
        }
        Map<Long, Order> orderMap = orderService.getOrderListByOrderIds(orderIds);

        /***
         * ----------收益管理-----------
         * 时间 2018.12.26 11:26
         * 订单号
         * 销售额
         * 赚取佣金
         * ----------提现管理-----------
         * 时间
         * 类型 提现支出，佣金收入
         * --------待结算佣金管理--------
         * 时间
         * 订单号
         * 销售额
         * 赚取佣金
         */
        for (MerchantOrder merchantOrder : merchantOrders) {
            AppletMerchantOrderPageDTO.AppletMerchantOrderDTO merchantOrderDTO = new AppletMerchantOrderPageDTO.AppletMerchantOrderDTO();
            Order order = null;
            if (type != 1) {
                order = orderMap.get(merchantOrder.getBizValue());
                if (order == null) {
                    log.error("<<<<<<<高级警报异常，查询结果集缺失 orderId:{} order:{}", merchantOrder.getBizValue(), order);
                    continue;
                }
            } else {
                if (merchantOrder.getType() == MerchantOrderTypeEnum.DRAW.getCode()) {
                    merchantOrderDTO.setType(1);
                } else {
                    merchantOrderDTO.setType(2);
                }
            }
            merchantOrderDTO.setCtime(merchantOrder.getCtime());
            if (order != null) {
                merchantOrderDTO.setSale(order.getFee().multiply(new BigDecimal(String.valueOf(order.getNumber()))).toString());
                merchantOrderDTO.setOrderNo(order.getOrderNo());
            }
            String index = "";
            //收益管理
            if (type == 0) {
                index = "+";
                if (merchantOrder.getType() == MerchantOrderTypeEnum.MERCHANT_PAY.getCode()) {
                    index = "-";
                    merchantOrderDTO.setOrderNo(merchantOrder.getOrderNo().split("@")[0]);
                }
            }
            merchantOrderDTO.setMoney(index + merchantOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());
            respDto.getDetail().getContent().add(merchantOrderDTO);
        }
        boolean pullDown = false;
        //是否还有下一页
        if (count > offset + limit) {
            pullDown = true;
        }
        respDto.getDetail().setPullDown(pullDown);
        simpleResponse.setData(respDto);
        return simpleResponse;
    }

    /*******************************私有方法**********************************************************************/
    /***
     * 映射清算活动金额converter
     * @param activityDef
     */
    private BalanceOfClearingProgressDto covertUserMoneyTradeDetail(ActivityDef activityDef) {
        if (!CollectionUtils.isEmpty(this.balanceOfClearingProgressConverters)) {
            for (BalanceOfClearingProgressConverter converter : this.balanceOfClearingProgressConverters) {
                BalanceOfClearingProgressDto dto = converter.converterBalanceOfClearingProgress(activityDef);
                if (dto != null) {
                    return dto;
                }
            }
        }
        return null;
    }

    /***
     * 获取退款(已经and正在)的所有订单
     * @param needDealMerchantOrders
     * @return
     */
    private Map<Long, List<MerchantOrder>> getRefundMerchantOrder(List<MerchantOrder> needDealMerchantOrders) {
        Long merchantId = needDealMerchantOrders.get(0).getMerchantId();
        Long shopId = needDealMerchantOrders.get(0).getShopId();
        List<Long> refundMerchantOrderIds = new ArrayList<>();
        for (MerchantOrder merchantOrder : needDealMerchantOrders) {
            if (refundMerchantOrderIds.indexOf(merchantOrder.getId()) == -1) {
                refundMerchantOrderIds.add(merchantOrder.getId());
            }
        }
        QueryWrapper<MerchantOrder> refundWrapper = new QueryWrapper<>();
        refundWrapper.lambda()
                .eq(MerchantOrder::getMerchantId, this.merchantService.getParentMerchantId(merchantId))
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMREFUND.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .in(MerchantOrder::getStatus, Arrays.asList(OrderStatusEnum.ORDERING.getCode(), OrderStatusEnum.ORDERED.getCode()))
                .in(MerchantOrder::getRefundOrderId, refundMerchantOrderIds)
        ;
        List<MerchantOrder> refundMerchantOrders = this.merchantOrderService.list(refundWrapper);
        Map<Long, List<MerchantOrder>> map = new HashMap<>();
        if (!CollectionUtils.isEmpty(refundMerchantOrders)) {
            for (MerchantOrder merchantOrder : refundMerchantOrders) {
                List<MerchantOrder> merchantOrders = map.get(merchantOrder.getRefundOrderId());
                if (CollectionUtils.isEmpty(merchantOrders)) {
                    merchantOrders = new ArrayList<>();
                }
                merchantOrders.add(merchantOrder);
                map.put(merchantOrder.getRefundOrderId(), merchantOrders);
            }
        }
        return map;
    }

    /***
     * 补库存的订单
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    private List<MerchantOrder> getAddStock(Long merchantId, Long shopId, Long activityDefId) {
        QueryWrapper<ActivityDefAudit> activityDefAuditWrapper = new QueryWrapper<>();
        activityDefAuditWrapper.eq("activity_def_id", activityDefId);
        activityDefAuditWrapper.eq("status", ActivityDefAuditStatusEnum.PASS.getCode());
        activityDefAuditWrapper.eq("del", DelEnum.NO.getCode());
        List<ActivityDefAudit> activityDefAudits = this.activityDefAuditService.list(activityDefAuditWrapper);
        if (CollectionUtils.isEmpty(activityDefAudits)) {
            return new ArrayList<>();
        }
        List<Long> activityDefAuditIds = new ArrayList<>();
        for (ActivityDefAudit activityDefAudit : activityDefAudits) {
            if (activityDefAuditIds.indexOf(activityDefAudit.getId()) == -1) {
                activityDefAuditIds.add(activityDefAudit.getId());
            }
        }
        if (CollectionUtils.isEmpty(activityDefAuditIds)) {
            return new ArrayList<>();
        }
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getMerchantId, this.merchantService.getParentMerchantId(merchantId))
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getType, OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode())
                .eq(MerchantOrder::getPayType, OrderPayTypeEnum.WEIXIN.getCode())
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .in(MerchantOrder::getBizValue, activityDefAuditIds)
        ;
        List<MerchantOrder> merchantOrders = this.merchantOrderService.list(wrapper);

        return merchantOrders;
    }

    /***
     * 预存金退款
     * @param merchantOrder
     * @return
     */
    private SimpleResponse prestoreRefund(MerchantOrder merchantOrder, BigDecimal refundTotalAmount) {
        SimpleResponse d = new SimpleResponse();
        List<CustomerJsonVo> customerJsonVos = this.getCustomers(merchantOrder.getCustomerJson());
        if (customerJsonVos == null || customerJsonVos.size() == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("退款的员工不存在");
            return d;
        }
        BigDecimal remainAmount;
        for (CustomerJsonVo customerJsonVo : customerJsonVos) {
            Map<String, Object> params = new HashMap<>();
            params.put("merchant_id", merchantOrder.getMerchantId());
            params.put("shop_id", merchantOrder.getShopId());
            params.put("customer_id", customerJsonVo.getCustomerId());
            QueryWrapper<MerchantBalance> wrapper = new QueryWrapper<>();
            wrapper.allEq(params);
            MerchantBalance merchantBalance = this.merchantBalanceService.getOne(wrapper);
            if (merchantBalance == null) {
                continue;
            }
            remainAmount = refundTotalAmount.subtract(customerJsonVo.getDepositAdvance());
            if (remainAmount.compareTo(BigDecimal.ZERO) < 0) {
                if (refundTotalAmount.compareTo(BigDecimal.ZERO) > 0) {
                    this.merchantBalanceService.addUpdateByVersionId(merchantBalance, refundTotalAmount, BigDecimal.ZERO);
                }
                break;
            }
            this.merchantBalanceService.addUpdateByVersionId(merchantBalance, customerJsonVo.getDepositAdvance(), BigDecimal.ZERO);
        }
        if (merchantOrder.getBizType() == BizTypeEnum.AD.getCode()) {
            //TODO: 百川退款后的操作-广告
        } else if (merchantOrder.getBizType() == BizTypeEnum.NEWMAN_ORDER_ACTIVITY.getCode()) {
            //TODO:百川退款后的操作-活动定义
        } else if (merchantOrder.getBizType() == BizTypeEnum.PLAN_ACTIVITY.getCode()) {
            //TODO:曲成退款后的操作--活动计划
        }
        return d;
    }

    /***
     * wx退款
     * @param merchantOrder
     * @param refundTotalAmount
     * @return
     */
    private SimpleResponse wxRefund(MerchantOrder merchantOrder, BigDecimal initTotalAmount, BigDecimal refundTotalAmount, String refundOrderNo) {
        SimpleResponse d = new SimpleResponse();
        List<CustomerJsonVo> customerJsonVos = this.getCustomers(merchantOrder.getCustomerJson());
        if (customerJsonVos == null || customerJsonVos.size() == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("退款的员工不存在");
            return d;
        }
        //开始申请退款
        Map<String, Object> param = weixinPayService.getRefundPayOrderReqParams(merchantOrder.getOrderNo(), refundOrderNo,
                initTotalAmount, refundTotalAmount, WeixinPayConstants.SELLER_NOTIFY_SOURCE);
        Map<String, Object> result = weixinPayService.refundPayOrder(param);
        log.info("<<<<<<<<<< refund resp: {} req: {}", result, param);
        String WX_RETURN_CODE_VAL = "FAIL";
        String WX_RETURN_CODE_KEY = "return_code";
        if (result.get(WX_RETURN_CODE_KEY) != null && WX_RETURN_CODE_VAL.equals(result.get(WX_RETURN_CODE_KEY))
                || result.get("result_code") != null && WX_RETURN_CODE_VAL.equals(result.get("result_code"))) {
            String errMsg = null;
            if (result.get("err_code") != null && StringUtils.isNotBlank(result.get("err_code").toString())) {
                errMsg = result.get("err_code").toString();
                if (result.get("err_code_des") != null && StringUtils.isNotBlank(result.get("err_code_des").toString())) {
                    errMsg = result.get("err_code_des").toString();
                }
            } else if (result.get("return_msg") != null && StringUtils.isNotBlank(result.get("return_msg").toString())) {
                errMsg = result.get("return_msg").toString();
            }
            return new SimpleResponse(-1, "调用微信申请退款接口有误,微信返回代码:" + result == null ? "" : errMsg);
        }
        return new SimpleResponse(0, "success");
    }

    /***
     * 获取订单的所属员工集合
     * @param merchantOrderCustomerJson
     * @return
     */
    private List<CustomerJsonVo> getCustomers(String merchantOrderCustomerJson) {
        if (StringUtils.isBlank(merchantOrderCustomerJson)) {
            return new ArrayList<CustomerJsonVo>();
        }
        return JSONObject.parseArray(merchantOrderCustomerJson, CustomerJsonVo.class);
    }

    /***
     * 预存金支付方式
     * @param merchantOrder
     * @param merchantId
     * @param shopId
     * @param amount
     * @return
     */
    private SimpleResponse prestorePayType(MerchantOrder merchantOrder,
                                           Long merchantId,
                                           Long shopId,
                                           String amount,
                                           Long customerId) {
        SimpleResponse d = new SimpleResponse();
        List<Object> customerList = new ArrayList<>();

        MerchantBalance cusMerchantBalance = this.merchantBalanceService.query(merchantId, shopId, customerId);
        //子账号是否满足消费条件
        if (cusMerchantBalance != null && new BigDecimal(amount).compareTo(cusMerchantBalance.getDepositAdvance()) <= 0) {
            //支付的预存金 new BigDecimal(amount)
            Map<String, Object> customerSelfMap = this.getCustomerMap(cusMerchantBalance, new BigDecimal(amount), true);
            if (CollectionUtils.isEmpty(customerSelfMap)) {
                //更新失败
                log.warn("更新失败{}", cusMerchantBalance);
            } else {
                customerList.add(customerSelfMap);
            }
        }
        if (CollectionUtils.isEmpty(customerList)) {
            BigDecimal prestoreAmount = this.merchantBalanceService.sumDepositAdvance(merchantId, null, shopId);
            if (prestoreAmount.compareTo(new BigDecimal(amount)) <= 0) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                d.setMessage("预存金不充足，无法付款");
                return d;
            }
            List<MerchantBalance> merchantBalances = this.merchantBalanceService.queryList(merchantId, shopId);
            if (merchantBalances == null || merchantBalances.size() == 0) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                d.setMessage("没有预存金，无法付款");
                return d;
            }

            prestoreAmount = BigDecimal.ZERO;
            //先清算自己的预存金
            if (cusMerchantBalance.getDepositAdvance().compareTo(BigDecimal.ZERO) > 0) {
                prestoreAmount = prestoreAmount.add(cusMerchantBalance.getDepositAdvance());

                Map<String, Object> customerSelfMap = this.getCustomerMap(cusMerchantBalance, cusMerchantBalance.getDepositAdvance(), true);
                if (CollectionUtils.isEmpty(customerSelfMap)) {
                    //更新失败
                    log.warn("更新失败{}", cusMerchantBalance);
                    //防止并发(更新失败,预存金重置处理)
                    prestoreAmount = BigDecimal.ZERO;
                } else {
                    customerList.add(customerSelfMap);
                }
            }

            for (MerchantBalance merchantBalance : merchantBalances) {
                Map<String, Object> customerMap = new HashMap<>();
                if (merchantBalance.getCustomerId().longValue() == customerId.longValue()) {
                    continue;
                }
                //余额要大于0
                if (merchantBalance.getDepositAdvance().compareTo(BigDecimal.ZERO) <= 0) {
                    continue;
                }
                //统计金额
                prestoreAmount = prestoreAmount.add(merchantBalance.getDepositAdvance());
                //当预存金够时，跳出循环
                if (new BigDecimal(amount).compareTo(prestoreAmount) <= 0) {
                    //剩余的预存金
                    BigDecimal lastCustomerPrestore = prestoreAmount.subtract(new BigDecimal(amount));
                    //支付的预存金
                    BigDecimal preAmount = merchantBalance.getDepositAdvance().subtract(lastCustomerPrestore);
                    customerMap = this.getCustomerMap(merchantBalance, preAmount, false);
                    if (CollectionUtils.isEmpty(customerMap)) {
                        //更新失败
                        log.warn("更新失败{}", cusMerchantBalance);
                        prestoreAmount.subtract(merchantBalance.getDepositAdvance());
                        continue;
                    } else {
                        customerList.add(customerMap);
                    }
                    break;
                }

                customerMap = this.getCustomerMap(merchantBalance, merchantBalance.getDepositAdvance(), false);
                if (CollectionUtils.isEmpty(customerMap)) {
                    //更新失败
                    log.warn("更新失败{}", cusMerchantBalance);
                    prestoreAmount.subtract(merchantBalance.getDepositAdvance());
                } else {
                    customerList.add(customerMap);
                }
            }
        }
        if (CollectionUtils.isEmpty(customerList)) {
            throw new IllegalArgumentException("预存金不足以支付~");
        }
        merchantOrder.setCustomerJson(JSON.toJSONString(customerList));
        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
        merchantOrder.setPayType(OrderPayTypeEnum.CASH.getCode());
        merchantOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
        this.merchantOrderService.save(merchantOrder);

        if (merchantOrder.getBizType() == BizTypeEnum.AD.getCode()) {
            //百川退款后的操作-广告
            this.adSellerService.updatePurchaseStatus(merchantOrder.getBizValue());
        } else if (merchantOrder.getBizType() == BizTypeEnum.NEWMAN_ORDER_ACTIVITY.getCode()) {
            //TODO:百川退款后的操作-活动定义
        } else if (merchantOrder.getBizType() == BizTypeEnum.PLAN_ACTIVITY.getCode()) {
            //TODO:曲成退款后的操作--活动计划
        }

        Map<String, Object> respMap = new HashMap<>();
        respMap.put("code_url", "");
        respMap.put("prepay_id", "");
        respMap.put("orderId", merchantOrder.getId());
        d.setData(respMap);
        return d;
    }

    /***
     * 支付的安全性验证
     * @param dto
     * @return
     */
    private SimpleResponse safetyVerification4pay(MerchantOrderReq dto) {
        if (dto.getMerchantId() == null || dto.getMerchantId() <= 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        Merchant merchant = this.merchantService.getMerchant(dto.getMerchantId());
        if (merchant == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        dto.setCustomerId(merchant.getId());//子账号/父账号
        if (merchant.getType() == MerchantTypeEnum.PARERNT_MERCHANT.getCode()) {
            dto.setMerchantId(merchant.getId());//父账号赋值
        } else {
            dto.setMerchantId(merchant.getParentId());//父账号赋值
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "success");
    }

    /***
     * 提现的安全性验证
     * @param dto
     * @return
     */
    private SimpleResponse safetyVerification4cash(MerchantOrderMktTransferReq dto) {
        if (dto.getMerchantId() == null || dto.getMerchantId() <= 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        Merchant merchant = this.merchantService.getMerchant(dto.getMerchantId());
        if (merchant == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        dto.setCustomerId(merchant.getId());//子账号/父账号
        if (merchant.getType() == MerchantTypeEnum.PARERNT_MERCHANT.getCode()) {
            dto.setMerchantId(merchant.getId());//父账号赋值
        } else {
            dto.setMerchantId(merchant.getParentId());//父账号赋值
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "success");
    }

    /***
     * 存储map，更新余额表
     * @param merchantBalance 余额表
     * @param commitAmount 支付的金额
     * @param isYourself 是否是本人
     * @return
     */
    private Map<String, Object> getCustomerMap(MerchantBalance merchantBalance,
                                               BigDecimal commitAmount,
                                               boolean isYourself) {
        Map<String, Object> customerMap = new HashMap<>();
        int resp = this.merchantBalanceService.lessUpdateByVersionId(merchantBalance, commitAmount, BigDecimal.ZERO);
        if (resp == 1) {
            customerMap = this.setCustomerJson(commitAmount.toString(), merchantBalance.getCustomerId(), isYourself);
        }
        return customerMap;
    }

    /***
     * 微信扫码下单方式
     * @param merchantOrder
     * @param dto
     * @param outTraderNo
     * @param ipAddress
     * @param attach
     * @param openID
     * @return
     */
    private SimpleResponse wxPayType(MerchantOrder merchantOrder,
                                     MerchantOrderReq dto,
                                     String outTraderNo,
                                     String ipAddress,
                                     String attach,
                                     String openID) {
        SimpleResponse d = new SimpleResponse();

        String body = this.getWxPayBody(dto);

        //灰度设置,若该商户是灰度版本内人员，则付款金额为0.01
//        CommonDict commonDict = commonDictService.selectByTypeKey("mobile_pay", "paychneel");
//        if(commonDict != null && StringUtils.isNotBlank(commonDict.getValue())) {
//            List<String> merchantIds = Arrays.asList(commonDict.getValue());
//            for (String str : merchantIds) {
//                if (str.indexOf(String.valueOf(dto.getMerchantId())) != -1) {
//                    dto.setAmount("0.01");
//                    merchantOrder.setTotalFee(new BigDecimal(dto.getAmount()));
//                }
//            }
//        }

        Map<String, Object> unifiedorderReqParams = new HashMap<>();
        if (dto.getPayPlatform() == 1) {
            unifiedorderReqParams = this.weixinPayService.getUnifiedorderReqParams(new BigDecimal(dto.getAmount()), outTraderNo, ipAddress,
                    wxpayProperty.getPubAppId(), WeixinPayConstants.NATIVE, attach, openID, WeixinPayConstants.SELLER_NOTIFY_SOURCE, body);
        } else {
            unifiedorderReqParams = this.weixinPayService.getUnifiedorderReqParamsH5(new BigDecimal(dto.getAmount()), outTraderNo, ipAddress,
                    wxpayProperty.getPubAppId(), attach, openID, WeixinPayConstants.SELLER_NOTIFY_SOURCE, body, dto.getPayPlatform());
        }
        if (CollectionUtils.isEmpty(unifiedorderReqParams)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10.getCode(), "微信传参异常");
        }
        merchantOrder.setReqJson(JSON.toJSONString(unifiedorderReqParams));
        if (MerchantOrderReq.payment_wx.equals(dto.getPayType())) {
            merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
            merchantOrder.setPayType(OrderPayTypeEnum.WEIXIN.getCode());
        } else {
            merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode());
            merchantOrder.setPayType(OrderPayTypeEnum.WEIXIN.getCode());
        }
        this.merchantOrderService.save(merchantOrder);

        //step5: 拼接调用微信平台统一下单接口的参数
        //统一下单的返回
        Map<String, Object> unifiedorderRespMap = this.weixinPayService.unifiedorder(unifiedorderReqParams);
        log.info("小程序支付后的结果 xmlMap:{}", unifiedorderRespMap);
        if (unifiedorderRespMap == null || (StringUtils.isNotBlank(unifiedorderRespMap.get("return_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("return_code").toString()))
                || (StringUtils.isNotBlank(unifiedorderRespMap.get("result_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("result_code").toString()))) {
            if (StringUtils.isNotBlank(unifiedorderRespMap.get("return_msg").toString())) {
                d.setMessage(unifiedorderRespMap.get("return_msg").toString());
                merchantOrder.setRemark(unifiedorderRespMap.get("return_msg").toString());
            } else if (StringUtils.isNotBlank(unifiedorderRespMap.get("err_code_des").toString())) {
                d.setMessage(unifiedorderRespMap.get("err_code_des").toString());
                merchantOrder.setRemark(unifiedorderRespMap.get("err_code_des").toString());
            } else {
                d.setMessage("微信充值异常...");
                merchantOrder.setRemark("微信充值异常...");
            }
            //更新db
            merchantOrder.setUtime(new Date());
            this.merchantOrderService.updateById(merchantOrder);

            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            return d;
        }

        Map<String, Object> respMap = new HashMap<>();
        if (unifiedorderRespMap.get("code_url") != null) {
            respMap.put("code_url", unifiedorderRespMap.get("code_url"));
        }
        if (unifiedorderRespMap.get("mweb_url") != null) {
            respMap.put("mweb_url", unifiedorderRespMap.get("mweb_url"));
        }
        respMap.put("prepay_id", unifiedorderRespMap.get("prepay_id"));
        respMap.put("orderId", merchantOrder.getId());

        int timeoutMinute = 30;//30分钟过期
        if (dto.getPayPlatform() != 1) {
            timeoutMinute = 4;//官方内定5分钟过期(h5支付)
        }
        respMap.put("cutoffTime", DateUtils.addMinutes(new Date(), timeoutMinute));
        d.setData(respMap);
        //存储redis 30分钟过期
        this.stringRedisTemplate.opsForValue().set(this.getMerchantOrderRedisKey(dto), JSON.toJSONString(d), timeoutMinute, TimeUnit.MINUTES);
        return d;
    }


    /***
     * 获取交易的body
     * @param dto
     * @return
     */
    private String getWxPayBody(MerchantOrderReq dto) {
        String body = WeixinPayConstants.BODY;
        if (dto != null) {
            String bizType = "";
            if (dto.getBizType() != null && dto.getBizType() > 0 && dto.getBizType() != BizTypeEnum.ACTIVITY_AUDIT.getCode()) {
                bizType = BizTypeEnum.findByDesc(dto.getBizType());
            }
            if (dto.getActivityAdId() != null) {
                bizType = BizTypeEnum.findByDesc(BizTypeEnum.AD.getCode());
            }

            boolean notNullTradeTypePay = dto.getTradeType() != null && dto.getTradeType().equals(MerchantOrderReq.trade_type_pay);
            boolean notNullTradeTypeAddStock = dto.getTradeType() != null && dto.getTradeType().equals(MerchantOrderReq.trade_type_addStock);
            if (notNullTradeTypePay) {
                if (dto.getBizType() == BizTypeEnum.FENDUODUO_PROXY.getCode()) {
                    body = body + "-" + WeixinPayConstants.MERCHANTSELLER_BODY_BUY;
                } else if (dto.getBizType() == BizTypeEnum.PURCHASE_PUBLIC.getCode()) {
                    body = body + "-" + WeixinPayConstants.MERCHANTSELLER_BODY_BUY;
                } else {
                    body = body + "-" + WeixinPayConstants.MERCHANTSELLER_BODY_CREATEPAY;
                }
            } else if (notNullTradeTypeAddStock) {
                if (dto.getBizType() == BizTypeEnum.FENDUODUO_PROXY.getCode()) {
                    body = body + "-" + WeixinPayConstants.MERCHANTSELLER_BODY_RENEW;
                } else {
                    body = body + "-" + WeixinPayConstants.MERCHANTSELLER_BODY_ADDSTOCKPAY;
                }
            }

            if (StringUtils.isNotBlank(bizType)) {
                body = body + "-" + bizType;
            }
        }
        return body;
    }

    /***
     * 填充merchantOrder
     * @param merchantOrder
     * @param dto
     * @return
     */
    private MerchantOrder fillMerchantOrderBizTypeAndBizValue(MerchantOrder merchantOrder,
                                                              MerchantOrderReq dto) {
        if (dto.getActivityAdId() != null && dto.getActivityAdId() > 0) {
            merchantOrder.setBizType(BizTypeEnum.AD.getCode());
            merchantOrder.setBizValue(dto.getActivityAdId());
        } else {
            if (dto.getBizValue() > 0) {
                merchantOrder.setBizValue(dto.getBizValue());
            } else {
                merchantOrder.setBizValue(0L);
            }
            if (dto.getBizType() != null && dto.getBizType() > 0) {
                merchantOrder.setBizType(dto.getBizType());
            } else {
                merchantOrder.setBizType(0);
            }
        }
        // 为了调整店铺购买流程，暂且修改remark为空
        if (merchantOrder.getBizType() == BizTypeEnum.PURCHASE_PUBLIC.getCode()
                || merchantOrder.getBizType() == BizTypeEnum.PURCHASE_PRIVATE.getCode()
                || merchantOrder.getBizType() == BizTypeEnum.LEVELUP_PRIVATE.getCode()
                || merchantOrder.getBizType() == BizTypeEnum.PURCHASE_CLEARANCE.getCode()
                || merchantOrder.getBizType() == BizTypeEnum.PURCHASE_WEIGHTING.getCode()) {
            merchantOrder.setRemark("");
        }
        return merchantOrder;
    }

    /***
     * 获取rediskey
     * @param dto
     * @return
     */
    private String getMerchantOrderRedisKey(MerchantOrderReq dto) {
        if (dto.getCustomerId() == null) {
            dto.setCustomerId(0L);
        }
        Long bizValue = 0L;
        Integer bizType = 0;
        if (dto.getActivityAdId() != null && dto.getActivityAdId() > 0) {
            bizValue = dto.getActivityAdId();
            bizType = BizTypeEnum.AD.getCode();
        }
        if (dto.getBizValue() > 0 && dto.getBizType() != null && dto.getBizType() > 0) {
            bizValue = dto.getBizValue();
            bizType = dto.getBizType();

        }
        Long shopId = 0L;
        if (dto.getShopId() != null && dto.getShopId() > 0) {
            shopId = dto.getShopId();
        }
        return MERCHANTORDERREDISKEY + dto.getMerchantId() + shopId + dto.getCustomerId() + bizValue + bizType + dto.getAmount();
    }

    private Map<String, Object> setCustomerJson(String customerPerstore, Long customerId, boolean isStart) {
        Map<String, Object> customerMap = new HashMap<>();
        customerMap.put("deposit_advance", customerPerstore);
        customerMap.put("customer_id", customerId);
        customerMap.put("is_start", isStart);
        return customerMap;
    }

    /***
     * 下订单时，验证金额的有效性
     * @param dto
     * @return
     */
    private Boolean checkCreateAmount(MerchantOrderReq dto) {
        if (debugMode == 0) {
            //若是该模式不验证金额概念
            return true;
        }
        return this.checkCreateAmountConverters.convert(dto);
    }
}
