package mf.code.api.seller.service.wxPayCallbackBus.impl;

import mf.code.api.seller.service.wxPayCallbackBus.WxPayCallbackBusConverter;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.seller.service.wxPayCallbackBus.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 13:50
 */
@Service
public class PurchasePublicCallbackConverter implements WxPayCallbackBusConverter {
    @Autowired
    private MerchantOrderService merchantOrderService;

    @Override
    public String convert(MerchantOrder merchantOrder) {
        if (merchantOrder.getBizType() == BizTypeEnum.PURCHASE_PUBLIC.getCode()) {
            // 这里关于绑定店铺的需要，修改为空字符串
            merchantOrder.setRemark("");
            merchantOrderService.updateById(merchantOrder);

            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }

//        Map<String, Object> jsonMap = new HashMap<>();
//        if (merchantOrder.getBizType() == BizTypeEnum.PURCHASE_PUBLIC.getCode()) {
//            //获取字典表信息
//            CommonDict commonDict = this.commonDictService.selectByTypeKey("jkmfPurchase", "pay_menu");
//            List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
//            if (!CollectionUtils.isEmpty(objects)) {
//                for (Object obj : objects) {
//                    //[{"money":"588.00","name":"购买公有版","rate":100},{"money":"5888.00","name":"购买私有版","rate":100}]
//                    jsonMap = JSONObject.parseObject(obj.toString(), Map.class);
//                    boolean bizVersion = jsonMap != null && merchantOrder.getBizValue().equals(Long.valueOf(jsonMap.get("bizVersion").toString()));
//                    if (bizVersion) {
//                        break;
//                    }
//                }
//            }
//            //获取购买周期
//            int months = 0;
//            if (jsonMap != null && jsonMap.get("purchaseCycle") != null) {
//                months = Integer.valueOf(jsonMap.get("purchaseCycle").toString());
//            }
//
//            //购买集客魔方
//            MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(merchantOrder.getShopId());
//            if (merchantShop == null) {
//                return ApiStatusEnum.SUCCESS.getMessage();
//            }
//            //更新存储店铺
//            this.updateMerchantShop(merchantShop, months);
//            Merchant merchant = this.merchantService.getMerchant(merchantShop.getMerchantId());
//            if (merchant == null) {
//                return ApiStatusEnum.SUCCESS.getMessage();
//            }
//            //更新存储商户
//            this.updateMerchant(merchant, merchantShop);
//
//            //集客师概念，若该商户是被邀请进来的支付，则给邀请的人奖金
//            this.updateTeacherAbout(merchant.getId(), merchantShop, merchantOrder.getTotalFee());
//
//            return ApiStatusEnum.SUCCESS.getMessage();
//        }
//        return null;
//    }

//    private void updateMerchant(Merchant merchant, MerchantShop merchantShop) {
//        List<Map<String, Object>> list = new ArrayList<>();
//        if (StringUtils.isNotBlank(merchant.getPurchaseVersionJson())) {
//            Map<String, Object> map = new HashMap<>();
//            List<Object> objects = JSONObject.parseArray(merchant.getPurchaseVersionJson(), Object.class);
//            if (!CollectionUtils.isEmpty(objects)) {
//                for (Object object : objects) {
//                    Map<String, Object> jsonMap = JSONObject.parseObject(object.toString(), Map.class);
//                    if (!merchantShop.getPurchaseVersion().equals(Integer.valueOf(jsonMap.get("version").toString()))) {
//                        map.put("version", merchantShop.getPurchaseVersion());
//                        list.add(map);
//                    } else {
//                        list.add(jsonMap);
//                    }
//                }
//            }
//        } else {
//            Map<String, Object> map = new HashMap<>();
//            map.put("version", merchantShop.getPurchaseVersion());
//            list.add(map);
//        }
//        this.merchantService.updateById(merchant);
//    }
//
//    private void updateMerchantShop(MerchantShop merchantShop, int months) {
//        merchantShop.setPurchaseVersion(MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode());
//        Map<String, Object> purchase = new HashMap<>();
//        int addDays = 0;
//        if (StringUtils.isNotBlank(merchantShop.getPurchaseJson())) {
//            Map<String, Object> merchantShopJson = JSONObject.parseObject(merchantShop.getPurchaseJson(), Map.class);
//            if (merchantShopJson != null && merchantShopJson.get("expireTime") != null && merchantShopJson.get("purchaseTime") != null) {
//                addDays = (int) DateUtil.dayDiff(new Date(), DateUtil.parseDate(merchantShopJson.get("expireTime").toString(), null, "yyyy-MM-dd HH:mm:ss"));
//            }
//
//            Date expire = DateUtils.addDays(DateUtils.addMonths(new Date(), months), addDays);
//            purchase.put("purchaseTime", merchantShopJson.get("purchaseTime"));
//            purchase.put("expireTime", DateFormatUtils.format(expire, "yyyy-MM-dd HH:mm:ss"));
//        } else {
//            purchase.put("purchaseTime", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
//            purchase.put("expireTime", DateFormatUtils.format(DateUtils.addMonths(new Date(), months), "yyyy-MM-dd HH:mm:ss"));
//        }
//        merchantShop.setPurchaseJson(JSONObject.toJSONString(purchase));
//        merchantShop.setPurchaseVersion(MerchantShopPurchaseVersionEnum.VERSION_PUBLIC.getCode());
//        merchantShop.setUtime(new Date());
//        this.merchantShopService.updateById(merchantShop);
//    }
//
//    private void updateTeacherAbout(Long merchantId, MerchantShop merchantShop, BigDecimal totalFee) {
//
//        QueryWrapper<TeacherInvite> teacherInviteQueryWrapper = new QueryWrapper<>();
//        teacherInviteQueryWrapper.lambda()
//                .eq(TeacherInvite::getMerchantId, merchantId)
//                .eq(TeacherInvite::getBizType, TeacherInviteBizTypeEnum.RECRUIT_MERCHANT.getCode())
//                .orderByDesc(TeacherInvite::getId)
//        ;
//        List<TeacherInvite> teacherInvites = this.teacherInviteService.list(teacherInviteQueryWrapper);
//        if (CollectionUtils.isEmpty(teacherInvites)) {
//            return;
//        }
//        //确保幂等性
//        for (TeacherInvite teacherInvite : teacherInvites) {
//            if (teacherInvite.getStatus().equals(TeacherInviteStatusEnum.EFFECTIVE.getCode())) {
//                //若已经邀请过，则返回
//                return;
//            }
//        }
//        //更新邀请信息，
//        TeacherInvite teacherInvite40 = teacherInvites.get(0);
//        teacherInvite40.setStatus(TeacherInviteStatusEnum.EFFECTIVE.getCode());
//        teacherInvite40.setBizValue(merchantShop.getId());
//        teacherInvite40.setPicUrl(StringUtils.isNotBlank(merchantShop.getPicPath()) ? merchantShop.getPicPath() : "");
//        teacherInvite40.setName(merchantShop.getShopName());
//        teacherInvite40.setUtime(new Date());
//        boolean b = this.teacherInviteService.updateTeacherInviteById(teacherInvite40);
//        if (!b) {
//            return;
//        }
//
//        //更新金额
//        BigDecimal bonus40 = totalFee.multiply(new BigDecimal("40")).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
//        this.teacherOrderService.addBonus2Teacher(teacherInvite40.getTid(), TeacherOrderBizTypeEnum.RECRUIT_MERCHANT_INCOME.getDesc(), merchantId, TeacherOrderBizTypeEnum.RECRUIT_MERCHANT_INCOME.getCode(), merchantShop.getId(), bonus40, "40");
//        Teacher teacher40 = this.teacherService.getById(teacherInvite40.getTid());
//        if (teacher40 != null) {
//            this.teacherService.updateAddBalanceByAmount(teacher40.getId(), bonus40);
//        }
//
//        //判断该集客师是否有上级
//        teacherInviteQueryWrapper = new QueryWrapper<>();
//        teacherInviteQueryWrapper.lambda()
//                .eq(TeacherInvite::getBizType, TeacherInviteBizTypeEnum.RECOMMAND_TEACHER.getCode())
//                .eq(TeacherInvite::getBizValue, teacherInvite40.getTid())
//                .eq(TeacherInvite::getStatus, TeacherInviteStatusEnum.EFFECTIVE.getCode())
//        ;
//        TeacherInvite teacherInvite5 = this.teacherInviteService.getOne(teacherInviteQueryWrapper);
//        BigDecimal bonus5 = BigDecimal.ZERO;
//        Teacher teacher5 = null;
//        if (teacherInvite5 != null) {
//            bonus5 = totalFee.multiply(new BigDecimal("5")).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
//            this.teacherOrderService.addBonus2Teacher(teacherInvite5.getTid(), TeacherOrderBizTypeEnum.RECOMMAND_TEACHER_INCOME.getDesc(), merchantId, TeacherOrderBizTypeEnum.RECOMMAND_TEACHER_INCOME.getCode(), teacherInvite5.getBizValue(), bonus5, "5");
//            teacher5 = this.teacherService.getById(teacherInvite5.getTid());
//            if (teacher5 != null) {
//                this.teacherService.updateAddBalanceByAmount(teacher5.getId(), bonus5);
//            }
//        }
//
//        //今日收益,今日招募数,总收益来排序,平台发出的奖金,招募一位商家，首页弹幕
//        this.updateRedisData(teacher40, teacherInvite40, bonus40, teacherInvite5, bonus5);
//    }
//
//    private void updateRedisData(Teacher teacher40, TeacherInvite teacherInvite40, BigDecimal bonus40, TeacherInvite teacherInvite5, BigDecimal bonus5) {
//        if (teacherInvite40 != null) {
//            //平台总发奖金更新
//            this.teacherRedisService.saveSendTotalBonus(bonus40);
//            //今日收益更新
//            this.teacherRedisService.saveTodayInfo(teacherInvite40.getTid(), bonus40, 1);
//            //排行榜更新
//            this.teacherRedisService.saveRankingList(teacherInvite40.getTid().toString(), bonus40);
//            //招募一位商家，首页弹幕
//            this.teacherRedisService.saveHomePageBarrage(teacher40.getRoleName(), teacher40.getAvatarUrl(), 2);
//            // 首页增加收益弹窗
//            this.teacherRedisService.saveIncomeDialog40(teacherInvite40.getTid(), bonus40);
//
//            // 发送模板消息通知 -- 商家招募成功
//            Map<String, Object> data = TemplateMessageData.incomeArrivalAdvice4RecruitMerchant(teacher40.getId().toString(), null, teacherInvite40.getName(), bonus40);
//            templateMessageService.sendTemplateMessage(wxmpProperty.getTeacherIncomeReachMsgTmpId(), teacher40, data);
//        }
//
//        if (teacherInvite5 != null) {
//            //平台总发奖金更新
//            this.teacherRedisService.saveSendTotalBonus(bonus5);
//            //今日收益更新
//            this.teacherRedisService.saveTodayInfo(teacherInvite5.getTid(), bonus5, 0);
//            //排行榜更新
//            this.teacherRedisService.saveRankingList(teacherInvite5.getTid().toString(), bonus5);
//            // 首页增加收益弹窗
//            this.teacherRedisService.saveIncomeDialog5(teacherInvite5.getTid(), bonus5);
//
//            // 发送模板消息通知 -- 下级讲师招募商家成功
//            Map<String, Object> data = TemplateMessageData.incomeArrivalAdvice4RecruitMerchant(teacherInvite5.getTid().toString(), teacher40.getRoleName(), teacherInvite40.getName(), bonus5);
//            templateMessageService.sendTemplateMessage(wxmpProperty.getTeacherIncomeReachMsgTmpId(), teacherInvite5.getTid().toString(), data);
//        }
//    }
}
