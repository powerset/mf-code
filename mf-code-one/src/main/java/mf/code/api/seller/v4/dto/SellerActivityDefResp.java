package mf.code.api.seller.v4.dto;

import lombok.Data;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.seller.utils.SellerTransformParam;
import mf.code.activity.constant.ActivityDefTypeEnum;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;

/**
 * mf.code.api.seller.v4.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月20日 08:28
 */
@Data
public class SellerActivityDefResp {
    //店铺编号
    private Long shopId;
    //活动定义编号
    private Long activityDefId;
    //状态
    private int status;
    //消耗保证金
    private String costDeposit;
    //剩余保证金
    private String earnDeposit;
    //剩余库存
    private int stock;
    //库存变更记数
    private int changeStockTimes;
    //活动参与人数
    private int joinPerson;
    //商品编号
    private Long goodsId;
    //占用库存
    private int occupyStock;

    //活动二维码
    private String activityCode;

    /**展现无关数据**/
    private String depositDef;
    private int type;

    public void from(ActivityDef activityDef) {
        this.shopId = activityDef.getShopId();
        this.activityDefId = activityDef.getId();
        if (activityDef != null && activityDef.getStock() != null && activityDef.getStockDef() != null && activityDef.getStock() - activityDef.getStockDef() * 0.1 <= 0) {
            this.status = NumberUtils.toInt(SellerTransformParam.STOCK_WARN_STATUS);
        }
        String statusVO = SellerTransformParam.PO2VO4ActivityDefStatus(activityDef.getStatus());
        this.status = NumberUtils.toInt(statusVO);
        this.type = activityDef.getType();
        if(statusVO.equals(SellerTransformParam.PAY_WAIT_STATUS)){
            this.depositDef = activityDef.getDepositDef().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        if (activityDef.getType().equals(ActivityDefTypeEnum.ASSIST.getCode()) || activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode())) {
            this.goodsId = activityDef.getGoodsId();
        }
        if (activityDef.getType().equals(ActivityDefTypeEnum.ASSIST_V2.getCode()) || activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())) {
            this.goodsId = activityDef.getGoodsId();
        }
        if (activityDef != null && activityDef.getStock() != null && activityDef.getStock() > 0) {
            this.stock = activityDef.getStock();
        }
        //消耗的保证金
        BigDecimal costDefDeposit = activityDef.getDepositDef().subtract(activityDef.getDeposit());

        this.earnDeposit = activityDef.getDeposit().setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.costDeposit = costDefDeposit.setScale(2, BigDecimal.ROUND_DOWN).toString();
    }

    private boolean commissionAndGoodsPrice(ActivityDef activityDef){
        boolean activityType = activityDef.getType().equals(ActivityDefTypeEnum.ASSIST.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.ASSIST_V2.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.MCH_PLAN.getCode());
        if(activityType){
            return true;
        }
        return false;
    }
}
