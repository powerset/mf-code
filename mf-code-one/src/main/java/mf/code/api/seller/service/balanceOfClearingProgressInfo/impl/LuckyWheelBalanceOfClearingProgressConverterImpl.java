package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 18:06
 */
@Service
@Slf4j
public class LuckyWheelBalanceOfClearingProgressConverterImpl extends BalanceOfClearingProgressService {

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean luckyWheelType = activityDef.getType().equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
        if (luckyWheelType) {
            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.from(activityDef);
            return dto;
        }
        return null;
    }
}
