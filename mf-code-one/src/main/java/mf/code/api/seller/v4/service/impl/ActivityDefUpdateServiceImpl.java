package mf.code.api.seller.v4.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.DepositStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.seller.v4.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月19日 16:39
 */
@Service
@Slf4j
public class ActivityDefUpdateServiceImpl implements ActivityDefUpdateService {
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /***
     * 非草稿修改，支持部分修改
     * @param req
     * @return
     * <pre>
     * 对已发布中的活动 的修改 进行校验：
     *     赠品和免单商品活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
     *                                      领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
     *                                      无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
     *     幸运大转盘 活动，支持修改的选项有：无。
     *     拆红包支持修改的选项有：帮拆人数，活动时长；无法修改的有：红包金额，红包库存，保证金，
     *
     *     收藏加购活动，支持修改的选项有：活动时长，商品    无法修改的有：红包金额，红包库存，保证金，
     *     回填订单活动，支持修改的选项有：无
     *     好评晒图活动，支持修改的选项有：活动时长，若是指定，则可以改商品    无法修改的有：红包金额，红包库存，保证金，
     * </pre>
     */
    @Override
    public SimpleResponse activityDefUpdateNotSave(CreateDefReq req, ActivityDef activityDef) {
        Integer type = req.getType();
        if (req.getType() == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()
                || req.getType() == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()
                || req.getType() == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "该类型活动无法修改");
        }
        SimpleResponse simpleResponseCheck = activityDefAboutService.checkActivityDefUpdateNotSave(req);
        if (simpleResponseCheck.error()) {
            return simpleResponseCheck;
        }
        // 按照类型修改数据
        req.init();
        CreateDefReq.ExtraData extraData = req.getExtraDataObject();
        // 赠品和免单商品活动，支持修改的选项有
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()
                || type == ActivityDefTypeEnum.ORDER_BACK_V2.getCode()
                || type == ActivityDefTypeEnum.ASSIST.getCode()
                || type == ActivityDefTypeEnum.ASSIST_V2.getCode()) {
            if (extraData.getCondPersionCount() != null && extraData.getCondPersionCount() > 0) {
                activityDef.setCondPersionCount(extraData.getCondPersionCount());
            }
            if (extraData.getHitsPerDraw() != null && extraData.getHitsPerDraw() > 0) {
                activityDef.setHitsPerDraw(extraData.getHitsPerDraw());
            }
            if (StringUtils.isNotBlank(extraData.getCondDrawTime()) && Integer.valueOf(extraData.getCondDrawTime()) > 0) {
                activityDef.setCondDrawTime(Integer.valueOf(extraData.getCondDrawTime()));
            }
            if (StringUtils.isNotBlank(extraData.getMissionNeedTime()) && Integer.valueOf(extraData.getMissionNeedTime()) > 0) {
                activityDef.setMissionNeedTime(Integer.valueOf(extraData.getMissionNeedTime()));
            }
            if (StringUtils.isNotBlank(extraData.getKeywords())) {
                String keyWord = extraData.getKeywords();
                if (StringUtils.isNotBlank(keyWord)) {
                    String[] split = keyWord.split(",");
                    activityDef.setKeyWord(JSON.toJSONString(Arrays.asList(split)));
                }
            }
            if (StringUtils.isNotBlank(extraData.getMerchantCouponJson())) {
                String merchantCouponJson = extraData.getMerchantCouponJson();
                List<Long> merchantCouponList = new ArrayList<>();
                String[] split = merchantCouponJson.split(",");
                for (String merchantCoupon : split) {
                    if (!RegexUtils.StringIsNumber(merchantCoupon)) {
                        continue;
                    }
                    merchantCouponList.add(Long.valueOf(merchantCoupon));
                }
                if (!CollectionUtils.isEmpty(merchantCouponList)) {
                    activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
                }
            }
            if (StringUtils.isNotBlank(extraData.getServiceWx())) {
                List<Map> serviceWxs = JSONArray.parseArray(extraData.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(serviceWxs)) {
                    activityDef.setServiceWx(JSONArray.toJSONString(serviceWxs));
                }
            }
        }
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            if (extraData.getCondPersionCount() != null && extraData.getCondPersionCount() > 0) {
                activityDef.setCondPersionCount(extraData.getCondPersionCount() + 1);
            }
            if (StringUtils.isNotBlank(extraData.getCondDrawTime()) && Integer.valueOf(extraData.getCondDrawTime()) > 0) {
                activityDef.setCondDrawTime(Integer.valueOf(extraData.getCondDrawTime()));
            }
            if (StringUtils.isNotBlank(extraData.getServiceWx())) {
                List<Map> serviceWxs = JSONArray.parseArray(extraData.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(serviceWxs)) {
                    activityDef.setServiceWx(JSONArray.toJSONString(serviceWxs));
                }
            }
        }
        if (type == ActivityDefTypeEnum.FAV_CART.getCode() || type == ActivityDefTypeEnum.FAV_CART_V2.getCode()) {
            if (StringUtils.isNotBlank(extraData.getCondDrawTime()) && Integer.valueOf(extraData.getCondDrawTime()) > 0) {
                activityDef.setCondDrawTime(Integer.valueOf(extraData.getCondDrawTime()));
            }
            if (StringUtils.isNotBlank(extraData.getKeywords())) {
                String keyWord = extraData.getKeywords();
                if (StringUtils.isNotBlank(keyWord)) {
                    String[] split = keyWord.split(",");
                    activityDef.setKeyWord(JSON.toJSONString(Arrays.asList(split)));
                }
            }
        }
        if (type == ActivityDefTypeEnum.GOOD_COMMENT.getCode() || type == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()) {
            if (StringUtils.isNotBlank(extraData.getCondDrawTime()) && Integer.valueOf(extraData.getCondDrawTime()) > 0) {
                activityDef.setCondDrawTime(Integer.valueOf(extraData.getCondDrawTime()));
            }
        }

        //如果是修改中奖人数，需要判断中奖人数>当前库存人数 否则不允许改动
        if (activityDef != null && activityDef.getHitsPerDraw() != null && activityDef.getHitsPerDraw() > 0
                && activityDef.getHitsPerDraw() > activityDef.getStock()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "中奖人数已超出当前库存数,请重新输入中奖人数");
        }
        //保证金计算
//        Map data = (Map) this.activityDefAboutService.getDeposit(req).getData();
//        BigDecimal depositDef = new BigDecimal(data.get("depositDef").toString());
//        if (null != depositDef) {
//            activityDef.setDepositDef(depositDef);
//            activityDef.setDeposit(depositDef);
//        }
        // 商户填写的数据
//        BigDecimal goodsPrice = null;
//        if (StringUtils.isNotBlank(req.getGoodsPrice())) {
//            goodsPrice = new BigDecimal(req.getGoodsPrice());
//            activityDef.setGoodsPrice(goodsPrice);
//        } else {
//            goodsPrice = new BigDecimal(data.get("goodsPrice").toString());
//        }
//        if (null != goodsPrice) {
//            activityDef.setGoodsPrice(goodsPrice);
//        }
        activityDef.setId(req.getActivityDefId());
        activityDef.setStatus(activityDef.getStatus());
        Integer integer = this.activityDefService.updateActivityDef(activityDef);
        Assert.isTrue(integer > 0, "更新数据异常 to夜辰");

        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            return activityDefAuditService.saveError();
        }

        if (this.checkAddActivityTask(activityDef)) {
            //更新activityTask
            List<ActivityTask> activityTasks = this.activityDefAboutService.addActivityTasks(activityDef, req);
            if (!CollectionUtils.isEmpty(activityTasks)) {
                this.activityTaskService.updateBatchById(activityTasks);
            }
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }

    /***
     * 草稿修改，任意修改
     * @param req
     * @return
     */
    @Override
    public SimpleResponse activityDefUpdateSave(CreateDefReq req, ActivityDef activityDef) {
        //修改赋值
        req.to(activityDef);
        //草稿状态,初始库存的修改
        if (req.getStockDef() != null && req.getStockDef() > 0) {
            activityDef.setStock(req.getStockDef());
        }
        //保证金计算
        Map data = (Map) this.activityDefAboutService.getDeposit(req).getData();
        BigDecimal depositDef = new BigDecimal(data.get("depositDef").toString());
        if (null != depositDef) {
            activityDef.setDepositDef(depositDef);
            activityDef.setDeposit(depositDef);
        }
        // 商户填写的数据
        BigDecimal goodsPrice = null;
        if (!activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                && !activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
            if (StringUtils.isNotBlank(req.getGoodsPrice())) {
                goodsPrice = new BigDecimal(req.getGoodsPrice());
                activityDef.setGoodsPrice(goodsPrice);
            } else {
                goodsPrice = new BigDecimal(data.get("goodsPrice").toString());
            }
            if (null != goodsPrice) {
                activityDef.setGoodsPrice(goodsPrice);
            }
        }
        activityDef.setId(req.getActivityDefId());
        activityDef.setStatus(activityDef.getStatus());
        Integer integer = this.activityDefService.updateActivityDef(activityDef);
        Assert.isTrue(integer > 0, "更新数据异常 to夜辰");

        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            return activityDefAuditService.saveError();
        }

        if (this.checkAddActivityTask(activityDef)) {
            //更新activityTask
            List<ActivityTask> activityTasks = this.activityDefAboutService.addActivityTasks(activityDef, req);
            if (!CollectionUtils.isEmpty(activityTasks)) {
                this.activityTaskService.updateBatchById(activityTasks);
            }
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(this.activityDefAboutService.addResponse(activityDef));
        return simpleResponse;
    }

    @Override
    public SimpleResponse activityDefNewBieUpdateSave(CreateDefReq req, ActivityDef activityDef) {
        if (req.getStockDef() == null || activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数错误，@to大妖怪");
        }
        //草稿状态,初始库存的修改
        if (req.getStockDef() > 0) {
            activityDef.setStock(req.getStockDef());
        }

        // 计算活动保证金
        BigDecimal depositDef = activityDefAboutService.calcNewBieTastDeposit(req.getExtraDataObject().getReadAmount(),
                req.getExtraDataObject().getScanAmount(), req.getStockDef());
        if (null != depositDef) {
            activityDef.setDepositDef(depositDef);
            activityDef.setDeposit(depositDef);
        }

        if (activityDef.getStockDef().equals(req.getStockDef())) {
            activityDef.setStockDef(req.getStockDef());
            activityDef.setStock(req.getStockDef());
        }

        activityDef.setCommission(new BigDecimal(req.getExtraDataObject().getReadAmount())
                .add(new BigDecimal(req.getExtraDataObject().getScanAmount())));
        activityDefService.updateActivityDef(activityDef);

        // 查询子活动新人任务
        ActivityDef readActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        if (readActivityDef == null) {
            log.error("子活动新人任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动新人任务查询出错");
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getReadAmount())) {
            readActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getReadAmount()));
            activityDefService.updateActivityDef(readActivityDef);
        }

        // 查询子活动扫一扫
        ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        if (scanActivityDef == null) {
            log.error("子活动扫一扫任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动扫一扫任务查询出错");
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getServiceWx())) {
            scanActivityDef.setServiceWx(req.getExtraDataObject().getServiceWx());
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
            scanActivityDef.setKeyWord(req.getExtraDataObject().getKeywords());
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getScanAmount())) {
            scanActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getScanAmount()));
        }

        activityDefService.updateActivityDef(scanActivityDef);

        // 查询子活动拆红包
        ActivityDef openRedPackActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        if (openRedPackActivityDef == null) {
            log.error("子活动拆红包任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动拆红包任务查询出错");
        }

        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            return activityDefAuditService.saveError();
        }

        return new SimpleResponse();
    }

    @Override
    public SimpleResponse activityDefNewBieUpdateNotSave(CreateDefReq req, ActivityDef activityDef) {
        if (req.getStockDef() == null || activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数错误，@to大妖怪");
        }

        // 查询子活动扫一扫
        ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        if (scanActivityDef == null) {
            log.error("子活动扫一扫任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动扫一扫任务查询出错");
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getServiceWx())) {
            scanActivityDef.setServiceWx(req.getExtraDataObject().getServiceWx());
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
            scanActivityDef.setKeyWord(req.getExtraDataObject().getKeywords());
        }

        activityDefService.updateActivityDef(scanActivityDef);

        return new SimpleResponse();
    }

    @Override
    public SimpleResponse activityDefShopManagerUpdateSave(CreateDefReq req, ActivityDef activityDef) {
        if (req == null || activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数错误，@to大妖怪");
        }

        // 计算活动保证金
        BigDecimal depositDef = activityDefAboutService.calcShopManagerTastDeposit(req.getExtraDataObject().getSetWechatAmount(),
                req.getExtraDataObject().getInviteAmount(), req.getExtraDataObject().getDevelopAmount(),
                req.getExtraDataObject().getDevelopTaskTimes(), req.getStockDef());

        if (depositDef != null) {
            activityDef.setDepositDef(depositDef);
            activityDef.setDeposit(depositDef);
        }

        if (req.getStockDef() != null) {
            activityDef.setStockDef(req.getStockDef());
            activityDef.setStock(req.getStockDef());
            activityDef.setCommission(new BigDecimal(req.getExtraDataObject().getSetWechatAmount())
                    .add(new BigDecimal(req.getExtraDataObject().getInviteAmount())
                            .add(new BigDecimal(req.getExtraDataObject().getDevelopAmount()))));
        }

        activityDefService.updateActivityDef(activityDef);

        // 查询子活动设置微信群任务
        ActivityDef setWechatActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        if (setWechatActivityDef == null) {
            log.error("子活动设置微信群任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动设置微信群任务查询出错");
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getSetWechatAmount())) {
            setWechatActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getSetWechatAmount()));
        }

        activityDefService.updateActivityDef(setWechatActivityDef);

        // 查询子活动邀请下级粉丝任务
        ActivityDef inviteActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        if (inviteActivityDef == null) {
            log.error("子活动邀请下级粉丝任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动邀请下级粉丝任务查询出错");
        }

        if (req.getExtraDataObject().getInviteCount() != null) {
            inviteActivityDef.setCondPersionCount(req.getExtraDataObject().getInviteCount());
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getInviteAmount())) {
            inviteActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getInviteAmount()));
        }

        activityDefService.updateActivityDef(inviteActivityDef);

        // 查询子活动发展粉丝成为店长任务
        ActivityDef developActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        if (developActivityDef == null) {
            log.error("子活动发展粉丝成为店长任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动发展粉丝成为店长任务查询出错");
        }

        if (req.getExtraDataObject().getDevelopCount() != null) {
            developActivityDef.setCondPersionCount(req.getExtraDataObject().getDevelopCount());
        }

        if (StringUtils.isNotBlank(req.getExtraDataObject().getDevelopAmount())) {
            developActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getDevelopAmount()));
        }

        activityDefService.updateActivityDef(developActivityDef);

        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            return activityDefAuditService.saveError();
        }

        return new SimpleResponse();

    }

    @Override
    public SimpleResponse activityDefShopManagerUpdateNotSave(CreateDefReq req, ActivityDef activityDef) {
        if (req == null || activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数错误，@to大妖怪");
        }

        // 查询子活动邀请下级粉丝任务
        ActivityDef inviteActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        if (inviteActivityDef == null) {
            log.error("子活动邀请下级粉丝任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动邀请下级粉丝任务查询出错");
        }

        if (req.getExtraDataObject().getInviteCount() != null && !inviteActivityDef.getCondPersionCount().equals(req.getExtraDataObject().getInviteCount())) {
            inviteActivityDef.setCondPersionCount(req.getExtraDataObject().getInviteCount());
        } else {
            inviteActivityDef.setCondPersionCount(null);
        }

        activityDefService.updateActivityDef(inviteActivityDef);

        // 查询子活动发展粉丝成为店长任务
        ActivityDef developActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        if (developActivityDef == null) {
            log.error("子活动发展粉丝成为店长任务查询出错，请检查活动，id={}， @to大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "子活动发展粉丝成为店长任务查询出错");
        }

        if (req.getExtraDataObject().getDevelopCount() != null
                && !developActivityDef.getCondPersionCount().equals(req.getExtraDataObject().getDevelopCount())) {
            developActivityDef.setCondPersionCount(req.getExtraDataObject().getDevelopCount());
        } else {
            developActivityDef.setCondPersionCount(null);
        }

        activityDefService.updateActivityDef(developActivityDef);

        return new SimpleResponse();
    }

    /**
     * 报销活动草稿修改 任意修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    @Override
    public SimpleResponse activityDefReimbursementUpdateSave(CreateDefReq req, ActivityDef activityDef) {
        if (req == null || activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数错误， @to大妖怪");
        }

        activityDef.setType(req.getType());
        activityDef.setGoodsPrice(new BigDecimal(req.getAmount()));
        activityDef.setCondDrawTime(Integer.parseInt(req.getExtraDataObject().getCondDrawTime()));
        activityDef.setCondPersionCount(req.getExtraDataObject().getCondPersionCount());
        activityDef.setStockDef(req.getStockDef());
        activityDef.setStock(req.getStockDef());
        activityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        activityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        activityDef.setShopId(req.getShopId());
        activityDef.setMerchantId(req.getMerchantId());
        activityDef.setDeposit(activityDefAboutService.calcReimbursementDeposit(req.getStockDef(), req.getAmount()));
        activityDef.setDepositDef(activityDefAboutService.calcReimbursementDeposit(req.getStockDef(), req.getAmount()));
        activityDef.setHitsPerDraw(req.getExtraDataObject().getHitsPerDraw());

        Map<String, Object> amountJson = new HashMap<>(2);
        Map<String, Object> perAmount = new HashMap<>(2);
        perAmount.put("min", activityDefAboutService.calcPerMinAmount(new BigDecimal(req.getAmount())));
        perAmount.put("max", req.getExtraDataObject().getMaxReimbursement());
        amountJson.put("perAmount", perAmount);
        amountJson.put("personalMaxAmount", req.getExtraDataObject().getMaxAmount());
        activityDef.setAmountJson(JSON.toJSONString(amountJson));

        activityDefService.updateActivityDef(activityDef);

        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            return activityDefAuditService.saveError();
        }
        return new SimpleResponse();
    }

    @Override
    public SimpleResponse activityDefReimbursementUpdateNotSave(CreateDefReq req, ActivityDef activityDef) {
        if (req == null || activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数错误， @to大妖怪");
        }

        activityDef.setCondDrawTime(Integer.parseInt(req.getExtraDataObject().getCondDrawTime()));

        activityDefService.updateActivityDef(activityDef);
        return new SimpleResponse();
    }


    /***
     * 判断是否更新activityTask
     * @param activityDef
     * @return
     */
    private boolean checkAddActivityTask(ActivityDef activityDef) {
        //好评晒图非全店，更新activityTask表
        boolean goodCommentActivitTask = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode()) && activityDef.getGoodsId() != null && !activityDef.getGoodsId().equals("-1");
        boolean goodCommentV2ActivitTask = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()) && activityDef.getGoodsId() != null && !activityDef.getGoodsId().equals("-1");
        boolean favCartActivityTask = activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode());
        boolean favCartV2ActivityTask = activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode());
        if (goodCommentActivitTask || goodCommentV2ActivitTask || favCartActivityTask || favCartV2ActivityTask) {
            return true;
        }
        return false;
    }
}
