package mf.code.api.seller.v4.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.seller.v4.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月19日 16:38
 */
public interface ActivityDefUpdateService {

    /***
     * 非草稿修改，支持部分修改
     * @param req
     * @return
     * <pre>
     * 对已发布中的活动 的修改 进行校验：
     *     赠品和免单商品活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
     *                                      领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
     *                                      无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
     *     幸运大转盘 活动，支持修改的选项有：无。
     *     拆红包支持修改的选项有：帮拆人数，活动时长；无法修改的有：红包金额，红包库存，保证金，
     *
     *     收藏加购活动，支持修改的选项有：活动时长，商品    无法修改的有：红包金额，红包库存，保证金，
     *     回填订单活动，支持修改的选项有：无
     *     好评晒图活动，支持修改的选项有：活动时长，若是指定，则可以改商品    无法修改的有：红包金额，红包库存，保证金，
     * </pre>
     */
    SimpleResponse activityDefUpdateNotSave(CreateDefReq req, ActivityDef activityDef);


    /***
     * 草稿修改，任意修改
     * @param req
     * @return
     */
    SimpleResponse activityDefUpdateSave(CreateDefReq req, ActivityDef activityDef);

    /**
     * 新人任务草稿修改 任意修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    SimpleResponse activityDefNewBieUpdateSave(CreateDefReq req, ActivityDef activityDef);

    /**
     * 新人任务非草稿修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    SimpleResponse activityDefNewBieUpdateNotSave(CreateDefReq req, ActivityDef activityDef);

    /**
     * 店长任务草稿修改 任意修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    SimpleResponse activityDefShopManagerUpdateSave(CreateDefReq req, ActivityDef activityDef);

    /**
     * 店长任务非草稿修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    SimpleResponse activityDefShopManagerUpdateNotSave(CreateDefReq req, ActivityDef activityDef);

    /**
     * 报销活动草稿修改 任意修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    SimpleResponse activityDefReimbursementUpdateSave(CreateDefReq req, ActivityDef activityDef);

    /**
     * 报销活动非草稿修改
     *
     * @param req
     * @param activityDef
     * @return
     */
    SimpleResponse activityDefReimbursementUpdateNotSave(CreateDefReq req, ActivityDef activityDef);
}
