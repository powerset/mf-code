package mf.code.api.seller;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.service.SellerUnionMarketCouponService;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.dto.ProductEntity;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.one.dto.*;
import mf.code.upay.repo.po.Product;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 联合营销--优惠券
 *
 * @author yunshan
 */
@Slf4j
@RestController
@RequestMapping("/api/seller/coupon/unionMarketing")
public class SellerUnionMarketIngCouponApi {
    @Autowired
    private SellerUnionMarketCouponService sellerUnionMarketCouponService;

    @Autowired
    private GoodsAppService goodsAppService;

    /**
     * 查询所有已经创建的优惠券
     *
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/queryAllCoupons")
    public SimpleResponse queryAllCoupons(@RequestParam("shopId") Long shopId,
                                          @RequestParam(value = "page", defaultValue = "1") Long page,
                                          @RequestParam(value = "size", defaultValue = "10") Long size) {

        UnionMarketingCouponDTO dto = new UnionMarketingCouponDTO();
        List<SellerMerchantShopCouponDTO> couponList = new ArrayList<>();
        dto.setTotalCount(0);
        dto.setPage(page);
        dto.setSize(size);
        dto.setCouponList(couponList);

        if (page <= 0) {
            page = 1L;
        }
        if (size <= 0) {
            size = 10L;
        }
        //已创建优惠券总数
        int totalCount = sellerUnionMarketCouponService.countAllCoupons(shopId);
        long offset = (page - 1) * size;
        if (totalCount <= offset) {
            dto.setTotalCount(totalCount);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
        }

        //已创建优惠券集合信息
        couponList = sellerUnionMarketCouponService.queryAllCoupons(shopId, page, size);

        if (CollectionUtils.isEmpty(couponList) || couponList.size() < 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_DB, dto);
        }


        dto.setCouponList(couponList);
        dto.setTotalCount(totalCount);
        dto.setPage(page);
        dto.setSize(size);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
    }


    /**
     * 添加优惠券
     *
     * @param sellerShopCouponDTO
     * @return
     */
    @PostMapping(value = "/addNewCoupons")
    public SimpleResponse addNewCoupons(@RequestBody SellerShopCouponDTO sellerShopCouponDTO) {
        if (null == sellerShopCouponDTO) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1);
        }

        //校验参数
        if (null == sellerShopCouponDTO.getShopId() ||
                StringUtils.isEmpty(sellerShopCouponDTO.getName()) ||
                null == sellerShopCouponDTO.getDisplayEndTime() ||
                null == sellerShopCouponDTO.getDisplayStartTime() ||
                null == sellerShopCouponDTO.getStockDef() ||
                null == sellerShopCouponDTO.getLimitAmount() ||
                null == sellerShopCouponDTO.getLimitPerNum() ||
                null == sellerShopCouponDTO.getAmount() ||
                null == sellerShopCouponDTO.getType() ||
                null == sellerShopCouponDTO.getMerchantId()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
        }

        return sellerUnionMarketCouponService.addNewCoupons(sellerShopCouponDTO);
    }


    /**
     * 查询领取情况
     *
     * @return
     */
    @GetMapping(value = "/queryCouponsInfo")
    public SimpleResponse queryCouponsInfo(@RequestParam("shopId") Long shopId,
                                           @RequestParam("couponId") Long couponId,
                                           @RequestParam(value = "page", defaultValue = "1") Long page,
                                           @RequestParam(value = "size", defaultValue = "10") Long size) {

        UnionMarketingCouponDTO dto = new UnionMarketingCouponDTO();
        List<UserReceivedCouponDTO> couponinfoList = new ArrayList<>();
        dto.setTotalCount(0);
        dto.setUsedCount(0);
        dto.setReceiveCount(0);
        dto.setPage(page);
        dto.setSize(size);
        dto.setUserCouponInfoList(couponinfoList);

        if (null == couponId) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND, dto);
        }

        if (page <= 1) {
            page = 1L;
        }
        if (size <= 0) {
            size = 10L;
        }
        couponinfoList = sellerUnionMarketCouponService.queryUserCouponsList(shopId, couponId, page, size);
        if (CollectionUtils.isEmpty(couponinfoList)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
        }

        int totalCount = sellerUnionMarketCouponService.countCouponsInfo(shopId, couponId);

        int count = sellerUnionMarketCouponService.countCouponsInfoUsed(shopId, couponId);

        dto.setUserCouponInfoList(couponinfoList);
        dto.setTotalCount(totalCount);
        dto.setUsedCount(count);
        dto.setReceiveCount(totalCount);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
    }

    /**
     * 结束发放
     *
     * @param sellerShopCouponDTO
     * @return
     */
    @PostMapping(value = "/finishIssued")
    public SimpleResponse finishIssued(@RequestBody SellerShopCouponDTO sellerShopCouponDTO) {

        return sellerUnionMarketCouponService.finishIssued(sellerShopCouponDTO.getShopId(), sellerShopCouponDTO.getCouponId());
    }

    /**
     * 查询已经选择的商品
     *
     * @param couponId
     * @return
     */
    @GetMapping(value = "/getCheckedGood")
    public SimpleResponse getCheckedGood(@RequestParam("shopId") Long shopId,
                                         @RequestParam("couponId") Long couponId) {

        //查询优惠券信息并获取到产品id
        MerchantShopCoupon merchantShopCoupon = sellerUnionMarketCouponService.getCheckedGood(couponId);

        if (null == merchantShopCoupon) {
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        //获取已选取的商品信息
        Long productId = merchantShopCoupon.getProductId();

        List<Long> productIdList = new ArrayList<>();
        productIdList.add(productId);

        List<ProductEntity> productEntityList = goodsAppService.listByIds(productIdList);

        if (CollectionUtils.isEmpty(productEntityList) || productEntityList.size() < 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR);
        }

        ProductEntity productEntity = productEntityList.get(0);

        Map<String, Object> map = new HashMap<>();
        map.put("chekcProductInfo", productEntity);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
    }


    /**
     * 选择优惠券关联的商品
     *
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/getGoodsInShop")
    public SimpleResponse getGoodsInShop(@RequestParam("shopId") Long shopId,
                                         @RequestParam("merchantId") Long merchantId,
                                         @RequestParam(value = "page", defaultValue = "10") Integer page,
                                         @RequestParam(value = "size", defaultValue = "1") Integer size) {

        Map<String, Object> map = new HashMap<>();
        map.put("productList", new ArrayList<>());
        map.put("count", 0);
        map.put("page", page);
        map.put("size", size);

        int offset = (page - 1) * size;
        //查询该店铺下的所有已上架的商品数量
        int count = goodsAppService.queryProductCountByShopId(shopId, merchantId);
        if (count <= offset) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
        }
        map.put("count", count);

        //查询该店铺下的所有已上架的商品 TODO 直接返回数据库结构不可取
        List<Product> productList = goodsAppService.querySelfProductByShopId(shopId, page, size);
        map.put("productList", productList);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
    }

    /**
     * 修改创建的优惠券信息
     *
     * @param coupon
     * @return
     */
    @PostMapping(value = "/updateNewCouponsInfo")
    public SimpleResponse updateNewCouponsInfo(@RequestBody SellerShopCouponDTO coupon) {
        if (coupon.getId() == null
                || coupon.getDisplayStartTime() == null
                || coupon.getDisplayEndTime() == null
                || coupon.getShopId() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入必要参数");
        }
        return sellerUnionMarketCouponService.updateNewCouponsInfo(coupon);
    }

    /**
     * 根据优惠券id查询优惠券信息
     *
     * @param shopId
     * @param couponId
     * @return
     */
    @GetMapping(value = "/findCouponById")
    public SimpleResponse findCouponById(@RequestParam("shopId") Long shopId,
                                         @RequestParam("couponId") Long couponId) {
        MerchantShopCoupon merchantShopCoupon = sellerUnionMarketCouponService.findCouponById(shopId, couponId);

        if (null == merchantShopCoupon) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "您无权查看该优惠券");
        }
        MerchantShopCouponDTO merchantShopCouponDTO = new MerchantShopCouponDTO();
        BeanUtils.copyProperties(merchantShopCoupon, merchantShopCouponDTO);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, merchantShopCouponDTO);
    }


    /**
     * 删除已经创建的优惠券
     *
     * @param sellerShopCouponDTO
     * @return
     */
    @PostMapping(value = "/deleteNewCouponById")
    public SimpleResponse deleteNewCouponById(@RequestBody SellerShopCouponDTO sellerShopCouponDTO) {
        return sellerUnionMarketCouponService.deleteNewCouponById(sellerShopCouponDTO.getCouponId());
    }

}
