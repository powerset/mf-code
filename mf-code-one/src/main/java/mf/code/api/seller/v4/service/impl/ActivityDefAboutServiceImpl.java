package mf.code.api.seller.v4.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.activity.service.activitydef.impl.ActivityDefNewBieTaskService;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.common.constant.*;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.v4.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月19日 16:03
 */
@Service
@Slf4j
public class ActivityDefAboutServiceImpl implements ActivityDefAboutService {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;

    @Override
    public SimpleResponse checkAmountJson(String amountJson, ActivityDef activityDef) {
        if (StringUtils.isBlank(amountJson)) {
            return new SimpleResponse();
        }
        //String a = "[{\"amount\":\"6\",\"from\":\"1\",\"to\":\"5\"},{\"amount\":\"6\",\"from\":\"5\",\"to\":\"10\"}]";
        List<Map> mapList = JSONObject.parseArray(amountJson, Map.class);
        BigDecimal maxAmount = BigDecimal.ZERO;
        for (Map map : mapList) {
            if (map != null && map.get("form") != null && map.get("to") != null) {
                if (new BigDecimal(map.get("from").toString()).compareTo(BigDecimal.ZERO) < 0) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "金额区间不能为负数");
                }
                if (new BigDecimal(map.get("to").toString()).compareTo(BigDecimal.ZERO) < 0) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "金额区间不能为负数");
                }
            }
            if (map != null && map.get("amount") != null) {
                BigDecimal amount = new BigDecimal(map.get("amount").toString());
                if (amount.compareTo(maxAmount) > 0) {
                    maxAmount = amount;
                }
            }
        }

        if (activityDef != null) {
            boolean exception = maxAmount.compareTo(activityDef.getDepositDef()) > 0 || maxAmount.compareTo(activityDef.getDeposit()) > 0;
            if (exception) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO8.getCode(), "修改金额不能低于保证金");
            }
        }
        for (int i = 0; i < mapList.size(); i++) {
            Map mapIndex = mapList.get(i);
            if (i + 1 < mapList.size()) {
                Map mapIndexNext = mapList.get(i + 1);
                if (mapIndexNext != null && mapIndex != null && new BigDecimal(mapIndex.get("to").toString()).compareTo(new BigDecimal(mapIndexNext.get("from").toString())) != 0) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "金额区间的数字之间需要连贯");
                }
            }
        }
        return new SimpleResponse();
    }

    @Override
    public SimpleResponse getDeposit(CreateDefReq req) {
        Long goodsId = null;
        List<Long> goodsIdList = new ArrayList<>();
        if (StringUtils.isNotBlank(req.getGoodsIds())) {
            String[] split = req.getGoodsIds().split(",");
            if (split.length == 1) {
                goodsId = Long.valueOf(split[0]);
            } else {
                for (String goodsId1 : split) {
                    goodsIdList.add(Long.valueOf(goodsId1));
                }
            }
        }
        if (null != goodsId) {
            Goods goods = goodsService.getById(goodsId);
            if (null == goods) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3, "该商品编号未找到对应的商品 to木子");
            }
            req.setGoodsPrice(goods.getDisplayPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
        }

        int type = req.getType();
        BigDecimal depositDef = null;
        BigDecimal commission = null;
        if (req.getExtraDataObject() == null && StringUtils.isNotBlank(req.getExtraData())) {
            req.init();
        }
        if (req != null && req.getExtraDataObject() != null && StringUtils.isNotBlank(req.getExtraDataObject().getCommission())) {
            commission = new BigDecimal(req.getExtraDataObject().getCommission());
        }
        //回填订单的保证金计算
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
            depositDef = new BigDecimal(req.getDepositDef());
            //此刻的回填订单按照定值返现
            if (StringUtils.isNotBlank(req.getAmount())) {
                req.setGoodsPrice(req.getAmount());
            }
        }
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) {
            depositDef = new BigDecimal(req.getDepositDef());
            //此刻的回填订单按照定值返现
            if (StringUtils.isNotBlank(req.getAmount())) {
                req.setGoodsPrice(req.getAmount());
            }
        }

        int stockDef = 0;
        if (req.getStockDef() != null && req.getStockDef() > 0) {
            stockDef = req.getStockDef();
        }
        //转盘类的佣金计算
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            // 新人平均成本价 (0 * 44 + 20 * 50 + 40 * 5 + 75 * 1)/10000(每次的花费) * 4(每人抽奖次数)
            CommonDict commonDict = this.commonDictService.selectByTypeKey("luckyWheel", "amount");
            BigDecimal averageCost = new BigDecimal(commonDict.getValue()).multiply(new BigDecimal("4")).setScale(2, BigDecimal.ROUND_DOWN);
            // 所需保证金
            depositDef = averageCost.multiply(new BigDecimal(stockDef));
        }
        //红包金额*库存类的保证金计算(拆红包，好评晒图，收藏加购)
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() || type == ActivityDefTypeEnum.GOOD_COMMENT.getCode()
                || type == ActivityDefTypeEnum.FAV_CART.getCode()) {
            // 总金额就是总金额
            depositDef = new BigDecimal(req.getStockDef()).multiply(new BigDecimal(req.getAmount()));
        }
        if (type == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() || type == ActivityDefTypeEnum.FAV_CART_V2.getCode()) {
            // 总金额就是总金额
            depositDef = new BigDecimal(req.getStockDef()).multiply(new BigDecimal(req.getAmount()));
        }
        //商品价格*库存+佣金*库存类的保证金计算(赠品+免单)
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode() || type == ActivityDefTypeEnum.ASSIST.getCode()) {
            if (StringUtils.isBlank(req.getGoodsPrice())) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4, "请输入红包金额...");
            }
            depositDef = new BigDecimal(req.getGoodsPrice()).multiply(new BigDecimal(stockDef));
            // 添加额外佣金
            if (commission != null) {
                depositDef = depositDef.add(commission.multiply(new BigDecimal(stockDef)));
            }
        }
        //商品价格*库存+佣金*库存类的保证金计算(赠品+免单)
        if (type == ActivityDefTypeEnum.ORDER_BACK_V2.getCode() || type == ActivityDefTypeEnum.ASSIST_V2.getCode()) {
            if (StringUtils.isBlank(req.getGoodsPrice())) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4, "请输入红包金额...");
            }
            depositDef = new BigDecimal(req.getGoodsPrice()).multiply(new BigDecimal(stockDef));
            // 添加额外佣金
            if (commission != null) {
                depositDef = depositDef.add(commission.multiply(new BigDecimal(stockDef)));
            }
        }

        // 新手任务保证金计算
        if (type == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
            if (req == null || req.getStockDef() == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数，@to木子");
            }
            // 查询活动
            ActivityDef mainActivityDef = activityDefService.selectByPrimaryKey(req.getActivityDefId());
            if (mainActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动不存在");
            }

            if (!mainActivityDef.getMerchantId().equals(req.getMerchantId()) || !mainActivityDef.getShopId().equals(req.getShopId())) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "没有操作权限");
            }

            // 查询阅读任务子活动
            ActivityDef readActivityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
            if (readActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询阅读任务子活动失败");
            }

            // 查询扫一扫任务子活动
            ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
            if (readActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询扫一扫任务子子活动失败");
            }

            depositDef = activityDefAboutService.calcNewBieTastDeposit(readActivityDef.getCommission().toString(), scanActivityDef.getCommission().toString(), req.getStockDef());
        }

        // 店长任务保证金计算
        if (type == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
            // 查询主活动
            ActivityDef mainActivityDef = activityDefService.selectByPrimaryKey(req.getActivityDefId());
            if (mainActivityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动不存在");
            }

            // 查询设置微信群子活动
            ActivityDef setWechatAcitvityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
            if (setWechatAcitvityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "设置微信群子活动不存在");
            }

            // 查询邀请粉丝任务子活动
            ActivityDef inviteAcitvityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
            if (inviteAcitvityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "邀请粉丝任务子活动不存在");
            }

            // 查询发展粉丝为店长活动
            ActivityDef developAcitvityDef = activityDefService.selectByParentIdAndType(mainActivityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
            if (developAcitvityDef == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "发展粉丝为店长活动不存在");
            }

            depositDef = activityDefAboutService.calcShopManagerTastDeposit(setWechatAcitvityDef.getCommission().toString(),
                    inviteAcitvityDef.getCommission().toString(), developAcitvityDef.getCommission().toString(),
                    developAcitvityDef.getCondPersionCount(), req.getStockDef());

        }

        // 计算全额报销保证金
        if (type == ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            depositDef = activityDefAboutService.calcReimbursementDeposit(req.getStockDef(), req.getAmount());
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultVO = new HashMap<>();
        // 总金额，这里拆红包的总保证金。
        resultVO.put("depositDef", depositDef);
        resultVO.put("goodsPrice", BigDecimal.ZERO);
        if (StringUtils.isNotBlank(req.getGoodsPrice())) {
            resultVO.put("goodsPrice", req.getGoodsPrice());
        }
        simpleResponse.setData(resultVO);
        return simpleResponse;
    }

    /***
     * 计算活动的保证金
     * @param req
     * @return
     */
    @Override
    public SimpleResponse getDepositByProduct(CreateDefReq req) {
        int type = req.getType();
        BigDecimal depositDef = null;
        BigDecimal commission = null;
        if (req.getExtraDataObject() == null && StringUtils.isNotBlank(req.getExtraData())) {
            req.init();
        }
        if (req.getExtraDataObject() != null && StringUtils.isNotBlank(req.getExtraDataObject().getCommission())) {
            commission = new BigDecimal(req.getExtraDataObject().getCommission());
        }
        Long goodsId = null;
        if (StringUtils.isNotBlank(req.getGoodsIds())) {
            String[] split = req.getGoodsIds().split(",");
            // 2019-06-04 有关商品的活动目前只有收藏加购有可能是一个goodsId
            if (split.length == 1 && type != ActivityDefTypeEnum.FAV_CART_V2.getCode()) {
                goodsId = Long.valueOf(split[0]);
            }
        }
        if (null != goodsId) {
            String goodsPrice = req.getExtraDataObject().getGoodsPrice();
            if (StringUtils.isBlank(goodsPrice)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "请输入商品返现金额");
            }
            req.setGoodsPrice(new BigDecimal(goodsPrice).setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        //回填订单的保证金计算
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) {
            depositDef = new BigDecimal(req.getDepositDef());
            //此刻的回填订单按照定值返现
            if (StringUtils.isNotBlank(req.getAmount())) {
                req.setGoodsPrice(req.getAmount());
            }
        }

        int stockDef = 0;
        if (req.getStockDef() != null && req.getStockDef() > 0) {
            stockDef = req.getStockDef();
        }
        //转盘类的佣金计算
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            // 新人平均成本价 (0 * 44 + 20 * 50 + 40 * 5 + 75 * 1)/10000(每次的花费) * 4(每人抽奖次数)
            CommonDict commonDict = this.commonDictService.selectByTypeKey("luckyWheel", "amount");
            BigDecimal averageCost = new BigDecimal(commonDict.getValue()).multiply(new BigDecimal("4")).setScale(2, BigDecimal.ROUND_DOWN);
            // 所需保证金
            depositDef = averageCost.multiply(new BigDecimal(stockDef));
        }
        //红包金额*库存类的保证金计算(拆红包，好评晒图，收藏加购)
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()
                || type == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()
                || type == ActivityDefTypeEnum.FAV_CART_V2.getCode()) {
            // 总金额就是总金额
            if (commission != null) {
                depositDef = commission.multiply(new BigDecimal(stockDef));
            } else {
                depositDef = new BigDecimal(req.getStockDef()).multiply(new BigDecimal(req.getAmount()));
            }
        }
        //商品价格*库存+佣金*库存类的保证金计算(赠品+免单)
        if (type == ActivityDefTypeEnum.ORDER_BACK_V2.getCode() || type == ActivityDefTypeEnum.ASSIST_V2.getCode()) {
            if (StringUtils.isBlank(req.getGoodsPrice())) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4, "请输入红包金额...");
            }
            depositDef = new BigDecimal(req.getGoodsPrice()).multiply(new BigDecimal(stockDef));
            // 添加额外佣金
            if (commission != null) {
                depositDef = depositDef.add(commission.multiply(new BigDecimal(stockDef)));
            }
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultVO = new HashMap<>();
        // 总金额，这里拆红包的总保证金。
        resultVO.put("depositDef", depositDef);
        resultVO.put("goodsPrice", BigDecimal.ZERO);
        if (StringUtils.isNotBlank(req.getGoodsPrice())) {
            resultVO.put("goodsPrice", req.getGoodsPrice());
        }
        simpleResponse.setData(resultVO);
        return simpleResponse;
    }

    @Override
    public ActivityDef addActivityDef(CreateDefReq req) {
        if (StringUtils.isNotBlank(req.getExtraData()) && req.getExtraDataObject() == null) {
            req.init();
        }
        if (req.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) && StringUtils.isBlank(req.getGoodsPrice())) {
            this.openRedpacketInit(req);
        }
        Date now = new Date();
        ActivityDef activityDef = new ActivityDef();

        // 初始化数据（非商户填写）
        activityDef.setMerchantId(Long.valueOf(req.getMerchantId()));
        activityDef.setShopId(Long.valueOf(req.getShopId()));
        activityDef.setType(req.getType());
        activityDef.setStartTime(now);
        activityDef.setEndTime(DateUtils.addYears(now, 100));
        activityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        activityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        activityDef.setCondDrawTime(10080);
        activityDef.setDel(DelEnum.NO.getCode());
        activityDef.setJeton(new BigDecimal(0.00));
        activityDef.setPublishTime(new Date(0));
        activityDef.setApplyTime(now);
        activityDef.setUtime(now);
        activityDef.setCtime(now);
        activityDef.setWeighting(WeightingEnum.YES.getCode());
        activityDef.setStockType(1);
        if (StringUtils.isNotBlank(req.getAmount())) {
            activityDef.setGoodsPrice(new BigDecimal(req.getAmount()));
        }
        //保证金计算
        Map data = (Map) this.getDeposit(req).getData();
        BigDecimal depositDef = new BigDecimal(data.get("depositDef").toString());

        // 商户填写的数据
        BigDecimal goodsPrice = null;
        if (StringUtils.isNotBlank(req.getGoodsPrice())) {
            goodsPrice = new BigDecimal(req.getGoodsPrice());
            activityDef.setGoodsPrice(goodsPrice);
        } else {
            goodsPrice = new BigDecimal(data.get("goodsPrice").toString());
        }

        if (StringUtils.isNotBlank(req.getGoodsIds())) {
            List<Long> goodsIdList = new ArrayList<>();
            String[] split = req.getGoodsIds().split(",");
            //不是收藏，好评，回填时，一个商品存储goodsId
            if (split.length == 1 && !req.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode()) &&
                    !req.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()) &&
                    !req.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode()) &&
                    !req.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode()) &&
                    !req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) &&
                    !req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
                activityDef.setGoodsId(Long.valueOf(split[0]));
            } else {
                for (String goodsId : split) {
                    goodsIdList.add(Long.valueOf(goodsId));
                }
                activityDef.setGoodsIds(JSON.toJSONString(goodsIdList));
            }
        }
        if (null != goodsPrice) {
            activityDef.setGoodsPrice(goodsPrice);
        }
        if (req.getStockDef() != null && req.getStockDef() > 0) {
            activityDef.setStockDef(req.getStockDef());
            activityDef.setStock(req.getStockDef());
        }
        if (null != depositDef) {
            activityDef.setDepositDef(depositDef);
            activityDef.setDeposit(depositDef);
        }

        //金额区间
        if (StringUtils.isNotBlank(req.getAmountJson())) {
            List<Map> amountJson = JSONArray.parseArray(req.getAmountJson(), Map.class);
            if (!CollectionUtils.isEmpty(amountJson)) {
                activityDef.setAmountJson(JSONArray.toJSONString(amountJson));
            }
        }

        if (req != null && req.getExtraDataObject() != null) {
            if (StringUtils.isNotBlank(req.getExtraDataObject().getKeywords())) {
                String keyWord = req.getExtraDataObject().getKeywords();
                if (StringUtils.isNotBlank(keyWord)) {
                    String[] split = keyWord.split(",");
                    activityDef.setKeyWord(JSON.toJSONString(Arrays.asList(split)));
                }
            }
            if (StringUtils.isNotBlank(req.getExtraDataObject().getCondDrawTime())) {
                // 分
                activityDef.setCondDrawTime(Integer.valueOf(req.getExtraDataObject().getCondDrawTime()));
            }
            Integer condPersionCount = req.getExtraDataObject().getCondPersionCount();
            if (condPersionCount != null && condPersionCount > 0) {
                activityDef.setCondPersionCount(Integer.valueOf(condPersionCount));
            }
            Integer hitsPerDraw = req.getExtraDataObject().getHitsPerDraw();
            if (hitsPerDraw != null && hitsPerDraw > 0) {
                activityDef.setHitsPerDraw(hitsPerDraw);
            }
            if (StringUtils.isNotBlank(req.getExtraDataObject().getMissionNeedTime())) {
                activityDef.setMissionNeedTime(Integer.valueOf(req.getExtraDataObject().getMissionNeedTime()));
            }
            if (StringUtils.isNotBlank(req.getExtraDataObject().getMerchantCouponJson())) {
                String merchantCouponJson = req.getExtraDataObject().getMerchantCouponJson();
                List<Long> merchantCouponList = new ArrayList<>();
                String[] split = merchantCouponJson.split(",");
                for (String merchantCoupon : split) {
                    if (!RegexUtils.StringIsNumber(merchantCoupon)) {
                        continue;
                    }
                    merchantCouponList.add(Long.valueOf(merchantCoupon));
                }
                if (!CollectionUtils.isEmpty(merchantCouponList)) {
                    activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
                }
            }
            List<Map> serviceWxs = JSONArray.parseArray(req.getExtraDataObject().getServiceWx(), Map.class);
            if (!CollectionUtils.isEmpty(serviceWxs)) {
                activityDef.setServiceWx(JSONArray.toJSONString(serviceWxs));
            }

            // 设置佣金
            activityDef.setCommission(BigDecimal.ZERO);
            if (StringUtils.isNotBlank(req.getExtraDataObject().getCommission())) {
                BigDecimal commission = new BigDecimal(req.getExtraDataObject().getCommission());
                if (commission != null && commission.compareTo(BigDecimal.ONE) >= 0) {
                    activityDef.setCommission(commission);
                }
            } else {
                //若没有佣金
                if (req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) && StringUtils.isBlank(req.getAmountJson())) {
                    activityDef.setCommission(new BigDecimal(req.getAmount()));
                }
                //红包金额等同于佣金的概念活动(收藏加购，拆红包，好评晒图)
                if (req.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode())
                        || req.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())
                        || req.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode())
                        || req.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode())
                        || req.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode())
                        || req.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
                    activityDef.setCommission(new BigDecimal(req.getAmount()));
                }

                //去除佣金概念活动,转盘，助力，免单
                List<Integer> list = new ArrayList<>();
                list.add(ActivityDefTypeEnum.ORDER_BACK.getCode());
                list.add(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
                list.add(ActivityDefTypeEnum.ASSIST.getCode());
                list.add(ActivityDefTypeEnum.ASSIST_V2.getCode());
                list.add(ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
                if ((activityDef.getCommission() == null || activityDef.getCommission().compareTo(BigDecimal.ZERO) == 0)
                        && null != goodsPrice && !list.contains(req.getType())) {
                    activityDef.setCommission(goodsPrice);
                }
            }
            //扣库存方式
            if (req.getExtraDataObject().getStockType() > 0) {
                activityDef.setStockType(req.getExtraDataObject().getStockType());
            }
            //是否加权
            if (!req.getExtraDataObject().isWeighting()) {
                activityDef.setWeighting(WeightingEnum.NO.getCode());
            } else {
                activityDef.setWeighting(WeightingEnum.YES.getCode());
            }
            // 任务下单方式
            if (req.getExtraDataObject().getFinishType() != null) {
                activityDef.setFinishType(req.getExtraDataObject().getFinishType());
            }
            // 审核方式
            if (req.getExtraDataObject().getAuditType() != null) {
                activityDef.setAuditType(req.getExtraDataObject().getAuditType());
            }
        }
        return activityDef;
    }

    @Override
    public List<ActivityTask> addActivityTasks(ActivityDef activityDef, CreateDefReq req) {
        Integer type = 0;
        if (activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode()) || activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode())) {
            type = ActivityTaskTypeEnum.FAVCART.getCode();
        } else if (activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode()) || activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode())) {
            type = ActivityTaskTypeEnum.GOOD_COMMENT.getCode();
        }

        Long merchantId = activityDef.getMerchantId();
        Long shopId = activityDef.getShopId();
        Long activityDefId = activityDef.getId();
        Integer stockDef = activityDef.getStockDef();
        String serviceWx = activityDef.getServiceWx();
        Integer missionNeedTime = null;
        if (activityDef.getMissionNeedTime() != null) {
            missionNeedTime = activityDef.getMissionNeedTime();
        }
        if (activityDef.getMissionNeedTime() == null && activityDef.getCondDrawTime() != null) {
            missionNeedTime = activityDef.getCondDrawTime();
        }
        String amount = req.getAmount();
        List<Long> goodsIdList = JSON.parseArray(activityDef.getGoodsIds(), Long.class);
        // 20190119 更新客服微信号
        if (StringUtils.isNotBlank(activityDef.getServiceWx())) {
            if (JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(csMapList)) {
                    //微信号
                    serviceWx = (String) csMapList.get(0).get("wechat");
                }
            }
        }

        QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityTask::getActivityDefId, activityDefId)
                .eq(ActivityTask::getDel, DelEnum.NO.getCode());
        List<ActivityTask> activityTaskList = this.activityTaskService.list(wrapper);
        if (CollectionUtils.isNotEmpty(activityTaskList)) {
            return null;
        }

        Date now = new Date();

        List<ActivityTask> activityTasks = new ArrayList<>();
        for (Long goodsId : goodsIdList) {
            ActivityTask activityTask = new ActivityTask();
            activityTask.setMerchantId(merchantId);
            activityTask.setShopId(shopId);
            activityTask.setActivityDefId(activityDefId);
            activityTask.setType(type);
            activityTask.setStartTime(now);
            activityTask.setEndTime(DateUtils.addYears(now, 100));
            activityTask.setMissionNeedTime(missionNeedTime);
            BigDecimal rpAmount = new BigDecimal(amount);
            activityTask.setRpAmount(rpAmount);
            activityTask.setRpStockDef(stockDef);
            activityTask.setRpStock(stockDef);
            activityTask.setWechat(serviceWx);
            activityTask.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
            activityTask.setStatus(ActivityTaskStatusEnum.SAVE.getCode());
            activityTask.setDel(DelEnum.NO.getCode());
            if (rpAmount.equals(BigDecimal.ZERO)) {
                activityTask.setDel(DelEnum.YES.getCode());
            }
            activityTask.setCtime(now);
            activityTask.setUtime(now);
            activityTask.setGoodsId(goodsId);
            activityTasks.add(activityTask);
        }
        return activityTasks;
    }

    @Override
    public Map addResponse(ActivityDef activityDef) {
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityDefId", activityDef.getId());
        resultVO.put("merchantId", activityDef.getMerchantId());
        resultVO.put("shopId", activityDef.getShopId());
        resultVO.put("depositDef", activityDef.getDepositDef());
        return resultVO;
    }

    /***
     * 非草稿修改，支持部分修改
     * @param req
     * @return
     * <pre>
     * 对已发布中的活动 的修改 进行校验：
     *     赠品和免单商品活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
     *                                      领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
     *                                      无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
     *     幸运大转盘 活动，支持修改的选项有：无。
     *     拆红包支持修改的选项有：帮拆人数，活动时长；无法修改的有：红包金额，红包库存，保证金，
     *
     *     收藏加购活动，支持修改的选项有：活动时长，商品    无法修改的有：红包金额，红包库存，保证金，
     *     回填订单活动，支持修改的选项有：无
     *     好评晒图活动，支持修改的选项有：活动时长，若是指定，则可以改商品    无法修改的有：红包金额，红包库存，保证金，
     * </pre>
     */
    @Override
    public SimpleResponse checkActivityDefUpdateNotSave(CreateDefReq req) {
        int type = req.getType();
        if (StringUtils.isNotBlank(req.getExtraData()) && req.getExtraDataObject() == null) {
            req.init();
        }

        ActivityDef activityDefExit = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
        Assert.notNull(activityDefExit, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该编号的活动不存在，defId=" + req.getActivityDefId());
        if (type != activityDefExit.getType()) {
            log.info("活动类型无法修改");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "活动类型无法修改");
        }
        if (req.getStockDef() != null && req.getStockDef() > 0 && !activityDefExit.getStockDef().equals(req.getStockDef())) {
            log.info("活动库存无法修改");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "活动库存无法修改");
        }
        if (StringUtils.isNotBlank(req.getDepositDef()) && new BigDecimal(req.getDepositDef()).compareTo(activityDefExit.getDepositDef()) != 0) {
            log.info("活动保证金无法修改");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "活动保证金无法修改");
        }
        // 赠品和免单商品 活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
        // 领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
        // 无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
        if (type == ActivityDefTypeEnum.ASSIST.getCode() || type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
            // 比较数据库中的数据，和新提交的数据 是否一致
            if (StringUtils.isNotBlank(req.getGoodsIds()) && !req.getGoodsIds().equals(String.valueOf(activityDefExit.getGoodsId()))) {
                log.info("活动商品无法修改");
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "活动商品无法修改");
            }
        }
        if (type == ActivityDefTypeEnum.ASSIST.getCode()) {
            if (req.getExtraDataObject() != null && req.getExtraDataObject().getStockType() > 0 && req.getExtraDataObject().getStockType() != activityDefExit.getStockType()) {
                log.info("扣库存方式无法修改");
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "扣库存方式无法修改");
            }
        }
        // 幸运大转盘 活动，支持修改的选项有：无。
        //回填订单活动，支持修改的选项有：无

        //收藏加购活动，支持修改的选项有：活动时长，商品    无法修改的有：红包金额，红包库存，保证金，
        if (type == ActivityDefTypeEnum.FAV_CART.getCode() || type == ActivityDefTypeEnum.FAV_CART_V2.getCode()) {
            if (req.getExtraDataObject() != null && StringUtils.isNotBlank(req.getExtraDataObject().getCommission())
                    && new BigDecimal(req.getExtraDataObject().getCommission()).compareTo(activityDefExit.getCommission()) != 0) {
                log.info("红包金额无法修改");
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "红包金额无法修改");
            }
        }
        //好评晒图活动，支持修改的选项有：活动时长，若是指定，则可以改商品    无法修改的有：红包金额，红包库存，保证金，
        if (type == ActivityDefTypeEnum.GOOD_COMMENT.getCode()) {
            ActivityDef activityDefUpdate = this.addActivityDef(req);
            if (activityDefExit != null && activityDefExit.getGoodsId() != null && activityDefExit.getGoodsId().equals("-1") && !activityDefUpdate.getGoodsId().equals(activityDefExit.getGoodsId())) {
                log.info("全店下的商品无法修改");
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "全店下的商品无法修改");
            }
        }
        // 拆红包支持修改的选项有：帮拆人数，活动时长；无法修改的有：红包金额，红包库存，保证金，
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            if (StringUtils.isNotBlank(req.getAmount()) &&
                    (activityDefExit.getGoodsPrice().compareTo(new BigDecimal(req.getAmount())) != 0 ||
                            activityDefExit.getCommission().compareTo(new BigDecimal(req.getAmount())) != 0)) {
                log.info("单个红包金额无法修改");
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO8.getCode(), "单个红包金额无法修改");
            }
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    /***
     * 初始化拆红包数据
     * @param req
     * @return
     */
    @Override
    public CreateDefReq openRedpacketInit(CreateDefReq req) {
        if (req.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
            if (req != null && req.getExtraDataObject() == null) {
                req.setExtraDataObject(new CreateDefReq.ExtraData());
            }
            if (req.getExtraDataObject().getCondPersionCount() != null && req.getExtraDataObject().getCondPersionCount() > 0) {
                req.getExtraDataObject().setCondPersionCount(req.getExtraDataObject().getCondPersionCount() + 1);
            }
            req.getExtraDataObject().setHitsPerDraw(1);
            req.setGoodsPrice(req.getAmount());
        }
        return req;
    }

    /***
     * 获取库存变更记录
     * @param activityDef
     * @return
     */
    @Override
    public int getCountChangeStockByDefAudit(ActivityDef activityDef) {
        QueryWrapper<ActivityDefAudit> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDefAudit::getMerchantId, activityDef.getMerchantId())
                .eq(ActivityDefAudit::getShopId, activityDef.getShopId())
                .eq(ActivityDefAudit::getActivityDefId, activityDef.getId())
                .eq(ActivityDefAudit::getDepositStatus, DepositStatusEnum.PAYMENT.getCode());
        return this.activityDefAuditService.count(wrapper);
    }

    /***
     * 根据活动定义-查询该活动的已参与人数
     * @param activityDef
     * @return
     */
    @Override
    public int getUserJoinNumByActivityDef(ActivityDef activityDef) {
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == activityDef.getType()) {
            return this.selectCountUserActivity(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId());
        } else if (ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == activityDef.getType()) {
            return this.selectCountUserActivity(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId());
        } else if (ActivityDefTypeEnum.ASSIST.getCode() == activityDef.getType()) {
            return this.selectCountActivity(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), ActivityTypeEnum.ASSIST);
        } else if (ActivityDefTypeEnum.ASSIST_V2.getCode() == activityDef.getType()) {
            return this.selectCountActivity(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), ActivityTypeEnum.ASSIST_V2);
        } else if (ActivityDefTypeEnum.FAV_CART.getCode() == activityDef.getType()) {
            return this.selectCountUserTask(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), UserTaskTypeEnum.REDPACK_TASK_FAV_CART);
        } else if (ActivityDefTypeEnum.FAV_CART_V2.getCode() == activityDef.getType()) {
            return this.selectCountUserTask(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2);
        } else if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDef.getType()) {
            return this.selectCountUserCoupon(activityDef);
        } else if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDef.getType()) {
            return this.selectCountActivity(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), ActivityTypeEnum.OPEN_RED_PACKET_START);
        } else if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == activityDef.getType()) {
            return this.selectCountUserTask(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), UserTaskTypeEnum.ORDER_REDPACK);
        } else if (ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode() == activityDef.getType()) {
            return this.selectCountUserTask(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), UserTaskTypeEnum.ORDER_REDPACK_V2);
        } else if (ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() == activityDef.getType()) {
            return this.selectCountUserTask(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), UserTaskTypeEnum.GOOD_COMMENT_V2);
        } else if (ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDef.getType()) {
            return this.selectCountUserTask(activityDef.getMerchantId(), activityDef.getShopId(), activityDef.getId(), UserTaskTypeEnum.GOOD_COMMENT);
        } else {
            return 0;
        }
    }

    private int selectCountUserCoupon(ActivityDef activityDef) {
        QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
        ucWrapper.lambda()
                .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                .eq(UserCoupon::getShopId, activityDef.getShopId())
                .eq(UserCoupon::getActivityDefId, activityDef.getId());
        return this.userCouponService.count(ucWrapper);
    }

    /***
     * 获取活动定义等同于单价概念
     * @param activityDef
     * @param activityDefAudit
     * @return
     */
    @Override
    public BigDecimal getDefAuditUnitPrice(ActivityDef activityDef, ActivityDefAudit activityDefAudit) {
        BigDecimal unitPrice = null;
        if (activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                || activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
            unitPrice = activityDefAudit.getDepositApply();
        } else if (activityDef.getType().equals(ActivityDefTypeEnum.LUCKY_WHEEL.getCode())) {
            CommonDict commonDict = this.commonDictService.selectByTypeKey("luckyWheel", "amount");
            unitPrice = new BigDecimal(commonDict.getValue()).multiply(new BigDecimal("4")).setScale(2, BigDecimal.ROUND_DOWN);
        } else {
            unitPrice = activityDefAudit.getDepositApply().divide(new BigDecimal(activityDefAudit.getStockApply()), 2, BigDecimal.ROUND_DOWN);
        }
        return unitPrice;
    }

    @Override
    public boolean splitNewBieTaskDefReq(CreateDefReq req, ActivityDef mainActivityDef, ActivityDef readActivityDef, ActivityDef scanActivityDef) {
        if (req == null || mainActivityDef == null || readActivityDef == null || scanActivityDef == null) {
            return false;
        }

        // 计算活动保证金
        BigDecimal depositDef = calcNewBieTastDeposit(req.getExtraDataObject().getReadAmount(), req.getExtraDataObject().getScanAmount(), req.getStockDef());

        // 组装主任务活动参数
        mainActivityDef.setShopId(req.getShopId());
        mainActivityDef.setMerchantId(req.getMerchantId());
        mainActivityDef.setType(req.getType());
        mainActivityDef.setStockDef(req.getStockDef());
        mainActivityDef.setStock(req.getStockDef());
        mainActivityDef.setDepositDef(depositDef);
        mainActivityDef.setDeposit(depositDef);
        mainActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        mainActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());

        // 计算主活动佣金（任务1佣金+任务2佣金）
        BigDecimal commission = new BigDecimal(req.getExtraDataObject().getReadAmount())
                .add(new BigDecimal(req.getExtraDataObject().getScanAmount()));
        mainActivityDef.setCommission(commission);

        // 组装阅读新手任务参数
        readActivityDef.setShopId(req.getShopId());
        readActivityDef.setMerchantId(req.getMerchantId());
        readActivityDef.setType(ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        readActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getReadAmount()));
        readActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        readActivityDef.setStockDef(req.getStockDef());
        readActivityDef.setStock(req.getStockDef());
        readActivityDef.setDepositDef(new BigDecimal(req.getExtraDataObject().getReadAmount()).multiply(new BigDecimal(req.getStockDef())));
        readActivityDef.setDeposit(new BigDecimal(req.getExtraDataObject().getReadAmount()).multiply(new BigDecimal(req.getStockDef())));
        readActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());

        // 组装扫一扫任务参数
        scanActivityDef.setShopId(req.getShopId());
        scanActivityDef.setMerchantId(req.getMerchantId());
        scanActivityDef.setServiceWx(req.getExtraDataObject().getServiceWx());
        scanActivityDef.setKeyWord(req.getExtraDataObject().getKeywords());
        scanActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getScanAmount()));
        scanActivityDef.setType(ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        scanActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        scanActivityDef.setStockDef(req.getStockDef());
        scanActivityDef.setStock(req.getStockDef());
        scanActivityDef.setDepositDef(new BigDecimal(req.getExtraDataObject().getScanAmount()).multiply(new BigDecimal(req.getStockDef())));
        scanActivityDef.setDeposit(new BigDecimal(req.getExtraDataObject().getScanAmount()).multiply(new BigDecimal(req.getStockDef())));
        scanActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());

        return true;
    }

    @Override
    public BigDecimal calcNewBieTastDeposit(String readAmount, String scanAmount, Integer stockDef) {
        if (StringUtils.isBlank(readAmount) || StringUtils.isBlank(scanAmount) || stockDef == null) {
            return BigDecimal.ZERO;
        }
        // 计算保证金 （活动保证金=（任务一红包金额+任务二红包金额）* 新人任务库存数）
        BigDecimal depositDef = (new BigDecimal(readAmount)
                .add(new BigDecimal(scanAmount))
                .multiply(new BigDecimal(stockDef)));
        return depositDef;
    }

    @Override
    public boolean splitShopManagerTaskDefReq(CreateDefReq req, ActivityDef mainActivityDef,
                                              ActivityDef setWechatActivityDef,
                                              ActivityDef inviteActivityDef, ActivityDef developActivityDef) {
        if (req == null || mainActivityDef == null
                || setWechatActivityDef == null || inviteActivityDef == null || developActivityDef == null) {

        }

        // 计算活动保证金
        BigDecimal depositDef = calcShopManagerTastDeposit(req.getExtraDataObject().getSetWechatAmount(),
                req.getExtraDataObject().getInviteAmount(), req.getExtraDataObject().getDevelopAmount(),
                req.getExtraDataObject().getDevelopTaskTimes(), req.getStockDef());

        // 组装主任务活动参数
        mainActivityDef.setShopId(req.getShopId());
        mainActivityDef.setMerchantId(req.getMerchantId());
        mainActivityDef.setType(req.getType());
        mainActivityDef.setStockDef(req.getStockDef());
        mainActivityDef.setStock(req.getStockDef());
        mainActivityDef.setDepositDef(depositDef);
        mainActivityDef.setDeposit(depositDef);
        mainActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        mainActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        // 计算主活动佣金（任务1佣金+任务2佣金+任务3佣金）
        BigDecimal commission = new BigDecimal(req.getExtraDataObject().getSetWechatAmount())
                .add(new BigDecimal(req.getExtraDataObject().getInviteAmount()))
                .add(new BigDecimal(req.getExtraDataObject().getDevelopAmount()));
        mainActivityDef.setCommission(commission);

        // 组装设置微信群任务活动参数
        setWechatActivityDef.setShopId(req.getShopId());
        setWechatActivityDef.setMerchantId(req.getMerchantId());
        setWechatActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getSetWechatAmount()));
        setWechatActivityDef.setType(ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        setWechatActivityDef.setStartNum(req.getExtraDataObject().getSetWechatTaskTimes());
        setWechatActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        setWechatActivityDef.setStockDef(req.getStockDef());
        setWechatActivityDef.setStock(req.getStockDef());
        setWechatActivityDef.setDepositDef(new BigDecimal(req.getExtraDataObject().getSetWechatAmount()).multiply(new BigDecimal(req.getStockDef())));
        setWechatActivityDef.setDeposit(new BigDecimal(req.getExtraDataObject().getSetWechatAmount()).multiply(new BigDecimal(req.getStockDef())));
        setWechatActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());


        // 组装邀请下级粉丝任务活动参数
        inviteActivityDef.setShopId(req.getShopId());
        inviteActivityDef.setMerchantId(req.getMerchantId());
        inviteActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getInviteAmount()));
        inviteActivityDef.setType(ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        inviteActivityDef.setCondPersionCount(req.getExtraDataObject().getInviteCount());
        inviteActivityDef.setStartNum(req.getExtraDataObject().getInviteTaskTimes());
        inviteActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        inviteActivityDef.setStockDef(req.getStockDef());
        inviteActivityDef.setStock(req.getStockDef());
        inviteActivityDef.setDepositDef(new BigDecimal(req.getExtraDataObject().getInviteAmount()).multiply(new BigDecimal(req.getStockDef())));
        inviteActivityDef.setDeposit(new BigDecimal(req.getExtraDataObject().getInviteAmount()).multiply(new BigDecimal(req.getStockDef())));
        inviteActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());

        // 组装发展粉丝成为店长任务活动参数
        developActivityDef.setShopId(req.getShopId());
        developActivityDef.setMerchantId(req.getMerchantId());
        developActivityDef.setCommission(new BigDecimal(req.getExtraDataObject().getDevelopAmount()));
        developActivityDef.setType(ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        developActivityDef.setCondPersionCount(req.getExtraDataObject().getDevelopCount());
        developActivityDef.setStartNum(req.getExtraDataObject().getDevelopTaskTimes());
        developActivityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        developActivityDef.setStockDef(req.getStockDef());
        developActivityDef.setStock(req.getStockDef());
        developActivityDef.setDepositDef(new BigDecimal(req.getExtraDataObject().getDevelopAmount()).multiply(new BigDecimal(req.getStockDef())));
        developActivityDef.setDeposit(new BigDecimal(req.getExtraDataObject().getDevelopAmount()).multiply(new BigDecimal(req.getStockDef())));
        developActivityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());

        return true;
    }

    @Override
    public BigDecimal calcShopManagerTastDeposit(String setWechatAmount, String inviteAmount, String developAmount, Integer developTaskTimes, Integer stockDef) {
        if (StringUtils.isBlank(setWechatAmount) || StringUtils.isBlank(inviteAmount) ||
                StringUtils.isBlank(developAmount) || developTaskTimes == null || stockDef == null) {
            return BigDecimal.ZERO;
        }

        // 计算保证金 （活动保证金=（任务一红包金额+任务二红包金额+任务三红包金额x10）X 任务参与人数）
        BigDecimal depositDef = (new BigDecimal(setWechatAmount)
                .add(new BigDecimal(inviteAmount))
                .add(new BigDecimal(developAmount).multiply(BigDecimal.TEN)))
                .multiply(new BigDecimal(stockDef));

        return depositDef;
    }

    /**
     * 添加报销活动
     *
     * @param req
     * @return
     */
    @Override
    public ActivityDef addActivityDefReimbursement(CreateDefReq req) {
        if (req == null) {
            return null;
        }

        ActivityDef activityDef = new ActivityDef();
        activityDef.setType(req.getType());
        activityDef.setGoodsPrice(new BigDecimal(req.getAmount()));
        activityDef.setCondDrawTime(Integer.parseInt(req.getExtraDataObject().getCondDrawTime()));
        activityDef.setCondPersionCount(req.getExtraDataObject().getCondPersionCount());
        activityDef.setStockDef(req.getStockDef());
        activityDef.setStock(req.getStockDef());
        activityDef.setDepositStatus(DepositStatusEnum.AWAITING_PAYMENT.getCode());
        activityDef.setStatus(ActivityDefStatusEnum.SAVE.getCode());
        activityDef.setShopId(req.getShopId());
        activityDef.setMerchantId(req.getMerchantId());
        activityDef.setDeposit(calcReimbursementDeposit(req.getStockDef(), req.getAmount()));
        activityDef.setDepositDef(calcReimbursementDeposit(req.getStockDef(), req.getAmount()));
        activityDef.setDel(DelEnum.NO.getCode());
        activityDef.setCtime(new Date());
        activityDef.setUtime(new Date());
            activityDef.setHitsPerDraw(req.getExtraDataObject().getHitsPerDraw());

        Map<String, Object> amountJson = new HashMap<>(2);
        Map<String, Object> perAmount = new HashMap<>(2);
        perAmount.put("min", calcPerMinAmount(new BigDecimal(req.getAmount())).toString());
        perAmount.put("max", req.getExtraDataObject().getMaxReimbursement());
        amountJson.put("perAmount", perAmount);
        amountJson.put("personalMaxAmount", req.getExtraDataObject().getMaxAmount());
        activityDef.setAmountJson(JSON.toJSONString(amountJson));

        return activityDef;
    }

    /**
     * 计算报销活动单人单次报销最小值
     *
     * @param amount
     * @return
     */
    public BigDecimal calcPerMinAmount(BigDecimal amount) {
        return amount.multiply(new BigDecimal(3));
    }

    /**
     * 计算报销活动保证金
     *
     * @param stockDef
     * @param amount
     * @return
     */
    public BigDecimal calcReimbursementDeposit(int stockDef, String amount) {
        return new BigDecimal(amount).multiply(new BigDecimal(stockDef));
    }

    /***
     * 根据活动定义，查询参与该活动定义的人数(收藏加购，好评晒图，回填订单)
     * 注意：因为userTask未存储defId,所以只能先查询到activityTaskId，然后在查询userTask记录分组统计数字
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param userTaskTypeEnum
     * @return
     */
    private int selectCountUserTask(Long merchantId, Long shopId, Long activityDefId, UserTaskTypeEnum userTaskTypeEnum) {
        List<Integer> types = new ArrayList<>();
        types.add(userTaskTypeEnum.getCode());
        if (userTaskTypeEnum == UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2) {
            types.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
        }

        QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
        userTaskWrapper.lambda()
                .eq(UserTask::getMerchantId, merchantId)
                .eq(UserTask::getShopId, shopId)
                .eq(UserTask::getActivityDefId, activityDefId)
                .in(UserTask::getType, types)
                .groupBy(UserTask::getUserId);

        List<UserTask> userTasks = this.userTaskService.list(userTaskWrapper);
        int num = 0;
        if (!CollectionUtils.isEmpty(userTasks)) {
            num = userTasks.size();
        }
        return num;
    }

    /***
     * 根据活动定义，查询参与该活动定义的人数(转盘，助力，拆红包)
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param activityTypeEnum
     * @return
     */
    private int selectCountActivity(Long merchantId, Long shopId, Long activityDefId, ActivityTypeEnum activityTypeEnum) {
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getMerchantId, merchantId)
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getActivityDefId, activityDefId)
                .eq(Activity::getType, activityTypeEnum.getCode())
        ;
        return this.activityService.count(wrapper);
    }

    /***
     * 根据活动定义(免单)
     * 注意：因为userActivity未存储defId,所以只能先查询到activityId，然后在查询userActivity记录分组统计数字
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    private int selectCountUserActivity(Long merchantId, Long shopId, Long activityDefId) {
        // 获取机器人
        List<User> robots = userService.listRobot();
        // 免单抽奖 统计页
        List<Long> robotIds = new ArrayList<>();
        for (User user : robots) {
            robotIds.add(user.getId());
        }
        //查询汇总userActivity
        QueryWrapper<UserActivity> userActivityQueryWrapper = new QueryWrapper<>();
        userActivityQueryWrapper.lambda()
                .eq(UserActivity::getMerchantId, merchantId)
                .eq(UserActivity::getShopId, shopId)
                .notIn(UserActivity::getUserId, robotIds)
                .eq(UserActivity::getActivityDefId, activityDefId)
        ;
        return this.userActivityService.count(userActivityQueryWrapper);
    }
}
