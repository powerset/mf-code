package mf.code.api.seller;

import mf.code.api.seller.service.SellerWxmpService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.seller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月21日 13:59
 */
@RestController
@RequestMapping("/api/seller/Wxmp")
public class SellerWxmpV3Api {
    @Autowired
    private SellerWxmpService sellerWxmpService;

    /***
     * 创建场景wx小程序码
     * @param merchantId 商户编号
     * @param shopId 店铺编号
     * @param wxmpType wx小程序码类型 1:店铺二维码 2：活动二维码 3：商品二维码
     * @param sceneType 场景类型 1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览
     * @param goodsId 商品编号
     * @return
     */
    @RequestMapping(path = "/getSceneWxmp", method = RequestMethod.GET)
    public SimpleResponse createSceneWxmp(@RequestParam(name = "merchantId") Long merchantId,
                                          @RequestParam(name = "shopId") Long shopId,
                                          @RequestParam(name = "wxmpType") int wxmpType,
                                          @RequestParam(name = "sceneType", required = false, defaultValue = "0") int sceneType,
                                          @RequestParam(name = "goodsId", required = false, defaultValue = "0") Long goodsId) {
        return this.sellerWxmpService.createSceneWxmp(merchantId, shopId, wxmpType, sceneType, goodsId);
    }

}
