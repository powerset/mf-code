package mf.code.api.seller.v4.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.activitydef.ActivityDefDelegate;
import mf.code.api.MybatisPageDto;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.dto.SupplyStockReq;
import mf.code.api.seller.utils.SellerTransformParam;
import mf.code.api.seller.v4.dto.*;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefJoinUserService;
import mf.code.api.seller.v4.service.ActivityDefUpdateService;
import mf.code.api.seller.v4.service.SellerActivityDefV4Service;
import mf.code.statistics.api.service.SellerStatisticsService;
import mf.code.statistics.api.utils.StatisticsValidator;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.v4.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月18日 16:53
 */
@Service
@Slf4j
public class SellerActivityDefV4ServiceImpl implements SellerActivityDefV4Service {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityDefUpdateService activityDefUpdateService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityDefJoinUserService activityDefJoinUserService;
    @Autowired
    private SellerStatisticsService sellerStatisticsService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserService userService;

    /***
     * v4版创建营销活动
     * @param req
     * @return
     */
    @Override
    @Transactional
    public SimpleResponse createDef(CreateDefReq req) {
        if (req == null || req.getType() == null || req.getMerchantId() == null || req.getShopId() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "入参不满足条件");
        }
        // 模版模式调用创建流程
        ActivityDefDelegate activityDefDelegate = new ActivityDefDelegate();
        SimpleResponse simpleResponse = activityDefDelegate.create(req);
        if (simpleResponse.error()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        }
        return simpleResponse;
    }

    @Override
    public SimpleResponse updateDef(CreateDefReq req) {
        if (req == null || req.getActivityDefId() == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        // 模版模式调用更新流程
        ActivityDefDelegate activityDefDelegate = new ActivityDefDelegate();
        SimpleResponse simpleResponse = activityDefDelegate.update(req);
        if (simpleResponse.error()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        }
        return simpleResponse;
    }

    /***
     * v4版查询营销活动
     * @param activityDefId
     * @return
     */
    @Override
    public SimpleResponse queryDef(Long activityDefId) {
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityDefId);
        Assert.isTrue(activityDef != null, "该编号的活动不存在，defId=" + activityDefId);

        // 当活动类型为新人任务时
        if (activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
            return queryNewBieDef(activityDef);
        }

        // 当活动类型为店长任务时
        if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
            return queryShopManagerDef(activityDef);
        }

        if (activityDef.getType() == ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            return queryReimbursementDef(activityDef);
        }
        QueryActivityDefResp resp = new QueryActivityDefResp();
        resp.from(activityDef);
        List<Long> merchantShopCouponIds = JSONObject.parseArray(activityDef.getMerchantCouponJson(), Long.class);

        if (!CollectionUtils.isEmpty(merchantShopCouponIds)) {
            QueryWrapper<MerchantShopCoupon> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(MerchantShopCoupon::getMerchantId, activityDef.getMerchantId())
                    .eq(MerchantShopCoupon::getShopId, activityDef.getShopId())
                    .in(MerchantShopCoupon::getId, merchantShopCouponIds)
            ;
            List<MerchantShopCoupon> merchantShopCouponList = merchantShopCouponService.list(queryWrapper);
            if (!CollectionUtils.isEmpty(merchantShopCouponList)) {
                List<Map> list = new ArrayList<>();
                for (MerchantShopCoupon merchantShopCoupon : merchantShopCouponList) {
                    list.add(BeanMapUtil.beanToMapIgnore(merchantShopCoupon, "merchantId", "shopId", "amount", "del", "ctime", "utime", "validDay", "desc", "tpwd", "displayEndTime", "displayStartTime"));
                }
                resp.setMerchantCouponJson(list);
            }
        }

        List<Long> goodsIdList = null;
        if (activityDef.getGoodsId() != null) {
            goodsIdList = new ArrayList<>();
            goodsIdList.add(activityDef.getGoodsId());
        }
        if (activityDef.getGoodsIds() != null) {
            goodsIdList = JSON.parseArray(activityDef.getGoodsIds(), Long.class);

        }
        if (goodsIdList != null) {
            Map<String, Object> goodsVOList = goodsService.getGoodsVOListByIds(goodsIdList);
            List<Map<String, Object>> goodsVOs = (List<Map<String, Object>>) goodsVOList.get("goodsVOList");
            resp.setGoodsVOList(goodsVOs);
            if (!CollectionUtils.isEmpty(goodsVOs)) {
                resp.setGoodsMainPic(goodsVOs.get(0).get("displayBanner").toString());
            }
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /**
     * 查询全额报销活动
     * @return
     */
    private SimpleResponse queryReimbursementDef(ActivityDef activityDef){
        if (activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "参数不正确");
        }

        if (StringUtils.isBlank(activityDef.getAmountJson())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "数据有误");
        }

        Map<String, Object> result = new HashMap<>(7);
        result.put("stockDef", activityDef.getStockDef());
        JSONObject amount = JSON.parseObject(activityDef.getAmountJson());
        result.put("id", activityDef.getId());
        result.put("maxReimbursement", amount.getJSONObject("perAmount").get("max"));
        result.put("minReimbursement", amount.getJSONObject("perAmount").get("min"));
        result.put("maxAmount", amount.get("personalMaxAmount"));
        result.put("amount", activityDef.getGoodsPrice());
        result.put("condDrawTime", activityDef.getCondDrawTime());
        result.put("depositDef", activityDef.getDepositDef());
        result.put("hitsPerDraw", activityDef.getHitsPerDraw());
        result.put("type", activityDef.getType());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(result);
        return simpleResponse;
    }

    /**
     * 查询新人任务详情
     *
     * @return
     */
    private SimpleResponse queryNewBieDef(ActivityDef activityDef) {
        if (activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        // 查询新手阅读任务
        ActivityDef readActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        if (readActivityDef == null) {
            log.error("新人阅读任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        // 查询扫一扫任务
        ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        if (scanActivityDef == null) {
            log.error("扫一扫任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        // 查询拆红包任务
        ActivityDef openRedPackActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        if (openRedPackActivityDef == null) {
            log.error("拆红包任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        QueryNewBieAcvityDefResp resp = new QueryNewBieAcvityDefResp();
        resp.setId(activityDef.getId());
        resp.setStockDef(activityDef.getStock());
        resp.setDepositDef(activityDef.getDepositDef());

        resp.setServiceWx(scanActivityDef.getServiceWx());
        resp.setKeywords(scanActivityDef.getKeyWord());
        resp.setScanAmount(scanActivityDef.getCommission());

        resp.setOpenRedPackactiveId(openRedPackActivityDef.getId());
        resp.setOpenRedPackActivitySock(openRedPackActivityDef.getStock());
        resp.setReadAmount(readActivityDef.getCommission());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);

        return simpleResponse;

    }

    /**
     * 查询店长任务详情
     *
     * @return
     */
    private SimpleResponse queryShopManagerDef(ActivityDef activityDef) {
        if (activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        // 查询设置微信群任务
        ActivityDef setWechatActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        if (setWechatActivityDef == null) {
            log.error("设置微信群任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        // 查询邀请下级粉丝任务
        ActivityDef inviteActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        if (inviteActivityDef == null) {
            log.error("邀请下级粉丝任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        // 查询发展粉丝为店长任务
        ActivityDef developActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        if (developActivityDef == null) {
            log.error("发展粉丝为店长任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询异常");
        }

        QueryShopManagerDefResp resp = new QueryShopManagerDefResp();
        resp.setId(activityDef.getId());
        resp.setStockDef(activityDef.getStock());
        resp.setDepositDef(activityDef.getDepositDef());

        resp.setSetWechatAmount(setWechatActivityDef.getCommission());

        resp.setInviteCount(inviteActivityDef.getCondPersionCount());
        resp.setInviteAmount(inviteActivityDef.getCommission());

        resp.setDevelopCount(developActivityDef.getCondPersionCount());
        resp.setDevelopAmount(developActivityDef.getCommission());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);

        return simpleResponse;
    }

    /***
     * v4版分页查询营销活动
     * @param merchantId
     * @param shopId
     * @param status 1 待支付 2 进行中 3 已结束 4 库存警告 5 已结算
     * @param type (2 免单抽奖活动 4, "幸运大转盘" 5, "回填订单红包任务") 6, "赠品活动") 7, "收藏加购" 9, "拆红包" 10, "好评晒图"),
     * @param size
     * @param pageNum
     * @return
     */
    @Override
    public SimpleResponse queryDefByPage(Long merchantId,
                                         Long shopId,
                                         Integer status,
                                         Integer type,
                                         Integer size,
                                         Integer pageNum) {
        Page<ActivityDef> page = new Page<>(pageNum, size);
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                .orderByDesc(ActivityDef::getId)
        ;
        if (status != null && StringUtils.equals(SellerTransformParam.STOCK_WARN_STATUS, String.valueOf(status))) {
            wrapper.apply("stock - stock_def * 0.1 <= 0");
        }
        if (type != null) {
            List<Integer> typeList = new ArrayList<>();
            if (type == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode() || type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
                typeList.add(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
                typeList.add(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode());
                wrapper.and(params -> params.in("type", typeList));
            } else if (type == ActivityDefTypeEnum.ORDER_BACK_V2.getCode() || type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
                typeList.add(ActivityDefTypeEnum.ORDER_BACK.getCode());
                typeList.add(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
                wrapper.and(params -> params.in("type", typeList));
            } else if (type == ActivityDefTypeEnum.FAV_CART_V2.getCode() || type == ActivityDefTypeEnum.FAV_CART.getCode()) {
                typeList.add(ActivityDefTypeEnum.FAV_CART.getCode());
                typeList.add(ActivityDefTypeEnum.FAV_CART_V2.getCode());
                wrapper.and(params -> params.in("type", typeList));
            } else if (type == ActivityDefTypeEnum.ASSIST_V2.getCode() || type == ActivityDefTypeEnum.ASSIST.getCode()) {
                typeList.add(ActivityDefTypeEnum.ASSIST.getCode());
                typeList.add(ActivityDefTypeEnum.ASSIST_V2.getCode());
                wrapper.and(params -> params.in("type", typeList));
            } else if (type == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() || type == ActivityDefTypeEnum.GOOD_COMMENT.getCode()) {
                typeList.add(ActivityDefTypeEnum.GOOD_COMMENT.getCode());
                typeList.add(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode());
                wrapper.and(params -> params.in("type", typeList));
            } else {
                wrapper.and(params -> params.eq("type", type));
            }
        }
        if (status != null) {
            List<Integer> statusList = SellerTransformParam.VO2PO4ActivityDefStatus(String.valueOf(status));
            if (null == statusList) {
                String error = "所传的status值 和 接口定义的status值 不匹配，to 木子";
                SellerTransformParam.sendErrorMessage(error);
            }
            wrapper.and(params -> params.in("status", statusList));
        }
        IPage<ActivityDef> activityDefPage = activityDefService.page(page, wrapper);
        List<ActivityDef> activityDefs = activityDefPage.getRecords();

        SimpleResponse simpleResponse = new SimpleResponse();
        MybatisPageDto<SellerActivityDefResp> defRespMybatisPageDto = new MybatisPageDto<SellerActivityDefResp>();
        if (CollectionUtils.isEmpty(activityDefs)) {
            simpleResponse.setData(defRespMybatisPageDto);
            return simpleResponse;
        }

        for (ActivityDef activityDef : activityDefs) {
            SellerActivityDefResp resp = new SellerActivityDefResp();
            resp.from(activityDef);
            //库存变更记录
            resp.setChangeStockTimes(this.activityDefAboutService.getCountChangeStockByDefAudit(activityDef));
            //用户参与人数 TODO 待优化
            if (activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
                SimpleResponse newBieApplyDetailResp = newBieApplyDetail(activityDef, 1,  1);
                if (newBieApplyDetailResp.error()) {
                    return newBieApplyDetailResp;
                } else {
                    QueryApplyDetailResp queryApplyDetailResp = (QueryApplyDetailResp) newBieApplyDetailResp.getData();
                    resp.setJoinPerson(queryApplyDetailResp.getJoinCount());
                }
            } else if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
                SimpleResponse newBieApplyDetailResp = shopManagerApplyDetail(activityDef, 1,  1);
                if (newBieApplyDetailResp.error()) {
                    return newBieApplyDetailResp;
                } else {
                    QueryApplyDetailResp queryApplyDetailResp = (QueryApplyDetailResp) newBieApplyDetailResp.getData();
                    resp.setJoinPerson(queryApplyDetailResp.getJoinCount());
                }
            } else if (activityDef.getType() == ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode()){
                SimpleResponse reimbursementJoinInfo = this.activityDefJoinUserService.reimbursementJoinInfo(1, 1, activityDef);
                if (reimbursementJoinInfo.error()) {
                    return reimbursementJoinInfo;
                } else {
                    QueryActivityDefUserJoinResp queryActivityDefUserJoinResp = (QueryActivityDefUserJoinResp) reimbursementJoinInfo.getData();
                    resp.setJoinPerson(queryActivityDefUserJoinResp.getJoinNum());
                }
            } else {
                resp.setJoinPerson(this.activityDefAboutService.getUserJoinNumByActivityDef(activityDef));
            }

            //占用库存
            int occupyStock = sellerStatisticsService.getOccupyStock(activityDef.getId(), String.valueOf(activityDef.getType()), null);
            resp.setOccupyStock(occupyStock);
            defRespMybatisPageDto.getContent().add(resp);
        }
        defRespMybatisPageDto.from(activityDefPage.getCurrent(), activityDefPage.getSize(), activityDefPage.getTotal(), activityDefPage.getPages());
        simpleResponse.setData(defRespMybatisPageDto);
        return simpleResponse;
    }

    /***
     * 获取保证金
     * @param req
     * @return
     */
    @Override
    public SimpleResponse getDepositDef(CreateDefReq req) {

        // 模版模式调用更新流程
        ActivityDefDelegate activityDefDelegate = new ActivityDefDelegate();
        SimpleResponse simpleResponse = activityDefDelegate.deposit(req);
        if (simpleResponse.error()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        }
        return simpleResponse;
    }

    /***
     * 补库存
     * @param req defId, stock必填，其余不要
     * @return
     */
    @Override
    @Transactional
    public SimpleResponse supplyAddStock(CreateDefReq req) {
        Assert.isTrue(req.getActivityDefId() != null && req.getActivityDefId() > 0, "亲，请输入正确的数值");
        ActivityDef activityDef = null;
        if (req.getActivityDefId() != null && req.getActivityDefId() > 0) {
            activityDef = this.activityDefService.selectByPrimaryKey(req.getActivityDefId());
            Assert.isTrue(activityDef != null, "该编号的活动不存在哦！defId=" + req.getActivityDefId());
            Assert.isTrue(activityDef.getStatus() != ActivityDefStatusEnum.REFUNDED.getCode(), "该活动已清算，不能补充库存！defId=" + req.getActivityDefId());
            req.from(activityDef);
            req.setExtraDataObject(new CreateDefReq.ExtraData());
            req.getExtraDataObject().from(activityDef);
        } else {
            req.init();
        }
        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        if (!success) {
            return RedisForbidRepeat.NXError();
        }
        SimpleResponse deposit = this.activityDefAboutService.getDeposit(req);
        if (deposit.error()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return deposit;
        }
        Map data = (Map) deposit.getData();
        BigDecimal depositDef = new BigDecimal(data.get("depositDef").toString());

        if (activityDef.getType().equals(ActivityDefTypeEnum.NEWBIE_TASK.getCode())) {
            // 新手任务补库存
            SimpleResponse simpleResponse = newBieAddStock(activityDef, req, depositDef);
            if (simpleResponse.error()) {
                return simpleResponse;
            }
        } else if (activityDef.getType().equals(ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode())) {
            // 店长任务补库存
            SimpleResponse simpleResponse = shopManagerAddStock(activityDef, req, depositDef);
            if (simpleResponse.error()) {
                return simpleResponse;
            }
        }
        SupplyStockReq supplyStockReq = new SupplyStockReq();
        supplyStockReq.setDepositDef(depositDef.toString());
        if (req.getStockDef() != null && req.getStockDef() > 0) {
            supplyStockReq.setStockDef(String.valueOf(req.getStockDef()));
        }
        supplyStockReq.setActivityDefId(String.valueOf(req.getActivityDefId()));
        supplyStockReq.setMerchantId(String.valueOf(req.getMerchantId()));
        ActivityDefAudit activityDefAudit = activityDefAuditService.supplyStock(supplyStockReq);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
            return activityDefAuditService.saveError();
        }

        // 处理返回数据
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityDefAuditId", activityDefAudit.getId());
        resultVO.put("merchantId", activityDefAudit.getMerchantId());
        resultVO.put("shopId", activityDefAudit.getShopId());
        resultVO.put("depositDef", activityDefAudit.getDepositApply());

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + req.getMerchantId());
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resultVO);
        return simpleResponse;
    }

    // 新手任务补库存
    private SimpleResponse newBieAddStock(ActivityDef activityDef, CreateDefReq req, BigDecimal depositDef) {
        if (activityDef == null || depositDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数");
        }

        // 查询阅读新手任务
        ActivityDef readActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        if (readActivityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询阅读新手任务出错");
        }

        // 查询扫一扫任务
        ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        if (scanActivityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询扫一扫任务出错");
        }

        readActivityDef.setStockDef(readActivityDef.getStockDef() + req.getStockDef());
        readActivityDef.setStock(readActivityDef.getStock() + req.getStockDef());
        readActivityDef.setDepositDef(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        readActivityDef.setDeposit(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        activityDefService.updateActivityDef(readActivityDef);

        scanActivityDef.setStockDef(scanActivityDef.getStockDef() + req.getStockDef());
        scanActivityDef.setStock(scanActivityDef.getStock() + req.getStockDef());
        scanActivityDef.setDepositDef(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        scanActivityDef.setDeposit(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        activityDefService.updateActivityDef(scanActivityDef);

        return new SimpleResponse();
    }

    // 店长任务补库存
    private SimpleResponse shopManagerAddStock(ActivityDef activityDef, CreateDefReq req, BigDecimal depositDef) {
        if (activityDef == null || depositDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "请传入正确的参数");
        }

        // 查询设置微信群任务
        ActivityDef setWechatActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        if (setWechatActivityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询设置微信群任务出错");
        }

        // 查询邀请下级粉丝任务
        ActivityDef inviteActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        if (inviteActivityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询邀请下级粉丝任务出错");
        }

        // 查询发展粉丝成为店长任务
        ActivityDef developActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        if (inviteActivityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "查询发展粉丝成为店长任务出错");
        }

        setWechatActivityDef.setStockDef(setWechatActivityDef.getStockDef() + req.getStockDef());
        setWechatActivityDef.setStock(setWechatActivityDef.getStock() + req.getStockDef());
        setWechatActivityDef.setDepositDef(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        setWechatActivityDef.setDeposit(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        activityDefService.updateActivityDef(setWechatActivityDef);

        inviteActivityDef.setStockDef(inviteActivityDef.getStockDef() + req.getStockDef());
        inviteActivityDef.setStock(inviteActivityDef.getStock() + req.getStockDef());
        inviteActivityDef.setDepositDef(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        inviteActivityDef.setDeposit(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        activityDefService.updateActivityDef(inviteActivityDef);

        developActivityDef.setStockDef(developActivityDef.getStockDef() + req.getStockDef());
        developActivityDef.setStock(developActivityDef.getStock() + req.getStockDef());
        developActivityDef.setDepositDef(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        developActivityDef.setDeposit(new BigDecimal(req.getAmount()).multiply(new BigDecimal(req.getStockDef())));
        activityDefService.updateActivityDef(developActivityDef);

        return new SimpleResponse();
    }

    /***
     * v4版分页查询营销活动参与人数具体信息
     * @param merchantId
     * @param shopId
     * @param type (2 免单抽奖活动 4, "幸运大转盘" 5, "回填订单红包任务") 6, "赠品活动") 7, "收藏加购" 9, "拆红包" 10, "好评晒图"),
     * @param size
     * @param pageNum
     * @return
     */
    @Override
    public SimpleResponse queryActivityDefUserJoinByPage(Long merchantId, Long shopId, Long activityDefId, Integer type, Integer size, Integer pageNum) {
        // 参数校验
        SimpleResponse<Object> response = StatisticsValidator.checkParam4queryActivityDefUserJoin(String.valueOf(pageNum), String.valueOf(size), String.valueOf(activityDefId));
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 获取活动定义信息
        ActivityDef activityDef = activityDefService.getById(activityDefId);
        if (null == activityDef) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("errorMessage", "数据库未查到ActivityDef活动的信息，id = " + activityDefId);
            response.setData(resultVO);
            return response;
        }
        // 初始化展示数据
        if (ActivityDefTypeEnum.ORDER_BACK.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.orderBackUserJoinInfo(pageNum, size, activityDef);
        } else if (ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.orderBackUserJoinInfo(pageNum, size, activityDef);
        } else if (ActivityDefTypeEnum.ASSIST.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.assistOrOpenRedPackUserJoinInfo(pageNum, size, activityDef);
        } else if (ActivityDefTypeEnum.ASSIST_V2.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.assistOrOpenRedPackUserJoinInfo(pageNum, size, activityDef);
        } else if (ActivityDefTypeEnum.FAV_CART.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.userTaskUserJoinInfo(pageNum, size, activityDef, UserTaskTypeEnum.REDPACK_TASK_FAV_CART);
        } else if (ActivityDefTypeEnum.FAV_CART_V2.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.userTaskUserJoinInfo(pageNum, size, activityDef, UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2);
        } else if (ActivityDefTypeEnum.LUCKY_WHEEL.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.luckyWheelUserJoinInfo(pageNum, size, activityDef);
        } else if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.assistOrOpenRedPackUserJoinInfo(pageNum, size, activityDef);
        } else if (ActivityDefTypeEnum.FILL_BACK_ORDER.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.userTaskUserJoinInfo(pageNum, size, activityDef, UserTaskTypeEnum.ORDER_REDPACK);
        } else if (ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.userTaskUserJoinInfo(pageNum, size, activityDef, UserTaskTypeEnum.ORDER_REDPACK_V2);
        } else if (ActivityDefTypeEnum.GOOD_COMMENT.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.userTaskUserJoinInfo(pageNum, size, activityDef, UserTaskTypeEnum.GOOD_COMMENT);
        } else if (ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.userTaskUserJoinInfo(pageNum, size, activityDef, UserTaskTypeEnum.GOOD_COMMENT_V2);
        } else if (ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode() == activityDef.getType()) {
            return this.activityDefJoinUserService.reimbursementJoinInfo(pageNum, size, activityDef);
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "不支持的活动类型");
        }
    }

    @Override
    public SimpleResponse getOpenRedPackActivityInfo(Long merchantId, Long shopId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (merchantId == null || shopId == null) {
            simpleResponse.setData(null);
            return simpleResponse;
        }
        ActivityDef activityDef = activityDefService.selectLastOpenRedPackActiyityDef(merchantId, shopId);
        if (activityDef == null) {
            simpleResponse.setData(null);
            return simpleResponse;
        }

        boolean exist = false;

        if (activityDef.getParentId() == null) {
            exist = true;
        }

        if (activityDef != null && activityDef.getParentId() != null && !activityDef.getId().equals(activityDef.getParentId())) {
            // 当拆红包活动有关联父活动时，查询父活动是否清算
            ActivityDef parentActivityDef = activityDefService.selectByPrimaryKey(activityDef.getParentId());
            if (parentActivityDef == null || parentActivityDef.getDel() == DelEnum.YES.getCode()) {
                exist = true;
            }

            if (parentActivityDef != null && parentActivityDef.getStatus() == ActivityDefStatusEnum.REFUNDED.getCode()) {
                exist = true;
            }
        }

        if (exist) {
            Map<String, Object> result = new HashMap<>(2);
            result.put("activityId", activityDef.getId());
            result.put("stockDef", activityDef.getStock());
            simpleResponse.setData(result);
        } else {
            simpleResponse.setData(null);
            return simpleResponse;
        }

        return simpleResponse;
    }

    @Override
    public SimpleResponse applyDetail(Long merchantId, Long activityDefId, Integer size, Integer pageNum) {
        if (merchantId == null || activityDefId == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "入参不满足条件");
        }

        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        if (activityDef == null || !activityDef.getMerchantId().equals(merchantId)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动不存在,@to 木子");
        }

        // 当活动类型是新人任务时
        if (activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
            return this.newBieApplyDetail(activityDef, size, pageNum);
        }

        // 当活动类型是店长任务时
        if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
            return this.shopManagerApplyDetail(activityDef, size, pageNum);
        }

        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "活动类型不正确，@to 木子");
    }

    /**
     * 查询新人任务活动参与人数
     *
     * @param activityDef
     * @param size
     * @param pageNum
     * @return
     */
    private SimpleResponse newBieApplyDetail(ActivityDef activityDef, Integer size, Integer pageNum) {

        // 统计任务一（新手任务）完成人数
        ActivityDef readTaskActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        if (readTaskActivityDef == null) {
            log.error("阅读任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "新手任务不存在，@to 大妖怪");
        }
        Integer task1Count = 0;

        // 统计任务2（扫一扫任务）完成人数
        ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
        if (scanActivityDef == null) {
            log.error("扫一扫任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "扫一扫任务不存在，@to 大妖怪");
        }
        Integer task2Count = 0;

        // 统计任务3（拆红包任务）完成人数
        ActivityDef openRedPackActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
//        if (openRedPackActivityDef == null) {
//            log.error("拆红包任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
//            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "拆红包任务不存在，@to 大妖怪");
//        }
        Integer task3Count = 0;

        // 计算完成人数
        Integer finishedCount = 0;
        // 统计总参与人数
        Integer joinCount = 0;

        // 查询列表
        List<Long> activityDefIds = new ArrayList<>();
        activityDefIds.add(readTaskActivityDef.getId());
        activityDefIds.add(scanActivityDef.getId());
        if (openRedPackActivityDef != null) {
            activityDefIds.add(openRedPackActivityDef.getId());
        }

        List<Integer> types = new ArrayList<>();
        types.add(UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        types.add(UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode());
        types.add(UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode());

        List<Map<String, Object>> list = new ArrayList<>();

        Page<UserCoupon> page = new Page<>(pageNum, size);
        IPage<UserCoupon> userCoupons = userCouponService.page(
                page,
                new QueryWrapper<UserCoupon>()
                        .lambda()
                        .in(UserCoupon::getActivityDefId, activityDefIds)
                        .eq(UserCoupon::getActivityTaskId, activityDef.getId())
                        .in(UserCoupon::getType, types)
                        .groupBy(UserCoupon::getUserId)
        );


        List<UserCoupon> allUserCouponList = userCouponService.list(
                new QueryWrapper<UserCoupon>()
                        .lambda()
                        .in(UserCoupon::getActivityDefId, activityDefIds)
                        .eq(UserCoupon::getActivityTaskId, activityDef.getId())
                        .in(UserCoupon::getType, types)
                        .groupBy(UserCoupon::getUserId)
        );

        for (UserCoupon item : allUserCouponList) {
            UserCoupon readCoupon = null;
            UserCoupon scanCoupon = null;
            UserCoupon openReedPackCoupon = null;
            if (item.getActivityDefId().equals(readTaskActivityDef.getId())) {
                // 当奖品类型为阅读任务时，对扫一扫任务进行补充
                readCoupon = item;
                scanCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, scanActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

            } else if (item.getActivityDefId().equals(scanActivityDef.getId())) {
                // 当奖品类型为扫一扫任务时，对阅读任务进行补充
                scanCoupon = item;
                readCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, readTaskActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            // 对拆红包任务进行补充
            if (openRedPackActivityDef != null) {
                openReedPackCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, openRedPackActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            if (readCoupon != null) {
                task1Count ++;
            }

            if (scanCoupon != null) {
                task2Count ++;
            }

            if (openReedPackCoupon != null) {
                task3Count ++;
            }

            // 统计任务完成人数，任务1、2、3都完成才算一个
            if (readCoupon != null && scanCoupon != null && openReedPackCoupon != null) {
                finishedCount++;
            }

            UserTask userTask = null;
            // 统计总参与人数，任务1、任务2、任务3完成一个，算一个
            if (readCoupon != null || scanCoupon != null || openReedPackCoupon != null) {
                joinCount++;
            }
        }

        for (UserCoupon item : page.getRecords()) {
            UserCoupon readCoupon = null;
            UserCoupon scanCoupon = null;
            UserCoupon openReedPackCoupon = null;
            if (item.getActivityDefId().equals(readTaskActivityDef.getId())) {
                // 当奖品类型为阅读任务时，对扫一扫任务进行补充
                readCoupon = item;
                scanCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, scanActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

            } else if (item.getActivityDefId().equals(scanActivityDef.getId())) {
                // 当奖品类型为扫一扫任务时，对阅读任务进行补充
                scanCoupon = item;
                readCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, readTaskActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            // 对拆红包任务进行补充
            if (openRedPackActivityDef != null) {
                openReedPackCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, openRedPackActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            // 对比参加时间
            List<UserCoupon> userCouponList = new ArrayList<>();
            if (readCoupon != null) {
                userCouponList.add(readCoupon);
            }
            if (scanCoupon != null) {
                userCouponList.add(scanCoupon);
            }
            if (openReedPackCoupon != null) {
                userCouponList.add(openReedPackCoupon);
            }
            userCouponList.sort((a, b) -> (int) (a.getCtime().getTime() - b.getCtime().getTime()));


            // 查询用户信息
            User user = userService.selectByPrimaryKey(item.getUserId());

            Map<String, Object> tableItem = new HashMap<>();
            if (userCouponList.get(0).getCtime() != null) {
                tableItem.put("joinTime", DateUtil.dateToString(userCouponList.get(0).getCtime(), DateUtil.POINT_DATE_FORMAT_TWO));
            } else {
                continue;
            }
            tableItem.put("userName", user.getNickName());
            tableItem.put("phone", user.getMobile());
            tableItem.put("tast1Status", readCoupon != null);
            tableItem.put("tast2Status", scanCoupon != null);
            tableItem.put("tast3Status", openReedPackCoupon != null);

            list.add(tableItem);
        }

        QueryApplyDetailResp resp = new QueryApplyDetailResp();
        resp.setJoinCount(joinCount);
        resp.setFinishedCount(finishedCount);
        resp.setTask1Count(task1Count);
        resp.setTask2Count(task2Count);
        resp.setTask3Count(task3Count);
        resp.setType(activityDef.getType());

        resp.setList(list);

        resp.setTotal(userCoupons.getTotal());
        resp.setCurrentPage(userCoupons.getCurrent());
        resp.setSize(userCoupons.getSize());
        resp.setPages(userCoupons.getPages());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);


        return simpleResponse;
    }

    /**
     * 查询店长任务活动参与人数
     *
     * @param activityDef
     * @param size
     * @param pageNum
     * @return
     */
    private SimpleResponse shopManagerApplyDetail(ActivityDef activityDef, Integer size, Integer pageNum) {
        // 统计总参与人数
        Integer joinCount = 0;

        // 统计任务一（设置微信群任务）完成人数
        ActivityDef setWechatTaskActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        if (setWechatTaskActivityDef == null) {
            log.error("设置微信群任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "设置微信群任务不存在，@to 大妖怪");
        }
        Integer task1Count = 0;

        // 统计任务2（邀请下级粉丝任务）完成人数
        ActivityDef inviteActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        if (inviteActivityDef == null) {
            log.error("邀请下级粉丝任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "邀请下级粉丝任务不存在，@to 大妖怪");
        }
        Integer task2Count = 0;

        // 统计任务3（发展粉丝成为店长任务）完成人数
        ActivityDef developActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(), ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        if (developActivityDef == null) {
            log.error("发展粉丝成为店长任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "发展粉丝成为店长任务不存在，@to 大妖怪");
        }
        Integer task3Count = 0;

        // 计算完成人数
        Integer finishedCount = 0;

        // 查询列表
        List<Long> activityDefIds = new ArrayList<>();
        activityDefIds.add(setWechatTaskActivityDef.getId());
        activityDefIds.add(inviteActivityDef.getId());
        activityDefIds.add(developActivityDef.getId());

        List<Map<String, Object>> list = new ArrayList<>();

        Page<UserCoupon> page = new Page<>(pageNum, size);
        IPage<UserCoupon> userCoupons = userCouponService.page(
                page,
                new QueryWrapper<UserCoupon>()
                        .lambda()
                        .in(UserCoupon::getActivityDefId, activityDefIds)
                        .eq(UserCoupon::getActivityTaskId, activityDef.getId())
                        .groupBy(UserCoupon::getUserId)
        );

        List<UserCoupon> allUserCouponList = userCouponService.list(
                new QueryWrapper<UserCoupon>()
                        .lambda()
                        .in(UserCoupon::getActivityDefId, activityDefIds)
                        .eq(UserCoupon::getActivityTaskId, activityDef.getId())
                        .groupBy(UserCoupon::getUserId)
        );

        for (UserCoupon item : allUserCouponList) {
            UserCoupon setWechatCoupon = null;
            UserCoupon inviteCoupon = null;
            UserCoupon developCoupon = null;

            if (item.getActivityDefId().equals(setWechatTaskActivityDef.getId())) {
                // 当奖品类型为设置微信群任务时, 补充邀请粉丝、发展粉丝为店长任务

                setWechatCoupon = item;

                inviteCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, inviteActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                developCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, developActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            if (item.getActivityDefId().equals(inviteActivityDef.getId())) {
                // 当奖品类型为邀请粉丝任务时, 补充设置微信群、发展粉丝为店长任务
                setWechatCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, setWechatTaskActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                inviteCoupon = item;

                developCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, developActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            if (item.getActivityDefId().equals(developActivityDef.getId())) {
                // 当奖品类型为邀请粉丝任务时, 补充设置微信群、发展粉丝为店长任务
                setWechatCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, setWechatTaskActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                inviteCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, inviteActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                developCoupon = item;
            }

            if (setWechatCoupon != null) {
                task1Count ++;
            }

            if (inviteCoupon != null) {
                task2Count ++;
            }

            if (developCoupon != null) {
                task3Count ++;
            }

            // 统计任务完成人数，任务1、2、3都完成才算一个
            if (setWechatCoupon != null && inviteCoupon != null && developCoupon != null) {
                finishedCount++;
            }

            if (setWechatCoupon != null || inviteCoupon != null || developCoupon != null) {
                joinCount++;
            }
        }

        for (UserCoupon item : userCoupons.getRecords()) {
            UserCoupon setWechatCoupon = null;
            UserCoupon inviteCoupon = null;
            UserCoupon developCoupon = null;

            if (item.getActivityDefId().equals(setWechatTaskActivityDef.getId())) {
                // 当奖品类型为设置微信群任务时, 补充邀请粉丝、发展粉丝为店长任务

                inviteCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, inviteActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                developCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, developActivityDef.getId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                setWechatCoupon = item;
            }

            if (item.getActivityDefId().equals(inviteActivityDef.getId())) {
                // 当奖品类型为邀请粉丝任务时, 补充设置微信群、发展粉丝为店长任务
                setWechatCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, setWechatTaskActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                inviteCoupon = item;

                developCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, developActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );
            }

            if (item.getActivityDefId().equals(developActivityDef.getId())) {
                // 当奖品类型为邀请粉丝任务时, 补充设置微信群、发展粉丝为店长任务
                setWechatCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, setWechatTaskActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                inviteCoupon = userCouponService.getOne(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityDefId, inviteActivityDef.getId())
                                .eq(UserCoupon::getUserId, item.getUserId())
                );

                developCoupon = item;
            }

            // 对比参加时间
            List<UserCoupon> userCouponList = new ArrayList<>();
            if (setWechatCoupon != null) {
                userCouponList.add(setWechatCoupon);
            }
            if (inviteCoupon != null) {
                userCouponList.add(inviteCoupon);
            }
            if (developCoupon != null) {
                userCouponList.add(developCoupon);
            }
            userCouponList.sort((a, b) -> (int) (a.getCtime().getTime() - b.getCtime().getTime()));


            // 查询用户信息
            User user = userService.selectByPrimaryKey(item.getUserId());

            Map<String, Object> tableItem = new HashMap<>();
            tableItem.put("joinTime", DateUtil.dateToString(userCouponList.get(0).getCtime(), DateUtil.POINT_DATE_FORMAT_TWO));
            tableItem.put("userName", user.getNickName());
            tableItem.put("phone", user.getMobile());
            tableItem.put("tast1Status", setWechatCoupon != null);
            tableItem.put("tast2Status", inviteCoupon != null);
            tableItem.put("tast3Status", developCoupon != null);
            if (user.getRole() == UserRoleEnum.SHOP_MANAGER.getCode()) {
                tableItem.put("becomeShopManagerTime", user.getRoleTime());
            }

            list.add(tableItem);
        }

        QueryApplyDetailResp resp = new QueryApplyDetailResp();
        resp.setJoinCount(joinCount);
        resp.setFinishedCount(finishedCount);
        resp.setTask1Count(task1Count);
        resp.setTask2Count(task2Count);
        resp.setTask3Count(task3Count);
        resp.setType(activityDef.getType());

        resp.setList(list);

        resp.setTotal(userCoupons.getTotal());
        resp.setCurrentPage(userCoupons.getCurrent());
        resp.setSize(userCoupons.getSize());
        resp.setPages(userCoupons.getPages());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);


        return simpleResponse;
    }
}
