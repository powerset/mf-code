package mf.code.api.seller.v4.service;

import mf.code.api.seller.dto.CreateDefReq;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.seller.v4.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月18日 16:52
 */
public interface SellerActivityDefV4Service {
    /***
     * v4版创建营销活动
     * @param req
     * @return
     */
    SimpleResponse createDef(CreateDefReq req);

    /***
     * v4版更新营销活动
     * @param req
     * @return
     */
    SimpleResponse updateDef(CreateDefReq req);


    /***
     * v4版查询营销活动
     * @param activityDefId
     * @return
     */
    SimpleResponse queryDef(Long activityDefId);


    /***
     * v4版分页查询营销活动
     * @param merchantId
     * @param shopId
     * @param status 1 待支付 2 进行中 3 已结束 4 库存警告 5 已结算
     * @param type (2 免单抽奖活动 4, "幸运大转盘" 5, "回填订单红包任务") 6, "赠品活动") 7, "收藏加购" 9, "拆红包" 10, "好评晒图"),
     * @param size
     * @param pageNum
     * @return
     */
    SimpleResponse queryDefByPage(Long merchantId,
                                  Long shopId,
                                  Integer status,
                                  Integer type,
                                  Integer size,
                                  Integer pageNum);


    /***
     * 获取保证金
     * @param req
     * @return
     */
    SimpleResponse getDepositDef(CreateDefReq req);

    /***
     * 补库存
     * @param req defId, stock必填，其余不要
     * @return
     */
    SimpleResponse supplyAddStock(CreateDefReq req);


    /***
     * v4版分页查询营销活动参与人数具体信息
     * @param merchantId
     * @param shopId
     * @param type (2 免单抽奖活动 4, "幸运大转盘" 5, "回填订单红包任务") 6, "赠品活动") 7, "收藏加购" 9, "拆红包" 10, "好评晒图"),
     * @param size
     * @param pageNum
     * @return
     */
    SimpleResponse queryActivityDefUserJoinByPage(Long merchantId,
                                                  Long shopId,
                                                  Long activityDefId,
                                                  Integer type,
                                                  Integer size,
                                                  Integer pageNum);

    /**
     * 查询当前用户是否有拆红包活动
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    SimpleResponse getOpenRedPackActivityInfo(Long merchantId, Long shopId);

    /**
     * 查询参与用户详情
     *
     * @param merchantId
     * @param activityDefId
     * @param size
     * @param pageNum
     * @return
     */
    SimpleResponse applyDetail(Long merchantId, Long activityDefId, Integer size, Integer pageNum);
}
