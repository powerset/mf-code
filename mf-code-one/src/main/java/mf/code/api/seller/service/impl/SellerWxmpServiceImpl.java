package mf.code.api.seller.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityDefTypeService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.enums.SceneTypeEnum;
import mf.code.api.seller.enums.WxmpTypeEnum;
import mf.code.api.seller.service.SellerWxmpService;
import mf.code.common.caller.wxmp.WeixinMpConstants;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.api.seller.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月21日 14:05
 */
@Service
public class SellerWxmpServiceImpl implements SellerWxmpService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private ActivityDefTypeService activityDefTypeService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private WxpayProperty wxpayProperty;

    /***
     * 创建场景wx小程序码
     * @param merchantId 商户编号
     * @param shopId 店铺编号
     * @param wxmpType wx小程序码类型 1:店铺二维码 2：活动二维码 3：商品二维码
     * @param sceneType 场景类型 1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览
     * @param goodsId 商品编号
     * @return
     */
    @Override
    public SimpleResponse createSceneWxmp(Long merchantId, Long shopId, int wxmpType, int sceneType, Long goodsId) {
        Object[] objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_SHOP};
        String wxaCodeUnlimit = "";
        if (WxmpTypeEnum.WXMP_SHOP.getCode() == wxmpType) {
            //店铺小程序码
            wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), StringUtil.join(objects, ","), WeixinMpConstants.WXCODESHOPPATH + shopId + "/", 430, false);
        } else if (WxmpTypeEnum.WXMP_ACTIVITY.getCode() == wxmpType) {
            List<ActivityDef> activityDefList = this.getActivityDef(merchantId, shopId, null);
            if (CollectionUtils.isEmpty(activityDefList)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该商户没有创建任何活动");
            }
            //活动小程序码
            if (sceneType == 0) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "场景码没有具体数值");
            }
            //具体的跳转所对应的入参支持(待前端小伙伴确定后跟进)
            Long detailParameterValule = getDetailParameterValule(activityDefList, merchantId, shopId, sceneType,goodsId);
            if (detailParameterValule == null && sceneType != SceneTypeEnum.FILLORDER.getCode()  && sceneType != SceneTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "没有具体的业务参数，sceneType={}", sceneType);
            }
            objects = getActivityObjects(objects, merchantId, shopId, sceneType, detailParameterValule);
            String path = getActivityPath(merchantId, shopId, sceneType) + "/";
            wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), StringUtil.join(objects, ","), path, 430, false);
        } else if (WxmpTypeEnum.WXMP_GOODS.getCode() == wxmpType) {
            //商品小程序码
            objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_GOODS, goodsId};
            wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), StringUtil.join(objects, ","), WeixinMpConstants.WXCODE_GOODS_PATH + shopId + goodsId + "/", 430, false);
        }
        if (StringUtils.isBlank(wxaCodeUnlimit)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "生成小程序码失败");
        }
        Map<String, Object> resp = new HashMap<>();
        resp.put("imgUrl", wxaCodeUnlimit);
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }


    /***
     * 查询活动任务
     * @param merchantId
     * @param shopId
     * @return
     */
    private List<ActivityTask> getActivityTasks(Long merchantId, Long shopId) {
        QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityTask::getMerchantId, merchantId)
                .eq(ActivityTask::getShopId, shopId)
                .eq(ActivityTask::getDepositStatus, DepositStatusEnum.PAYMENT.getCode())
                .eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
                .eq(ActivityTask::getDel, DelEnum.NO.getCode())
                .in(ActivityTask::getType, Arrays.asList(ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode(),
                        ActivityTaskTypeEnum.FAVCART.getCode(), ActivityTaskTypeEnum.GOOD_COMMENT.getCode()))
                .orderByDesc(ActivityTask::getId)
        ;
        return activityTaskService.list(wrapper);
    }


    /***
     * 匹配小程序码的场景
     * @param objects
     * @param merchantId
     * @param shopId
     * @param sceneType
     * @return
     */
    @Override
    public Object[] getActivityObjects(Object[] objects, Long merchantId, Long shopId, int sceneType, Long detailParameterValule) {
        if (sceneType == SceneTypeEnum.FILLORDER.getCode()) {
            if (detailParameterValule != null) {
                objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_FILLORDER, detailParameterValule};
            } else {
                objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_FILLORDER};
            }
        } else if (sceneType == SceneTypeEnum.GOODCOMMENT.getCode()) {
            objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_GOODCOMMENT20, detailParameterValule};
        } else if (sceneType == SceneTypeEnum.OPENREDPACK.getCode()) {
            objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_OPENREDPACK, detailParameterValule};
        } else if (sceneType == SceneTypeEnum.ASSIST.getCode()) {
            if (detailParameterValule != null) {
                objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_ASSIST, detailParameterValule};
            } else {
                //若无具体活动，则跳转首页，暂时性。后期跳免费领取页面/选择商品页面
                objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_SHOP};
            }
        } else if (sceneType == SceneTypeEnum.LUCKYWHEEL.getCode()) {
            objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_LUCKYWHEEL, detailParameterValule};
        } else if (sceneType == SceneTypeEnum.FAVCART.getCode()) {
            objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_FAVCART, detailParameterValule};
        } else if (sceneType == SceneTypeEnum.ORDERBACK.getCode()) {
            objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_ORDERBACK, detailParameterValule};
        } else if (sceneType == SceneTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            if (detailParameterValule != null) {
                objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_FULL_REIMBURSEMENT, detailParameterValule};
            } else {
                objects = new Object[]{merchantId, shopId, WeixinMpConstants.SCENE_ACTIVITY_FULL_REIMBURSEMENT};
            }
        }
        return objects;
    }

    /***
     * 获取场景码对应的业务值
     * @param activityDefList
     * @param merchantId
     * @param shopId
     * @param sceneType
     * @return
     */
    @Override
    public Long getDetailParameterValule(List<ActivityDef> activityDefList, Long merchantId, Long shopId, int sceneType) {
        //场景类型 1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览
        /***
         * 6;//回填订单活动场景生成的小程序码 scene后可不带参数
         * 7;//好评晒图活动场景生成的小程序码 scene后 goodsId
         * 11;//收藏加购活动场景生成的小程序码 scene后 goodsId
         * 8;//拆红包活动场景生成的小程序码 scene后 activityDefId
         * 9;//助力活动场景生成的小程序码 scene后 activityId
         * 10;//幸运大转盘活动场景生成的小程序码 scene后 activityId
         * 12;//免单抽奖活动场景生成的小程序码 scene后 activityDefId
         */
        Long parameterValule = null;
        List<ActivityTask> activityTasks = null;
        if (sceneType == SceneTypeEnum.FILLORDER.getCode() ||
                sceneType == SceneTypeEnum.FAVCART.getCode() ||
                sceneType == SceneTypeEnum.GOODCOMMENT.getCode()) {
            activityTasks = this.getActivityTasks(merchantId, shopId);
        }

        if (sceneType == SceneTypeEnum.FILLORDER.getCode()) {
            //回填订单 不带参数
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.GOODCOMMENT.getCode()) {
            //好评晒图 todo 新好评晒图
            ActivityTask activityDefGoodsComment = this.activityDefTypeService.getGoodsCommentPo(activityTasks);
            if (activityDefGoodsComment != null) {
                parameterValule = activityDefGoodsComment.getActivityDefId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.OPENREDPACK.getCode()) {
            //拆红包
            ActivityDef activityDefOpenRedPack = this.activityDefTypeService.getOpenRedPack(activityDefList);
            if (activityDefOpenRedPack != null) {
                parameterValule = activityDefOpenRedPack.getId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.ASSIST.getCode()) {
            //赠品
            ActivityDef activityDefAssist = this.activityDefTypeService.getAssist(activityDefList);
            if (activityDefAssist != null) {
                //Activity activity = this.getActivity(merchantId, shopId, activityDefAssist.getId(), ActivityTypeEnum.ASSIST.getCode());
                List typeList = new ArrayList();
                //typeList.add(activityDefAssist.getType());
                typeList.add(ActivityTypeEnum.ASSIST.getCode());
                typeList.add(ActivityTypeEnum.ASSIST_V2.getCode());
                Activity activity = this.getActivityAfterAddType(merchantId, shopId, activityDefAssist.getId(), typeList);
                if (activity != null) {
                    parameterValule = activity.getId();
                }
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.LUCKYWHEEL.getCode()) {
            //幸运大转盘
            ActivityDef activityDefLuckyWheel = this.activityDefTypeService.getLuckyWheel(activityDefList);
            if (activityDefLuckyWheel != null) {
                Activity activity = this.getActivity(merchantId, shopId, activityDefLuckyWheel.getId(), ActivityTypeEnum.LUCK_WHEEL.getCode());
                if (activity != null) {
                    // 活动id
                    parameterValule = activity.getId();
                }
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.FAVCART.getCode()) {
            //收藏加购
            ActivityTask activityDefFavCart = this.activityDefTypeService.getFavCartPo(activityTasks);
            if (activityDefFavCart != null) {
                // 商品id
                parameterValule = activityDefFavCart.getGoodsId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.ORDERBACK.getCode()) {
            //免单抽奖
            ActivityDef activityDefOrderBack = this.activityDefTypeService.getOrderBack(activityDefList);
            if (activityDefOrderBack != null) {
                parameterValule = activityDefOrderBack.getId();
            }
            return parameterValule;
        }
        return parameterValule;
    }

    /***
     * 获取场景码对应的业务值
     * @param activityDefList
     * @param merchantId
     * @param shopId
     * @param sceneType
     * @return
     */
    @Override
    public Long getDetailParameterValule(List<ActivityDef> activityDefList, Long merchantId, Long shopId, int sceneType, Long goodsId) {
        //场景类型 1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览
        /***
         * 6;//回填订单活动场景生成的小程序码 scene后可不带参数
         * 7;//好评晒图活动场景生成的小程序码 scene后 goodsId
         * 11;//收藏加购活动场景生成的小程序码 scene后 goodsId
         * 8;//拆红包活动场景生成的小程序码 scene后 activityDefId
         * 9;//助力活动场景生成的小程序码 scene后 activityId
         * 10;//幸运大转盘活动场景生成的小程序码 scene后 activityId
         * 12;//免单抽奖活动场景生成的小程序码 scene后 activityDefId
         */
        Long parameterValule = null;
        List<ActivityTask> activityTasks = null;
        if (sceneType == SceneTypeEnum.FILLORDER.getCode() ||
                sceneType == SceneTypeEnum.FAVCART.getCode() ||
                sceneType == SceneTypeEnum.GOODCOMMENT.getCode() ||
                sceneType == SceneTypeEnum.FULL_REIMBURSEMENT.getCode()){
            activityTasks = this.getActivityTasks(merchantId, shopId);
        }

        if (sceneType == SceneTypeEnum.FILLORDER.getCode()) {
            //回填订单 不带参数
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.GOODCOMMENT.getCode()) {
            //好评晒图 todo 新好评晒图
            ActivityTask activityDefGoodsComment = this.activityDefTypeService.getGoodsCommentPo(activityTasks);
            if (activityDefGoodsComment != null) {
                parameterValule = activityDefGoodsComment.getActivityDefId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.OPENREDPACK.getCode()) {
            //拆红包
            ActivityDef activityDefOpenRedPack = this.activityDefTypeService.getOpenRedPack(activityDefList);
            if (activityDefOpenRedPack != null) {
                parameterValule = activityDefOpenRedPack.getId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.ASSIST.getCode()) {
            //赠品
            ActivityDef activityDefAssist = this.activityDefTypeService.getAssist(activityDefList,goodsId);
            if(activityDefAssist!=null){
                parameterValule=activityDefAssist.getId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.LUCKYWHEEL.getCode()) {
            //幸运大转盘
            ActivityDef activityDefLuckyWheel = this.activityDefTypeService.getLuckyWheel(activityDefList);
            if (activityDefLuckyWheel != null) {
                Activity activity = this.getActivity(merchantId, shopId, activityDefLuckyWheel.getId(), ActivityTypeEnum.LUCK_WHEEL.getCode());
                if (activity != null) {
                    // 活动id
                    parameterValule = activity.getId();
                }
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.FAVCART.getCode()) {
            //收藏加购
            ActivityTask activityDefFavCart = this.activityDefTypeService.getFavCartPo(activityTasks);
            if (activityDefFavCart != null) {
                // 商品id
                parameterValule = activityDefFavCart.getGoodsId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.ORDERBACK.getCode()) {
            //免单抽奖
            ActivityDef activityDefOrderBack = this.activityDefTypeService.getOrderBack(activityDefList,goodsId);
            if (activityDefOrderBack != null) {
                parameterValule = activityDefOrderBack.getId();
            }
            return parameterValule;
        } else if (sceneType == SceneTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            // 全额报销
            return parameterValule;
        }
        return parameterValule;
    }

    /***
     * 小程序码存储oss的地址
     * @param merchantId
     * @param shopId
     * @param sceneType
     * @return
     */
    private String getActivityPath(Long merchantId, Long shopId, int sceneType) {
        String path = "";
        if (sceneType == SceneTypeEnum.FILLORDER.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_FILLORDER_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.GOODCOMMENT.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_GOODCOMMENT_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.OPENREDPACK.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_OPENREDPACK_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.ASSIST.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_ASSIST_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.LUCKYWHEEL.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_LUCKYWHEEL_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.FAVCART.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_FAVCART_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.ORDERBACK.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_ORDERBACK_PATH + merchantId + shopId;
        } else if (sceneType == SceneTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            path = WeixinMpConstants.WXCODE_ACTIVITY_FULL_REIMBURSEMENT_PATH + merchantId + shopId;
        }
        return path;
    }

    /***
     * 查询活动定义
     * @param merchantId
     * @param shopId
     * @param type
     * @return
     */
    @Override
    public List<ActivityDef> getActivityDef(Long merchantId, Long shopId, Integer type) {
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                .orderByDesc(ActivityDef::getId)
        ;
        if (type != null) {
            wrapper.and(params -> params.eq("type", type));
        }
        return this.activityDefService.list(wrapper);
    }

    /***
     * 获取活动
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param type
     * @return
     */
    private Activity getActivity(Long merchantId, Long shopId, Long activityDefId, int type) {
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getMerchantId, merchantId)
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getActivityDefId, activityDefId)
                .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
                .eq(Activity::getDel, DelEnum.NO.getCode())
                .eq(Activity::getType, type)
                .orderByDesc(Activity::getId)
        ;
        Activity activity = null;
        List<Activity> activities = this.activityService.list(wrapper);
        if (!CollectionUtils.isEmpty(activities)) {
            activity = activities.get(0);
        }
        return activity;
    }

    /***
     * 获取活动
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param typeList
     * @return
     */
    private Activity getActivityAfterAddType(Long merchantId, Long shopId,  Long activityDefId,List<Integer> typeList) {
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getMerchantId, merchantId)
                .eq(Activity::getShopId, shopId)
                .in(Activity::getActivityDefId, activityDefId)
                .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
                .eq(Activity::getDel, DelEnum.NO.getCode())
                .in(Activity::getType, typeList)
                .orderByDesc(Activity::getId)
        ;
        Activity activity = null;
        List<Activity> activities = this.activityService.list(wrapper);
        if (!CollectionUtils.isEmpty(activities)) {
            activity = activities.get(0);
        }
        return activity;
    }
}
