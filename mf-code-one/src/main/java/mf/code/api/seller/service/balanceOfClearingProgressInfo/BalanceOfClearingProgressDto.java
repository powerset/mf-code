package mf.code.api.seller.service.balanceOfClearingProgressInfo;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.ucoupon.repo.po.UserCoupon;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 16:28
 */
@Data
public class BalanceOfClearingProgressDto {
    public static final int BindingActivity = 1;
    public static final int AuditTaskActivity = 2;
    public static final int CanCash = 3;

    //计划保证金==总支付
    private String sumPayMoney = "0";
    //消耗金额==返现了多少
    private String costMoney = "0";
    //剩余金额==剩余库存的金额
    private String remainMoney = "0";
    //绑定金额==占用的金额
    private String bindingMoney = "0";
    //清算余额进度状态 1：绑定活动倒计时 2:审核中任务存在 3:可以清算
    private int status;
    //倒计时
    private Long cutoffTime = 0L;
    //审核中数目
    private int auditNum;

    //时间截止时间
    @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
    private Date cutoffDate;
    /***
     * 活动定义编号
     */
    private Long activityDefId;


    /***
     * 活动类统计
     * @param activityDef
     * @param activityList
     * @param userTaskList
     * @param userCoupons
     */
    public void from(ActivityDef activityDef, List<Activity> activityList, List<UserTask> userTaskList, List<UserCoupon> userCoupons) {
        //中奖数目
        int awarded = 0;
        //审核中数目
        int auditWait = 0;
        //审核失败|审核成功数目
        int auditFailOrAuditSuccess = 0;
        //消耗商品金额
        BigDecimal amount = BigDecimal.ZERO;
        //消耗佣金金额
        BigDecimal commmision = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(userCoupons)) {
            awarded = userCoupons.size();
            for (UserCoupon userCoupon : userCoupons) {
                if (userCoupon.getAmount() != null && userCoupon.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                    amount = amount.add(userCoupon.getAmount());
                }
                if (StringUtils.isNotBlank(userCoupon.getCommissionDef())) {
                    Map<String, Object> jsonMap = JSONObject.parseObject(userCoupon.getCommissionDef(), Map.class);
                    commmision = commmision.add(new BigDecimal(jsonMap.get("commissionDef").toString()));
                }
            }
        }
        Map<Long, UserTask> userTaskMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userTaskList)) {
            for (UserTask userTask : userTaskList) {
                userTaskMap.put(userTask.getActivityId(), userTask);
                if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                    auditWait = auditWait + 1;
                }
                if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_ERROR.getCode() ||
                        userTask.getStatus() == UserTaskStatusEnum.INIT.getCode() ||
                        userTask.getStatus() == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                    auditFailOrAuditSuccess = auditFailOrAuditSuccess + 1;
                }
            }
        }
        //活动进行中并且未开奖的数目
        int activityInProgress = 0;
        Activity activity1 = null;
        if (!CollectionUtils.isEmpty(activityList)) {
            for (Activity activity : activityList) {
                if (activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode() && activity.getAwardStartTime() == null) {
                    activityInProgress = activityInProgress + activity.getHitsPerDraw();
                }
                if (activity1 == null && activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
                    activity1 = activity;
                }
            }
        }
        //倒计时
        if (activityInProgress > 0 || auditWait > 0 || auditFailOrAuditSuccess > 0) {
            UserTask userTask = userTaskMap.get(activity1.getId());
            Date time = activity1.getEndTime();
            if (userTask != null && userTask.getStartTime() != null) {
                time = userTask.getStartTime();
            }
            this.cutoffTime = DateUtils.addHours(time, 24).getTime();
            this.cutoffDate = DateUtils.addHours(time, 24);
        }
        /***佣金概念***/

        //计划金额
        this.sumPayMoney = activityDef.getDepositDef().setScale(2, BigDecimal.ROUND_DOWN).toString();
        //消耗金额
        BigDecimal costAmount = amount.add(commmision);
        this.costMoney = costAmount.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //总绑定数目
        int bindingNum = auditWait + auditFailOrAuditSuccess + activityInProgress;
        BigDecimal bindDefAmount = activityDef.getGoodsPrice().multiply(new BigDecimal(String.valueOf(bindingNum)));
        BigDecimal bindCommissionAmount = activityDef.getCommission().multiply(new BigDecimal(String.valueOf(bindingNum)));
        //绑定金额
        BigDecimal bindingAmount = bindDefAmount.add(bindCommissionAmount);
        this.bindingMoney = bindingAmount.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //剩余金额
        this.remainMoney = activityDef.getDepositDef().subtract(costAmount).subtract(bindingAmount).setScale(2, BigDecimal.ROUND_DOWN).toString();

        if (activityInProgress > 0 || auditFailOrAuditSuccess > 0) {
            this.status = BindingActivity;
        }
        if (auditWait > 0) {
            this.status = AuditTaskActivity;
        }
        boolean canCash = bindingAmount.compareTo(BigDecimal.ZERO) == 0;
        if (canCash) {
            this.status = CanCash;
        }
        this.auditNum = auditWait;
        this.activityDefId = activityDef.getId();
    }

    /***
     * 转盘 旧订单 类统计
     * @param activityDef
     */
    public void from(ActivityDef activityDef) {
        this.sumPayMoney = activityDef.getDepositDef().setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.remainMoney = activityDef.getDeposit().setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.bindingMoney = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.costMoney = activityDef.getDepositDef().subtract(activityDef.getDeposit()).setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.status = CanCash;
        this.auditNum = 0;//TODO?
        this.activityDefId = activityDef.getId();
    }

    /***
     * 新版订单类统计
     * @param activityDef
     * @param userTasks
     */
    public void from(ActivityDef activityDef, List<UserTask> userTasks) {
        this.sumPayMoney = activityDef.getDepositDef().setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.remainMoney = activityDef.getDeposit().setScale(2, BigDecimal.ROUND_DOWN).toString();

        //绑定的金额
        BigDecimal bindingAmount = BigDecimal.ZERO;
        int bindingNum = 0;
        if (!CollectionUtils.isEmpty(userTasks)) {
            bindingNum = userTasks.size();
            BigDecimal commission = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
            bindingAmount = commission.multiply(new BigDecimal(bindingNum));
        }
        this.bindingMoney = bindingAmount.setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.costMoney = activityDef.getDepositDef().subtract(activityDef.getDeposit()).setScale(2, BigDecimal.ROUND_DOWN).toString();

        boolean canCash = bindingAmount.compareTo(BigDecimal.ZERO) == 0;
        if (canCash) {
            this.status = CanCash;
        }
        if (bindingNum > 0) {
            this.status = AuditTaskActivity;
        }
        if (bindingAmount.compareTo(BigDecimal.ZERO) > 0) {
            this.remainMoney = activityDef.getDeposit().subtract(bindingAmount).setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        this.auditNum = bindingNum;
        this.activityDefId = activityDef.getId();
    }


    /***
     * 红包任务，拆红包 类统计
     * @param activityDef
     * @param sum
     * @param award
     * @param reamin
     * @param binding
     * @param auditNum
     * @param failOrAuditSuccess
     */
    public void from(ActivityDef activityDef, BigDecimal sum, BigDecimal award, BigDecimal reamin, BigDecimal binding, int auditNum, int failOrAuditSuccess) {
        this.sumPayMoney = sum.setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.remainMoney = reamin.setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.bindingMoney = binding.setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.costMoney = award.setScale(2, BigDecimal.ROUND_DOWN).toString();
        this.auditNum = auditNum;
        if (failOrAuditSuccess > 0) {
            this.status = BindingActivity;
        }
        if (auditNum > 0) {
            this.status = BindingActivity;
        }
        boolean canCash = binding.compareTo(BigDecimal.ZERO) == 0;
        if (canCash) {
            this.status = CanCash;
        }
        this.activityDefId = activityDef.getId();
    }

    /***
     * 好评晒图，收藏加购 类统计
     * @param activityDef
     * @param userTaskList
     * @param userCoupons
     */
    public void from(ActivityDef activityDef, List<UserTask> userTaskList, List<UserCoupon> userCoupons) {
        //审核中数目
        int auditWait = 0;
        //消耗商品金额
        BigDecimal amount = BigDecimal.ZERO;
        //消耗佣金金额
        BigDecimal commmision = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(userCoupons)) {
            for (UserCoupon userCoupon : userCoupons) {
                if (userCoupon.getAmount() != null && userCoupon.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                    amount = amount.add(userCoupon.getAmount());
                }
                if (StringUtils.isNotBlank(userCoupon.getCommissionDef())) {
                    Map<String, Object> jsonMap = JSONObject.parseObject(userCoupon.getCommissionDef(), Map.class);
                    commmision = commmision.add(new BigDecimal(jsonMap.get("commissionDef").toString()));
                }
            }
        }
        //消耗的总金额
        BigDecimal costAmount = amount.add(commmision);
        //绑定的金额
        BigDecimal bindingAmount = BigDecimal.ZERO;
        int bindingNum = 0;
        if (!CollectionUtils.isEmpty(userTaskList)) {
            bindingNum = userTaskList.size();
            bindingAmount = activityDef.getCommission().multiply(new BigDecimal(bindingNum));
            for (UserTask userTask : userTaskList) {
                if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                    auditWait++;
                }
            }
        }
        //总金额
        this.sumPayMoney = activityDef.getDepositDef().setScale(2, BigDecimal.ROUND_DOWN).toString();
        //消耗的总金额
        this.costMoney = costAmount.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //绑定金额
        this.bindingMoney = bindingAmount.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //剩余金额
        this.remainMoney = activityDef.getDepositDef().subtract(costAmount).subtract(bindingAmount).setScale(2, BigDecimal.ROUND_DOWN).toString();

        if (bindingNum > 0) {
            this.status = AuditTaskActivity;
        }

        boolean canCash = bindingAmount.compareTo(BigDecimal.ZERO) == 0;
        if (canCash && auditWait == 0) {
            this.status = CanCash;
        }
        if (auditNum > 0) {
            this.status = BindingActivity;
        }
        this.auditNum = auditWait;
        this.activityDefId = activityDef.getId();
    }
}
