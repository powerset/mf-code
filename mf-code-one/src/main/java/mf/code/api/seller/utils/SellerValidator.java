package mf.code.api.seller.utils;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.dto.OperationReq;
import mf.code.api.seller.dto.SupplyStockReq;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BaseValidator;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * mf.code.api.seller.utils
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-27 上午9:20
 */
@Slf4j
public class SellerValidator extends BaseValidator {

    public static SimpleResponse<Object> checkCreateParams(ActivityDefV2Req activityDefV2Req) {
        // 校验必填参数
        SimpleResponse<Object> response = checkBaseParams(activityDefV2Req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        int type = Integer.parseInt(activityDefV2Req.getType());

        // 根据活动类型，判断必填参数 校验
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
            // 创建免单商品活动 校验 必传参数
            return checkGiftParams(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            // 创建 原 新人有礼活动
            return checkGiftParams(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.ASSIST.getCode()) {
            // 创建 助力裂变活动
            return checkGiftParams(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            // 创建新人大转盘互动 校验 必传参数
            return checkLuckyWheelParams(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.RED_PACKET.getCode()) {
            // 创建红包活动 校验必传参数
            return checkRedPacketParams(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            // 创建拆红包活动 校验必传参数
            return checkOpenRedPacketParams(activityDefV2Req);
        }

        String errorMessage = "所传的活动类型type和校验的活动类型type无法对应，须沟通解决 to 木子 and 百川";
        return sendErrorMessage(errorMessage);
    }

    public static SimpleResponse<Object> checkParams4getDeposit(ActivityDefV2Req activityDefV2Req) {
        // 当存在activityDefId 时
        String activityDefId = activityDefV2Req.getActivityDefId();
        if (StringUtils.isNotBlank(activityDefId) && RegexUtils.StringIsNumber(activityDefId)) {
            return checkParam4SupplyStock(activityDefV2Req);
        }
        // 校验必填参数
        String typeStr = activityDefV2Req.getType();
        if (StringUtils.isBlank(typeStr) || !RegexUtils.StringIsNumber(typeStr)) {
            String errorMessage = "活动类型type未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        // 参数转化
        int type = Integer.parseInt(typeStr);

        // 根据活动类型，判断必填参数 校验
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
            // 创建免单商品活动 校验 必传参数
            return checkGiftParams4getDeposit(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            // 创建 原新人有礼活动 校验 必传参数
            return checkGiftParams4getDeposit(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.ASSIST.getCode()) {
            // 创建 助力裂变活动 校验 必传参数
            return checkGiftParams4getDeposit(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            // 创建新人大转盘互动 校验 必传参数
            return checkLuckyWheelParams(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.RED_PACKET.getCode()) {
            // 创建红包活动 校验必传参数
            return checkRedPacketParams4getDeposit(activityDefV2Req);
        }
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            // 创建红包活动 校验必传参数
            return checkOpenRedPacketParams4getDeposit(activityDefV2Req);
        }
        String errorMessage = "所传的活动类型type和校验的活动类型type无法对应，须沟通解决 to 木子 and 百川";
        return sendErrorMessage(errorMessage);
    }

    private static SimpleResponse<Object> checkOpenRedPacketParams4getDeposit(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String stockDef = activityDefV2Req.getStockDef();
        String openRedpacketAmount = activityDefV2Req.getOpenRedpacketAmount();

        if (StringUtils.isBlank(stockDef) || !RegexUtils.StringIsNumber(openRedpacketAmount)) {
            String errorMessage = "商品库存stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写商品库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(openRedpacketAmount) || !RegexUtils.isAmount(openRedpacketAmount)) {
            String errorMessage = "活动商品goodsId未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择活动商品";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkRedPacketParams4getDeposit(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String goodsIds = activityDefV2Req.getGoodsIdList();
        String stockDef = activityDefV2Req.getStockDef();
        String fillBackOrderAmount = activityDefV2Req.getFillBackOrderAmount();
        String favCartAmount = activityDefV2Req.getFavCartAmount();
        String goodCommentAmount = activityDefV2Req.getGoodCommentAmount();

        if (StringUtils.isBlank(goodsIds)) {
            String errorMessage = "活动商品组goodsIds未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择商品组";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(stockDef) || !RegexUtils.StringIsNumber(stockDef)) {
            String errorMessage = "初始红包库存stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写初始红包库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(fillBackOrderAmount) || !RegexUtils.StringIsNumber(fillBackOrderAmount)) {
            String errorMessage = "回填订单金额fillBackOrderAmount未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(favCartAmount) || !RegexUtils.StringIsNumber(favCartAmount)) {
            String errorMessage = "收藏加购金额favCartAmount未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(goodCommentAmount) || !RegexUtils.StringIsNumber(goodCommentAmount)) {
            String errorMessage = "好评晒图金额goodCommentAmount未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkParam4SupplyStock(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String stockDef = activityDefV2Req.getStockDef();
        if (StringUtils.isBlank(stockDef)) {
            String errorMessage = "新增库存数stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写商品库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkGiftParams4getDeposit(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String stockDef = activityDefV2Req.getStockDef();
        String goodsId = activityDefV2Req.getGoodsId();
        BigDecimal commission = activityDefV2Req.getCommission();

        if (StringUtils.isBlank(stockDef)) {
            String errorMessage = "商品库存stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写商品库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)) {
            String errorMessage = "活动商品goodsId未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择活动商品";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        // 任务佣金必须大于等于1元（暂定）
        if (commission == null || commission.compareTo(BigDecimal.ONE) < 0) {
            String errorMessage = "填写正确的任务佣金价格";
            String infoMessage = "请填写任务佣金价格";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkRedPacketParams(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String goodsIds = activityDefV2Req.getGoodsIdList();
        String stockDef = activityDefV2Req.getStockDef();
        String fillBackOrderAmount = activityDefV2Req.getFillBackOrderAmount();
        String favCartAmount = activityDefV2Req.getFavCartAmount();
        String goodCommentAmount = activityDefV2Req.getGoodCommentAmount();
        String goodCommentMissionNeedTime = activityDefV2Req.getGoodCommentMissionNeedTime();
        String favCartMissionNeedTime = activityDefV2Req.getFavCartMissionNeedTime();

        if (StringUtils.isBlank(goodsIds)) {
            String errorMessage = "活动商品组goodsIds未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择商品组";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(stockDef) || !RegexUtils.StringIsNumber(stockDef)) {
            String errorMessage = "初始红包库存stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写初始红包库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(fillBackOrderAmount) || !RegexUtils.StringIsNumber(fillBackOrderAmount)) {
            String errorMessage = "回填订单金额fillBackOrderAmount未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(favCartAmount) || !RegexUtils.StringIsNumber(favCartAmount)) {
            String errorMessage = "收藏加购金额favCartAmount未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(goodCommentAmount) || !RegexUtils.StringIsNumber(goodCommentAmount)) {
            String errorMessage = "好评晒图金额goodCommentAmount未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(goodCommentMissionNeedTime) || !RegexUtils.StringIsNumber(goodCommentMissionNeedTime)) {
            String errorMessage = "好评晒图任务时间goodCommentMissionNeedTime未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(favCartMissionNeedTime) || !RegexUtils.StringIsNumber(favCartMissionNeedTime)) {
            String errorMessage = "收藏加购任务时间favCartMissionNeedTime未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkLuckyWheelParams(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String stockDef = activityDefV2Req.getStockDef();
        if (StringUtils.isBlank(stockDef) || !RegexUtils.StringIsNumber(stockDef)) {
            String errorMessage = "奖励人数stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写奖励人数";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkGiftParams(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        String goodsId = activityDefV2Req.getGoodsId();
        String condPersionCount = activityDefV2Req.getCondPersionCount();
        String hitsPerDraw = activityDefV2Req.getHitsPerDraw();
        String condDrawTime = activityDefV2Req.getCondDrawTime();
        String missionNeedTime = activityDefV2Req.getMissionNeedTime();
        String keyWord = activityDefV2Req.getKeyWord();
        String serviceWx = activityDefV2Req.getServiceWx();
        String stockDef = activityDefV2Req.getStockDef();
        String weighting = activityDefV2Req.getWeighting();
        BigDecimal commission = activityDefV2Req.getCommission();
        String stockType = activityDefV2Req.getStockType();

        if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)) {
            String errorMessage = "活动商品goodsId未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择活动商品";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(condPersionCount) || !RegexUtils.StringIsNumber(condPersionCount)) {
            String errorMessage = "参与人数condPersionCount未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写参与人数";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(hitsPerDraw) || !RegexUtils.StringIsNumber(hitsPerDraw)) {
            String errorMessage = "中奖人数hitsPerDraw未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写中奖人数";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(condDrawTime) || !RegexUtils.StringIsNumber(condDrawTime)) {
            String errorMessage = "活动时间condDrawTime未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写活动时间";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(missionNeedTime) || !RegexUtils.StringIsNumber(missionNeedTime)) {
            String errorMessage = "领奖时间missionNeedTime未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写领奖时间";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(keyWord)) {
            String errorMessage = "关键词keyWord未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择关键词";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        // 20190119 暂时去掉 歪歪
//        if (StringUtils.isBlank(serviceWx)) {
//            String errorMessage = "客服微信号serviceWx未设置正确，to 木子，前端须先做好校验";
//            String infoMessage = "请填写客服微信号";
//            return sendErrorMessage(errorMessage, infoMessage);
//        }
        if (StringUtils.isBlank(stockDef)) {
            String errorMessage = "商品库存stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写商品库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(weighting) || !RegexUtils.StringIsNumber(weighting)) {
            String errorMessage = "是否加权weighting未设置正确，to 木子，前端须先做好校验";
            return sendErrorMessage(errorMessage);
        }
        if (commission == null || commission.compareTo(BigDecimal.ONE) < 0) {
            String errorMessage = "请填写正确的任务佣金";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(stockType) || !RegexUtils.StringIsNumber(stockType)) {
            String errorMessage = "请选择扣库存方式";
            String infoMessage = "请选择扣库存方式";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private static SimpleResponse<Object> checkOpenRedPacketParams(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        String condPersionCount = activityDefV2Req.getCondPersionCount();
        String condDrawTime = activityDefV2Req.getCondDrawTime();
        String stockDef = activityDefV2Req.getStockDef();
        String openRedpacketAmount = activityDefV2Req.getOpenRedpacketAmount();
        String stockType = activityDefV2Req.getStockType();

        if (StringUtils.isBlank(condPersionCount) || !RegexUtils.StringIsNumber(condPersionCount)) {
            String errorMessage = "参与人数condPersionCount未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写参与人数";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(condDrawTime) || !RegexUtils.StringIsNumber(condDrawTime)) {
            String errorMessage = "活动时间condDrawTime未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写活动时间";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(stockDef)) {
            String errorMessage = "红包库存stockDef未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写红包库存";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        if (StringUtils.isBlank(openRedpacketAmount) || !RegexUtils.StringIsNumber(openRedpacketAmount)) {
            String errorMessage = "拆红包任务红包金额未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请填写拆红包任务红包金额";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }


    public static SimpleResponse<Object> checkShopPage(String current, String size, String id) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验参数
        if (StringUtils.isBlank(id) || !RegexUtils.StringIsNumber(id)) {
            String errorMessage = "Id未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(current) || !RegexUtils.StringIsNumber(current)) {
            String errorMessage = "页码current未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(size) || !RegexUtils.StringIsNumber(size)) {
            String errorMessage = "页面展示数量size未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    public static SimpleResponse<Object> checkGoodsId(String goodsId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验参数
        if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)) {
            String errorMessage = "活动商品goodsId未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择活动商品";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    public static SimpleResponse<Object> checkGoodsIdList(String goodsIdList) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验参数
        if (StringUtils.isBlank(goodsIdList)) {
            String errorMessage = "活动商品组goodsIds未设置正确，to 木子，前端须先做好校验";
            String infoMessage = "请选择商品组";
            return sendErrorMessage(errorMessage, infoMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    public static SimpleResponse<Object> checkParams4createAble(ActivityDefV2Req activityDefV2Req) {
        // 校验必填参数
        SimpleResponse<Object> response = checkBaseParams(activityDefV2Req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        int type = Integer.parseInt(activityDefV2Req.getType());
        String goodsId = activityDefV2Req.getGoodsId();
        String goodsIdList = activityDefV2Req.getGoodsIdList();
        // 根据活动类型，判断必填参数 校验
        if (type == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
            // 校验 goodsId
            return checkGoodsId(goodsId);
        }
        if (type == ActivityDefTypeEnum.ORDER_BACK_V2.getCode()) {
            // 校验 goodsId
            return checkGoodsId(goodsId);
        }
        if (type == ActivityDefTypeEnum.ASSIST.getCode()) {
            // 校验 goodsId
            return checkGoodsId(goodsId);
        }
        if (type == ActivityDefTypeEnum.ASSIST_V2.getCode()) {
            // 校验 goodsId
            return checkGoodsId(goodsId);
        }
        if (type == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            // 校验 goodsId
            return checkGoodsId(goodsId);
        }
        if (type == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            // 创建新人大转盘互动 校验 必传参数
            return response;
        }
        if (type == ActivityDefTypeEnum.RED_PACKET.getCode()) {
            // 校验 goodsIdList
            return checkGoodsIdList(goodsIdList);
        }
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) {
            // 校验 goodsIdList
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()) {
            // 校验 goodsIdList
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }
        if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            // 拆红包 不需要校验
            return response;
        }
        if (type == ActivityDefTypeEnum.FAV_CART.getCode()) {
            // 收藏加购
            return response;
        }
        if (type == ActivityDefTypeEnum.FAV_CART_V2.getCode()) {
            // 收藏加购
            return response;
        }
        if (type == ActivityDefTypeEnum.GOOD_COMMENT.getCode()) {
            // 好评晒图
            return response;
        }

        if (type == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()) {
            // 好评晒图
            return response;
        }
        if (type == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
            // 添加新人任务
            return response;
        }

        if (type == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
            // 添加店长任务
            return response;
        }

        if (type == ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode()) {
            // 添加全额报销活动
            return response;
        }

        String errorMessage = "所传的活动类型type和校验的活动类型type无法对应，须沟通解决 to 木子 and 百川";
        return sendErrorMessage(errorMessage);
    }

    private static SimpleResponse<Object> checkBaseParams(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        String merchantId = activityDefV2Req.getMerchantId();
        String shopId = activityDefV2Req.getShopId();
        String type = activityDefV2Req.getType();

        if (StringUtils.isBlank(merchantId) || !RegexUtils.StringIsNumber(merchantId)) {
            String errorMessage = "商户merchantId未设置正确，to 歪歪";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            String errorMessage = "店铺shopId未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        if (StringUtils.isBlank(type) || !RegexUtils.StringIsNumber(type)) {
            String errorMessage = "活动类型type未设置正确，to 木子";
            return sendErrorMessage(errorMessage);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    public static SimpleResponse<Object> checkActivityDefId(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId) || !RegexUtils.StringIsNumber(activityDefId)) {
            String error = "活动定义activityDefId未设置正确，to 木子";
            return sendErrorMessage(error);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    public static SimpleResponse<Object> checkParams4ConfigActivityDef(OperationReq operationReq) {
        SimpleResponse<Object> response = checkActivityDefId(operationReq.getActivityDefId());
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }

        String operation = operationReq.getOperation();
        if (StringUtils.isBlank(operation) || !RegexUtils.StringIsNumber(operation)) {
            String error = "活动操作operation未设置正确，to 木子";
            return sendErrorMessage(error);
        }
        return response;
    }

    public static SimpleResponse<Object> checkParams4SupplyStock(SupplyStockReq supplyStockReq) {
        SimpleResponse<Object> response = checkActivityDefId(supplyStockReq.getActivityDefId());
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        String merchantId = supplyStockReq.getMerchantId();
        if (StringUtils.isBlank(merchantId) || !RegexUtils.StringIsNumber(merchantId)) {
            String errorMessage = "商户merchantId未设置正确，to 歪歪";
            return sendErrorMessage(errorMessage);
        }
        String stockDef = supplyStockReq.getStockDef();
        if (StringUtils.isBlank(stockDef) || !RegexUtils.StringIsNumber(stockDef)) {
            String error = "新增库存数stockDef未设置正确，to 木子, 前端须先做好校验";
            return sendErrorMessage(error);
        }
        return response;
    }
}
