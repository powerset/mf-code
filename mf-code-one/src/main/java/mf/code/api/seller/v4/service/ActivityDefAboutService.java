package mf.code.api.seller.v4.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.v4.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月19日 16:03
 */
public interface ActivityDefAboutService {

    /***
     * 验证金额区间的有效性
     * @param amountJson
     * @return
     */
    SimpleResponse checkAmountJson(String amountJson, ActivityDef activityDef);

    /***
     * 计算活动的保证金
     * @param req
     * @return
     */
    SimpleResponse getDeposit(CreateDefReq req);

    /***
     * 计算活动的保证金
     * @param req
     * @return
     */
    SimpleResponse getDepositByProduct(CreateDefReq req);

    /***
     * 拼装activityDef基类
     * @param req
     * @return
     */
    ActivityDef addActivityDef(CreateDefReq req);

    /***
     * 1：回填订单，2：好评返现，3：收藏加购
     * @param activityDef
     * @return
     */
    List<ActivityTask> addActivityTasks(ActivityDef activityDef, CreateDefReq req);

    /***
     * 创建、修改 活动成功的统一返回
     * @param activityDef
     * @return
     */
    Map addResponse(ActivityDef activityDef);

    /***
     * 非草稿修改，支持部分修改
     * @param req
     * @return
     * <pre>
     * 对已发布中的活动 的修改 进行校验：
     *     赠品和免单商品活动，支持修改的选项有：参与人数condPersionCount，中奖人数hitsPerDraw，活动时间（分）condDrawTime，
     *                                      领奖时间missionNeedTime，关键词keyWord，优惠券merchantCouponJson，客服微信号serviceWx。
     *                                      无法修改的选项有：类型type, 活动商品goodsId，库存stockDef
     *     幸运大转盘 活动，支持修改的选项有：无。
     *     拆红包支持修改的选项有：帮拆人数，活动时长；无法修改的有：红包金额，红包库存，保证金，
     *
     *     收藏加购活动，支持修改的选项有：活动时长，商品    无法修改的有：红包金额，红包库存，保证金，
     *     回填订单活动，支持修改的选项有：无
     *     好评晒图活动，支持修改的选项有：活动时长，若是指定，则可以改商品    无法修改的有：红包金额，红包库存，保证金，
     * </pre>
     */
    SimpleResponse checkActivityDefUpdateNotSave(CreateDefReq req);

    /***
     * 初始化拆红包数据
     * @param req
     * @return
     */
    CreateDefReq openRedpacketInit(CreateDefReq req);

    /***
     * 获取库存变更记录
     * @param activityDef
     * @return
     */
    int getCountChangeStockByDefAudit(ActivityDef activityDef);

    /***
     * 根据活动定义-查询该活动的已参与人数
     * @param activityDef
     * @return
     */
    int getUserJoinNumByActivityDef(ActivityDef activityDef);

    /***
     * 清算时，根据金额，获取单价
     * @param activityDef
     * @param activityDefAudit
     * @return
     */
    BigDecimal getDefAuditUnitPrice(ActivityDef activityDef, ActivityDefAudit activityDefAudit);

    /**
     * 拆分新人任务
     *
     * @param req
     * @param mainActivityDef
     * @param readActivityDef
     * @param scanActivityDef
     * @return
     */
    boolean splitNewBieTaskDefReq(CreateDefReq req, ActivityDef mainActivityDef, ActivityDef readActivityDef, ActivityDef scanActivityDef);

    /**
     * 计算新人任务保证金
     *
     * @param readAmount
     * @param scanAmount
     * @param stockDef
     * @return
     */
    BigDecimal calcNewBieTastDeposit(String readAmount, String scanAmount, Integer stockDef);

    /**
     * 拆分店长任务
     *
     * @param req
     * @param mainActivityDef
     * @param setWechatActivityDef
     * @param inviteActivityDef
     * @param developActivityDef
     * @return
     */
    boolean splitShopManagerTaskDefReq(CreateDefReq req, ActivityDef mainActivityDef,
                                       ActivityDef setWechatActivityDef, ActivityDef inviteActivityDef,
                                       ActivityDef developActivityDef);

    /**
     * 计算店长任务保证金
     *
     * @param setWechatAmount
     * @param inviteAmount
     * @param developAmount
     * @param developTaskTimes
     * @param stockDef
     * @return
     */
    BigDecimal calcShopManagerTastDeposit(String setWechatAmount, String inviteAmount, String developAmount, Integer developTaskTimes, Integer stockDef);

    /**
     * 添加报销活动
     *
     * @param req
     */
    ActivityDef addActivityDefReimbursement(CreateDefReq req);

    /**
     * 计算报销活动保证金
     *
     * @param stockDef
     * @param amount
     * @return
     */
    BigDecimal calcReimbursementDeposit(int stockDef, String amount);

    /**
     * 计算报销活动单人单次报销最小值
     *
     * @param amount
     * @return
     */
    BigDecimal calcPerMinAmount(BigDecimal amount);
}
