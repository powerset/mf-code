package mf.code.api.seller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.dto.OperationReq;
import mf.code.api.seller.dto.SupplyStockReq;
import mf.code.api.seller.service.SellerActivityDefQV2Service;
import mf.code.api.seller.service.SellerActivityDefUV2Service;
import mf.code.api.seller.utils.SellerTransformParam;
import mf.code.api.seller.utils.SellerValidator;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.JsonParseUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * mf.code.api.seller.service
 * Description:
 *
 * @author 百川
 * @date 2018-12-26 下午5:43
 */
@Slf4j
@RestController
/**
 * 注意:保持版本号与其他controller一致，全部小写v
 * 此处兼容处理
 */
@RequestMapping(value = {"/api/seller/V2/activity/def", "/api/seller/v2/activity/def"})
public class SellerActivityDefV2Api {
    @Autowired
    private SellerActivityDefUV2Service sellerActivityDefUV2Service;
    @Autowired
    private SellerActivityDefQV2Service sellerActivityDefQV2Service;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;

    @Deprecated
    @PostMapping("/saveOrUpdateActivityDef")
    public SimpleResponse saveOrUpdateActivityDef(@RequestBody ActivityDefV2Req activityDefV2Req) {
        return sellerActivityDefUV2Service.saveOrUpdateActivityDef(activityDefV2Req);
    }

    @Deprecated
    @GetMapping("/getDeposit")
    public SimpleResponse getDeposit(ActivityDefV2Req activityDefV2Req) {
        return sellerActivityDefQV2Service.getDeposit(activityDefV2Req);
    }

    @Deprecated
    @GetMapping("/listPageActivityDef")
    public SimpleResponse listPageActivityDef(@RequestParam("shopId") String shopId,
                                              @RequestParam(value = "type", required = false) String type,
                                              @RequestParam(value = "status", required = false) String status,
                                              @RequestParam(value = "current", defaultValue = "1") String current,
                                              @RequestParam(value = "size", defaultValue = "10") String size) {
        return sellerActivityDefQV2Service.listPageActivityDef(current, size, shopId, type, status);
    }

    @GetMapping("/createAble")
    public SimpleResponse createAble(ActivityDefV2Req activityDefV2Req) {
        SimpleResponse<Object> response = SellerValidator.checkParams4createAble(activityDefV2Req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        Long merchantId = Long.valueOf(activityDefV2Req.getMerchantId());
        Long shopId = Long.valueOf(activityDefV2Req.getShopId());
        Integer type = Integer.valueOf(activityDefV2Req.getType());
        Long goodsId = null;
        if (StringUtils.isNotBlank(activityDefV2Req.getGoodsId())) {
            String goodsIdStr = activityDefV2Req.getGoodsId();
            goodsId = Long.valueOf(goodsIdStr);
        }
        List<Long> goodsIdList = null;
        if (StringUtils.isNotBlank(activityDefV2Req.getGoodsIdList())) {
            String goodsIds = activityDefV2Req.getGoodsIdList();
            goodsIdList = new ArrayList<>();
            String[] split = goodsIds.split(",");
            for (String gids : split) {
                goodsIdList.add(Long.valueOf(gids));
            }
        }
        // 此时goodsId为productid，需要查询到goodsId
        if (type == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()
                || type == ActivityDefTypeEnum.FAV_CART_V2.getCode()
                || type == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()
                || type == ActivityDefTypeEnum.ASSIST_V2.getCode()
                || type == ActivityDefTypeEnum.ORDER_BACK_V2.getCode()) {
            QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(Goods::getMerchantId, activityDefV2Req.getMerchantId())
                    .eq(Goods::getShopId, activityDefV2Req.getShopId());
            if (goodsId != null) {
                queryWrapper.lambda().eq(Goods::getProductId, goodsId);
            }
            if (!CollectionUtils.isEmpty(goodsIdList)) {
                queryWrapper.lambda().in(Goods::getProductId, goodsIdList);
            }
            List<Goods> list = goodsService.list(queryWrapper);
            if (!CollectionUtils.isEmpty(list)) {
                if (goodsId != null) {
                    goodsId = list.get(0).getId();
                }
                if (!CollectionUtils.isEmpty(goodsIdList)) {
                    goodsIdList.clear();
                    for (Goods goods : list) {
                        goodsIdList.add(goods.getId());
                    }
                }
            }
        }
        return activityDefCheckService.createAble(merchantId, shopId, type, goodsId, goodsIdList, null);
    }

    @GetMapping("/queryActivityDef")
    public SimpleResponse queryActivityDef(@RequestParam("activityDefId") String activityDefId) {
        return sellerActivityDefQV2Service.queryActivityDef(activityDefId);
    }

    @PostMapping(value = "/configActivityDef")
    public SimpleResponse configActivityDef(@RequestBody OperationReq operationReq) {
        return sellerActivityDefUV2Service.configActivityDef(operationReq);
    }

    @PostMapping(value = "/supplyStock")
    public SimpleResponse supplyStock(@RequestBody SupplyStockReq supplyStockReq) {
        return sellerActivityDefUV2Service.supplyStock(supplyStockReq);
    }

    /***
     * 库存变更记录
     * @param current
     * @param size
     * @param activityDefId
     * @return
     */
    @GetMapping("/queryAuditRecord")
    public SimpleResponse queryAuditRecord(@RequestParam("current") String current,
                                           @RequestParam("size") String size,
                                           @RequestParam("activityDefId") String activityDefId) {
        SimpleResponse<Object> response = SellerValidator.checkShopPage(current, size, activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        return activityDefAuditService.listPageRecord4ActivityDefAudit(Long.valueOf(current), Long.valueOf(size), Long.valueOf(activityDefId));
    }

    @GetMapping("/queryGoodsInfo")
    public SimpleResponse queryGoodsInfo(@RequestParam("activityDefId") String activityDefId) {

        // 校验参数
        SimpleResponse<Object> response = SellerValidator.checkActivityDefId(activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        Long adid = Long.valueOf(activityDefId);

        // 获取活动信息
        ActivityDef activityDef = activityDefService.getById(adid);
        if (null == activityDef) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("errorMessage", "数据库未查到ActivityDef活动的信息，id = " + adid);
            response.setData(resultVO);
            return response;
        }
        String typeVO = SellerTransformParam.PO2VOActivityDefType(activityDef.getType());

        List<Long> goodsIdList = null;
        if (activityDef.getGoodsId() != null) {
            goodsIdList = new ArrayList<>();
            goodsIdList.add(activityDef.getGoodsId());
        }
        if (activityDef.getGoodsIds() != null) {
            goodsIdList = JSON.parseArray(activityDef.getGoodsIds(), Long.class);

        }
        // TODO 待修改查询商品信息
        if (goodsIdList != null) {
            Map<String, Object> goodsVOList = goodsService.getGoodsVOListByIds(goodsIdList);
            if (StringUtils.equals(SellerTransformParam.GIFT_TYPE, typeVO)
                    || StringUtils.equals(SellerTransformParam.FREE_GOODS_TYPE, typeVO)) {
                if (JsonParseUtil.booJsonArr(activityDef.getKeyWord())) {
                    goodsVOList.put("activityDefKeywords", JSON.parseArray(activityDef.getKeyWord()));
                }

            }
            response.setData(goodsVOList);
        }
        return response;
    }

    @GetMapping("/error")
    public SimpleResponse error() {
        // emailService.sendTextEmail("测试", "ceshi");
        int i = 1 / 0;
        return null;
    }
}
