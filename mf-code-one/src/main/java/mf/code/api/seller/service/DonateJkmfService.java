package mf.code.api.seller.service;

import mf.code.common.simpleresp.SimpleResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * mf.code.api.seller.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年09月23日 15:02
 */
public interface DonateJkmfService {

    /***
     * 购买st系列-全网通-赠送集客魔方使用
     *
     * @param phone
     * @param request
     * @return
     */
    SimpleResponse donateJkmf(String phone, HttpServletRequest request, HttpServletResponse response);
}
