package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.dto.DepositBO;
import mf.code.api.seller.service.SellerActivityDefQV2Service;
import mf.code.api.seller.utils.SellerTransformParam;
import mf.code.api.seller.utils.SellerVO;
import mf.code.api.seller.utils.SellerValidator;
import mf.code.statistics.api.service.SellerStatisticsService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.service.MerchantShopCouponService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-27 下午5:49
 */
@Slf4j
@Service
public class SellerActivityDefQV2ServiceImpl implements SellerActivityDefQV2Service {
    private final GoodsService goodsService;
    private final ActivityDefService activityDefService;
    private final ActivityDefAuditService activityDefAuditService;
    private final SellerStatisticsService sellerStatisticsService;
    private final ActivityTaskService activityTaskService;
    private final MerchantShopCouponService merchantShopCouponService;

    @Autowired
    public SellerActivityDefQV2ServiceImpl(GoodsService goodsService,
                                           ActivityDefService activityDefService,
                                           ActivityDefAuditService activityDefAuditService,
                                           SellerStatisticsService sellerStatisticsService,
                                           ActivityTaskService activityTaskService,
                                           MerchantShopCouponService merchantShopCouponService) {
        this.goodsService = goodsService;
        this.activityDefService = activityDefService;
        this.activityDefAuditService = activityDefAuditService;
        this.sellerStatisticsService = sellerStatisticsService;
        this.activityTaskService = activityTaskService;
        this.merchantShopCouponService = merchantShopCouponService;
    }

    @Override
    public SimpleResponse<Object> getDeposit(ActivityDefV2Req activityDefV2Req) {
        // 校验参数
        SimpleResponse<Object> response = SellerValidator.checkParams4getDeposit(activityDefV2Req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }

        String activityDefId = activityDefV2Req.getActivityDefId();

        int stockDef = Integer.parseInt(activityDefV2Req.getStockDef());
        BigDecimal totalAmount = null;
        BigDecimal commission = activityDefV2Req.getCommission();
        List<Long> goodsIdList;
        int type;
        String openRedpacketAmount = activityDefV2Req.getOpenRedpacketAmount();
        if (StringUtils.isNotBlank(activityDefId) && null == activityDefV2Req.getDepositDef()) {
            // 补库存 获取保证金额 （排除修改逻辑）
            Long adid = Long.valueOf(activityDefId);
            ActivityDef activityDef = activityDefService.getById(adid);
            type = activityDef.getType();
            goodsIdList = new ArrayList<>();
            if (activityDef.getGoodsId() != null) {
                goodsIdList.add(activityDef.getGoodsId());
            }
            String goodsIds = activityDef.getGoodsIds();
            if (JsonParseUtil.booJsonArr(goodsIds)) {
                goodsIdList.addAll(JSON.parseArray(goodsIds, Long.class));
            }
            if (type == ActivityDefTypeEnum.RED_PACKET.getCode()) {
                totalAmount = activityTaskService.getTotalAmount(adid);
            }
            if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                // 总金额就是总金额
                if (StringUtils.isNotBlank(openRedpacketAmount) && RegexUtils.StringIsNumber(openRedpacketAmount)) {
                    totalAmount = new BigDecimal(activityDefV2Req.getStockDef())
                            .multiply(new BigDecimal(openRedpacketAmount));
                } else {
                    totalAmount = new BigDecimal(activityDefV2Req.getStockDef())
                            .multiply(activityDef.getGoodsPrice());
                }

            }
            if (commission == null) {
                commission = activityDef.getCommission();
            }
        } else {
            type = Integer.parseInt(activityDefV2Req.getType());
            goodsIdList = new ArrayList<>();
            String goodsIds = activityDefV2Req.getGoodsIdList();
            if (StringUtils.isNotBlank(goodsIds)) {
                String[] split = goodsIds.split(",");
                for (String str : split) {
                    goodsIdList.add(Long.valueOf(str));
                }
            }
            if (StringUtils.isNotBlank(activityDefV2Req.getGoodsId())) {
                String goodsId = activityDefV2Req.getGoodsId();
                goodsIdList.add(Long.valueOf(goodsId));
            }
            if (type == ActivityDefTypeEnum.RED_PACKET.getCode()) {
                BigDecimal fillBackOrderAmount = new BigDecimal(activityDefV2Req.getFillBackOrderAmount());
                BigDecimal favCartAmount = new BigDecimal(activityDefV2Req.getFavCartAmount());
                BigDecimal goodCommentAmount = new BigDecimal(activityDefV2Req.getGoodCommentAmount());
                totalAmount = fillBackOrderAmount.add(favCartAmount).add(goodCommentAmount);
            }
            if (type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                // 总金额就是总金额
                totalAmount = new BigDecimal(activityDefV2Req.getStockDef())
                        .multiply(new BigDecimal(openRedpacketAmount));
            }
        }

        // 加工请求数据
        DepositBO depositBO = new DepositBO();
        depositBO.setGoodsIdList(goodsIdList);
        depositBO.setStockDef(stockDef);
        depositBO.setType(type);
        depositBO.setTotalAmount(totalAmount);
        depositBO.setCommission(commission);
        return goodsService.getDeposit(depositBO);
    }

    @Override
    public SimpleResponse listPageActivityDef(String current, String size, String shopId, String type, String status) {
        // 校验参数
        SimpleResponse<Object> response = SellerValidator.checkShopPage(current, size, shopId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        List<Integer> typeList = SellerTransformParam.VO2PO4ActivityDefType(type);
        if (null == typeList) {
            String error = "所传的type值 和 接口定义的type值 不匹配，to 木子";
            SellerTransformParam.sendErrorMessage(error);
        }
        List<Integer> statusList = SellerTransformParam.VO2PO4ActivityDefStatus(status);
        if (null == statusList) {
            String error = "所传的status值 和 接口定义的status值 不匹配，to 木子";
            SellerTransformParam.sendErrorMessage(error);
        }

        Long sid = Long.valueOf(shopId);
        Long crt = Long.valueOf(current);
        Long sz = Long.valueOf(size);

        // 加工查询条件
        Page<ActivityDef> adpage = new Page<>(crt, sz);
        adpage.setDesc("id");
        QueryWrapper<ActivityDef> adWrapper = new QueryWrapper<>();
        adWrapper.lambda()
                .eq(ActivityDef::getShopId, sid)
                .in(ActivityDef::getType, typeList)
                .in(ActivityDef::getStatus, statusList)
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        if (StringUtils.equals(SellerTransformParam.STOCK_WARN_STATUS, status)) {
            adWrapper.apply("stock - stock_def * 0.1 <= 0");
        }
        IPage<ActivityDef> activityDefPage = activityDefService.page(adpage, adWrapper);

        // 加工展示数据
        Map<String, Object> resultVO = SellerVO.init4listPageActivityDef();
        resultVO.put("total", activityDefPage.getTotal());
        resultVO.put("pages", activityDefPage.getPages());
        resultVO.put("size", activityDefPage.getSize());
        resultVO.put("current", activityDefPage.getCurrent());

        List<ActivityDef> activityDefs = activityDefPage.getRecords();
        if (CollectionUtils.isEmpty(activityDefs)) {
            response.setMessage("还没有任何活动，赶快去创建吧");
            return response;
        }

        List<Map<String, Object>> activityDefVOList = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            Map<String, Object> activityDefVO = new HashMap<>();
            String typeVO = SellerTransformParam.PO2VOActivityDefType(activityDef.getType());
            Long activityDefId = activityDef.getId();
            Long goodsId = activityDef.getGoodsId();

            activityDefVO.put("type", typeVO);
            activityDefVO.put("id", activityDefId);

            // --红包列表处理--begin--
            // if (StringUtils.equals(typeVO, SellerTransformParam.RED_PACKET_TYPE)) {
            //     Map<String, Object> activityTaskVO = listPageActivityTask(activityDefId);
            //     if (null != activityTaskVO) {
            //         activityDefVO.putAll(activityTaskVO);
            //     }
            //     continue;
            // }
            // --红包列表处理--end----

            if (StringUtils.equals(SellerTransformParam.GIFT_TYPE, typeVO)
                    || StringUtils.equals(SellerTransformParam.FREE_GOODS_TYPE, typeVO)) {
                activityDefVO.put("goodsId", goodsId);
                Goods goods = goodsService.getById(goodsId);
                if (null == goods) {
                    return goodsService.selectError(goodsId);
                }
                activityDefVO.put("goodsName", goods.getDisplayGoodsName());
            }
            String statusVO = SellerTransformParam.PO2VO4ActivityDefStatus(activityDef.getStatus());
            activityDefVO.put("status", statusVO);

            BigDecimal deposit = activityDef.getDeposit();
            Integer stockDef = activityDef.getStockDef();
            Integer stock = activityDef.getStock() == null ? 0 : activityDef.getStock();
            // ---红包 剩余库存 和 保证金 的处理 ----begin----
            if (StringUtils.equals(typeVO, SellerTransformParam.RED_PACKET_TYPE)) {
                QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
                wrapper.lambda()
                        .eq(ActivityTask::getActivityDefId, activityDefId)
                        .eq(ActivityTask::getDel, DelEnum.NO.getCode());
                List<ActivityTask> activityTasks = activityTaskService.list(wrapper);

                deposit = BigDecimal.ZERO;
                stockDef = 0;
                stock = 0;
                if(!CollectionUtils.isEmpty(activityTasks)){
                    for (ActivityTask activityTask : activityTasks) {
                        stockDef = stockDef + activityTask.getRpStockDef();
                        stock = stock + activityTask.getRpStock();
                        deposit = deposit.add(activityTask.getRpAmount().multiply(new BigDecimal(activityTask.getRpStock())));
                    }
                }
            }
            // ---红包 剩余库存 和 保证金 的处理 ----end------
            // ---- 赠品和免单商品 保证金 的处理 ----begin----
            if (StringUtils.equals(typeVO, SellerTransformParam.GIFT_TYPE)
                    || StringUtils.equals(typeVO, SellerTransformParam.FREE_GOODS_TYPE)) {
                if(activityDef.getGoodsPrice().compareTo(BigDecimal.ZERO) > 0){
                    // 赠品和免单抽奖的保证金金额需要加上佣金金额
                    deposit = (activityDef.getGoodsPrice().add(activityDef.getCommission())).multiply(new BigDecimal(stock));
                }
            }
            // ---- 赠品和免单商品 保证金 的处理 ----end------
            activityDefVO.put("deposit", deposit);
            activityDefVO.put("stock", stock);

            activityDefVO.put("userJoinCount", 0);
            response = sellerStatisticsService.queryActivityDefUserJoin("1", "10", activityDefId.toString(), false, null);
            if (response != null && response.getCode() == ApiStatusEnum.SUCCESS.getCode()) {
                Map data = (Map) response.getData();
                activityDefVO.put("userJoinCount", data.get("userJoinCount"));
            }
            activityDefVO.put("supplyCount", 0);
            response = activityDefAuditService.listPageRecord4ActivityDefAudit(1L, 10L, activityDefId);
            if (response != null || response.getCode() == ApiStatusEnum.SUCCESS.getCode()) {
                Map data = (Map) response.getData();
                activityDefVO.put("supplyCount", data.get("supplyCount") == null ? 0 : data.get("supplyCount"));
            }
            if (stockDef * 0.1 >= stock && StringUtils.equals(statusVO, SellerTransformParam.PUBLISHED_STATUS) && !statusVO.equals(ActivityDefStatusEnum.REFUNDED.getCode())) {
                activityDefVO.put("status", SellerTransformParam.STOCK_WARN_STATUS);
            }

            // ----占用库存数----begin----
            int occupyStock = sellerStatisticsService.getOccupyStock(activityDefId, typeVO, null);
            activityDefVO.put("occupyStock", occupyStock);
            // ----占用库存数----end------
            activityDefVOList.add(activityDefVO);
        }
        resultVO.put("activityDefVOList", activityDefVOList);

        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse queryActivityDef(String activityDefId) {
        // 校验参数
        SimpleResponse<Object> response = SellerValidator.checkActivityDefId(activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        Long adid = Long.valueOf(activityDefId);

        // 获取活动信息
        ActivityDef activityDef = activityDefService.getById(adid);
        if (null == activityDef) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("errorMessage", "数据库未查到ActivityDef活动的信息，id = " + adid);
            response.setData(resultVO);
            return response;
        }

        // 加工展现参数
        Map<String, Object> resultVO = new HashMap<>();
        Integer type = activityDef.getType();
        resultVO.put("activityDefId", activityDef.getId());
        resultVO.put("type", type);
        resultVO.put("shopId", activityDef.getShopId());

        Integer condPersionCount = activityDef.getCondPersionCount();
        resultVO.put("condPersionCount", condPersionCount);
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == type) {
            resultVO.put("condPersionCount", condPersionCount - 1);
        }
        resultVO.put("hitsPerDraw", activityDef.getHitsPerDraw());
        if (activityDef.getCondDrawTime() != null) {
            resultVO.put("condDrawTime", activityDef.getCondDrawTime());
        }
        resultVO.put("missionNeedTime", activityDef.getMissionNeedTime());
        if (activityDef.getKeyWord() != null) {
            List<String> keyWrodList = JSON.parseArray(activityDef.getKeyWord(), String.class);
            resultVO.put("keyWord", keyWrodList);
        }
        // 20190119 更新客服微信号和微信图片
        if(StringUtils.isNotBlank(activityDef.getServiceWx())){
            if(JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(csMapList)) {
                    //微信号
                    resultVO.put("serviceWx", csMapList.get(0).get("wechat"));
                    //微信二维码
                    resultVO.put("serviceWxPic", csMapList.get(0).get("pic"));
                }
            }else {
                resultVO.put("serviceWx", activityDef.getServiceWx());
                resultVO.put("serviceWxPic", "");
            }
        }
        resultVO.put("weighting", activityDef.getWeighting());
        resultVO.put("stockDef", activityDef.getStockDef());
        resultVO.put("depositDef", activityDef.getDepositDef());
        resultVO.put("commission", activityDef.getCommission());
        if (activityDef.getMerchantCouponJson() != null) {
            List<Long> merchantCouponIdList = JSON.parseArray(activityDef.getMerchantCouponJson(), Long.class);
            Map<String, Object> merchantShopCouponVOList = merchantShopCouponService.getMerchantShopCouponVOListByIds(merchantCouponIdList);
            resultVO.putAll(merchantShopCouponVOList);
        }
        List<Long> goodsIdList = null;
        if (activityDef.getGoodsId() != null) {
            goodsIdList = new ArrayList<>();
            goodsIdList.add(activityDef.getGoodsId());
        }
        if (activityDef.getGoodsIds() != null) {
            goodsIdList = JSON.parseArray(activityDef.getGoodsIds(), Long.class);

        }
        if (goodsIdList != null) {
            Map<String, Object> goodsVOList = goodsService.getGoodsVOListByIds(goodsIdList);
            resultVO.putAll(goodsVOList);
        }
        // 红包任务返回 三个红包任务（回填订单，好评返现，收藏加购）的金额
        if (type.equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            Map<String, Object> rpAmountResult = activityTaskService.queryRpAmountByActivityDefId(adid);
            resultVO.putAll(rpAmountResult);
        }
        // 拆红包活动 返回单个红包金额。从goods_price取用
        if (type.equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
            resultVO.put("openRedpacketAmount", activityDef.getGoodsPrice());
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }
}
