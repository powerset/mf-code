package mf.code.api.seller.service.impl;/**
 * create by qc on 2019/2/19 0019
 */

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.platform.dto.DtoProxyServer;
import mf.code.api.seller.dto.DtoRecallReq;
import mf.code.api.seller.dto.DtoRecallTalkReq;
import mf.code.api.seller.dto.ProxyServerInfoResp;
import mf.code.api.seller.enums.SceneTypeEnum;
import mf.code.api.seller.service.RecallV3Service;
import mf.code.common.caller.aliyundayu.DayuCaller;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.shorturl.ShortUrlTool;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.dao.MerchantProxyServerMapper;
import mf.code.merchant.repo.dao.MerchantShopMapper;
import mf.code.merchant.repo.enums.MerchantProxyServerTypeEnum;
import mf.code.merchant.repo.po.MerchantProxyServer;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.recall.repo.dao.RecallMsgMapper;
import mf.code.recall.repo.dao.RecallTalkMapper;
import mf.code.recall.repo.po.RecallMsg;
import mf.code.recall.repo.po.RecallTalk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author gbf
 */
@Slf4j
@Service
public class RecallV3ServiceImpl implements RecallV3Service {

    private final RecallTalkMapper recallTalkMapper;
    private final RecallMsgMapper recallMsgMapper;
    private final CommonDictService commonDictService;
    private final ActivityDefService activityDefService;
    private final MerchantProxyServerMapper merchantProxyServerMapper;
    private final GoodsService goodsService;
    private final DayuCaller dayuCaller;
    private final MerchantShopMapper merchantShopMapper;

    @Autowired
    public RecallV3ServiceImpl(RecallTalkMapper recallTalkMapper, RecallMsgMapper recallMsgMapper, CommonDictService commonDictService, ActivityDefService activityDefService, MerchantProxyServerMapper merchantProxyServerMapper, GoodsService goodsService, DayuCaller dayuCaller, MerchantShopMapper merchantShopMapper) {
        this.recallTalkMapper = recallTalkMapper;
        this.recallMsgMapper = recallMsgMapper;
        this.commonDictService = commonDictService;
        this.activityDefService = activityDefService;
        this.merchantProxyServerMapper = merchantProxyServerMapper;
        this.goodsService = goodsService;
        this.dayuCaller = dayuCaller;
        this.merchantShopMapper = merchantShopMapper;
    }

    /**
     * 创建话术
     *
     * @param dto ..
     * @return .
     */
    @Override
    public SimpleResponse createTalk(DtoRecallTalkReq dto) {
        RecallTalk recallTalk = new RecallTalk();
        recallTalk.setText(dto.getText());
        recallTalk.setShopId(dto.getShopId());
        recallTalk.setCtime(new Date());
        recallTalk.setUtime(new Date());
        recallTalk.setDel(0);
        recallTalk.setVersion(3L);

        SimpleResponse simpleResponse = new SimpleResponse();
        int result = recallTalkMapper.insertSelective(recallTalk);
        if (result == 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.SUCCESS);
        } else {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR);
        }
        return simpleResponse;
    }

    /**
     * 查找话术
     *
     * @param shopId .
     * @return .
     */
    @Override
    public SimpleResponse findTalk(Long shopId) {
        List<RecallTalk> list = recallTalkMapper.findAll(shopId);
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(list);
        return response;
    }

    @Override
    public SimpleResponse updateTalk(DtoRecallTalkReq dto) {
        RecallTalk recallTalk = new RecallTalk();
        recallTalk.setId(dto.getId());
        recallTalk.setShopId(dto.getShopId());
        recallTalk.setText(dto.getText());
        recallTalk.setUtime(new Date());
        int result = recallTalkMapper.updateByPrimaryKeySelective(recallTalk);
        SimpleResponse response;
        if (result == 1) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }

    @Override
    public SimpleResponse delTalk(DtoRecallTalkReq dto) {
        RecallTalk recallTalk = new RecallTalk();
        recallTalk.setId(dto.getId());
        recallTalk.setDel(1);
        recallTalk.setUtime(new Date());
        int result = recallTalkMapper.updateByPrimaryKeySelective(recallTalk);
        SimpleResponse response;
        if (result == 1) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }

    /**
     * 创建信息
     *
     * @param dto ..
     * @return ..
     */
    @Override
    public SimpleResponse createRecall(DtoRecallReq dto, HttpServletRequest request) {
        RecallMsg msg = new RecallMsg();
        msg.setShopId(dto.getShopId());
        msg.setText(dto.getText());
        msg.setType(dto.getQrType());
        msg.setCategory(dto.getCategory());
        msg.setActivityType(dto.getActivityType());
        msg.setGoodsId(dto.getGoodsId());
        msg.setUrlQr(dto.getUrlQr());
        msg.setUrlShort(dto.getUrlShort());
        msg.setUrlPoster(dto.getUrlPoster());
        msg.setActivityId(dto.getActivityId());
        msg.setDel(0);
        msg.setCtime(new Date());
        msg.setUtime(new Date());

        Map m = new HashMap(5);
        m.put("shopId", msg.getShopId());
        m.put("type", msg.getType());
        m.put("goodsId", msg.getGoodsId());
        m.put("activityId", msg.getActivityId());
        m.put("activityType", msg.getActivityType());
        m.put("category", msg.getCategory());

        RecallMsg recallMsg = null;
        // 4:自己上传的二维码
        if (msg.getType() != 4) {
            recallMsg = recallMsgMapper.selectByShopIdGoodsIdActivityId(m);
        }
        if (recallMsg == null) {
            int result = recallMsgMapper.insertSelective(msg);
            //获取id
            Long id = msg.getId();
            String root = request.getRequestURL().toString().split("/api")[0];
            String requestURL = root + "/api/seller/v3/recall/showPoster?id=" + id;
            //获取短链接
            String urlShort = ShortUrlTool.born(requestURL);
            SimpleResponse response;
            if (result == 1) {
                msg = new RecallMsg();
                msg.setId(id);
                msg.setUrlShort(urlShort);
                //更新短链接
                recallMsgMapper.updateByPrimaryKeySelective(msg);
                response = new SimpleResponse(ApiStatusEnum.SUCCESS);
                Map mm = new HashMap<>(1);
                mm.put("urlShort", urlShort);
                mm.put("id", msg.getId());
                response.setData(mm);
            } else {
                response = new SimpleResponse(ApiStatusEnum.ERROR);
            }
            return response;
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6);
        }
    }

    /**
     * 查询
     *
     * @param shopId ..
     * @return ..
     */
    @Override
    public SimpleResponse findRecalMsg(Long shopId, int pageSize, int pageCurrent) {
        //分页插件 -- 原型设计中并没有分页, 这里只是防备以后
        Page<Object> page = PageHelper.startPage(pageCurrent, pageSize);
        List<RecallMsg> list = recallMsgMapper.findAll(shopId);
        PageInfo info = page.toPageInfo();
        List<Map> resultList = new ArrayList<>();
        for (RecallMsg msg : list) {
            Map m = new HashMap(6);
            m.put("ctime", DateUtil.dateToString(msg.getCtime(), DateUtil.LONG_DATE_FORMAT));
            m.put("id", msg.getId());
            m.put("text", msg.getText());
            m.put("addrQr", msg.getUrlShort());
            m.put("timeScan", msg.getScanTime());
            m.put("timeClick", msg.getClickTime());
            resultList.add(m);
        }
        info.setList(resultList);
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        //如果要分页,就将info设置进去
        response.setData(info);
        return response;
    }

    /**
     * 修改信息
     *
     * @param dto ..
     * @return ..
     */
    @Override
    public SimpleResponse updateRecall(DtoRecallReq dto) {
        RecallMsg msg = new RecallMsg();
        msg.setId(dto.getId());
        msg.setText(dto.getText());
        msg.setType(dto.getQrType());
        msg.setActivityType(dto.getActivityType());
        msg.setGoodsId(dto.getGoodsId());
        msg.setUrlQr(dto.getUrlQr());
        msg.setShopId(dto.getShopId());
        msg.setUrlShort(dto.getUrlShort());
        msg.setUrlPoster(dto.getUrlPoster());
        msg.setUtime(new Date());
        int result = recallMsgMapper.updateByPrimaryKeySelective(msg);
        Map m = new HashMap(2);
        m.put("urlShort", msg.getUrlShort());
        m.put("id", msg.getId());
        SimpleResponse response;
        if (result == 1) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
            response.setData(m);
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }

    /**
     * 删除信息
     *
     * @param dto ..
     * @return ..
     */
    @Override
    public SimpleResponse delRecall(DtoRecallReq dto) {
        RecallMsg msg = new RecallMsg();
        msg.setId(dto.getRecallId());
        msg.setDel(1);
        msg.setUtime(new Date());
        int result = recallMsgMapper.updateByPrimaryKeySelective(msg);
        SimpleResponse response;
        if (result == 1) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }

    /**
     * 复制
     *
     * @param id ..
     * @return ..
     */
    @Override
    public SimpleResponse copyRecall(Long id) {
        RecallMsg recallMsg = recallMsgMapper.selectByPrimaryKey(id);
        int result = recallMsgMapper.insertSelective(recallMsg);
        SimpleResponse response;
        if (result == 1) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }

    /**
     * 短链接的跳转地址 进入后将累加点击次数
     *
     * @param id ..
     * @return .
     */
    @Override
    public RecallMsg showPoster(Long id) {
        // 查询真是地址
        RecallMsg msg = recallMsgMapper.selectByPrimaryKey(id);
        //增加点击次数
        recallMsgMapper.addClickTime(id);
        return msg;
    }

    /**
     * yc支付金额
     *
     * @param merchantId
     * @param type
     * @return
     */
    @Override
    public SimpleResponse queryProxyServerInfo(Long merchantId, int type) {
        SimpleResponse simpleResponse = new SimpleResponse();
        ProxyServerInfoResp proxyServerInfoResp = new ProxyServerInfoResp();
        proxyServerInfoResp.setProxyServerInfos(new ArrayList<>());
        if (MerchantProxyServerTypeEnum.FENDUODUO.getCode() == type) {
            CommonDict commonDict = this.commonDictService.selectByTypeKey("fenduoduo", "pay_menu");
            List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
            if (!CollectionUtils.isEmpty(objects)) {
                for (Object obj : objects) {
                    //[{"trade_type":1,"money":"6800.00","name":"购买"},{"trade_type":3,"money":"3400.00","name":"续费"}]
                    JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                    Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                    ProxyServerInfoResp.ProxyServerInfo proxyServerInfo = new ProxyServerInfoResp.ProxyServerInfo();
                    proxyServerInfo.from(jsonMap);
                    proxyServerInfoResp.getProxyServerInfos().add(proxyServerInfo);
                }
            }
        }
        simpleResponse.setData(proxyServerInfoResp);
        return simpleResponse;
    }

    /**
     * 根据条件查询goods
     *
     * @param shopId       .
     * @param activityType .
     * @return .
     */
    @Override
    public SimpleResponse findGoods(Long shopId, int activityType) {
        List<Goods> list;
        List<Integer> typeList = new ArrayList<>();
        if (activityType == SceneTypeEnum.FILLORDER.getCode()
                || activityType == SceneTypeEnum.GOODCOMMENT.getCode()
                || activityType == SceneTypeEnum.FAVCART.getCode()) {
            //在task表
            if (activityType == SceneTypeEnum.FAVCART.getCode()) {
                activityType = 3;
            }
            list = activityDefService.selectGoodsByTask(shopId, activityType);
        } else {
            //在def表
            if (activityType == SceneTypeEnum.ASSIST.getCode()) {
                //助力
                typeList.add(6);
                typeList.add(7);
                typeList.add(13);
            } else if (activityType == SceneTypeEnum.LUCKYWHEEL.getCode()) {
                //轮盘
                typeList.add(4);
            } else if (activityType == SceneTypeEnum.ORDERBACK.getCode()) {
                // 抽奖
                typeList.add(12);
            } else if (activityType == SceneTypeEnum.OPENREDPACK.getCode()) {
                // 拆红包
                typeList.add(9);
            } else if (activityType == SceneTypeEnum.FULL_REIMBURSEMENT.getCode()) {
                typeList.add(10);
            }
            log.info("<<<<<<<<<<<< 前端入参={},sql查询对应type={}", activityType, typeList);
            list = activityDefService.selectGoodsByDef(shopId, typeList);
        }
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(list);
        return response;
    }

    @Override
    public SimpleResponse getInfo(Long id) {
        RecallMsg recallMsg = recallMsgMapper.selectByPrimaryKey(id);
        Long goodsId = recallMsg.getGoodsId();
        Goods goods = goodsService.selectById(goodsId);
        String goodsName = "";
        if (goods != null) {
            goodsName = goods.getDisplayGoodsName();
        }
        Map m = new HashMap(14);
        m.put("displayGoodsName", goodsName);
        m.put("goodsId", goodsId);
        m.put("id", recallMsg.getId());
        m.put("merchantId", recallMsg.getMerchantId());
        m.put("shopId", recallMsg.getShopId());
        m.put("text", recallMsg.getText());
        m.put("type", recallMsg.getType());
        m.put("activityType", recallMsg.getActivityType());
        m.put("activityId", recallMsg.getActivityId());
        m.put("urlQr", recallMsg.getUrlQr());
        m.put("urlShort", recallMsg.getUrlShort());
        m.put("urlPoster", recallMsg.getUrlPoster());
        m.put("clickTime", recallMsg.getClickTime());
        m.put("scanTime", recallMsg.getScanTime());
        m.put("ctime", recallMsg.getCtime());
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(m);
        return response;
    }

    /**
     * 粉多多的购买列表
     *
     * @param merchantId ..
     * @return ..
     */
    @Override
    public SimpleResponse getPayList(Long merchantId) {
        List<MerchantProxyServer> list = merchantProxyServerMapper.getPayList(merchantId);
        List<Map> resultList = new ArrayList<>();
        for (MerchantProxyServer e : list) {
            Map m = new HashMap(3);
            Date expireTime = e.getExpireTime();
            Date now = new Date();
            long result = DateUtil.secondDiff(now, expireTime);
            if (result > 0) {
                m.put("stat", 1);
            } else {
                m.put("stat", 0);
            }
            m.put("buyTime", DateUtil.dateToString(e.getBuyTime(), DateUtil.POINT_DATE_FORMAT));
            m.put("expireTime", DateUtil.dateToString(e.getExpireTime(), DateUtil.POINT_DATE_FORMAT));
            resultList.add(m);
        }
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(resultList);
        return response;
    }

    /**
     * 粉多多是否支付和有效期
     *
     * @param merchantId .
     * @return ..
     */
    @Override
    public SimpleResponse activeInfo(Long merchantId) {
        List<MerchantProxyServer> list = merchantProxyServerMapper.getPayList(merchantId);
        MerchantProxyServer info = null;
        if (list.size() > 0) {
            info = list.get(list.size() - 1);
        }
        Map resultMap = new HashMap(3);
        if (info != null) {
            Date expireTime = info.getExpireTime();
            Date now = new Date();
            long result = DateUtil.secondDiff(now, expireTime);
            if (result > 0) {
                resultMap.put("stat", 1);
            } else {
                resultMap.put("stat", 0);
            }
            resultMap.put("buyTime", DateUtil.dateToString(info.getBuyTime(), DateUtil.POINT_DATE_FORMAT));
            resultMap.put("expireTime", DateUtil.dateToString(info.getExpireTime(), DateUtil.POINT_DATE_FORMAT));
        }
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(resultMap);
        return response;
    }

    @Override
    public SimpleResponse getPackageQrList(Long shopId, int pageCurrent, int pageSize) {

        Page<Object> page = PageHelper.startPage(pageCurrent, pageSize);
        List<RecallMsg> list = recallMsgMapper.getPackageQrList(shopId);
        PageInfo info = page.toPageInfo();
        List<Map> resultList = new ArrayList<>();
        for (RecallMsg msg : list) {
            Map m = new HashMap(6);
            // 时间格式
            m.put("ctime", DateUtil.dateToString(msg.getCtime(), DateUtil.LONG_DATE_FORMAT));
            m.put("id", msg.getId());
            m.put("text", msg.getText());
            m.put("addrQr", msg.getUrlQr());
            m.put("addrShort", msg.getUrlShort());
            m.put("addrPoster", msg.getUrlPoster());
            m.put("timeScan", msg.getScanTime());
            m.put("timeClick", msg.getClickTime());
            m.put("category", msg.getCategory());
            m.put("type", msg.getType());
            resultList.add(m);
        }
        info.setList(resultList);
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        //如果要分页,就将info设置进去
        response.setData(info);
        return response;
    }

    @Override
    public SimpleResponse getFddList(int role, int pageCurrent, int pageSize) {
        Map m = new HashMap(1);
        m.put("role", role);
        Page<Object> page = PageHelper.startPage(pageCurrent, pageSize);
        List<Map> list = recallMsgMapper.getFddList(m);
        for (Map data : list) {
            data.put("pay_type", 2);
            if (data.get("activate_time") == null || "0000-00-00 00:00:00".equals(data.get("activate_time").toString())) {
                data.put("stat", 0);
            } else {
                data.put("stat", 1);
            }
        }

        //累计成交
        Integer total = recallMsgMapper.getTotal();
        // 今日累计
        Map paramTotalToday = new HashMap(2);
        Date[] dates = DateUtil.getBeginAndEnd(new Date());
        paramTotalToday.put("begin", dates[0]);
        paramTotalToday.put("end", dates[1]);
        Integer totalToday = recallMsgMapper.getTotalToday(paramTotalToday);
        // 待转账
        Integer totalTodo = recallMsgMapper.getTotalTodo();

        Map resultMap = new HashMap(4);
        PageInfo info = page.toPageInfo();
        info.setList(list);
        resultMap.put("list", info);
        resultMap.put("total", total);
        resultMap.put("totalToday", totalToday);
        resultMap.put("totalTodo", totalTodo);
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(resultMap);
        return response;
    }

    @Override
    public int incrScanTime(String recallScene) {
        Map m = new HashMap(3);
        if (recallScene != null && !"".equals(recallScene)) {
            String[] split = recallScene.split(",");
            m.put("merchantId", split[0]);
            m.put("shopId", split[1]);
            Integer scene = Integer.parseInt(split[2]);
            if (scene == 5 || scene == 7 || scene == 11) {
                m.put("goodsId", split[3]);
            }
            if (scene == 10 || scene == 9) {
                m.put("activityId", split[3]);
            }
        }

        return recallMsgMapper.incrScanTime(m);
    }

    @Override
    public Long insertQrcodeUrlAndGetRowId(String url) {
        RecallMsg recallMsg = new RecallMsg();
        recallMsg.setUrlQr(url);
        int result = recallMsgMapper.insertSelective(recallMsg);
        if (result > 0) {
            return recallMsg.getId();
        }
        return null;
    }

    /**
     * 校验身份信息,角色信息
     *
     * @param role
     * @param userId
     * @return
     */
    @Override
    public Boolean verifyRole(int role, Long userId) {
        // 角色: 平台
        if (role == 1) {

        } else if (role == 2) {
            // 角色: 粉多多

        }
        return true;
    }

    /**
     * 激活账户
     *
     * @param dto
     * @return
     */
    @Override
    public SimpleResponse active(DtoProxyServer dto) {
        MerchantProxyServer e = new MerchantProxyServer();
        e.setId(dto.getId());
        e.setUtime(new Date());
        e.setActivateTime(new Date());
        int result = merchantProxyServerMapper.updateByPrimaryKeySelective(e);
        SimpleResponse response;
        if (result > 0) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
            this.sendSms(e.getId());
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }

    /**
     * 发送短信
     *
     * @param id
     */
    private void sendSms(Long id) {
        MerchantProxyServer merchantProxyServer = merchantProxyServerMapper.selectByPrimaryKey(id);
        if (merchantProxyServer != null) {
            Long merchantId = merchantProxyServer.getMerchantId();
            MerchantShop merchantShop = merchantShopMapper.selectByPrimaryKey(merchantId);
            if (merchantShop != null) {
                log.error("商户id/商户phone:" + merchantId + "/" + merchantShop.getPhone());
                //发送短信
                dayuCaller.smsFanMuchActivate(merchantShop.getPhone());
            }
        }
    }

    /**
     * 添加附加信息
     *
     * @param dto
     * @return
     */
    @Override
    public SimpleResponse addInfo(DtoProxyServer dto) {
        MerchantProxyServer server = merchantProxyServerMapper.selectByPrimaryKey(dto.getId());
        MerchantProxyServer e = new MerchantProxyServer();
        e.setId(dto.getId());
        e.setPayFee(server.getFee().divide(new BigDecimal(2), 2, BigDecimal.ROUND_HALF_UP));
        e.setUtime(new Date());
        e.setPayBillNo(dto.getPayBillNo());

        int result = merchantProxyServerMapper.updateByPrimaryKeySelective(e);
        SimpleResponse response;
        if (result > 0) {
            response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        } else {
            response = new SimpleResponse(ApiStatusEnum.ERROR);
        }
        return response;
    }
}
