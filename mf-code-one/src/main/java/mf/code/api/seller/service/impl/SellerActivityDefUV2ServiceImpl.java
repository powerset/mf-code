package mf.code.api.seller.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.seller.dto.ActivityDefV2Req;
import mf.code.api.seller.dto.ActivityTaskBO;
import mf.code.api.seller.dto.OperationReq;
import mf.code.api.seller.dto.SupplyStockReq;
import mf.code.api.seller.service.SellerActivityDefQV2Service;
import mf.code.api.seller.service.SellerActivityDefUV2Service;
import mf.code.api.seller.utils.SellerTransformParam;
import mf.code.api.seller.utils.SellerValidator;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-26 下午9:28
 */
@Slf4j
@Service
public class SellerActivityDefUV2ServiceImpl implements SellerActivityDefUV2Service {
    private static final String SHOPQRCODEPATH = "activity/cspic/";
    private final StringRedisTemplate stringRedisTemplate;
    private final ActivityDefService activityDefService;
    private final GoodsService goodsService;
    private final SellerActivityDefQV2Service sellerActivityDefQV2Service;
    private final ActivityDefAuditService activityDefAuditService;
    private final ActivityTaskService activityTaskService;
    private final OssCaller ossCaller;
    private final ActivityDefCheckService activityDefCheckService;

    @Autowired
    public SellerActivityDefUV2ServiceImpl(StringRedisTemplate stringRedisTemplate,
                                           ActivityDefService activityDefService,
                                           GoodsService goodsService,
                                           SellerActivityDefQV2Service sellerActivityDefQV2Service,
                                           ActivityDefAuditService activityDefAuditService,
                                           ActivityTaskService activityTaskService,
                                           OssCaller ossCaller,
                                           ActivityDefCheckService activityDefCheckService) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.activityDefService = activityDefService;
        this.goodsService = goodsService;
        this.sellerActivityDefQV2Service = sellerActivityDefQV2Service;
        this.activityDefAuditService = activityDefAuditService;
        this.activityTaskService = activityTaskService;
        this.ossCaller = ossCaller;
        this.activityDefCheckService = activityDefCheckService;
    }

    @Override
    public SimpleResponse saveOrUpdateActivityDef(ActivityDefV2Req activityDefV2Req) {
        // 必填参数校验
        SimpleResponse<Object> response = SellerValidator.checkCreateParams(activityDefV2Req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }

        //参数转化
        Long adid = null;
        String activityDefId = activityDefV2Req.getActivityDefId();
        if (StringUtils.isNotBlank(activityDefId) && RegexUtils.StringIsNumber(activityDefId)) {
            adid = Long.valueOf(activityDefId);
        }

        Long merchantId = Long.valueOf(activityDefV2Req.getMerchantId());
        Long shopId = Long.valueOf(activityDefV2Req.getShopId());
        Integer type = Integer.valueOf(activityDefV2Req.getType());
        Long goodsId = null;
        String goodsIdStr = activityDefV2Req.getGoodsId();
        if (StringUtils.isNotBlank(goodsIdStr)) {
            goodsId = Long.valueOf(goodsIdStr);
        }
        List<Long> goodsIdList = null;
        String goodsIds = activityDefV2Req.getGoodsIdList();
        if (StringUtils.isNotBlank(goodsIds)) {
            goodsIdList = new ArrayList<>();
            String[] split = goodsIds.split(",");
            for (String str : split) {
                goodsIdList.add(Long.valueOf(str));
            }
        }

        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        if (!success) {
            return RedisForbidRepeat.NXError();
        }
        // 如果拆红包活动需要额外处理
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == type) {
            activityDefV2Req.setCondPersionCount("" + (Integer.valueOf(activityDefV2Req.getCondPersionCount()) + 1));
            activityDefV2Req.setHitsPerDraw("1");
            activityDefV2Req.setGoodsPrice(new BigDecimal(activityDefV2Req.getOpenRedpacketAmount()));
            // 佣金金额
            activityDefV2Req.setCommission(new BigDecimal(activityDefV2Req.getOpenRedpacketAmount()));
        }

        // 对发布中的活动进行修改
        boolean published = activityDefCheckService.checkPublished(activityDefV2Req);
        if (published) {
            return this.updatePublishedActivityDef(activityDefV2Req);
        }

        // 对活动进行修改
        if (null != adid) {
            return this.updateActivityDefFromActivityDefV2Req(activityDefV2Req);
        }

        // 校验是否有创建活动的资格
        response = activityDefCheckService.createAble(merchantId, shopId, type, goodsId, goodsIdList, adid);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        // 加工冗余字段
        // 获取定义保证金
        activityDefV2Req = setDeposit2ActivityDefV2Req(activityDefV2Req);
        if (null == activityDefV2Req) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("获取保证金额失败");
            return response;
        }

        /*
         * 拆红包人数，需要程序加一 例：帮拆数为5，则单个红包需要被5名好友帮拆才能被成功领取。发起者自身也拆红包1次，但不记做帮拆人数内
         * 添加默认值，添加单个红包金额到goods_price
         */
        if (ActivityDefTypeEnum.OPEN_RED_PACKET.getCode() == type) {
            activityDefV2Req.setCondPersionCount("" + (Integer.valueOf(activityDefV2Req.getCondPersionCount()) + 1));
            activityDefV2Req.setHitsPerDraw("1");
            activityDefV2Req.setGoodsPrice(new BigDecimal(activityDefV2Req.getOpenRedpacketAmount()));
            // 佣金金额
            activityDefV2Req.setCommission(new BigDecimal(activityDefV2Req.getOpenRedpacketAmount()));
        }
        // 创建 或 更新 活动定义
        ActivityDef activityDef = activityDefCheckService.saveOrUpdateActivityDefFromActivityDefV2Req(activityDefV2Req);
        if (null == activityDef) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            Map<String, String> resultVO = new HashMap<>();
            resultVO.put("errorMessage", "创建活动定义出错，活动未创建，to 百川");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            response.setData(resultVO);
            return response;
        }
        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return activityDefAuditService.saveError();
        }

        // 如果创建的是 红包活动，则记录到 activity_task表中
        if (activityDef.getType().equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            ActivityTaskBO activityTaskBO = new ActivityTaskBO();
            activityTaskBO.setActivityDef(activityDef);
            activityTaskBO.setFillbackOrderAmount(new BigDecimal(activityDefV2Req.getFillBackOrderAmount()));
            activityTaskBO.setFavCartAmount(new BigDecimal(activityDefV2Req.getFavCartAmount()));
            activityTaskBO.setGoodCommentAmout(new BigDecimal(activityDefV2Req.getGoodCommentAmount()));
            activityTaskBO.setGoodCommentMissionNeedTime(Integer.valueOf(activityDefV2Req.getGoodCommentMissionNeedTime()));
            activityTaskBO.setFavCartMissionNeedTime(Integer.valueOf(activityDefV2Req.getFavCartMissionNeedTime()));
            success = activityTaskService.saveOrUpdateFromActivityDef(activityTaskBO);
            if (!success) {
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
                return activityTaskService.saveError();
            }
        }

        // 处理返回数据
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityDefId", activityDef.getId());
        resultVO.put("merchantId", activityDef.getMerchantId());
        resultVO.put("shopId", activityDef.getShopId());
        resultVO.put("depositDef", activityDefV2Req.getDepositDef());

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;

    }

    @Override
    public SimpleResponse configActivityDef(OperationReq operationReq) {
        // 必填参数校验
        SimpleResponse<Object> response = SellerValidator.checkParams4ConfigActivityDef(operationReq);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转化
        Long activityDefId = Long.valueOf(operationReq.getActivityDefId());
        String operation = operationReq.getOperation();

        if (StringUtils.equals(SellerTransformParam.DEL, operation)) {
            // 校验是否有删除的资格， 只有在保存的状态 才有删除资格
            return activityDefCheckService.removeActivityDefAtSaveStatus(activityDefId);
        }
        Integer status = SellerTransformParam.getActivityDefStatusByOperation(operation);
        if (null == status) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            response.setMessage("活动操作operation 未设置正确，请参考 Yapi 字段说明，to 木子");
            return response;
        }
        return activityDefCheckService.suspendOrResumeActivityDef(activityDefId, status);
    }

    @Override
    public SimpleResponse supplyStock(SupplyStockReq supplyStockReq) {
        // 校验必填参数
        SimpleResponse<Object> response = SellerValidator.checkParams4SupplyStock(supplyStockReq);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }

        Long merchantId = Long.valueOf(supplyStockReq.getMerchantId());

        // redis防重拦截
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        if (!success) {
            return RedisForbidRepeat.NXError();
        }

        // 计算保证金
        ActivityDefV2Req req = new ActivityDefV2Req();
        req.setActivityDefId(supplyStockReq.getActivityDefId());
        req.setStockDef(supplyStockReq.getStockDef());
        response = sellerActivityDefQV2Service.getDeposit(req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }

        Map data = (Map) response.getData();
        supplyStockReq.setDepositDef(data.get("depositDef").toString());
        // 补库存记录入库
        ActivityDefAudit activityDefAudit = activityDefAuditService.supplyStock(supplyStockReq);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return activityDefAuditService.saveError();
        }

        // 处理返回数据
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityDefAuditId", activityDefAudit.getId());
        resultVO.put("merchantId", activityDefAudit.getMerchantId());
        resultVO.put("shopId", activityDefAudit.getShopId());
        resultVO.put("depositDef", activityDefAudit.getDepositApply());

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    /**
     * 上传微信客服图片
     *
     * @param image
     * @return
     */
    @Override
    public SimpleResponse createActivityCsCode(MultipartFile image) {
        SimpleResponse simpleResponse = new SimpleResponse();
        String ext = FilenameUtils.getExtension(image.getOriginalFilename());
        String[] imageType = {"gif", "png", "jpg", "jpeg", "bmp"};
        if (StringUtils.isBlank(ext) || !Arrays.asList(imageType).contains(ext)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            simpleResponse.setMessage("文件格式格式错误");
            return simpleResponse;
        }
        String bucket = SHOPQRCODEPATH + "wechat/" + image.getOriginalFilename();
        String imgUrl;
        try {
            imgUrl = ossCaller.uploadObject2OSSInputstream(image.getInputStream(), bucket);
        } catch (IOException e) {
            log.error("客服二维码图片上传失败");
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("图片上传失败");
            return simpleResponse;
        }
        Map<String, Object> map = new HashMap<>(1);
        map.put("imgUrl", imgUrl);
        simpleResponse.setData(map);
        return simpleResponse;
    }

    @Override
    public SimpleResponse updatePublishedActivityDef(ActivityDefV2Req activityDefV2Req) {
        Long merchantId = Long.valueOf(activityDefV2Req.getMerchantId());
        // 对发布中的活动进行修改
        SimpleResponse<Object> response = activityDefCheckService.checkUnUpateParamWhenPublished(activityDefV2Req);
        if (response == null) {
            response = new SimpleResponse<>();
            log.error("数据库异常，未更新成功");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
        }
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            // 校验未通过，直接返回校验未通过原因
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return response;
        }
        // 校验更新参数通过, 更新数据
        ActivityDef activityDef = activityDefCheckService.updateFromActivityDefV2Req(activityDefV2Req);
        if (activityDef == null) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            log.error("数据库异常，未更新成功");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
        }

        if (StringUtils.equals(activityDefV2Req.getType(), ActivityDefTypeEnum.RED_PACKET.getCode() + "")) {
            ActivityTaskBO activityTaskBO = new ActivityTaskBO();
            activityTaskBO.setActivityDef(activityDef);
            activityTaskBO.setGoodCommentMissionNeedTime(Integer.valueOf(activityDefV2Req.getGoodCommentMissionNeedTime()));
            activityTaskBO.setFavCartMissionNeedTime(Integer.valueOf(activityDefV2Req.getGoodCommentMissionNeedTime()));
            boolean success = activityTaskService.updatePublished(activityTaskBO);
            if (!success) {
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
                return activityTaskService.saveError();
            }
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        return response;
    }

    @Override
    public SimpleResponse updateActivityDefFromActivityDefV2Req(ActivityDefV2Req activityDefV2Req) {
        Long merchantId = Long.valueOf(activityDefV2Req.getMerchantId());

        SimpleResponse<Object> response = new SimpleResponse<>();
        // 获取定义保证金
        activityDefV2Req = setDeposit2ActivityDefV2Req(activityDefV2Req);
        if (null == activityDefV2Req) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("获取保证金失败");
            return response;
        }

        // 对活动定义进行修改
        ActivityDef activityDef = activityDefCheckService.updateFromActivityDefV2Req(activityDefV2Req);
        if (null == activityDef) {
            log.error("数据库异常，未更新成功");
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            return response;
        }

        // 提交时，同时也 插入 库存记录表
        ActivityDefAudit activityDefAudit = activityDefAuditService.saveOrUpdateAudit4ActivityDef(activityDef);
        if (null == activityDefAudit) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return activityDefAuditService.saveError();
        }

        // 如果创建的是 红包活动，则记录到 activity_task表中
        if (activityDef.getType().equals(ActivityDefTypeEnum.RED_PACKET.getCode())) {
            ActivityTaskBO activityTaskBO = new ActivityTaskBO();
            activityTaskBO.setActivityDef(activityDef);
            activityTaskBO.setFillbackOrderAmount(new BigDecimal(activityDefV2Req.getFillBackOrderAmount()));
            activityTaskBO.setFavCartAmount(new BigDecimal(activityDefV2Req.getFavCartAmount()));
            activityTaskBO.setGoodCommentAmout(new BigDecimal(activityDefV2Req.getGoodCommentAmount()));
            activityTaskBO.setGoodCommentMissionNeedTime(Integer.valueOf(activityDefV2Req.getGoodCommentMissionNeedTime()));
            activityTaskBO.setFavCartMissionNeedTime(Integer.valueOf(activityDefV2Req.getFavCartMissionNeedTime()));
            boolean success = activityTaskService.saveOrUpdateFromActivityDef(activityTaskBO);
            if (!success) {
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
                return activityTaskService.saveError();
            }
        }
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
        return response;
    }

    private ActivityDefV2Req setDeposit2ActivityDefV2Req(ActivityDefV2Req activityDefV2Req) {
        Long merchantId = Long.valueOf(activityDefV2Req.getMerchantId());
        Long goodsId = null;
        String goodsIdStr = activityDefV2Req.getGoodsId();
        if (StringUtils.isNotBlank(goodsIdStr)) {
            goodsId = Long.valueOf(goodsIdStr);
        }
        List<Long> goodsIdList;
        String goodsIds = activityDefV2Req.getGoodsIdList();
        if (StringUtils.isNotBlank(goodsIds)) {
            goodsIdList = new ArrayList<>();
            String[] split = goodsIds.split(",");
            for (String str : split) {
                goodsIdList.add(Long.valueOf(str));
            }
        }

        if (null != goodsId) {
            Goods goods = goodsService.getById(goodsId);
            if (null == goods) {
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
                return null;
            }
            activityDefV2Req.setGoodsPrice(goods.getDisplayPrice());
        }
        SimpleResponse response = sellerActivityDefQV2Service.getDeposit(activityDefV2Req);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_DEF_CREATE_FORBID_REPEAT + merchantId);
            return null;
        }
        Map data = (Map) response.getData();
        activityDefV2Req.setDepositDef((BigDecimal) data.get("depositDef"));
        return activityDefV2Req;
    }

    @Override
    public SimpleResponse updateActivityDef(ActivityDefV2Req activityDefV2Req) {
        return null;
    }

}
