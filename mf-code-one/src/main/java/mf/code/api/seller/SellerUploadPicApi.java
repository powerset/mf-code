package mf.code.api.seller;

import mf.code.api.seller.service.SellerUploadPicService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * mf.code.api.seller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月21日 14:02
 */
@RestController
@RequestMapping(value = {"/api/seller/uploadPic", "/api/platform/uploadPic"})
public class SellerUploadPicApi {
    @Autowired
    private SellerUploadPicService sellerUploadPicService;

    /***
     * 老用户召回上传二维码
     * @param image
     * @param shopId
     * @return
     */
    @RequestMapping(path = "/uploadPicRecall", method = RequestMethod.POST)
    public SimpleResponse uploadPicRecall(@RequestParam(name = "image") MultipartFile image,
                                          @RequestParam(name = "shopId") Long shopId) {
        return this.sellerUploadPicService.uploadPicRecall(image, shopId);
    }
}
