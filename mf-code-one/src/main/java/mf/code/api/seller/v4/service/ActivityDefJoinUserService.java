package mf.code.api.seller.v4.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.seller.v4.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月22日 19:42
 */
public interface ActivityDefJoinUserService {
    SimpleResponse assistOrOpenRedPackUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef);

    SimpleResponse luckyWheelUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef);

    SimpleResponse orderBackUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef);

    SimpleResponse userTaskUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef, UserTaskTypeEnum userTaskTypeEnum);

    /**
     * 查询报销活动参与信息
     *
     * @param pageNum
     * @param size
     * @param activityDef
     * @return
     */
    SimpleResponse reimbursementJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef);
}
