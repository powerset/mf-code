package mf.code.api.seller;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.verify.VerifyService;
import mf.code.summary.constant.BizTypeEnum;
import mf.code.summary.service.SummaryV2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author gbf
 */
@RestController
@RequestMapping(value = "/api/seller/statistic")
public class SummaryV2Api {
    private final SummaryV2Service summaryV2Service;
    private final VerifyService verifyService;

    @Autowired
    public SummaryV2Api(SummaryV2Service summaryV2Service, VerifyService verifyService) {
        this.summaryV2Service = summaryV2Service;
        this.verifyService = verifyService;
    }

    /**
     * 用户统计
     * <pre>
     *     1:全部 2:助力 3:抽奖 4:轮盘 5:红包 0:拆红包
     *      当活动类型为:赠品活动 / 免单抽奖：
     *      展示:参与活动用户数、中奖用户数、领奖用户数
     *      当活动类型为:红包任务：
     *      展示:参与活动用户数、领奖用户数
     * </pre>
     *
     * @param shopId 店铺
     * @return -->/api/seller/statistic/summaryUser
     */
    @GetMapping(value = "/summaryUser")
    public SimpleResponse summary(@RequestParam(value = "shopId") Long shopId,
                                  @RequestParam(value = "type", required = false, defaultValue = "2") Integer type) {
        SimpleResponse error = verifyService.shopError(shopId);
        if (error != null) {
            return error;
        }
        if (type == null) {
            return new SimpleResponse(2, "活动类型不能为空");
        }
        BizTypeEnum typeEnum = this.getBizTypeEnum(type);

        SimpleResponse simpleResponse = summaryV2Service.summaryUser(shopId, typeEnum);
        Object data = simpleResponse.getData();
        if (data instanceof Map) {
            ((Map) simpleResponse.getData()).put("type", type);
        }
        return simpleResponse;
    }


    /**
     * 柱图 新增用户
     *
     * @param shopId 店铺
     * @param type   类型 1:日 2:周 3:月
     * @return -->/api/seller/statistic/summaryNewbie
     */
    @GetMapping(value = "summaryNewbie")
    public SimpleResponse summaryNewbie(@RequestParam(value = "shopId") Long shopId,
                                        @RequestParam(value = "type", required = false) Integer type) {
        SimpleResponse error = verifyService.shopError(shopId);
        if (error != null) {
            return error;
        }
        type = type == null ? 2 : type;
        return summaryV2Service.summaryNewbie(shopId, type);
    }

    /**
     * 裂变情况
     * <pre>
     *          api :1:全部 2:助力 3:抽奖 4:轮盘 5:红包 0:拆红包
     *          数据库: 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动',
     * </pre>
     *
     * @param shopId       店铺
     * @param dateType     日期类型 1:日 2:周 3:月
     * @param activityType 活动类型
     * @return -->/api/seller/statistic/summaryActivity
     */
    @GetMapping(value = "summaryActivity")
    public SimpleResponse summaryActivity(@RequestParam(value = "shopId") Long shopId,
                                          @RequestParam(value = "dateType", required = false) Integer dateType,
                                          @RequestParam(value = "activityType", required = false, defaultValue = "2") Integer activityType) {
        SimpleResponse error = verifyService.shopError(shopId);
        if (error != null) {
            return error;
        }
        dateType = dateType == null ? 1 : dateType;
        switch (activityType) {
            case 0:
                break;
        }
        BizTypeEnum bizTypeEnum = null;
        if (activityType == 0) {
            bizTypeEnum = BizTypeEnum.OPEN_RED_PACKET;
        } else if (activityType == 2) {
            bizTypeEnum = BizTypeEnum.ASSIST;
        } else if (activityType == 3) {
            bizTypeEnum = BizTypeEnum.LUCK_DRAW;
        } else {
            bizTypeEnum = BizTypeEnum.LUCK_WHEEL;
        }
        return summaryV2Service.summaryActivity(shopId, dateType, bizTypeEnum);
    }

    /**
     * 财务
     * <pre>
     *      api :1:全部 2:助力 3:抽奖 4:轮盘 5:红包 0:拆红包 对应数据库=>
     *      数据库: 活动类型：1轮盘抽奖，2计划类活动，3新人有礼活动，4商品免单抽奖，5回填订单活动 6助力活动 9拆红包',
     * </pre>
     *
     * @param shopId 店铺
     * @param type   类型
     * @return -->/api/seller/statistic/summaryFission
     */
    @GetMapping(value = "summaryFission")
    public SimpleResponse summaryFinanceNew(@RequestParam(value = "shopId") Long shopId,
                                            @RequestParam(value = "type", required = false, defaultValue = "2") Integer type) {
        SimpleResponse error = verifyService.shopError(shopId);
        if (error != null) {
            return error;
        }
        BizTypeEnum bizTypeEnum = this.getBizTypeEnum(type);
        SimpleResponse simpleResponse = summaryV2Service.summaryFinanceV2(shopId, type, bizTypeEnum);
        Object data = simpleResponse.getData();
        if (data instanceof Map) {
            ((Map) simpleResponse.getData()).put("type", type);
        }
        return simpleResponse;
    }

    /**
     * 用户统计/裂变情况/财务概况 前端传的活动类型是相同的
     *
     * @param type 活动类型
     * @return 业务中的活动类型枚举
     */
    private BizTypeEnum getBizTypeEnum(int type) {
        switch (type) {
            case 0:
                return BizTypeEnum.OPEN_RED_PACKET;
            case 2:
                return BizTypeEnum.ASSIST;
            case 3:
                return BizTypeEnum.LUCK_DRAW;
            case 4:
                return BizTypeEnum.LUCK_WHEEL;
            case 5:
                return BizTypeEnum.FILL_ORDER;
            case 6:
                return BizTypeEnum.GOOD_COMMENT;
            case 7:
                return BizTypeEnum.CART;
        }
        return null;
    }
}
