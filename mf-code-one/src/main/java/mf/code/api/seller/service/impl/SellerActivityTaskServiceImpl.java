package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v7.service.AppleteUserTaskCashbackService;
import mf.code.api.seller.dto.UserTaskAuditReq;
import mf.code.api.seller.dto.UserTaskV2AuditReq;
import mf.code.api.seller.dto.UserTaskV3AuditReq;
import mf.code.api.seller.service.SellerActivityTaskService;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.api.seller.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2018-11-14 17:43
 */
@Service
@Slf4j
public class SellerActivityTaskServiceImpl implements SellerActivityTaskService {

    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private UpayWxOrderService upayWxOrderService;

    @Autowired
    private UpayBalanceService upayBalanceService;

    @Autowired
    private UserCouponService userCouponService;

    @Autowired
    private AppleteUserTaskCashbackService appleteUserTaskCashbackService;


    /**
     * 商户审核用户任务
     *
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SimpleResponse auditUserTask(UserTaskAuditReq req) {
        SimpleResponse response = new SimpleResponse();
        if (!StringUtils.isNumeric(req.getUserTaskId()) || StringUtils.isBlank(req.getStatus())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("请求参数错误");
            return response;
        }
        Long userTaskId = Long.valueOf(req.getUserTaskId());
        UserTask userTaskOrg = userTaskService.selectByPrimaryKey(userTaskId);
        if (userTaskOrg == null || userTaskOrg.getStatus() != UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("该任务不允许修改");
            return response;
        }
        int status = Integer.valueOf(req.getStatus());
        UserTask usertask = new UserTask();
        usertask.setId(userTaskId);
        usertask.setUtime(new Date());
        usertask.setAuditingTime(new Date());
        usertask.setStatus(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
        usertask.setAuditingReason("[]");
        if (status != 1) {
            usertask.setStatus(UserTaskStatusEnum.AUDIT_ERROR.getCode());
            if (StringUtils.isBlank(req.getReason())) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                response.setMessage("请输入审核不通过原因");
                return response;
            }
            if (req.getReason().length() > 20) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                response.setMessage("审核不通过原因请在20字以内");
                return response;
            }
            List<Integer> indexArray = JSONArray.parseArray(req.getIndexArray(), Integer.class);
            List<String> picsJson = JSONArray.parseArray(userTaskOrg.getPics(), String.class);
            int size = 0;
            if (picsJson == null) {
                size = 1;
            } else {
                size = picsJson.size();
            }
            List<Map<String, Object>> reasonJson = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                Map<String, Object> map = new HashMap<>(2);
                map.put("index", i);
                if (indexArray == null || indexArray.contains(i)) {
                    map.put("reason", req.getReason());
                }
                reasonJson.add(map);
            }
            usertask.setAuditingReason(JSONArray.toJSONString(reasonJson));
        }
        int sqlResult = userTaskService.update(usertask);
        if (sqlResult == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("审核失败");
            return response;
        }
        // 审核成功，红包任务处理
        if (Integer.valueOf(req.getStatus()) == 1) {
            // 通过，如果是红包任务, 创建订单，发红包,创建user_coupon
            if (userTaskOrg.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.REDPACKTASK, UserCouponTypeEnum.FAV_CART);
            } else if (userTaskOrg.getType() == UserTaskTypeEnum.GOOD_COMMENT.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.GOODSCOMMENT, UserCouponTypeEnum.GOODS_COMMENT);
            } else {
                //若是抽奖类任务
                this.saveRedisCostTime(userTaskOrg, Long.valueOf(req.getUserTaskId()));
            }
        } else {
            // 更新userActivity
            if (userTaskOrg.getUserActivityId() != null && userTaskOrg.getUserActivityId() > 0) {
                UserActivity userActivity = new UserActivity();
                userActivity.setId(userTaskOrg.getUserActivityId());
                userActivity.setUtime(new Date());
                userActivityService.updateByPrimaryKey(userActivity);
            }
            //失败时间存储
            this.saveRedisCostTime(userTaskOrg, Long.valueOf(req.getUserTaskId()));
        }
        sendTemplateMessageService.sendTemplateMsg2Task(userTaskId, userTaskOrg.getUserId());
        return response;
    }

    /***
     * v2版本审核用户任务
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SimpleResponse auditUserTaskV2(UserTaskV2AuditReq req) {
        req.form();
        SimpleResponse response = new SimpleResponse();
        if (!StringUtils.isNumeric(req.getUserTaskId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请求参数错误");
        }
        Long userTaskId = Long.valueOf(req.getUserTaskId());
        UserTask userTaskOrg = userTaskService.selectByPrimaryKey(userTaskId);
        if (userTaskOrg == null || userTaskOrg.getStatus() != UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该任务不允许修改");
        }
        if (CollectionUtils.isEmpty(req.getPhotos())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请输入审核不通过原因");
        }

        int status = 1;
        UserTask usertask = new UserTask();
        usertask.setId(userTaskId);
        usertask.setUtime(new Date());
        usertask.setAuditingTime(new Date());
        usertask.setStatus(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
        usertask.setAuditingReason("[]");
        // 当前容错处理 FIXME
        if (!CollectionUtils.isEmpty(req.getPhotos())) {
            for (UserTaskV2AuditReq.Reason reason : req.getPhotos()) {
                if (null == reason || reason.getReason() == null) {
                    continue;
                }
                if (reason.getReason().length() > 20) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "审核不通过原因请在20字以内");
                }
                if (reason.getStatus() == -1) {
                    status = -1;
                }
            }
        }
        if (status != 1) {
            usertask.setStatus(UserTaskStatusEnum.AUDIT_ERROR.getCode());
            List<UserTaskV2AuditReq.Reason> pics = new ArrayList<>();
            for (UserTaskV2AuditReq.Reason reason : req.getPhotos()) {
                UserTaskV2AuditReq.Reason needReason = reason;
                if (StringUtils.isBlank(reason.getReason())) {
                    needReason.setReason("");
                    pics.add(needReason);
                }
            }
            usertask.setAuditingReason(JSONArray.toJSONString(pics));
        }
        int sqlResult = userTaskService.update(usertask);
        if (sqlResult == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "审核更新异常");
        }
        // 审核成功，红包任务处理
        if (status == 1) {
            // 通过，如果是红包任务, 创建订单，发红包,创建user_coupon
            if (userTaskOrg.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.REDPACKTASK, UserCouponTypeEnum.FAV_CART);
            } else if (userTaskOrg.getType() == UserTaskTypeEnum.GOOD_COMMENT.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.GOODSCOMMENT, UserCouponTypeEnum.GOODS_COMMENT);
            } else {
                //若是抽奖类任务
                this.saveRedisCostTime(userTaskOrg, Long.valueOf(req.getUserTaskId()));
            }
        } else {
            // 更新userActivity
            if (userTaskOrg.getUserActivityId() != null && userTaskOrg.getUserActivityId() > 0) {
                UserActivity userActivity = new UserActivity();
                userActivity.setId(userTaskOrg.getUserActivityId());
                userActivity.setUtime(new Date());
                userActivityService.updateByPrimaryKey(userActivity);
            }
            //失败时间存储
            this.saveRedisCostTime(userTaskOrg, Long.valueOf(req.getUserTaskId()));
        }
        sendTemplateMessageService.sendTemplateMsg2Task(userTaskId, userTaskOrg.getUserId());
        return response;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SimpleResponse auditUserTaskV3(UserTaskV3AuditReq req) {
        /**参数校验**/
        req.form();
        SimpleResponse response = new SimpleResponse();
        if (!StringUtils.isNumeric(req.getUserTaskId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请求参数错误");
        }
        Long userTaskId = Long.valueOf(req.getUserTaskId());
        UserTask userTaskOrg = userTaskService.selectByPrimaryKey(userTaskId);
        if (userTaskOrg == null || userTaskOrg.getStatus() != UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该任务不允许修改");
        }
        if(userTaskOrg != null && userTaskOrg.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()){
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "已审核通过，无需审核");
        }
        if (CollectionUtils.isEmpty(req.getPhotos())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请输入审核不通过原因");
        }

        int status = 1;
        UserTask usertask = new UserTask();
        usertask.setId(userTaskId);
        usertask.setUtime(new Date());
        usertask.setAuditingTime(new Date());
        usertask.setStatus(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
        usertask.setAuditingReason("[]");
        // 当前容错处理 FIXME
        if (!CollectionUtils.isEmpty(req.getPhotos())) {
            for (UserTaskV3AuditReq.Reason reason : req.getPhotos()) {
                if (null == reason || reason.getReason() == null) {
                    continue;
                }
                if (reason.getReason().length() > 50) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "审核不通过原因请在50字以内");
                }
                if (reason.getStatus() == -1) {
                    status = -1;
                }
            }
        }

        /**审核状态不通过需要修改user_task表**/
        if (status != 1) {
            usertask.setStatus(UserTaskStatusEnum.AUDIT_ERROR.getCode());
            List<UserTaskV3AuditReq.Reason> pics = new ArrayList<>();
            log.info(">>>>>>>>>>have find getPhotos=" + JSONArray.toJSONString(req.getPhotos()));
            for (UserTaskV3AuditReq.Reason reason : req.getPhotos()) {
                UserTaskV3AuditReq.Reason re = new UserTaskV3AuditReq.Reason();
                re.setIndex(reason.getIndex());
                if (null == reason.getReason()) {
                    re.setReason(" ");
                } else {
                    re.setReason(reason.getReason());
                }
                if (null == reason.getOrderId()) {
                    re.setOrderId("");
                } else {
                    re.setOrderId(reason.getOrderId());
                }
                re.setPic(reason.getPic());
                re.setStatus(reason.getStatus());
                pics.add(re);
            }

            log.info(">>>>>>>>>>>>>>find pics = " + JSONArray.toJSONString(pics));
            usertask.setAuditingReason(JSONArray.toJSONString(pics));
        }
        int sqlResult = userTaskService.update(usertask);
        if (sqlResult == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "审核更新异常");
        }

        // 审核成功，红包任务处理
        if (status == 1) {
            //类型好评返现9 old
            if (userTaskOrg.getType() == UserTaskTypeEnum.GOOD_COMMENT.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.GOODSCOMMENT, UserCouponTypeEnum.GOODS_COMMENT);
            }
            //类型好评返现9 new
            else if (userTaskOrg.getType() == UserTaskTypeEnum.GOOD_COMMENT_V2.getCode()) {
                SimpleResponse simpleResponse = appleteUserTaskCashbackService.userTaskCashback(userTaskOrg, BizTypeEnum.GOODSCOMMENT, UserCouponTypeEnum.GOODS_COMMENT, userTaskOrg.getType());
                Assert.isTrue(!simpleResponse.error(), simpleResponse.getCode(), simpleResponse.getMessage());
            }
            //类型是回填订单6 old
            else if (userTaskOrg.getType() == UserTaskTypeEnum.ORDER_REDPACK.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.RANDOMREDPACK, UserCouponTypeEnum.ORDER_RED_PACKET);
            }
            //类型是回填订单6 new
            else if (userTaskOrg.getType() == UserTaskTypeEnum.ORDER_REDPACK_V2.getCode()) {
                SimpleResponse simpleResponse = appleteUserTaskCashbackService.userTaskCashback(userTaskOrg, BizTypeEnum.RANDOMREDPACK, UserCouponTypeEnum.ORDER_RED_PACKET, userTaskOrg.getType());
                Assert.isTrue(!simpleResponse.error(), simpleResponse.getCode(), simpleResponse.getMessage());
            }
            //类型是2:免单商品抽奖发起者 3:免单商品抽奖参与者 old
            else if (userTaskOrg.getType() == UserTaskTypeEnum.ORDER_BACK_START.getCode()
                    || userTaskOrg.getType() == UserTaskTypeEnum.ORDER_BACK_JOIN.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.ORDER_BACK, UserCouponTypeEnum.ORDER_BACK_START);
            }
            //类型是2:免单商品抽奖发起者 3:免单商品抽奖参与者 new
            else if (userTaskOrg.getType() == UserTaskTypeEnum.ORDER_BACK_START_V2.getCode() ||
                    userTaskOrg.getType() == UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode()) {
                SimpleResponse simpleResponse = appleteUserTaskCashbackService.userTaskCashback(userTaskOrg, BizTypeEnum.ORDER_BACK, UserCouponTypeEnum.ORDER_BACK_START, userTaskOrg.getType());
                Assert.isTrue(!simpleResponse.error(), simpleResponse.getCode(), simpleResponse.getMessage());
            }
            //类型是4,5,8助力活动发起者 old
            else if (userTaskOrg.getType() == UserTaskTypeEnum.ASSIST_START.getCode() ||
                    userTaskOrg.getType() == UserTaskTypeEnum.NEW_MAN_START.getCode() ||
                    userTaskOrg.getType() == UserTaskTypeEnum.NEW_MAN_JOIN.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.ASSIST_ACTIVITY, UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST);
            }
            //类型是16助力活动发起者 new
            else if (userTaskOrg.getType() == UserTaskTypeEnum.ASSIST_START_V2.getCode()) {
                SimpleResponse simpleResponse = appleteUserTaskCashbackService.userTaskCashback(userTaskOrg, BizTypeEnum.ASSIST_ACTIVITY, UserCouponTypeEnum.ASSIST, userTaskOrg.getType());
                Assert.isTrue(!simpleResponse.error(), simpleResponse.getCode(), simpleResponse.getMessage());
            }
            //收藏架构 old
            else if (userTaskOrg.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                auditRpTaskSuccessByType(response, userTaskOrg, BizTypeEnum.REDPACKTASK, UserCouponTypeEnum.FAV_CART);
            }
            //收藏架构 new
            else if (userTaskOrg.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode()) {
                SimpleResponse simpleResponse = appleteUserTaskCashbackService.userTaskCashback(userTaskOrg, BizTypeEnum.REDPACKTASK, UserCouponTypeEnum.FAV_CART, userTaskOrg.getType());
                Assert.isTrue(!simpleResponse.error(), simpleResponse.getCode(), simpleResponse.getMessage());
            } else {
                //若是抽奖类任务
                this.saveRedisCostTime(userTaskOrg, Long.valueOf(req.getUserTaskId()));
            }
        } else {
            // 更新userActivity
            if (userTaskOrg.getUserActivityId() != null && userTaskOrg.getUserActivityId() > 0) {
                UserActivity userActivity = new UserActivity();
                userActivity.setId(userTaskOrg.getUserActivityId());
                userActivity.setUtime(new Date());
                userActivityService.updateByPrimaryKey(userActivity);
            }
            //失败时间存储
            this.saveRedisCostTime(userTaskOrg, Long.valueOf(req.getUserTaskId()));
        }
        sendTemplateMessageService.sendTemplateMsg2Task(userTaskId, userTaskOrg.getUserId());
        return response;
    }


    /***
     * 存储redis，审核时间-提交时间的时间差(若是抽奖类任务，审核通过时也需要存储)
     * @param userTask
     * @param userTaskId
     */
    private void saveRedisCostTime(UserTask userTask, Long userTaskId) {
        // 存储审核任务的审核更新时间
        String costStr = redisService.getUserTaskSupplyAuditCost(userTaskId);
        if (userTask.getReceiveTime() != null) {
            Long costTime = 0L;
            if (StringUtils.isNotBlank(costStr)) {
                costTime = NumberUtils.toLong(costStr);
            }
            //提交-审核的时间差值
            redisService.updateUserTaskSupplyAuditCost(userTaskId, costTime + DateUtil.secondDiff(userTask.getReceiveTime(), new Date()));
        }
    }

//    private SimpleResponse auditTaskByType(SimpleResponse response, UserTask userTaskOrg,
//                                           BizTypeEnum bizTypeEnum, UserCouponTypeEnum userCouponTypeEnum, Integer type) {
//        Long activityTaskId = userTaskOrg.getActivityTaskId();
//        ActivityTask activityTask = activityTaskService.getById(activityTaskId);
//        // 查询应发金额
//        if (activityTask == null) {
//            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
//            response.setMessage("审核失败1");
//            return response;
//        }
//        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配, 扣除佣金
//        CommissionDisDto commissionDisDto = new CommissionDisDto(userTaskOrg.getUserId(), userTaskOrg.getShopId(),
//                userCouponTypeEnum, activityTask.getRpAmount());
//        commissionDisDto.setActivityTaskId(activityTask.getId());
//        commissionDisDto.setActivityDefId(userTaskOrg.getActivityDefId());
//        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);
//
//        // 信息加工入库
//        UserTask userTask = new UserTask();
//        userTask.setId(userTaskOrg.getId());
//        userTask.setUserId(userTaskOrg.getUserId());
//        userTask.setActivityId(userTaskOrg.getActivityId());
//        userTask.setOrderId(userTaskOrg.getOrderId());
//        userTask.setRpAmount(commissionBO.getCommission());
//        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
//        userTask.setReceiveTime(new Date());
//        userTask.setUtime(new Date());
//        int rows = userTaskService.update(userTask);
//        if (rows == 0) {
//            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
//            return response;
//        }
//
//        // 更新userActivity
//        if (userTaskOrg.getUserActivityId() != null && userTaskOrg.getUserActivityId() > 0) {
//            UserActivity userActivity = new UserActivity();
//            userActivity.setId(userTaskOrg.getUserActivityId());
//            userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
//            userActivity.setUtime(new Date());
//            userActivityService.updateByPrimaryKey(userActivity);
//        }
//        redisService.saveRedisUserTaskRedPack(userTaskOrg.getId());
//        redisService.deleteUserTaskCommitAndAudit(userTaskOrg.getId());
//
//        Long aid = userTaskOrg.getActivityId();
//        Activity activityDB = activityService.findById(aid);
//        // 中奖保证金扣除
//        Integer integer = activityDefService.reduceDeposit(activityDB.getActivityDefId(), activityDB.getPayPrice(), 0);
//
//        if (integer <= 0) {
//            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
//            response.setMessage("任务库存不足");
//            return response;
//        }
//
//        long uid = userTaskOrg.getUserId();
//        long shopId = userTaskOrg.getShopId();
//
//        //免单，助力/赠品 需要创建订单，对用户余额操作，并将中奖信息入库
//        if (type == UserTaskTypeEnum.ORDER_BACK_START_V2.getCode() || type == UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode() || type == UserTaskTypeEnum.ASSIST_START_V2.getCode()) {
//            //创建订单
//            UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(uid, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.PALTFORMRECHARGE.getCode(),
//                    null, "中奖任务红包", activityDB.getMerchantId(), shopId, OrderStatusEnum.ORDERED.getCode(), activityDB.getPayPrice()
//                    , IpUtil.getServerIp(), null, "中奖任务红包", new Date(), null, null, bizTypeEnum.getCode(), aid);
//            this.upayWxOrderService.create(upayWxOrder);
//            // 对用户余额进行操作
//            this.upayBalanceService.updateOrCreate(activityDB.getMerchantId(), shopId, uid, activityDB.getPayPrice());
//            // 中奖信息入库user_coupon表
//            List<Long> amountUserList = new ArrayList<>();
//            amountUserList.add(uid);
//
//            boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT + aid + ":" + uid);
//            if (!success) {
//                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
//                return response;
//            }
//            success = userCouponService.saveAmountUsersFromActivity(activityDB, amountUserList);
//            if (!success) {
//                //log.error("插入user_coupon表失败");
//                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
//                response.setMessage("插入user_coupon表失败");
//                return response;
//            }
//        }
//
//        redisService.deleteUserTaskCommitAndAudit(userTask.getId());
//        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT + aid + ":" + uid);
//
//        return response;
//    }

    /**
     * 任务审核成功，更新操作
     *
     * @param response
     * @param userTaskOrg
     * @param bizTypeEnum
     * @param userCouponTypeEnum
     * @return
     */
    private SimpleResponse auditRpTaskSuccessByType(SimpleResponse response, UserTask userTaskOrg,
                                                    BizTypeEnum bizTypeEnum, UserCouponTypeEnum userCouponTypeEnum) {
        Long activityTaskId = userTaskOrg.getActivityTaskId();
        ActivityTask activityTask = activityTaskService.getById(activityTaskId);
        // 查询应发金额
        if (activityTask == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("审核失败1");
            return response;
        }
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配, 扣除佣金
        CommissionDisDto commissionDisDto = new CommissionDisDto(userTaskOrg.getUserId(), userTaskOrg.getShopId(),
                userCouponTypeEnum, activityTask.getRpAmount());
        commissionDisDto.setActivityTaskId(activityTask.getId());
        commissionDisDto.setActivityDefId(userTaskOrg.getActivityDefId());
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        // 信息加工入库
        UserTask userTask = new UserTask();
        userTask.setId(userTaskOrg.getId());
        userTask.setUserId(userTaskOrg.getUserId());
        userTask.setActivityId(userTaskOrg.getActivityId());
        userTask.setOrderId(userTaskOrg.getOrderId());
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        userTask.setReceiveTime(new Date());
        userTask.setUtime(new Date());
        int rows = userTaskService.update(userTask);
        if (rows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }

        // 更新userActivity
        if (userTaskOrg.getUserActivityId() != null && userTaskOrg.getUserActivityId() > 0) {
            UserActivity userActivity = new UserActivity();
            userActivity.setId(userTaskOrg.getUserActivityId());
            userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
            userActivity.setUtime(new Date());
            userActivityService.updateByPrimaryKey(userActivity);
        }

        redisService.saveRedisUserTaskRedPack(userTaskOrg.getId());
        redisService.deleteUserTaskCommitAndAudit(userTaskOrg.getId());
        return response;
    }

    /**
     * 分页查询用户任务
     *
     * @param params
     * @param page
     * @param size
     * @return
     */
    @Override
    public SimpleResponse pageListUserTask(Map<String, Object> params, Integer page, Integer size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        params.put("del", DelEnum.NO.getCode());
        int count = userTaskService.countUserTask(params);
        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("page", page);
        result.put("size", size);
        if (count == 0) {
            result.put("list", new ArrayList<>());
            simpleResponse.setData(result);
            return simpleResponse;
        }
        params.putAll(PageUtil.pageSizeTolimit(page, size, count));
        List<UserTask> userTaskList = userTaskService.query(params);
        List<Map<String, Object>> userTaskMapList = new ArrayList<>();
        for (int i = 0; i < userTaskList.size(); i++) {
            userTaskMapList.add(BeanMapUtil.beanToMapIgnore(userTaskList.get(i), "utime"));
        }
        // 拼接用户手机号，微信号，加权计划名，关联活动批次号，转化类型
        copyUserTaskToResult(userTaskMapList);
        result.put("list", userTaskMapList);
        simpleResponse.setData(result);
        return simpleResponse;
    }

    @Override
    public SimpleResponse taskAuditList(Map<String, Object> params, Integer page, Integer size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        params.put("del", DelEnum.NO.getCode());
        int count = userTaskService.countUserTask(params);
        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("page", page);
        result.put("size", size);
        if (count == 0) {
            result.put("list", new ArrayList<>());
            simpleResponse.setData(result);
            return simpleResponse;
        }
        params.putAll(PageUtil.pageSizeTolimit(page, size, count));
        List<UserTask> userTaskList = userTaskService.query(params);
        List<Map<String, Object>> userTaskMapList = new ArrayList<>();

        Map<Long, ActivityDef> activityDefMap = activitydefWeightByUserTask(userTaskList);
        for (int i = 0; i < userTaskList.size(); i++) {
            UserTask userTask = userTaskList.get(i);
            Map<String, Object> map = BeanMapUtil.beanToMapIgnoreV3(userTask, "utime");
            map.put("weight", false);
            if (userTask.getActivityId() != null && userTask.getActivityId() > 0) {
                ActivityDef def = activityDefMap.get(userTask.getActivityId());
                if (def != null && def.getWeighting() == WeightingEnum.YES.getCode()) {
                    map.put("weight", true);
                }
            }
            userTaskMapList.add(map);
        }
        if (null == userTaskMapList || userTaskMapList.size() < 1) {
            log.info(">>>>>>>>>>>>>>>have find userTaskMapList is null!!");
            result.put("list", new ArrayList<>());
            simpleResponse.setData(result);
            return simpleResponse;
        }
        // 拼接用户手机号，微信号，加权计划名，关联活动批次号，转化类型
        copyUserTaskToResultV3(userTaskMapList);
        result.put("list", userTaskMapList);
        simpleResponse.setData(result);
        return simpleResponse;
    }

    /***
     *
     * @param userTasks
     * @return key:activityId, value:def
     */
    private Map<Long, ActivityDef> activitydefWeightByUserTask(List<UserTask> userTasks) {
        List<Long> activityIds = new ArrayList<>();
        for (UserTask userTask : userTasks) {
            if (userTask.getActivityId() != null && userTask.getActivityId() > 0 && activityIds.indexOf(userTask.getActivityId()) == -1) {
                activityIds.add(userTask.getActivityId());
            }
        }
        if (CollectionUtils.isEmpty(activityIds)) {
            return new HashMap<>();
        }
        QueryWrapper<Activity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .in(Activity::getId, activityIds);
        List<Activity> activities = activityService.list(queryWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            return new HashMap<>();
        }
        List<Long> defIds = new ArrayList<>();
        for (Activity activity : activities) {
            if (defIds.indexOf(activity.getActivityDefId()) == -1) {
                defIds.add(activity.getActivityDefId());
            }
        }
        if (CollectionUtils.isEmpty(defIds)) {
            return new HashMap<>();
        }
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(ActivityDef::getId, defIds);
        List<ActivityDef> defs = activityDefService.list(wrapper);
        if (CollectionUtils.isEmpty(defs)) {
            return new HashMap<>();
        }
        Map<Long, ActivityDef> activityDefMap = new HashMap<>();
        for (ActivityDef def : defs) {
            activityDefMap.put(def.getId(), def);
        }

        Map<Long, ActivityDef> resp = new HashMap<>();
        for (Activity activity : activities) {
            ActivityDef def = activityDefMap.get(activity.getActivityDefId());
            if (def != null) {
                resp.put(activity.getId(), def);
            }
        }

        return resp;
    }

    /**
     * 拼接用户手机号，微信号，加权计划名，关联活动批次号，转化类型
     *
     * @param userTaskMapList
     */
    private void copyUserTaskToResultV3(List<Map<String, Object>> userTaskMapList) {
        if (CollectionUtils.isEmpty(userTaskMapList)) {
            return;
        }
        Map<Long, User> userMap = new LinkedHashMap<>(userTaskMapList.size());
        Map<Long, Activity> activityMap = new LinkedHashMap<>(userTaskMapList.size());
        Map<Long, MerchantShop> shopMap = new LinkedHashMap<>(userTaskMapList.size());
        Map<Long, Goods> goodsMap = new LinkedHashMap<>(userTaskMapList.size());
        for (Map<String, Object> usertaskMap : userTaskMapList) {
            if (usertaskMap.get("userId") == null) {
                continue;
            }
            userMap.put((Long) usertaskMap.get("userId"), null);
            if (usertaskMap.get("activityId") != null) {
                activityMap.put((Long) usertaskMap.get("activityId"), null);
            }
            shopMap.put((Long) usertaskMap.get("shopId"), null);
            if (usertaskMap.get("activityId") != null) {
                goodsMap.put((Long) usertaskMap.get("goodsId"), null);
            }
        }
        Map<String, Object> params = new LinkedHashMap<>();
        if (!CollectionUtils.isEmpty(userMap)) {
            params.put("userIds", userMap.keySet());
            List<User> userList = userService.query(params);
            for (User user : userList) {
                userMap.put(user.getId(), user);
            }
        }
        if (!CollectionUtils.isEmpty(shopMap)) {
            List<MerchantShop> shopList = merchantShopService.selectShopByIdList(new ArrayList<>(shopMap.keySet()));
            for (MerchantShop shop : shopList) {
                shopMap.put(shop.getId(), shop);
            }
        }
        params.clear();
        if (!CollectionUtils.isEmpty(activityMap)) {
            params.put("activityIds", activityMap.keySet());
            params.put("del", DelEnum.NO.getCode());
            List<Activity> activityList = activityService.findPage(params);
            for (Activity activity : activityList) {
                activityMap.put(activity.getId(), activity);
            }
        }
        params.clear();
        if (!CollectionUtils.isEmpty(goodsMap)) {
            Collection<Goods> goodsList = goodsService.listByIds(goodsMap.keySet());
            for (Goods goods : goodsList) {
                goodsMap.put(goods.getId(), goods);
            }
        }
        for (Map<String, Object> usertaskMap : userTaskMapList) {
            usertaskMap.put("ctime", DateUtil.dateToString((Date) usertaskMap.get("ctime"), DateUtil.FORMAT_ONE));
            usertaskMap.put("auditingTime", DateUtil.dateToString((Date) usertaskMap.get("auditingTime"), DateUtil.FORMAT_ONE));
            usertaskMap.put("receiveTime", DateUtil.dateToString((Date) usertaskMap.get("receiveTime"), DateUtil.FORMAT_ONE));
            usertaskMap.put("phone", "未绑定");
            if (userMap.get(usertaskMap.get("userId")) != null) {
                usertaskMap.put("wechat", userMap.get(usertaskMap.get("userId")).getNickName());
                if (StringUtils.isNotBlank(userMap.get(usertaskMap.get("userId")).getMobile())) {
                    usertaskMap.put("phone", userMap.get(usertaskMap.get("userId")).getMobile());
                }
            }


            if (null != usertaskMap.get("type") && (int) usertaskMap.get("type") == UserTaskTypeEnum.MCH_PLAN.getCode()) {
                MerchantShop shop = shopMap.get(usertaskMap.get("shopId"));
                if (shop != null) {
                    usertaskMap.put("planTitle", shop.getShopName() + "加权计划");

                    if (null != usertaskMap.get("activityId") && null != activityMap.get(usertaskMap.get("activityId"))) {
                        usertaskMap.put("batchNum", activityMap.get(usertaskMap.get("activityId")).getBatchNum());
                    }

                }
            }
            if (null != usertaskMap.get("goodsId") && null != goodsMap.get(usertaskMap.get("goodsId"))) {
                usertaskMap.put("goodsName", goodsMap.get(usertaskMap.get("goodsId")).getDisplayGoodsName());
            }

            Integer type = (Integer) usertaskMap.get("type");
            usertaskMap.put("type", type);
//            // 中奖者任务
//            if (type == UserTaskTypeEnum.ORDER_BACK_JOIN.getCode()
//                    || type == UserTaskTypeEnum.ORDER_BACK_START.getCode()
//                    || type == UserTaskTypeEnum.ORDER_BACK_START_V2.getCode()
//                    || type == UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode()
//            ) {
//                //免单
//                usertaskMap.put("type", 3);
//            } else if (type == UserTaskTypeEnum.NEW_MAN_JOIN.getCode()
//                    || type == UserTaskTypeEnum.NEW_MAN_START.getCode()
//                    || type == UserTaskTypeEnum.ASSIST_START.getCode()
//                    || type == UserTaskTypeEnum.ASSIST_START_V2.getCode()
//            ) {
//                //赠品
//                usertaskMap.put("type", 4);
//            } else if (type == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()
//                       || type ==  UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode()) {
//                //收藏
//                usertaskMap.put("type", 5);
//            } else if (type == UserTaskTypeEnum.GOOD_COMMENT.getCode() || type == UserTaskTypeEnum.GOOD_COMMENT_V2.getCode()) {
//                //好评
//                usertaskMap.put("type", 6);
//            }
//            else if (type == UserTaskTypeEnum.ORDER_REDPACK.getCode() || type == UserTaskTypeEnum.ORDER_REDPACK_V2.getCode()){
//                //回填
//                usertaskMap.put("type", 7);
//            }
        }
    }


    /**
     * 拼接用户手机号，微信号，加权计划名，关联活动批次号，转化类型
     *
     * @param userTaskMapList
     */
    private void copyUserTaskToResult(List<Map<String, Object>> userTaskMapList) {
        if (CollectionUtils.isEmpty(userTaskMapList)) {
            return;
        }
        Map<Long, User> userMap = new HashMap<>(userTaskMapList.size());
        Map<Long, Activity> activityMap = new HashMap<>(userTaskMapList.size());
        Map<Long, MerchantShop> shopMap = new HashMap<>(userTaskMapList.size());
        Map<Long, Goods> goodsMap = new HashMap<>(userTaskMapList.size());
        for (Map<String, Object> usertaskMap : userTaskMapList) {
            userMap.put((Long) usertaskMap.get("userId"), null);
            if (usertaskMap.get("activityId") != null) {
                activityMap.put((Long) usertaskMap.get("activityId"), null);
            }
            shopMap.put((Long) usertaskMap.get("shopId"), null);
            if (usertaskMap.get("activityId") != null) {
                goodsMap.put((Long) usertaskMap.get("goodsId"), null);
            }
        }
        Map<String, Object> params = new HashMap<>();
        if (!CollectionUtils.isEmpty(userMap)) {
            params.put("userIds", userMap.keySet());
            List<User> userList = userService.query(params);
            for (User user : userList) {
                userMap.put(user.getId(), user);
            }
        }
        if (!CollectionUtils.isEmpty(shopMap)) {
            List<MerchantShop> shopList = merchantShopService.selectShopByIdList(new ArrayList<>(shopMap.keySet()));
            for (MerchantShop shop : shopList) {
                shopMap.put(shop.getId(), shop);
            }
        }
        params.clear();
        if (!CollectionUtils.isEmpty(activityMap)) {
            params.put("activityIds", activityMap.keySet());
            params.put("del", DelEnum.NO.getCode());
            List<Activity> activityList = activityService.findPage(params);
            for (Activity activity : activityList) {
                activityMap.put(activity.getId(), activity);
            }
        }
        params.clear();
        if (!CollectionUtils.isEmpty(goodsMap)) {
            Collection<Goods> goodsList = goodsService.listByIds(goodsMap.keySet());
            for (Goods goods : goodsList) {
                goodsMap.put(goods.getId(), goods);
            }
        }
        for (Map<String, Object> usertaskMap : userTaskMapList) {
            usertaskMap.put("ctime", DateUtil.dateToString((Date) usertaskMap.get("ctime"), DateUtil.FORMAT_ONE));
            usertaskMap.put("auditingTime", DateUtil.dateToString((Date) usertaskMap.get("auditingTime"), DateUtil.FORMAT_ONE));
            usertaskMap.put("receiveTime", DateUtil.dateToString((Date) usertaskMap.get("receiveTime"), DateUtil.FORMAT_ONE));
            usertaskMap.put("phone", "未绑定");
            if (userMap.get(usertaskMap.get("userId")) != null) {
                usertaskMap.put("wechat", userMap.get(usertaskMap.get("userId")).getNickName());
                if (StringUtils.isNotBlank(userMap.get(usertaskMap.get("userId")).getMobile())) {
                    usertaskMap.put("phone", userMap.get(usertaskMap.get("userId")).getMobile());
                }
            }


            if (null != usertaskMap.get("type") && (int) usertaskMap.get("type") == UserTaskTypeEnum.MCH_PLAN.getCode()) {
                MerchantShop shop = shopMap.get(usertaskMap.get("shopId"));
                if (shop != null) {
                    usertaskMap.put("planTitle", shop.getShopName() + "加权计划");

                    if (null != usertaskMap.get("activityId") && null != activityMap.get(usertaskMap.get("activityId"))) {
                        usertaskMap.put("batchNum", activityMap.get(usertaskMap.get("activityId")).getBatchNum());
                    }

                }
            }
            if (null != usertaskMap.get("goodsId") && null != goodsMap.get(usertaskMap.get("goodsId"))) {
                usertaskMap.put("goodsName", goodsMap.get(usertaskMap.get("goodsId")).getDisplayGoodsName());
            }

            Integer type = (Integer) usertaskMap.get("type");
            // 中奖者任务
            if (type == UserTaskTypeEnum.MCH_PLAN.getCode()
                    || type == UserTaskTypeEnum.ORDER_BACK_START.getCode()
                    || type == UserTaskTypeEnum.ORDER_BACK_JOIN.getCode()) {
                usertaskMap.put("type", 3);
            } else if (type == UserTaskTypeEnum.NEW_MAN_JOIN.getCode()
                    || type == UserTaskTypeEnum.NEW_MAN_START.getCode()
                    || type == UserTaskTypeEnum.ASSIST_START.getCode()) {
                usertaskMap.put("type", 4);
            } else if (type == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                usertaskMap.put("type", 5);
            } else if (type == UserTaskTypeEnum.GOOD_COMMENT.getCode()) {
                usertaskMap.put("type", 6);
            }
        }
    }
}
