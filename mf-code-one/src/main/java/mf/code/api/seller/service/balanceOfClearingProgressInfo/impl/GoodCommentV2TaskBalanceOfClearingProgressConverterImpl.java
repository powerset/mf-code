package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 18:06
 */
@Service
@Slf4j
public class GoodCommentV2TaskBalanceOfClearingProgressConverterImpl extends BalanceOfClearingProgressService {
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserCouponService userCouponService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean goodCommentType = activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode());
        if (goodCommentType) {
            //查询活动消耗金额-userCoupon
            QueryWrapper<UserCoupon> userCouponWrapper = new QueryWrapper<>();
            userCouponWrapper.lambda()
                    .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                    .eq(UserCoupon::getShopId, activityDef.getShopId())
                    .in(UserCoupon::getActivityDefId, activityDef.getId())
                    .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
            ;
            List<UserCoupon> userCoupons = this.userCouponService.list(userCouponWrapper);

            //获取活动绑定金额
            QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
            userTaskWrapper.lambda()
                    .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                    .eq(UserTask::getShopId, activityDef.getShopId())
                    .eq(UserTask::getActivityDefId, activityDef.getId())
                    .eq(UserTask::getType, UserTaskTypeEnum.GOOD_COMMENT_V2.getCode())
                    .in(UserTask::getStatus, Arrays.asList(
                            UserTaskStatusEnum.AUDIT_ERROR.getCode(),
                            UserTaskStatusEnum.AUDIT_WAIT.getCode(),
                            UserTaskStatusEnum.AUDIT_SUCCESS.getCode()
                    ))
                    .orderByDesc(UserTask::getId)
            ;
            List<UserTask> userTasks = this.userTaskService.list(userTaskWrapper);

            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.from(activityDef, userTasks, userCoupons);
            return dto;
        }
        return null;
    }
}
