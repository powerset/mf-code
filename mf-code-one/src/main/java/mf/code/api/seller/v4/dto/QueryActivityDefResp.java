package mf.code.api.seller.v4.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.WeightingEnum;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.v4.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月20日 19:41
 */
@Data
public class QueryActivityDefResp {
    /**
     * 活动定义编号(更新时需要)
     */
    private Long activityDefId;

    /**
     * 店铺编号
     */
    private Long shopId;
    /**
     * 商户编号
     */
    private Long merchantId;
    /**
     * 活动类型
     */
    private Integer type;
    /**
     * 商品信息,若是多个，则用逗号分隔（非必填）
     */
    private String goodsIds;
    /**
     * 商品库存
     */
    private String stockDef;
    /**
     * 红包金额,单个概念（非必填）
     */
    private String amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    /**
     * 区间红包金额,若是区间，则用json存储[{"from":"0.1", "to":"0.2","amount":"1"},{},{}]（非必填）
     */
    private List<Map> amountJson;
    /**
     * 关键词（非必填）
     */
    private List<String> keywords;
    /**
     * 活动时长(单位:分)（非必填）
     */
    private String condDrawTime;
    /**
     * 领奖(任务)时长（分）（非必填）
     */
    private String missionNeedTime;
    /**
     * 是否加权（非必填）(true:加权;false:不加权)
     */
    private boolean weighting;
    /**
     * 扣库存方式（非必填）: 1,用户发起成团活动时，商品库存数-1; 2,用户完成成团条件后，商品库存数-1
     */
    private int stockType;
    /**
     * 商品免单额外佣金（非必填）
     */
    private String commission;
    /**
     * 优惠券（非必填）("优惠券1,优惠券2")，入库[1,2]JsonString
     */
    private List<Map> merchantCouponJson;
    /**
     * 客服微信信息（非必填）;json存储格式:[{"wechat":"","pic":"https://asset.wxyundian.com/data-test3/activity/cspic/wechat/WechatIMG9.jpeg"}]
     */
    private List<Map> serviceWx = new ArrayList<>();
    /**
     * 成团条件(满多少人开奖触发),若是拆红包，存储时+1
     */
    private Integer condPersionCount;
    /**
     * 中奖名额(开奖-几个人中奖)
     */
    private Integer hitsPerDraw;
    /**
     * 保证金
     */
    private String depositDef;
    /**
     * 成交方式
     */
    private Integer finishType;
    /**
     * 审核方式
     */
    private Integer auditType;
    /**
     * 商品价值
     */
    private String goodsPrice;
    /**
     * 商户自定义商品主图（仅淘宝下单存在）
     */
    private String goodsMainPic;
    /**
     * 商品详细
     */
    private List<Map<String, Object>> goodsVOList;

    public void from(ActivityDef activityDef) {
        this.merchantId = activityDef.getMerchantId();
        this.shopId = activityDef.getShopId();
        this.activityDefId = activityDef.getId();
        this.type = activityDef.getType();
        if (activityDef.getGoodsId() != null && activityDef.getGoodsId() > 0) {
            this.goodsIds = String.valueOf(activityDef.getGoodsId());
        }
        if (StringUtils.isNotBlank(activityDef.getGoodsIds())) {
            this.goodsIds = StringUtils.join(JSONObject.parseArray(activityDef.getGoodsIds()), ",");
        }
        if (activityDef.getGoodsPrice() != null && activityDef.getGoodsPrice().compareTo(BigDecimal.ZERO) > 0) {
            this.amount = activityDef.getGoodsPrice().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        if (activityDef.getCommission() != null && activityDef.getCommission().compareTo(BigDecimal.ZERO) > 0) {
            this.amount = activityDef.getCommission().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        if (StringUtils.isNotBlank(activityDef.getAmountJson())) {
            this.amountJson = JSON.parseArray(activityDef.getAmountJson(), Map.class);
        }
        this.stockDef = String.valueOf(activityDef.getStockDef());
        this.keywords = JSON.parseArray(activityDef.getKeyWord(), String.class);
        this.condDrawTime = String.valueOf(activityDef.getCondDrawTime());
        this.missionNeedTime = String.valueOf(activityDef.getMissionNeedTime());
        if (activityDef.getWeighting() == WeightingEnum.YES.getCode()) {
            this.weighting = true;
        }
        this.stockType = activityDef.getStockType();
        if (StringUtils.isNotBlank(activityDef.getServiceWx())) {
            this.serviceWx = JSON.parseArray(activityDef.getServiceWx(), Map.class);
        }
        if (activityDef.getCondPersionCount() != null && activityDef.getCondPersionCount() > 0) {
            this.condPersionCount = activityDef.getCondPersionCount();
            if (activityDef.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
                this.condPersionCount = this.condPersionCount - 1;
            }
        }
        if (activityDef.getHitsPerDraw() != null && activityDef.getHitsPerDraw() > 0) {
            this.hitsPerDraw = activityDef.getHitsPerDraw();
        }
        if (activityDef.getCommission() != null && activityDef.getCommission().compareTo(BigDecimal.ZERO) > 0) {
            this.commission = activityDef.getCommission().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        if (activityDef.getFinishType() != null) {
            this.finishType = activityDef.getFinishType();
        }
        if (activityDef.getAuditType() != null) {
            this.auditType = activityDef.getAuditType();
        }
        if (activityDef.getGoodsPrice() != null) {
            this.goodsPrice = activityDef.getGoodsPrice().toString();
        }
        this.depositDef = activityDef.getDepositDef().setScale(2, BigDecimal.ROUND_DOWN).toString();
    }
}
