package mf.code.api.seller.v4;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.dto.CreateDefReq;
import mf.code.api.seller.v4.service.SellerActivityDefV4Service;
import mf.code.common.exception.ArgumentException;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.seller.v4
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月18日 15:21
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/seller/v4/activity/def")
public class SellerActivityDefV4Api {
    @Autowired
    private SellerActivityDefV4Service sellerActivityDefV4Service;

    /***
     * v4版创建营销活动
     * @param req
     * @return
     */
    @RequestMapping(path = "/createDef", method = RequestMethod.POST)
    public SimpleResponse createDef(@RequestBody CreateDefReq req) {
        return this.sellerActivityDefV4Service.createDef(req);
    }

    /***
     * v4版更新营销活动
     * @param req
     * @return
     */
    @RequestMapping(path = "/updateDef", method = RequestMethod.POST)
    public SimpleResponse updateDef(@RequestBody CreateDefReq req) {
        return this.sellerActivityDefV4Service.updateDef(req);
    }

    /***
     * v4版查看营销活动
     * @param activityDefId
     * @return
     */
    @RequestMapping(path = "/queryDef", method = RequestMethod.GET)
    public SimpleResponse queryDef(@RequestParam("merchantId") Long merchantId,
                                   @RequestParam("shopId") Long shopId,
                                   @RequestParam("activityDefId") Long activityDefId) {
        return this.sellerActivityDefV4Service.queryDef(activityDefId);
    }

    /***
     * v4版分页查询营销活动
     * @param merchantId
     * @param shopId
     * @param status 1 待支付 2 进行中 3 已结束 4 库存警告 5 已结算
     * @param type (2 免单抽奖活动 4, "幸运大转盘" 5, "回填订单红包任务") 6, "赠品活动") 7, "收藏加购" 9, "拆红包" 10, "好评晒图"),
     * @param size
     * @param pageNum
     * @return
     */
    @RequestMapping(path = "/queryDefByPage", method = RequestMethod.GET)
    public SimpleResponse queryDefByPage(@RequestParam("merchantId") Long merchantId,
                                         @RequestParam("shopId") Long shopId,
                                         @RequestParam(name = "status", required = false) Integer status,
                                         @RequestParam(name = "type", required = false) Integer type,
                                         @RequestParam(name = "size", defaultValue = "10") Integer size,
                                         @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum) {
        return this.sellerActivityDefV4Service.queryDefByPage(merchantId, shopId, status, type, size, pageNum);
    }


    /***
     * v4版获取保证金
     * @param req
     * @return
     */
    @RequestMapping(path = "/getDepositDef", method = RequestMethod.POST)
    public SimpleResponse getDepositDef(@RequestBody CreateDefReq req) {
        return this.sellerActivityDefV4Service.getDepositDef(req);
    }

    /***
     * v4版补库存
     * @param req defId, stock必填，其余不要
     * @return
     */
    @RequestMapping(path = "/supplyAddStock", method = RequestMethod.POST)
    public SimpleResponse supplyAddStock(@RequestBody CreateDefReq req) {
        return this.sellerActivityDefV4Service.supplyAddStock(req);
    }

    /***
     * v4版分页查询营销活动参与人数具体信息
     * @param merchantId
     * @param shopId
     * @param type (2 免单抽奖活动 4, "幸运大转盘" 5, "回填订单红包任务") 6, "赠品活动") 7, "收藏加购" 9, "拆红包" 10, "好评晒图"),
     * @param size
     * @param pageNum
     * @return
     */
    @RequestMapping(path = "/queryActivityDefUserJoinByPage", method = RequestMethod.GET)
    public SimpleResponse queryActivityDefUserJoinByPage(@RequestParam("merchantId") Long merchantId,
                                                         @RequestParam("shopId") Long shopId,
                                                         @RequestParam("activityDefId") Long activityDefId,
                                                         @RequestParam(name = "type", required = false) Integer type,
                                                         @RequestParam(name = "size", defaultValue = "10") Integer size,
                                                         @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum) {
        return this.sellerActivityDefV4Service.queryActivityDefUserJoinByPage(merchantId, shopId, activityDefId, type, size, pageNum);
    }

    /**
     * 查询当前用户拆红包活动
     *
     * @return
     */
    @GetMapping("/getOpenRedPackActivityInfo")
    public SimpleResponse getOpenRedPackActivityInfo(@RequestParam("merchantId") Long merchantId,
                                                     @RequestParam("shopId") Long shopId) {

        return this.sellerActivityDefV4Service.getOpenRedPackActivityInfo(merchantId, shopId);
    }

    /**
     * 查询参与用户信息
     *
     * @return
     */
    @GetMapping("/applyDetail")
    public SimpleResponse applyDetail(@RequestParam("merchantId") Long merchantId,
                                      @RequestParam("activityDefId") Long activityDefId,
                                      @RequestParam(name = "size", defaultValue = "10") Integer size,
                                      @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum) {

        return this.sellerActivityDefV4Service.applyDetail(merchantId, activityDefId, size, pageNum);
    }
}
