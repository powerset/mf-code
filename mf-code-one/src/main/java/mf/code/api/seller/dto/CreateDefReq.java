package mf.code.api.seller.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.WeightingEnum;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月18日 15:27
 */
@Data
public class CreateDefReq {
    /**
     * 活动定义编号(更新时需要)
     */
    private Long activityDefId;
    /**
     * 店铺编号
     */
    private Long shopId;
    /**
     * 商户编号
     */
    private Long merchantId;
    /**
     * 活动类型
     */
    private Integer type;
    /**
     * 额外数据对象
     */
    private String extraData;
    /**
     * 商品信息，当前版本为productId,若是多个，则用逗号分隔（非必填）
     */
    private String goodsIds;
    /**
     * 商品库存
     */
    private Integer stockDef;
    /**
     * 红包金额,单个概念（非必填）
     */
    private String amount;
    /**
     * 区间红包金额,若是区间，则用json存储[{"from":"0.1", "to":"0.2","amount":"1"},{},{}]（非必填）
     */
    private String amountJson;
    /**
     * 保证金金额(订单专用)
     */
    private String depositDef;


    /**
     * 冗余逻辑字段：非前后端交互字段
     **/
    private String goodsPrice;

    private ExtraData extraDataObject;

    public void init() {
        if (StringUtils.isNotBlank(this.extraData)) {
            CreateDefReq.ExtraData picsJson = JSONObject.parseObject(this.extraData, CreateDefReq.ExtraData.class);
            if (picsJson != null) {
                this.extraDataObject = picsJson;
            }
        }
    }

    @Data
    public static class ExtraData {
        /***********助力-免单字段***************/
        /**
         * 关键词（非必填）（新手任务时为群口令）
         */
        private String keywords;
        /**
         * 活动时长(单位:分)（非必填）
         */
        private String condDrawTime;
        /**
         * 领奖(任务)时长（分）（非必填）
         */
        private String missionNeedTime;
        /**
         * 是否加权（非必填）(true:加权;false:不加权)
         */
        private boolean weighting;
        /**
         * 扣库存方式（非必填）: 1,用户发起成团活动时，商品库存数-1; 2,用户完成成团条件后，商品库存数-1
         */
        private int stockType;
        /**
         * 商品免单额外佣金（非必填）
         */
        private String commission;
        /**
         * 优惠券（非必填）("优惠券1,优惠券2")，入库[1,2]JsonString
         */
        private String merchantCouponJson;
        /**
         * 客服微信信息（非必填）;json存储格式:[{"wechat":"","pic":"https://asset.wxyundian.com/data-test3/activity/cspic/wechat/WechatIMG9.jpeg"}]
         */
        private String serviceWx;
        /**
         * 成团条件(满多少人开奖触发),若是拆红包，存储时+1
         */
        private Integer condPersionCount;
        /**
         * 中奖名额(开奖-几个人中奖)
         */
        private Integer hitsPerDraw;
        /**
         * 审核方式（1:人工审核 2:自动审核）
         */
        private Integer auditType;
        /**
         * 任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）
         */
        private Integer finishType;
        /**
         * 商户自定义商品主图
         */
        private String goodsMainPic;
        /**
         * 商品返现价格（淘宝下单）
         */
        private String goodsPrice;
        /**
         * 商品skuId（微信下单所传）
         */
        private Long skuId;

        /**
         * 阅读新手任务 金额
         */
        private String readAmount;

        /**
         * 扫一扫 金额
         */
        private String scanAmount;

        /**
         * 拆红包活动id
         */
        private Long openRedPackactiveId;

        /**
         * 设置微信群任务佣金（店长任务专用）
         */
        private String setWechatAmount;

        /**
         * 邀请下级粉丝人数（店长任务专用）
         */
        private Integer inviteCount;

        /**
         * 完成邀请下级粉丝任务佣金（店长任务专用）
         */
        private String inviteAmount;

        /**
         * 发展粉丝成为店长人数（店长任务专用）
         */
        private Integer developCount;

        /**
         * 完成发展粉丝为店长任务佣金（店长任务专用）
         */
        private String developAmount;

        /**
         * 店长任务——设置微信群任务每个店长限领次数，默认1,（店长任务专用，该参数当前版本不是从前端传来）
         */
        private Integer setWechatTaskTimes = 1;

        /**
         * 店长任务——要请粉丝任务每个店长限领次数，默认1,（店长任务专用，该参数当前版本不是从前端传来）
         */
        private Integer inviteTaskTimes = 1;

        /**
         * 店长任务——发展粉丝为店长任务每个店长限领次数，默认10,（店长任务专用，该参数当前版本不是从前端传来）
         */
        private Integer developTaskTimes = 10;

        /**
         * 单人单次报销的最大值
         */
        private String maxReimbursement;

        /**
         * 单人报销最大值
         */
        private String maxAmount;

        public void from(ActivityDef activityDef) {
            if (StringUtils.isNotBlank(activityDef.getKeyWord())) {
                List<String> keywords = JSON.parseArray(activityDef.getKeyWord(), String.class);
                this.keywords = StringUtils.join(keywords, ",");
            }
            if (activityDef.getCondDrawTime() != null && activityDef.getCondDrawTime() > 0) {
                this.condDrawTime = String.valueOf(activityDef.getCondDrawTime());
            }
            if (activityDef.getMissionNeedTime() != null && activityDef.getMissionNeedTime() > 0) {
                this.missionNeedTime = String.valueOf(activityDef.getMissionNeedTime());
            }
            if (activityDef.getWeighting() == null || WeightingEnum.YES.getCode() == activityDef.getWeighting()) {
                this.weighting = true;
            }
            if (activityDef.getStockType() != null && activityDef.getStockType() > 0) {
                this.stockType = activityDef.getStockType();
            }
            if (StringUtils.isNotBlank(activityDef.getMerchantCouponJson())) {
                this.merchantCouponJson = StringUtils.join(JSONObject.parseArray(activityDef.getMerchantCouponJson()), ",");
            }
            if (StringUtils.isNotBlank(activityDef.getServiceWx())) {
                this.serviceWx = activityDef.getServiceWx();
            }
            if (activityDef.getCondPersionCount() != null && activityDef.getCondPersionCount() > 0) {
                this.condPersionCount = activityDef.getCondPersionCount();
            }
            if (activityDef.getHitsPerDraw() != null && activityDef.getHitsPerDraw() > 0) {
                this.hitsPerDraw = activityDef.getHitsPerDraw();
            }
            if (activityDef.getCommission() != null && activityDef.getCommission().compareTo(BigDecimal.ZERO) > 0) {
                this.commission = activityDef.getCommission().setScale(2, BigDecimal.ROUND_DOWN).toString();
            }
            if (activityDef.getAuditType() != null) {
                this.auditType = activityDef.getAuditType();
            }
            if (activityDef.getFinishType() != null) {
                this.finishType = activityDef.getFinishType();
            }
            if (activityDef.getGoodsPrice() != null) {
                this.goodsPrice = activityDef.getGoodsPrice().toString();
            }
        }
    }

    public void from(ActivityDef activityDef) {
        this.merchantId = activityDef.getMerchantId();
        this.shopId = activityDef.getShopId();
        this.activityDefId = activityDef.getId();
        this.type = activityDef.getType();
        if (activityDef.getGoodsId() != null && activityDef.getGoodsId() > 0 && StringUtils.isBlank(this.goodsIds)) {
            this.goodsIds = String.valueOf(activityDef.getGoodsId());
        }
        if (StringUtils.isNotBlank(activityDef.getGoodsIds()) && StringUtils.isBlank(this.goodsIds)) {
            this.goodsIds = StringUtils.join(JSONObject.parseArray(activityDef.getGoodsIds()), ",");
        }
        if (activityDef.getGoodsPrice() != null && activityDef.getGoodsPrice().compareTo(BigDecimal.ZERO) > 0 && StringUtils.isBlank(this.amount)) {
            this.amount = activityDef.getGoodsPrice().setScale(2, BigDecimal.ROUND_DOWN).toString();
        } else if (activityDef.getCommission() != null && activityDef.getCommission().compareTo(BigDecimal.ZERO) > 0 && StringUtils.isBlank(this.amount)) {
            this.amount = activityDef.getCommission().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        if (activityDef.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode()) && activityDef.getGoodsId() != null && activityDef.getGoodsId() == -1 && StringUtils.isBlank(this.amountJson)) {
            this.amountJson = activityDef.getAmountJson();
        }
        if (activityDef.getStockDef() != null && activityDef.getStockDef() > 0 && this.getStockDef() == null) {
            this.stockDef = activityDef.getStockDef();
        }
        if (activityDef.getGoodsPrice() != null && activityDef.getGoodsPrice().compareTo(BigDecimal.ZERO) > 0) {
            this.goodsPrice = activityDef.getGoodsPrice().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
    }


    public ActivityDef to(ActivityDef activityDef) {
        this.init();
        if (StringUtils.isNotBlank(this.getAmount()) || StringUtils.isNotBlank(this.getAmountJson())) {
            if (activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())) {
                if (StringUtils.isNotBlank(this.getAmountJson())) {
                    //金额区间
                    List<Map> amountJson = JSONArray.parseArray(this.getAmountJson(), Map.class);
                    if (!CollectionUtils.isEmpty(amountJson)) {
                        activityDef.setAmountJson(JSONArray.toJSONString(amountJson));
                    }
                    activityDef.setGoodsPrice(BigDecimal.ZERO);
                }
                if (StringUtils.isNotBlank(this.getAmount())) {
                    activityDef.setAmountJson("");
                    activityDef.setGoodsPrice(new BigDecimal(this.getAmount()));
                }
            } else {
                if (StringUtils.isNotBlank(this.getAmount())) {
                    activityDef.setGoodsPrice(new BigDecimal(this.getAmount()));
                }
            }
        }

        if (StringUtils.isNotBlank(this.getGoodsIds())) {
            List<Long> goodsIdList = new ArrayList<>();
            String[] split = this.getGoodsIds().split(",");
            //不是收藏，好评，回填时，一个商品存储goodsId
            if (split.length == 1
                    && !this.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode())
                    && !this.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode())
                    && !this.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode())
                    && !this.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode())
                    && !this.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                    && !this.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
                activityDef.setGoodsId(Long.valueOf(split[0]));
            } else {
                for (String goodsId : split) {
                    goodsIdList.add(Long.valueOf(goodsId));
                }
                activityDef.setGoodsIds(JSON.toJSONString(goodsIdList));
            }
        }
        if (this.getStockDef() != null && this.getStockDef() > 0) {
            activityDef.setStockDef(this.getStockDef());
        }

        if (this != null && this.getExtraDataObject() != null) {
            if (StringUtils.isNotBlank(this.getExtraDataObject().getKeywords())) {
                String keyWord = this.getExtraDataObject().getKeywords();
                if (StringUtils.isNotBlank(keyWord)) {
                    String[] split = keyWord.split(",");
                    activityDef.setKeyWord(JSON.toJSONString(Arrays.asList(split)));
                }
            }
            String condDrawTime = this.getExtraDataObject().getCondDrawTime();
            if (StringUtils.isNotBlank(condDrawTime)) {
                // 分
                activityDef.setCondDrawTime(Integer.valueOf(condDrawTime));
            }
            Integer condPersionCount = this.getExtraDataObject().getCondPersionCount();
            if (condPersionCount != null && condPersionCount > 0) {
                if (this.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
                    activityDef.setCondPersionCount(Integer.valueOf(condPersionCount) + 1);
                } else {
                    activityDef.setCondPersionCount(Integer.valueOf(condPersionCount));
                }
            }
            Integer hitsPerDraw = this.getExtraDataObject().getHitsPerDraw();
            if (hitsPerDraw != null && hitsPerDraw > 0) {
                activityDef.setHitsPerDraw(hitsPerDraw);
            }
            String missionNeedTime = this.getExtraDataObject().getMissionNeedTime();
            if (StringUtils.isNotBlank(missionNeedTime)) {
                activityDef.setMissionNeedTime(Integer.valueOf(missionNeedTime));
            }
            String merchantCouponJson = this.getExtraDataObject().getMerchantCouponJson();
            if (StringUtils.isNotBlank(merchantCouponJson)) {
                List<Long> merchantCouponList = new ArrayList<>();
                String[] split = merchantCouponJson.split(",");
                for (String merchantCoupon : split) {
                    if (!RegexUtils.StringIsNumber(merchantCoupon)) {
                        continue;
                    }
                    merchantCouponList.add(Long.valueOf(merchantCoupon));
                }
                if (!CollectionUtils.isEmpty(merchantCouponList)) {
                    activityDef.setMerchantCouponJson(JSON.toJSONString(merchantCouponList));
                }
            }
            if (StringUtils.isNotBlank(this.getExtraDataObject().getServiceWx())) {
                List<Map> serviceWxs = JSONArray.parseArray(this.getExtraDataObject().getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(serviceWxs)) {
                    activityDef.setServiceWx(JSONArray.toJSONString(serviceWxs));
                }
            }
            // 设置佣金
            activityDef.setCommission(BigDecimal.ZERO);
            if (StringUtils.isNotBlank(this.getExtraDataObject().getCommission())) {
                BigDecimal commission = new BigDecimal(this.getExtraDataObject().getCommission());
                if (commission != null && commission.compareTo(BigDecimal.ONE) >= 0) {
                    activityDef.setCommission(commission);
                }
            } else {
                //若没有佣金
                if (this.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()) && StringUtils.isBlank(this.getAmountJson())) {
                    activityDef.setCommission(new BigDecimal(this.getAmount()));
                }
                //红包金额等同于佣金的概念活动(收藏加购，拆红包，好评晒图)
                if (this.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode())
                        || this.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode())
                        || this.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())
                        || this.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT.getCode())
                        || this.getType().equals(ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode())
                        || this.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode())) {
                    activityDef.setCommission(new BigDecimal(this.getAmount()));
                    activityDef.setGoodsPrice(new BigDecimal(this.getAmount()));
                }
            }
            //扣库存方式
            if (this.getExtraDataObject().getStockType() > 0) {
                activityDef.setStockType(this.getExtraDataObject().getStockType());
            }
            //是否加权
            if (!this.getExtraDataObject().isWeighting()) {
                activityDef.setWeighting(WeightingEnum.NO.getCode());
            } else {
                activityDef.setWeighting(WeightingEnum.YES.getCode());
            }
        }
        return activityDef;
    }
}
