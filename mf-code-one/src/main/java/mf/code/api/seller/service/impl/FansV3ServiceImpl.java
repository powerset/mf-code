package mf.code.api.seller.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.SendTemplateMessage.impl.push.PushSendTemplateMsgServiceImpl;
import mf.code.api.seller.dto.DtoPush;
import mf.code.api.seller.service.FansV3Service;
import mf.code.common.constant.RocketMqTopicTagEnum;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.push.repo.dao.PushHistoryMapper;
import mf.code.push.repo.po.PushHistory;
import mf.code.push.service.PushHistoryService;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author gbf
 */
@Slf4j
@Service
public class FansV3ServiceImpl implements FansV3Service {
    private final PushHistoryMapper pushHistoryMapper;
    private final UserService userService;
    private final UserPubJoinService userPubJoinService;
    private final UserActivityService userActivityService;
    private final StringRedisTemplate stringRedisTemplate;
    private final GoodsService goodsService;
    private final RocketMQTemplate rocketMQTemplate;
    private final PushHistoryService pushHistoryService;

    @Autowired
    public FansV3ServiceImpl(PushHistoryMapper pushHistoryMapper, UserService userService, UserPubJoinService userPubJoinService, UserActivityService userActivityService, StringRedisTemplate stringRedisTemplate, GoodsService goodsService, RocketMQTemplate rocketMQTemplate, PushHistoryService pushHistoryService) {
        this.pushHistoryMapper = pushHistoryMapper;
        this.userService = userService;
        this.userPubJoinService = userPubJoinService;
        this.userActivityService = userActivityService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.goodsService = goodsService;
        this.rocketMQTemplate = rocketMQTemplate;
        this.pushHistoryService = pushHistoryService;
    }

    /**
     * 推送动作
     *
     * @param dto
     * @return
     */
    @Override
    public SimpleResponse push(DtoPush dto) {
        //推送消息的基础信息
        Long merchantId = dto.getMerchantId();
        Long shopId = dto.getShopId();
        Integer activityType = dto.getActivityType();

        SimpleResponse simpleResponse = new SimpleResponse();
        Integer size = pushHistoryService.countPushHistoryTodayByShopId(shopId);
        if (size > 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("每天仅能推送一次小程序消息");
            return simpleResponse;
        }

        PushHistory e = new PushHistory();
        e.setMerchantId(dto.getMerchantId());
        e.setShopId(dto.getShopId());
        e.setType(dto.getType());
        e.setActivityType(dto.getActivityType());
        e.setActivityId(dto.getActivityId());
        if (dto.getGoodsId() != null) {
            Goods goods = goodsService.selectById(dto.getGoodsId());
            e.setGoodsId(dto.getGoodsId());
            e.setGoodsName(goods.getDisplayGoodsName());
            e.setGoodsPic(goods.getPicUrl());
        }
        e.setReachTotal(dto.getReachTotal());
        e.setRemind(dto.getRemind());
        e.setDetail(dto.getDetail());
        e.setExtra(dto.getExtra());
        e.setUtime(new Date());
        e.setCtime(new Date());
        int result = pushHistoryMapper.insertSelective(e);
        if (result > 0) {
            //查询推送的用户
            List<Map> users = this.notActiveUserList(shopId);

            //推送
            // ----- wxPushService.send(merchantId, shopId, activityType, users, e.getId());
            //消息推送

            // 队列
            Map<String, Object> tempMap = new HashMap<>(5);
            tempMap.put("merchantId", dto.getMerchantId());
            tempMap.put("shopId", dto.getShopId());
            tempMap.put("activityType", activityType);
            tempMap.put("pushId", e.getId());
            for (Map user : users) {
                tempMap.put("userId", user.get("id"));
                try {
                    this.rocketMQTemplate.syncSend(RocketMqTopicTagEnum.TEMPLATE_PUSH_RECALL.getTopic() + ":"
                            + RocketMqTopicTagEnum.TEMPLATE_PUSH_RECALL.getTag(), JSON.toJSONString(tempMap));
                    log.info("rocketmq消息生产：" + JSON.toJSONString(tempMap));
                } catch (Exception ee) {
                    log.error("rocketmq消息生产错误：" + JSON.toJSONString(tempMap), ee);
                }
            }
            return simpleResponse;
        } else {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR);
            return simpleResponse;
        }
    }


    /**
     * 列表头部统计数据项
     *
     * @param shopId .
     * @return ..
     */
    @Override
    public SimpleResponse summary(Long shopId) {
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        Map<String, Object> respMap = new HashMap<>(5);
        respMap.put("all", userService.countAll(shopId));
        respMap.put("hasPhone", userService.countHasPhone(shopId));
        //开始时间  5天内是可触达的,为活跃用户
        Date begin = DateUtil.getDateAfterZero(DateUtil.addDay(new Date(), -4));
        //结束时间  今天23:59:59
        Date end = DateUtil.getDateBeforeZero(new Date());
        respMap.put("active", userService.countActive(shopId, begin, end));

        //不活跃用户
        respMap.put("notActive", notActiveUserList(shopId).size());
        //沉睡用户
        end = DateUtil.getDateBeforeZero(DateUtil.addDay(new Date(), -7));
        // sleeping中的sql只用到了<end
        respMap.put("sleeping", userService.sleeping(shopId, begin, end));
        response.setData(respMap);
        return response;
    }

    /**
     * 列表
     *
     * @param shopId
     * @param selectType
     * @param begin
     * @param end
     * @param dateType    7=近期天 15=近15天 类推
     * @param phone
     * @param pageCurrent
     * @param pageSize
     * @return
     */
    @Override
    public SimpleResponse list(Long shopId, Long selectType, Long begin, Long end, Long dateType, Long phone, Integer pageCurrent, Integer pageSize) {
        Map<String, Object> param = new HashMap<>(8);
        param.put("shopId", shopId);
        param.put("selectType", selectType);
        param.put("begin", begin == null ? 0 : DateUtil.dateToString(new Date(begin), DateUtil.FORMAT_ONE));
        if (dateType != null) {
            if (dateType == 7L) {
                param.put("begin", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -7))[0], DateUtil.FORMAT_ONE));
            }
            if (dateType == 15L) {
                param.put("begin", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -15))[0], DateUtil.FORMAT_ONE));
            }
            if (dateType == 30L) {
                param.put("begin", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -30))[0], DateUtil.FORMAT_ONE));
            }
        }
        param.put("end", begin == null ? DateUtil.dateToString(new Date(), DateUtil.FORMAT_ONE) : DateUtil.dateToString(new Date(end), DateUtil.FORMAT_ONE));
        param.put("phone", phone);
        //活跃
        if (selectType == 3) {
            param.put("userStatTimeBegin", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -4))[0], DateUtil.FORMAT_ONE));
            param.put("userStatTimeEnd", DateUtil.dateToString(new Date(), DateUtil.FORMAT_ONE));

            // 不活跃
        } else if (selectType == 4) {
            param.put("userStatTimeBegin", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -6))[0], DateUtil.FORMAT_ONE));
            param.put("userStatTimeEnd", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -5))[1], DateUtil.FORMAT_ONE));

            //沉睡
        } else if (selectType == 5) {
            param.put("userStatTimeEnd", DateUtil.dateToString(DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -6))[0], DateUtil.FORMAT_ONE));
        }


        Page<Map<String, Object>> page = PageHelper.startPage(pageCurrent, pageSize);
        List<User> list = userService.fansList(param);
        List<Map<String, Object>> resMaps = new ArrayList<>();
        for (User u : list) {
            Date utime = u.getUtime();
            String userStat = this.getUserStat(utime);
            Map<String, Object> m = new HashMap<>(11);
            m.put("userId", u.getId());
            m.put("wxAvatar", u.getAvatarUrl());
            m.put("wxNickName", u.getNickName());
            m.put("phone", u.getMobile());
            m.put("registTime", u.getGrantTime());
            m.put("userStatus", userStat);
            // 活动参与数
            m.put("joinActivityTotal", joinActivityTotal(u.getShopId(), u.getId()));
            //收益
            m.put("totalEarnings", this.earnings(u.getShopId(), u.getId()));
            m.put("fans", userPubJoinService.sumFans(shopId, u.getId()));
            m.put("parentPhone", this.getParent(u.getShopId(), u.getId()) == null ? "" : this.getParent(u.getShopId(), u.getId()).getMobile());
            m.put("lastLoginTime", DateUtil.todayYesterdayBeforYesterday(u.getUtime()));
            resMaps.add(m);
        }
        PageInfo<Map<String, Object>> info = page.toPageInfo();
        info.setList(resMaps);

        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(info);
        return response;
    }

    /**
     * 获取上级用户
     *
     * @param shopId /
     * @param userId /
     * @return /
     */
    private User getParent(Long shopId, Long userId) {
        UserPubJoin parent = userPubJoinService.findParent(shopId, userId);
        if (parent == null) {
            return null;
        }
        return userService.selectByPrimaryKey(parent.getUserId());
    }


    /**
     * 推送历史
     *
     * @param shopId
     * @param activityType
     * @param pageCurrent
     * @param pageSize
     * @return
     */
    @Override
    public SimpleResponse history(Long shopId, Integer activityType, int pageCurrent, int pageSize) {
        Page<PushHistory> page = PageHelper.startPage(pageCurrent, pageSize);
        Map<String, Object> m = new HashMap<>(2);
        m.put("shopId", shopId);
        m.put("activityType", activityType);
        List<PushHistory> pushHistories = pushHistoryMapper.selectByShopId(m);
        for (PushHistory history : pushHistories) {
            //在redis的set中获取点击次数 . 点击次数是在登陆拦截中写入redis中set集合中.
            Long size = stringRedisTemplate.opsForSet().size(RedisKeyConstant.RECALL_PUSH_STATISTICS + shopId + ":" + history.getId());
            history.setClickTotal(size == null ? 0 : size.intValue());
        }
        PageInfo<PushHistory> info = page.toPageInfo();
        info.setList(pushHistories);
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(info);
        return response;
    }


    @Override
    public boolean incrClickTime(Long pushId) {
        return pushHistoryMapper.incrClickTime(pushId);
    }

    /**
     * 异步  将通过分享进入的请求,解析场景值,自增:推送统计的点击次数
     *
     * @param scene /
     */
//    @Async
    @Override
    public void asyncIncrClickPush(String scene) {
        if (StringUtils.isBlank(scene)) {
            return;
        }
        String[] split = scene.split(",");
        String shopId = split[1];
        String pushId = "null";
        String userId = "null";
        for (String str : split) {
            if (str.contains(PushSendTemplateMsgServiceImpl.SCENE_PUSH_MARK_PUSHID) || str.contains(PushSendTemplateMsgServiceImpl.SCENE_PUSH_MARK_USERID)) {
                String[] pids = str.split(PushSendTemplateMsgServiceImpl.SCENE_PUSH_MARK_PUSHID);
                if (pids.length > 1) {
                    pushId = pids[1];
                }

                String[] userids = str.split(PushSendTemplateMsgServiceImpl.SCENE_PUSH_MARK_USERID);
                if (userids.length > 1) {
                    userId = userids[1];
                }
            }
        }
        // gbf 2019/3/4 0004 改成redis方式
        Long addResult = stringRedisTemplate.opsForSet().add(RedisKeyConstant.RECALL_PUSH_STATISTICS + shopId + ":" + pushId, userId);
        if (addResult.longValue() > 0) {
            this.incrClickTime(Long.parseLong(pushId));
        }
    }


    /**
     * 计算收益
     *
     * @param userId /
     * @return /
     */
    private BigDecimal earnings(Long shopId, Long userId) {
        Map<String, Object> totalScottareParams = new HashMap<>(2);
        totalScottareParams.put("shopId", shopId);
        totalScottareParams.put("userId", userId);
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());

        //缴税总金额
        BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);
        //任务收益
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getShopId, shopId)
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .eq(UserPubJoin::getSubUid, userId)
        ;
        List<UserPubJoin> userPubJoins = this.userPubJoinService.list(userPubJoinQueryWrapper);
        BigDecimal taskProfit = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            taskProfit = userPubJoins.get(0).getTotalCommission();
        }
        BigDecimal totalProfit = taskProfit.add(allMinerTotalScottare);
        return totalProfit.setScale(2, BigDecimal.ROUND_DOWN);
    }

    private Long joinActivityTotal(Long shopId, Long userId) {
//        Long sumUserTask = userTaskService.sumUserTask(shopId, userId);
        return userActivityService.sumUserJoinActivity(shopId, userId);
    }

    /**
     * 用户是否活跃
     *
     * @param date /
     * @return /
     */
    private String getUserStat(Date date) {
        if (date != null) {
            // 第二参数-第一参数
            boolean b = DateUtil.timeSub(date, DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -4))[0]);
            boolean b2 = DateUtil.timeSub(date, DateUtil.getBeginAndEnd(DateUtil.addDay(new Date(), -6))[0]);
            if (b) {
                return "活跃用户";
            } else if (b2) {
                return "不活跃用户";
            } else {
                return "沉睡用户";
            }
        } else {
            return "沉睡用户";
        }
    }

    /**
     * 不活跃用户列表
     *
     * @param shopId .
     * @return .
     */
    private List<Map> notActiveUserList(Long shopId) {
        Date begin = DateUtil.getDateAfterZero(DateUtil.addDay(new Date(), -6));
        Date end = DateUtil.getDateBeforeZero(DateUtil.addDay(new Date(), -5));
        return userService.countNotActive(shopId, begin, end);
    }
}
