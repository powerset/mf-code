package mf.code.api.seller.service.checkMerchantOrderAmount.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.api.seller.service.checkMerchantOrderAmount.CheckCreateAmountConverter;
import mf.code.merchant.constants.BizTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.api.seller.service.checkMerchantOrderAmount.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 11:39
 */
@Service
public class CreateDefConverter implements CheckCreateAmountConverter {
    @Autowired
    private ActivityDefAuditService activityDefAuditService;

    @Override
    public boolean convert(MerchantOrderReq dto) {
        if (BizTypeEnum.isCreateDefMerchantOrder(dto.getBizType())) {
            //def编号
            QueryWrapper<ActivityDefAudit> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityDefAudit::getActivityDefId, dto.getBizValue())
                    .orderByDesc(ActivityDefAudit::getCtime)
            ;
            List<ActivityDefAudit> activityDefAudits = this.activityDefAuditService.list(wrapper);
            if (!CollectionUtils.isEmpty(activityDefAudits) && activityDefAudits.get(0).getDepositApply().compareTo(new BigDecimal(dto.getAmount())) == 0) {
                return true;
            }
        }
        return false;
    }
}
