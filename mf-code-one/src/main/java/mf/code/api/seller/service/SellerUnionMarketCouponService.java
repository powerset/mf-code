package mf.code.api.seller.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.one.dto.SellerMerchantShopCouponDTO;
import mf.code.one.dto.SellerShopCouponDTO;
import mf.code.one.dto.UserReceivedCouponDTO;

import java.util.List;

/**
 * 商户端-联合营销-优惠券 service
 * @author yunshan
 */
public interface SellerUnionMarketCouponService {
    /**
     * 查询所有已经创建的优惠券
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    List<SellerMerchantShopCouponDTO> queryAllCoupons(Long shopId, Long page, Long size);

    /**
     * 查询所有已经创建的优惠券--总数
     * @param shopId
     * @return
     */
    int countAllCoupons(Long shopId);

    /**
     * 添加优惠券
     * @param sellerShopCouponDTO
     * @return
     */
    SimpleResponse addNewCoupons(SellerShopCouponDTO sellerShopCouponDTO);

    /**
     * 查询领取情况
     *
     * @param shopId
     * @param couponId
     * @param num
     * @param size
     * @return
     */
    List<UserReceivedCouponDTO> queryUserCouponsList(Long shopId, Long couponId, Long num, Long size);

    /**
     * 优惠券已经被领取的数量
     * @param shopId
     * @param couponId
     * @return
     */
    int countCouponsInfo(Long shopId, Long couponId);

    /**
     * 优惠券已经被使用的数量
     * @param shopId
     * @param couponId
     * @return
     */
    int countCouponsInfoUsed(Long shopId, Long couponId);

    /**
     * 结束发放
     * @param shopId
     * @param couponId
     * @return
     */
    SimpleResponse finishIssued(Long shopId, Long couponId);

    /**
     * 查询已经选择的商品
     * @param couponId
     * @return
     */
    MerchantShopCoupon getCheckedGood(Long couponId);

    /**
     * 修改创建的优惠券信息
     * @param coupon
     * @return
     */
    SimpleResponse updateNewCouponsInfo(SellerShopCouponDTO coupon);

    /**
     * 删除已经创建的优惠券
     * @param couponId
     * @return
     */
    SimpleResponse deleteNewCouponById(Long couponId);

    /**
     * 根据优惠券id查询优惠券信息
     * @param shopId
     * @param couponId
     * @return
     */
    MerchantShopCoupon findCouponById(Long shopId, Long couponId);
}
