package mf.code.api.seller;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.category.repo.po.CategoryShop;
import mf.code.category.repo.po.CategoryTaobao;
import mf.code.category.service.CategoryService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller
 * Description: 品类
 *
 * @author: gel
 * @date: 2018-10-26 16:37
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/seller/category")
public class SellerCategoryApi {

    @Autowired
    private CategoryService categoryService;
    /**
     * 请求品类
     * @return
     */
    @GetMapping("/getCategory")
    public SimpleResponse getCategory(){
        SimpleResponse simpleResponse = new SimpleResponse();
        List<CategoryTaobao> categoryTaobaoList = categoryService.selectCategoryLevelOne();
        simpleResponse.setData(categoryTaobaoList);
        return simpleResponse;
    }

    /**
     * 请求店铺类目
     * @return
     */
    @GetMapping("/getShopCategory")
    public SimpleResponse getShopCategory(){
        SimpleResponse simpleResponse = new SimpleResponse();
        List<CategoryShop> categoryList = categoryService.selectCategoryShopList();
        simpleResponse.setData(categoryList);
        return simpleResponse;
    }

    @GetMapping("/aa")
    public SimpleResponse aa(@RequestParam(value = "string") String string,
                             @RequestParam(value = "token", required = false) String token){
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(string + "--" + token);
        return simpleResponse;
    }
    @PostMapping("/bb")
    public SimpleResponse bb(@RequestBody Map<String, Object> map){
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(map);
        return simpleResponse;
    }

}
