package mf.code.api.seller.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.enums.OrderPayTypeEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * mf.code.api.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月17日 19:56
 */
@Data
public class MerchantOrderReq {
    public static final Integer plat_pay_pc = 1;
    public static final Integer palat_pay_mobile_android = 2;
    public static final Integer palat_pay_mobile_ios = 3;
    public static final Integer palat_pay_mobile_wap = 3;

    //扫码付款方式|移动端支付(h5)
    public static final Integer payment_wx = 1;
    //预存金付款方式
    public static final Integer payment_prestore = 2;
    //充值
    public static final Integer prestore = 3;

    //支付保证金
    public static final Integer trade_type_pay = 1;
    //保证金结算退回
    public static final Integer trade_type_refund = 2;
    //补库存
    public static final Integer trade_type_addStock = 3;

    /***
     * 支付平台 1：pc 2：安卓 3：ios 4:wap网站
     */
    private Integer payPlatform = 1;

    /***
     * 用户ip
     */
    private String userIp;

    /***
     * 金额 单位元
     */
    private String amount;

    /***
     * 商户编号
     */
    private Long merchantId;

    /***
     * 店铺编号(非必填)
     */
    private Long shopId = 0L;

    /***
     * 支付方式
     */
    private Integer payType;

    /***
     * 交易方式
     */
    private Integer tradeType;

    /***
     * 员工编号 后端自己填充
     */
    private Long customerId;

    /*************************************/
    /***
     * 1:活动计划编号，2:活动定义编号，3:活动定义编号，4：活动定义编号，5:活动定义编号，6：广告位的主键编号,7:活动定义审核主键编号',8:"活动定义编号",9:粉多多,10, "bizVersion",11, "bizVersion",12,"bizVersion" 14：defId 15：defId 16：defId
     */
    private long bizValue;
    /***
     * 1：计划内活动，2：赠品活动，3:轮盘抽奖，4:订单抽奖活动，5:红包活动，6.广告,7.活动定义审核',8:"拆红包",9:粉多多,10, "购买集客魔方公有版",11, "购买集客魔方私有版",12, "升级集客魔方私有版" 14：收藏加购 15：好评晒图 16：回填订单
     */
    private Integer bizType;

    /***
     * 广告主键编号
     */
    private Long activityAdId;


    public void from(MerchantOrder merchantOrder) {
        this.merchantId = merchantOrder.getMerchantId();
        this.shopId = merchantOrder.getShopId();
        this.amount = merchantOrder.getTotalFee().toString();
        this.payType = merchantOrder.getPayType();
        if (merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()) {
            if (merchantOrder.getPayType() == OrderPayTypeEnum.CASH.getCode()) {
                this.payType = payment_prestore;
            } else {
                this.payType = payment_wx;
            }
        } else if (merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()) {
            this.payType = prestore;
        }
        if (StringUtils.isNotBlank(merchantOrder.getCustomerJson())) {
            List<CustomerJsonVo> customerJsonVos = JSONObject.parseArray(merchantOrder.getCustomerJson(), CustomerJsonVo.class);
            if (customerJsonVos != null && customerJsonVos.size() > 0) {
                for (CustomerJsonVo customerJsonVo : customerJsonVos) {
                    if (customerJsonVo.isStart()) {
                        this.customerId = customerJsonVo.getCustomerId();
                        break;
                    }
                }
            }
        }
        if (merchantOrder.getBizType() == BizTypeEnum.PLAN_ACTIVITY.getCode()) {
            this.bizValue = merchantOrder.getBizValue();
            this.bizType = BizTypeEnum.PLAN_ACTIVITY.getCode();
        } else if (merchantOrder.getBizType() == BizTypeEnum.NEWMAN_ORDER_ACTIVITY.getCode()) {
            this.bizValue = merchantOrder.getBizValue();
            this.bizType = BizTypeEnum.NEWMAN_ORDER_ACTIVITY.getCode();
        } else if (merchantOrder.getBizType() == BizTypeEnum.AD.getCode()) {
            this.activityAdId = merchantOrder.getBizValue();
            this.bizType = BizTypeEnum.AD.getCode();
        }
    }
}
