package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 18:06
 */
@Service
@Slf4j
public class FillOrderBackV2TaskBalanceOfClearingProgressConverterImpl extends BalanceOfClearingProgressService {
    @Autowired
    private UserTaskService userTaskService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean fillOrderBackV2Type = activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode());
        if (fillOrderBackV2Type) {
            //获取活动绑定金额
            QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
            userTaskWrapper.lambda()
                    .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                    .eq(UserTask::getShopId, activityDef.getShopId())
                    .eq(UserTask::getActivityDefId, activityDef.getId())
                    .eq(UserTask::getType, UserTaskTypeEnum.ORDER_REDPACK_V2.getCode())
                    .in(UserTask::getStatus, Arrays.asList(
                            UserTaskStatusEnum.AUDIT_WAIT.getCode()
                    ))
                    .orderByDesc(UserTask::getId)
            ;
            List<UserTask> userTasks = this.userTaskService.list(userTaskWrapper);
            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.from(activityDef, userTasks);
            return dto;
        }
        return null;
    }
}
