package mf.code.api.seller;

import mf.code.api.seller.dto.ApplyRefundDto;
import mf.code.api.seller.dto.MerchantOrderMktTransferReq;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.api.seller.service.DonateJkmfService;
import mf.code.api.seller.service.SellerMerchantOrderService;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.seller
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月17日 19:49
 */
@RestController
@RequestMapping("/api/seller/merchantOrder")
public class SellerMerchantOrderApi {
    @Autowired
    private SellerMerchantOrderService sellerMerchantOrderService;
    @Autowired
    private DonateJkmfService donateJkmfService;
    @Autowired
    private CommonDictService commonDictService;

    /***
     * 购买st系列-全网通-赠送集客魔方使用
     *
     * @param phone
     * @return
     */
    @GetMapping("donateJkmf")
    public SimpleResponse donateJkmf(@RequestParam("phone") String phone, HttpServletRequest request, HttpServletResponse response) {
        return donateJkmfService.donateJkmf(phone, request, response);
    }

    /***
     * 创建商户订单
     * @param req
     * @param request
     * @return
     */
    @RequestMapping(path = "/creatMerchantOrder", method = RequestMethod.POST)
    public SimpleResponse creatMerchantOrder(@RequestBody MerchantOrderReq req, HttpServletRequest request) {
        return this.sellerMerchantOrderService.creatMerchantOrder(req, request);
    }

    /***
     * 获取支付方式
     * @param merchantId
     * @return
     * <pre>
     *     paychannel: 1:扫码支付
     *                 2:移动端h5支付
     * </pre>
     */
    @RequestMapping(path = "/getMerchantOrderPayChannel", method = RequestMethod.GET)
    public SimpleResponse getMerchantOrderPayChannel(@RequestParam(name = "merchantId") final Long merchantId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map map = new HashMap<>();
//        CommonDict commonDict = commonDictService.selectByTypeKey("mobile_pay", "paychneel");
//        if (commonDict == null || StringUtils.isBlank(commonDict.getValue())) {
//            map.put("paychannel", 1);
//            simpleResponse.setData(map);
//            return simpleResponse;
//        }
//
//        List<String> merchantIds = Arrays.asList(commonDict.getValue());
//        if (!CollectionUtils.isEmpty(merchantIds)) {
//            for (String str : merchantIds) {
//                if (str.indexOf(String.valueOf(merchantId)) != -1) {
//                    map.put("paychannel", 2);
//                    simpleResponse.setData(map);
//                    return simpleResponse;
//                }
//            }
//        }
        map.put("paychannel", 2);
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 查询商户订单状态
     * @param orderKeyID
     * @return
     */
    @RequestMapping(path = "/queryMerchantOrderStatus", method = RequestMethod.GET)
    public SimpleResponse queryMerchantOrderStatus(@RequestParam(name = "orderId") final Long orderKeyID) {
        return this.sellerMerchantOrderService.queryMerchantOrderStatus(orderKeyID);
    }

    /**
     * 微信支付回调
     *
     * @param request
     * @return
     */
    @RequestMapping(path = "/wx/pay/callback", method = RequestMethod.POST)
    public String wxPayCallback(HttpServletRequest request) {
        return this.sellerMerchantOrderService.wxPayCallback(request);
    }

    /**
     * 微信  退款回调
     *
     * @param request
     * @return
     */
    @RequestMapping(path = "/wx/refund/callback", method = RequestMethod.POST)
    public String wxRefundCallback(HttpServletRequest request) {
        return this.sellerMerchantOrderService.wxRefundCallback(request);
    }

    /***
     * 商户提现-企业付款(公众号支付)
     * @param req
     * @return
     */
    @RequestMapping(path = "/createMktTransfersMerchantOrder", method = RequestMethod.POST)
    public SimpleResponse createMktTransfersMerchantOrder(@RequestBody MerchantOrderMktTransferReq req, HttpServletRequest request) {
        return this.sellerMerchantOrderService.createMktTransfersMerchantOrder(req, request);
    }

    /***
     * 查询商户财务统计-除广告
     * @param merchantId
     * @param shopId
     * @param start
     * @param end
     * @param type
     * @return
     */
    @RequestMapping(path = "/queryRecord", method = RequestMethod.GET)
    public SimpleResponse queryRecord(@RequestParam(name = "merchantId") Long merchantId,
                                      @RequestParam(name = "shopId") Long shopId,
                                      @RequestParam(name = "start", required = false) String start,
                                      @RequestParam(name = "end", required = false) String end,
                                      @RequestParam(name = "size", defaultValue = "10") Integer limit,
                                      @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                      @RequestParam(name = "type", defaultValue = "0") int type) {
        return this.sellerMerchantOrderService.queryRecord(merchantId, shopId, start, end, limit, pageNum, type);
    }

    /***
     * 查询清算活动余额进度情况
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    @RequestMapping(path = "/queryBalanceOfClearingProgressInfo", method = RequestMethod.GET)
    public SimpleResponse queryBalanceOfClearingProgressInfo(@RequestParam(name = "merchantId") Long merchantId,
                                                             @RequestParam(name = "shopId") Long shopId,
                                                             @RequestParam(name = "activityDefId") Long activityDefId) {
        return this.sellerMerchantOrderService.queryBalanceOfClearingProgressInfo(merchantId, shopId, activityDefId);
    }

    /***
     * 申请结算退款
     * @param applyRefundDto
     * @return
     */
    @RequestMapping(path = "/applyRefund", method = RequestMethod.POST)
    public SimpleResponse applyRefund(@RequestBody ApplyRefundDto applyRefundDto) {
        return this.sellerMerchantOrderService.applyRefund(applyRefundDto);
    }

    /***
     * 查询结算退款状态
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @return
     */
    @RequestMapping(path = "/queryApplyRefundStatus", method = RequestMethod.GET)
    public SimpleResponse queryApplyRefundStatus(@RequestParam(name = "merchantId") Long merchantId,
                                                 @RequestParam(name = "shopId") Long shopId,
                                                 @RequestParam(name = "activityDefId") Long activityDefId) {
        return this.sellerMerchantOrderService.queryApplyRefundStatus(merchantId, shopId, activityDefId);
    }

    /***
     * 查询商户余额信息
     * @param merchantId
     * @param shopId
     * @return
     */
    @RequestMapping(path = "/queryMerchantBalanceInfo", method = RequestMethod.GET)
    public SimpleResponse queryMerchantBalanceInfo(@RequestParam(name = "merchantId") Long merchantId,
                                                   @RequestParam(name = "shopId", required = false, defaultValue = "0") Long shopId) {
        return this.sellerMerchantOrderService.queryMerchantBalanceInfo(merchantId, shopId);
    }

    /***
     * 获取购买东西的信息
     * @param merchantId
     * @param type 1：购买小程序
     * @return
     */
    @RequestMapping(path = "/queryInfo2buy", method = RequestMethod.GET)
    public SimpleResponse queryInfo2buy(@RequestParam(name = "merchantId") final Long merchantId,
                                        @RequestParam(name = "type") final int type) {
        return this.sellerMerchantOrderService.queryInfo2buy(merchantId, type);
    }
}
