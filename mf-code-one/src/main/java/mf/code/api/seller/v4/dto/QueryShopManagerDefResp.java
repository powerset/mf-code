package mf.code.api.seller.v4.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.api.seller.v4.dto
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-20 14:08
 */
@Data
public class QueryShopManagerDefResp {

    /**
     * 主活动id
     */
    private Long id;

    /**
     * 预计店长任务参与人数
     */
    private Integer stockDef;

    /**
     * 设置微信群任务佣金（店长任务专用）
     */
    private BigDecimal setWechatAmount;

    /**
     * 邀请下级粉丝人数（店长任务专用）
     */
    private Integer inviteCount;

    /**
     * 完成邀请下级粉丝任务佣金（店长任务专用）
     */
    private BigDecimal inviteAmount;

    /**
     * 发展粉丝成为店长人数（店长任务专用）
     */
    private Integer developCount;

    /**
     * 完成发展粉丝为店长任务佣金（店长任务专用）
     */
    private BigDecimal developAmount;

    /**
     * 活动保证金
     */
    private BigDecimal depositDef;
}
