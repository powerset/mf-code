package mf.code.api.seller.service.wxPayCallbackBus.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.common.dto.PupOrderReq;
import mf.code.api.common.service.PubUserOrderService;
import mf.code.api.seller.service.wxPayCallbackBus.WxPayCallbackBusConverter;
import mf.code.common.caller.wxpublic.WeixinPublicHelper;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.MerchantInviteTemp;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantInviteTempService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * mf.code.api.seller.service.wxPayCallbackBus.impl
 *
 * @description: 公众号购买
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月08日 10:57
 */
@Service
@Slf4j
public class PubBuyCourseCallbackConverter implements WxPayCallbackBusConverter {
    @Autowired
    private MerchantInviteTempService merchantInviteTempService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private PubUserOrderService pubUserOrderService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WeixinPublicHelper weixinPublicHelper;

    @Override
    public String convert(MerchantOrder merchantOrder) {
        if (merchantOrder.getBizType() == BizTypeEnum.PUBLIC_BUY_COURSE.getCode()) {
            MerchantInviteTemp merchantInviteTemp = this.merchantInviteTempService.getById(merchantOrder.getBizValue());
            if (merchantInviteTemp == null) {
                return ApiStatusEnum.SUCCESS.getMessage();
            }
            //支付成功去除redis
            PupOrderReq req = new PupOrderReq();
            req.setPubOpenId(merchantInviteTemp.getOpenid());
            String reidsKey = this.pubUserOrderService.getMerchantOrderRedisKey(req);
            this.stringRedisTemplate.delete(reidsKey);
            // 推送购买成功消息
            weixinPublicHelper.sendPaySuccessMsg(merchantInviteTemp.getOpenid());
            //判断是否有上级
            if (merchantInviteTemp.getParentId() == null || merchantInviteTemp.getParentId() == 0 ||
                    StringUtils.isBlank(merchantInviteTemp.getParentOpenid())) {
                log.info("公众号购买返现-无上级,结束！");
                return ApiStatusEnum.SUCCESS.getMessage();
            }
            CommonDict commonDict = this.commonDictService.selectByTypeKey("weixinPublic", "pay_menu");
            if (commonDict == null) {
                log.info("公众号购买返现-订单列表找不到，结束");
                return ApiStatusEnum.SUCCESS.getMessage();
            }
            //获取返现金额
            Map<String, Object> jsonMap = JSONObject.parseObject(commonDict.getValue(), Map.class);
            if (jsonMap == null || jsonMap.get("backMoney") == null) {
                log.info("公众号购买返现-返现金额不存在，结束");
                return ApiStatusEnum.SUCCESS.getMessage();
            }

            //获取上级的openId
            String parentOpenId = merchantInviteTemp.getParentOpenid();
            BigDecimal amount = new BigDecimal(jsonMap.get("backMoney").toString());

            //给该用户进行提现
            this.pubUserOrderService.createMktTransfersOrder(merchantInviteTemp.getParentId(), parentOpenId,
                    merchantInviteTemp.getNickname(), amount);
            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }
}
