package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.ActivityStatusEnum;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月22日 18:06
 */
@Service
@Slf4j
public class OpenRedPackBalanceOfClearingProgressConverterImpl extends BalanceOfClearingProgressService {
    @Autowired
    private ActivityService activityService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean openRedPackType = activityDef.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        if (openRedPackType) {
            //查询活动占用数目
            Map<String, Object> bindingParams = new HashMap<>();
            bindingParams.put("merchantId", activityDef.getMerchantId());
            bindingParams.put("shopId", activityDef.getShopId());
            bindingParams.put("activityDefId", activityDef.getId());
            bindingParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
            bindingParams.put("awardStartTimeIsNull", true);
            int bindingNum = this.activityService.countActivity(bindingParams);
            //查询活动消耗数目
            Map<String, Object> costParams = new HashMap<>();
            costParams.put("merchantId", activityDef.getMerchantId());
            costParams.put("shopId", activityDef.getShopId());
            costParams.put("activityDefId", activityDef.getId());
            costParams.put("status", ActivityStatusEnum.END.getCode());
            int costNum = this.activityService.countActivity(costParams);
            //拆红包成功-但是还没解锁的数目-对于商家来说,钱已出去
            Map<String, Object> costParamsBinding = new HashMap<>();
            costParamsBinding.put("merchantId", activityDef.getMerchantId());
            costParamsBinding.put("shopId", activityDef.getShopId());
            costParamsBinding.put("activityDefId", activityDef.getId());
            costParamsBinding.put("status", ActivityStatusEnum.PUBLISHED.getCode());
            costParamsBinding.put("awardStartTimeIsNotNull", true);
            costNum = costNum + this.activityService.countActivity(costParamsBinding);
            //取最近的一条数据
            Activity activity = null;
            Page<Activity> page = new Page<>(1, 1);
            QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
            activityWrapper.lambda()
                    .eq(Activity::getMerchantId, activityDef.getMerchantId())
                    .eq(Activity::getShopId, activityDef.getShopId())
                    .eq(Activity::getActivityDefId, activityDef.getId())
                    .in(Activity::getStatus, Arrays.asList(
                            ActivityStatusEnum.PUBLISHED.getCode(),
                            ActivityStatusEnum.END.getCode()
                    ))
                    .orderByDesc(Activity::getId)
            ;
            IPage<Activity> activityIPage = this.activityService.page(page, activityWrapper);
            if (!CollectionUtils.isEmpty(activityIPage.getRecords())) {
                activity = activityIPage.getRecords().get(0);
            }

            BigDecimal sum = activityDef.getDepositDef();
            BigDecimal remain = activityDef.getDeposit();
            BigDecimal binding = this.getMoney(activityDef, bindingNum);
            BigDecimal cost = this.getMoney(activityDef, costNum);
            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.from(activityDef, sum, cost, remain, binding, bindingNum, 0);
            if (activity != null) {
                dto.setCutoffTime(DateUtils.addHours(activity.getEndTime(), 24).getTime());
                dto.setCutoffDate(DateUtils.addHours(activity.getEndTime(), 24));
            }
            return dto;
        }
        return null;
    }

    private BigDecimal getMoney(ActivityDef activityDef, int num) {
        BigDecimal money = activityDef.getGoodsPrice() != null ? activityDef.getGoodsPrice() : activityDef.getCommission();
        return money.multiply(new BigDecimal(String.valueOf(num)));
    }
}
