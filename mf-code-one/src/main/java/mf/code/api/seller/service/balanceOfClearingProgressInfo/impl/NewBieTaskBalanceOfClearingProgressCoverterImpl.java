package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.service.UserActivityService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;


/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-25 14:02
 */
@Service
@Slf4j
public class NewBieTaskBalanceOfClearingProgressCoverterImpl extends BalanceOfClearingProgressService {

    @Autowired
    private ActivityDefService activityDefService;

    @Autowired
    private UserCouponService userCouponService;

    @Autowired
    private UserActivityService userActivityService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        if (activityDef == null) {
            return null;
        }
        boolean newBieType = activityDef.getType().equals(ActivityDefTypeEnum.NEWBIE_TASK.getCode());

        if (newBieType) {
            // 查询子活动阅读任务
            ActivityDef readTaskActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                    ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode());
            if (readTaskActivityDef == null) {
                log.error("子活动阅读任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
                return null;
            }

            // 查询子活动扫一扫任务
            ActivityDef scanActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                    ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode());
            if (scanActivityDef == null) {
                log.error("子活动扫一扫任务不存在，主活动id={},@to 大妖怪", activityDef.getId());
                return null;
            }

            // 查询子活动拆红包任务
            ActivityDef openRedPackActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                    ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
            if (openRedPackActivityDef == null) {
                log.error("子活动抢红包 任务不存在，主活动id={},@to 大妖怪", activityDef.getId());
                return null;
            }

            // 消耗金额
            BigDecimal deposit = BigDecimal.ZERO;

            // 查询阅读任务奖励
            List<UserCoupon> readTaskUserCoupons = userCouponService.list(
                    new QueryWrapper<UserCoupon>()
                            .lambda()
                            .eq(UserCoupon::getActivityDefId, readTaskActivityDef.getId())
                            .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode())
            );

            deposit = deposit.add(new BigDecimal(readTaskUserCoupons.size()).multiply(readTaskActivityDef.getCommission()));

            List<UserCoupon> scanTaskUserCoupons = userCouponService.list(
                    new QueryWrapper<UserCoupon>()
                            .lambda()
                            .eq(UserCoupon::getActivityDefId, scanActivityDef.getId())
                            .eq(UserCoupon::getType, UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode())
            );

            deposit = deposit.add(new BigDecimal(scanTaskUserCoupons.size()).multiply(scanActivityDef.getCommission()));

            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();

            List<UserActivity> userActivities = userActivityService.list(
                    new QueryWrapper<UserActivity>()
                            .lambda()
                            .eq(UserActivity::getType, UserActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode())
                            .eq(UserActivity::getActivityDefId, openRedPackActivityDef.getId())
                            .eq(UserActivity::getStatus, UserActivityStatusEnum.RED_PACKAGE_ING.getCode())
            );

            if (CollectionUtils.isNotEmpty(userActivities)) {
                dto.setStatus(BalanceOfClearingProgressDto.AuditTaskActivity);
            } else {
                dto.setStatus(BalanceOfClearingProgressDto.CanCash);
            }

            // 计划金额
            dto.setSumPayMoney(activityDef.getDepositDef().setScale(2,  BigDecimal.ROUND_DOWN).toString());
            // 消耗金额
            dto.setCostMoney(deposit.setScale(2,  BigDecimal.ROUND_DOWN).toString());
            // 绑定金额
            dto.setBindingMoney(BigDecimal.ZERO.toString());
            // 剩余金额
            dto.setRemainMoney(activityDef.getDepositDef().subtract(deposit).setScale(2,  BigDecimal.ROUND_DOWN).toString());
            dto.setAuditNum(0);
            dto.setActivityDefId(activityDef.getId());

            return dto;
        }

        return null;
    }
}
