package mf.code.api.seller.service.wxPayCallbackBus.impl;

import mf.code.activity.service.ActivityDefCheckService;
import mf.code.api.seller.service.wxPayCallbackBus.WxPayCallbackBusConverter;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.seller.service.wxPayCallbackBus.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 13:50
 */
@Service
public class CreateDefCallbackConverter implements WxPayCallbackBusConverter {
    @Autowired
    private ActivityDefCheckService activityDefCheckService;

    @Override
    public String convert(MerchantOrder merchantOrder) {
        if (BizTypeEnum.isCreateDefMerchantOrder(merchantOrder.getBizType())) {
            //创建活动定义
            this.activityDefCheckService.pay2start(merchantOrder.getBizValue());
            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }
}
