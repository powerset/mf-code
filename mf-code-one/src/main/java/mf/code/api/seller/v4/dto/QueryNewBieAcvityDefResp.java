package mf.code.api.seller.v4.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.api.seller.v4.dto
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-20 13:48
 */
@Data
public class QueryNewBieAcvityDefResp {

    /**
     * 主活动id
     */
    private Long id;

    /**
     * 计划拉新人数
     */
    private Integer stockDef;

    /**
     * 阅读新手任务 金额
     */
    private BigDecimal readAmount;

    /**
     * 客服微信信息（非必填）;json存储格式:[{"wechat":"","pic":"https://asset.wxyundian.com/data-test3/activity/cspic/wechat/WechatIMG9.jpeg"}]
     */
    private String serviceWx;

    /**
     * 群口令
     */
    private String keywords;

    /**
     * 扫一扫 金额
     */
    private BigDecimal scanAmount;

    /**
     * 拆红包活动id
     */
    private Long openRedPackactiveId;

    /**
     * 拆红包活动库存
     */
    private Integer openRedPackActivitySock;

    /**
     * 活动保证金
     */
    private BigDecimal depositDef;
}
