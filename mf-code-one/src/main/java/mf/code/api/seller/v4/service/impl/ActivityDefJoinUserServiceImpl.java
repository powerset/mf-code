package mf.code.api.seller.v4.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityService;
import mf.code.api.seller.v4.dto.QueryActivityDefUserJoinResp;
import mf.code.api.seller.v4.service.ActivityDefAboutService;
import mf.code.api.seller.v4.service.ActivityDefJoinUserService;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.MybatisPage;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.v4.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月22日 19:42
 */
@Service
@Slf4j
public class ActivityDefJoinUserServiceImpl implements ActivityDefJoinUserService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefAboutService activityDefAboutService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserPubJoinService userPubJoinService;

    @Override
    public SimpleResponse assistOrOpenRedPackUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef) {
        QueryActivityDefUserJoinResp resp = new QueryActivityDefUserJoinResp();
        resp.fromActivityDef(activityDef, 0, 0, 0);
        SimpleResponse<Object> response = new SimpleResponse<>();
        Long activityDefId = activityDef.getId();
        // 赠品 统计页
        Page<Activity> page = new Page<>(pageNum, size);
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.lambda().eq(Activity::getActivityDefId, activityDefId);
        int total = this.activityService.count(aWrapper);
        IPage<Activity> iPage = activityService.page(page, aWrapper);
        List<Activity> activities = iPage.getRecords();
        if (CollectionUtils.isEmpty(activities)) {
            response.setData(resp);
            return response;
        }
        //获取用户信息
        List<Long> allUserIdList = new ArrayList<>();
        List<Long> activityIdList = new ArrayList<>();
        for (Activity activity : activities) {
            if (activity.getUserId() != null && activity.getUserId() > 0 && allUserIdList.indexOf(activity.getUserId()) == -1) {
                allUserIdList.add(activity.getUserId());
            }
            activityIdList.add(activity.getId());
        }
        if (CollectionUtils.isEmpty(allUserIdList)) {
            response.setData(resp);
            return response;
        }
        Map<Long, User> userMap = this.getUserMap(allUserIdList);
        //获取中奖数目
        QueryWrapper<UserTask> taskQueryWrapper = new QueryWrapper<>();
        taskQueryWrapper.lambda()
                .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                .eq(UserTask::getShopId, activityDef.getShopId())
                .eq(UserTask::getActivityDefId, activityDef.getId())
                .in(UserTask::getType, Arrays.asList(UserTaskTypeEnum.ASSIST_START.getCode(),
                        UserTaskTypeEnum.ASSIST_START_V2.getCode()))
        ;
        int countAwardNum = this.userTaskService.count(taskQueryWrapper);

        //获取返现数目
        QueryWrapper<UserCoupon> couponWrapper = new QueryWrapper<>();
        couponWrapper.lambda()
                .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                .eq(UserCoupon::getShopId, activityDef.getShopId())
                .eq(UserCoupon::getActivityDefId, activityDef.getId())
                .groupBy(UserCoupon::getUserId)
        ;
        List<UserCoupon> countUserCoupon = this.userCouponService.list(couponWrapper);
        int countMoneyBackNum = 0;
        if (!CollectionUtils.isEmpty(countUserCoupon)) {
            countMoneyBackNum = countUserCoupon.size();
        }

        int joinNum = this.activityDefAboutService.getUserJoinNumByActivityDef(activityDef);
        resp.fromActivityDef(activityDef, joinNum, countAwardNum, countMoneyBackNum);

        //获取中奖情况
        taskQueryWrapper = new QueryWrapper<>();
        taskQueryWrapper.lambda()
                .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                .eq(UserTask::getShopId, activityDef.getShopId())
                .eq(UserTask::getActivityDefId, activityDef.getId())
                .in(UserTask::getActivityId, activityIdList)
                .in(UserTask::getType, Arrays.asList(UserTaskTypeEnum.ASSIST_START.getCode(), UserTaskTypeEnum.ASSIST_START_V2.getCode()))
        ;
        List<UserTask> userTasks = this.userTaskService.list(taskQueryWrapper);
        Map<Long, UserTask> userTaskMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userTasks)) {
            for (UserTask userTask : userTasks) {
                userTaskMap.put(userTask.getActivityId(), userTask);
            }
        }
        //获取返现情况
        couponWrapper = new QueryWrapper<>();
        couponWrapper.lambda()
                .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                .eq(UserCoupon::getShopId, activityDef.getShopId())
                .eq(UserCoupon::getActivityDefId, activityDef.getId())
                .in(UserCoupon::getActivityId, activityIdList)
                .groupBy(UserCoupon::getUserId)
        ;
        List<UserCoupon> userCoupons = this.userCouponService.list(couponWrapper);
        Map<Long, UserCoupon> userCouponMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userCoupons)) {
            for (UserCoupon userCoupon : userCoupons) {
                userCouponMap.put(userCoupon.getActivityId(), userCoupon);
            }
        }

        resp.getDetails().setContent(new ArrayList<>());
        resp.getDetails().from(pageNum, size, total);
        for (Activity activity : activities) {
            QueryActivityDefUserJoinResp.Detail detail = new QueryActivityDefUserJoinResp.Detail();
            UserTask userTask = userTaskMap.get(activity.getId());
            UserCoupon userCoupon = userCouponMap.get(activity.getId());
            User user = userMap.get(activity.getUserId());
            if (null == user) {
                continue;
            }
            if (StringUtils.isNotBlank(user.getMobile())) {
                detail.setPhone(user.getMobile());
            }
            if (userTask != null) {
                detail.setAwardInfo("已中奖");
            }
            if (userCoupon != null) {
                detail.setBackAmountInfo("已返现");
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            detail.fromActivity(activity, userInfo);
            resp.getDetails().getContent().add(detail);
        }
        response.setData(resp);
        return response;
    }

    @Override
    public SimpleResponse luckyWheelUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        QueryActivityDefUserJoinResp resp = new QueryActivityDefUserJoinResp();
        resp.fromActivityDef(activityDef, 0, 0, 0);
        Long activityDefId = activityDef.getId();
        // 幸运大转盘 统计页
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.lambda().eq(Activity::getActivityDefId, activityDefId);
        List<Activity> activities = this.activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            response.setData(resp);
            return response;
        }

        // 获取对应活动id的集合
        List<Long> activityIdList = new ArrayList<>();
        for (Activity activity : activities) {
            activityIdList.add(activity.getId());
        }

        int joinNum = this.activityDefAboutService.getUserJoinNumByActivityDef(activityDef);
        resp.fromActivityDef(activityDef, joinNum, joinNum, joinNum);

        QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
        ucWrapper.lambda()
                .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                .eq(UserCoupon::getShopId, activityDef.getShopId())
                .eq(UserCoupon::getActivityDefId, activityDefId)
                .orderByDesc(UserCoupon::getId)
        ;

        int total = this.userCouponService.count(ucWrapper);
        Page<UserCoupon> page = new Page<>(pageNum, size);
        IPage<UserCoupon> iPage = userCouponService.page(page, ucWrapper);
        List<UserCoupon> userCoupons = iPage.getRecords();
        if (CollectionUtils.isEmpty(userCoupons)) {
            response.setData(resp);
            return response;
        }

        List<Long> allUserIdList = new ArrayList<>();
        for (UserCoupon userCoupon : userCoupons) {
            if (userCoupon.getUserId() != null && userCoupon.getUserId() > 0 && allUserIdList.indexOf(userCoupon.getUserId()) == -1) {
                allUserIdList.add(userCoupon.getUserId());
            }
        }
        // 获取此任务的所有用户信息.
        Map<Long, User> userMap = this.getUserMap(allUserIdList);

        resp.getDetails().setContent(new ArrayList<>());
        resp.getDetails().from(pageNum, size, total);
        for (UserCoupon userCoupon : userCoupons) {
            QueryActivityDefUserJoinResp.Detail detail = new QueryActivityDefUserJoinResp.Detail();
            User user = userMap.get(userCoupon.getUserId());
            if (null == user) {
                continue;
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());

            if (StringUtils.isNotBlank(user.getMobile())) {
                detail.setPhone(user.getMobile());
            }
            detail.setAwardInfo("已中奖");
            detail.setBackAmountInfo("已返现");
            detail.setJoinTime(userCoupon.getCtime());
            detail.setJoinUser(userInfo);
            resp.getDetails().getContent().add(detail);
        }
        response.setData(resp);
        return response;
    }

    @Override
    public SimpleResponse orderBackUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        QueryActivityDefUserJoinResp resp = new QueryActivityDefUserJoinResp();
        resp.fromActivityDef(activityDef, 0, 0, 0);
        Long activityDefId = activityDef.getId();
        // 获取机器人
        List<User> robots = userService.listRobot();
        // 免单抽奖 统计页
        List<Long> robotIds = new ArrayList<>();
        for (User user : robots) {
            robotIds.add(user.getId());
        }

        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.eq("activity_def_id", activityDefId);
        List<Activity> activities = activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            response.setData(resp);
            return response;
        }

        // 获取对应活动id的集合
        List<Long> activityIdList = new ArrayList<>();
        for (Activity activity : activities) {
            activityIdList.add(activity.getId());
        }

        List<Integer> statusList = new ArrayList<>();
        statusList.add(UserActivityStatusEnum.AWARD_EXPIRE.getCode());
        statusList.add(UserActivityStatusEnum.NO_WIN.getCode());
        statusList.add(UserActivityStatusEnum.WINNER.getCode());
        statusList.add(UserActivityStatusEnum.COUPON.getCode());

        //返现人数
        QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
        ucWrapper.lambda()
                .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                .eq(UserCoupon::getShopId, activityDef.getShopId())
                .eq(UserCoupon::getActivityDefId, activityDefId)
                .isNull(UserCoupon::getCommissionDef)
                .orderByDesc(UserCoupon::getId)
        ;
        List<UserCoupon> userCoupons = this.userCouponService.list(ucWrapper);
        int awardCashBackNum = userCoupons.size();
        Map<Long, UserCoupon> userCouponMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userCoupons)) {
            for (UserCoupon userCoupon : userCoupons) {
                userCouponMap.put(userCoupon.getUserId(), userCoupon);
            }
        }

        //中奖人数
        QueryWrapper<UserTask> taskQueryWrapper = new QueryWrapper<>();
        taskQueryWrapper.lambda()
                .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                .eq(UserTask::getShopId, activityDef.getShopId())
                .in(UserTask::getActivityId, activityIdList)
                .in(UserTask::getType, Arrays.asList(UserTaskTypeEnum.ORDER_BACK_START.getCode(),
                        UserTaskTypeEnum.ORDER_BACK_JOIN.getCode(),
                        UserTaskTypeEnum.ORDER_BACK_START_V2.getCode(),
                        UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode()))
        ;
        List<UserTask> userTasks = this.userTaskService.list(taskQueryWrapper);
        int countAwardNum = userTasks.size();
        Map<Long, UserTask> userTaskMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userTasks)) {
            for (UserTask userTask : userTasks) {
                userTaskMap.put(userTask.getUserId(), userTask);
            }
        }

        int joinNum = this.activityDefAboutService.getUserJoinNumByActivityDef(activityDef);
        resp.fromActivityDef(activityDef, joinNum, countAwardNum, awardCashBackNum);


        // 通过activityIdList查询 user_activity表 取得所有用户id
        Page<UserActivity> page = new Page<>(pageNum, size);
        QueryWrapper<UserActivity> uaWrapper = new QueryWrapper<>();
        uaWrapper.lambda()
                .eq(UserActivity::getMerchantId, activityDef.getMerchantId())
                .eq(UserActivity::getShopId, activityDef.getShopId())
                .eq(UserActivity::getActivityDefId, activityDef.getId())
                .in(UserActivity::getStatus, statusList)
                .notIn(UserActivity::getUserId, robotIds)
                .orderByDesc(UserActivity::getId)
        ;
        int total = this.userActivityService.count(uaWrapper);

        IPage<UserActivity> iPage = this.userActivityService.page(page, uaWrapper);
        List<UserActivity> userActivities = iPage.getRecords();
        if (CollectionUtils.isEmpty(userActivities)) {
            response.setData(resp);
            return response;
        }

        List<Long> allUserIdList = new ArrayList<>();
        for (UserActivity userActivity : userActivities) {
            if (userActivity.getUserId() != null && userActivity.getUserId() > 0 && allUserIdList.indexOf(userActivity.getUserId()) == -1) {
                allUserIdList.add(userActivity.getUserId());
            }
        }
        // 获取此任务的所有用户信息.
        Map<Long, User> userMap = this.getUserMap(allUserIdList);

        resp.getDetails().setContent(new ArrayList<>());
        resp.getDetails().from(pageNum, size, total);
        for (UserActivity userActivity : userActivities) {
            QueryActivityDefUserJoinResp.Detail detail = new QueryActivityDefUserJoinResp.Detail();
            User user = userMap.get(userActivity.getUserId());
            if (null == user) {
                continue;
            }
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            if (StringUtils.isNotBlank(user.getMobile())) {
                detail.setPhone(user.getMobile());
            }
            UserTask userTask = userTaskMap.get(userActivity.getUserId());
            UserCoupon userCoupon = userCouponMap.get(userActivity.getUserId());
            if (userTask != null) {
                detail.setAwardInfo("已中奖");
            }
            if (userCoupon != null) {
                detail.setBackAmountInfo("已返现");
            }
            detail.setJoinTime(userActivity.getCtime());
            detail.setJoinUser(userInfo);
            resp.getDetails().getContent().add(detail);
        }
        response.setData(resp);
        return response;
    }

    @Override
    public SimpleResponse userTaskUserJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef, UserTaskTypeEnum userTaskTypeEnum) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        QueryActivityDefUserJoinResp resp = new QueryActivityDefUserJoinResp();
        resp.fromActivityDef(activityDef, 0, 0, 0);
        Long activityDefId = activityDef.getId();

        List<Integer> types = new ArrayList<>();
        types.add(userTaskTypeEnum.getCode());
        if (userTaskTypeEnum == UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2) {
            types.add(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
        }

        //获取中奖数目
        QueryWrapper<UserTask> taskQueryWrapper = new QueryWrapper<>();
        taskQueryWrapper.lambda()
                .eq(UserTask::getMerchantId, activityDef.getMerchantId())
                .eq(UserTask::getShopId, activityDef.getShopId())
                .eq(UserTask::getActivityDefId, activityDefId)
                .in(UserTask::getType, types)
                .groupBy()
        ;
        int total = this.userTaskService.count(taskQueryWrapper);
        Page<UserTask> page = new Page<>(pageNum, size);
        IPage<UserTask> iPage = this.userTaskService.page(page, taskQueryWrapper);
        List<UserTask> userTasks = iPage.getRecords();
        if (CollectionUtils.isEmpty(userTasks)) {
            response.setData(resp);
            return response;
        }

        List<Long> allUserIdList = new ArrayList<>();
        for (UserTask userTask : userTasks) {
            if (userTask.getUserId() != null && userTask.getUserId() > 0 && allUserIdList.indexOf(userTask.getUserId()) == -1) {
                allUserIdList.add(userTask.getUserId());
            }
        }
        // 获取此任务的所有用户信息.
        Map<Long, User> userMap = this.getUserMap(allUserIdList);

        int couponType = 0;
        if (userTaskTypeEnum.getCode() == UserTaskTypeEnum.GOOD_COMMENT.getCode()
                || userTaskTypeEnum.getCode() == UserTaskTypeEnum.GOOD_COMMENT_V2.getCode()) {
            couponType = UserCouponTypeEnum.GOODS_COMMENT.getCode();
        } else if (userTaskTypeEnum.getCode() == UserTaskTypeEnum.ORDER_REDPACK.getCode()
                || userTaskTypeEnum.getCode() == UserTaskTypeEnum.ORDER_REDPACK_V2.getCode()) {
            couponType = UserCouponTypeEnum.ORDER_RED_PACKET.getCode();
        } else if (userTaskTypeEnum.getCode() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()
                || userTaskTypeEnum.getCode() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode()) {
            couponType = UserCouponTypeEnum.FAV_CART.getCode();
        }
        QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
        ucWrapper.lambda()
                .eq(UserCoupon::getMerchantId, activityDef.getMerchantId())
                .eq(UserCoupon::getShopId, activityDef.getShopId())
                .eq(UserCoupon::getActivityDefId, activityDefId)
                .eq(UserCoupon::getType, couponType)
        ;
        List<UserCoupon> userCoupons = this.userCouponService.list(ucWrapper);
        int moneyBackNum = this.userCouponService.count(ucWrapper);
        int joinNum = this.activityDefAboutService.getUserJoinNumByActivityDef(activityDef);
        resp.fromActivityDef(activityDef, joinNum, joinNum, moneyBackNum);

        Map<Long, UserCoupon> userCouponMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userCoupons)) {
            for (UserCoupon userCoupon : userCoupons) {
                userCouponMap.put(userCoupon.getUserId(), userCoupon);
            }
        }

        resp.getDetails().setContent(new ArrayList<>());
        resp.getDetails().from(pageNum, size, total);
        for (UserTask userTask : userTasks) {
            QueryActivityDefUserJoinResp.Detail detail = new QueryActivityDefUserJoinResp.Detail();
            User user = userMap.get(userTask.getUserId());
            if (null == user) {
                continue;
            }
            UserCoupon userCoupon = userCouponMap.get(userTask.getUserId());
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatarUrl());
            userInfo.put("nickName", user.getNickName());
            if (StringUtils.isNotBlank(user.getMobile())) {
                detail.setPhone(user.getMobile());
            }
            if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode() ||
                    userTask.getStatus() == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                detail.setAwardInfo("已中奖");
            }
            if (StringUtils.isNotBlank(userTask.getOrderId())) {
                detail.setOrderNum(userTask.getOrderId());
            }
            if (userCoupon != null) {
                detail.setBackAmountInfo("已返现");
                if (StringUtils.isNotBlank(userCoupon.getCommissionDef())) {
                    Map<String, Object> jsonMap = JSONObject.parseObject(userCoupon.getCommissionDef(), Map.class);
                    if (jsonMap != null && jsonMap.get("commissionDef") != null && StringUtils.isNotBlank(jsonMap.get("commissionDef").toString())) {
                        detail.setAmount(jsonMap.get("commissionDef").toString());
                    }
                }
            }
            detail.setJoinUser(userInfo);
            detail.setJoinTime(userTask.getCtime());
            resp.getDetails().getContent().add(detail);
        }
        response.setData(resp);
        return response;
    }

    /**
     * 查询报销活动参与信息
     *
     * @param pageNum
     * @param size
     * @param activityDef
     * @return
     */
    @Override
    public SimpleResponse reimbursementJoinInfo(Integer pageNum, Integer size, ActivityDef activityDef) {
        if (pageNum == null || size == null || activityDef == null) {
            return new SimpleResponse();
        }

        // 查询参与记录
        Page<UserActivity> page = new Page<>(pageNum, size);
        IPage<UserActivity> userActivityIPage = userActivityService.page(
                page,
                new QueryWrapper<UserActivity>()
                        .lambda()
                        .eq(UserActivity::getActivityDefId, activityDef.getId())
                        .in(UserActivity::getType, UserActivityTypeEnum.FULL_REIMBURSEMENT_START.getCode(), UserActivityTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode())
        );

        List<QueryActivityDefUserJoinResp.Detail> details = new ArrayList<>();
        for (UserActivity item:page.getRecords()) {
            QueryActivityDefUserJoinResp.Detail detail = new QueryActivityDefUserJoinResp.Detail();
            detail.setJoinTime(item.getCtime());

            // 查询活动
            Activity activity = activityService.findById(item.getActivityId());
            if (activity == null) {
                log.error("活动不存在，活动id={} @to 大妖怪", item.getActivityId());
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1,"活动不存在 @to 大妖怪");
            }

            if (UserActivityTypeEnum.FULL_REIMBURSEMENT_START.getCode() == item.getType()) {
                detail.setApplyAmount(activity.getDeposit().toString());
                // 查询获奖记录
                List<UserCoupon> userCoupons = userCouponService.list(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityId, item.getActivityId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.FULL_REIMBURSEMENT_START.getCode())
                                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                );

                BigDecimal amount = BigDecimal.ZERO;
                for (UserCoupon userCoupon : userCoupons) {
                    amount = amount.add(userCoupon.getAmount());
                }

                detail.setAmount(amount.toString());
            } else {
                detail.setApplyAmount(BigDecimal.ZERO.toString());
                detail.setAmount(BigDecimal.ZERO.toString());
            }

            if (!detail.getApplyAmount().equals("0") && detail.getAmount().equals(detail.getApplyAmount())){
                detail.setAwardInfo("已完成");
            } else {
                detail.setAwardInfo("未完成");
            }

            // 查询用户
            User user = userService.selectByPrimaryKey(item.getUserId());
            if (user == null) {
                log.error("用户不存在，用户id={} @to 大妖怪", item.getUserId());
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1,"用户不存在 @to 大妖怪");
            }
            Map<String, Object> joinUser = new HashMap<>(2);
            joinUser.put("avatarUrl", user.getAvatarUrl());
            joinUser.put("nickName", user.getNickName());
            detail.setJoinUser(joinUser);
            detail.setPhone(user.getMobile());

            details.add(detail);
        }

        // 统计活动完成人数
        int finishCount = 0;
        // 统计拉新人数
        int inviteCount = 0;

        List<UserActivity> userActivities = userActivityService.list(
                new QueryWrapper<UserActivity>()
                        .lambda()
                        .eq(UserActivity::getActivityDefId, activityDef.getId())
                        .in(UserActivity::getType, UserActivityTypeEnum.FULL_REIMBURSEMENT_START.getCode(), UserActivityTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode())
        );

        for (UserActivity userActivity : userActivities) {
            // 查询活动
            Activity activity = activityService.findById(userActivity.getActivityId());
            if (activity == null) {
                log.error("活动不存在，活动id={} @to 大妖怪", userActivity.getActivityId());
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1,"活动不存在 @to 大妖怪");
            }

            String applyAmount = "0";
            String amountCount = "0";

            if (UserActivityTypeEnum.FULL_REIMBURSEMENT_JOIN.getCode() == userActivity.getType()) {
                inviteCount ++;
            }

            if (UserActivityTypeEnum.FULL_REIMBURSEMENT_START.getCode() == userActivity.getType()) {
                applyAmount = activity.getDeposit().toString();
                // 查询获奖记录
                List<UserCoupon> userCoupons = userCouponService.list(
                        new QueryWrapper<UserCoupon>()
                                .lambda()
                                .eq(UserCoupon::getActivityId, userActivity.getActivityId())
                                .eq(UserCoupon::getType, UserCouponTypeEnum.FULL_REIMBURSEMENT_START.getCode())
                                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                );

                BigDecimal amount = BigDecimal.ZERO;
                for (UserCoupon userCoupon : userCoupons) {
                    amount = amount.add(userCoupon.getAmount());
                }

                amountCount = amount.toString();
            } else {
                applyAmount = BigDecimal.ZERO.toString();
                amountCount = BigDecimal.ZERO.toString();
            }

            if (!applyAmount.equals("0") && amountCount.equals(applyAmount)){
                finishCount ++;
            }
        }

        QueryActivityDefUserJoinResp resp = new QueryActivityDefUserJoinResp();
        resp.setActivityDefId(activityDef.getId());
        resp.setShopId(activityDef.getShopId());
        resp.setMerchantId(activityDef.getMerchantId());
        resp.setType(activityDef.getType());
        resp.setJoinNum((int) userActivityIPage.getTotal());
        resp.setAwardNum(finishCount);
        resp.setInviteCount(inviteCount);

        MybatisPage  mybatisPage = new MybatisPage();
        mybatisPage.setContent(details);
        mybatisPage.setSize((int) userActivityIPage.getSize());
        mybatisPage.setPage((int) userActivityIPage.getCurrent());
        mybatisPage.setTotal((int) userActivityIPage.getTotal());

        resp.setDetails(mybatisPage);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);

        return simpleResponse;
    }

    private Map<Long, User> getUserMap(List<Long> allUserIdList) {
        Map<Long, User> userMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(allUserIdList)) {
            QueryWrapper<User> userWrapper = new QueryWrapper<>();
            userWrapper.in("id", allUserIdList);
            List<User> users = userService.list(userWrapper);
            for (User user : users) {
                userMap.put(user.getId(), user);
            }
        }
        return userMap;
    }
}
