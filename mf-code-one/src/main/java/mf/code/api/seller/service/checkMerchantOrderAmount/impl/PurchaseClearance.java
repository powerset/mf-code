package mf.code.api.seller.service.checkMerchantOrderAmount.impl;

import com.alibaba.fastjson.JSONObject;
import mf.code.api.seller.dto.MerchantOrderReq;
import mf.code.api.seller.service.checkMerchantOrderAmount.CheckCreateAmountConverter;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.merchant.constants.BizTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.seller.service.checkMerchantOrderAmount.impl
 *
 * @description: 购买公有版
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月26日 11:43
 */
@Service
public class PurchaseClearance implements CheckCreateAmountConverter {
    @Autowired
    private CommonDictService commonDictService;

    @Override
    public boolean convert(MerchantOrderReq dto) {
        if (dto.getBizType() == BizTypeEnum.PURCHASE_CLEARANCE.getCode()) {
            //购买集客魔方-清仓版
            CommonDict commonDict = this.commonDictService.selectByTypeKey("jkmfPurchase", "pay_menu");
            List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
            if (!CollectionUtils.isEmpty(objects)) {
                for (Object obj : objects) {
                    //[{"money":"588.00","name":"购买公有版","rate":100},{"money":"5888.00","name":"购买私有版","rate":100}]
                    JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                    Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                    boolean bizVersion = jsonMap != null && dto.getBizValue() == Long.valueOf(jsonMap.get("bizVersion").toString());
                    boolean amount = jsonMap != null && new BigDecimal(dto.getAmount()).compareTo(new BigDecimal(jsonMap.get("money").toString())) == 0;
                    if (amount && bizVersion) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
