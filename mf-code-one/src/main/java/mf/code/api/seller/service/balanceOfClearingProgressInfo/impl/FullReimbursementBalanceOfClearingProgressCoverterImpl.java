package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月03日 10:37
 */
@Service
@Slf4j
public class FullReimbursementBalanceOfClearingProgressCoverterImpl extends BalanceOfClearingProgressService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        boolean full_reimbursement = activityDef.getType().equals(ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode());
        if (full_reimbursement) {
            QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
            activityQueryWrapper.lambda()
                    .eq(Activity::getMerchantId, activityDef.getMerchantId())
                    .eq(Activity::getShopId, activityDef.getShopId())
                    .eq(Activity::getType, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode())
                    .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
                    .eq(Activity::getDel, DelEnum.NO.getCode())
            ;
            activityQueryWrapper.orderByDesc("id");
            List<Activity> activityList = activityService.list(activityQueryWrapper);

            Date time = null;

            //计算活动是否有进行中的，统计绑定金额
            BigDecimal binding = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityList)) {
                time = activityList.get(0).getEndTime();

                List<Long> activityIds = new ArrayList<>();
                for (Activity activity : activityList) {
                    if (activityIds.indexOf(activity.getId()) == -1) {
                        activityIds.add(activity.getId());
                    }

                }
                QueryWrapper<UpayWxOrder> upayWxOrderQueryWrapper = new QueryWrapper<>();
                upayWxOrderQueryWrapper.lambda()
                        .eq(UpayWxOrder::getMchId, activityDef.getMerchantId())
                        .eq(UpayWxOrder::getShopId, activityDef.getShopId())
                        .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                        .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
                        .eq(UpayWxOrder::getBizType, UpayWxOrderBizTypeEnum.FULL_REIMBURSEMENT.getCode())
                        .in(UpayWxOrder::getBizValue, activityIds)
                ;
                List<UpayWxOrder> upayWxOrders = upayWxOrderService.list(upayWxOrderQueryWrapper);
                Map<Long, List<UpayWxOrder>> upayWxOrderMap = new HashMap<>();
                if (!CollectionUtils.isEmpty(upayWxOrders)) {
                    for (UpayWxOrder upayWxOrder : upayWxOrders) {
                        List<UpayWxOrder> upayWxOrders1 = upayWxOrderMap.get(upayWxOrder.getBizValue());
                        if (CollectionUtils.isEmpty(upayWxOrders1)) {
                            upayWxOrders1 = new ArrayList<>();
                        }
                        upayWxOrderMap.put(upayWxOrder.getBizValue(), upayWxOrders1);
                    }
                }

                for (Activity activity : activityList) {
                    if (activity.getDeposit().compareTo(BigDecimal.ZERO) <= 0) {
                        log.error("<<<<<<<<该活动没有报销金额，警告！警告！警告！");
                        continue;
                    }
                    //该活动的总报销金额
                    BigDecimal amount = activity.getDeposit();
                    //已经消耗出去的金额
                    BigDecimal totalCost = BigDecimal.ZERO;
                    List<UpayWxOrder> upayWxOrders1 = upayWxOrderMap.get(activity.getId());
                    if (!CollectionUtils.isEmpty(upayWxOrders1)) {
                        for (UpayWxOrder upayWxOrder : upayWxOrders1) {
                            totalCost = totalCost.add(upayWxOrder.getTotalFee());
                        }
                    }
                    binding = amount.subtract(totalCost);
                }
            }
            BigDecimal sum = activityDef.getDepositDef();
            BigDecimal remain = activityDef.getDeposit();
            BigDecimal cost = activityDef.getDepositDef().subtract(activityDef.getDeposit());

            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();
            dto.setSumPayMoney(sum.setScale(2, BigDecimal.ROUND_DOWN).toString());
            dto.setRemainMoney(remain.setScale(2, BigDecimal.ROUND_DOWN).toString());
            dto.setBindingMoney(binding.setScale(2, BigDecimal.ROUND_DOWN).toString());
            dto.setCostMoney(cost.setScale(2, BigDecimal.ROUND_DOWN).toString());
            dto.setActivityDefId(activityDef.getId());
            dto.setStatus(BalanceOfClearingProgressDto.CanCash);
            if (binding.compareTo(BigDecimal.ZERO) > 0) {
                dto.setStatus(BalanceOfClearingProgressDto.BindingActivity);
            }
            if (time != null) {
                dto.setCutoffTime(DateUtils.addHours(time, 24).getTime());
                dto.setCutoffDate(DateUtils.addHours(time, 24));
            }
            return dto;
        }
        return null;
    }
}
