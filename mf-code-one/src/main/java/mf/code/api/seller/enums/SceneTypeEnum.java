package mf.code.api.seller.enums;

/**
 * mf.code.api.seller.enums
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月21日 14:57
 */
public enum SceneTypeEnum {
    //场景类型 1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览 10:全额报销
    FILLORDER(1, "回填订单"),
    GOODCOMMENT(2, "好评晒图"),
    OPENREDPACK(3, "拆红包"),
    ASSIST(4, "助力"),
    LUCKYWHEEL(5, "轮盘"),
    FAVCART(6, "收藏加购"),
    ORDERBACK(7, "免单抽奖"),
    WEIGHTED_TRIAL(8, "加权试用"),
    WEIGHTED_BROWSING(9, "加权浏览"),
    FULL_REIMBURSEMENT(32, "全额报销活动"),
    ;
    private int code;
    private String message;

    SceneTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Integer findByCode(Integer code) {
        for (SceneTypeEnum typeEnum : SceneTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getCode();
            }
        }
        return null;
    }


    public static String findByDesc(Integer code) {
        for (SceneTypeEnum typeEnum : SceneTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return "";
    }
}
