package mf.code.api.seller.v4.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.goods.repo.po.MybatisPage;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * mf.code.api.seller.v4.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月20日 19:41
 */
@Data
public class QueryActivityDefUserJoinResp {
    //活动定义编号(更新时需要)
    private Long activityDefId;
    //店铺编号
    private Long shopId;
    //商户编号
    private Long merchantId;
    //活动类型
    private Integer type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    //参与人数
    private int joinNum;
    //中奖人数
    private int awardNum;
    //返现人数
    private int backAmountNum;
    //领取金额
    private String totalAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //消耗库存
    private int costStock;

    /**
     * 拉新人数（报销活动专用）
     */
    private int inviteCount;

    private MybatisPage<Detail> details;

    @Data
    public static class Detail {
        //参与时间
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date joinTime;
        //参与人信息
        private Map<String, Object> joinUser;
        //手机号
        private String phone = "未绑定";
        //中奖情况
        private String awardInfo = "未中奖";
        //返现情况
        private String backAmountInfo = "未返现";
        // 申请报销金额
        private String applyAmount;
        //中奖金额/红包金额
        private String amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        //回填订单编号
        private String orderNum;

        public void fromActivity(Activity activity, Map<String, Object> userInfo) {
            this.joinTime = activity.getCtime();
            this.joinUser = userInfo;
        }
    }

    public void fromActivityDef(ActivityDef activityDef, int joinNum, int countAwardNum, int countMoneyBackNum) {
        this.merchantId = activityDef.getMerchantId();
        this.shopId = activityDef.getShopId();
        this.activityDefId = activityDef.getId();
        this.type = activityDef.getType();
        this.startTime = activityDef.getPublishTime();
        if (activityDef.getStockDef() != null && activityDef.getStock() != null) {
            this.costStock = activityDef.getStockDef() - activityDef.getStock();
        }
        this.joinNum = joinNum;
        this.awardNum = countAwardNum;
        this.backAmountNum = countMoneyBackNum;
        if (activityDef.getDepositDef() != null && activityDef.getDeposit() != null) {
            this.totalAmount = activityDef.getDepositDef().subtract(activityDef.getDeposit()).setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        this.details = new MybatisPage<>(0, 0, 0);
    }
}
