package mf.code.api.seller.service.balanceOfClearingProgressInfo.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressDto;
import mf.code.api.seller.service.balanceOfClearingProgressInfo.BalanceOfClearingProgressService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.api.seller.service.balanceOfClearingProgressInfo.impl
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-26 15:42
 */
@Service
@Slf4j
public class ShopMannagerTaskBalanceOfClearingProgressCoverterImpl extends BalanceOfClearingProgressService {

    @Autowired
    private ActivityDefService activityDefService;

    @Autowired
    private UserCouponService userCouponService;

    @Override
    public BalanceOfClearingProgressDto converterBalanceOfClearingProgress(ActivityDef activityDef) {
        if (activityDef == null) {
            return null;
        }

        boolean shopManagerType = activityDef.getType().equals(ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode());

        if (shopManagerType) {
            // 查询子活动设置微信群任务
            ActivityDef setWechatActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
            if (setWechatActivityDef == null) {
                log.error("子活动设置微信群任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
                return null;
            }

            // 查询子活动邀请下级粉丝任务
            ActivityDef inviteActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
            if (inviteActivityDef == null) {
                log.error("子活动邀请下级粉丝任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
                return null;
            }

            // 查询子活动发展粉丝成为店长任务
            ActivityDef developActivityDef = activityDefService.selectByParentIdAndType(activityDef.getId(),
                    ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
            if (developActivityDef == null) {
                log.error("子活动发展粉丝成为店长任务不存在，主活动id={}, @to 大妖怪", activityDef.getId());
                return null;
            }

            // 消耗金额
            BigDecimal deposit = BigDecimal.ZERO;

            // 查询设置微信群任务奖励
            List<UserCoupon> setWechatUserCoupons = userCouponService.list(
                    new QueryWrapper<UserCoupon>()
                            .lambda()
                            .eq(UserCoupon::getActivityDefId, setWechatActivityDef.getId())
                            .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode())
            );

            deposit = deposit.add(new BigDecimal(setWechatUserCoupons.size()).multiply(setWechatActivityDef.getCommission()));

            // 查询邀请下级粉丝任务奖励
            List<UserCoupon> inviteUserCoupons = userCouponService.list(
                    new QueryWrapper<UserCoupon>()
                            .lambda()
                            .eq(UserCoupon::getActivityDefId, inviteActivityDef.getId())
                            .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode())
            );

            deposit = deposit.add(new BigDecimal(inviteUserCoupons.size()).multiply(inviteActivityDef.getCommission()));

            // 查询发展粉丝成为店长任务奖励
            List<UserCoupon> developUserCoupons = userCouponService.list(
                    new QueryWrapper<UserCoupon>()
                            .lambda()
                            .eq(UserCoupon::getActivityDefId, developActivityDef.getId())
                            .eq(UserCoupon::getType, UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode())
            );

            deposit = deposit.add(new BigDecimal(developUserCoupons.size()).multiply(developActivityDef.getCommission()));

            BalanceOfClearingProgressDto dto = new BalanceOfClearingProgressDto();

            // 计划金额
            dto.setSumPayMoney(activityDef.getDepositDef().setScale(2,  BigDecimal.ROUND_DOWN).toString());
            // 消耗金额
            dto.setCostMoney(deposit.setScale(2,  BigDecimal.ROUND_DOWN).toString());
            // 绑定金额
            dto.setBindingMoney(BigDecimal.ZERO.toString());
            // 剩余金额
            dto.setRemainMoney(activityDef.getDepositDef().subtract(deposit).setScale(2,  BigDecimal.ROUND_DOWN).toString());
            dto.setAuditNum(0);
            dto.setActivityDefId(activityDef.getId());
            dto.setStatus(BalanceOfClearingProgressDto.CanCash);

            return dto;
        }

        return null;
    }
}
