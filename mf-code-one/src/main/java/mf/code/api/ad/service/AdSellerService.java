package mf.code.api.ad.service;

import mf.code.api.ad.dto.AdPurchaseStatusReq;
import mf.code.api.ad.dto.PurchaseReq;
import mf.code.api.ad.dto.ResetAdpicReq;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.ad.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 下午1:53
 */
public interface AdSellerService {
    /**
     * 查看某一流量主的渠道信息
     * @param adChannelId 流量主id
     * @return SimpleResponse
     */
    SimpleResponse queryAdChannel(String adChannelId);

    /**
     * 展示某一流量主的购买页
     * @return SimpleResponse
     */
    SimpleResponse showPurchasePage();

    /**
     * 广告主（商户）购买广告
     * @param purchaseReq 购买信息
     * @return SimpleResponse
     */
    SimpleResponse purchase(PurchaseReq purchaseReq);

    /**
     * 列表展示商户的购买记录
     * @param merchantId 商户id
     * @param current 当前页码
     * @param size 页码大小
     * @return SimpleResponse
     */
    SimpleResponse listPurchaseRecord(String merchantId, String current, String size);

    /**
     * 启动or暂停广告
     * @param adPurchaseStatusReq 启动请求
     * @return SimpleResponse
     */
    SimpleResponse setAdStatus(AdPurchaseStatusReq adPurchaseStatusReq);

    /**
     * 图片审核不通过 展示重新提交的页面
     * @param adPurchaseId
     * @return
     */
    SimpleResponse showResetPage(String adPurchaseId);

    /**
     * 更新（重新提交）广告图片
     * @param resetAdpicReq
     * @return
     */
    SimpleResponse updateAdPic(ResetAdpicReq resetAdpicReq);

    /***
     * 更新广告主
     * @param adPurchaseId
     * @return
     */
    boolean updatePurchaseStatus(Long adPurchaseId);
}
