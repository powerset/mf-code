package mf.code.api.ad;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.ad.dto.AdPurchaseStatusReq;
import mf.code.api.ad.dto.PurchaseReq;
import mf.code.api.ad.dto.ResetAdpicReq;
import mf.code.api.ad.service.AdSellerService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.ad
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 下午1:39
 */
@Slf4j
@RestController
@RequestMapping("/api/ad/seller")
public class AdSellerApi {
    private final AdSellerService adSellerService;

    @Autowired
    public AdSellerApi(AdSellerService adSellerService) {
        this.adSellerService = adSellerService;
    }

    @GetMapping("/queryAdChannel")
    public SimpleResponse queryAdChannel(@RequestParam("adChannelId") String adChannelId) {
        return adSellerService.queryAdChannel(adChannelId);
    }

    @GetMapping("/showPurchasePage")
    public SimpleResponse showPurchasePage() {
        return adSellerService.showPurchasePage();
    }

    @PostMapping("/purchase")
    public SimpleResponse purchase(@RequestBody PurchaseReq purchaseReq) {
        return adSellerService.purchase(purchaseReq);
    }

    @GetMapping("/listPurchaseRecord")
    public SimpleResponse listPurchaseRecord(@RequestParam("merchantId") String merchantId,
                                             @RequestParam(value = "current", defaultValue = "1") String current,
                                             @RequestParam(value = "size", defaultValue = "10") String size) {
        return adSellerService.listPurchaseRecord(merchantId, current, size);
    }

    @PostMapping("/setAdStatus")
    public SimpleResponse setAdStatus(@RequestBody AdPurchaseStatusReq adPurchaseStatusReq) {
        return adSellerService.setAdStatus(adPurchaseStatusReq);
    }

    @GetMapping("/showResetPage")
    public SimpleResponse showResetPage(@RequestParam("adPurchaseId") String adPurchaseId) {
        return adSellerService.showResetPage(adPurchaseId);
    }

    @PostMapping("/updateAdPic")
    public SimpleResponse updateAdPic(@RequestBody ResetAdpicReq resetAdpicReq) {
        return adSellerService.updateAdPic(resetAdpicReq);
    }
}
