package mf.code.api.ad.service;

import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.web.multipart.MultipartFile;

/**
 * mf.code.api.ad.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 上午10:14
 */
public interface AdUploadService {

    SimpleResponse createAdChannelPic(MultipartFile image);
}
