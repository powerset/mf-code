package mf.code.api.ad.dto;

import lombok.Data;

/**
 * mf.code.api.ad.dto
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-24 下午6:41
 */
@Data
public class ResetAdpicReq {
    private String adPurchaseId;
    private String adPic;
}
