package mf.code.api.ad;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.ad.dto.AdChannelReq;
import mf.code.api.ad.dto.AdChannelStatusReq;
import mf.code.api.ad.dto.AuditAdPicReq;
import mf.code.api.ad.service.AdPlatformService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.platform
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-15 上午9:36
 */
@Slf4j
@RestController
@RequestMapping("/api/ad/platform")
public class AdPlatformApi {
    private final AdPlatformService adPlatformService;

    @Autowired
    public AdPlatformApi(AdPlatformService adPlatformService) {
        this.adPlatformService = adPlatformService;
    }

    @PostMapping("/saveAdChannel")
    public SimpleResponse saveAdChannel(@RequestBody AdChannelReq adChannelReq) {
        return adPlatformService.saveAdChanel(adChannelReq);
    }

    @GetMapping("/listAdChannel")
    public SimpleResponse listAdChannel(@RequestParam(value = "current", defaultValue = "1") String current,
                                        @RequestParam(value = "size", defaultValue = "10") String size) {
        return adPlatformService.listAdChannel(current, size);
    }

    @GetMapping("/listAuditAdPic")
    public SimpleResponse listAuditAdPic(@RequestParam(value = "current", defaultValue = "1") String current,
                                        @RequestParam(value = "size", defaultValue = "10") String size) {
        return adPlatformService.listAuditAdPic(current, size);
    }

    @GetMapping("/queryAdChannel")
    public SimpleResponse queryAdChannel(@RequestParam("adChannelId") String adChannelId) {
        return adPlatformService.queryAdChannel(adChannelId);
    }

    @PostMapping("/updateAdChannel")
    public SimpleResponse updateAdChannel(@RequestBody AdChannelReq adChannelReq) {
        return adPlatformService.updateAdChannel(adChannelReq);
    }

    @PostMapping("/setAdChannelStatus")
    public SimpleResponse setAdChannelStatus(@RequestBody AdChannelStatusReq adChannelStatusReq) {
        return adPlatformService.setAdChannelStatus(adChannelStatusReq);
    }

    @PostMapping("/auditAdPic")
    public SimpleResponse auditAdPic(@RequestBody AuditAdPicReq auditAdPicReq) {
        return adPlatformService.auditAdPic(auditAdPicReq);
    }

    @GetMapping("/adChannelStatistics")
    public SimpleResponse adChannelStatistics(@RequestParam("adChannelId") String adChannelId,
                                              @RequestParam(value = "startTime", required = false) String startTime,
                                              @RequestParam(value = "endTime", required = false) String endTime) {
        return adPlatformService.adChannelStatistics(adChannelId, startTime, endTime);
    }

    @GetMapping("/adPurchaseStatistics")
    public SimpleResponse adPurchaseStatistics(@RequestParam("shopId") String shopId,
                                              @RequestParam(value = "startTime", required = false) String startTime,
                                              @RequestParam(value = "endTime", required = false) String endTime) {
        return adPlatformService.adPurchaseStatistics(shopId, startTime, endTime);
    }
}
