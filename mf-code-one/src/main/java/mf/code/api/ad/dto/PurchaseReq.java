package mf.code.api.ad.dto;

import lombok.Data;

/**
 * mf.code.api.ad.dto
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 下午4:17
 */
@Data
public class PurchaseReq {
    private String merchantId;
    private String adChannelId;
    private String stockDef;
    private String adPic;
}
