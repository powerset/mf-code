package mf.code.api.ad.service;

import mf.code.api.ad.dto.AdChannelReq;
import mf.code.api.ad.dto.AdChannelStatusReq;
import mf.code.api.ad.dto.AuditAdPicReq;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.platform.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-15 上午9:42
 */
public interface AdPlatformService {
    /**
     * 创建流量主
     * @param adChannelReq 请求参数
     * @return SimpleResponse
     */
    SimpleResponse saveAdChanel(AdChannelReq adChannelReq);

    /**
     * 列表展示所有流量主（创建时间倒序（id倒序））
     * @param current 当前页
     * @param size 大小
     * @return SimpleResponse
     */
    SimpleResponse listAdChannel(String current, String size);

    /**
     * 查看某一流量主的创建信息
     * @param adChannelId 流量主id
     * @return SimpleResponse
     */
    SimpleResponse queryAdChannel(String adChannelId);

    /**
     * 修改某一流量主的信息
     * @param adChannelReq 请求参数
     * @return SimpleResponse
     */
    SimpleResponse updateAdChannel(AdChannelReq adChannelReq);

    /**
     * 设置流量主服务的状态 启动或停止
     * @param adChannelStatusReq 请求参数
     * @return SimpleResponse
     */
    SimpleResponse setAdChannelStatus(AdChannelStatusReq adChannelStatusReq);

    /**
     * 审核广告主图片
     * @param auditAdPicReq 请求参数
     * @return SimpleResponse
     */
    SimpleResponse auditAdPic(AuditAdPicReq auditAdPicReq);

    /**
     * 列表展示 广告图片 审核页
     * @param current 当前页
     * @param size 大小
     * @return SimpleResponse
     */
    SimpleResponse listAuditAdPic(String current, String size);

    /**
     * 流量主业务统计
     * @param adChannelId 流量主id
     * @param startTime 开始时间
     * @param endTime 截止时间
     * @return SimpleResponse
     */
    SimpleResponse adChannelStatistics(String adChannelId, String startTime, String endTime);

    /**
     * 广告主业务统计
     * @param shopId
     * @param startTime
     * @param endTime
     * @return
     */
    SimpleResponse adPurchaseStatistics(String shopId, String startTime, String endTime);
}
