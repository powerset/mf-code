package mf.code.api.ad.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * mf.code.api.ad.dto
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-19 下午5:28
 */
@Data
public class AuditAdPicReq {
    private String adPurchaseId;
    private String auditStatus;
    private String auditReason;
    @JsonProperty("platformId")
    private String platUserId;
}
