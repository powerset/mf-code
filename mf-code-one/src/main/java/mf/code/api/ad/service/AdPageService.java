package mf.code.api.ad.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.ad.service
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-18 下午2:12
 */
public interface AdPageService {
    /**
     * 展示广告详情
     * @param adChannelId 流量主id
     * @return SimpleResponse
     */
    SimpleResponse<Object> showAdPage(String adChannelId, String userAgent, String ipAddress);
}
