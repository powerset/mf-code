package mf.code.api.ad;

import mf.code.api.ad.service.AdUploadService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * mf.code.api.ad
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-17 上午10:13
 */
@RestController
@RequestMapping(value = "/api/ad/upload")
public class AdUploadApi {
    private final AdUploadService adUploadService;

    @Autowired
    public AdUploadApi(AdUploadService adUploadService) {
        this.adUploadService = adUploadService;
    }

    /**
     * 上传流量主图片
     */
    @PostMapping(path = "/createAdPic")
    public SimpleResponse createAdChannelPic(@RequestParam(name = "image") MultipartFile image) {
        return adUploadService.createAdChannelPic(image);
    }
}
