package mf.code.api.ad.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * mf.code.api.platform.dto
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-15 上午9:48
 */
@Data
public class AdChannelReq {
    // 修改所需参数
    private String adChannelId;

    // 创建流量主
    private String name;
    private String channelDesc;
    private String channelPic;
    private String adDesc;
    private String adPic;
    private String stockDef;
    private String unitStock;
    private String unitPrice;
    @JsonProperty("platformId")
    private String platUserId;
}
