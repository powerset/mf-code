package mf.code.api.ad;

import lombok.extern.slf4j.Slf4j;
import mf.code.ad.repo.po.AdPurchase;
import mf.code.ad.service.AdPurchaseService;
import mf.code.ad.service.AdUserService;
import mf.code.api.ad.service.AdPageService;
import mf.code.common.redis.ad.AdRedisKeys;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RandomStrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.ad
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-18 下午1:33
 */
@Slf4j
@RestController
@RequestMapping("/api/ad/v2/user")
public class AdPageV2Api {
    private final AdPageService adPageService;
    private final AdPurchaseService adPurchaseService;
    private final AdUserService adUserService;
    private final StringRedisTemplate stringRedisTemplate;

    @Autowired
    public AdPageV2Api(AdPageService adPageService, AdPurchaseService adPurchaseService, AdUserService adUserService, StringRedisTemplate stringRedisTemplate) {
        this.adPageService = adPageService;
        this.adPurchaseService = adPurchaseService;
        this.adUserService = adUserService;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @GetMapping("/showAdPage")
    public SimpleResponse showAdPage(@RequestParam("adChannelId") String reqChannelId,
                                     HttpServletRequest request,
                                     HttpServletResponse response) {
        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();

        String userAgent = request.getHeader("User-Agent");
        String ipAddress = IpUtil.getIpAddress(request);

        // 获取uuid
        String uuid = this.getUUID(request);
        String redisHashKey = AdRedisKeys.UUID + uuid + ":" + reqChannelId;

        // 初始化数据
        Map redisMapByUUID = null;
        boolean flag = false;

        // 通过uuid去redis获取数据
        redisMapByUUID = this.getCookiesByUUID(redisHashKey);
        String channelId = redisMapByUUID.get(AdRedisKeys.CHANNEL) == null ? null : redisMapByUUID.get(AdRedisKeys.CHANNEL).toString();
        String merchantId = redisMapByUUID.get(AdRedisKeys.MERCHANT) == null ? null : redisMapByUUID.get(AdRedisKeys.MERCHANT).toString();

        if (channelId != null && merchantId != null) {
            if (channelId.equals(reqChannelId)) {
                flag = true;
            }
        }


        if (flag) {
            // 存在cookie，直接获取 广告信息 展示
            AdPurchase adPurchase = adPurchaseService.getById(Long.valueOf(merchantId));
            if (null == adPurchase) {
                // 路由 重新选择 并计费
                simpleResponse = adPageService.showAdPage(reqChannelId, userAgent, ipAddress);
                if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                    return simpleResponse;
                }
                Map resultVO = (Map) simpleResponse.getData();
                String adPurchaseId = resultVO.get("adPurchaseId").toString();
                String cookieValue = reqChannelId + "-" + adPurchaseId;
                Cookie cookie = new Cookie("adChannelKey", cookieValue);
                // 过期时间 1天（自然日）
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
                Date endTime = cal.getTime();
                int overTime = (int) ((endTime.getTime() - System.currentTimeMillis()) / 1000);
                cookie.setMaxAge(overTime);
                cookie.setPath("/");
                response.addCookie(cookie);

                return simpleResponse;
            }
            // 展示信息加工  获取查看了多少次
            int userCount = adUserService.count();
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("adPic", adPurchase.getAdPic());
            resultVO.put("userCount", userCount);
            resultVO.put("adPurchaseId", adPurchase.getId());
            simpleResponse.setStatusEnum(ApiStatusEnum.SUCCESS);
            simpleResponse.setData(resultVO);
            return simpleResponse;
        }

        // 不存在cookie， 路由选择 并计费
        simpleResponse = adPageService.showAdPage(reqChannelId, userAgent, ipAddress);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return simpleResponse;
        }
        Map resultVO = (Map) simpleResponse.getData();
        merchantId = resultVO.get("adPurchaseId").toString();

        // 设置 redis map值
        redisMapByUUID = new HashMap<>();
        redisMapByUUID.put(AdRedisKeys.CHANNEL, reqChannelId);
        redisMapByUUID.put(AdRedisKeys.MERCHANT, merchantId);

        // 写redis , 设置过期
        stringRedisTemplate.opsForHash().putAll(redisHashKey, redisMapByUUID);
        stringRedisTemplate.expire(redisHashKey, getEndTime(), TimeUnit.SECONDS);


        // 写Cookie , 给用户
        Cookie cookie = new Cookie("uuid", uuid);
        response.addCookie(cookie);
        return simpleResponse;
    }

    /**
     * 过期时间
     *
     * @return
     */
    private Long getEndTime() {
        // 过期时间 1天（自然日）
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date endTime = cal.getTime();
        Long overTime = ((endTime.getTime() - System.currentTimeMillis()) / 1000);
        return overTime;
    }

    /**
     * 根据uuid获取原cookies中的内容
     *
     * @param redisHashKey
     * @return
     */
    private Map getCookiesByUUID(String redisHashKey) {
        Map<Object, Object> mapInRedisByUUID = stringRedisTemplate.opsForHash().entries(redisHashKey);
        if (mapInRedisByUUID == null) {
            return null;
        }
        return mapInRedisByUUID;
    }

    /**
     * 获取uuid
     *
     * @param request
     * @return
     */
    private String getUUID(HttpServletRequest request) {

        String uuid = null;
        // 校验 cookie
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                String name = cookie.getName();
                if ("uuid".equals(name)) {
                    uuid = cookie.getValue();
                    break;
                }
            }
        } else {
            uuid = this.createUUID();
        }

        return uuid;
    }

    /**
     * 创建uuid
     *
     * @return
     */
    private String createUUID() {
        return System.currentTimeMillis() + "-" + RandomStrUtil.randomStr(7);
    }

}
