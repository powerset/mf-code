package mf.code.api.ad.dto;

import lombok.Data;

/**
 * mf.code.api.ad.dto
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-18 上午10:53
 */
@Data
public class AdPurchaseStatusReq {
    private String adPurchaseId;
    private String status;
}
