package mf.code.api.ad.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * mf.code.api.ad.dto
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-19 下午5:00
 */
@Data
public class AdChannelStatusReq {
    private String adChannelId;
    private String status;
    @JsonProperty("platformId")
    private String platUserId;
}
