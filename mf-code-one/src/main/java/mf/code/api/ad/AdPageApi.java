package mf.code.api.ad;

import lombok.extern.slf4j.Slf4j;
import mf.code.ad.repo.po.AdPurchase;
import mf.code.ad.service.AdPurchaseService;
import mf.code.ad.service.AdUserService;
import mf.code.api.ad.service.AdPageService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.ad
 * Description:
 *
 * @author: 百川
 * @date: 2018-12-18 下午1:33
 */
@Slf4j
@RestController
@RequestMapping("/api/ad/user")
public class AdPageApi {
    private final AdPageService adPageService;
    private final AdPurchaseService adPurchaseService;
    private final AdUserService adUserService;

    @Autowired
    public AdPageApi(AdPageService adPageService, AdPurchaseService adPurchaseService, AdUserService adUserService) {
        this.adPageService = adPageService;
        this.adPurchaseService = adPurchaseService;
        this.adUserService = adUserService;
    }

    @GetMapping("/showAdPage")
    public SimpleResponse showAdPage(@RequestParam("adChannelId") String adChannelId,
                                     HttpServletRequest request,
                                     HttpServletResponse response) {
        SimpleResponse<Object> simpleResponse = new SimpleResponse<>();

        String userAgent = request.getHeader("User-Agent");
        String ipAddress = IpUtil.getIpAddress(request);

        // 校验cookie
        String adChannelValue = null;
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                String name = cookie.getName();
                if ("adChannelKey".equals(name)) {
                    adChannelValue = cookie.getValue();
                    break;
                }
            }
        }

        // 校验是否是扫的同一个流量主二维码
        boolean flag = false;
        String acidInCookie;
        String apidInCookie = null;
        if (StringUtils.isNotBlank(adChannelValue)) {
            // 从 adChannelValue 获取流量主id 和 广告主购买批次编号
            String[] split = adChannelValue.split("-");
            acidInCookie = split[0];
            apidInCookie = split[1];
            if (StringUtils.isNotBlank(acidInCookie)
                    && RegexUtils.StringIsNumber(acidInCookie)
                    && StringUtils.isNotBlank(apidInCookie)
                    && RegexUtils.StringIsNumber(apidInCookie)
                    && adChannelId.equals(acidInCookie)) {
                flag = true;
            }
        }

        if (flag && StringUtils.isNotBlank(apidInCookie)) {
            // 存在cookie，直接获取 广告信息 展示
            AdPurchase adPurchase = adPurchaseService.getById(Long.valueOf(apidInCookie));
            if (null == adPurchase) {
                // 路由 重新选择 并计费
                simpleResponse = adPageService.showAdPage(adChannelId, userAgent, ipAddress);
                if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                    return simpleResponse;
                }
                Map resultVO = (Map)simpleResponse.getData();
                String adPurchaseId = resultVO.get("adPurchaseId").toString();
                String cookieValue = adChannelId+"-"+adPurchaseId;
                Cookie cookie = new Cookie("adChannelKey",cookieValue);
                // 过期时间 1天（自然日）
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
                Date endTime = cal.getTime();
                int overTime = (int)((endTime.getTime() - System.currentTimeMillis())/1000);
                cookie.setMaxAge(overTime);
                cookie.setPath("/");
                response.addCookie(cookie);

                return simpleResponse;
            }

            // 展示信息加工
            // 获取查看了多少次
            int userCount = adUserService.count();
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("adPic", adPurchase.getAdPic());
            resultVO.put("userCount", userCount);
            resultVO.put("adPurchaseId", adPurchase.getId());
            simpleResponse.setStatusEnum(ApiStatusEnum.SUCCESS);
            simpleResponse.setData(resultVO);
            return simpleResponse;
        }

        // 不存在cookie， 路由选择 并计费
        simpleResponse = adPageService.showAdPage(adChannelId, userAgent, ipAddress);
        if (simpleResponse.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return simpleResponse;
        }

        // 设置Cookie
        Map resultVO = (Map)simpleResponse.getData();
        String adPurchaseId = resultVO.get("adPurchaseId").toString();
        String cookieValue = adChannelId+"-"+adPurchaseId;
        Cookie cookie = new Cookie("adChannelKey",cookieValue);
        // 过期时间 1天（自然日）
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date endTime = cal.getTime();
        int overTime = (int)((endTime.getTime() - System.currentTimeMillis())/1000);


        //写redis
        cookie.setMaxAge(overTime);
        cookie.setPath("/");
        response.addCookie(cookie);
        return simpleResponse;
    }

}
