package mf.code.api.temp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.api.temp.service.AsyncTempService;
import mf.code.api.temp.service.TempService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantOrderService;
import mf.code.merchant.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.temp.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月28日 11:09
 */
@Service
public class TempServiceImpl implements TempService {
    @Autowired
    private AsyncTempService asyncTempService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantOrderService merchantOrderService;

    @Override
    public SimpleResponse oldDataCheckpoint() {
        asyncTempService.oldDataCheckpoint();
        return new SimpleResponse();
    }

    @Override
    public SimpleResponse savePlatformOrderByOldTaskData() {
        asyncTempService.savePlatformOrderByOldTaskData();
        return new SimpleResponse();
    }

    @Override
    public SimpleResponse addPurchaseMerchant(String phone, int time) {
        Merchant merchant = merchantService.getMerchantByPhone(phone);
        if (merchant == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该商户未注册");
        }

        //查看是否创建了商户订单
        QueryWrapper<MerchantOrder> merchantOrderQueryWrapper = new QueryWrapper<>();
        merchantOrderQueryWrapper.lambda()
                .eq(MerchantOrder::getMerchantId, merchant.getId())
                .eq(MerchantOrder::getBizType, BizTypeEnum.PURCHASE_PUBLIC.getCode())
        ;
        if (true) {

        }


        return null;
    }
}
