package mf.code.api.temp.service;

import org.springframework.scheduling.annotation.Async;

/**
 * mf.code.api.temp.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月28日 11:11
 */
public interface AsyncTempService {
    @Async
    void oldDataCheckpoint();

    @Async
    void savePlatformOrderByOldTaskData();
}
