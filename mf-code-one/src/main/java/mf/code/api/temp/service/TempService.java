package mf.code.api.temp.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.temp.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月28日 11:09
 */
public interface TempService {
    SimpleResponse oldDataCheckpoint();

    SimpleResponse savePlatformOrderByOldTaskData();

    SimpleResponse addPurchaseMerchant(String phone, int time);
}
