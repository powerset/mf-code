package mf.code.api.temp;

import mf.code.api.temp.service.TempService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.temp
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月28日 11:02
 */
@RestController
@RequestMapping(value = "/tempapi/applet")
public class TempApi {

    @Autowired
    private TempService tempService;

    @GetMapping(path = "/oldDataCheckpoint")
    public SimpleResponse oldDataCheckpoint() {
        return tempService.oldDataCheckpoint();
    }

    @GetMapping(path = "/savePlatformOrderByOldTaskData")
    public SimpleResponse savePlatformOrderByOldTaskData() {
        return tempService.savePlatformOrderByOldTaskData();
    }

    @GetMapping(path = "/addPurchaseMerchant")
    public SimpleResponse addPurchaseMerchant(@RequestParam("phone") String phone,
                                              @RequestParam("time") int time) {
        return tempService.addPurchaseMerchant(phone, time);
    }
}
