package mf.code.api.temp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.api.temp.service.AsyncTempService;
import mf.code.common.email.EmailService;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.distribution.constant.PlatformOrderBizTypeEnum;
import mf.code.distribution.constant.PlatformOrderStatusEnum;
import mf.code.distribution.constant.PlatformOrderTypeEnum;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.PlatformOrder;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.PlatformOrderService;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UserConstant;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.temp.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月28日 11:11
 */
@Service
@Slf4j
public class AsyncTempServiceImpl implements AsyncTempService {
    @Value("${checkpoint.oldDataTime}")
    private String oldDataTime;

    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserService userService;
    @Autowired
    private PlatformOrderService platformOrderService;

    private final int MAX_LIMIT = 30;

    @Autowired
    private DistributionAppService distributionAppService;

    @Async
    @Override
    public void oldDataCheckpoint() {
        int pageNum = 1;
        Date date = DateUtil.stringtoDate(oldDataTime, DateUtil.FORMAT_ONE);
        if (date == null) {
            log.info("<<<<<<<<<<<< 未配置日期 <<<<<<<<<<<");
            return;
        }
        QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .le(UserPubJoin::getCtime, date)
        ;
        for (; ; pageNum++) {
            Page<UserPubJoin> page = new Page<>(pageNum, MAX_LIMIT);
            IPage<UserPubJoin> userPubJoinIPage = userPubJoinService.page(page, wrapper);
            List<UserPubJoin> upperUserPubJoins = userPubJoinIPage.getRecords();
            if (CollectionUtils.isEmpty(upperUserPubJoins)) {
                //发邮件，清算结束
                emailService.sendSimpleMail("闯关老数据查询为空,终于结束啦", "冲鸭~~~");
                break;
            }

            for (UserPubJoin userPubJoin1 : upperUserPubJoins) {
                Long shopId = userPubJoin1.getShopId();
                Long userId = userPubJoin1.getSubUid();
                MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
                if (merchantShop == null) {
                    continue;
                }
                //查找该用户是否已经处理过旧数据
                UpayWxOrder upayWxOrderOne = getUpayWxOrderOne(shopId, userId);
                if (upayWxOrderOne != null) {
                    continue;
                }

                //获取收益
                List<UserPubJoin> userPubJoins = this.getUserPubJoin(shopId, userId, null);
                UserPubJoin userPubJoin = null;
                if (!CollectionUtils.isEmpty(userPubJoins)) {
                    userPubJoin = userPubJoins.get(0);
                }

                //获取字典表-税率信息and关卡信息
                List<CommonDict> commonDicts = this.commonDictService.selectListByType("checkpoint");
                //任务金额+缴税金额
                Map<String, Object> totalScottareParams = new HashMap<>();
                totalScottareParams.put("shopId", shopId);
                totalScottareParams.put("userId", userId);
                totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
                BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);

                //获取已通关数目
                List<UpayWxOrder> upayWxOrders = getUpayWxOrder(shopId, userId);
                CommonDict commonDict = null;
                for (CommonDict commonDict1 : commonDicts) {
                    if ("level".equals(commonDict1.getKey())) {
                        commonDict = commonDict1;
                    }
                }
                Map<Long, Map<String, Object>> checkpointMap = new HashMap<>();
                if (commonDict != null) {
                    List<Object> commonDictObjects = JSONObject.parseArray(commonDict.getValue(), Object.class);
                    for (Object obj : commonDictObjects) {
                        Map<String, Object> jsonMap = JSONObject.parseObject(obj.toString(), Map.class);
                        checkpointMap.put(Long.valueOf(jsonMap.get("name").toString()), jsonMap);
                    }
                }
                BigDecimal passTaskProfit = BigDecimal.ZERO;
                BigDecimal passScottareProfit = BigDecimal.ZERO;
                if (!CollectionUtils.isEmpty(upayWxOrders)) {
                    for (UpayWxOrder upayWxOrder : upayWxOrders) {
                        Map<String, Object> checkpoint = checkpointMap.get(upayWxOrder.getBizValue());
                        Map<String, Object> upayWxOrderJsonMap = JSONObject.parseObject(upayWxOrder.getRemark(), Map.class);
                        if (checkpoint != null && upayWxOrderJsonMap != null) {
                            passTaskProfit = passTaskProfit.add(new BigDecimal(upayWxOrderJsonMap.get("commission").toString()));
                            passScottareProfit = passScottareProfit.add(new BigDecimal(upayWxOrderJsonMap.get("scottare").toString()));
                        }
                    }
                }

                BigDecimal taskProfit = BigDecimal.ZERO;
                BigDecimal scottareProfit = BigDecimal.ZERO;
                if (userPubJoin != null) {
                    taskProfit = userPubJoin.getTotalCommission().subtract(passTaskProfit).setScale(2, BigDecimal.ROUND_DOWN);
                }
                scottareProfit = allMinerTotalScottare.subtract(passScottareProfit).setScale(2, BigDecimal.ROUND_DOWN);

                BigDecimal sum = taskProfit.add(scottareProfit);
                if (sum.compareTo(BigDecimal.ZERO) > 0) {
                    //存储
                    UpayWxOrder upayWxOrderUpper = upayWxOrderService.addPo(userId, OrderPayTypeEnum.CASH.getCode(),
                            OrderTypeEnum.PALTFORMRECHARGE.getCode(), null, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getMessage(),
                            merchantShop.getMerchantId(), shopId, OrderStatusEnum.ORDERED.getCode(), sum,
                            IpUtil.getServerIp(), null, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getMessage(), new Date(), null, null,
                            BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode(), 0L);
                    upayWxOrderService.create(upayWxOrderUpper);
                    // 更新用户余额
                    upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), userId, sum);
                }
            }
        }
    }

    @Async
    @Override
    public void savePlatformOrderByOldTaskData() {
        int pageNum = 1;

        //获取平台用户
        Map<String, Object> params = new HashMap<>(4);
        params.put("merchantId", 0);
        params.put("shopId", 0);
        params.put("grantStatus", UserConstant.GRANT_STATUS_PLAT);
        List<User> users = userService.listPageByParams(params);
        User platUser = users.get(0);

        String dateStr = "2019-06-29 10:30:00";
        Date date = DateUtil.stringtoDate(dateStr, DateUtil.FORMAT_ONE);

        List<Integer> list = new ArrayList<>();
        list.add(UserCouponTypeEnum.LUCKY_WHEEL.getCode());
        list.add(UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
        list.add(UserCouponTypeEnum.GOODS_COMMENT.getCode());
        list.add(UserCouponTypeEnum.FAV_CART.getCode());
        list.add(UserCouponTypeEnum.MCH_PLAN.getCode());
        list.add(UserCouponTypeEnum.ORDER_BACK_START.getCode());
        list.add(UserCouponTypeEnum.ORDER_BACK_JOIN.getCode());
        list.add(UserCouponTypeEnum.NEW_MAN_START.getCode());
        list.add(UserCouponTypeEnum.NEW_MAN_JOIN.getCode());
        list.add(UserCouponTypeEnum.ASSIST.getCode());
        list.add(UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode());
        list.add(UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode());
        list.add(UserCouponTypeEnum.ORDER_BACK_START_V2.getCode());
        list.add(UserCouponTypeEnum.ORDER_BACK_JOIN_V2.getCode());
        list.add(UserCouponTypeEnum.ASSIST_V2.getCode());
        list.add(UserCouponTypeEnum.ORDER_RED_PACKET_V2.getCode());
        list.add(UserCouponTypeEnum.GOODS_COMMENT_V2.getCode());
        list.add(UserCouponTypeEnum.FAV_CART_V2.getCode());
        list.add(UserCouponTypeEnum.FAV_CART_V2.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode());
        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(UserCoupon::getType, list)
                .gt(UserCoupon::getCommission, BigDecimal.ZERO)
        ;
        for (; ; pageNum++) {
            Page<UserCoupon> page = new Page<>(pageNum, MAX_LIMIT);
            IPage<UserCoupon> userCouponIPage = userCouponService.page(page, wrapper);
            List<UserCoupon> userCoupons = userCouponIPage.getRecords();
            if (CollectionUtils.isEmpty(userCoupons)) {
                //发邮件，清算结束
                emailService.sendSimpleMail("任务系列平台订单查询为空,终于结束啦", "冲鸭~~~");
                break;
            }

            List<Long> userIds = new ArrayList<>();
            for (UserCoupon userCoupon : userCoupons) {
                if (userIds.indexOf(userCoupon.getUserId()) == -1) {
                    userIds.add(userCoupon.getUserId());
                }
            }

            QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
            userPubJoinQueryWrapper.lambda()
                    .eq(UserPubJoin::getType, UserPubJoinTypeEnum.SHOPPINGMALL.getCode())
                    .in(UserPubJoin::getSubUid, userIds)
            ;
            List<UserPubJoin> userPubJoins = userPubJoinService.list(userPubJoinQueryWrapper);
            if (CollectionUtils.isEmpty(userPubJoins)) {
                continue;
            }
            Map<Long, UserPubJoin> userPubJoinMap = new HashMap<>();
            for (UserPubJoin userPubJoin : userPubJoins) {
                userPubJoinMap.put(userPubJoin.getSubUid(), userPubJoin);
            }

            for (UserCoupon userCoupon : userCoupons) {
                //查看是否已经处理过
                QueryWrapper<PlatformOrder> platformOrderQueryWrapper = new QueryWrapper<>();
                platformOrderQueryWrapper.lambda()
                        .eq(PlatformOrder::getType, PlatformOrderTypeEnum.PALTFORM_PRESTORE.getCode())
                        .eq(PlatformOrder::getStatus, PlatformOrderStatusEnum.SUCCESS.getCode())
                        .notIn(PlatformOrder::getBizType, Arrays.asList(
                                PlatformOrderBizTypeEnum.TRANSACTION.getCode(),
                                PlatformOrderBizTypeEnum.SPECS_COMMISSION.getCode(),
                                PlatformOrderBizTypeEnum.USER_COMMISSION_RECYCLING.getCode(),
                                PlatformOrderBizTypeEnum.USER_COMMISSION_TAX.getCode(),
                                PlatformOrderBizTypeEnum.USER_TEAM_REWARD.getCode()
                        ))
                        .eq(PlatformOrder::getBizValue, userCoupon.getId());
                PlatformOrder exit = platformOrderService.getOne(platformOrderQueryWrapper);
                if (exit != null) {
                    continue;
                }

                UserPubJoin userPubJoin = userPubJoinMap.get(userCoupon.getUserId());
                if (userPubJoin == null) {
                    continue;
                }
                //判断上级是否是平台
                if (!userPubJoin.getUserId().equals(platUser.getId())) {
                    continue;
                }
                if (StringUtils.isBlank(userCoupon.getCommissionDef())) {
                    continue;
                }
                //若是转盘的话，2019-06-29 10:30:00 之后的数据不做处理
                if (UserCouponTypeEnum.LUCKY_WHEEL.getCode() == userCoupon.getType()) {
                    if (userCoupon.getCtime().getTime() >= date.getTime()) {
                        continue;
                    }
                }

                //获取该用户缴税佣金
                Map<String, Object> jsonMap = JSONObject.parseObject(userCoupon.getCommissionDef(), Map.class);
                BigDecimal commissionTax = BigDecimal.ZERO;
                if (jsonMap != null && StringUtils.isNotBlank(jsonMap.get("tax").toString())) {
                    commissionTax = new BigDecimal(jsonMap.get("tax").toString());
                }

                if (commissionTax.compareTo(BigDecimal.ZERO) <= 0) {
                    continue;
                }
                PlatformOrderBizTypeEnum platformOrderBizTypeEnum = PlatformOrderBizTypeEnum.findByUserCouponType(userCoupon.getType());
                //是平台,补充平台的订单
                PlatformOrder platformOrder = new PlatformOrder();
                platformOrder.setMerchantId(userCoupon.getMerchantId());
                platformOrder.setShopId(userCoupon.getShopId());
                platformOrder.setUserId(userCoupon.getUserId());
                platformOrder.setType(PlatformOrderTypeEnum.PALTFORM_PRESTORE.getCode());
                platformOrder.setPayType(1);
                platformOrder.setOrderNo("V5" + RandomStrUtil.randomStr(6) + System.currentTimeMillis());
                platformOrder.setOrderName(platformOrderBizTypeEnum.getDesc());
                platformOrder.setPubAppId("");
                platformOrder.setStatus(PlatformOrderStatusEnum.SUCCESS.getCode());
                platformOrder.setTotalFee(commissionTax);
                platformOrder.setTradeType(0);
                platformOrder.setTradeId("");
                platformOrder.setRemark(null);
                platformOrder.setNotifyTime(new Date());
                platformOrder.setPaymentTime(new Date());
                platformOrder.setCtime(new Date());
                platformOrder.setUtime(new Date());
                platformOrder.setReqJson(null);
                platformOrder.setBizType(platformOrderBizTypeEnum.getCode());
                platformOrder.setBizValue(userCoupon.getId());
                platformOrderService.save(platformOrder);
            }
        }
    }

    /***
     * 查询是否已经处理过旧数据
     * @param shopId
     * @param userId
     * @return
     */
    private UpayWxOrder getUpayWxOrderOne(Long shopId, Long userId) {
        QueryWrapper<UpayWxOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayWxOrder::getShopId, shopId)
                .eq(UpayWxOrder::getUserId, userId)
                .eq(UpayWxOrder::getType, OrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .eq(UpayWxOrder::getBizType, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode())
                .orderByDesc(UpayWxOrder::getId)
        ;
        return this.upayWxOrderService.getOne(wrapper);
    }

    /***
     * 获取已通关的订单信息
     * @param shopId
     * @param userId
     * @return
     */
    private List<UpayWxOrder> getUpayWxOrder(Long shopId, Long userId) {
        QueryWrapper<UpayWxOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayWxOrder::getShopId, shopId)
                .eq(UpayWxOrder::getUserId, userId)
                .eq(UpayWxOrder::getType, OrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .eq(UpayWxOrder::getBizType, BizTypeEnum.CHECKPOINT_OLD_DATA_CLEANED.getCode())
                .orderByDesc(UpayWxOrder::getId)
        ;
        return this.upayWxOrderService.list(wrapper);
    }


    /***
     * 获取累计缴税
     * @param shopId
     * @param userId
     * @return
     */
    private List<UserPubJoin> getUserPubJoin(Long shopId, Long userId, Long bossUserId) {
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getShopId, shopId)
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
        ;
        if (userId != null && userId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("sub_uid", userId));
        }
        if (bossUserId != null && bossUserId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("user_id", bossUserId));
        }
        return this.userPubJoinService.list(userPubJoinQueryWrapper);
    }
}
