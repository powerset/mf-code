package mf.code.api;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class MybatisPageDto<T> {

	private List<Object> content = new ArrayList<>();
	private long size;// 每页条数
	private long total;// 总条数
	private long currentPage;// 当前页
	private long pages;//总页码

	public MybatisPageDto() {
	}

	public MybatisPageDto(long current, long size, long total, long pages){
		this.size = size;
		this.total = total;
		this.currentPage = current;
		this.pages = pages;
		this.content = new ArrayList<>();
	}

	public void from(long current, long size, long total, long pages) {
		this.size = size;
		this.total = total;
		this.currentPage = current;
		this.pages = pages;
	}
}
