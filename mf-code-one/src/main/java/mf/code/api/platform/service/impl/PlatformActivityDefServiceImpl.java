package mf.code.api.platform.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefAuditTypeEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.platform.service.PlatformActivityDefService;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.PageUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.platform.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-17 下午2:01
 */
@Slf4j
@Service
public class PlatformActivityDefServiceImpl implements PlatformActivityDefService {
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityService activityService;

    @Override
    public SimpleResponse listApplyActivityDef(String pageSize, String currentPage, String types, String applyStartTime, String applyEndTime, String depositStatus, String shopId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(types)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        // redis 缓存

        String[] typeArray = types.trim().split(",");
        List<Integer> typeList = new ArrayList<>();
        for (String type : typeArray) {
            typeList.add(Integer.valueOf(type));
        }

        Integer size = Integer.valueOf(pageSize);
        Integer page = Integer.valueOf(currentPage);

        // 定义查询条件
        Map<String, Object> param = new HashMap<>(16);
        param.put("types", typeList);
        param.put("stockDef", 0);
        if (StringUtils.isNotBlank(applyStartTime)) {
            param.put("applyStartTime", DateUtil.stringtoDate(applyStartTime, DateUtil.FORMAT_TWO));
        }
        if (StringUtils.isNotBlank(applyEndTime)) {
            param.put("applyEndTime", DateUtil.stringtoDate(applyEndTime, DateUtil.FORMAT_TWO));
        }
        if (StringUtils.isNotBlank(depositStatus)) {
            param.put("depositStatus", Integer.valueOf(depositStatus));
        }
        if (StringUtils.isNotBlank(shopId)) {
            param.put("shopId", Long.valueOf(shopId));
        }
        param.put("del", DelEnum.NO.getCode());

        // 查询总记录数
        Integer count = activityDefAuditService.countByParam(param);

        // 信息加工
        HashMap<String, Object> resultVO = new HashMap<>(16);
        resultVO.put("count", count);
        resultVO.put("page", page);
        resultVO.put("size", size);

        if (count == 0) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        // 设置分页查询条件--活动展示列表
        param.putAll(PageUtil.pageSizeTolimit(page, size, count));
        param.put("order", "id");
        param.put("direct", "desc");
        // 分页查询 活动列表
        List<ActivityDefAudit> activityDefAudits = activityDefAuditService.listPageByParams(param);

        List<Map<String, Object>> activityDefAuditVOs = new ArrayList<>();
        log.info(activityDefAuditVOs.toString());
        for (ActivityDefAudit activityDefAudit : activityDefAudits) {
            Map<String, Object> activityDefAuditVO = new HashMap<>();
            activityDefAuditVO.put("activityDefId", activityDefAudit.getActivityDefId());
            activityDefAuditVO.put("activityDefAuditId", activityDefAudit.getId());
            Merchant merchant = merchantService.getMerchant(activityDefAudit.getMerchantId());
            if (merchant == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("没有查到商户信息");
                return  response;
            }
            activityDefAuditVO.put("phone", merchant.getPhone());
            activityDefAuditVO.put("applyTime", DateUtil.dateToString(activityDefAudit.getApplyTime(), DateUtil.FORMAT_TWO));
            MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityDefAudit.getShopId());
            if (merchantShop == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("没有查到商户店铺信息");
                return  response;
            }
            activityDefAuditVO.put("shopId", activityDefAudit.getShopId());
            activityDefAuditVO.put("shopName", merchantShop.getShopName());
            Integer type = activityDefAudit.getType();
            activityDefAuditVO.put("depositStatus", activityDefAudit.getDepositStatus());
            BigDecimal depositApply = activityDefAudit.getDepositApply();
            activityDefAuditVO.put("applyDeposit", depositApply);
            activityDefAuditVO.put("applyStock", activityDefAudit.getStockApply());
            activityDefAuditVO.put("type", type);

            BigDecimal taskRpDepositApply = activityDefAudit.getTaskRpDepositApply();
            if (taskRpDepositApply == null) {
                taskRpDepositApply = BigDecimal.ZERO;
            }
            activityDefAuditVO.put("applyTaskRpDeposit", taskRpDepositApply);
            activityDefAuditVO.put("applyTaskRpNum", activityDefAudit.getTaskRpNumApply());

            if (activityDefAudit.getDepositStatus() == DepositStatusEnum.AWAITING_PAYMENT.getCode()
                    && (activityDefAudit.getApplyTime().getTime() + 7*24*3600*1000) < System.currentTimeMillis()) {
                activityDefAuditVO.put("depositStatus", DepositStatusEnum.INVALID.getCode());
            }
            if (type == ActivityDefAuditTypeEnum.NEW_MAN.getCode() || type == ActivityDefAuditTypeEnum.ORDER_BACK.getCode()) {
                activityDefAuditVO.put("goodsId", activityDefAudit.getGoodsId());
            }
            if (type == ActivityDefAuditTypeEnum.FILL_BACK_ORDER.getCode()) {
                activityDefAuditVO.put("goodsSize", JSON.parseArray(activityDefAudit.getGoodsIds()).size());
            }
            activityDefAuditVO.put("applyTotalDeposit", taskRpDepositApply.add(depositApply));
            activityDefAuditVOs.add(activityDefAuditVO);
        }

        resultVO.put("activityDefAuditVOs", activityDefAuditVOs);


        // 返回结果
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse queryGoodsInfo(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        Long activityDefID = Long.valueOf(activityDefId);
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefID);
        if (null == activityDef) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        Long goodsId = activityDef.getGoodsId();
        String goodsIds = activityDef.getGoodsIds();
        List<Map<String, Object>> listVO = new ArrayList<>();
        if (null != goodsId) {
            Goods goods = goodsService.selectById(goodsId);
            Map<String, Object> resultVO = new HashMap<>(16);
            resultVO.put("title", goods.getDisplayGoodsName());
            resultVO.put("price", goods.getDisplayPrice());
            resultVO.put("keyWord", activityDef.getKeyWord());
            listVO.add(resultVO);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(listVO);
            return response;
        }
        if (StringUtils.isNotBlank(goodsIds) && JsonParseUtil.booJsonArr(goodsIds)) {
            List<Long> goodsIdList = JSON.parseArray(goodsIds, Long.class);
            List<Goods> goodsList = goodsService.selectByIdList(goodsIdList);

            for (Goods goods : goodsList) {
                HashMap<String, Object> goodsVO = new HashMap<>(16);
                goodsVO.put("title", goods.getDisplayGoodsName());
                goodsVO.put("price", goods.getDisplayPrice());
                listVO.add(goodsVO);
            }
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(listVO);
            return  response;
        }
        response.setStatusEnum(ApiStatusEnum.ERROR_DB);
        return response;
    }

    @Override
    public SimpleResponse queryActivityDefStockInfo(String activityDefId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        Long activityDefID = Long.valueOf(activityDefId);
        Map<String, Object> param = new HashMap<>(16);
        param.put("activityDefId", activityDefID);
        param.put("status" , ActivityDefAuditStatusEnum.PASS.getCode());
        param.put("order", "apply_time");
        param.put("direct", "desc");
        List<ActivityDefAudit> activityDefAudits = activityDefAuditService.listPageByParams(param);
        List<Map<String, Object>> listVO = new ArrayList<>();
        for (ActivityDefAudit activityDefAudit : activityDefAudits) {
            HashMap<String, Object> activityDefAuditVO = new HashMap<>(16);
            activityDefAuditVO.put("applyTime", activityDefAudit.getApplyTime());
            activityDefAuditVO.put("applyStock", activityDefAudit.getStockApply());
            activityDefAuditVO.put("applyDeposit", activityDefAudit.getDepositApply());
            activityDefAuditVO.put("status", activityDefAudit.getStatus());
            activityDefAuditVO.put("actualStock", activityDefAudit.getStockActual());
            activityDefAuditVO.put("actualDeposit", activityDefAudit.getDepositActual());
            activityDefAuditVO.put("voucher", activityDefAudit.getVoucher());
            activityDefAuditVO.put("initStock", activityDefAudit.getStockDef());
            activityDefAuditVO.put("finalStock", activityDefAudit.getStockFinal());
            listVO.add(activityDefAuditVO);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(listVO);
        return response;
    }

    @Override
    public SimpleResponse activityDefAudit(String platUserId, String activityDefAuditId, String activityDefId,
                                           String actualTotalDeposit, String actualDeposit, String actualStock,
                                           String actualTaskRpDeposit, String actualTaskRpNum, String voucher,
                                           String type) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        if (StringUtils.isBlank(platUserId)
                || StringUtils.isBlank(activityDefAuditId)
                || StringUtils.isBlank(actualTotalDeposit)
                || StringUtils.isBlank(actualDeposit)
                || StringUtils.isBlank(actualStock)
                || StringUtils.isBlank(actualTaskRpDeposit)
                || StringUtils.isBlank(actualTaskRpNum)
                || StringUtils.isBlank(voucher)
                || StringUtils.isBlank(type)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        ActivityDefAudit activityDefAudit = activityDefAuditService.selectByPrimaryKey(Long.valueOf(activityDefAuditId));
        if (activityDefAudit == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        // 更新审核记录表

        BigDecimal depositActual = activityDefAudit.getDepositApply();
        BigDecimal taskRpDepositActual = activityDefAudit.getTaskRpDepositApply();

        Integer taskRpNumActual = activityDefAudit.getTaskRpNumApply();
        Integer stockActual = activityDefAudit.getStockApply();


        Date now = new Date();
        activityDefAudit.setId(Long.valueOf(activityDefAuditId));
        activityDefAudit.setPlatUserId(Long.valueOf(platUserId));

        activityDefAudit.setStockActual(stockActual);
        activityDefAudit.setDepositActual(depositActual);
        activityDefAudit.setTaskRpDepositActual(taskRpDepositActual);
        activityDefAudit.setTaskRpNumActual(taskRpNumActual);
        activityDefAudit.setStockFinal(stockActual);
        activityDefAudit.setDepositFinal(depositActual);
        activityDefAudit.setTaskRpDepositFinal(taskRpDepositActual);
        activityDefAudit.setTaskRpNumFinal(taskRpNumActual);

        activityDefAudit.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        activityDefAudit.setVoucher(voucher);

        activityDefAudit.setPublishTime(now);
        activityDefAudit.setAuditTime(now);
        activityDefAudit.setStatus(ActivityDefAuditStatusEnum.PASS.getCode());
        activityDefAudit.setUtime(now);
        Integer activityDefAuditRows = activityDefAuditService.updateByPrimaryKeySelective(activityDefAudit);
        if (activityDefAuditRows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：更新activity_def_audit失败");
            return response;
        }


        ActivityDef activityDef = new ActivityDef();
        activityDef.setId(Long.valueOf(activityDefId));
        activityDef.setStatus(ActivityDefStatusEnum.PUBLISHED.getCode());
        activityDef.setStartTime(now);
        activityDef.setEndTime(DateUtils.addYears(now, 100));
        activityDef.setDepositDef(depositActual);
        activityDef.setStockDef(stockActual);
        activityDef.setStock(stockActual);
        activityDef.setDeposit(depositActual);
        activityDef.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        activityDef.setVoucher(voucher);
        activityDef.setPublishTime(now);
        activityDef.setTaskRpNumDef(taskRpNumActual);
        activityDef.setTaskRpNum(taskRpNumActual);
        activityDef.setTaskRpDepositDef(taskRpDepositActual);
        activityDef.setTaskRpDeposit(taskRpDepositActual);
        activityDef.setTotalDeposit(depositActual.add(taskRpDepositActual));
        activityDef.setAuditingTime(now);
        activityDef.setUtime(now);
        Integer ActivityDefRows = activityDefService.updateActivityDef(activityDef);
        if (ActivityDefRows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：更新activity_def表出错");
            return response;
        }

        if (Integer.parseInt(type) == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            ActivityDef activityDefDB = activityDefService.selectByPrimaryKey(Long.valueOf(activityDefId));
            Activity activity = new Activity();
            activity.setMerchantId(activityDefDB.getMerchantId());
            activity.setShopId(activityDefDB.getShopId());
            activity.setActivityDefId(activityDefDB.getId());
            activity.setType(ActivityTypeEnum.LUCK_WHEEL.getCode());
            activity.setStatus(ActivityStatusEnum.PUBLISHED.getCode());
            activity.setStartTime(now);
            activity.setEndTime(DateUtils.addYears(now, 100));
            activity.setCtime(now);
            activity.setUtime(now);
            activity.setWeighting(activityDefDB.getWeighting());
            int activityRows = activityService.add(activity);
            if (activityRows == 0) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("数据库异常：新增activity表出错");
                return response;
            }
        }

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public SimpleResponse listStockAudit(String pageSize, String currentPage, String type, String applyStartTime, String applyEndTime, String depositStatus, String shopId) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        Integer size = Integer.valueOf(pageSize);
        Integer page = Integer.valueOf(currentPage);

        // 定义查询条件
        Map<String, Object> param = new HashMap<>(16);
        if (StringUtils.isNotBlank(type)) {
            param.put("type", type);
        }
        if (StringUtils.isNotBlank(applyStartTime)) {
            param.put("applyStartTime", DateUtil.stringtoDate(applyStartTime, DateUtil.FORMAT_TWO));
        }
        if (StringUtils.isNotBlank(applyEndTime)) {
            param.put("applyEndTime", DateUtil.stringtoDate(applyEndTime, DateUtil.FORMAT_TWO));
        }
        if (StringUtils.isNotBlank(depositStatus)) {
            param.put("depositStatus", Integer.valueOf(depositStatus));
        }
        if (StringUtils.isNotBlank(shopId)) {
            param.put("shopId", Integer.valueOf(shopId));
        }
        param.put("stockDefUnexpect",0);
        param.put("del", 0);

        // 查询总记录数
        Integer count = activityDefAuditService.countByParam(param);

        // 信息加工
        HashMap<String, Object> resultVO = new HashMap<>(16);
        resultVO.put("count", count);
        resultVO.put("page", page);
        resultVO.put("size", size);

        if (count == 0) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        // 设置分页查询条件--活动展示列表
        param.putAll(PageUtil.pageSizeTolimit(page, size, count));
        param.put("order", "id");
        param.put("direct", "desc");
        // 分页查询 活动列表
        List<ActivityDefAudit> activityDefAudits = activityDefAuditService.listPageByParams(param);

        List<Map<String, Object>> activityDefAuditVOs = new ArrayList<>();
        if (!CollectionUtils.isEmpty(activityDefAudits)) {
            for (ActivityDefAudit activityDefAudit : activityDefAudits) {
                Map<String, Object> activityDefAuditVO = new HashMap<>();
                activityDefAuditVO.put("activityDefId", activityDefAudit.getActivityDefId());
                activityDefAuditVO.put("activityDefAuditId", activityDefAudit.getId());
                Merchant merchant = merchantService.getMerchant(activityDefAudit.getMerchantId());
                activityDefAuditVO.put("phone", merchant.getPhone());
                activityDefAuditVO.put("applyTime", DateUtil.dateToString(activityDefAudit.getApplyTime(), DateUtil.FORMAT_TWO));
                MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityDefAudit.getShopId());
                if (merchantShop == null) {
                    response.setStatusEnum(ApiStatusEnum.ERROR);
                    response.setMessage("查不到该申请的店铺信息");
                    return response;
                }
                activityDefAuditVO.put("shopId",activityDefAudit.getShopId());
                activityDefAuditVO.put("shopName", merchantShop.getShopName());
                activityDefAuditVO.put("goodsId", activityDefAudit.getGoodsId());
                activityDefAuditVO.put("goodsIds", activityDefAudit.getGoodsIds());
                ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefAudit.getActivityDefId());
                if (activityDef == null) {
                    response.setStatusEnum(ApiStatusEnum.ERROR);
                    response.setMessage("查不到商户创建的该活动信息");
                    return response;
                }

                BigDecimal taskRpDepositApply = activityDefAudit.getTaskRpDepositApply();
                if (taskRpDepositApply == null) {
                    taskRpDepositApply = BigDecimal.ZERO;
                }
                activityDefAuditVO.put("applyTaskRpDeposit", taskRpDepositApply);
                activityDefAuditVO.put("applyTaskRpNum", activityDefAudit.getTaskRpNumApply());

                activityDefAuditVO.put("leftStock", activityDef.getStock());
                activityDefAuditVO.put("type", activityDef.getType());
                activityDefAuditVO.put("applyStock", activityDefAudit.getStockApply());
                BigDecimal depositApply = activityDefAudit.getDepositApply();
                activityDefAuditVO.put("applyDeposit", depositApply);
                activityDefAuditVO.put("depositStatus", activityDefAudit.getDepositStatus());
                activityDefAuditVO.put("actualStock", activityDefAudit.getStockActual());
                activityDefAuditVO.put("voucher", activityDefAudit.getVoucher());

                activityDefAuditVO.put("applyTotalDeposit", taskRpDepositApply.add(depositApply));

                activityDefAuditVOs.add(activityDefAuditVO);
            }
        }

        resultVO.put("activityDefAuditVOs", activityDefAuditVOs);

        // 返回结果
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse activityDefStockAudit(String activityDefAuditId, String activityDefId, String actualDeposit, String actualStock, String voucher, String actualTotalDeposit, String platUserId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefAuditId)
                || StringUtils.isBlank(platUserId)
                || StringUtils.isBlank(activityDefId)
                || StringUtils.isBlank(actualDeposit)
                || StringUtils.isBlank(actualStock)
                || StringUtils.isBlank(actualTotalDeposit)
                || StringUtils.isBlank(voucher)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        ActivityDefAudit activityDefAudit = activityDefAuditService.selectByPrimaryKey(Long.valueOf(activityDefAuditId));
        if (activityDefAudit == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }


        /*BigDecimal depositActual = new BigDecimal(actualDeposit);*/
        BigDecimal depositActual = activityDefAudit.getDepositApply();

        BigDecimal depositDef = activityDefAudit.getDepositDef();
        Integer stockDef = activityDefAudit.getStockDef();

        BigDecimal depositFinal = depositDef.add(depositActual);
        Integer stockActual = activityDefAudit.getStockApply();
        int stockFinal = stockDef + stockActual;

        activityDefAudit.setPlatUserId(Long.valueOf(platUserId));
        activityDefAudit.setDepositActual(depositActual);
        activityDefAudit.setStockActual(stockActual);
        activityDefAudit.setDepositFinal(depositFinal);
        activityDefAudit.setStockFinal(stockFinal);
        activityDefAudit.setVoucher(voucher);
        activityDefAudit.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        activityDefAudit.setStatus(ActivityDefAuditStatusEnum.PASS.getCode());
        Date now = new Date();
        activityDefAudit.setAuditTime(now);
        activityDefAudit.setUtime(now);
        Integer rows = activityDefAuditService.updateByPrimaryKeySelective(activityDefAudit);
        if (rows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }

        Integer activityDefRows = activityDefService.updateDepositDefAndStockDef(Long.valueOf(activityDefId), depositActual, stockActual);
        if (activityDefRows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    @Override
    public SimpleResponse queryActivityDefStockVoucher(String activityDefAuditId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(activityDefAuditId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        ActivityDefAudit activityDefAudit = activityDefAuditService.selectByPrimaryKey(Long.valueOf(activityDefAuditId));
        HashMap<String, Object> resultVO = new HashMap<>();
        resultVO.put("voucher", activityDefAudit.getVoucher());
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    @Override
    public SimpleResponse commitVoucher(String platUserId, String activityDefAuditId, String voucher) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (StringUtils.isBlank(platUserId)
                || StringUtils.isBlank(activityDefAuditId)
                || StringUtils.isBlank(voucher)
                || !RegexUtils.StringIsNumber(platUserId)
                || !RegexUtils.StringIsNumber(activityDefAuditId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("请求参数异常：请检查platUserId,activityDefAuditId,voucher是否未设置");
            return response;
        }
        ActivityDefAudit activityDefAudit = activityDefAuditService.selectByPrimaryKey(Long.valueOf(activityDefAuditId));
        if (activityDefAudit == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("提交付款凭证失败：无此活动审核记录");
            return response;
        }
        Date now = new Date();
        if (activityDefAudit.getStockDef() == 0) {
            // 提交创建活动的付款截图
            Long activityDefId = activityDefAudit.getActivityDefId();
            ActivityDef activityDef = new ActivityDef();
            activityDef.setId(activityDefId);
            activityDef.setVoucher(voucher);
            activityDef.setUtime(now);
            Integer rows = activityDefService.updateActivityDef(activityDef);
            if (rows == 0) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("提交付款凭证失败：无法更新活动定义表");
                return response;
            }
        }
        // 提交补库存的付款截图
        activityDefAudit.setVoucher(voucher);
        activityDefAudit.setPlatUserId(Long.valueOf(platUserId));
        activityDefAudit.setUtime(now);
        Integer rows = activityDefAuditService.updateByPrimaryKeySelective(activityDefAudit);
        if (rows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("提交付款凭证失败：无法更新审核记录表");
            return response;
        }

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

}
