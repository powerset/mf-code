package mf.code.api.platform;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.api.platform.dto.PlatformActivityDefReq;
import mf.code.api.platform.service.PlatformOrderBackDefService;
import mf.code.common.job.TimeCreateActivityScheduledService;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.platform
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 14:19
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/platform/v1/activity/def")
public class PlatformOrderBackApi {
    @Autowired
    private PlatformOrderBackDefService platformOrderBackDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 创建 平台免单抽奖活动 -- 7.22 联合营销
     *
     * @param platformActivityDefReq
     * @return
     */
    @PostMapping(value = "/createActivityDef")
    public SimpleResponse createPlatformOrderBackActivity(@RequestBody PlatformActivityDefReq platformActivityDefReq) {
        if (platformActivityDefReq == null) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        String goodsId = platformActivityDefReq.getGoodsId();
        String skuId = platformActivityDefReq.getSkuId();
        String stockDef = platformActivityDefReq.getStockDef();
        String activityDefType = platformActivityDefReq.getActivityDefType();
        if (StringUtils.isBlank(goodsId) || !RegexUtils.StringIsNumber(goodsId)
                || StringUtils.isBlank(skuId) || !RegexUtils.StringIsNumber(skuId)
                || StringUtils.isBlank(stockDef) || !RegexUtils.StringIsNumber(stockDef)
                || StringUtils.isBlank(activityDefType) || !RegexUtils.StringIsNumber(activityDefType)) {
            log.error("参数异常, goodsId = {}, stockDef = {}", goodsId, stockDef);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        // 防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.CREATE_ACTIVITY_DEF_FORBID_REPEAT + skuId + ":" + 0);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "点击太快，请稍候再试");
        }
        SimpleResponse simpleResponse = platformOrderBackDefService.createPlatformOrderBackActivityDef(Long.valueOf(goodsId), Long.valueOf(skuId), Integer.valueOf(stockDef), Integer.valueOf(activityDefType));
        stringRedisTemplate.delete(RedisKeyConstant.CREATE_ACTIVITY_DEF_FORBID_REPEAT + skuId + ":" + 0);
        return simpleResponse;
    }

    /**
     * 挂起或启动 平台免单抽奖活动 -- 7.22 联合营销
     * @param activityDefId
     * @param status
     * @return
     */
    @GetMapping(value = "/startOrStopActivityDef")
    public SimpleResponse startOrStopActivityDef(@RequestParam("activityDefId") String activityDefId,
                                                 @RequestParam("status") String status) {
        if (StringUtils.isBlank(activityDefId) || !RegexUtils.StringIsNumber(activityDefId)
                || StringUtils.isBlank(status) || !RegexUtils.StringIsNumber(status)) {
            log.error("参数异常, activityDefId = {}, status = {}", activityDefId, status);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return platformOrderBackDefService.startOrStopActivityDef(Long.valueOf(activityDefId), Integer.valueOf(status));
    }

    /**
     * 查询 平台免单抽奖活动 列表页
     * @return
     */
    @GetMapping(value = "/queryActivityDefListPage")
    public SimpleResponse queryActivityDefListPage(@RequestParam("activityDefType") String activityDefType,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") String pageSize,
                                                   @RequestParam(value = "currentPage", defaultValue = "1") String currentPage) {
        if (StringUtils.isBlank(activityDefType) || !RegexUtils.StringIsNumber(activityDefType)) {
            log.error("参数异常, activityDeftype = {}", activityDefType);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return platformOrderBackDefService.queryActivityDefListPage(Integer.valueOf(activityDefType), Long.valueOf(pageSize), Long.valueOf(currentPage));
    }
}
