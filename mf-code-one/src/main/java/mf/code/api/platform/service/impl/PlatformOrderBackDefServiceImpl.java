package mf.code.api.platform.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefAuditTypeEnum;
import mf.code.activity.constant.ActivityDefFinishTypeEnum;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.MybatisPageDto;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.api.platform.service.PlatformOrderBackDefService;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.dto.ProductSkuEntity;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.platform.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 14:29
 */
@Slf4j
@Service
public class PlatformOrderBackDefServiceImpl implements PlatformOrderBackDefService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private ApolloPropertyService apolloPropertyService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;

    /**
     * 创建 平台免单抽奖活动定义
     * @param goodsId
     * @param stockDef
     * @return
     */
    @Override
    public SimpleResponse createPlatformOrderBackActivityDef(Long goodsId, Long skuId, Integer stockDef, Integer activityDefType) {
        List<Long> goodsIds = new ArrayList<>();
        goodsIds.add(goodsId);
        goodsIds.add(skuId);
        // 是否已创建
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getGoodsIds, JSON.toJSONString(goodsIds))
                .eq(ActivityDef::getType, ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        int count = activityDefService.count(wrapper);
        if (count > 0) {
            log.error("此商品活动已存在, goodsId = {}", goodsId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "此商品活动已存在");
        }
        // 获取商品信息
        List<Long> productSkuIdList = new ArrayList<>();
        productSkuIdList.add(skuId);
        List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
        if (CollectionUtils.isEmpty(productEntities)) {
            log.error("查无此商品, skuId = {}", skuId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "查无此商品");
        }
        ProductEntity productEntity = productEntities.get(0);
        if (productEntity.getPlatSupportRatio() == null) {
            log.error("非分销广场商品，无法创建");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "非分销广场商品，无法创建");
        }

        // 复制一份sku 作为活动sku
        ProductSkuEntity productSkuEntity = goodsAppService.createSkuForPlatformOrderBack(productEntity.getSkuId(), stockDef);
        if (productSkuEntity == null) {
            log.error("创建平台免单抽奖sku失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "创建平台免单抽奖sku失败");
        }

        List<MerchantShopCouponDTO> merchantShopCouponDTOS = merchantShopCouponService.selectByProductId(goodsId);
        List<Long> couponList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(merchantShopCouponDTOS)) {
            for (MerchantShopCouponDTO merchantShopCouponDTO : merchantShopCouponDTOS) {
                couponList.add(merchantShopCouponDTO.getId());
            }
        }

        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        Date startTime = DateUtil.stringtoDate(apolloLhyxProperty.getLhyx().getPreActivity().getStartTime(), DateUtil.FORMAT_ONE);
        Date endTime = DateUtil.stringtoDate(apolloLhyxProperty.getLhyx().getPreActivity().getEndTime(), DateUtil.FORMAT_ONE);
        String kf = apolloLhyxProperty.getLhyx().getPreActivity().getKf();

        Map<String, Object> serviceWx = new HashMap<>();
        serviceWx.put("wechat", "集客名品客服");
        serviceWx.put("pic", kf);
        List<Map<String, Object>> serviceWxList = new ArrayList<>();
        serviceWxList.add(serviceWx);

        Date now = new Date();
        ActivityDef activityDef = new ActivityDef();
        activityDef.setMerchantId(productEntity.getMerchantId());
        activityDef.setParentId(0L);
        activityDef.setShopId(productEntity.getShopId());
        activityDef.setType(activityDefType);
        activityDef.setStatus(ActivityDefStatusEnum.PUBLISHED.getCode());
        activityDef.setStartTime(startTime);
        activityDef.setEndTime(endTime);
        activityDef.setGoodsIds(JSON.toJSONString(goodsIds));
        activityDef.setGoodsId(productSkuEntity.getId());
        activityDef.setGoodsPrice(productEntity.getProductPriceMin());
        activityDef.setStockType(ActivityDefStockTypeEnum.CREATE_REDUCE.getCode());
        activityDef.setStockDef(stockDef);
        activityDef.setStock(stockDef);
        activityDef.setDepositDef(BigDecimal.ZERO);
        activityDef.setDeposit(BigDecimal.ZERO);
        activityDef.setDepositStatus(DepositStatusEnum.PAYMENT.getCode());
        activityDef.setStartNum(1);
        activityDef.setPublishTime(now);
        activityDef.setCondDrawTime((int)((endTime.getTime() - startTime.getTime())/60000L));
        activityDef.setCondPersionCount(null);
        activityDef.setHitsPerDraw(stockDef);
        activityDef.setMissionNeedTime(null);
        if (!CollectionUtils.isEmpty(couponList)) {
            activityDef.setMerchantCouponJson(JSON.toJSONString(couponList));
        }
        activityDef.setWeighting(WeightingEnum.NO.getCode());
        activityDef.setApplyTime(now);
        activityDef.setAuditingTime(now);
        activityDef.setDel(DelEnum.NO.getCode());
        activityDef.setCtime(now);
        activityDef.setUtime(now);
        activityDef.setServiceWx(JSON.toJSONString(serviceWxList));
        activityDef.setJeton(BigDecimal.ZERO);
        activityDef.setCommission(BigDecimal.ZERO);
        activityDef.setFinishType(ActivityDefFinishTypeEnum.WEICHAT.getCode());
        activityDef.setAuditType(ActivityDefAuditTypeEnum.ORDER_BACK.getCode());

        boolean save = activityDefService.save(activityDef);
        if (!save) {
            log.error("数据库存储活动定义异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "网络异常，请稍候重试");
        }

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.CREATE_ACTIVITY_DEF_FORBID_REPEAT + productSkuEntity.getId() + ":" + ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode(), goodsId.toString());
        stringRedisTemplate.expire(RedisKeyConstant.CREATE_ACTIVITY_DEF_FORBID_REPEAT + productSkuEntity.getId() + ":" + ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode(),
                (int)((endTime.getTime() - System.currentTimeMillis())/60000L), TimeUnit.MINUTES);

        return new SimpleResponse();
    }

    /**
     * 挂起或启动 平台免单抽奖活动 -- 7.22 联合营销
     * @param activityDefId
     * @param status
     * @return
     */
    @Override
    public SimpleResponse startOrStopActivityDef(Long activityDefId, Integer status) {
        ActivityDef activityDef = activityDefService.getById(activityDefId);
        if (activityDef == null) {
            log.error("查无此活动定义, activityDefId = {}", activityDefId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "查无此活动定义");
        }
        if (status != ActivityDefStatusEnum.CLOSE.getCode() && status != ActivityDefStatusEnum.PUBLISHED.getCode()) {
            log.error("非挂起或发布操作, status = {}", status);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "操作失败，非挂起或发布操作");
        }
        if (status == ActivityDefStatusEnum.PUBLISHED.getCode()) {
            // 校验是否有同类活动在进行中
            String goodsIds = activityDef.getGoodsIds();
            if (StringUtils.isNotBlank(goodsIds)) {
                // 是否已创建
                QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
                wrapper.lambda()
                        .eq(ActivityDef::getGoodsIds, JSON.toJSONString(goodsIds))
                        .eq(ActivityDef::getType, ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode())
                        .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                        .eq(ActivityDef::getDel, DelEnum.NO.getCode());
                int count = activityDefService.count(wrapper);
                if (count > 0) {
                    log.error("此商品活动已存在, goodsId = {}", goodsIds);
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "此商品活动已存在");
                }
            }
        }
        activityDef.setStatus(status);
        activityDef.setUtime(new Date());
        boolean save = activityDefService.updateById(activityDef);
        if (!save) {
            log.error("数据库存储活动定义异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "网络异常，请稍候重试");
        }
        return new SimpleResponse();
    }

    /**
     * 查询 平台免单抽奖活动 列表页
     * @param activityDefType
     * @param pageSize
     * @param currentPage
     * @return
     */
    @Override
    public SimpleResponse queryActivityDefListPage(Integer activityDefType, Long pageSize, Long currentPage) {
        Page<ActivityDef> page = new Page<>(currentPage, pageSize);
        page.setDesc("id");

        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getType, ActivityDefTypeEnum.PLATFORM_ORDER_BACK.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        IPage<ActivityDef> activityDefIPage = activityDefService.page(page, wrapper);
        if (activityDefIPage == null || CollectionUtils.isEmpty(activityDefIPage.getRecords())) {
            log.error("查询数据库异常");
            MybatisPageDto<Map> mybatisPageDto = new MybatisPageDto<>(currentPage, pageSize, 0, 0);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, mybatisPageDto);
        }
        List<ActivityDef> records = activityDefIPage.getRecords();

        List<Long> skuIds = new ArrayList<>();
        for (ActivityDef activityDef : records) {
            skuIds.add(activityDef.getGoodsId());
        }

        List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(productEntities)) {
            log.error("查询商品信息异常， skuIds = {}", skuIds);
            MybatisPageDto<Map> mybatisPageDto = new MybatisPageDto<>(currentPage, pageSize, 0, 0);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, mybatisPageDto);
        }
        Map<Long, ProductEntity> productEntityMap = new HashMap<>();
        for (ProductEntity productEntity : productEntities) {
            productEntityMap.put(productEntity.getSkuId(), productEntity);
        }

        List<Object> recordVOList = new ArrayList<>();

        for (ActivityDef activityDef : records) {
            ProductEntity productEntity = productEntityMap.get(activityDef.getGoodsId());
            if (CollectionUtils.isEmpty(productEntity.getProductSkuList())) {
                continue;
            }
            Map<String, Object> recordVO = new HashMap<>();
            recordVO.put("id", activityDef.getId());
            recordVO.put("goodsId", JSON.parseArray(activityDef.getGoodsIds()).get(0));
            recordVO.put("skuId", productEntity.getProductSkuList().get(0).getOldSkuId());
            recordVO.put("status", activityDef.getStatus());
            recordVO.put("stock", activityDef.getStock());
            recordVOList.add(recordVO);
        }
        MybatisPageDto<Map> mybatisPageDto = new MybatisPageDto<>(currentPage, pageSize, activityDefIPage.getTotal(), activityDefIPage.getPages());
        mybatisPageDto.setContent(recordVOList);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, mybatisPageDto);
    }


}
