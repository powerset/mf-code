package mf.code.api.platform.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.platform.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 14:28
 */
public interface PlatformOrderBackDefService {
    /**
     * 创建 平台免单抽奖活动定义
     * @param goodsId
     * @param stockDef
     * @return
     */
    SimpleResponse createPlatformOrderBackActivityDef(Long goodsId, Long skuId, Integer stockDef, Integer activityDefType);

    /**
     * 挂起或启动 平台免单抽奖活动 -- 7.22 联合营销
     * @param activityDefId
     * @param status
     * @return
     */
    SimpleResponse startOrStopActivityDef(Long activityDefId, Integer status);

    /**
     * 查询 平台免单抽奖活动 列表页
     * @param activityDefType
     * @param pageSize
     * @param currentPage
     * @return
     */
    SimpleResponse queryActivityDefListPage(Integer activityDefType, Long pageSize, Long currentPage);
}
