package mf.code.api.platform.dto;

import lombok.Data;

/**
 * mf.code.api.platform.dto
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 14:26
 */
@Data
public class PlatformActivityDefReq {
    /**
     * 商品skuId
     */
    private String skuId;
    /**
     * 商品Id
     */
    private String goodsId;
    /**
     * 活动库存
     */
    private String stockDef;
    /**
     * 活动定义类型
     */
    private String activityDefType;
}
