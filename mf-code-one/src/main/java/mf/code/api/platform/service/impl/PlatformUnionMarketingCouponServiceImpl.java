package mf.code.api.platform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.sd4324530.fastweixin.util.CollectionUtil;
import mf.code.api.platform.service.PlatformUnionMarketingCouponService;
import mf.code.merchant.repo.dao.MerchantShopCouponMapper;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.one.dto.MerchantShopCouponDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlatformUnionMarketingCouponServiceImpl implements PlatformUnionMarketingCouponService {

    @Autowired
    private MerchantShopCouponMapper merchantShopCouponMapper;

    @Override
    public List<MerchantShopCouponDTO> queryAllCouponsByGoodId(Long goodId, Long page, Long size) {

        QueryWrapper<MerchantShopCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantShopCoupon::getProductId, goodId)
                .eq(MerchantShopCoupon::getType, 2)
                .eq(MerchantShopCoupon::getDel, 0);

        Page<MerchantShopCoupon> iPage = new Page<>(page, size);

        IPage<MerchantShopCoupon> upayMerchantShopCouponIPage = merchantShopCouponMapper.selectPage(iPage, wrapper);
        if (upayMerchantShopCouponIPage == null) {
            return null;
        }

        List<MerchantShopCoupon> records = upayMerchantShopCouponIPage.getRecords();
        if (CollectionUtil.isEmpty(records) || records.size() < 1) {
            return null;
        }
        List<MerchantShopCouponDTO> merchantShopCouponDTOList = new ArrayList<>();
        for (MerchantShopCoupon merchantShopCoupon : records) {
            MerchantShopCouponDTO merchantShopCouponDTO = new MerchantShopCouponDTO();
            BeanUtils.copyProperties(merchantShopCoupon, merchantShopCouponDTO);
            merchantShopCouponDTOList.add(merchantShopCouponDTO);
        }

        return merchantShopCouponDTOList;
    }

    @Override
    public int queryAllCouponsByGoodIdCount(Long goodId) {

        QueryWrapper<MerchantShopCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantShopCoupon::getShopId, goodId)
                .eq(MerchantShopCoupon::getType, 2)
                .eq(MerchantShopCoupon::getDel, 0);


        return merchantShopCouponMapper.selectCount(wrapper);
    }
}
