package mf.code.api.platform;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.platform.service.PlatformUnionMarketingCouponService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.MerchantShopCouponDTO;
import mf.code.one.dto.UnionMarketingCouponDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 运营--联合营销--优惠券
 * author：yunshan
 */
@Slf4j
@RestController
@RequestMapping("/api/platform/coupon/unionMarketing")
public class PlatformUnionMarketingCouponApi {

    @Autowired
    private PlatformUnionMarketingCouponService platformUnionMarketingCouponService;

    /**
     * 根据商品id查询所有和该商品关联的优惠券
     *
     * @param goodId
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/queryAllCouponsByGoodId")
    public SimpleResponse queryAllCouponsByGoodId(@RequestParam("goodId") Long goodId,
                                                  @RequestParam(value = "page", defaultValue = "10") Long page,
                                                  @RequestParam(value = "size", defaultValue = "1") Long size) {


        UnionMarketingCouponDTO dto = new UnionMarketingCouponDTO();
        List<MerchantShopCouponDTO> couponList = new ArrayList<>();
        dto.setTotalCount(0);
        dto.setPage(page);
        dto.setSize(size);
        dto.setMList(couponList);

        if (null == goodId) {
            return new SimpleResponse(ApiStatusEnum.ERROR_DB, dto);
        }

        if (page < 0) {
            page = 1L;
        }

        if (size <= 0) {
            size = 10L;
        }

        couponList = platformUnionMarketingCouponService.queryAllCouponsByGoodId(goodId, page, size);
        if (CollectionUtils.isEmpty(couponList) || couponList.size() < 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_DB, dto);
        }

        int count = platformUnionMarketingCouponService.queryAllCouponsByGoodIdCount(goodId);

        dto.setTotalCount(count);
        dto.setMList(couponList);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, dto);
    }
}
