package mf.code.api.platform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.api.MybatisPageDto;
import mf.code.api.money.ReportService;
import mf.code.api.platform.service.ExpertExcelService;
import mf.code.api.platform.service.PlatformMerchantOrderService;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.repo.enums.OrderStatusEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantOrderService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.platform.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月02日 20:31
 */
@Service
public class PlatformMerchantOrderServiceImpl implements PlatformMerchantOrderService {
    @Autowired
    private ReportService reportService;
    @Autowired
    private MerchantOrderService merchantOrderService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ExpertExcelService expertExcelService;

    @Override
    public SimpleResponse queryRecord(String start,
                                      String end,
                                      Integer limit,
                                      Integer pageNum,
                                      Long shopId,
                                      int type,
                                      String merchantOrderNo) {
        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isBlank(start) || StringUtils.isBlank(end)) {
            Date endDay = new Date();
            startTime = DateUtils.addDays(endDay, -30);
            endTime = DateUtils.addDays(endDay, 1);
        } else {
            startTime = DateUtil.parseDate(start, null, "yyyy-MM-dd");
            endTime = DateUtils.addDays(DateUtil.parseDate(end, null, "yyyy-MM-dd"), 1);
        }

        SimpleResponse simpleResponse = new SimpleResponse();

        // 分页查询 排序方式
        Page<MerchantOrder> page = new Page<>(pageNum, limit);
        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .orderByDesc(MerchantOrder::getCtime)
        ;
        if (type != 0) {
            //魔方软件交易方式
            if (type != MfTradeTypeEnum.GOODS_TRADE.getCode() && type != MfTradeTypeEnum.CASH.getCode()) {
                wrapper.and(params -> params.eq("mf_trade_type", type));
                wrapper.and(params -> params.ne("type", OrderTypeEnum.USERCASH.getCode()));
                wrapper.and(params -> params.notIn("biz_type", Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_PRIVATE.getCode(), BizTypeEnum.LEVELUP_PRIVATE.getCode())));
            }
            //提现
            if (type == MfTradeTypeEnum.CASH.getCode()) {
                wrapper.and(params -> params.eq("type", OrderTypeEnum.USERCASH.getCode()));
            }
            //商品交易-待结算
            if (type == MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //商品交易-已结算
            if (type == MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //仅退款
            if (type == MfTradeTypeEnum.GOODS_SALE_REFUND.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_REFUND.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
            }
            //退货退款
            if (type == MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
            }
            //成为店长的收益
            if (type == MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //开通店铺
            if (type == MfTradeTypeEnum.OPEN_SHOP.getCode()) {
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
                wrapper.and(params -> params.in("biz_type", Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_PRIVATE.getCode(), BizTypeEnum.LEVELUP_PRIVATE.getCode())));
            }
        }
        //模糊搜索
        if (StringUtils.isNotBlank(merchantOrderNo)) {
            wrapper.and(params -> params.like("order_no", merchantOrderNo));
        } else {
            Date finalStartTime = startTime;
            Date finalEndTime = endTime;
            wrapper.and(params -> params.between("ctime", finalStartTime, finalEndTime));
        }

        IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);

        MybatisPageDto<MerchantOrder> merchantOrderMybatisPage = new MybatisPageDto<MerchantOrder>();
        if (merchantOrderIPage.getRecords() == null || merchantOrderIPage.getRecords().size() == 0) {
            simpleResponse.setData(merchantOrderMybatisPage);
            return simpleResponse;
        }
        /***
         * 时间 2018.12.26 11:26
         * 交易类型 支付保证金 保证金结算退回 补库存
         * 交易金额 -10000
         * 交易明细 新人有礼活动保证金 10000元
         * 支付账户 微信号15167753095
         * 到账账户 集客魔方商户号
         * 操作人 15167753095（主账号） 15167753091（子账号）
         * 店铺名 xxx
         */
        Map<Long, String> tradeDetailMap = this.reportService.getTradeDetail(merchantOrderIPage.getRecords());
        Map<Long, String> merchantInfoMap = this.reportService.getMerchantInfo(merchantOrderIPage.getRecords());
        merchantOrderMybatisPage.setContent(new ArrayList<>());
        for (MerchantOrder merchantOrder : merchantOrderIPage.getRecords()) {
            String tradeDetailObj = tradeDetailMap.get(merchantOrder.getId());
            String merchantInfoObj = merchantInfoMap.get(merchantOrder.getId());
            if (StringUtils.isBlank(merchantInfoObj) || StringUtils.isBlank(tradeDetailObj)) {
                continue;
            }
            JSONObject tradeDetailJsonObject = JSONObject.parseObject(tradeDetailObj);
            JSONObject merchantJsonObject = JSONObject.parseObject(merchantInfoObj);
            if (merchantJsonObject == null || tradeDetailJsonObject == null) {
                continue;
            }
            merchantOrderMybatisPage.getContent().add(this.reportService.assembleSingleMerchantOrderInfo(merchantOrder, tradeDetailJsonObject, merchantJsonObject));
        }
        merchantOrderMybatisPage.from(merchantOrderIPage.getCurrent(), merchantOrderIPage.getSize(), merchantOrderIPage.getTotal(), merchantOrderIPage.getPages());
        simpleResponse.setData(merchantOrderMybatisPage);
        return simpleResponse;
    }

    /***
     *
     * @param platformId
     * @param start
     * @param end
     * @param shopId
     * @param type
     *
     * <pre>
     *     (1,"支付保证金"),
     *     (2,"保证金结算退回"),
     *     (3,"补库存"),
     *     (6, "仅退款"),
     *     (7, "退货退款"),
     *     (8, "提现"),
     *     (9,商品交易((4, "商品售出待结算"),(5, "商品售出已结算")))
     *     (10,升级店长)
     *     (11,开通店铺)
     *  </pre>
     * @return
     */
    @Override
    public SimpleResponse packagePlatformReport(Long platformId,
                                                String start,
                                                String end,
                                                Long shopId,
                                                int type) {
        String redisKey = RedisKeyConstant.EXCEL_REPEAT + shopId;
        String redisStr = this.stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isNotBlank(redisStr)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "打包下载正在进行中，请勿重复");
        }

        String index = "";
        if (StringUtils.isBlank(end) && StringUtils.isBlank(start)) {
            Date endDay = new Date();
            index = DateFormatUtils.format(endDay, "yyyy-MM-dd");
        } else {
            index = start + "_" + end;
        }

        String fileName = shopId + "All" + type + "_" + index;


        Map resp = new HashMap();
        resp.put("key", fileName);

        String existRedisKey = RedisKeyConstant.EXCEL_EXIST + fileName;
        String existRedisStr = this.stringRedisTemplate.opsForValue().get(existRedisKey);
        if (StringUtils.isNotBlank(existRedisStr)) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resp);
        }

        //设置防重处理
        stringRedisTemplate.opsForValue().set(redisKey, "0", 2, TimeUnit.MINUTES);

        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isNotBlank(start)) {
            startTime = DateUtil.parseDate(start, null, "yyyy-MM-dd");
            if (StringUtils.isBlank(end)) {
                Date endDay = new Date();
                endTime = DateUtils.addDays(endDay, 1);
            } else {
                endTime = DateUtils.addDays(DateUtil.parseDate(end, null, "yyyy-MM-dd"), 1);
            }
        }

        QueryWrapper<MerchantOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(MerchantOrder::getShopId, shopId)
                .eq(MerchantOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
        ;
        if (type != 0) {
            //魔方软件交易方式
            if (type != MfTradeTypeEnum.GOODS_TRADE.getCode() && type != MfTradeTypeEnum.CASH.getCode()) {
                wrapper.and(params -> params.eq("mf_trade_type", type));
                wrapper.and(params -> params.ne("type", OrderTypeEnum.USERCASH.getCode()));
                wrapper.and(params -> params.notIn("biz_type", Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_PRIVATE.getCode(), BizTypeEnum.LEVELUP_PRIVATE.getCode())));
            }
            //提现
            if (type == MfTradeTypeEnum.CASH.getCode()) {
                wrapper.and(params -> params.eq("type", OrderTypeEnum.USERCASH.getCode()));
            }
            //商品交易-待结算
            if (type == MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_WILL_CLEANING.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //商品交易-已结算
            if (type == MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_CLEANINGED.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //仅退款
            if (type == MfTradeTypeEnum.GOODS_SALE_REFUND.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_REFUND.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
            }
            //退货退款
            if (type == MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
            }
            //成为店长的收益
            if (type == MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode()) {
                wrapper.and(params -> params.in("mf_trade_type", Arrays.asList(MfTradeTypeEnum.SHOP_MANAGER_PAY.getCode())));
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()));
            }
            //开通店铺
            if (type == MfTradeTypeEnum.OPEN_SHOP.getCode()) {
                wrapper.and(params -> params.eq("type", OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode()));
                wrapper.and(params -> params.in("biz_type", Arrays.asList(BizTypeEnum.PURCHASE_PUBLIC.getCode(),
                        BizTypeEnum.PURCHASE_PRIVATE.getCode(), BizTypeEnum.LEVELUP_PRIVATE.getCode())));
            }
        }
        if (startTime != null && endTime != null) {
            Date finalStartTime = startTime;
            Date finalEndTime = endTime;
            wrapper.and(params -> params.between("ctime", finalStartTime, finalEndTime));
        }
        wrapper.orderByDesc("ctime");

        //导出excel异步处理开始
        expertExcelService.merchantExpertExcel(wrapper, start, end, type);

        return new SimpleResponse(ApiStatusEnum.SUCCESS, resp);
    }

    /***
     * 打包下载
     *
     * @param key
     * @param platformId
     * @return
     */
    @Override
    public SimpleResponse downloadPlatformReport(Long platformId, String key) {
        Map map = new HashMap();
        map.put("status", 1);//进行中

        String redisKey = RedisKeyConstant.EXCEL_EXIST + key;
        String existRedisStr = this.stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isNotBlank(existRedisStr)) {
            map.put("status", 2);//下载结束
            map.put("url", existRedisStr);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, map);
    }
}
