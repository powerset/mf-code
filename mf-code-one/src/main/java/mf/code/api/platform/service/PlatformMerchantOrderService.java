package mf.code.api.platform.service;

import mf.code.common.simpleresp.SimpleResponse;

import javax.servlet.http.HttpServletResponse;

/**
 * mf.code.api.platform.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月02日 20:31
 */
public interface PlatformMerchantOrderService {

    SimpleResponse queryRecord(String start,
                               String end,
                               Integer limit,
                               Integer pageNum,
                               Long shopId,
                               int type,
                               String merchantOrderNo);

    SimpleResponse packagePlatformReport(Long platformId,
                                         String start,
                                         String end,
                                         Long shopId,
                                         int type);

    /***
     * 打包下载
     *
     * @param key
     * @param platformId
     * @return
     */
    SimpleResponse downloadPlatformReport(Long platformId, String key);
}
