package mf.code.api.platform.service;

import mf.code.one.dto.MerchantShopCouponDTO;

import java.util.List;


public interface PlatformUnionMarketingCouponService {
    /**
     * 根据商品id查询所有和该商品关联的优惠券
     * @param goodId
     * @param page
     * @param size
     * @return
     */
    List<MerchantShopCouponDTO> queryAllCouponsByGoodId(Long goodId, Long page, Long size);

    /**
     * 根据商品id查询所有和该商品关联的优惠券 总数量
     * @param goodId
     * @return
     */
    int queryAllCouponsByGoodIdCount(Long goodId);
}
