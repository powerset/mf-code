package mf.code.api.platform.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import mf.code.api.money.AssembleSingleMerchantOrderDto;
import mf.code.common.easyexcel.FieldTypeAnnotation;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import org.springframework.beans.BeanUtils;

/**
 * mf.code.api.money
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月02日 20:57
 */
@Data
public class AssembleSingleMerchantOrderReportDto extends BaseRowModel {
    //订单编号
    private Long id;
    //创建时间
    @ExcelProperty(value = "交易时间", index = 0)
    private String ctime;
    //店铺名
    @ExcelProperty(value = "交易店铺", index = 1)
    private String shopName;
    //交易类型
    @ExcelProperty(value = "交易类型", index = 2)
    private String mfTradeType;
    //金额
    @ExcelProperty(value = "交易金额(元)", index = 3)
    private String totalFee;
    //交易明细
    @ExcelProperty(value = "交易明细", index = 4)
    private String tradeDetail;
    //到账账户
    @ExcelProperty(value = "到账账户", index = 5)
    private String arrivalAccount;
    //支付账户
    @ExcelProperty(value = "关联微信商户号", index = 6)
    private String connWxMerchantOrderNo;
    //商户手机号
    @ExcelProperty(value = "商户手机号", index = 7)
    private String payAccount;

    public void from(AssembleSingleMerchantOrderDto dto) {
        BeanUtils.copyProperties(dto, this);
        if (dto.getBizType() == BizTypeEnum.PURCHASE_PUBLIC.getCode()
                || dto.getBizType() == BizTypeEnum.PURCHASE_PRIVATE.getCode()
                || dto.getBizType() == BizTypeEnum.LEVELUP_PRIVATE.getCode()) {
            dto.setMfTradeType(MfTradeTypeEnum.OPEN_SHOP.getCode());
        }
        this.mfTradeType = MfTradeTypeEnum.findByDesc(dto.getMfTradeType());
    }
}
