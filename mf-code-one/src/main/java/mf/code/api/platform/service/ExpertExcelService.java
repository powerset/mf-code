package mf.code.api.platform.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.merchant.repo.po.MerchantOrder;

import java.util.Date;

/**
 * mf.code.api.platform.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月01日 15:21
 */
public interface ExpertExcelService {

    /***
     * 商户数据导出
     *
     * @param wrapper
     */
    void merchantExpertExcel(QueryWrapper<MerchantOrder> wrapper, String start, String end, int type);
}
