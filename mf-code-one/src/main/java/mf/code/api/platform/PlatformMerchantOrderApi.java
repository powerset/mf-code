package mf.code.api.platform;

import mf.code.api.platform.service.PlatformMerchantOrderService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.platform
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月02日 20:29
 */
@RestController
@RequestMapping(value = "/api/platform/merchantOrder")
public class PlatformMerchantOrderApi {
    @Autowired
    private PlatformMerchantOrderService platformMerchantOrderService;

    /***
     * 运营端财务统计
     * @param start
     * @param end
     * @param limit
     * @param pageNum
     * @param shopId
     * @param type
     * @return
     */
    @RequestMapping(path = "/queryRecord", method = RequestMethod.GET)
    public SimpleResponse queryRecord(@RequestParam(name = "start", required = false) String start,
                                      @RequestParam(name = "end", required = false) String end,
                                      @RequestParam(name = "size", defaultValue = "10") Integer limit,
                                      @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                      @RequestParam(name = "shopId", defaultValue = "0") Long shopId,
                                      @RequestParam(name = "merchantOrderNo", defaultValue = "") String merchantOrderNo,
                                      @RequestParam(name = "type", defaultValue = "0") int type) {
        if (shopId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请选择一个店铺进行查询...");
        }
        return this.platformMerchantOrderService.queryRecord(start, end, limit, pageNum, shopId, type, merchantOrderNo);
    }

    /***
     * 打包下载筛选结果
     * @param start
     * @param end
     * @param shopId
     * @param type
     * @param platformId
     * @return
     */
    @RequestMapping(path = "/packagePlatformReport", method = RequestMethod.GET)
    public SimpleResponse packagePlatformReport(@RequestParam(name = "start", required = false) String start,
                                                @RequestParam(name = "end", required = false) String end,
                                                @RequestParam(name = "shopId", defaultValue = "0") Long shopId,
                                                @RequestParam(name = "type", defaultValue = "0") int type,
                                                @RequestParam(name = "platformId", defaultValue = "0") Long platformId) {
        if (shopId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请选择店铺");
        }
        return this.platformMerchantOrderService.packagePlatformReport(platformId, start, end, shopId, type);
    }

    /***
     * 打包下载
     *
     * @param key
     * @param platformId
     * @return
     */
    @RequestMapping(path = "/downloadPlatformReport", method = RequestMethod.GET)
    public SimpleResponse downloadPlatformReport(@RequestParam(name = "key") String key,
                                                 @RequestParam(name = "platformId", defaultValue = "0") Long platformId) {
        return this.platformMerchantOrderService.downloadPlatformReport(platformId, key);
    }
}
