package mf.code.api.platform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.money.ReportService;
import mf.code.api.platform.dto.AssembleSingleMerchantOrderReportDto;
import mf.code.api.platform.service.ExpertExcelService;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.easyexcel.EasyExcelUtil;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantOrderService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.platform.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月01日 15:21
 */
@Service
@Slf4j
public class ExpertExcelServiceImpl implements ExpertExcelService {
    @Autowired
    private MerchantOrderService merchantOrderService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private OssCaller ossCaller;

    private final int MAX_LIMIT = 50;

    @Async
    @Override
    public void merchantExpertExcel(QueryWrapper<MerchantOrder> wrapper, String start, String end, int type) {
        int pageNum = 1;

        Long shopId = null;
        List<AssembleSingleMerchantOrderReportDto> dtoList = new ArrayList<>();
        for (; ; pageNum++) {
            Page<MerchantOrder> page = new Page<>(pageNum, MAX_LIMIT);
            IPage<MerchantOrder> merchantOrderIPage = merchantOrderService.page(page, wrapper);
            if (CollectionUtils.isEmpty(merchantOrderIPage.getRecords())) {
                break;
            }
            List<MerchantOrder> merchantOrders = merchantOrderIPage.getRecords();
            shopId = merchantOrders.get(0).getShopId();
            Map<Long, String> tradeDetailMap = reportService.getTradeDetail(merchantOrders);
            Map<Long, String> merchantInfoMap = reportService.getMerchantInfo(merchantOrders);
            for (MerchantOrder merchantOrder : merchantOrders) {
                String tradeDetailObj = tradeDetailMap.get(merchantOrder.getId());
                String merchantInfoObj = merchantInfoMap.get(merchantOrder.getId());
                if (StringUtils.isBlank(merchantInfoObj) || StringUtils.isBlank(tradeDetailObj)) {
                    continue;
                }
                JSONObject tradeDetailJsonObject = JSONObject.parseObject(tradeDetailObj);
                JSONObject merchantJsonObject = JSONObject.parseObject(merchantInfoObj);
                if (merchantJsonObject == null || tradeDetailJsonObject == null) {
                    continue;
                }

                AssembleSingleMerchantOrderReportDto dto = new AssembleSingleMerchantOrderReportDto();
                dto.from(reportService.assembleSingleMerchantOrderInfo(merchantOrder, tradeDetailJsonObject, merchantJsonObject));
                dtoList.add(dto);
            }
        }

        String ossUrl = "";
        String index = "";
        if (StringUtils.isBlank(end) && StringUtils.isBlank(start)) {
            Date endDay = new Date();
            index = DateFormatUtils.format(endDay, "yyyy-MM-dd");
        } else {
            index = start + "_" + end;
        }
        String fileName = shopId + "All" + type + "_" + index;

        try {
            InputStream inputStream = EasyExcelUtil.exportInputStream(dtoList, AssembleSingleMerchantOrderReportDto.class);
            String bucket = "excel/" + fileName + ".xlsx";
            ossUrl = this.ossCaller.uploadObject2OSSInputstream(inputStream, bucket);
            log.info("<<<<<<<<打包下载已经结束: url:{}", ossUrl);
        } finally {
            String redisKey = RedisKeyConstant.EXCEL_REPEAT + shopId;
            //完成之后，删除redis
            this.stringRedisTemplate.delete(redisKey);

            //成功后将此次的url进行redis存储
            if (StringUtils.isNotBlank(ossUrl)) {
                String existRedisKey = RedisKeyConstant.EXCEL_EXIST + fileName;
                if (StringUtils.isBlank(end)) {
                    this.stringRedisTemplate.opsForValue().set(existRedisKey, ossUrl, 1, TimeUnit.DAYS);
                } else {
                    this.stringRedisTemplate.opsForValue().set(existRedisKey, ossUrl, 30, TimeUnit.DAYS);
                }
            }
        }
    }
}
