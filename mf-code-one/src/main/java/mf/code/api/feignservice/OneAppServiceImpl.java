package mf.code.api.feignservice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;
import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.domain.applet.aggregateroot.JoinRuleActivity;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityDefV8AboutService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v6.service.NewbieOpenRedPackageService;
import mf.code.api.applet.v8.dto.CheckpointTaskSpaceDTO;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceAboutService;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.common.CommonCodeTypeEnum;
import mf.code.common.CommonCodeUserTypeEnum;
import mf.code.common.RedisKeyConstant;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.repo.dto.LiveCodeDTO;
import mf.code.common.repo.dto.WeChatCodeDTO;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonCodeService;
import mf.code.common.service.CommonConfService;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.IpUtil;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.*;
import mf.code.uactivity.repo.dao.UserPubJoinMapper;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.*;
import mf.code.upay.repo.enums.UpayWxOrderMfTradeTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UserTaskIncomeResp;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.one.api.applet.v6.feignservice
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 10:30
 */
@Slf4j
@RestController
public class OneAppServiceImpl {
    @Autowired
    private AppletActivityDefRepository appletActivityDefRepository;
    @Autowired
    private AppletActivityRepository appletActivityRepository;
    @Autowired
    private JoinRuleActivityRepository joinRuleActivityRepository;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UserPubJoinMapper userPubJoinMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefV8AboutService activityDefV8AboutService;
    @Autowired
    private CheckpointTaskSpaceAboutService checkpointTaskSpaceAboutService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommonCodeService commonCodeService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private NewbieOpenRedPackageService newbieOpenRedPackageService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private CommonConfService commonConfService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;
    @Autowired
    private CommonDictService commonDictService;

    /***
     * 存储数据字典表
     *
     * @param commonDictReq
     * @return
     */
    @PostMapping("/feignapi/one/applet/v10/saveCommonDict")
    public SimpleResponse createCommonDict(@RequestBody CommonDictReqDTO commonDictReq) {
        SimpleResponse simpleResponse = new SimpleResponse();
        CommonDict commonDict = commonDictService.selectByTypeKey(commonDictReq.getType(), commonDictReq.getKey());
        if (commonDict != null) {
            simpleResponse.setData(1);
            return simpleResponse;
        }
        log.info("<<<<<<<<req:{}", commonDictReq);
        commonDict = new CommonDict();
        commonDict.setType(commonDictReq.getType());
        commonDict.setKey(commonDictReq.getKey());
        commonDict.setValue(commonDictReq.getValue());
        commonDict.setValue1("");
        if (StringUtils.isNotBlank(commonDictReq.getValue1())) {
            commonDict.setValue1(commonDictReq.getValue1());
        }
        simpleResponse.setData(commonDictService.create(commonDict));
        return simpleResponse;
    }

    /***
     * 查询字典表配置信息-小程序码专用
     *
     * @param type
     * @param key
     * @return
     */
    @GetMapping("/feignapi/one/applet/v10/queryCommonDictByAppletCode")
    public SimpleResponse queryCommonDict(@RequestParam("type") String type, @RequestParam("key") String key) {
        SimpleResponse simpleResponse = new SimpleResponse();
        CommonDict commonDict = commonDictService.selectByTypeKey(type, key);
        if (commonDict == null) {
            log.error("<<<<<<<<查询不到记录哦，请刷新重试-_-");
            return new SimpleResponse();
        }
        Map map = new HashMap();
        if (StringUtils.isNotBlank(commonDict.getValue())) {
            map = JSONObject.parseObject(commonDict.getValue(), Map.class);
        }
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /**
     * 店长 上传二维码 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardUploadShopManagerQR")
    public String giveAwardUploadShopManagerQR(@RequestParam("userId") Long userId,
                                               @RequestParam("activityDefId") Long activityDefId) {
        if (userId == null || userId < 1 || activityDefId == null || activityDefId < 1) {
            log.error("参数异常");
            return null;
        }
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getParentId, activityDefId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        ActivityDef activityDef = activityDefService.getOne(wrapper);
        if (activityDef == null) {
            log.info("商户没有配置 店长邀请粉丝任务");
            return null;
        }

        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, activityDef.getShopId(),
                UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX, activityDef.getCommission());
        commissionDisDto.setActivityDefId(activityDef.getId());

        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        return commissionBO.getCommission().toString();
    }

    /**
     * 店长 邀请粉丝达标 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardInviteFans")
    public String giveAwardInviteFans(@RequestParam("userId") Long userId,
                                      @RequestParam("activityDefId") Long activityDefId) {
        if (userId == null || userId < 1 || activityDefId == null || activityDefId < 1) {
            log.error("参数异常");
            return null;
        }
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getParentId, activityDefId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        ActivityDef activityDef = activityDefService.getOne(wrapper);
        if (activityDef == null) {
            log.info("商户没有配置 店长邀请粉丝任务, 或粉丝任务已挂起");
            return null;
        }
        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, activityDef.getShopId(),
                UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS, activityDef.getCommission());
        commissionDisDto.setActivityDefId(activityDef.getId());

        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        return commissionBO.getCommission().toString();
    }

    /**
     * 发展粉丝成为店长 发奖
     *
     * @param userId
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/giveAwardRecommendShopManager")
    public String giveAwardRecommendShopManager(@RequestParam("userId") Long userId,
                                                @RequestParam("activityDefId") Long activityDefId) {
        if (userId == null || userId < 1 || activityDefId == null || activityDefId < 1) {
            log.error("参数异常");
            return null;
        }
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getParentId, activityDefId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        ActivityDef activityDef = activityDefService.getOne(wrapper);
        if (activityDef == null) {
            log.info("商户没有配置 店长邀请粉丝任务, 或 已被挂起");
            return null;
        }
        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, activityDef.getShopId(),
                UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER, activityDef.getCommission());
        commissionDisDto.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        return commissionBO.getCommission().toString();
    }

    /**
     * 获取 店长 邀请粉丝任务
     *
     * @param activityDefId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/findShopManagerInviteFansTask")
    public ActivityDefDTO findShopManagerInviteFansTask(@RequestParam("activityDefId") Long activityDefId) {
        if (activityDefId == null || activityDefId < 1) {
            log.error("参数异常");
            return null;
        }
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getParentId, activityDefId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        ActivityDef activityDef = activityDefService.getOne(wrapper);
        if (activityDef == null) {
            log.info("商户没有配置 店长邀请粉丝任务，或活动定义已被挂起");
            return null;
        }
        ActivityDefDTO activityDefDTO = new ActivityDefDTO();
        BeanUtils.copyProperties(activityDef, activityDefDTO);
        return activityDefDTO;
    }

    /**
     * 下线 全平台所有店铺中 任务三佣金为0的店长任务
     *
     * @return
     */
    @Async
    @GetMapping("/feignapi/one/platform/offlineShopManagerActivityDef")
    public void offlineShopManagerActivityDef() {
        Date now = new Date();
        // 获取所有发布中的 任务三佣金为0的子任务
        for (; ; ) {
            Page<ActivityDef> page = new Page<>(1, 20);
            page.setDesc("ctime");
            QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
            wrapper.lambda()
                    .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode())
                    .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                    .eq(ActivityDef::getCommission, BigDecimal.ZERO)
                    .eq(ActivityDef::getDel, DelEnum.NO.getCode());
            IPage<ActivityDef> activityDefIPage = activityDefService.page(page, wrapper);
            if (activityDefIPage == null || CollectionUtils.isEmpty(activityDefIPage.getRecords())) {
                log.info("下线完成");
                break;
            }
            List<ActivityDef> records = activityDefIPage.getRecords();
            List<Long> activityDefIdList = new ArrayList<>();
            for (ActivityDef activityDef : records) {
                activityDefIdList.add(activityDef.getParentId());
            }
            // 下线主活动
            Collection<ActivityDef> activityDefList = activityDefService.listByIds(activityDefIdList);
            List<ActivityDef> offlineActivityDefList = new ArrayList<>();
            for (ActivityDef activityDef : activityDefList) {
                activityDef.setStatus(ActivityDefStatusEnum.CLOSE.getCode());
                activityDef.setUtime(now);
                offlineActivityDefList.add(activityDef);
            }
            // 下线子活动
            QueryWrapper<ActivityDef> parentIdWrapper = new QueryWrapper<>();
            parentIdWrapper.lambda()
                    .in(ActivityDef::getParentId, activityDefIdList);
            List<ActivityDef> subActivityDefList = activityDefService.list(parentIdWrapper);
            for (ActivityDef activityDef : subActivityDefList) {
                activityDef.setStatus(ActivityDefStatusEnum.CLOSE.getCode());
                activityDef.setUtime(now);
                offlineActivityDefList.add(activityDef);
            }
            boolean success = activityDefService.updateBatchById(offlineActivityDefList, 50);
            if (!success) {
                log.error("数据库异常");
                break;
            }
        }

    }

    /**
     * 领取店长任务，扣绑定店铺库存 库存
     *
     * @param userId
     * @param shopId
     * @return true 扣成功，false 扣失败
     */
    @Transactional
    @GetMapping("/feignapi/one/applet/v8/getShopManagerTask")
    public Long getShopManagerTask(@RequestParam("userId") Long userId,
                                   @RequestParam("shopId") Long shopId) {
        if (userId == null || userId < 1 || shopId == null || shopId < 1) {
            log.error("参数异常");
            return null;
        }
        boolean nx = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.SHOP_MANAGER_TASK_FORBID_REPEAT + userId);
        if (!nx) {
            return 0L;
        }
        // 获取店长任务
        QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
        userTaskWrapper.lambda()
                .eq(UserTask::getType, UserTaskTypeEnum.SHOP_MANAGER_TASK.getCode())
                .eq(UserTask::getUserId, userId);
        UserTask userTaskDB = userTaskService.getOne(userTaskWrapper);
        if (userTaskDB != null) {
            return userTaskDB.getActivityDefId();
        }
        // 获取店铺是否有店长任务
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getType, ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode())
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        List<ActivityDef> activityDefList = activityDefService.list(wrapper);
        if (CollectionUtils.isEmpty(activityDefList)) {
            return null;
        }
        ActivityDef activityDef = activityDefList.get(0);
        if (activityDef.getStock() <= 0) {
            return null;
        }
        Integer rows = activityDefService.reduceDeposit(activityDef.getId(), BigDecimal.ZERO, 1);
        if (rows == 0) {
            log.info("库存不足");
            return null;
        }
        Date now = new Date();
        UserTask userTask = new UserTask();
        userTask.setType(UserTaskTypeEnum.SHOP_MANAGER_TASK.getCode());
        userTask.setUserId(userId);
        userTask.setMerchantId(activityDef.getMerchantId());
        userTask.setShopId(activityDef.getShopId());
        userTask.setActivityDefId(activityDef.getId());
        userTask.setActivityId(0L);
        userTask.setActivityTaskId(0L);
        userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
        userTask.setRead(0);
        userTask.setStockFlag(0);
        userTask.setCtime(now);
        userTask.setUtime(now);
        boolean save = userTaskService.save(userTask);
        if (!save) {
            log.error("店长任务存储失败");
            throw new RuntimeException();
        }
        stringRedisTemplate.delete(RedisKeyConstant.SHOP_MANAGER_TASK_FORBID_REPEAT + userId);
        return activityDef.getId();
    }

    /**
     * 查询 用户闯关任务收益 和 店长任务情况
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/queryUserTaskIncome")
    public UserTaskIncomeResp queryUserTaskIncome(@RequestParam("userId") Long userId) {
        if (userId == null || userId < 1) {
            log.error("参数异常");
            return null;
        }
        // 累计获取 下级贡献给用户 的任务佣金
        QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .eq(UserPubJoin::getUserId, userId);
        List<UserPubJoin> userPubJoins = userPubJoinMapper.selectList(wrapper);

        BigDecimal totalCommission = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            for (UserPubJoin userPubJoin : userPubJoins) {
                totalCommission = totalCommission.add(userPubJoin.getTotalScottare());
            }
        }

        // 累计获取 用户完成任务的奖金
        QueryWrapper<UserPubJoin> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .eq(UserPubJoin::getSubUid, userId);
        List<UserPubJoin> userPubJoinList = userPubJoinMapper.selectList(queryWrapper);
        BigDecimal totalAward = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(userPubJoinList)) {
            for (UserPubJoin userPubJoin : userPubJoinList) {
                totalAward = totalAward.add(userPubJoin.getTotalCommission());
            }
        }

        // 获取 今日收益 -- 任务奖金
        String userTodayInfo = stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + userId);
        JSONObject userToday = JSON.parseObject(userTodayInfo);
        if (CollectionUtils.isEmpty(userToday)) {
            userToday = new JSONObject();
            userToday.put("todayTaskProfit", "0.00");
            userToday.put("todayScottareProfit", "0.00");
            userToday.put("todayScottare", "0.00");
        }

        // 获取店长任务
        QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
        userTaskWrapper.lambda()
                .eq(UserTask::getType, UserTaskTypeEnum.SHOP_MANAGER_TASK.getCode())
                .eq(UserTask::getUserId, userId);
        UserTask userTask = userTaskService.getOne(userTaskWrapper);

        List<UserCoupon> userCouponList = null;
        if (userTask != null) {
            List<Integer> typeList = new ArrayList<>();
            typeList.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
            typeList.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
            typeList.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
            QueryWrapper<UserCoupon> userCouponWrapper = new QueryWrapper<>();
            userCouponWrapper.lambda()
                    .eq(UserCoupon::getUserId, userId)
                    .in(UserCoupon::getType, typeList);
            userCouponList = userCouponService.list(userCouponWrapper);
        }

        // 获取 今日收益 -- 任务佣金
        UserTaskIncomeResp userTaskIncomeResp = new UserTaskIncomeResp();
        userTaskIncomeResp.setUserId(userId);
        userTaskIncomeResp.setTodayAward(new BigDecimal(userToday.getString("todayTaskProfit")));
        userTaskIncomeResp.setTodayCommission(new BigDecimal(userToday.getString("todayScottareProfit")));
        userTaskIncomeResp.setTotalCommission(totalCommission);
        userTaskIncomeResp.setTotalAward(totalAward);
        if (userTask != null) {
            UserTaskDTO userTaskDTO = new UserTaskDTO();
            BeanUtils.copyProperties(userTask, userTaskDTO);
            Map<Long, UserTaskDTO> userTaskMap = new HashMap<>();
            userTaskMap.put(userTaskDTO.getId(), userTaskDTO);
            userTaskIncomeResp.setUserTaskMap(userTaskMap);
        }
        if (!CollectionUtils.isEmpty(userCouponList)) {
            Map<Long, UserCouponDTO> userCouponDTOMap = new HashMap<>();
            for (UserCoupon userCoupon : userCouponList) {
                UserCouponDTO userCouponDTO = new UserCouponDTO();
                BeanUtils.copyProperties(userCoupon, userCouponDTO);
                userCouponDTOMap.put(userCouponDTO.getId(), userCouponDTO);
            }
            userTaskIncomeResp.setUserCouponMap(userCouponDTOMap);
        }

        return userTaskIncomeResp;
    }

    /**
     * 批量获取 用户任务收益
     *
     * @param userIds
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/listUserTaskIncome")
    public List<UserTaskIncomeResp> listUserTaskIncome(@RequestParam("userIds") List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            log.error("参数异常");
            return null;
        }
        // 累计获取 下级贡献给用户 的任务佣金
        QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .in(UserPubJoin::getUserId, userIds);
        List<UserPubJoin> userPubJoins = userPubJoinMapper.selectList(wrapper);
        Map<Long, BigDecimal> userTotalCommission = new HashMap<>();
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            for (UserPubJoin userPubJoin : userPubJoins) {
                BigDecimal totalCommission = userTotalCommission.get(userPubJoin.getUserId());
                if (totalCommission == null) {
                    totalCommission = BigDecimal.ZERO;
                }
                totalCommission = totalCommission.add(userPubJoin.getTotalScottare());
                userTotalCommission.put(userPubJoin.getUserId(), totalCommission);
            }
        }
        // 累计获取 用户完成任务的奖金
        QueryWrapper<UserPubJoin> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .in(UserPubJoin::getSubUid, userIds);
        List<UserPubJoin> userPubJoinList = userPubJoinMapper.selectList(queryWrapper);
        Map<Long, BigDecimal> userTotalAward = new HashMap<>();
        if (!CollectionUtils.isEmpty(userPubJoinList)) {
            for (UserPubJoin userPubJoin : userPubJoinList) {
                BigDecimal totalAward = userTotalAward.get(userPubJoin.getSubUid());
                if (totalAward == null) {
                    totalAward = BigDecimal.ZERO;
                }
                totalAward = totalAward.add(userPubJoin.getTotalCommission());
                userTotalAward.put(userPubJoin.getSubUid(), totalAward);
            }
        }
        // 获取 今日收益 -- 任务奖金
        Map<Long, JSONObject> userTodayInfoMap = new HashMap<>();
        for (Long userId : userIds) {
            String userTodayInfo = stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + userId);
            JSONObject userToday = JSON.parseObject(userTodayInfo);
            if (CollectionUtils.isEmpty(userToday)) {
                userToday = new JSONObject();
                userToday.put("todayTaskProfit", "0.00");
                userToday.put("todayScottareProfit", "0.00");
                userToday.put("todayScottare", "0.00");
            }
            userTodayInfoMap.put(userId, userToday);
        }

        // 获取店长任务
        QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
        userTaskWrapper.lambda()
                .eq(UserTask::getType, UserTaskTypeEnum.SHOP_MANAGER_TASK.getCode())
                .in(UserTask::getUserId, userIds);
        List<UserTask> userTaskList = userTaskService.list(userTaskWrapper);
        Map<Long, UserTask> userTaskMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userTaskList)) {
            for (UserTask userTask : userTaskList) {
                userTaskMap.put(userTask.getUserId(), userTask);
            }
        }

        List<Integer> typeList = new ArrayList<>();
        typeList.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        typeList.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        typeList.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        QueryWrapper<UserCoupon> userCouponWrapper = new QueryWrapper<>();
        userCouponWrapper.lambda()
                .eq(UserCoupon::getUserId, userIds)
                .in(UserCoupon::getType, typeList);
        List<UserCoupon> userCouponList = userCouponService.list(userCouponWrapper);
        Map<Long, List<UserCoupon>> userCouponListMap = new HashMap<>();
        for (UserCoupon userCoupon : userCouponList) {
            List<UserCoupon> userCoupons = userCouponListMap.get(userCoupon.getUserId());
            if (CollectionUtils.isEmpty(userCoupons)) {
                userCoupons = new ArrayList<>();
            }
            userCoupons.add(userCoupon);
            userCouponListMap.put(userCoupon.getUserId(), userCoupons);
        }

        List<UserTaskIncomeResp> userTaskIncomeRespList = new ArrayList<>();
        for (Long userId : userIds) {
            // 获取 今日收益 -- 任务佣金
            UserTaskIncomeResp userTaskIncomeResp = new UserTaskIncomeResp();
            userTaskIncomeResp.setUserId(userId);
            JSONObject userToday = userTodayInfoMap.get(userId);
            if (!CollectionUtils.isEmpty(userToday)) {
                userTaskIncomeResp.setTodayAward(new BigDecimal(userToday.getString("todayTaskProfit")));
                userTaskIncomeResp.setTodayCommission(new BigDecimal(userToday.getString("todayScottareProfit")));
            }
            BigDecimal totalCommission = userTotalCommission.get(userId);
            if (totalCommission == null) {
                totalCommission = BigDecimal.ZERO;
            }
            userTaskIncomeResp.setTotalCommission(totalCommission);
            BigDecimal totalAward = userTotalAward.get(userId);
            if (totalAward == null) {
                totalAward = BigDecimal.ZERO;
            }
            userTaskIncomeResp.setTotalAward(totalAward);

            UserTask userTask = userTaskMap.get(userId);
            if (userTask != null) {
                UserTaskDTO userTaskDTO = new UserTaskDTO();
                BeanUtils.copyProperties(userTask, userTaskDTO);
                Map<Long, UserTaskDTO> userTaskDTOMap = new HashMap<>();
                userTaskDTOMap.put(userTaskDTO.getId(), userTaskDTO);
                userTaskIncomeResp.setUserTaskMap(userTaskDTOMap);
            }
            List<UserCoupon> userCoupons = userCouponListMap.get(userId);
            if (!CollectionUtils.isEmpty(userCoupons)) {
                Map<Long, UserCouponDTO> userCouponDTOMap = new HashMap<>();
                for (UserCoupon userCoupon : userCoupons) {
                    UserCouponDTO userCouponDTO = new UserCouponDTO();
                    BeanUtils.copyProperties(userCoupon, userCouponDTO);
                    userCouponDTOMap.put(userCouponDTO.getId(), userCouponDTO);
                }
                userTaskIncomeResp.setUserCouponMap(userCouponDTOMap);
            }
            userTaskIncomeRespList.add(userTaskIncomeResp);
        }

        return userTaskIncomeRespList;
    }

    /**
     * 获取新人任务
     *
     * @param shopId
     * @param userId
     * @return null 无活动定义 非null 返回以下内容
     * Map<String, Object> resultVO = new HashMap<>();
     * resultVO.put("newbiwAmount", totalAmount); 可以做的新人任务金额总和
     * resultVO.put("shopManagerAmount", totalAmount); 可以做的店长任务金额总和
     * resultVO.put("dailyAmount", totalAmount);可以做的日常任务金额总和
     */
    @GetMapping("/feignapi/one/applet/v6/queryNewcomerTaskInfo")
    public Map<String, Object> queryNewcomerTaskInfo(@RequestParam("shopId") Long shopId,
                                                     @RequestParam("userId") Long userId) {
        Map<String, Object> resultVO = new HashMap<>();

        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            return null;
        }
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            return null;
        }
        CheckpointTaskSpaceDTO checkpointTaskSpaceDTO = new CheckpointTaskSpaceDTO();
        checkpointTaskSpaceAboutService.queryNewbieTaskDetail(checkpointTaskSpaceDTO, checkpointTaskSpaceDTO.getTasks(), user);
        resultVO.put("newbieAmount", checkpointTaskSpaceDTO.getEstimateProfit().toString());

        checkpointTaskSpaceDTO = new CheckpointTaskSpaceDTO();
        checkpointTaskSpaceAboutService.queryShopManagerTaskDetail(checkpointTaskSpaceDTO, checkpointTaskSpaceDTO.getTasks(), user);
        resultVO.put("shopManagerAmount", checkpointTaskSpaceDTO.getEstimateProfit().toString());

        checkpointTaskSpaceDTO = new CheckpointTaskSpaceDTO();
        checkpointTaskSpaceAboutService.queryDailyTaskDetail(checkpointTaskSpaceDTO, checkpointTaskSpaceDTO.getTasks(), merchantShop.getMerchantId(), shopId, userId);
        resultVO.put("dailyAmount", checkpointTaskSpaceDTO.getEstimateProfit().toString());

        return resultVO;
    }

    /***
     * 查询报销活动信息
     *
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/v9/queryFullReimbursementInfo")
    public Map<String, Object> queryFullReimbursementInfo(@RequestParam("shopId") Long shopId,
                                                          @RequestParam("userId") Long userId) {
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityDefId", 0);
        resultVO.put("activityId", 0);
        log.info("<<<<<<<< 报销活动信息，开始查询");
        if (shopId == null || shopId <= 0 || userId == null || userId <= 0) {
            log.error("参数异常, shopId = {}, userId = {}", shopId, userId);
            return null;
        }
        AppletActivityDef appletActivityDef = appletActivityDefRepository.findByShopIdType(shopId, ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode());
        if (appletActivityDef != null) {
            //是否存在活动定义
            resultVO.put("status", 1);
            resultVO.put("activityDefId", appletActivityDef.getId());
        }
        // 获取店铺的 该用户的正在进行的报销活动进行中的
        QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
        activityQueryWrapper.lambda()
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getUserId, userId)
                .eq(Activity::getType, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode())
                .eq(Activity::getStatus, mf.code.activity.constant.ActivityStatusEnum.PUBLISHED.getCode())
                .eq(Activity::getDel, mf.code.common.DelEnum.NO.getCode())
                .isNull(Activity::getAwardStartTime)
        ;
        activityQueryWrapper.orderByDesc("id");
        List<Activity> activities = activityService.list(activityQueryWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            return null;
        }

        Activity activity = activities.get(0);
        if (activity != null) {
            //是否有进行中
            resultVO.put("status", 2);
            resultVO.put("activityId", activity.getId());
        }
        return resultVO;
    }


    /**
     * 拆红包是否完成了
     *
     * @param shopId
     * @param userId
     * @return null无活动定义；有活动定义 且用户：0没有活动, 2有进行中活动, 1 完成-金额待解锁
     */
    @GetMapping("/feignapi/one/applet/v6/queryOpenRedPackageStatus")
    public Map<String, Object> queryOpenRedPackageStatus(@RequestParam("shopId") Long shopId,
                                                         @RequestParam("userId") Long userId) {
        log.info("<<<<<<<<拆红包信息 开始查询");
        if (shopId == null || shopId < 1 || userId == null || userId < 1) {
            log.error("参数异常, shopId = {}, userId = {}", shopId, userId);
            return null;
        }
        // 获取店铺的 拆红包活动定义
        AppletActivityDef appletActivityDef = appletActivityDefRepository.findByShopIdType(shopId, ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        if (appletActivityDef == null) {
            log.info("<<<<<<<<拆红包信息-没有拆红包活动定义");
            return null;
            // appletActivityDef = appletActivityDefRepository.findByShopIdTypeEndStatus(shopId, ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
            // if (appletActivityDef == null) {
            //     return null;
            // }
        }

        // 2.3迭代 新增 新手任务-拆红包逻辑
        Activity publishedActivity = newbieOpenRedPackageService.hasPublishedActivityByUserId(userId);
        if (publishedActivity != null) {
            // 装配活动
            AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(publishedActivity, userId, ScenesEnum.HOME_PAGE);
            appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);
            log.info("<<<<<<<<拆红包信息-装配活动 openRedPackageActivity:{}", openRedPackageActivity);
            if (openRedPackageActivity.isExpired()) {
                // 活动过期
                // 补全 用户参与事件
                if (openRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
                    appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
                    openRedPackageActivity.setExpired();
                    appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
                }
                // 从别人活动卡片，进入自己的过期活动，弹窗

                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("status", 0);
                resultVO.put("activityDefId", appletActivityDef.getId());
                resultVO.put("amount", openRedPackageActivity.getActivityInfo().getPayPrice());
                return resultVO;
            }

            // 进行中的活动
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("status", 2);
            resultVO.put("amount", openRedPackageActivity.getActivityInfo().getPayPrice());
            resultVO.put("activityDefId", appletActivityDef.getId());
            resultVO.put("activityId", openRedPackageActivity.getId());

            if (openRedPackageActivity.isAwarded()) {
                resultVO.put("status", 1);
            }
            log.info("<<<<<<<<拆红包信息-结束 resp:{}", resultVO);
            return resultVO;
        }


        // 按活动参与规则，获取用户进行中或已完成活动
        JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRule(appletActivityDef.getJoinRule());
        log.info("<<<<<<<<拆红包信息-按活动参与规则，获取用户进行中或已完成活动 joinRuleActivity：{}", joinRuleActivity);
        // 判断用户是否能参与活动定义
        if (joinRuleActivity.canJoinByUserId(userId)) {
            // 没有进行中活动且可发起新活动
            if (appletActivityDef.canCreate()) {
                // 可以参与 -- 说明，可以发起新活动
                log.info("<<<<<<<<拆红包信息-用户没有拆红包活动， shopId = {}, userId = {}", shopId, userId);

                ActivityDef activityDef = appletActivityDef.getActivityDefInfo();
                BigDecimal commission = activityDef.getCommission();
                BigDecimal goodsPrice = activityDef.getGoodsPrice();
                BigDecimal activityAmount = commission.compareTo(BigDecimal.ZERO) == 0 ? goodsPrice : commission;

                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("status", 0);
                resultVO.put("activityDefId", appletActivityDef.getId());
                resultVO.put("amount", activityAmount);
                return resultVO;
            }
            log.info("<<<<<<<<拆红包信息-库存不足");
            return null;
        }
        // 说明 已有进行中活动或不可再发起新活动了 处理返回参数 -- 各种情况判断

        // 获取用户进行中的活动列表
        List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
        log.info("<<<<<<<<拆红包信息-按活动参与规则，获取用户进行中的活动列表 activityListSize：{}", activityList.size());
        Activity activity;
        if (CollectionUtils.isEmpty(activityList)) {
            // 无进行中活动，获取用户已完成的活动
            Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
            if (completedActivityId == null) {
                log.error("openRedPackagePopup逻辑异常，须排查");
                return null;
            }
            AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
            Activity activityInfo = completedActivity.getActivityInfo();
            if (activityInfo == null) {
                log.error("openRedPackagePopup 查无此活动，activityId = {}", completedActivityId);
                return null;
            }
            activity = activityInfo;
        } else {
            activity = activityList.get(0);
        }

        // 装配活动
        AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, userId, ScenesEnum.HOME_PAGE);
        appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);
        log.info("<<<<<<<<拆红包信息-装配活动 openRedPackageActivity:{}", openRedPackageActivity);
        if (openRedPackageActivity.isExpired()) {
            // 活动过期
            // 补全 用户参与事件
            if (openRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
                appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
                openRedPackageActivity.setExpired();
                appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
            }
            // 从别人活动卡片，进入自己的过期活动，弹窗

            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("status", 0);
            resultVO.put("activityDefId", appletActivityDef.getId());
            resultVO.put("amount", openRedPackageActivity.getActivityInfo().getPayPrice());
            return resultVO;
        }

        // 进行中的活动
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("status", 2);
        resultVO.put("amount", openRedPackageActivity.getActivityInfo().getPayPrice());
        resultVO.put("activityDefId", appletActivityDef.getId());
        resultVO.put("activityId", openRedPackageActivity.getId());

        if (openRedPackageActivity.isAwarded()) {
            resultVO.put("status", 1);
        }
        log.info("<<<<<<<<拆红包信息-结束 resp:{}", resultVO);
        return resultVO;
    }

    /**
     * 完成支付 -- 拆红包任务完成
     *
     * @param activityId
     */
    @GetMapping("/feignapi/one/applet/v6/completeOpenRedPackagePay")
    public SimpleResponse completeOpenRedPackagePay(@RequestParam("activityId") Long activityId) {
        if (activityId == null || activityId < 1) {
            log.error("参数异常, activityId = {}", activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此活动，activityId = {}", activityId);
        }
        AppletActivity appletActivity = appletActivityRepository.findById(activityId);
        if (appletActivity == null) {
            log.error("查无此活动, activityId = {}", activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此活动，activityId = {}", activityId);
        }
        Activity activity = appletActivity.getActivityInfo();
        AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, activity.getUserId(), ScenesEnum.HOME_PAGE);
        if (!openRedPackageActivity.isAwarded()) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS);
        }
        openRedPackageActivity.completePayTask();
        boolean save = appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
        if (!save) {
            log.error("入库失败, activityId = {}", activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "入库失败");
        }

        QueryWrapper<UpayWxOrder> upayWxOrderQueryWrapper = new QueryWrapper<>();
        upayWxOrderQueryWrapper.lambda()
                .eq(UpayWxOrder::getShopId, activity.getShopId())
                .eq(UpayWxOrder::getUserId, activity.getUserId())
                .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                .in(UpayWxOrder::getBizType, Arrays.asList(BizTypeEnum.OPEN_RED_PACKAGE.getCode(),
                        BizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode()))
                .eq(UpayWxOrder::getBizValue, activityId)
                .eq(UpayWxOrder::getMfTradeType, UpayWxOrderMfTradeTypeEnum.WILLCLEANING.getCode())
        ;
        upayWxOrderQueryWrapper.orderByAsc("id");
        List<UpayWxOrder> upayWxOrders = upayWxOrderService.list(upayWxOrderQueryWrapper);
        if (!CollectionUtils.isEmpty(upayWxOrders)) {
            UpayWxOrder upayWxOrder = upayWxOrders.get(0);
            upayWxOrder.setMfTradeType(UpayWxOrderMfTradeTypeEnum.CLEANINGED.getCode());
            upayWxOrder.setUtime(new Date());
            boolean b = upayWxOrderService.updateById(upayWxOrder);
            if (!b) {
                log.error("<<<<<<<<更新拆红包订单失败, upaywxorderId = {}", upayWxOrder.getId());
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "更新拆红包订单失败,");
            }
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    /***
     * 首页获取新手模块数据展现
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/queryHomePageNewbieTask")
    public Map<String, Object> queryHomePageNewbieTask(@RequestParam("shopId") Long shopId,
                                                       @RequestParam("userId") Long userId) {
        Map<String, Object> resp = new HashMap<>();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = null;
        taskDTO = activityDefV8AboutService.queryNewbieTaskDetail(userId, true);
        if (taskDTO != null) {
            resp.putAll(BeanMapUtil.beanToMap(taskDTO));
            return resp;
        }
        taskDTO = activityDefV8AboutService.queryShopManagerTaskDetail(userId, true);
        if (taskDTO != null) {
            resp.putAll(BeanMapUtil.beanToMap(taskDTO));
            return resp;
        }
        taskDTO = activityDefV8AboutService.queryDailyTaskDetail(merchantShop.getMerchantId(), shopId, userId, true);
        if (taskDTO != null) {
            resp.putAll(BeanMapUtil.beanToMap(taskDTO));
            return resp;
        }
        return null;
    }

    /**
     * 小程序端店长添加群二维码
     *
     * @param shopManagerCodeDTO
     * @return
     */
    @PostMapping("/feignapi/one/applet/saveGroupCodeForShopManager")
    public String saveGroupCodeForShopManager(@RequestBody ShopManagerCodeDTO shopManagerCodeDTO) {

        LiveCodeDTO liveCodeDTO = new LiveCodeDTO();
        liveCodeDTO.setScanLimit(100);
        liveCodeDTO.setUserId(shopManagerCodeDTO.getUserId());
        liveCodeDTO.setUserType(CommonCodeUserTypeEnum.USER.getCode());
        List<WeChatCodeDTO> weCodeListDTOList = new ArrayList<>();
        WeChatCodeDTO weChatCodeDTO = new WeChatCodeDTO();
        weChatCodeDTO.setWeChatPath(shopManagerCodeDTO.getGroupCodePath());
        weChatCodeDTO.setWeChatType(CommonCodeTypeEnum.GROUP_CODE.getCode());
        weChatCodeDTO.setSort(0);
        weChatCodeDTO.setScanNum(100);
        weCodeListDTOList.add(weChatCodeDTO);
        liveCodeDTO.setLiveCodeList(weCodeListDTOList);
        return commonCodeService.createOrUpdateLiveCode(liveCodeDTO);
    }

    /**
     * 小程序端店长查询群二维码
     *
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/getGroupCodeByUserId")
    public String getGroupCodeByUserId(@RequestParam(value = "userId") Long userId) {
        return commonCodeService.queryLiveCodeByUserId(userId);
    }

    /**
     * 从图片中获取微信二维码
     *
     * @param shopManagerCodeDTO
     * @return
     */
    @PostMapping("/feignapi/one/applet/getWxQRFromPicUrl")
    public String getWxQRFromPicUrl(@RequestBody ShopManagerCodeDTO shopManagerCodeDTO) {
        if (shopManagerCodeDTO == null) {
            return null;
        }
        return commonCodeService.getWxQRFramPicUrl(shopManagerCodeDTO.getGroupCodePath(), shopManagerCodeDTO.getUserId());
    }

    /***
     * 成为店长时，查看是否有下级佣金的记录，有：统一存储到upay_wx_order内
     * @param userId
     * @return
     */
    @GetMapping("/feignapi/one/applet/saveShopManagerCommissionByPend")
    public SimpleResponse saveShopManagerCommissionByPend(@RequestParam(value = "userId") Long userId,
                                                          @RequestParam(value = "shopId") Long shopId) {
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            return new SimpleResponse();
        }
        //判断是否成为店长
        if (user.getRole() != 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "未成为店长");
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            return new SimpleResponse();
        }
        List<Integer> list = new ArrayList<>();
        list.add(UpayWxOrderPendBizTypeEnum.ACTIVITY.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.LUCKY_WHEEL.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.RANDOMREDPACK.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.REDPACKTASK.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.GOODSCOMMENT.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.OPEN_RED_PACKAGE.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.ASSIST_ACTIVITY.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.ORDER_BACK.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.NEWMAN_START.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.CHECKPOINT.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.REBATE.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.CONTRIBUTION.getCode());
        list.add(UpayWxOrderPendBizTypeEnum.SHOPPING.getCode());
        Map<String, Object> params = new HashMap<>();
        params.put("type", UpayWxOrderTypeEnum.REBATE.getCode());
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("userId", userId);
        params.put("bizTypes", list);
        String str = distributionAppService.sumTotalFeeByUpayWxOrderPend(params);
        if (StringUtils.isBlank(str)) {
            return new SimpleResponse();
        }
        BigDecimal amount = new BigDecimal(str);
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            UpayWxOrder upayWxOrderUpper = upayWxOrderService.addPo(user.getId(), OrderPayTypeEnum.CASH.getCode(),
                    OrderTypeEnum.REBATE.getCode(), null, BizTypeEnum.LEVLEUP_SHOP_MANAGER.getMessage(),
                    merchantShop.getMerchantId(), merchantShop.getId(), OrderStatusEnum.ORDERED.getCode(), amount,
                    IpUtil.getServerIp(), null, "任务佣金", new Date(), null, null,
                    BizTypeEnum.LEVLEUP_SHOP_MANAGER.getCode(), userId);
            upayWxOrderService.create(upayWxOrderUpper);
            // 更新用户余额
            upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), user.getId(), amount);
        }


        Map<String, Object> shopManagerParams = new HashMap<>();
        shopManagerParams.put("userId", userId);
        shopManagerParams.put("type", UpayWxOrderTypeEnum.REBATE.getCode());
        shopManagerParams.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        shopManagerParams.put("bizType", UpayWxOrderPendBizTypeEnum.SHOP_MANAGER_PAY.getCode());
        List<UpayWxOrderReq> upayWxOrderReqList = distributionAppService.queryShopManagerPend(shopManagerParams);
        if (!CollectionUtils.isEmpty(upayWxOrderReqList)) {
            for (UpayWxOrderReq upayWxOrderReq : upayWxOrderReqList) {
                UpayWxOrder upayWxOrderUpper1 = upayWxOrderService.addPo(user.getId(), OrderPayTypeEnum.CASH.getCode(),
                        OrderTypeEnum.REBATE.getCode(), null, BizTypeEnum.SHOP_MANAGER_PAY.getMessage(),
                        merchantShop.getMerchantId(), merchantShop.getId(), OrderStatusEnum.ORDERED.getCode(), upayWxOrderReq.getTotalFee(),
                        IpUtil.getServerIp(), null, "成功引导粉丝成为店长", new Date(), null, null,
                        BizTypeEnum.SHOP_MANAGER_PAY.getCode(), upayWxOrderReq.getBizValue());
                upayWxOrderService.create(upayWxOrderUpper1);
                // 更新用户余额
                upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), user.getId(), upayWxOrderReq.getTotalFee());
            }
        }

        return new SimpleResponse();
    }

    /**
     * 获取联合营销配置信息
     * 返回值result内容
     * status：返回状态码 0：预热活动未开始 1：预热活动进行中 2：预热活动已结束 3：正式活动进行中 4：正式活动已结束
     * time: 距离下一个状态剩余毫秒数
     * count：累计免单商品数
     * money：累计免单商品总价值
     * banner：banner列表，list类型
     * url：banner地址
     * gid：跳转商品id
     *
     * @return
     * @to yy
     */
    @GetMapping("/feignapi/one/applet/getUnionConf")
    public ApolloLHYXDTO getUnionConf() {
        return commonConfService.getUnionConf();
    }

    /**
     * 查询用户是否拥有尚未使用的平台活动中奖资格
     */
    @GetMapping("/feignapi/one/applet/checkUserTaskForPlatOrderBack")
    public SimpleResponse checkUserTaskForPlatOrderBack(@RequestParam(value = "userId") Long userId,
                                                        @RequestParam(value = "userTaskId") Long userTaskId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        UserTask userTask = userTaskService.selectByPrimaryKey(userTaskId);
        if (userTask == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("查无此次活动中奖记录");
            return simpleResponse;
        }
        // 是否是该用户的中奖任务
        if (!userTask.getUserId().equals(userId)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("该中奖记录不属于您");
            return simpleResponse;
        }
        // 是否是平台抽奖，后期也可加其他类型判断
        if (userTask.getType() != UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该中奖活动类型不匹配");
            return simpleResponse;
        }
        if (userTask.getStatus() != UserTaskStatusEnum.INIT.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("您已经领取过奖励");
            return simpleResponse;
        }
        return simpleResponse;
    }


    /**
     * 查询用户是否拥有尚未使用的平台活动中奖资格
     */
    @GetMapping("/feignapi/one/applet/updateUserTaskForPlatOrderBack")
    public SimpleResponse updateUserTaskForPlatOrderBack(@RequestParam(value = "userId") Long userId,
                                                         @RequestParam(value = "userTaskId") Long userTaskId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        UserTask userTask = userTaskService.selectByPrimaryKey(userTaskId);
        if (userTask == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("查无此次活动中奖记录");
            return simpleResponse;
        }
        // 是否是该用户的中奖任务
        if (!userTask.getUserId().equals(userId)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("该中奖记录不属于您");
            return simpleResponse;
        }
        // 是否是平台抽奖，后期也可加其他类型判断
        if (userTask.getType() != UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该中奖活动类型不匹配");
            return simpleResponse;
        }
        if (userTask.getStatus() != UserTaskStatusEnum.INIT.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("您已经领取过奖励");
            return simpleResponse;
        }
        // 信息加工入库
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        userTask.setUtime(new Date());
        int rows = userTaskService.update(userTask);
        if (rows == 0) {
            simpleResponse.setMessage("发奖失败");
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 根据商品id查询优惠卷
     *
     * @param productId
     * @return
     */
    @GetMapping("/feignapi/one/applet/findMerchantShopCouponByProductId")
    public List<MerchantShopCouponDTO> findMerchantShopCouponByProductId(@RequestParam("productId") Long productId) {
        return merchantShopCouponService.selectByProductId(productId);
    }

    /***
     * 查询商品是否作用于活动信息
     * @param productId
     * @return
     */
    @GetMapping("/feignapi/one/applet/queryProductActivityInfo")
    public Map<String, Object> queryProductActivityInfo(@RequestParam(value = "productId") Long productId) {
        String redisKey = mf.code.uactivity.repo.redis.RedisKeyConstant.ACTIVITY_GOODS_JOIN + productId;
        String str = stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isBlank(str)) {
            return new HashMap<>();
        }
        Map map = JSONObject.parseObject(str, Map.class);
        if (map == null) {
            return new HashMap<>();
        }
        Long activityId = 0L;
        Long skuId = 0L;
        if (map.get("activityId") != null && StringUtils.isNotBlank(map.get("activityId").toString())) {
            activityId = NumberUtils.toLong(map.get("activityId").toString());
        }
        if (map.get("skuId") != null && StringUtils.isNotBlank(map.get("skuId").toString())) {
            skuId = NumberUtils.toLong(map.get("skuId").toString());
        }

        Activity activity = activityService.findById(activityId);
        if (activity == null || activity.getDel() == DelEnum.YES.getCode()) {
            return new HashMap<>();
        }
        int status = 0;
        if (activity.getAwardStartTime() != null) {
            status = 3;
        } else {
            status = activity.getStatus();
        }
        Map resp = new HashMap();
        resp.put("activityId", activityId);
        resp.put("status", status);
        resp.put("skuId", skuId);
        return resp;
    }

    /**
     * 发放优惠卷
     *
     * @param grantCouponDTO
     * @return
     */
    @PostMapping("/feignapi/one/applet/grantCoupon")
    public Map<Long, Long> grantCoupon(@RequestBody GrantCouponDTO grantCouponDTO) {
        return merchantShopCouponService.grantCouponById(grantCouponDTO);
    }

    /**
     * 通过id查询该优惠劵是否可用
     *
     * @param couponId
     * @return
     */
    @GetMapping("/feignapi/one/applet/coupon/findCouponById")
    public UserCouponDTO findCouponById(@RequestParam("couponId") Long couponId) {
        UserCoupon byId = userCouponService.findById(couponId);
        if (byId == null) {
            return null;
        }
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponService.selectByPriKey(byId.getCouponId());
        if (merchantShopCoupon == null) {
            return null;
        }
        Date now = new Date();
        if (merchantShopCoupon.getDisplayStartTime().after(now) || merchantShopCoupon.getDisplayEndTime().before(now)) {
            return null;
        }
        UserCouponDTO userCouponDTO = new UserCouponDTO();
        BeanUtils.copyProperties(byId, userCouponDTO);
        return userCouponDTO;
    }

    /**
     * 通过用户id和商品id查询优惠劵
     *
     * @param userId
     * @param productId
     * @return
     */
    @GetMapping("/feignapi/one/applet/findUserMerchantShopCouponByProductId")
    public List<UserCouponDTO> findUserMerchantShopCouponByProductId(@RequestParam("userId") Long userId, @RequestParam("productId") Long productId) {
        return merchantShopCouponService.findUserMerchantShopCouponByProductId(userId, productId);
    }

    /**
     * 使用优惠券
     *
     * @param discountId
     * @return
     */
    @GetMapping("/feignapi/one/applet/coupon/useUserCoupon")
    public SimpleResponse useUserCoupon(@RequestParam("couponId") Long discountId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        UserCoupon byId = userCouponService.findById(discountId);
        if (byId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            simpleResponse.setMessage("查无此该优惠券");
            return simpleResponse;
        }
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponService.selectByPriKey(byId.getCouponId());
        if (merchantShopCoupon == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("查无此该优惠券");
            return simpleResponse;
        }
        Date now = new Date();
        if (merchantShopCoupon.getDisplayStartTime().after(now) || merchantShopCoupon.getDisplayEndTime().before(now)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            simpleResponse.setMessage("该优惠券不可用");
            return simpleResponse;
        }
        byId.setStatus(UserCouponStatusEnum.USED.getCode());
        byId.setUtime(new Date());
        int count = userCouponService.updateByPrimaryKeySelective(byId);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("使用优惠券失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }


    /**
     * 返还优惠券
     *
     * @param discountId
     * @return
     */
    @GetMapping("/feignapi/one/applet/coupon/backUserCoupon")
    public SimpleResponse backUserCoupon(@RequestParam("couponId") Long discountId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        UserCoupon byId = userCouponService.findById(discountId);
        if (byId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            simpleResponse.setMessage("查无此该优惠券");
            return simpleResponse;
        }
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponService.selectByPriKey(byId.getCouponId());
        if (merchantShopCoupon == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("查无此该优惠券");
            return simpleResponse;
        }
        Date now = new Date();
        byId.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
        if (merchantShopCoupon.getDisplayStartTime().after(now) || merchantShopCoupon.getDisplayEndTime().before(now)) {
            byId.setStatus(UserCouponStatusEnum.EXPIRE.getCode());
        }
        byId.setUtime(new Date());
        int count = userCouponService.updateByPrimaryKeySelective(byId);
        if (count == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("返还优惠券失败，请稍后重试");
            return simpleResponse;
        }
        return simpleResponse;
    }
}
