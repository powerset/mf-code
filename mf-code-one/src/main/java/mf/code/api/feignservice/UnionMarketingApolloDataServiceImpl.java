package mf.code.api.feignservice;/**
 * create by qc on 2019/7/18 0018
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支持联合营销活动的apollo数据配置
 *
 * @author gbf
 * 2019/7/18 0018、10:03
 */
@Slf4j
@RestController
public class UnionMarketingApolloDataServiceImpl {

    @Autowired
    private ApolloPropertyService apolloPropertyService;

    /**
     * 获取apollo配置的skuid
     *
     * @return
     */
    @GetMapping("/feignapi/one/unionmarketing/apollodata/skuids")
    public String getSkuIds() {
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        return JSON.toJSONString(apolloLhyxProperty);
    }
}
