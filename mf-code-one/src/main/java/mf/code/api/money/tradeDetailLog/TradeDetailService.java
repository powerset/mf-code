package mf.code.api.money.tradeDetailLog;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.money.tradeDetailLog
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月17日 17:25
 */
public class TradeDetailService implements TradeDetailConverter {

    /***
     * 判断是否是活动类
     * @param merchantOrder
     * @return
     */
    public boolean isActivityType(MerchantOrder merchantOrder) {
        if (merchantOrder == null || merchantOrder.getBizType() == null || merchantOrder.getBizType() <= 0) {
            return false;
        }
        Integer merchantOrderBizTypeEnum = BizTypeEnum.findByCode(merchantOrder.getBizType());
        if(merchantOrderBizTypeEnum != null && !this.isRedPackType(merchantOrder)){
            return true;
        }
        return false;
    }

    /***
     * 判断是否是红包任务类
     * @param merchantOrder
     * @return
     */
    public boolean isRedPackType(MerchantOrder merchantOrder) {
        if (merchantOrder == null || merchantOrder.getBizType() == null || merchantOrder.getBizType() <= 0) {
            return false;
        }
        Integer merchantOrderBizTypeEnum = BizTypeEnum.findByCode(merchantOrder.getBizType());
        if(merchantOrderBizTypeEnum != null && merchantOrderBizTypeEnum == BizTypeEnum.FAVTASK.getCode()){
            return true;
        }
        return false;
    }

    /***
     * 获取红包类详细信息
     * @param merchantOrder
     * @param activityDefAudit
     * @param activityTaskListMap
     * @return
     */
    public String getRedPackDetail(MerchantOrder merchantOrder, ActivityDefAudit activityDefAudit, Map<Long, List<ActivityTask>> activityTaskListMap) {
        String tradeDetailMessage = "";
        List<ActivityTask> activityTaskList = activityTaskListMap.get(merchantOrder.getBizValue());
        BigDecimal fillBackOrderAmount = BigDecimal.ZERO;
        BigDecimal goodCommentAmount = BigDecimal.ZERO;
        BigDecimal favcartAmount = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(activityTaskList)) {
            Map<String, Object> map = new HashMap<>();
            for (ActivityTask activityTask : activityTaskList) {
                BigDecimal stockApply = new BigDecimal(activityDefAudit.getStockApply().toString());
                if (activityTask.getRpStockDef() != null && activityTask.getRpStockDef() > 0) {
                    if (ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode() == activityTask.getType()) {
                        fillBackOrderAmount = fillBackOrderAmount.add(activityTask.getRpAmount().multiply(stockApply));
                    } else if (ActivityTaskTypeEnum.GOOD_COMMENT.getCode() == activityTask.getType()) {
                        goodCommentAmount = goodCommentAmount.add(activityTask.getRpAmount().multiply(stockApply));
                    } else if (ActivityTaskTypeEnum.FAVCART.getCode() == activityTask.getType()) {
                        favcartAmount = favcartAmount.add(activityTask.getRpAmount().multiply(stockApply));
                    }
                }
            }
            for (ActivityTask activityTask : activityTaskList) {
                if (activityTask.getRpStockDef() != null && activityTask.getRpStockDef() > 0) {
                    if (ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode() == activityTask.getType()) {
                        map.put("fillBackOrder", ActivityTaskTypeEnum.FILL_BACK_ORDER.getDesc() + " " + fillBackOrderAmount.toString() + "元");
                    } else if (ActivityTaskTypeEnum.GOOD_COMMENT.getCode() == activityTask.getType()) {
                        map.put("goodComment", ActivityTaskTypeEnum.GOOD_COMMENT.getDesc() + " " + goodCommentAmount.toString() + "元");
                    } else if (ActivityTaskTypeEnum.FAVCART.getCode() == activityTask.getType()) {
                        map.put("favcart", ActivityTaskTypeEnum.FAVCART.getDesc() + " " + favcartAmount.toString() + "元");
                    }
                }
            }
            StringBuffer stringBuffer = new StringBuffer();
            if (!CollectionUtils.isEmpty(map)) {
                for (Object obj : map.entrySet()) {
                    Map.Entry me = (Map.Entry) obj;
                    stringBuffer.append(me.getValue().toString());
                    stringBuffer.append(",");
                }
            }
            tradeDetailMessage = (stringBuffer.length() > 0 && stringBuffer.lastIndexOf(",") == stringBuffer.length() - 1)
                    ? stringBuffer.substring(0, stringBuffer.length() - 1) : stringBuffer.toString();

        }
        return tradeDetailMessage;
    }

    @Override
    public String tradeDetailConverter(MerchantOrder merchantOrder,
                                       Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> createActivityTaskMap,
                                       Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        return null;
    }
}
