package mf.code.api.money.tradeDetailLog.impl.addStock;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.money.tradeDetailLog.TradeDetailService;
import mf.code.activity.constant.ActivityDefAuditTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.money.tradeDetailLog.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月17日 21:05
 */

@Service
public class AddStockActivityTypeImpl extends TradeDetailService {

    @Override
    public String tradeDetailConverter(MerchantOrder merchantOrder,
                                       Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> createActivityTaskMap,
                                       Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        boolean isActivityTypeConverter = this.isActivityType(merchantOrder);
        boolean isAddStockActivityType = merchantOrder.getMfTradeType() != null && merchantOrder.getMfTradeType() > 0 &&
                merchantOrder.getMfTradeType() == MfTradeTypeEnum.ADD_STOCK.getCode();
        if (isAddStockActivityType && isActivityTypeConverter) {
            String tradeDetailMessage = "";
            ActivityDefAudit addStockActivityDefAudit = addStockActivityDefAuditMap.get(merchantOrder.getBizValue());
            if (addStockActivityDefAudit != null) {
                String moneyLog = " " + addStockActivityDefAudit.getDepositApply().toString() + "元";
                tradeDetailMessage = ActivityDefAuditTypeEnum.findByDesc(addStockActivityDefAudit.getType()) + moneyLog;
            }
            return tradeDetailMessage;
        }
        return null;
    }
}
