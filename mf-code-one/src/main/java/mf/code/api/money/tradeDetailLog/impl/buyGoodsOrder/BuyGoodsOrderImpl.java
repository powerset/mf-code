package mf.code.api.money.tradeDetailLog.impl.buyGoodsOrder;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.money.tradeDetailLog.TradeDetailService;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.money.tradeDetailLog.impl
 *
 * @description: 购买商品  订单编号存储过程，在支付商品 回调内定义
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月17日 21:05
 */

@Service
public class BuyGoodsOrderImpl extends TradeDetailService {

    @Override
    public String tradeDetailConverter(MerchantOrder merchantOrder,
                                       Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> createActivityTaskMap,
                                       Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        boolean buyGoodsOrder = merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode()
                && merchantOrder.getBizType() == BizTypeEnum.SHOPPING.getCode();
        if (buyGoodsOrder) {
            String tradeDetailMessage = "";
            tradeDetailMessage = "订单编号" + merchantOrder.getOrderNo().split("_")[0];
            return tradeDetailMessage;
        }
        return null;
    }
}
