package mf.code.api.money.tradeDetailLog.impl.create;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.money.tradeDetailLog.TradeDetailService;
import mf.code.activity.constant.ActivityDefAuditTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.money.tradeDetailLog.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月17日 17:21
 */
@Service
public class CreateActivityTypeImpl extends TradeDetailService {

    @Override
    public String tradeDetailConverter(MerchantOrder merchantOrder,
                                       Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> createActivityTaskMap,
                                       Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        boolean isActivityTypeConverter = this.isActivityType(merchantOrder);
        boolean isCreateActivityType = merchantOrder.getMfTradeType() != null && merchantOrder.getMfTradeType() > 0 &&
                merchantOrder.getMfTradeType() == MfTradeTypeEnum.PAY_ORDER.getCode();

        if (isCreateActivityType && isActivityTypeConverter) {
            String tradeDetailMessage = "";
            ActivityDefAudit creatActivityDefAudit = createActivityDefAuditMap.get(merchantOrder.getBizValue());
            if (creatActivityDefAudit == null) {
                return tradeDetailMessage;
            }
            String moneyLog = " " + creatActivityDefAudit.getDepositApply().toString() + "元";
            tradeDetailMessage = ActivityDefAuditTypeEnum.findByDesc(creatActivityDefAudit.getType()) + moneyLog;
            return tradeDetailMessage;
        }
        return null;
    }
}
