package mf.code.api.money.tradeDetailLog.impl.create;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.money.tradeDetailLog.TradeDetailService;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.money.tradeDetailLog.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月17日 21:02
 */
@Service
public class CreateRedPackTaskImpl extends TradeDetailService {

    @Override
    public String tradeDetailConverter(MerchantOrder merchantOrder,
                                       Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> createActivityTaskMap,
                                       Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        boolean isRedPackTaskConverter = this.isRedPackType(merchantOrder);
        boolean isCreateRedPackTask = merchantOrder.getMfTradeType() != null && merchantOrder.getMfTradeType() > 0 &&
                merchantOrder.getMfTradeType() == MfTradeTypeEnum.PAY_ORDER.getCode();
        if (isCreateRedPackTask && isRedPackTaskConverter) {
            String tradeDetailMessage = "";
            ActivityDefAudit creatActivityDefAudit = createActivityDefAuditMap.get(merchantOrder.getBizValue());
            if (creatActivityDefAudit == null || CollectionUtils.isEmpty(createActivityTaskMap)) {
                return tradeDetailMessage;
            }

            tradeDetailMessage = this.getRedPackDetail(merchantOrder, creatActivityDefAudit, createActivityTaskMap);
            return tradeDetailMessage;
        }
        return null;
    }
}
