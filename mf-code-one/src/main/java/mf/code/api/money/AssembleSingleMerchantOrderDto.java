package mf.code.api.money;

import lombok.Data;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * mf.code.api.money
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月02日 20:57
 */
@Data
public class AssembleSingleMerchantOrderDto {
    //订单编号
    private Long id;
    //店铺名
    private String shopName;
    //操作人
    private String operator;
    //创建时间
    private String ctime;
    //交易类型
    private int mfTradeType;
    //金额
    private String totalFee;
    //交易明细
    private String tradeDetail;
    //支付账户
    private String payAccount;
    //到账账户
    private String arrivalAccount;
    //业务类型
    private int bizType;
    //关联微信商户号
    private String connWxMerchantOrderNo;

    public void from(MerchantOrder merchantOrder,
                     String totalFee,
                     Map tradeDetail,
                     String payAccount,
                     String arrivalAccount,
                     String operator) {
        this.id = merchantOrder.getId();
        this.operator = operator;
        this.arrivalAccount = arrivalAccount;
        this.payAccount = payAccount;
        if (!CollectionUtils.isEmpty(tradeDetail)) {
            this.tradeDetail = tradeDetail.get("tradeDetail").toString();
        }
        this.totalFee = totalFee;
        this.ctime = DateFormatUtils.format(merchantOrder.getCtime(), "yyyy-MM-dd HH:mm:ss");
        this.mfTradeType = merchantOrder.getMfTradeType();
        this.bizType = merchantOrder.getBizType();
        if (merchantOrder.getType() == OrderTypeEnum.USERCASH.getCode()) {
            this.mfTradeType = MfTradeTypeEnum.CASH.getCode();
        }
        this.connWxMerchantOrderNo = merchantOrder.getOrderNo();
        if (merchantOrder.getOrderNo().contains("_")) {
            this.connWxMerchantOrderNo = merchantOrder.getOrderNo().split("_")[0];
        }
        if(merchantOrder.getOrderNo().contains("@")){
            this.connWxMerchantOrderNo = merchantOrder.getOrderNo().split("@")[0];
        }
    }
}
