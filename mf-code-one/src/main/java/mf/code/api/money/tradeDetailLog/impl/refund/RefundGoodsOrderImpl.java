package mf.code.api.money.tradeDetailLog.impl.refund;

import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.money.tradeDetailLog.TradeDetailService;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.MerchantOrder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.money.tradeDetailLog.impl
 *
 * @description: 购买商品退款  订单编号存储过程，在退款商品 回调内定义
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月17日 21:05
 */

@Service
public class RefundGoodsOrderImpl extends TradeDetailService {

    @Override
    public String tradeDetailConverter(MerchantOrder merchantOrder,
                                       Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> createActivityTaskMap,
                                       Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                       Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        boolean refundGoodsOrder = merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode() && merchantOrder.getBizType() == BizTypeEnum.SHOPPING.getCode();
        if (refundGoodsOrder) {
            String tradeDetailMessage = "";
            tradeDetailMessage = "订单编号" + merchantOrder.getOrderNo().split("@")[0];
            return tradeDetailMessage;
        }
        return null;
    }
}
