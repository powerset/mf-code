package mf.code.api.money.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDefAudit;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefAuditService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.money.AssembleSingleMerchantOrderDto;
import mf.code.api.money.ReportService;
import mf.code.api.money.tradeDetailLog.TradeDetailConverter;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.constants.MfTradeTypeEnum;
import mf.code.merchant.repo.enums.OrderTypeEnum;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.upay.service.OrderService;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.money.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月02日 20:18
 */
@Service
@Slf4j
public class ReportServiceImpl implements ReportService {
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private ActivityDefAuditService activityDefAuditService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private List<TradeDetailConverter> tradeDetailConverters;
    @Autowired
    private OrderService orderService;
    @Autowired
    private MerchantShopService merchantShopService;

    /***
     * 获取交易明细
     * @param merchantOrders
     * @return
     */
    @Override
    public Map<Long, String> getTradeDetail(List<MerchantOrder> merchantOrders) {
        List<Long> activityDefIds = new ArrayList<>();
        List<Long> activityDefAuditIds = new ArrayList<>();
        for (MerchantOrder merchantOrder : merchantOrders) {
            boolean isActivityDefType = merchantOrder.getBizType() != BizTypeEnum.ACTIVITY_AUDIT.getCode();
            if (isActivityDefType && activityDefIds.indexOf(merchantOrder.getBizValue()) == -1) {
                activityDefIds.add(merchantOrder.getBizValue());
            }
            if (merchantOrder.getBizType() == BizTypeEnum.ACTIVITY_AUDIT.getCode() &&
                    activityDefAuditIds.indexOf(merchantOrder.getBizValue()) == -1) {
                activityDefAuditIds.add(merchantOrder.getBizValue());
            }
        }

        Map<Long, ActivityDefAudit> createActivityDefAuditMap = new HashMap<>();
        Map<Long, List<ActivityTask>> createActivityTaskMap = new HashMap<>();
        Map<Long, ActivityDefAudit> addStockActivityDefAuditMap = new HashMap<>();
        Map<Long, List<ActivityTask>> addStockActivityTaskMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(activityDefIds)) {
            QueryWrapper<ActivityDefAudit> activityDefAuditWrapper = new QueryWrapper<>();
            activityDefAuditWrapper.in("activity_def_id", activityDefIds);
            List<ActivityDefAudit> createActivityDefAudits = this.activityDefAuditService.list(activityDefAuditWrapper);
            if (!CollectionUtils.isEmpty(createActivityDefAudits)) {
                createActivityDefAuditMap = this.getActivityDefAuditInfoMap(createActivityDefAudits, false);
                QueryWrapper<ActivityTask> activityTaskWrapper = new QueryWrapper<>();
                activityTaskWrapper.in("activity_def_id", activityDefIds);
                List<ActivityTask> activityTasks = this.activityTaskService.list(activityTaskWrapper);
                createActivityTaskMap = this.getActivityTaskInfoMap(activityTasks, null);
            }
        }
        if (!CollectionUtils.isEmpty(activityDefAuditIds)) {
            QueryWrapper<ActivityDefAudit> activityDefAuditWrapper = new QueryWrapper<>();
            activityDefAuditWrapper.in("id", activityDefAuditIds);
            List<ActivityDefAudit> addStockActivityDefAudits = this.activityDefAuditService.list(activityDefAuditWrapper);
            if (!CollectionUtils.isEmpty(addStockActivityDefAudits)) {
                addStockActivityDefAuditMap = this.getActivityDefAuditInfoMap(addStockActivityDefAudits, true);
                List<Long> auditDefIds = new ArrayList<>();
                for (ActivityDefAudit activityDefAudit : addStockActivityDefAudits) {
                    if (auditDefIds.indexOf(activityDefAudit.getActivityDefId()) == -1) {
                        auditDefIds.add(activityDefAudit.getActivityDefId());
                    }
                }
                Map<Long, ActivityDefAudit> activityDefAuditMap = this.getActivityDefAuditInfoMap(addStockActivityDefAudits, false);
                QueryWrapper<ActivityTask> activityTaskWrapper = new QueryWrapper<>();
                activityTaskWrapper.in("activity_def_id", activityDefIds);
                List<ActivityTask> activityTasks = this.activityTaskService.list(activityTaskWrapper);
                addStockActivityTaskMap = this.getActivityTaskInfoMap(activityTasks, activityDefAuditMap);
            }
        }

        Map<Long, String> respMap = new HashMap<>();
        for (MerchantOrder merchantOrder : merchantOrders) {
            String tradeDetailMessage = this.covertTradeDetail(merchantOrder, createActivityDefAuditMap, createActivityTaskMap, addStockActivityDefAuditMap, addStockActivityTaskMap);
            Map<String, Object> map = new HashMap<>();
            map.put("tradeDetail", tradeDetailMessage);
            respMap.put(merchantOrder.getId(), JSON.toJSONString(map));
        }
        return respMap;
    }

    /***
     * 映射交易明细converter
     * @param merchantOrder
     * @param createActivityDefAuditMap
     * @param createActivityTaskMap
     * @param addStockActivityDefAuditMap
     * @param addStockActivityTaskMap
     * @return
     */
    private String covertTradeDetail(MerchantOrder merchantOrder,
                                     Map<Long, ActivityDefAudit> createActivityDefAuditMap,
                                     Map<Long, List<ActivityTask>> createActivityTaskMap,
                                     Map<Long, ActivityDefAudit> addStockActivityDefAuditMap,
                                     Map<Long, List<ActivityTask>> addStockActivityTaskMap) {
        String tradeDetailMessage = "";
        if (this.tradeDetailConverters != null) {
            for (TradeDetailConverter converter : this.tradeDetailConverters) {
                tradeDetailMessage = converter.tradeDetailConverter(merchantOrder, createActivityDefAuditMap, createActivityTaskMap, addStockActivityDefAuditMap, addStockActivityTaskMap);
                if (tradeDetailMessage != null) {
                    break;
                }
            }
        }
        return tradeDetailMessage;
    }

    /***
     * 商户信息 对象 商户名称+子账号/主账号
     * @param merchantOrders
     * @return
     */
    @Override
    public Map<Long, String> getMerchantInfo(List<MerchantOrder> merchantOrders) {
        Map<Long, String> respMap = new HashMap<>();
        if(CollectionUtils.isEmpty(merchantOrders)){
            return respMap;
        }
        List<Long> merchantIds = new ArrayList<>();
        List<Long> shopIds = new ArrayList<>();
        for (MerchantOrder merchantOrder : merchantOrders) {
            if (merchantIds.indexOf(merchantOrder.getMerchantId()) == -1) {
                merchantIds.add(merchantOrder.getMerchantId());
            }
            if (shopIds.indexOf(merchantOrder.getShopId()) == -1) {
                shopIds.add(merchantOrder.getShopId());
            }
        }

        Map<String, Object> merchantParams = new HashMap<>();
        merchantParams.put("merchantIds", merchantIds);
        List<Merchant> merchants = this.merchantService.selectByParams(merchantParams);
        if (CollectionUtils.isEmpty(merchants)) {
            return respMap;
        }
        Map<Long, Merchant> merchantMap = new HashMap<>();
        for (Merchant merchant : merchants) {
            merchantMap.put(merchant.getId(), merchant);
        }
        List<MerchantShop> merchantShops = (List<MerchantShop>) merchantShopService.listByIds(shopIds);
        Map<Long, MerchantShop> merchantShopMap = new HashMap<>();
        for (MerchantShop merchantShop : merchantShops) {
            merchantShopMap.put(merchantShop.getId(), merchantShop);
        }

        for (MerchantOrder merchantOrder : merchantOrders) {
            Merchant merchant = merchantMap.get(merchantOrder.getMerchantId());
            if (merchant == null) {
                continue;
            }
            MerchantShop merchantShop = merchantShopMap.get(merchantOrder.getShopId());
            if (merchantShop == null) {
                continue;
            }
            Map<String, Object> map = new HashMap<>();
            map.put("merchantName", merchant.getPhone());
            map.put("shopName", merchantShop.getShopName());
            map.put("isprimaryAccount", false);
            if (merchant.getParentId() == null || merchant.getParentId() == 0) {
                map.put("isprimaryAccount", true);
            }
            //账户的openid
            map.put("merchantOpenId", merchant.getOpenId());
            map.put("", "");//TODO:具体支付人详情，多人支付下的场景
            respMap.put(merchantOrder.getId(), JSON.toJSONString(map));
        }
        return respMap;
    }

    /***
     * 拼接单个商户交易对象
     * @param merchantOrder
     * @param tradeDetailJsonObject
     * @param merchantJsonObject
     * @return
     */
    @Override
    public AssembleSingleMerchantOrderDto assembleSingleMerchantOrderInfo(MerchantOrder merchantOrder, JSONObject tradeDetailJsonObject, JSONObject merchantJsonObject) {
        Map merchantJsonMap = JSONObject.toJavaObject(merchantJsonObject, Map.class);
        String payAccount = merchantJsonMap.get("merchantName").toString();
        String shopName = merchantJsonMap.get("shopName").toString();
        boolean isprimaryAccount = (boolean) merchantJsonMap.get("isprimaryAccount");
        String accountLevel = "";
        String operator = "";
        if (isprimaryAccount) {
            accountLevel = "(" + "主账号" + ")";
        } else {
            accountLevel = "(" + "子账号" + ")";
        }
        //TODO:子账号版本上线后，将其下面这行去掉
        accountLevel = "";
        operator = payAccount + accountLevel;

        String moneyLogo = "-";
        List<Integer> indexSubtracts = Arrays.asList(MfTradeTypeEnum.ADD_STOCK.getCode(), MfTradeTypeEnum.PAY_ORDER.getCode(),
                MfTradeTypeEnum.GOODS_SALE_REFUND.getCode(), MfTradeTypeEnum.GOODS_SALE_REFUND_AND_GOODS.getCode(),
                MfTradeTypeEnum.CASH.getCode(),MfTradeTypeEnum.OPEN_SHOP.getCode());
        boolean jkmfMerchant = indexSubtracts.contains(merchantOrder.getMfTradeType());
        if (!jkmfMerchant) {
            moneyLogo = "+";
        }

        String arrivalAccount = this.getMerchantOrderDetailArrivalAccount(merchantOrder, merchantJsonObject, jkmfMerchant);

        AssembleSingleMerchantOrderDto dto = new AssembleSingleMerchantOrderDto();
        String totalFee = moneyLogo + merchantOrder.getTotalFee().toString();
        dto.from(merchantOrder, totalFee, JSONObject.toJavaObject(tradeDetailJsonObject, Map.class), payAccount, arrivalAccount, operator);
        dto.setShopName(shopName);
        return dto;
    }

    /***********************************************************************************************/
    //获取到账账户信息
    private String getMerchantOrderDetailArrivalAccount(MerchantOrder merchantOrder, JSONObject merchantJsonObject, boolean jkmfMerchant) {
        Map merchantJsonMap = JSONObject.toJavaObject(merchantJsonObject, Map.class);
        String payAccount = merchantJsonMap.get("merchantName").toString();
        String arrivalAccount = "集客魔方商户号";
        if (!jkmfMerchant) {
            arrivalAccount = StringUtils.isNotBlank(merchantOrder.getPayOpenId()) &&
                    merchantJsonMap.get("merchantOpenId").equals(merchantOrder.getPayOpenId()) ? "非操作人本人支付" : payAccount;
        }
        //商户提现场景，到账不展现
        if (merchantOrder.getType() == OrderTypeEnum.USERCASH.getCode()) {
            return "";
        }
        //c端用户购买商品时
        if (merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PRESTORE.getCode() && merchantOrder.getBizType() == BizTypeEnum.SHOPPING.getCode()) {
            return payAccount;
        }
        //c端用户退款/退货退款
        if (merchantOrder.getType() == OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode() && merchantOrder.getBizType() == BizTypeEnum.SHOPPING.getCode()) {
            //查询c端用户信息
            User user = this.orderService.getUserInfoByOrderId(merchantOrder.getBizValue());
            if (user == null) {
                return "";
            }
            return user.getNickName();
        }

        return arrivalAccount;
    }

    private Map<Long, ActivityDefAudit> getActivityDefAuditInfoMap(List<ActivityDefAudit> activityDefAudits, boolean isAddStock) {
        Map<Long, ActivityDefAudit> activityDefAuditMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(activityDefAudits)) {
            for (ActivityDefAudit activityDefAudit : activityDefAudits) {
                if (isAddStock) {
                    activityDefAuditMap.put(activityDefAudit.getId(), activityDefAudit);
                } else {
                    activityDefAuditMap.put(activityDefAudit.getActivityDefId(), activityDefAudit);
                }
            }
        }
        return activityDefAuditMap;
    }

    private Map<Long, List<ActivityTask>> getActivityTaskInfoMap(List<ActivityTask> activityTasks, Map<Long, ActivityDefAudit> activityDefAuditMap) {
        Map<Long, List<ActivityTask>> activityTaskMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(activityTasks)) {
            for (ActivityTask activityTask : activityTasks) {
                Long key = activityTask.getActivityDefId();
                if (!CollectionUtils.isEmpty(activityDefAuditMap)) {
                    ActivityDefAudit activityDefAudit = activityDefAuditMap.get(activityTask.getActivityDefId());
                    if (activityDefAudit == null) {
                        continue;
                    }
                    key = activityDefAudit.getId();
                }
                List<ActivityTask> activityTaskList = activityTaskMap.get(key);
                if (CollectionUtils.isEmpty(activityTaskList)) {
                    activityTaskList = new ArrayList<ActivityTask>();
                }
                activityTaskList.add(activityTask);
                activityTaskMap.put(key, activityTaskList);
            }
        }
        return activityTaskMap;
    }
}
