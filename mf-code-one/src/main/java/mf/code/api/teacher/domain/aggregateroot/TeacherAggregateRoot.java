package mf.code.api.teacher.domain.aggregateroot;

import lombok.Data;
import mf.code.api.teacher.domain.valueobject.IncomeHistory;
import mf.code.api.teacher.domain.valueobject.InvitationHistory;
import mf.code.api.teacher.dto.ApplyTeacherReq;
import mf.code.common.utils.RegexUtils;
import mf.code.common.utils.TokenUtil;
import mf.code.teacher.constant.TeacherRoleEnum;
import mf.code.teacher.constant.TeacherStatusEnum;
import mf.code.teacher.repo.po.Teacher;
import mf.code.teacher.repo.po.TeacherInvite;
import mf.code.teacher.repo.po.TeacherOrder;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.teacher.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-28 上午11:34
 */
@Data
public class TeacherAggregateRoot {
	// teacherId，唯一标识
	private Long id;
	private String openid;
	private Teacher teacherInfo;
	private Integer role;

	// 邀请历史记录
	private InvitationHistory invitationHistory;

	// 用户资金收益
	private IncomeHistory incomeHistory;

	public Boolean applyToBeTeacher(ApplyTeacherReq applyTeacherReq) {

		if (TeacherRoleEnum.TEACHER.getCode().equals(this.role)
				|| TeacherStatusEnum.ING.getCode().equals(this.teacherInfo.getStatus())) {
			return false;
		}

		Date now = new Date();

		this.teacherInfo.setRoleName(applyTeacherReq.getRoleName());
		this.teacherInfo.setPhone(applyTeacherReq.getPhone());
		this.teacherInfo.setMerchantNumber(0);
		this.teacherInfo.setStatus(TeacherStatusEnum.ING.getCode());
		this.teacherInfo.setApplyTime(now);
		this.teacherInfo.setUtime(now);
		String merchantNumber = applyTeacherReq.getMerchantNumber();
		if (StringUtils.isNotBlank(merchantNumber) && RegexUtils.StringIsNumber(merchantNumber)) {
			this.teacherInfo.setMerchantNumber(Integer.valueOf(merchantNumber));
		}
		return true;
	}

	public Map<String, Object> getMyPageTeacherInfo() {
		Map<String, Object> myPageTeacherInfo = new HashMap<>();
		myPageTeacherInfo.put("avatarUrl", this.teacherInfo.getAvatarUrl());
		if (TeacherRoleEnum.VISTOR.getCode().equals(role)) {
			myPageTeacherInfo.put("roleName", this.teacherInfo.getNickName());
			myPageTeacherInfo.put("applied", TeacherStatusEnum.ING.getCode().equals(this.teacherInfo.getStatus()));
			return myPageTeacherInfo;
		}
		myPageTeacherInfo.put("roleName", this.teacherInfo.getRoleName());
		myPageTeacherInfo.put("phone", this.teacherInfo.getPhone());
		myPageTeacherInfo.put("popularizeCode", this.teacherInfo.getPopularizeCode());
		myPageTeacherInfo.put("balance", this.teacherInfo.getBalance());
		myPageTeacherInfo.put("totalCountMerchants",this.invitationHistory.getTotalCountMerchants());
		myPageTeacherInfo.put("recruitMerchantIncome",this.incomeHistory.getRecruitMerchantIncome());
		myPageTeacherInfo.put("totalCountTeachers", this.invitationHistory.getTotalCountTeachers());
		myPageTeacherInfo.put("recommandTeacherIncome", this.incomeHistory.getRecommandTeacherIncome());
		myPageTeacherInfo.put("quarterBouns", "-");
		myPageTeacherInfo.put("yearBouns", "-");
		myPageTeacherInfo.put("promotionBouns", "-");
		return myPageTeacherInfo;
	}

	public Map<String, Object> getHomePageTeacherInfo() {
		String roleName = this.teacherInfo.getRoleName();
		String nickName = this.teacherInfo.getNickName();
		TeacherInvite pubTeacherInvite = this.getInvitationHistory().getPubTeacherInvite();

		Map<String, Object> homePageTeacherInfo = getIncomeInfo();

		// 原申请成为集客师的数据
		Map<String, Object> applyData = new HashMap<>();
		applyData.put("roleName", roleName == null? nickName :roleName);
		applyData.put("phone", this.teacherInfo.getPhone());
		applyData.put("merchantNumber", this.teacherInfo.getMerchantNumber());
		applyData.put("fromPopularizeCode", pubTeacherInvite == null? "": pubTeacherInvite.getInviteCode());

		homePageTeacherInfo.put("applyData", applyData);
		homePageTeacherInfo.put("avatarUrl", this.teacherInfo.getAvatarUrl());
		homePageTeacherInfo.put("roleName", nickName);

		if (TeacherRoleEnum.VISTOR.getCode().equals(role)) {
			return homePageTeacherInfo;
		}
		homePageTeacherInfo.put("roleName", roleName);
		return homePageTeacherInfo;
	}

	public Map<String, Object> getLoginInfo() {
		// 生成用户令牌
		String token = TokenUtil.encryptToken(this.teacherInfo.getId().toString(), this.teacherInfo.getOpenid(), this.teacherInfo.getCtime(), TokenUtil.TEACHER);

		Map<String, Object> loginInfo = new HashMap<>();
		loginInfo.put("token", token);
		loginInfo.put("tid", this.teacherInfo.getId());
		loginInfo.put("nickName", this.teacherInfo.getNickName());
		loginInfo.put("phone", this.teacherInfo.getPhone());
		loginInfo.put("avatarUrl", this.teacherInfo.getAvatarUrl());
		return loginInfo;
	}

	private Map<String, Object> getIncomeInfo() {
		Map<String, Object> incomeInfo = new HashMap<>();
		if (TeacherRoleEnum.VISTOR.getCode().equals(role)) {
			incomeInfo.put("totalIncome", "-");
			incomeInfo.put("totalCountMerchants", "-");
			incomeInfo.put("todayIncome", "-");
			incomeInfo.put("todayCountMerchants", "-");
			return incomeInfo;
		}
		incomeInfo.put("totalIncome", this.getTeacherInfo().getTotalIncome());
		incomeInfo.put("totalCountMerchants", this.invitationHistory.getTotalCountMerchants());
		incomeInfo.put("todayIncome", this.incomeHistory.getTodayIncome());
		incomeInfo.put("todayCountMerchants",this.invitationHistory.getTodayMerchants());
		return incomeInfo;
	}

	public void setInvitationHistory(List<TeacherInvite> myInvitationRecords) {
		if (this.invitationHistory == null) {
			this.invitationHistory = new InvitationHistory(myInvitationRecords);
		}
		this.invitationHistory.init(myInvitationRecords);
	}

	public void setIncomeHistory(List<TeacherOrder> records) {
		this.incomeHistory = new IncomeHistory(records);
	}

	public void setUncertifiedInvitationHistory(List<TeacherInvite> uncertifiedInvitationRecords) {
		if (this.invitationHistory == null) {
			this.invitationHistory = new InvitationHistory();
		}
		this.invitationHistory.setUncertifiedInvitationRecords(uncertifiedInvitationRecords);
	}

	public void setPubTeacherInvite(List<TeacherInvite> pubTeacherInvite) {
		if (this.invitationHistory == null) {
			this.invitationHistory = new InvitationHistory();
		}
		if (!CollectionUtils.isEmpty(pubTeacherInvite)) {
			this.invitationHistory.setPubTeacherInvite(pubTeacherInvite.get(pubTeacherInvite.size()-1));
		}
	}
}
