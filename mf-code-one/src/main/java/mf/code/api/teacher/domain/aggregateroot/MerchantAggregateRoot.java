package mf.code.api.teacher.domain.aggregateroot;

import lombok.Data;
import mf.code.merchant.repo.po.Merchant;

/**
 * mf.code.api.teacher.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-03-04 下午3:30
 */
@Data
public class MerchantAggregateRoot {
	private Long id;
	private Merchant merchantInfo;
}
