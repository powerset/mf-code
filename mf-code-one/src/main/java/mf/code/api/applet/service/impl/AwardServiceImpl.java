package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.AwardService;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月30日 14:18
 */
@Service
public class AwardServiceImpl implements AwardService {
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisService redisService;
    @Autowired
    private GoodsService goodsService;

    /***
     * 获取活动参与人数
     * @param activityID
     * @param userID
     * @return
     */
    @Override
    public SimpleResponse queryAwardJoin(Long activityID, Long userID) {
        Map<String,Object> respMap = new HashMap<String,Object>();
        SimpleResponse d = new SimpleResponse();
        if(activityID == null || activityID == 0){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        Activity activity = this.activityService.findById(activityID);
        if(activity == null){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该活动不存在");
            return d;
        }

        respMap.put("activityJoinPerson",this.queryJoinInfo(activity, userID));
        d.setData(respMap);
        return d;
    }

    /***
     * 查询奖品详情
     * @param merchantID
     * @param shopID
     * @param activityId
     * @return
     */
    @Override
    public SimpleResponse queryGoodsDetail(Long merchantID, Long shopID, Long activityId) {
        SimpleResponse d = new SimpleResponse();
        if(merchantID == null || merchantID == 0 || shopID == null || shopID == 0 || activityId == null || activityId == 0){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        Map<String, Object> activityParams = new HashMap<String, Object>();
        activityParams.put("merchantId",merchantID);
        activityParams.put("shopId",shopID);
        activityParams.put("activityId",activityId);
        List<Activity> activities = this.activityService.findPage(activityParams);
        if(activities == null || activities.size() == 0){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该活动不存在");
            return d;
        }
        Activity activity = activities.get(0);
        if (activity.getGoodsId() == null || activity.getGoodsId() == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            d.setMessage("该活动的商品编号异常");
            return d;
        }
        Goods goods = this.goodsService.selectById(activity.getGoodsId());
        if(goods == null){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            d.setMessage("该活动的商品编号异常");
            return d;
        }

        Map<String,Object> resp = new HashMap<>();
        resp.put("goodsUrl",goods.getDescPath());
        resp.put("picUrl",goods.getPicUrl());
        resp.put("displayGoodsName",goods.getDisplayGoodsName());
        resp.put("displayPrice",goods.getDisplayPrice().toString());
        resp.put("id",goods.getId());
        d.setData(resp);
        return d;
    }

    /***
     * 获取中奖名单
     * @param activityID
     * @return
     */
    @Override
    public SimpleResponse queryWinninglist(Long activityID) {
        Map<String,Object> respMap = new HashMap<String,Object>();
        SimpleResponse d = new SimpleResponse();
        if(activityID == null || activityID == 0){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        //查询活动,获取开奖时间，还有过期时间
        Activity activity = this.activityService.findById(activityID);
        if(activity == null){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该活动查询不存在");
            return d;
        }

        //查询user_task,获取中奖状态
        List<UserTask> userTasks = this.userTaskService.queryAwardTask(activity.getMerchantId(),activity.getShopId(),null,activity.getId());
        if(userTasks == null || userTasks.size() == 0 ){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该活动没有中奖信息");
            return d;
        }
        List<Long> winnerUserIDs = new ArrayList<Long>();
        Map<Long, UserTask> userTaskMap = new HashMap<Long,UserTask>();
        for (UserTask userTask : userTasks){
            userTaskMap.put(userTask.getUserId(),userTask);
            if(winnerUserIDs.indexOf(userTask.getUserId()) == -1){
                winnerUserIDs.add(userTask.getUserId());
            }
        }
        if(CollectionUtils.isEmpty(winnerUserIDs)){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("中奖名单查询用户不存在!");
            return d;
        }
        //根据joinUserIDs查询user相关信息，目的获取nick_name
        Map<String,Object> userParams = new HashMap<>();
        userParams.put("userIds",winnerUserIDs);
        List<User> users = this.userService.query(userParams);
        if(users == null || users.size() == 0){
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("中奖名单查询用户不存在");
            return d;
        }
        Map<Long, User> userMap = this.queryUserMap(users);

        //查询redis,参与人展现,直接倒叙排列
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(activityID);
        Map<Long,JSONObject> joinUserMap = this.queryJoinUserMap(activityPersonList);

        LinkedList<Object> linkedList = new LinkedList<Object>();
        for(Long winnerUserID : winnerUserIDs){
            User user = userMap.get(winnerUserID);
            UserTask userTask = userTaskMap.get(winnerUserID);
            Map<String,Object> map = new HashMap<>();
            JSONObject jsonObject = joinUserMap.get(winnerUserID);
            if(jsonObject != null && user != null){
                map.put("avatarUrl",StringUtils.isNotBlank(jsonObject.getString("avatarUrl"))?jsonObject.getString("avatarUrl"):"");//头像地址
                map.put("userId",winnerUserID);//用户编号
                map.put("nickName",user.getNickName());//用户昵称
                map.put("awardTime", activity.getAwardStartTime() != null ?DateFormatUtils.format(activity.getAwardStartTime(),"yyyy.MM.dd HH:mm"):"");
                map.put("cutoffTime", userTask.getCtime() != null ?DateUtils.addMinutes(userTask.getCtime() ,activity.getMissionNeedTime()).getTime():0);//过期时间的截止时间
                //获取该用户邀请人信息
                List<String> activityUserJoinList = this.redisService.queryRedisJoinInviteUser(activityID,jsonObject.getLong("uid"));
                //用户自己的抽奖码信息
                List<Long> awardNos = JSONObject.parseArray(jsonObject.get("awardNo").toString(),Long.class);
                map.put("joinNum",activityUserJoinList.size() + awardNos.size());//邀请人数目
                //获奖状态 -1: 中奖任务过期 1:发奖中 3: 发奖成功
                map.put("awardStatus",this.userTaskService.getUserTaskStatus(userTask));
                linkedList.add(map);
            }
        }
        respMap.put("awardList",linkedList);
        d.setData(respMap);
        return d;
    }

    /***
     * 处理用户集合
     * @param users
     * @return
     */
    private Map<Long, User> queryUserMap(List<User> users){
        Map<Long, User> userMap = new HashMap<Long,User>();
        for(User user : users){
            userMap.put(user.getId(),user);
        }
        return  userMap;
    }

    /***
     * 处理参与人数集合 redis
     * @param activityPersonList
     * @return
     */
    private Map<Long,JSONObject> queryJoinUserMap(List<String> activityPersonList){
        Map<Long,JSONObject> joinUserMap = new HashMap<>();
        if(activityPersonList != null && activityPersonList.size() > 0){
            for(int i = 0; i < activityPersonList.size(); i++){
                JSONObject jsonObject = JSON.parseObject(activityPersonList.get(i).toString());
                joinUserMap.put(jsonObject.getLong("uid"),jsonObject);
            }
        }
        return joinUserMap;
    }

    /***
     * 获取参与人详细信息
     */
    private List<Object> queryJoinInfo(Activity activity, Long userID){
        List<Object> listYourself = new ArrayList<>();
        List<Object> listOther = new ArrayList<>();
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(activity.getId());
        if(activityPersonList == null || activityPersonList.size() == 0){
            return new ArrayList<>();
        }
        Map<Long, BigDecimal> awardRateMap = this.getUserAwardRate(activity.getId());
        if(CollectionUtils.isEmpty(awardRateMap)){
            return new ArrayList<>();
        }
        for(int i = 0; i < activityPersonList.size(); i++){
            JSONObject jsonObject = JSON.parseObject(activityPersonList.get(i).toString());
            //判断该用户是否机器人用户
            Map<String, Object> joinUserMap = this.userService.robotUserInfo(jsonObject.getLong("uid"));
            if(joinUserMap == null || joinUserMap.size() == 0){
                joinUserMap.put("avatarUrl", jsonObject.getString("avatarUrl"));
                joinUserMap.put("nickName",StringUtils.isNotBlank(jsonObject.getString("nickName"))?jsonObject.getString("nickName"):"");
            }
            joinUserMap.put("uid",jsonObject.getLong("uid"));
            joinUserMap.put("total",activityPersonList.size());
            //获取中奖率
            if(awardRateMap.get(jsonObject.getLong("uid")) != null){
                joinUserMap.put("awardRate",awardRateMap.get(jsonObject.getLong("uid")));
            }
            //用户的中奖码
            List<Long> awardNos = JSONObject.parseArray(jsonObject.get("awardNo").toString(),Long.class);
            //获取该用户邀请人信息
            List<String> activityUserJoinList = this.redisService.queryRedisJoinInviteUser(activity.getId(),jsonObject.getLong("uid"));
            //当没有邀请的时候,只有本人参与，头像上的数字不显示
            boolean orderPackStart = (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode())
                    && activity.getUserId() != null && activity.getUserId() == NumberUtils.toLong(jsonObject.get("awardNo").toString());
            //邀请人数
            if(!orderPackStart && (activityUserJoinList == null || activityUserJoinList.size() == 0)){
                joinUserMap.put("joinNum",0);
            }else {
                joinUserMap.put("joinNum",activityUserJoinList.size() + 1);
            }

            joinUserMap.put("isInventor",false);
            //判断该活动是否是发起者活动，并且判断该参与人是否是发起者本人
            if(activity.getUserId() != null && activity.getUserId().equals(jsonObject.getLong("uid"))){
                joinUserMap.put("isInventor",true);
            }
            //抽奖码个数
            joinUserMap.put("awardCodeNum",activityUserJoinList.size() + awardNos.size());
            if(jsonObject.getLong("uid") == userID.longValue()){
                //获取该用户邀请人信息
                joinUserMap.put("inviteUsers",this.queryInviteInfo(activityUserJoinList,jsonObject));
                listYourself.add(joinUserMap);
            }else {
                //获取该用户邀请人信息
                listOther.add(joinUserMap);
                //TODO:after1.0版本后的某一个版本展现--->非本人参与人数的查看
                //joinUserMap.put("inviteUsers",this.queryInviteInfo(activityUserJoinList,jsonObject));
            }
        }

        //对除自己外的人数进行排序
        Collections.sort(listOther, new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                Map<String, Object> first = (Map) o1;
                Map<String, Object> second = (Map) o2;
                //倒叙
                BigDecimal secondRate =null;
                BigDecimal firstRate = null;
                if(second.get("awardRate") != null && StringUtils.isNotBlank(second.get("awardRate").toString())){
                    secondRate = new BigDecimal(second.get("awardRate").toString());
                }
                if(first.get("awardRate") != null && StringUtils.isNotBlank(first.get("awardRate").toString())){
                    firstRate = new BigDecimal(first.get("awardRate").toString());
                }
                if(secondRate == null || firstRate == null){
                    return -1;
                }
                return (secondRate.subtract(firstRate)).intValue();
            }
        });
        listYourself.addAll(listOther);

        return listYourself;
    }

    /***
     * 获取邀请人详细信息
     * @param activityUserJoinList
     * @return
     */
    private List<Object> queryInviteInfo(List<String> activityUserJoinList,JSONObject userJsonObject){
        //获取该用户邀请人信息
        LinkedList<Object> list = new LinkedList<>();
        if(activityUserJoinList != null && activityUserJoinList.size() > 0) {
            for(String str : activityUserJoinList){
                Map<String,Object> joinUserMap = new HashMap<>();
                JSONObject jsonObject = JSON.parseObject(str);
                joinUserMap.put("isYourself", false);
                joinUserMap.put("avatarUrl", StringUtils.isNotBlank(jsonObject.getString("avatarUrl")) ? jsonObject.getString("avatarUrl") : "");
                joinUserMap.put("uid", jsonObject.getLong("uid") == 0 ? 0 : jsonObject.getLong("uid"));
                joinUserMap.put("awardNo", jsonObject.get("awardNo") == null ? 0 : JSONObject.parseArray(jsonObject.get("awardNo").toString(),Long.class).get(0));
                list.add(joinUserMap);
            }
        }
        //用户自己信息
        List<Long> awardNos = JSONObject.parseArray(userJsonObject.get("awardNo").toString(),Long.class);
        for(long awardNo : awardNos){
            Map<String,Object> joinUserMap = new HashMap<>();
            joinUserMap.put("avatarUrl", StringUtils.isNotBlank(userJsonObject.getString("avatarUrl")) ? userJsonObject.getString("avatarUrl") : "");
            joinUserMap.put("uid", userJsonObject.getLong("uid") == 0 ? 0 : userJsonObject.getLong("uid"));
            joinUserMap.put("awardNo", awardNo);
            joinUserMap.put("isYourself", true);
            list.addLast(joinUserMap);
        }
        return list;
    }

    // 私有方法：获取每个用户的中奖概率
    private Map<Long, BigDecimal> getUserAwardRate(Long aid) {
        Map<Long, BigDecimal> userMap = new HashMap<>();
        BigDecimal totalCount = new BigDecimal("0");
        Map<Object, Object> userRateMap = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + aid);
        if(userRateMap != null && userRateMap.size() > 0){
            for (Object value : userRateMap.values()) {
                totalCount = totalCount.add(new BigDecimal((String)value));
            }
            for (Map.Entry<Object, Object> entry : userRateMap.entrySet()) {
                String userId = (String) entry.getKey();
                BigDecimal count = new BigDecimal((String) entry.getValue()).multiply(new BigDecimal(100));
                BigDecimal userAwardRate = count.divide(totalCount, 2, RoundingMode.HALF_UP);
                userMap.put(NumberUtils.toLong(userId), userAwardRate);
            }
        }
        return userMap;
    }
}
