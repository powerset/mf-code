package mf.code.api.applet.v3.dto;

import lombok.Data;
import lombok.NonNull;
import mf.code.activity.constant.UserCouponTypeEnum;

import java.math.BigDecimal;

/**
 * mf.code.api.applet.v3.dto
 * Description:
 *
 * @author: gel
 * @date: 2019-02-18 10:12
 */
@Data
public class CommissionDisDto {

    @NonNull
    private Long userId;
    @NonNull
    Long shopId;
    @NonNull
    UserCouponTypeEnum userCouponType;
    @NonNull
    BigDecimal commissionDef;
    /**
     * 活动类型
     */
    Long activityId;
    Long activityDefId;
    Long activityTaskId;
    /**
     * 免单商品返现金额
     */
    BigDecimal goodsPrice = BigDecimal.ZERO;
}
