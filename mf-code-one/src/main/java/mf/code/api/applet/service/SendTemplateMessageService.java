package mf.code.api.applet.service;

import mf.code.activity.repo.po.Activity;
import mf.code.common.repo.po.CommonDict;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.user.repo.po.User;

import java.util.List;

/**
 * mf.code.api.applet.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月27日 11:13
 */
public interface SendTemplateMessageService {

    /***
     * 活动开奖的推送
     * @param shopID  店铺id
     * @param activityID 活动id
     * @param nextAwardUserID 顺延中奖者用户编号
     * @return
     */
    void sendTemplateMsg2Award(Long shopID, Long activityID, Long nextAwardUserID);

    /***
     * 审核任务的推送
     * @param userTaskID
     * @param userID
     */
    void sendTemplateMsg2Task(Long userTaskID, Long userID);

    /***
     * 成团后的第二次提醒
     * @param userTask
     * @param user
     */
    void sendTemplateMsg2ActivityCountDown(UserTask userTask, User user);

    /***
     * 任务倒计时推送消息
     * @param goodsId
     */
    void sendTemplateMsg2UserTaskCountDown(Long goodsId, Long userID, Long activityId);

    /**
     * 财富大闯关等级提升推送
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param checkpointfinishNum
     * @param commonDict
     */
    void sendTemplateMsg2CheckpointFinish(Long merchantId, Long shopId, Long userId, int checkpointfinishNum, CommonDict commonDict);

    /**
     * 不活跃粉丝推送召回
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param pushId
     * @param activityType
     */
    void sendWxPushRecall(Long merchantId, Long shopId, Long userId, Long pushId, Integer activityType);

    void sendTemplateMsg2PlatformOrderBack(Activity activity, List<Long> userIds);
}
