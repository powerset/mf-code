package mf.code.api.applet.v3.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityCommissionService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.enums.CheckpointTaskSpaceEnum;
import mf.code.api.applet.v3.service.CommissionAboutService;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.exception.CommissionReduceException;
import mf.code.common.email.EmailService;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.IpUtil;
import mf.code.distribution.constant.FanActionEventEnum;
import mf.code.distribution.dto.FanActionForMQ;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.vo.UserCouponAboutVO;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UserConstant;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v3.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-15 上午10:38
 */
@Slf4j
@Data
@Service
public class CommissionServiceImpl implements CommissionService {
    private final CommonDictService commonDictService;
    private final ActivityService activityService;
    private final ActivityDefService activityDefService;
    private final ActivityCommissionService activityCommissionService;
    private final UserCouponService userCouponService;
    private final UserService userService;
    private final UserPubJoinService userPubJoinService;
    private final MerchantShopService merchantShopService;
    private final EmailService emailService;
    private final StringRedisTemplate stringRedisTemplate;
    private final UpayWxOrderService upayWxOrderService;
    private final UpayBalanceService upayBalanceService;
    private final ActivityTaskService activityTaskService;
    @Autowired(required = false)
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private AppletUserRepository appletUserRepository;
    @Autowired
    private CommissionAboutService commissionAboutService;
    @Autowired
    private DistributionAppService distributionAppService;

    @Autowired
    public CommissionServiceImpl(CommonDictService commonDictService, ActivityService activityService, ActivityDefService activityDefService, ActivityCommissionService activityCommissionService, UserCouponService userCouponService, UserService userService, UserPubJoinService userPubJoinService, MerchantShopService merchantShopService, EmailService emailService, StringRedisTemplate stringRedisTemplate, UpayWxOrderService upayWxOrderService, UpayBalanceService upayBalanceService, ActivityTaskService activityTaskService) {
        this.commonDictService = commonDictService;
        this.activityService = activityService;
        this.activityDefService = activityDefService;
        this.activityCommissionService = activityCommissionService;
        this.userCouponService = userCouponService;
        this.userService = userService;
        this.userPubJoinService = userPubJoinService;
        this.merchantShopService = merchantShopService;
        this.emailService = emailService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.upayWxOrderService = upayWxOrderService;
        this.upayBalanceService = upayBalanceService;
        this.activityTaskService = activityTaskService;
    }

    @Override
    public CommissionBO commissionDistribution(CommissionDisDto commissionDisDto) {
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(commissionDisDto.getActivityDefId());
        if (activityDef == null) {
            log.error("<<<<<<<<完成任务存储数据，查询活动定义异常： commissionDisDto:{}", commissionDisDto);
            // 查询活动定义不存在
            throw new CommissionReduceException();
        }
        //判断是不是顶级活动定义，用户扣除保证金
        ActivityDef upperActivityDef = null;
        if (activityDef.getParentId() != null && activityDef.getParentId() > 0 && !activityDef.getParentId().equals(activityDef.getId())) {
            upperActivityDef = activityDefService.selectByPrimaryKey(activityDef.getParentId());
        }
        Long activityDefId = upperActivityDef == null ? commissionDisDto.getActivityDefId() : upperActivityDef.getId();
        //0、 2019-03-20 转移各处扣除佣金到此处，统一扣除
        Integer rows = activityDefService.reduceDeposit(activityDefId, commissionDisDto.getCommissionDef(), 0);
        if (rows == 0) {
            // 扣除佣金失败，抛异常
            throw new CommissionReduceException();
        }
        // 1、计算佣金
        CommissionBO commissionBO = commissionCalculate(commissionDisDto.getUserId(), commissionDisDto.getCommissionDef());
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(commissionDisDto.getShopId());

        // 2、存储user_coupon -- 记录完成此活动，自己能获得的奖金（已扣贡献值），仅做当前店铺的记录
        User fromUser = userService.selectByPrimaryKey(commissionBO.getFromUserId());
        Date now = new Date();
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setShopId(commissionDisDto.getShopId());
        userCoupon.setMerchantId(merchantShop.getMerchantId());
        userCoupon.setUserId(fromUser.getId());
        userCoupon.setType(commissionDisDto.getUserCouponType().getCode());
        userCoupon.setActivityId(commissionDisDto.getActivityId());
        userCoupon.setActivityDefId(commissionDisDto.getActivityDefId());

        List<Integer> list = new ArrayList<>();
        list.addAll(UserCouponAboutVO.addNewbieTaskType());
        list.addAll(UserCouponAboutVO.addShopManagerTaskType());
        if (list.contains(commissionDisDto.getUserCouponType().getCode())) {
            userCoupon.setActivityTaskId(activityDef.getParentId());
        }
        // 必填字段，但是需要判断
        if (userCoupon.getActivityId() == null) {
            userCoupon.setActivityId(commissionDisDto.getActivityDefId());
        }
        if (userCoupon.getActivityId() == null) {
            userCoupon.setActivityId(commissionDisDto.getActivityTaskId());
        }
        userCoupon.setAmount(BigDecimal.ZERO);
        userCoupon.setCommissionDef(JSON.toJSONString(commissionBO));
        //佣金赋值
        BigDecimal commission = commissionBO.getCommission();
        //幸运大转盘 直接给全额
        if (activityDef.getType() == ActivityDefTypeEnum.LUCKY_WHEEL.getCode()) {
            commission = commissionDisDto.getCommissionDef();
        }
        userCoupon.setCommission(commission);
        userCoupon.setStatus(UserCouponStatusEnum.RECEIVIED.getCode());
        userCoupon.setAwardTime(now);
        userCoupon.setCtime(now);
        userCoupon.setUtime(now);
        userCouponService.insertSelective(userCoupon);

        //直接入钱包
        boolean directEnterWallet = ActivityDefTypeEnum.directEnterWallet(activityDef.getType());
        if (directEnterWallet && commission.compareTo(BigDecimal.ZERO) > 0) {
            //直接入钱包的余额
            commissionAboutService.saveUpayWxOrderAndUpdateBalance(activityDef, true, null, fromUser, commission, merchantShop);
            distributionAppService.addIncomeRankListOfShopByUserIds(merchantShop.getId(), Arrays.asList(fromUser.getId()));
            return commissionBO;
        }
        // 3、检查是否能插入佣金user_pub_join -- 此处 只修改自己的闯关进度
        //非任务活动 佣金 全给平台-店长，新手任务出现之后概念
        if (ActivityDefTypeEnum.checkNotTaskDefType(activityDef.getType())) {
            //佣金给平台
            commissionAboutService.finishNotTaskDefType(activityDef, commissionDisDto, commissionBO, fromUser);
            return commissionBO;
        }
        /*
         * 20190704 团队管理版本添加包含佣金的活动埋点
         * 需要判断新人，店长，日常任务类型。 activityDef为详细的活动类型。upperActivityDef为可能存在的父级活动类型
         */
        asyncSendFinishTaskMq(activityDef, upperActivityDef, commissionBO);
        UserPubJoin userPubJoin = updateUserPubJoin(commissionDisDto, commissionBO, fromUser);
        if (userPubJoin == null) {
            return commissionBO;
        }

        /***
         * 2019-06-20新手店长任务迭代，将任务奖金直接入upay_wx_order 4、检查是否达成目标
         */
//        finshCheckPoint(userPubJoin, commissionBO, commissionDisDto, merchantShop);
        /***
         * 2019-06-20新手店长任务迭代，存储数据转移到平台概念 5、存入redis
         */
//        updateRedisData(commissionDisDto, commissionBO);

        //只有转收益页面才牵扯到佣金 新手任务+店长任务+日常任务
        boolean canCaculateCommission = ActivityDefTypeEnum.checkCaculateCommission(activityDef.getType())
                || commissionDisDto.getUserCouponType().getCode() == UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode();
        if (canCaculateCommission) {
            //2019-06-20新手店长任务迭代 最新完成任务的存储
            commissionAboutService.finishCheckPointNew(userCoupon, activityDef, userPubJoin, commissionBO, commissionDisDto, merchantShop);
            //2019-06-20新手店长任务迭代 最新完成任务的平台级存入redis
            commissionAboutService.updateCheckPointRedisDataNew(activityDef, commissionDisDto, commissionBO);
            distributionAppService.addIncomeRankListOfShopByUserIds(merchantShop.getId(), Arrays.asList(commissionBO.getFromUserId(),
                    commissionBO.getToUserId()));
        }
        return commissionBO;
    }

    /**
     * 异步发送完成任务埋点
     *
     * @param activityDef
     * @param upperActivityDef
     * @param commissionBO
     */
    private void asyncSendFinishTaskMq(ActivityDef activityDef, ActivityDef upperActivityDef, CommissionBO commissionBO) {
        FanActionForMQ fanActionForMQ = new FanActionForMQ();
        fanActionForMQ.setMid(activityDef.getMerchantId());
        fanActionForMQ.setSid(activityDef.getShopId());
        fanActionForMQ.setUid(commissionBO.getFromUserId());
        fanActionForMQ.setEventId(activityDef.getId());
        fanActionForMQ.setCurrent(DateUtil.getCurrentMillis());
        String fanActionEvent = FanActionEventEnum.DAILY_TASK.getCode();
        if (upperActivityDef != null) {
            if (upperActivityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK.getCode()) {
                fanActionEvent = FanActionEventEnum.NEWBIE_TASK.getCode();
            }
            if (upperActivityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode()) {
                fanActionEvent = FanActionEventEnum.SHOP_MANAGER_TASK.getCode();
            }
        }
        fanActionForMQ.setEvent(fanActionEvent);
        fanActionForMQ.setEventType(activityDef.getType().toString());
        fanActionForMQ.setAmount(commissionBO.getCommissionDef());
        fanActionForMQ.setTax(commissionBO.getTax());
        fanActionForMQ.setExtra(BeanMapUtil.beanToMap(commissionBO));
        rocketMQTemplate.asyncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.BIZ_LOG_FAN_ACTION),
                JSON.toJSONString(fanActionForMQ), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("完成任务埋点发送成功");
                    }

                    @Override
                    public void onException(Throwable e) {
                        log.info("完成任务埋点发送失败:" + fanActionForMQ.toString());
                    }
                });
    }

    private UserPubJoin updateUserPubJoin(CommissionDisDto commissionDisDto, CommissionBO commissionBO, User fromUser) {
        // 查找闯关记录
        Map<String, Object> params = new HashMap<>(4);
        params.put("subUid", commissionDisDto.getUserId());
        params.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        params.put("shopId", commissionDisDto.getShopId());
        // 查询该店铺下财富大闯关类型的数据记录，虽然只有绑定店铺才能抓取矿工，获得税金，但是其他店铺依旧可以自己做任务，自己收获佣金
        List<UserPubJoin> userPubJoins = userPubJoinService.listPageByParams(params);
        UserPubJoin userPubJoin;
        if (CollectionUtils.isEmpty(userPubJoins)) {
            // 获取上级
            Map<String, Object> vipShopParams = new HashMap<>(4);
            vipShopParams.put("subUid", commissionDisDto.getUserId());
            vipShopParams.put("type", UserPubJoinTypeEnum.SHOPPINGMALL.getCode());
            vipShopParams.put("shopId", fromUser.getVipShopId());
            List<UserPubJoin> vipShopUserPubJoins = userPubJoinService.listPageByParams(vipShopParams);
            if (CollectionUtils.isEmpty(vipShopUserPubJoins)) {
                Map<String, Object> userParams = new HashMap<>(4);
                userParams.put("merchantId", 0);
                userParams.put("shopId", 0);
                userParams.put("grantStatus", UserConstant.GRANT_STATUS_PLAT);
                List<User> users = userService.listPageByParams(userParams);
                User platUser = users.get(0);
                userPubJoin = userPubJoinService.insertUserPubJoin(-1L, platUser.getId(), fromUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            } else {
                userPubJoin = vipShopUserPubJoins.get(0);
                Long puid = userPubJoin.getUserId();
                userPubJoin = userPubJoinService.insertUserPubJoin(-1L, puid, fromUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            }
            // 查询该店铺下财富大闯关类型的数据记录，虽然只有绑定店铺才能抓取矿工，获得税金，但是其他店铺依旧可以自己做任务，自己收获佣金

        } else {
            if (userPubJoins.size() > 1) {
                String message = "当前用户同一店铺存在两条财富大闯关记录，存在严重问题:";
                log.error(message + commissionDisDto.getUserId() + JSON.toJSONString(userPubJoins));
                emailService.sendSimpleMail(message + commissionDisDto.getUserId(), JSON.toJSONString(userPubJoins));
            }
            userPubJoin = userPubJoins.get(0);
        }
        // 3.1 当前用户的绑定店铺是否与当前店铺相同，如果不相同则则判断userId是否为平台账号
        Long toUserId = userPubJoin.getUserId();
        User toUser = userService.selectByPrimaryKey(toUserId);
        // if (!userPubJoin.getShopId().equals(fromUser.getVipShopId())
        //         || !userPubJoin.getShopId().equals(commissionDisDto.getShopId())) {
        //     if (UserConstant.GRANT_STATUS_PLAT.intValue() != toUser.getGrantStatus()) {
        //         log.info("当前用户登录店铺与绑定店铺不匹配，并且有上级，存在重大问题" + JSON.toJSONString(commissionDisDto));
        //         emailService.sendSimpleMail("当前用户登录店铺与绑定店铺不匹配，并且有上级，存在重大问题:"
        //                 + commissionDisDto.getUserId(), JSON.toJSONString(userPubJoins));
        //         return null;
        //     }
        // }
        // 3.2、检查是否是平台账号，如果是平台账号则额外更新一条数据
        String lockKey = RedisKeyConstant.CHECKPOINT_UPJ_REPEAT + userPubJoin.getId();
        try {
            commissionBO.setToUserId(toUserId);
            if (UserConstant.GRANT_STATUS_PLAT.intValue() == toUser.getGrantStatus()) {
                // 更新平台税金金额
                params.clear();
                params.put("userId", toUserId);
                params.put("subUid", 0L);
                params.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
                List<UserPubJoin> userPubJoinList = userPubJoinService.listPageByParams(params);
                if(!CollectionUtils.isEmpty(userPubJoinList)){
                    String lockKey2 = RedisKeyConstant.CHECKPOINT_UPJ_REPEAT + userPubJoinList.get(0).getId();
                    boolean bool2 = RedisForbidRepeat.NX(stringRedisTemplate, lockKey2, 3, TimeUnit.SECONDS);
                    if (bool2) {
                        userPubJoinService.updatePubJoinCommission(userPubJoinList.get(0).getId(), BigDecimal.ZERO, commissionBO.getTax());
                    }
                }
            }
            boolean bool = RedisForbidRepeat.NX(stringRedisTemplate, lockKey, 3, TimeUnit.SECONDS);
            if (bool) {
                // 3.3、更新自己userPubJoin -- 自己的贡献及闯关值
                userPubJoinService.updatePubJoinCommission(userPubJoin.getId(), commissionBO.getCommission(), commissionBO.getTax());
            }
        } finally {
            stringRedisTemplate.delete(lockKey);
        }
        return userPubJoin;
    }

    /**
     * 计算佣金
     */
    @Override
    public CommissionBO commissionCalculate(Long userId, BigDecimal commissionDef) {
        return activityCommissionService.calculateCommission(userId, commissionDef);
    }

    /***
     * 返回前端的佣金计算
     * @param userId
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public CommissionResp commissionCheckpoint(Long userId, Long shopId, Long merchantId) {
        CommissionResp resp = new CommissionResp();
        resp.setCheckpoint(new CommissionResp.Checkpoint());

        CommonDict commonDict = this.commonDictService.selectByTypeKey("checkpoint", "level");
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getShopId, shopId)
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
                .eq(UserPubJoin::getSubUid, userId)
        ;
        List<UserPubJoin> userPubJoins = this.userPubJoinService.list(userPubJoinQueryWrapper);
        UserPubJoin userPubJoin = null;
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            userPubJoin = userPubJoins.get(0);
        }
        //缴税总金额
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("shopId", shopId);
        totalScottareParams.put("userId", userId);
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);

        resp.getCheckpoint().fromCheckpoint(commonDict, userPubJoin, allMinerTotalScottare);
        resp.from(userPubJoin, allMinerTotalScottare);
        resp.fromAllCheckpoint(commonDict);

        return resp;
    }

    /**
     * 更新redis数据，方便后续查询
     *
     * @param commissionDisDto 参数
     * @param commissionBO     佣金分配详情
     */
    private void updateRedisData(CommissionDisDto commissionDisDto, CommissionBO commissionBO) {
        if (commissionBO.getCommission().compareTo(BigDecimal.ZERO) == 0) {
            return;
        }
        // 5.1、本人今日收益（任务所得，税收所得），今日缴税 闯关今日信息 {"todayTaskProfit":"0.1","todayScottareProfit":"0.1","todayScottare":"0.1"}
        log.info("用户任务完成：" + JSON.toJSONString(commissionBO));
        String todayinfoKey1 = RedisKeyConstant.CHECKPOINT_TODAYINFO + commissionDisDto.getShopId() + ":" + commissionBO.getFromUserId();
        String todayInfoValue1 = stringRedisTemplate.opsForValue().get(todayinfoKey1);
        JSONObject valObj1 = JSON.parseObject(todayInfoValue1);
        if (CollectionUtils.isEmpty(valObj1)) {
            valObj1 = new JSONObject();
            valObj1.put("todayTaskProfit", "0.00");
            valObj1.put("todayScottareProfit", "0.00");
            valObj1.put("todayScottare", "0.00");
        }
        valObj1.put("todayTaskProfit", new BigDecimal(valObj1.getString("todayTaskProfit")).add(commissionBO.getCommission()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        valObj1.put("todayScottare", new BigDecimal(valObj1.getString("todayScottare")).add(commissionBO.getTax()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        stringRedisTemplate.opsForValue().set(todayinfoKey1, JSON.toJSONString(valObj1), DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);

        String fromUserTodayInfo = stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + commissionBO.getFromUserId());
        JSONObject fromUserToday = JSON.parseObject(fromUserTodayInfo);
        if (CollectionUtils.isEmpty(fromUserToday)) {
            fromUserToday = new JSONObject();
            fromUserToday.put("todayTaskProfit", "0.00");
            fromUserToday.put("todayScottareProfit", "0.00");
            fromUserToday.put("todayScottare", "0.00");
        }
        fromUserToday.put("todayTaskProfit", new BigDecimal(fromUserToday.getString("todayTaskProfit")).add(commissionBO.getCommission()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        fromUserToday.put("todayScottare", new BigDecimal(fromUserToday.getString("todayScottare")).add(commissionBO.getTax()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + commissionBO.getFromUserId(), JSON.toJSONString(fromUserToday), DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);

        String todayinfoKey2 = RedisKeyConstant.CHECKPOINT_TODAYINFO + commissionDisDto.getShopId() + ":" + commissionBO.getToUserId();
        String todayInfoValue2 = stringRedisTemplate.opsForValue().get(todayinfoKey2);
        JSONObject valObj2 = JSON.parseObject(todayInfoValue2);
        if (CollectionUtils.isEmpty(valObj2)) {
            valObj2 = new JSONObject();
            valObj2.put("todayTaskProfit", "0.00");
            valObj2.put("todayScottareProfit", "0.00");
            valObj2.put("todayScottare", "0.00");
        }
        valObj2.put("todayScottareProfit", new BigDecimal(valObj2.getString("todayScottareProfit")).add(commissionBO.getTax()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        stringRedisTemplate.opsForValue().set(todayinfoKey2, JSON.toJSONString(valObj2), DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);

        String toUserTodayInfo = stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + commissionBO.getToUserId());
        JSONObject toUserToday = JSON.parseObject(toUserTodayInfo);
        if (CollectionUtils.isEmpty(toUserToday)) {
            toUserToday = new JSONObject();
            toUserToday.put("todayTaskProfit", "0.00");
            toUserToday.put("todayScottareProfit", "0.00");
            toUserToday.put("todayScottare", "0.00");
        }
        toUserToday.put("todayTaskProfit", new BigDecimal(toUserToday.getString("todayTaskProfit")).add(commissionBO.getCommission()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        toUserToday.put("todayScottare", new BigDecimal(toUserToday.getString("todayScottare")).add(commissionBO.getTax()).setScale(2, BigDecimal.ROUND_DOWN).toString());
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + commissionBO.getToUserId(), JSON.toJSONString(toUserToday), DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);

        // 5.2、一段时间总税收金额，暂定30天
        String minerprovideProfitKey = RedisKeyConstant.CHECKPOINT_MINERPROVIDE_PROFIT + commissionDisDto.getShopId() + ":" + commissionBO.getToUserId();
        String minerprovideProfitValue = stringRedisTemplate.opsForValue().get(minerprovideProfitKey);
        if (minerprovideProfitValue == null) {
            minerprovideProfitValue = "0.00";
        }
        minerprovideProfitValue = new BigDecimal(minerprovideProfitValue).add(commissionBO.getTax()).setScale(2, BigDecimal.ROUND_DOWN).toString();
        stringRedisTemplate.opsForValue().set(minerprovideProfitKey, minerprovideProfitValue, 30L, TimeUnit.DAYS);

        // 5.3、一段时间任务所得详情，暂定30天 [{"type":1,"amount":"0.1"}] 其中 type 从CheckpointTaskSpaceEnum枚举里取
        String usertaskFinishKey = RedisKeyConstant.CHECKPOINT_USERTASK_FINISH + commissionDisDto.getShopId() + ":" + commissionBO.getFromUserId();
        String usertaskFinishValue = stringRedisTemplate.opsForValue().get(usertaskFinishKey);
        JSONArray taskArr = JSON.parseArray(usertaskFinishValue);
        if (CollectionUtils.isEmpty(taskArr)) {
            taskArr = new JSONArray();
        }
        Map<String, Object> map = new HashMap<>(2);
        map.put("type", changeUserCouponToCheckpoint(commissionDisDto.getUserCouponType()).getCode());
        map.put("amount", commissionBO.getCommission());
        taskArr.add(map);
        stringRedisTemplate.opsForValue().set(usertaskFinishKey, JSON.toJSONString(taskArr), 30L, TimeUnit.DAYS);

        // 5.4、关卡完成
        if (commissionBO.isFromFinish()) {
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.CHECKPOINT_FINISH + commissionDisDto.getShopId() + ":" + commissionBO.getFromUserId(),
                    commissionBO.getFromLevel(), 30L, TimeUnit.DAYS);
        }
    }

    /**
     * 是否达成关卡 TODO 贡献所有店铺已累加，但过关还是分店铺 ，需要与提现逻辑一起调整，待处理
     *
     * @param userPubJoin      邀请记录（subUid为自己的用户id）
     * @param commissionBO     佣金详情
     * @param commissionDisDto 参数
     * @param merchantShop     店铺
     */
    private void finshCheckPoint(UserPubJoin userPubJoin, CommissionBO commissionBO, CommissionDisDto commissionDisDto, MerchantShop merchantShop) {
        // 获取公共配置
        CommonDict byTypeKey = commonDictService.selectByTypeKey("checkpoint", "level");
        List<Map> array = JSONObject.parseArray(byTypeKey.getValue(), Map.class);
        // 获取自己此店铺中所有下级矿工的累计贡献总和
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("shopId", merchantShop.getId());
        totalScottareParams.put("userId", commissionBO.getFromUserId());
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal totalScottareProfit = this.userPubJoinService.sumByTotalScottare(totalScottareParams);
        // -_-||这里之前如果是刚创建的则会缺少字段，需要重新查询
        UserPubJoin pubJoin = userPubJoinService.getById(userPubJoin.getId());
        BigDecimal afterAmount = pubJoin.getTotalCommission().add(totalScottareProfit);
        BigDecimal beforeAmount = afterAmount.subtract(commissionBO.getCommission());
        // 关卡累计目标金额
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (Map map : array) {
            // 当前关卡金额即为待发放金额
            BigDecimal levelAmount = new BigDecimal(map.get("money").toString());
            totalAmount = totalAmount.add(levelAmount);
            if (totalAmount.compareTo(beforeAmount) > 0 && totalAmount.compareTo(afterAmount) <= 0) {
                commissionBO.setFromLevel(JSON.toJSONString(map));
                commissionBO.setFromFinish(true);
                log.info("用户关卡升级：" + JSON.toJSONString(commissionBO));
                //创建订单
                // remark: {"leftCommission":"0.02",leftScottare:"0.03"}
                Map<String, String> remark = new HashMap<>();
                remark.put("leftCommission", afterAmount.subtract(totalAmount).toString());
                remark.put("leftScottare", "0.00");
                // 当前通关的金额组成
                remark.put("commission", totalAmount.subtract(totalScottareProfit).toString());
                remark.put("scottare", totalScottareProfit.toString());
                UpayWxOrder upayWxOrder = upayWxOrderService.addPo(commissionBO.getFromUserId(), OrderPayTypeEnum.CASH.getCode(),
                        OrderTypeEnum.PALTFORMRECHARGE.getCode(), null, BizTypeEnum.CHECKPOINT.getMessage(), merchantShop.getMerchantId(),
                        merchantShop.getId(), OrderStatusEnum.ORDERED.getCode(), levelAmount, IpUtil.getServerIp(), null,
                        JSON.toJSONString(remark), new Date(), null, null, BizTypeEnum.CHECKPOINT.getCode(), Long.valueOf(map.get("name").toString()));
                upayWxOrderService.create(upayWxOrder);
                // 更新用户余额
                upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), commissionBO.getFromUserId(), levelAmount);
                //消息推送
                Map<String, Object> tempMap = new HashMap<>();
                tempMap.put("merchantId", merchantShop.getMerchantId());
                tempMap.put("shopId", merchantShop.getId());
                tempMap.put("userId", commissionBO.getFromUserId());
                tempMap.put("finishNum", map.get("name"));
                tempMap.put("commonDict", byTypeKey);
                try {
                    this.rocketMQTemplate.syncSend(RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTopic() + ":"
                            + RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTag(), JSON.toJSONString(tempMap));
                    log.info("rocketmq消息生产：" + JSON.toJSONString(tempMap));
                } catch (Exception e) {
                    log.error("rocketmq消息生产错误：" + JSON.toJSONString(tempMap), e);
                }
                break;
            }
        }
        // 本人缴税，上级是否能过关
        User toUser = userService.selectByPrimaryKey(pubJoin.getUserId());
        if (toUser.getGrantStatus().equals(UserConstant.GRANT_STATUS_PLAT)
                || commissionBO.getTax().equals(BigDecimal.ZERO)) {
            // 如果上级是平台则跳过
            return;
        }
        // 获取上级此店铺中所有下级矿工的累计贡献总和
        Map<String, Object> params = new HashMap<>(4);
        params.put("shopId", merchantShop.getId());
        params.put("userId", commissionBO.getToUserId());
        params.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal toProfit = this.userPubJoinService.sumByTotalScottare(params);
        // 获取上级此店铺财富值和贡献值
        params.clear();
        params.put("shopId", merchantShop.getId());
        params.put("subUid", commissionBO.getToUserId());
        params.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        // 查询该店铺下财富大闯关类型的数据记录，虽然只有绑定店铺才能抓取矿工，获得税金，但是其他店铺依旧可以自己做任务，自己收获佣金
        List<UserPubJoin> userPubJoins = userPubJoinService.listPageByParams(params);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            // 获取上级的上级
            Map<String, Object> vipShopParams = new HashMap<>(4);
            vipShopParams.put("subUid", toUser.getId());
            vipShopParams.put("type", UserPubJoinTypeEnum.SHOPPINGMALL.getCode());
            vipShopParams.put("shopId", toUser.getVipShopId());
            List<UserPubJoin> vipShopUserPubJoins = userPubJoinService.listPageByParams(vipShopParams);
            if (CollectionUtils.isEmpty(vipShopUserPubJoins)) {
                Map<String, Object> userParams = new HashMap<>(4);
                userParams.put("merchantId", 0);
                userParams.put("shopId", 0);
                userParams.put("grantStatus", UserConstant.GRANT_STATUS_PLAT);
                List<User> users = userService.listPageByParams(userParams);
                User platUser = users.get(0);
                userPubJoin = userPubJoinService.insertUserPubJoin(-1L, platUser.getId(), toUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            } else {
                userPubJoin = vipShopUserPubJoins.get(0);
                Long puid = userPubJoin.getUserId();
                userPubJoin = userPubJoinService.insertUserPubJoin(-1L, puid, toUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            }
            pubJoin = userPubJoin;
        } else {
            pubJoin = userPubJoins.get(0);
        }
        // 因为前面已经保存了数据，所以在此查出的数据应该是最后的金额
        BigDecimal toAfterAmount = pubJoin.getTotalCommission().add(toProfit);
        BigDecimal toBeforeAmount = toAfterAmount.subtract(commissionBO.getTax());
        totalAmount = BigDecimal.ZERO;
        for (Map map : array) {
            // 当前关卡金额即为待发放金额
            BigDecimal levelAmount = new BigDecimal(map.get("money").toString());
            totalAmount = totalAmount.add(levelAmount);
            if (totalAmount.compareTo(toBeforeAmount) > 0 && totalAmount.compareTo(toAfterAmount) <= 0) {
                commissionBO.setToLevel(JSON.toJSONString(map));
                commissionBO.setToFinish(true);
                log.info("上级用户关卡升级：" + JSON.toJSONString(commissionBO));
                //创建订单
                // remark: {"leftCommission":"0.02",leftScottare:"0.03"}
                Map<String, String> remark = new HashMap<>();
                remark.put("leftCommission", "0.00");
                remark.put("leftScottare", toAfterAmount.subtract(totalAmount).toString());
                remark.put("commission", pubJoin.getTotalCommission().toString());
                remark.put("scottare", totalAmount.subtract(pubJoin.getTotalCommission()).toString());
                UpayWxOrder upayWxOrder = upayWxOrderService.addPo(commissionBO.getToUserId(), OrderPayTypeEnum.CASH.getCode(),
                        OrderTypeEnum.PALTFORMRECHARGE.getCode(), null, BizTypeEnum.CHECKPOINT.getMessage(), merchantShop.getMerchantId(),
                        merchantShop.getId(), OrderStatusEnum.ORDERED.getCode(), levelAmount, IpUtil.getServerIp(), null,
                        JSON.toJSONString(remark), new Date(), null, null, BizTypeEnum.CHECKPOINT.getCode(), (Long) map.get("name"));
                upayWxOrderService.create(upayWxOrder);
                // 更新用户余额
                upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), commissionBO.getToUserId(), levelAmount);
                //消息推送
                Map<String, Object> tempMap = new HashMap<>();
                tempMap.put("merchantId", merchantShop.getMerchantId());
                tempMap.put("shopId", merchantShop.getId());
                tempMap.put("userId", commissionBO.getFromUserId());
                tempMap.put("finishNum", map.get("name"));
                tempMap.put("commonDict", byTypeKey);
                try {
                    this.rocketMQTemplate.syncSend(RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTopic() + ":"
                            + RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTag(), JSON.toJSONString(tempMap));
                    log.info("rocketmq消息生产：" + JSON.toJSONString(tempMap));
                } catch (Exception e) {
                    log.error("rocketmq消息生产错误：" + JSON.toJSONString(tempMap), e);
                }
                return;
            }
        }
    }

    private CheckpointTaskSpaceEnum changeUserCouponToCheckpoint(UserCouponTypeEnum userCouponType) {
        switch (userCouponType) {
            case LUCKY_WHEEL:
                return CheckpointTaskSpaceEnum.LUCKYWHEEL;
            case ORDER_RED_PACKET:
                return CheckpointTaskSpaceEnum.OPENREDPACK;
            case COUPON:
                break;
            case GOODS_COMMENT:
                return CheckpointTaskSpaceEnum.GOODCOMMENT;
            case FAV_CART:
                return CheckpointTaskSpaceEnum.FAVCART;
            case MCH_PLAN:
                break;
            case ORDER_BACK_START:
            case ORDER_BACK_JOIN:
            case NEW_MAN_START:
            case NEW_MAN_JOIN:
                return CheckpointTaskSpaceEnum.ORDERBACK;
            case ASSIST:
                return CheckpointTaskSpaceEnum.ASSIST;
            case OPEN_RED_PACKAGE_START:
            case OPEN_RED_PACKAGE_ASSIST:
                return CheckpointTaskSpaceEnum.OPENREDPACK;
            default:
                break;
        }
        return CheckpointTaskSpaceEnum.ORDERBACK;
    }


    private BizTypeEnum changeUserCouponToBizType(UserCouponTypeEnum userCouponType) {
        switch (userCouponType) {
            case LUCKY_WHEEL:
                return BizTypeEnum.LUCKY_WHEEL;
            case ORDER_RED_PACKET:
                return BizTypeEnum.RANDOMREDPACK;
            case COUPON:
                break;
            case GOODS_COMMENT:
                return BizTypeEnum.GOODSCOMMENT;
            case FAV_CART:
                return BizTypeEnum.REDPACKTASK;
            case MCH_PLAN:
                break;
            case ORDER_BACK_START:
            case ORDER_BACK_JOIN:
                return BizTypeEnum.ORDER_BACK;
            case NEW_MAN_START:
            case NEW_MAN_JOIN:
                return BizTypeEnum.NEWMAN_START;
            case ASSIST:
                return BizTypeEnum.ASSIST_ACTIVITY;
            case OPEN_RED_PACKAGE_START:
            case OPEN_RED_PACKAGE_ASSIST:
                return BizTypeEnum.OPEN_RED_PACKAGE;
            default:
                break;
        }
        return BizTypeEnum.ACTIVITY;
    }
}
