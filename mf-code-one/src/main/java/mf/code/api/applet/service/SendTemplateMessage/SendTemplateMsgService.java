package mf.code.api.applet.service.SendTemplateMessage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.push.service.PushHistoryService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.service.SendTemplateMessage
 * <p>
 * from: 1.新人|2.活动|3.中奖任务审核|4.红包任务审核|6.助力活动|9.拆红包|10.好评晒图|11.财富大闯关
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 09:47
 */
@Slf4j
public class SendTemplateMsgService implements SendTemplateMessageConverter {
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private PushHistoryService pushHistoryService;
    /**
     * 放大/换行 第n行
     */
    public static final String EMPHASISKEYWORD1 = "keyword1.DATA";
    public static final String EMPHASISKEYWORD2 = "keyword2.DATA";
    public static final String EMPHASISKEYWORD3 = "keyword3.DATA";
    public static final String EMPHASISKEYWORD4 = "keyword4.DATA";

    /***
     * 获取推送消息的具体跳转页
     * @param activity 活动实体
     * @param userID 是否是新人有礼发起者
     * @param userTask 审核任务
     * @return
     */
    public String getPageUrl(Activity activity,
                             Long userID,
                             UserTask userTask) {
        //"from: 1.新人|2.活动"|3 中奖任务审核|4红包任务审核|6助力活动|9拆红包|10好评晒图---前端小伙伴的定义
        int from = 2;
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode() && activity.getUserId() != null && activity.getUserId().equals(userID)) {
            from = 1;
        }
        if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST.getCode()) {
            from = 6;
        }
        if (activity.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()) {
            from = 9;
        }
        Long goodsId = 0L;
        Long taskId = 0L;
        if (userTask != null && userTask.getType() != null) {
            if (userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                from = 4;
                taskId = userTask.getId();
                goodsId = userTask.getGoodsId();
            } else if (userTask.getType() == UserTaskTypeEnum.GOOD_COMMENT.getCode()) {
                from = 10;
                taskId = userTask.getId();
                goodsId = userTask.getGoodsId();
            } else {
                from = 3;
            }
        }
        Long activityDefId = 0L;
        if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
            activityDefId = activity.getActivityDefId();
        }
        return this.getPageUrl(activity.getMerchantId(), activity.getShopId(), from, activity.getId(), activityDefId, goodsId, taskId);
    }

    /***
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public String getPageUrlCheckpoint(Long merchantId, Long shopId) {
        int from = 11;
        //source--推送消息来源
        return this.getPageUrl(merchantId, shopId, from, 0L, 0L, 0L, 0L);
    }

    /***
     * pageUrl
     * @param merchantId
     * @param shopId
     * @param from
     * @param activityId
     * @param activityDefId
     * @param goodsId
     * @param taskId
     * @return
     */
    public String getPageUrl(Long merchantId,
                             Long shopId,
                             int from,
                             Long activityId,
                             Long activityDefId,
                             Long goodsId,
                             Long taskId) {
        //source--推送消息来源
        String result = "source=1&merchantId=" + String.valueOf(merchantId) +
                "&shopId=" + String.valueOf(shopId) +
                "&from=" + String.valueOf(from) +
                "&activityId=" + String.valueOf(activityId) +
                "&activityDefId=" + String.valueOf(activityDefId) +
                "&goodsId=" + String.valueOf(goodsId) +
                "&taskId=" + String.valueOf(taskId);
        return result;
    }

    /***
     * 依赖于场景参数跳转的落地页详情
     * @param objects
     * @return
     */
    public String getPageUrlrelyOnScene(Object[] objects) {
        if (objects.length > 0) {
            return "scene=" + StringUtil.join(objects, ",");
        }
        return "";
    }

    /***
     * 填充消息模板关键词
     * @param keywordNum 关键词数量
     * @param objects value对象信息
     * @return
     */
    @Override
    public Map<String, WxSendMsgVo.TemplateData> getTempteDate(int keywordNum, Object[] objects) {
        Map<String, WxSendMsgVo.TemplateData> m = new HashMap<>();
        for (int i = 0; i < keywordNum; i++) {
            WxSendMsgVo.TemplateData map = new WxSendMsgVo.TemplateData();
            String key = "keyword" + (i + 1);
            String value = objects[i].toString();
            map.setValue(value);
            m.put(key, map);
        }
        return m;
    }

    /***
     * 当formId使用过了，无限递归
     * @param stringObjectMap
     * @param vo
     * @param userId
     * @return
     */
    @Override
    public boolean respMap(Map<String, Object> stringObjectMap, WxSendMsgVo vo, Long userId) {
        boolean isRetry = stringObjectMap != null && ("41029").equals(stringObjectMap.get("errcode"));
        if (isRetry) {
            String formId = this.queryRedisUserFormIds(userId);
            if (StringUtils.isBlank(formId)) {
                log.warn("========消息推送formId已耗尽，请尽快补充， vo={}", vo);
                return false;
            }
            if (!vo.getForm_id().equals(formId)) {
                vo.setForm_id(formId);
                stringObjectMap = new HashMap<>();
                stringObjectMap = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
                log.info("递归抽奖活动推送消息结果：{}, parms:{}", stringObjectMap, vo);
                this.respMap(stringObjectMap, vo, userId);
            }
        }
        return true;
    }

    @Override
    public String queryRedisUserFormIds(Long userID) {
        //查询redis,参与人展现,直接倒叙排列
        BoundListOperations formIDS = this.stringRedisTemplate.boundListOps(wxmpProperty.getRedisKey(userID));
        List<Object> formIDStrs = formIDS.range(0, -1);
        if (formIDStrs == null || formIDStrs.size() == 0) {
            return null;
        }
        String formID = null;
        int index = 0;
        for (Object obj : formIDStrs) {
            JSONObject jsonObject = JSON.parseObject(obj.toString());
            //判断该formId是否为有效的 7天内有效
            long DayMillis = 24 * 60 * 60 * 1000;
            if ((System.currentTimeMillis() - jsonObject.getLong("ctime")) / DayMillis > 6) {
                //过期都删除
                this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(userID), index, obj);
                index++;
                continue;
            }
            formID = jsonObject.getString("formId");
            //用完即删
            this.stringRedisTemplate.opsForList().remove(wxmpProperty.getRedisKey(userID), index, obj);
            break;
        }
        return formID;
    }

    @Override
    public String awardSendTemplateMessageConverter(MerchantShop merchantShop,
                                                    Activity activity,
                                                    Long nextAwardUserID) {
        return null;
    }

    @Override
    public String userTaskSendTemplateMessageConverter(MerchantShop merchantShop,
                                                       Activity activity,
                                                       UserTask userTask,
                                                       User user,
                                                       Goods goods) {
        return null;
    }

    @Override
    public String countDownSendTemplateMessageConverter(Integer countDownType,
                                                        Long userID,
                                                        Long activityId,
                                                        Long goodsId,
                                                        UserTask userTask,
                                                        User user) {
        return null;
    }

    @Override
    public String sendTemplateMsg2CheckpointFinish(Long merchantId,
                                                   Long shopId,
                                                   Long userId,
                                                   int checkpointfinishNum,
                                                   CommonDict commonDict) {
        return null;
    }

    /**
     * 推送召回   复制叶辰
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param pushId
     * @param activityType
     * @return
     */
    @Override
    public String sendWxPushRecall(Long merchantId, Long shopId, Long userId, Long pushId, Integer activityType) {
        return null;
    }
}
