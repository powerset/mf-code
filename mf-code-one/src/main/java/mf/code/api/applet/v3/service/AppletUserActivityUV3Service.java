package mf.code.api.applet.v3.service;

import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.v3.dto.RedPackageReq;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.repo.po.User;

/**
 * mf.code.api.applet.v3.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-17 下午1:09
 */
public interface AppletUserActivityUV3Service {
	/**
	 * 立即拆开
	 * @param redPackageReq
	 * @return
	 */
	SimpleResponse openRedPackage(RedPackageReq redPackageReq);

	/**
	 * 发起活动
	 * @param userId
	 * @param activityDefId
	 * @return
	 */
	Activity createActivity(Long userId, Long activityDefId);

	/**
	 * 参与或助力活动
	 * @param userId
	 * @param activity
	 */
	User joinOrAssistActivity(Long userId, Long pubUserId, Activity activity);

	/**
	 * 通用 开奖方法
	 */
	boolean distributePrizes(Activity activity);

	/**
	 * 开奖后，对用户资余额操作
	 * @param activity
	 */
	void addUserBalance(Activity activity);

	/**
	 * 拆红包活动 从分享卡片进入活动详情
	 * @param userId
	 * @param pubUserId
	 * @param activityId
	 * @return
	 */
	SimpleResponse pubJoinOpenRedPackageActivity(String userId, String pubUserId, String activityId, Integer pubJoinScene);

	/**
	 * 扫码弹窗回填订单
	 * @param backFillOrderReq
	 * @return
	 */
	SimpleResponse backfillOrderPopup(BackFillOrderReq backFillOrderReq);

	/***
	 * 扫码弹窗回填订单异步处理
	 * @param backFillOrderReq
	 * @return
	 */
	SimpleResponse backfillOrderPopupAsync(BackFillOrderReq backFillOrderReq);


	/**
	 * 扫码弹窗回填订单
	 * @param shopId
	 * @param userId
	 * @param orderId
	 * @return
	 */
	SimpleResponse backfillOrderPopupReplace(Long shopId, Long userId, String orderId);

	SimpleResponse queryBackfillOrderResult(String orderId);

	SimpleResponse backfillOrderAsync(BackFillOrderReq req);
}
