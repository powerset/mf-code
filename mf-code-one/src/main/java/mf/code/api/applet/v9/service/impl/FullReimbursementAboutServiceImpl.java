package mf.code.api.applet.v9.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.domain.applet.valueobject.ActivityPopupStatusEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.v9.service.FullReimbursementAboutService;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.utils.DateUtil;
import mf.code.one.constant.ActivityDetailStatusEnum;
import mf.code.one.dto.activitydetail.AssertUserDto;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v9.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月05日 17:33
 */
@Service
@Slf4j
public class FullReimbursementAboutServiceImpl implements FullReimbursementAboutService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserService userService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private ActivityService activityService;

    /***
     * 获取活动的状态
     *
     * @param userId 用户编号
     * @param appletAssistActivity 小程序助力对象信息
     * @return
     */
    @Override
    public ActivityDetailStatusEnum findByActivityDetailStatusEnum(Long userId, AppletAssistActivity appletAssistActivity) {
        Map<String, Map<String, Object>> assistUserInfoMap = appletAssistActivity.getAssistUserInfoMap();
        Activity activityInfo = appletAssistActivity.getActivityInfo();

        ActivityDetailStatusEnum activityDetailStatusEnum = null;
        //是否是本人
        boolean self = userId.equals(activityInfo.getUserId());
        //是否开奖
        boolean award = appletAssistActivity.getIsAwardDialog();
        //判断是否过期
        boolean expired = appletAssistActivity.isExpired();

        if (expired) {
            activityDetailStatusEnum = ActivityDetailStatusEnum.EXPIRED;
            return activityDetailStatusEnum;
        }
        if (self) {
            activityDetailStatusEnum = ActivityDetailStatusEnum.SELF_INVITE;
            //是否开奖
            if (activityInfo.getAwardStartTime() != null) {
                activityDetailStatusEnum = ActivityDetailStatusEnum.AWARDED;
                return activityDetailStatusEnum;
            }
            return activityDetailStatusEnum;
        }

        //是否助力
        log.info("<<<<<<<< 助力者们信息：assistMap:{}", JSONObject.toJSONString(assistUserInfoMap));
        if (!CollectionUtils.isEmpty(assistUserInfoMap)) {
            Map<String, Object> stringObjectMap = assistUserInfoMap.get(userId.toString());
            log.info("<<<<<<<< 助力者们信息：assistMap:{}", JSONObject.toJSONString(stringObjectMap));
            if (stringObjectMap != null) {
                // 已助力
                activityDetailStatusEnum = ActivityDetailStatusEnum.HELPED_FRIEND_CREATE;
                String redisKey = RedisKeyConstant.ASSIST_SUCCESS + activityInfo.getId() + ":" + userId;
                String str = stringRedisTemplate.opsForValue().get(redisKey);
                if (StringUtils.isNotBlank(str)) {
                    //未助力
                    activityDetailStatusEnum = ActivityDetailStatusEnum.HELP_FRIEND;
                }
                return activityDetailStatusEnum;
            } else {
                //老用户
                activityDetailStatusEnum = ActivityDetailStatusEnum.OLD_USER_CREATE;
                return activityDetailStatusEnum;
            }
        } else {
            //老用户
            activityDetailStatusEnum = ActivityDetailStatusEnum.OLD_USER_CREATE;
            return activityDetailStatusEnum;
        }
    }

    /***
     * 用户第一次助力时，若该活动的前几人直接到账数目还未达到,存储到账redis
     *
     * @param appletAssistActivity
     * @param fullReimbursementMoneyNum 助力已到账人数
     */
    @Override
    public void saveCanCashRedisByFullReimbursement(Long userId, AppletAssistActivity appletAssistActivity, int fullReimbursementMoneyNum) {
        BigDecimal openAmount = appletAssistActivity.getOpenAmount();
        Activity activityInfo = appletAssistActivity.getActivityInfo();
        String redisKeyJoin = RedisKeyConstant.ACTIVITY_JOINLIST + activityInfo.getId();
        Long count = stringRedisTemplate.opsForList().size(redisKeyJoin);
        log.info("<<<<<<<< 助力直接入账存储redis1 助力开始 stringMap:{}, upaySize:{} hit:{}",
                count, fullReimbursementMoneyNum, activityInfo.getHitsPerDraw());
        if (count <= activityInfo.getHitsPerDraw() && activityInfo.getHitsPerDraw() >= fullReimbursementMoneyNum) {
            if (openAmount != null && openAmount.compareTo(BigDecimal.ZERO) > 0 && !userId.equals(activityInfo.getUserId())) {
                String redisKey = RedisKeyConstant.ACTIVITY_ASSIST_CAN_CASH + activityInfo.getId() + ":" + activityInfo.getUserId();
                BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
                List range = activityPersons.range(0, -1);
                if ((!CollectionUtils.isEmpty(range) && activityInfo.getHitsPerDraw() > range.size())
                        || CollectionUtils.isEmpty(range)) {
                    JSONObject userJsonObject = new JSONObject();

                    User user = userService.selectByPrimaryKey(userId);
                    if (user == null) {
                        return;
                    }
                    userJsonObject.put("nickName", user.getNickName());
                    userJsonObject.put("avatarUrl", user.getAvatarUrl());
                    userJsonObject.put("amount", openAmount);
                    stringRedisTemplate.opsForList().leftPush(redisKey, userJsonObject.toJSONString());
                    stringRedisTemplate.expire(redisKey, activityInfo.getEndTime().getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
                    log.info("<<<<<<<< 助力直接入账存储redis3 完成");
                }
            }
        }
    }

    @Override
    public List queryFullReimbursementDialog(Long userId, ActivityDef activityDef, Activity activity, int type) {
        List list = new ArrayList();

        boolean self = userId.equals(activity.getUserId());

        if (self) {
            String redisKey = RedisKeyConstant.ACTIVITY_ASSIST_CAN_CASH + activity.getId() + ":" + activity.getUserId();
            BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
            List range = activityPersons.range(0, -1);
            if (!CollectionUtils.isEmpty(range)) {
                JSONObject jsonObject = JSON.parseObject(range.get(0).toString());
                if (jsonObject != null) {
                    Map map = new HashMap();
                    map.put("type", 1);
                    map.put("nickName", jsonObject.get("nickName"));
                    map.put("amount", jsonObject.get("amount"));
                    list.add(map);
                    stringRedisTemplate.delete(redisKey);
                }
            }
        }
        //已过期
        boolean expire = false;
        //已中奖
        boolean award = false;
        if (activity != null) {
            if (System.currentTimeMillis() > activity.getEndTime().getTime()) {
                expire = true;
            }
            //已开奖
            if (activity.getAwardStartTime() != null) {
                award = true;
            }
        }
        if (award) {
            //本人中奖
            if (self) {
                Map map = new HashMap();
                map.put("amount", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN));
                map.put("type", 2);

                String redisKey = RedisKeyConstant.AWARDDIALOG_ACTIVITY_UID + activity.getId() + ":" + userId;
                String redisStr = stringRedisTemplate.opsForValue().get(redisKey);
                if(StringUtils.isNotBlank(redisStr)){
                    list.add(map);
                    stringRedisTemplate.delete(redisKey);
                }
            } else {
                if (type == ActivityDetailStatusEnum.HELP_FRIEND.getCode()) {
                    Map map = new HashMap();
                    map.put("amount", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN));
                    map.put("type", 5);
                    list.add(map);
                }
            }
        }
        if (expire && !award) {
            if (self) {
                //本人过期
                Map map = new HashMap();
                map.put("amount", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN));
                map.put("type", 3);
                list.add(map);
            } else {
                //助力人进入过期活动
                Map map = new HashMap();
                map.put("amount", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN));
                map.put("type", 4);

                if (activityDef != null) {
                    String amountJson = activityDef.getAmountJson();
                    JSONObject jsonObject = JSON.parseObject(amountJson);
                    if (jsonObject != null) {
                        JSONObject jsonObjectPerAmount = JSON.parseObject(jsonObject.get("perAmount").toString());
                        map.put("amount", jsonObjectPerAmount.get("max"));
                    }
                }
                list.add(map);
            }
        }
        return list;
    }

    @Override
    public BigDecimal getFullReimbursementMoney(Long mid, Long sid, Long userId, Long activityId, Integer hitPerDraw) {
        Map wxOrderParams = new HashMap();
        wxOrderParams.put("mchId", mid);
        wxOrderParams.put("shopId", sid);
        wxOrderParams.put("userId", userId);
        wxOrderParams.put("type", OrderTypeEnum.PALTFORMRECHARGE.getCode());
        wxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        wxOrderParams.put("bizTypes", Arrays.asList(
                BizTypeEnum.FULL_REIMBURSEMENT.getCode()
        ));
        wxOrderParams.put("bizValue", activityId);
        wxOrderParams.put("order", "id");
        wxOrderParams.put("direct", "asc");
        List<UpayWxOrder> upayWxOrders = upayWxOrderService.query(wxOrderParams);
        if (CollectionUtils.isEmpty(upayWxOrders)) {
            return BigDecimal.ZERO;
        }

        if (hitPerDraw != null && upayWxOrders.size() > hitPerDraw) {
            upayWxOrders = upayWxOrders.subList(0, hitPerDraw);
        }
        BigDecimal sum = BigDecimal.ZERO;
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            sum = sum.add(upayWxOrder.getTotalFee());
        }
        return sum;
    }

    @Override
    public int getFullReimbursementMoneyNum(Long mid, Long sid, Long userId, Long activityId) {
        Map wxOrderParams = new HashMap();
        wxOrderParams.put("mchId", mid);
        wxOrderParams.put("shopId", sid);
        wxOrderParams.put("userId", userId);
        wxOrderParams.put("type", OrderTypeEnum.PALTFORMRECHARGE.getCode());
        wxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        wxOrderParams.put("bizTypes", Arrays.asList(
                BizTypeEnum.FULL_REIMBURSEMENT.getCode()
        ));
        wxOrderParams.put("bizValue", activityId);
        wxOrderParams.put("order", "id");
        wxOrderParams.put("direct", "asc");
        List<UpayWxOrder> upayWxOrders = upayWxOrderService.query(wxOrderParams);
        return upayWxOrders.size();
    }

    /***
     * 查找报销活动进行中的
     *
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public Map<String, Object> fullReimbursementPopup(Long shopId, Long userId) {
        Map<String, Object> resultVO = new HashMap<>();
        //有无正在进行的活动
        QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
        activityQueryWrapper.lambda()
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getUserId, userId)
                .eq(Activity::getType, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode())
                .eq(Activity::getStatus, mf.code.activity.constant.ActivityStatusEnum.PUBLISHED.getCode())
                .eq(Activity::getDel, mf.code.common.DelEnum.NO.getCode())
                .isNull(Activity::getAwardStartTime)
        ;
        activityQueryWrapper.orderByDesc("id");
        List<Activity> activities = activityService.list(activityQueryWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            resultVO.put("activityStatus", ActivityPopupStatusEnum.NONE.getCode());
            return resultVO;
        }
        Activity activityInfo = activities.get(0);

        String redisKey = RedisKeyConstant.ACTIVITY_JOINLIST + activityInfo.getId();
        BoundListOperations activityPersons = this.stringRedisTemplate.boundListOps(redisKey);
        List joinAssistUserInfo = activityPersons.range(0, -1);

        //剩余几个人可完成
        int remain = activityInfo.getCondPersionCount() - joinAssistUserInfo.size();
        resultVO.put("activityDefId", activityInfo.getActivityDefId());
        resultVO.put("activityId", activityInfo.getId());
        resultVO.put("activityStatus", ActivityPopupStatusEnum.FEW_FRIENDS.getCode());
        resultVO.put("surplusFriend", remain);
        resultVO.put("leftAmount", activityInfo.getDeposit());
        resultVO.put("leftTime", activityInfo.getEndTime().getTime() - System.currentTimeMillis());
        return resultVO;
    }

    /***
     * 获取活动助力信息--后期这方法 被百川取缔掉
     * @param activityId
     * @return
     */
    @Override
    public AppletMybatisPageDto<AssertUserDto> getAssistListPage(Long activityId, int offset, int size) {
        AppletMybatisPageDto<AssertUserDto> respList = new AppletMybatisPageDto<>(6, 0);
        String redisKey = RedisKeyConstant.ACTIVITY_JOINLIST + activityId;
        Long count = stringRedisTemplate.opsForList().size(redisKey);
        if (count == null || count <= 0) {
            return respList;
        }
        int start = offset;
        int end = (offset / size + 1) * size - 1;
        List<String> strs = stringRedisTemplate.opsForList().range(redisKey, start, end);
        if (CollectionUtils.isEmpty(strs)) {
            return respList;
        }

        for (String str : strs) {
            JSONObject jsonObject = JSONObject.parseObject(str);
            String amount = jsonObject.getString("amount");
            String avatarUrl = jsonObject.getString("avatarUrl");
            String nickName = jsonObject.getString("nickName");
            String time = jsonObject.getString("time");

            AssertUserDto assertUserDto = new AssertUserDto();
            assertUserDto.setAmount(amount);
            assertUserDto.setAvatarUrl(avatarUrl);
            assertUserDto.setNickName(nickName);
            assertUserDto.setDate(DateUtil.parseDate(time, null, "yyyy.MM.dd HH.mm"));
            respList.getContent().add(assertUserDto);
        }
        if (count > offset / size + 1) {
            respList.setPullDown(true);
        }
        respList.setLimit(size);
        respList.setOffset(offset);
        return respList;
    }
}
