package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.repo.redis.RedisKeyConstant;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.TaskSpaceNodeDto;
import mf.code.api.applet.service.AppletTaskSpaceService;
import mf.code.api.applet.service.HomePageService;
import mf.code.api.applet.v3.service.CheckpointTaskSpaceService;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.caller.wxmp.WeixinMpConstants;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisCommonKeys;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.FakeDataUtil;
import mf.code.common.utils.ListTools;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.repo.redis.RedisKeys;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月31日 09:48
 */
@Service
@Slf4j
public class HomePageServiceImpl implements HomePageService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private AppletTaskSpaceService appletTaskSpaceService;
    @Autowired
    private CheckpointTaskSpaceService checkpointTaskSpaceService;

    /***
     * 查询活动首页信息 //TODO:轮盘抽奖展现，返现红包展现
     * @param merchantID
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryHomePage(Long merchantID, Long shopID, Long userID, String scene, int offset, int size) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        SimpleResponse d = new SimpleResponse();
        if (shopID == null || shopID == 0 ||
                merchantID == null || merchantID == 0 ||
                userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }

        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("根据店铺编号shopId:" + shopID + "未查询到该店铺的存在~");
            return d;
        }
        Merchant merchant = this.merchantService.getMerchant(merchantID);
        if (merchant == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("根据商户编号merchantID:" + merchantID + "未查询到该店铺的存在~");
            return d;
        }
        String shopInfoStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOMEPAGE_ACTIVITY_SHOP + shopID);
        if (StringUtils.isNotBlank(shopInfoStr)) {//商户店铺信息的展现
            respMap.put("shopInfo", JSON.parseObject(shopInfoStr, Map.class));
        } else {
            respMap.put("shopInfo", this.queryMerchantShop(merchantShop));
        }
        String merchantStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOMEPAGE_ACTIVITY_MERCHANT + shopID);
        if (StringUtils.isNotBlank(merchantStr)) {//商户计划活动
            respMap.put("merchantActivity", JSON.parseObject(merchantStr, Map.class));
        } else {
            respMap.put("merchantActivity", this.queryMerchantActivity(merchantShop.getMerchantId(), shopID));
        }
        String orderStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOMEPAGE_ACTIVITY_ORDER + shopID);
        if (StringUtils.isNotBlank(orderStr) && offset == 0) {//用户发起活动 等于0时存储redis10s
            respMap.put("exemptionSquare", JSON.parseObject(orderStr, Map.class));
        } else {
            respMap.put("exemptionSquare", this.queryUserOrderActivity(merchantShop.getMerchantId(), shopID, userID, offset, size));
        }

        Map<String, Object> luckyWheelParams = new HashMap<String, Object>();
        luckyWheelParams.put("merchantId", merchantID);
        luckyWheelParams.put("shopId", shopID);
        luckyWheelParams.put("type", ActivityTypeEnum.LUCK_WHEEL.getCode());
        luckyWheelParams.put("del", DelEnum.NO.getCode());
        luckyWheelParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        luckyWheelParams.put("order", "ctime");
        luckyWheelParams.put("direct", "desc");
        List<Activity> activities = this.activityService.findPage(luckyWheelParams);

        Map<String, Object> map = new HashMap<>();
        //轮盘是否弹窗
        map.put("isLuckyWheel", false);
        //轮盘挂件是否存在
        map.put("isLuckyWheelExist", false);
        map.put("activityId", 0);
        respMap.put("luckyWheel", map);
        if (activities != null && activities.size() > 0) {
            respMap.put("luckyWheel", this.addLuckyWheel(activities.get(0), userID));
        }
        respMap.put("isDialogOrder", false);
        if (StringUtils.isNotBlank(scene)) {
            List<String> sceneIDs = Arrays.asList(scene.split(","));
            if (sceneIDs != null && sceneIDs.size() >= 3 && WeixinMpConstants.SCENE_PACKAGE == NumberUtils.toInt(sceneIDs.get(2))) {
                Map<String, Object> activityDefParams = new HashMap<>();
                activityDefParams.put("merchantId", merchantID);
                activityDefParams.put("shopId", shopID);
                activityDefParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
                activityDefParams.put("del", DelEnum.NO.getCode());
                activityDefParams.put("type", ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
                List<ActivityDef> activityDefs = this.activityDefService.selectByParams(activityDefParams);
                if (activityDefs != null && activityDefs.size() > 0) {
                    respMap.put("isDialogOrder", true);
                }
            }
        }
        // 无数据添加假数据
        FakeDataUtil.homeData(respMap, offset);
        d.setData(respMap);
        return d;
    }


    private Map setWheelInfo(Map<String, Object> respMap, Long merchantID, Long shopID, String scene, Long userID) {
        Map<String, Object> luckyWheelParams = new HashMap<String, Object>();
        luckyWheelParams.put("merchantId", merchantID);
        luckyWheelParams.put("shopId", shopID);
        luckyWheelParams.put("type", ActivityTypeEnum.LUCK_WHEEL.getCode());
        luckyWheelParams.put("del", DelEnum.NO.getCode());
        luckyWheelParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        luckyWheelParams.put("order", "ctime");
        luckyWheelParams.put("direct", "desc");
        List<Activity> activities = this.activityService.findPage(luckyWheelParams);

        Map<String, Object> map = new HashMap<>();
        //轮盘是否弹窗
        map.put("isLuckyWheel", false);
        //轮盘挂件是否存在
        map.put("isLuckyWheelExist", false);
        map.put("activityId", 0);
        respMap.put("luckyWheel", map);
        if (activities != null && activities.size() > 0) {
            respMap.put("luckyWheel", this.addLuckyWheel(activities.get(0), userID));
        }
        respMap.put("isDialogOrder", false);
        if (StringUtils.isNotBlank(scene)) {
            List<String> sceneIDs = Arrays.asList(scene.split(","));
            if (sceneIDs != null && sceneIDs.size() >= 3 && WeixinMpConstants.SCENE_PACKAGE == NumberUtils.toInt(sceneIDs.get(2))) {
                Map<String, Object> activityDefParams = new HashMap<>();
                activityDefParams.put("merchantId", merchantID);
                activityDefParams.put("shopId", shopID);
                activityDefParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
                activityDefParams.put("del", DelEnum.NO.getCode());
                activityDefParams.put("type", ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
                List<ActivityDef> activityDefs = this.activityDefService.selectByParams(activityDefParams);
                if (activityDefs != null && activityDefs.size() > 0) {
                    respMap.put("isDialogOrder", true);
                }
            }
        }
        return respMap;
    }

    /**
     * 首页服务 v1.2
     */
    @Override
    public SimpleResponse queryHomePageV12(Long merchantID, Long shopID, Long userID, String scene, Long lastId, int pageSize) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        respMap = this.setBaseInfoV12(respMap, shopID, userID, merchantShop);
        respMap = this.setWheelInfo(respMap, merchantID, shopID, scene, userID);


        SimpleResponse response = new SimpleResponse();
        response.setData(respMap);
        return new SimpleResponse();
    }

    /**
     * 设置页面基础数据 v1.2
     *
     * @param respMap
     * @param shopID
     * @param userID
     * @param merchantShop
     * @return
     */
    private Map setBaseInfoV12(Map<String, Object> respMap, Long shopID, Long userID, MerchantShop merchantShop) {
        String shopInfoStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOMEPAGE_ACTIVITY_SHOP + shopID);
        //商户店铺信息的展现
        if (StringUtils.isNotBlank(shopInfoStr)) {
            respMap.put("shopInfo", JSON.parseObject(shopInfoStr, Map.class));
        } else {
            respMap.put("shopInfo", this.queryMerchantShop(merchantShop));
        }
        String orderStr = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOMEPAGE_ACTIVITY_ORDER + shopID);
        //用户发起活动 等于0时存储redis10s
        if (StringUtils.isNotBlank(orderStr)) {
            respMap.put("exemptionSquare", JSON.parseObject(orderStr, Map.class));
        } else {
            respMap.put("exemptionSquare", this.queryUserOrderActivity(merchantShop.getMerchantId(), shopID, userID, 1, 1));
        }
        return respMap;
    }


    /***
     * 轮盘抽奖的轮盘显示信息
     * @param activity
     * @return
     */
    private Map<String, Object> addLuckyWheel(Activity activity, Long userID) {
        Map<String, Object> map = new HashMap<>();
        map.put("isLuckyWheel", false);
        map.put("isLuckyWheelExist", false);
        map.put("activityId", 0);
        if (activity != null) {
//            String key = RedisKeyConstant.HOMEPAGE_TURNTABLE_SHOP + activity.getShopId() + ":" + activity.getId() + ":" + userID;
            //存储redis
//            this.stringRedisTemplate.opsForValue().setIfAbsent(key,"0");
//            String str = this.stringRedisTemplate.opsForValue().get(key);
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
            if (activityDef == null || activityDef.getStock() < 0 || activityDef.getDeposit().compareTo(new BigDecimal("1")) < 0) {
                log.warn("该轮盘活动定义不满足条件, activiyId:{}", activity.getId());
                return map;
            }
            map.put("activityId", activity.getId());
            map.put("isLuckyWheelExist", true);
            map.put("isLuckyWheel", true);
//            if (StringUtils.isNotBlank(str) && "0".equals(str)){
//                 this.stringRedisTemplate.opsForValue().increment(key,1);
//            }
        }
        return map;
    }


    /***
     * 查询订单广场某个商品下的所有活动
     * @param merchantID
     * @param shopID
     * @param goodsID
     * @return
     */
    @Override
    public SimpleResponse queryExemptionSquareIndexActivity(Long merchantID, Long shopID, Long goodsID, Long userID, int offset, int size) {
        SimpleResponse d = new SimpleResponse();
        if (shopID == null || shopID == 0 || merchantID == null || merchantID == 0 || goodsID == null || goodsID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        Merchant merchant = this.merchantService.getMerchant(merchantID);
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        Goods goods = this.goodsService.selectById(goodsID);
        if (merchantShop == null || merchant == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该店铺|商户不存在");
            return d;
        }
        //条件查询该商品下用户发起的待开奖的活动
        Map<String, Object> activityParams = this.selectActivityParams(merchantID, shopID, goodsID, null, 1,
                null, Arrays.asList(2, 4), null, true, "end_time", "asc");
        activityParams.put("size", size);
        activityParams.put("offset", offset);
        List<Activity> activitys = this.activityService.findPage(activityParams);
        int countActivity = this.activityService.countActivity(activityParams);
        if (activitys == null || activitys.size() == 0) {
            d.setData(this.returnInfoObject(offset, size, countActivity, new ArrayList<Object>()));
            return d;
        }
        List<Long> userIDs = new ArrayList<>();
        Map<Long, User> userMap = new HashMap<>();
        for (Activity activity : activitys) {
            if (userIDs.indexOf(activity.getUserId()) == -1) {
                userIDs.add(activity.getUserId());
            }
        }
        if (!CollectionUtils.isEmpty(userIDs)) {
            Map<String, Object> userParams = new HashMap<>();
            userParams.put("userIds", userIDs);
            List<User> users = this.userService.query(userParams);
            if (users != null && users.size() > 0) {
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }
        }
        List<Object> list = new ArrayList<Object>();
        for (Activity activity : activitys) {
            User user = userMap.get(activity.getUserId());
            if (user == null) {
                continue;
            }
            int activityStatus = this.activityService.getActivityStatus(activity.getStatus(), activity.getStartTime(), activity.getAwardStartTime(), activity.getEndTime());
            if (activityStatus != 2) {//等于2时，活动状态待开奖
                continue;
            }
            String avatarUrl = user.getAvatarUrl();
            String nickName = user.getNickName();
            //判断该用户是否机器人用户
            Map<String, Object> joinUserMap = this.userService.robotUserInfo(user.getId());
            if (joinUserMap != null && joinUserMap.size() > 0) {
                if (StringUtils.isNotBlank(joinUserMap.get("avatarUrl").toString())) {
                    avatarUrl = joinUserMap.get("avatarUrl").toString();
                }
                if (StringUtils.isNotBlank(joinUserMap.get("nickName").toString())) {
                    nickName = joinUserMap.get("nickName").toString();
                }
            }

            //活动参与人数
            List<String> redisJoinActivityUser = this.redisService.queryRedisJoinUser(activity.getId());
            list.add(this.addContent(activity.getHitsPerDraw(), activity.getCondPersionCount(),
                    goods.getPicUrl(), goods.getDisplayGoodsName(), goods.getDisplayPrice(), goods.getId(),
                    2, DateFormatUtils.format(activity.getStartTime(), "HH:mm"),
                    0, redisJoinActivityUser.size(), nickName, avatarUrl, activity.getType(), activity.getId(),
                    this.newManStart(activity, userID)));
        }
        d.setData(this.returnInfoObject(offset, size, countActivity, list));
        return d;
    }

    @Override
    public SimpleResponse checkIsClickLuckyWheel(Long merchantID, Long shopID, Long userID, Boolean isClickLuckyWheel) {
        Map<String, Object> luckyWheelParams = new HashMap<String, Object>();
        luckyWheelParams.put("merchantId", merchantID);
        luckyWheelParams.put("shopId", shopID);
        luckyWheelParams.put("type", ActivityTypeEnum.LUCK_WHEEL.getCode());
        luckyWheelParams.put("del", DelEnum.NO.getCode());
        luckyWheelParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        List<Activity> activities = this.activityService.findPage(luckyWheelParams);
        if (activities == null || activities.size() == 0) {
            log.warn("该店铺轮盘活动不存在，shopId:{}", shopID);
            return new SimpleResponse();
        }
        Activity activity = activities.get(0);

        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
        if (activityDef == null || activityDef.getStock() < 0 || activityDef.getDeposit().compareTo(new BigDecimal("1")) < 0) {
            log.warn("该轮盘活动定义不满足条件, activiyId:{}", activity.getId());
            return new SimpleResponse();
        }
        //存储redis
        this.stringRedisTemplate.opsForValue().setIfAbsent(
                RedisKeyConstant.HOMEPAGE_TURNTABLE_SHOP + activity.getShopId() + ":" + activity.getId() + ":" + userID, "0");

        return new SimpleResponse();
    }


    /****
     * 判断是否是新手发起本人
     * @param activity
     * @param userID
     * @return
     */
    private boolean newManStart(Activity activity, Long userID) {
        //判断该活动是否是新人有礼发起者
        boolean isYourself = false;
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                && activity.getUserId() != null
                && activity.getUserId().longValue() == userID.longValue()) {
            isYourself = true;
        }
        return isYourself;
    }

    /***
     * 获取用户发起的活动
     * @param merchantID
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    private Map<String, Object> queryUserOrderActivity(Long merchantID, Long shopID, Long userID, int offset, int size) {
        /**保证其下所有活动都是进行中的，这时需要判断，后期优化*/
        String key = mf.code.uactivity.repo.redis.RedisKeyConstant.GOODS_ID_RANKING_LIST + shopID;
        Map<String, Object> respMap = new HashMap<>();
        int start = offset;
        int end = offset + size - 1;
        //获取该set的redis的总数
        Long zCard = this.stringRedisTemplate.opsForZSet().zCard(key);
        //reverseRange 按照score降序 ,range 按照score升序
        Set<String> goodsIDsStr = this.stringRedisTemplate.opsForZSet().range(key, start, end);
        if (goodsIDsStr == null || goodsIDsStr.size() == 0) {
            return this.returnInfoObject(offset, size, zCard.intValue(), new ArrayList<Object>());
        }
        List<Long> goodsIDs = new ArrayList<Long>();
        for (String goodsID : goodsIDsStr) {
            goodsIDs.add(NumberUtils.toLong(goodsID));
        }
        //查询商品信息
        Map<Long, Goods> goodsMap = this.queryGoods(goodsIDs);

        List<Object> list = new ArrayList<Object>();
        for (Long goodsID : goodsIDs) {
            Goods goods = goodsMap.get(goodsID);
            if (goods == null) {
                continue;
            }
            //根据goodsID查询活动 只查待开奖的(已发布) 1已发布; 2:免单商品抽奖发起者 4:新手有礼发起者
            List<Activity> activities = this.activityService.findPage(
                    this.selectActivityParams(merchantID, shopID, goodsID, null, 1,
                            null, Arrays.asList(2, 4), false, true, "end_time", "asc"));
            if (activities == null || activities.size() == 0) {
                this.stringRedisTemplate.opsForZSet().remove(key + shopID, goods.getId().toString());
                log.error("超级警戒~~~~商品编号为{}的活动不存在", goodsID);
                zCard--;
                continue;
            }
            Activity activity = activities.get(0);
            int activityStatus = this.activityService.getActivityStatus(activity.getStatus(), activity.getStartTime(), activity.getAwardStartTime(), activity.getEndTime());
            if (activityStatus != 2) {//等于2时，活动状态待开奖
                continue;
            }
            User user = this.userService.selectByPrimaryKey(activity.getUserId());
            String avatarUrl = user.getAvatarUrl();
            String nickName = user.getNickName();
            //判断该用户是否机器人用户
            Map<String, Object> joinUserMap = this.userService.robotUserInfo(user.getId());
            if (joinUserMap != null && joinUserMap.size() > 0) {
                // adua
                if (StringUtils.isNotBlank(joinUserMap.get("avatarUrl").toString())) {
                    avatarUrl = joinUserMap.get("avatarUrl").toString();
                }
                if (StringUtils.isNotBlank(joinUserMap.get("nickName").toString())) {
                    nickName = joinUserMap.get("nickName").toString();
                }
            }
            if (user == null) {
                log.error("超级警戒~~~~用户不存在");
                continue;
            }
            List<String> redisJoinActivityUser = this.redisService.queryRedisJoinUser(activity.getId());
            list.add(this.addContent(activity.getHitsPerDraw(), activity.getCondPersionCount(),
                    goods.getPicUrl(), goods.getDisplayGoodsName(), goods.getDisplayPrice(), goods.getId(),
                    2, DateFormatUtils.format(activity.getStartTime(), "HH:mm"),
                    activities.size(), redisJoinActivityUser.size(), nickName, avatarUrl, activity.getType(), activity.getId(),
                    this.newManStart(activity, userID)));
        }
        respMap = this.returnInfoObject(offset, size, zCard.intValue(), list);
        //用户发起,当offset为0时，保存redis
        if (offset == 0) {
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.HOMEPAGE_ACTIVITY_ORDER + shopID, JSON.toJSONString(respMap), 10, TimeUnit.SECONDS);
        }
        return respMap;
    }

    private Map<String, Object> queryMerchantActivity(Long merchantID, Long shopID) {
        Map<String, Object> respMap = new HashMap<String, Object>();

        List<Object> respList = new ArrayList<Object>();
        //查询活动信息 1已发布 -1抽奖未成功 2已结束; 1:计划类抽奖
        List<Activity> activityList = this.activityService.findPage(
                this.selectActivityParams(merchantID, shopID, null, Arrays.asList(1, -1, 2), null,
                        1, null, true, false, "start_time", "asc"));
        if (activityList == null || activityList.size() == 0) {//为空时的非正常处理展现
            respMap.put("index", 0);
            respMap.put("activities", new ArrayList<>());
            return respMap;
        }

        //查询goods货物信息
        List<Long> goodsIDs = new ArrayList<Long>();
        for (Activity activity : activityList) {
            if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                goodsIDs.add(activity.getGoodsId());
            }
        }
        Map<Long, Goods> goodsMap = this.queryGoods(goodsIDs);
        int index = -1;
        for (Activity activity : activityList) {
            index++;
            //待开始
            Boolean willStart = activity.getStatus() == 1 && activity.getStartTime().getTime() > System.currentTimeMillis();
            //待开奖
            Boolean willAward = activity.getStatus() == 1 && activity.getAwardStartTime() == null &&
                    activity.getEndTime().getTime() > System.currentTimeMillis() && System.currentTimeMillis() > activity.getStartTime().getTime();
            //活动开奖未结束，并且下个活动还没到
            Boolean awardedButNoEndAndNextTimeNot = false;
            //已开奖未结束
            Boolean awardedButNoEnd = activity.getStatus() == 1 && System.currentTimeMillis() > activity.getEndTime().getTime() && activity.getAwardStartTime() != null;
            if (awardedButNoEnd) {
                //若当下活动开奖未结束，并且下个活动时间未到，则显示当下开奖未结束的活动
                Activity activityNext = activityList.get(index);
                if (activityNext.getStartTime().getTime() > System.currentTimeMillis()) {
                    awardedButNoEndAndNextTimeNot = true;
                }
            }
            if (willStart || willAward || awardedButNoEndAndNextTimeNot) {
                break;
            }
        }
        for (Activity activity : activityList) {
            Goods goods = goodsMap.get(activity.getGoodsId());
            if (goods == null) {
                continue;
            }
            int status = this.activityService.getActivityStatus(activity.getStatus(), activity.getStartTime(), activity.getAwardStartTime(), activity.getEndTime());
            List<String> redisJoinActivityUser = this.redisService.queryRedisJoinUser(activity.getId());
            respList.add(this.addContent(activity.getHitsPerDraw(), activity.getCondPersionCount(),
                    goods.getDisplayBanner(), goods.getDisplayGoodsName(), goods.getDisplayPrice(), goods.getId(),
                    status, DateFormatUtils.format(activity.getStartTime(), "HH:mm"), null,
                    redisJoinActivityUser.size(), null, null, activity.getType(), activity.getId(),
                    false));
        }

        respMap.put("index", index);
        respMap.put("activities", respList);
        //对当前信息存储redis
        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.HOMEPAGE_ACTIVITY_MERCHANT + shopID, JSON.toJSONString(respMap), 10, TimeUnit.SECONDS);
        return respMap;
    }

    /***
     * 查询商户店铺信息
     * @param merchantShop
     * @return
     */
    private Map<String, Object> queryMerchantShop(MerchantShop merchantShop) {
        Map<String, Object> respMap = this.merchantShopService.getCustomServiceAbout(merchantShop);
        respMap.put("picPath", StringUtils.isNotBlank(merchantShop.getPicPath()) ? this.ossCaller.cdnReplace(merchantShop.getPicPath()) : "");
        respMap.put("shopName", merchantShop.getShopName());
        respMap.put("shopId", merchantShop.getId());
        respMap.put("merchantId", merchantShop.getMerchantId());
        //对店铺信息进行存储redis
        if (StringUtils.isNotBlank(merchantShop.getPicPath())) {
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.HOMEPAGE_ACTIVITY_SHOP + merchantShop.getId(), JSON.toJSONString(respMap), 1, TimeUnit.DAYS);
        }
        return respMap;
    }

    /***
     * 查询商户商品信息
     * @param goodsIDs
     * @return
     */
    private Map<Long, Goods> queryGoods(List<Long> goodsIDs) {
        Map<Long, Goods> respMap = new HashMap<Long, Goods>();
        if (goodsIDs != null && goodsIDs.size() > 0) {
            Map<String, Object> goodsParams = new HashMap<String, Object>();
            goodsParams.put("goodsIds", goodsIDs);
            List<Goods> goods = this.goodsService.pageList(goodsParams);
            if (goods != null && goods.size() > 0) {
                for (Goods good : goods) {
                    respMap.put(good.getId(), good);
                }
            }
        }
        return respMap;
    }

    /***
     * 返回信息结果赋值
     * @param offset
     * @param size
     * @param total
     * @param list
     * @return
     */
    private Map<String, Object> returnInfoObject(int offset, int size, int total, List<Object> list) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("offset", offset);
        respMap.put("limit", size);
        respMap.put("total", total);
        respMap.put("content", list);
        return respMap;
    }

    /***
     * 对返回结果的content对象进行赋值
     * @param hitsPerDraw 每个计划内活动中奖人数
     * @param condPersionCount 满足人数开奖
     * @param displayBanner 商品图片
     * @param displayGoodsName 商品标题
     * @param displayPrice 商品价格
     * @param status 状态-1:已结束;1:待开始;2:待开奖;3:已开奖
     * @param startTime 开始时间
     * @param activityNum 该商品下有多少活动
     * @param joinActivityNum 参与活动人数
     * @param nickName 用户发起活动的用户名
     * @param avatarUrl 用户发起活动的头像
     * @return
     */
    private Map<String, Object> addContent(Integer hitsPerDraw,
                                           Integer condPersionCount,
                                           String displayBanner,
                                           String displayGoodsName,
                                           BigDecimal displayPrice,
                                           Long goodsID,
                                           Integer status,
                                           String startTime,
                                           Integer activityNum,
                                           Integer joinActivityNum,
                                           String nickName,
                                           String avatarUrl,
                                           Integer type,
                                           Long activityID,
                                           boolean isYourself) {
        Map<String, Object> map = new HashMap<>();
        map.put("hitsPerDraw", hitsPerDraw);
        map.put("condPersionCount", condPersionCount);
        map.put("picUrl", displayBanner);
        map.put("title", displayGoodsName);
        map.put("price", displayPrice);
        map.put("goodsId", goodsID);
        map.put("status", status);
        map.put("startTime", startTime);
        map.put("activityNum", activityNum);
        map.put("joinActivityNum", joinActivityNum);
        map.put("nickName", nickName);
        map.put("avatarUrl", avatarUrl);
        map.put("type", type);
        map.put("activityId", activityID);
        map.put("isYourself", isYourself);
        return map;
    }

    /***
     * 对商户活动的查询的条件的封装
     * @param merchantID 商户id
     * @param shopID 店铺id
     * @param statusList 状态
     * @param type 活动类型 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者
     * @param selectToday 是否查询今天
     * @param order 排序字段
     * @param direct asc|desc
     * @return
     */
    private Map<String, Object> selectActivityParams(Long merchantID,
                                                     Long shopID,
                                                     Long goodsID,
                                                     List<Integer> statusList,
                                                     Integer status,
                                                     Integer type,
                                                     List<Integer> types,
                                                     Boolean selectToday,
                                                     Boolean willAward,
                                                     String order,
                                                     String direct) {
        Map<String, Object> activityParams = new HashMap<String, Object>();
        activityParams.put("merchantId", merchantID);
        activityParams.put("shopId", shopID);
        activityParams.put("goodsId", goodsID);
        activityParams.put("type", type);
        activityParams.put("types", types);
        activityParams.put("activityStatuses", statusList);
        activityParams.put("status", status);
        activityParams.put("order", order);
        activityParams.put("direct", direct);
        if (willAward != null && willAward) {//是否只查待开奖
            activityParams.put("willAward", willAward);
        }
        if (selectToday != null && selectToday) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date zero = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date finalTime = calendar.getTime();
            activityParams.put("limitTime", DateFormatUtils.format(zero, "yyyy-MM-dd HH:mm:ss"));//需要查询当天
            activityParams.put("finalTime", DateFormatUtils.format(finalTime, "yyyy-MM-dd HH:mm:ss"));//需要查询当天尾端时间
        }
        return activityParams;
    }


    /**
     * 根据sku id 查询活动列表
     *
     * @param merchantId
     * @param shopId
     * @param skuId
     * @param userId
     * @param lastId
     * @return
     */
    @Override
    public SimpleResponse getActivityArrBySku(Long merchantId, Long shopId, Long skuId, Long userId, Long lastId, int pageSize) {
        // todo 1. 统计剩余sku数量  2. 联表查询开奖人数/红包数量/优惠券数量/奖品数量  3. 活动已进行时间   4 我的参与状态

        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        Map param = new HashMap(4);
        // todo yy sku 尚未确定
        param.put("merchantId", merchantId);
        param.put("shopId", shopId);
        param.put("userId", userId);
        param.put("skuId", skuId);
        List<Activity> activityArr = activityService.getActivityArrBySku(param);

        Map responseMap = new HashMap();
        responseMap.put("data", ListTools.remove4Paging(activityArr, pageSize));
        responseMap.put("hasNextPage", activityArr.size() > 10 ? true : false);
        response.setData(responseMap);

        return response;
    }

/**
 * ####################################################################### gbf begin#######################################################################
 */
    /**
     * 获取弹出类型
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param newbie
     * @return
     */
    @Override
    public Map getMissionType(Long merchantId, Long shopId, Long userId, boolean newbie) {
        Map map = null;
        List<TaskSpaceTypeEnum> missionTypeList = new ArrayList<>();
        Integer type = null;

        for (int i = 0; i < TaskSpaceTypeEnum.values().length + 2; i++) {
            map = new HashMap();
            //获取任务类型
            type = getRanIntFromPopoutEnum(type, missionTypeList, merchantId, shopId, userId, newbie);
            if (type == -1) {
                map.put("missionType", type);
                return map;
            }
            //附加信息 , 如果该任务 不合法  就返回null
            Map resultInfo = this.addMissionExtraInfo(type, map, merchantId, shopId, userId);
            if (resultInfo != null) {
                break;
            }
        }
        map.put("missionType", type);
        // todo gbf 优化精确的过期时间
        String nowStr = DateUtil.getYYMMDD();
        String redisKey = RedisKeys.POP_OUT_MISSION_TYPE + userId + ":" + nowStr;
        stringRedisTemplate.opsForValue().set(redisKey, type.toString(), 2, TimeUnit.DAYS);
        return map;
    }

    /**
     * 给前端做任务需要的附加信息
     *
     * @param type
     * @param respDataMap
     * @return
     */
    private Map addMissionExtraInfo(Integer type, Map respDataMap, Long merchantId, Long shopId, Long userId) {
        if (type == null) {
            return null;
        }
        //发起抽奖
        if (type == TaskSpaceTypeEnum.LAUNCH_LUCKY.getType()) {
            List<ActivityDef> activityDefList = activityDefService.selectByMerchantIdShopId(merchantId, shopId);
            if (activityDefList == null || activityDefList.size() == 0) {
                return null;
            }
            return respDataMap;

        } else if (type == TaskSpaceTypeEnum.JOIN_LUCKY.getType()) { // 参与抽奖
            //参与抽奖
            return this.getInfoFromActivity(merchantId, shopId, respDataMap);

        } else if (type == TaskSpaceTypeEnum.RECEIVE_GIFT.getType()) {
            // 如果是新人有礼物 , 也就是助力活动 , 也就是赠品活动
            //前端只需要defId
            List<ActivityDef> activityDefList = activityDefService.selectByMerchantIdShopId4Newbie(merchantId, shopId);
            if (activityDefList == null || activityDefList.size() == 0) {
                return null;
            }
            return respDataMap;

        } else if (type == TaskSpaceTypeEnum.GOOD_COMMENT.getType()
                || type == TaskSpaceTypeEnum.FAV_CART.getType()
                || type == TaskSpaceTypeEnum.ORDER_BACK.getType()) {
            //
            return this.selectByMerchantIdShopIdFromActivityTask(merchantId, shopId, respDataMap);

        } else if (type == TaskSpaceTypeEnum.LUCK_WHEEL.getType()) {
            //如果是大转盘 , 需要一个对象
            Map<String, Object> luckyWheelParams = new HashMap<String, Object>();
            luckyWheelParams.put("merchantId", merchantId);
            luckyWheelParams.put("shopId", shopId);
            luckyWheelParams.put("type", ActivityTypeEnum.LUCK_WHEEL.getCode());
            luckyWheelParams.put("del", DelEnum.NO.getCode());
            luckyWheelParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
            luckyWheelParams.put("order", "ctime");
            luckyWheelParams.put("direct", "desc");
            List<Activity> activities = this.activityService.findPage(luckyWheelParams);
            if (activities != null && activities.size() > 0) {
                respDataMap.put("luckyWheel", this.addLuckyWheelV2(activities.get(0), merchantId, shopId, userId));
            } else {
                return null;
            }
        }
        return respDataMap;
    }


    /**
     * from activity_task
     * where  shop_id=#{shopId}
     * and rp_stock>0
     * and `status`=3
     * and del=0
     *
     * @param merchantId
     * @param shopId
     * @param respDataMap
     * @return
     */
    private Map selectByMerchantIdShopIdFromActivityTask(Long merchantId, Long shopId, Map respDataMap) {
        List<ActivityTask> activityList = activityTaskService.selectByMerchantIdShopIdFromActivityTask(merchantId, shopId);
        if (activityList == null || activityList.size() == 0) {
            return null;
        }

        //任务空间只返回任务类型
//        int rundomNum = new Random().nextInt(activityList.size());
//        ActivityTask activityTask = activityList.get(rundomNum);
//        respDataMap.put("defId", activityTask.getActivityDefId());
//        respDataMap.put("goodsId", activityTask.getGoodsId());
//        respDataMap.put("activityId", activityTask.getId());

        return respDataMap;
    }

    /**
     * 查询acitivity 中type=1 的
     *
     * @param merchantId
     * @param shopId
     * @param respDataMap
     * @return
     */
    private Map getInfoFromActivity(Long merchantId, Long shopId, Map respDataMap) {
        List<Activity> activityList = activityService.selectByMerchantIdShopId(merchantId, shopId);
        if (activityList == null || activityList.size() == 0) {
            return null;
        }
        //任务空间只返回任务类型
//        int rundomNum = new Random().nextInt(activityList.size());
//        Activity activity = activityService.findById(activityList.get(rundomNum).getId());
//        respDataMap.put("defId", activity.getActivityDefId());
//        respDataMap.put("goodsId", activity.getGoodsId());
//        respDataMap.put("activityId", activity.getId());
        return respDataMap;
    }

    private Map setLuckWheelInfo(Map m, Long merchantId, Long shopId, Long userId) {
        //如果是大转盘 , 需要一个对象
        Map<String, Object> luckyWheelParams = new HashMap<String, Object>();
        luckyWheelParams.put("merchantId", merchantId);
        luckyWheelParams.put("shopId", shopId);
        luckyWheelParams.put("type", ActivityTypeEnum.LUCK_WHEEL.getCode());
        luckyWheelParams.put("del", DelEnum.NO.getCode());
        luckyWheelParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        luckyWheelParams.put("order", "ctime");
        luckyWheelParams.put("direct", "desc");
        List<Activity> activities = this.activityService.findPage(luckyWheelParams);
        if (activities != null && activities.size() > 0) {
            m.put("luckyWheel", this.addLuckyWheelV2(activities.get(0), merchantId, shopId, userId));
        }
        return m;
    }

    private Map addInfo(Map m, Integer type, Long shopId) {
        if (type == 3) {
            ActivityDef activityDef = activityDefService.selectOpenRPDefByShopId(shopId);
            if (activityDef != null) {
                Activity activity = activityService.findByDefId4Summary(activityDef.getId());
                m.put("price", activityDef.getGoodsPrice());
                m.put("defId", activityDef.getId());
                if (activity != null) {
                    m.put("activityId", activity.getId());
                }
            }
        }
        return m;
    }

    /**
     * 随机任务
     *
     * @param shopId
     * @param userId
     */
    @Override
    public Map getMissionTypeByYYSuport(Long shopId, Long userId, Long merchantId) {
        Map m = new HashMap();

        String redisKey = RedisKeys.POP_OUT_MISSION_TYPE + merchantId + ":" + shopId + ":" + userId;
        String redisVal = stringRedisTemplate.opsForValue().get(redisKey);
        if (redisVal != null) {
            Integer type = Integer.parseInt(redisVal);
            if (type == TaskSpaceTypeEnum.LUCK_WHEEL.getType()) {
                this.setLuckWheelInfo(m, merchantId, shopId, userId);
            }
            m.put("missionType", redisVal);
            m = addInfo(m, type, shopId);
            return m;
        }

        SimpleResponse response = appletTaskSpaceService.queryTaskSpaceList(shopId, userId);
        Object data = response.getData();
        List<TaskSpaceNodeDto> list = new ArrayList<>();
        if (data != null) {
            list = (List<TaskSpaceNodeDto>) ((Map<String, Object>) data).get("list");
            if (list.size() > 0) {
                TaskSpaceNodeDto dto = new TaskSpaceNodeDto();
                if (list.size() > 1) {
                    int rundomNum = new Random().nextInt(list.size());
                    dto = list.get(rundomNum);
                } else {
                    dto = list.get(0);
                }

                Integer missionType = dto.getType();

                // 如果今日任务是拆红包活动，则不展示弹窗
                if (missionType == 3) {
                    m.put("missionType", -1);
                    m.put("popout", false);
                    return m;
                }

                //判断是否新人
                String rediskey = "home:newbie:shopId:" + shopId + ":userId:" + userId;
                String redisval = stringRedisTemplate.opsForValue().get(rediskey);
                if (redisval != null) {
                    for (TaskSpaceNodeDto d : list) {
                        if (d.getType() == TaskSpaceTypeEnum.LUCK_WHEEL.getType()) {
                            missionType = dto.getType();
                        }
                    }
                }

                if (missionType == TaskSpaceTypeEnum.LUCK_WHEEL.getType()) {
                    this.setLuckWheelInfo(m, merchantId, shopId, userId);
                }
                if (dto.getType() == 3) {
                    m = addInfo(m, dto.getType(), shopId);
                }
                m.put("missionType", dto.getType());
                stringRedisTemplate.opsForValue().set(redisKey, dto.getType().toString());

                long timeout = DateUtil.getTimeoutSecond();
                stringRedisTemplate.expire(redisKey, timeout, TimeUnit.SECONDS);
                return m;
            }
        }
        m.put("missionType", -1);
        m.put("popout", false);
        return m;
    }


    /**
     * 重载
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public Map getMissionType(Long merchantId, Long shopId, Long userId) {
        return getMissionType(merchantId, shopId, userId, false);
    }

    /**
     * 判断是否与大转盘活动
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    private boolean getLuckyWheel(Long merchantId, Long shopId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("merchantId", merchantId);
        param.put("shopId", shopId);
        param.put("type", ActivityTypeEnum.LUCK_WHEEL.getCode());
        param.put("del", DelEnum.NO.getCode());
        param.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        param.put("order", "ctime");
        param.put("direct", "desc");
        List<Activity> activities = this.activityService.findPage(param);

        if (activities != null && activities.size() > 0) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activities.get(0).getActivityDefId());
            if (activityDef != null && activityDef.getStock() > 0 || activityDef.getDeposit().compareTo(new BigDecimal("1")) > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 递归:获取弹出类型
     * 如果返回值是-1 ,说明没用任何任务可以做
     *
     * @param userId
     * @return
     */
    private Integer getRanIntFromPopoutEnum(Integer typeOld, List<TaskSpaceTypeEnum> missionTypeList, Long merchantId, Long shopId, Long userId, boolean newbie) {

        String nowStr = DateUtil.getYYMMDD();
        Date yesterdayDate = DateUtil.addDay(new Date(), -1);
        String yesterday = DateUtil.dateToString(yesterdayDate, DateUtil.FORMAT_FOUR);
        String redisKey = RedisKeys.POP_OUT_MISSION_TYPE + userId + ":" + nowStr;
        // 如果是新人, 就直接返回轮盘
        if (newbie) {
            if (this.getLuckyWheel(merchantId, shopId)) {//如果 有大转盘活动
                Integer type = TaskSpaceTypeEnum.LUCK_WHEEL.getType();
                stringRedisTemplate.opsForValue().set(redisKey, type.toString(), 2, TimeUnit.DAYS);
                return type;
            }
        }
        String redisKeyYesterday = RedisKeys.POP_OUT_MISSION_TYPE + userId + ":" + yesterday;

        // 获取昨天的弹出类型
        String redisValYesterday = stringRedisTemplate.opsForValue().get(redisKeyYesterday);

        // 先获取redis中的数据
        String redisValToday = stringRedisTemplate.opsForValue().get(redisKey);

        if (redisValToday != null) {
            return Integer.parseInt(redisValToday);
        }

        if (typeOld == null) {
            // todo gbf 可redis优化
            User user = userService.selectByPrimaryKey(userId);
            String mobile = user.getMobile();
            TaskSpaceTypeEnum[] values = TaskSpaceTypeEnum.values();
            for (TaskSpaceTypeEnum e : values) {
                // 剔除手机任务
                if (mobile != null && e.getType() == TaskSpaceTypeEnum.BIND_PHONE.getType()) {
                    continue;
                }
                //剔除昨日的类型
                if (redisValYesterday != null && e.getType() == Integer.parseInt(redisValYesterday)) {
                    if (e.getType() == Integer.parseInt(redisValYesterday)) {
                        continue;
                    }
                }
                // 剔除试用
                if (e.getType() == TaskSpaceTypeEnum.JOIN_TRIAL.getType()) {
                    continue;
                }

                missionTypeList.add(e);
            }
        } else {
            // 这个任务类型需要被剔除
            for (int j = 0; j < missionTypeList.size(); j++) {
                TaskSpaceTypeEnum e = missionTypeList.get(j);
                if (e.getType() == typeOld) {
                    missionTypeList.remove(j);
                    break;
                }
            }
        }
        //1 生成的今日任务类型 , 2 设置redis

        int randomNum;
        if (missionTypeList.size() == 0) {
            return -1;
        } else {
            randomNum = new Random().nextInt(missionTypeList.size());
        }
        typeOld = missionTypeList.get(randomNum).getType();
        return typeOld;
    }


    /**
     * 今日任务-大转盘
     *
     * @param activity
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    private Map<String, Object> addLuckyWheelV2(Activity activity, Long merchantId, Long shopId, Long userId) {
        Map<String, Object> map = new HashMap<>();
        if (activity != null) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
            Long aLong = userCouponService.cumWheelMoney(merchantId, shopId, userId);
            if (activityDef == null || activityDef.getStock() < 0 || activityDef.getDeposit().compareTo(new BigDecimal("1")) < 0) {
                log.warn("该轮盘活动定义不满足条件, activiyId:{}", activity.getId());
                return map;
            }
            if (aLong == null) {
                map.put("amount", 0);
            } else {
                map.put("amount", aLong);
            }
            map.put("isLuckyWheelExist", true);
            map.put("isLuckyWheel", true);
            map.put("activityId", activity.getId());
        }
        return map;
    }

    @Override
    public Map statisticsGoodsV2(Long merchantId, Long shopId) {
        // 获取两个值 : goodsTotal 和 goodsStock
        Map map = activityDefService.statisticsGoods(merchantId, shopId);
        return map;
    }

    @Override
    public Integer buildOrGetData4Item(Integer hundred, Long merchantId, Long shopId, Long goodsId) {
        String key = RedisCommonKeys.FAKE_DATA_LUCHY_DRAW_LIST_ITEM + merchantId + ":" + shopId;
        Integer fakeData;
        Object o = stringRedisTemplate.opsForHash().get(key, goodsId.toString());
        if (o == null) {
            fakeData = getRandom(hundred);
            stringRedisTemplate.opsForHash().put(key, goodsId.toString(), fakeData.toString());
        } else {
            fakeData = Integer.parseInt(o.toString());
        }
        return fakeData;
    }


    private Integer getRandom(Integer hundred) {
        Random r = new Random();
        hundred = r.nextInt(hundred) + 1;

        Integer ten = r.nextInt(99);
        return (Integer.parseInt(hundred.toString()) * 100) + ten;
    }

    @Override
    public Integer buildOrGetData4Statistics(Long merchantId, Long shopId) {
        String key = RedisCommonKeys.FAKE_DATA_LUCHY_DRAW_LIST_ITEM + merchantId + ":" + shopId;
        List<Object> values = stringRedisTemplate.opsForHash().values(key);
        Integer result = 0;
        for (Object o : values) {
            result = result + Integer.parseInt(o.toString());
        }
        return result;
    }
}
