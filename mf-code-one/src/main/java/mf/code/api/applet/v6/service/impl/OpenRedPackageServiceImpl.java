package mf.code.api.applet.v6.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.aggregateroot.*;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.domain.applet.valueobject.ActivityPopupStatusEnum;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.v6.service.NewbieOpenRedPackageService;
import mf.code.api.applet.v6.service.OpenRedPackageService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v6.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-14 09:52
 */
@Slf4j
@Service
public class OpenRedPackageServiceImpl implements OpenRedPackageService {
	@Autowired
	private AppletActivityDefRepository appletActivityDefRepository;
	@Autowired
	private AppletActivityRepository appletActivityRepository;
	@Autowired
	private JoinRuleActivityRepository joinRuleActivityRepository;
	@Autowired
	private AppletUserRepository appletUserRepository;
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private NewbieOpenRedPackageService newbieOpenRedPackageService;
	@Autowired
	private UserService userService;

	/**
	 * 用户进入首页或详情页 获取 拆红包弹框类型
	 *
	 * @param shopId
	 * @param userId
	 * @return <per>
	 * 0. 什么都不弹
	 * 1. 弹出 帮好友拆了1.2元，快去找好友帮拆你的红包吧 -- 活动详情页
	 * 2. 弹出 拆红包窗口 -- 活动列表
	 * 3. 弹出 红包已被抢光了，下次早点来！ -- 活动详情页
	 * 4. 弹出 任务失败，再来挑战一次 -- 活动详情页
	 * 5. 弹出 你已有一个红包啦，不要贪心哦 -- 活动详情页
	 * 6. 弹出 恭喜您！已拆得所有现金红包 -- 活动详情页
	 * 7. 弹出 恭喜您！好友帮你拆出了红包 -- 活动详情页
	 * 8. 弹出 分享给你一个5元红包，立即来拆开吧 -- 活动详情页
	 * 9. 弹出 完成下单即可提现现金 -- 提现
	 * 10.弹出 完成支付即可提现现金 -- 提现
	 * 11.弹出 您的钱包中有5元可提现 还有10元待解锁 -- 提现
	 * 12.弹出 你有10元红包待拆开 还差2个好友就能拆开啦 -- 首页和商品详情页
	 * 13.弹出 你有10元红包待提现 -- 首页和商品详情页
	 * </per>
	 */
	@Override
	public SimpleResponse openRedPackagePopup(Long shopId, Long userId, Integer scenes) {
		ScenesEnum scenesEnum = ScenesEnum.findById(scenes);

		// 2.3迭代 新增 新手任务-拆红包逻辑
		User user = userService.getById(userId);
		if (user.getVipShopId().equals(shopId)) {
			boolean completeNewbieOpenRedPackage = newbieOpenRedPackageService.isCompleteNewbieOpenRedPackage(userId);
			ActivityDef newbieActivityDef = newbieOpenRedPackageService.hasExistNewbieTask(user.getVipShopId());
			if (!completeNewbieOpenRedPackage && newbieActivityDef != null) {
				AppletActivityDef appletActivityDef = appletActivityDefRepository.findByShopIdType(user.getVipShopId(), ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
				// 没有进行中活动且可发起新活动
				if (appletActivityDef.canCreate()) {
					// 增装用户 最新一条活动记录
					Activity newbieActivity = newbieOpenRedPackageService.hasPublishedActivityByUserId(userId);
					if (newbieActivity != null) {
						// 装配活动
						AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(newbieActivity, userId, scenesEnum);
						appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);

						if (openRedPackageActivity.isExpired()) {
							// 活动过期
							// 补全 用户参与事件
							if (openRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
								appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
								openRedPackageActivity.setExpired();
								appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
							}
						}

						Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
						return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
					}
					// 可以参与 -- 说明，可以发起新活动
					ActivityDef activityDef = appletActivityDef.getActivityDefInfo();
					BigDecimal commission = activityDef.getCommission();
					BigDecimal goodsPrice = activityDef.getGoodsPrice();
					BigDecimal activityAmount = commission.compareTo(BigDecimal.ZERO) == 0 ? goodsPrice : commission;

					Map<String, Object> resultVO = new HashMap<>();
					resultVO.put("activityDefId", appletActivityDef.getId());
					resultVO.put("activityStatus", ActivityPopupStatusEnum.OPEN.getCode());

					Activity lastActivity = newbieOpenRedPackageService.lastNewbieOpenRedPackage(userId);
					if (lastActivity != null && lastActivity.getStatus() == ActivityStatusEnum.FAILURE.getCode() && scenes.equals(ScenesEnum.ACTIVITY_PAGE.getCode())) {
						resultVO.put("activityStatus", ActivityPopupStatusEnum.FAILED.getCode());
					}
					resultVO.put("money", activityAmount);
					return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
				}
				log.info("库存不足");
				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}

			Activity newbieActivity = newbieOpenRedPackageService.hasPublishedActivityByUserId(userId);
			if (newbieActivity != null) {
				// 装配活动
				AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(newbieActivity, userId, scenesEnum);
				appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);

				if (openRedPackageActivity.isExpired()) {
					// 活动过期
					// 补全 用户参与事件
					if (openRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
						appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
						openRedPackageActivity.setExpired();
						appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
					}
				}

				Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
		}

		// 获取店铺的 拆红包活动定义 -- 正常拆红包
		AppletActivityDef appletActivityDef = appletActivityDefRepository.findByShopIdType(shopId, ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
		if (appletActivityDef == null) {
			log.info("没有对应类型的活动定义在发布中");
			Map<String, Object> resultVO = new HashMap<>();
			resultVO.put("activityStatus", ActivityPopupStatusEnum.NONE.getCode());
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		// 按活动参与规则，获取用户进行中或已完成活动
		JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRule(appletActivityDef.getJoinRule());
		// 判断用户是否能参与活动定义
		if (joinRuleActivity.canJoinByUserId(userId)) {
			// 没有进行中活动且可发起新活动
			if (appletActivityDef.canCreate()) {
				// 增装用户 最新一条活动记录
				joinRuleActivityRepository.addLastUserActivity(joinRuleActivity, userId);

				// 可以参与 -- 说明，可以发起新活动
				ActivityDef activityDef = appletActivityDef.getActivityDefInfo();
				BigDecimal commission = activityDef.getCommission();
				BigDecimal goodsPrice = activityDef.getGoodsPrice();
				BigDecimal activityAmount = commission.compareTo(BigDecimal.ZERO) == 0 ? goodsPrice : commission;

				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityDefId", appletActivityDef.getId());
				resultVO.put("activityStatus", ActivityPopupStatusEnum.OPEN.getCode());

				if (joinRuleActivity.isExpiredLastUserActivity(userId) && scenes.equals(ScenesEnum.ACTIVITY_PAGE.getCode())) {
					resultVO.put("activityStatus", ActivityPopupStatusEnum.FAILED.getCode());
				}
				resultVO.put("money", activityAmount);
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
			log.info("库存不足");
			Map<String, Object> resultVO = new HashMap<>();
			resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		// 说明 已有进行中活动或不可再发起新活动了 处理返回参数 -- 各种情况判断

		// 获取用户进行中的活动列表
		List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
		Activity activity;
		if (CollectionUtils.isEmpty(activityList)) {
			// 无进行中活动，获取用户已完成的活动
			Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
			if (completedActivityId == null) {
				log.error("openRedPackagePopup逻辑异常，须排查");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "openRedPackagePopup逻辑异常，须排查");
			}
			AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
			Activity activityInfo = completedActivity.getActivityInfo();
			if (activityInfo == null) {
				log.error("openRedPackagePopup 查无此活动，activityId = {}", completedActivityId);
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
			}
			activity = activityInfo;
		} else {
			activity = activityList.get(0);
		}
		// 装配活动
		AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, userId, scenesEnum);
		appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);

		if (openRedPackageActivity.isExpired()) {
			// 活动过期
			// 补全 用户参与事件
			if (openRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
				appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
				openRedPackageActivity.setExpired();
				appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
			}
		}

		Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
		return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
	}

	/**
	 * 2.您有一个红包待拆开； 14.恭喜成功提现10元，再送你10元红包 -转2状态
	 * 创建拆红包
	 *
	 * @param activityDefId
	 * @param userId
	 * @return
	 */
	@Override
	public SimpleResponse createOpenRedPackage(Long activityDefId, Long userId) {
		// 2.3迭代 新增 新手任务-拆红包逻辑

		User user = userService.getById(userId);
		if (user.getVipShopId().equals(user.getShopId())) {
			Activity newbieActivity = newbieOpenRedPackageService.hasPublishedActivityByUserId(userId);
			if (newbieActivity != null) {
				// 装配活动
				AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(newbieActivity, userId, ScenesEnum.ACTIVITY_PAGE);
				appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);

				Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
		}


		// 正常拆红包逻辑
		AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(activityDefId);
		if (null == appletActivityDef) {
			log.error("查无此活动定义，activityDefId = {}", activityDefId);
			return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此活动定义");
		}
		// 获取符合参与规则的进行中活动及已完成记录
		JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRule(appletActivityDef.getJoinRule());
		// 用户是否可以发起活动
		if (!joinRuleActivity.canJoinByUserId(userId)) {
			// 说明 已有进行中活动或不可再发起新活动了 处理返回参数 -- 各种情况判断

			// 获取用户进行中的活动列表
			List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(userId);
			Activity activity;
			if (CollectionUtils.isEmpty(activityList)) {
				// 无进行中活动，获取用户已完成的活动
				Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
				if (completedActivityId == null) {
					log.error("openRedPackagePopup逻辑异常，须排查");
					return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "openRedPackagePopup逻辑异常，须排查");
				}
				AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
				Activity activityInfo = completedActivity.getActivityInfo();
				if (activityInfo == null) {
					log.error("openRedPackagePopup 查无此活动，activityId = {}", completedActivityId);
					return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
				}
				activity = activityInfo;
			} else {
				activity = activityList.get(0);
			}
			// 装配活动
			AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, userId, ScenesEnum.ACTIVITY_PAGE);
			appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);

			Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		// 活动是否可以发起
		if (!appletActivityDef.canCreate()) {
			log.info("库存不足");
			Map<String, Object> resultVO = new HashMap<>();
			resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		// 获取用户信息
		AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
		if (appletUser == null) {
			log.error("无效用户 userId = {}", userId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
		}
		// 从活动定义中创建用户活动
		AppletActivity appletActivity = appletActivityDef.newActivity(appletUser.getUserinfo());
		// 重新装配成 OpenRedPackageActivity
		AppletAssistActivity openRedPackageActivity = appletActivity.newOpenRedPackageActivity(appletUser.getUserinfo());
		// 生成用户参与事件
		UserJoinEvent userJoinEvent = openRedPackageActivity.newUserJoinEvent(userId);

		// 持久化活动状态 -- 事务
		boolean save = appletActivityRepository.saveCreateByStockType(openRedPackageActivity);
		if (!save) {
			log.error("活动库存不足");
			return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "活动库存不足");
		}
		// 额外判断是否开奖 -- 处理只需1人就开奖的情况
		if (openRedPackageActivity.isLottery(openRedPackageActivity.getTrigger())) {
			// 补全 用户参与事件
			appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
			// 抽奖，并发起相应的中奖任务
			List<Long> winnerIds = openRedPackageActivity.drawLottery();
			// 持久化活动状态
			save = appletActivityRepository.saveLottery(openRedPackageActivity);
			if (!save) {
				log.info("库存不足");
				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
		}
		// 处理返回数据
		Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
		return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
	}

	/**
	 * 8.分享给你一个5元红包，立即来拆开吧
	 * 帮拆红包
	 *
	 * @param activityId
	 * @param userId
	 * @return
	 */
	@Override
	public SimpleResponse helpOpenRedPackage(Long activityId, Long userId) {
		// 获取 活动
		AppletActivity appletActivity = appletActivityRepository.findById(activityId);
		if (null == appletActivity) {
			log.error("查无此活动，activityId = {}", activityId);
			return new SimpleResponse<>(ApiStatusEnum.ERROR_DB.getCode(), "查无此活动");
		}
		AppletAssistActivity openRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(appletActivity.getActivityInfo(), userId, ScenesEnum.ACTIVITY_PAGE);
		appletActivityRepository.addAssistActivityInfo(openRedPackageActivity);

		if (openRedPackageActivity.isExpired()) {
			// 活动过期
			if (openRedPackageActivity.getActivityInfo().getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
				// 补全 用户参与事件
				appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
				openRedPackageActivity.setExpired();
				appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(openRedPackageActivity);
			}

			return createVisitorOpenRedPackage(openRedPackageActivity);
		}

		// 活动是否可以助力
		if (!openRedPackageActivity.canJoinByUserId(userId)) {
			log.info("活动已完成或已过期 或 自己不可参加自己的活动，activityId = {}", activityId);
			// 用户发起自己的活动
			return createVisitorOpenRedPackage(openRedPackageActivity);
		}
		// 按帮拆规则 对用户帮拆资格进行 校验
		boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + openRedPackageActivity.getActivityInfo().getShopId() + ":" + userId, 365, TimeUnit.DAYS);
		if (!success) {
			log.info("此店铺已帮拆，无法再帮拆， shopId= {}, userId = {}", openRedPackageActivity.getActivityInfo().getShopId(), userId);
			// 用户发起自己的活动
			return createVisitorOpenRedPackage(openRedPackageActivity);
		}
		// 活动计数
		Long trigger = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
		stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, openRedPackageActivity.queryLifeCycle(), TimeUnit.MINUTES);
		if (trigger == null) {
			log.error("活动计数异常，redis异常");
			stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
			// 用户发起自己的活动
			return createVisitorOpenRedPackage(openRedPackageActivity);
		}
		// 是否参与成功
		if (!openRedPackageActivity.isJoinSuccess(trigger)) {
			log.info("活动已完成，activityId = {}", activityId);
			// 用户发起自己的活动
			return createVisitorOpenRedPackage(openRedPackageActivity);
		}

		// 获取用户信息
		AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
		if (appletUser == null) {
			log.error("无效用户, userId = {}", userId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候再试");
		}
		// 通过访问者 补全 活动邀请访问者的记录
		appletActivityRepository.addActivityInviteEventByVisitor(openRedPackageActivity, appletUser.getUserId(), openRedPackageActivity.getActivityInfo().getUserId());
		openRedPackageActivity.inviteSuccess();

		// 将用户成为参与者 -- 获取抽奖码
		Map<String, Object> member = openRedPackageActivity.becomeMemberByUser(appletUser.getUserinfo());
		// 生成用户参与事件
		UserJoinEvent userJoinEvent = openRedPackageActivity.newUserJoinEvent(userId);

		// 持久化活动状态 -- 事务
		boolean save = appletActivityRepository.saveAssistAppletAssistActivity(openRedPackageActivity);
		if (!save) {
			log.error("帮拆后更新数据异常");
			return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "网络异常，请稍候再试");
		}

		// 是否开奖
		if (openRedPackageActivity.isLottery(trigger)) {
			// 补全 用户参与事件
			appletActivityRepository.findUserJoinEventByAppletActivity(openRedPackageActivity, null);
			// 抽奖，并发起相应的中奖任务
			List<Long> winnerIds = openRedPackageActivity.drawLottery();
			// 持久化活动状态
			save = appletActivityRepository.saveLottery(openRedPackageActivity);
			if (!save) {
				log.info("库存不足");
				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
		}
		// 处理返回数据
		return createVisitorOpenRedPackage(openRedPackageActivity);
	}

	/**
	 * 为帮拆着创建 拆红包活动 -- 从分享卡片帮拆逻辑
	 *
	 * @param openRedPackageActivity
	 * @return
	 */
	private SimpleResponse createVisitorOpenRedPackage(AppletAssistActivity openRedPackageActivity) {
		Long visitUserId = openRedPackageActivity.getVisitUserId();

		User user = userService.getById(visitUserId);
		if (user.getVipShopId().equals(user.getShopId())) {
			// 2.3迭代 新增 新手任务-拆红包逻辑
			Activity newbieActivity = newbieOpenRedPackageService.hasPublishedActivityByUserId(visitUserId);
			if (newbieActivity != null) {
				// 装配活动
				AppletAssistActivity newbieOpenRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(newbieActivity, visitUserId, ScenesEnum.ACTIVITY_PAGE);
				appletActivityRepository.addAssistActivityInfo(newbieOpenRedPackageActivity);
				// 装配活动
				newbieOpenRedPackageActivity.setHelpAmount(openRedPackageActivity.getOpenAmount());

				if (newbieOpenRedPackageActivity.isExpired()) {
					// 活动过期
					// 补全 用户参与事件
					if (newbieOpenRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
						appletActivityRepository.findUserJoinEventByAppletActivity(newbieOpenRedPackageActivity, null);
						newbieOpenRedPackageActivity.setExpired();
						appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(newbieOpenRedPackageActivity);
					}
				}
				Map<String, Object> resultVO = newbieOpenRedPackageActivity.queryActivityPageVO();
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
		}


		// 获取店铺的 拆红包活动定义 -- 正常逻辑
		AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(openRedPackageActivity.getActivityDefId());
		if (appletActivityDef == null) {
			log.error("获取活动定义异常");
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
		}
		// 按活动参与规则，获取用户进行中或已完成活动
		JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRule(appletActivityDef.getJoinRule());
		// 判断用户是否能参与活动定义
		if (joinRuleActivity.canJoinByUserId(visitUserId)) {
			// 没有进行中活动且可发起新活动
			if (!appletActivityDef.canCreate()) {
				log.info("库存不足");
				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
			// 发起新活动之前，是否需要再次点击处理 -- 当自己进入自己过期的活动弹窗
			if (openRedPackageActivity.getActivityInfo().getUserId().equals(visitUserId) && openRedPackageActivity.isExpired()) {
				// 如果进入的是自己的活动
				Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
			// 可以参与 -- 说明，可以发起新活动
			// 获取用户信息
			AppletUserAggregateRoot appletUser = appletUserRepository.findById(visitUserId);
			if (appletUser == null) {
				log.error("无效用户, userId = {}", visitUserId);
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
			}
			AppletAssistActivity visitorOpenRedPackageActivity = openRedPackageActivity.newOpenRedPackageActivity(appletUser.getUserinfo());
			// 生成用户参与事件
			UserJoinEvent userJoinEvent = visitorOpenRedPackageActivity.newUserJoinEvent(visitUserId);
			// 自己没有活动，无帮拆机会，点击被人分享链接进入的时候，应该有拆的弹窗
			if (visitorOpenRedPackageActivity.getHelpAmount() == null) {
				// 店铺下有没有 此用户的过期活动
				joinRuleActivityRepository.addLastUserActivity(joinRuleActivity, visitUserId);

				Map<String, Object> resultVO = openRedPackageActivity.queryActivityPageVO();
				resultVO.put("activityStatus", ActivityPopupStatusEnum.OPEN.getCode());
				if (joinRuleActivity.isExpiredLastUserActivity(visitUserId) && openRedPackageActivity.getScenesEnum() == ScenesEnum.ACTIVITY_PAGE) {
					resultVO.put("activityStatus", ActivityPopupStatusEnum.FAILED.getCode());
				}
				return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
			}
			// 持久化活动状态 -- 事务
			boolean save = appletActivityRepository.saveCreateByStockType(visitorOpenRedPackageActivity);
			if (!save) {
				log.error("活动库存不足");
				Map<String, Object> resultVO = new HashMap<>();
				resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
				return new SimpleResponse<>(ApiStatusEnum.SUCCESS, resultVO);
			}
			// 额外判断是否开奖 -- 处理只需1人就开奖的情况
			if (visitorOpenRedPackageActivity.isLottery(visitorOpenRedPackageActivity.getTrigger())) {
				// 补全 用户参与事件
				appletActivityRepository.findUserJoinEventByAppletActivity(visitorOpenRedPackageActivity, null);
				// 抽奖，并发起相应的中奖任务
				List<Long> winnerIds = visitorOpenRedPackageActivity.drawLottery();
				// 持久化活动状态
				save = appletActivityRepository.saveLottery(visitorOpenRedPackageActivity);
				if (!save) {
					log.info("库存不足");
					Map<String, Object> resultVO = new HashMap<>();
					resultVO.put("activityStatus", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
					return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
				}
			}
			// 处理返回数据
			Map<String, Object> resultVO = visitorOpenRedPackageActivity.queryActivityPageVO();
			return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
		}
		// 开始处理 已有进行中活动或不可再发起新活动的情况
		// 获取用户进行中的活动列表
		List<Activity> activityList = joinRuleActivity.queryPublishedActivityByUserId(visitUserId);
		Activity activity;
		if (CollectionUtils.isEmpty(activityList)) {
			// 无进行中活动，获取用户已完成的活动
			Long completedActivityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(visitUserId);
			if (completedActivityId == null) {
				log.error("openRedPackagePopup逻辑异常，须排查");
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "openRedPackagePopup逻辑异常，须排查");
			}
			AppletActivity completedActivity = appletActivityRepository.findById(completedActivityId);
			Activity activityInfo = completedActivity.getActivityInfo();
			if (activityInfo == null) {
				log.error("openRedPackagePopup 查无此活动，activityId = {}", completedActivityId);
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍候重试");
			}
			activity = activityInfo;
		} else {
			activity = activityList.get(0);
		}
		AppletAssistActivity oldOpenRedPackageActivity;
		if (activity.getId().equals(openRedPackageActivity.getId())) {
			oldOpenRedPackageActivity = openRedPackageActivity;
		} else {
			oldOpenRedPackageActivity = appletActivityRepository.assemblyOpenRedPackageActivity(activity, visitUserId, ScenesEnum.ACTIVITY_PAGE);
			// 装配活动
			appletActivityRepository.addAssistActivityInfo(oldOpenRedPackageActivity);
			oldOpenRedPackageActivity.setHelpAmount(openRedPackageActivity.getOpenAmount());
		}
		if (oldOpenRedPackageActivity.isExpired()) {
			// 活动过期
			// 补全 用户参与事件
			if (oldOpenRedPackageActivity.getActivityInfo().getStatus().equals(ActivityStatusEnum.PUBLISHED.getCode())) {
				appletActivityRepository.findUserJoinEventByAppletActivity(oldOpenRedPackageActivity, null);
				oldOpenRedPackageActivity.setExpired();
				appletActivityRepository.saveExpiredOrCompleteOpenRedPackageActivity(oldOpenRedPackageActivity);
			}
		}
		Map<String, Object> resultVO = oldOpenRedPackageActivity.queryActivityPageVO();
		return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
	}
}
