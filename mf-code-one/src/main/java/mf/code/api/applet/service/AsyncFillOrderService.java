package mf.code.api.applet.service;

import mf.code.api.applet.dto.BackFillOrderReq;
import org.springframework.scheduling.annotation.Async;

/**
 * mf.code.api.applet.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月09日 15:08
 */
public interface AsyncFillOrderService {

    void dealWithFillOrderPopupAsync(String orderId, Long merhcantId, Long shopId, Long userId);

    void dealwith(String orderId, Long merchantId, Long shopId, BackFillOrderReq req);
}
