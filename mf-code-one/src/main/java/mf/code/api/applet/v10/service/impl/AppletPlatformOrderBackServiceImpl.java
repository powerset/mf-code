package mf.code.api.applet.v10.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.card.Sku;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletLotteryActivity;
import mf.code.activity.domain.applet.application.impl.AppletLotteryActivityServiceAbstractImpl;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.v10.service.AppletPlatformOrderBackService;
import mf.code.api.applet.v10.service.PlatformMsActivityService;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.api.feignclient.OrderAppService;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.service.CommonConfService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.WxNickUtil;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.ProductEntity;
import mf.code.one.constant.ActivityApiEntrySceneEnum;
import mf.code.one.dto.ApolloLHYXDTO;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.upay.repo.po.ProductSku;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * mf.code.api.applet.v10.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 11:58
 */
@Slf4j
@Service
public class AppletPlatformOrderBackServiceImpl extends AppletLotteryActivityServiceAbstractImpl implements AppletPlatformOrderBackService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CommonConfService commonConfService;
    @Autowired
    private OrderAppService orderAppService;
    @Autowired
    private PlatformMsActivityService platformMsActivityService;
    @Autowired
    private ApolloPropertyService apolloPropertyService;

    @Override
    protected AppletActivity assembleLotteryActivity(AppletActivity appletActivity, Map<String, Object> params) {
        return null;
    }

    @Override
    protected SimpleResponse activityPageVO(AppletActivity appletActivity, Map<String, Object> params) {
        AppletLotteryActivity appletLotteryActivity = (AppletLotteryActivity) appletActivity;

        int type = Integer.parseInt(params.get("type").toString());
        if (type == ActivityApiEntrySceneEnum.ASSIST.getCode()
                || type == ActivityApiEntrySceneEnum.CREATE.getCode()) {
            // redis更新活动列表
            stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + appletActivity.getId(), JSON.toJSONString(appletLotteryActivity.getJoinUserInfo()));
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + appletActivity.getId(), appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);

            Map<String, Object> resultDate = new HashMap<>();
            resultDate.put("joinStatus", 1);
            resultDate.put("awardNO", appletActivity.getJoinUserInfo().get("awardNo"));
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultDate);
        }

        Long userId = Long.valueOf(params.get("userId").toString());
        Long offset = Long.valueOf(params.get("offset").toString());
        Long size = Long.valueOf(params.get("size").toString());

        // 虚拟数据支持
        processInventedData(appletLotteryActivity, offset, size);

        Activity activity = appletLotteryActivity.getActivityInfo();
        ProductEntity product = appletLotteryActivity.getProductEntity();
        AppletUserAggregateRoot visitor = appletUserRepository.findById(appletActivity.getVisitUserId());

        Map<String, Object> goods = new HashMap<>();
        goods.put("title", product.getProductTitle());
        goods.put("picUrl", StringUtils.isBlank(product.getMainPics()) ? new ArrayList<>() : JSON.parseArray(product.getMainPics()));
        goods.put("price", product.getThirdPriceMax());
        goods.put("goodsId", product.getId());
        goods.put("skuId", activity.getGoodsId());
        goods.put("detail", StringUtils.isBlank(product.getDetailPics()) ? new ArrayList<>() : JSON.parseArray(product.getDetailPics()));
        String propsJson = product.getProductSkuList().get(0).getPropsJson();
        if (StringUtils.isNotBlank(propsJson)) {
            List<String> skuValues = new ArrayList<>();
            JSONArray objects = JSON.parseArray(propsJson);
            for (int i = 0; i < objects.size(); i++) {
                JSONObject jsonObject = objects.getJSONObject(i);
                skuValues.add(jsonObject.getString("specsValue"));
            }
            goods.put("sku", skuValues);
        }

        Map<String, Object> activityDetail = new HashMap<>();
        activityDetail.put("activityId", activity.getId());
        activityDetail.put("startTime", activity.getStartTime() != null ? DateFormatUtils.format(activity.getStartTime(), "MM月dd日 HH:mm") : "");//活动开始
        activityDetail.put("endTime", activity.getEndTime() != null ? DateFormatUtils.format(activity.getEndTime(), "MM月dd日 HH:mm") : "");//活动结束
        activityDetail.put("leftTime", activity.getEndTime().getTime() - System.currentTimeMillis());
        Map<String, Object> serviceWx = appletLotteryActivity.getServiceWx();
        if (!CollectionUtils.isEmpty(serviceWx)) {
            activityDetail.put("code", serviceWx.get("customServiceCode"));
        }
        activityDetail.put("condPersionCount", activity.getCondPersionCount() == null ? 0L : activity.getCondPersionCount());//满人开奖
        activityDetail.put("activityUserId", activity.getUserId());
        //满时间开奖
        activityDetail.put("condDrawTime", activity.getEndTime() != null ? DateFormatUtils.format(activity.getEndTime(), "MM月dd日 HH:mm") : "");
        Long batchNum = activity.getBatchNum();
        activityDetail.put("hitsPerDraw", batchNum == null ? activity.getHitsPerDraw() : batchNum);//开奖份数
        Map<Object, Object> userWinningProbability = appletLotteryActivity.getUserWinningProbability();
        Long joinCount = stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activity.getId());

        activityDetail.put("joinCount", joinCount== null ? "0" : joinCount);//活动参加人数
        activityDetail.put("goods", goods);

        Map<String, Object> activityJoin = new HashMap<>();
        int userAwardNoSize = 0;
        activityJoin.put("nickName", visitor.getUserinfo().getNickName());
        activityJoin.put("avatarUrl", visitor.getUserinfo().getAvatarUrl());
        activityJoin.put("userAwardNoSize", userAwardNoSize);
        activityJoin.put("userWinningProbability", "+" + appletLotteryActivity.queryUserWinningProbability() + "%");
        if (!CollectionUtils.isEmpty(userWinningProbability) && userWinningProbability.get(userId.toString()) != null) {
            userAwardNoSize = Integer.parseInt(userWinningProbability.get(userId.toString()).toString());
            activityJoin.put("userAwardNoSize", userAwardNoSize);
        }
        activityJoin.put("joinUserInfoPage", appletLotteryActivity.getJoinUserInfoPage());
        activityJoin.put("pubJoinUserInfoPage", appletLotteryActivity.getPubJoinUserInfoPage());
        // 未参与
        int joinStatus = 0;
        // 已过期
        int activityStatus = -1;
        if (appletLotteryActivity.isPublished()) {
            //进行中 未参与
            activityStatus = 1;
        }
        if (appletLotteryActivity.isAwarded()) {
            //已开奖 -- 未中奖
            activityStatus = 5;
        }
        if (appletLotteryActivity.isAwarded() && appletLotteryActivity.isWinner()) {
            //已开奖 -- 中奖
            activityStatus = 3;
        }
        if (appletLotteryActivity.isAwarded() && appletLotteryActivity.getAwardInfo() != null) {
            activityStatus = 4;
        }

        if (activityStatus == 1 && userAwardNoSize != 0) {
            // 已参与，去邀请
            activityStatus = 2;
        }
        if (userAwardNoSize != 0) {
            // 参与
            joinStatus = 1;
        }
        activityDetail.put("activityStatus", activityStatus);
        activityDetail.put("joinStatus", joinStatus);

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("couponDetail", appletLotteryActivity.getAwardInfo());
        resultVO.put("activityDetail", activityDetail);
        resultVO.put("activityJoin", activityJoin);
        resultVO.put("activityWinnerPerson", appletLotteryActivity.getWinnerInfoPage());
        UserTask userTask = appletLotteryActivity.getUserTask();
        resultVO.put("taskId", userTask == null ? 0 : userTask.getId());
        resultVO.put("taskFinishStatus", userTask == null ? 0 : userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode() ? 1 : 0);
        resultVO.put("dialog", 0);
        String dialog = stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_WINNER_DIALOG + activity.getId() + ":" + appletLotteryActivity.getVisitUserId());
        if (StringUtils.isNotBlank(dialog)) {
            resultVO.put("dialog", dialog);
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_WINNER_DIALOG + activity.getId() + ":" + appletLotteryActivity.getVisitUserId());
        }


        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    // 加工虚拟数据
    private void processInventedData(AppletLotteryActivity appletLotteryActivity, Long offset, Long size) {
        Long activityId = appletLotteryActivity.getId();
        Activity activity = appletLotteryActivity.getActivityInfo();
        Long batchNum = activity.getBatchNum();
        Integer hitsPerDraw = activity.getHitsPerDraw();

        // 虚拟数据个数
        long inventedSize = 0;
        if (batchNum != null && batchNum > hitsPerDraw) {
            inventedSize = batchNum - hitsPerDraw;
        }

        // 数据拷贝
        Long realTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        realTotal = realTotal == null ? 0 : realTotal;
        Long showTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId);
        showTotal = showTotal == null ? 0 : showTotal;
        if (realTotal + inventedSize != showTotal) {
            // 不一致，重设 RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId
            resetInventedData(appletLotteryActivity, inventedSize);
            showTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId);
            showTotal = showTotal == null ? 0 : showTotal;
        }

        mf.code.api.AppletMybatisPageDto<Map> joinUserInfoPage = new mf.code.api.AppletMybatisPageDto<>(size, offset);
        appletLotteryActivity.setJoinUserInfoPage(joinUserInfoPage);
        //查询redis,参与人展现,直接倒叙排列 -- 分页获取
        List<String> range = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId, offset, offset + size - 1);
        //判断redis数据是否为空，为空则去数据库插入再次更新insert一波，非空直接返回
        List<Map> joinUserInfoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(range)) {
            for (String userInfo : range) {
                Map joinUserInfo = JSON.parseObject(userInfo, Map.class);
                joinUserInfoList.add(joinUserInfo);
            }
        }
        joinUserInfoPage.setContent(joinUserInfoList);
        if (offset + size < showTotal) {
            joinUserInfoPage.setPullDown(true);
        }

        mf.code.api.AppletMybatisPageDto<Map> winnerInfoPage = new mf.code.api.AppletMybatisPageDto<>(size, offset);
        appletLotteryActivity.setWinnerInfoPage(winnerInfoPage);

        if (appletLotteryActivity.isAwarded()) {
            Long realWinnerTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId);
            realWinnerTotal = realWinnerTotal == null ? 0 : realWinnerTotal;
            Long showWinnerTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_WINNER_LIST_SHOW + activityId);
            showWinnerTotal = showWinnerTotal == null ? 0 : showWinnerTotal;
            if (realWinnerTotal + inventedSize != showWinnerTotal) {
                // 不一致，重设 RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId
                resetInventedData(appletLotteryActivity, inventedSize);
                showWinnerTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_WINNER_LIST_SHOW + activityId);
                showWinnerTotal = showWinnerTotal == null ? 0 : showWinnerTotal;
            }
            List<String> winnerRange = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_WINNER_LIST_SHOW + activityId, offset, offset + size - 1);
            List<Map> winnerUserInfoList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(winnerRange)) {
                for (String userInfo : winnerRange) {
                    Map winnerUserInfo = JSON.parseObject(userInfo, Map.class);
                    winnerUserInfoList.add(winnerUserInfo);
                }
            }
            winnerInfoPage.setContent(winnerUserInfoList);
            if (offset + size < showWinnerTotal) {
                winnerInfoPage.setPullDown(true);
            }
        }

    }

    // 重设虚拟数据
    private void resetInventedData(AppletLotteryActivity appletLotteryActivity, long inventedSize) {
        Long activityId = appletLotteryActivity.getId();
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId);
        this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_WINNER_LIST_SHOW + activityId);

        // 获取虚拟用户数据信息
        List<String> range = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_JOINLIST_INVENTED + activityId, 0, -1);
        if (CollectionUtils.isEmpty(range)) {
            range = new ArrayList<>();
        }
        if (range.size() != inventedSize) {
            // 重设 redis 虚拟用户数据
            this.stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_JOINLIST_INVENTED + activityId);

            for (int i = 0; i < inventedSize; i++) {
                String randomWxNick = WxNickUtil.getRandomWxNick();
                String randomWxAvatarUrlFromOss = WxNickUtil.getRandomWxAvatarUrlFromOss();

                if (StringUtils.isNotBlank(randomWxNick) && StringUtils.isNotBlank(randomWxAvatarUrlFromOss)) {

                    Map<String, Object> member = new HashMap<>();
                    // 生成抽奖码
                    long currentTime = System.currentTimeMillis();
                    long awardNo = currentTime % 100000000;
                    List<Long> awardNoList = new ArrayList<>();
                    awardNoList.add(awardNo);

                    member.put("uid", -i);
                    member.put("nickName", randomWxNick);
                    member.put("avatarUrl", randomWxAvatarUrlFromOss);
                    member.put("awardNo", awardNoList);
                    member.put("time", System.currentTimeMillis());

                    stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST_INVENTED + activityId, JSON.toJSONString(member));
                }
            }
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST_INVENTED + activityId, appletLotteryActivity.queryLifeCycle(), TimeUnit.MINUTES);

            range = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_JOINLIST_INVENTED + activityId, 0, -1);
            if (CollectionUtils.isEmpty(range)) {
                range = new ArrayList<>();
            }
        }

        Long realTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST + activityId);
        realTotal = realTotal == null ? 0 : realTotal;
        for (int i = 0; i < realTotal; i = i + 50) {
            int end = i + 50 - 1;
            List<String> joinRange = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, i, end);
            if (CollectionUtils.isEmpty(joinRange)) {
                joinRange = new ArrayList<>();
            }
            this.stringRedisTemplate.opsForList().leftPushAll(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId, joinRange);
        }
        if (!CollectionUtils.isEmpty(range)) {
            this.stringRedisTemplate.opsForList().leftPushAll(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activityId, range);
        }

        if (appletLotteryActivity.isAwarded()) {
            Long winnerTotal = this.stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId);
            winnerTotal = winnerTotal == null ? 0 : winnerTotal;
            for (int i = 0; i < winnerTotal; i = i + 50) {
                int end = i + 50 - 1;
                List<String> winnerRange = this.stringRedisTemplate.opsForList().range(RedisKeyConstant.ACTIVITY_WINNER_LIST + activityId, i, end);
                if (CollectionUtils.isEmpty(winnerRange)) {
                    winnerRange = new ArrayList<>();
                }
                this.stringRedisTemplate.opsForList().leftPushAll(RedisKeyConstant.ACTIVITY_WINNER_LIST_SHOW + activityId, winnerRange);
            }
            if (!CollectionUtils.isEmpty(range)) {
                this.stringRedisTemplate.opsForList().leftPushAll(RedisKeyConstant.ACTIVITY_WINNER_LIST_SHOW + activityId, range);
            }
        }

    }

    /**
     * 预热活动列表展示页
     *
     * @param offset
     * @param size
     * @return
     */
    @Override
    public AppletMybatisPageDto preActivityListPage(Integer activityType, Long offset, Long size) {
        // 分页获取活动信息
        Page<Activity> page = new Page<>(offset / size + 1, size);
        page.setDesc("id");

        List<Integer> statusList = new ArrayList<>();
        statusList.add(ActivityStatusEnum.PUBLISHED.getCode());
        statusList.add(ActivityStatusEnum.END.getCode());
        statusList.add(ActivityStatusEnum.FAILURE.getCode());

        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getType, activityType)
                .in(Activity::getStatus, statusList)
                .eq(Activity::getDel, DelEnum.NO.getCode());
        IPage<Activity> activityIPage = activityService.page(page, wrapper);
        if (activityIPage == null || CollectionUtils.isEmpty(activityIPage.getRecords())) {
            return new AppletMybatisPageDto(size, offset);
        }
        Map<Long, Activity> activityMap = new HashMap<>();

        List<Activity> records = activityIPage.getRecords();
        List<Long> productSkuIdList = new ArrayList<>();
        for (Activity activity : records) {
            productSkuIdList.add(activity.getGoodsId());
            activityMap.put(activity.getGoodsId(), activity);
        }
        // 获取活动的商品信息
        List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
        if (CollectionUtils.isEmpty(productEntities)) {
            log.error("获取商品信息异常, productSkuIdList = {}", productSkuIdList);
            return new AppletMybatisPageDto(size, offset);
        }

        // 处理页面展示信息
        List<Map> productVOList = new ArrayList<>();
        for (ProductEntity product : productEntities) {
            Activity activity = activityMap.get(product.getSkuId());
            if (activity == null) {
                continue;
            }
            // redis更新活动列表
            Long joinSize = stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activity.getId());

            Map<String, Object> productVO = new HashMap<>();
            productVO.put("activityId", activity.getId());
            productVO.put("pic", JSON.parseArray(product.getMainPics()).get(0));
            productVO.put("productTitle", product.getProductTitle());
            productVO.put("freeStock", activity.getBatchNum() == null ? activity.getHitsPerDraw() : activity.getBatchNum());
            productVO.put("price", product.getThirdPriceMax());
            productVO.put("joinSize", joinSize);

            productVOList.add(productVO);
        }

        AppletMybatisPageDto<Map> appletMybatisPageDto = new AppletMybatisPageDto<>(size, offset);
        appletMybatisPageDto.setContent(productVOList);

        if (offset + size < activityIPage.getTotal()) {
            appletMybatisPageDto.setPullDown(true);
        }

        return appletMybatisPageDto;
    }

    /**
     * <p>
     * 此方法需要在爆款商品和推荐商品中过滤掉秒杀商品
     * </p>
     * 过滤逻辑:
     * - 从配置中获取秒杀商品skuid
     * - 通过秒杀skuid列表获取sku商品列表
     * - 从sku商品列表中获取商品id列表
     * - 返回爆款商品时过滤掉id在sku商品列表id中的爆款商品
     * - 返回推荐商品时过滤掉id在sk商品列表id中的推荐商品
     *
     * @return
     */
    @Override
    public SimpleResponse sellUnion(Long offset, Long size, Long shopId, Long userId) {

        Map<String, Object> result = new HashMap<>();

        // 查询配置
        ApolloLHYXDTO config = commonConfService.getUnionConf();

        // 获取全部配置
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        // 获取秒杀数据
        List<ApolloLhyxProperty.Ms> msList = apolloLhyxProperty.getLhyx().getMs();
        List<ApolloLhyxProperty.Batch> batchList = new ArrayList<>();
        for (ApolloLhyxProperty.Ms ms : msList) {
            List<ApolloLhyxProperty.Batch> batch = ms.getBatch();
            batchList.addAll(batch);
        }

        // 获取秒杀sku列表
        List<ApolloLhyxProperty.Skus> skuList = new ArrayList<>();
        for (ApolloLhyxProperty.Batch batch : batchList) {
            List<ApolloLhyxProperty.Skus> batchSkus = batch.getSkus();
            skuList.addAll(batchSkus);
        }


        List<Long> skuIds = new ArrayList<>();
        for (ApolloLhyxProperty.Skus sku : skuList) {
            skuIds.add(Long.parseLong(sku.getSku().split(",")[0]));
        }

        // 获取sku商品列表
        List<ProductEntity> productEntityList = goodsAppService.queryProductDetailBySkuIds(skuIds);

        // 获取商品id用于过滤
        List<Long> skuProductId = productEntityList
                .stream()
                .map(ProductEntity::getId)
                .collect(Collectors.toList());

        // 查询状态
        // 状态码0：预热活动未开始1：预热活动进行中 2:预热活动已结束 3：正式活动进行中4：正式活动已结束
        Integer status = config.getStatus();
        result.put("status", status);
        result.put("time", config.getTime());
        result.put("banner", config.getBanner());
        result.put("times", config.getTimes());
        if (status <= 2) {
            // 当正式活动没有开始时，查询 预热活动列表展示页
            AppletMybatisPageDto appletMybatisPageDto = this.preActivityListPage(ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode(), offset, size);
            result.put("prePage", appletMybatisPageDto);
            result.put("count", config.getCount());
            result.put("money", config.getMoney());
        } else {
            AppletMybatisPageDto appletMybatisPageDto = new AppletMybatisPageDto();
            appletMybatisPageDto.setLimit(0);
            appletMybatisPageDto.setOffset(offset);
            appletMybatisPageDto.setContent(new ArrayList());
            appletMybatisPageDto.setPullDown(false);
            result.put("prePage", appletMybatisPageDto);
            // 获取爆款商品
            String hotIdString = config.getHotIds();
            List<Long> hotIds = Arrays.asList(hotIdString.split(","))
                    .stream()
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            List<ProductEntity> hots = sortProductListByIds(hotIds, goodsAppService.listByIds(hotIds));
            if (hots == null) {
                hots = Lists.newArrayList();
            }

            List<Map<String, Object>> hotPriducts = new ArrayList<>();
            for (ProductEntity productEntity : hots) {
                Map<String, Object> hotProduct = new HashMap<>(4);
                hotProduct.put("productId", productEntity.getId());
                hotProduct.put("mainPics", JSON.parseArray(productEntity.getMainPics()).get(0));
                hotProduct.put("productTitle", productEntity.getProductTitle());
                hotProduct.put("productPriceMin", productEntity.getProductPriceMin());
                hotPriducts.add(hotProduct);
            }


            // 过滤秒杀商品
            result.put("hotList",
                    hotPriducts.stream()
                            .filter(product -> !skuProductId.contains(Long.parseLong(product.get("productId").toString())))
                            .collect(Collectors.toList())
            );

            // 获取广告位商品
            String[] groupids1 = config.getGroup1Ids().split(",");
            List<Long> groupId1List = new ArrayList<>();
            for (String id : groupids1) {
                if (!skuProductId.contains(Long.parseLong(id))) {
                    groupId1List.add(Long.parseLong(id));
                }
            }

            String[] groupids2 = config.getGroup2Ids().split(",");
            List<Long> groupId2List = new ArrayList<>();
            for (String id : groupids2) {
                if (!skuProductId.contains(Long.parseLong(id))) {
                    groupId2List.add(Long.parseLong(id));
                }
            }

            List<Long> groupIds = new ArrayList<>();
            groupIds.addAll(groupId1List);
            groupIds.addAll(groupId2List);

            List<ProductEntity> groupList = sortProductListByIds(groupIds, goodsAppService.listByIds(groupIds));
            if (groupList == null) {
                groupList = Lists.newArrayList();
            }

            List<ProductEntity> groups = groupList
                    .stream()
                    .filter(product -> !skuProductId.contains(product.getId()))
                    .collect(Collectors.toList());

            List<Map<String, Object>> group1Products = new ArrayList<>();
            List<Map<String, Object>> group2Products = new ArrayList<>();
            for (int i = 0; i < groups.size(); i++) {
                ProductEntity productEntity = groups.get(i);
                Map<String, Object> groupProduct = new HashMap<>(6);
                groupProduct.put("productId", productEntity.getId());
                groupProduct.put("mainPics", JSON.parseArray(productEntity.getMainPics()).get(0));
                groupProduct.put("productTitle", productEntity.getProductTitle());

                List<ProductSku> skus = goodsAppService.querySkuListByProductIds(Lists.newArrayList(productEntity.getId()));

                if (skus == null || CollectionUtils.isEmpty(skus)) {
                    groupProduct.put("productPriceMin", productEntity.getProductPriceMin());
                    groupProduct.put("productPriceMax", productEntity.getProductPriceMax());
                    skus = Lists.newArrayList();
                } else {
                    groupProduct.put("productPriceMin", productEntity.getProductPriceMin());
                    groupProduct.put("productPriceMax", productEntity.getThirdPriceMax());
                }

                // 统计销售量
                int selled = 0;
                for (ProductSku sku : skus) {
                    selled += sku.getStockDef() - sku.getStock();
                }
                groupProduct.put("selled", selled);

                if (i < groupId1List.size()) {
                    group1Products.add(groupProduct);
                } else {
                    group2Products.add(groupProduct);
                }
            }
            result.put("group1Goods", group1Products);
            result.put("group2Goods", group2Products);

            // 获取推荐列表
            List<ApolloLHYXDTO.Recommend> zstjList = config.getRecommend();
            List<Long> productIds = zstjList
                    .stream()
                    .map(ApolloLHYXDTO.Recommend::getGid)
                    .collect(Collectors.toList());
            List<ProductEntity> productEntities = sortProductListByIds(productIds, goodsAppService.listByIds(productIds));
            if (productEntities == null) {
                productEntities = Lists.newArrayList();
            }
            List<Map<String, Object>> recommend = new ArrayList<>();
            for (ProductEntity productEntity : productEntities) {
                Map<String, Object> recommendProduct = new HashMap<>(6);
                recommendProduct.put("productId", productEntity.getId());
                recommendProduct.put("mainPics", JSON.parseArray(productEntity.getMainPics()).get(0));
                recommendProduct.put("productTitle", productEntity.getProductTitle());

                List<Long> productIdList = new ArrayList<>();
                productIdList.add(productEntity.getId());
                List<ProductSku> skus = goodsAppService.querySkuListByProductIds(productIdList);
                if (skus == null || CollectionUtils.isEmpty(skus)) {
                    recommendProduct.put("productPriceMin", productEntity.getProductPriceMin());
                    recommendProduct.put("productPriceMax", productEntity.getProductPriceMax());
                    skus = Lists.newArrayList();
                } else {
                    recommendProduct.put("productPriceMin", productEntity.getProductPriceMin());
                    recommendProduct.put("productPriceMax", productEntity.getThirdPriceMax());
                }

                // 统计销售量
                int selled = 0;
                for (ProductSku sku : skus) {
                    selled += sku.getStockDef() - sku.getStock();
                }
                recommendProduct.put("selled", selled);

                recommend.add(recommendProduct);
            }

            // 过滤秒杀商品
            result.put("recommend",
                    recommend
                            .stream()
                            .filter(product -> !skuProductId.contains(Long.parseLong(product.get("productId").toString())))
                            .collect(Collectors.toList())
            );
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(result);

        return simpleResponse;
    }

    /**
     * 根据id顺序对产品列表进行排序
     *
     * @param ids
     * @param productEntityList
     * @return
     */
    private List<ProductEntity> sortProductListByIds(List<Long> ids, List<ProductEntity> productEntityList) {
        List<ProductEntity> resultList = new ArrayList<>();
        for (int i = 0; i < ids.size(); i++) {
            int finalI = i;
            resultList.add(
                    productEntityList
                            .stream()
                            .filter(productEntity -> productEntity.getId().equals(ids.get(finalI)))
                            .collect(Collectors.toList())
                            .get(0)
            );
        }

        return resultList;
    }


    @Override
    public SimpleResponse unionPageSeckill(Long userId, Long shopId, String batchIdStr, String dayIdStr) {
        Long batchId = null;
        if (StringUtils.isNotBlank(batchIdStr)) {
            batchId = Long.parseLong(batchIdStr);
        }

        Long dayId = null;
        if (StringUtils.isNotBlank(dayIdStr)) {
            dayId = Long.parseLong(dayIdStr);
        }

        Map<String, Object> result = new HashMap<>(2);
        // 查询配置
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();

        // 获取秒杀活动日期
        List<ApolloLhyxProperty.Ms> msList = apolloLhyxProperty.getLhyx().getMs();
        List<Map<String, Object>> sessions = new ArrayList<>();
        for (ApolloLhyxProperty.Ms ms : msList) {
            Map<String, Object> map = new HashMap<>();
            map.put("day", ms.getActivityDay().split("-")[2]);

            // 获取当天正在进行的场次
            SimpleResponse response = platformMsActivityService.getMsActivityTime(shopId, userId, ms.getId(), null);
            if (response.error()) {
                return response;
            }
            Map<String, Object> data1 = (Map<String, Object>) response.getData();
            List<HashMap<String, Object>> batches = (List<HashMap<String, Object>>) data1.get("times");
            if (CollectionUtils.isEmpty(batches)) {
                log.error("当天没有活动场次");
                continue;
            }

            Integer index = (Integer) data1.get("index");

            List<Map<String, Object>> times = new ArrayList<>();
            Map<String, Object> time1 = new HashMap<>(2);
            HashMap<String, Object> batch1 = batches.get(index);
            time1.put("time", batch1.get("startTime"));
            time1.put("batchId", batch1.get("id"));
            time1.put("dayId", ms.getId());
            time1.put("status", batch1.get("status"));
            times.add(time1);

            if (batchId == null) {
                if (Integer.parseInt(batch1.get("status").toString()) == 1) {
                    batchId = Long.getLong(batch1.get("id").toString());
                }
            }

            if (index < batches.size() - 1) {
                Map<String, Object> time2 = new HashMap<>(2);
                Map<String, Object> batch2 = batches.get(index + 1);
                time2.put("time", batch2.get("startTime"));
                time2.put("batchId", batch2.get("id"));
                time2.put("dayId", ms.getId());
                time2.put("status", batch2.get("status"));
                if (Integer.parseInt(batch2.get("status").toString()) == 1) {
                    batchId = Long.getLong(batch2.get("id").toString());
                }
                times.add(time2);
            } else {
                Map<String, Object> time2 = new HashMap<>(2);
                Map<String, Object> batch2 = batches.get(index - 1);
                time2.put("time", batch2.get("startTime"));
                time2.put("batchId", batch2.get("id"));
                time2.put("dayId", ms.getId());
                time2.put("status", batch2.get("status"));
                if (Integer.parseInt(batch2.get("status").toString()) == 1) {
                    batchId = Long.getLong(batch2.get("id").toString());
                }
                times = new ArrayList<>();
                times.add(time2);
                times.add(time1);

            }

            map.put("times", times);
            sessions.add(map);
        }

        result.put("sessions", sessions);

        //活动开始时间
        Date startTime = DateUtil.parseDate(apolloLhyxProperty.getLhyx().getActivity().getStartTime(), null, DateUtil.FORMAT_ONE);
        //活动结束时间
        Date endTime = DateUtil.parseDate(apolloLhyxProperty.getLhyx().getActivity().getEndTime(), null, DateUtil.FORMAT_ONE);

        //当前时间距离活动开始时间已经第几天了
        long dayDiffStart = DateUtil.dayDiff(startTime, new Date());
        if (dayDiffStart < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动5");
        }
        long dayDiffEnd = DateUtil.dayDiff(endTime, new Date());
        if (dayDiffEnd > 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动6");
        }

        result.put("dayId", dayDiffStart + 1);


        SimpleResponse response = platformMsActivityService.getMsActivityTime(shopId, userId, dayDiffStart + 1, null);
        Map<String, Object> data = (Map<String, Object>) response.getData();
        Integer index = (Integer) data.get("index");
        List<HashMap<String, Object>> batches = (List<HashMap<String, Object>>) data.get("times");
        result.put("batchId", batches.get(index).get("id"));

        // 获取秒杀活动列表
        SimpleResponse msResponse = platformMsActivityService.getMsActivityOneBatch(userId, shopId, dayId, batchId, 0, 6);
        if (msResponse.error()) {
            return msResponse;
        }
        Map<String, Object> msMap = (Map<String, Object>) msResponse.data;
        result.put("buttonStatus", msMap.get("buttonStatus"));
        AppletMybatisPageDto<ProductDistributionDTO> respList = (AppletMybatisPageDto<ProductDistributionDTO>) msMap.get("skus");
        result.put("secKills", respList.getContent());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(result);
        return simpleResponse;
    }
}
