package mf.code.api.applet.service.SendTemplateMessage.impl.userTask;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMsgService;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.goods.repo.po.Goods;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;

/**
 * mf.code.api.applet.service.SendTemplateMessage.impl.award
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 10:53
 */
@Service
@Slf4j
public class NomalAwardUserTaskSendTemplateMsgConverterImpl extends SendTemplateMsgService {
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @Override
    public String userTaskSendTemplateMessageConverter(MerchantShop merchantShop,
                                                       Activity activity,
                                                       UserTask userTask,
                                                       User user,
                                                       Goods goods) {
        String formIDStr = this.queryRedisUserFormIds(user.getId());
        if (StringUtils.isBlank(formIDStr)) {
            log.warn("审核任务编号：{}，该用户编号：{} 的formId不存在", userTask.getId(), user.getId());
            return null;
        }
        this.addUserTaskSingleSendTemplateMsg(activity, userTask, formIDStr, user.getOpenId(), goods);
        return ApiStatusEnum.SUCCESS.getMessage();
    }

    private Map<String, Object> addUserTaskSingleSendTemplateMsg(Activity activity, UserTask userTask, String formId, String openID, Goods goods) {
        String templateID = wxmpProperty.getAuditingResultMsgTmpId();

        boolean needGoodInfo = false;
        String auditingContent = "";
        if (Arrays.asList(
                UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode(),
                UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode()).contains(userTask.getType())) {
            auditingContent = "收藏加购任务";
            needGoodInfo = true;
        } else if (Arrays.asList(
                UserTaskTypeEnum.GOOD_COMMENT_V2.getCode(),
                UserTaskTypeEnum.GOOD_COMMENT.getCode()).contains(userTask.getType())) {
            auditingContent = "好评晒图任务";
        } else if (Arrays.asList(
                UserTaskTypeEnum.ORDER_REDPACK_V2.getCode(),
                UserTaskTypeEnum.ORDER_REDPACK.getCode()).contains(userTask.getType())) {
            auditingContent = "回填订单任务";
        } else {
            auditingContent = "中奖任务";
            needGoodInfo = true;
        }
        String auditStatus = "您的审核已通过";
        boolean success = true;
        if (userTask.getStatus() != UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
            auditStatus = "您的审核未通过";
            success = false;
        }

        String content = auditingContent;
        if (goods != null && needGoodInfo) {
            content = "《" + goods.getDisplayGoodsName() + "》" + auditingContent;
        }

        String finalNotice = "请及时查看审核详情，并按要求在规定时间内完成任务";
        if (success) {
            finalNotice = "您已获得任务佣金奖励，快去看看吧，点击查看详情";
        }

        Object[] objects = {auditStatus, content, finalNotice};
        int keywordNum = objects.length;

        WxSendMsgVo vo = new WxSendMsgVo();
        vo.from(templateID, openID, formId, this.getPageUrl(activity, null, userTask), this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);

        Map<String, Object> stringObjectMap = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
        log.info("审核任务推送消息结果：{}, parms:{}", stringObjectMap, vo);
        this.respMap(stringObjectMap, vo, userTask.getUserId());
        return stringObjectMap;
    }
}
