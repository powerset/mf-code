package mf.code.api.applet.service.UserMoneyTradeDetail.impl.task;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyIndexNameEnum;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 19:21
 */
@Service
@Slf4j
public class NewbieTaskTradeDetailConverterImpl extends UserMoneyTradeDetailService {
    @Override
    public Map<String, Object> getTaskTradeDetail(UpayWxOrder upayWxOrder,
                                                  Integer payType,
                                                  Map<Long, User> userMap) {
        //PALTFORMRECHARGE：平台充值 USERCASH：用户提现 USERPAY：用户支付 PALTFORMREFUND：平台退款
        boolean nullPayType = payType == null;
        boolean paltformrecharge = OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType();
        boolean newbieTask = BizTypeEnum.NEWBIE_TASK_BONUS.getCode() == upayWxOrder.getBizType()
                || BizTypeEnum.NEWBIE_TASK_REDPACK.getCode() == upayWxOrder.getBizType()
                || BizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode() == upayWxOrder.getBizType();
        if (nullPayType && paltformrecharge && newbieTask) {
            String title = "完成新手任务-";
            if (BizTypeEnum.NEWBIE_TASK_BONUS.getCode() == upayWxOrder.getBizType()) {
                title = title + "加群享福利，扫一扫任务";
            } else if (BizTypeEnum.NEWBIE_TASK_REDPACK.getCode() == upayWxOrder.getBizType()) {
                title = title + "查看攻略，拿红包任务";
            } else if (BizTypeEnum.NEWBIE_TASK_OPENREDPACK.getCode() == upayWxOrder.getBizType()) {
                title = title + "新人红包，奖励到任务";
            }
            Map<String, Object> map = new HashMap<>();
            map.put("title", title);
            map.put("type", UserMoneyIndexNameEnum.TASK_AWARD.getCode());
            map.put("indexName", UserMoneyIndexNameEnum.TASK_AWARD.getMessage());
            String plus_minus = "+";
            map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());//直接删除多余的小数位，如2.357会变成2.35
            map.put("time", "");
            if (upayWxOrder.getPaymentTime() != null) {
                map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
            }
            return map;
        }
        return null;
    }
}
