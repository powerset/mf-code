package mf.code.api.applet.service.SendTemplateMessage.impl.award;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMsgService;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * mf.code.api.applet.service.SendTemplateMessage.impl.award
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 10:53
 */
@Service
@Slf4j
public class OpenRedPackSendTemplateMsgConverterImpl extends SendTemplateMsgService {
    @Autowired
    private UserService userService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @Override
    public String awardSendTemplateMessageConverter(MerchantShop merchantShop,
                                                    Activity activity,
                                                    Long nextAwardUserID) {
        //拆红包推送
        if (activity.getType() == ActivityTypeEnum.OPEN_RED_PACKET_START.getCode()) {
            this.sendOpenRedPackMessage(activity);
            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }

    /***
     * 拆红包推送
     * @param activity
     */
    private void sendOpenRedPackMessage(Activity activity) {
        String formIDStr = this.queryRedisUserFormIds(activity.getUserId());
        if (StringUtils.isNotBlank(formIDStr)) {
            String templateID = wxmpProperty.getOrderProessMsgTmpId();

            Object[] objects = new Object[]{"你有红包可提现", "邀请好友帮拆的金额已达到要求", "红包金额已自动发放至小程序钱包，点击前往查看"};
            if (activity.getStatus() == ActivityStatusEnum.FAILURE.getCode()) {
                templateID = wxmpProperty.getAssembleFailMsgTmpId();
                objects = new Object[]{"红包已失效", "邀请好友帮拆的金额未达到要求", "点击重新领取帮拆红包"};
            }

            int keywordNum = objects.length;
            User user = this.userService.selectByPrimaryKey(activity.getUserId());
            WxSendMsgVo vo = new WxSendMsgVo();
            vo.from(templateID, user.getOpenId(), formIDStr, this.getPageUrl(activity, null, null),
                    this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);
            Map<String, Object> stringObjectMapNewMan = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
            log.info("抽奖活动推送消息结果--拆红包：{}, parms:{}", stringObjectMapNewMan, vo);
            this.respMap(stringObjectMapNewMan, vo, user.getId());
        } else {
            log.warn("超级警戒，拆红包未得到推送消息");
        }
    }
}
