package mf.code.api.applet.v3.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.AppletMybatisPageDto;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.v3.dto.Miner;
import mf.code.api.applet.v3.dto.MinerDetailResp;
import mf.code.api.applet.v3.dto.MinerProgressDetailResp;
import mf.code.api.applet.v3.dto.MyMinerDto;
import mf.code.api.applet.v3.service.MinerService;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v3.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月14日 09:50
 */
@Slf4j
@Service
public class MinerServiceImpl implements MinerService {
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private AppletUserRepository appletUserRepository;

    /***
     * 矿工详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryMinerDetail(Long merchantId, Long shopId, Long userId, int offset, int size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MinerDetailResp minerDetailResp = new MinerDetailResp();
        minerDetailResp.setDetail(new AppletMybatisPageDto<MinerDetailResp.MinerScottareDetail>());
        minerDetailResp.getDetail().from(size, offset, 0);
        minerDetailResp.getDetail().setContent(new ArrayList<>());

        //矿工用户信息
        User user = this.userService.selectByPrimaryKey(userId);
        if (user != null) {
            minerDetailResp.from(user);
        }
        List<UserPubJoin> userPubJoins = this.getUserPubJoin(shopId, userId, null);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
            userPubJoinQueryWrapper.lambda()
                    .eq(UserPubJoin::getType, UserPubJoinTypeEnum.SHOPPINGMALL.getCode())
                    .eq(UserPubJoin::getSubUid, userId)
            ;
            userPubJoins = userPubJoinService.list(userPubJoinQueryWrapper);
            if(CollectionUtils.isEmpty(userPubJoins)){
                simpleResponse.setData(minerDetailResp);
                return simpleResponse;
            }
        }
        minerDetailResp.from(userPubJoins.get(0));

        IPage<UserCoupon> userCouponIPage = this.getUserCoupon(merchantId, shopId, userId, null, offset, size);
        List<UserCoupon> userCoupons = userCouponIPage.getRecords();
        if (CollectionUtils.isEmpty(userCoupons)) {
            minerDetailResp.getDetail().from(size, offset, 0);
            simpleResponse.setData(minerDetailResp);
            return simpleResponse;
        }
        for (UserCoupon userCoupon : userCoupons) {
            MinerDetailResp.MinerScottareDetail detail = new MinerDetailResp.MinerScottareDetail();
            detail.from(userCoupon);
            minerDetailResp.getDetail().getContent().add(detail);
        }
        //获取今日相关数值
        String todayCheckPoint = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO + shopId + ":" + userId);
        if (StringUtils.isNotBlank(todayCheckPoint)) {
            JSONObject jsonObject = JSONObject.parseObject(todayCheckPoint);
            Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
            if (jsonMap != null && jsonMap.get("todayScottare") != null) {
                //今日上交税收
                minerDetailResp.setTodayScottare(new BigDecimal(jsonMap.get("todayScottare").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString());
            }
        }
        int pullDown = 0;
        //是否还有下一页
        if (userCouponIPage.getRecords().size() >= size && userCouponIPage.getPages() > userCouponIPage.getCurrent()) {
            pullDown = 1;
        }
        minerDetailResp.getDetail().from(userCouponIPage.getSize(), offset, pullDown);
        simpleResponse.setData(minerDetailResp);
        return simpleResponse;
    }

    /***
     * 矿工进度详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryMinerProgressDetail(Long merchantId, Long shopId, Long userId, int offset, int size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MinerProgressDetailResp minerProgressDetailResp = new MinerProgressDetailResp();
        minerProgressDetailResp.setDetail(new AppletMybatisPageDto<MinerProgressDetailResp.MinerProgressDetail>());
        minerProgressDetailResp.getDetail().from(size, offset, 0);
        minerProgressDetailResp.getDetail().setContent(new ArrayList<>());
        List<UserPubJoin> userPubJoins = this.getUserPubJoin(shopId, userId, null);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            simpleResponse.setData(minerProgressDetailResp);
            return simpleResponse;
        }
        //总缴税金额
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("shopId", shopId);
        totalScottareParams.put("userId", userId);
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);

        minerProgressDetailResp.from(userPubJoins.get(0), allMinerTotalScottare);
        //获取所有的下级矿工们的user_id
        List<UserPubJoin> subUserPubJoins = this.getUserPubJoin(shopId, null, userId);
        List<Long> subUserIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(subUserPubJoins)) {
            for (UserPubJoin userPubJoin : subUserPubJoins) {
                if (subUserIds.indexOf(userPubJoin.getSubUid()) == -1) {
                    subUserIds.add(userPubJoin.getSubUid());
                }
            }
        }
        List<User> users = this.getUser(subUserIds);
        Map<Long, User> userMap = new HashMap<Long, User>();
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                userMap.put(user.getId(), user);
            }
        }

        //将自己加上
        subUserIds.add(userId);
        IPage<UserCoupon> userCouponIPage = this.getUserCoupon(merchantId, shopId, null, subUserIds, offset, size);
        List<UserCoupon> userCoupons = userCouponIPage.getRecords();
        if (!CollectionUtils.isEmpty(userCoupons)) {
            //获取详细
            for (UserCoupon userCoupon : userCoupons) {
                User user = userMap.get(userCoupon.getUserId());
                MinerProgressDetailResp.MinerProgressDetail detail = new MinerProgressDetailResp.MinerProgressDetail();
                detail.from(userCoupon, user);
                minerProgressDetailResp.getDetail().getContent().add(detail);
            }
        }

        //获取今日相关数值
        String todayCheckPoint = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO + shopId + ":" + userId);
        minerProgressDetailResp.fromTodayProfit(todayCheckPoint);

        int pullDown = 0;
        //是否还有下一页
        if (userCouponIPage.getRecords().size() >= size && userCouponIPage.getPages() > userCouponIPage.getCurrent()) {
            pullDown = 1;
        }
        minerProgressDetailResp.getDetail().from(size, offset, pullDown);
        simpleResponse.setData(minerProgressDetailResp);
        return simpleResponse;
    }

    /***
     * 查询我的矿工概况
     * @param merchantId
     * @param shopId
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMyMiner(Long merchantId, Long shopId, Long userId, int offset, int size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        //我的矿工(头像+昵称)+累计缴税金额+今日缴税+进度规则(先查找所有矿工缴税最多的那个矿工金额 占领80%，总长度佣金=最多的矿工金额/80%)
        simpleResponse.setData(this.getMinersInfo(merchantId, shopId, userId, offset, size));
        return simpleResponse;
    }

    /***
     * 查询我的闯关地图
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryCheckpoint(Long merchantId, Long shopId, Long userId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MyMinerDto myMinerDto = new MyMinerDto();
        myMinerDto.setCheckpoint(new MyMinerDto.Checkpoint());
        //本人头像信息
        User user = this.userService.selectByPrimaryKey(userId);
        //获取收益
        List<UserPubJoin> userPubJoins = this.getUserPubJoin(shopId, userId, null);
        UserPubJoin userPubJoin = null;
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            userPubJoin = userPubJoins.get(0);
        }
        //获取字典表-税率信息and关卡信息
        List<CommonDict> commonDicts = this.commonDictService.selectListByType("checkpoint");
        //任务金额+缴税金额
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("shopId", shopId);
        totalScottareParams.put("userId", userId);
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);
        //获取已通关数目
        List<UpayWxOrder> upayWxOrders = getUpayWxOrder(merchantId, shopId, userId);
        //获取税率
        myMinerDto.fromRate(commonDicts);
        //获取所有的关卡
        myMinerDto.fromAllCheckpoint(commonDicts);
        //获取用户信息
        myMinerDto.fromUser(user);
        //获取用户当前关卡的任务收益+缴税收益
        myMinerDto.fromProfit(userPubJoin, allMinerTotalScottare, upayWxOrders, commonDicts);
        //关卡数，关卡目标金额
        myMinerDto.getCheckpoint().fromCheckpointGoalMoney(commonDicts, userPubJoin, allMinerTotalScottare);
        //弹窗信息
        myMinerDto.setDialogInfo(this.getCheckpointDialog(shopId, userId, userPubJoin, allMinerTotalScottare, commonDicts));

        simpleResponse.setData(myMinerDto);
        return simpleResponse;
    }

    /***
     * 获取已通关的订单信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    private List<UpayWxOrder> getUpayWxOrder(Long merchantId, Long shopId, Long userId) {
        QueryWrapper<UpayWxOrder> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UpayWxOrder::getMchId, merchantId)
                .eq(UpayWxOrder::getShopId, shopId)
                .eq(UpayWxOrder::getUserId, userId)
                .eq(UpayWxOrder::getType, OrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
                .eq(UpayWxOrder::getBizType, BizTypeEnum.CHECKPOINT.getCode())
                .orderByDesc(UpayWxOrder::getId)
        ;
        return this.upayWxOrderService.list(wrapper);
    }

    /***
     * 获取闯关地图页面的弹窗信息
     * @param userId 用户编号
     * @param userPubJoin 用户参与对象信息
     * @param allMinerTotalScottare 所有下级矿工们的缴税收益
     * @param commonDicts 关卡字典表
     * @return
     */
    private List<MyMinerDto.DiaLogInfo> getCheckpointDialog(Long shopId,
                                                            Long userId,
                                                            UserPubJoin userPubJoin,
                                                            BigDecimal allMinerTotalScottare,
                                                            List<CommonDict> commonDicts) {
        List<MyMinerDto.DiaLogInfo> diaLogInfos = new ArrayList<>();
        String checkpoint_minerprovide_profit_key = RedisKeyConstant.CHECKPOINT_MINERPROVIDE_PROFIT + shopId + ":" + userId;
        String checkpoint_usertask_finish_key = RedisKeyConstant.CHECKPOINT_USERTASK_FINISH + shopId + ":" + userId;
        String checkpoint_finish_key = RedisKeyConstant.CHECKPOINT_FINISH + shopId + ":" + userId;
        String checkpoint_todayinfo_key = RedisKeyConstant.CHECKPOINT_TODAYINFO + shopId + ":" + userId;
        String checkpoint_difference_amount_key = RedisKeyConstant.CHECKPOINT_DIFFERENCE_AMOUNT + shopId + ":" + userId;

        //矿工们给上级产生的总缴税收益
        String addScottareProfit = this.stringRedisTemplate.opsForValue().get(checkpoint_minerprovide_profit_key);
        //用户完成任务的具体类型+佣金金额(value=json字符串[{"type":1,"amount":"0.1"}]),其中 type 从CheckpointTaskSpaceEnum枚举里取
        String userCheckPointTask = this.stringRedisTemplate.opsForValue().get(checkpoint_usertask_finish_key);
        //用户是否通关(value=json字符串{"value":"不屈黑铁","money":0,"name":0})
        String userCheckPointFinish = this.stringRedisTemplate.opsForValue().get(checkpoint_finish_key);
        //获取该员工的总收益
        BigDecimal userWinProfit = BigDecimal.ZERO;
        if (userPubJoin != null) {
            userWinProfit = userPubJoin.getTotalCommission().add(allMinerTotalScottare);
        }
        //获取今日相关信息
        String todayCheckPoint = this.stringRedisTemplate.opsForValue().get(checkpoint_todayinfo_key);
        if (StringUtils.isNotBlank(addScottareProfit) && StringUtils.isNotBlank(userCheckPointTask)) {
            //两者都不为空，则合并弹窗
            MyMinerDto.DiaLogInfo diaLogInfo = new MyMinerDto.DiaLogInfo();
            diaLogInfo.setType(1);
            diaLogInfo.fromAddScottareProfit(addScottareProfit);
            diaLogInfo.fromUserCheckPointTask(userCheckPointTask, todayCheckPoint);
            diaLogInfos.add(diaLogInfo);
            this.stringRedisTemplate.delete(checkpoint_minerprovide_profit_key);
            this.stringRedisTemplate.delete(checkpoint_usertask_finish_key);
        } else {
            if (StringUtils.isNotBlank(addScottareProfit)) {
                MyMinerDto.DiaLogInfo diaLogInfo = new MyMinerDto.DiaLogInfo();
                diaLogInfo.setType(2);
                diaLogInfo.fromAddScottareProfit(addScottareProfit);
                diaLogInfos.add(diaLogInfo);
                this.stringRedisTemplate.delete(checkpoint_minerprovide_profit_key);
            }
            if (StringUtils.isNotBlank(userCheckPointTask)) {
                MyMinerDto.DiaLogInfo diaLogInfo = new MyMinerDto.DiaLogInfo();
                diaLogInfo.setType(3);
                diaLogInfo.fromUserCheckPointTask(userCheckPointTask, todayCheckPoint);
                diaLogInfos.add(diaLogInfo);
                this.stringRedisTemplate.delete(checkpoint_usertask_finish_key);
            }
        }
        if (StringUtils.isNotBlank(userCheckPointFinish)) {
            //是否通关
            MyMinerDto.DiaLogInfo diaLogInfo = new MyMinerDto.DiaLogInfo();
            diaLogInfo.setType(4);
            diaLogInfo.fromUserCheckPointFinish(userCheckPointFinish);
            diaLogInfos.add(diaLogInfo);
            this.stringRedisTemplate.delete(checkpoint_finish_key);
            this.stringRedisTemplate.delete(checkpoint_difference_amount_key);
        } else {
            if (userWinProfit != null && userWinProfit.compareTo(BigDecimal.ZERO) > 0) {
                //判断总收益是否是否占当前关卡的50%或者90%。若是，弹窗
                MyMinerDto.DiaLogInfo diaLogInfo = new MyMinerDto.DiaLogInfo();
                diaLogInfo.setType(5);
                diaLogInfo.fromDifferenceAmount(userWinProfit, commonDicts);
                if (new BigDecimal(diaLogInfo.getDifferenceAmount()).compareTo(BigDecimal.ZERO) > 0) {
                    //总收益是否是否占当前关卡的50%或者90%，弹窗redis
                    String userCheckPointDifferenceAmount = this.stringRedisTemplate.opsForValue().get(checkpoint_difference_amount_key);
                    if (StringUtils.isBlank(userCheckPointDifferenceAmount)) {
                        this.stringRedisTemplate.opsForValue().set(checkpoint_difference_amount_key, "1");
                        if (diaLogInfo.getRate() >= 90) {
                            this.stringRedisTemplate.opsForValue().set(checkpoint_difference_amount_key, "2");
                        }
                        diaLogInfos.add(diaLogInfo);
                    }
                    boolean dialog90_1 = StringUtils.isNotBlank(userCheckPointDifferenceAmount) && userCheckPointDifferenceAmount.equals("1");
                    if (diaLogInfo.getRate() >= 90 && dialog90_1) {
                        this.stringRedisTemplate.opsForValue().set(checkpoint_difference_amount_key, "2");
                        diaLogInfos.add(diaLogInfo);
                    }
                }
            }
        }
        return diaLogInfos;
    }

    /***
     * 获取我的矿工们的信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    private AppletMybatisPageDto<Miner> getMinersInfo(Long merchantId, Long shopId, Long userId, int offset, int size) {
        AppletMybatisPageDto<Miner> myMi = new AppletMybatisPageDto<Miner>();
        myMi.from(size, offset, 0);
        myMi.setContent(new ArrayList<>());

        //获取我的矿工们
        List<UserPubJoin> userPubJoins = this.findAllMyMiner(shopId, null, userId, offset, size);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            return myMi;
        }

        Integer total = this.countAllMyMiner(shopId, userId);

        List<Long> subUserIds = new ArrayList<>();
        Map<Long, UserPubJoin> userPubJoinMap = new HashMap<>();
        for (UserPubJoin subUserPubJoin : userPubJoins) {
            if (subUserIds.indexOf(subUserPubJoin.getSubUid()) == -1) {
                subUserIds.add(subUserPubJoin.getSubUid());
            }
            userPubJoinMap.put(subUserPubJoin.getSubUid(), subUserPubJoin);
        }
        //获取我的矿工们的信息
        List<User> users = this.getUser(subUserIds);
        Map<Long, User> userMap = new HashMap<Long, User>();
        if (!CollectionUtils.isEmpty(users)) {
            for (User subUser : users) {
                userMap.put(subUser.getId(), subUser);
            }
        }

        //获取总进度金额=最大总缴税金额/80%
        BigDecimal bigScottare = userPubJoins.get(0).getTotalScottare()
                .multiply(new BigDecimal("100"))
                .divide(new BigDecimal("80"), 5, BigDecimal.ROUND_DOWN);

        //获取我的矿工们
        for (UserPubJoin userPubJoin : userPubJoins) {
            User subUser = userMap.get(userPubJoin.getSubUid());
            UserPubJoin subUserPubJoin = userPubJoinMap.get(userPubJoin.getSubUid());
            Miner detail = new Miner();
            String todayCheckPoint = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO + shopId + ":" + userPubJoin.getSubUid());
            String todayScottare = null;
            if (StringUtils.isNotBlank(todayCheckPoint)) {
                JSONObject jsonObject = JSONObject.parseObject(todayCheckPoint);
                Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                if (jsonMap != null && jsonMap.get("todayScottare") != null) {
                    //今日上交税收
                    todayScottare = new BigDecimal(jsonMap.get("todayScottare").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString();
                }
            }
            detail.fromMiner(subUser, subUserPubJoin, todayScottare, bigScottare);
            myMi.getContent().add(detail);
        }

        int pullDown = 0;
        //是否还有下一页
        if (offset + size < total) {
            pullDown = 1;
        }
        myMi.from(size, offset, pullDown);
        return myMi;
    }

    private Integer countAllMyMiner(Long shopId, Long userId) {

        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("shopId", shopId);
        return userPubJoinService.countAllMyMiner(params);

    }

    /***
     * 获取详细
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    private IPage<UserCoupon> getUserCoupon(Long merchantId, Long shopId, Long userId, List<Long> subUserIds, int offset, int size) {
        //获取页码
        int pageNum = offset / size + 1;
        Page<UserCoupon> page = new Page<>(pageNum, size);
        QueryWrapper<UserCoupon> userCouponQueryWrapper = new QueryWrapper<>();
        userCouponQueryWrapper.lambda()
                .eq(UserCoupon::getMerchantId, merchantId)
                .eq(UserCoupon::getShopId, shopId)
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                .gt(UserCoupon::getCommission, BigDecimal.ZERO)
                .orderByDesc(UserCoupon::getId)
        ;
        if (!CollectionUtils.isEmpty(subUserIds)) {
            userCouponQueryWrapper.and(params -> params.in("user_id", subUserIds));
        }
        if (userId != null && userId > 0) {
            userCouponQueryWrapper.and(params -> params.eq("user_id", userId));
        }
        IPage<UserCoupon> userCouponIPage = userCouponService.page(page, userCouponQueryWrapper);
        return userCouponIPage;
    }


    /***
     * 获取我的所有矿工
     * @param shopId
     * @param userId
     * @param bossUserId
     * @param offset
     * @param size
     * @return
     */
    private List<UserPubJoin> findAllMyMiner(Long shopId, Long userId, Long bossUserId, int offset, int size) {

        Map<String, Object> params = new HashMap<>();
        params.put("userId", bossUserId);
        params.put("shopId", shopId);
        params.put("offset", offset);
        params.put("size", size);
        return userPubJoinService.findAllMyMiner(params);

        //获取页码
        // int pageNum = offset / size + 1;
        // Page<UserPubJoin> page = new Page<>(pageNum, size);
        // QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        // userPubJoinQueryWrapper.lambda()
        //         .eq(UserPubJoin::getShopId, shopId)
        //         .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
        //         .orderByDesc(UserPubJoin::getTotalScottare)//以缴税金额倒叙排列
        // ;
        // if (userId != null && userId > 0) {
        //     userPubJoinQueryWrapper.and(params -> params.eq("sub_uid", userId));
        // }
        // if (bossUserId != null && bossUserId > 0) {
        //     userPubJoinQueryWrapper.and(params -> params.eq("user_id", bossUserId));
        // }
        // IPage<UserPubJoin> userPubJoinIPage = userPubJoinService.page(page, userPubJoinQueryWrapper);
        // return userPubJoinIPage;
    }

    /***
     * 获取累计缴税
     * @param shopId
     * @param userId
     * @return
     */
    private List<UserPubJoin> getUserPubJoin(Long shopId, Long userId, Long bossUserId) {
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getShopId, shopId)
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
        ;
        if (userId != null && userId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("sub_uid", userId));
        }
        if (bossUserId != null && bossUserId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("user_id", bossUserId));
        }
        return this.userPubJoinService.list(userPubJoinQueryWrapper);
    }

    /***
     * 获取矿工信息
     * @param userIds
     * @return
     */
    private List<User> getUser(List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return new ArrayList<>();
        }
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(User::getId, userIds)
        ;
        return this.userService.list(wrapper);
    }
}
