package mf.code.api.applet.login.domain.valueobject;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.user.constant.UserPubJoinTypeEnum;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-23 下午3:20
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class CheckpointsInvitationSpecification extends InviteSpecification {

	CheckpointsInvitationSpecification() {
		super.userPubJoinType = UserPubJoinTypeEnum.CHECKPOINTS.getCode()+"";
		super.activityId = "0";
		super.invitationRoleEnum = InvitationRoleEnum.SUB;
	}

	// 这要看被邀请者的状态
	@Override
	protected Boolean canInviteAppletSubUser(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser) {
		return null;
	}

	// 是否可以被别人抓为矿工
	@Override
	protected Boolean canBeInvitedByAppletPubUser(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser) {

		if (appletSubUser.getInvitationHistory() == null) {
			log.info("未初始化邀请历史记录");
			return false;
		}

		List<UserPubJoin> userPubJoins = appletSubUser.getInvitationHistory().getRecords().get(super.userPubJoinType);
		if (userPubJoins == null) {
			return true;
		}

		String platform = appletSubUser.getInvitationHistory().getPlatformId().toString();
		for (UserPubJoin userPubJoin : userPubJoins) {
			// 存在 并且 上级不是平台，则不能被抓
			if (!StringUtils.equals(userPubJoin.getUserId().toString(), platform)) {
				log.info("已经是其他boss的矿工了，无法再被抓");
				return false;
			}
		}

		/*if (StringUtils.equals(appletPubUser.getUserId().toString(), platform)) {
			return true;
		}*/

		Long pubUserVipShopId = appletPubUser.getUserinfo().getVipShopId();
		// 判断pubUser是不是在自己的粉丝店铺抓矿工
		if (!appletSubUser.getShopId().equals(pubUserVipShopId)) {
			// 被邀请进来的用户更新店铺后的shopId，与邀请者的粉丝店铺Id不匹配，说明邀请者是在非他粉丝店铺中邀请的，不能成为矿工
			log.info("非此店铺粉丝，无法抓取矿工");
			return false;
		}

		// 判断user和pubUser是不是同一个店铺的粉丝
		if (!appletSubUser.getUserinfo().getVipShopId().equals(pubUserVipShopId)) {
			log.info("矿工不是此店铺的粉丝");
			return false;
		}
		return true;
	}
}
