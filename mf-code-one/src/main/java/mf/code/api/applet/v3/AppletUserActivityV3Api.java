package mf.code.api.applet.v3;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.v3.dto.RedPackageReq;
import mf.code.api.applet.v3.service.AppletUserActivityQV3Service;
import mf.code.api.applet.v3.service.AppletUserActivityUV3Service;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.applet.v3
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-17 下午1:05
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v3/userActivity")
public class AppletUserActivityV3Api {
    private final AppletUserActivityUV3Service appletUserActivityUV3Service;
    private final AppletUserActivityQV3Service appletUserActivityQV3Service;

    @Autowired
    public AppletUserActivityV3Api(AppletUserActivityUV3Service appletUserActivityUV3Service, AppletUserActivityQV3Service appletUserActivityQV3Service) {
        this.appletUserActivityUV3Service = appletUserActivityUV3Service;
        this.appletUserActivityQV3Service = appletUserActivityQV3Service;
    }

    @PostMapping("/openRedPackage")
    public SimpleResponse openRedPackage(@RequestBody RedPackageReq redPackageReq) {
        return appletUserActivityUV3Service.openRedPackage(redPackageReq);
    }

    @GetMapping("/queryOpenRedPackageBarrage")
    public SimpleResponse openRedPackageBarrage(@RequestParam(value = "current", defaultValue = "1") String current,
                                                @RequestParam(value = "size", defaultValue = "10") String size,
                                                @RequestParam("activityDefId") String activityDefId) {
        return appletUserActivityQV3Service.openRedPackageBarrage(current, size, activityDefId);
    }

    @GetMapping("/checkOpenRedPackageValidity")
    public SimpleResponse checkOpenRedPackageValidity(@RequestParam("merchantId") String merchantId,
                                                      @RequestParam("shopId") String shopId,
                                                      @RequestParam("userId") String userId,
                                                      @RequestParam("activityDefId") String activityDefId,
                                                      @RequestParam(value = "in", defaultValue = "0") String in) {
        return appletUserActivityQV3Service.checkOpenRedPackageValidity(merchantId, shopId, userId, activityDefId, in);
    }

    @GetMapping("/pubJoinOpenRedPackageActivity")
    public SimpleResponse pubJoinOpenRedPackageActivity(@RequestParam("userId") String userId,
                                                        @RequestParam("pubUserId") String pubUserId,
                                                        @RequestParam("activityId") String activityId,
                                                        @RequestParam(value = "pubJoinScene", defaultValue = "1") Integer pubJoinScene) {
        return appletUserActivityUV3Service.pubJoinOpenRedPackageActivity(userId, pubUserId, activityId, pubJoinScene);
    }

    /**
     * 扫码弹窗回填订单
     *
     * @param backFillOrderReq
     * @return
     */
    @PostMapping(path = "/backfillOrderPopup")
    public SimpleResponse backfillOrderPopup(@RequestBody BackFillOrderReq backFillOrderReq) {
//        return appletUserActivityUV3Service.backfillOrderPopup(backFillOrderReq);
        return appletUserActivityUV3Service.backfillOrderPopupAsync(backFillOrderReq);
    }

    @GetMapping(value = "/queryBackfillOrderResult")
    public SimpleResponse queryBackfillOrderResult(@RequestParam(name = "userId") Long userId,
                                                   @RequestParam(name = "orderId") String orderId){
        return appletUserActivityUV3Service.queryBackfillOrderResult(orderId);
    }

    /**
     * 扫码弹窗回填订单
     *
     * @param backFillOrderReq
     * @return
     */
    @PostMapping(path = "/backfillOrderPopupReplace")
    public SimpleResponse backfillOrderPopupReplace(@RequestBody BackFillOrderReq backFillOrderReq) {
        SimpleResponse response = new SimpleResponse();
        if (!StringUtils.isNumeric(backFillOrderReq.getUserId())
                || StringUtils.isBlank(backFillOrderReq.getOrderId())
                || !StringUtils.isNumeric(backFillOrderReq.getShopId())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("请求参数错误");
            return response;
        }
        Long userId = Long.valueOf(backFillOrderReq.getUserId());
        String orderId = backFillOrderReq.getOrderId();
        Long shopId = Long.valueOf(backFillOrderReq.getShopId());
        return appletUserActivityUV3Service.backfillOrderPopupReplace(shopId, userId, orderId);
    }


}
