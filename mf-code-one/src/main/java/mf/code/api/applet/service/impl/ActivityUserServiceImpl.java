package mf.code.api.applet.service.impl;/**
 * create by gbf on 2018/11/8 0008
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.ActivityUserService;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.Constants;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author gbf
 * @description :对用户发起的活动的操作
 */
@Service
@Slf4j
public class ActivityUserServiceImpl implements ActivityUserService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private AppletUserActivityService appletUserActivityService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 更新zset  如果有此goodsId就不在更新 , 如果没有就add
     */
    private Integer updateZSet(Long shopId, Long goodsId) {
        Set<String> zset = stringRedisTemplate.opsForZSet().range(mf.code.uactivity.repo.redis.RedisKeyConstant.GOODS_ID_RANKING_LIST + shopId, 0L, -1L);
        boolean boo = zset.contains(goodsId);
        if (!boo) {
            stringRedisTemplate.opsForZSet().add(mf.code.uactivity.repo.redis.RedisKeyConstant.GOODS_ID_RANKING_LIST + shopId,
                    goodsId.toString(),
                    System.currentTimeMillis());
        }
        return 1;
    }

    /**
     * 调用退款方法
     *
     * @param orderId
     * @param type
     * @param uid
     * @param aid
     * @param orderNo
     * @return
     */
    private SimpleResponse callRefund(Long orderId, Integer type, Long uid, Long aid, String orderNo) {
        UpayWxOrder order = upayWxOrderService.findById(orderId);
        BigDecimal totalFee = null;
        if (order != null) {
            totalFee = order.getTotalFee();
        }
        //退款业务
        SimpleResponse response = upayWxOrderService.refund(type, order.getMchId(), order.getShopId(), uid, totalFee, orderId, Constants.BizType.MIAN_DAN_CHOU_JIANG, aid, orderNo);
        if (response.getCode() == 0) {
            //退款成功了
            return new SimpleResponse(Constants.RefundStatus.REFUND_SUCCESS, "库存不足,将退款");
        } else {
            //退款失败了
            return new SimpleResponse(Constants.RefundStatus.REFUND_FAILD, "库存不足,退款失败,错误码:" + response.getCode() + ",错误信息:" + response.getMessage());
        }
    }

    /**
     * 退款 并 返回response
     *
     * @param orderId
     * @param type
     * @param uid
     * @param aid
     * @param orderNo
     * @return
     */
    private SimpleResponse refundAndReturnResp(Long orderId, Integer type, Long uid, Long aid, String orderNo, String errorMsg) {
        Map<String, Object> wxOrderParams = new HashMap<>();
        wxOrderParams.put("refundOrderId", orderId);
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(wxOrderParams);
        SimpleResponse response = new SimpleResponse();
        if (upayWxOrders == null || upayWxOrders.size() == 0) {
            response = callRefund(orderId, type, uid, aid, orderNo);
            if (response.getCode() == 0 || response.getCode() == Constants.RefundStatus.REFUND_SUCCESS) {
                response = new SimpleResponse(Constants.RefundStatus.REFUND_SUCCESS, "库存不够,已经退款,错误信息:" + errorMsg);
            } else {
                response = new SimpleResponse(Constants.RefundStatus.REFUND_FAILD, "库存不够,退款失败.错误信息:" + response.getMessage());
            }
        }
        return response;
    }

    /**
     * 回调后更新
     *
     * @param type          支付类型    1: 余额支付  2:微信支付
     * @param uid           用户id
     * @param aid           活动id
     * @param orderId       订单主键id
     * @param shopId
     * @param goodsId
     * @param activityDefId fId 商户定义用户发起的活动id
     * @param paymentTime   微信解出来的时间
     * @param orderNo       订单No(32位生成的订单号, 并非订单主键id)
     * @return
     */
    @Override
    public SimpleResponse updateActivityDef4Callback(int type,
                                                     Long uid,
                                                     Long aid,
                                                     Long orderId,
                                                     Long shopId,
                                                     Long goodsId,
                                                     Long activityDefId,
                                                     Date paymentTime,
                                                     String orderNo) {
        // redis 防重
        String redisForbidKey = RedisKeyConstant.FORBID_KEY + uid.toString() + aid.toString() + orderId.toString();
        boolean boo = RedisForbidRepeat.NX(stringRedisTemplate, redisForbidKey);
        if (!boo) {
            return new SimpleResponse(-1, "请勿重复提交");
        }

        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        int stockNum = activityDef.getHitsPerDraw();
        if (activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode()) ||
                activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())) {
            // 免单抽奖发起
            // 2 调用杨花接口 插入抽奖码
            SimpleResponse response = appletUserActivityService.freeOrderLeader(uid, aid);
            if (response.getCode() != 0) {
                log.error("插入抽奖码方法返回{}{}", response.getCode(), response.getMessage());
                return refundAndReturnResp(orderId, type, uid, aid, orderNo, response.getMessage());
            }
        } else if (activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode())) {
            //新人有礼活动扣除2个库存
            stockNum = stockNum + 1;
            SimpleResponse response = appletUserActivityService.newManLeader(uid, aid);
            if (response.getCode() != 0) {
                log.error("新人有礼 发起失败{}{}", response.getCode(), response.getMessage());
                return refundAndReturnResp(orderId, type, uid, aid, orderNo, response.getMessage());
            }
        }

        //调用百川完成, 改activity_def 的 库存
        if (activityDefService.reduceStockAfterCallback(activityDefId, stockNum, activityDef.getStock()) != 1) {
            //需要退款
            return refundAndReturnResp(orderId, type, uid, aid, orderNo, "库存不足");
        } else {
            //库存够 更新语句返回 1
            // 更新zset .
            updateZSet(activityDef.getShopId(), goodsId);
            // 3 改activity状态 status=1 , 并且生成开始和结束时间
            if (activityService.updateActivityStatus(aid, ActivityStatusEnum.PUBLISHED.getCode()) != 1) {
                log.error("更新活动状态失败");
            }
            // 4 upay_wx_order表 支付状态
            if (type != OrderPayTypeEnum.CASH.getCode()) {
                if (upayWxOrderService.updateOrderStatusPlus(createOrderObj(orderId, paymentTime)) != 1) {
                    log.error("更新订单表支付状态失败");
                }
            }
            return new SimpleResponse(0, "success.创建订单成功");
        }
    }

    @Override
    @Transactional
    public SimpleResponse updateActivityDef4Callback4Wx(int type,
                                                        Long uid,
                                                        Long aid,
                                                        Long orderId,
                                                        Long shopId,
                                                        Long goodsId,
                                                        Long activityDefId,
                                                        Date paymentTime,
                                                        String orderNo) {
        // redis 防重
        String redisForbidKey = RedisKeyConstant.FORBID_KEY + uid.toString() + aid.toString() + orderId.toString();
        log.info("stringRedisTemplate:{} ,redisForbidKey:{}", stringRedisTemplate, redisForbidKey);
        boolean boo = RedisForbidRepeat.NX(stringRedisTemplate, redisForbidKey);
        if (!boo) {
            return new SimpleResponse(-1, "请勿重复提交");
        }

        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        //新人一次减2个库存,正常发起用户的活动只减1个库存
        int stockNum = activityDef.getHitsPerDraw();
        if (activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK.getCode()) ||
                activityDef.getType().equals(ActivityDefTypeEnum.ORDER_BACK_V2.getCode())) {
            // 2 调用杨花接口 插入抽奖码
            SimpleResponse response = appletUserActivityService.freeOrderLeader(uid, aid);
            if (response.getCode() != 0) {
                log.error("插入抽奖码方法返回{}{}", response.getCode(), response.getMessage());
                //走退款
                return refundAndReturnResp(orderId, type, uid, aid, orderNo, response.getMessage());
            }
        } else if (activityDef.getType().equals(ActivityDefTypeEnum.NEW_MAN.getCode())) {
            //新人有礼活动扣除2个库存  这里已经扣除了库存
            stockNum = activityDef.getHitsPerDraw() + 1;
            SimpleResponse response = appletUserActivityService.newManLeader(uid, aid);
            if (response.getCode() != 0) {
                log.error(" 新人有礼 发起失败{}{}", response.getCode(), response.getMessage());
                //走退款
                return refundAndReturnResp(orderId, type, uid, aid, orderNo, response.getMessage());
            }
        }

        //调用百川完成, 改activity_def 的 库存
        Integer subStockResult = activityDefService.reduceStockAfterCallback(activityDefId, stockNum, activityDef.getStock());
        if (subStockResult == 0) {
            //走退款
            return refundAndReturnResp(orderId, type, uid, aid, orderNo, "库存不足");
        } else {
            //库存够 更新语句返回 1
            // 更新zset .
            updateZSet(activityDef.getShopId(), goodsId);
            // 3 改activity状态 status=1 , 并且生成开始和结束时间
            if (activityService.updateActivityStatus(aid, 1) != 1) {
                log.error("更新活动状态失败.事务失败");
            }
            // 4 upay_wx_order表 支付状态
            if (type != OrderPayTypeEnum.CASH.getCode()) {
                if (upayWxOrderService.updateOrderStatusPlus(createOrderObj(orderId, paymentTime)) != 1) {
                    log.error(" 更新订单表支付状态失败");
                }
            }
        }
        return new SimpleResponse(0, "success");
    }


    private UpayWxOrder createOrderObj(Long orderId, Date paymentTime) {
        UpayWxOrder upayWxOrder = new UpayWxOrder();
        upayWxOrder.setId(orderId);
        upayWxOrder.setPaymentTime(paymentTime);
        Date date = new Date();
        upayWxOrder.setNotifyTime(date);
        upayWxOrder.setUtime(date);
        return upayWxOrder;
    }


}
