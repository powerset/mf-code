package mf.code.api.applet.login.domain.aggregateroot;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.login.domain.valueobject.InviteSpecification;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-22 下午5:59
 */
@Slf4j
@EqualsAndHashCode(callSuper = true)
@Data
public class InvitationEvent extends HandlingEventAggregateRoot {
	private AppletUserAggregateRoot appletPubUser;
	private AppletUserAggregateRoot appletSubUser;
	private InviteSpecification invitationSpecification;
	private UserPubJoin recordInfo;
	private Date invitationTime;

	InvitationEvent(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser, InviteSpecification invitationSpecification) {
		this.appletPubUser = appletPubUser;
		this.appletSubUser = appletSubUser;
		this.invitationSpecification = invitationSpecification;
		this.invitationTime = new Date();
		this.recordInfo = newUserPubJoin();
	}

	void resetRecordInfo (UserPubJoin recordInfo) {
		recordInfo.setUserId(this.appletPubUser.getUserId());
		recordInfo.setTotalScottare(BigDecimal.ZERO);
		recordInfo.setUtime(this.invitationTime);
		this.recordInfo = recordInfo;
	}

	private UserPubJoin newUserPubJoin() {
		// 获取被邀请者信息
		User user = this.appletSubUser.getUserinfo();

		UserPubJoin userPubJoin = new UserPubJoin();
		userPubJoin.setUserId(this.appletPubUser.getUserId());
		userPubJoin.setPubTime(this.invitationTime);
		userPubJoin.setSubUid(this.appletSubUser.getUserId());
		userPubJoin.setSubNick(user.getNickName());
		userPubJoin.setSubAvatar(user.getAvatarUrl());
		userPubJoin.setSubLoginTime(user.getGrantTime());
		userPubJoin.setSubActionStatus(0);
		userPubJoin.setSubActionTime(new Date(0));
		userPubJoin.setTotalScottare(BigDecimal.ZERO);
		userPubJoin.setTotalCommission(BigDecimal.ZERO);
		userPubJoin.setCtime(this.invitationTime);
		userPubJoin.setUtime(this.invitationTime);
		userPubJoin.setActivityId(Long.valueOf(this.invitationSpecification.getActivityId()));
		userPubJoin.setType(Integer.valueOf(invitationSpecification.getUserPubJoinType()));
		userPubJoin.setShopId(this.appletSubUser.getShopId());

		if (StringUtils.equals(this.invitationSpecification.getActivityId(), "0")) {
			userPubJoin.setSubActionStatus(1);
			userPubJoin.setSubActionTime(this.invitationTime);
		}

		return userPubJoin;
	}
}
