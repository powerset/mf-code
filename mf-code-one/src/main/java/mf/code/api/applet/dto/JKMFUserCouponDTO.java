package mf.code.api.applet.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * mf.code.api.applet.dto
 * Description:
 *
 * @author gel
 * @date 2019-07-18 15:07
 */
@Data
public class JKMFUserCouponDTO {

    private Long userId;
    private Long couponId;
    private Long userCouponId;
    private BigDecimal amount;
    private BigDecimal limitAmount;
    private Integer hasCoupon;
}
