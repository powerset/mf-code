package mf.code.api.applet.v8.service;

import mf.code.activity.constant.UserCouponTypeEnum;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:54
 */
public class UserCouponAboutDTO {

    /***
     * 闯关页面 用户中奖 收益明细的具体类型界定()
     * @return
     */
    public static List<Integer> checkPointMinerDetailUserCouponTyeps() {
        List<Integer> list = new ArrayList<>();
        list.add(UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
        list.add(UserCouponTypeEnum.GOODS_COMMENT.getCode());
        list.add(UserCouponTypeEnum.FAV_CART.getCode());
        list.add(UserCouponTypeEnum.ORDER_RED_PACKET_V2.getCode());
        list.add(UserCouponTypeEnum.GOODS_COMMENT_V2.getCode());
        list.add(UserCouponTypeEnum.FAV_CART_V2.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode());
        list.add(UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode());
        list.add(UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode());
        return list;
    }
}
