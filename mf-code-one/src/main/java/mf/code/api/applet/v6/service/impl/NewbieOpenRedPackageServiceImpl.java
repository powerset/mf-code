package mf.code.api.applet.v6.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.v6.service.NewbieOpenRedPackageService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.api.applet.v6.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-21 11:15
 */
@Slf4j
@Service
public class NewbieOpenRedPackageServiceImpl implements NewbieOpenRedPackageService {
	@Autowired
	private ActivityDefService activityDefService;
	@Autowired
	private ActivityService activityService;

	/**
	 * 是否是创建新手任务-拆红包
	 * @param userId
	 * @return true 完成，false 未完成
	 */
	@Override
	public boolean isCompleteNewbieOpenRedPackage(Long userId) {
		QueryWrapper<Activity> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Activity::getUserId, userId)
				.eq(Activity::getType, ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode())
				.eq(Activity::getStatus, ActivityStatusEnum.END.getCode())
				.eq(Activity::getDel, DelEnum.NO.getCode());
		int activityCount = activityService.count(wrapper);
		return activityCount != 0;
	}

	/**
	 * 是否存在新手任务
	 * @param shopId
	 * @return
	 */
	@Override
	public ActivityDef hasExistNewbieTask(Long shopId) {
		QueryWrapper<ActivityDef> defWrapper = new QueryWrapper<>();
		defWrapper.lambda()
				.eq(ActivityDef::getShopId, shopId)
				.eq(ActivityDef::getType, ActivityDefTypeEnum.NEWBIE_TASK.getCode())
				.eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
				.eq(ActivityDef::getDel, DelEnum.NO.getCode());
		return activityDefService.getOne(defWrapper);
	}

	/**
	 * 是否有进行中的 新人任务-拆红包
	 * @param userId
	 * @return true 有; false 没有
	 */
	@Override
	public Activity hasPublishedActivityByUserId(Long userId) {
		List<Integer> typeList = new ArrayList<>();
		typeList.add(ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode());
		typeList.add(ActivityTypeEnum.OPEN_RED_PACKET_START.getCode());

		QueryWrapper<Activity> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Activity::getUserId, userId)
				.in(Activity::getType, typeList)
				.eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
				.eq(Activity::getDel, DelEnum.NO.getCode());
		return activityService.getOne(wrapper);
	}

	/**
	 * 获取 绑定的 拆红包活动定义
	 * @param id
	 * @return
	 */
	@Override
	public ActivityDef findNewbieOpenRedPackageByParentId(Long id) {
		QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(ActivityDef::getParentId, id)
				.eq(ActivityDef::getType, ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())
				.eq(ActivityDef::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
				.eq(ActivityDef::getDel, DelEnum.NO.getCode());
		return activityDefService.getOne(wrapper);
	}

	/**
	 * 最新一条 记录
	 * @param userId
	 * @return
	 */
	@Override
	public Activity lastNewbieOpenRedPackage(Long userId) {

		QueryWrapper<Activity> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(Activity::getUserId, userId)
				.eq(Activity::getType, ActivityTypeEnum.NEWBIE_TASK_OPENREDPACKAGE.getCode())
				.eq(Activity::getDel, DelEnum.NO.getCode());

		Page<Activity> page = new Page<>(0, 1);
		page.setDesc("id");
		IPage<Activity> activityIPage = activityService.page(page, wrapper);
		if (activityIPage == null || CollectionUtils.isEmpty(activityIPage.getRecords())) {
			return null;
		}
		List<Activity> records = activityIPage.getRecords();

		return records.get(0);
	}

}
