package mf.code.api.applet.dto;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityPlan;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.constant.WeightingEnum;
import mf.code.common.utils.ConvertUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserTask;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月03日 19:39
 */
@Data
public class TaskResp extends CommissionResp {
    public static final int recommitType_failed = 1;
    public static final int recommitType_timeout = 2;

    private Long taskId = 0L;
    private Long userTaskId = 0L;
    private String orderNo = "";
    private String keyword = "";
    private String competitorPic = "";
    private String goodsPic1 = "";
    private String goodsPic2 = "";
    //审核拒绝理由
    private String auditingReason = "";
    //任务状态
    private Integer status;
    //任务类型
    private Integer type;
    //截止时间的时间戳
    private Long cutoffTime = 0L;

    //红包金额
    private String redPackMoney = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //接收红包的时间-任务完成得红包的时间
    private String redPackMoneyTime = "";
    //是否弹窗--主要用于红包收藏加购任务
    @JsonProperty("isDialog")
    private boolean dialog;

    /***客服信息***/
    private String customServiceWxNum = "";
    //店铺名
    private String shopName;
    /***非抽奖活动任务展现***/
    //商品名
    private String title;
    //商品编号
    private Long goodsId;
    //商品图片
    private String goodsPicUrl;
    //商品价格
    private String goodsPrice;
    //添加店铺商品url信息
    private String goodsUrl = "";

    /***活动相关***/
    @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
    //活动开始时间
    private Date startTime;
    @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
    //活动结束时间
    private Date endTime;

    /***好评晒图***/
    //是否填写了订单
    private boolean isFillInOrder;
    //购买时间
    private String buyTime = "";

    //展现的图
    private List<String> pics = new ArrayList<>();
    //展现的原因理由-下标对象
    private List<ShowReason> showReason = new ArrayList<>();

    //是否加权
    private boolean weight;

    //订单不通过原因
    private String orderReason;

    //佣金
    private String commission;

    //预计可赚
    private String estimateProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

    @Data
    private static class ShowReason {
        private String reason;
        private int index;
        private String pic;
    }

    /***
     * 中奖任务/红包任务 1.1版本
     * @param userTask
     * @param activity
     * @param isDialog
     */
    public void fromUserTask(UserTask userTask, Activity activity, boolean isDialog) {
        this.dialog = isDialog;
        if (activity != null && activity.getWeighting() == WeightingEnum.YES.getCode()) {
            //1是加权，0是不加权
            this.weight = true;
        }
        if (StringUtils.isNotBlank(userTask.getOrderId())) {
            this.orderNo = userTask.getOrderId();
        }
        if (StringUtils.isNotBlank(userTask.getKeyWords())) {
            this.keyword = userTask.getKeyWords();
        }
        if (userTask.getReceiveTime() != null) {
            this.redPackMoneyTime = DateFormatUtils.format(userTask.getReceiveTime(), "yyyy.MM.dd HH:mm");
        }
        if (userTask.getRpAmount() != null) {
            this.redPackMoney = userTask.getRpAmount().setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
        this.taskId = userTask.getId();
        this.userTaskId = userTask.getId();
        this.status = userTask.getStatus();
        this.type = userTask.getType();
        if (userTask != null && StringUtils.isNotBlank(userTask.getOrderId())) {
            this.setFillInOrder(true);
        }
        if (StringUtils.isNotBlank(userTask.getTaskGoodsUrl())) {
            this.goodsUrl = userTask.getTaskGoodsUrl();
        }
        if (StringUtils.isNotBlank(userTask.getPics())) {
            this.pics = JSONObject.parseArray(userTask.getPics(), String.class);
        }
        if (activity != null) {
            this.startTime = activity.getStartTime();
            this.endTime = activity.getEndTime();
        }
        if (StringUtils.isNotBlank(userTask.getAuditingReason()) && JsonParseUtil.booJsonArr(userTask.getAuditingReason())) {
            List<Object> objects = JSONObject.parseArray(userTask.getAuditingReason(), Object.class);
            if (!CollectionUtils.isEmpty(objects)) {
                for (Object obj : objects) {
                    //[{"orderId":"","pic":"","index":0,"reason":"一"},{"index":1, "reason":""},{"index":2, "reason":"三"}]
                    JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                    Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                    if (jsonMap.get("reason") != null && StringUtils.isNotBlank(jsonMap.get("reason").toString())) {
                        ShowReason showReason = new ShowReason();
                        if (jsonMap.get("orderId") != null && StringUtils.isNotBlank(jsonMap.get("orderId").toString())) {
                            this.orderReason = jsonMap.get("reason").toString();
                            continue;
                        }

                        showReason.setIndex((Integer) jsonMap.get("index"));
                        showReason.setReason(jsonMap.get("reason").toString());
                        this.showReason.add(showReason);
                    }
                }
            }
        } else {
            if (StringUtils.isNotBlank(userTask.getAuditingReason())) {
                this.auditingReason = userTask.getAuditingReason();
            }
        }
    }

    /***
     * 获取图片信息、红包金额相关信息1.1版本
     * @param userTask
     * @param activityDef
     * @param activityPlan
     */
    public void fromPics(UserTask userTask, ActivityDef activityDef, ActivityPlan activityPlan, ActivityTask activityTask, BigDecimal rate) {
        List<String> pics = new ArrayList<String>();
        if (StringUtils.isNotBlank(userTask.getPics())) {
            pics = JSONObject.parseArray(userTask.getPics(), String.class);
        }
        Map<String, Object> respMap = new HashMap<>();
        if (activityDef != null) {
            respMap.put("redPackMoney", activityDef.getGoodsPrice() == null ? activityDef.getCommission().toString() : activityDef.getGoodsPrice().toString());
        } else if (activityPlan != null) {
            respMap.put("redPackMoney", activityPlan.getRedpackUnitPrice() == null ? BigDecimal.ZERO : activityPlan.getRedpackUnitPrice().toString());
        } else if (activityTask != null) {
            BigDecimal tax = activityTask.getRpAmount().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
            respMap.put("redPackMoney", activityTask.getRpAmount() == null ? BigDecimal.ZERO : activityTask.getRpAmount().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        if (UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() == userTask.getType()) {
            if (pics != null && pics.size() >= 2) {
                int i = 1;
                for (String pic : pics) {
                    respMap.put("goodsPic" + i, pic);
                    i++;
                }
            }
        } else if (UserTaskTypeEnum.GOOD_COMMENT.getCode() == userTask.getType()) {
            if (pics != null && pics.size() >= 1) {
                int i = 1;
                for (String pic : pics) {
                    respMap.put("goodsPic" + i, pic);
                    i++;
                }
            }
        } else {
            if (pics != null && pics.size() >= 3) {
                respMap.put("competitorPic", pics.get(0));
                pics.remove(0);
                int i = 1;
                for (String pic : pics) {
                    respMap.put("goodsPic" + i, pic);
                    i++;
                }
            }
        }
        if (respMap != null) {
            if (respMap.get("redPackMoney") != null) {
                this.redPackMoney = respMap.get("redPackMoney").toString();
            }
            if (respMap.get("goodsPic1") != null) {
                this.goodsPic1 = respMap.get("goodsPic1").toString();
            }
            if (respMap.get("goodsPic2") != null) {
                this.goodsPic2 = respMap.get("goodsPic2").toString();
            }
            if (respMap.get("competitorPic") != null) {
                this.competitorPic = respMap.get("competitorPic").toString();
            }
        }
    }

    /***
     * 获取商品信息
     * @param goods
     */
    public void fromGoods(Goods goods) {
        if (goods != null) {
            this.title = goods.getDisplayGoodsName();
            this.goodsId = goods.getId();
            this.goodsPicUrl = goods.getPicUrl();
            this.goodsPrice = goods.getDisplayPrice().toString();
            this.redPackMoney = goods.getDisplayPrice().toString();
        }
    }


    public void fromShop(MerchantShop merchantShop) {
        this.shopName = ConvertUtil.dataMasking(merchantShop.getShopName());
        if (StringUtils.isNotBlank(merchantShop.getCsJson())) {
            List<Object> objects = JSONObject.parseArray(merchantShop.getCsJson(), Object.class);
            //目前取第一个
            if (objects != null && objects.size() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(objects.get(0).toString());
                Map<String, Object> csJsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                this.customServiceWxNum = csJsonMap.get("wechat").toString();
            }
        }
    }

    /***
     * 获取倒计时信息
     * @param userTask
     * @param activity
     * @param activityTask
     */
    public void fromUserTaskCountdown(UserTask userTask, ActivityDef activityDef, Activity activity, ActivityTask activityTask, Long totalTimeCost) {
        if (userTask != null && userTask.getStartTime() != null) {
            Integer missionNeedTime = 0;
            if (activity != null && activity.getMissionNeedTime() != null && activity.getMissionNeedTime() > 0) {
                missionNeedTime = activity.getMissionNeedTime();
            }
            if (activityTask != null && activityTask.getMissionNeedTime() != null && activityTask.getMissionNeedTime() > 0) {
                missionNeedTime = activityTask.getMissionNeedTime();
            }
            if (activityDef != null) {
                if (activityDef.getMissionNeedTime() != null && activityDef.getMissionNeedTime() > 0) {
                    missionNeedTime = activityDef.getMissionNeedTime();
                }
                if (activityDef.getCondDrawTime() != null && activityDef.getCondDrawTime() > 0) {
                    missionNeedTime = activityDef.getCondDrawTime();
                }
            }
            Date overTime = DateUtils.addMinutes(userTask.getStartTime(), missionNeedTime);
            overTime = DateUtils.addSeconds(overTime, totalTimeCost.intValue());
            this.cutoffTime = overTime.getTime();
        }
    }
}
