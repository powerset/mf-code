package mf.code.api.applet.service.UserMoneyTradeDetail;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.time.DateFormatUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 18:05
 */
@Slf4j
public class UserMoneyTradeDetailService implements UserMoneyTradeDetailConverter {


    /***
     * type:
     *      1:免单抽奖(奖品名)(+)
     *      2:轮盘抽奖(轮盘抽奖)(+)
     *      3:随机红包(随机成功)(+)
     *      4:红包任务(好评返现任务)(+)
     *      5:钱包提现(提现成功)(-)
     *      6:钱包充值(微信充值)(+)
     *      7:钱包支出(微信充值发起抽奖活动)(-)
     *               (活动退款至微信)(-)
     *      8:钱包退回(抽奖活动退款)(+)
     *      9:好评晒图任务()(+)
     *      10:拆红包()(+)
     * @param upayWxOrder
     * @param payType 当用户发起的 1：微信充值，2：退款 额外多出一条记录
     * @return
     */
    private Map<String, Object> getTradeDetail(UpayWxOrder upayWxOrder, Integer payType, Map<Long, Activity> activityMap, Map<Long, Goods> goodsMap) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("title", "");
        map.put("type", 0);
        map.put("orderId", upayWxOrder.getId());
        String plus_minus = "";
        if (payType == null) {
            //PALTFORMRECHARGE：平台充值 USERCASH：用户提现 USERPAY：用户支付 PALTFORMREFUND：平台退款
            if (OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType()) {
                if (BizTypeEnum.ACTIVITY.getCode() == upayWxOrder.getBizType()) {
                    if (upayWxOrder.getBizValue() > 0) {
                        Activity activity = activityMap.get(upayWxOrder.getBizValue());
                        if (activity != null && activity.getGoodsId() != null && activity.getGoodsId() > 0) {
                            Goods goods = goodsMap.get(activity.getGoodsId());
                            if (goods == null) {
                                log.error("超级警戒~~交易明细的活动对应的商品不存在，订单编号为：{}", upayWxOrder.getId());
                            }
                            //免单抽奖
                            map.put("title", goods.getDisplayGoodsName());
                            map.put("type", 1);
                            plus_minus = "+";
                        }
                    }
                } else if (BizTypeEnum.LUCKY_WHEEL.getCode() == upayWxOrder.getBizType()) {
                    map.put("title", "轮盘抽奖");
                    map.put("type", 2);
                    plus_minus = "+";
                } else if (BizTypeEnum.RANDOMREDPACK.getCode() == upayWxOrder.getBizType()) {
                    map.put("title", "回填订单红包");
                    map.put("type", 3);
                    plus_minus = "+";
                } else if (BizTypeEnum.REDPACKTASK.getCode() == upayWxOrder.getBizType()) {
                    map.put("title", "收藏加购任务");
                    map.put("type", 4);
                    plus_minus = "+";
                } else if (BizTypeEnum.GOODSCOMMENT.getCode() == upayWxOrder.getBizType()) {
                    map.put("title", "好评晒图任务");
                    map.put("type", 9);
                    plus_minus = "+";
                } else if (BizTypeEnum.OPEN_RED_PACKAGE.getCode() == upayWxOrder.getBizType()) {
                    map.put("title", "拆红包");
                    map.put("type", 10);
                    plus_minus = "+";
                }
            } else if (OrderTypeEnum.USERCASH.getCode() == upayWxOrder.getType()) {
                map.put("title", "提现成功");
                map.put("type", 5);
                plus_minus = "-";
            } else if (OrderTypeEnum.USERPAY.getCode() == upayWxOrder.getType()) {
                //CASH 余额发起活动，WEIXIN 微信发起活动
                if (OrderPayTypeEnum.CASH.getCode() == upayWxOrder.getPayType()) {
                    map.put("title", "余额发起抽奖活动");
                    map.put("type", 7);
                    plus_minus = "-";
                } else {
                    map.put("title", "微信充值");
                    map.put("type", 6);
                    plus_minus = "+";
                }
            } else if (OrderTypeEnum.PALTFORMREFUND.getCode() == upayWxOrder.getType()) {
                if (OrderPayTypeEnum.CASH.getCode() == upayWxOrder.getPayType()) {
                    map.put("title", "抽奖活动退款至钱包");
                    map.put("type", 8);
                    plus_minus = "+";
                } else {
                    map.put("title", "抽奖活动退款至微信-发起");
                    map.put("type", 8);
                    plus_minus = "+";
                }
            }
        } else {
            if (payType == 1) {//微信充值，发起活动
                map.put("title", "微信充值发起抽奖活动");
                map.put("type", 7);
                plus_minus = "-";
            }
            if (payType == 2) {
                map.put("title", "抽奖活动退款至微信-到账");
                map.put("type", 7);
                plus_minus = "-";
            }
        }
        map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());//直接删除多余的小数位，如2.357会变成2.35
        map.put("time", "");
        if (upayWxOrder.getPaymentTime() != null) {
            map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
        }
        return map;
    }

    public UserMoneyIndexNameEnum getIndexName(UpayWxOrder upayWxOrder) {
        UserMoneyIndexNameEnum message = null;
        if (BizTypeEnum.ACTIVITY.getCode() == upayWxOrder.getBizType()) {
            message = UserMoneyIndexNameEnum.ACTIVITY;
        } else if (BizTypeEnum.ASSIST_ACTIVITY.getCode() == upayWxOrder.getBizType()) {
            message = UserMoneyIndexNameEnum.ASSIST_ACTIVITY;
        } else if (BizTypeEnum.ORDER_BACK.getCode() == upayWxOrder.getBizType()) {
            message = UserMoneyIndexNameEnum.ORDER_BACK;
        } else if (BizTypeEnum.NEWMAN_START.getCode() == upayWxOrder.getBizType()) {
            message = UserMoneyIndexNameEnum.NEWMAN_START;
        }
        return message;
    }


    /***
     *
     * @param upayWxOrder
     * @param payType
     * @param activityMap
     * @param goodsMap
     * @return
     */
    @Override
    public Map<String, Object> getUserMoneyTradeDetail(UpayWxOrder upayWxOrder,
                                                       Integer payType,
                                                       Map<Long, Activity> activityMap,
                                                       Map<Long, Goods> goodsMap,
                                                       CommonDict commonDict) {
        return null;
    }

    @Override
    public Map<String, Object> getTaskTradeDetail(UpayWxOrder upayWxOrder,
                                                  Integer payType,
                                                  Map<Long, User> userMap) {
        return null;
    }

    @Override
    public Map<String, Object> covertUserMoneyTradeDetailForProductGoodsShopping(UpayWxOrder upayWxOrder, Integer payType, Map<Long, Map> shopMallMap) {
        return null;
    }

    @Override
    public Map<String, Object> covertUserMoneyTradeDetailForContribution(UpayWxOrder upayWxOrder, Integer payType, Map<Long, User> userMap) {
        return null;
    }
}
