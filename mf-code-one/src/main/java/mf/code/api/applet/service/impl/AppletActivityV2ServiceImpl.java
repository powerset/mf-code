package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSONArray;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.ActivityDefResp;
import mf.code.api.applet.service.AppletActivityV2Service;
import mf.code.common.constant.*;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.FakeDataUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.upay.service.UpayBalanceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.api.applet.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2018-11-07 17:16
 */
@Service
public class AppletActivityV2ServiceImpl implements AppletActivityV2Service {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private UserTaskService userTaskService;


    @Override
    public SimpleResponse orderbackV2(Long shopId, Long userId) {
        /**
         * 1、查询activity_def
         * 2、查询activity，新人有礼活动参与资格
         * 3、查询activity，免单机会参与资格
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        if (shopId == null || userId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("参数为空");
            return simpleResponse;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("传入店铺信息有误");
            return simpleResponse;
        }
        // 查询activity_def
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantShop.getMerchantId());
        params.put("shopId", shopId);
        params.put("queryTime", new Date());
        params.put("typeList", Arrays.asList(ActivityDefTypeEnum.ORDER_BACK.getCode(),ActivityDefTypeEnum.ASSIST.getCode()));
        params.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        params.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.selectByParams(params);
        // 数据分类
        List<ActivityDefResp> assistList = new ArrayList<>();
        List<ActivityDefResp> orderBackList = new ArrayList<>();
        Map<String, Object> resultMap = new HashMap<>(2);
        if (CollectionUtils.isEmpty(activityDefs)) {
            resultMap.put("newManList", assistList);
            resultMap.put("orderBackList", orderBackList);
            // 添加假数据
            FakeDataUtil.orderBackData(resultMap);
            simpleResponse.setData(resultMap);
            return simpleResponse;
        }
        List<Long> defIdList = new ArrayList<>();
        List<Long> goodsIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            ActivityDefResp resp = copyDefToResp(activityDef);
            goodsIds.add(activityDef.getGoodsId());
            if (activityDef.getType() == ActivityDefTypeEnum.ASSIST.getCode()) {
                assistList.add(resp);
            }
            if (activityDef.getType() == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
                orderBackList.add(resp);
                defIdList.add(activityDef.getId());
            }
        }
        // 查询商品数据
        List<Goods> goodsList = goodsService.selectByIdList(goodsIds);
        if (CollectionUtils.isEmpty(goodsList)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("数据有误");
            return simpleResponse;
        }
        Map<Long, Goods> goodsMap = new HashMap<>(goodsList.size());
        for (Goods goods : goodsList) {
            goodsMap.put(goods.getId(), goods);
        }
        // 查询新人有礼活动参与资格
        Map<String, Object> assistParams = new HashMap<>();
        assistParams.put("merchantId", merchantShop.getMerchantId());
        assistParams.put("shopId", shopId);
        assistParams.put("userId", userId);
        assistParams.put("type", ActivityTypeEnum.ASSIST.getCode());
        List<Integer> statusList = new ArrayList<>();
        // 未付款发起成功，不允许重新发起
//        statusList.add(ActivityStatusEnum.UNPUBLISHED.getCode());
        // 正在进行中，不允许重新发起
        statusList.add(ActivityStatusEnum.PUBLISHED.getCode());
        // 发起者，中奖者都完成任务，记做已结束，不允许重新发起
        statusList.add(ActivityStatusEnum.END.getCode());
        assistParams.put("activityStatuses", statusList);
        List<Activity> newManActList = activityService.findPage(assistParams);
        // 判断有无，修改新人有礼任务状态
        for (ActivityDefResp resp : assistList) {
            resp.setPicUrl(goodsMap.get(Long.valueOf(resp.getGoodsId())).getPicUrl());
            resp.setTitle(goodsMap.get(Long.valueOf(resp.getGoodsId())).getDisplayGoodsName());
            // 只要完成或正在进行一次该店铺下的新人有礼活动，就显示已发起
            if (!CollectionUtils.isEmpty(newManActList)) {
                for (Activity activity : newManActList) {
                    if (!activity.getActivityDefId().equals(Long.valueOf(resp.getActivityDefId()))) {
                        continue;
                    }
                    resp.setActivityId(activity.getId().toString());
                    if (activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()
                            && activity.getAwardStartTime() == null) {
                        resp.setActStatus(OrderBackFrontStatusEnum.NEW_ING.getCode().toString());
                        resp.setErrorMsg("活动未结束不能发起");
                        break;
                    }
                    // 只有验证发起者有没有完成任务user_task
                    Map<String, Object> taskParams = new HashMap<>();
                    taskParams.put("type", UserTaskTypeEnum.ASSIST_START.getCode());
                    taskParams.put("userId", userId);
                    taskParams.put("merchantId", merchantShop.getMerchantId());
                    taskParams.put("shopId", merchantShop.getId());
                    taskParams.put("activityId", activity.getId());
                    List<Integer> userTaskStatusList = new ArrayList<>();
                    userTaskStatusList.add(UserTaskStatusEnum.INIT.getCode());
                    userTaskStatusList.add(UserTaskStatusEnum.AUDIT_WAIT.getCode());
                    userTaskStatusList.add(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
                    userTaskStatusList.add(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
                    taskParams.put("statusList", userTaskStatusList);
                    List<UserTask> countUserTask = userTaskService.listPageByParams(taskParams);
                    if (!CollectionUtils.isEmpty(countUserTask)) {
                        resp.setActStatus(OrderBackFrontStatusEnum.NEW_TASK.getCode().toString());
                        // 添加个人微信号
                        Map<String, Object> customServiceAbout = merchantShopService.getCustomServiceAbout(merchantShop);
                        if (!CollectionUtils.isEmpty(customServiceAbout) && StringUtils.isBlank(resp.getCustomServiceWxNum())) {
                            resp.setCustomServiceWxNum(customServiceAbout.get("customServiceWxNum").toString());
                        }
                        resp.setErrorMsg("任务进行中");
                        resp.setTaskStatus(countUserTask.get(0).getStatus().toString());
                        break;
                    }
                }
            }
        }

        // 查询免单活动参与资格
        Map<String, Object> orderBackParams = new HashMap<>();
        orderBackParams.put("merchantId", merchantShop.getMerchantId());
        orderBackParams.put("shopId", shopId);
        orderBackParams.put("userId", userId);
        orderBackParams.put("types", Arrays.asList(ActivityTypeEnum.ORDER_BACK_START.getCode(),ActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
        List<Integer> activityStatuses = new ArrayList<>();
        activityStatuses.add(ActivityStatusEnum.UNPUBLISHED.getCode());
        activityStatuses.add(ActivityStatusEnum.PUBLISHED.getCode());
        activityStatuses.add(ActivityStatusEnum.FAILURE.getCode());
        orderBackParams.put("activityStatuses", activityStatuses);
        orderBackParams.put("activityDefIdList", defIdList);
        List<Activity> orderBackActList = activityService.findPage(orderBackParams);
        Map<String, Object> countMap = new HashMap<>();
        // 如果不为空则需要修改免单活动参与状态
        for (ActivityDefResp resp : orderBackList) {
            resp.setPicUrl(goodsMap.get(Long.valueOf(resp.getGoodsId())).getPicUrl());
            resp.setTitle(goodsMap.get(Long.valueOf(resp.getGoodsId())).getDisplayGoodsName());
            if (CollectionUtils.isEmpty(orderBackActList)) {
                continue;
            }
            for (Activity activity : orderBackActList) {
                if (!activity.getActivityDefId().toString().equals(resp.getActivityDefId())) {
                    continue;
                }
                if (ActivityStatusEnum.UNPUBLISHED.getCode() == activity.getStatus()) {
                    resp.setActivityId(activity.getId().toString());
                    resp.setActStatus(OrderBackFrontStatusEnum.ORDER_PAY.getCode().toString());
                    resp.setErrorMsg("可发起支付");
                    break;
                } else if (ActivityStatusEnum.PUBLISHED.getCode() == activity.getStatus()) {
                    resp.setActStatus(OrderBackFrontStatusEnum.ORDER_CANNOT.getCode().toString());
                    resp.setErrorMsg("活动未结束不能发起");
                    break;
                } else {
                    resp.setActStatus(OrderBackFrontStatusEnum.ORDER_PAY.getCode().toString());
                    resp.setErrorMsg("");
                }
            }
            // TODO 查询完成活动次数，待优化
            countMap.put("activityDefId", resp.getActivityDefId());
            countMap.put("merchantId", merchantShop.getMerchantId());
            countMap.put("shopId", merchantShop.getId());
            countMap.put("userId", userId);
            countMap.put("del", DelEnum.NO.getCode());
            int size = activityService.countActivity(countMap);
            if (size >= 3) {
                resp.setActStatus(OrderBackFrontStatusEnum.ORDER_CANNOT.getCode().toString());
                resp.setErrorMsg("本商品您已发起3次抽奖活动，不能再发起");
                continue;
            }
        }
        resultMap.put("newManList", assistList);
        resultMap.put("orderBackList", orderBackList);
        // 添加假数据
        FakeDataUtil.orderBackData(resultMap);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    private ActivityDefResp copyDefToResp(ActivityDef activityDef) {
        ActivityDefResp resp = new ActivityDefResp();
        resp.setActivityDefId(activityDef.getId().toString());
        resp.setPrice(activityDef.getGoodsPrice().toString());
        resp.setStock(activityDef.getStock().toString());
        resp.setGoodsId(activityDef.getGoodsId().toString());
        // 如果助力库存不足
        if (activityDef.getType() == ActivityDefTypeEnum.ASSIST.getCode()) {
            resp.setActStatus(OrderBackFrontStatusEnum.NEW_PAY.getCode().toString());
            if (activityDef.getStock() <= 0) {
                resp.setActStatus(OrderBackFrontStatusEnum.STOCK.getCode().toString());
                resp.setErrorMsg("该商品暂无库存");
            }
        }
        // 无库存不能参加
        if (activityDef.getStock() <= 0) {
            resp.setActStatus(OrderBackFrontStatusEnum.STOCK.getCode().toString());
            resp.setErrorMsg("该商品暂无库存");
        }
        // 20190119 更新客服微信号和微信图片
        if(StringUtils.isNotBlank(activityDef.getServiceWx())){
            if(JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(csMapList)) {
                    //微信号
                    resp.setCustomServiceWxNum((String) csMapList.get(0).get("wechat"));
                    //微信二维码
                    resp.setCustomServiceCode((String) csMapList.get(0).get("pic"));
                }
            }
        }
        return resp;
    }
}
