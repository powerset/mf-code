package mf.code.api.applet.v8;

import mf.code.api.applet.v8.service.UserMoneyV8Service;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.v8
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:26
 */
@RestController
@RequestMapping("/api/applet/v8/userMoney")
public class UserMoneyV8Api {
    @Autowired
    private UserMoneyV8Service userMoneyV8Service;

    /***
     * 查询我的提现记录
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    @RequestMapping(path = "/queryPresentOrderLog", method = RequestMethod.GET)
    public SimpleResponse queryPresentOrderLog(@RequestParam(name = "merchantId") final Long merchantId,
                                               @RequestParam(name = "shopId") final Long shopID,
                                               @RequestParam(name = "userId") final Long userID,
                                               @RequestParam(name = "limit", required = false, defaultValue = "10") int size,
                                               @RequestParam(name = "offset", required = false, defaultValue = "0") int offset) {
        return this.userMoneyV8Service.queryPresentOrderLog(merchantId, shopID, userID, offset, size);
    }

    /***
     * 查询收入记录
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @param type 1:自购省 2：分享赚 3：任务奖金 4：任务佣金
     * @param section 1: 平台 2：店铺
     * @return
     */
    @RequestMapping(path = "/queryIncomeLog", method = RequestMethod.GET)
    public SimpleResponse queryIncomeLog(@RequestParam(name = "merchantId") final Long merchantId,
                                         @RequestParam(name = "shopId") final Long shopID,
                                         @RequestParam(name = "userId") final Long userID,
                                         @RequestParam(name = "limit", required = false, defaultValue = "10") int size,
                                         @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                         @RequestParam(name = "type", required = false, defaultValue = "0") Integer type,
                                         @RequestParam(name = "section", required = false, defaultValue = "2") Integer section) {
        return this.userMoneyV8Service.queryIncomeLog(merchantId, shopID, userID, offset, size, type, section);
    }

    /***
     * 查询交易明细
     * @param merchantID
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @return
     */
    @RequestMapping(path = "/queryTradeDetail", method = RequestMethod.GET)
    public SimpleResponse queryTradeDetail(@RequestParam(name = "merchantId") final Long merchantID,
                                           @RequestParam(name = "shopId") final Long shopID,
                                           @RequestParam(name = "userId") final Long userID,
                                           @RequestParam(name = "pageLastId", required = false) final Long pageLastID,
                                           @RequestParam(name = "limit", required = false, defaultValue = "10") int size,
                                           @RequestParam(name = "offset", required = false, defaultValue = "0") int offset) {
        return this.userMoneyV8Service.queryTradeDetail(merchantID, shopID, userID, offset, size, pageLastID);
    }
}
