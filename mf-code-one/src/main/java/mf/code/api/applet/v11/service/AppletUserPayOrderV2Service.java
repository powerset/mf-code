package mf.code.api.applet.v11.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.UserOrderReqDTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * mf.code.api.applet.v11.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月25日 17:17
 */
public interface AppletUserPayOrderV2Service {

    /***
     * 支付创建
     *
     * @param reqDTO
     * @return
     */
    SimpleResponse createUpayWxOrder(UserOrderReqDTO reqDTO);

    /***
     * 申请退款
     *
     * @return
     */
    SimpleResponse applyRefundUpayWxOrder();

    /***
     * 支付回调
     *
     * @param orderNo 订单编号
     * @param transactionId 交易流水
     * @param totalFee 支付金额
     * @param paymentTime 支付时间
     * @param payOpenId 支付人标识
     * @return
     */
    String wxPayCallback(String orderNo, String transactionId, BigDecimal totalFee, Date paymentTime, String payOpenId);

    /***
     * 退款回调
     *
     * @param orderNo
     * @param refundOrderNo
     * @param totalFee
     * @param settlementRefundFee
     * @param transactionId
     * @return
     */
    String wxRefundCallback(String orderNo, String refundOrderNo, BigDecimal totalFee, BigDecimal settlementRefundFee, String transactionId);

    /***
     * 查询订单的状态
     *
     * @param orderKeyID
     * @return
     */
    SimpleResponse queryOrderStatus(Long orderKeyID);
}
