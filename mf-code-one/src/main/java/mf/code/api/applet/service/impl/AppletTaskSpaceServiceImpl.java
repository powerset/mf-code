package mf.code.api.applet.service.impl;

import mf.code.activity.constant.*;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityCommissionService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.TaskSpaceNodeDto;
import mf.code.api.applet.service.AppletTaskSpaceService;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.constant.*;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * mf.code.api.applet.service.impl
 * Description: 任务空间
 *
 * @author: gel
 * @date: 2018-12-26 13:59
 */
@Service
public class AppletTaskSpaceServiceImpl implements AppletTaskSpaceService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private ActivityCommissionService activityCommissionService;

    /**
     * 查询任务空间列表
     * <p>
     * <view wx:if='{{ item.type === 1 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">回填订单拿红包</view>
     * <view class="taskspace-item-taskinfo">回填已购商品订单号即可拿红包</view>
     * </view>
     * <view wx:if='{{ item.type === 2 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">玩一次幸运大转盘</view>
     * <view class="taskspace-item-taskinfo">转动大转盘有机会领取10000元奖金</view>
     * </view>
     * <view wx:if='{{ item.type === 9 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">绑定手机号</view>
     * <view class="taskspace-item-taskinfo">绑定手机号可以解锁提现等功能</view>
     * </view>
     * <view wx:if='{{ item.type === 10 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">领取一次店铺赠品</view>
     * <view class="taskspace-item-taskinfo">完成一定数量好友邀请即可免费领取</view>
     * </view>
     * <view wx:if='{{ item.type === 11 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">发起一次抽奖</view>
     * <view class="taskspace-item-taskinfo">对已购商品发起抽奖可以获得免单机会</view>
     * </view>
     * <view wx:if='{{ item.type === 12 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">参加一次抽奖</view>
     * <view class="taskspace-item-taskinfo">邀请越多好友抽中免单大奖几率越高</view>
     * </view>
     * <view wx:if='{{ item.type === 13 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">商品好评晒图</view>
     * <view class="taskspace-item-taskinfo">对本店已购商品好评晒图即可拿红包</view>
     * </view>
     * <view wx:if='{{ item.type === 14 }}' class="taskspace-item-info">
     * <view class="taskspace-item-task">商品收藏加购</view>
     * <view class="taskspace-item-taskinfo">对本店商品进行收藏加购即可拿红包</view>
     * </view>
     *
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryTaskSpaceList(Long shopId, Long userId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        // 数据redis缓存，暂时不加
//        String taskSpaceCacheKey = RedisKeyConstant.TASK_SPACE_LIST + userId;
//        String taskSpaceValue = stringRedisTemplate.opsForValue().get(taskSpaceCacheKey);
//        if (StringUtils.isNotBlank(taskSpaceValue)) {
//            JSONArray taskSpaceList = JSONArray.parseArray(taskSpaceValue);
//            simpleResponse.setData(taskSpaceList);
//            return simpleResponse;
//        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("店铺数据错误");
            return simpleResponse;
        }
        List<TaskSpaceNodeDto> taskSpaceList = new ArrayList<>();
        // 一、是否注册手机号 2019-03-20删除注册手机号任务，因为每日任务需要关联
//        checkTaskBindPhone(userId, taskSpaceList);
        // 二、领取店铺赠品，该用户在shopId店铺下，成功完成领奖的
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("merchantId", merchantShop.getMerchantId());
        paramMap.put("shopId", shopId);
        paramMap.put("typeList", Arrays.asList(ActivityDefTypeEnum.NEW_MAN.getCode(), ActivityDefTypeEnum.ASSIST.getCode()));
        paramMap.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        List<ActivityDef> activityDefList = activityDefService.selectByParams(paramMap);
        if (!CollectionUtils.isEmpty(activityDefList)) {
            boolean hasTask = false;
            for (ActivityDef activityDef : activityDefList) {
                if (activityDef.getStock() > 0) {
                    hasTask = true;
                    break;
                }
            }
            if (hasTask) {
                // 添加排序,修改时间倒叙
                paramMap.clear();
                paramMap.put("merchantId", merchantShop.getMerchantId());
                paramMap.put("shopId", shopId);
                paramMap.put("userId", userId);
                paramMap.put("status", UserTaskStatusEnum.AWARD_SUCCESS.getCode());
                paramMap.put("types", Arrays.asList(UserTaskTypeEnum.ASSIST_START.getCode(),
                        UserTaskTypeEnum.NEW_MAN_START.getCode(), UserTaskTypeEnum.NEW_MAN_JOIN.getCode()));
                int userTaskCount = userTaskService.countUserTask(paramMap);
                TaskSpaceNodeDto taskSpaceGift = new TaskSpaceNodeDto();
                taskSpaceGift.setType(TaskSpaceTypeEnum.RECEIVE_GIFT.getType());
                taskSpaceGift.setTag(TaskSpaceTypeEnum.RECEIVE_GIFT.getTag());
                taskSpaceGift.setHitNum(userTaskCount);
                taskSpaceGift.setLastTime(0L);
                if (userTaskCount > 0) {
                    paramMap.put("order", "utime");
                    paramMap.put("direct", "desc");
                    paramMap.put("limit", 1);
                    paramMap.put("offset", 0);
                    List<UserTask> query = userTaskService.query(paramMap);
                    taskSpaceGift.setLastTime(query.size() == 0 ? 0L : query.get(0).getUtime().getTime());
                }
                taskSpaceGift.setTitle("领取一次店铺赠品");
                taskSpaceGift.setExplain("完成一定数量好友邀请即可免费领取");
                taskSpaceList.add(taskSpaceGift);
            }
        }

        // 三、轮盘抽奖,轮盘有库存，保证金充足才显示
        Map<String, Object> luckyWheelParams = new HashMap<String, Object>();
        luckyWheelParams.put("merchantId", merchantShop.getMerchantId());
        luckyWheelParams.put("shopId", shopId);
        luckyWheelParams.put("type", ActivityDefTypeEnum.LUCKY_WHEEL.getCode());
        luckyWheelParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        luckyWheelParams.put("del", DelEnum.NO.getCode());
        List<ActivityDef> luckyActivityDefList = this.activityDefService.selectByParams(luckyWheelParams);
        if (!CollectionUtils.isEmpty(luckyActivityDefList)) {
            // 判断条件添加活动是否下架
            ActivityDef activityDef = luckyActivityDefList.get(0);
            if (activityDef != null && activityDef.getStatus() == ActivityDefStatusEnum.PUBLISHED.getCode()
                    && activityDef.getStock() > 0 && activityDef.getDeposit().compareTo(new BigDecimal("1")) >= 0) {
                luckyWheelParams.clear();
                luckyWheelParams.put("merchantId", merchantShop.getMerchantId());
                luckyWheelParams.put("shopId", shopId);
                luckyWheelParams.put("userId", userId);
                luckyWheelParams.put("type", UserCouponTypeEnum.LUCKY_WHEEL.getCode());
                luckyWheelParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
                luckyWheelParams.put("order", "id");
                luckyWheelParams.put("direct", "desc");
                List<UserCoupon> userCouponList = userCouponService.listPageByparams(luckyWheelParams);
                TaskSpaceNodeDto taskSpaceWheel = new TaskSpaceNodeDto();
                taskSpaceWheel.setType(TaskSpaceTypeEnum.LUCK_WHEEL.getType());
                taskSpaceWheel.setTag(TaskSpaceTypeEnum.LUCK_WHEEL.getTag());
                if (CollectionUtils.isEmpty(userCouponList)) {
                    taskSpaceWheel.setTotalAmount(BigDecimal.ZERO);
                    taskSpaceWheel.setLastTime(0L);
                } else {
                    BigDecimal wheelAmount = BigDecimal.ZERO;
                    for (UserCoupon userCoupon : userCouponList) {
                        wheelAmount = wheelAmount.add(userCoupon.getAmount());
                    }
                    taskSpaceWheel.setTotalAmount(wheelAmount.setScale(2, RoundingMode.HALF_UP));
                    taskSpaceWheel.setLastTime(userCouponList.get(0).getUtime().getTime());
                }
                taskSpaceWheel.setTitle("玩一次幸运大转盘");
                taskSpaceWheel.setExplain("转动大转盘有机会领取10000元奖金");
                // 清空查询条件
                luckyWheelParams.clear();
                luckyWheelParams.put("activityDefId", activityDef.getId());
                List<Activity> byDefId = activityService.findPage(luckyWheelParams);
                if (!CollectionUtils.isEmpty(byDefId)) {
                    taskSpaceWheel.setActivityId(Long.valueOf(byDefId.get(0).getId()));
                }
                taskSpaceList.add(taskSpaceWheel);
            }
        }
        // 四、0.1元发起抽奖
        Map<String, Object> paramMap4 = new HashMap<>();
        paramMap4.put("merchantId", merchantShop.getMerchantId());
        paramMap4.put("shopId", shopId);
        paramMap4.put("type", ActivityDefTypeEnum.ORDER_BACK.getCode());
        paramMap4.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        paramMap4.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefList4 = activityDefService.selectByParams(paramMap4);
        if (!CollectionUtils.isEmpty(activityDefList4)) {
            boolean hasTask = false;
            for (ActivityDef activityDef : activityDefList4) {
                if (activityDef.getStock() > 0) {
                    hasTask = true;
                    break;
                }
            }
            if (hasTask) {
                Map<String, Object> activityParams = new HashMap<>();
                activityParams.put("merchantId", merchantShop.getMerchantId());
                activityParams.put("shopId", shopId);
                activityParams.put("types", Arrays.asList(UserActivityTypeEnum.ORDER_BACK_START.getCode(), UserActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
                activityParams.put("userId", userId);
                activityParams.put("order", "id");
                activityParams.put("direct", "desc");
                List<UserActivity> userActivityList = userActivityService.pageListUserActivity(activityParams);
                TaskSpaceNodeDto taskSpaceActivity = new TaskSpaceNodeDto();
                taskSpaceActivity.setType(TaskSpaceTypeEnum.LAUNCH_LUCKY.getType());
                taskSpaceActivity.setTag(TaskSpaceTypeEnum.LAUNCH_LUCKY.getTag());
                if (CollectionUtils.isEmpty(userActivityList)) {
                    taskSpaceActivity.setHitNum(0);
                    taskSpaceActivity.setLastTime(0L);
                } else {
                    taskSpaceActivity.setJoinNum(userActivityList.size());
                    int count = 0;
                    for (UserActivity userActivity : userActivityList) {
                        if (UserActivityStatusEnum.WINNER.getCode() == userActivity.getStatus()) {
                            count++;
                        }
                    }
                    taskSpaceActivity.setHitNum(count);
                    taskSpaceActivity.setLastTime(userActivityList.get(0).getUtime().getTime());
                }
                taskSpaceActivity.setTitle("发起一次抽奖");
                taskSpaceActivity.setExplain("对已购商品发起抽奖可以获得免单机会");
                taskSpaceList.add(taskSpaceActivity);
            }
        }

        // 五、参与一次商品抽奖活动
        Map<String, Object> paramMap5 = new HashMap<>();
        paramMap5.put("merchantId", merchantShop.getMerchantId());
        paramMap5.put("shopId", shopId);
        paramMap5.put("type", ActivityDefTypeEnum.ORDER_BACK.getCode());
        paramMap5.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        paramMap5.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefList5 = activityDefService.selectByParams(paramMap5);
        if (!CollectionUtils.isEmpty(activityDefList5)) {
            List<Long> activityDefIds = new ArrayList<>();
            for (ActivityDef activityDef : activityDefList5) {
                activityDefIds.add(activityDef.getId());
            }
            Map<String, Object> params = new HashMap<>();
            params.put("activityDefIds", activityDefIds);
            params.put("status", ActivityStatusEnum.PUBLISHED.getCode());
            List<Activity> page = activityService.findPage(params);
            // 用来判断是否展示
            boolean containDef = false;
            for (ActivityDef activityDef : activityDefList5) {
                if (activityDef.getStock() <= 0) {
                    if (CollectionUtils.isEmpty(page)) {
                        containDef = false;
                        continue;
                    }
                    for (Activity activity : page) {
                        if (activityDef.getId().equals(activity.getActivityDefId())) {
                            containDef = true;
                        }
                    }
                } else {
                    containDef = true;
                }
            }
            if (containDef) {
                Map<String, Object> joinLuckyParams = new HashMap<>();
                joinLuckyParams.put("merchantId", merchantShop.getMerchantId());
                joinLuckyParams.put("shopId", shopId);
                joinLuckyParams.put("userId", userId);
                joinLuckyParams.put("types", Arrays.asList(UserActivityTypeEnum.ORDER_BACK_JOIN.getCode(),
                        UserActivityTypeEnum.ORDER_BACK_START.getCode(),UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode(),
                        UserActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
                joinLuckyParams.put("order", "utime");
                joinLuckyParams.put("direct", "desc");
                List<UserActivity> userActivityList1 = userActivityService.pageListUserActivity(joinLuckyParams);
                TaskSpaceNodeDto taskSpaceActivity1 = new TaskSpaceNodeDto();
                taskSpaceActivity1.setType(TaskSpaceTypeEnum.JOIN_LUCKY.getType());
                taskSpaceActivity1.setTag(TaskSpaceTypeEnum.JOIN_LUCKY.getTag());
                if (CollectionUtils.isEmpty(userActivityList1)) {
                    taskSpaceActivity1.setHitNum(0);
                    taskSpaceActivity1.setLastTime(0L);
                } else {
                    taskSpaceActivity1.setJoinNum(userActivityList1.size());
                    int count = 0;
                    for (UserActivity userActivity : userActivityList1) {
                        if (UserActivityStatusEnum.WINNER.getCode() == userActivity.getStatus()) {
                            count++;
                        }
                    }
                    taskSpaceActivity1.setHitNum(count);
                    taskSpaceActivity1.setLastTime(userActivityList1.get(0).getUtime().getTime());
                }
                taskSpaceActivity1.setTitle("参加一次抽奖");
                taskSpaceActivity1.setExplain("邀请越多好友抽中免单大奖几率越高");
                taskSpaceList.add(taskSpaceActivity1);
            }
        }
        // 六、试用 TODO 二期需求没有

        // 七、回填订单，八、好评晒图，九、收藏加购
        Map<String, Object> goodsCommentTaskParams = new HashMap<>();
        goodsCommentTaskParams.put("merchantId", merchantShop.getMerchantId());
        goodsCommentTaskParams.put("shopId", shopId);
        goodsCommentTaskParams.put("typeList", Arrays.asList(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(),
                ActivityDefTypeEnum.FAV_CART.getCode(), ActivityDefTypeEnum.GOOD_COMMENT.getCode()));
        goodsCommentTaskParams.put("status", ActivityTaskStatusEnum.PUBLISHED.getCode());
        goodsCommentTaskParams.put("depositStatus", DepositStatusEnum.PAYMENT.getCode());
        goodsCommentTaskParams.put("del", DelEnum.NO.getCode());
        goodsCommentTaskParams.put("order", "id");
        goodsCommentTaskParams.put("direct", "desc");
        List<ActivityDef> activityDefs = activityDefService.selectByParams(goodsCommentTaskParams);
        if (!CollectionUtils.isEmpty(activityDefs)) {
            boolean goodsComment = false;
            boolean fillBack = false;
            boolean favcart = false;
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getType().equals(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())) {
                    if (activityDef.getDeposit().compareTo(BigDecimal.ZERO) <= 0) {
                        continue;
                    }
                } else if (activityDef.getStock() <= 0) {
                    continue;
                }
                if (activityDef.getType() == ActivityTaskTypeEnum.GOOD_COMMENT.getCode()) {
                    if (goodsComment) {
                        continue;
                    }
                    Map<String, Object> goodsCommentParams = new HashMap<>();
                    goodsCommentParams.put("merchantId", merchantShop.getMerchantId());
                    goodsCommentParams.put("shopId", shopId);
                    goodsCommentParams.put("userId", userId);
                    goodsCommentParams.put("type", UserCouponTypeEnum.GOODS_COMMENT.getCode());
                    goodsCommentParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
                    goodsCommentParams.put("order", "id");
                    goodsCommentParams.put("direct", "desc");
                    List<UserCoupon> userCouponList = userCouponService.listPageByparams(goodsCommentParams);

                    // TODO 待优化
                    TaskSpaceNodeDto taskSpaceComment = new TaskSpaceNodeDto();
                    taskSpaceComment.setType(TaskSpaceTypeEnum.GOOD_COMMENT.getType());
                    taskSpaceComment.setTag(TaskSpaceTypeEnum.GOOD_COMMENT.getTag());
                    if (CollectionUtils.isEmpty(userCouponList)) {
                        taskSpaceComment.setTotalAmount(BigDecimal.ZERO);
                        taskSpaceComment.setAmount(activityDef.getGoodsPrice());
                        taskSpaceComment.setLastTime(0L);
                    } else {
                        BigDecimal wheelAmount = BigDecimal.ZERO;
                        for (UserCoupon userCoupon : userCouponList) {
                            wheelAmount = wheelAmount.add(userCoupon.getAmount());
                        }
                        taskSpaceComment.setTotalAmount(wheelAmount.setScale(2, RoundingMode.HALF_UP));
                        taskSpaceComment.setAmount(activityDef.getGoodsPrice());
                        taskSpaceComment.setLastTime(userCouponList.get(0).getUtime().getTime());
                    }
                    taskSpaceComment.setTitle("商品好评晒图");
                    taskSpaceComment.setExplain("对本店已购商品好评晒图即可拿红包");
                    taskSpaceList.add(taskSpaceComment);
                    goodsComment = true;
                }
                if (activityDef.getType() == ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode()) {
                    if (fillBack) {
                        continue;
                    }
                    Map<String, Object> goodsCommentParams = new HashMap<>();
                    goodsCommentParams.put("merchantId", merchantShop.getMerchantId());
                    goodsCommentParams.put("shopId", shopId);
                    goodsCommentParams.put("userId", userId);
                    goodsCommentParams.put("type", UserCouponTypeEnum.ORDER_RED_PACKET.getCode());
                    goodsCommentParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
                    goodsCommentParams.put("order", "id");
                    goodsCommentParams.put("direct", "desc");
                    List<UserCoupon> userCouponList = userCouponService.listPageByparams(goodsCommentParams);

                    // TODO 待优化
                    TaskSpaceNodeDto taskSpaceOrderBack = new TaskSpaceNodeDto();
                    taskSpaceOrderBack.setType(TaskSpaceTypeEnum.ORDER_BACK.getType());
                    taskSpaceOrderBack.setTag(TaskSpaceTypeEnum.ORDER_BACK.getTag());
                    if (CollectionUtils.isEmpty(userCouponList)) {
                        taskSpaceOrderBack.setTotalAmount(BigDecimal.ZERO);
                        taskSpaceOrderBack.setAmount(activityDef.getGoodsPrice());
                        taskSpaceOrderBack.setLastTime(0L);
                    } else {
                        BigDecimal wheelAmount = BigDecimal.ZERO;
                        for (UserCoupon userCoupon : userCouponList) {
                            wheelAmount = wheelAmount.add(userCoupon.getAmount());
                        }
                        taskSpaceOrderBack.setTotalAmount(wheelAmount.setScale(2, RoundingMode.HALF_UP));
                        taskSpaceOrderBack.setAmount(activityDef.getGoodsPrice());
                        taskSpaceOrderBack.setLastTime(userCouponList.get(0).getUtime().getTime());
                    }
                    taskSpaceOrderBack.setTitle("回填订单拿红包");
                    taskSpaceOrderBack.setExplain("回填已购商品订单号即可拿红包");
                    taskSpaceList.add(taskSpaceOrderBack);
                    fillBack = true;
                }
                if (activityDef.getType() == ActivityTaskTypeEnum.FAVCART.getCode()) {
                    if (favcart) {
                        continue;
                    }
                    Map<String, Object> goodsCommentParams = new HashMap<>();
                    goodsCommentParams.put("merchantId", merchantShop.getMerchantId());
                    goodsCommentParams.put("shopId", shopId);
                    goodsCommentParams.put("userId", userId);
                    goodsCommentParams.put("type", UserCouponTypeEnum.FAV_CART.getCode());
                    goodsCommentParams.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
                    goodsCommentParams.put("order", "id");
                    goodsCommentParams.put("direct", "desc");
                    List<UserCoupon> userCouponList = userCouponService.listPageByparams(goodsCommentParams);

                    // TODO 待优化
                    TaskSpaceNodeDto taskSpaceFavCart = new TaskSpaceNodeDto();
                    taskSpaceFavCart.setType(TaskSpaceTypeEnum.FAV_CART.getType());
                    taskSpaceFavCart.setTag(TaskSpaceTypeEnum.FAV_CART.getTag());
                    if (CollectionUtils.isEmpty(userCouponList)) {
                        taskSpaceFavCart.setTotalAmount(BigDecimal.ZERO);
                        taskSpaceFavCart.setAmount(activityDef.getGoodsPrice());
                        taskSpaceFavCart.setLastTime(0L);
                    } else {
                        BigDecimal wheelAmount = BigDecimal.ZERO;
                        for (UserCoupon userCoupon : userCouponList) {
                            wheelAmount = wheelAmount.add(userCoupon.getAmount());
                        }
                        taskSpaceFavCart.setTotalAmount(wheelAmount.setScale(2, RoundingMode.HALF_UP));
                        taskSpaceFavCart.setAmount(activityDef.getGoodsPrice());
                        taskSpaceFavCart.setLastTime(userCouponList.get(0).getUtime().getTime());
                    }
                    taskSpaceFavCart.setTitle("商品收藏加购");
                    taskSpaceFavCart.setExplain("对本店商品进行收藏加购即可拿红包");
                    taskSpaceList.add(taskSpaceFavCart);
                    favcart = true;
                }

            }
        }
        // 十、拆红包
        Map<String, Object> paramMap10 = new HashMap<>();
        paramMap10.put("merchantId", merchantShop.getMerchantId());
        paramMap10.put("shopId", shopId);
        paramMap10.put("type", ActivityDefTypeEnum.OPEN_RED_PACKET.getCode());
        paramMap10.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        paramMap10.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefList10 = activityDefService.selectByParams(paramMap10);
        if (!CollectionUtils.isEmpty(activityDefList10)) {
            boolean hasStock = false;
            Long activityDefId = null;
            BigDecimal redPackageAmount = BigDecimal.ZERO;
            for (ActivityDef activityDef : activityDefList10) {
                if (activityDef.getStock() > 0) {
                    activityDefId = activityDef.getId();
                    hasStock = true;
                    redPackageAmount = activityDef.getGoodsPrice();
                    break;
                }
            }
            if (hasStock) {
                Map<String, Object> activityParams = new HashMap<>();
                activityParams.put("merchantId", merchantShop.getMerchantId());
                activityParams.put("shopId", shopId);
                activityParams.put("activityDefId", activityDefId);
                activityParams.put("typeList", Arrays.asList(UserActivityTypeEnum.OPEN_RED_PACKET_START.getCode(),
                        UserActivityTypeEnum.OPEN_RED_PACKET_JOIN.getCode()));
                activityParams.put("userId", userId);
                activityParams.put("order", "id");
                activityParams.put("direct", "desc");
                List<UserActivity> userActivityList = userActivityService.pageListUserActivity(activityParams);
                TaskSpaceNodeDto taskSpaceActivity = new TaskSpaceNodeDto();
                taskSpaceActivity.setActivityDefId(activityDefId);
                taskSpaceActivity.setType(TaskSpaceTypeEnum.OPEN_RED_PACKET.getType());
                taskSpaceActivity.setTag(TaskSpaceTypeEnum.OPEN_RED_PACKET.getTag());
                taskSpaceActivity.setAmount(activityDefList10.get(0).getGoodsPrice());
                taskSpaceActivity.setTotalAmount(BigDecimal.ZERO);
                taskSpaceActivity.setHitNum(0);
                taskSpaceActivity.setLastTime(0L);
                if (!CollectionUtils.isEmpty(userActivityList)) {
                    taskSpaceActivity.setJoinNum(userActivityList.size());
                    int count = 0;
                    for (UserActivity userActivity : userActivityList) {
                        if (UserActivityStatusEnum.WINNER.getCode() == userActivity.getStatus()) {
                            count++;
                        }
                        // 判断用户活动表，类型拆红包发起，状态为未中奖，则认为存在进行中活动
                        if (userActivity.getType() == UserActivityTypeEnum.OPEN_RED_PACKET_START.getCode()) {
                            if (taskSpaceActivity.getActivityId() == null) {
                                taskSpaceActivity.setActivityId(userActivity.getActivityId());
                            }
                        }
                    }
                    taskSpaceActivity.setHitNum(count);
                    taskSpaceActivity.setTotalAmount(redPackageAmount.multiply(new BigDecimal(count)));
                    taskSpaceActivity.setLastTime(userActivityList.get(0).getUtime().getTime());
                }
                taskSpaceActivity.setTitle("拆红包");
                taskSpaceActivity.setExplain("拆红包测试文字");
                taskSpaceList.add(taskSpaceActivity);
            }
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("list", taskSpaceList);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 校验是否需要手机号任务
     *
     * @param userId
     * @param taskSpaceList
     */
    private void checkTaskBindPhone(Long userId, List<TaskSpaceNodeDto> taskSpaceList) {
        User user = userService.selectByPrimaryKey(userId);
        if (!StringUtils.isNumeric(user.getMobile())) {
            TaskSpaceNodeDto taskSpaceNodeDto = new TaskSpaceNodeDto();
            taskSpaceNodeDto.setType(TaskSpaceTypeEnum.BIND_PHONE.getType());
            taskSpaceNodeDto.setTag(TaskSpaceTypeEnum.BIND_PHONE.getTag());
            taskSpaceNodeDto.setHitNum(0);
            taskSpaceNodeDto.setLastTime(Long.MAX_VALUE);
            taskSpaceNodeDto.setTitle("绑定手机号");
            taskSpaceNodeDto.setExplain("绑定手机号可以解锁提现等功能");
            taskSpaceList.add(taskSpaceNodeDto);
        }
    }


    @Override
    public SimpleResponse pageHistory(Long shopId, Long userId, Integer offset, Integer limit) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (userId == null || userId == 0 || shopId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("入参不满足条件");
            return simpleResponse;
        }

        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("该店铺不存在");
            return simpleResponse;
        }

        Map<String, Object> userActivityParams = new HashMap<>(5);
        userActivityParams.put("merchantId", merchantShop.getMerchantId());
        userActivityParams.put("shopId", shopId);
        userActivityParams.put("userId", userId);
        userActivityParams.put("types", UserActivityTypeEnum.findTypeForTaskHistory());
        userActivityParams.put("order", "utime");
        userActivityParams.put("direct", "desc");
        int countAwardNum = this.userActivityService.countUserActivity(userActivityParams);
        userActivityParams.put("offset", offset);
        userActivityParams.put("size", limit);
        List<UserActivity> userActivity = this.userActivityService.pageListUserActivity(userActivityParams);
        Map<String, Object> respMap = new HashMap<>(4);
        respMap.put("offset", offset);
        respMap.put("limit", limit);
        respMap.put("total", countAwardNum);
        respMap.put("content", new ArrayList<>());
        simpleResponse.setData(respMap);
        if (userActivity == null || userActivity.size() == 0) {
            return simpleResponse;
        }
        List<Long> goodsIDs = new ArrayList<>();
        List<Long> activityIds = new ArrayList<>();
        List<Long> userActivityIds = new ArrayList<>();
        List<Long> activityTaskIds = new ArrayList<>();
        List<Long> activityDefIds = new ArrayList<>();
        for (UserActivity userActivity1 : userActivity) {
            if (userActivity1.getGoodsId() != null && userActivity1.getGoodsId() > 0) {
                goodsIDs.add(userActivity1.getGoodsId());
            }
            if (userActivity1.getActivityId() != null && userActivity1.getActivityId() > 0) {
                activityIds.add(userActivity1.getActivityId());
            }
            userActivityIds.add(userActivity1.getId());
            if (userActivity1.getActivityTaskId() != null && userActivity1.getActivityTaskId() > 0) {
                activityTaskIds.add(userActivity1.getActivityTaskId());
            }
            if (userActivity1.getActivityDefId() != null && activityDefIds.indexOf(userActivity1.getActivityDefId()) == -1) {
                activityDefIds.add(userActivity1.getActivityDefId());
            }
        }
        Map<Long, UserTask> userTaskMap = queryUserTaskInfo(merchantShop.getMerchantId(), shopId, userId, activityIds);
        Map<Long, UserTask> userTaskByUActivityMap = queryuserTaskByUActivityInfo(merchantShop.getMerchantId(), shopId, userId, userActivityIds);
        Map<Long, UserTask> userTaskByATaskMap = queryuserTaskByATaskMapInfo(merchantShop.getMerchantId(), shopId, userId, activityTaskIds);
        Map<Long, Goods> goodsMap = goodsService.queryGoodsInfo(goodsIDs);
        List<Activity> activities = queryActivityInfo(merchantShop.getMerchantId(), shopId, userId, activityIds);
        Map<Long, Activity> activityMap = new HashMap<>();
        for (Activity activity : activities) {
            activityMap.put(activity.getId(), activity);
            if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0 && activityDefIds.indexOf(activity.getActivityDefId()) == -1) {
                activityDefIds.add(activity.getActivityDefId());
            }
        }
        Map<Long, ActivityDef> activityDefMap = getActivityDefMap(activities, activityDefIds);
        Map<Long, ActivityDef> activityDefByDefMap = getActivityDefByDefMap(activityDefIds);
        Map<Long, ActivityTask> activityTaskMap = getActivityTaskMap(activityTaskIds);
        List<Object> list = new ArrayList<>();
        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        for (UserActivity userActivity1 : userActivity) {
            Map<String, Object> map = new HashMap<>();
            ActivityDef activityDef = null;
            ActivityTask activityTask = null;
            if (userActivity1.getActivityId() != null && userActivity1.getActivityId() > 0) {
                activityDef = activityDefMap.get(userActivity1.getActivityId());
            }
            if (userActivity1.getActivityTaskId() != null && userActivity1.getActivityTaskId() > 0) {
                activityTask = activityTaskMap.get(userActivity1.getActivityTaskId());
            }
            if (userActivity1.getActivityDefId() != null && userActivity1.getActivityDefId() > 0) {
                activityDef = activityDefByDefMap.get(userActivity1.getActivityDefId());
            }
            if (activityDef != null) {
                BigDecimal amount = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
                BigDecimal tax = amount.multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
                map.put("commission", amount.subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
                if (activityDef.getType().equals(ActivityDefTypeEnum.OPEN_RED_PACKET.getCode())) {
                    map.put("commission", activityDef.getCommission().setScale(2, BigDecimal.ROUND_DOWN).toString());
                }
            }
            if (activityTask != null) {
                BigDecimal tax = activityTask.getRpAmount().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
                map.put("commission", activityTask.getRpAmount().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
            }
            Goods goods = goodsMap.get(userActivity1.getGoodsId());
            if (goods != null) {
                map.put("picUrl", ossCaller.cdnReplace(goods.getPicUrl()));
                map.put("title", goods.getXxbtopTitle());
            }
            map.put("startTime", DateFormatUtils.format(userActivity1.getCtime(), "yyyy.MM.dd HH:mm"));
            map.put("activityId", userActivity1.getActivityId());
            map.put("userId", userActivity1.getUserId());
            map.put("activityDefId", userActivity1.getActivityDefId());
            UserTask userTask = null;
            Activity activity = null;
            if (userActivity1.getActivityId() != null && userActivity1.getActivityId() > 0) {
                userTask = userTaskMap.get(userActivity1.getActivityId());
                activity = activityMap.get(userActivity1.getActivityId());
            } else {
                userTask = userTaskByUActivityMap.get(userActivity1.getId());
            }
            if (userTask == null) {
                userTask = userTaskByATaskMap.get(userActivity1.getActivityTaskId());
            }
            map.put("goodsId", userActivity1.getGoodsId());
            if (userActivity1.getGoodsId() == null && userTask != null) {
                map.put("goodsId", userTask.getGoodsId());
            }
            changeTypeAndTagAndStatus(userActivity1, userTask, activity, map);
            list.add(map);
        }
        respMap.put("content", list);
        simpleResponse.setData(respMap);
        return simpleResponse;
    }

    private Map<Long, ActivityDef> getActivityDefMap(List<Activity> activities, List<Long> activityDefIds) {
        Map<Long, ActivityDef> activityDefMap = new HashMap<>();
        if (CollectionUtils.isEmpty(activityDefIds)) {
            return activityDefMap;
        }
        List<ActivityDef> activityDefs = (List<ActivityDef>) this.activityDefService.listByIds(activityDefIds);
        if (CollectionUtils.isEmpty(activityDefs)) {
            return activityDefMap;
        }
        for (ActivityDef activityDef : activityDefs) {
            activityDefMap.put(activityDef.getId(), activityDef);
        }
        Map<Long, ActivityDef> respMap = new HashMap<>();
        for (Activity activity : activities) {
            ActivityDef activityDef = activityDefMap.get(activity.getActivityDefId());
            if (activityDef != null) {
                respMap.put(activity.getId(), activityDef);
            }
        }
        return respMap;
    }

    private Map<Long, ActivityDef> getActivityDefByDefMap(List<Long> activityDefIds) {
        Map<Long, ActivityDef> activityDefMap = new HashMap<>();
        if (CollectionUtils.isEmpty(activityDefIds)) {
            return activityDefMap;
        }
        List<ActivityDef> activityDefs = (List<ActivityDef>) this.activityDefService.listByIds(activityDefIds);
        if (CollectionUtils.isEmpty(activityDefs)) {
            return activityDefMap;
        }
        for (ActivityDef activityDef : activityDefs) {
            activityDefMap.put(activityDef.getId(), activityDef);
        }
        return activityDefMap;
    }

    private Map<Long, ActivityTask> getActivityTaskMap(List<Long> activityTaskIds) {
        if (CollectionUtils.isEmpty(activityTaskIds)) {
            return new HashMap<>();
        }
        Map<Long, ActivityTask> activityTaskMap = new HashMap<>();
        List<ActivityTask> activityTasks = (List<ActivityTask>) this.activityTaskService.listByIds(activityTaskIds);
        if (!CollectionUtils.isEmpty(activityTaskIds)) {
            for (ActivityTask activityTask : activityTasks) {
                activityTaskMap.put(activityTask.getId(), activityTask);
            }
        }
        return activityTaskMap;
    }

    private List<Activity> queryActivityInfo(Long merchantId, Long shopId, Long userId, List<Long> activityIds) {
        if (CollectionUtils.isEmpty(activityIds)) {
            return new ArrayList<>();
        }
        //开奖时间
        Map<String, Object> activityParams = new HashMap<>(5);
        activityParams.put("merchantId", merchantId);
        activityParams.put("shopId", shopId);
        activityParams.put("activityIds", activityIds);
        List<Activity> activityList = activityService.findPage(activityParams);
        return activityList;
    }

    private Map<Long, UserTask> queryuserTaskByATaskMapInfo(Long merchantId, Long shopId, Long userId, List<Long> userActivityIds) {
        if (CollectionUtils.isEmpty(userActivityIds)) {
            return new HashMap<>();
        }
        //开奖时间
        Map<String, Object> activityParams = new HashMap<>(5);
        activityParams.put("merchantId", merchantId);
        activityParams.put("shopId", shopId);
        activityParams.put("userId", userId);
        activityParams.put("activityTaskIds", userActivityIds);
        List<UserTask> userTaskList = userTaskService.listPageByParams(activityParams);
        if (CollectionUtils.isEmpty(userTaskList)) {
            return new HashMap<>();
        }
        Map<Long, UserTask> activityMap = new HashMap<>();
        for (UserTask userTask : userTaskList) {
            activityMap.put(userTask.getActivityTaskId(), userTask);
        }
        return activityMap;
    }

    private Map<Long, UserTask> queryuserTaskByUActivityInfo(Long merchantId, Long shopId, Long userId, List<Long> userActivityIds) {
        if (CollectionUtils.isEmpty(userActivityIds)) {
            return new HashMap<>();
        }
        //开奖时间
        Map<String, Object> activityParams = new HashMap<>(5);
        activityParams.put("merchantId", merchantId);
        activityParams.put("shopId", shopId);
        activityParams.put("userId", userId);
        activityParams.put("userActivityIds", userActivityIds);
        List<UserTask> userTaskList = userTaskService.listPageByParams(activityParams);
        if (CollectionUtils.isEmpty(userTaskList)) {
            return new HashMap<>();
        }
        Map<Long, UserTask> activityMap = new HashMap<>();
        for (UserTask userTask : userTaskList) {
            activityMap.put(userTask.getUserActivityId(), userTask);
        }
        return activityMap;
    }

    /***
     * 获取用户任务状态
     * @param activityIds
     * @return
     */
    private Map<Long, UserTask> queryUserTaskInfo(Long merchantId, Long shopId, Long userId, List<Long> activityIds) {
        if (CollectionUtils.isEmpty(activityIds)) {
            return new HashMap<>();
        }
        //开奖时间
        Map<String, Object> activityParams = new HashMap<>(5);
        activityParams.put("merchantId", merchantId);
        activityParams.put("shopId", shopId);
        activityParams.put("userId", userId);
        activityParams.put("activityIds", activityIds);
        List<UserTask> userTaskList = userTaskService.listPageByParams(activityParams);
        if (CollectionUtils.isEmpty(userTaskList)) {
            return new HashMap<>(activityIds.size());
        }
        Map<Long, UserTask> activityMap = new HashMap<>();
        for (UserTask userTask : userTaskList) {
            activityMap.put(userTask.getActivityId(), userTask);
        }
        return activityMap;
    }

    @Override
    public SimpleResponse listSku(Long shopId, Long userId, Integer type, Integer offset, Integer limit) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("店铺数据错误");
            return simpleResponse;
        }
        Map<String, Object> resultMap = new HashMap<>();
        List<Long> goodsIdList = new ArrayList<>();
        List<Map<String, Object>> maps = new ArrayList<>();
        if (type == TaskSpaceTypeEnum.ORDER_BACK.getType() || type == TaskSpaceTypeEnum.FAV_CART.getType()
                || type == TaskSpaceTypeEnum.GOOD_COMMENT.getType()) {
            int taskType = ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode();
            if (type == TaskSpaceTypeEnum.FAV_CART.getType()) {
                taskType = ActivityTaskTypeEnum.FAVCART.getCode();
            }
            if (type == TaskSpaceTypeEnum.GOOD_COMMENT.getType()) {
                taskType = ActivityTaskTypeEnum.GOOD_COMMENT.getCode();
            }
            Map<String, Object> taskParams = new HashMap<>();
            taskParams.put("merchantId", merchantShop.getMerchantId());
            taskParams.put("shopId", shopId);
            taskParams.put("type", taskType);
            taskParams.put("status", ActivityTaskStatusEnum.PUBLISHED.getCode());
            taskParams.put("del", DelEnum.NO.getCode());
            taskParams.put("offset", offset);
            taskParams.put("limit", limit);
            List<ActivityTask> activityTasks = activityTaskService.selectByParams(taskParams);
            if (CollectionUtils.isEmpty(activityTasks)) {
                resultMap.put("list", new ArrayList<>());
                simpleResponse.setData(resultMap);
                return simpleResponse;
            }
            for (ActivityTask activityTask : activityTasks) {
                goodsIdList.add(activityTask.getGoodsId());
            }
            Map<String, Object> params = new HashMap<>();
            params.put("goodsIds", goodsIdList);
            List<Goods> goodsList = goodsService.pageList(params);
            for (ActivityTask activityTask : activityTasks) {
                if (activityTask.getRpStock() <= 0) {
                    continue;
                }
                Map<String, Object> goodsSkuMap = new HashMap<>();
                goodsSkuMap.put("goodsId", activityTask.getGoodsId());
                for (Goods goods : goodsList) {
                    if (activityTask.getGoodsId().equals(goods.getId())) {
                        goodsSkuMap.put("picUrl", goods.getPicUrl());
                        goodsSkuMap.put("displayGoodsName", goods.getDisplayGoodsName());
                        goodsSkuMap.put("displayPrice", goods.getDisplayPrice());
                        goodsSkuMap.put("descPath", goods.getDescPath());
                        goodsSkuMap.put("displayBanner", goods.getDisplayBanner());
                    }
                }
                goodsSkuMap.put("activityTaskId", activityTask.getId());
                maps.add(goodsSkuMap);
            }
        } else if (type == TaskSpaceTypeEnum.JOIN_LUCKY.getType() || type == TaskSpaceTypeEnum.LAUNCH_LUCKY.getType()
                || type == TaskSpaceTypeEnum.RECEIVE_GIFT.getType()) {
            int defType = ActivityDefTypeEnum.ORDER_BACK.getCode();
            if (type == TaskSpaceTypeEnum.LAUNCH_LUCKY.getType()) {
                defType = ActivityDefTypeEnum.ORDER_BACK.getCode();
            }
            if (type == TaskSpaceTypeEnum.RECEIVE_GIFT.getType()) {
                defType = ActivityDefTypeEnum.ASSIST.getCode();
            }
            Map<String, Object> defParams = new HashMap<>();
            defParams.put("merchantId", merchantShop.getMerchantId());
            defParams.put("shopId", shopId);
            defParams.put("type", defType);
            defParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
            defParams.put("del", DelEnum.NO.getCode());
            defParams.put("offset", offset);
            defParams.put("limit", limit);
            List<ActivityDef> activityDefs = activityDefService.selectByParams(defParams);
            if (CollectionUtils.isEmpty(activityDefs)) {
                resultMap.put("list", new ArrayList<>());
                simpleResponse.setData(resultMap);
                return simpleResponse;
            }
            List<Long> activityDefIds = new ArrayList<>();
            for (ActivityDef activityDef : activityDefs) {
                goodsIdList.add(activityDef.getGoodsId());
                activityDefIds.add(activityDef.getId());
            }
            Map<String, Object> params = new HashMap<>();
            params.put("goodsIds", goodsIdList);
            List<Goods> goodsList = goodsService.pageList(params);
            params.clear();
            params.put("activityDefIds", activityDefIds);
            params.put("status", ActivityStatusEnum.PUBLISHED.getCode());
            List<Activity> page = activityService.findPage(params);
            for (ActivityDef activityDef : activityDefs) {
                if (activityDef.getStock() <= 0) {
                    if (CollectionUtils.isEmpty(page)) {
                        continue;
                    }
                    boolean containDef = false;
                    for (Activity activity : page) {
                        if (activityDef.getId().equals(activity.getActivityDefId())) {
                            containDef = true;
                        }
                    }
                    if (!containDef) {
                        continue;
                    }
                }
                Map<String, Object> goodsSkuMap = new HashMap<>();
                goodsSkuMap.put("goodsId", activityDef.getGoodsId());
                for (Goods goods : goodsList) {
                    if (activityDef.getGoodsId().equals(goods.getId())) {
                        goodsSkuMap.put("picUrl", goods.getPicUrl());
                        goodsSkuMap.put("displayGoodsName", goods.getDisplayGoodsName());
                        goodsSkuMap.put("displayPrice", goods.getDisplayPrice());
                        goodsSkuMap.put("descPath", goods.getDescPath());
                        goodsSkuMap.put("displayBanner", goods.getDisplayBanner());
                    }
                }
                goodsSkuMap.put("activityDefId", activityDef.getId());
                maps.add(goodsSkuMap);
            }
        } else if (type == TaskSpaceTypeEnum.JOIN_TRIAL.getType()) {

        } else {
            resultMap.put("list", new ArrayList<>());
            simpleResponse.setData(resultMap);
            return simpleResponse;
        }
        resultMap.put("list", maps);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 转换type类型
     *
     * @param userActivity
     * @return
     */
    private void changeTypeAndTagAndStatus(UserActivity userActivity, UserTask userTask, Activity activity, Map<String, Object> map) {
        if (userActivity == null || userActivity.getType() == null) {
            return;
        }
        UserActivityTypeEnum userActivityTypeEnum = UserActivityTypeEnum.findByCode(userActivity.getType());
        if (userActivityTypeEnum == null) {
            return;
        }
        TaskSpaceTypeEnum spaceTypeEnum = getTaskSpaceTypeEnum(userActivityTypeEnum);
        map.put("tag", spaceTypeEnum.getTag());
        map.put("type", spaceTypeEnum.getType());
        // 1:新人任务，2:活动任务，3:红包任务
        // 用户任务 状态
        // ---- 0.1元发起抽奖&参加一次抽奖活动
        //1：进行中
        //2：已中奖
        //3：未中奖
        //-- 4：领奖中
        //9：待审核
        //10：审核成功
        //11：审核失败
        //5：领取成功
        //8：待返现
        //6：返现失败
        //13：任务失败
        // ---- 领取赠品
        //1：进行中
        //7：已成团
        //14:未成团
        //-- 4：领奖中
        //9：待审核
        //10：审核成功
        //11：审核失败
        //5：领取成功
        //8：待返现
        //6：返现失败
        //13：任务失败
        // ---- 红包任务
        //9：待审核
        //10：审核成功
        //11：审核失败
        //12：领取成功
        //13：任务失败
        if (userTask != null) {
            map.put("taskId", userTask.getId());
            map.put("orderId", userTask.getOrderId());
            UserTaskStatusEnum statusEnum = UserTaskStatusEnum.findByCode(userTask.getStatus());
            if (statusEnum == null) {
                return;
            }
            switch (statusEnum) {
                case AUDIT_ERROR:
                    map.put("status", 11);
                    break;
                case AWARD_EXPIRE:
                    map.put("status", 13);
                    break;
                case INIT:
                    // 区分任务类型 1:新人任务，2:活动任务，3:红包任务
                    map.put("status", 2);
                    if (spaceTypeEnum.getTag() == 3) {
                        // 这里是中间状态
                        map.put("status", 4);
                    }
                    if (spaceTypeEnum.getTag() == 1) {
                        map.put("status", 7);
                    }
                    break;
                case AUDIT_WAIT:
                    map.put("status", 9);
                    break;
                case AUDIT_SUCCESS:
                    // 区分任务类型 1:新人任务，2:活动任务，3:红包任务
                    if (spaceTypeEnum.getTag() == 1) {
                        map.put("status", 4);
                    }
                    if (spaceTypeEnum.getTag() == 2) {
                        map.put("status", 4);
                    }
                    if (spaceTypeEnum.getTag() == 3) {
                        map.put("status", 10);
                    }
                    break;
                case AWARD_SUCCESS:
                    // 区分任务类型 1:新人任务，2:活动任务，3:红包任务
                    if (spaceTypeEnum.getTag() == 1) {
                        map.put("status", 5);
                    }
                    if (spaceTypeEnum.getTag() == 2) {
                        map.put("status", 5);
                    }
                    if (spaceTypeEnum.getTag() == 3) {
                        map.put("status", 12);
                    }
                    break;
                default:
                    map.put("status", 3);
                    break;
            }
        } else {
            // 只是参与，并未开奖或中奖
            UserActivityStatusEnum statusEnum = UserActivityStatusEnum.findByCode(userActivity.getStatus());
            if (statusEnum == null) {
                return;
            }
            switch (statusEnum) {
                case AWARD_EXPIRE:
                    // 过期
                    map.put("status", 13);
                    break;
                case NO_QUALIFICATION:
                    // 没有中奖资格
                    map.put("status", 3);
                    break;
                case NO_WIN:
                    // 要细化
                    map.put("status", 3);
                    if (activity != null) {
                        ActivityStatusEnum byCode = ActivityStatusEnum.findByCode(activity.getStatus());
                        switch (byCode) {
                            case PUBLISHED:
                                map.put("status", 1);
                                if (activity.getAwardStartTime() != null) {
                                    map.put("status", 3);
                                }
                                break;
                            case UNPUBLISHED:
                            case END:
                            case FAILURE:
                                map.put("status", 3);
                                if (spaceTypeEnum == TaskSpaceTypeEnum.OPEN_RED_PACKET) {
                                    map.put("status", 13);
                                }
                                break;
                            case DEL:
                                map.put("status", 3);
                                break;
                            default:
                                map.put("status", 3);
                                break;
                        }
                    }
                    break;
                case WINNER:
                    map.put("status", 1);
                    if (activity != null && activity.getAwardStartTime() != null) {
                        map.put("status", 3);
                    }
                    if (spaceTypeEnum == TaskSpaceTypeEnum.OPEN_RED_PACKET) {
                        map.put("status", 5);
                    }
                    break;
                case RED_PACKAGE:
                    map.put("status", 12);
                    break;
                case RED_PACKAGE_ING:
                    map.put("status", 1);
                    break;
                default:
                    map.put("status", 3);
                    break;
            }
        }
    }

    private TaskSpaceTypeEnum getTaskSpaceTypeEnum(UserActivityTypeEnum userActivityTypeEnum) {
        TaskSpaceTypeEnum spaceTypeEnum;
        // 1:新人任务，2:活动任务，3:红包任务
        switch (userActivityTypeEnum) {
            // 红包任务
            case MCH_PLAN:
                spaceTypeEnum = TaskSpaceTypeEnum.JOIN_TRIAL;
                break;
            case ORDER_BACK_START:
                spaceTypeEnum = TaskSpaceTypeEnum.LAUNCH_LUCKY;
                break;
            case ORDER_BACK_JOIN:
                spaceTypeEnum = TaskSpaceTypeEnum.JOIN_LUCKY;
                break;
            case NEW_MAN_START:
            case ASSIST:
                spaceTypeEnum = TaskSpaceTypeEnum.RECEIVE_GIFT;
                break;
            case NEW_MAN_JOIN:
            case ASSIST_JOIN:
                spaceTypeEnum = TaskSpaceTypeEnum.JOIN_LUCKY;
                break;
            case FILL_BACK_ORDER:
            case FILL_BACK_ORDER_V2:
                spaceTypeEnum = TaskSpaceTypeEnum.ORDER_BACK;
                break;
            case FAV_CART:
            case FAV_CART_V2:
                spaceTypeEnum = TaskSpaceTypeEnum.FAV_CART;
                break;
            case GOODS_COMMENT:
                spaceTypeEnum = TaskSpaceTypeEnum.GOOD_COMMENT;
                break;
            case OPEN_RED_PACKET_START:
            case OPEN_RED_PACKET_JOIN:
                spaceTypeEnum = TaskSpaceTypeEnum.OPEN_RED_PACKET;
                break;
            case ORDER_BACK_START_V2:
            case ORDER_BACK_JOIN_V2:
                spaceTypeEnum = TaskSpaceTypeEnum.JOIN_LUCKY_V2;
                break;
            case ASSIST_V2:
            case ASSIST_JOIN_V2:
                spaceTypeEnum = TaskSpaceTypeEnum.RECEIVE_GIFT_V2;
                break;
            case GOODS_COMMENT_V2:
                spaceTypeEnum = TaskSpaceTypeEnum.GOOD_COMMENT_V2;
                break;
            default:
                spaceTypeEnum = TaskSpaceTypeEnum.JOIN_LUCKY;
                break;
        }
        return spaceTypeEnum;
    }
}
