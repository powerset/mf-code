package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSONArray;
import mf.code.api.applet.service.AppletGoodsService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月05日 14:34
 */
@Service
public class AppletGoodsServiceImpl implements AppletGoodsService {
    @Autowired
    private GoodsService goodsService;

    @Override
    public SimpleResponse queryGoodsDetail(Long merchantID, Long shopID, Long goodsId) {
        SimpleResponse d = new SimpleResponse();
        if (merchantID == null || merchantID == 0 || shopID == null || shopID == 0 || goodsId == null || goodsId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        Goods goods = this.goodsService.selectById(goodsId);
        if (goods == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动的商品编号异常");
        }
        Map<String, Object> resp = new HashMap<>();
        resp.put("goodsUrl", goods.getDescPath());
        resp.put("picUrl", goods.getPicUrl());
        resp.put("displayGoodsName", goods.getDisplayGoodsName());
        resp.put("displayPrice", goods.getDisplayPrice().toString());
        resp.put("id", goods.getId());

        List<String> goodsUrls = new ArrayList<>();
        if (goods.getDescPath().contains("[")) {
            goodsUrls = JSONArray.parseArray(goods.getDescPath(), String.class);
        } else {
            goodsUrls.add(goods.getDescPath());
        }
        resp.put("goodsPath", goodsUrls);
        d.setData(resp);
        return d;
    }
}
