package mf.code.api.applet.v7;

import mf.code.api.applet.dto.TaskV7Req;
import mf.code.api.applet.v7.service.AppletUserTaskV7Service;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.applet.v2
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月03日 10:47
 */
@RestController
@RequestMapping("/api/applet/v7/userTask")
public class AppletUserTaskV7Api {
    @Autowired
    private AppletUserTaskV7Service appletUserTaskV7Service;

    /***
     * 查询任务
     * @param merchantId 商户编号
     * @param shopId 店铺编号
     * @param userId 用户编号
     * @param taskId 任务编号
     * @return
     */
    @RequestMapping(path = "/queryUserTask", method = RequestMethod.GET)
    public SimpleResponse queryUserTask(@RequestParam(name = "merchantId") Long merchantId,
                                        @RequestParam(name = "shopId") Long shopId,
                                        @RequestParam(name = "userId") Long userId,
                                        @RequestParam(name = "taskId") Long taskId) {
        return appletUserTaskV7Service.queryUserTask(merchantId, shopId, userId, taskId);
    }

    /***
     * 获取userTaskId
     * <pre>
     *    中奖任务：
     *      查询userTask,
     *        返回taskId,
     *    非中奖任务(回填订单不走创建流程):
     *      创建userTask：其中收藏加购任务创建，需要传入商品信息(goodsId)
     *          返回taskId
     * </pre>
     * @param merchantId 商户编号
     * @param shopId 店铺编号
     * @param userId 用户编号
     * @param activityId 活动编号
     * @param activityDefId 活动定义编号
     * @param goodsId 收藏加购
     * @param source 1:任务空间 2：TODO
     * @return userTaskId
     */
    @RequestMapping(path = "/getUserTaskId", method = RequestMethod.GET)
    public SimpleResponse queryUserTask(@RequestParam(name = "merchantId") Long merchantId,
                                        @RequestParam(name = "shopId") Long shopId,
                                        @RequestParam(name = "userId") Long userId,
                                        @RequestParam(name = "activityId", required = false, defaultValue = "0") Long activityId,
                                        @RequestParam(name = "orderId", required = false) String orderId,
                                        @RequestParam(name = "activityDefId", required = false, defaultValue = "0") Long activityDefId,
                                        @RequestParam(name = "source", required = false, defaultValue = "0") Integer source,
                                        @RequestParam(name = "goodsId", required = false, defaultValue = "0") Long goodsId) {
        return appletUserTaskV7Service.getUserTaskId(merchantId, shopId, userId, activityId, activityDefId, goodsId, orderId, source);
    }

    /***
     * 任务审核提交
     * @param taskV7Req
     * @return
     */
    @PostMapping("/commitUserTask")
    public SimpleResponse commitUserTask(@RequestBody TaskV7Req taskV7Req) {
        return appletUserTaskV7Service.commitUserTaskAudit(taskV7Req);
    }

    /***
     * 任务的重新提交
     * @param merchantId
     * @param shopId
     * @param userId
     * @param taskId
     * @param retryCommitType 重新提交类型 1：失败重新提交 2：超时重新提交(此场景需要重置，startTime)
     * @return
     */
    @RequestMapping(path = "/retryCommitUserTask", method = RequestMethod.GET)
    public SimpleResponse retryCommitUserTask(@RequestParam(name = "merchantId") Long merchantId,
                                              @RequestParam(name = "shopId") Long shopId,
                                              @RequestParam(name = "userId") Long userId,
                                              @RequestParam(name = "taskId") Long taskId,
                                              @RequestParam(name = "recommitType", required = false, defaultValue = "1") int retryCommitType) {
        return appletUserTaskV7Service.retryCommitUserTask(merchantId, shopId, userId, taskId, retryCommitType);
    }
}
