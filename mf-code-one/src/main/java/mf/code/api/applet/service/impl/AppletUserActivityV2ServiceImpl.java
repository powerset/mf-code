package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.*;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityOrder;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityOrderService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.service.AppletUserActivityV2Service;
import mf.code.api.applet.service.AsyncFillOrderService;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.common.caller.luckcrm.LuckCrmCaller;
import mf.code.common.constant.*;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.verify.TaobaoVerifyService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2019-01-03 20:20
 */
@Slf4j
@Service
public class AppletUserActivityV2ServiceImpl implements AppletUserActivityV2Service {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityOrderService activityOrderService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private LuckCrmCaller luckCrmCaller;
    @Autowired
    private AppletUserActivityService appletUserActivityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private TaobaoVerifyService taobaoVerifyService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 回填订单
     *
     * @param req
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse backfillOrderV2(BackFillOrderReq req) {
        /*
         * 1、调用喜销宝 2、校验返回结果 3、创建activity
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        if (!StringUtils.isNumeric(req.getUserId()) || StringUtils.isBlank(req.getOrderId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请求参数错误");
            return simpleResponse;
        }
        Long userId = Long.valueOf(req.getUserId());
        String orderId = req.getOrderId();
        Long shopId = Long.valueOf(req.getShopId());

        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("店铺错误");
            return simpleResponse;
        }

        // 如果同一订单已经被回填过就不能再次领取红包
        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", merchantShop.getMerchantId());
        userTaskParams.put("shopId", shopId);
        userTaskParams.put("userId", userId);
        userTaskParams.put("type", UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTaskParams.put("orderId", orderId);
        int countUserTask = userTaskService.countUserTask(userTaskParams);
        if (countUserTask > 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10.getCode(), "该订单已完成回填确认，请填写其他订单");
        }

        // redis防重拦截,60秒。由于是订单号做主键，所以暂不管异常无法解锁问题
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.BACKFILLORDER_REPEAT + orderId, 60, TimeUnit.SECONDS);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "订单校验中...");
        }

        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            simpleResponse.setMessage("用户未授权");
            // 主动删除redis锁
            stringRedisTemplate.delete(RedisKeyConstant.BACKFILLORDER_REPEAT + orderId);
            return simpleResponse;
        }
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantShop.getMerchantId());
        // 检查订单信息
        simpleResponse = checkOrderInfoFromXxb(orderInfo);
        if (simpleResponse.error()) {
            // 主动删除redis锁
            stringRedisTemplate.delete(RedisKeyConstant.BACKFILLORDER_REPEAT + orderId);
            return simpleResponse;
        }
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        JSONArray numiids = jsonObject.getJSONArray("num_iid");
        // 筛选库中没有的商品
        Map<String, Goods> goodsMap = new HashMap<>();
        List<Long> goodsIds = new ArrayList<>();
        // 查询订单中的商品信息
        simpleResponse = selectGoodsFromOrderInfo(merchantShop.getMerchantId(), numiids, goodsIds, goodsMap);
        if (simpleResponse.error()) {
            // 主动删除redis锁
            stringRedisTemplate.delete(RedisKeyConstant.BACKFILLORDER_REPEAT + orderId);
            return simpleResponse;
        }
        // 妥协处理，回填订单没有商品ID，而好评返现是有的
        if (StringUtils.isNotBlank(req.getGoodsId())) {
            return executeGoodComment(req, simpleResponse, shopId, jsonObject, goodsMap);
        }
        // 检查有没有回填订单活动的activity_def
        Map<String, Object> defParams = new HashMap<>();
        defParams.put("shopId", shopId);
        defParams.put("type", ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
        defParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        defParams.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.selectByParams(defParams);
        if (CollectionUtils.isEmpty(activityDefs)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("暂无该订单对应的任务");
            // 主动删除redis锁
            stringRedisTemplate.delete(RedisKeyConstant.BACKFILLORDER_REPEAT + orderId);
            return simpleResponse;
        }
        // 执行回填订单逻辑
        simpleResponse = executeFillBackOrder(activityDefs, req, jsonObject, goodsIds);
        if (simpleResponse.error()) {
            // 主动删除redis锁
            stringRedisTemplate.delete(RedisKeyConstant.BACKFILLORDER_REPEAT + orderId);
            return simpleResponse;
        }
        Map map = null;
        if (simpleResponse != null && simpleResponse.getData() != null) {
            map = (Map) simpleResponse.getData();
        }
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userId, shopId, merchantShop.getMerchantId());

        Map<String, Object> resultMap = BeanMapUtil.beanToMap(commissionResp);

        //前端展现数值的字段
        resultMap.put("commission", BigDecimal.ZERO.toString());
        if (map != null) {
            resultMap.put("commission", map.get("commission"));
        }

        Goods goods = goodsMap.get(numiids.get(0).toString());
        // 继续组装结果
        resultMap.putAll(combineResult(req, shopId, jsonObject, goods));
        simpleResponse.setData(resultMap);
        appletUserActivityService.updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, goods, jsonObject);
        // 自动激活免单抽奖活动
        if (!CollectionUtils.isEmpty(goodsIds)) {
            createActviityFromDefByGoodsId(userId, orderId, merchantShop, jsonObject, goodsIds);
        }
        // 主动删除redis锁
        stringRedisTemplate.delete(RedisKeyConstant.BACKFILLORDER_REPEAT + orderId);
        return simpleResponse;
    }

    /**
     * 执行好评刷图任务逻辑
     *
     * @param req
     * @param simpleResponse
     * @param shopId
     * @param jsonObject
     * @param goodsMap
     * @return
     */
    private SimpleResponse executeGoodComment(BackFillOrderReq req, SimpleResponse simpleResponse, Long shopId, JSONObject jsonObject, Map<String, Goods> goodsMap) {
        Long goodsIdReq = Long.valueOf(req.getGoodsId());
        Goods goods = null;
        for (Goods good : goodsMap.values()) {
            // goodsMap肯定不为空，但是不保证goods包含id
            if (null == good.getId()) {
                continue;
            }
            if (good.getId().equals(goodsIdReq)) {
                goods = good;
            }
        }
        if (goods == null) {
            // 这里应该不会出现
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该订单未包含任务商品");
            return simpleResponse;
        }
        // 查询好评返现的任务，然后赋值
        simpleResponse.setData(combineResult(req, shopId, jsonObject, goods));
        return simpleResponse;
    }

    /**
     * 组装结果
     *
     * @param req        入参
     * @param shopId     店铺id
     * @param jsonObject 订单信息
     * @param goods      商品信息
     * @return resultMap返回结果
     */
    private Map<String, Object> combineResult(BackFillOrderReq req, Long shopId, JSONObject jsonObject, Goods goods) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("goodsId", goods.getId());
        resultMap.put("picPath", goods.getPicUrl());
        resultMap.put("displayPrice", goods.getDisplayPrice());
        resultMap.put("title", goods.getXxbtopTitle());
        // 支付时间（格式：yyyy.MM.dd HH:mm）
        Date payTime = DateUtil.stringtoDate(jsonObject.getString("pay_time"), DateUtil.FORMAT_ONE);
        resultMap.put("payTime", DateUtil.dateToString(payTime, "yyyy.MM.dd HH:mm"));
        resultMap.put("orderId", req.getOrderId());
        resultMap.put("amount", 0);
        // 检查有没有该商品的好评返现活动的activity_task， 返回amount字段，用于连接好评任务
        Map<String, Object> taskParams = new HashMap<>();
        taskParams.put("shopId", shopId);
        taskParams.put("goodsId", goods.getId());
        taskParams.put("type", ActivityDefTypeEnum.GOOD_COMMENT.getCode());
        taskParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        taskParams.put("del", DelEnum.NO.getCode());
        List<ActivityTask> activityTaskList = activityTaskService.selectByParams(taskParams);
        if (!CollectionUtils.isEmpty(activityTaskList)) {
            for (ActivityTask activityTask : activityTaskList) {
                Map<String, Object> utparams = new HashMap<>();
                utparams.put("shopId", shopId);
                utparams.put("userId", Long.valueOf(req.getUserId()));
                utparams.put("type", UserTaskTypeEnum.GOOD_COMMENT.getCode());
                utparams.put("status", UserTaskStatusEnum.AUDIT_WAIT.getCode());
                utparams.put("activityTaskIds", Collections.singletonList(activityTask.getId()));
                List<UserTask> userTaskList = userTaskService.listPageByParams(utparams);
                if (!CollectionUtils.isEmpty(userTaskList)) {
                    CommissionBO commissionBO = commissionService.commissionCalculate(Long.valueOf(req.getUserId()), activityTask.getRpAmount());
                    resultMap.put("amount", commissionBO.getCommission());
                    UserTask updateComment = userTaskList.get(0);
                    updateComment.setId(updateComment.getId());
                    updateComment.setOrderId(req.getOrderId());
                    updateComment.setUtime(new Date());
                    userTaskService.update(updateComment);
                }
            }
        }
        return resultMap;
    }

    private SimpleResponse executeFillBackOrder(List<ActivityDef> activityDefs, BackFillOrderReq req, JSONObject orderObj, List<Long> goodsIds) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Long userId = Long.valueOf(req.getUserId());
        Long shopId = Long.valueOf(req.getShopId());
        String orderId = req.getOrderId();
        // 如果同一订单已经被回填过就不能再次领取红包
        ActivityDef activityDef = activityDefs.get(0);
        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", activityDef.getMerchantId());
        userTaskParams.put("shopId", shopId);
        userTaskParams.put("userId", userId);
        userTaskParams.put("type", UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTaskParams.put("orderId", orderId);
        int countUserTask = userTaskService.countUserTask(userTaskParams);
        if (countUserTask > 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            simpleResponse.setMessage("该订单已完成回填确认，请填写其他订单");
            return simpleResponse;
        }
        /*
         * 判断是否是阶梯定价的类型，并计算出应扣除佣金金额
         * 区间红包金额,若是区间，则用json存储[{"from":"0.1", "to":"0.2","amount":"1"},{},{}]（非必填）
         */
        BigDecimal realCommission = calcCommission(orderObj, activityDef);


        // 创建userActivty
        UserActivity userActivity = new UserActivity();
        userActivity.setUserId(userId);
        userActivity.setMerchantId(activityDef.getMerchantId());
        userActivity.setShopId(shopId);
        userActivity.setActivityDefId(activityDef.getId());
        userActivity.setActivityId(0L);
        userActivity.setType(UserActivityTypeEnum.FILL_BACK_ORDER.getCode());
        userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        userActivity.setActivityDefId(activityDef.getId());
        int rows = userActivityService.insertUserActivity(userActivity);
        if (rows == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("回填订单失败");
            return simpleResponse;
        }
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, shopId, UserCouponTypeEnum.ORDER_RED_PACKET, realCommission);
        commissionDisDto.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        UserTask userTask = new UserTask();
        userTask.setUserId(userId);
        // 直接通过发放奖励
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        userTask.setActivityDefId(activityDef.getId());
        // 设置默认值
        userTask.setActivityId(0L);
        userTask.setUserActivityId(userActivity.getId());
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setMerchantId(activityDef.getMerchantId());
        userTask.setShopId(shopId);
        userTask.setType(UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTask.setOrderId(orderId);
        userTask.setActivityDefId(activityDef.getId());
        if (!CollectionUtils.isEmpty(goodsIds)) {
            userTask.setGoodsId(goodsIds.get(0));
        }
        Date now = new Date();
        userTask.setStartTime(now);
        userTask.setReceiveTime(now);
        userTask.setAuditingTime(now);
        userTask.setCtime(now);
        userTask.setUtime(now);
        userTaskService.insertUserTask(userTask);

        Map map = new HashMap();
        map.put("commission", commissionBO.getCommission());
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /**
     * 根据订单中的商品numiids查询商品信息
     *
     * @param merchantId 商户id
     * @param numiids    淘宝商品id
     * @param goodsIds   商品id
     * @param goodsMap   商品map
     * @return
     */
    private SimpleResponse selectGoodsFromOrderInfo(Long merchantId, JSONArray numiids, List<Long> goodsIds, Map<String, Goods> goodsMap) {
        SimpleResponse simpleResponse = new SimpleResponse();
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "display_price", "xxbtop_price", "pic_url", "display_goods_name", "xxbtop_title", "xxbtop_num_iid");
        queryWrapper.in("xxbtop_num_iid", numiids);
        List<Goods> goodsList = goodsService.list(queryWrapper);
        log.info("根据订单中的商品numiids查询商品信息" + JSON.toJSONString(goodsList));
        JSONArray numiidsRemove = new JSONArray();
        if (!CollectionUtils.isEmpty(goodsList)) {
            for (Goods goods : goodsList) {
                goodsIds.add(goods.getId());
                goodsMap.put(goods.getXxbtopNumIid(), goods);
                numiids.remove(goods.getXxbtopNumIid());
                numiidsRemove.add(goods.getXxbtopNumIid());
            }
        }
        // 如果存在不存在的商品，则向喜销宝查询
        if (!CollectionUtils.isEmpty(numiids)) {
            for (Object numiid : numiids) {
                String xxbResult = taobaoVerifyService.getGoodsInfo(numiid.toString(), merchantId);
                if (luckCrmCaller.isError(xxbResult)) {
                    continue;
                }
                Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
                if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() <= 1) {
                    continue;
                }
                // 组装参数 名称-主图-详情图-价格
                Goods goods = new Goods();
                goods.setXxbtopTitle(xxbMap.get("title").toString());
                goods.setDisplayGoodsName(xxbMap.get("title").toString());
                goods.setPicUrl(xxbMap.get("pic_url").toString());
                goods.setXxbtopPrice(xxbMap.get("price").toString());
                goods.setDisplayPrice(new BigDecimal(xxbMap.get("price").toString()));
                goods.setXxbtopNumIid(numiid.toString());
                goodsMap.put(numiid.toString(), goods);
            }
        }
        if (CollectionUtils.isEmpty(goodsMap)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该订单未包含任务商品");
            return simpleResponse;
        }
        numiids.addAll(0, numiidsRemove);
        return simpleResponse;
    }

    /**
     * 检查用户填写订单
     *
     * @param orderInfo
     * @return
     */
    private SimpleResponse checkOrderInfoFromXxb(String orderInfo) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (luckCrmCaller.isError(orderInfo)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("订单填写有误");
            return simpleResponse;
        }
        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date createDate = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (createDate.getTime() < DateUtil.threeMouthBefore().getTime()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("该订单已失效");
            return simpleResponse;
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            simpleResponse.setMessage("该订单未完成");
            return simpleResponse;
        }
        // 检查任务
        // 2019-03-20修改为全店商品皆可以成功回填订单
        JSONArray numIids = jsonObject.getJSONArray("num_iid");
        if (CollectionUtils.isEmpty(numIids)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            simpleResponse.setMessage("该订单数据错误");
            return simpleResponse;
        }
        return simpleResponse;
    }

    /**
     * 判断是否是阶梯定价的类型，并计算出应扣除佣金金额
     *
     * @param jsonObject  订单信息
     * @param activityDef 活动定价
     * @return 总佣金
     */
    @Override
    public BigDecimal calcCommission(JSONObject jsonObject, ActivityDef activityDef) {
        BigDecimal payAmount = jsonObject.getBigDecimal("payment");
        BigDecimal realCommission = BigDecimal.ZERO;
        if (JsonParseUtil.booJsonArr(activityDef.getAmountJson())) {
            // 阶梯计算金额, max为记录区间最大金额，maxAmount为记录max最大时的区间奖励；min，minAmount同理
            BigDecimal max = BigDecimal.ZERO, min = new BigDecimal("10000");
            BigDecimal maxAmount = BigDecimal.ZERO, minAmount = BigDecimal.ZERO;
            List<Map> jsonArray = JSONArray.parseArray(activityDef.getAmountJson(), Map.class);
            for (Map map : jsonArray) {
                BigDecimal from = new BigDecimal(map.get("from").toString());
                BigDecimal to = new BigDecimal(map.get("to").toString());
                BigDecimal amount = new BigDecimal(map.get("amount").toString());
                if (payAmount.compareTo(from) >= 0 && payAmount.compareTo(to) <= 0) {
                    realCommission = amount;
                }
                if (min.compareTo(from) > 0) {
                    min = from;
                    minAmount = amount;
                }
                if (max.compareTo(to) < 0) {
                    max = to;
                    maxAmount = amount;
                }

            }
            // 如果区间内不符合，则寻求区间外的金额
            if (realCommission.compareTo(BigDecimal.ZERO) == 0) {
                if (payAmount.compareTo(min) < 0) {
                    realCommission = minAmount;
                }
                if (payAmount.compareTo(max) > 0) {
                    realCommission = maxAmount;
                }
            }
        } else {
            // 此处goods_price字段存储的是单个任务的佣金
            realCommission = activityDef.getGoodsPrice();
        }
        return realCommission;
    }

    /**
     * 依据商品ID创建免单活动
     *
     * @param userId       用户id
     * @param orderId      订单号
     * @param merchantShop 店铺
     * @param jsonObject   订单信息
     * @param goodsIds     订单包含商品id
     */
    private void createActviityFromDefByGoodsId(Long userId, String orderId, MerchantShop merchantShop, JSONObject jsonObject, List<Long> goodsIds) {
        Map<String, Object> defsParams = new HashMap<>();
        defsParams.clear();
        defsParams.put("merchantId", merchantShop.getMerchantId());
        defsParams.put("shopId", merchantShop.getId());
        defsParams.put("goodsIdList", goodsIds);
        defsParams.put("type", ActivityDefTypeEnum.ORDER_BACK.getCode());
        defsParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        List<ActivityDef> activityDefList = activityDefService.selectByParams(defsParams);
        if (CollectionUtils.isEmpty(activityDefList)) {
            return;
        }
        ActivityDef orderBackDef = activityDefList.get(0);
        // 判断同一用户同一活动定义，总共只能发起三次
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantShop.getMerchantId());
        params.put("shopId", merchantShop.getId());
        params.put("activityDefId", orderBackDef.getId());
        params.put("userId", userId);
        params.put("types", Arrays.asList(ActivityTypeEnum.ORDER_BACK_START.getCode(),ActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
        params.put("del", DelEnum.NO.getCode());
        List<Activity> activityList = activityService.findPage(params);
        if (activityList.size() >= 3) {
            return;
        }
        // 创建activity, 创建不成功，不再返回错误信息
        SimpleResponse simpleResponse1 = appletUserActivityService.createActivityFromDef(userId, orderId,
                orderBackDef.getId(), ActivityStatusEnum.UNPUBLISHED.getCode());
        if (simpleResponse1.error()) {
            return;
        }
        if (simpleResponse1.getData() == null) {
            return;
        }
        Activity activity = (Activity) simpleResponse1.getData();
        // 订单表
        Goods goods = goodsService.selectById(orderBackDef.getGoodsId());
        appletUserActivityService.updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), activity, goods, jsonObject);
    }

    /**
     * 获取回填订单任务返显数据
     *
     * @param userId
     * @param taskId
     * @return
     */
    @Override
    public SimpleResponse orderInfo(Long userId, Long taskId) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (taskId == null || userId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("参数错误");
            return simpleResponse;
        }
        UserTask userTask = userTaskService.selectByPrimaryKey(taskId);
        if (userTask == null || StringUtils.isBlank(userTask.getOrderId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("任务详情错误");
            return simpleResponse;
        }
        Map<String, Object> resultMap = new HashMap<>();
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(userTask.getMerchantId(), userTask.getShopId(), userTask.getUserId(), userTask.getOrderId());
        if (!CollectionUtils.isEmpty(activityOrders)) {
            // 支付时间（格式：yyyy.MM.dd HH:mm）
            resultMap.put("payTime", DateUtil.dateToString(activityOrders.get(0).getPayTime(), "yyyy.MM.dd HH:mm"));
        } else {
            resultMap.put("payTime", DateUtil.dateToString(userTask.getCtime(), "yyyy.MM.dd HH:mm"));
        }
        if (userTask.getGoodsId() == null) {
            resultMap.put("picPath", activityOrders.get(0).getPicUrl());
            resultMap.put("displayPrice", activityOrders.get(0).getPayment());
            resultMap.put("title", activityOrders.get(0).getTitle());
        } else {
            Goods goods = goodsService.selectById(userTask.getGoodsId());
            resultMap.put("goodsId", goods.getId());
            resultMap.put("picPath", goods.getPicUrl());
            resultMap.put("displayPrice", goods.getDisplayPrice());
            resultMap.put("title", goods.getXxbtopTitle());
        }
        resultMap.put("orderId", userTask.getOrderId());
        resultMap.put("amount", 0);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    @Override
    public SimpleResponse queryBackfillOrderResult(String orderId) {
        if(StringUtils.isBlank(orderId)){
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请填写回填订单");
        }
        String redisResultKey = RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId;
        String str = stringRedisTemplate.opsForValue().get(redisResultKey);
        if(StringUtils.isBlank(str)){
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "服务开小差了，请重试-_-");
        }
        //删除结果redis
        stringRedisTemplate.delete(redisResultKey);
        return JSONObject.parseObject(str, SimpleResponse.class);
    }
}
