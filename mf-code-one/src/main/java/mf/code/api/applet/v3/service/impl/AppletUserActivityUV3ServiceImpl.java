package mf.code.api.applet.v3.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.*;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityOrder;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.*;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.service.AppletUserActivityV2Service;
import mf.code.api.applet.service.AsyncFillOrderService;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.api.applet.utils.AppletValidator;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.dto.RedPackageReq;
import mf.code.api.applet.v3.service.AppletUserActivityQV3Service;
import mf.code.api.applet.v3.service.AppletUserActivityUV3Service;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.common.caller.luckcrm.LuckCrmCaller;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.common.verify.TaobaoVerifyService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v3.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-17 下午1:10
 */
@Slf4j
@Service
public class AppletUserActivityUV3ServiceImpl implements AppletUserActivityUV3Service {
    @Autowired
    private TaobaoVerifyService taobaoVerifyService;
    @Autowired
    private AsyncFillOrderService asyncFillOrderService;
    @Autowired
    private AppletUserActivityQV3Service appletUserActivityQV3Service;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityOrderService activityOrderService;
    @Autowired
    private LuckCrmCaller luckCrmCaller;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private ActivityCommissionService activityCommissionService;
    @Autowired
    private AppletUserActivityV2Service appletUserActivityV2Service;
    @Autowired
    private ActivityDefCheckService activityDefCheckService;



    @Override
    public SimpleResponse openRedPackage(RedPackageReq redPackageReq) {
        SimpleResponse<Object> response;

        String userId = redPackageReq.getUserId();
        String pubUserId = redPackageReq.getPubUserId();
        String activityId = redPackageReq.getActivityId();
        String activityDefId = redPackageReq.getActivityDefId();
        Integer pubJoinScene = redPackageReq.getPubJoinScene();
        // 入参校验
        response = AppletValidator.checkParams4AssistAndCreate(userId, activityId, activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 防重
        String raid = activityId;
        if (StringUtils.isNotBlank(activityDefId)) {
            raid = activityDefId;
        }
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 通用参数转化
        Long uid = Long.valueOf(userId);
        Long puid = null;
        if (StringUtils.isNotBlank(pubUserId) && RegexUtils.StringIsNumber(pubUserId)) {
            puid = Long.valueOf(pubUserId);
        }
        // 从活动列表中 点击立即拆开
        if (StringUtils.isNotBlank(activityDefId)) {
            Long adid = Long.valueOf(activityDefId);
            response = createOpenRedPackageActivity(uid, puid, adid, pubJoinScene);
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
            return response;
        }
        // 从邀请活动中 点击立即拆开
        if (StringUtils.isNotBlank(activityId)) {
            Long aid = Long.valueOf(activityId);
            response = openRedPackageActivityFromActivity(uid, puid, aid, pubJoinScene);
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
            return response;
        }


        return response;
    }


    @Override
    public SimpleResponse pubJoinOpenRedPackageActivity(String userId, String pubUserId, String activityId, Integer pubJoinScene) {
        SimpleResponse<Object> response = AppletValidator.checkParams4UserIdAndActivityId(userId, activityId);
        if (response != null && response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 参数转换
        Long uid = Long.valueOf(userId);
        Long puid = null;
        if (StringUtils.isNotBlank(pubUserId) && RegexUtils.StringIsNumber(pubUserId)) {
            puid = Long.valueOf(pubUserId);
        }
        Long aid = Long.valueOf(activityId);

        Activity activity = activityService.getById(aid);
        if (activity == null) {
            log.error("查询不到此活动，activityId = " + activityId);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            return response;
        }

        // 校验用户发起活动的资格
        Activity activityJoinner = appletUserActivityQV3Service.createableActivity(uid, activity.getActivityDefId());
        if (activityJoinner != null) {
            // 自动助力活动，并跳转至自己的活动详情
            RedPackageReq redPackageReq = new RedPackageReq();
            redPackageReq.setUserId(userId);
            redPackageReq.setActivityId(activityId);
            redPackageReq.setPubUserId(activity.getUserId().toString());
            redPackageReq.setPubJoinScene(pubJoinScene);
            return this.openRedPackage(redPackageReq);
        }

        // 获取用户信息
		/*User user = userService.getById(uid);
		if (user == null) {
			log.error("查询不到用户信息，userId = " +userId);
			response.setStatusEnum(ApiStatusEnum.ERROR_DB);
			response.setMessage("网络异常，请稍候重试");
			return response;
		}*/

        boolean expired = activityService.checkExpired(null, uid, activity.getActivityDefId());
        if (expired) {
            log.info("任务失败, 弹出在挑战一次的弹框");
            // 清除redis历史数据
            Object startUser = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId(), userId);
            if (startUser != null) {
                Map startUserVO = JSON.parseObject(startUser.toString(), Map.class);
                Object raid = startUserVO.get("activityId");
                // 清除发起信息
                stringRedisTemplate.opsForHash().delete(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId(), userId);
                // 助力者信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + raid + ":" + userId);
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + raid);
                // 删除奖池信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + raid);
                // 删除活动助力金集合
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + raid);
            }

            // 加工返回信息
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("activityStatus", 4);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        // 加工邀请者信息
		/*Map<String, Object> pubUserVO = new HashMap<>();
		pubUserVO.put("activityId", aid);
		pubUserVO.put("activityAmount", activity.getPayPrice());
		pubUserVO.put("avatarUrl", user.getAvatarUrl());
		pubUserVO.put("nickName", user.getNickName());*/

        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        BigDecimal tax = activity.getPayPrice().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityStatus", 8);
        resultVO.put("activityAmount", activity.getPayPrice().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
        /*resultVO.put("pubUserVO", pubUserVO);*/
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    /**
     * 扫码弹窗回填订单
     *
     * @param shopId
     * @param userId
     * @param orderId
     * @return
     */
    @Override
    public SimpleResponse backfillOrderPopupReplace(Long shopId, Long userId, String orderId) {
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "店铺错误");
        }


        return null;
    }

    @Override
    public SimpleResponse queryBackfillOrderResult(String orderId) {
        if(StringUtils.isBlank(orderId)){
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请填写回填订单");
        }
        String redisResultKey = mf.code.common.redis.RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId;
        String str = stringRedisTemplate.opsForValue().get(redisResultKey);
        if(StringUtils.isBlank(str)){
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "服务开小差了，请重试-_-");
        }
        //删除结果redis
        stringRedisTemplate.delete(redisResultKey);
        return JSONObject.parseObject(str, SimpleResponse.class);
    }

    @Override
    public SimpleResponse backfillOrderAsync(BackFillOrderReq req) {
        /*
         * 核心流程 1、调用喜销宝 2、校验返回结果 3、创建activity
         */
        if (!StringUtils.isNumeric(req.getUserId()) || StringUtils.isBlank(req.getOrderId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请求参数错误");
        }
        Long userId = Long.valueOf(req.getUserId());
        String orderId = req.getOrderId();
        Long shopId = Long.valueOf(req.getShopId());

        // redis防重拦截,120秒。由于是订单号做主键，所以暂不管异常无法解锁问题
        String redisRepeatKey = mf.code.common.redis.RedisKeyConstant.BACKFILLORDER_ASYNC_REPEAT + orderId;
        boolean successRepeat = RedisForbidRepeat.NX(stringRedisTemplate, redisRepeatKey, 120, TimeUnit.SECONDS);
        if (!successRepeat) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "订单已填写处理中...");
        }

        // redis异步处理返回结果
        String result = stringRedisTemplate.opsForValue().get(mf.code.common.redis.RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId);
        if (StringUtils.isNotBlank(result)) {
            return JSONObject.parseObject(result, SimpleResponse.class);
        }

        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            stringRedisTemplate.delete(redisRepeatKey);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "店铺错误");
        }

        // 如果同一订单已经被回填过就不能再次领取红包
        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", merchantShop.getMerchantId());
        userTaskParams.put("shopId", shopId);
        userTaskParams.put("userId", userId);
        userTaskParams.put("type", UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTaskParams.put("orderId", orderId);
        int countUserTask = userTaskService.countUserTask(userTaskParams);
        if (countUserTask > 0) {
            stringRedisTemplate.delete(redisRepeatKey);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该订单已完成回填确认，请填写其他订单");
        }

        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            stringRedisTemplate.delete(redisRepeatKey);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "用户未授权");
        }

        //异步结果key
        String redisResultKey = mf.code.common.redis.RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId;
        SimpleResponse simpleResponse = new SimpleResponse();
        Map resp = new HashMap();
        resp.put("status", 1);
        simpleResponse.setData(resp);
        stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), 30, TimeUnit.MINUTES);
        //开始异步处理
        asyncFillOrderService.dealwith(req.getOrderId(), merchantShop.getMerchantId(), merchantShop.getId(), req);

        return new SimpleResponse();
    }

    @Override
    public SimpleResponse backfillOrderPopup(BackFillOrderReq backFillOrderReq) {

        /*
         * 1、调用喜销宝
         * 2、校验返回结果
         * 3、抽红包
         * popStatus 1免单红包金额+好评任务弹窗 2免单红包金额+免单商品任务提示弹窗 3免单商品任务提示
         */
        SimpleResponse response = new SimpleResponse();
        if (!StringUtils.isNumeric(backFillOrderReq.getUserId())
                || StringUtils.isBlank(backFillOrderReq.getOrderId())
                || !StringUtils.isNumeric(backFillOrderReq.getShopId())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("请求参数错误");
            return response;
        }
        Long userId = Long.valueOf(backFillOrderReq.getUserId());
        String orderId = backFillOrderReq.getOrderId();
        Long shopId = Long.valueOf(backFillOrderReq.getShopId());
        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            response.setMessage("用户未授权");
            return response;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("店铺错误");
            return response;
        }

        // 如果同一订单已经被回填过就不能再次领取红包
        QueryWrapper<ActivityOrder> aoWrapper = new QueryWrapper<>();
        aoWrapper.lambda()
                .eq(ActivityOrder::getShopId, shopId)
                .eq(ActivityOrder::getUserId, userId)
                .eq(ActivityOrder::getOrderId, orderId);
        List<ActivityOrder> activityOrders = activityOrderService.list(aoWrapper);
        if (!CollectionUtils.isEmpty(activityOrders)) {
            log.info("该订单已完成回填确认，请填写其他订单");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            response.setMessage("该订单已完成回填确认，请填写其他订单");
            return response;
        }
        // 校验所填订单号
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantShop.getMerchantId());
        if (luckCrmCaller.isError(orderInfo)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            response.setMessage("订单填写有误");
            return response;
        }
        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date date = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (date.getTime() < DateUtil.threeMouthBefore().getTime()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            response.setMessage("该订单已失效");
            return response;
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            response.setMessage("您的订单还未确认收货");
            return response;
        }
        // 检查任务
        JSONArray numIids = jsonObject.getJSONArray("num_iid");
        if (CollectionUtils.isEmpty(numIids)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            response.setMessage("该订单数据错误");
            return response;
        }

        // 以上用户订单校验通过，进入业务主流程

        // 校验该订单号 是否在 商家在后台创建的商品表里
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("popStatus", 3);
        resultMap.put("amount", 0.00);
        Long activityDefId = null;
        // 获取免单商品抽奖活动定义
        QueryWrapper<ActivityDef> adWrapper = new QueryWrapper<>();
        adWrapper.lambda()
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.ORDER_BACK.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.list(adWrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            log.info("没有免单抽奖活动");
        } else {
            activityDefId = activityDefs.get((int) (Math.random() * activityDefs.size())).getId();
            resultMap.put("activityDefId", activityDefId);
        }
        // 返回结果
        if (activityDefId != null) {
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + userId, activityDefId.toString(), 1, TimeUnit.DAYS);
        }

        Map<String, Object> params = new HashMap<>();
        params.put("shopId", shopId);
        params.put("numIids", numIids);
        params.put("del", DelEnum.NO.getCode());
        List<Goods> goodsList = goodsService.pageList(params);
        // 查询商品,如果商品为空则抽取红包
        // 首页扫码回填订单，不参与活动认领，不创建活动
        Map<String, Goods> goodsIdMap = new HashMap<>();
        Set<Long> goodsIdSet = new HashSet<>();
        if (!CollectionUtils.isEmpty(goodsList)) {
            for (Goods goods : goodsList) {
                goodsIdMap.put(goods.getXxbtopNumIid(), goods);
                goodsIdSet.add(goods.getId());
            }
        } else {
            // 如果存在不存在的商品，则向喜销宝查询
            for (Object numiid : numIids) {
                String xxbResult = taobaoVerifyService.getGoodsInfo(numiid.toString(), merchantShop.getMerchantId());
                if (luckCrmCaller.isError(xxbResult)) {
                    continue;
                }
                Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
                if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() <= 1) {
                    continue;
                }
                // 组装参数 名称-主图-详情图-价格
                Goods goods = new Goods();
                goods.setXxbtopTitle(xxbMap.get("title").toString());
                goods.setDisplayGoodsName(xxbMap.get("title").toString());
                goods.setPicUrl(xxbMap.get("pic_url").toString());
                goods.setXxbtopPrice(xxbMap.get("price").toString());
                goods.setDisplayPrice(new BigDecimal(xxbMap.get("price").toString()));
                goods.setXxbtopNumIid(numiid.toString());
                goodsIdMap.put(numiid.toString(), goods);
            }
        }
        if (CollectionUtils.isEmpty(goodsIdMap)) {
            log.info("后台和淘宝没有此商品信息");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            resultMap.put("popStatus", 3);
            response.setData(resultMap);
            response.setMessage("回填订单红包已经被抢光了，下次早点来哦");
        }

        // 查看该商品 有无 回填订单红包 任务
        QueryWrapper<ActivityDef> atWrapper = new QueryWrapper<>();
        atWrapper.lambda()
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        List<ActivityDef> activityDefList = activityDefService.list(atWrapper);
        if (CollectionUtils.isEmpty(activityDefList) || activityDefList.get(0).getDeposit().compareTo(BigDecimal.ZERO) <= 0) {
            log.info("该商品 没有回填订单红包任务");
            for (Goods goods : goodsIdMap.values()) {
                updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, goods, jsonObject);
            }
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            resultMap.put("popStatus", 3);
            response.setData(resultMap);
            response.setMessage("回填订单红包已经被抢光了，下次早点来哦");
            return response;
        }
        ActivityDef activityDef = activityDefList.get(0);

        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("shopId", shopId);
        userTaskParams.put("userId", userId);
        userTaskParams.put("type", UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTaskParams.put("orderId", orderId);
        int countUserTask = userTaskService.countUserTask(userTaskParams);
        if (countUserTask > 0) {
            log.info("该订单已完成回填确认，请填写其他订单");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            response.setMessage("该订单已完成回填确认，请填写其他订单");
            return response;
        }
        // 创建userActivty
        UserActivity userActivity = new UserActivity();
        if (!CollectionUtils.isEmpty(goodsList)) {
            userActivity.setGoodsId(goodsList.get(0).getId());
        }
        userActivity.setUserId(userId);
        userActivity.setMerchantId(merchantShop.getMerchantId());
        userActivity.setShopId(shopId);
        userActivity.setActivityId(0L);
        userActivity.setType(UserActivityTypeEnum.FILL_BACK_ORDER.getCode());
        userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        userActivity.setActivityDefId(activityDef.getId());
        int rows = userActivityService.insertUserActivity(userActivity);
        if (rows == 0) {
            log.error("回填订单失败");
            return goodCommentTask(shopId, userId, goodsIdSet, orderId, resultMap);
        }

        BigDecimal realAmount = appletUserActivityV2Service.calcCommission(jsonObject, activityDef);
        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, shopId, UserCouponTypeEnum.ORDER_RED_PACKET, realAmount);
        commissionDisDto.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userId, shopId, merchantShop.getMerchantId());
        resultMap.putAll(BeanMapUtil.beanToMap(commissionResp));

        UserTask userTask = new UserTask();
        userTask.setUserId(userId);
        userTask.setActivityDefId(activityDef.getId());
        if (!CollectionUtils.isEmpty(goodsList)) {
            userTask.setGoodsId(goodsList.get(0).getId());
        }
        // 直接通过发放奖励
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        // 设置默认值
        userTask.setActivityId(0L);
        userTask.setUserActivityId(userActivity.getId());
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setMerchantId(merchantShop.getMerchantId());
        userTask.setShopId(shopId);
        userTask.setType(UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTask.setOrderId(orderId);
        Date now = new Date();
        userTask.setStartTime(now);
        userTask.setReceiveTime(now);
        userTask.setAuditingTime(now);
        userTask.setCtime(now);
        userTask.setUtime(now);
        userTaskService.insertUserTask(userTask);
        for (Goods goods : goodsIdMap.values()) {
            updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, goods, jsonObject);
        }

        resultMap.put("amount", commissionBO.getCommission());
        resultMap.put("commission", commissionBO.getCommission());
        resultMap.put("popStatus", 2);
        return goodCommentTask(shopId, userId, goodsIdSet, orderId, resultMap);
    }

    @Override
    public SimpleResponse backfillOrderPopupAsync(BackFillOrderReq backFillOrderReq) {
        /*
         * 1、调用喜销宝
         * 2、校验返回结果
         * 3、抽红包
         * popStatus 1免单红包金额+好评任务弹窗 2免单红包金额+免单商品任务提示弹窗 3免单商品任务提示
         */
        if (!StringUtils.isNumeric(backFillOrderReq.getUserId())
                || StringUtils.isBlank(backFillOrderReq.getOrderId())
                || !StringUtils.isNumeric(backFillOrderReq.getShopId())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请求参数错误");
        }
        Long userId = Long.valueOf(backFillOrderReq.getUserId());
        String orderId = backFillOrderReq.getOrderId();
        Long shopId = Long.valueOf(backFillOrderReq.getShopId());

        // redis防重拦截,120秒。由于是订单号做主键，所以暂不管异常无法解锁问题
        String redisRepeatKey = mf.code.common.redis.RedisKeyConstant.BACKFILLORDER_ASYNC_REPEAT + orderId;
        boolean successRepeat = RedisForbidRepeat.NX(stringRedisTemplate, redisRepeatKey, 120, TimeUnit.SECONDS);
        if (!successRepeat) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "订单已填写处理中...");
        }

        // redis异步处理返回结果
        String redisResultKey = mf.code.common.redis.RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId;
        String result = stringRedisTemplate.opsForValue().get(redisResultKey);
        if (StringUtils.isNotBlank(result)) {
            return JSONObject.parseObject(result, SimpleResponse.class);
        }

        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "用户未授权");
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "店铺错误");
        }

        // 如果同一订单已经被回填过就不能再次领取红包
        QueryWrapper<ActivityOrder> aoWrapper = new QueryWrapper<>();
        aoWrapper.lambda()
                .eq(ActivityOrder::getShopId, shopId)
                .eq(ActivityOrder::getUserId, userId)
                .eq(ActivityOrder::getOrderId, orderId);
        List<ActivityOrder> activityOrders = activityOrderService.list(aoWrapper);
        if (!CollectionUtils.isEmpty(activityOrders)) {
            log.info("该订单已完成回填确认，请填写其他订单");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该订单已完成回填确认，请填写其他订单");
        }

        //异步结果key
        SimpleResponse simpleResponse = new SimpleResponse();
        Map resp = new HashMap();
        resp.put("status", 1);
        simpleResponse.setData(resp);
        stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), 30, TimeUnit.MINUTES);
        //开始异步处理
        asyncFillOrderService.dealWithFillOrderPopupAsync(orderId, merchantShop.getMerchantId(), shopId, userId);
        return new SimpleResponse();
    }


    private SimpleResponse goodCommentTask(Long shopId, Long userId, Set goodsIds, String orderId, Map resultVO) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        if (goodsIds.size() == 0) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        // 校验 商家是否有好评晒图 的红包任务
        QueryWrapper<ActivityTask> atWrapper = new QueryWrapper<>();
        atWrapper.lambda()
                .eq(ActivityTask::getShopId, shopId)
                .in(ActivityTask::getGoodsId, goodsIds)
                .eq(ActivityTask::getType, ActivityTaskTypeEnum.GOOD_COMMENT.getCode())
                .eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
                .eq(ActivityTask::getDel, DelEnum.NO.getCode());
        List<ActivityTask> activityTasks = activityTaskService.list(atWrapper);
        if (CollectionUtils.isEmpty(activityTasks)) {
            log.info("该商品 没有好评晒图红包任务");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        // 校验回填订单红包任务 是否还有库存
        ActivityTask activityTask = activityTasks.get(0);
        if (activityTasks.get(0).getRpStock() <= 0) {
            log.info("好评晒图已经被抢光了，下次早点来哦");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        Long goodsId = activityTask.getGoodsId();

        // 校验是否有 好评晒图的红包任务 资格

        QueryWrapper<UserTask> userTaskParams = new QueryWrapper<>();
        userTaskParams.lambda()
                .eq(UserTask::getShopId, shopId)
                .eq(UserTask::getGoodsId, goodsId)
                .eq(UserTask::getUserId, userId)
                .eq(UserTask::getType, UserTaskTypeEnum.GOOD_COMMENT.getCode())
                .ne(UserTask::getStatus, UserTaskStatusEnum.AWARD_EXPIRE.getCode());
        int countUserTask = userTaskService.count(userTaskParams);
        if (countUserTask > 0) {
            log.info("你已参加过好评返现任务");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        resultVO.put("popStatus", 1);
        resultVO.put("orderId", orderId);
        resultVO.put("goodsId", goodsId);
        response.setData(resultVO);
        return response;
    }

    /**
     * 活动订单回填（包括任务订单）
     *
     * @param userId     用户ID
     * @param orderId    订单ID该商品不在免单活动内
     * @param activity   创建活动实例
     * @param goods      商品实例
     * @param jsonObject 喜销宝返回订单详情
     */
    private Integer updateOrCreateActivityOrder(Long userId, String orderId, Long shopId, Activity activity, Goods goods, JSONObject jsonObject) {
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        // 如果同一订单已经被回填过，需要修改补充字段
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(merchantShop.getMerchantId(),
                merchantShop.getId(), userId, orderId);
        if (CollectionUtils.isEmpty(activityOrders)) {
            return createActivityOrder(userId, orderId, merchantShop, activity, goods, jsonObject);
        }
        ActivityOrder activityOrder = activityOrders.get(0);
        copyParamToActivityOrder(activity, goods, jsonObject, activityOrder);
        return activityOrderService.updateActivityOrder(activityOrder);
    }

    /**
     * 活动订单回填（包括任务订单）
     *
     * @param userId     用户ID
     * @param orderId    订单ID
     * @param activity   创建活动实例
     * @param goods      商品实例
     * @param jsonObject 喜销宝返回订单详情
     */
    private Integer createActivityOrder(Long userId, String orderId, MerchantShop merchantShop, Activity activity, Goods goods, JSONObject jsonObject) {

        ActivityOrder activityOrder = new ActivityOrder();
        activityOrder.setUserId(userId);
        activityOrder.setOrderId(orderId);
        activityOrder.setMerchantId(merchantShop.getMerchantId());
        activityOrder.setShopId(merchantShop.getId());
        copyParamToActivityOrder(activity, goods, jsonObject, activityOrder);
        activityOrder.setCtime(new Date());
        activityOrder.setUtime(new Date());
        return activityOrderService.createActivityOrder(activityOrder);
    }

    /**
     * 拷贝参数
     *
     * @param activity
     * @param goods
     * @param jsonObject
     * @param activityOrder
     */
    private void copyParamToActivityOrder(Activity activity, Goods goods, JSONObject jsonObject, ActivityOrder activityOrder) {
        if (activity != null) {
            activityOrder.setActivityId(activity.getId());
            activityOrder.setActivityDefId(activity.getActivityDefId());
        }
        if (goods != null) {
            activityOrder.setDisplayGoodsBanner(goods.getDisplayBanner());
            activityOrder.setDisplayGoodsName(goods.getDisplayGoodsName());
            activityOrder.setDisplayGoodsPrice(goods.getDisplayPrice());
            activityOrder.setGoodsId(goods.getId());
            activityOrder.setPicUrl(goods.getPicUrl());
            activityOrder.setTitle(goods.getXxbtopTitle());
        }
        if (jsonObject != null) {
            activityOrder.setSrcType(GoodsSrcTypeEnum.TAOBAO.getCode());
            activityOrder.setTradeStatus(jsonObject.getString("status"));
            activityOrder.setXxbtopRes(jsonObject.toJSONString());
            activityOrder.setStartTime(DateUtil.stringtoDate(jsonObject.getString("created"), DateUtil.FORMAT_ONE));
            activityOrder.setPayTime(DateUtil.stringtoDate(jsonObject.getString("pay_time"), DateUtil.FORMAT_ONE));
            activityOrder.setPayment(new BigDecimal(jsonObject.getString("payment")));
            if (!JsonParseUtil.booJsonArr(jsonObject.get("num_iid").toString())) {
                return;
            }
            JSONArray numIids = JSONArray.parseArray(jsonObject.get("num_iid").toString());
            activityOrder.setNumIid(jsonObject.get("num_iid").toString());
            // 这里比较恶心，目前只取了第一条
            MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityOrder.getShopId());
            String xxbResult = taobaoVerifyService.getGoodsInfo(numIids.get(0).toString(), merchantShop.getMerchantId());
            if (luckCrmCaller.isError(xxbResult)) {
                return;
            }
            Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
            if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() == 1) {
                return;
            }
            // 名称-主图
            activityOrder.setPicUrl(xxbMap.get("pic_url").toString());
            activityOrder.setTitle(xxbMap.get("title").toString());
        }
    }


    /**
     * 从邀请活动中 点击立即拆开
     *
     * @param userId
     * @param pubUserId
     * @param activityId
     * @return
     */
    private SimpleResponse<Object> openRedPackageActivityFromActivity(Long userId, Long pubUserId, Long activityId, Integer pubJoinScene) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验活动aid
        Activity activity = activityService.findById(activityId);
        if (null == activity) {
            log.error("没有查询到此对应的activity表中数据信息，请检查activityId参数是否异常 to 诗琪 or 金纯");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("未查到此活动");
            return response;
        }
        if (pubUserId == null) {
            pubUserId = activity.getUserId();
        }
        // 用户助力活动
        assistOpenRedPackageActivity(userId, pubUserId, activity);
        // 用户发起活动
        return this.createOpenRedPackageActivity(userId, pubUserId, activity.getActivityDefId(), pubJoinScene);
    }

    private void assistOpenRedPackageActivity(Long userId, Long pubUserId, Activity activity) {
        Long activityId = activity.getId();
        Long activityDefId = activity.getActivityDefId();
        Long condPersionCount = Long.valueOf(activity.getCondPersionCount());
        long redisExpired = activity.getEndTime().getTime() - activity.getStartTime().getTime() + 86400000;

        // 校验用户是否有助力的资格
        boolean assistable = appletUserActivityQV3Service.assistableActivity(userId, activity);
        if (!assistable) {
            log.warn("用户助力资格已用完");
            return;
        }
        // 校验活动是否 可助力
        boolean validity = activityService.checkValidity(activity);
        if (!validity) {
            log.warn("活动已开奖 或 已过期，不可助力");
            // 回滚助力资格
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + activityDefId + ":" + userId);
            return;
        }
        // 活动计数+1
        Long trigger = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, redisExpired, TimeUnit.MILLISECONDS);
        if (trigger == null) {
            log.error("redis异常, 用户助力资格回滚");
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + activityDefId + ":" + userId);
            return;
        }
        // 防并发
        if (trigger > condPersionCount) {
            log.warn("活动已开奖，活动计数回滚，用户助力资格回滚");
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + activityDefId + ":" + userId);
            return;
        }
        // 参与或助力活动
        User user = this.joinOrAssistActivity(userId, pubUserId, activity);
        if (user == null) {
            log.error("用户参与或助力活动异常, 活动计数回滚，用户助力资格回滚");
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + activityDefId + ":" + userId);
            return;
        }
        // 加工用户参与活动的信息 并存入redis
        this.processOpenRedPackageJoin(activity.getUserId(), user, activity);
        // 判断活动是否开奖
        if (trigger.equals(condPersionCount)) {
            // 拆红包活动 开奖
            boolean success = this.distributePrizes4OpenRedPackage(user, activity);
            if (!success) {
                log.error("拆红包活动 开奖失败");
                stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, -1);
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + activityDefId + ":" + userId);
                return;
            }
        }
    }

    private boolean distributePrizes4OpenRedPackage(User user, Activity activity) {
        Long activityId = activity.getId();
        Long userId = activity.getUserId();

        // 从redis中 获取拆红包记录 入库
        saveUserCoupon4OpenRedPackageAssister(activity);
        // 调用 通用开奖方法
        boolean success = this.distributePrizes(activity);
        if (!success) {
            return false;
        }
        // 加入 拆红包 轮播弹幕
        this.openRedPackageWinner(user, activity);
        // 发起者 中奖弹框
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_DIALOG + activityId + ":" + userId, "1");
        return true;
    }

    private void openRedPackageWinner(User user, Activity activity) {
        String nickName = user.getNickName();
        Long activityDefId = activity.getActivityDefId();

        // 处理中奖者名字
        char[] chars = nickName.toCharArray();
        if (chars.length <= 1) {
            nickName = "**" + nickName;
        } else {
            nickName = chars[0] + "**" + chars[chars.length - 1];
        }
        // **H 领取到5元现金
        Map<String, Object> winner = new HashMap<>();
        winner.put("userId", user.getId());
        winner.put("nickName", nickName);
        winner.put("amount", activity.getPayPrice());
        // 分页存入redis
        String winners = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId);
        if (StringUtils.isBlank(winners)) {
            List<Map<String, Object>> winnerList = new ArrayList<>();
            winnerList.add(winner);
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId, JSON.toJSONString(winnerList), 365, TimeUnit.DAYS);
            return;
        }
        List<Map> winnerList = JSON.parseArray(winners, Map.class);
        winnerList.add(winner);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId, JSON.toJSONString(winnerList), 365, TimeUnit.DAYS);
    }

    // 从redis中 获取拆红包记录 入库
    private void saveUserCoupon4OpenRedPackageAssister(Activity activity) {
        Long activityId = activity.getId();
        // 活动参与人 入库
        String assisters = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId);
        if (JsonParseUtil.booJsonArr(assisters)) {
            List<Map> assisterList = JSON.parseArray(assisters, Map.class);
            for (Map assistInfo : assisterList) {
                userCouponService.saveFromOpenRedPackageAssist(activity, UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode(), assistInfo);
            }
        }
    }

    @Override
    public Activity createActivity(Long userId, Long activityDefId) {
        ActivityDef activityDef = activityDefService.getById(activityDefId);
        if (null == activityDef) {
            return null;
        }
        // 校验活动定义 是否能正常发起活动, 2019-03-21将扣佣金步骤置后。
        boolean validity = activityDefCheckService.checkValidity(activityDef.getId(), activityDef, true);
        if (!validity) {
            log.warn("库存不足");
            return null;
        }
        // 创建的活动入库
        Activity activity = activityService.saveFromActivityDef(userId, activityDef);
        if (activity == null) {
            Integer updateRows = activityDefService.addDeposit(activityDef.getId(), BigDecimal.ZERO, 1);
            if (updateRows == 0) {
                log.error("紧急：创建活动失败，退库存失败，请手动补偿数据库库存");
            }
            return null;
        }
        // 用户参与活动记录 入库 （发起者）
        UserActivity userActivity = userActivityService.saveFromActivity(userId, activity, true);
        if (userActivity == null) {
            Integer updateRows = activityDefService.addDeposit(activityDef.getId(), BigDecimal.ZERO, 1);
            if (updateRows == 0) {
                log.error("紧急：创建用户活动记录失败，退库存失败，请手动补偿数据库库存");
            }
            return null;
        }
        return activity;
    }

    @Override
    public User joinOrAssistActivity(Long userId, Long pubUserId, Activity activity) {
        Long activityId = activity.getId();
        // 获取用户信息
        User user = userService.getById(userId);
        if (user == null) {
            return null;
        }
        // 更新用户邀请记录
        if (pubUserId != null) {
            userPubJoinService.updatePubJoinSuccess(pubUserId, userId, activityId);
        }
        // 用户参与活动记录入库
        UserActivity userActivity = userActivityService.saveFromActivity(userId, activity, false);
        if (userActivity == null) {
            return null;
        }

        return user;
    }

    @Override
    public boolean distributePrizes(Activity activity) {
        // 更新活动至开奖
        boolean update = activityService.updateAwardStartTime(activity);
        if (!update) {
            return false;
        }
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
		/*// 活动发起人 入库
		UserCoupon userCoupon = userCouponService.saveFromActivity(activity, null);*/
        // 发奖
        this.addUserBalance(activity);
		/*// 更新用户至已领奖状态
		userCouponService.updateAward(userCoupon);*/

        CommissionDisDto commissionDisDto = new CommissionDisDto(activity.getUserId(), activity.getShopId(),
                UserCouponTypeEnum.findByCode(userCouponService.getUserCouponTypeFromActivityType(activity.getType())),
                activity.getPayPrice());
        commissionDisDto.setActivityId(activity.getId());
        commissionDisDto.setActivityDefId(activity.getActivityDefId());
        commissionService.commissionDistribution(commissionDisDto);
        // 发送消息模板
        this.sendTemplateMessageService.sendTemplateMsg2Award(activity.getShopId(), activity.getId(), null);

        return true;
    }

    @Override
    public void addUserBalance(Activity activity) {
        Long activityId = activity.getId();
        Long userId = activity.getUserId();
        Long merchantId = activity.getMerchantId();
        Long shopId = activity.getShopId();
        BigDecimal payPrice = activity.getPayPrice();
        // 更新用户活动记录至中奖
        userActivityService.update2WinnerFromActivity(activity);
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
		/*// 创建奖金订单
		UpayWxOrder upayWxOrder = upayWxOrderService.addPo(userId,
				OrderPayTypeEnum.CASH.getCode(),
				OrderTypeEnum.PALTFORMRECHARGE.getCode(),
				null,
				"拆红包金额",
				merchantId,
				shopId,
				OrderStatusEnum.ORDERED.getCode(),
				payPrice,
				IpUtil.getServerIp(),
				null,
				"拆红包金额",
				new Date(),
				null,
				null,
				BizTypeEnum.OPEN_RED_PACKAGE.getCode(),
				activityId);
		this.upayWxOrderService.create(upayWxOrder);
		// 对用户余额进行操作
		upayBalanceService.updateOrCreate(merchantId,
				shopId,
				userId,
				payPrice);*/
    }

    /**
     * 发起自己的拆红包活动
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    private SimpleResponse<Object> createOpenRedPackageActivity(Long userId, Long pubUserId, Long activityDefId, Integer pubJoinScene) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        // 校验用户发起活动的资格
        Activity activity = appletUserActivityQV3Service.createableActivity(userId, activityDefId);
        if (activity != null) {
            return appletUserActivityQV3Service.openRedPackageStatus(activity, 1, pubJoinScene);
        }
        // 获取用户信息
        User user = userService.getById(userId);
        if (user == null) {
            log.error("用户不合法");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("用户不合法");
            return response;
        }
        // 发起活动
        Activity newActivity = this.createActivity(userId, activityDefId);
        if (newActivity == null) {
            log.info("活动发起失败");
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("activityStatus", 3);
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("红包已被领取光啦，下次早点来！");
            response.setData(resultVO);
            return response;
        }
        // 处理活动状态
        Long activityId = newActivity.getId();
        long redisExpired = newActivity.getEndTime().getTime() - newActivity.getStartTime().getTime() + 86400000;

        // 活动计数加1
        stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, redisExpired, TimeUnit.MILLISECONDS);
        // 初始化奖池金额
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId, BigDecimal.ZERO.toString());
        // 拆红包算法
        List<BigDecimal> amountList = this.listAssistAmount(newActivity.getCondPersionCount(), newActivity.getPayPrice());
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId, JSON.toJSONString(amountList), redisExpired, TimeUnit.MILLISECONDS);
        // 加工用户参与拆红包活动的信息并存入redis
        this.processOpenRedPackageJoin(pubUserId, user, newActivity);

        // 获取拆红包活动详情数据
        Map<String, Object> resultVO = appletUserActivityQV3Service.queryOpenRedPackageActivity(newActivity);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    /**
     * 加工用户参与拆红包活动的信息并存入redis
     *
     * @param pubUserId
     * @param user
     * @param activity
     */
    private void processOpenRedPackageJoin(Long pubUserId, User user, Activity activity) {
        Long activityId = activity.getId();
        Long userId = activity.getUserId();
        long redisExpired = activity.getEndTime().getTime() - activity.getStartTime().getTime() + 86400000;

        // 获取活动的金额列表
        String amounts = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId);
        // 获取此次参与活动的金额列表下标
        int index = Integer.parseInt(stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_TRIGGER + activityId)) - 1;
        List amountList = JSON.parseObject(amounts, List.class);
        if (CollectionUtils.isEmpty(amountList) && index == 0) {
            // 拆红包算法
            amountList = this.listAssistAmount(activity.getCondPersionCount(), activity.getPayPrice());
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + activityId, JSON.toJSONString(amountList), redisExpired, TimeUnit.MILLISECONDS);
        }
        //本次获奖金额
        BigDecimal amount = (BigDecimal) amountList.get(index);

        // 更新活动奖池金额
        String awardPool = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId);
        BigDecimal totalAmount = BigDecimal.ZERO;
        if (StringUtils.isNotBlank(awardPool)) {
            totalAmount = new BigDecimal(awardPool);
        }
        totalAmount = totalAmount.add(amount);
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + activityId, totalAmount.toString(), redisExpired, TimeUnit.MILLISECONDS);

        // 加工用户信息
        Map<String, Object> userInfo = appletUserActivityQV3Service.processUserInfoVO4OpenRedPackageActivity(user, new Date(), amount);

        if (!stringRedisTemplate.opsForHash().hasKey(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), userId.toString())) {
            // 发起者
            userInfo.put("pubUserId", pubUserId);
            userInfo.put("awardPool", totalAmount);
            userInfo.put("activityId", activityId);
            stringRedisTemplate.opsForHash().put(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), userId.toString(), JSON.toJSONString(userInfo));
            stringRedisTemplate.expire(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), 180, TimeUnit.DAYS);
        } else {
            // 助力者弹框
            String assistDialogs = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
            List<Map> assistDialogList = new ArrayList<>();
            if (!JsonParseUtil.booJsonArr(assistDialogs)) {
                assistDialogList.add(userInfo);
            } else {
                assistDialogList = JSON.parseArray(assistDialogs, Map.class);
                assistDialogList.add(userInfo);
            }
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId, JSON.toJSONString(assistDialogList));
            stringRedisTemplate.expire(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId, redisExpired, TimeUnit.MILLISECONDS);

            // 第一处：助力者统计 tip:此处 发起人不放入助力者列表 assistVOList, 可放置 第二处
            String assisters = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId);
            List<Map> assisterList = new ArrayList<>();
            if (!JsonParseUtil.booJsonArr(assisters)) {
                assisterList.add(userInfo);
            } else {
                assisterList = JSON.parseArray(assisters, Map.class);
                assisterList.add(userInfo);
            }
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId, JSON.toJSONString(assisterList));
            stringRedisTemplate.expire(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId, redisExpired, TimeUnit.MILLISECONDS);
        }
        // 第二出：助力者统计 tip:此处 发起人 放入助力者列表 assistVOList， 可放置第一处

        // 更新活动奖池金额
        Map startUserVO;
        Object startUser = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), userId.toString());
        if (startUser != null) {
            startUserVO = JSON.parseObject(startUser.toString(), Map.class);
            startUserVO.put("awardPool", totalAmount);
            stringRedisTemplate.opsForHash().put(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), userId.toString(), JSON.toJSONString(startUserVO));
            stringRedisTemplate.expire(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activity.getActivityDefId().toString(), 180, TimeUnit.DAYS);
        }
    }

    /**
     * 拆红包核心算法
     *
     * @param condPersionCount
     * @param amount
     * @return
     */
    private List<BigDecimal> listAssistAmount(Integer condPersionCount, BigDecimal amount) {
        // 加入财富大闯关后， 调整帮拆金额
        CommonDict commonDict = commonDictService.selectByTypeKey("checkpoint", "rate");
        String rate = commonDict.getValue();
        amount = amount.multiply(new BigDecimal(100 - Integer.parseInt(rate))).divide(new BigDecimal(100), 2, RoundingMode.DOWN);


        // 后部分人数
        int n = condPersionCount / 2;
        // 每人平均金额
        BigDecimal averAmount = amount.divide(new BigDecimal(condPersionCount), 2, RoundingMode.DOWN);
        // 对下一人抽取时，相对上一人金额刻度的 偏移量
        int vi = 1;
        // 抽取金额的随机范围（金额刻度）
        int size = 4;
        // 金额的随机取值范围和偏移的金额刻度之间的重合部分大小（金额刻度）
        int overSize = size - vi;
        //金额刻度长度为：
        int len = (n - 1) * vi + size;
        // 每人平均金额下再按人数划分的金额刻度
        BigDecimal scaleAmount = averAmount.divide(new BigDecimal(len), 2, RoundingMode.DOWN);

        LinkedList<BigDecimal> amountList = new LinkedList<>();
        // 计算 金额列表 并存入redis,List类型
        // 从中间开始计算随机金额，前一半使用leftPush, 后一半使用rightPush
        if (condPersionCount % 2 != 0) {
            amountList.addFirst(averAmount);
        }
        for (int i = 0; i < n; i++) {
            BigDecimal stepAmount = scaleAmount.multiply(new BigDecimal(Math.random() * size + vi * i));
            stepAmount = stepAmount.setScale(2, RoundingMode.DOWN);
            amountList.addLast(averAmount.subtract(stepAmount));
            amountList.addFirst(averAmount.add(stepAmount));
        }
        // 矫正 资金总额
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (BigDecimal unitAmount : amountList) {
            totalAmount = totalAmount.add(unitAmount);
        }
        BigDecimal leftAmount = amount.subtract(totalAmount);
        BigDecimal firstAmount = amountList.pollFirst();
        firstAmount = firstAmount.add(leftAmount);
        amountList.addFirst(firstAmount);
        return amountList;
    }
}
