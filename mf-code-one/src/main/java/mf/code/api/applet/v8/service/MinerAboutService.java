package mf.code.api.applet.v8.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.v8.dto.MyMinerV8Dto;
import mf.code.api.applet.v8.dto.NewbieTask;
import mf.code.common.repo.po.CommonDict;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 10:40
 */
public interface MinerAboutService {

    /***
     * 获取闯关地图页面的弹窗信息
     * @param user 用户
     * @param userPubJoin 用户参与对象信息
     * @param allMinerTotalScottare 所有下级矿工们的缴税收益
     * @param commonDicts 关卡字典表
     * @return
     */
    List<MyMinerV8Dto.DiaLogInfo> getCheckpointDialog(Long shopId,
                                                      User user,
                                                      UserPubJoin userPubJoin,
                                                      BigDecimal allMinerTotalScottare,
                                                      List<CommonDict> commonDicts);

    /***
     * 获取累计缴税
     * @param shopId
     * @param userId
     * @return
     */
    List<UserPubJoin> getUserPubJoin(Long shopId, Long userId, Long bossUserId);

    /***
     * 获取下级人数
     * @param shopId
     * @param userId
     * @return
     */
    List<UserPubJoin> getUserPubJoinMinerNum(Long shopId, Long userId, Long bossUserId);

    /***
     * 完成新手任务的进度情况
     * @return
     */
    List<NewbieTask> queryFinishProgressInfo(User user);

    /***
     * 获取矿工信息
     * @param userIds
     * @return
     */
    List<User> getUser(List<Long> userIds);

    /***
     * 获取详细
     * @param merchantId
     * @param shopId
     * @param userId
     * @param subUserIds
     * @param offset
     * @param size
     * @return
     */
    IPage<UserCoupon> getUserCoupon(Long merchantId, Long shopId, Long userId, List<Long> subUserIds, int offset, int size);
}
