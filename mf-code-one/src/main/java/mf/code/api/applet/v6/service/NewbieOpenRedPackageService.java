package mf.code.api.applet.v6.service;

import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;

/**
 * mf.code.api.applet.v6.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-06-21 11:14
 */
public interface NewbieOpenRedPackageService {
	/**
	 * 是否是新手任务-拆红包
	 * @param userId
	 * @return
	 */
	boolean isCompleteNewbieOpenRedPackage(Long userId);

	/**
	 * 是否存在新手任务
	 * @param shopId
	 * @return
	 */
	ActivityDef hasExistNewbieTask(Long shopId);

	/**
	 * 是否有进行中的 新人任务-拆红包
	 * @param userId
	 * @return true 有; false 没有
	 */
	Activity hasPublishedActivityByUserId(Long userId);

	/**
	 * 获取 绑定的 拆红包活动定义
	 * @param id
	 * @return
	 */
	ActivityDef findNewbieOpenRedPackageByParentId(Long id);

	/**
	 * 最新一条 记录
	 * @param userId
	 * @return
	 */
	Activity lastNewbieOpenRedPackage(Long userId);
}
