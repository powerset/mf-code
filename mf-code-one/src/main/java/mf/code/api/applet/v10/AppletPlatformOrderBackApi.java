package mf.code.api.applet.v10;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v10.service.AppletPlatformOrderBackService;
import mf.code.common.job.TimeCreateActivityScheduledService;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.one.constant.ActivityApiEntrySceneEnum;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.v10
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 11:53
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v10/userActivity")
public class AppletPlatformOrderBackApi {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AppletPlatformOrderBackService appletPlatformOrderBackService;

    /**
     * 平台免单抽奖 -- 参与
     *
     * @param activityId
     * @param userId
     * @return
     */
    @GetMapping("/joinPlatformOrderBack")
    public SimpleResponse assistFullReimbursement(@RequestParam("userId") String userId,
                                                  @RequestParam("shopId") String shopId,
                                                  @RequestParam("pubUserId") String pubUserId,
                                                  @RequestParam("activityId") String activityId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(activityId) || !RegexUtils.StringIsNumber(activityId)) {
            log.error("参数异常, userId = {}, activityId = {}", userId, activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        // 防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "点击太快，请稍候再试");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("pubUserId", pubUserId);
        params.put("shopId", shopId);
        params.put("activityId", activityId);
        params.put("type", ActivityApiEntrySceneEnum.ASSIST.getCode());

        SimpleResponse result = appletPlatformOrderBackService.join(params);
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
        return result;
    }

    /**
     * 平台免单抽奖 -- 活动详情
     *
     * @param activityId
     * @param userId
     * @return
     */
    @GetMapping("/queryActivity")
    public SimpleResponse queryActivity(@RequestParam("userId") String userId,
                                        @RequestParam("shopId") String shopId,
                                        @RequestParam("activityId") String activityId,
                                        @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                        @RequestParam(name = "size", required = false, defaultValue = "16") int size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(activityId) || !RegexUtils.StringIsNumber(activityId)) {
            log.error("参数异常, userId = {}, shopId = {}, activityDefType = {}, scenes = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("shopId", shopId);
        params.put("activityId", activityId);
        params.put("offset", offset);
        params.put("size", size);
        params.put("type", ActivityApiEntrySceneEnum.QUERY.getCode());
        return appletPlatformOrderBackService.queryActivityByParams(params);
    }

    /**
     * 联合营销页
     *
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/sellUnion")
    public SimpleResponse sellUnion(@RequestParam("userId") String userId,
                                    @RequestParam("shopId") String shopId,
                                    @RequestParam(value = "offset", required = false, defaultValue = "1") Long offset,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") Long size) {

        return appletPlatformOrderBackService.sellUnion(offset, size, Long.parseLong(userId), Long.parseLong(shopId));
    }

    /**
     * 查询联合营销页秒杀数据
     *
     * @param userId
     * @param shopId
     * @param batchId
     * @return
     */
    @GetMapping("/unionPageSeckill")
    public SimpleResponse unionPageSeckill(@RequestParam("userId") Long userId,
                                           @RequestParam("shopId") Long shopId,
                                           @RequestParam(value = "batchId", required = false) String batchId,
                                           @RequestParam(value = "dayId", required = false) String datId) {
        return appletPlatformOrderBackService.unionPageSeckill(userId, shopId, batchId, datId);
    }
}
