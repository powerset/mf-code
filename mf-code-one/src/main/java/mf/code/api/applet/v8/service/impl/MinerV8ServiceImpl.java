package mf.code.api.applet.v8.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.AppletMybatisPageDto;
import mf.code.api.applet.v3.dto.MinerDetailResp;
import mf.code.api.applet.v6.service.NewbieOpenRedPackageService;
import mf.code.api.applet.v8.dto.MinerProgressDetailV8Resp;
import mf.code.api.applet.v8.dto.MyMinerV8Dto;
import mf.code.api.applet.v8.dto.NewbieTask;
import mf.code.api.applet.v8.service.MinerAboutService;
import mf.code.api.applet.v8.service.MinerV8Service;
import mf.code.api.feignservice.OneAppServiceImpl;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.upay.service.UpayBalanceService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 10:27
 */
@Service
public class MinerV8ServiceImpl implements MinerV8Service {
    @Autowired
    private UserService userService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private MinerAboutService minerAboutService;
    @Autowired
    private OneAppServiceImpl oneAppService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private NewbieOpenRedPackageService newcomerOpenRedPackageService;
    @Autowired
    private UserTaskService userTaskService;

    /***
     * 闯关页面信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryCheckpointInfo(Long merchantId, Long shopId, Long userId) {
        //本人头像信息
        User user = userService.selectByPrimaryKey(userId);
        //获取本人收益
        List<UserPubJoin> userPubJoins = minerAboutService.getUserPubJoin(shopId, userId, null);
        UserPubJoin userPubJoin = null;
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            userPubJoin = userPubJoins.get(0);
        }
        //获取字典表-税率信息and关卡信息
        List<CommonDict> commonDicts = commonDictService.selectListByType("checkpoint");
        //任务金额+缴税金额
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("shopId", shopId);
        totalScottareParams.put("userId", userId);
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal allMinerTotalScottare = userPubJoinService.sumByTotalScottare(totalScottareParams);

        MyMinerV8Dto myMinerDto = new MyMinerV8Dto();
        myMinerDto.setCheckpoint(new MyMinerV8Dto.Checkpoint());
        //获取税率
        myMinerDto.fromRate(commonDicts);
        //获取所有的关卡
        myMinerDto.fromAllCheckpoint(commonDicts);
        //获取用户信息
        myMinerDto.fromUser(user);
        //获取用户当前关卡的任务收益+缴税收益
        myMinerDto.fromProfit(userPubJoin, allMinerTotalScottare, commonDicts);
        //关卡数，关卡目标金额
        myMinerDto.getCheckpoint().fromCheckpointGoalMoney(commonDicts, userPubJoin, allMinerTotalScottare);
        //弹窗信息
        myMinerDto.setDialogInfo(minerAboutService.getCheckpointDialog(shopId, user, userPubJoin, allMinerTotalScottare, commonDicts));

        //获取各个任务的完成情况
        List<NewbieTask> newbieTasks = minerAboutService.queryFinishProgressInfo(user);
        myMinerDto.setNewbieTasks(newbieTasks);
        //获取矿工数==该用户有多少个下级
        List<UserPubJoin> userPubJoinMiners = minerAboutService.getUserPubJoinMinerNum(null, null, userId);
        if (!CollectionUtils.isEmpty(userPubJoinMiners)) {
            myMinerDto.setMinerNum(userPubJoinMiners.size());
        }
        //获取可提现金额
        myMinerDto.setCashMoney(upayBalanceService.canCashMoney(merchantId, shopId, userId).toString());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(myMinerDto);
        return simpleResponse;
    }

    /**
     * 获取 新人任务（闯关任务） 弹窗
     *
     * @param shopId
     * @param userId
     * @return dialogType: 0.无弹窗; 1.你有Y元现金待领取
     */
    @Override
    public SimpleResponse queryNewcomerTaskPopup(Long shopId, Long userId) {
        Map<String, Object> newcomerTaskInfo = oneAppService.queryNewcomerTaskInfo(shopId, userId);
        if (CollectionUtils.isEmpty(newcomerTaskInfo) || BigDecimal.ZERO.compareTo(new BigDecimal(newcomerTaskInfo.get("newbieAmount").toString())) == 0) {
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("dialogType", 0);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
        }
        BigDecimal newBieAmount = new BigDecimal(newcomerTaskInfo.get("newbieAmount").toString());

        Activity activity = newcomerOpenRedPackageService.hasPublishedActivityByUserId(userId);
        boolean completeNewbieOpenRedPackage = newcomerOpenRedPackageService.isCompleteNewbieOpenRedPackage(userId);

        if (activity != null || completeNewbieOpenRedPackage) {
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("dialogType", 0);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
        }

        QueryWrapper<UserTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserTask::getType, UserTaskTypeEnum.NEWBIE_TASK.getCode())
                .eq(UserTask::getUserId, userId);
        int count = userTaskService.count(wrapper);
        if (count != 0) {
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("dialogType", 0);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
        }

        newcomerTaskInfo.put("dialogType", 1);
        newcomerTaskInfo.put("amount", newBieAmount);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, newcomerTaskInfo);
    }

    /***
     * 用户(矿工角色)的收益明细
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryMinerProgressDetail(Long merchantId, Long shopId, Long userId, int offset, int size) {
        User selfUser = userService.selectByPrimaryKey(userId);
        if (selfUser == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "未查找到该用户");
        }
        boolean shopManager = false;
        if (selfUser.getRole() == 1) {
            shopManager = true;
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        MinerProgressDetailV8Resp minerProgressDetailResp = new MinerProgressDetailV8Resp();
        minerProgressDetailResp.setRole(selfUser.getRole());
        minerProgressDetailResp.setDetail(new AppletMybatisPageDto<MinerProgressDetailV8Resp.MinerProgressDetail>());
        minerProgressDetailResp.getDetail().from(size, offset, 0);
        minerProgressDetailResp.getDetail().setContent(new ArrayList<>());
        //查询自己的收益
        List<UserPubJoin> userPubJoins = minerAboutService.getUserPubJoin(shopId, userId, null);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            simpleResponse.setData(minerProgressDetailResp);
            return simpleResponse;
        }
        //获取下级们总缴税金额
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("shopId", shopId);
        totalScottareParams.put("userId", userId);
        totalScottareParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(totalScottareParams);

        //判断是否成为店长，若没有成为店长，则不差矿工的记录 获取所有的下级矿工们的user_id
        List<Long> subUserIds = new ArrayList<>();
        Map<Long, User> userMap = new HashMap<Long, User>();
        if (shopManager) {
            List<UserPubJoin> subUserPubJoins = minerAboutService.getUserPubJoin(shopId, null, userId);
            if (!CollectionUtils.isEmpty(subUserPubJoins)) {
                for (UserPubJoin userPubJoin : subUserPubJoins) {
                    if (subUserIds.indexOf(userPubJoin.getSubUid()) == -1) {
                        subUserIds.add(userPubJoin.getSubUid());
                    }
                }
            }
            List<User> users = minerAboutService.getUser(subUserIds);
            if (!CollectionUtils.isEmpty(users)) {
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }
        }

        BigDecimal newbieTask_open_red_pack = BigDecimal.ZERO;

        //将自己加上
        subUserIds.add(userId);
        IPage<UserCoupon> userCouponIPage = minerAboutService.getUserCoupon(merchantId, shopId, null, subUserIds, offset, size);
        List<UserCoupon> userCoupons = userCouponIPage.getRecords();
        if (!CollectionUtils.isEmpty(userCoupons)) {
            //获取详细
            for (UserCoupon userCoupon : userCoupons) {
//                if (userCoupon.getType() == UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode() && !userId.equals(userCoupon.getUserId())) {
//                    continue;
//                }
                if (userCoupon.getType() == UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode() && userId.equals(userCoupon.getUserId())) {
                    newbieTask_open_red_pack = userCoupon.getAmount();
                }
                User user = userMap.get(userCoupon.getUserId());
                MinerProgressDetailV8Resp.MinerProgressDetail detail = new MinerProgressDetailV8Resp.MinerProgressDetail();
                detail.from(userCoupon, user, shopManager);
                minerProgressDetailResp.getDetail().getContent().add(detail);
            }
        }
        //获取今日相关数值
        String todayCheckPoint = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + userId);

        //获取今日累计
        minerProgressDetailResp.fromTodayProfit(shopManager, todayCheckPoint);
        //汇总历史累计相关数据
        minerProgressDetailResp.from(newbieTask_open_red_pack, shopManager, userPubJoins.get(0), allMinerTotalScottare);

        int pullDown = 0;
        //是否还有下一页
        if (userCouponIPage.getRecords().size() >= size && userCouponIPage.getPages() > userCouponIPage.getCurrent()) {
            pullDown = 1;
        }
        minerProgressDetailResp.getDetail().from(size, offset, pullDown);
        simpleResponse.setData(minerProgressDetailResp);
        return simpleResponse;
    }

    /***
     * 我的矿工分页
     * @param merchantId
     * @param shopId
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMinerDetail(Long merchantId, Long shopId, Long userId, int offset, int size) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MinerDetailResp minerDetailResp = new MinerDetailResp();
        minerDetailResp.setDetail(new AppletMybatisPageDto<MinerDetailResp.MinerScottareDetail>());
        minerDetailResp.getDetail().from(size, offset, 0);
        minerDetailResp.getDetail().setContent(new ArrayList<>());

        //矿工用户信息
        User user = this.userService.selectByPrimaryKey(userId);
        if (user != null) {
            minerDetailResp.from(user);
        }
        List<UserPubJoin> userPubJoins = minerAboutService.getUserPubJoin(null, userId, null);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
            userPubJoinQueryWrapper.lambda()
                    .eq(UserPubJoin::getType, UserPubJoinTypeEnum.SHOPPINGMALL.getCode())
                    .eq(UserPubJoin::getSubUid, userId)
            ;
            userPubJoins = userPubJoinService.list(userPubJoinQueryWrapper);
            if (CollectionUtils.isEmpty(userPubJoins)) {
                simpleResponse.setData(minerDetailResp);
                return simpleResponse;
            }
        }
        minerDetailResp.from(userPubJoins.get(0));

        IPage<UserCoupon> userCouponIPage = minerAboutService.getUserCoupon(merchantId, shopId, userId, null, offset, size);
        List<UserCoupon> userCoupons = userCouponIPage.getRecords();
        if (CollectionUtils.isEmpty(userCoupons)) {
            minerDetailResp.getDetail().from(size, offset, 0);
            simpleResponse.setData(minerDetailResp);
            return simpleResponse;
        }
        for (UserCoupon userCoupon : userCoupons) {
            MinerDetailResp.MinerScottareDetail detail = new MinerDetailResp.MinerScottareDetail();
            detail.from(userCoupon);
            minerDetailResp.getDetail().getContent().add(detail);
        }
        //获取今日相关数值
        String todayCheckPoint = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + userId);
        if (StringUtils.isNotBlank(todayCheckPoint)) {
            JSONObject jsonObject = JSONObject.parseObject(todayCheckPoint);
            Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
            if (jsonMap != null && jsonMap.get("todayScottare") != null) {
                //今日上交税收
                minerDetailResp.setTodayScottare(new BigDecimal(jsonMap.get("todayScottare").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString());
            }
        }
        int pullDown = 0;
        //是否还有下一页
        if (userCouponIPage.getRecords().size() >= size && userCouponIPage.getPages() > userCouponIPage.getCurrent()) {
            pullDown = 1;
        }
        minerDetailResp.getDetail().from(userCouponIPage.getSize(), offset, pullDown);
        simpleResponse.setData(minerDetailResp);
        return simpleResponse;
    }
}
