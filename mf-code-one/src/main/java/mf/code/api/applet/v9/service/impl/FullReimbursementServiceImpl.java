package mf.code.api.applet.v9.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;
import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.domain.applet.application.impl.AppletAssistActivityServiceAbstractImpl;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.domain.applet.valueobject.ActivityPopupStatusEnum;
import mf.code.activity.domain.applet.valueobject.ScenesEnum;
import mf.code.activity.domain.applet.valueobject.UserBoard;
import mf.code.activity.domain.applet.valueobject.UserBoardConfig;
import mf.code.activity.domain.applet.valueobject.config.JoinRule;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.v9.service.FullReimbursementAboutService;
import mf.code.api.applet.v9.service.FullReimbursementService;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.common.utils.RobotUserUtil;
import mf.code.one.constant.ActivityApiEntrySceneEnum;
import mf.code.one.constant.ActivityDetailStatusEnum;
import mf.code.one.dto.activitydetail.ActivityFullReimbursementDTO;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.constant.UpayWxOrderTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v9.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-02 15:34
 */
@Slf4j
@Service
public class FullReimbursementServiceImpl extends AppletAssistActivityServiceAbstractImpl implements FullReimbursementService {
    @Value("${platformData.fullreimbrusementTotal}")
    private String fullreimbrusementTotal;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private AppletActivityDefRepository appletActivityDefRepository;
    @Autowired
    private JoinRuleActivityRepository joinRuleActivityRepository;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private FullReimbursementAboutService fullReimbursementAboutService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UserService userService;

    /**
     * 每次助力金额的幅度百分比
     */
    private Integer floatRate = 30;

    /**
     * 自定义 活动
     *
     * @param appletActivity
     * @return
     */
    @Override
    protected AppletActivity assembleNewActivity(AppletActivity appletActivity, Map<String, Object> params) {
        BigDecimal amount = new BigDecimal(params.get("amount").toString());

        // 对用户输入的金额进行校验
        JoinRule joinRule = appletActivity.getJoinRule();
        BigDecimal perMinAmount = joinRule.getPerMinAmount();
        BigDecimal perMaxAmount = joinRule.getPerMaxAmount();
        if (perMinAmount.compareTo(amount) > 0 || perMaxAmount.compareTo(amount) < 0) {
            return null;
        }

        Activity activityInfo = appletActivity.getActivityInfo();
        // 设置 用户报销金额
        activityInfo.setDeposit(amount);

        // 计算参与人数
        BigDecimal payPrice = activityInfo.getPayPrice();
        BigDecimal amountCount = amount.divide(payPrice, 0, BigDecimal.ROUND_CEILING);
        Integer condPersionCount = Integer.valueOf(amountCount.toString());
        activityInfo.setCondPersionCount(condPersionCount);

        // 计算活动时间
        Date startTime = activityInfo.getStartTime();
        Date endTime = activityInfo.getEndTime();
        long perTime = endTime.getTime() - startTime.getTime();
        long activityTime = perTime * condPersionCount;
        activityInfo.setEndTime(DateUtils.addSeconds(startTime, (int) (activityTime / 1000)));

        AppletAssistActivity appletAssistActivity = new AppletAssistActivity();
        appletAssistActivity.setId(appletActivity.getId());
        appletAssistActivity.setActivityDefId(appletActivity.getActivityDefId());
        appletAssistActivity.setActivityInfo(activityInfo);
        appletAssistActivity.setUserInfo(appletActivity.getUserInfo());
        appletAssistActivity.setJoinRule(appletActivity.getJoinRule());
        appletAssistActivity.setCreateRule(appletActivity.getCreateRule());
        appletAssistActivity.setTrigger(0L);
        appletAssistActivity.setOpenAmount(BigDecimal.ZERO);
        appletAssistActivity.setScenesEnum(ScenesEnum.ACTIVITY_PAGE);
        appletAssistActivity.setVisitUserId(activityInfo.getUserId());

        UserBoard userBoard = new UserBoard();
        userBoard.setActivityDefId(appletAssistActivity.getActivityDefId());
        appletAssistActivity.setUserBoardConfig(new UserBoardConfig());
        calculationAssistAmountList(appletAssistActivity);

        User userInfo = appletActivity.getUserInfo();
        Map<String, Object> startUserInfo = new HashMap<>();
        startUserInfo.put("userId", userInfo.getId());
        startUserInfo.put("avatarUrl", userInfo.getAvatarUrl());
        startUserInfo.put("nickName", userInfo.getNickName());
        startUserInfo.put("time", DateUtil.dateToString(new Date(), DateUtil.POINT_DATE_FORMAT_TWO));
        startUserInfo.put("amount", BigDecimal.ZERO);

        startUserInfo.put("awardPool", BigDecimal.ZERO);
        appletAssistActivity.setStartUserInfo(startUserInfo);

        return appletAssistActivity;
    }

    /**
     * 计算助力金额
     */
    private void calculationAssistAmountList(AppletAssistActivity appletAssistActivity) {
        if (!CollectionUtils.isEmpty(appletAssistActivity.getAssistAmountList())) {
            return;
        }

        Activity activityInfo = appletAssistActivity.getActivityInfo();
        Integer condPersionCount = activityInfo.getCondPersionCount();
        BigDecimal payPrice = activityInfo.getPayPrice();
        BigDecimal totalAmount = payPrice.multiply(new BigDecimal(condPersionCount));

        List<BigDecimal> assistAmountList = new ArrayList<>();

        if (1 == condPersionCount) {
            assistAmountList = new ArrayList<>();
            assistAmountList.add(totalAmount);
            appletAssistActivity.setAssistAmountList(assistAmountList);
            return;
        }
        // 前几人直接发奖
        Integer hitsPerDraw = activityInfo.getHitsPerDraw();
        Integer size;
        if (hitsPerDraw > condPersionCount) {
            size = hitsPerDraw;
        } else {
            size = condPersionCount;
        }
        BigDecimal currentAmount = BigDecimal.ZERO;
        Integer minRate = 100 - floatRate;
        Integer diff = floatRate * 2;
        BigDecimal balanceAmount = payPrice.multiply(new BigDecimal("2"));
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            if (i % 2 == 0) {
                int diffRate = random.nextInt(diff);
                int currentRate = minRate + diffRate;
                BigDecimal amount = payPrice.multiply(new BigDecimal(currentRate))
                        .divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
                assistAmountList.add(amount);
                currentAmount = currentAmount.add(amount);
            } else {
                // 上一次的金额
                BigDecimal lastAmount = assistAmountList.get(i - 1);
                BigDecimal amount = balanceAmount.subtract(lastAmount);
                assistAmountList.add(amount);
                currentAmount = currentAmount.add(amount);
            }
        }

        BigDecimal reimbursementAmount = activityInfo.getDeposit();
        // 报销值矫正
        BigDecimal lastAmount = assistAmountList.get(size - 1);
        assistAmountList.remove(size - 1);
        assistAmountList.add(reimbursementAmount.subtract(currentAmount).add(lastAmount));
        appletAssistActivity.setAssistAmountList(assistAmountList);
    }

    /**
     * 自定义活动页面交互
     *
     * @param appletActivity
     * @return
     */
    @Override
    protected SimpleResponse activityPageVO(AppletActivity appletActivity, Map<String, Object> params) {
        Long userId = null;
        if (params.get("userId") != null) {
            userId = NumberUtils.toLong(params.get("userId").toString());
        }
        int offset = 0;
        if (params.get("offset") != null) {
            offset = NumberUtils.toInt(params.get("offset").toString());
        }
        int size = 0;
        if (params.get("limit") != null) {
            size = NumberUtils.toInt(params.get("limit").toString());
        }
        int type = 0;
        if (params.get("type") != null) {
            type = NumberUtils.toInt(params.get("type").toString());
        }
        AppletAssistActivity appletAssistActivity = (AppletAssistActivity) appletActivity;
        Activity activityInfo = appletAssistActivity.getActivityInfo();
        if (activityInfo == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无此活动");
        }

        //此刻调用助力过来的交互
        if (size == 0 && type == ActivityApiEntrySceneEnum.ASSIST.getCode()) {
            int fullReimbursementMoneyNum = fullReimbursementAboutService.getFullReimbursementMoneyNum(activityInfo.getMerchantId(),
                    activityInfo.getShopId(), activityInfo.getUserId(), activityInfo.getId());
            //判断是否是新人助力，存储redis
            fullReimbursementAboutService.saveCanCashRedisByFullReimbursement(userId, appletAssistActivity, fullReimbursementMoneyNum);

            return new SimpleResponse();
        }

        //判断获取该活动的前几人的助力直接到账
        int hitPerDraw = activityInfo.getHitsPerDraw();
        //获取已入账金额的人数   hitPerDraw与已助力的人做对比
        Map<String, Map<String, Object>> assistUserInfoMap = appletAssistActivity.getAssistUserInfoMap();
        //计算已入账的金额
        BigDecimal accountEntry = fullReimbursementAboutService.getFullReimbursementMoney(activityInfo.getMerchantId(),
                activityInfo.getShopId(), activityInfo.getUserId(), activityInfo.getId(), hitPerDraw);
        //计算已助力的金额
        BigDecimal awardPool = BigDecimal.ZERO;
        Map<String, Object> startUserInfo = appletAssistActivity.getStartUserInfo();
        if (!CollectionUtils.isEmpty(startUserInfo)) {
            if (StringUtils.isNotBlank(startUserInfo.get("awardPool").toString())) {
                awardPool = new BigDecimal(startUserInfo.get("awardPool").toString());
            }
        }
        //计算已报销部分的金额
        BigDecimal binding = awardPool;

        //活动倒计时
        Long cutoffTime = activityInfo.getEndTime().getTime() - System.currentTimeMillis();
        //报销活动金额
        BigDecimal applyAmount = activityInfo.getDeposit();
        //用户自己
        User userInfo = appletAssistActivity.getUserInfo();
        //获取活动详情的状态
        ActivityDetailStatusEnum activityDetailStatusEnum = fullReimbursementAboutService.findByActivityDetailStatusEnum(userId, appletAssistActivity);
        //获取平台已报销的金额
        Map<String, Object> activityParams = new HashMap<>();
        activityParams.put("type", ActivityTypeEnum.FULL_REIMBURSEMENT.getCode());
        BigDecimal sum = activityService.sumByParams(activityParams);
        //平台累计金额
        BigDecimal platform = new BigDecimal(fullreimbrusementTotal).add(sum);

        //开奖
        if (activityDetailStatusEnum == ActivityDetailStatusEnum.AWARDED) {
            binding = activityInfo.getDeposit().subtract(accountEntry);
            accountEntry = activityInfo.getDeposit();
        }

        //组装返回结果
        ActivityFullReimbursementDTO respDTO = new ActivityFullReimbursementDTO();
        respDTO.setReimburseAmountTotal(platform.setScale(2, BigDecimal.ROUND_DOWN).toString());
        respDTO.setReimbursed(binding.toString());
        respDTO.setToBeReimburse(applyAmount.toString());
        respDTO.setMoneyInWallet(accountEntry.toString());

        //容错处理
        if (activityDetailStatusEnum == null) {
            activityDetailStatusEnum = ActivityDetailStatusEnum.EXPIRED;
        }
        //若是新人，可助力
        if (activityDetailStatusEnum == ActivityDetailStatusEnum.HELP_FRIEND) {
            Map<String, Object> stringObjectMap = assistUserInfoMap.get(userId.toString());
            if (!CollectionUtils.isEmpty(stringObjectMap)) {
                respDTO.setHelpAmount(stringObjectMap.get("amount").toString());
            }
        }

        respDTO.setStatus(activityDetailStatusEnum.getCode());
        respDTO.setActivityDefId(activityInfo.getActivityDefId());
        respDTO.setActivityId(activityInfo.getId());
        respDTO.setRemainingTime(cutoffTime);

        if (userInfo != null) {
            respDTO.setAvatarUrl(userInfo.getAvatarUrl());
            respDTO.setNickName(userInfo.getNickName());
        }
        if (size > 0) {
            respDTO.setList(fullReimbursementAboutService.getAssistListPage(activityInfo.getId(), offset, size));
            AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(appletAssistActivity.getActivityDefId());
            //弹窗信息
            respDTO.setDialog(fullReimbursementAboutService.queryFullReimbursementDialog(userId, appletActivityDef.getActivityDefInfo(), appletAssistActivity.getActivityInfo(), activityDetailStatusEnum.getCode()));
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(respDTO);
        return simpleResponse;
    }

    /***
     * 删除助力的助力金额信息
     * @param redisKey
     */
    @Override
    public void deleteActivityAssistRedis(String redisKey) {
        if (StringUtils.isNotBlank(redisKey)) {
            stringRedisTemplate.delete(redisKey);
        }
    }

    /***
     * 全额报销-- 创建活动页的前置过滤和数据支持
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    @Override
    public SimpleResponse activityCreateFilter(String merchantId, String shopId, String userId) {
        long mid = NumberUtils.toLong(merchantId);
        long sid = NumberUtils.toLong(shopId);
        long aid = 0L;

        List dialogList = new ArrayList();
        //查找该店铺是否该活动
        QueryWrapper<ActivityDef> activityDefQueryWrapper = new QueryWrapper<>();
        activityDefQueryWrapper.lambda()
                .eq(ActivityDef::getMerchantId, mid)
                .eq(ActivityDef::getShopId, sid)
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getType, ActivityDefTypeEnum.FULL_REIMBURSEMENT.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
        ;
        activityDefQueryWrapper.orderByDesc("id");
        List<ActivityDef> activityDefs = activityDefService.list(activityDefQueryWrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            SimpleResponse simpleResponse = new SimpleResponse();
            Map resp = new HashMap();
            //活动已结束
            Map map = new HashMap();
            map.put("type", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
            dialogList.add(map);
            resp.put("activityId", aid);
            resp.put("dialog", dialogList);
            simpleResponse.setData(resp);
            return simpleResponse;
        }
        ActivityDef activityDef = activityDefs.get(0);
        if (activityDef == null || activityDef.getDeposit().compareTo(BigDecimal.ZERO) <= 0) {
            //活动已结束
            Map map = new HashMap();
            map.put("type", ActivityPopupStatusEnum.STOCK_ZERO.getCode());
            dialogList.add(map);
        }

        //有无正在进行的活动
        QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
        activityQueryWrapper.lambda()
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getUserId, userId)
                .eq(Activity::getActivityDefId, activityDef.getId())
                .eq(Activity::getType, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode())
                .eq(Activity::getStatus, mf.code.activity.constant.ActivityStatusEnum.PUBLISHED.getCode())
                .eq(Activity::getDel, mf.code.common.DelEnum.NO.getCode())
                .isNull(Activity::getAwardStartTime)
        ;
        activityQueryWrapper.orderByDesc("id");
        List<Activity> activities = activityService.list(activityQueryWrapper);
        if (!CollectionUtils.isEmpty(activities)) {
            Map map = new HashMap();
            map.put("type", ActivityPopupStatusEnum.ACTIVITY_ING.getCode());
            dialogList.add(map);
            aid = activities.get(0).getId();
        }

        BigDecimal min = BigDecimal.ZERO;
        BigDecimal max = BigDecimal.ZERO;
        BigDecimal personalMaxAmount = BigDecimal.ZERO;
        if (activityDef != null) {
            String amountJson = activityDef.getAmountJson();
            JSONObject jsonObject = JSON.parseObject(amountJson);

            if (jsonObject != null) {
                personalMaxAmount = new BigDecimal(jsonObject.get("personalMaxAmount").toString());
                JSONObject jsonObjectPerAmount = JSON.parseObject(jsonObject.get("perAmount").toString());
                min = new BigDecimal(jsonObjectPerAmount.get("min").toString());
                max = new BigDecimal(jsonObjectPerAmount.get("max").toString());
            }
        }

        //获取该店铺下该活动定义下用户已经发起过的活动
        BigDecimal sumWxOrderTotalFee = BigDecimal.ZERO;
        QueryWrapper<Activity> acWrapper = new QueryWrapper<>();
        acWrapper.lambda()
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getUserId, userId)
                .eq(Activity::getActivityDefId, activityDef.getId())
                .eq(Activity::getType, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode())
        ;
        List<Activity> activitiesJoins = activityService.list(acWrapper);
        List<Long> aids = new ArrayList<>();
        if (!CollectionUtils.isEmpty(activitiesJoins)) {
            for (Activity activity : activitiesJoins) {
                if (aids.indexOf(activity.getId()) == -1) {
                    aids.add(activity.getId());
                }
            }
            //获取该用户的已报销额度
            Map wxOrderParams = new HashMap();
            wxOrderParams.put("mchId", mid);
            wxOrderParams.put("shopId", sid);
            wxOrderParams.put("userId", userId);
            wxOrderParams.put("type", OrderTypeEnum.PALTFORMRECHARGE.getCode());
            wxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
            wxOrderParams.put("bizValues", aids);
            wxOrderParams.put("bizTypes", Arrays.asList(
                    BizTypeEnum.FULL_REIMBURSEMENT.getCode()
            ));
            sumWxOrderTotalFee = this.upayWxOrderService.sumUpayWxOrderTotalFee(wxOrderParams);
        }

        //用户剩余可用额度计算
        BigDecimal remainFee = personalMaxAmount.subtract(sumWxOrderTotalFee);
        if (max.compareTo(BigDecimal.ZERO) > 0 && sumWxOrderTotalFee.compareTo(personalMaxAmount) >= 0
                || (remainFee.compareTo(BigDecimal.ZERO) > 0 && remainFee.compareTo(min) < 0)) {
            //额度已经超限
            Map map = new HashMap();
            map.put("type", ActivityPopupStatusEnum.HAVE_ONE.getCode());
            dialogList.add(map);
        }

        if (remainFee.compareTo(BigDecimal.ZERO) > 0 && remainFee.compareTo(max) < 0) {
            max = remainFee;
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        Map resp = new HashMap();
        resp.put("min", min);
        resp.put("max", max);
        resp.put("activityDefId", activityDef != null ? activityDef.getId() : 0);
        resp.put("activityId", aid);
        resp.put("dialog", dialogList);
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 弹幕
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    @Override
    public SimpleResponse barrage(String merchantId, String shopId, String userId) {
        Page<UpayWxOrder> page = new Page<>(1, 20);
        QueryWrapper<UpayWxOrder> upayWxOrderQueryWrapper = new QueryWrapper<>();
        upayWxOrderQueryWrapper.lambda()
                .eq(UpayWxOrder::getType, UpayWxOrderTypeEnum.PALTFORMRECHARGE.getCode())
                .eq(UpayWxOrder::getStatus, UpayWxOrderStatusEnum.ORDERED.getCode())
                .eq(UpayWxOrder::getBizType, BizTypeEnum.FULL_REIMBURSEMENT.getCode())
        ;
        upayWxOrderQueryWrapper.orderByDesc("id");
        IPage<UpayWxOrder> upayWxOrderIPage = this.upayWxOrderService.page(page, upayWxOrderQueryWrapper);
        List<UpayWxOrder> upayWxOrders = upayWxOrderIPage.getRecords();

        List list = new ArrayList();
        if (!CollectionUtils.isEmpty(upayWxOrders)) {
            List<Long> userIds = new ArrayList<>();
            for (UpayWxOrder upayWxOrder : upayWxOrders) {
                if (userIds.indexOf(upayWxOrder.getUserId()) == -1) {
                    userIds.add(upayWxOrder.getUserId());
                }
            }
            Map<Long, User> userMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(userIds)) {
                userMap = userService.queryByUserIdsToMap(userIds);
            }

            for (UpayWxOrder upayWxOrder : upayWxOrders) {
                User user = userMap.get(upayWxOrder.getUserId());
                if (user == null) {
                    continue;
                }
                Map map = new HashMap();
                map.put("nickName", user.getNickName());
                map.put("avatarUrl", user.getAvatarUrl());
                map.put("amount", upayWxOrder.getTotalFee());
                list.add(map);
            }
        }
        //查找获取假数据的模型
        if (upayWxOrders.size() < 20) {
            int num = 20 - upayWxOrders.size();
            list.addAll(getRobotAssistUser(num));
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resp = new HashMap<>();
        resp.put("barrageInfo", list);
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 报销活动弹窗-首页
     * @param shopId
     * @param userId
     * @param scenes
     * @return
     */
    @Override
    public SimpleResponse fullReimbursementPopup(Long shopId, Long userId, Integer scenes) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (ScenesEnum.HOME_PAGE.getCode().equals(scenes)) {
            Map<String, Object> map = fullReimbursementAboutService.fullReimbursementPopup(shopId, userId);
            simpleResponse.setData(map);
            return simpleResponse;
        }
        Map map = new HashMap();
        map.put("activityStatus", ActivityPopupStatusEnum.NONE.getCode());
        simpleResponse.setData(map);
        return new SimpleResponse();
    }

    /***
     * 获取假数据
     * @param limit
     * @return
     */
    private List<Object> getRobotAssistUser(int limit) {
        List<Object> list = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            Map<String, Object> map = RobotUserUtil.getAssistUserAvatarUrlAndNickFromOss();
            map.put("amount", RandomStrUtil.randomNum(2, 5, 2));
            list.add(map);
        }
        return list;
    }
}
