package mf.code.api.applet.v3.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v3.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月12日 11:20
 */
public interface CheckpointTaskSpaceService {

    /***
     * 查询关卡任务空间列表
     * @param merchantId
     * @param shopID
     * @return
     */
    SimpleResponse queryCheckpointTaskSpace(Long merchantId, Long shopID, Long userId);

    /***
     * 选择商品详细
     * @param merchantId
     * @param shopID
     * @param type
     * @return
     */
    SimpleResponse queryGoodsDetail(Long merchantId, Long shopID, Long userId, int type, int offset, int size);


    SimpleResponse getBarrage(Long merchantId,
                              Long shopId);

    SimpleResponse getItemList(Long merchantId, Long shopId);

	SimpleResponse queryActivity(Long merchantId, Long shopID, Long userId);
}
