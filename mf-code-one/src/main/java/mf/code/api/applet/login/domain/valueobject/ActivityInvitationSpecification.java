package mf.code.api.applet.login.domain.valueobject;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.user.constant.UserPubJoinTypeEnum;

import java.util.List;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-19 下午4:22
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class ActivityInvitationSpecification extends InviteSpecification {
	// 配置活动 是否是邀请得功能
	private Boolean isInvite = true;
	// 配置活动 邀请维度
	private InvitationDimensionEnum invitationDimension = InvitationDimensionEnum.ACTIVITY;

	public ActivityInvitationSpecification(Activity activity) {
		Integer activityType = activity.getType();

		super.activityId = activity.getId().toString();
		super.userPubJoinType = UserPubJoinTypeEnum.getCodeByActivityType(activityType)+"";
		super.shopId = activity.getShopId().toString();
		super.invitationRoleEnum = InvitationRoleEnum.PUB;
		if (ActivityTypeEnum.LUCK_WHEEL.getCode() == activityType) {
			this.invitationDimension = InvitationDimensionEnum.SHOP;
		}
		if (ActivityTypeEnum.ORDER_REDPACK.getCode() == activityType) {
			this.isInvite = false;
		}
		if (ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode() == activityType) {
			this.isInvite = false;
		}
	}

	public Boolean isInvite () {
		return isInvite;
	}

	// 是否可以邀请别人
	@Override
	protected Boolean canInviteAppletSubUser(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser) {
		if (appletPubUser.getInvitationHistory() == null) {
			log.info("未初始化邀请历史记录");
			return false;
		}
		List<UserPubJoin> userPubJoins = appletPubUser.getInvitationHistory().getRecords().get(super.activityId);
		if (userPubJoins == null) {
			return true;
		}

		for (UserPubJoin userPubJoin : userPubJoins) {
			if (appletSubUser.getUserId().equals(userPubJoin.getSubUid())) {
				log.info("曾今邀请过此用户");
				return false;
			}
		}
		return true;
	}

	// 是否可以被别人邀请
	@Override
	protected Boolean canBeInvitedByAppletPubUser(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser) {
		return null;
	}
}
