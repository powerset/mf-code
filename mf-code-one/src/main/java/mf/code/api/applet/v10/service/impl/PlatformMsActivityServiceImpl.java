package mf.code.api.applet.v10.service.impl;

import mf.code.api.applet.v10.service.PlatformMsActivityService;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.common.apollo.ApolloLhyxProperty;
import mf.code.common.apollo.ApolloPropertyService;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.one.dto.MsInfoDTO;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v10.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月16日 08:49
 */
@Service
public class PlatformMsActivityServiceImpl implements PlatformMsActivityService {
    @Autowired
    private ApolloPropertyService apolloPropertyService;
    @Autowired
    private GoodsAppService goodsAppService;

    /***
     * 获取秒杀活动的时间段轴
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse getMsActivityTime(Long shopId, Long userId, Long dayId, Long batchId) {
        SimpleResponse msBatch = getMsBatch(dayId);
        if (msBatch.error()) {
            return msBatch;
        }
        List<ApolloLhyxProperty.Batch> batchs = (List<ApolloLhyxProperty.Batch>) msBatch.getData();
        if (CollectionUtils.isEmpty(batchs)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动9");
        }

        List<Object> batchList = new ArrayList<>();

        Long startTimeStamp = 0L;
        Long endTimeStamp = 0L;
        for (ApolloLhyxProperty.Batch batch : batchs) {
            Date batchTime = DateUtil.parseDate(batch.getStartTime(), null, "yyyy-MM-dd HH:mm");
            batch.setStartTime(DateFormatUtils.format(batchTime, "HH:mm"));
            Date endTime = DateUtil.parseDate(batch.getEndTime(), null, "yyyy-MM-dd HH:mm");
            batch.setEndTime(DateFormatUtils.format(endTime, "HH:mm"));
            startTimeStamp = batchTime.getTime();
            endTimeStamp = endTime.getTime();


            Map<String, Object> map = new HashMap<>();
            map.putAll(BeanMapUtil.beanToMapIgnore(batch, "skus"));
            int status = getMsActivityStatus(startTimeStamp, endTimeStamp);
            map.put("status", status);
            batchList.add(map);
        }
        int index = getBatchIndex(batchs);
        Map<String, Object> resp = new HashMap<>();
        resp.put("index", index);
        resp.put("times", batchList);
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 获取秒杀活动某个时间段 商品详情信息
     *
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse getMsActivityOneBatch(Long userId, Long shopId, Long dayId, Long batchId, int offset, int size) {
        SimpleResponse msBatch = getMsBatch(dayId);
        if (msBatch.error()) {
            return msBatch;
        }
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        //活动结束时间
        String activityEndTimeStr = apolloLhyxProperty.getLhyx().getActivity().getEndTime();
        Date activityEndTime = DateUtil.parseDate(activityEndTimeStr, null, "yyyy-MM-dd HH:mm");

        List<ApolloLhyxProperty.Batch> batchs = (List<ApolloLhyxProperty.Batch>) msBatch.getData();
        if (CollectionUtils.isEmpty(batchs)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动9");
        }

        Map<Long, ApolloLhyxProperty.Batch> batchMap = new HashMap<>();
        for (ApolloLhyxProperty.Batch batch : batchs) {
            batchMap.put(batch.getId(), batch);
            Date batchTime = DateUtil.parseDate(batch.getStartTime(), null, "yyyy-MM-dd HH:mm");
            batch.setStartTime(DateFormatUtils.format(batchTime, "HH:mm"));
            Date endTime = DateUtil.parseDate(batch.getEndTime(), null, "yyyy-MM-dd HH:mm");
            batch.setEndTime(DateFormatUtils.format(endTime, "HH:mm"));
            batch.setStartTimeStamp(batchTime.getTime());
            batch.setEndTimeStamp(endTime.getTime());
        }

        int index = getBatchIndex(batchs);
        ApolloLhyxProperty.Batch batch = null;
        if (batchId != null && batchId > 0) {
            batch = batchMap.get(batchId);
        } else {
            batch = batchs.get(index);
        }
        int status = getMsActivityStatus(batch.getStartTimeStamp(), batch.getEndTimeStamp());

        int count = batch.getSkus().size();
        List<String> skuStrs = new ArrayList<>();
        for (ApolloLhyxProperty.Skus sku : batch.getSkus()) {
            skuStrs.add(sku.getSku());
        }
        if (CollectionUtils.isEmpty(skuStrs)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动下的商品开小差啦~请刷新");
        }

        int start = offset;
        int end = (offset / size + 1) * size;

        if (count < end) {
            skuStrs = skuStrs.subList(start, count);
        } else {
            skuStrs = skuStrs.subList(start, end);
        }


        AppletMybatisPageDto<Object> respList = new AppletMybatisPageDto<>(6, 0);
        //yy支持 获取sku信息
        MsInfoDTO msInfoDTO = new MsInfoDTO();
        msInfoDTO.setUserId(userId);
        msInfoDTO.setShopId(shopId);
        msInfoDTO.setTime(String.valueOf(batch.getStartTimeStamp()));
        //以活动批次结束
        msInfoDTO.setEndTime(String.valueOf(batch.getEndTimeStamp()));
        //已活动日期点结束
        msInfoDTO.setEndTime(String.valueOf(activityEndTime.getTime()));

        msInfoDTO.setSkus(skuStrs);
        List<ProductDistributionDTO> skus = goodsAppService.queryOrCreateForFlashSale(msInfoDTO);
        if (!CollectionUtils.isEmpty(skus)) {
            for (ProductDistributionDTO productDistributionDTO : skus) {
                int salesVolume = NumberUtils.toInt(productDistributionDTO.getSalesVolume()) + productDistributionDTO.getInitSale();
                productDistributionDTO.setSalesVolume(String.valueOf(salesVolume));
                Map<String, Object> map = new HashMap<>();
                map.putAll(BeanMapUtil.beanToMapIgnore(productDistributionDTO, "userId", "merchantId", "shopId", "parentId",
                        "selfSupportRatio", "platSupportRatio", "productSpecs", "putawayTime", "rebate", "initSale"));
                int stockTotal = productDistributionDTO.getStockDef() + productDistributionDTO.getInitSale();
                BigDecimal ratio = new BigDecimal(String.valueOf(salesVolume)).multiply(new BigDecimal(String.valueOf(100)))
                        .divide(new BigDecimal(String.valueOf(stockTotal)), 2, BigDecimal.ROUND_DOWN);
                map.put("ratio", ratio);
                //活动时间结束的依然可以选择购买
                initProductData(map, status);
                respList.getContent().add(map);
            }
        }
        if (count > offset + size) {
            respList.setPullDown(true);
        }
        respList.setLimit(size);
        respList.setOffset(offset);

        Map<String, Object> resp = new HashMap<>();

        long cutoffTime = DateUtil.timeSub(batch.getStartTime(), batch.getEndTime(), DateUtil.FORMAT_FIVE);
        String now = DateFormatUtils.format(new Date(), DateUtil.FORMAT_FIVE);
        long nowTime = DateUtil.stringtoDate(now, DateUtil.FORMAT_FIVE).getTime();
        if (DateUtil.stringtoDate(batch.getStartTime(), DateUtil.FORMAT_FIVE).getTime() < nowTime) {
            cutoffTime = DateUtil.timeSub(now, batch.getEndTime(), DateUtil.FORMAT_FIVE);
        } else if (DateUtil.stringtoDate(batch.getStartTime(), DateUtil.FORMAT_FIVE).getTime() > nowTime) {
            cutoffTime = DateUtil.timeSub(now, batch.getStartTime(), DateUtil.FORMAT_FIVE);
        }

        resp.put("skus", respList);
        resp.put("status", status);
        resp.put("cutoffTime", cutoffTime);
        initProductRespData(resp, status);
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     *
     * @param map
     * @param status
     *      1:进行中
     *      2:结束
     *      3:未开始
     */
    private void initProductData(Map<String, Object> map, int status) {
        //未开始时，数据初始化
        if (status == 3) {
            map.put("ratio", String.valueOf(0));
            map.put("salesVolume", String.valueOf(0));
        }
    }

    /***
     * 活动时间结束，商品依旧可买
     *
     * @param map
     * @param status
     */
    private void initProductRespData(Map<String, Object> map, int status) {
        map.put("buttonStatus", status);
        if (status == 2) {
            map.put("buttonStatus", 1);
        }
    }

    /***
     * 获取秒杀活动 批次信息
     *
     * @param dayId 第n天的编号 批次活动 非必传
     * @return
     */
    private SimpleResponse getMsBatch(Long dayId) {
        //获取apollo配置属性相关值
        ApolloLhyxProperty apolloLhyxProperty = apolloPropertyService.getApolloLhyxProperty();
        if (apolloLhyxProperty == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动1");
        }
        ApolloLhyxProperty.Lhyx lhyx = apolloLhyxProperty.getLhyx();
        if (lhyx == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动2");
        }
        ApolloLhyxProperty.Activity activity = lhyx.getActivity();
        if (activity == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动3");
        }
        //活动开始时间
        Date startTime = DateUtil.parseDate(activity.getStartTime(), null, DateUtil.FORMAT_ONE);
        //活动结束时间
        Date endTime = DateUtil.parseDate(activity.getEndTime(), null, DateUtil.FORMAT_ONE);
        //活动的时间批次
        List<ApolloLhyxProperty.Ms> mses = lhyx.getMs();
        if (CollectionUtils.isEmpty(mses)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动4");
        }
        Map<Long, ApolloLhyxProperty.Ms> msMap = new HashMap<>();
        for (ApolloLhyxProperty.Ms ms : mses) {
            msMap.put(ms.getId(), ms);
        }
        //当前时间距离活动开始时间已经第几天了
        long dayDiffStart = DateUtil.dayDiff(startTime, new Date());
        if (dayDiffStart < 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动5");
        }
        long dayDiffEnd = DateUtil.dayDiff(endTime, new Date());
        if (dayDiffEnd > 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动6");
        }

        ApolloLhyxProperty.Ms ms = null;
        if (dayId != null && dayId > 0) {
            ms = msMap.get(dayId);
        } else {
            if (dayDiffStart >= 0) {
                //此刻是当天
                dayDiffStart = dayDiffStart + 1;
            }
            ms = msMap.get(dayDiffStart);
        }
        if (ms == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动7");
        }

        //将此时的活动时间线展示出来
        List<ApolloLhyxProperty.Batch> batchs = ms.getBatch();
        if (CollectionUtils.isEmpty(batchs)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该活动已结束啦，请参与其它活动8");
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(batchs);
        return simpleResponse;
    }

    /***
     * 获取时间轴批次的下标
     *
     * @param batchs
     * @return
     */
    private int getBatchIndex(List<ApolloLhyxProperty.Batch> batchs) {
        int index = -1;
        for (ApolloLhyxProperty.Batch batch : batchs) {
            index++;
            Date start = DateUtil.parseDate(batch.getStartTime(), null, "HH:mm");
            Date end = DateUtil.parseDate(batch.getEndTime(), null, "HH:mm");
            Date now = DateUtil.parseDate(DateFormatUtils.format(new Date(), "HH:mm"), null, "HH:mm");
            if ((now.getTime() >= start.getTime() && now.getTime() < end.getTime()) || start.getTime() > now.getTime()) {
                break;
            }
        }
        return index;
    }

    /***
     * 获取ms活动的状态
     * <pre>
     *     1:进行中
     *     2:结束
     *     3:未开始
     * </pre>
     * @param startTimeStamp
     * @param endTimeStamp
     * @return
     */
    private int getMsActivityStatus(Long startTimeStamp, Long endTimeStamp) {
        Date now = DateUtil.parseDate(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm"), null, "yyyy-MM-dd HH:mm");
        if (startTimeStamp > now.getTime()) {
            return 3;
        } else if (now.getTime() > endTimeStamp) {
            return 2;
        } else {
            return 1;
        }
    }
}
