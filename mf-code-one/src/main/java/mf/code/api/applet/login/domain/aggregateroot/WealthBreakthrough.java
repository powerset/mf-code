package mf.code.api.applet.login.domain.aggregateroot;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mf.code.api.applet.login.domain.valueobject.WealthBreakthroughSettings;

import java.math.BigDecimal;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-25 下午1:38
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WealthBreakthrough extends PlatformGameAggregateRoot {
	private BigDecimal taskRevenue = BigDecimal.ZERO;
	private BigDecimal scottareRevenue = BigDecimal.ZERO;
	private WealthBreakthroughSettings settings;
	private Integer level = 0;
}
