package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.UserMoneyService;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailConverter;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.OrderService;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.repo.redis.RedisKeyConstant;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月01日 14:28
 */
@Service
@Slf4j
public class UserMoneyServiceImpl implements UserMoneyService {
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private UserService userService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private List<UserMoneyTradeDetailConverter> userMoneyTradeDetailConverters;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private OrderService orderService;

    /***
     * 查询用户余额
     * @param userID
     * @return
     */
    @Override
    public SimpleResponse queryUserBalance(Long merchantID, Long shopID, Long userID) {
        SimpleResponse d = new SimpleResponse();

        if (userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        Map<String, Object> resp = new HashMap<String, Object>();
        UpayBalance upayBalance = this.upayBalanceService.query(merchantID, shopID, userID);
        if (upayBalance == null) {
            resp.put("balance", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            d.setData(resp);
            return d;
        }
        resp.put("balance", upayBalance.getBalance().setScale(2, BigDecimal.ROUND_DOWN).toString());
        d.setData(resp);
        return d;
    }

    /***
     * 用户订单记录
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @param type
     * @return
     */
    @Override
    public SimpleResponse queryPresentOrderLog(Long shopID, Long userID, int offset, int size, String type) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        SimpleResponse d = new SimpleResponse();
        if (shopID == null || shopID == 0 || userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("该店铺不存在");
            return d;
        }
        if (OrderTypeEnum.PALTFORMRECHARGE.toString().toLowerCase().equals(type)) {
            d.setData(this.queryPresentOrder(merchantShop.getMerchantId(), shopID, userID, offset, size, OrderTypeEnum.PALTFORMRECHARGE.getCode()));
        } else {
            //提现记录
            d.setData(this.queryPresentOrder(merchantShop.getMerchantId(), shopID, userID, offset, size, OrderTypeEnum.USERCASH.getCode()));
        }
        return d;
    }

    /***
     * 查询收入记录
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryIncomeLog(Long shopID, Long userID, int offset, int size) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        SimpleResponse d = new SimpleResponse();
        if (shopID == null || shopID == 0 || userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }

        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该店铺不存在");
            return d;
        }

        Map<String, Object> wxOrderParams = new HashMap<String, Object>();
        wxOrderParams.put("mchId", merchantShop.getMerchantId());
        wxOrderParams.put("shopId", shopID);
        wxOrderParams.put("userId", userID);
        wxOrderParams.put("type", OrderTypeEnum.PALTFORMRECHARGE.getCode());
        wxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        wxOrderParams.put("order", "payment_time");
        wxOrderParams.put("direct", "desc");
        wxOrderParams.put("size", size);
        wxOrderParams.put("offset", offset);
        BigDecimal sumWxOrderTotalFee = this.upayWxOrderService.sumUpayWxOrderTotalFee(wxOrderParams);
        wxOrderParams.put("bizTypes", Arrays.asList(
                BizTypeEnum.ORDER_BACK.getCode(),
                BizTypeEnum.ASSIST_ACTIVITY.getCode(),
                BizTypeEnum.CHECKPOINT.getCode()
        ));
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(wxOrderParams);
        int countWxOrder = this.upayWxOrderService.countUpayWxOrder(wxOrderParams);
        if (upayWxOrders == null || upayWxOrders.size() == 0) {
            respMap.put("offset", offset);
            respMap.put("limit", size);
            respMap.put("total", countWxOrder);
            respMap.put("content", new ArrayList<>());
            respMap.put("sumMoney", sumWxOrderTotalFee.setScale(2, BigDecimal.ROUND_DOWN).toString());
            d.setData(respMap);
            return d;
        }
        List<Long> activityIDs = new ArrayList<Long>();
        for (UpayWxOrder wxOrder : upayWxOrders) {
            if (wxOrder.getBizValue() != null && wxOrder.getBizValue() > 0 &&
                    (BizTypeEnum.ACTIVITY.getCode() == wxOrder.getBizType() ||
                            BizTypeEnum.ASSIST_ACTIVITY.getCode() == wxOrder.getBizType() ||
                            BizTypeEnum.ORDER_BACK.getCode() == wxOrder.getBizType() ||
                            BizTypeEnum.NEWMAN_START.getCode() == wxOrder.getBizType()) &&
                    activityIDs.indexOf(wxOrder.getBizValue()) == -1) {
                activityIDs.add(wxOrder.getBizValue());
            }
        }

        Map<Long, Activity> activityMap = new HashMap<>();
        Map<Long, Goods> goodsMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(activityIDs)) {
            Map<String, Object> activityParams = new HashMap<String, Object>();
            activityParams.put("merchantId", merchantShop.getMerchantId());
            activityParams.put("shopId", merchantShop.getId());
            activityParams.put("activityIds", activityIDs);
            List<Activity> activities = this.activityService.findPage(activityParams);
            List<Long> goodsIDs = new ArrayList<Long>();
            if (activities != null && activities.size() > 0) {
                for (Activity activity : activities) {
                    activityMap.put(activity.getId(), activity);
                    if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                        goodsIDs.add(activity.getGoodsId());
                    }
                }
            }
            goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        }

        CommonDict commonDict = this.commonDictService.selectByTypeKey("checkpoint", "level");

        List<Object> list = new ArrayList<Object>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            list.add(this.covertUserMoneyTradeDetail(upayWxOrder, null, activityMap, goodsMap, commonDict));
        }
        d.setData(this.returnInfoObject(offset, size, countWxOrder, list, sumWxOrderTotalFee));
        return d;
    }

    /***
     * 2018.11.16之后的最新分页返回对象
     * @param offset
     * @param size
     * @param total
     * @param list
     * @param amount
     * @return
     */
    private Map<String, Object> returnInfoObjectNew(int offset, int size, int total, List<Object> list, BigDecimal amount) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("offset", offset);
        respMap.put("limit", size);
        respMap.put("isPullDown", false);
        if (total > 0) {
            respMap.put("isPullDown", true);
        }
        respMap.put("content", list);
        if (amount != null) {
            respMap.put("sumMoney", amount);//收入记录-汇总金额
        }
        return respMap;
    }

    /***
     * 查询交易明细
     * @param merchantID
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryTradeDetail(Long merchantID, Long shopID, Long userID, int offset, int size, Long pageLastID) {
        SimpleResponse d = new SimpleResponse();
        if (merchantID == null || merchantID == 0 || shopID == null || shopID == 0 || userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        Map<String, Object> upayWxOrderParams = this.selectUpayOrderParams(merchantID, shopID, userID, offset, size, null);
        upayWxOrderParams.put("order", "id");
        upayWxOrderParams.put("direct", "desc");
        upayWxOrderParams.put("pageLastId", pageLastID);
        upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
        if (upayWxOrders == null || upayWxOrders.size() == 0) {
            d.setData(this.returnInfoObjectNew(offset, size, 0, new ArrayList<>(), null));
            return d;
        }
        List<Long> activityIDs = new ArrayList<Long>();
        List<Long> productOrderIds = new ArrayList<>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            if (OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType() &&
                    (BizTypeEnum.ACTIVITY.getCode() == upayWxOrder.getBizType() ||
                            BizTypeEnum.ASSIST_ACTIVITY.getCode() == upayWxOrder.getBizType() ||
                            BizTypeEnum.ORDER_BACK.getCode() == upayWxOrder.getBizType() ||
                            BizTypeEnum.NEWMAN_START.getCode() == upayWxOrder.getBizType()) && upayWxOrder.getBizValue() > 0) {
                if (activityIDs.indexOf(upayWxOrder.getBizValue()) == -1) {
                    activityIDs.add(upayWxOrder.getBizValue());
                }
            }
            //商品编号
            if (BizTypeEnum.SHOPPING.getCode() == upayWxOrder.getBizType() && upayWxOrder.getBizValue() != null && upayWxOrder.getBizValue() > 0) {
                if (productOrderIds.indexOf(upayWxOrder.getBizValue()) == -1) {
                    productOrderIds.add(upayWxOrder.getBizValue());
                }
            }
        }
        Map<Long, Goods> goodsMap = new HashMap<>();
        Map<Long, Activity> activityMap = new HashMap<Long, Activity>();
        if (!CollectionUtils.isEmpty(activityIDs)) {
            Map<String, Object> activityParams = new HashMap<String, Object>();
            activityParams.put("merchantId", merchantID);
            activityParams.put("shopId", shopID);
            activityParams.put("activityIds", activityIDs);
            List<Activity> activities = this.activityService.findPage(activityParams);
            List<Long> goodsIDs = new ArrayList<Long>();
            if (activities != null && activities.size() > 0) {
                for (Activity activity : activities) {
                    activityMap.put(activity.getId(), activity);
                    if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                        goodsIDs.add(activity.getGoodsId());
                    }
                }
            }
            goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        }

        Map<Long, Map> productGoodsShoppingMap = this.orderService.getUpayOrderProductInfo(productOrderIds);

        List<Object> list = new ArrayList<Object>();
        int listIndex = 0;//条数
        CommonDict commonDict = this.commonDictService.selectByTypeKey("checkpoint", "level");
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            //活动中奖信息信息
            Map<String, Object> map2 = this.covertUserMoneyTradeDetail(upayWxOrder, null, activityMap, goodsMap, commonDict);
            if (!CollectionUtils.isEmpty(map2)) {
                list.add(map2);
            }

            //用户支付-不是购物biztype
            if (OrderTypeEnum.USERPAY.getCode() == upayWxOrder.getType() && OrderPayTypeEnum.WEIXIN.getCode() == upayWxOrder.getPayType()) {
                Map<String, Object> map = this.covertUserMoneyTradeDetail(upayWxOrder, 1, null, null, null);
                if (!CollectionUtils.isEmpty(map)) {
                    list.add(map);
                }
                Map<String, Object> map1 = this.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, 1, productGoodsShoppingMap);
                if (!CollectionUtils.isEmpty(map1)) {
                    //购买商品交易明细
                    list.add(map1);
                }
            }
            //用户退款
            boolean wxPlatformInvok = OrderTypeEnum.PALTFORMREFUND.getCode() == upayWxOrder.getType() ||
                    OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType();
            if (wxPlatformInvok && OrderPayTypeEnum.WEIXIN.getCode() == upayWxOrder.getPayType()) {
                Map<String, Object> map = this.covertUserMoneyTradeDetail(upayWxOrder, 2, null, null, null);
                if (!CollectionUtils.isEmpty(map)) {
                    list.add(map);
                }
                //购买商品交易明细
                Map<String, Object> map1 = this.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, 2, productGoodsShoppingMap);
                if (!CollectionUtils.isEmpty(map1)) {
                    //退款商品交易明细
                    list.add(map1);
                }
            }
            listIndex++;
        }
        //查询数目小于本要展现的数目
        int countUpayWxOrder = 0;
        if (listIndex >= size) {
            countUpayWxOrder = 1;
        }
        d.setData(this.returnInfoObjectNew(offset, size, countUpayWxOrder, list, null));
        return d;
    }

    /***
     * 映射用户记录推送converter
     * @param upayWxOrder
     * @param payType
     * @param activityMap
     * @param goodsMap
     */
    private Map<String, Object> covertUserMoneyTradeDetail(UpayWxOrder upayWxOrder,
                                                           Integer payType,
                                                           Map<Long, Activity> activityMap,
                                                           Map<Long, Goods> goodsMap,
                                                           CommonDict commonDict) {
        if (!CollectionUtils.isEmpty(this.userMoneyTradeDetailConverters)) {
            for (UserMoneyTradeDetailConverter converter : this.userMoneyTradeDetailConverters) {
                Map<String, Object> userMoneyTradeDetail = converter.getUserMoneyTradeDetail(upayWxOrder, payType, activityMap, goodsMap, commonDict);
                if (!CollectionUtils.isEmpty(userMoneyTradeDetail)) {
                    return userMoneyTradeDetail;
                }
            }
        }
        return null;
    }

    /***
     * 映射用户记录推送converter
     * @param upayWxOrder
     * @param shopMallMap
     */
    private Map<String, Object> covertUserMoneyTradeDetailForProductGoodsShopping(UpayWxOrder upayWxOrder,
                                                                                  Integer payType,
                                                                                  Map<Long, Map> shopMallMap) {
        if (!CollectionUtils.isEmpty(this.userMoneyTradeDetailConverters)) {
            for (UserMoneyTradeDetailConverter converter : this.userMoneyTradeDetailConverters) {
                Map<String, Object> userMoneyTradeDetail = converter.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, payType, shopMallMap);
                if (!CollectionUtils.isEmpty(userMoneyTradeDetail)) {
                    return userMoneyTradeDetail;
                }
            }
        }
        return null;
    }

    /***
     * 查询订单记录
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @param type
     * @return
     */
    private Map<String, Object> queryPresentOrder(Long merchantID, Long shopID, Long userID, int offset, int size, int type) {
        //查询订单
        Map<String, Object> upayWxOrderParams = this.selectUpayOrderParams(merchantID, shopID, userID, offset, size, type);
        upayWxOrderParams.put("type", OrderTypeEnum.USERCASH.getCode());//用户提现
        upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());//用户提现
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
        int countupayWxOrder = this.upayWxOrderService.countUpayWxOrder(upayWxOrderParams);
        if (upayWxOrders == null || upayWxOrders.size() == 0) {//为空时的非正常处理展现
            Map<String, Object> respMap = new HashMap<String, Object>();
            respMap.put("offset", offset);
            respMap.put("limit", size);
            respMap.put("total", 0);
            respMap.put("content", new ArrayList<>());
            return respMap;
        }

        return this.returnInfoObject(offset, size, countupayWxOrder, this.addContent(upayWxOrders), null);
    }

    private Map<String, Object> selectUpayOrderParams(Long merchantID, Long shopID, Long userID, int offset, int size, Integer type) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("mchId", merchantID);
        map.put("shopId", shopID);
        map.put("userId", userID);
        map.put("size", size);
        map.put("offset", offset);
        if (type == null) {
            map.put("type", type);
        }
        map.put("order", "payment_time");
        map.put("direct", "desc");
        return map;
    }

    /***
     * 返回信息结果赋值
     * @param offset
     * @param size
     * @param total
     * @param list
     * @return
     */
    private Map<String, Object> returnInfoObject(int offset, int size, int total, List<Object> list, BigDecimal amount) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("offset", offset);
        respMap.put("limit", size);
        respMap.put("total", total);
        respMap.put("content", list);
        if (amount != null) {
            respMap.put("sumMoney", amount.setScale(2, BigDecimal.ROUND_DOWN).toString());//收入记录-汇总金额
        }
        return respMap;
    }

    /***
     * 对返回结果的content对象进行赋值
     * @param upayWxOrders
     * @return
     */
    private List<Object> addContent(List<UpayWxOrder> upayWxOrders) {
        List<Object> list = new ArrayList<Object>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            Map<String, Object> map = new HashMap<>();
            map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
            map.put("price", "-" + upayWxOrder.getTotalFee());
            list.add(map);
        }
        return list;
    }

    /***
     * 企业付款-用户提现
     * @param map
     * @return
     */
    @Override
    public SimpleResponse createMktTransfers(Map<String, Object> map) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> respMap = new HashMap<String, Object>();
        //入参安全性验证
        if (!(StringUtils.isNotBlank(map.get("userId").toString()) && StringUtils.isNotBlank(map.get("amount").toString())
                && RegexUtils.StringIsNumber(map.get("userId").toString()) && RegexUtils.StringIsNumber(map.get("amount").toString())
                && StringUtils.isNotBlank(map.get("shopId").toString()) && RegexUtils.StringIsNumber(map.get("shopId").toString()))) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        /**
         * step1:基础数据梳理
         */
        Long userID = NumberUtils.toLong(map.get("userId").toString());
        BigDecimal amount = new BigDecimal(map.get("amount").toString());
        Long shopID = NumberUtils.toLong(map.get("shopId").toString());
        if (amount.compareTo(BigDecimal.ZERO) == 0 || amount.compareTo(new BigDecimal("2")) < 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            d.setMessage("最低2元提现");
            return d;
        }

//        String str = "目前为测试阶段";
//        if(StringUtils.isNotBlank(str)){
//            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
//            d.setMessage("请稍后...");
//            return d;
//        }

        //redis去重
        if (!this.stringRedisTemplate.opsForValue().setIfAbsent(RedisKeyConstant.USERPRESENTMONEY + userID, "")) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("微信提现正在触发，请勿重复");
            return d;
        }

        try {

            User user = this.userService.selectByPrimaryKey(userID);
            MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
            UpayBalance upayBalance = this.upayBalanceService.query(merchantShop.getMerchantId(), shopID, userID);
            //用户的安全性验证
            if (user == null || merchantShop == null || upayBalance == null) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                d.setMessage("该用户|店铺|余额 不存在");
                return d;
            }

            //余额的准确性处理 BigDecimal 0表示相等，-1表示小于，1表示大于
            if (upayBalance.getBalance().compareTo(BigDecimal.ZERO) == 0 || upayBalance.getBalance().compareTo(amount) < 0) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                d.setMessage("提现余额和账户余额不匹配，提现余额：" + amount + "元，账户余额：" + upayBalance.getBalance() + "元");
                log.error("提现余额和账户余额不匹配，提现余额：{} 元，账户余额：{}", amount, upayBalance.getBalance());
                return d;
            }

            /**
             * step2:调用微信企业支付接口
             */
            //提现描述
            String remark = wxpayProperty.getMfName() + "-" + wxpayProperty.getDesc() + userID;
            Map<String, Object> reqMapParams = this.weixinPayService.getTransferReqParams(user.getOpenId(), null, amount, remark, WeixinPayConstants.PAYSCENE_APPLET_APPID);
            Map<String, Object> xmlMap = this.weixinPayService.transfer(reqMapParams);
            log.info("用户提现-微信支付返回：xmlMap {}", xmlMap);
            //通信标识+交易标识  状态都成功时 做数据处理
            if (xmlMap == null) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
                return d;
            }
            if ((StringUtils.isNotBlank(xmlMap.get("return_code").toString()) && "FAIL".equals(xmlMap.get("return_code").toString().toUpperCase()))
                    || (StringUtils.isNotBlank(xmlMap.get("result_code").toString()) && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase()))) {
                if (StringUtils.isNotBlank(xmlMap.get("result_code").toString())
                        && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase())
                        && StringUtils.isNotBlank(xmlMap.get("err_code").toString().toUpperCase())
                        && "SYSTEMERROR".equals(xmlMap.get("err_code").toString().toUpperCase())) {//为这种错误时，需确认是否真的失败
                    boolean checkStatus = this.weixinPayService.checkTransfers(xmlMap.get("nonce_str").toString(), xmlMap.get("sign").toString(), xmlMap.get("partner_trade_no").toString());
                    if (!checkStatus) {
                        //TODO:提现失败，是重试，还是让用户重新走流程
                        d.setStatusEnum(ApiStatusEnum.ERROR_WXPAY);
                        d.setMessage("微信零钱出现异常，请重新发起提现请求");
                    }
                }
                d.setStatusEnum(ApiStatusEnum.ERROR_WXPAY);
                if (StringUtils.isNotBlank(xmlMap.get("return_msg").toString()) || StringUtils.isNotBlank(xmlMap.get("err_code_des").toString())) {
                    d.setMessage(StringUtils.isNotBlank(xmlMap.get("err_code_des").toString()) ? xmlMap.get("err_code_des").toString() : xmlMap.get("return_msg").toString());
                }
                return d;
            }
            //插入成功，设置过期时间
            stringRedisTemplate.expire(RedisKeyConstant.USERPRESENTMONEY + userID, 4, TimeUnit.SECONDS);
            /**
             * step3:db处理  订单表的处理 订单表-的创建-用户余额的更新
             */
            //订单表的创建
            String orderName = user.getNickName() + "余额提现";
            UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(userID, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.USERCASH.getCode(),
                    xmlMap.get("partner_trade_no").toString(), orderName, merchantShop.getMerchantId(), shopID,
                    OrderStatusEnum.ORDERED.getCode(), amount, IpUtil.getServerIp(), xmlMap.get("payment_no").toString(), remark,
                    DateUtil.parseDate(xmlMap.get("payment_time").toString(), null, "yyyy-MM-dd HH:mm:ss"),
                    null, JSON.toJSONString(reqMapParams), null, null);
            this.upayWxOrderService.create(upayWxOrder);
            //更新余额表
            upayBalance.setBalance(BigDecimal.ZERO);
            this.upayBalanceService.update(upayBalance);
        } finally {
            //结束时，对key进行删除处理
            this.stringRedisTemplate.delete(RedisKeyConstant.USERPRESENTMONEY + userID);
        }

        return d;
    }
}
