package mf.code.api.applet;

import mf.code.api.applet.service.MyPageService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.applet.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月02日 17:19
 */
@RestController
@RequestMapping("/api/applet/mypage")
public class MyPageApi {
    @Autowired
    private MyPageService myPageService;

    /**
     * 查询我的页面
     */
    @RequestMapping(path = "/queryMypage", method = RequestMethod.GET)
    public SimpleResponse queryMypage(@RequestParam(name = "userId") final Long userID,
                                      @RequestParam(name = "shopId") final Long shopID) {
        return this.myPageService.queryMypage(userID, shopID);
    }



    /**
     * 查询我的卡券
     */
    @RequestMapping(path = "/queryMyShopCoupon", method = RequestMethod.GET)
    public SimpleResponse queryMyShopCoupon(@RequestParam(name = "userId") final Long userID,
                                            @RequestParam(name = "shopId") final Long shopID,
                                            @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                            @RequestParam(name = "limit", required = false, defaultValue = "10") int size) {
        return this.myPageService.queryMyShopCoupon(userID, shopID, offset, size);
    }

    /**
     * 查询我的中奖记录
     */
    @RequestMapping(path = "/queryMyAwardLog", method = RequestMethod.GET)
    public SimpleResponse queryMyAwardLog(@RequestParam(name = "userId") final Long userID,
                                          @RequestParam(name = "shopId") final Long shopID,
                                          @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                          @RequestParam(name = "limit", required = false, defaultValue = "10") int size) {
        return this.myPageService.queryMyAwardLogV2(userID, shopID, offset, size);
    }

    /**
     * 查询我参与的任务
     */
    @RequestMapping(path = "/queryMyJoinTask", method = RequestMethod.GET)
    public SimpleResponse queryMyJoinTask(@RequestParam(name = "userId") final Long userID,
                                          @RequestParam(name = "shopId") final Long shopID,
                                          @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                          @RequestParam(name = "limit", required = false, defaultValue = "10") int size,
                                          @RequestParam(name = "type", required = false, defaultValue = "merchant") String type) {
        return this.myPageService.queryMyJoinTaskV2(userID, shopID, offset, size, type);
    }

    /**
     * 查询我的订单
     */
    @RequestMapping(path = "/queryMyOrder", method = RequestMethod.GET)
    public SimpleResponse queryMyOrder(@RequestParam(name = "userId") final Long userID,
                                       @RequestParam(name = "merchantId") final Long merchantID,
                                       @RequestParam(name = "shopId") final Long shopID,
                                       @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                       @RequestParam(name = "limit", required = false, defaultValue = "10") int size) {
        return this.myPageService.queryMyOrder(userID, merchantID, shopID, offset, size);
    }

    /**
     * 查询我的已购商品
     */
    @RequestMapping(path = "/queryMyGoods", method = RequestMethod.GET)
    public SimpleResponse queryMyGoods(@RequestParam(name = "userId") final Long userID,
                                       @RequestParam(name = "shopId") final Long shopID,
                                       @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                       @RequestParam(name = "limit", required = false, defaultValue = "10") int size) {
        return this.myPageService.queryMyGoods(userID, shopID, offset, size);
    }
}
