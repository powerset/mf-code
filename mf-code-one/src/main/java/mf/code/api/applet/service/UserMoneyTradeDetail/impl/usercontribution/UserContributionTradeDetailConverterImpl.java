package mf.code.api.applet.service.UserMoneyTradeDetail.impl.usercontribution;

import com.alibaba.excel.util.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyIndexNameEnum;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 19:21
 */
@Service
@Slf4j
public class UserContributionTradeDetailConverterImpl extends UserMoneyTradeDetailService {

    @Override
    public Map<String, Object> covertUserMoneyTradeDetailForContribution(UpayWxOrder upayWxOrder, Integer payType, Map<Long, User> userMap) {
        boolean notNullPayType = payType != null && payType == 1;
        boolean userPayType = OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType();
        boolean contribution = BizTypeEnum.CONTRIBUTION.getCode() == upayWxOrder.getBizType();

        if (notNullPayType && userPayType && contribution && !CollectionUtils.isEmpty(userMap)) {
            User user = userMap.get(upayWxOrder.getBizValue());
            if (user == null) {
                return new HashMap<>();
            }
            Map<String, Object> map = new HashMap<>();
            map.put("title", user.getNickName() + "购买商品贡献");
            map.put("type", UserMoneyIndexNameEnum.SHOPPING_CONTRIBUTION.getCode());
            map.put("indexName", UserMoneyIndexNameEnum.SHOPPING_CONTRIBUTION.getMessage());
            String plus_minus = "+";
            //直接删除多余的小数位，如2.357会变成2.35
            map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());
            map.put("time", DateFormatUtils.format(upayWxOrder.getCtime(), "yyyy.MM.dd HH:mm"));
            return map;
        }
        return null;
    }
}
