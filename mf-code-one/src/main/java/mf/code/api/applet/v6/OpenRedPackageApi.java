package mf.code.api.applet.v6;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v6.service.OpenRedPackageService;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.applet.v6
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-14 09:47
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v6/userActivity")
public class OpenRedPackageApi {
	@Autowired
	private OpenRedPackageService openRedPackageService;
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	/**
	 * 用户进入首页或详情页 -- 活动主页 --通过场景值区分 获取 拆红包弹框类型
	 * @param shopId
	 * @param userId
	 * @return
	 * <per>
	 *     0. 什么都不弹
	 *     1. 弹出 帮好友拆了1.2元，快去找好友帮拆你的红包吧 -- 活动详情页
	 *     2. 弹出 您有一个红包待拆开 -- 活动列表
	 *     3. 弹出 红包已被抢光了，下次早点来！ -- 活动详情页
	 *     4. 弹出 任务失败，再来挑战一次 -- 活动详情页
	 *     5. 弹出 你已有一个红包啦，不要贪心哦 -- 活动详情页
	 *     6. 弹出 恭喜您！已拆得所有现金红包 -- 活动详情页
	 *     7. 弹出 恭喜您！好友帮你拆出了红包 -- 活动详情页
	 *     8. 弹出 分享给你一个5元红包，立即来拆开吧 -- 活动详情页
	 *     9. 弹出 完成下单即可提现现金 -- 提现
	 *     10.弹出 完成支付即可提现现金 -- 提现
	 *     11.弹出 您的钱包中有5元可提现 还有10元待解锁 -- 提现
	 *     12.弹出 你有10元红包待拆开 还差2个好友就能拆开啦 -- 首页和商品详情页
	 *     13.弹出 你有10元红包待提现 -- 首页和商品详情页
	 * </per>
	 */
	@GetMapping("/openRedPackagePopup")
	public SimpleResponse openRedPackagePopup(@RequestParam("shopId") String shopId,
	                                          @RequestParam("userId") String userId,
	                                          @RequestParam("scenes") String scenes) {
		if (StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
				|| StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
				|| StringUtils.isBlank(scenes) || !RegexUtils.StringIsNumber(scenes)) {
			log.error("参数异常, shopId = {}, userId = {}, scenes", shopId, userId, scenes);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
		}

		return openRedPackageService.openRedPackagePopup(Long.valueOf(shopId), Long.valueOf(userId), Integer.valueOf(scenes));
	}

	/**
	 * 2.您有一个红包待拆开； 14.恭喜成功提现10元，再送你10元红包 -转2状态；8.分享给你一个5元红包，立即来拆开吧
	 * @param activityDefId
	 * @param activityId
	 * @param userId
	 * @return
	 */
	@GetMapping("/openRedPackage")
	public SimpleResponse openRedPackage(@RequestParam("userId") String userId,
	                                     @RequestParam(value = "activityDefId", required = false) String activityDefId,
	                                     @RequestParam(value = "activityId", required = false) String activityId) {
		if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)) {
			log.error("参数异常, userId = {}", userId);
			return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "参数异常");
		}
		if (StringUtils.isNotBlank(activityId) && RegexUtils.StringIsNumber(activityId)) {
			// 防重
			boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
			if (!success) {
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "点击太快，请稍候再试");
			}
			SimpleResponse result = openRedPackageService.helpOpenRedPackage(Long.valueOf(activityId), Long.valueOf(userId));
			stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
			return result;
		}
		if (StringUtils.isNotBlank(activityDefId) && RegexUtils.StringIsNumber(activityDefId)) {
			// 非分享邀请 的拆红包 -- 无帮拆
			// 防重
			boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityDefId + ":" + userId);
			if (!success) {
				return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "点击太快，请稍候再试");
			}
			SimpleResponse result = openRedPackageService.createOpenRedPackage(Long.valueOf(activityDefId), Long.valueOf(userId));
			stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityDefId + ":" + userId);
			return result;
		}
		log.error("参数异常, activityDefId = {}, activityId = {}", activityDefId, activityId);
		return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "参数异常");
	}
}
