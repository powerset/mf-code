package mf.code.api.applet.service.impl;

import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityAboutService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.UactivityService;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.caller.wxmp.WeixinMpConstants;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.FakeDataUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年10月29日 15:44
 */
@Service
public class UactivityServiceImpl implements UactivityService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private UserService userService;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private ActivityAboutService activityAboutService;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private UserTaskService userTaskService;

    //TODO:后期存储到mongo
    private static List<String> POSTER_DOCUMENTS = new ArrayList<String>();

    static {
        POSTER_DOCUMENTS.add("有福同享，话不多说，我希望和你一起领奖！");
        POSTER_DOCUMENTS.add("见证我们感情的时候到了，快来和我一起参与抽奖吧！");
        POSTER_DOCUMENTS.add("急死了，我正在参与免单抽奖，快来帮帮我~");
        POSTER_DOCUMENTS.add("只剩下一分钟就开奖了，快来和我一起参与");
    }

    /***
     * 分享海报
     * @param merchantID 商户编号
     * @param shopID 店铺编号
     * @param activityID 活动编号
     * @param userID 用户编号
     * @param newManStartId 新人有礼发起者编号
     * @param fromID 来源编号
     * @return
     */
    @Override
    public SimpleResponse sharePoster(Long merchantID, Long shopID, Long activityID, Long userID, Long newManStartId, Long fromID, Long activityDefId) {
        SimpleResponse d = new SimpleResponse();
        if (merchantID == null || merchantID == 0 || shopID == null || shopID == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        User user = this.userService.selectByPrimaryKey(userID);
        if (user == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该用户编号不在我们的用户库里");
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该店铺不存在");
        }
        Activity activity = this.activityService.findById(activityID);
        Map<String, Object> resp = new HashMap<String, Object>();
        if (activity != null) {
            Goods goods = this.goodsService.selectById(activity.getGoodsId());
            resp.put("title", "");
            resp.put("title1", "");
            resp.put("title2", "");
            resp.put("picUrl", "");
            resp.put("price", BigDecimal.ZERO);
            resp.put("goodsId", 0L);
            if (goods != null && StringUtils.isNotBlank(goods.getDisplayGoodsName())) {
                resp.put("title", goods.getDisplayGoodsName());
                if (goods.getDisplayGoodsName().length() <= 9) {
                    resp.put("title1", goods.getDisplayGoodsName());
                    resp.put("title2", "");
                } else {
                    String title1 = goods.getDisplayGoodsName().substring(0, 9);
                    resp.put("title1", title1);
                    String title2 = goods.getDisplayGoodsName().replaceAll(title1, "");
                    resp.put("title2", title2.length() > 9 ? title2.substring(0, 9) : title2);
                }
                resp.put("picUrl", StringUtils.isNotBlank(goods.getPicUrl()) ? this.ossCaller.cdnReplace(goods.getPicUrl()) : "");
                resp.put("price", goods.getDisplayPrice());
                resp.put("goodsId", goods.getId());
            }
        }

        resp.put("avatarUrl", user.getAvatarUrl());
        resp.put("nickName", user.getNickName());

        resp.put("shopName", merchantShop.getShopName());
        resp.put("documents", POSTER_DOCUMENTS);

        Object[] objects = new Object[]{merchantID, shopID, WeixinMpConstants.SCENE_POSTER, activityID, userID, fromID, newManStartId, activityDefId};
        String wxaCodeUnlimit = this.weixinMpService.getWXACodeUnlimit(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), StringUtil.join(objects, ","), WeixinMpConstants.WXCODESHOPPOSTERPATH + "/" + shopID + "/", 430, false);
        if (StringUtils.isBlank(wxaCodeUnlimit)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "生成小程序码失败");
        }
        resp.put("appletCode", wxaCodeUnlimit);
//        resp.put("appletCode", "");
//        String posterCodeUrl = CDNURL + "/" + bucketCatalog + WeixinMpConstants.APPLETACTIVITYSHAREPOSTERPATH + shopID + "/" + userID + "/" + activityID + ".png";
//        if (BufferedImageUtil.checkUrlIsValid(posterCodeUrl)) {
//            resp.put("posterCodeUrl", posterCodeUrl);
//        } else {
//            this.posterService.syncGenerateAppletPoster(
//                    user.getAvatarUrl(),
//                    goods.getPicUrl(),
//                    goods.getDisplayGoodsName(),
//                    goods.getDisplayPrice().setScale(2,BigDecimal.ROUND_DOWN).toString(),
//                    merchantID,
//                    shopID,
//                    activityID,
//                    userID,
//                    fromID,
//                    newManStartId
//            );
//            resp.put("posterCodeUrl", posterCodeUrl);
//        }
        d.setData(resp);
        return d;
    }

    /**
     * 功能描述: 活动页逻辑
     *
     * @param: [map] 其中 activityId,userId 必传
     * @return: mf.code.common.simpleresp.SimpleResponse
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/29 0029 11:22
     */
    @Override
    public SimpleResponse queryUserActivityIndex(Long activityID, Long userID, Integer sceneId, Long fromID) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        SimpleResponse d = new SimpleResponse();
        if (activityID == null || userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        if (activityID == 0) {
            return FakeDataUtil.activityData();
        }
        Activity activity = this.activityService.findById(activityID);
        //查询目的：获取 活动开始时间，活动结束时间
        if (activity == null || activity.getDel() == 1) {//等于1时 说明该活动已删除
            //活动不存在异常处理
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("活动已经不存在");
            return d;
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(activity.getShopId());
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            d.setMessage("根据活动表的shopid查询merchantshop,未查询到结果");
            return d;
        }

        respMap.put("isNextActivity", false);
        Activity nextActivity = null;
        if (sceneId == 1) {//该进入为分享进入
            nextActivity = this.checkShowNextActivity(activity, sceneId, userID);
            if (!nextActivity.getId().equals(activity.getId())) {
                respMap.put("isNextActivity", true);
                respMap.put("nextActivity", this.getNextActivityAbout(nextActivity));
            }
        }

        //获取微信号
        Map<String, Object> customServiceMap = this.activityAboutService.getServiceWx(activity);

        //查询活动参与人数
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(activityID);
        //查询活动用户邀请人数
        List<String> activityPersonInviteList = this.redisService.queryRedisJoinInviteUser(activityID, userID);

        UserTask userTask = userTaskService.findByUserIdActivityId(activityID, userID);

        respMap.put("activityDetail", this.activityAboutService.queryActivityAndGoods(activity, merchantShop, activityPersonList, customServiceMap));
        respMap.put("activityJoin", this.activityAboutService.queryUserActivityJoin(userID, activity, activityPersonList, activityPersonInviteList, merchantShop, customServiceMap));
        respMap.put("activityJoinPerson", this.activityAboutService.queryRedisInviteUser(activity, activityID, activityPersonList));
        respMap.put("activityWinnerPerson", this.activityAboutService.queryRedisWinner(activity, activityPersonList));
        respMap.put("taskId", userTask != null ? userTask.getId() : 0);
        d.setData(respMap);
        return d;
    }

    /***
     * 获取下个活动的相关信息
     * @param nextActivity
     * @return
     */
    private Map<String, Object> getNextActivityAbout(Activity nextActivity) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("goods", this.activityAboutService.queryGoodsAbout(nextActivity.getGoodsId()));
        //查询活动参与人数
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(nextActivity.getId());
        map.put("joinLess", nextActivity.getCondPersionCount() - activityPersonList.size());//活动开奖人数差额
        map.put("activityId", nextActivity.getId());//活动编号
        map.put("activityType", nextActivity.getType());//0:幸运大转盘 1:计划类抽奖  2:免单商品抽奖发起者 3:免单商品抽奖参与者 4:新手有礼发起者 5:新手有礼抽奖参与者
        return map;
    }

    /***
     * 验证是否跳转下一个活动
     * @param activity
     * @param sceneId
     * @param userID
     * @return
     */
    private Activity checkShowNextActivity(Activity activity, Integer sceneId, Long userID) {
        //已开奖未结束
        Boolean awardedButNoEnd = activity.getStatus() == 1 && System.currentTimeMillis() < activity.getEndTime().getTime() && activity.getAwardStartTime() != null;
        //已结束
        Boolean awardedAndEnd = activity.getStatus() == 2;
        //过期失效
        Boolean timeOutActivity = activity.getStatus() == -1;
        //判断该活动是否过期(已开奖未结束、已结束、过期失效)，并且场景是分享,并且是未参加的人
        boolean nextActivity = (awardedButNoEnd || awardedAndEnd || timeOutActivity) && sceneId == 1;
        if (!nextActivity) {
            return activity;
        }
        //查询该店铺下其余的活动，未开奖的
        Map<String, Object> activityParams = new HashMap<String, Object>();
        activityParams.put("merchantId", activity.getMerchantId());
        activityParams.put("shopId", activity.getShopId());
        activityParams.put("types", Arrays.asList(ActivityTypeEnum.NEW_MAN_START.getCode(), ActivityTypeEnum.ORDER_BACK_START.getCode(), ActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
        activityParams.put("status", ActivityStatusEnum.PUBLISHED.getCode());
        activityParams.put("del", DelEnum.NO.getCode());
        activityParams.put("awardStartTimeIsNull", "");
        List<Activity> activities = this.activityService.findPage(activityParams);
        if (activities != null && activities.size() > 0) {
            //判断该用户是否已经参加该活动
            Map<String, Object> userActivityParams = new HashMap<>();
            userActivityParams.put("merchantId", activity.getMerchantId());
            userActivityParams.put("shopId", activity.getShopId());
            userActivityParams.put("activityId", activity.getId());
            userActivityParams.put("userId", userID);
            userActivityParams.put("statusList", Arrays.asList(
                    UserActivityStatusEnum.NO_WIN.getCode(),
                    UserActivityStatusEnum.WINNER.getCode(),
                    UserActivityStatusEnum.AWARD_EXPIRE.getCode(),
                    UserActivityStatusEnum.COUPON.getCode()
            ));
            List<UserActivity> userActivities = this.userActivityService.pageListUserActivity(userActivityParams);
            if (userActivities == null || userActivities.size() == 0) {
                activity = activities.get(0);
            }
        }
        return activity;
    }
}
