package mf.code.api.applet.service.UserMoneyTradeDetail.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyIndexNameEnum;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailService;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 19:21
 */
@Service
@Slf4j
public class VirtualRecordTradeDetailConverterImpl extends UserMoneyTradeDetailService {
    @Override
    public Map<String, Object> getUserMoneyTradeDetail(UpayWxOrder upayWxOrder,
                                                       Integer payType,
                                                       Map<Long, Activity> activityMap,
                                                       Map<Long, Goods> goodsMap,
                                                       CommonDict commonDict) {
        //PALTFORMRECHARGE：平台充值 USERCASH：用户提现 USERPAY：用户支付 PALTFORMREFUND：平台退款
        boolean nullPayType = payType != null;
        boolean notShopping = BizTypeEnum.SHOPPING.getCode() != upayWxOrder.getBizType()
                && BizTypeEnum.SHOP_MANAGER_PAY.getCode() != upayWxOrder.getBizType();
        if (nullPayType && notShopping) {
            Map<String, Object> map = new HashMap<>();
            if (payType == 1) {//微信充值，发起活动
                map.put("title", "微信充值发起抽奖活动");
                map.put("type", 7);
            }
            if (payType == 2) {
                map.put("title", "抽奖活动退款至微信-到账");
                map.put("type", 7);
            }
            map.put("indexName", UserMoneyIndexNameEnum.WALLET_EXPENDITURE.getMessage());
            String plus_minus = "-";
            map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());//直接删除多余的小数位，如2.357会变成2.35
            map.put("time", "");
            if (upayWxOrder.getPaymentTime() != null) {
                map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
            }
            return map;
        }
        return null;
    }
}
