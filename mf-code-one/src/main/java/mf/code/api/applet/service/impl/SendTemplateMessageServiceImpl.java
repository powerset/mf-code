package mf.code.api.applet.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.SendTemplateMessage.CountDownType;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMessageConverter;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.api.feignclient.CommAppService;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.templatemessge.OneTemplateMessageData;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.dto.UserResp;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月27日 11:14
 */
@Service
@Slf4j
public class SendTemplateMessageServiceImpl implements SendTemplateMessageService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private List<SendTemplateMessageConverter> sendTemplateMessageConverters;

    @Autowired
    private CommAppService commAppService;
    @Autowired
    private WxmpProperty wxmpProperty;

    @Async
    @Override
    public void sendTemplateMsg2Award(Long shopID, Long activityID, Long nextAwardUserID) {
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        Activity activity = this.activityService.findById(activityID);
        if (merchantShop == null || activity == null) {
            log.warn("店铺编号为：{}，活动编号为：{}，推送消息时，对应的店铺|活动不存在", shopID, activityID);
            return;
        }
        this.covertAwardSendTemplateMsg(merchantShop, activity, nextAwardUserID);
    }

    @Async
    @Override
    public void sendTemplateMsg2Task(Long userTaskID, Long userID) {
        UserTask userTask = this.userTaskService.selectByPrimaryKey(userTaskID);
        if (userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
            return;
        }
        if (userTask == null) {
            log.warn("审核任务编号：{}，审核任务不存在", userTaskID);
            return;
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(userTask.getShopId());
        if (merchantShop == null) {
            log.warn("审核任务编号：{}，店铺不存在", userTaskID);
            return;
        }
        User user = this.userService.selectByPrimaryKey(userID);
        if (user == null) {
            log.warn("审核任务编号：{}，用户编号:{},用户不存在", userTaskID, userID);
            return;
        }
        Activity activity = this.activityService.findById(userTask.getActivityId());
        if (activity == null) {
            log.warn("审核任务编号：{}，审核任务对应的活动不存在", userTaskID);
            return;
        }
        Goods goods = this.goodsService.selectById(activity.getGoodsId());
        this.covertUserTaskSendTemplateMsg(merchantShop, activity, userTask, user, goods);
    }

    /***
     * 倒计时任务推送消息
     * @param goodsId
     */
    @Async
    @Override
    public void sendTemplateMsg2UserTaskCountDown(Long goodsId, Long userID, Long activityId) {
        this.covertCountDownSendTemplateMsg(CountDownType.COUNTDOWN_TASK.getCode(), userID, activityId, goodsId, null, null);
    }

    /***
     * 闯关通关某关的推送消息
     * @param userId
     * @param checkpointfinishNum
     */
    @Override
    public void sendTemplateMsg2CheckpointFinish(Long merchantId, Long shopId, Long userId, int checkpointfinishNum, CommonDict commonDict) {
        this.covertCheckpointFinishSendTemplateMsg(merchantId, shopId, userId, checkpointfinishNum, commonDict);
    }

    /**
     * 粉丝召回推送  复制叶辰
     * @param merchantId
     * @param shopId
     * @param userId
     * @param pushId
     * @param activityType
     */
    @Override
    public void sendWxPushRecall(Long merchantId, Long shopId, Long userId, Long pushId, Integer activityType) {
        String str = "";
        if (!CollectionUtils.isEmpty(this.sendTemplateMessageConverters)) {
            for (SendTemplateMessageConverter converter : this.sendTemplateMessageConverters) {
                str = converter.sendWxPushRecall(merchantId, shopId, userId, pushId,activityType);
                if (StringUtils.isNotBlank(str)) {
                    break;
                }
            }
        }
    }

    @Async
    @Override
    public void sendTemplateMsg2PlatformOrderBack(Activity activity, List<Long> userIds) {
        if (activity == null || CollectionUtils.isEmpty(userIds)) {
            log.error("参数异常，activity = {}, userIds = {}", activity, userIds);
            return;
        }
        Collection<User> users = userService.listByIds(userIds);
        if (CollectionUtils.isEmpty(users)) {
            log.error("查询不到数据, userIds = {}", userIds);
            return;
        }
        for (User user : users) {
            TemplateMsgDTO templateMsgDTO = new TemplateMsgDTO();
            templateMsgDTO.setTemplateId(wxmpProperty.getUserAwardResultMsgTmpId());
            UserResp userResp = new UserResp();
            userResp.setOpenId(user.getOpenId());
            userResp.setId(user.getId());
            templateMsgDTO.setUser(userResp);
            Map<String, Object> data = OneTemplateMessageData.freeOrderBack(
                    activity.getMerchantId(), activity.getShopId(), activity.getId());
            templateMsgDTO.setData(data);
            commAppService.sendJkmfTemplateMsg(templateMsgDTO);
        }
    }

    /***
     * 成团后的第二次提醒
     * @param userTask
     * @param user
     */
    @Override
    public void sendTemplateMsg2ActivityCountDown(UserTask userTask, User user) {
        this.covertCountDownSendTemplateMsg(CountDownType.AWARD_SECOND_REMIND.getCode(), null, null, null, userTask, user);
    }

    /***
     * 通关某关的财富推送消息
     * @param userId
     * @param checkpointfinishNum
     * @param commonDict
     */
    private void covertCheckpointFinishSendTemplateMsg(Long merchantId, Long shopId, Long userId, int checkpointfinishNum, CommonDict commonDict) {
        String str = "";
        if (!CollectionUtils.isEmpty(this.sendTemplateMessageConverters)) {
            for (SendTemplateMessageConverter converter : this.sendTemplateMessageConverters) {
                str = converter.sendTemplateMsg2CheckpointFinish(merchantId, shopId, userId, checkpointfinishNum, commonDict);
                if (StringUtils.isNotBlank(str)) {
                    break;
                }
            }
        }
    }

    /***
     * 映射开奖推送converter
     * @param merchantShop
     * @param activity
     * @param nextAwardUserID
     * @return
     */
    private void covertAwardSendTemplateMsg(MerchantShop merchantShop,
                                            Activity activity,
                                            Long nextAwardUserID) {
        String str = "";
        if (!CollectionUtils.isEmpty(this.sendTemplateMessageConverters)) {
            for (SendTemplateMessageConverter converter : this.sendTemplateMessageConverters) {
                str = converter.awardSendTemplateMessageConverter(merchantShop, activity, nextAwardUserID);
                if (StringUtils.isNotBlank(str)) {
                    break;
                }
            }
        }
    }

    /***
     * 映射中奖任务推送converter
     * @param activity
     * @param userTask
     * @param user
     * @param goods
     */
    private void covertUserTaskSendTemplateMsg(MerchantShop merchantShop,
                                               Activity activity,
                                               UserTask userTask,
                                               User user,
                                               Goods goods) {
        String str = "";
        if (!CollectionUtils.isEmpty(this.sendTemplateMessageConverters)) {
            for (SendTemplateMessageConverter converter : this.sendTemplateMessageConverters) {
                str = converter.userTaskSendTemplateMessageConverter(merchantShop, activity, userTask, user, goods);
                if (StringUtils.isNotBlank(str)) {
                    break;
                }
            }
        }
    }


    /***
     * 映射倒计时推送converter
     * @param countDownType
     * @param userID
     * @param activityId
     * @param goodsId
     * @param userTask
     * @param user
     */
    private void covertCountDownSendTemplateMsg(Integer countDownType,
                                                Long userID,
                                                Long activityId,
                                                Long goodsId,
                                                UserTask userTask,
                                                User user) {
        String str = "";
        if (!CollectionUtils.isEmpty(this.sendTemplateMessageConverters)) {
            for (SendTemplateMessageConverter converter : this.sendTemplateMessageConverters) {
                str = converter.countDownSendTemplateMessageConverter(countDownType, userID, activityId, goodsId, userTask, user);
                if (StringUtils.isNotBlank(str)) {
                    break;
                }
            }
        }
    }
}
