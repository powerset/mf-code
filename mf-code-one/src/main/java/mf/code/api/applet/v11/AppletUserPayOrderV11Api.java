package mf.code.api.applet.v11;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v11.service.AppletUserPayOrderV2Service;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.CertHttpUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.MD5Util;
import mf.code.common.utils.SortUtil;
import mf.code.one.dto.UserOrderReqDTO;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * mf.code.api.applet
 *
 * @description: 小程序用户支付订单
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 11:51
 */
@RestController
@RequestMapping("/api/applet/userOrder/v11")
@Slf4j
public class AppletUserPayOrderV11Api {
    @Autowired
    private AppletUserPayOrderV2Service appletUserPayOrderService;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @RequestMapping(path = "/creatOrder", method = RequestMethod.POST)
    public SimpleResponse createOrder(@RequestBody UserOrderReqDTO reqDTO) {
        if (reqDTO == null || !reqDTO.check()) {
            log.error("参数异常, reqDTO = {}", reqDTO);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        return this.appletUserPayOrderService.createUpayWxOrder(reqDTO);
    }

    /***
     * 查询订单的状态
     *
     * @param orderKeyID
     * @return
     */
    @RequestMapping(path = "/queryOrderStatus", method = RequestMethod.GET)
    public SimpleResponse queryOrderStatus(@RequestParam(name = "orderId") final Long orderKeyID) {
        return this.appletUserPayOrderService.queryOrderStatus(orderKeyID);
    }

    /**
     * 微信支付回调
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/wx/pay/callback")
    public String wxPayCallback(HttpServletRequest request) {
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        log.info("开始进入回调--");
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信支付回调失败!没有字段参数返回";
        }

        //验证签名
        Map<String, Object> checkSign = CertHttpUtil.paraFilter(wxXmlMap);
        String sortSign = SortUtil.buildSignStr(checkSign);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //对回调的参数进行验签
        if (!MD5Util.md5(sign).toUpperCase()
                .equals(wxXmlMap.get("sign"))) {
            return "wxpayNotify:微信支付回调失败!签名不一致";
        }
        //获取基本数据
        String resultCode = wxXmlMap.get("result_code").toString();
        if ("FAIL".equals(resultCode)) {
            return "支付失败";
        }
        // 订单号
        String orderNo = wxXmlMap.get("out_trade_no").toString();
        //流水号
        String transactionId = wxXmlMap.get("transaction_id").toString();

        // 金额,分转元
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(wxXmlMap.get("total_fee").toString())).divide(new BigDecimal(100));
        // 微信支付时间
        Date paymentTime = DateUtil.parseDate(wxXmlMap.get("time_end").toString(), null, "yyyyMMddHHmmss");
        //微信支付openid标识
        String payOpenId = wxXmlMap.get("openid").toString();

        return this.appletUserPayOrderService.wxPayCallback(orderNo, transactionId, totalFee, paymentTime, payOpenId);
    }
//
//    /**
//     * 微信  退款回调
//     *
//     * @param request
//     * @return
//     */
//    @PostMapping(value = "/wx/refund/callback")
//    public String wxRefundCallback(HttpServletRequest request) {
//        return this.appletUserPayOrderService.wxRefundCallback(request);
//    }
}
