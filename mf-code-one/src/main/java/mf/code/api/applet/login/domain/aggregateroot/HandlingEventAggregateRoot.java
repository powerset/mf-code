package mf.code.api.applet.login.domain.aggregateroot;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.login.domain.valueobject.InviteSpecification;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.user.constant.UserPubJoinTypeEnum;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-20 下午3:17
 */
@Data
@Slf4j
public class HandlingEventAggregateRoot {

	public static InvitationEvent newInvitationEvent(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser, InviteSpecification invitationSpecification) {
		if (appletPubUser == null || appletSubUser == null || invitationSpecification == null || appletPubUser.getUserId().equals(appletSubUser.getUserId())) {
			log.info("非邀请需求");
			return null;
		}

		if (!invitationSpecification.canInvitation(appletPubUser, appletSubUser)) {
			return null;
		}
		InvitationEvent invitationEvent = new InvitationEvent(appletPubUser, appletSubUser, invitationSpecification);
		Integer type = Integer.valueOf(invitationSpecification.getUserPubJoinType());
		if (type == UserPubJoinTypeEnum.CHECKPOINTS.getCode()) {
			List<UserPubJoin> userPubJoins = appletSubUser.getInvitationHistory().getRecords().get(type.toString());
			if (CollectionUtils.isEmpty(userPubJoins)) {
				appletSubUser.addInvitationEvent2History(type, invitationEvent.getRecordInfo());
				return invitationEvent;
			}
			for (UserPubJoin userPubJoin : userPubJoins) {
				if (userPubJoin.getShopId().equals(appletPubUser.getShopId())) {
					invitationEvent.resetRecordInfo(userPubJoin);
					appletSubUser.addInvitationEvent2History(type, invitationEvent.getRecordInfo());
					return invitationEvent;
				}
			}
		}

		appletPubUser.addInvitationEvent2History(invitationSpecification.getActivityId(), invitationEvent.getRecordInfo());
		return invitationEvent;
	}

}
