package mf.code.api.applet.v8.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:30
 */
public interface UserMoneyV8Service {
    /***
     * 查询我的提现记录
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse queryPresentOrderLog(Long merchantId, Long shopID, Long userID, int offset, int size);

    /***
     * 查询收入记录
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @param type 1:自购省 2：分享赚 3：任务奖金 4：任务佣金
     * @return
     */
    SimpleResponse queryIncomeLog(Long merchantId, Long shopID, Long userID, int offset, int size, Integer type, Integer section);

    /***
     * 查询交易明细
     * @param merchantID
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @return
     */
    SimpleResponse queryTradeDetail(Long merchantID, Long shopID, Long userID, int offset, int size, Long pageLastID);
}
