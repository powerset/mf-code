package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.ActivityUserService;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.service.AppletUserPayOrderService;
import mf.code.common.caller.wxpay.*;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.Constants;
import mf.code.common.exception.SqlUpdateFaildException;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.merchant.repo.po.Merchant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月09日 13:42
 */

@Service
@Slf4j
public class AppletUserPayOrderServiceImpl implements AppletUserPayOrderService {
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private UserService userService;
    @Autowired
    private WeixinPayServiceImpl weixinPayServiceImpl;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private ActivityUserService activityUserService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private AppletUserActivityService appletUserActivityService;
    @Autowired
    private ActivityDefService activityDefService;

    @Override
    public SimpleResponse createOrder(Map<String, Object> map, HttpServletRequest request, boolean isRobot) {
        SimpleResponse d = new SimpleResponse();
        //入参安全性验证
        if ((map.get("amount") == null || !RegexUtils.StringIsNumber(map.get("amount").toString()) ||
                map.get("shopId") == null || !RegexUtils.StringIsNumber(map.get("shopId").toString()) ||
                map.get("userId") == null || !RegexUtils.StringIsNumber(map.get("userId").toString()) ||
                map.get("merchantId") == null || !RegexUtils.StringIsNumber(map.get("merchantId").toString()) ||
                map.get("activityDefId") == null || !RegexUtils.StringIsNumber(map.get("activityDefId").toString()))) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        /***
         * step1:入参数据梳理
         */
        Long activityID = 0L;
        Long merchantID = NumberUtils.toLong(map.get("merchantId").toString());
        Long shopID = NumberUtils.toLong(map.get("shopId").toString());
        Long userID = NumberUtils.toLong(map.get("userId").toString());
        Long activityDefID = NumberUtils.toLong(map.get("activityDefId").toString());
        BigDecimal amount = new BigDecimal(map.get("amount").toString());
        Activity activity = null;
        //1：用户余额支付(默认余额)2微信支付
        Integer payType = null;
        if (map.get("payType") != null && RegexUtils.StringIsNumber(map.get("payType").toString())) {
            payType = NumberUtils.toInt(map.get("payType").toString());
        } else {
            //默认，用户余额支付
            payType = 1;
        }
        if (map.get("activityId") != null && RegexUtils.StringIsNumber(map.get("activityId").toString())) {
            activityID = NumberUtils.toLong(map.get("activityId").toString());
            activity = this.activityService.findById(activityID);
        } else {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityDefID);
            if (activityDef == null || activityDef.getStatus() != ActivityDefStatusEnum.PUBLISHED.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动已下线");
            }
            if (activityDef.getStock() < activityDef.getHitsPerDraw()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "库存不足啦");
            }
            // 根据def创建activity
            SimpleResponse simpleResponse = appletUserActivityService.createActivityFromDef(userID, null, activityDefID, ActivityStatusEnum.UNPUBLISHED.getCode());
            if (simpleResponse.error()) {
                return simpleResponse;
            }
            activityID = ((Activity) simpleResponse.getData()).getId();
            activity = (Activity) simpleResponse.getData();
        }

        /***
         * step2:对象入参的判定
         */
        User user = this.userService.selectByPrimaryKey(userID);
        if (!isRobot) {
            MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
            Merchant merchant = this.merchantService.getMerchant(merchantID);
            if (merchant == null || merchantShop == null || user == null) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                d.setMessage("该商户|该店铺|该用户|该商品不存在");
                return d;
            }
        }

        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = null;
        if (request == null) {
            ipAddress = IpUtil.getServerIp();
        } else {
            ipAddress = request.getHeader("X-Forwarded-For") != null && StringUtils.isNotBlank(request.getHeader("X-Forwarded-For").toString()) ?
                    request.getHeader("X-Forwarded-For").toString() : IpUtil.getServerIp();
        }

        //订单号
        String outTraderNo = "V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        //附加数据
        String attach = String.valueOf(userID);

        /**创建c端用户订单**/
        BizTypeEnum bizTypeEnum = null;
        if (activity.getType() == ActivityTypeEnum.MCH_PLAN.getCode()) {
            bizTypeEnum = BizTypeEnum.ACTIVITY;
        } else if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            bizTypeEnum = BizTypeEnum.NEWMAN_START;
        } else if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
            bizTypeEnum = BizTypeEnum.ASSIST_ACTIVITY;
        } else if (activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            bizTypeEnum = BizTypeEnum.ORDER_BACK;
        }

        //当用户为余额支付时,则直接扣款返回
        if (payType == 2) {
            return this.wxPayType(d, merchantID, userID, shopID, activityID, amount, outTraderNo, ipAddress, attach, user.getOpenId(), bizTypeEnum);
        } else {
            UpayBalance upayBalance = this.upayBalanceService.query(merchantID, shopID, userID);
            //余额的准确性处理 BigDecimal 0表示相等，-1表示小于，1表示大于
            if (upayBalance == null || upayBalance.getBalance().subtract(amount).compareTo(BigDecimal.ZERO) == -1) {
                d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                d.setMessage("钱包余额不足，不能发起");
                return d;
            }

            // begin
            //订单创建
            UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(userID, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.USERPAY.getCode(),
                    null, null, merchantID, shopID, OrderStatusEnum.ORDERED.getCode(), amount, IpUtil.getServerIp(),
                    null, attach, new Date(), null, null, bizTypeEnum.getCode(), activityID);
            //            this.upayWxOrderService.create(upayWxOrder);
            //余额相减
            //            upayBalance.setBalance(upayBalance.getBalance().subtract(amount));
            //            this.upayBalanceService.update(upayBalance);
            // over
            Integer andUpdateBalanceResult = upayWxOrderService.createAndUpdateBalance(upayWxOrder, amount);
            if (andUpdateBalanceResult == 0) {
                return new SimpleResponse(10, "创建订单/更新余额失败");
            }

            //更新:活动，库存(曲成支持)
            SimpleResponse simpleResponse = this.activityUserService.updateActivityDef4Callback(Constants.PayType.BALANCE_PAY, userID, activityID, upayWxOrder.getId(), shopID, activity.getGoodsId(), activityDefID, new Date(), upayWxOrder.getOrderNo());
            if (simpleResponse.getCode() != 0) {
                return simpleResponse;
            }

            Map<String, Object> respMap = this.addRespMap(null, null, null, null, upayWxOrder.getId(), activity.getId());
            d.setData(respMap);
            return d;
        }
    }

    @Override
    public SimpleResponse queryOrderStatus(Long orderKeyID) {
        SimpleResponse d = new SimpleResponse();
        //入参安全性验证
        if (orderKeyID == null || orderKeyID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        UpayWxOrder upayWxOrder = this.upayWxOrderService.selectByKeyId(orderKeyID);
        if (upayWxOrder == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该订单不存在");
            return d;
        }
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("status", upayWxOrder.getStatus());
        //下单中时,调用微信平台接口查验
        if (upayWxOrder.getStatus() == OrderStatusEnum.FAILED.getCode() || upayWxOrder.getStatus() == OrderStatusEnum.TIMEOUT.getCode()) {
            respMap.put("status", OrderStatusEnum.FAILED.getCode());
        }

        if (upayWxOrder.getStatus() == OrderStatusEnum.ORDERING.getCode() && System.currentTimeMillis() - upayWxOrder.getCtime().getTime() >= 2 * 60 * 1000) {
            respMap.put("status", OrderStatusEnum.FAILED.getCode());

            Map<String, Object> queryParams = this.weixinPayService.getQueryOrderParams(upayWxOrder.getOrderNo(), WeixinPayConstants.PAYSCENE_APPLET_APPID);
            Map<String, Object> respQueryOrderMap = this.weixinPayService.queryOrder(queryParams);
            log.info("查询微信该订单信息:{}", respQueryOrderMap);
            boolean returnCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("return_code") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("return_code").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("return_code"));
            boolean resultCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("result_code") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("result_code").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("result_code"));
            boolean tradeStateSuccess = respQueryOrderMap != null && respQueryOrderMap.get("trade_state") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("trade_state").toString());
            if (returnCodeSuccess && resultCodeSuccess && tradeStateSuccess) {
                /***
                 * SUCCESS—支付成功 REFUND—转入退款 NOTPAY—未支付 CLOSED—已关闭 REVOKED—已撤销（刷卡支付）USERPAYING--用户支付中 PAYERROR--支付失败(其他原因，如银行返回失败)
                 */
                upayWxOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
                upayWxOrder.setTradeId(respQueryOrderMap.get("transaction_id").toString());
                upayWxOrder.setPaymentTime(DateUtil.parseDate(respQueryOrderMap.get("time_end").toString(), null, "yyyyMMddHHmmss"));
                upayWxOrder.setUtime(new Date());
                this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);

                respMap.put("status", OrderStatusEnum.ORDERED.getCode());
            }
        }
        d.setData(respMap);
        return d;
    }

    @Override
    public String wxPayCallback(HttpServletRequest request) {
        SimpleResponse response;
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        log.info("开始进入回调--");
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信支付回调失败!没有字段参数返回";
        }

        //验证签名
        Map<String, Object> checkSign = CertHttpUtil.paraFilter(wxXmlMap);
        String sortSign = SortUtil.buildSignStr(checkSign);
        String sign = sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        //对回调的参数进行验签
        if (!MD5Util.md5(sign).toUpperCase()
                .equals(wxXmlMap.get("sign"))) {
            return "wxpayNotify:微信支付回调失败!签名不一致";
        }

        //获取基本数据
        String resultCode = wxXmlMap.get("result_code").toString();
        if ("FAIL".equals(resultCode)) {
            return "支付失败";
        }
        // 订单号
        String orderNo = wxXmlMap.get("out_trade_no").toString();
        // 金额,分转元
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(wxXmlMap.get("total_fee").toString())).divide(new BigDecimal(100));
        // 微信支付时间
        Date paymentTime = new Date(Long.parseLong(wxXmlMap.get("time_end").toString()));

        UpayWxOrder order = upayWxOrderService.selectByOrderNoPlus(orderNo);
        if (order == null || order.getTotalFee() == null) {
            log.error("该订单不存在or订单金额不存在");
            return WXPayUtil.resp();
        }
        if (totalFee.subtract(order.getTotalFee()).compareTo(BigDecimal.ZERO) != 0) {
            return "支付失败-下单金额跟回调的金额不匹配";
        }
        Long uid = order.getUserId();
        // activityId
        Long aid = order.getBizValue();
        Activity activity = this.activityService.findById(aid);
        Long activityDefId = activity.getActivityDefId();
        // 作为zset的key
        Long shopId = order.getShopId();
        // zset成员
        Long goodsId = activity.getGoodsId();

        try {
            // 状态更新成功
            // 支付  后回调
            response = activityUserService.updateActivityDef4Callback4Wx(OrderPayTypeEnum.WEIXIN.getCode(), uid, aid, order.getId(), shopId, goodsId, activityDefId, paymentTime, orderNo);
            if (response.getCode() == 0) {
                return WXPayUtil.resp();
            }
        } catch (SqlUpdateFaildException e) {
            log.error("SqlUpdateFaildException: {}", e);
        }
        return null;
    }

    @Override
    public String wxRefundCallback(HttpServletRequest request) {
        SimpleResponse response;
        /* 准备:  1  解析 微信参数   1.1 验签      2 防重   3. 获取所需参数*/
        Map<String, Object> wxXmlMap = weixinPayService.unifiedorderNotifyUrl(request);
        log.info(">>>>>>>>>>prase xml:{}", wxXmlMap);
        if (wxXmlMap == null || wxXmlMap.size() == 0 || (wxXmlMap.get("return_code") != null && wxXmlMap.get("return_code").equals("FAIL"))) {
            return "wxpayNotify:微信退款回调失败!没有字段参数返回";
        }

        String decryptDataReqInfo = null;
        if (wxXmlMap.get("return_code") != null && "SUCCESS".equals(wxXmlMap.get("return_code"))) {
            try {
                decryptDataReqInfo = DESUtil.decryptData(wxXmlMap.get("req_info").toString(), wxpayProperty.getMchSecretKey());
            } catch (Exception e) {
                log.error("Exceptionl:{}", e);
            }
        }
        log.info(">>>>>>>>>>decryptDataReqInfo:{}", decryptDataReqInfo);
        Map<String, Object> xmlMapReqInfo = new HashMap<>();
        if (StringUtils.isNotBlank(decryptDataReqInfo)) {
            xmlMapReqInfo = WXPayUtil.xmlToMap(decryptDataReqInfo);
            log.info(">>>>>>>>>>xmlMapReqInfo:{}", xmlMapReqInfo);
        }
        if (xmlMapReqInfo == null || xmlMapReqInfo.size() == 0) {
            return "wxpayNotify:微信退款回调失败!req_info解密没有字段返回";
        }
        //获取基本数据 // SUCCESS-退款成功 CHANGE-退款异常 REFUNDCLOSE—退款关闭
        String refundStatus = xmlMapReqInfo.get("refund_status").toString();
        if (!"SUCCESS".equals(refundStatus)) {
            return "退款未成功";
        }
        // 原订单号
        String orderNo = xmlMapReqInfo.get("out_trade_no").toString();
        // 退款金额
        BigDecimal totalFee = BigDecimal.valueOf(NumberUtils.toInt(xmlMapReqInfo.get("refund_fee").toString())).divide(new BigDecimal(100));

        //退款订单号
        String refundOrderNo = xmlMapReqInfo.get("out_refund_no").toString();

        UpayWxOrder order = upayWxOrderService.selectByOrderNoPlus(refundOrderNo);

        Long orderId = order.getId();
        Long uid = order.getUserId();
        Long aid = order.getBizValue();
        Activity activity = this.activityService.findById(aid);
        Long activityDefId = activity.getActivityDefId();
        // 作为zset的key
        Long shopId = order.getShopId();
        // zset成员
        Long goodsId = activity.getGoodsId();
        Long merchantId = activity.getMerchantId();

        try {
            // 状态更新成功
            response = upayWxOrderService.wxRefundCallback(orderId, uid);
            if (response.getCode() == 0) {
                return WXPayUtil.resp();
            }
        } catch (SqlUpdateFaildException e) {
            log.error("SqlUpdateFaildException: {}", e);
        }
        return null;
    }

    /***
     * 微信支付方式的支付
     * @param d
     * @param merchantID
     * @param userID
     * @param shopID
     * @param activityID
     * @param amount
     * @param outTraderNo
     * @param ipAddress
     * @param attach
     * @return
     */
    private SimpleResponse wxPayType(SimpleResponse d,
                                     Long merchantID,
                                     Long userID,
                                     Long shopID,
                                     Long activityID,
                                     BigDecimal amount,
                                     String outTraderNo,
                                     String ipAddress,
                                     String attach,
                                     String openID,
                                     BizTypeEnum bizTypeEnum) {
        String body = WeixinPayConstants.APPLETE_BODY;
        //拼装微信统一下单的参数
        Map<String, Object> unifiedorderReqParams = this.weixinPayService.getUnifiedorderReqParams(amount, outTraderNo, ipAddress,
                wxpayProperty.getMfAppId(), WeixinPayConstants.JSAPI, attach, openID, WeixinPayConstants.APPLET_NOTIFY_SOURCE, body);
        /***
         * step3:储存db
         */
        UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(userID, OrderPayTypeEnum.WEIXIN.getCode(), OrderTypeEnum.USERPAY.getCode(), outTraderNo,
                null, merchantID, shopID, OrderStatusEnum.ORDERING.getCode(), amount, ipAddress, null, attach, null, null,
                JSONObject.toJSONString(unifiedorderReqParams), bizTypeEnum.getCode(), activityID);
        this.upayWxOrderService.create(upayWxOrder);

        /***
         * step4: 拼接调用微信平台统一下单接口的参数
         */
        //统一下单的返回
        Map<String, Object> unifiedorderRespMap = this.weixinPayService.unifiedorder(unifiedorderReqParams);
        log.info("小程序支付后的结果 xmlMap:{}", unifiedorderRespMap);
        if (unifiedorderRespMap == null || (StringUtils.isNotBlank(unifiedorderRespMap.get("return_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("return_code").toString()))
                || (StringUtils.isNotBlank(unifiedorderRespMap.get("result_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("result_code").toString()))) {
            if (StringUtils.isNotBlank(unifiedorderRespMap.get("return_msg").toString())) {
                d.setMessage(unifiedorderRespMap.get("return_msg").toString());
                upayWxOrder.setRemark(unifiedorderRespMap.get("return_msg").toString());
            } else if (StringUtils.isNotBlank(unifiedorderRespMap.get("err_code_des").toString())) {
                d.setMessage(unifiedorderRespMap.get("err_code_des").toString());
                upayWxOrder.setRemark(unifiedorderRespMap.get("err_code_des").toString());
            } else {
                d.setMessage("微信充值异常...");
                upayWxOrder.setRemark("微信充值异常...");
            }
            //更新db
            upayWxOrder.setStatus(OrderStatusEnum.FAILED.getCode());
            upayWxOrder.setUtime(new Date());
            this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);
            return d;
        }

        /***
         * step5: 拼接参数返回前端--
         */
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("timeStamp", String.valueOf(DateUtil.getCurrentSeconds()));
        respMap.put("nonceStr", RandomStrUtil.randomStr(28));
        String packageInfo = "prepay_id=" + unifiedorderRespMap.get("prepay_id");
        respMap.put("package", packageInfo);
        respMap.put("signType", "MD5");
        String sortSign = SortUtil.buildSignStr(respMap);
        String sign = "appId=" + wxpayProperty.getMfAppId() + "&" + sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        log.info("小程序返回前端的sign：{}", sign);
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        //签名sign
        respMap.put("paySign", sign);
        respMap.put("orderId", upayWxOrder.getId());
        respMap.put("activityId", activityID);
        d.setData(respMap);
        return d;
    }

    private Map<String, Object> addRespMap(String timeStamp, String nonceStr, String packageInfo, String sign, Long orderkeyID, Long activityID) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("timeStamp", StringUtils.isNotBlank(timeStamp) ? timeStamp : "");
        respMap.put("nonceStr", StringUtils.isNotBlank(nonceStr) ? nonceStr : "");
        respMap.put("package", StringUtils.isNotBlank(packageInfo) ? packageInfo : "");
        respMap.put("signType", "MD5");
        //签名sign
        respMap.put("paySign", StringUtils.isNotBlank(sign) ? sign : "");
        respMap.put("orderId", orderkeyID);
        respMap.put("activityId", activityID);
        return respMap;
    }
}
