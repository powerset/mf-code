package mf.code.api.applet.v8;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v8.service.MinerV8Service;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.applet.v8
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 10:21
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/applet/v8/miner")
public class MinerV8Api {
    @Autowired
    private MinerV8Service minerV8Service;

    /***
     * 闯关页面信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @RequestMapping(path = "/queryCheckpoint", method = RequestMethod.GET, headers = {"token"})
    public SimpleResponse queryCheckpoint(@RequestParam(name = "merchantId") Long merchantId,
                                          @RequestParam(name = "shopId") Long shopId,
                                          @RequestParam(name = "userId") Long userId) {
        return minerV8Service.queryCheckpointInfo(merchantId, shopId, userId);
    }

    /**
     * 获取 新人任务（闯关任务） 弹窗
     *
     * @return
     */
    @GetMapping("/queryNewcomerTaskPopup")
    public SimpleResponse queryNewcomerTaskPopup(@RequestParam("shopId") String shopId,
                                                 @RequestParam("userId") String userId) {
        if (StringUtils.isBlank(shopId) || StringUtils.isBlank(userId)) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return minerV8Service.queryNewcomerTaskPopup(Long.valueOf(shopId), Long.valueOf(userId));
    }

    /***
     * 进度详情-收益明细
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @RequestMapping(path = "/queryMinerProgressDetail", method = RequestMethod.GET)
    public SimpleResponse queryMinerProgressDetail(@RequestParam(name = "merchantId") Long merchantId,
                                                   @RequestParam(name = "shopId") Long shopId,
                                                   @RequestParam(name = "userId") Long userId,
                                                   @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                                   @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0
                || userId == null || userId == 0) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return minerV8Service.queryMinerProgressDetail(merchantId, shopId, userId, offset, size);
    }

    /***
     * 矿工详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @RequestMapping(path = "/queryMinerDetail", method = RequestMethod.GET)
    public SimpleResponse queryMinerDetail(@RequestParam(name = "merchantId") Long merchantId,
                                           @RequestParam(name = "shopId") Long shopId,
                                           @RequestParam(name = "userId") Long userId,
                                           @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                           @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0
                || userId == null || userId == 0) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return minerV8Service.queryMinerDetail(merchantId, shopId, userId, offset, size);
    }
}
