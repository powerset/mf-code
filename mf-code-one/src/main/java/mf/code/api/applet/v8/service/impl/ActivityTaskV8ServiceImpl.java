package mf.code.api.applet.v8.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v8.service.ActivityTaskV8Service;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceAboutService;
import mf.code.api.feignservice.OneAppServiceImpl;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.exception.CommissionReduceException;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.JsonParseUtil;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.redis.RedisKeyConstant;
import mf.code.one.vo.UserCouponAboutVO;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.upay.service.UpayBalanceService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月21日 11:24
 */
@Service
@Slf4j
public class ActivityTaskV8ServiceImpl implements ActivityTaskV8Service {
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private CheckpointTaskSpaceAboutService checkpointTaskSpaceAboutService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserService userService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private UserTaskService userTaskService;

    @Override
    public SimpleResponse finishActivityTask(Long merchantId, Long shopId, Long activityDefId, Long userId, String kws) {
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "页面开小差啦");
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(user.getVipShopId());
        if (merchantShop == null) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "页面开小差啦");
        }

        merchantId = merchantShop.getMerchantId();
        shopId = merchantShop.getId();
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        if (activityDef == null) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该任务不存在");
        }
        //根据子活动查询父级活动定义
        ActivityDef upperActivityDef = null;
        if (activityDef.getParentId() != null && activityDef.getParentId() > 0 && !activityDef.getParentId().equals(activityDef.getId())) {
            upperActivityDef = activityDefService.selectByPrimaryKey(activityDef.getParentId());
        } else {
            upperActivityDef = activityDef;
        }
        if (upperActivityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该任务不存在");
        }
        //判断是否已经扣过库存
        boolean b = queryExistUserTask(merchantId, shopId, userId, activityDef.getParentId());
        if(!b){
            if (upperActivityDef.getStock() <= 0) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "任务库存不足啦");
            }
        }

        //判断任务合理性
        if (activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode()) {
            if (StringUtils.isBlank(kws)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "请填写口令");
            }
            if (!activityDef.getKeyWord().equals(kws)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "口令没有匹配，请重新输入");
            }
        }
        //判断是否已经做过
        Map<Long, List<UserCoupon>> longListMap = checkpointTaskSpaceAboutService.queryActivityDefInfo(merchantId, shopId, userId, null,
                UserCouponAboutVO.addNewbieTaskType());
        List<UserCoupon> userCoupons = longListMap.get(activityDef.getId());
        if (!CollectionUtils.isEmpty(userCoupons)) {
            for (UserCoupon userCoupon : userCoupons) {
                if (UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode() == userCoupon.getType()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "任务已完成啦");
                }
                if (UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode() == userCoupon.getType()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "任务已完成啦");
                }
            }
        }

        //防重处理
        String redisKey = RedisKeyConstant.NEWBIETASK_COMMIT_REPEAT + activityDef.getId();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, redisKey);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "点击太快，请稍候再试");
        }

        try {
            if (!b) {
                saveUserTask(merchantId, shopId, userId, activityDef.getParentId());
                //扣库存
                log.info("<<<<<<<< 开始扣库存： def:{}", upperActivityDef);
                Integer subStockResult = activityDefService.reduceStockAfterCallback(upperActivityDef.getId(), 1, upperActivityDef.getStock());
                if (subStockResult == 0) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "任务库存减少失败啦");
                }
            }
            //扣保证金，分配佣金
            UserCouponTypeEnum userCouponTypeEnum = null;
            if (activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode()) {
                userCouponTypeEnum = UserCouponTypeEnum.NEWBIE_TASK_BONUS;
            } else if (activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode()) {
                userCouponTypeEnum = UserCouponTypeEnum.NEWBIE_TASK_REDPACK;
            }
            CommissionDisDto commissionDisDto = new CommissionDisDto(userId, shopId,
                    userCouponTypeEnum, activityDef.getCommission());
            commissionDisDto.setActivityTaskId(null);
            commissionDisDto.setActivityDefId(activityDefId);
            commissionDisDto.setActivityId(null);
            try {
                CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);
            } catch (CommissionReduceException e) {
                log.error("Exception :{}", e);
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO15.getCode(), "啊哦！奖金被领完了，老板正在充钱呢~");
            } catch (Exception e) {
                log.error("Exception :{}", e);
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO15.getCode(), "服务开小差了，请稍后重试~");
            }

            //获取可提现金额
            BigDecimal canCashMoney = upayBalanceService.canCashMoney(merchantId, shopId, userId);
            // 1、计算佣金
            CommissionBO commissionBO = commissionService.commissionCalculate(commissionDisDto.getUserId(), commissionDisDto.getCommissionDef());

            Map map = new HashMap<>();
            map.put("canCashMoney", canCashMoney);
            map.put("amount", commissionBO.getCommission());
            SimpleResponse simpleResponse = new SimpleResponse();
            simpleResponse.setData(map);
            return simpleResponse;
        } finally {
            stringRedisTemplate.delete(redisKey);
        }
    }

    /***
     * 获取群码
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse getGroupCode(Long merchantId, Long shopId, Long activityDefId, Long userId) {
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        if (activityDef == null) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该任务不存在");
        }
        Map map = new HashMap<>();
        if (StringUtils.isNotBlank(activityDef.getServiceWx())) {
            if (JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(csMapList)) {
                    //微信二维码
                    map.put("groupCode", csMapList.get(0).get("pic"));
                }
            }
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(map);
        return simpleResponse;
    }

    private boolean saveUserTask(Long merchantId, Long shopId, Long userId, Long activityDefId) {
        Date now = new Date();
        UserTask userTask = new UserTask();
        userTask.setType(UserTaskTypeEnum.NEWBIE_TASK.getCode());
        userTask.setUserId(userId);
        userTask.setMerchantId(merchantId);
        userTask.setShopId(shopId);
        userTask.setActivityDefId(activityDefId);
        userTask.setActivityId(0L);
        userTask.setActivityTaskId(0L);
        userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
        userTask.setRead(0);
        userTask.setStockFlag(0);
        userTask.setCtime(now);
        userTask.setUtime(now);
        boolean save = userTaskService.save(userTask);
        return save;
    }

    private boolean queryExistUserTask(Long merchantId, Long shopId, Long userId, Long activityDefId) {
        QueryWrapper<UserTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserTask::getMerchantId, merchantId)
                .eq(UserTask::getShopId, shopId)
                .eq(UserTask::getUserId, userId)
                .eq(UserTask::getActivityDefId, activityDefId)
        ;
        UserTask one = userTaskService.getOne(wrapper);
        if (one != null) {
            return true;
        }
        return false;
    }
}
