package mf.code.api.applet.v2;/**
 * create by qc on 2018/12/24 0024
 */

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.service.HomePageService;
import mf.code.common.caller.wxmp.WeixinMpConstants;
import mf.code.common.constant.ActivityTaskStatusEnum;
import mf.code.activity.constant.ActivityTaskTypeEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.verify.VerifyService;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.service.MerchantShopService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author gbf
 * @description :
 */

@RestController
@RequestMapping("/api/applet/v2/home")
public class HomePageV2Api {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private HomePageService homePageService;
    @Autowired
    private VerifyService verifyService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ActivityTaskService activityTaskService;


    /**
     * 首页基础数据
     *
     * @param merchantId
     * @param shopId
     * @param userId
     * @param pageSize
     * @param lastId
     * @param scene
     * @return
     */
    @GetMapping("baseData")
    public SimpleResponse homePageV2(@RequestParam(name = "merchantId", required = true) final Long merchantId,
                                     @RequestParam(name = "shopId", required = true) final Long shopId,
                                     @RequestParam(name = "userId", required = true) final Long userId,
                                     @RequestParam(name = "date", required = false) final String date,
                                     @RequestParam(name = "pageSize", required = false, defaultValue = "10") final int pageSize,
                                     @RequestParam(name = "lastId", required = false) final Long lastId,
                                     @RequestParam(name = "scene", required = false) final String scene) {

        SimpleResponse response;
        Map respDataMap = new HashMap();

        //校验userId是否合法
        response = verifyService.userIdError(userId);
        if (response != null) {
            return response;
        }
        //校验merchantId和shopId 是否合法
        response = verifyService.merchantShopError(merchantId, shopId);
        if (response != null) {
            return response;
        }

        String result = stringRedisTemplate.opsForValue().get(RedisKeyConstant.NEWBIE + merchantId + shopId + userId);
        boolean newbie = false;
        if (result == null) {
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.NEWBIE + merchantId + shopId + userId, "true");
            newbie = true;
        }

        // 获取店铺信息
        Map shopInfoMap = merchantShopService.getShopInfo4HomepageV2(shopId);
        // 产品列表
        List<Map> goodsMap = goodsService.getHomePageDataV2(merchantId, shopId, userId);
        // 处理挂件信息
        Map widgetMap = homePageService.getMissionTypeByYYSuport(shopId, userId, merchantId);
        String rediskey = "mf:home:popout:userId" + userId + ":date:" + date;
        if (stringRedisTemplate.opsForValue().get(rediskey) == null) {
            widgetMap.put("popout", true);
            stringRedisTemplate.opsForValue().set(rediskey, "1");
            stringRedisTemplate.expire(rediskey, DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);
        } else {
            widgetMap.put("popout", false);
        }
        if (widgetMap != null && widgetMap.get("missionType") != null && "-1".equals(widgetMap.get("missionType").toString())) {
            widgetMap.put("popout", false);
        }
        Long stockTotal = 0L;
        for (Map m : goodsMap) {
            Integer stock = m.get("stock") == null ? 0 : Integer.parseInt(m.get("stock").toString());
            stockTotal = stockTotal + stock;
        }
        // 查询两个统计结果:可抽奖商品总数和商品可抽奖的库存 : goodsTotal 和 goodsStock
        Map statistics = new HashMap();//homePageService.statisticsGoodsV2(merchantId, shopId);
        // 在redis中统计假数据
        Integer res = homePageService.buildOrGetData4Statistics(merchantId, shopId);
        String fakeIncr = "home:fake:joinMan:shopId:" + shopId;
        Long increment = stringRedisTemplate.opsForValue().increment(fakeIncr, 1L);
        statistics.put("goodsTotal", stockTotal);
        statistics.put("joinManTotal", res + increment);
        statistics.put("hitManTotal", (res / 3 + 43 + increment) / 3 + 35);

        // 返回数据组装
        respDataMap.put("statistics", statistics);
        respDataMap.put("shopInfo", shopInfoMap);
        respDataMap.put("listData", goodsMap);
        respDataMap.put("widget", widgetMap);
        respDataMap.put("isDialogOrder", false);

        //叶辰支持
        if (StringUtils.isNotBlank(scene)) {
            List<String> sceneIDs = Arrays.asList(scene.split(","));
            if (sceneIDs != null && sceneIDs.size() >= 3 && WeixinMpConstants.SCENE_PACKAGE == NumberUtils.toInt(sceneIDs.get(2))) {
                QueryWrapper<ActivityTask> atWrapper = new QueryWrapper<>();
                atWrapper.lambda()
                        .eq(ActivityTask::getType, ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode())
                        .eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
                        .eq(ActivityTask::getDel, DelEnum.NO.getCode());
                atWrapper.apply("rp_stock >= 1");
                List<ActivityTask> activityTasks = activityTaskService.list(atWrapper);
                if (!CollectionUtils.isEmpty(activityTasks)) {
                    respDataMap.put("isDialogOrder", true);
                }
            }
        }

        response = new SimpleResponse();
        response.setData(respDataMap);
        return response;
    }


    /**
     * 今日任务
     * <p>
     * url : /api/applet/v2/home/getMission
     */
    @GetMapping("getMission")
    public SimpleResponse popout(@RequestParam(name = "merchantId", required = true) final Long merchantId,
                                 @RequestParam(name = "shopId", required = true) final Long shopId,
                                 @RequestParam(name = "userId", required = true) final Long userId) {
        SimpleResponse response;

//        response = verifyService.idempotence(merchantId, shopId, userId);
//        if (response != null) {
//            return response;
//        }
        response = verifyService.userIdError(userId);
        if (response != null) {
            return response;
        }
        response = verifyService.merchantShopError(merchantId, shopId);
        if (response != null) {
            return response;
        }
        Map widgetMap = homePageService.getMissionTypeByYYSuport(shopId, userId, merchantId);
        response = new SimpleResponse();
        response.setData(widgetMap);
        return response;
    }
}
