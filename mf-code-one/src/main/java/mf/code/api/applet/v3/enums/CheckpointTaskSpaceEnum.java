package mf.code.api.applet.v3.enums;

/**
 * mf.code.api.applet.v3
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月18日 10:29
 */
public enum CheckpointTaskSpaceEnum {
    OPENREDPACK(1, "拆红包"),
    LUCKYWHEEL(2, "幸运大转盘"),
    FAVCART(3, "收藏加购"),
    GOODCOMMENT(4, "好评晒图"),
    FILLORDER(5, "回填订单"),
    ASSIST(6, "赠品"),
    ORDERBACK(7, "免单抽奖"),

    /***
     * 好评晒图-v2
     */
    GOOD_COMMENT_V2(8, "好评晒图"),
    FULLREIMBURSEMENT(9, "全额报销"),
    ;
    private int code;
    private String message;

    CheckpointTaskSpaceEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Integer findByCode(Integer code) {
        for (CheckpointTaskSpaceEnum typeEnum : CheckpointTaskSpaceEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getCode();
            }
        }
        return null;
    }


    public static String findByDesc(Integer code){
        for (CheckpointTaskSpaceEnum typeEnum : CheckpointTaskSpaceEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return "";
    }
}
