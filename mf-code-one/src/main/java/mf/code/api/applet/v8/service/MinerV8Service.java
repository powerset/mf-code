package mf.code.api.applet.v8.service;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.repo.po.UserPubJoin;

import java.util.List;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 10:26
 */
public interface MinerV8Service {
    /***
     * 闯关页面信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse queryCheckpointInfo(Long merchantId, Long shopId, Long userId);

    /**
     * 获取 新人任务（闯关任务） 弹窗
     *
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse queryNewcomerTaskPopup(Long shopId, Long userId);

    /***
     * 进度详情-收益明细
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse queryMinerProgressDetail(Long merchantId, Long shopId, Long userId, int offset, int size);

    /***
     * 我的矿工分页
     * @param merchantId
     * @param shopId
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse queryMinerDetail(Long merchantId, Long shopId, Long userId, int offset, int size);
}
