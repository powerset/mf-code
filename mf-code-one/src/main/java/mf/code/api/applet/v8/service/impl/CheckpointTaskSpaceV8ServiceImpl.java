package mf.code.api.applet.v8.service.impl;

import mf.code.api.applet.v8.dto.CheckpointTaskSpaceDTO;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceAboutService;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceV8Service;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 16:47
 */
@Service
public class CheckpointTaskSpaceV8ServiceImpl implements CheckpointTaskSpaceV8Service {
    @Autowired
    private UserService userService;
    @Autowired
    private CheckpointTaskSpaceAboutService checkpointTaskSpaceAboutService;

    @Override
    public SimpleResponse queryCheckpointTaskSpace(Long merchantId, Long shopId, Long userId) {
        //本人头像信息
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "获取用户信息异常，请重试呀~");
        }
        CheckpointTaskSpaceDTO checkpointTaskSpaceDTO = new CheckpointTaskSpaceDTO();
        checkpointTaskSpaceDTO.from(user);
        //获取新手任务详细
        checkpointTaskSpaceAboutService.queryNewbieTaskDetail(checkpointTaskSpaceDTO, checkpointTaskSpaceDTO.getTasks(), user);
        //获取店长任务详细
        checkpointTaskSpaceAboutService.queryShopManagerTaskDetail(checkpointTaskSpaceDTO, checkpointTaskSpaceDTO.getTasks(), user);
        //获取日常任务详细
        checkpointTaskSpaceAboutService.queryDailyTaskDetail(checkpointTaskSpaceDTO, checkpointTaskSpaceDTO.getTasks(), merchantId, shopId, userId);
        //获取已赚收益
        checkpointTaskSpaceAboutService.queryFinishProfit(checkpointTaskSpaceDTO, merchantId, shopId, user);
        //弹窗信息
        checkpointTaskSpaceAboutService.queryDialog(checkpointTaskSpaceDTO, merchantId, shopId, user);

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(checkpointTaskSpaceDTO);
        return simpleResponse;
    }
}
