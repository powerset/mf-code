package mf.code.api.applet.v3.dto;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import mf.code.api.AppletMybatisPageDto;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * mf.code.api.applet.v3.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月14日 10:49
 */
@Data
public class MinerDetailResp {
    //矿工昵称
    private String nickName;
    //矿工头像
    private String avatarUrl;
    //矿工成为时间
    @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
    private Date time;
    //累计缴税
    private String totalScottare = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //今日缴税
    private String todayScottare = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //矿工缴税详情
    private AppletMybatisPageDto<MinerScottareDetail> detail;

    @Data
    public static class MinerScottareDetail {
        //时间
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
        private Date time;
        //工作项目
        private String projectMessage;
        //矿工缴税
        private String money = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        public void from(UserCoupon userCoupon) {
            this.time = userCoupon.getCtime();
            this.projectMessage = UserCouponTypeEnum.findByDesc(userCoupon.getType());
            Object object = JSONObject.parse(userCoupon.getCommissionDef());
            JSONObject jsonObject = JSONObject.parseObject(object.toString());
            Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
            if (jsonMap != null) {
                String  commissionDef = jsonMap.get("commissionDef").toString();
                String  rate = jsonMap.get("rate").toString();
                BigDecimal bigDecimal = new BigDecimal(commissionDef).multiply(new BigDecimal(rate)).divide(new BigDecimal("100"));
                this.money = bigDecimal.setScale(2, BigDecimal.ROUND_DOWN).toString();
            }
        }
    }

    public void from(UserPubJoin userPubJoin) {
        //成为矿工时间
        this.setTime(userPubJoin.getCtime());
        //获取累计缴税
        this.setTotalScottare(userPubJoin.getTotalScottare().setScale(2, BigDecimal.ROUND_DOWN).toString());
    }

    public void from(User user) {
        this.nickName = user.getNickName();
        this.avatarUrl = user.getAvatarUrl();
    }
}
