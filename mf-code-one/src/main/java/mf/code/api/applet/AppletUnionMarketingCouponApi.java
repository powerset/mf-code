package mf.code.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.api.applet.dto.JKMFUserCouponDTO;
import mf.code.api.applet.dto.UserCouponQueryDTO;
import mf.code.api.applet.service.AppletUnionMarketingCouponService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.constant.UserCouponStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 小程序-联合营销-优惠券
 *
 * @author yunshan
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/coupon/unionMarketing")
public class AppletUnionMarketingCouponApi {
    @Autowired
    private AppletUnionMarketingCouponService appletUnionMarketingCouponService;

    /**
     * 我的卡券
     * 根据优惠券的使用状态查询优惠券信息，status 1，未使用，2已使用，3以过期
     * <p>
     * 根据类型查询已拥有的优惠券信息
     *
     * @param type   3，淘宝，29集客优惠券
     * @param status 0：未领奖，1：已领奖，2已使用 3已过期
     * @param userId
     * @param offset
     * @param limit
     * @return
     */
    @GetMapping(value = "/queryCouponsByTypeAndStatus")
    public SimpleResponse queryCouponsByTypeAndStatus(@RequestParam("type") Integer type,
                                                      @RequestParam("status") Integer status,
                                                      @RequestParam("userId") Long userId,
                                                      @RequestParam("shopId") Long shopId,
                                                      @RequestParam(value = "offset", defaultValue = "1") Integer offset,
                                                      @RequestParam(value = "limit", defaultValue = "10") Integer limit) {

        SimpleResponse simpleResponse = new SimpleResponse();
        if (type != UserCouponTypeEnum.COUPON.getCode()
                && type != UserCouponTypeEnum.JKMF_COUPON.getCode()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("查询优惠券类型错误");
            return simpleResponse;
        }
        if (UserCouponStatusEnum.findByCode(status) == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("查询优惠券状态错误");
            return simpleResponse;
        }
        UserCouponQueryDTO userCouponQueryDTO = new UserCouponQueryDTO();
        userCouponQueryDTO.setShopId(shopId);
        userCouponQueryDTO.setUserId(userId);
        userCouponQueryDTO.setType(type);
        userCouponQueryDTO.setStatus(status);
        userCouponQueryDTO.setLimit(limit);
        userCouponQueryDTO.setOffset(offset);

        return appletUnionMarketingCouponService.queryMyCoupon(userCouponQueryDTO);
    }


    /**
     * 查看优惠券
     * <p>
     * 可用优惠群和不可用优惠券都只展示优惠券未使用的状态，已使用和已过期则不使用
     * <p>
     * 不可用优惠券是指优惠券关联的产品id和入参的产品id不一致，且优惠券的状态是未使用
     * <p>
     * 根据优惠券使用状况查询优惠券信息列表
     *
     * @param status 1,可用优惠券，2不可用优惠券
     * @param userId
     * @param offset
     * @param limit
     * @return
     */
    @GetMapping(value = "/queryCouponByStatus")
    public SimpleResponse queryJKMFCouponByStatusForOrder(@RequestParam("status") Integer status,
                                                          @RequestParam("userId") Long userId,
                                                          @RequestParam("shopId") Long shopId,
                                                          @RequestParam("goodId") Long goodId,
                                                          @RequestParam("skuId") Long skuId,
                                                          @RequestParam(value = "offset", defaultValue = "1") Integer offset,
                                                          @RequestParam(value = "limit", defaultValue = "10") Integer limit) {

        SimpleResponse simpleResponse = new SimpleResponse();
        if (status != 1 && status != 2) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("查询优惠券状态错误");
            return simpleResponse;
        }
        UserCouponQueryDTO userCouponQueryDTO = new UserCouponQueryDTO();
        userCouponQueryDTO.setGoodId(goodId);
        userCouponQueryDTO.setSkuId(skuId);
        userCouponQueryDTO.setShopId(shopId);
        userCouponQueryDTO.setUserId(userId);
        userCouponQueryDTO.setType(UserCouponTypeEnum.JKMF_COUPON.getCode());
        userCouponQueryDTO.setStatus(status);
        userCouponQueryDTO.setLimit(limit);
        userCouponQueryDTO.setOffset(offset);

        return appletUnionMarketingCouponService.queryJKMFCouponByStatusForOrder(userCouponQueryDTO);
    }


    /**
     * 确认订单--获取优惠券价格
     * 查询可以使用的优惠券的金额
     *
     * @param shopId
     * @param userId
     * @param goodId
     * @param orderPrice
     * @return
     */
    @GetMapping(value = "/canApplyCoupon")
    public SimpleResponse queryCanApplyCoupon(@RequestParam("shopId") Long shopId,
                                              @RequestParam("userId") Long userId,
                                              @RequestParam("goodId") Long goodId,
                                              @RequestParam("skuId") Long skuId,
                                              @RequestParam("orderPrice") BigDecimal orderPrice) {

        SimpleResponse simpleResponse = new SimpleResponse();
        JKMFUserCouponDTO jkmfUserCouponDTO = new JKMFUserCouponDTO();
        jkmfUserCouponDTO.setAmount(BigDecimal.ZERO);
        jkmfUserCouponDTO.setCouponId(0L);
        if (orderPrice.compareTo(BigDecimal.ZERO) <= 0) {
            simpleResponse.setData(jkmfUserCouponDTO);
            return simpleResponse;
        }
        jkmfUserCouponDTO = appletUnionMarketingCouponService.queryCanApplyCoupon(shopId, userId, goodId, skuId, orderPrice);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, jkmfUserCouponDTO);

    }

}
