package mf.code.api.applet.service;

import com.alibaba.fastjson.JSONObject;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;

/**
 * mf.code.api.applet.service
 * Description:
 *
 * @author: gel
 * @date: 2019-01-03 20:20
 */
public interface AppletUserActivityV2Service {

    SimpleResponse backfillOrderV2(BackFillOrderReq req);

    BigDecimal calcCommission(JSONObject jsonObject, ActivityDef activityDef);

    SimpleResponse orderInfo(Long userId, Long taskId);

    SimpleResponse queryBackfillOrderResult(String orderId);
}
