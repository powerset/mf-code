package mf.code.api.applet.v7.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityCommissionService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.TaskV7Req;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v7.service.AppletUserTaskV7AboutService;
import mf.code.api.applet.v7.service.AppletUserTaskV7Service;
import mf.code.common.constant.ActivityDefStatusEnum;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * mf.code.api.applet.v7.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月22日 15:07
 */
@Service
@Slf4j
public class AppletUserTaskV7ServiceImpl implements AppletUserTaskV7Service {
    @Autowired
    private AppletUserTaskV7AboutService appletUserTaskV7AboutService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityCommissionService activityCommissionService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private UserActivityService userActivityService;

    @Override
    public SimpleResponse queryUserTask(Long merchantId, Long shopId, Long userId, Long taskId) {
        boolean check = merchantId != null && merchantId > 0 && shopId != null && shopId > 0 &&
                userId != null && userId > 0 && taskId != null && taskId > 0;
        if (!check) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        //获取店铺信息
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该店铺不存在");
        }
        //查询任务
        UserTask userTask = userTaskService.selectByPrimaryKey(taskId);
        if (userTask == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该任务不存在");
        }
        //当前佣金关卡数
        CommissionResp commissionResp = commissionService.commissionCheckpoint(userId, shopId, merchantId);
        //税率
        BigDecimal rate = activityCommissionService.getCommissionRate();
        //任务倒计时
        String costStr = redisService.getUserTaskSupplyAuditCost(userTask.getId());
        //查询商品信息
        Goods goods = null;
        if (userTask.getGoodsId() != null && userTask.getGoodsId() > 0) {
            goods = goodsService.selectById(userTask.getGoodsId());
        }

        if (Arrays.asList(
                UserTaskTypeEnum.ORDER_BACK_JOIN.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START.getCode(),
                UserTaskTypeEnum.ASSIST_START.getCode(),
                UserTaskTypeEnum.GOOD_COMMENT.getCode()).contains(userTask.getType())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "不匹配的任务类型");
        }

        //中奖任务and非中奖任务的区分处理
        if (Arrays.asList(
                UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START_V2.getCode(),
                UserTaskTypeEnum.ASSIST_START_V2.getCode()).contains(userTask.getType())) {
            return appletUserTaskV7AboutService.queryUserTaskByActivity(userTask, merchantShop, commissionResp, rate, costStr, goods);
        } else {
            return appletUserTaskV7AboutService.queryUserTaskByActivityDef(userTask, merchantShop, commissionResp, rate, costStr, goods);
        }
    }

    /***
     * 任务提交
     * @param taskV7Req
     * @return
     */
    @Override
    public SimpleResponse commitUserTaskAudit(TaskV7Req taskV7Req) {
        if (!taskV7Req.check()) {
            log.error("<<<<<<<<提交任务-入参缺失 req:{}", JSONObject.toJSONString(taskV7Req));
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请传入有效的入参");
        }
        if (StringUtils.isNotBlank(taskV7Req.getOrderId()) && taskV7Req.getOrderId().length() != 18) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请输入准确的订单号");
        }
        List<String> pics = JSONArray.parseArray(taskV7Req.getPics(), String.class);

        //判断是否是回填订单
        boolean fillOrderBack = StringUtils.isNotBlank(taskV7Req.getOrderId()) && StringUtils.isBlank(taskV7Req.getPics())
                && (taskV7Req.getTaskId() == null || taskV7Req.getTaskId() == 0);
        if (fillOrderBack) {
            SimpleResponse simpleResponse = appletUserTaskV7AboutService.commitFillorderBackTaskAudit(taskV7Req);
            return simpleResponse;
        }

        UserTask userTask = userTaskService.selectByPrimaryKey(taskV7Req.getTaskId());

        Long activityId = userTask.getActivityId();
        Long activiyDefId = userTask.getActivityDefId();

        boolean awardTypeUserTask = appletUserTaskV7AboutService.queryUserTaskTypeByAward(userTask);

        Activity activity = null;
        //助力，免单
        if (activityId != null && activityId > 0) {
            activity = activityService.findById(activityId);
        }

        ActivityDef activityDef = null;
        //好评，收藏
        if (activiyDefId != null && activiyDefId > 0) {
            activityDef = activityDefService.selectByPrimaryKey(activiyDefId);
        }
        SimpleResponse simpleResponse = appletUserTaskV7AboutService.checkValid(taskV7Req, activityDef, activity, pics);
        if (simpleResponse.error()) {
            return simpleResponse;
        }

        //中奖任务(免单+助力)
        if (awardTypeUserTask && activity != null) {
            simpleResponse = appletUserTaskV7AboutService.commitActiviyTaskAudit(taskV7Req, activity, pics);
        }
        //非中奖任务(收藏，好评)
        if (!awardTypeUserTask && activityDef != null) {
            simpleResponse = appletUserTaskV7AboutService.commitActiviyDefTaskAudit(taskV7Req, activityDef, pics);
        }

        return simpleResponse;
    }

    /***
     * 获取userTaskId
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activityId
     * @param activityDefId
     * @return
     */
    @Override
    public SimpleResponse getUserTaskId(Long merchantId, Long shopId, Long userId, Long activityId, Long activityDefId, Long goodsId, String orderId, Integer source) {
        boolean check = merchantId != null && merchantId > 0 && shopId != null && shopId > 0 && userId != null && userId > 0;
        if (!check) {
            log.error("获取用户任务的入参：merchantId:{}, shopId:{}, userId:{}, activityDefId:{},activityId:{}", merchantId, shopId, userId, activityDefId, activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        //助力，免单
        Activity activity = null;
        if (activityId > 0) {
            activity = activityService.findById(activityId);
        }

        ActivityDef activityDef = null;
        //好评，收藏
        if (activityDefId > 0) {
            activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        }

        //若是回填，则自己查询获取回填订单定义活动
        if (activityId == 0 && activityDefId == 0 && StringUtils.isNotBlank(orderId)) {
            QueryWrapper<ActivityDef> activityDefQueryWrapper = new QueryWrapper<>();
            activityDefQueryWrapper.lambda()
                    .eq(ActivityDef::getMerchantId, merchantId)
                    .eq(ActivityDef::getShopId, shopId)
                    .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                    .in(ActivityDef::getType, Arrays.asList(
                            ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(),
                            ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()
                    ))
                    .eq(ActivityDef::getDel, DelEnum.NO.getCode())
            ;
            activityDefQueryWrapper.orderByDesc("id");
            List<ActivityDef> activityDefs = activityDefService.list(activityDefQueryWrapper);
            if (CollectionUtils.isEmpty(activityDefs)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "啊哦，商家订单回填返现活动正在上线哦~~");
            }
            activityDef = activityDefs.get(0);
        }

        //中奖任务(免单+助力)
        if (activity != null) {
            return appletUserTaskV7AboutService.getUserTaskIdByActivity(merchantId, shopId, userId, activity);
        }

        //非中奖任务(收藏，好评，回填)
        if (activityDef != null) {
            return appletUserTaskV7AboutService.getUserTaskIdByActivityDef(merchantId, shopId, userId, activityDef, goodsId, orderId, source);
        }
        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无活动");
    }

    /***
     * 任务的重新提交
     * @param merchantId
     * @param shopId
     * @param userId
     * @param taskId
     * @param retryCommitType 重新提交类型 1：失败重新提交 2：超时重新提交(此场景需要重置，startTime)
     * @return
     */
    @Override
    public SimpleResponse retryCommitUserTask(Long merchantId, Long shopId, Long userId, Long taskId, int retryCommitType) {
        boolean check = merchantId != null && merchantId > 0 && shopId != null && shopId > 0 &&
                userId != null && userId > 0 && taskId != null && taskId > 0;
        if (!check) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参异常");
        }
        UserTask userTask = userTaskService.selectByPrimaryKey(taskId);
        if (userTask == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该任务已不存在");
        }

        //中奖类(免单，助力)
        if (appletUserTaskV7AboutService.queryUserTaskTypeByAward(userTask)) {
            SimpleResponse simpleResponse = appletUserTaskV7AboutService.compareUserTask(userTask);
            if (simpleResponse.error()) {
                return simpleResponse;
            }
        } else {
            Integer status = userTask.getStatus();
            if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "审核中，请勿再次提交");
            }
            if (status == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO14.getCode(), "任务已完成，请勿再次提交");
            }

            if (status == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                //非中奖类，查询此刻是否还有库存(提前判断，不做扣除处理)
                if (userTask.getActivityDefId() == null || userTask.getActivityDefId() == 0) {
                    log.error("<<<<<<<< 非中奖类 任务 没有def信息，userTask:{}", userTask);
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "活动异常，请稍后重试");
                }
                ActivityDef activityDef = activityDefService.selectByPrimaryKey(userTask.getActivityDefId());
                if (activityDef == null) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "活动下线啦！");
                }
                if (activityDef.getDeposit().compareTo(BigDecimal.ZERO) <= 0 || (activityDef.getStock() != null && activityDef.getStock() <= 0)) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "库存不足啦！");
                }
            }
        }

        Date now = new Date();

        userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
        userTask.setUtime(now);
        userTask.setAuditingReason(null);
        if (retryCommitType == 2) {
            ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(userTask.getActivityDefId());
            if (null == activityDef) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该活动已下线！");
            } else {
                if (Arrays.asList(ActivityDefStatusEnum.REFUNDED.getCode(), ActivityDefStatusEnum.CLOSE.getCode(),
                        ActivityDefStatusEnum.END.getCode()).contains(activityDef.getStatus())) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该活动已下线！");
                }
                if (activityDef.getStock() != null && activityDef.getStock() <= 0) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "库存不足了");
                }
            }
            //若是超时，则重置开始时间
            userTask.setStartTime(now);
            if (userTask.getUserActivityId() != null && userTask.getUserActivityId() > 0) {
                UserActivity userActivity = userActivityService.selectById(userTask.getUserActivityId());
                if (userActivity != null) {
                    userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE_ING.getCode());
                    userActivity.setUtime(now);
                    this.userActivityService.updateById(userActivity);
                }
            }
        }
        boolean b = userTaskService.updateById(userTask);
        log.info("<<<<<<<< 重新提交任务 status：{}", b);
        if (!b) {
            log.error("<<<<<<<< userTask重新提交更新异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "网络异常，请稍后重试");
        }
        return new SimpleResponse();
    }
}
