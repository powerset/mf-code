package mf.code.api.applet.v8.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.v8.service.UserMoneyAboutDTO;
import mf.code.api.applet.v8.service.UserMoneyAboutService;
import mf.code.api.applet.v8.service.UserMoneyV8Service;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.OrderService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import mf.code.user.vo.UpayWxOrderAboutVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:30
 */
@Service
@Slf4j
public class UserMoneyV8ServiceImpl implements UserMoneyV8Service {
    @Autowired
    private UserMoneyAboutService userMoneyAboutService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

    /***
     * 查询我的提现记录
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryPresentOrderLog(Long merchantId, Long shopID, Long userID, int offset, int size) {
        if (merchantId == null || merchantId == 0 || shopID == null || shopID == 0 || userID == null || userID == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> respMap = userMoneyAboutService.queryPresentOrder(merchantId, shopID, userID, offset, size);
        simpleResponse.setData(respMap);
        return simpleResponse;
    }

    /***
     * 查询收入记录
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @param type 1:自购省 2：分享赚 3：任务奖金 4：任务佣金
     * @return
     */
    @Override
    public SimpleResponse queryIncomeLog(Long merchantId, Long shopID, Long userID, int offset, int size,
                                         Integer type, Integer section) {
        if (merchantId == null || merchantId == 0 || shopID == null || shopID == 0 || userID == null || userID == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        List<Integer> bizTypes = new ArrayList<>();
        List<Integer> payOrderType = null;
        if (type == 1) {
            bizTypes = UpayWxOrderAboutVO.addSelfPurchaseRebate();
            payOrderType = Arrays.asList(OrderTypeEnum.PALTFORMRECHARGE.getCode());
        } else if (type == 2) {
            bizTypes = UpayWxOrderAboutVO.addShareCommission();
            payOrderType = Arrays.asList(OrderTypeEnum.PALTFORMRECHARGE.getCode());
        } else if (type == 3) {
            bizTypes = UpayWxOrderAboutVO.addTaskAward();
            payOrderType = Arrays.asList(OrderTypeEnum.PALTFORMRECHARGE.getCode());
        } else if (type == 4) {
            bizTypes = UpayWxOrderAboutVO.addTaskCommission();
            payOrderType = Arrays.asList(OrderTypeEnum.REBATE.getCode());
        } else {
            payOrderType = Arrays.asList(OrderTypeEnum.PALTFORMRECHARGE.getCode(),OrderTypeEnum.REBATE.getCode());
        }
        //查询条件
        Map<String, Object> wxOrderParams = UserMoneyAboutDTO.selectUpayOrderIncomeParams(merchantId, shopID, userID, bizTypes, payOrderType, section, offset, size);
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(wxOrderParams);
        if (CollectionUtils.isEmpty(upayWxOrders)) {
            Map<String, Object> respMap = new HashMap<String, Object>();
            respMap.put("offset", offset);
            respMap.put("limit", size);
            respMap.put("total", 0);
            respMap.put("content", new ArrayList<>());
            respMap.put("sumMoney", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
            return new SimpleResponse(ApiStatusEnum.SUCCESS, respMap);
        }
        BigDecimal sumWxOrderTotalFee = this.upayWxOrderService.sumUpayWxOrderTotalFee(wxOrderParams);
        int countWxOrder = this.upayWxOrderService.countUpayWxOrder(wxOrderParams);

        List<Long> activityIds = new ArrayList<Long>();
        List<Long> productOrderIds = new ArrayList<Long>();
        List<Long> userIds = new ArrayList<>();
        List<Long> upayWxOrderIds = new ArrayList<>();
        for (UpayWxOrder wxOrder : upayWxOrders) {
            if (wxOrder.getBizValue() != null && wxOrder.getBizValue() > 0
                    && UserMoneyAboutDTO.isActivity(wxOrder)
                    && activityIds.indexOf(wxOrder.getBizValue()) == -1) {
                activityIds.add(wxOrder.getBizValue());
            }
            //商品编号
            if (BizTypeEnum.REBATE.getCode() == wxOrder.getBizType()
                    && wxOrder.getBizValue() != null && wxOrder.getBizValue() > 0
                    && productOrderIds.indexOf(wxOrder.getBizValue()) == -1) {
                productOrderIds.add(wxOrder.getBizValue());
            }
            //用户编号
            boolean isUserIdType = BizTypeEnum.CONTRIBUTION.getCode() == wxOrder.getBizType()
                    || BizTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode() == wxOrder.getBizType()
                    || OrderTypeEnum.REBATE.getCode() == wxOrder.getType();
            if (isUserIdType
                    && wxOrder.getBizValue() != null && wxOrder.getBizValue() > 0
                    && userIds.indexOf(wxOrder.getBizValue()) == -1) {
                userIds.add(wxOrder.getBizValue());
            }
        }
        Map<Long, Activity> activityMap = new HashMap<>();
        Map<Long, Goods> goodsMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(activityIds)) {
            Map<String, Object> activityParams = new HashMap<String, Object>();
            activityParams.put("merchantId", merchantId);
            activityParams.put("shopId", shopID);
            activityParams.put("activityIds", activityIds);
            List<Activity> activities = activityService.findPage(activityParams);
            List<Long> goodsIDs = new ArrayList<Long>();
            if (!CollectionUtils.isEmpty(activities)) {
                for (Activity activity : activities) {
                    activityMap.put(activity.getId(), activity);
                    if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                        goodsIDs.add(activity.getGoodsId());
                    }
                }
            }
            goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        }

        CommonDict commonDict = commonDictService.selectByTypeKey("checkpoint", "level");

        SimpleResponse simpleResponse = new SimpleResponse();
        List<Object> list = new ArrayList<Object>();

        Map<Long, Map> productGoodsShoppingMap = orderService.getUpayOrderProductInfo(productOrderIds);
        Map<Long, User> userMap = userService.queryByUserIdsToMap(userIds);
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            Map<String, Object> map = userMoneyAboutService.covertUserMoneyTradeDetail(upayWxOrder, null, activityMap, goodsMap, commonDict);
            if (!CollectionUtils.isEmpty(map)) {
                list.add(map);
            }
            Map<String, Object> map1 = userMoneyAboutService.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, 1, productGoodsShoppingMap);
            if (!CollectionUtils.isEmpty(map1)) {
                //购买商品交易明细
                list.add(map1);
            }
            Map<String, Object> map2 = userMoneyAboutService.covertUserMoneyTradeDetailForContribution(upayWxOrder, 1, userMap);
            if (!CollectionUtils.isEmpty(map2)) {
                //购买商品团员贡献交易明细
                list.add(map2);
            }

            Map<String, Object> map3 = userMoneyAboutService.covertTaskTradeDetail(upayWxOrder, null, userMap);
            if (!CollectionUtils.isEmpty(map3)) {
                //店长、新手任务
                list.add(map3);
            }
        }
        Map<String, Object> respMap = UserMoneyAboutDTO.returnInfoObject(offset, size, countWxOrder, list, sumWxOrderTotalFee);
        simpleResponse.setData(respMap);
        return simpleResponse;
    }

    /***
     * 查询交易明细
     * @param merchantID
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @return
     */
    @Override
    public SimpleResponse queryTradeDetail(Long merchantID, Long shopID, Long userID, int offset, int size, Long pageLastID) {
        if (merchantID == null || merchantID == 0 || shopID == null || shopID == 0 || userID == null || userID == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        Map<String, Object> upayWxOrderParams = UserMoneyAboutDTO.selectUpayOrderParams(merchantID, shopID, userID, offset, size, null);
        upayWxOrderParams.put("order", "id");
        upayWxOrderParams.put("direct", "desc");
        upayWxOrderParams.put("pageLastId", pageLastID);
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
        if (upayWxOrders == null || upayWxOrders.size() == 0) {
            Map<String, Object> respMap = UserMoneyAboutDTO.returnInfoObjectNew(offset, size, 0, new ArrayList<>(), null);
            return new SimpleResponse(ApiStatusEnum.SUCCESS, respMap);
        }
        List<Long> activityIDs = new ArrayList<Long>();
        List<Long> productOrderIds = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        List<Long> upayWxOrderIds = new ArrayList<>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            if (OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType()
                    && UserMoneyAboutDTO.isActivity(upayWxOrder)
                    && upayWxOrder.getBizValue() > 0
                    && activityIDs.indexOf(upayWxOrder.getBizValue()) == -1) {
                activityIDs.add(upayWxOrder.getBizValue());
            }
            //商品编号
            if (BizTypeEnum.SHOPPING.getCode() == upayWxOrder.getBizType()
                    && upayWxOrder.getBizValue() != null && upayWxOrder.getBizValue() > 0
                    && productOrderIds.indexOf(upayWxOrder.getBizValue()) == -1) {
                productOrderIds.add(upayWxOrder.getBizValue());
            }
            //用户编号
            if ((BizTypeEnum.CONTRIBUTION.getCode() == upayWxOrder.getBizType() ||
                    BizTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode() == upayWxOrder.getBizType())
                    && upayWxOrder.getBizValue() != null && upayWxOrder.getBizValue() > 0
                    && userIds.indexOf(upayWxOrder.getBizValue()) == -1) {
                userIds.add(upayWxOrder.getBizValue());
            }
            boolean needSeleUserIdType = BizTypeEnum.SHOP_MANAGER_PAY.getCode() == upayWxOrder.getBizType();
            //先查出升级店长的订单编号，从而获取哪个用户提供的
            if (needSeleUserIdType && upayWxOrderIds.indexOf(upayWxOrder.getBizValue()) == -1) {
                upayWxOrderIds.add(upayWxOrder.getBizValue());
            }
        }
        if (!CollectionUtils.isEmpty(upayWxOrderIds)) {
            List<UpayWxOrder> needUpayOrders = upayWxOrderService.list(new QueryWrapper<UpayWxOrder>()
                    .lambda().in(UpayWxOrder::getId, upayWxOrderIds));
            if (!CollectionUtils.isEmpty(needUpayOrders)) {
                for (UpayWxOrder upayWxOrder : needUpayOrders) {
                    if(userIds.indexOf(upayWxOrder.getUserId()) == -1){
                        userIds.add(upayWxOrder.getUserId());
                    }
                }
            }
        }
        Map<Long, Goods> goodsMap = new HashMap<>();
        Map<Long, Activity> activityMap = new HashMap<Long, Activity>();
        if (!CollectionUtils.isEmpty(activityIDs)) {
            Map<String, Object> activityParams = new HashMap<String, Object>();
            activityParams.put("merchantId", merchantID);
            activityParams.put("shopId", shopID);
            activityParams.put("activityIds", activityIDs);
            List<Activity> activities = this.activityService.findPage(activityParams);
            List<Long> goodsIDs = new ArrayList<Long>();
            if (activities != null && activities.size() > 0) {
                for (Activity activity : activities) {
                    activityMap.put(activity.getId(), activity);
                    if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                        goodsIDs.add(activity.getGoodsId());
                    }
                }
            }
            goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        }
        Map<Long, Map> productGoodsShoppingMap = orderService.getUpayOrderProductInfo(productOrderIds);
        Map<Long, User> userMap = userService.queryByUserIdsToMap(userIds);
        List<Object> list = new ArrayList<Object>();
        int listIndex = 0;//条数
        CommonDict commonDict = this.commonDictService.selectByTypeKey("checkpoint", "level");
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            //活动中奖信息信息
            Map<String, Object> map2 = userMoneyAboutService.covertUserMoneyTradeDetail(upayWxOrder, null, activityMap, goodsMap, commonDict);
            if (!CollectionUtils.isEmpty(map2)) {
                list.add(map2);
            }
            //用户支付-不是购物biztype
            if (OrderTypeEnum.USERPAY.getCode() == upayWxOrder.getType() && OrderPayTypeEnum.WEIXIN.getCode() == upayWxOrder.getPayType()) {
                Map<String, Object> map = userMoneyAboutService.covertUserMoneyTradeDetail(upayWxOrder, 1, null, null, null);
                if (!CollectionUtils.isEmpty(map)) {
                    list.add(map);
                }
                Map<String, Object> map1 = userMoneyAboutService.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, 1, productGoodsShoppingMap);
                if (!CollectionUtils.isEmpty(map1)) {
                    //购买商品交易明细
                    list.add(map1);
                }
                Map<String, Object> map3 = userMoneyAboutService.covertUserMoneyTradeDetailForContribution(upayWxOrder, 1, userMap);
                if (!CollectionUtils.isEmpty(map3)) {
                    //购买商品团员贡献交易明细
                    list.add(map3);
                }
            }
            Map<String, Object> map4 = userMoneyAboutService.covertTaskTradeDetail(upayWxOrder, null, userMap);
            if (!CollectionUtils.isEmpty(map4)) {
                //店长、新手任务
                list.add(map4);
            }
            //用户退款
            boolean wxPlatformInvok = OrderTypeEnum.PALTFORMREFUND.getCode() == upayWxOrder.getType() ||
                    OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType();
            if (wxPlatformInvok && OrderPayTypeEnum.WEIXIN.getCode() == upayWxOrder.getPayType()) {
                Map<String, Object> map = userMoneyAboutService.covertUserMoneyTradeDetail(upayWxOrder, 2, null, null, null);
                if (!CollectionUtils.isEmpty(map)) {
                    list.add(map);
                }
                //购买商品交易明细
                Map<String, Object> map1 = userMoneyAboutService.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, 2, productGoodsShoppingMap);
                if (!CollectionUtils.isEmpty(map1)) {
                    //退款商品交易明细
                    list.add(map1);
                }
            }
            listIndex++;
        }
        //查询数目小于本要展现的数目
        int countUpayWxOrder = 0;
        if (listIndex >= size) {
            countUpayWxOrder = 1;
        }
        Map<String, Object> respMap = UserMoneyAboutDTO.returnInfoObjectNew(offset, size, countUpayWxOrder, list, null);
        return new SimpleResponse(ApiStatusEnum.SUCCESS, respMap);
    }
}
