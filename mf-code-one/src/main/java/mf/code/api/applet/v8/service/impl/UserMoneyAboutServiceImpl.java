package mf.code.api.applet.v8.service.impl;

import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailConverter;
import mf.code.api.applet.v8.service.UserMoneyAboutDTO;
import mf.code.api.applet.v8.service.UserMoneyAboutService;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:38
 */
@Service
public class UserMoneyAboutServiceImpl implements UserMoneyAboutService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private List<UserMoneyTradeDetailConverter> userMoneyTradeDetailConverters;

    /***
     * 查询提现记录
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> queryPresentOrder(Long merchantID, Long shopID, Long userID, int offset, int size) {
        //查询订单
        Map<String, Object> upayWxOrderParams = UserMoneyAboutDTO.selectUpayOrderParams(merchantID, shopID, userID, offset, size, null);
        //用户提现
        upayWxOrderParams.put("type", OrderTypeEnum.USERCASH.getCode());
        //用户提现
        upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        List<UpayWxOrder> upayWxOrders = this.upayWxOrderService.query(upayWxOrderParams);
        //为空时的非正常处理展现
        if (CollectionUtils.isEmpty(upayWxOrders)) {
            Map<String, Object> respMap = new HashMap<String, Object>();
            respMap.put("offset", offset);
            respMap.put("limit", size);
            respMap.put("total", 0);
            respMap.put("content", new ArrayList<>());
            return respMap;
        }
        int countupayWxOrder = this.upayWxOrderService.countUpayWxOrder(upayWxOrderParams);
        return UserMoneyAboutDTO.returnInfoObject(offset, size, countupayWxOrder, UserMoneyAboutDTO.addContent(upayWxOrders), null);
    }

    /***
     * 映射用户记录推送converter
     * @param upayWxOrder
     * @param payType
     * @param activityMap
     * @param goodsMap
     */
    @Override
    public Map<String, Object> covertUserMoneyTradeDetail(UpayWxOrder upayWxOrder, Integer payType, Map<Long, Activity> activityMap,
                                                          Map<Long, Goods> goodsMap, CommonDict commonDict) {
        List<UserMoneyTradeDetailConverter> userMoneyTradeDetailConverters = this.userMoneyTradeDetailConverters;
        if (!CollectionUtils.isEmpty(userMoneyTradeDetailConverters)) {
            for (UserMoneyTradeDetailConverter converter : userMoneyTradeDetailConverters) {
                Map<String, Object> userMoneyTradeDetail = converter.getUserMoneyTradeDetail(upayWxOrder, payType, activityMap, goodsMap, commonDict);
                if (!CollectionUtils.isEmpty(userMoneyTradeDetail)) {
                    return userMoneyTradeDetail;
                }
            }
        }
        return null;
    }

    @Override
    public Map<String, Object> covertUserMoneyTradeDetailForProductGoodsShopping(UpayWxOrder upayWxOrder, int payType,
                                                                                 Map<Long, Map> productGoodsShoppingMap) {
        List<UserMoneyTradeDetailConverter> userMoneyTradeDetailConverters = this.userMoneyTradeDetailConverters;
        if (!CollectionUtils.isEmpty(userMoneyTradeDetailConverters)) {
            for (UserMoneyTradeDetailConverter converter : userMoneyTradeDetailConverters) {
                Map<String, Object> userMoneyTradeDetail = converter.covertUserMoneyTradeDetailForProductGoodsShopping(upayWxOrder, payType, productGoodsShoppingMap);
                if (!CollectionUtils.isEmpty(userMoneyTradeDetail)) {
                    return userMoneyTradeDetail;
                }
            }
        }
        return null;
    }

    /***
     * 映射用户购买商品团员贡献记录converter
     * @param upayWxOrder
     * @param userMap
     */
    @Override
    public Map<String, Object> covertUserMoneyTradeDetailForContribution(UpayWxOrder upayWxOrder, int payType, Map<Long, User> userMap) {
        List<UserMoneyTradeDetailConverter> userMoneyTradeDetailConverters = this.userMoneyTradeDetailConverters;
        if (!CollectionUtils.isEmpty(userMoneyTradeDetailConverters)) {
            for (UserMoneyTradeDetailConverter converter : userMoneyTradeDetailConverters) {
                Map<String, Object> userMoneyTradeDetail = converter.covertUserMoneyTradeDetailForContribution(upayWxOrder, payType, userMap);
                if (!CollectionUtils.isEmpty(userMoneyTradeDetail)) {
                    return userMoneyTradeDetail;
                }
            }
        }
        return null;
    }

    @Override
    public Map<String, Object> covertTaskTradeDetail(UpayWxOrder upayWxOrder, Integer payType, Map<Long, User> userMap) {
        List<UserMoneyTradeDetailConverter> userMoneyTradeDetailConverters = this.userMoneyTradeDetailConverters;
        if (!CollectionUtils.isEmpty(userMoneyTradeDetailConverters)) {
            for (UserMoneyTradeDetailConverter converter : userMoneyTradeDetailConverters) {
                Map<String, Object> userMoneyTradeDetail = converter.getTaskTradeDetail(upayWxOrder, payType, userMap);
                if (!CollectionUtils.isEmpty(userMoneyTradeDetail)) {
                    return userMoneyTradeDetail;
                }
            }
        }
        return null;
    }
}
