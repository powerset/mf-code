package mf.code.api.applet.v10.service;

import mf.code.activity.domain.applet.application.AppletLotteryActivityServiceAbstract;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v10.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-15 11:57
 */
public interface AppletPlatformOrderBackService extends AppletLotteryActivityServiceAbstract {

    /**
     * 预热活动列表展示页
     *
     * @param currentPage
     * @param pageSize
     * @return
     */
    AppletMybatisPageDto preActivityListPage(Integer activityType, Long currentPage, Long pageSize);

    /**
     * 联合营销页数据
     *
     * @param offset
     * @param size
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse sellUnion(Long offset, Long size, Long shopId, Long userId);

    /**
     * 查询联合营销页秒杀数据
     *
     * @param userId
     * @param shopId
     * @param batchIdStr
     * @param dayIdStr
     * @return
     */
    SimpleResponse unionPageSeckill(Long userId, Long shopId, String batchIdStr, String dayIdStr);
}
