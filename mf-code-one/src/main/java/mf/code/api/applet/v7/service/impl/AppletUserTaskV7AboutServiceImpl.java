package mf.code.api.applet.v7.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.dto.TaskResp;
import mf.code.api.applet.dto.TaskV7Req;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v7.service.AppletUserTaskV7AboutService;
import mf.code.api.feignservice.OneAppServiceImpl;
import mf.code.common.constant.*;
import mf.code.common.exception.CommissionReduceException;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.Assert;
import mf.code.common.utils.RobotUserUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.repo.po.GoodsPlus;
import mf.code.goods.service.GoodsPlusService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.xmlbeans.impl.xb.xmlconfig.Usertypeconfig;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v7.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月22日 16:42
 */
@Service
@Slf4j
public class AppletUserTaskV7AboutServiceImpl implements AppletUserTaskV7AboutService {
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private GoodsPlusService goodsPlusService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private OneAppServiceImpl oneAppService;

    /***
     * 任务提交验证入参
     * @param activityDef
     * @param activity
     * @param pics
     * @return
     */
    @Override
    public SimpleResponse checkValid(TaskV7Req taskV7Req, ActivityDef activityDef, Activity activity, List<String> pics) {
        if (activity != null) {
            Integer activityStatus = activity.getStatus();
            if (activityStatus == ActivityStatusEnum.FAILURE.getCode()
                    || activityStatus == ActivityStatusEnum.DEL.getCode()
                    || activityStatus == ActivityStatusEnum.END.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "活动已结束，请参加其他活动");
            }
            if (activity.getDel() == DelEnum.YES.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该活动已下架");
            }
            if (CollectionUtils.isEmpty(pics) || pics.size() < 3) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请上传完截图提交");
            }
        }
        if (activityDef != null) {
            if (activityDef.getDel() == DelEnum.YES.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该活动已下架");
            }
            if (activityDef.getStatus() != ActivityDefStatusEnum.PUBLISHED.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该活动已下线");
            }
            //回填订单
            if (Arrays.asList(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(),
                    ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()).contains(activityDef.getType())) {
                if (StringUtils.isBlank(taskV7Req.getOrderId())) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "请填写准确的订单号");
                }
            }
            //收藏加购
            if (Arrays.asList(
                    ActivityDefTypeEnum.FAV_CART.getCode(),
                    ActivityDefTypeEnum.FAV_CART_V2.getCode()).contains(activityDef.getType())) {
                //具体两张图,一张空图
                if (CollectionUtils.isEmpty(pics) || pics.size() < 3) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请上传完截图提交");
                }
            }
            //好评晒图
            if (Arrays.asList(
                    ActivityDefTypeEnum.GOOD_COMMENT.getCode(),
                    ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()).contains(activityDef.getType())) {
                //具体一张图,两张空图
                if (CollectionUtils.isEmpty(pics) || pics.size() < 3) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "请上传完截图提交");
                }
                if (StringUtils.isBlank(taskV7Req.getOrderId())) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "请填写准确的订单号");
                }
            }
        }
        return new SimpleResponse();
    }

    /***
     * 活动类 任务提交(免单，助力)
     * @param taskV7Req
     * @param activity
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse commitActiviyTaskAudit(TaskV7Req taskV7Req, Activity activity, List<String> pics) {
        String redisKey = RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + activity.getId() + ":" + taskV7Req.getUserId();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, redisKey);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10);
        }
        try {
            // 判断中奖任务是否有资格提交审核
            List<UserTask> userTasks = userTaskService.selectByUidAndAid(taskV7Req.getUserId(), activity.getId());
            if (CollectionUtils.isEmpty(userTasks)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "中奖任务不存在");
            }
            for (UserTask userTask : userTasks) {
                SimpleResponse simpleResponse = compareUserTask(userTask);
                if (simpleResponse.error()) {
                    return simpleResponse;
                }
            }
            // 信息加工入库
            UserTask userTask = userTasks.get(0);
            userTask.setPics(JSONObject.toJSONString(pics));
            userTask.setStatus(UserTaskStatusEnum.AUDIT_WAIT.getCode());
            userTask.setActivityDefId(activity.getActivityDefId());
            if (StringUtils.isNotBlank(taskV7Req.getOrderId())) {
                userTask.setOrderId(taskV7Req.getOrderId());
            }
            userTask.setReceiveTime(new Date());
            userTask.setUtime(new Date());
            boolean rows = this.userTaskService.updateById(userTask);
            if (!rows) {
                log.error("<<<<<<<<数据库更新失败：没有此用户任务记录");
            }
            Assert.isTrue(rows, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "更新异常~~请重试");
        } finally {
            stringRedisTemplate.delete(redisKey);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核");
    }

    /***
     * 回填订单提交审核
     * @param taskV7Req
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse commitFillorderBackTaskAudit(TaskV7Req taskV7Req) {
        // 如果同一订单已经被回填过就不能再次领取红包
        QueryWrapper<UserTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserTask::getMerchantId, taskV7Req.getMerchantId())
                .eq(UserTask::getShopId, taskV7Req.getShopId())
                .eq(UserTask::getUserId, taskV7Req.getUserId())
                .in(UserTask::getType, Arrays.asList(
                        UserTaskTypeEnum.ORDER_REDPACK.getCode(),
                        UserTaskTypeEnum.ORDER_REDPACK_V2.getCode()))
                .eq(UserTask::getOrderId, taskV7Req.getOrderId())
        ;
        List<UserTask> userTasks = userTaskService.list(wrapper);
        if (!CollectionUtils.isEmpty(userTasks)) {
            for (UserTask userTask : userTasks) {
                if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该订单已完成回填确认，请填写其他订单");
                }
                if (userTask.getStatus() == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该订单审核中，请勿重复");
                }
            }
        }

        QueryWrapper<ActivityDef> activityDefQueryWrapper = new QueryWrapper<>();
        activityDefQueryWrapper.lambda()
                .eq(ActivityDef::getMerchantId, taskV7Req.getMerchantId())
                .eq(ActivityDef::getShopId, taskV7Req.getShopId())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .in(ActivityDef::getType, Arrays.asList(
                        ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(),
                        ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()
                ))
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
        ;
        activityDefQueryWrapper.orderByDesc("id");
        List<ActivityDef> activityDefs = activityDefService.list(activityDefQueryWrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "啊哦，商家订单回填返现活动正在上线哦~~");
        }
        ActivityDef activityDef = activityDefs.get(0);
        String redisKey = RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + activityDef.getId() + ":" + taskV7Req.getUserId();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, redisKey);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10);
        }

        if (stringRedisTemplate.hasKey(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskV7Req.getUserId())) {
            String activityDefId = stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskV7Req.getUserId());
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("popStatus", 3);
            resultVO.put("activityDefId", Long.valueOf(activityDefId));
            stringRedisTemplate.delete(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskV7Req.getUserId());
            return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核", resultVO);
        }

        //回填是创建过程
        SimpleResponse userTaskByFillOrder = createUserTaskByFillOrder(taskV7Req.getMerchantId(), taskV7Req.getShopId(), taskV7Req.getUserId(), activityDef, taskV7Req);
        if (userTaskByFillOrder.error()) {
            return userTaskByFillOrder;
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核");
    }

    /***
     * 定义类任务提交(收藏加购，好评晒图)
     * @param taskV7Req
     * @param activityDef
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse commitActiviyDefTaskAudit(TaskV7Req taskV7Req, ActivityDef activityDef, List<String> pics) {
        String redisKey = RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + activityDef.getId() + ":" + taskV7Req.getUserId();
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, redisKey);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10);
        }

        if (stringRedisTemplate.hasKey(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskV7Req.getUserId())) {
            String activityDefId = stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskV7Req.getUserId());
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("popStatus", 3);
            resultVO.put("activityDefId", Long.valueOf(activityDefId));
            stringRedisTemplate.delete(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskV7Req.getUserId());
            return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核", resultVO);
        }

        try {
            //非回填是更新过程
            QueryWrapper<UserTask> userTaskQueryWrapper = new QueryWrapper<>();
            userTaskQueryWrapper.lambda()
                    .eq(UserTask::getActivityDefId, activityDef.getId())
                    .eq(UserTask::getUserId, taskV7Req.getUserId())
                    .eq(UserTask::getId, taskV7Req.getTaskId())
            ;
            userTaskQueryWrapper.orderByDesc("id");
            List<UserTask> userTasks = userTaskService.list(userTaskQueryWrapper);
            if (CollectionUtils.isEmpty(userTasks)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "中奖任务不存在");
            }

            for (UserTask userTask : userTasks) {
                SimpleResponse simpleResponse = compareUserTask(userTask);
                if (simpleResponse.error()) {
                    return simpleResponse;
                }
            }

            // 信息加工入库
            UserTask userTask = userTasks.get(0);

            //在规定时间范围内，非第一次提交,不扣库存
            String costStr = redisService.getUserTaskSupplyAuditCost(userTask.getId());
            if (StringUtils.isBlank(costStr)) {
                //扣库存
                if (userTask.getActivityDefId() != null && userTask.getActivityDefId() > 0) {
                    if (activityDefService.reduceDeposit(userTask.getActivityDefId(), BigDecimal.ZERO, 1) <= 0) {
                        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "库存不足啦！");
                    }
                }
            }

            userTask.setPics(JSONObject.toJSONString(pics));
            userTask.setStatus(UserTaskStatusEnum.AUDIT_WAIT.getCode());
            userTask.setActivityDefId(activityDef.getId());
            userTask.setAuditingReason("");
            if (StringUtils.isNotBlank(taskV7Req.getOrderId())) {
                userTask.setOrderId(taskV7Req.getOrderId());
            }
            userTask.setReceiveTime(new Date());
            userTask.setUtime(new Date());
            boolean rows = this.userTaskService.updateById(userTask);
            if (!rows) {
                return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "数据库更新失败：没有此用户任务记录");
            }

            //更新userActivity
            if (userTask.getUserActivityId() != null && userTask.getUserActivityId() > 0) {
                UserActivity userActivity = new UserActivity();
                userActivity.setId(userTask.getUserActivityId());
                userActivity.setUtime(new Date());
                this.userActivityService.updateByPrimaryKey(userActivity);
            }
        } finally {
            stringRedisTemplate.delete(redisKey);
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核");
    }

    /***
     * 获取中奖类活动taskId(免单+助力)
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activity
     * @return
     */
    @Override
    public SimpleResponse getUserTaskIdByActivity(Long merchantId, Long shopId, Long userId, Activity activity) {
        List<UserTask> userTasks = userTaskService.selectByUidAndAid(userId, activity.getId());
        if (CollectionUtils.isEmpty(userTasks)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "中奖任务活动不存在");
        }
        UserTask userTask = userTasks.get(0);

        Map map = new HashMap<>();
        map.put("userTaskId", userTask.getId());
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 获取非中奖类taskId(回填，收藏加购，好评)
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activityDef
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SimpleResponse getUserTaskIdByActivityDef(Long merchantId, Long shopId, Long userId, ActivityDef activityDef, Long goodsId, String orderId, Integer source) {
        QueryWrapper<UserTask> userTaskQueryWrapper = new QueryWrapper<>();
        userTaskQueryWrapper.lambda()
                .eq(UserTask::getActivityDefId, activityDef.getId())
                .eq(UserTask::getUserId, userId)
        ;
        if (StringUtils.isNotBlank(orderId)) {
            userTaskQueryWrapper.and(params -> params.eq("order_id", orderId));
        }
        userTaskQueryWrapper.orderByDesc("id");
        List<UserTask> userTasks = userTaskService.list(userTaskQueryWrapper);

        UserTask userTask = null;
        boolean fillOrderBackTask = activityDef.getType() == ActivityDefTypeEnum.FILL_BACK_ORDER.getCode()
                || activityDef.getType() == ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode();
        if (!CollectionUtils.isEmpty(userTasks)) {
            userTask = userTasks.get(0);
            //若已经完成,且从任务空间进来,则重新新建
            if (source == 1 && userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                userTask = null;
            } else {
                userTask = userTasks.get(0);
            }
        }
        if (userTask == null && !fillOrderBackTask) {
            //创建
            //查询关键词
            boolean favCart = activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART.getCode()) ||
                    activityDef.getType().equals(ActivityDefTypeEnum.FAV_CART_V2.getCode());
            String keywords = "";
            if (favCart) {
                List<String> kws = JSONArray.parseArray(activityDef.getKeyWord(), String.class);
                if (CollectionUtils.isEmpty(kws)) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "收藏加购需要关键词");
                }
                Random random = new Random();
                int s = random.nextInt(kws.size());
                keywords = kws.get(s);
            }
            userTask = createUserTask(merchantId, shopId, userId, activityDef, keywords, goodsId);
        }
        //非中奖任务，需要创建在返回数据..
        Map map = new HashMap<>();
        map.put("userTaskId", userTask.getId());
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(map);
        return simpleResponse;
    }

    /***
     * 查询用户任务-通过活动方式(中奖任务)
     * @param userTask
     * @param merchantShop
     * @param commissionResp
     * @param rate
     * @param costStr
     * @return
     */
    @Override
    public SimpleResponse queryUserTaskByActivity(UserTask userTask, MerchantShop merchantShop, CommissionResp commissionResp,
                                                  BigDecimal rate, String costStr, Goods goods) {
        //中奖任务,若从没进，24小时倒计时，进了之后，立马更新开始时间，开始进入任务倒计时
        Date now = new Date();
        if (userTask.getStartTime() == null) {
            userTask.setStartTime(now);
            userTask.setUtime(now);
            int updateRows = userTaskService.update(userTask);
            if (updateRows == 0) {
                log.error("<<<<<<<<user_task表，更新startTime执行sql失败");
                return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "网络异常，请稍候重试");
            }
        }
        //查询活动
        Activity activity = null;
        ActivityDef activityDef = null;
        if (userTask.getActivityId() != null && userTask.getActivityId() > 0) {
            activity = activityService.findById(userTask.getActivityId());
            activityDef = activityDefService.selectByPrimaryKey(activity.getActivityDefId());
        }
        //弹窗
        boolean redisUserTaskRedPack = this.redisService.getRedisUserTaskRedPack(userTask.getId());
        TaskResp resp = new TaskResp();
        BeanUtils.copyProperties(commissionResp, resp);
        resp.setCommission(userTask.getRpAmount().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.fromShop(merchantShop);
        resp.fromUserTask(userTask, activity, redisUserTaskRedPack);
        resp.fromGoods(goods);
        resp.fromPics(userTask, activityDef, null, null, null);
        resp.fromUserTaskCountdown(userTask, null, activity, null, NumberUtils.toLong(costStr));

//        //今日预计可赚收益
//        Map<String, Object> map = oneAppService.queryNewcomerTaskInfo(merchantShop.getId(), userTask.getUserId());
//        BigDecimal estimateProfit = BigDecimal.ZERO;
//        if (map != null) {
//            if (map.get("newbieAmount") != null) {
//                estimateProfit = estimateProfit.add(new BigDecimal(map.get("newbieAmount").toString()));
//            }
//            if (map.get("shopManagerAmount") != null) {
//                estimateProfit = estimateProfit.add(new BigDecimal(map.get("shopManagerAmount").toString()));
//            }
//            if (map.get("dailyAmount") != null) {
//                estimateProfit = estimateProfit.add(new BigDecimal(map.get("dailyAmount").toString()));
//            }
//        }

        //中奖任务无预计可赚
        resp.setEstimateProfit(BigDecimal.ZERO.toString());
        //无佣金概念
        resp.setCommission(resp.getRedPackMoney());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 查询用户任务-通过活动定义方式(非中奖任务)
     * @param userTask
     * @param merchantShop
     * @param commissionResp
     * @param rate
     * @param costStr
     * @return
     */
    @Override
    public SimpleResponse queryUserTaskByActivityDef(UserTask userTask, MerchantShop merchantShop, CommissionResp commissionResp,
                                                     BigDecimal rate, String costStr, Goods goods) {
        //获取活动定义
        ActivityDef activityDef = null;
        if (userTask.getActivityDefId() != null && userTask.getActivityDefId() > 0) {
            activityDef = activityDefService.selectByPrimaryKey(userTask.getActivityDefId());
        }
        //查询 收藏加购,好评晒图,回填订单的弹窗
        boolean redisUserTaskRedPack = this.redisService.getRedisUserTaskRedPack(userTask.getId());

        TaskResp resp = new TaskResp();
        BeanUtils.copyProperties(commissionResp, resp);
        resp.fromUserTask(userTask, null, redisUserTaskRedPack);
        resp.fromPics(userTask, activityDef, null, null, rate);
        resp.fromShop(merchantShop);
        resp.setCommission(userTask.getRpAmount().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.fromUserTaskCountdown(userTask, activityDef, null, null, NumberUtils.toLong(costStr));
        resp.fromGoods(goods);

        //今日预计可赚收益
        Map<String, Object> map = oneAppService.queryNewcomerTaskInfo(merchantShop.getId(), userTask.getUserId());
        BigDecimal estimateProfit = BigDecimal.ZERO;
        if (map != null) {
            if (map.get("newbieAmount") != null) {
                estimateProfit = estimateProfit.add(new BigDecimal(map.get("newbieAmount").toString()));
            }
            if (map.get("shopManagerAmount") != null) {
                estimateProfit = estimateProfit.add(new BigDecimal(map.get("shopManagerAmount").toString()));
            }
            if (map.get("dailyAmount") != null) {
                estimateProfit = estimateProfit.add(new BigDecimal(map.get("dailyAmount").toString()));
            }
            resp.setEstimateProfit(estimateProfit.setScale(2, BigDecimal.ROUND_DOWN).toString());
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }


    /***
     * 创建userTask
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activityDef
     * @param keywords
     * @param goodsId
     * @return
     */
    private UserTask createUserTask(Long merchantId, Long shopId, Long userId, ActivityDef activityDef, String keywords, Long goodsId) {
        int userActivityType = -1;
        int userTaskType = -5;
        //回填订单
        if (Arrays.asList(
                ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()).contains(activityDef.getType())) {
            userActivityType = UserActivityTypeEnum.FILL_BACK_ORDER_V2.getCode();
            userTaskType = UserTaskTypeEnum.ORDER_REDPACK_V2.getCode();
        } else if (Arrays.asList(
                ActivityDefTypeEnum.FAV_CART_V2.getCode()).contains(activityDef.getType())) {
            //收藏加购
            userActivityType = UserActivityTypeEnum.FAV_CART_V2.getCode();
            userTaskType = UserTaskTypeEnum.REDPACK_TASK_FAV_CART_V2.getCode();
        } else if (Arrays.asList(
                ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()).contains(activityDef.getType())) {
            //好评晒图
            userActivityType = UserActivityTypeEnum.GOODS_COMMENT_V2.getCode();
            userTaskType = UserTaskTypeEnum.GOOD_COMMENT_V2.getCode();
        } else {
            log.error("<<<<<<<< 非中奖任务-获取类型异常，类型没有匹配 activityDef:{}, userTaskType:{}, userActivityType:{}", activityDef, userTaskType, userActivityType);
        }

        //创建userActivity流程
        UserActivity userActivity = null;
        try {
            userActivity = userActivityService.insertUserActivityByActivityDef(merchantId, shopId, userId, activityDef.getId(), userActivityType, UserActivityStatusEnum.RED_PACKAGE_ING.getCode());
        } catch (Exception e) {
            log.error("<<<<<<<<非中奖任务，创建userActivity异常 e:{}", e);
        }
        if (userActivity == null) {
            log.error("<<<<<<<<非中奖任务，创建userActivity异常");
        }
        Assert.isTrue(userActivity != null, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "存储异常~~请重试");
        //创建userTask流程
        BigDecimal amount = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
        CommissionBO commissionBO = commissionService.commissionCalculate(userId, amount);

        UserTask userTask = null;
        try {
            userTask = userTaskService.insertUserTaskByActivityDef(merchantId, shopId, userId, UserTaskStatusEnum.INIT.getCode(), userTaskType, keywords, goodsId, activityDef.getId(), userActivity.getId(), commissionBO.getCommission());
        } catch (Exception e) {
            log.error("<<<<<<<<非中奖任务，创建UserTask异常 e:{}", e);
        }
        Assert.isTrue(userTask != null, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "存储异常~~请重试");
        return userTask;
    }

    /***
     * 查看任务的有效性
     * @param userTask
     * @return
     */
    @Override
    public SimpleResponse compareUserTask(UserTask userTask) {
        Integer status = userTask.getStatus();
        if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO11.getCode(), "审核中，请勿再次提交");
        }
        if (status == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO12.getCode(), "中奖任务超时，请勿再次提交");
        }
        if (status == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO13.getCode(), "截图已审核通过，请勿再次提交");
        }
        if (status == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO14.getCode(), "任务已完成，请勿再次提交");
        }
        return new SimpleResponse();
    }

    /***
     * 查看任务是否中奖类型
     * @param userTask
     * @return
     */
    @Override
    public boolean queryUserTaskTypeByAward(UserTask userTask) {
        if (Arrays.asList(
                UserTaskTypeEnum.ASSIST_START.getCode(),
                UserTaskTypeEnum.ASSIST_START_V2.getCode(),
                UserTaskTypeEnum.ORDER_BACK_JOIN.getCode(),
                UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START_V2.getCode()).contains(userTask.getType())) {
            return true;
        }
        return false;
    }

    /***
     * 获取关键词信息
     * @param merchantID
     * @param goodsId
     * @return
     */
    private String getRedPackFavCart(Long merchantID, Long goodsId) {
        String keywords = "";
        QueryWrapper<GoodsPlus> goodsPlusWrapper = new QueryWrapper<>();
        goodsPlusWrapper.lambda().eq(GoodsPlus::getMerchantId, merchantID).eq(GoodsPlus::getGoodsId, goodsId)
                .eq(GoodsPlus::getType, GoodsPlusTypeEnum.KEY_WORDS.getCode())
        ;
        GoodsPlus goodsPlus = this.goodsPlusService.getOne(goodsPlusWrapper);
        if (goodsPlus == null) {
            return null;
        }
        List<String> kwds = JSONObject.parseArray(goodsPlus.getValue(), String.class);
        //随机取一个
        if (kwds.size() == 1) {
            keywords = kwds.get(0);
        } else {
            keywords = kwds.get(NumberUtils.toInt(RobotUserUtil.getRandom(0, kwds.size() - 1)));
        }
        return keywords;
    }


    /***
     * 创建回填订单任务
     * @param merhcantId
     * @param shopId
     * @param userId
     * @param activityDef
     * @param taskV7Req
     * @return
     */
    private SimpleResponse createUserTaskByFillOrder(Long merhcantId, Long shopId, Long userId, ActivityDef activityDef, TaskV7Req taskV7Req) {
        //扣库存
        BigDecimal amount = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
        CommissionBO commissionBO = commissionService.commissionCalculate(userId, amount);

        // 创建userActivty
        UserActivity userActivity = new UserActivity();
        userActivity.setMerchantId(merhcantId);
        userActivity.setUserId(userId);
        userActivity.setShopId(shopId);
        userActivity.setActivityId(0L);
        userActivity.setType(UserActivityTypeEnum.FILL_BACK_ORDER_V2.getCode());
        userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE_ING.getCode());
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        userActivity.setActivityDefId(activityDef.getId());
        int rows = userActivityService.insertUserActivity(userActivity);
        if (rows == 0) {
            log.error("<<<<<<<<回填订单创建userActivity失败");
        }
        Assert.isTrue(rows > 0, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "存储异常~~请重试");

        UserTask userTask = new UserTask();
        userTask.setMerchantId(merhcantId);
        userTask.setShopId(shopId);
        userTask.setUserId(userId);
        userTask.setActivityDefId(activityDef.getId());
        userTask.setUserActivityId(userActivity.getId());
        // 设置默认值
        userTask.setActivityId(0L);
        userTask.setStatus(UserTaskStatusEnum.AUDIT_WAIT.getCode());
        userTask.setType(UserTaskTypeEnum.ORDER_REDPACK_V2.getCode());
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setOrderId(taskV7Req.getOrderId());

        Date now = new Date();
        userTask.setStartTime(now);
        userTask.setCtime(now);
        userTask.setUtime(now);
        int userTaskRow = userTaskService.insertUserTask(userTask);
        if (userTaskRow == 0) {
            log.error("<<<<<<<<回填订单创建userTask失败");
        }
        Assert.isTrue(userTaskRow > 0, ApiStatusEnum.ERROR_BUS_NO1.getCode(), "存储异常~~请重试");

        return new SimpleResponse();
    }

}
