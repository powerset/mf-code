package mf.code.api.applet.douyin;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.seller.service.SellerMerchantOrderService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.douyin
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年08月14日 11:26
 */
@RestController
@RequestMapping(value = "/api/applet/douyin")
@Slf4j
public class AppletMerchantOrderApi {
    @Autowired
    private SellerMerchantOrderService sellerMerchantOrderService;

    /***
     * 查询商户财务记录-流水
     *
     * @param merchantId
     * @param shopId
     * @param limit
     * @param offset
     * @param type 0交易、退款(包含待结算的) 1：提现、收入(只包含已结算的) 2:待结算佣金管理,本月待结算 3:待结算佣金管理,下月待结算
     * @return
     */
    @RequestMapping(path = "/queryRecordLog", method = RequestMethod.GET)
    public SimpleResponse queryRecordLog(@RequestParam(name = "merchantId") Long merchantId,
                                         @RequestParam(name = "shopId") Long shopId,
                                         @RequestParam(name = "userId") Long userId,
                                         @RequestParam(name = "limit", required = false, defaultValue = "10") int limit,
                                         @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                         @RequestParam(name = "type", defaultValue = "0") int type) {
        return this.sellerMerchantOrderService.queryRecordLog(merchantId, shopId, limit, offset, type);
    }
}
