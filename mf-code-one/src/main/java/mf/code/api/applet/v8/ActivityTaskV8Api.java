package mf.code.api.applet.v8;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.v8.service.ActivityTaskV8Service;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.v8
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月21日 11:15
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/applet/v8/activityTask")
public class ActivityTaskV8Api {
    @Autowired
    private ActivityTaskV8Service activityTaskV8Service;

    /***
     * 完成活动任务型-业务
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param userId
     * @return
     */
    @GetMapping("/finish")
    public SimpleResponse finishActivityTask(@RequestParam(name = "merchantId") Long merchantId,
                                             @RequestParam(name = "shopId") Long shopId,
                                             @RequestParam(name = "activityDefId") Long activityDefId,
                                             @RequestParam(name = "kws", required = false) String kws,
                                             @RequestParam(name = "userId") Long userId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0
                || activityDefId == null || activityDefId == 0 || userId == null || userId == 0) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return activityTaskV8Service.finishActivityTask(merchantId, shopId, activityDefId, userId, kws);
    }

    /***
     * 获取群码
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param userId
     * @return
     */
    @GetMapping("/getGroupCode")
    public SimpleResponse getGroupCode(@RequestParam(name = "merchantId") Long merchantId,
                                       @RequestParam(name = "shopId") Long shopId,
                                       @RequestParam(name = "activityDefId") Long activityDefId,
                                       @RequestParam(name = "userId") Long userId) {
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0
                || activityDefId == null || activityDefId == 0 || userId == null || userId == 0) {
            log.error("参数异常, userId = {}, shopId = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return activityTaskV8Service.getGroupCode(merchantId, shopId, activityDefId, userId);
    }
}
