package mf.code.api.applet.v8.service;

import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.constant.UpayWxOrderMfTradeTypeEnum;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:54
 */
public class UserMoneyAboutDTO {

    /***
     * 根据条件查询订单
     * @param merchantID
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @param type
     * @return
     */
    public static Map<String, Object> selectUpayOrderParams(Long merchantID, Long shopID, Long userID, int offset, int size, Integer type) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("mchId", merchantID);
        map.put("shopId", shopID);
        map.put("userId", userID);
        if (type == null) {
            map.put("type", type);
        }
        map.put("status", OrderStatusEnum.ORDERED.getCode());
        map.put("mfTradeType", UpayWxOrderMfTradeTypeEnum.CLEANINGED.getCode());
        map.put("order", "payment_time");
        map.put("direct", "desc");
        map.put("size", size);
        map.put("offset", offset);
        return map;
    }

    /***
     * 返回信息结果赋值
     * @param offset
     * @param size
     * @param total
     * @param list
     * @param amount
     * @return
     */
    public static Map<String, Object> returnInfoObject(int offset, int size, int total, List<Object> list, BigDecimal amount) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("offset", offset);
        respMap.put("limit", size);
        respMap.put("total", total);
        respMap.put("content", list);
        respMap.put("isPullDown", false);
        //是否还有下一页
        if (offset + size < total) {
            respMap.put("isPullDown", false);
        }
        if (amount != null) {
            //收入记录-汇总金额
            respMap.put("sumMoney", amount.setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        return respMap;
    }

    /***
     * 对返回结果的content对象进行赋值
     * @param upayWxOrders
     * @return
     */
    public static List<Object> addContent(List<UpayWxOrder> upayWxOrders) {
        List<Object> list = new ArrayList<Object>();
        for (UpayWxOrder upayWxOrder : upayWxOrders) {
            Map<String, Object> map = new HashMap<>();
            map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
            map.put("price", "-" + upayWxOrder.getTotalFee());
            list.add(map);
        }
        return list;
    }


    /***
     * 收录记录的查询条件
     * @param merchantId
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @param bizTypes
     * @param section 1平台查询 2店铺查询
     * @return
     */
    public static Map<String, Object> selectUpayOrderIncomeParams(Long merchantId, Long shopID, Long userID, List<Integer> bizTypes,
                                                                  List<Integer> payOrderTypes, Integer section, int offset, int size) {
        Map<String, Object> wxOrderParams = new HashMap<String, Object>();
        if (section != null && section == 2) {
            wxOrderParams.put("shopId", shopID);
        }
        wxOrderParams.put("userId", userID);
        if (!CollectionUtils.isEmpty(payOrderTypes)) {
            wxOrderParams.put("types", payOrderTypes);
        }
        wxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        if (!CollectionUtils.isEmpty(bizTypes)) {
            wxOrderParams.put("bizTypes", bizTypes);
        }
        wxOrderParams.put("mfTradeType", UpayWxOrderMfTradeTypeEnum.CLEANINGED.getCode());
        wxOrderParams.put("order", "payment_time");
        wxOrderParams.put("direct", "desc");
        wxOrderParams.put("size", size);
        wxOrderParams.put("offset", offset);
        return wxOrderParams;
    }

    /***
     * 判断wx订单表记录是否是活动订单
     * @param wxOrder
     * @return
     */
    public static boolean isActivity(UpayWxOrder wxOrder) {
        List<Integer> list = new ArrayList<>();
        list.add(BizTypeEnum.ACTIVITY.getCode());
        list.add(BizTypeEnum.ASSIST_ACTIVITY.getCode());
        list.add(BizTypeEnum.ORDER_BACK.getCode());
        list.add(BizTypeEnum.NEWMAN_START.getCode());
        if (list.contains(wxOrder.getBizType())) {
            return true;
        }
        return false;
    }

    /***
     * 2018.11.16之后的最新分页返回对象
     * @param offset
     * @param size
     * @param total
     * @param list
     * @param amount
     * @return
     */
    public static Map<String, Object> returnInfoObjectNew(int offset, int size, int total, List<Object> list, BigDecimal amount) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("offset", offset);
        respMap.put("limit", size);
        respMap.put("isPullDown", false);
        if (total > 0) {
            respMap.put("isPullDown", true);
        }
        respMap.put("content", list);
        if (amount != null) {
            //收入记录-汇总金额
            respMap.put("sumMoney", amount);
        }
        return respMap;
    }
}
