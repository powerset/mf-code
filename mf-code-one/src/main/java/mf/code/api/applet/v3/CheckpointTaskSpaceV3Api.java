package mf.code.api.applet.v3;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v3.service.CheckpointTaskSpaceService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.v3
 *
 * @description: 闯关类任务空间列表
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月12日 10:45
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v3/checkpointTaskSpace")
public class CheckpointTaskSpaceV3Api {
    @Autowired
    private CheckpointTaskSpaceService checkpointTaskSpaceService;

    /**
     * 关卡任务空间列表
     * 存储用户任务表？
     * <p>
     * 1.点击“拆红包”，在当前页面弹出拆红包弹窗
     * 点击“大转盘”，在当前页面弹出大转盘
     * 点击“参与商品收藏加购、好评晒图任务、赠品领取任务、免单任务”跳转至商品选择页面
     * 2、拆红包任务
     * 情况①：用户完成拆红包任务 / 拆红包任务已结束。拆红包样式统一变为灰色，不显示在当前页面，显示到我的历史任务页面
     * 情况②：活动无库存时，点击拆红包后，在弹窗中点击“拆开”按钮，toast提示“活动暂无库存”（提示以以前的文案为准）
     * 3、参与幸运大转盘抽奖
     * 情况①：商户后台挂起该活动时。大转盘卡片样式变为灰色，不显示在当前页面，显示到我的历史任务页面
     * 情况②：活动无库存时，点击大转盘卡片后，点击“抽奖”按钮，在弹窗上进行toast提示“活动暂无库存”（提示以以前的文案为准）
     * 4、参与收藏加购任务
     * 情况①：商户后台挂起该活动时。大转盘卡片样式变为灰色，不显示在当前页面，显示到我的历史任务页面
     * 情况②：活动无库存时，点击大转盘卡片后，点击“抽奖”按钮，在弹窗上进行toast提示“活动暂无库存”（提示以以前的文案为准）
     * <p>
     * a.拆红包；
     * b.幸运大转盘；
     * c.收藏加购；
     * d.好评晒图；
     * e.回填订单；
     * f.赠品;
     * g.免单抽奖；
     */
    @RequestMapping(path = "/queryCheckpointTaskSpace", method = RequestMethod.GET)
    public SimpleResponse queryCheckpointTaskSpace(@RequestParam(name = "merchantId") Long merchantId,
                                                   @RequestParam(name = "shopId") Long shopID,
                                                   @RequestParam(name = "userId") Long userId) {
        return this.checkpointTaskSpaceService.queryCheckpointTaskSpace(merchantId, shopID, userId);
    }

    /***
     * 选择商品详细
     * @param merchantId
     * @param shopID
     * @param type 1.收藏加购 2.好评晒图 3.赠品 4.免单抽奖 5:回填订单
     * @return
     */
    @RequestMapping(path = "/selectGoodsInfo", method = RequestMethod.GET)
    public SimpleResponse queryGoodsDetail(@RequestParam(name = "merchantId") Long merchantId,
                                           @RequestParam(name = "shopId") Long shopID,
                                           @RequestParam(name = "userId") Long userId,
                                           @RequestParam(name = "type") int type,
                                           @RequestParam(name = "offset", required = false,defaultValue="0") int offset,
                                           @RequestParam(name = "limit", required = false, defaultValue = "6") final int size) {
        return this.checkpointTaskSpaceService.queryGoodsDetail(merchantId, shopID, userId, type, offset, size);
    }

    /***
     * 闯关地图弹幕
     * @param merchantId
     * @param shopId
     * @param userId 用于拦截过滤筛选通过
     * @return
     */
    @RequestMapping(path = "/getBarrage", method = RequestMethod.GET)
    public SimpleResponse getBarrage(@RequestParam(name = "merchantId") Long merchantId,
                                     @RequestParam(name = "shopId") Long shopId,
                                     @RequestParam(name = "userId", required = false) Long userId) {
        return this.checkpointTaskSpaceService.getBarrage(merchantId, shopId);
    }
}
