package mf.code.api.applet.service.UserMoneyTradeDetail;

import mf.code.activity.repo.po.Activity;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;

import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 18:05
 */
public interface UserMoneyTradeDetailConverter {


    Map<String, Object> getUserMoneyTradeDetail(UpayWxOrder upayWxOrder,
                                                Integer payType,
                                                Map<Long, Activity> activityMap,
                                                Map<Long, Goods> goodsMap,
                                                CommonDict commonDict);

    Map<String, Object> getTaskTradeDetail(UpayWxOrder upayWxOrder,
                                           Integer payType,
                                           Map<Long, User> userMap);

    Map<String, Object> covertUserMoneyTradeDetailForProductGoodsShopping(UpayWxOrder upayWxOrder,
                                                                          Integer payType,
                                                                          Map<Long, Map> shopMallMap);


    Map<String, Object> covertUserMoneyTradeDetailForContribution(UpayWxOrder upayWxOrder, Integer payType, Map<Long, User> userMap);
}
