package mf.code.api.applet.service.UserMoneyTradeDetail;

import mf.code.upay.repo.enums.BizTypeEnum;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 20:19
 */
public enum UserMoneyIndexNameEnum {
    /***
     *      1:免单抽奖(奖品名)(+)
     *      2:轮盘抽奖(轮盘抽奖)(+)
     *      3:随机红包(随机成功)(+)
     *      4:红包任务(好评返现任务)(+)
     *      5:钱包提现(提现成功)(-)
     *      6:钱包充值(微信充值)(+)
     *      7:钱包支出(微信充值发起抽奖活动)(-)
     *               (活动退款至微信)(-)
     *      8:钱包退回(抽奖活动退款)(+)
     *      9:好评晒图任务()(+)
     *      10:拆红包()(+)
     */

    ACTIVITY(1, "加权计划"),
    LUCKY_WHEEL(2, "轮盘抽奖"),
    RANDOMREDPACK(3, "随机红包"),
    REDPACKTASK(4, "红包任务"),
    WALLET_CASH(5, "钱包提现"),
    WALLET_RECHARGE(6, "钱包充值"),
    WALLET_EXPENDITURE(7, "钱包支出"),
    WALLET_REFUND(8, "钱包退回"),
    GOODSCOMMENT(9, "好评晒图任务"),
    OPEN_RED_PACKAGE(10, "拆红包"),
    ASSIST_ACTIVITY(11, "助力活动"),
    ORDER_BACK(12, "免单抽奖"),
    NEWMAN_START(13, "新人有礼"),
    CHECKPOINT(14, "财富大闯关"),
    GOSHOPPING_ORDER(15, "购买商品"),
    REFUND_GOSHOPPING_ORDER(16, "申请退款成功"),
    SHOPPING_REBATE(17, "购买返利"),
    SHOPPING_CONTRIBUTION(18, "团员贡献"),
    TASK_AWARD(19, "任务奖金"),
    TASK_COMMISSION(20, "任务佣金"),
    LEVEL_UP_SHOPMANAGER(21, "升级店长"),
    ;

    private int code;
    private String message;

    UserMoneyIndexNameEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String findByDesc(int code) {
        for (BizTypeEnum typeEnum : BizTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum.getMessage();
            }
        }
        return null;
    }
}
