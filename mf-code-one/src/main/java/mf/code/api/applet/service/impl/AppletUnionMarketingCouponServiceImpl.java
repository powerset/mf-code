package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.sd4324530.fastweixin.util.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.api.applet.dto.JKMFUserCouponDTO;
import mf.code.api.applet.dto.UserCouponQueryDTO;
import mf.code.api.applet.service.AppletUnionMarketingCouponService;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.constant.ProductConstant;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.dto.SellerProductAndSkuResultDTO;
import mf.code.merchant.repo.dao.MerchantShopCouponMapper;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.one.constant.MerchantShopCouponTypeEnum;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.dto.CouponInfoDTO;
import mf.code.one.dto.UnionMarketingCouponDTO;
import mf.code.ucoupon.repo.dao.UserCouponMapper;
import mf.code.ucoupon.repo.po.UserCoupon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author gel
 */
@Slf4j
@Service
public class AppletUnionMarketingCouponServiceImpl implements AppletUnionMarketingCouponService {
    @Autowired
    private UserCouponMapper userCouponMapper;
    @Autowired
    private MerchantShopCouponMapper merchantShopCouponMapper;
    @Autowired
    private GoodsAppService goodsAppService;

    @Value("${aliyunoss.compressionRatio}")
    private String compressionRatioPic;

    /**
     * 我的卡券
     * 根据优惠券的使用状态查询优惠券信息，status 1，未使用，2已使用，3以过期
     * <p>
     * 根据类型查询已拥有的优惠券信息
     *
     * @param userCouponQueryDTO 1，未使用，2已使用，3以过期
     * @return
     */
    @Override
    public SimpleResponse queryMyCoupon(UserCouponQueryDTO userCouponQueryDTO) {
        SimpleResponse simpleResponse = new SimpleResponse();

        int limit = userCouponQueryDTO.getLimit();
        int offset = userCouponQueryDTO.getOffset();

        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getShopId, userCouponQueryDTO.getShopId())
                .eq(UserCoupon::getUserId, userCouponQueryDTO.getUserId())
                .eq(UserCoupon::getStatus, userCouponQueryDTO.getStatus())
                .eq(UserCoupon::getType, userCouponQueryDTO.getType());
        Integer count = userCouponMapper.selectCount(wrapper);

        UnionMarketingCouponDTO unionMarketingCouponDTO = new UnionMarketingCouponDTO();
        unionMarketingCouponDTO.setTotalCount(count);
        unionMarketingCouponDTO.setIsPullDown(false);
        unionMarketingCouponDTO.setLimit(limit);
        unionMarketingCouponDTO.setOffset(offset);
        unionMarketingCouponDTO.setCouponInfoDTOList(new ArrayList<>());
        simpleResponse.setData(unionMarketingCouponDTO);
        // 偏移量大于总条数直接返回空列表
        if (offset > count) {
            return simpleResponse;
        }

        QueryWrapper<UserCoupon> listWrapper = new QueryWrapper<>();
        listWrapper.lambda()
                .eq(UserCoupon::getShopId, userCouponQueryDTO.getShopId())
                .eq(UserCoupon::getUserId, userCouponQueryDTO.getUserId())
                .eq(UserCoupon::getStatus, userCouponQueryDTO.getStatus())
                .eq(UserCoupon::getType, userCouponQueryDTO.getType());

        Page<UserCoupon> page = new Page<>(offset / limit + 1, limit);

        IPage<UserCoupon> upayMerchantShopCouponIPage = userCouponMapper.selectPage(page, wrapper);
        if (upayMerchantShopCouponIPage == null) {
            return simpleResponse;
        }

        List<UserCoupon> records = upayMerchantShopCouponIPage.getRecords();
        if (CollectionUtil.isEmpty(records)) {
            return simpleResponse;
        }
        List<CouponInfoDTO> couponInfoDTOList = new ArrayList<>();
        for (UserCoupon userCoupon : records) {
            CouponInfoDTO couponInfoDTO = changeCouponInfoDTO(userCoupon);
            if (couponInfoDTO != null) {
                couponInfoDTOList.add(couponInfoDTO);
            }
        }
        unionMarketingCouponDTO.setCouponInfoDTOList(couponInfoDTOList);
        unionMarketingCouponDTO.setIsPullDown(true);
        simpleResponse.setData(unionMarketingCouponDTO);
        return simpleResponse;
    }

    /**
     * 重新封装 CouponInfoDTO
     *
     * @param record
     * @return
     */
    private CouponInfoDTO changeCouponInfoDTO(UserCoupon record) {
        CouponInfoDTO couponInfoDTO = new CouponInfoDTO();
        Long couponId = record.getCouponId();
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponMapper.selectByPrimaryKey(couponId);
        if (null == merchantShopCoupon) {
            return null;
        }

        couponInfoDTO.setUserCouponId(record.getId());
        couponInfoDTO.setAmount(merchantShopCoupon.getAmount());
        couponInfoDTO.setCouponId(merchantShopCoupon.getId());
        couponInfoDTO.setCouponName(merchantShopCoupon.getName());
        couponInfoDTO.setLimitAmount(merchantShopCoupon.getLimitAmount());
        couponInfoDTO.setDisplayEndTime(merchantShopCoupon.getDisplayEndTime());
        couponInfoDTO.setDisplayStartTime(merchantShopCoupon.getDisplayStartTime());
        couponInfoDTO.setEndTime(DateUtil.dateToString(merchantShopCoupon.getDisplayEndTime(), DateUtil.POINT_DATE_FORMAT));
        couponInfoDTO.setStartTime(DateUtil.dateToString(merchantShopCoupon.getDisplayStartTime(), DateUtil.POINT_DATE_FORMAT));
        couponInfoDTO.setStatus(record.getStatus());
        Long productId = merchantShopCoupon.getProductId();
        couponInfoDTO.setProductId(productId);

        //根据产品id查询产品信息
        List<Long> productIdList = new ArrayList<>();
        productIdList.add(productId);
        List<ProductEntity> productList = goodsAppService.listByIds(productIdList);
        if (CollectionUtil.isEmpty(productList) || productList.size() < 1) {
            return null;
        }

        ProductEntity productEntity = productList.get(0);
        String productName = productEntity.getProductTitle();
        List<String> stringList = JSON.parseArray(productEntity.getMainPics(), String.class);
        if (!CollectionUtils.isEmpty(stringList)) {
            couponInfoDTO.setMainPic(stringList.get(0) + compressionRatioPic);
        }
        couponInfoDTO.setProductName(productName);
        return couponInfoDTO;
    }


    /**
     * 查询可使用的优惠券，并分析出最佳的优惠金额
     *
     * @param shopId
     * @param userId
     * @param goodId
     * @param orderPrice
     * @return
     */
    @Override
    public JKMFUserCouponDTO queryCanApplyCoupon(Long shopId, Long userId, Long goodId, Long skuId, BigDecimal orderPrice) {
        JKMFUserCouponDTO jkmfUserCouponDTO = new JKMFUserCouponDTO();
        jkmfUserCouponDTO.setCouponId(null);
        jkmfUserCouponDTO.setAmount(BigDecimal.ZERO);
        jkmfUserCouponDTO.setLimitAmount(BigDecimal.ZERO);
        jkmfUserCouponDTO.setHasCoupon(0);

        if (orderPrice.compareTo(BigDecimal.ZERO) <= 0) {
            return jkmfUserCouponDTO;
        }

        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getShopId, shopId)
                .eq(UserCoupon::getUserId, userId)
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                .eq(UserCoupon::getType, UserCouponTypeEnum.JKMF_COUPON.getCode());

        List<UserCoupon> records = userCouponMapper.selectList(wrapper);

        if (CollectionUtil.isEmpty(records)) {
            return jkmfUserCouponDTO;
        }
        List<Long> couponIdList = new ArrayList<>();
        for (UserCoupon userCoupon : records) {
            couponIdList.add(userCoupon.getCouponId());
        }
        if (couponIdList.size() > 1000) {
            couponIdList = couponIdList.subList(0, 1000);
        }

        // 按照商品，优惠券id列表，优惠券金额倒叙 ==> 查询出金额最大可以优先使用的优惠券
        QueryWrapper<MerchantShopCoupon> merchantShopCouponWrapper = new QueryWrapper<>();
        merchantShopCouponWrapper.lambda()
                .in(MerchantShopCoupon::getId, couponIdList)
                .eq(MerchantShopCoupon::getShopId, shopId)
                .eq(MerchantShopCoupon::getType, MerchantShopCouponTypeEnum.GOODS.getCode())
                .eq(MerchantShopCoupon::getProductId, goodId)
                .orderByDesc(MerchantShopCoupon::getAmount);

        List<MerchantShopCoupon> merchantShopCouponList = merchantShopCouponMapper.selectList(merchantShopCouponWrapper);
        if (CollectionUtils.isEmpty(merchantShopCouponList)) {
            return jkmfUserCouponDTO;
        }
        // 部分商品不能使用优惠券，这里需要判断商品属性，检查优惠券使用状态
        Map<String, SellerProductAndSkuResultDTO> productAndSkuMap = new HashMap<>();
        SimpleResponse productAndSkuResponse = goodsAppService.queryProductSkuMapBySkuIdList(Arrays.asList(skuId));
        if (productAndSkuResponse != null && !productAndSkuResponse.error()) {
            String jsonString = JSON.toJSONString(productAndSkuResponse.getData());
            productAndSkuMap = JSONObject.parseObject(jsonString, new TypeReference<HashMap<String, SellerProductAndSkuResultDTO>>() {
            });
        }
        SellerProductAndSkuResultDTO productAndSkuResultDTO = productAndSkuMap.get(skuId.toString());
        if (productAndSkuResultDTO != null) {
            if (productAndSkuResultDTO.getNotShow() == ProductConstant.ProductSku.NotShow.FLASH_SALE
                    || productAndSkuResultDTO.getNotShow() == ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK) {
                return jkmfUserCouponDTO;
            }
        }
        /*
         * TODO 这里可能的问题是一个商品多个优惠券，容易出现排序上的问题：
         *  - 金额相同，limit不同，可能查出来的不一定是用户所想的。
         *  - 如果没有金额比订单金额小的优惠券会出现过滤掉问题，选不出最佳的优惠券问题
         */
        MerchantShopCoupon tempCoupon = null;
        for (MerchantShopCoupon merchantShopCoupon : merchantShopCouponList) {
            // 排除门槛金额大于订单金额的优惠券
            if (merchantShopCoupon.getLimitAmount().compareTo(orderPrice) > 0) {
                continue;
            }
            // 排除优惠金额大于订单金额的优惠券
            if (merchantShopCoupon.getAmount().compareTo(orderPrice) > 0) {
                tempCoupon = merchantShopCoupon;
                continue;
            }

            return getJkmfUserCouponDTO(jkmfUserCouponDTO, merchantShopCoupon, records);
        }
        // 遍历之后都没有合适的优惠券，但是tempCoupon有，则说明存在优惠券,并且遍历的列表为倒叙，所以金额为最接近订单金额的优惠券。所以直接返回该优惠券
        if (tempCoupon != null) {
            return getJkmfUserCouponDTO(jkmfUserCouponDTO, tempCoupon, records);
        }
        return jkmfUserCouponDTO;
    }

    /**
     * 获取返回结果
     *
     * @param jkmfUserCouponDTO
     * @param tempCoupon
     * @return
     */
    private JKMFUserCouponDTO getJkmfUserCouponDTO(JKMFUserCouponDTO jkmfUserCouponDTO, MerchantShopCoupon tempCoupon, List<UserCoupon> records) {
        for (UserCoupon userCoupon : records) {
            // 需要返回用户
            if (userCoupon.getCouponId().equals(tempCoupon.getId())) {
                jkmfUserCouponDTO.setUserCouponId(userCoupon.getId());
            }
        }
        jkmfUserCouponDTO.setCouponId(tempCoupon.getId());
        jkmfUserCouponDTO.setAmount(tempCoupon.getAmount());
        jkmfUserCouponDTO.setLimitAmount(tempCoupon.getLimitAmount());
        jkmfUserCouponDTO.setHasCoupon(1);
        return jkmfUserCouponDTO;
    }

    /**
     * 查看优惠券
     * <p>
     * 可用优惠群和不可用优惠券都只展示优惠券未使用的状态，已使用和已过期则不使用
     * <p>
     * 不可用优惠券是指优惠券关联的产品id和入参的产品id不一致，且优惠券的状态是未使用
     * <p>
     * 根据优惠券使用状况查询优惠券信息列表
     *
     * @param userCouponQueryDTO
     * @return
     */
    @Override
    public SimpleResponse queryJKMFCouponByStatusForOrder(UserCouponQueryDTO userCouponQueryDTO) {

        SimpleResponse simpleResponse = new SimpleResponse();
        int limit = userCouponQueryDTO.getLimit();
        int offset = userCouponQueryDTO.getOffset();
        UnionMarketingCouponDTO unionMarketingCouponDTO = new UnionMarketingCouponDTO();
        unionMarketingCouponDTO.setTotalCount(0);
        unionMarketingCouponDTO.setIsPullDown(false);
        unionMarketingCouponDTO.setLimit(limit);
        unionMarketingCouponDTO.setOffset(offset);
        unionMarketingCouponDTO.setCouponInfoDTOList(new ArrayList<>());
        unionMarketingCouponDTO.setCanUseCount(0);
        unionMarketingCouponDTO.setNotUseCount(0);
        simpleResponse.setData(unionMarketingCouponDTO);

        // 这里数据结构，没有办法反查。所以直接查询用户全部优惠券
        if (offset > 0) {
            return simpleResponse;
        }
        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getShopId, userCouponQueryDTO.getShopId())
                .eq(UserCoupon::getUserId, userCouponQueryDTO.getUserId())
                .eq(UserCoupon::getType, userCouponQueryDTO.getType());
        List<UserCoupon> userCouponList = userCouponMapper.selectList(wrapper);
        if (userCouponList.size() > 100) {
            log.error("用户优惠券存在大于100张的情况，需要优化订单页面的优惠券查询");
        }
        if (CollectionUtils.isEmpty(userCouponList)) {
            return simpleResponse;
        }
        List<Long> couponIdList = new ArrayList<>();
        for (UserCoupon userCoupon : userCouponList) {
            couponIdList.add(userCoupon.getCouponId());
        }
        List<MerchantShopCoupon> merchantShopCouponList = merchantShopCouponMapper.selectBatchIds(couponIdList);
        if (CollectionUtils.isEmpty(merchantShopCouponList)) {
            return simpleResponse;
        }
        // 遍历优惠券定义，筛选能使用的优惠券
        Map<Long, Boolean> canUseCouponMap = new HashMap<>();
        for (MerchantShopCoupon merchantShopCoupon : merchantShopCouponList) {
            if (merchantShopCoupon.getProductId().equals(userCouponQueryDTO.getGoodId())) {
                Date now = new Date();
                if (merchantShopCoupon.getDisplayEndTime().after(now)
                        && merchantShopCoupon.getDisplayStartTime().before(now)) {
                    canUseCouponMap.put(merchantShopCoupon.getId(), true);
                    continue;
                }
            }
            canUseCouponMap.put(merchantShopCoupon.getId(), false);
        }
        // 查询可用不可用优惠券两个列表
        List<CouponInfoDTO> canUseList = new ArrayList<>();
        List<CouponInfoDTO> notUseList = new ArrayList<>();
        for (UserCoupon userCoupon : userCouponList) {
            CouponInfoDTO couponInfoDTO = changeCouponInfoDTO(userCoupon);
            if (couponInfoDTO == null) {
                continue;
            }
            if (canUseCouponMap.get(userCoupon.getCouponId())) {
                if (userCoupon.getStatus() == UserCouponStatusEnum.RECEIVIED.getCode()) {
                    canUseList.add(couponInfoDTO);
                    continue;
                }
            }
            notUseList.add(couponInfoDTO);
        }
        // 部分商品不能使用优惠券，这里需要判断商品属性，检查优惠券使用状态
        if (userCouponQueryDTO.getSkuId() != null) {
            Map<String, SellerProductAndSkuResultDTO> productAndSkuMap = new HashMap<>();
            SimpleResponse productAndSkuResponse = goodsAppService.queryProductSkuMapBySkuIdList(Arrays.asList(userCouponQueryDTO.getSkuId()));
            if (productAndSkuResponse != null && !productAndSkuResponse.error()) {
                String jsonString = JSON.toJSONString(productAndSkuResponse.getData());
                productAndSkuMap = JSONObject.parseObject(jsonString, new TypeReference<HashMap<String, SellerProductAndSkuResultDTO>>() {
                });
            }
            SellerProductAndSkuResultDTO productAndSkuResultDTO = productAndSkuMap.get(userCouponQueryDTO.getSkuId().toString());
            if (productAndSkuResultDTO != null) {
                // 如果是秒杀商品，则直接全部返回不可用
                if (productAndSkuResultDTO.getNotShow() == ProductConstant.ProductSku.NotShow.FLASH_SALE
                        || productAndSkuResultDTO.getNotShow() == ProductConstant.ProductSku.NotShow.PLANTFORM_ORDER_BACK) {
                    notUseList.addAll(canUseList);
                    canUseList.clear();
                    if (userCouponQueryDTO.getStatus() == 1) {
                        unionMarketingCouponDTO.setCouponInfoDTOList(canUseList);
                    } else {
                        unionMarketingCouponDTO.setCouponInfoDTOList(notUseList);
                    }
                    unionMarketingCouponDTO.setCanUseCount(canUseList.size());
                    unionMarketingCouponDTO.setNotUseCount(notUseList.size());
                    unionMarketingCouponDTO.setIsPullDown(false);
                    simpleResponse.setData(unionMarketingCouponDTO);
                    return simpleResponse;
                }
            }
        }

        if (userCouponQueryDTO.getStatus() == 1) {
            unionMarketingCouponDTO.setCouponInfoDTOList(canUseList);
        } else {
            unionMarketingCouponDTO.setCouponInfoDTOList(notUseList);
        }
        unionMarketingCouponDTO.setCanUseCount(canUseList.size());
        unionMarketingCouponDTO.setNotUseCount(notUseList.size());
        unionMarketingCouponDTO.setIsPullDown(false);
        simpleResponse.setData(unionMarketingCouponDTO);
        return simpleResponse;
    }
}
