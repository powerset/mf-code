package mf.code.api.applet.v8.dto;

import lombok.Data;
import mf.code.user.repo.po.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.api.applet.v8.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 17:13
 */
@Data
public class CheckpointTaskSpaceDTO {
    //昵称
    private String nickName;
    //头像
    private String avatarUrl;
    //用户角色 0：普通用户 1：店长
    private int role;
    //已赚取收益
    private BigDecimal earnProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN);
    //今天剩余任务数
    private int remainTask;
    //预估可赚
    private BigDecimal estimateProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN);

    private List<CheckpointTaskSpaceDialog> dialogs;

    /***
     * 具体类型的任务展现
     */
    private List<TaskDTO> tasks = new ArrayList<>();

    /***
     * 弹窗对象信息
     * type:1 邀请粉丝 2推荐下级成为店长
     */
    @Data
    public static class CheckpointTaskSpaceDialog {
        private String rebate;
        private String unit;
        private int num;
        private int type;
    }


    @Data
    public static class TaskDTO {
        //总库存
        private int stock;
        //剩余库存
        private int remainStock;
        //类型(新手，店长，日常)
        private int type;

        private Long merchantId;
        private Long shopId;
        /***
         * 任务类型的具体详情
         */
        private List<NewbieTask> taskDetails = new ArrayList<>();
    }

    public void from(User user) {
        this.nickName = user.getNickName();
        this.avatarUrl = user.getAvatarUrl();
        this.role = user.getRole();
    }
}
