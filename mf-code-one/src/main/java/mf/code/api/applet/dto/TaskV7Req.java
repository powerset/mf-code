package mf.code.api.applet.dto;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.api.applet.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月22日 15:21
 */
@Data
public class TaskV7Req {
    private Long merchantId;
    private Long shopId;
    private Long userId;

    /***
     * 中奖活动类必传
     */
    private Long taskId;

    /***
     * 消息推送的表单id
     */
    private String formIds;
    /***
     * 上传图片-多张，String
     */
    private String pics;

    /***
     * 20181217 添加用户提交商品URL
     */
    private String goodsUrl;

    /***
     * 回填订单号(回填订单类,好评晒图 必传)，中奖类任务必传
     */
    private String orderId;

    public boolean check() {
        //妥协判断
        boolean fillOrderBack = StringUtils.isNotBlank(this.orderId) && StringUtils.isBlank(this.getPics())
                && (this.getTaskId() == null || this.getTaskId() == 0);
        boolean notFillOrderBack = StringUtils.isNotBlank(pics) && taskId != null && taskId > 0;
        boolean check = merchantId != null && merchantId > 0 && shopId != null && shopId > 0 &&
                userId != null && userId > 0 && (fillOrderBack || notFillOrderBack);
        if (check) {
            return true;
        }
        return false;
    }
}
