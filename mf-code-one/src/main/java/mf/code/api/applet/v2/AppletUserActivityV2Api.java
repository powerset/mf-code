package mf.code.api.applet.v2;/**
 * create by qc on 2018/12/21 0021
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.dto.JoinActivityReq;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.service.AppletUserActivityV2Service;
import mf.code.api.applet.v3.service.AppletUserActivityUV3Service;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author gbf
 * @description :
 */

@Slf4j
@RestController
@RequestMapping("/api/applet/v2/userActivity")
public class AppletUserActivityV2Api {
    @Autowired
    private AppletUserActivityService appletUserActivityService;
    @Autowired
    private AppletUserActivityV2Service appletUserActivityV2Service;
    @Autowired
    private AppletUserActivityUV3Service appletUserActivityUV3Service;
    /**
     * 用户参与或召唤好友
     */
    @PostMapping("/joinActivity")
    public SimpleResponse joinActivityV12(@RequestBody JoinActivityReq joinActivityReq) {
        return appletUserActivityService.joinActivityV12(joinActivityReq.getUserId(), joinActivityReq.getActivityId(), joinActivityReq.getPubUid(),joinActivityReq.getHasShared());
    }

    @PostMapping(value = "/backfillOrder")
    public SimpleResponse backfillOrderV2(@RequestBody BackFillOrderReq req){
//        return appletUserActivityV2Service.backfillOrderV2(req);
        return appletUserActivityUV3Service.backfillOrderAsync(req);
    }

    @GetMapping(value = "/queryBackfillOrderResult")
    public SimpleResponse queryBackfillOrderResult(@RequestParam(name = "userId") Long userId,
                                                   @RequestParam(name = "orderId") String orderId){
        return appletUserActivityV2Service.queryBackfillOrderResult(orderId);
    }

    @GetMapping(value = "/orderInfo")
    public SimpleResponse orderInfo(@RequestParam(name = "userId") Long userId,
                                    @RequestParam(name = "taskId") Long taskId){
        return appletUserActivityV2Service.orderInfo(userId, taskId);
    }
}
