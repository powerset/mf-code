package mf.code.api.applet.v7.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v7.service.AppleteUserTaskCashbackService;
import mf.code.common.constant.DelEnum;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.exception.CommissionReduceException;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.IpUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * mf.code.api.applet.v7.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年05月28日 17:53
 */
@Service
@Slf4j
public class AppleteUserTaskCashbackServiceImpl implements AppleteUserTaskCashbackService {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private AppletUserActivityService appletUserActivityService;

    /***
     * 人工版-审核返现
     * @param userTaskOrg
     * @return
     */
    @Override
    public SimpleResponse userTaskCashback(UserTask userTaskOrg, BizTypeEnum bizTypeEnum, UserCouponTypeEnum userCouponTypeEnum, Integer type) {
        Activity activity = null;
        if (userTaskOrg.getActivityId() != null && userTaskOrg.getActivityId() > 0) {
            activity = activityService.findById(userTaskOrg.getActivityId());
        }
        ActivityDef activityDef = null;
        if (userTaskOrg.getActivityDefId() != null && userTaskOrg.getActivityDefId() > 0) {
            activityDef = activityDefService.selectByPrimaryKey(userTaskOrg.getActivityDefId());
        }

        boolean awardActiviyTask = Arrays.asList(
                UserTaskTypeEnum.ASSIST_START_V2.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START_V2.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START.getCode()).contains(userTaskOrg.getType());

        BigDecimal rpAmount = BigDecimal.ZERO;
        BigDecimal commission = BigDecimal.ZERO;
        if (awardActiviyTask && activity != null) {
            Long goodsId = activity.getGoodsId();
            if (goodsId == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该中奖活动无商品");
            }
            Goods goods = goodsService.selectById(goodsId);
            if (goods == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品不存在");
            }
            if (goods.getDel() == DelEnum.YES.getCode()) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该商品已下线");
            }
            activityDef = activityDefService.selectByPrimaryKey(activity.getActivityDefId());
            if (activityDef != null) {
                commission = activityDef.getCommission();
            }
        }
        if (!awardActiviyTask && activityDef != null) {
            //从def获取佣金
            commission = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
        }

        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配, 扣除佣金
        CommissionDisDto commissionDisDto = new CommissionDisDto(userTaskOrg.getUserId(), userTaskOrg.getShopId(),
                userCouponTypeEnum, commission);
        commissionDisDto.setActivityTaskId(null);
        commissionDisDto.setActivityDefId(userTaskOrg.getActivityDefId());
        commissionDisDto.setActivityId(userTaskOrg.getActivityId());
        CommissionBO commissionBO = null;
        try {
            commissionBO = commissionService.commissionDistribution(commissionDisDto);
        } catch (CommissionReduceException e) {
            log.error("Exception :{}", e);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO15.getCode(),
                    ActivityDefTypeEnum.findByDesc(activityDef.getType()) + "活动保证金不足，请先完成补库存");
        } catch (Exception e) {
            log.error("Exception :{}", e);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO15.getCode(), "服务开小差了，请稍后重试~");
        }

        // 信息加工入库
        UserTask userTask = new UserTask();
        userTask.setId(userTaskOrg.getId());
        userTask.setUserId(userTaskOrg.getUserId());
        userTask.setActivityId(userTaskOrg.getActivityId());
        userTask.setOrderId(userTaskOrg.getOrderId());
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        userTask.setReceiveTime(new Date());
        userTask.setUtime(new Date());
        int rows = userTaskService.update(userTask);
        if (rows == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "数据库异常");
        }

        // 小程序用户活动订单
        if (StringUtils.isNotBlank(userTaskOrg.getOrderId()) && UserTaskTypeEnum.ORDER_REDPACK_V2.getCode() == userTaskOrg.getType()) {
            appletUserActivityService.updateOrCreateActivityOrder(userTaskOrg.getUserId(), userTaskOrg.getOrderId(),
                    userTaskOrg.getShopId(), null, null, null);
        }

        // 更新userActivity
        if (userTaskOrg.getUserActivityId() != null && userTaskOrg.getUserActivityId() > 0) {
            UserActivity userActivity = new UserActivity();
            userActivity.setId(userTaskOrg.getUserActivityId());
            userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
            userActivity.setUtime(new Date());
            userActivityService.updateByPrimaryKey(userActivity);
        }
        redisService.saveRedisUserTaskRedPack(userTaskOrg.getId());
        log.info("<<<<<<<<审核通过，弹窗信息存入redis");
        redisService.deleteUserTaskCommitAndAudit(userTaskOrg.getId());

        Long aid = userTaskOrg.getActivityId();
        Activity activityDB = activityService.findById(aid);

        long uid = userTaskOrg.getUserId();
        long shopId = userTaskOrg.getShopId();

        //免单，助力/赠品 需要创建订单，对用户余额操作，并将中奖信息入库
        //创建订单
        if (type == UserTaskTypeEnum.ORDER_BACK_START_V2.getCode() || type == UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode() || type == UserTaskTypeEnum.ASSIST_START_V2.getCode()) {
            UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(uid, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.PALTFORMRECHARGE.getCode(),
                    null, "中奖任务红包", activityDB.getMerchantId(), shopId, OrderStatusEnum.ORDERED.getCode(), activityDB.getPayPrice()
                    , IpUtil.getServerIp(), null, "中奖任务红包", new Date(), null, null, bizTypeEnum.getCode(), aid);
            this.upayWxOrderService.create(upayWxOrder);
            // 对用户余额进行操作
            upayBalanceService.updateOrCreate(activityDB.getMerchantId(), shopId, uid, activityDB.getPayPrice());
            // 中奖信息入库user_coupon表
            List<Long> amountUserList = new ArrayList<>();
            amountUserList.add(uid);

            boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT + aid + ":" + uid);
            if (!success) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "点击太快，请稍后再试");
            }
            success = userCouponService.saveAmountUsersFromActivity(activityDB, amountUserList);
            if (!success) {
                log.error("插入user_coupon表失败");
                return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "插入user_coupon表失败");
            }
            Integer defRows = activityDefService.reduceDeposit(activityDef.getId(), activityDef.getGoodsPrice(), 0);
            if (defRows == 0) {
                // 扣除奖金失败，抛异常
                log.error("扣除def表保证金失败");
                return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "插入user_coupon表失败");
            }
        }

        redisService.deleteUserTaskCommitAndAudit(userTask.getId());
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT + aid + ":" + uid);

        return new SimpleResponse();
    }
}
