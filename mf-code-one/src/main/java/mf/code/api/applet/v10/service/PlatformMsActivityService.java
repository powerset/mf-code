package mf.code.api.applet.v10.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v10.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月16日 08:49
 */
public interface PlatformMsActivityService {

    /***
     * 获取秒杀活动的时间段轴
     * @param shopId
     * @param userId
     * @param dayId 第几天的对应编号
     * @return
     */
    SimpleResponse getMsActivityTime(Long shopId, Long userId, Long dayId, Long batchId);

    /***
     * 获取秒杀活动某个时间段 商品详情信息
     *
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse getMsActivityOneBatch(Long userId, Long shopId, Long dayId, Long batchId, int offset, int size);
}
