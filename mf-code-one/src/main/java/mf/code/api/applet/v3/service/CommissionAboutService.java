package mf.code.api.applet.v3.service;

import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;

import java.math.BigDecimal;

/**
 * mf.code.api.applet.v3.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月20日 14:11
 */
public interface CommissionAboutService {
    /****
     * 完成佣金任务-完成任务的存储
     * @param userPubJoin
     * @param commissionBO
     * @param commissionDisDto
     * @param merchantShop
     */
    void finishCheckPointNew(UserCoupon userCoupon, ActivityDef activityDef, UserPubJoin userPubJoin, CommissionBO commissionBO, CommissionDisDto commissionDisDto, MerchantShop merchantShop);

    /***
     * 完成任务的reids-佣金任务
     * @param commissionDisDto
     * @param commissionBO
     */
    void updateCheckPointRedisDataNew(ActivityDef activityDef, CommissionDisDto commissionDisDto, CommissionBO commissionBO);

    /***
     * 用户完成任务的存储
     * @param self
     * @param userId
     * @param commission
     * @param tax
     */
    void saveTodayProfitRedis(boolean self, Long userId, BigDecimal commission, BigDecimal tax);

    /***
     * 非任务 活动的佣金分配
     * @param activityDef
     * @param commissionBO
     * @param commissionDisDto
     * @param fromUser
     */
    void finishNotTaskDefType(ActivityDef activityDef, CommissionDisDto commissionDisDto, CommissionBO commissionBO, User fromUser);

    /***
     * 提现的弹窗相关
     * @param activityDefId
     * @param userId
     */
    void canCashDialogRedisData(Long activityDefId, Long userId);

    /***
     * 完成任务时 的金额去向 存储
     * @param self
     * @param user
     * @param amount
     * @param merchantShop
     */
    void saveUpayWxOrderAndUpdateBalance(ActivityDef activityDef, boolean self, User fromUser, User user, BigDecimal amount, MerchantShop merchantShop);
}
