package mf.code.api.applet.v6.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v6.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-05-14 09:51
 */
public interface OpenRedPackageService {

	/**
	 * 用户进入首页或详情页 获取 拆红包弹框类型
	 * @param shopId
	 * @param userId
	 * @return
	 * <per>
	 *     0. 什么都不弹
	 *     1. 弹出 帮好友拆了1.2元，快去找好友帮拆你的红包吧 -- 活动详情页
	 *     2. 弹出 拆红包窗口 -- 活动列表
	 *     3. 弹出 红包已被抢光了，下次早点来！ -- 活动详情页
	 *     4. 弹出 任务失败，再来挑战一次 -- 活动详情页
	 *     5. 弹出 你已有一个红包啦，不要贪心哦 -- 活动详情页
	 *     6. 弹出 恭喜您！已拆得所有现金红包 -- 活动详情页
	 *     7. 弹出 恭喜您！好友帮你拆出了红包 -- 活动详情页
	 *     8. 弹出 分享给你一个5元红包，立即来拆开吧 -- 活动详情页
	 *     9. 弹出 完成下单即可提现现金 -- 提现
	 *     10.弹出 完成支付即可提现现金 -- 提现
	 *     11.弹出 您的钱包中有5元可提现 还有10元待解锁 -- 提现
	 *     12.弹出 你有10元红包待拆开 还差2个好友就能拆开啦 -- 首页和商品详情页
	 *     13.弹出 你有10元红包待提现 -- 首页和商品详情页
	 * </per>>
	 */
	SimpleResponse openRedPackagePopup(Long shopId, Long userId, Integer scenes);

	/**
	 * 2.您有一个红包待拆开； 14.恭喜成功提现10元，再送你10元红包 -转2状态
	 * 创建拆红包
	 *
	 * @param activityDefId
	 * @param userId
	 * @return
	 */
	SimpleResponse createOpenRedPackage(Long activityDefId, Long userId);

	/**
	 * 8.分享给你一个5元红包，立即来拆开吧
	 * 帮拆红包
	 * @param activityId
	 * @param userId
	 * @return
	 */
	SimpleResponse helpOpenRedPackage(Long activityId, Long userId);
}
