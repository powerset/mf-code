package mf.code.api.applet.service.UserMoneyTradeDetail.impl.pay;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyIndexNameEnum;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailService;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 19:21
 */
@Service
@Slf4j
public class userPayBecomeShopmanagerTradeDetailConverterImpl extends UserMoneyTradeDetailService {
    @Override
    public Map<String, Object> getUserMoneyTradeDetail(UpayWxOrder upayWxOrder,
                                                       Integer payType,
                                                       Map<Long, Activity> activityMap,
                                                       Map<Long, Goods> goodsMap,
                                                       CommonDict commonDict) {
        //PALTFORMRECHARGE：平台充值 USERCASH：用户提现 USERPAY：用户支付 PALTFORMREFUND：平台退款
        boolean nullPayType = payType == null;
        boolean usercashType = OrderTypeEnum.USERPAY.getCode() == upayWxOrder.getType();
        boolean bizType  = BizTypeEnum.SHOP_MANAGER_PAY.getCode() == upayWxOrder.getBizType();
        if (nullPayType && usercashType && bizType) {
            Map<String, Object> map = new HashMap<>();
            map.put("title", "升级成为店长");
            map.put("type", UserMoneyIndexNameEnum.LEVEL_UP_SHOPMANAGER.getCode());
            map.put("indexName", UserMoneyIndexNameEnum.LEVEL_UP_SHOPMANAGER.getMessage());
            String plus_minus = "-";
            map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());//直接删除多余的小数位，如2.357会变成2.35
            map.put("time", "");
            if (upayWxOrder.getPaymentTime() != null) {
                map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
            }
            return map;
        }
        return null;
    }
}
