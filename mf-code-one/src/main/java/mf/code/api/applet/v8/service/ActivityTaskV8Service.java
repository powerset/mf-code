package mf.code.api.applet.v8.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月21日 11:24
 */
public interface ActivityTaskV8Service {
    /***
     * 完成活动任务型-业务
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param userId
     * @return
     */
    SimpleResponse finishActivityTask(Long merchantId, Long shopId, Long activityDefId, Long userId, String kws);

    /***
     * 获取群码
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param userId
     * @return
     */
    SimpleResponse getGroupCode(Long merchantId, Long shopId, Long activityDefId, Long userId);
}
