package mf.code.api.applet.v8;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceV8Service;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.v8
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 16:43
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/applet/v8/checkpointTaskSpace")
public class CheckpointTaskSpaceV8Api {
    @Autowired
    private CheckpointTaskSpaceV8Service checkpointTaskSpaceV8Service;

    /***
     * 任务空间列表改版----赚收益页面
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @RequestMapping(path = "/query", method = RequestMethod.GET)
    public SimpleResponse queryCheckpointTaskSpace(@RequestParam(name = "merchantId") Long merchantId,
                                                   @RequestParam(name = "shopId") Long shopId,
                                                   @RequestParam(name = "userId") Long userId) {
        return this.checkpointTaskSpaceV8Service.queryCheckpointTaskSpace(merchantId, shopId, userId);
    }
}
