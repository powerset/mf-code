package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.log4j.Log4j2;
import mf.code.activity.constant.*;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;
import mf.code.activity.domain.applet.aggregateroot.JoinRuleActivity;
import mf.code.activity.domain.applet.aggregateroot.UserJoinEvent;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.domain.applet.repository.JoinRuleActivityRepository;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityOrder;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityOrderService;
import mf.code.activity.service.ActivityPlanService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.service.SendTemplateMessageService;
import mf.code.api.applet.utils.AppletValidator;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.common.caller.luckcrm.LuckCrmCaller;
import mf.code.common.constant.*;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.common.verify.TaobaoVerifyService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.constant.LuckyWheelEnum;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UserConstant;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.uactivity.service.impl;
 * Description:
 *
 * @author 百川;
 * @date 2018/10/26 20:17
 */
@Service
@Log4j2
public class AppletUserActivityServiceImpl implements AppletUserActivityService {
    @Autowired
    private TaobaoVerifyService taobaoVerifyService;
    @Autowired
    private AppletActivityDefRepository appletActivityDefRepository;
    @Autowired
    private AppletActivityRepository appletActivityRepository;
    @Autowired
    private JoinRuleActivityRepository joinRuleActivityRepository;
    @Autowired
    private AppletUserRepository appletUserRepository;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserService userService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityOrderService activityOrderService;
    @Autowired
    private LuckCrmCaller luckCrmCaller;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private ActivityPlanService activityPlanService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private SendTemplateMessageService sendTemplateMessageService;
    @Autowired
    private CommissionService commissionService;
    // 超时重抽的次数限制
    @Value("${activity.winPrizeTimes}")
    private Integer winPrizeTimes;
    // 免单抽奖活动，发起者获得10个抽奖码
    @Value("${activity.freeOrderGetAwardCodeNumber}")
    private Integer freeOrderGetAwardCodeNumber;
    // 幸运大轮盘 默认机会
    @Value("${activity.luckyWheel.initVal}")
    private Integer initVal;
    // 幸运大轮盘 基础步长
    @Value("${activity.luckyWheel.initCount}")
    private Integer initCount;
    // 幸运大轮盘 上限机会
    @Value("${activity.luckyWheel.max}")
    private Integer max;
    // 同一用户活动认领次数
    private Integer activityReceiveNum = 3;

    /**
     * 用户参与或召唤好友
     */
    @Override
    public SimpleResponse joinActivity(String userIdstr, String activityIdstr, String pubUserIdstr) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        // 参数有效性校验
        if (StringUtils.isBlank(userIdstr) || StringUtils.isBlank(activityIdstr)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        if (StringUtils.equals(userIdstr, pubUserIdstr)) {
            pubUserIdstr = null;
        }
        Long uid = Long.valueOf(userIdstr);
        Long aid = Long.valueOf(activityIdstr);
        // 防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + aid + ":" + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 获取活动信息
        Activity activityDB = activityService.findById(aid);
        if (activityDB == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            return response;
        }
        Long mid = activityDB.getMerchantId();
        Long shopId = activityDB.getShopId();
        Long activityDefId = activityDB.getActivityDefId();
        Long activityPlanId = activityDB.getActivityPlanId();
        Long condPersionCount = Long.valueOf(activityDB.getCondPersionCount());
        Long goodsId = activityDB.getGoodsId();
        Integer hitsPerDraw = activityDB.getHitsPerDraw();
        String merchantCouponJson = activityDB.getMerchantCouponJson();
        Date startTime = activityDB.getStartTime();
        Date endTime = activityDB.getEndTime();
        Integer status = activityDB.getStatus();
        Integer type = activityDB.getType();
        Long activityLeader = activityDB.getUserId();

        int condDrawTime = (int) ((endTime.getTime() - startTime.getTime()) / 1000 / 60);


        // 当活动类型是 新人有礼发起者 或者 免单商品抽奖发起者， 并且如果参与进来的是活动发起者本人 则 返回 『不可参加自己发起的活动』
        if ((type == ActivityTypeEnum.NEW_MAN_START.getCode() || type == ActivityTypeEnum.ORDER_BACK_START.getCode() || type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode())
                && uid.equals(activityLeader)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO21);
            return response;
        }
        // 当活动类型是 免单商品抽奖发起者， 那么参与进来的用户类型应该修改为 免单商品抽奖参与者
        if (type == ActivityTypeEnum.ORDER_BACK_START.getCode()) {
            type = ActivityTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            type = ActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode();
        }
        // 当活动类型是 新人有礼发起者 ，那么参与进来的用户类型应该修改为 新人有礼参与者
        if (type == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            type = ActivityTypeEnum.NEW_MAN_JOIN.getCode();
        }
        // 获取关键字列表
        List<String> keyWordsList = JSONObject.parseArray(activityDB.getKeyWordsJson(), String.class);
        // 当没有提供关键词列表，则自己初始化
        if (CollectionUtils.isEmpty(keyWordsList)) {
            keyWordsList = new ArrayList<>();
            keyWordsList.add("");
        }
        int keyWordsListSize = keyWordsList.size();
        // 当活动未开始，则返回 『活动未开始，请稍后再来』
        if (startTime.getTime() > System.currentTimeMillis()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            return response;
        }
        // 当活动未开奖已结束 或者 强制下线 或者 已结束， 则返回 『活动已结束，请参加其他活动』

        Date awardStartTime = activityDB.getAwardStartTime();
        boolean isFailureEnd = null == awardStartTime && endTime.getTime() < System.currentTimeMillis();
        if (isFailureEnd || status == ActivityStatusEnum.DEL.getCode() || status == ActivityStatusEnum.END.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("活动已开奖，请参加其他活动");
            return response;
        }
        if (awardStartTime != null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("活动已开奖，请参加其他活动");
            return response;
        }
        // 配置优惠券
        Long couponId = null;
        Date couponStartTime = null;
        Date couponEndTime = null;
        if (StringUtils.isNotBlank(merchantCouponJson)) {
            JSONArray couponJsons = JSONObject.parseArray(merchantCouponJson);
            int size = couponJsons.size();
            MerchantShopCoupon merchantShopCoupon = null;
            if (size > 0) {
                int index = new Random().nextInt(size);
                if (StringUtils.isNotBlank(couponJsons.get(index).toString())) {
                    couponId = Long.parseLong(couponJsons.get(index).toString());
                    // 查询优惠券具体信息
                    merchantShopCoupon = merchantShopCouponService.selectByPriKey(couponId);
                    couponStartTime = merchantShopCoupon.getDisplayStartTime();
                    couponEndTime = merchantShopCoupon.getDisplayEndTime();
                }
            }
        }
        // 判断用户是否已经参与了此活动，如果参与了，返回response
        if (stringRedisTemplate.opsForHash().hasKey(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString())) {
            // 容错处理
            Map<String, Object> userActivityParam = new HashMap<>();
            userActivityParam.put("activityId", aid);
            userActivityParam.put("userId", uid);
            List<UserActivity> userActivities = userActivityService.pageListUserActivity(userActivityParam);
            if (!CollectionUtils.isEmpty(userActivities)) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
                return response;
            }
            stringRedisTemplate.opsForHash().delete(RedisKeyConstant.ACTIVITY_USER_COUNT + aid);
        }
        // 获取用户信息
        User user = userService.selectByPrimaryKey(uid);
        if (user == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            return response;
        }
        // 查看数据库是否有此用户，如果有则更新，如果没有则插入
        UserActivity userActivity;
        String userActivityRedis = stringRedisTemplate.opsForValue().get(RedisKeyConstant.NO_QUALIFICATION + aid + ":" + uid);
        if (StringUtils.isNotBlank(userActivityRedis)) {
            userActivity = JSON.parseObject(userActivityRedis, UserActivity.class);
        } else {
            // 用户活动数据入库
            userActivity = userActivityService
                    .insertUserActivity(uid, mid, shopId, aid, type, UserActivityStatusEnum.NO_QUALIFICATION.getCode());
            if (userActivity == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                return response;
            }
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.NO_QUALIFICATION + aid + ":" + uid,
                    JSON.toJSONString(userActivity), 1, TimeUnit.MINUTES);
        }

        // 活动计数
        Long rtn = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + aid, condDrawTime, TimeUnit.MINUTES);
        if (rtn == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR);
            return response;
        }

        if (rtn > condPersionCount) {
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("抱歉，该活动已开奖，可参加其它活动");
            return response;
        }
        // 加工用户邀请列表的用户信息 放入redis
        JSONObject userJsonObject = processActivityJoinUserInfo(user);

        if (StringUtils.isNotBlank(pubUserIdstr)) {
            Long pubUid = Long.valueOf(pubUserIdstr);
            // 该邀请人未参加此活动  处理
            Object pubUidObj = stringRedisTemplate.opsForHash().get(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, pubUid.toString());
            if (pubUidObj == null) {
                pubUserIdstr = null;
            }
        }


        // 判断判断pubUid是否存在
        if (StringUtils.isBlank(pubUserIdstr)) {
            // 更新redis抽奖概率
            stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString(), 1);
        } else {
            if (RegexUtils.StringIsNumber(pubUserIdstr)) {
                // 用户邀请记录入库
                UserPubJoin userPubJoin = userPubJoinService.selectByUserAndSubUidAndAid(Long.valueOf(pubUserIdstr), uid, aid);
                if (userPubJoin == null) {
                    userPubJoinService.userPubJoin(Long.valueOf(pubUserIdstr), activityDB, user);
                }
                int rows = userPubJoinService.updateActivityJoinBySubUidAndAid(uid, aid);
                if (rows == 0) {
                    stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
                    response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                    response.setMessage("数据库异常：更新用户邀请记录失败，没有此用户任务的被邀请记录");
                    return response;
                }
                // redis更新用户邀请列表 LPUSH
                stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + aid + ":" + pubUserIdstr, userJsonObject.toJSONString());
                // 更新redis抽奖概率
                stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, pubUserIdstr, 1);
                stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString(), 1);
            }
        }
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, condDrawTime, TimeUnit.MINUTES);
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + aid, userJsonObject.toJSONString());
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + aid, condDrawTime, TimeUnit.MINUTES);
        // uid, aid更新用户活动状态user_activity表
        userActivity.setStatus(UserActivityStatusEnum.NO_WIN.getCode());
        Date now = new Date();
        userActivity.setUtime(now);
        int rows = userActivityService.updateByPrimaryKey(userActivity);
        // 删除没资格缓存
        stringRedisTemplate.delete(RedisKeyConstant.NO_QUALIFICATION + aid + ":" + uid);
        if (rows == 0) {
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        // 判断是否触发开奖流程
        if (rtn.equals(condPersionCount)) {
            // 更新活动开奖时间
            Activity activity = new Activity();
            activity.setId(aid);
            activity.setAwardStartTime(now);
            activityService.update(activity);
            // 判断此活动是否是新人有礼发起, 新人本人的任务入库
            // 如果 此用户 有发起过此活动，并还在进行中， 则返回
            // status = 1  活动结束时间 > now; 拦截
            // 中奖 但任务未完成
            // status = 2  CTime+任务时间 > now; 拦截
            // 中奖 但任务已完成
            // status = 2  Ctime+任务时间 < now; 拦截
            if (activityDB.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
                userTaskService.insertUserTask(mid, shopId, aid, activityDB.getUserId(), UserTaskStatusEnum.INIT.getCode(),
                        ActivityTypeEnum.NEW_MAN_START.getCode(), keyWordsList.get(0), goodsId, null);
                userActivityService.updateStatusByUserIdAndActivityId(activityDB.getUserId(), aid, UserActivityStatusEnum.WINNER.getCode());
            }
            // 获取中奖名单 放入redis缓存
            ArrayList<Long> winner = getWinner(aid, condPersionCount, hitsPerDraw, condDrawTime);
            int keyWordsIndex = 0;
            for (int i = 0; i < hitsPerDraw; i++) {
                Long userId = winner.get(i);
                // 获取关键词
                if (i % keyWordsListSize == 0) {
                    keyWordsIndex = 0;
                }
                String keyWords = keyWordsList.get(keyWordsIndex);
                keyWordsIndex++;
                // 中奖任务开始，用户信息入库
                UserTask userTask = userTaskService.insertUserTask(mid, shopId, aid, userId,
                        UserTaskStatusEnum.INIT.getCode(), userActivity.getType(), keyWords, goodsId, null);
                // 更新中奖用户 中奖状态
                userActivityService.updateWinnerStatusByUserIdAndActivityId(userId, aid, UserActivityStatusEnum.WINNER.getCode());
            }

            if (couponId != null) {
                // 获取优惠券的用户
                Set<String> couponUsers = getCouponUser(aid, shopId, winner);
                List<Long> couponUserList = new ArrayList<>();
                for (String couponUser : couponUsers) {
                    stringRedisTemplate.opsForSet().add(RedisKeyConstant.SHOP_COUPON_USER_OWNER + shopId, couponUser);
                    couponUserList.add(Long.valueOf(couponUser));
                }
                if (!CollectionUtils.isEmpty(couponUserList)) {
                    userActivityService.batchUpdateCouponUserStatus(couponUserList, aid, couponId, couponStartTime, couponEndTime);
                    // 存入user_coupone表
                    boolean save = userCouponService.saveCouponUsersFromActivity(activity, couponUserList);
                    if (!save) {
                        log.error("AppletUserActivityServiceImpl_joinActivityV12_userCoupon表插入失败");
                    }
                }
            }

            // 获取未中奖红包的用户
            Set<String> taskCollectCartUsers = getTaskCollectCartUser(activityPlanId, activityDefId, aid, winner);
            List<Long> taskCollectCartUserList = new ArrayList<>();
            for (String taskCollectCartUser : taskCollectCartUsers) {
                stringRedisTemplate.opsForSet().add(RedisKeyConstant.ACTIVITY_PLAN_DEF_TASK_OWNER + activityPlanId + ":" + activityDefId, taskCollectCartUser);
                taskCollectCartUserList.add(Long.valueOf(taskCollectCartUser));
            }
            if (!CollectionUtils.isEmpty(taskCollectCartUserList)) {
                for (int i = 0; i < taskCollectCartUserList.size(); i++) {
                    UserTask userTask;
                    if (type == ActivityTypeEnum.MCH_PLAN.getCode()) {
                        // 计划内
                        int activityPlanRows = activityPlanService.reduceRedpackStock(activityPlanId);
                        if (activityPlanRows == 0) {
                            break;
                        }
                        userTask = new UserTask();
                        userTask.setGoodsId(goodsId);
                    } else {
                        // 计划外
                        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
                        int activityDefRows = activityDefService.reduceRedpackStock(activityDefId, activityDef.getTaskRpAmount());
                        if (activityDefRows == 0) {
                            break;
                        }
                        userTask = new UserTask();
                        userTask.setGoodsId(activityDef.getGoodsId());
                        userTask.setActivityDefId(activityDef.getId());
                    }

                    // 修改userActivity 表的 用户中奖记录
                    Long userId = taskCollectCartUserList.get(i);

                    // 插入userTask表的 任务记录
                    userTask.setType(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
                    userTask.setActivityId(aid);

                    userTask.setUserId(userId);
                    userTask.setMerchantId(mid);
                    userTask.setShopId(shopId);
                    // 获取关键词
                    if (i % keyWordsListSize == 0) {
                        keyWordsIndex = 0;
                    }
                    String keyWords = keyWordsList.get(keyWordsIndex);
                    keyWordsIndex++;
                    userTask.setKeyWords(keyWords);
                    userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
                    userTask.setCtime(now);
                    userTask.setUtime(now);
                    userTaskService.insertUserTask(userTask);
                }
            }
            //开奖推送消息
            this.sendTemplateMessageService.sendTemplateMsg2Award(shopId, aid, null);
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + aid + ":" + uid);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultDate = new HashMap<>();
        resultDate.put("joinStatus", 1);
        resultDate.put("awardNO", userJsonObject.getJSONArray("awardNo"));
        response.setData(resultDate);
        return response;
    }

    private SimpleResponse kickBad(String userIdstr, String hasShared) {
        String dateKey = DateUtil.getYYMMDD();
        String redisFKey = mf.code.user.repo.redis.RedisKeyConstant.USER_ATTENT_TIME_LIMIT + dateKey + ":" + userIdstr;
        //redis记录的参与次数
        String timesJoinStr = stringRedisTemplate.opsForValue().get(redisFKey);
        Integer timesJoin = 0;
        if (timesJoinStr != null) {
            timesJoin = Integer.parseInt(timesJoinStr);
        }
        Integer timeDef = 3;
        // redis设置的可参与次数
        String timeDefStr = stringRedisTemplate.opsForValue().get(mf.code.user.repo.redis.RedisKeyConstant.SETTING_ATTENT_TIME_LIMIT);
        if (timeDefStr != null) {
            timeDef = Integer.parseInt(timeDefStr);
        }
        if (timesJoin >= timeDef) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7000);
        }
        if (timesJoin == timeDef - 1 && !"shared".equals(hasShared)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7001);
        }
        //防并发
        if (!RedisForbidRepeat.NX(stringRedisTemplate, mf.code.user.repo.redis.RedisKeyConstant.USER_ATTACK_ASS_LIMIT + dateKey + ":" + userIdstr)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10);
        }
        return null;
    }

    @Override
    public SimpleResponse joinActivityV12(String userIdstr, String activityIdstr, String pubUserIdstr, String hasShared) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        // 参数有效性校验
        if (StringUtils.isBlank(userIdstr) || StringUtils.isBlank(activityIdstr)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        if (StringUtils.equals(userIdstr, pubUserIdstr)) {
            pubUserIdstr = null;
        }
        Long uid = Long.valueOf(userIdstr);
        Long aid = Long.valueOf(activityIdstr);
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + aid + ":" + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }

        response = kickBad(userIdstr, hasShared);
        if (response != null) {
            return response;
        } else {
            response = new SimpleResponse<>();
        }

        // 获取活动信息
        Activity activity = activityService.findById(aid);
        if (activity == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            return response;
        }
        Long mid = activity.getMerchantId();
        Long shopId = activity.getShopId();
        Long activityDefId = activity.getActivityDefId();
        Long activityPlanId = activity.getActivityPlanId();
        Long condPersionCount = Long.valueOf(activity.getCondPersionCount());
        Long goodsId = activity.getGoodsId();
        Integer hitsPerDraw = activity.getHitsPerDraw();
        String merchantCouponJson = activity.getMerchantCouponJson();
        Date startTime = activity.getStartTime();
        Date endTime = activity.getEndTime();
        Integer status = activity.getStatus();
        Integer type = activity.getType();
        Long activityLeader = activity.getUserId();

        int condDrawTime = (int) ((endTime.getTime() - startTime.getTime()) / 1000 / 60);
        condDrawTime = condDrawTime < 1440 ? 1440 : condDrawTime;


        // 当活动类型是 新人有礼发起者 或者 免单商品抽奖发起者， 并且如果参与进来的是活动发起者本人 则 返回 『不可参加自己发起的活动』
        if ((type == ActivityTypeEnum.NEW_MAN_START.getCode() || type == ActivityTypeEnum.ORDER_BACK_START.getCode()
                || type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode())
                && uid.equals(activityLeader)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO21);
            return response;
        }
        // 当活动类型是 免单商品抽奖发起者， 那么参与进来的用户类型应该修改为 免单商品抽奖参与者
        if (type == ActivityTypeEnum.ORDER_BACK_START.getCode()) {
            type = UserActivityTypeEnum.ORDER_BACK_JOIN.getCode();
        }
        if (type == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            type = UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode();
        }
        // 当活动类型是 新人有礼发起者 ，那么参与进来的用户类型应该修改为 新人有礼参与者
        if (type == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            type = UserActivityTypeEnum.NEW_MAN_JOIN.getCode();
        }
        // 获取关键字列表
        List<String> keyWordsList = JSONObject.parseArray(activity.getKeyWordsJson(), String.class);
        // 当没有提供关键词列表，则自己初始化
        if (CollectionUtils.isEmpty(keyWordsList)) {
            keyWordsList = new ArrayList<>();
            keyWordsList.add("");
        }
        int keyWordsListSize = keyWordsList.size();
        // 当活动未开始，则返回 『活动未开始，请稍后再来』
        if (startTime.getTime() > System.currentTimeMillis()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            return response;
        }
        // 当活动未开奖已结束 或者 强制下线 或者 已结束， 则返回 『活动已结束，请参加其他活动』

        Date awardStartTime = activity.getAwardStartTime();
        boolean isFailureEnd = null == awardStartTime && endTime.getTime() < System.currentTimeMillis();
        if (isFailureEnd || status == ActivityStatusEnum.DEL.getCode() || status == ActivityStatusEnum.END.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("活动已开奖，请参加其他活动");
            return response;
        }
        if (awardStartTime != null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("活动已开奖，请参加其他活动");
            return response;
        }
        // 配置优惠券
        Long couponId = null;
        Date couponStartTime = null;
        Date couponEndTime = null;
        if (StringUtils.isNotBlank(merchantCouponJson)) {
            JSONArray couponJsons = JSONObject.parseArray(merchantCouponJson);
            int size = couponJsons.size();
            MerchantShopCoupon merchantShopCoupon = null;
            if (size > 0) {
                int index = new Random().nextInt(size);
                if (StringUtils.isNotBlank(couponJsons.get(index).toString())) {
                    couponId = Long.parseLong(couponJsons.get(index).toString());
                    // 查询优惠券具体信息
                    merchantShopCoupon = merchantShopCouponService.selectByPriKey(couponId);
                    couponStartTime = merchantShopCoupon.getDisplayStartTime();
                    couponEndTime = merchantShopCoupon.getDisplayEndTime();
                }
            }
        }
        // 判断用户是否已经参与了此活动，如果参与了，返回response
        if (stringRedisTemplate.opsForHash().hasKey(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString())) {
            // 容错处理
            Map<String, Object> userActivityParam = new HashMap<>();
            userActivityParam.put("activityId", aid);
            userActivityParam.put("userId", uid);
            List<UserActivity> userActivities = userActivityService.pageListUserActivity(userActivityParam);
            if (!CollectionUtils.isEmpty(userActivities)) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
                return response;
            }
            stringRedisTemplate.opsForHash().delete(RedisKeyConstant.ACTIVITY_USER_COUNT + aid);
        }


        // 获取用户信息
        User user = userService.selectByPrimaryKey(uid);
        if (user == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            return response;
        }
        // 查看数据库是否有此用户，如果有则更新，如果没有则插入
        UserActivity userActivity;
        String userActivityRedis = stringRedisTemplate.opsForValue().get(RedisKeyConstant.NO_QUALIFICATION + aid + ":" + uid);
        if (StringUtils.isNotBlank(userActivityRedis)) {
            userActivity = JSON.parseObject(userActivityRedis, UserActivity.class);
        } else {
            // 用户活动数据入库
            userActivity = userActivityService
                    .insertUserActivity(uid, mid, shopId, aid, type, UserActivityStatusEnum.NO_QUALIFICATION.getCode());
            if (userActivity == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                return response;
            }
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.NO_QUALIFICATION + aid + ":" + uid,
                    JSON.toJSONString(userActivity), 1, TimeUnit.MINUTES);
        }

        // 活动计数
        Long rtn = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + aid, condDrawTime, TimeUnit.MINUTES);
        if (rtn == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR);
            return response;
        }

        if (rtn > condPersionCount) {
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("抱歉，该活动已开奖，可参加其它活动");
            return response;
        }
        // 加工用户邀请列表的用户信息 放入redis
        JSONObject userJsonObject = processActivityJoinUserInfo(user);

        if (StringUtils.isNotBlank(pubUserIdstr)) {
            Long pubUid = Long.valueOf(pubUserIdstr);
            // 该邀请人未参加此活动  处理
            Object pubUidObj = stringRedisTemplate.opsForHash().get(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, pubUid.toString());
            if (pubUidObj == null) {
                pubUserIdstr = null;
            }
        }


        // 判断判断pubUid是否存在
        if (StringUtils.isBlank(pubUserIdstr)) {
            // 更新redis抽奖概率
            stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString(), 1);
        } else {
            if (RegexUtils.StringIsNumber(pubUserIdstr)) {
                // 用户邀请记录入库
                UserPubJoin userPubJoin = userPubJoinService.selectByUserAndSubUidAndAid(Long.valueOf(pubUserIdstr), uid, aid);
                if (userPubJoin == null) {
                    userPubJoinService.userPubJoin(Long.valueOf(pubUserIdstr), activity, user);
                }
                int rows = userPubJoinService.updateActivityJoinBySubUidAndAid(uid, aid);
                if (rows == 0) {
                    stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
                    response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                    response.setMessage("数据库异常：更新用户邀请记录失败，没有此用户任务的被邀请记录");
                    return response;
                }
                // redis更新用户邀请列表 LPUSH
                stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_USER_JOINLIST + aid + ":" + pubUserIdstr, userJsonObject.toJSONString());
                // 更新redis抽奖概率
                stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, pubUserIdstr, 1);
                stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString(), 1);
            }
        }
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, condDrawTime, TimeUnit.MINUTES);
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + aid, userJsonObject.toJSONString());
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + aid, condDrawTime, TimeUnit.MINUTES);
        // uid, aid更新用户活动状态user_activity表
        userActivity.setStatus(UserActivityStatusEnum.NO_WIN.getCode());
        Date now = new Date();
        userActivity.setUtime(now);
        int rows = userActivityService.updateByPrimaryKey(userActivity);
        // 删除没资格缓存
        stringRedisTemplate.delete(RedisKeyConstant.NO_QUALIFICATION + aid + ":" + uid);
        if (rows == 0) {
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        // 判断是否触发开奖流程
        if (rtn.equals(condPersionCount)) {
            // 更新活动开奖时间
            activity.setId(aid);
            activity.setAwardStartTime(now);
            activityService.update(activity);
            // 判断此活动是否是新人有礼发起, 新人本人的任务入库
            // 如果 此用户 有发起过此活动，并还在进行中， 则返回
            // status = 1  活动结束时间 > now; 拦截
            // 中奖 但任务未完成
            // status = 2  CTime+任务时间 > now; 拦截
            // 中奖 但任务已完成
            // status = 2  Ctime+任务时间 < now; 拦截
            if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
                userTaskService.insertUserTask(mid, shopId, aid, activity.getUserId(), UserTaskStatusEnum.INIT.getCode(),
                        ActivityTypeEnum.NEW_MAN_START.getCode(), keyWordsList.get(0), goodsId, null);
                userActivityService.updateStatusByUserIdAndActivityId(activity.getUserId(), aid, UserActivityStatusEnum.WINNER.getCode());
            }
            // 获取中奖名单 放入redis缓存
            ArrayList<Long> winner = getWinner(aid, condPersionCount, hitsPerDraw, condDrawTime);
            int keyWordsIndex = 0;
            for (int i = 0; i < hitsPerDraw; i++) {
                Long userId = winner.get(i);
                // 获取关键词
                if (i % keyWordsListSize == 0) {
                    keyWordsIndex = 0;
                }
                String keyWords = keyWordsList.get(keyWordsIndex);
                keyWordsIndex++;
                Integer userTaskType = UserTaskTypeEnum.findByUserActivityType(userActivity.getType());
                // 中奖任务开始，用户信息入库
                UserTask userTask = userTaskService.insertUserTask(mid, shopId, aid, userId,
                        UserTaskStatusEnum.INIT.getCode(), userTaskType, keyWords, goodsId, null);
                // 更新中奖用户 中奖状态
                userActivityService.updateWinnerStatusByUserIdAndActivityId(userId, aid, UserActivityStatusEnum.WINNER.getCode());
            }

            if (couponId != null) {
                // 获取优惠券的用户
                Set<String> couponUsers = getCouponUser(aid, shopId, winner);
                List<Long> couponUserList = new ArrayList<>();
                for (String couponUser : couponUsers) {
                    stringRedisTemplate.opsForSet().add(RedisKeyConstant.SHOP_COUPON_USER_OWNER + shopId, couponUser);
                    couponUserList.add(Long.valueOf(couponUser));
                }
                if (!CollectionUtils.isEmpty(couponUserList)) {
                    userActivityService.batchUpdateCouponUserStatus(couponUserList, aid, couponId, couponStartTime, couponEndTime);

                    // 存入user_coupone表
                    boolean save = userCouponService.saveCouponUsersFromActivity(activity, couponUserList);
                    if (!save) {
                        log.error("AppletUserActivityServiceImpl_joinActivityV12_userCoupon表插入失败");
                    }
                }
            }
/*
            // 获取未中奖红包的用户
            Set<String> taskCollectCartUsers = getTaskCollectCartUser(activityPlanId, activityDefId, aid, winner);
            List<Long> taskCollectCartUserList = new ArrayList<>();
            for (String taskCollectCartUser : taskCollectCartUsers) {
                stringRedisTemplate.opsForSet().add(RedisKeyConstant.ACTIVITY_PLAN_DEF_TASK_OWNER + activityPlanId + ":" + activityDefId, taskCollectCartUser);
                taskCollectCartUserList.add(Long.valueOf(taskCollectCartUser));
            }
            if (!CollectionUtils.isEmpty(taskCollectCartUserList)) {
                for (int i = 0; i < taskCollectCartUserList.size(); i++) {
                    UserTask userTask;
                    if (type == ActivityTypeEnum.MCH_PLAN.getCode()) {
                        // 计划内
                        int activityPlanRows = activityPlanService.reduceRedpackStock(activityPlanId);
                        if (activityPlanRows == 0) {
                            break;
                        }
                        userTask = new UserTask();
                        userTask.setGoodsId(goodsId);
                    } else {
                        // 计划外
                        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
                        int activityDefRows = activityDefService.reduceRedpackStock(activityDefId, activityDef.getTaskRpAmount());
                        if (activityDefRows == 0) {
                            break;
                        }
                        userTask = new UserTask();
                        userTask.setGoodsId(activityDef.getGoodsId());
                    }

                    // 修改userActivity 表的 用户中奖记录
                    Long userId = taskCollectCartUserList.get(i);

                    // 插入userTask表的 任务记录
                    userTask.setType(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode());
                    userTask.setActivityId(aid);

                    userTask.setUserId(userId);
                    userTask.setMerchantId(mid);
                    userTask.setShopId(shopId);
                    // 获取关键词
                    if (i % keyWordsListSize == 0) {
                        keyWordsIndex = 0;
                    }
                    String keyWords = keyWordsList.get(keyWordsIndex);
                    keyWordsIndex++;
                    userTask.setKeyWords(keyWords);
                    userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
                    userTask.setCtime(now);
                    userTask.setUtime(now);
                    userTaskService.insertUserTask(userTask);
                }
            }
*/
            //开奖推送消息
            this.sendTemplateMessageService.sendTemplateMsg2Award(shopId, aid, null);
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + aid + ":" + uid);

        /**#########################################################################################
         *
         * 每天参与次数校验.这里处理7000和7001的情况
         *   <pre>
         *       第一次:        参与成功,   返回:0
         *       第二次:        参与成功,   返回:7002:还剩一次参与机会
         *       第三次:        参与不成功, 返回:7001:先分享
         *       第四次:        参与成功:   返回:0
         *       第五次及以上:  参与不成功. 返回:7000
         *   </pre>
         *   #########################################################################################
         */

        response = userService.timeLimit(uid, hasShared);
        Integer responseCode = response.getCode();
        Integer times = 1;
        // 7000:不能在参与   7001:先分享在参与
        if (responseCode == ApiStatusEnum.ERROR_BUS_NO7000.getCode()
                || responseCode == ApiStatusEnum.ERROR_BUS_NO7001.getCode()
                || responseCode == ApiStatusEnum.ERROR_BUS_NO10.getCode()) {
            return response;
        } else {
            Map map = (Map) response.getData();
            times = Integer.parseInt(map.get(UserConstant.responseTimesKey).toString());
        }
        Map<String, Object> resultDate = new HashMap<>();
        resultDate.put("joinStatus", 1);
        resultDate.put("awardNO", userJsonObject.getJSONArray("awardNo"));
        resultDate.put(UserConstant.responseTimesKey, times);
        response.setData(resultDate);
        return response;
    }

    @Override
    public SimpleResponse joinOrCreateAssist(String userId, String activityId, String activityDefId) {
        SimpleResponse<Object> response;

        // 入参校验
        response = AppletValidator.checkParams4AssistAndCreate(userId, activityId, activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }

        // 防重
        String raid = activityId;
        if (StringUtils.isNotBlank(activityDefId)) {
            raid = activityDefId;
        }
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }

        // 参数转化
        Long uid = Long.valueOf(userId);

        // 从列表 发起助力活动
        if (StringUtils.isNotBlank(activityDefId)) {
            response = createAssistFromDef(activityDefId, uid);
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
            return response;
        }

        // 参数转化
        Long aid = Long.valueOf(activityId);
        // 校验活动aid
        Activity activity = activityService.findById(aid);
        if (null == activity) {
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
            log.error("没有查询到此对应的activity表中数据信息，请检查activityId参数是否异常 to 诗琪 or 金纯");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("没有查询到此对应的activity表中数据信息，请检查activityId参数是否异常 to 诗琪 or 金纯");
            return response;
        }

        // 先助力 后 发起

        Long goodsId = activity.getGoodsId();
        // 校验是否有助力资格 并 处理助力逻辑
        response = assist4activity(uid, activity);

        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            // 没有助力成功，但可以发起自己的助力活动
            Long myActivityId = checkAbility2CreateAssist(uid, goodsId);
            if (myActivityId == null) {
                // 如果有资格 则发起活动
                response = createAssist(activity.getActivityDefId(), uid);
                if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                    stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);

                    // 库存不足，发送优惠券
                    if (response.getCode() == ApiStatusEnum.ERROR_BUS_NO3.getCode()) {
                        response = assist4coupon(uid, activity);
                        if (response.getCode() == ApiStatusEnum.ERROR_BUS_NO11.getCode()) {
                            return response;
                        }
                    }
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                    response.setMessage("活动库存不足");
                    return response;
                }
            }
            stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
            if (myActivityId != null) {
                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("activityId", myActivityId);
                response.setCode(ApiStatusEnum.SUCCESS.getCode());
                response.setData(resultVO);
                return response;
            }

            return response;
        }

        // 助力成功，校验是否有发起活动的资格
        Long myActivityId = checkAbility2CreateAssist(uid, goodsId);
        if (myActivityId == null) {
            // 如果有资格 则发起活动
            response = createAssist(activity.getActivityDefId(), uid);
            if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
                stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
                return response;
            }
        }

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + raid + ":" + userId);
        return response;
    }

    private SimpleResponse<Object> assist4couponList(List<Long> failedUserIds, List<Activity> activities) {
        Map<Long, Long> validActivityIdMap = new HashMap<>();
        for (Activity activity : activities) {
            validActivityIdMap.put(activity.getUserId(), activity.getId());
        }
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验商户是否有优惠券的资格
        String merchantCouponJson = activities.get(0).getMerchantCouponJson();
        if (StringUtils.isBlank(merchantCouponJson) || !JsonParseUtil.booJsonArr(merchantCouponJson)) {
            log.info("商户没有配置优惠券");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            response.setMessage("商户没有配置优惠券");
            return response;
        }

        // 获取基本参数
        List<Long> merchantCoupons = JSON.parseArray(merchantCouponJson, Long.class);
        Long merchantShopCouponId = merchantCoupons.get((int) (Math.random() * merchantCoupons.size()));

        // 有资格获取优惠券的用户
        QueryWrapper<UserActivity> userActivityWraper = new QueryWrapper<>();
        userActivityWraper.lambda()
                .eq(UserActivity::getMerchantShopCouponId, merchantShopCouponId)
                .eq(UserActivity::getStatus, UserActivityStatusEnum.COUPON.getCode())
                .in(UserActivity::getUserId, failedUserIds);
        List<UserActivity> userActivities = userActivityService.list(userActivityWraper);

        // 获取 优惠券用户
        if (CollectionUtils.isEmpty(userActivities)) {
            log.info("没有需要发优惠券的用户");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            response.setMessage("没有需要发优惠券的用户");
            return response;
        }

        for (UserActivity userActivity : userActivities) {
            if (validActivityIdMap.containsKey(userActivity.getUserId())) {
                validActivityIdMap.remove(userActivity.getUserId());
            }
        }

        List<Long> validActivitIds = new ArrayList<>(validActivityIdMap.values());

        // 获取可发送优惠券的活动列表
        QueryWrapper<UserActivity> uactivityWraper = new QueryWrapper<>();
        uactivityWraper.lambda()
                .eq(UserActivity::getStatus, UserActivityStatusEnum.NO_WIN.getCode())
                .in(UserActivity::getActivityId, validActivitIds);
        List<UserActivity> uactivitys = userActivityService.list(uactivityWraper);
        if (CollectionUtils.isEmpty(uactivitys)) {
            log.info("没有查到未中奖的助力活动(赶紧助力，获取更多优惠券吧)");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            response.setMessage("赶紧助力，获取更多优惠券吧");
            return response;
        }

        // 优惠券的具体参数
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponService.selectById(merchantShopCouponId);
        if (merchantShopCoupon == null) {
            log.error("merchant_shop_coupon表未查到此优惠券");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("优惠券不存在");
            return response;
        }
        Date displayStartTime = merchantShopCoupon.getDisplayStartTime();
        Date displayEndTime = merchantShopCoupon.getDisplayEndTime();
        BigDecimal amount = merchantShopCoupon.getAmount();
        String desc = merchantShopCoupon.getDesc();

        // 入库
        List<Long> couponUserList = new ArrayList<>();
        List<UserActivity> updateUserActivities = new ArrayList<>();
        for (UserActivity userActivity : userActivities) {
            userActivity.setMerchantShopCouponId(merchantShopCouponId);
            userActivity.setStatus(UserActivityStatusEnum.COUPON.getCode());
            userActivity.setCouponStartTime(displayStartTime);
            userActivity.setCouponEndTime(displayEndTime);
            userActivity.setUtime(new Date());
            updateUserActivities.add(userActivity);
            couponUserList.add(userActivity.getUserId());
        }

        boolean isSuccess = userActivityService.updateBatchById(updateUserActivities);
        if (!isSuccess) {
            log.error("更新user_activity表出错，优惠券发奖失败");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("更新user_activity表出错，优惠券发奖失败");
            return response;
        }

        boolean success = userCouponService.saveCouponUsersFromActivity(activities.get(0), couponUserList);
        if (!success) {
            log.error("更新user_coupon表出错");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("更新user_coupon表出错，优惠券发奖失败");
            return response;
        }

        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
        response.setMessage("获得优惠券");
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("displayStartTime", DateUtil.dateToString(displayStartTime, DateUtil.FORMAT_TWO));
        resultVO.put("displayEndTime", DateUtil.dateToString(displayEndTime, DateUtil.FORMAT_TWO));
        resultVO.put("amount", amount);
        resultVO.put("desc", desc);
        response.setData(resultVO);

        for (Long aid : validActivitIds) {
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_COUPONER + aid, JSONObject.toJSONString(resultVO));
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_COUPONER + aid, 7, TimeUnit.DAYS);
        }

        return response;
    }

    private SimpleResponse<Object> assist4coupon(Long uid, Activity activity) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验商户是否有优惠券的资格
        String merchantCouponJson = activity.getMerchantCouponJson();
        if (StringUtils.isBlank(merchantCouponJson) || !JsonParseUtil.booJsonArr(merchantCouponJson)) {
            log.info("商户没有配置优惠券");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            response.setMessage("商户没有配置优惠券");
            return response;
        }
        // 获取基本参数
        List<Long> merchantCoupons = JSON.parseArray(merchantCouponJson, Long.class);
        Long merchantShopCouponId = merchantCoupons.get((int) (Math.random() * merchantCoupons.size()));

        // 校验用户是否有获取优惠券的资格
        QueryWrapper<UserActivity> userActivityWraper = new QueryWrapper<>();
        userActivityWraper.eq("merchant_shop_coupon_id", merchantShopCouponId);
        userActivityWraper.eq("user_id", uid);
        userActivityWraper.eq("status", UserActivityStatusEnum.COUPON.getCode());
        List<UserActivity> userActivities = userActivityService.list(userActivityWraper);

        if (!CollectionUtils.isEmpty(userActivities)) {
            log.info("您已获取过此活动的优惠券，请到我的卡券中查看");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            response.setMessage("您已获取过此活动的优惠券，请到我的卡券中查看");
            return response;
        }

        // 为空 则发送优惠券
        // 获取可发送优惠券的活动列表
        QueryWrapper<UserActivity> uactivityWraper = new QueryWrapper<>();
        uactivityWraper.eq("shop_id", activity.getShopId());
        uactivityWraper.eq("user_id", uid);
        uactivityWraper.eq("status", UserActivityStatusEnum.NO_WIN.getCode());
        List<UserActivity> uactivitys = userActivityService.list(uactivityWraper);
        if (CollectionUtils.isEmpty(uactivitys)) {
            log.info("没有查到未中奖的助力活动(赶紧助力，获取更多优惠券吧)");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            response.setMessage("赶紧助力，获取更多优惠券吧");
            return response;
        }


        // 优惠券的具体参数
        MerchantShopCoupon merchantShopCoupon = merchantShopCouponService.selectById(merchantShopCouponId);
        if (merchantShopCoupon == null) {
            log.error("merchant_shop_coupon表未查到此优惠券");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("优惠券不存在");
            return response;
        }
        Date displayStartTime = merchantShopCoupon.getDisplayStartTime();
        Date displayEndTime = merchantShopCoupon.getDisplayEndTime();
        BigDecimal amount = merchantShopCoupon.getAmount();
        String desc = merchantShopCoupon.getDesc();

        // 入库
        UserActivity userActivity = uactivitys.get(0);
        userActivity.setMerchantShopCouponId(merchantShopCouponId);
        userActivity.setStatus(UserActivityStatusEnum.COUPON.getCode());
        userActivity.setCouponStartTime(displayStartTime);
        userActivity.setCouponEndTime(displayEndTime);
        userActivity.setUtime(new Date());
        boolean isSuccess = userActivityService.updateById(userActivity);
        if (!isSuccess) {
            log.error("更新user_activity表出错，优惠券发奖失败");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("更新user_activity表出错，优惠券发奖失败");
            return response;
        }
        List<Long> couponUserList = new ArrayList<>();
        couponUserList.add(uid);
        boolean success = userCouponService.saveCouponUsersFromActivity(activity, couponUserList);
        if (!success) {
            log.error("更新user_coupon表出错");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("更新user_coupon表出错，优惠券发奖失败");
            return response;
        }

        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
        response.setMessage("获得优惠券");
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("displayStartTime", DateUtil.dateToString(displayStartTime, DateUtil.FORMAT_TWO));
        resultVO.put("displayEndTime", DateUtil.dateToString(displayEndTime, DateUtil.FORMAT_TWO));
        resultVO.put("amount", amount);
        resultVO.put("desc", desc);
        response.setData(resultVO);

        return response;
    }

    private SimpleResponse<Object> createAssistFromDef(String activityDefId, Long uid) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数转化
        Long adid = Long.valueOf(activityDefId);
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(adid);
        if (null == activityDef) {
            log.error("activityDef表未查到此活动 id = {}", adid);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("商户没有发布此活动");
            return response;
        }
        Long goodsId = activityDef.getGoodsId();

        // 校验是否有发起活动的资格
        Long myActivityId = checkAbility2CreateAssist(uid, goodsId);
        if (myActivityId != null) {
            // 如果没资格 则返回有活动正在进行中 或 此商品 已中奖
            log.info("此商品 您有活动正在进行中 或已中奖");
            response.setCode(ApiStatusEnum.ERROR_BUS_NO1.getCode());
            response.setMessage("此商品 您有活动正在进行中 或已中奖");
            return response;
        }
        // 如果有资格 则发起活动, 有异常则直接抛出异常
        return createAssist(adid, uid);
    }

    /**
     * 助力并领取赠品任务
     *
     * @param activityId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse joinAssist(Long activityId, Long userId) {
        // 每日助力一次
        int timeout = (int) ((DateUtil.getDateBeforeZero(new Date()).getTime() - System.currentTimeMillis()) / 1000);
        boolean isSuccess = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ASSIST_ABILITY + userId, timeout, TimeUnit.SECONDS);
        if (!isSuccess) {
            log.info("今日助力次数已达上限，请明天再来~");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "今日助力次数已达上限，请明天再来~");
        }
        // 获取 活动
        AppletActivity appletActivity = appletActivityRepository.findById(activityId);
        if (null == appletActivity) {
            log.error("查无此活动，activityId = {}", activityId);
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            return new SimpleResponse<>(ApiStatusEnum.ERROR_DB.getCode(), "查无此活动");
        }
        // 活动是否可以助力
        if (!appletActivity.canJoinByUserId(userId)) {
            log.info("活动已完成，activityId = {}", activityId);
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            // 用户发起自己的活动
            return this.createAssist(appletActivity.getActivityInfo().getActivityDefId(), userId);
        }
        // 活动计数
        Long trigger = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + activityId, appletActivity.queryLifeCycle(), TimeUnit.MINUTES);
        if (trigger == null) {
            log.error("活动计数异常，redis异常");
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            // 用户发起自己的活动
            return this.createAssist(appletActivity.getActivityDefId(), userId);
        }
        // 是否参与成功
        if (!appletActivity.isJoinSuccess(trigger)) {
            log.info("活动已完成，activityId = {}", activityId);
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            // 用户发起自己的活动
            return this.createAssist(appletActivity.getActivityDefId(), userId);
        }
        // 是否可以添加到 用户看板 的即将完成排行榜
        if (appletActivity.isBoardSoonCompleteRanking(trigger)) {
            // TODO 登上用户看板的即将完成排行榜 left = condition; activityDefId; userId = activity.getUserId(); activityUser

        }

        // TODO 更新邀请记录 pubUserId = activity.getUserId(); subUserId = userId; activityId

        // 获取用户信息
        AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
        // 将用户成为参与者 -- 获取抽奖码
        Map<String, Object> member = appletActivity.becomeMemberByUser(appletUser.getUserinfo());
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, JSON.toJSONString(member));
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + activityId, appletActivity.queryLifeCycle(), TimeUnit.MINUTES);
        // 生成用户参与事件
        UserJoinEvent userJoinEvent = appletActivity.newUserJoinEvent(userId);

        // 是否开奖
        if (appletActivity.isLottery(trigger)) {
            // 补全 用户参与事件
            appletActivityRepository.findUserJoinEventByAppletActivity(appletActivity, null);
            // 抽奖，并发起相应的中奖任务
            List<Long> winnerIds = appletActivity.drawLottery();
            // 持久化活动状态
            boolean save = appletActivityRepository.saveLottery(appletActivity);
            if (!save) {
                log.error("活动库存不足");
                // TODO 给所有进行中的活动 发优惠券，并关闭所有活动 -- 包括本次活动
                return failedActivityByActivityDefId(appletActivity.getActivityDefId(), null);
            }
            // 中奖名单放入缓存
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER + activityId, JSONObject.toJSONString(winnerIds));
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER + activityId, appletActivity.queryLifeCycle(), TimeUnit.MINUTES);
            //开奖推送消息
            this.sendTemplateMessageService.sendTemplateMsg2Award(appletActivity.getActivityInfo().getShopId(), activityId, null);
            // 1.4新增剩余库存信息 即将完成用户排行榜---从排行榜中剔除
            stringRedisTemplate.opsForHash().delete(RedisKeyConstant.MF_ACTIVITY_ALMOST_COMPLETE + appletActivity.getActivityDefId(),
                    appletActivity.getActivityInfo().getUserId().toString());
        }

        // TODO 如果扣库存类型是 完成扣库存，则此次开奖后 若活动定义库存为0 ，则关闭所有进行中的活动，发优惠券，且关闭

        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    private SimpleResponse<Object> assist4activity(Long userId, Activity activity) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数已校验

        // activity.getUserId()就是pubUid
        Long puid = activity.getUserId();
        // 不能给自己助力
        if (userId.equals(puid)) {
            log.info("不能给自己助力");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            response.setMessage("不能给自己助力");
            return response;
        }

        Long goodsId = activity.getGoodsId();

        // 自己是否有进行中的活动
        // 通过userId 和 goodsId 查 她曾经发起过的此商品的所有活动activity表（商品纬度）
        QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
        activityWrapper.eq("user_id", userId);
        activityWrapper.eq("goods_id", goodsId);
        activityWrapper.eq("type", activity.getType());
        activityWrapper.eq("status", ActivityStatusEnum.PUBLISHED.getCode());
        Activity activityDB = activityService.getOne(activityWrapper);
        Long activityIdDB = null;
        if (null == activityDB) {
            // 如果没有进行中的活动，查user_activity表中曾经发起过的此商品的活动中是否有中奖
            QueryWrapper<UserActivity> uactivityWrapper = new QueryWrapper<>();
            uactivityWrapper.lambda()
                    .eq(UserActivity::getUserId, userId)
                    .eq(UserActivity::getGoodsId, goodsId)
                    .in(UserActivity::getType, Arrays.asList(UserActivityTypeEnum.ASSIST.getCode(), UserActivityTypeEnum.ASSIST_V2.getCode()))
                    .eq(UserActivity::getStatus, UserActivityStatusEnum.WINNER.getCode())
            ;
            UserActivity userActivity = userActivityService.getOne(uactivityWrapper);
            if (null != userActivity) {
                activityIdDB = userActivity.getActivityId();
            }
        } else {
            activityIdDB = activityDB.getId();
        }

        // 是否有助力资格
        Date now = new Date();
        Date dateBeforeZero = DateUtil.getDateBeforeZero(now);
        int timeout = (int) ((dateBeforeZero.getTime() - System.currentTimeMillis()) / 1000);
        boolean isSuccess = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ASSIST_ABILITY + userId, timeout, TimeUnit.SECONDS);
        if (!isSuccess) {
            log.info("今日助力次数已达上限，请明天再来~");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            response.setMessage("今日助力次数已达上限，请明天再来~");
            if (null != activityIdDB) {
                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("activityId", activityIdDB);
                response.setData(resultVO);
            }
            return response;
        }

        Long aid = activity.getId();
        Integer status = activity.getStatus();
        Long condPersionCount = Long.valueOf(activity.getCondPersionCount());
        Date startTime = activity.getStartTime();
        Date endTime = activity.getEndTime();
        int condDrawTime = (int) ((endTime.getTime() - startTime.getTime()) / 1000 / 60);
        // 1440分 = 1天
        condDrawTime = condDrawTime < 1440 ? 1440 : condDrawTime;

        // 获取关键字列表
        List<String> keyWordsList = JSONObject.parseArray(activity.getKeyWordsJson(), String.class);
        // 当没有提供关键词列表，则自己初始化
        if (CollectionUtils.isEmpty(keyWordsList)) {
            keyWordsList = new ArrayList<>();
            keyWordsList.add("");
        }
        int keyWordsListSize = keyWordsList.size();

        // 判断 活动 是否 过期
        Date awardStartTime = activity.getAwardStartTime();
        boolean isFailureEnd = null == awardStartTime && endTime.getTime() < System.currentTimeMillis();
        if (isFailureEnd || status == ActivityStatusEnum.DEL.getCode() || status == ActivityStatusEnum.END.getCode()) {
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("好友活动已开奖，且您的助力任务之前也已成功，获得了此商品，快参加其他奖品活动吧");
            if (null != activityIdDB) {
                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("activityId", activityIdDB);
                response.setData(resultVO);
            }
            return response;
        }
        if (awardStartTime != null) {
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("好友活动已开奖，且您的助力任务之前也已成功，获得了此商品，快参加其他奖品活动吧");
            if (null != activityIdDB) {
                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("activityId", activityIdDB);
                response.setData(resultVO);
            }
            return response;
        }

        // 活动计数
        Long rtn = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + aid, condDrawTime, TimeUnit.MINUTES);
        if (rtn == null) {
            log.info("redis异常");
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            response.setStatusEnum(ApiStatusEnum.ERROR);
            response.setMessage("redis异常");
            return response;
        }

        if (rtn > condPersionCount) {
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setMessage("好友活动已开奖，且您的助力任务之前也已成功，获得了此商品，快参加其他奖品活动吧");
            if (null != activityIdDB) {
                Map<String, Object> resultVO = new HashMap<>();
                resultVO.put("activityId", activityIdDB);
                response.setData(resultVO);
            }
            return response;
        }

        // 获取用户信息
        User user = userService.selectByPrimaryKey(userId);
        if (null == user) {
            log.error("user表 未查询到此用户,id = {}", userId);
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("请先登录");
            return response;
        }
        // 登录邀请信息已初始入库

        // 要是主动进来助力的，那么 pub_join_user表是没有邀请记录的，先查 uid-->sub_uid,aid 不确定是否只有一条记录
        // aid 唯一
        QueryWrapper<UserPubJoin> userPubJoinWrapper = new QueryWrapper<>();
        userPubJoinWrapper.eq("user_id", puid);
        userPubJoinWrapper.eq("sub_uid", userId);
        userPubJoinWrapper.eq("activity_id", aid);
        UserPubJoin userPubJoin = userPubJoinService.getOne(userPubJoinWrapper);
        if (null != userPubJoin) {
            // 要是邀请表有记录，则更新邀请表，记录邀请状态
            int rows = userPubJoinService.updateActivityJoinBySubUidAndAid(userId, aid);
            if (rows == 0) {
                log.error("数据库异常：更新用户邀请记录失败");
                stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
                stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("网络异常，请稍候重试");
                return response;
            }
        }
        // 如果邀请表没有记录，还是可以助力成功，助力信息记录在哪？
        // 助力者入库 支持夜辰那的展现逻辑
        // 用户活动数据入库
        UserActivity userActivity = userActivityService.saveFromActivity(userId, activity, false);
        if (userActivity == null) {
            log.error("用户参与助力活动入库失败，即插入user_activity表失败");
            stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
            stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("网络异常，请稍候重试");
            return response;
        }

        // 加工用户邀请列表的用户信息 放入redis（复用之前的方法，允许有些冗余字段）
        JSONObject userJsonObject = processActivityJoinUserInfo(user);
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + aid, userJsonObject.toJSONString());
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + aid, condDrawTime, TimeUnit.MINUTES);

        User startUser = userService.getById(activity.getUserId());
        JSONObject startUserJO = processActivityJoinUserInfo(startUser);

        // 1.4新增剩余库存信息 即将完成用户排行榜
        int condition = 1;// 还差几人上排行榜的条件
        if (rtn.equals(condPersionCount - condition)) {
            startUserJO.put("left", condition);
            stringRedisTemplate.opsForHash().put(RedisKeyConstant.MF_ACTIVITY_ALMOST_COMPLETE + activity.getActivityDefId(), startUser.getId().toString(), startUserJO.toJSONString());
        }

        // 判断是否触发开奖流程
        if (rtn.equals(condPersionCount)) {
            // 1.4期 新增 扣库存逻辑 ：扣库存方式：1提前占用库存法 2先到先得减库存法
            ActivityDef activityDef = activityDefService.getById(activity.getActivityDefId());
            Integer stockType = activityDef.getStockType();
            if (ActivityDefStockTypeEnum.COMPLETE_REDUCE.getCode().equals(stockType)) {
                // 1.4新增剩余库存信息 即将完成用户排行榜---从排行榜中剔除
                stringRedisTemplate.opsForHash().delete(RedisKeyConstant.MF_ACTIVITY_ALMOST_COMPLETE + activity.getActivityDefId(), startUser.getId().toString());

                int rows = activityDefService.reduceDeposit(activityDef.getId(), BigDecimal.ZERO, 1);
                if (rows == 0) {
                    log.info("活动库存不足");
                    return failedActivityByActivityDefId(activity.getActivityDefId(), null);
                }
                if (activityDef.getStock() == 1) {
                    failedActivityByActivityDefId(activity.getActivityDefId(), activity.getId());
                }
            }

            // 更新活动开奖时间
            activity.setAwardStartTime(now);
            activity.setUtime(now);
            int rows = activityService.update(activity);
            if (rows == 0) {
                log.info("数据库更新开奖时间失败");
                stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, -1);
                stringRedisTemplate.delete(RedisKeyConstant.ASSIST_ABILITY + userId);
                userActivityService.removeById(userActivity.getId());
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("网络异常，请稍候重试");
                return response;
            }

            // 开团成功 助力活动发起者 入库 user_task
            UserTask userTask = newUserTask(activity, keyWordsList.get((int) (Math.random() * keyWordsListSize)));
            userTaskService.insertUserTask(userTask);
            userActivityService.updateStatusByUserIdAndActivityId(puid, aid, UserActivityStatusEnum.WINNER.getCode());
            // 获取中奖名单 放入redis缓存
            List<Long> winner = new ArrayList<>();
            winner.add(puid);
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER + aid, JSONObject.toJSONString(winner));
            stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER + aid, condDrawTime, TimeUnit.MINUTES);

            //开奖推送消息
            this.sendTemplateMessageService.sendTemplateMsg2Award(activity.getShopId(), aid, null);
        }

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    private UserTask newUserTask(Activity activity, String keyWords) {
        QueryWrapper<UserActivity> uaWrapper = new QueryWrapper<>();
        uaWrapper.lambda()
                .eq(UserActivity::getActivityId, activity.getId())
                .eq(UserActivity::getUserId, activity.getUserId());
        UserActivity userActivity = userActivityService.getOne(uaWrapper);
        UserTask userTask = new UserTask();
        userTask.setType(UserTaskTypeEnum.ASSIST_START.getCode());
        if (activity.getType().equals(ActivityTypeEnum.ASSIST_V2.getCode())) {
            userTask.setType(UserTaskTypeEnum.ASSIST_START_V2.getCode());
        }
        userTask.setUserId(activity.getUserId());
        userTask.setKeyWords(keyWords);
        userTask.setMerchantId(activity.getMerchantId());
        userTask.setShopId(activity.getShopId());
        userTask.setActivityId(activity.getId());
        userTask.setGoodsId(activity.getGoodsId());
        userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
        userTask.setUserActivityId(userActivity.getId());
        userTask.setCtime(new Date());
        userTask.setUtime(new Date());
        if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
            userTask.setActivityDefId(activity.getActivityDefId());
        }
        return userTask;
    }

    private SimpleResponse<Object> failedActivityByActivityDefId(Long activityDefId, Long activityId) {
        // 1.4新增剩余库存信息 即将完成用户排行榜---清空
        stringRedisTemplate.delete(RedisKeyConstant.MF_ACTIVITY_ALMOST_COMPLETE + activityDefId);

        // 1.4迭代 活动剩余库存详情 多少人正在努力追赶
        stringRedisTemplate.delete(RedisKeyConstant.MF_ACTIVITY_PRUSUE + activityDefId);

        SimpleResponse<Object> response = new SimpleResponse<>();
        // 更新所有活动失败状态
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.lambda()
                .eq(Activity::getActivityDefId, activityDefId)
                .eq(Activity::getStatus, ActivityStatusEnum.PUBLISHED.getCode())
                .isNull(Activity::getAwardStartTime);
        List<Activity> failedActivities = activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(failedActivities)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }

        List<Activity> updateFailedActivities = new ArrayList<>();
        List<Long> failedUserId = new ArrayList<>();
        for (Activity failedActivity : failedActivities) {
            if (failedActivity.getId().equals(activityId)) {
                continue;
            }
            failedActivity.setStatus(ActivityStatusEnum.FAILURE.getCode());
            failedActivity.setUtime(new Date());
            updateFailedActivities.add(failedActivity);
            failedUserId.add(failedActivity.getUserId());
            //活动未成团推送消息
            this.sendTemplateMessageService.sendTemplateMsg2Award(failedActivity.getShopId(), failedActivity.getId(), null);
        }
        if (CollectionUtils.isEmpty(failedActivities)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            return response;
        }

        activityService.updateBatchById(updateFailedActivities);

        // 所有 未完成者 都获得 优惠券奖励， 并关闭活动
        assist4couponList(failedUserId, failedActivities);

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    /**
     * 领取（发起）赠品任务
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    public SimpleResponse<Object> createAssist(Long activityDefId, Long userId) {
        AppletActivityDef appletActivityDef = appletActivityDefRepository.findById(activityDefId);
        if (null == appletActivityDef) {
            log.error("查无此活动定义，activityDefId = {}", activityDefId);
            return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此活动定义");
        }
        AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
        if (appletUser == null) {
            log.error("无效用户， userId = {}", userId);
            return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "网络异常，请稍候重试");
        }
        // 获取符合参与规则的进行中活动及已完成记录
        JoinRuleActivity joinRuleActivity = joinRuleActivityRepository.findByJoinRule(appletActivityDef.getJoinRule());
        // 用户是否可以发起活动
        if (!joinRuleActivity.canJoinByUserId(userId)) {
            // 获取 用户有进行中 或 已完成的活动 -- 优先返回进行中的活动id
            Long activityId = joinRuleActivity.queryPublishedOrCompletedActivityIdByUserId(userId);
            log.info("用户有进行中 或 已完成的活动, activityId = {}", activityId);
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("activityId", activityId);
            return new SimpleResponse<>(ApiStatusEnum.SUCCESS, resultVO);
        }
        // 活动是否可以发起
        if (!appletActivityDef.canCreate()) {
            log.info("活动已结束");
            return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "活动已结束");
        }
        // 从活动定义中创建用户活动
        AppletActivity appletActivity = appletActivityDef.newActivity(appletUser.getUserinfo());
        // 从用户活动中创建用户参与活动记录 -- 参与事件
        UserJoinEvent userJoinEvent = appletActivity.newUserJoinEvent(userId);
        // 持久化活动状态 -- 事务
        boolean save = appletActivityRepository.saveCreateByStockType(appletActivity);
        if (!save) {
            log.error("活动库存不足");
            return new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "活动库存不足");
        }
        // 处理返回数据
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("activityId", appletActivity.getId());
        return new SimpleResponse<>(ApiStatusEnum.SUCCESS, resultVO);
    }

    private Long checkAbility2CreateAssist(Long userId, Long goodsId) {
        // 要求同一商品只能有一个进行中的活动，只能中奖一次，中奖后无法再发起此商品，未中奖可在此活动结束后再次发起，无发起次数限制
        // 不同商品 可同时发起，互相不干扰
        // ---------------------------------------

        // 通过userId 和 goodsId 查 她曾经发起过的此商品的所有活动activity表（商品纬度）
        QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
        activityWrapper.eq("user_id", userId);
        activityWrapper.eq("goods_id", goodsId);
        activityWrapper.in("type", Arrays.asList(ActivityTypeEnum.ASSIST.getCode(), ActivityTypeEnum.ASSIST_V2.getCode()));
        activityWrapper.orderByDesc("id");
        List<Activity> activitys = activityService.list(activityWrapper);

        // 如果数据库为空，说明是第一次发起，返回 有资格
        if (CollectionUtils.isEmpty(activitys)) {
            return null;
        }

        // 如果数据库不为空 判断她是否有此商品的进行中的活动
        for (Activity act : activitys) {
            // 如果有进行中的活动，返回 没资格
            if (act.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()) {
                return act.getId();
            }
        }

        // 如果没有进行中的活动，查user_activity表中曾经发起过的此商品的活动中是否有中奖
        QueryWrapper<UserActivity> uactivityWrapper = new QueryWrapper<>();
        uactivityWrapper.lambda()
                .eq(UserActivity::getUserId, userId)
                .eq(UserActivity::getGoodsId, goodsId)
                .eq(UserActivity::getStatus, UserActivityStatusEnum.WINNER.getCode());

        uactivityWrapper.orderByDesc("id");
        List<UserActivity> userActivities = userActivityService.list(uactivityWrapper);

        // 如果数据库为空 返回 有资格
        // 如果数据库不为空 返回 没资格
        if (CollectionUtils.isEmpty(userActivities)) {
            return null;
        }
        return userActivities.get(0).getActivityId();

    }

    /**
     * 免单商品抽奖发起者，专用的参与接口
     *
     * @param uid 发起者id
     * @param aid 发起的活动id
     * @return SimpleResponse 返回接口 成功 失败 的状态
     */
    @Override
    public SimpleResponse freeOrderLeader(Long uid, Long aid) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数有效性校验
        if (uid == null || aid == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        // 防重 防并发
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + aid + ":" + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 获取活动信息
        Activity activityDB = activityService.findById(aid);
        if (activityDB == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            return response;
        }
        Long mid = activityDB.getMerchantId();
        Long shopId = activityDB.getShopId();
        Integer type = UserActivityTypeEnum.findUserActivityTypeByActivityType(activityDB.getType());
        Long activityDefId = activityDB.getActivityDefId();
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        Integer condDrawTime = activityDef.getCondDrawTime();
        // 判断用户是否已经参与了此活动，如果参与了，返回response
        if (stringRedisTemplate.opsForHash().hasKey(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            return response;
        }
        // 获取用户信息
        User user = userService.selectByPrimaryKey(uid);
        if (user == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            return response;
        }
        // 用户活动数据入库
        UserActivity userActivity = userActivityService.insertUserActivity(uid, mid, shopId, aid, type, UserActivityStatusEnum.NO_WIN.getCode());
        if (userActivity == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        // 活动计数
        Long rtn = stringRedisTemplate.opsForValue().increment(RedisKeyConstant.ACTIVITY_TRIGGER + aid, 1);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_TRIGGER + aid, condDrawTime, TimeUnit.MINUTES);
        if (rtn == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR);
            return response;
        }
        // 加工用户邀请列表的用户信息 放入redis
        JSONObject userJsonObject = processActivityfreeOrderLeaderInfo(user);
        // 更新redis抽奖概率
        stringRedisTemplate.opsForHash().increment(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, uid.toString(), freeOrderGetAwardCodeNumber);
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_USER_COUNT + aid, condDrawTime, TimeUnit.MINUTES);
        // redis更新活动列表
        stringRedisTemplate.opsForList().leftPush(RedisKeyConstant.ACTIVITY_JOINLIST + aid, userJsonObject.toJSONString());
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_JOINLIST + aid, condDrawTime, TimeUnit.MINUTES);

        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + aid + ":" + uid);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultDate = new HashMap<>();
        resultDate.put("joinStatus", 1);
        resultDate.put("awardNO", userJsonObject.getJSONArray("awardNo"));
        response.setData(resultDate);
        return response;
    }


    @Override
    public SimpleResponse newManLeader(Long userId, Long activityId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数有效性校验
        if (userId == null || activityId == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        // 防重 防并发
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 获取活动信息
        Activity activityDB = activityService.findById(activityId);
        if (activityDB == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            return response;
        }
        Long mid = activityDB.getMerchantId();
        Long shopId = activityDB.getShopId();
        Long activityDefId = activityDB.getActivityDefId();
        Integer type = activityDB.getType();

        // 获取用户信息
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            return response;
        }
        // 判断用户是否发起过此活动，并还在进行中，未结束，返回response
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("userId", userId);
        param.put("type", UserActivityTypeEnum.NEW_MAN_START.getCode());
        List<UserActivity> userActivitys = userActivityService.pageListUserActivity(param);
        if (!CollectionUtils.isEmpty(userActivitys)) {
            for (UserActivity userActivityDB : userActivitys) {
                Activity act = activityService.findById(userActivityDB.getActivityId());
                if (act == null) {
                    continue;
                }
                if (!act.getActivityDefId().equals(activityDefId)) {
                    continue;
                }
                // 如果 此用户已经完成了此店铺的 新人有礼任务 则返回
                if (userActivityDB.getStatus() == UserActivityStatusEnum.WINNER.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                    response.setMessage("你已完成新手有礼任务，若还未领奖 赶快完成任务领奖吧");
                    return response;
                }
                // 如果 此用户 有发起过此活动，并还在进行中， 则返回
                // status = 1  活动结束时间 > now; 拦截
                // 中奖 但任务未完成
                // status = 2  CTime+任务时间 > now; 拦截
                // 中奖 但任务已完成
                // status = 2  Ctime+任务时间 < now; 拦截

                if (userActivityDB.getStatus() == UserActivityStatusEnum.NO_WIN.getCode()) {
                    Activity activity = activityService.findById(userActivityDB.getActivityId());
                    if (activity != null && activity.getEndTime() != null && activity.getEndTime().getTime() > System.currentTimeMillis()) {
                        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                        response.setMessage("你还有进行中的活动");
                        return response;
                    }
                }
            }
        }

        // 用户活动数据入库
        UserActivity userActivity = userActivityService.insertUserActivity(userId, mid, shopId, activityId, type, UserActivityStatusEnum.NO_WIN.getCode());
        if (userActivity == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        return response;
    }

    /**
     * 免单活动-订单认领，订单回填
     *
     * @param req 请求参数
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse backfillOrder(BackFillOrderReq req) {

        /*
         * 1、调用喜销宝
         * 2、校验返回结果
         * 3、创建activity
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        if (!StringUtils.isNumeric(req.getUserId()) || StringUtils.isBlank(req.getOrderId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请求参数错误");
            return simpleResponse;
        }
        Long afId = Long.valueOf(req.getAfId());
        Long userId = Long.valueOf(req.getUserId());
        String orderId = req.getOrderId();
        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            simpleResponse.setMessage("用户未授权");
            return simpleResponse;
        }
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(afId);
        if (activityDef == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("请求参数错误");
            return simpleResponse;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityDef.getShopId());
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("店铺错误");
            return simpleResponse;
        }
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantShop.getMerchantId());
        if (luckCrmCaller.isError(orderInfo)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("订单填写有误");
            return simpleResponse;
        }
        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date date = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (date.getTime() < DateUtil.threeMouthBefore().getTime()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("该订单已失效");
            return simpleResponse;
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            simpleResponse.setMessage("该订单未完成");
            return simpleResponse;
        }
        JSONArray numIids = jsonObject.getJSONArray("num_iid");
        Goods goods = goodsService.selectById(activityDef.getGoodsId());
        if (!numIids.contains(goods.getXxbtopNumIid())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该订单未包含活动商品");
            return simpleResponse;
        }
        // 判断评价, 订单无评价返回"[]"
        String orderComment = taobaoVerifyService.getOrderComment(orderId, merchantShop.getMerchantId());
        if (luckCrmCaller.isError(orderComment)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("请先对订单评价!");
            return simpleResponse;
        }
        JSONObject comment = JSON.parseObject(orderComment);
        Object tradeRate = comment.get("trade_rate");
        if (tradeRate == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("该订单 没有评价");
            return simpleResponse;
        }
        // 返回数据可能是单个对象，也可能是列表
        if (tradeRate instanceof JSONObject) {
            if (!((JSONObject) tradeRate).containsKey("result")) {
                simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
                simpleResponse.setMessage("请先对订单评价");
                return simpleResponse;
            }
        } else if (tradeRate instanceof JSONArray) {
            boolean containsKey = false;
            for (Object o : (JSONArray) tradeRate) {
                ((JSONObject) o).containsKey("result");
                containsKey = true;
            }
            if (!containsKey) {
                simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
                simpleResponse.setMessage("订单尚未评价");
                return simpleResponse;
            }
        } else {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("请先对订单评价.");
            return simpleResponse;
        }
        // 如果同一订单已经被回填过就不能再次领取红包
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(merchantShop.getMerchantId(),
                merchantShop.getId(), userId, orderId);
        if (!CollectionUtils.isEmpty(activityOrders)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该订单已完成回填确认，请填写其他订单");
            return simpleResponse;
        }
        // 判断同一用户同一活动定义，总共只能发起三次
        Map<String, Object> params = new HashMap<>();
        params.put("activityDefId", afId);
        params.put("merchantId", merchantShop.getMerchantId());
        params.put("shopId", merchantShop.getId());
        params.put("userId", userId);
        params.put("del", DelEnum.NO.getCode());
        int size = activityService.countActivity(params);
        Activity activity = null;
        if (size >= activityReceiveNum) {
            // 订单表
            updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), activity, goods, jsonObject);
            return simpleResponse;
        }

        // 判断当前用户有没有该活动定义下未结束的活动
        params.put("willAward", 1);
        int page = activityService.countActivity(params);
        if (page != 0) {
            // 订单表
            updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), activity, goods, jsonObject);
            return simpleResponse;
        }
        // 创建activity
        SimpleResponse simpleResponse1 = createActivityFromDef(userId, orderId, afId, ActivityStatusEnum.UNPUBLISHED.getCode());
        if (simpleResponse1.error()) {
            return simpleResponse1;
        }
        if (simpleResponse1.getData() == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO8);
            simpleResponse.setMessage("订单保存失败");
            return simpleResponse;
        }
        activity = (Activity) simpleResponse1.getData();
        // 订单表
        updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), activity, goods, jsonObject);
        return simpleResponse;
    }

    /**
     * 免单活动 单纯回填订单
     *
     * @param req 请求参数
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse backfillOrderOnly(BackFillOrderReq req) {

        /*
         * 1、调用喜销宝
         * 2、校验返回结果
         * 3、创建activity
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        if (!StringUtils.isNumeric(req.getUserId()) || StringUtils.isBlank(req.getOrderId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请求参数错误");
            return simpleResponse;
        }
        Long userId = Long.valueOf(req.getUserId());
        Long shopId = Long.valueOf(req.getShopId());
        String orderId = req.getOrderId();
        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            simpleResponse.setMessage("用户未授权");
            return simpleResponse;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("店铺错误");
            return simpleResponse;
        }
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantShop.getMerchantId());
        if (luckCrmCaller.isError(orderInfo)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("订单填写有误");
            return simpleResponse;
        }
        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date date = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (date.getTime() < DateUtil.threeMouthBefore().getTime()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("该订单已失效");
            return simpleResponse;
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            simpleResponse.setMessage("该订单未完成");
            return simpleResponse;
        }
        // 如果同一订单已经被回填过就不能再次领取红包
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(merchantShop.getMerchantId(),
                shopId, userId, orderId);
        if (!CollectionUtils.isEmpty(activityOrders)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该订单已完成回填确认，请填写其他订单");
            return simpleResponse;
        }
        // 订单表
        updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, null, jsonObject);
        return simpleResponse;
    }


    /**
     * 从活动定义创建活动
     *
     * @param userId
     * @param orderId
     * @param activityDefId
     * @param status
     * @return
     */
    @Override
    public SimpleResponse createActivityFromDef(Long userId, String orderId, Long activityDefId, Integer status) {
        return activityService.createActivityFromDef(userId, orderId, activityDefId, status);
        // TODO 待添加活动订单更新
    }

    /**
     * 首页弹窗，订单回填
     *
     * @param req 请求参数
     * @return SimpleResponse
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SimpleResponse backfillOrderPopup(BackFillOrderReq req) {

        /*
         * 1、调用喜销宝
         * 2、校验返回结果
         * 3、抽红包
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("awardKey", "");
        resultMap.put("amount", "0");
        simpleResponse.setData(resultMap);
        if (!StringUtils.isNumeric(req.getUserId()) || StringUtils.isBlank(req.getOrderId())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("请求参数错误");
            return simpleResponse;
        }
        Long userId = Long.valueOf(req.getUserId());
        String orderId = req.getOrderId();
        Long shopId = Long.valueOf(req.getShopId());
        // 用户是否授权
        User user = userService.selectByPrimaryKey(userId);
        if (user == null || user.getGrantStatus() != 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            simpleResponse.setMessage("用户未授权");
            return simpleResponse;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("店铺错误");
            return simpleResponse;
        }
        // 如果同一订单已经被回填过就不能再次领取红包
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(merchantShop.getMerchantId(),
                shopId, userId, orderId);
        if (!CollectionUtils.isEmpty(activityOrders)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该订单已完成回填确认，请填写其他订单");
            return simpleResponse;
        }
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantShop.getMerchantId());
        if (luckCrmCaller.isError(orderInfo)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("订单填写有误");
            return simpleResponse;
        }
        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date date = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (date.getTime() < DateUtil.threeMouthBefore().getTime()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            simpleResponse.setMessage("该订单已失效");
            return simpleResponse;
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            simpleResponse.setMessage("该订单未完成");
            return simpleResponse;
        }
        JSONArray numIids = jsonObject.getJSONArray("num_iid");
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantShop.getMerchantId());
        params.put("shopId", shopId);
        params.put("numIids", numIids);
        params.put("del", DelEnum.NO.getCode());
        List<Goods> goodsList = goodsService.pageList(params);
        // 查询商品,如果商品为空则抽取红包
        // 首页扫码回填订单，不参与活动认领，不创建活动
        Map<Long, Goods> goodsIdMap = new HashMap<>();
        // 创建订单表
        if (CollectionUtils.isEmpty(goodsList)) {
            updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, null, jsonObject);
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该商品不在免单活动内");
            return simpleResponse;
        }
        for (Goods goods : goodsList) {
            goodsIdMap.put(goods.getId(), goods);
        }
        params.put("goodsIds", goodsIdMap.keySet());
        List<ActivityDef> activityDefs = activityDefService.selectByParams(params);
        if (CollectionUtils.isEmpty(activityDefs)) {
            for (Goods goods : goodsList) {
                updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, goods, jsonObject);
            }
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO8);
            simpleResponse.setMessage("该商品不在免单活动内");
            return simpleResponse;
        }
        // 暂时不需要创建活动
//        // 订单中存在两个活动定义商品，创建两个对应的活动
//        for (ActivityDef activityDef : activityDefs) {
//            activityParams = activityService.createActivityFromDef(userId, orderId, activityDef.getId());
//            if (activityParams == null) {
//                simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO8);
//                simpleResponse.setMessage("订单保存失败");
//                return simpleResponse;
//            }
//            createActivityOrder(userId, orderId, activityParams, goodsIdMap.get(activityDef.getGoodsId()), jsonObject);
//        }

        // 红包呀不一定中的哟，还得看商品,用户可立即获得一个5元内的随机红包，1元概率80%，2-3元概率10%，4-5元10%
        simpleResponse = randomRedpack(merchantShop.getMerchantId(), merchantShop.getId(), userId, goodsList);
        if (simpleResponse.error()) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO9);
            simpleResponse.setMessage("该商品不在免单活动内");
            return simpleResponse;
        }
        updateOrCreateActivityOrder(userId, orderId, merchantShop.getId(), null, null, jsonObject);
        return simpleResponse;
    }

    /**
     * 查询是否存在该商品的订单返红包活动
     * 获得一个5元内的随机红包，1元概率80%，2-3元概率10%，4-5元10%
     *
     * @param userId
     */
    private SimpleResponse randomRedpack(Long merchantID, Long shopID, Long userId, List<Goods> goodsList) {
        /**
         * 1、随机红包
         * 2、添加余额
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantID);
        params.put("shopId", shopID);
        params.put("type", ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
        params.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        params.put("del", DelEnum.NO.getCode());
        List<ActivityDef> page = activityDefService.selectByParams(params);
        if (CollectionUtils.isEmpty(page)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该商品不在免单活动内");
            return simpleResponse;
        }
        // 取第一个活动
        ActivityDef activityDef = page.get(0);
        List<Long> goodsIds = JSONArray.parseArray(activityDef.getGoodsIds(), Long.class);
        if (goodsList == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该商品不在免单活动内");
            return simpleResponse;
        }
        // 标记商品列表中（回填订单中）是否包含商户定义允许发放红包商品
        boolean contains = false;
        for (Goods goods : goodsList) {
            if (goodsIds.contains(goods.getId())) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("该商品不在免单活动内");
            return simpleResponse;
        }
        // 随机金额，待修改为读取字典表
        CommonDict commonDict = commonDictService.selectByTypeKey("orderRedpack", "list");
        List<Map> mapList = JSON.parseArray(commonDict.getValue(), Map.class);
        BigDecimal redPack = RandomStrUtil.randomAmount(mapList, 1);

        // 更新定义库存
        Integer integer = activityDefService.reduceDeposit(activityDef.getId(), BigDecimal.ZERO, 1);
        if (integer == 0) {
            activityDef.setStatus(ActivityDefStatusEnum.END.getCode());
            activityDefService.updateActivityDef(activityDef);
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
            simpleResponse.setMessage("活动已结束");
            return simpleResponse;
        }
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配, 更新定义保证金
        CommissionDisDto dto = new CommissionDisDto(userId, shopID, UserCouponTypeEnum.ORDER_RED_PACKET, redPack);
        dto.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = commissionService.commissionDistribution(dto);

        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userId, shopID, merchantID);

        Map<String, Object> resultMap = BeanMapUtil.beanToMap(commissionResp);
        resultMap.put("awardKey", "");
        resultMap.put("amount", commissionBO.getCommission());
        resultMap.put("commission", commissionBO.getCommission());
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 活动订单回填（包括任务订单）
     *
     * @param userId     用户ID
     * @param orderId    订单ID
     * @param activity   创建活动实例
     * @param goods      商品实例
     * @param jsonObject 喜销宝返回订单详情
     */
    private Integer createActivityOrder(Long userId, String orderId, MerchantShop merchantShop, Activity activity, Goods goods, JSONObject jsonObject) {

        ActivityOrder activityOrder = new ActivityOrder();
        activityOrder.setUserId(userId);
        activityOrder.setOrderId(orderId);
        activityOrder.setMerchantId(merchantShop.getMerchantId());
        activityOrder.setShopId(merchantShop.getId());
        copyParamToActivityOrder(activity, goods, jsonObject, activityOrder);
        activityOrder.setCtime(new Date());
        activityOrder.setUtime(new Date());
        return activityOrderService.createActivityOrder(activityOrder);
    }

    /**
     * 拷贝参数
     *
     * @param activity
     * @param goods
     * @param jsonObject
     * @param activityOrder
     */
    private void copyParamToActivityOrder(Activity activity, Goods goods, JSONObject jsonObject, ActivityOrder activityOrder) {
        if (activity != null) {
            activityOrder.setActivityId(activity.getId());
            activityOrder.setActivityDefId(activity.getActivityDefId());
        }
        if (goods != null) {
            activityOrder.setDisplayGoodsBanner(goods.getDisplayBanner());
            activityOrder.setDisplayGoodsName(goods.getDisplayGoodsName());
            activityOrder.setDisplayGoodsPrice(goods.getDisplayPrice());
            activityOrder.setGoodsId(goods.getId());
            activityOrder.setPicUrl(goods.getPicUrl());
            activityOrder.setTitle(goods.getXxbtopTitle());
        }
        if (jsonObject != null) {
            activityOrder.setSrcType(GoodsSrcTypeEnum.TAOBAO.getCode());
            activityOrder.setTradeStatus(jsonObject.getString("status"));
            activityOrder.setXxbtopRes(jsonObject.toJSONString());
            activityOrder.setStartTime(DateUtil.stringtoDate(jsonObject.getString("created"), DateUtil.FORMAT_ONE));
            activityOrder.setPayTime(DateUtil.stringtoDate(jsonObject.getString("pay_time"), DateUtil.FORMAT_ONE));
            activityOrder.setPayment(new BigDecimal(jsonObject.getString("payment")));
            if (!JsonParseUtil.booJsonArr(jsonObject.get("num_iid").toString())) {
                return;
            }
            JSONArray numIids = JSONArray.parseArray(jsonObject.get("num_iid").toString());
            activityOrder.setNumIid(jsonObject.get("num_iid").toString());
            // 这里比较恶心，目前只取了第一条
            MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityOrder.getShopId());
            String xxbResult = taobaoVerifyService.getGoodsInfo(numIids.get(0).toString(), merchantShop.getMerchantId());
            if (luckCrmCaller.isError(xxbResult)) {
                return;
            }
            Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
            if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() == 1) {
                return;
            }
            // 名称-主图
            activityOrder.setPicUrl(xxbMap.get("pic_url").toString());
            activityOrder.setTitle(xxbMap.get("title").toString());
        }
    }

    /**
     * 活动订单回填（包括任务订单）
     *
     * @param userId     用户ID
     * @param orderId    订单ID该商品不在免单活动内
     * @param activity   创建活动实例
     * @param goods      商品实例
     * @param jsonObject 喜销宝返回订单详情
     */
    @Override
    public Integer updateOrCreateActivityOrder(Long userId, String orderId, Long shopId, Activity activity, Goods goods, JSONObject jsonObject) {
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        // 如果同一订单已经被回填过，需要修改补充字段
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(merchantShop.getMerchantId(),
                merchantShop.getId(), userId, orderId);
        if (CollectionUtils.isEmpty(activityOrders)) {
            return createActivityOrder(userId, orderId, merchantShop, activity, goods, jsonObject);
        }
        ActivityOrder activityOrder = activityOrders.get(0);
        copyParamToActivityOrder(activity, goods, jsonObject, activityOrder);
        return activityOrderService.updateActivityOrder(activityOrder);
    }

    /**
     * 幸运大轮盘 详情展示
     *
     * @param uid 用户id
     * @param aid 活动定义id
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse queryLuckyWheel(String uid, String aid, int offset, int size) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数有效性校验
        if (StringUtils.isBlank(uid) || StringUtils.isBlank(aid)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        String resultJS = stringRedisTemplate.opsForValue().get(RedisKeyConstant.LUCKY_WHEEL_PAGE_CACHE + aid + ":" + uid);
        if (StringUtils.isNotBlank(resultJS)) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(JSON.parseObject(resultJS));
            return response;
        }

        Long userId = Long.valueOf(uid);
        Long activityId = Long.valueOf(aid);

        // 校验活动有效性
        Activity activity = activityService.findById(activityId);
        if (activity == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：未查到此活动");
            return response;
        }

        Map<String, Object> param = new HashMap<>();
        param.put("userId", userId);
        param.put("shopId", activity.getShopId());
        param.put("type", UserPubJoinTypeEnum.LUCK_WHEEL.getCode());
        param.put("subActionStatus", UserPubJoinStatusEnum.JOINED.getCode());

        Map<String, Object> resultVO = getTimesAndChance(activity.getShopId(), userId);

        // 设置分页
        param.put("offset", offset);
        param.put("size", size);
        param.put("order", "id");
        param.put("direct", "desc");
        // 获取用户邀请记录
        List<UserPubJoin> userPubJoins = userPubJoinService.listPageByParams(param);
        List<Map<String, Object>> subUserInfos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            for (UserPubJoin userPubJoin : userPubJoins) {
                Map<String, Object> subUserInfo = new HashMap<>();
                subUserInfo.put("subUid", userPubJoin.getSubUid());
                subUserInfo.put("subNick", userPubJoin.getSubNick());
                subUserInfo.put("subAvatar", userPubJoin.getSubAvatar());
                subUserInfos.add(subUserInfo);
            }
        }
        resultVO.put("subUserInfos", subUserInfos);

        stringRedisTemplate.opsForValue().set(RedisKeyConstant.LUCKY_WHEEL_PAGE_CACHE + aid + ":" + uid, JSON.toJSONString(resultVO), 30, TimeUnit.MINUTES);

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    /**
     * 幸运大轮盘 邀请列表
     *
     * @param uid
     * @param aid
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryUserPubJoin(String uid, String aid, int offset, int size) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数有效性校验
        if (StringUtils.isBlank(uid) || StringUtils.isBlank(aid)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        Long userId = Long.valueOf(uid);
        Long activityId = Long.valueOf(aid);

        // 校验活动有效性
        Activity activity = activityService.findById(activityId);
        if (activity == null) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：未查到此活动");
            return response;
        }

        Map<String, Object> param = new HashMap<>();
        param.put("userId", userId);
        param.put("shopId", activity.getShopId());
        param.put("type", UserPubJoinTypeEnum.LUCK_WHEEL.getCode());
        param.put("subActionStatus", UserPubJoinStatusEnum.JOINED.getCode());
        int count = userPubJoinService.countByParams(param);
        // 设置分页
        param.put("offset", offset);
        param.put("size", size);
        param.put("order", "id");
        param.put("direct", "desc");
        // 获取用户邀请记录
        List<UserPubJoin> userPubJoins = userPubJoinService.listPageByParams(param);
        List<Map<String, Object>> subUserInfos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            for (UserPubJoin userPubJoin : userPubJoins) {
                Map<String, Object> subUserInfo = new HashMap<>();
                subUserInfo.put("subUid", userPubJoin.getSubUid());
                subUserInfo.put("subNick", userPubJoin.getSubNick());
                subUserInfo.put("subAvatar", userPubJoin.getSubAvatar());
                subUserInfos.add(subUserInfo);
            }
        }

        HashMap<Object, Object> resultVO = new HashMap<>(16);
        resultVO.put("offset", offset);
        resultVO.put("size", size);
        resultVO.put("count", count);
        resultVO.put("subUserInfos", subUserInfos);

        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }


    /**
     * 幸运大轮盘 试试手气
     *
     * @param userIdstr     用户id
     * @param activityIdstr 活动id
     * @param pubUserIdstr  邀请人id
     * @return SimpleResponse
     */
    // @Transactional(rollbackFor = Exception.class)
    @Override
    public SimpleResponse tryYourLuck(String merchantID, String shopID, String userIdstr, String activityIdstr, String pubUserIdstr) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 参数有效性校验
        if (StringUtils.isBlank(merchantID) || StringUtils.isBlank(shopID) || StringUtils.isBlank(userIdstr)
                || StringUtils.isBlank(activityIdstr)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        if (StringUtils.equals(userIdstr, pubUserIdstr)) {
            pubUserIdstr = null;
        }

        Long uid = Long.valueOf(userIdstr);
        Long aid = Long.valueOf(activityIdstr);
        Long mid = Long.valueOf(merchantID);
        Long sid = Long.valueOf(shopID);
        // 防重 防并发
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.LUCKY_WHEEL_FORBID_REPEAT + aid + ":" + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 检验活动是否结束
        Activity activity = activityService.findById(aid);
        if (activity.getStatus() == ActivityStatusEnum.END.getCode() || activity.getStatus() == ActivityStatusEnum.DEL.getCode()) {
            UpayBalance upayBalance = this.upayBalanceService.query(mid, sid, uid);
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("awardName", LuckyWheelEnum.THANKS.getCode());
            resultVO.put("awardAmount", 0);
            resultVO.put("balance", upayBalance == null || upayBalance.getBalance() == null ? BigDecimal.ZERO : upayBalance.getBalance());
            response.setData(resultVO);

            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("该活动已结束，请参加其他活动");
            return response;
        }

        User user = userService.selectByPrimaryKey(uid);


        Map<String, Object> resultMap;
        // 获取用户剩余抽奖次数
        String resultJS = stringRedisTemplate.opsForValue().get(RedisKeyConstant.LUCKY_WHEEL_PAGE_CACHE + aid + ":" + uid);
        if (StringUtils.isNotBlank(resultJS)) {
            resultMap = JSONObject.parseObject(resultJS);
        } else {
            resultMap = getTimesAndChance(sid, uid);
        }
        int times = Integer.parseInt(resultMap.get("times").toString());
        if (times <= 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO22);
            response.setData(resultMap);
            return response;
        }
        // 删除redis缓存
        stringRedisTemplate.delete(RedisKeyConstant.LUCKY_WHEEL_PAGE_CACHE + aid + ":" + uid);

        UserPubJoin userPubJoin = null;
        if (StringUtils.isNotBlank(pubUserIdstr) && RegexUtils.StringIsNumber(pubUserIdstr)) {

            Long pubUid = Long.valueOf(pubUserIdstr);
            // 获取uid的被邀请记录
            Map<String, Object> param = new HashMap<>();
            param.put("type", UserPubJoinTypeEnum.LUCK_WHEEL.getCode());
            param.put("userId", pubUid);
            param.put("subUid", uid);
            param.put("shopId", sid);
            param.put("aid", aid);
            List<UserPubJoin> userPubJoins = userPubJoinService.listPageByParams(param);
            if (CollectionUtils.isEmpty(userPubJoins)) {
                SimpleResponse resp = userPubJoinService.userPubJoin(Long.valueOf(pubUserIdstr), activity, user);
                if (resp.getCode() == 0) {
                    userPubJoin = (UserPubJoin) resp.getData();
                }
            }

            /*Map<String, Object> userCouponParam = new HashMap<>();
            userCouponParam.put("userId", uid);
            int count = userCouponService.countByParams(userCouponParam);
            if (count != 0) {
                pubUserIdstr = null;
            }*/

            // 已被 邀请人本人 邀请成功过
            /*UserPubJoin userPubJoinDB = userPubJoinService.selectByUserAndSubUidAndAid(pubUid, uid, aid);*/
            for (UserPubJoin userPubJoinDB : userPubJoins) {
                if (userPubJoinDB.getSubActionStatus() == UserPubJoinStatusEnum.JOINED.getCode()) {
                    pubUserIdstr = null;
                } else {
                    userPubJoin = userPubJoinDB;
                }
            }
        }


        Date now = new Date();
        if (StringUtils.isNotBlank(pubUserIdstr)) {
            Long pubUid = Long.valueOf(pubUserIdstr);
            // 获取uid的被邀请记录
            /*UserPubJoin userPubJoin4Checkpoints = userPubJoinService.selectByUserAndSubUidAndAid(pubUid, uid, aid);*/

            if (userPubJoin != null) {
                // 更新uid的参与状态 入库
                userPubJoin.setSubActionStatus(UserPubJoinStatusEnum.JOINED.getCode());
                userPubJoin.setSubActionTime(now);
                userPubJoin.setUtime(now);
                int rows = userPubJoinService.updateByPrimaryKeySelective(userPubJoin);
                if (rows == 0) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                    response.setMessage("数据库异常：更新邀请记录失败");
                    return response;
                }
            }
            // 删除redis缓存
            stringRedisTemplate.delete(RedisKeyConstant.LUCKY_WHEEL_PAGE_CACHE + aid + ":" + pubUid);
        }
        // 抽奖概率算法

        Map<Integer, Integer> awardPool = new HashMap<>();
        awardPool.put(LuckyWheelEnum.THANKS.getCode(), LuckyWheelEnum.THANKS.getProbability());
        awardPool.put(LuckyWheelEnum.RMB01_03.getCode(), LuckyWheelEnum.RMB01_03.getProbability());
        awardPool.put(LuckyWheelEnum.RMB03_05.getCode(), LuckyWheelEnum.RMB03_05.getProbability());
        Integer awardPoolKey = LuckyWheelEnum.THANKS.getCode();
        BigDecimal awardAmount = new BigDecimal(0);
        // 获取概率分母---总数
        Integer totalCount = 0;
        for (Map.Entry<Integer, Integer> awardEntry : awardPool.entrySet()) {
            totalCount = totalCount + awardEntry.getValue();
        }
        Integer start = 0;
        Integer end = 0;
        int indexCount;

        Integer random = new Random().nextInt(totalCount);
        for (Map.Entry<Integer, Integer> entry : awardPool.entrySet()) {
            indexCount = entry.getValue();
            end = end + indexCount;
            log.info("lucky wheel random = " + random);
            if (random >= start && random < end) {
                //中奖
                awardPoolKey = entry.getKey();
                break;
            } else {
                start = start + indexCount;
            }
        }
        // 获取实际中奖金额
        if (LuckyWheelEnum.THANKS.getCode().equals(awardPoolKey)) {
            awardAmount = new BigDecimal(0);
        }
        if (LuckyWheelEnum.RMB01_03.getCode().equals(awardPoolKey)) {
            awardAmount = new BigDecimal(Math.random() * 0.2 + 0.1);
        }
        if (LuckyWheelEnum.RMB03_05.getCode().equals(awardPoolKey)) {
            awardAmount = new BigDecimal(Math.random() * 0.2 + 0.3);
        }
        awardAmount = awardAmount.setScale(1, BigDecimal.ROUND_DOWN);

        Long activityDefId = activity.getActivityDefId();
        Integer integer = activityDefService.reduceDeposit(activityDefId, BigDecimal.ZERO, 1);
        if (integer == 0) {
            activity.setStatus(ActivityStatusEnum.END.getCode());
            activity.setUtime(now);
            activity.setEndTime(now);
            activityService.update(activity);
            ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
            if (activityDef == null) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                response.setMessage("该活动定义不存在");
                return response;
            }
            activityDef.setStatus(ActivityDefStatusEnum.END.getCode());
            activityDefService.updateActivityDef(activityDef);
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("活动已结束");
            return response;
        }
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
        CommissionDisDto commissionDisDto = new CommissionDisDto(uid, sid, UserCouponTypeEnum.LUCKY_WHEEL, awardAmount);
        commissionDisDto.setActivityId(aid);
        commissionDisDto.setActivityDefId(activityDefId);
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);
        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(uid, NumberUtils.toLong(shopID), NumberUtils.toLong(merchantID));

//        UserTask userTask = userTaskService.insertUserTask(mid, sid, aid, uid, UserTaskStatusEnum.AWARD_SUCCESS.getCode(), UserTaskTypeEnum.LUCK_WHEEL.getCode(), null, null);
//        if (null == userTask) {
//            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
//            response.setMessage("数据库异常: 插入userTask失败");
//            return response;
//        }

        UpayBalance upayBalance = this.upayBalanceService.query(mid, sid, uid);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> resultVO = BeanMapUtil.beanToMap(commissionResp);
        resultVO.put("awardName", awardPoolKey);
        resultVO.put("awardAmount", commissionBO != null && commissionBO.getCommission() != null ? commissionBO.getCommission() : BigDecimal.ZERO);
        resultVO.put("commission", awardAmount);
        resultVO.put("balance", upayBalance == null || upayBalance.getBalance() == null ? BigDecimal.ZERO : upayBalance.getBalance());
        response.setData(resultVO);
        stringRedisTemplate.delete(RedisKeyConstant.LUCKY_WHEEL_FORBID_REPEAT + aid + ":" + uid);
        return response;
    }

    // 私有方法：未中奖用户获得优惠券
    private Set<String> getCouponUser(Long aid, Long shopId, ArrayList<Long> winner) {
        // 获取机器人
        Map<String, Object> userParam = new HashMap<>();
        userParam.put("merchantId", 0);
        userParam.put("shopId", 0);
        userParam.put("grantStatus", -1);
        List<User> users = userService.listPageByParams(userParam);
        List<Long> robots = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                robots.add(user.getId());
            }
        }


        Map<Object, Object> userCountMap = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + aid);
        for (Map.Entry<Object, Object> entry : userCountMap.entrySet()) {
            Long userId = Long.parseLong((String) entry.getKey());

            // 排除机器人
            if (!CollectionUtils.isEmpty(robots)) {
                if (robots.contains(userId)) {
                    continue;
                }
            }

            if (winner.contains(userId)) {
                continue;
            }
            stringRedisTemplate.opsForSet().add(RedisKeyConstant.ACTIVITY_LOSER_SET + aid, userId.toString());
        }

        return stringRedisTemplate.opsForSet().difference(RedisKeyConstant.ACTIVITY_LOSER_SET + aid, RedisKeyConstant.SHOP_COUPON_USER_OWNER + shopId);
    }

    private Set<String> getTaskCollectCartUser(Long activityPlanId, Long activityDefId, Long aid, ArrayList<Long> winner) {
        // 获取机器人
        Map<String, Object> userParam = new HashMap<>();
        userParam.put("merchantId", 0);
        userParam.put("shopId", 0);
        userParam.put("grantStatus", -1);
        List<User> users = userService.listPageByParams(userParam);
        List<Long> robots = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                robots.add(user.getId());
            }
        }

        Map<Object, Object> userCountMap = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + aid);
        for (Map.Entry<Object, Object> entry : userCountMap.entrySet()) {
            Long userId = Long.parseLong((String) entry.getKey());

            // 排除机器人
            if (!CollectionUtils.isEmpty(robots)) {
                if (robots.contains(userId)) {
                    continue;
                }
            }

            if (winner.contains(userId)) {
                continue;
            }
            stringRedisTemplate.opsForSet().add(RedisKeyConstant.ACTIVITY_LOSER_SET + aid, userId.toString());
        }

        return stringRedisTemplate.opsForSet().difference(RedisKeyConstant.ACTIVITY_LOSER_SET + aid, RedisKeyConstant.ACTIVITY_PLAN_DEF_TASK_OWNER + activityPlanId + ":" + activityDefId);

    }

    // 私有方法：抽奖概率算法
    private ArrayList<Long> getWinner(Long aid, Long condPersionCount, Integer hitsPerDraw, Integer condDrawTime) {
        // 获取机器人
        Map<String, Object> userParam = new HashMap<>();
        userParam.put("merchantId", 0);
        userParam.put("shopId", 0);
        userParam.put("grantStatus", -1);
        List<User> users = userService.listPageByParams(userParam);
        List<Long> robots = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                robots.add(user.getId());
            }
        }

        int totalCount = 0;
        Map<Object, Object> userCountMap = stringRedisTemplate.opsForHash().entries(RedisKeyConstant.ACTIVITY_USER_COUNT + aid);
        for (Object value : userCountMap.values()) {
            totalCount = totalCount + Integer.parseInt((String) value);
        }
        int times;
        ArrayList<Long> hits = new ArrayList<>();
        if (condPersionCount <= hitsPerDraw * winPrizeTimes.longValue()) {
            // 当参与人数小于等于奖品数量的2倍时
            times = userCountMap.size();
        } else {
            // 当参与人数大于奖品数量的2倍时
            times = hitsPerDraw * winPrizeTimes;
        }
        for (int i = 0; i < times; i++) {
            int start = 0;
            int end = 0;
            int count;
            int random = new Random().nextInt(totalCount);
            for (Map.Entry<Object, Object> entry : userCountMap.entrySet()) {
                Long userId = Long.parseLong((String) entry.getKey());
                count = Integer.parseInt((String) entry.getValue());
                end = end + count;
                if (random >= start && random < end) {
                    //中奖

                    // 排除机器人
                    if (!CollectionUtils.isEmpty(robots)) {
                        if (robots.contains(userId)) {
                            i--;
                            break;
                        }
                    }

                    // 已中奖过滤
                    if (hits.contains(userId)) {
                        i--;
                        break;
                    }
                    // 新增中奖人
                    hits.add(userId);
                    break;
                } else {
                    start = start + count;
                }
            }
        }
        stringRedisTemplate.opsForValue().set(RedisKeyConstant.ACTIVITY_WINNER + aid, JSONObject.toJSONString(hits));
        stringRedisTemplate.expire(RedisKeyConstant.ACTIVITY_WINNER + aid, condDrawTime, TimeUnit.MINUTES);
        return hits;
    }

    // 私有方法：加工用户邀请列表的用户信息
    public JSONObject processActivityJoinUserInfo(User user) {
        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("uid", user.getId());
        userJsonObject.put("nickName", user.getNickName());
        userJsonObject.put("avatarUrl", user.getAvatarUrl());
        long currentTime = System.currentTimeMillis();
        long awardNo = currentTime % 100000000;
        List<Long> awardNoList = new ArrayList<>();
        awardNoList.add(awardNo);
        userJsonObject.put("awardNo", awardNoList);
        userJsonObject.put("time", currentTime);
        return userJsonObject;
    }

    // 私有方法： 免单活动抽奖发起者的 相关用户活动信息
    public JSONObject processActivityfreeOrderLeaderInfo(User user) {
        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("uid", user.getId());
        userJsonObject.put("nickName", user.getNickName());
        userJsonObject.put("avatarUrl", user.getAvatarUrl());
        long currentTime = System.currentTimeMillis();
        long awardNo = currentTime % 100000000;
        List<Long> awardNoList = new ArrayList<>();
        for (int i = 0; i < freeOrderGetAwardCodeNumber; i++) {
            awardNo = awardNo + i;
            awardNoList.add(awardNo);
        }
        userJsonObject.put("awardNo", awardNoList);
        userJsonObject.put("time", currentTime);
        return userJsonObject;
    }

    // 私有方法： 幸运大轮盘 获取用户 剩余次数 和 总机会
    public Map<String, Object> getTimesAndChance(Long shopId, Long userId) {
        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("userId", userId);
        param.put("type", UserPubJoinTypeEnum.LUCK_WHEEL.getCode());
        param.put("subActionStatus", UserPubJoinStatusEnum.JOINED.getCode());
        int pubCount = userPubJoinService.countByParams(param);
        int chance;
        int maxCount = (max - initVal) * initCount;
        // 再邀请多少人可获得一次抽奖机会
        int num;
        if (pubCount < maxCount) {
            chance = pubCount / initCount + initVal;
            num = pubCount % initCount;
            if (num == 0) {
                num = initCount;
            }
        } else {
            num = pubCount - (maxCount + initCount + 1);
            chance = max;
            for (int i = initCount + 1 + 1; num >= 0; i++) {
                num = num - i;
                chance = chance + 1;
            }
        }
        // 获取用户领奖记录
        Map<String, Object> userCouponParam = new HashMap<>();
        userCouponParam.put("userId", (Long) param.get("userId"));
        userCouponParam.put("shopId", (Long) param.get("shopId"));
        userCouponParam.put("type", UserCouponTypeEnum.LUCKY_WHEEL.getCode());
        int awardCount = userCouponService.countByParams(userCouponParam);
        // 剩余抽奖次数
        int times = chance - awardCount;

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("times", times);
        resultVO.put("chance", chance);
        resultVO.put("leftSize", Math.abs(num));
        return resultVO;
    }

}
