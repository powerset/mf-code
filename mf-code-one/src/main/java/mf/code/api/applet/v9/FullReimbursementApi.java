package mf.code.api.applet.v9;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v9.service.FullReimbursementService;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.one.constant.ActivityApiEntrySceneEnum;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.v9
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-02 15:18
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v9/userActivity")
public class FullReimbursementApi {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private FullReimbursementService fullReimbursementService;

    /**
     * 全额报销 -- 创建
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    @GetMapping("/createFullReimbursement")
    public SimpleResponse createFullReimbursement(@RequestParam("userId") String userId,
                                                  @RequestParam("amount") String amount,
                                                  @RequestParam("activityDefId") String activityDefId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(activityDefId) || !RegexUtils.StringIsNumber(activityDefId)
                || StringUtils.isBlank(amount) || !RegexUtils.isAmount(amount)) {
            log.error("参数异常, userId = {}, activityDefId = {}, amount = {}", userId, activityDefId, amount);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        // 防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityDefId + ":" + userId);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "点击太快，请稍候再试");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("activityDefId", activityDefId);
        params.put("amount", amount);
        params.put("type", ActivityApiEntrySceneEnum.CREATE.getCode());
        SimpleResponse result = fullReimbursementService.create(params);
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityDefId + ":" + userId);
        return result;
    }

    /**
     * 全额报销 -- 助力
     *
     * @param activityId
     * @param userId
     * @return
     */
    @GetMapping("/assistFullReimbursement")
    public SimpleResponse assistFullReimbursement(@RequestParam("userId") String userId,
                                                  @RequestParam("activityId") String activityId) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(activityId) || !RegexUtils.StringIsNumber(activityId)) {
            log.error("参数异常, userId = {}, activityId = {}", userId, activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        // 防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "点击太快，请稍候再试");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("activityId", activityId);
        params.put("type", ActivityApiEntrySceneEnum.ASSIST.getCode());

        SimpleResponse result = fullReimbursementService.assist(params);
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_SUBMIT_FORBID_REPEAT + activityId + ":" + userId);
        return result;
    }

    /**
     * 全额报销 -- 展示
     *
     * @param shopId
     * @param userId
     * @return
     */
    @GetMapping("/queryActivity")
    public SimpleResponse queryActivity(@RequestParam("userId") String userId,
                                        @RequestParam("shopId") String shopId,
                                        @RequestParam("activityId") String activityId,
                                        @RequestParam("activityDefType") String activityDefType,
                                        @RequestParam("scenes") String scenes,
                                        @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                        @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        if (StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(activityDefType) || !RegexUtils.StringIsNumber(activityDefType)
                || StringUtils.isBlank(scenes) || !RegexUtils.StringIsNumber(scenes)) {
            log.error("参数异常, userId = {}, shopId = {}, activityDefType = {}, scenes = {}", userId, shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("shopId", shopId);
        params.put("activityDefType", activityDefType);
        params.put("activityId", activityId);
        params.put("scenes", scenes);
        params.put("offset", offset);
        params.put("limit", size);
        params.put("type", ActivityApiEntrySceneEnum.QUERY.getCode());
        return fullReimbursementService.queryActivityByParams(params);
    }

    /***
     * 报销活动弹窗-首页
     * @param shopId
     * @param userId
     * @param scenes
     * @return
     */
    @GetMapping("/fullReimbursementPopup")
    public SimpleResponse fullReimbursementPopup(@RequestParam("shopId") String shopId,
                                                 @RequestParam("userId") String userId,
                                                 @RequestParam("scenes") String scenes) {
        if (StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)
                || StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)
                || StringUtils.isBlank(scenes) || !RegexUtils.StringIsNumber(scenes)) {
            log.error("参数异常, shopId = {}, userId = {}, scenes", shopId, userId, scenes);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        return fullReimbursementService.fullReimbursementPopup(Long.valueOf(shopId), Long.valueOf(userId), Integer.valueOf(scenes));
    }

    /**
     * 全额报销 -- 助力弹窗-用户已助力调用
     *
     * @param activityId
     * @param userId
     * @return
     */
    @GetMapping("/activityAssistRedis")
    public SimpleResponse queryActivity(@RequestParam("userId") String userId,
                                        @RequestParam("activityId") String activityId) {
        if (StringUtils.isBlank(activityId) || StringUtils.isBlank(userId)) {
            return new SimpleResponse();
        }
        String redisKey = RedisKeyConstant.ASSIST_SUCCESS + activityId + ":" + userId;
        fullReimbursementService.deleteActivityAssistRedis(redisKey);
        return new SimpleResponse();
    }

    /***
     * 全额报销-- 创建活动页的前置过滤和数据支持
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    @GetMapping("/activityCreateFilter")
    public SimpleResponse activityCreateFilter(@RequestParam("userId") String userId,
                                               @RequestParam("shopId") String shopId,
                                               @RequestParam("merchantId") String merchantId) {
        return fullReimbursementService.activityCreateFilter(merchantId, shopId, userId);
    }

    /***
     * 全额报销-- 弹幕
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    @GetMapping("/barrage")
    public SimpleResponse barrage(@RequestParam("userId") String userId,
                                  @RequestParam("shopId") String shopId,
                                  @RequestParam("merchantId") String merchantId) {
        return fullReimbursementService.barrage(merchantId, shopId, userId);
    }
}
