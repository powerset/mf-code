package mf.code.api.applet.v8.dto;

import lombok.Data;

/**
 * mf.code.api.applet.v8.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 15:26
 */
@Data
public class NewbieTask {
    //类型
    private int type;
    //是否完成
    private boolean finish;
    //返利金额
    private String rebate;
    //邀请人数达标
    private int inviteNum;
    //活动定义编号
    private Long activityDefId;
    //粉丝店铺商户编号
    private Long merchantId;
    //粉丝店铺店铺编号
    private Long shopId;
}
