package mf.code.api.applet.v11.upayorder.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.v11.upayorder.AbstractUpayOrderService;
import mf.code.api.feignclient.CommAppService;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.api.feignservice.OneAppServiceImpl;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.UserOrderReqDTO;
import mf.code.one.dto.UserPayOrderBizDTO;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.constant.UserRoleEnum;
import mf.code.user.dto.UpayWxOrderReq;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v11.upayorder.impl
 *
 * @description: TODO：百川支持
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月26日 18:25
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PayShopManagerService extends AbstractUpayOrderService {
    private final AppletUserRepository appletUserRepository;
    private final CommAppService commAppService;
    private final DistributionAppService distributionAppService;
    private final OneAppServiceImpl oneAppService;


    @Override
    public UpayWxOrderBizTypeEnum getType() {
        return UpayWxOrderBizTypeEnum.SHOP_MANAGER_PAY;
    }

    /***
     * 检查入参的有效性
     *
     * @param req
     * @return
     */
    @Override
    protected SimpleResponse assertReq(UserOrderReqDTO req) {
        BigDecimal amount = new BigDecimal(req.getAmount());

        BigDecimal salePrice = BigDecimal.ZERO;
        CommonDictDTO commonDictDTO = commAppService.selectByTypeKey("shopManager", "salePolicy");
        if (commonDictDTO != null && StringUtils.isNotBlank(commonDictDTO.getValue())) {
            JSONObject jsonObject = JSON.parseObject(commonDictDTO.getValue());
            salePrice = jsonObject.getBigDecimal("salePrice");
        }

        if (amount.compareTo(salePrice) != 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "金额异常");
        }

        return new SimpleResponse();
    }

    /***
     * 检查是否可创建
     *
     * @param req
     * @return
     */
    @Override
    protected SimpleResponse checkCanCreate(UserOrderReqDTO req) {
        Long userId = req.getUserId();

        AppletUserAggregateRoot appletUser = appletUserRepository.findById(userId);
        if (appletUser == null) {
            log.error("无效用户, userId = {}", userId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "无效用户");
        }
        if (appletUser.getUserinfo().getRole() == UserRoleEnum.SHOP_MANAGER.getCode()) {
            log.info("已成为店长");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "你已是店长");
        }

        CommonDictDTO commonDictDTO = commAppService.selectByTypeKey("shopManager", "salePolicy");
        if (commonDictDTO == null) {
            log.info("暂无付费升店长配置");
            return new SimpleResponse();
        }
        JSONObject jsonObject = JSON.parseObject(commonDictDTO.getValue());
        if (jsonObject == null) {
            log.error("付费升店长配置为空");
            return new SimpleResponse();
        }

        Map<String, Object> shopManagerSalePolicy = new HashMap<>();
        shopManagerSalePolicy.put("originalPrice", jsonObject.getBigDecimal("originalPrice"));
        shopManagerSalePolicy.put("salePrice", jsonObject.getBigDecimal("salePrice"));
        JSONArray policyJsonArray = jsonObject.getJSONArray("policyList");
        List<Map<String, Integer>> policyList = new ArrayList<>();
        for (Object obj : policyJsonArray) {
            Map<String, Integer> policy = new HashMap<>();
            JSONObject policyJsonObject = JSON.parseObject(JSON.toJSONString(obj));
            policy.put("hasSuperior", policyJsonObject.getInteger("hasSuperior"));
            policy.put("superiorRate", policyJsonObject.getInteger("superiorRate"));
            policy.put("shopRate", policyJsonObject.getInteger("shopRate"));
            policy.put("platformRate", policyJsonObject.getInteger("platformRate"));
            policyList.add(policy);
        }
        shopManagerSalePolicy.put("policyList", policyList);
        //获取当时的 付费升店长配置 -- 夜辰的DTO
        UserPayOrderBizDTO userPayOrderBizDTO = new UserPayOrderBizDTO();
        userPayOrderBizDTO.setRemark(JSONObject.toJSONString(shopManagerSalePolicy));
        return new SimpleResponse(ApiStatusEnum.SUCCESS, userPayOrderBizDTO);
    }

    /***
     * wx支付的回调业务处理
     *
     * @param order
     * @return
     */
    @Override
    protected SimpleResponse wxPayCallBackBiz(UpayWxOrder order) {
        if (order == null) {
            log.error("参数异常, order = {}", order);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }
        UpayWxOrderReq upayWxOrderReq = new UpayWxOrderReq();
        BeanUtils.copyProperties(order, upayWxOrderReq);

        return distributionAppService.becomeShopManagerV2(upayWxOrderReq);
    }

    /***
     * 查询订单支付成功后的业务数据返回
     *
     * @param upayWxOrder
     * @return
     */
    @Override
    protected Map queryStatusBiz(UpayWxOrder upayWxOrder) {
        AppletUserAggregateRoot byId = appletUserRepository.findById(upayWxOrder.getUserId());
        Map<String, Object> stringObjectMap = oneAppService.queryNewcomerTaskInfo(byId.getUserinfo().getVipShopId(), byId.getUserId());
        String dialogType = "0";
        if (!CollectionUtils.isEmpty(stringObjectMap)) {
            BigDecimal shopManagerAmount = new BigDecimal(stringObjectMap.get("shopManagerAmount").toString());
            if (shopManagerAmount.compareTo(BigDecimal.ZERO) > 0) {
                dialogType = "1";
            }
        }
        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("dialogType", dialogType);

        return new HashMap();
    }
}
