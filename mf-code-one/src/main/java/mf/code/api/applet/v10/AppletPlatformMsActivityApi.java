package mf.code.api.applet.v10;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v10.service.PlatformMsActivityService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.v10
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月16日 08:45
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v10/msActivity")
public class AppletPlatformMsActivityApi {
    @Autowired
    private PlatformMsActivityService platformMsActivityService;


    /***
     * 获取秒杀活动的时间段轴
     *
     * @param userId
     * @param shopId
     * @return
     */
    @GetMapping("/getMsActivityTime")
    public SimpleResponse getMsActivityTime(@RequestParam("userId") Long userId,
                                            @RequestParam(name = "dayId", required = false) Long dayId,
                                            @RequestParam(name = "batchId", required = false) Long batchId,
                                            @RequestParam("shopId") Long shopId) {
        return platformMsActivityService.getMsActivityTime(shopId, userId, dayId, batchId);
    }

    /***
     * 获取秒杀活动某个时间段 商品详情信息
     *
     * @param userId
     * @param dayId 第几天的数据
     * @param batchId 第几批次的数据
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/getMsActivityOneBatch")
    public SimpleResponse getMsActivityOneBatch(@RequestParam("userId") Long userId,
                                                @RequestParam("shopId") Long shopId,
                                                @RequestParam(name = "dayId", required = false) Long dayId,
                                                @RequestParam(name = "batchId", required = false) Long batchId,
                                                @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                                @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        return platformMsActivityService.getMsActivityOneBatch(userId, shopId, dayId, batchId, offset, size);
    }
}
