package mf.code.api.applet.service.SendTemplateMessage.impl.countDown;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.SendTemplateMessage.CountDownType;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMsgService;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * mf.code.api.applet.service.SendTemplateMessage.impl.award
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 10:53
 */
@Service
@Slf4j
public class CountDownTaskSendTemplateMsgConverterImpl extends SendTemplateMsgService {
    @Autowired
    private UserService userService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @Override
    public String countDownSendTemplateMessageConverter(Integer countDownType,
                                                        Long userID,
                                                        Long activityId,
                                                        Long goodsId,
                                                        UserTask userTask,
                                                        User user) {
        if (countDownType == CountDownType.COUNTDOWN_TASK.getCode()) {
            log.info("任务倒计时推送消息开始, activityId:{} userID:{} goodsId:{}", activityId, userID, goodsId);
            Goods goods = this.goodsService.selectById(goodsId);
            if (goods == null) {
                return null;
            }
            if(user == null){
                user = this.userService.selectByPrimaryKey(userID);
            }
            if (user == null) {
                log.warn("倒计时推送消息时该用户编号：{} 的用户不存在", userID);
                return null;
            }
            Activity activity = this.activityService.findById(activityId);
            if (activity == null) {
                log.warn("活动编号为：{}，推送消息时，对应的活动不存在", activityId);
                return null;
            }
            String templateID = wxmpProperty.getUserAwardResultMsgTmpId();
            Object[] objects = {"请尽快完成领奖", "《" + goods.getDisplayGoodsName() + "》" + "抽奖活动", "您已中奖，请尽快完成领奖，0.5小时后奖品将自动失效，无法领取！"};

            if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
                templateID = wxmpProperty.getAssembleSuccessMsgTmpId();
                objects = new Object[]{"请尽快完成领奖！", "《" + goods.getDisplayGoodsName() + "》新人有礼活动", "您已达到满团条件，请尽快完成领奖，2小时后奖品将自动失效，无法领取！"};
            }

            int keywordNum = objects.length;
            String formIDStr = this.queryRedisUserFormIds(userID);
            if (StringUtils.isBlank(formIDStr)) {
                log.warn("倒计时推送消息时该用户编号：{} 的formId不存在", userID);
                return null;
            }

            WxSendMsgVo vo = new WxSendMsgVo();
            vo.from(templateID, user.getOpenId(), formIDStr, this.getPageUrl(activity, userID, null), this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);

            Map<String, Object> stringObjectMap = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
            this.respMap(stringObjectMap, vo, userID);
            log.info("倒计时推送消息结果：{}, parms:{}", stringObjectMap, vo);
            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }
}
