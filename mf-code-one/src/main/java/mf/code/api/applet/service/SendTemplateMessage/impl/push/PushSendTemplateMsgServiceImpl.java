package mf.code.api.applet.service.SendTemplateMessage.impl.push;
/**
 * create by qc on 2019/3/1 0001
 */

import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMsgService;
import mf.code.api.seller.service.SellerWxmpService;
import mf.code.common.caller.wxmp.WeixinMpConstants;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.push.repo.po.PushHistory;
import mf.code.push.service.PushHistoryService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author gbf
 */
@Service
public class PushSendTemplateMsgServiceImpl extends SendTemplateMsgService {

    /**
     * 推送在场景值中的标识
     */
    public static String SCENE_PUSH_MARK_PUSHID = "pid";
    public static String SCENE_PUSH_MARK_USERID = "uid";
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private SellerWxmpService sellerWxmpService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private UserService userService;
    @Autowired
    private PushHistoryService pushHistoryService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @Async
    @Override
    public String sendWxPushRecall(Long merchantId, Long shopId, Long userId, Long pushId, Integer activityType) {
        User user = userService.selectByPrimaryKey(userId);
        //查询活动定义
        List<ActivityDef> activityDefList = sellerWxmpService.getActivityDef(merchantId, shopId, null);
        //获取场景码对应的业务值
        Long detailParameterValule = sellerWxmpService.getDetailParameterValule(activityDefList, merchantId, shopId, activityType);

        PushHistory pushHistoryById = pushHistoryService.getPushHistoryById(pushId);
        if (pushHistoryById == null) {
            return "为什么不能为空字符串";
        }
        Object[] objects = new Object[]{pushHistoryById.getRemind(), pushHistoryById.getDetail(), pushHistoryById.getExtra()};
        //填充消息模板关键词
        Map<String, WxSendMsgVo.TemplateData> templateDataMap = this.getTempteDate(objects.length, objects);
        //匹配小程序码的场景
        objects = sellerWxmpService.getActivityObjects(objects, merchantId, shopId, activityType, detailParameterValule);
        objects = new Object[]{StringUtil.join(objects, ","), SCENE_PUSH_MARK_USERID + userId, SCENE_PUSH_MARK_PUSHID + pushId};

        //创建消息体
        WxSendMsgVo vo = new WxSendMsgVo();
        //发送客服消息给用户
        vo.from(wxmpProperty.getOrderProessMsgTmpId(),
                user.getOpenId(),
                this.queryRedisUserFormIds(user.getId()),
                this.getPageUrlrelyOnScene(objects),
                templateDataMap,
                SendTemplateMsgService.EMPHASISKEYWORD1);
        Map<String, Object> stringObjectMapNewMan = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
        this.respMap(stringObjectMapNewMan, vo, userId);

        return "ok";
    }

    /**
     * 此处改为客户端声明的推送消息类型
     *
     * @param activityType
     * @return
     */
    private Integer getSceneCode(Integer activityType) {
        // sceneType:            1：回填订单 2:好评晒图 3：拆红包 4：助力 5：轮盘 6：收藏加购 7：免单抽奖 8：加权试用 9：加权浏览
        // activityType: 活动类型 1:回填订单2:好评3:拆红包4:助力5:轮盘6:收藏7:抽奖

        // 对应==>

        //             Integer SCENE_GOODS = 5;//商品场景生成的小程序码
        //             Integer SCENE_ACTIVITY_FILLORDER = 6;//回填订单活动场景生成的小程序码
        //             Integer SCENE_ACTIVITY_GOODCOMMENT = 7;//好评晒图活动场景生成的小程序码
        //             Integer SCENE_ACTIVITY_OPENREDPACK = 8;//拆红包活动场景生成的小程序码
        //             Integer SCENE_ACTIVITY_ASSIST = 9;//助力活动场景生成的小程序码
        //             Integer SCENE_ACTIVITY_LUCKYWHEEL = 10;//幸运大转盘活动场景生成的小程序码
        //             Integer SCENE_ACTIVITY_FAVCART = 11;//收藏加购活动场景生成的小程序码
        //             Integer SCENE_ACTIVITY_ORDERBACK = 12;//免单抽奖活动场景生成的小程序码
        if (activityType == 1) {
            return WeixinMpConstants.SCENE_ACTIVITY_FILLORDER;
        } else if (activityType == 2) {
            return WeixinMpConstants.SCENE_ACTIVITY_GOODCOMMENT;
        } else if (activityType == 3) {
            return WeixinMpConstants.SCENE_ACTIVITY_OPENREDPACK;
        } else if (activityType == 4) {
            return WeixinMpConstants.SCENE_ACTIVITY_ASSIST;
        } else if (activityType == 5) {
            return WeixinMpConstants.SCENE_ACTIVITY_LUCKYWHEEL;
        } else if (activityType == 6) {
            return WeixinMpConstants.SCENE_ACTIVITY_FAVCART;
        } else if (activityType == 7) {
            return WeixinMpConstants.SCENE_ACTIVITY_ORDERBACK;
        } else {
            return WeixinMpConstants.SCENE_SHOP;
        }
    }
}
