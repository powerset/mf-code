package mf.code.api.applet.v7.service;

import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.api.applet.dto.TaskV7Req;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.goods.repo.po.Goods;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserTask;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * mf.code.api.applet.v7.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月22日 16:42
 */
public interface AppletUserTaskV7AboutService {
    /***
     * 任务提交验证入参
     * @param taskV7Req
     * @param activityDef
     * @param activity
     * @param pics
     * @return
     */
    SimpleResponse checkValid(TaskV7Req taskV7Req, ActivityDef activityDef, Activity activity, List<String> pics);

    /***
     * 活动类 任务提交(免单，助力)
     * @param taskV7Req
     * @param activity
     * @return
     */
    SimpleResponse commitActiviyTaskAudit(TaskV7Req taskV7Req, Activity activity, List<String> pics);

    /***
     * 回填订单提交审核
     * @param taskV7Req
     * @return
     */
    SimpleResponse commitFillorderBackTaskAudit(TaskV7Req taskV7Req);

    /***
     * 定义类任务提交(收藏加购，回填，好评晒图)
     * @param taskV7Req
     * @param activityDef
     * @return
     */
    SimpleResponse commitActiviyDefTaskAudit(TaskV7Req taskV7Req, ActivityDef activityDef, List<String> pics);

    /***
     * 获取中奖类活动taskId(免单+助力)
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activity
     * @return
     */
    SimpleResponse getUserTaskIdByActivity(Long merchantId, Long shopId, Long userId, Activity activity);

    /***
     * 获取非中奖类taskId(回填，收藏加购，好评)
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activityDef
     * @return
     */
    SimpleResponse getUserTaskIdByActivityDef(Long merchantId, Long shopId, Long userId, ActivityDef activityDef, Long goodsId, String orderId, Integer source);

    /***
     * 查询用户任务-通过活动方式(中奖任务)
     * @param userTask
     * @param merchantShop
     * @param commissionResp
     * @param rate
     * @param costStr
     * @return
     */
    SimpleResponse queryUserTaskByActivity(UserTask userTask, MerchantShop merchantShop, CommissionResp commissionResp, BigDecimal rate, String costStr, Goods goods);

    /***
     * 查询用户任务-通过活动定义方式(非中奖任务)
     * @param userTask
     * @param merchantShop
     * @param commissionResp
     * @param rate
     * @param costStr
     * @return
     */
    SimpleResponse queryUserTaskByActivityDef(UserTask userTask, MerchantShop merchantShop, CommissionResp commissionResp, BigDecimal rate, String costStr, Goods goods);

    /***
     * 查看任务的有效性
     * @param userTask
     * @return
     */
    SimpleResponse compareUserTask(UserTask userTask);

    /***
     * 查看任务是否中奖类型
     * @param userTask
     * @return
     */
    boolean queryUserTaskTypeByAward(UserTask userTask);
}
