package mf.code.api.applet.v7.service;

import mf.code.api.applet.dto.TaskV7Req;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v7.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月22日 15:07
 */
public interface AppletUserTaskV7Service {
    /***
     * 查询任务
     * @param merchantId
     * @param shopId
     * @param userId
     * @param taskId
     * @return
     */
    SimpleResponse queryUserTask(Long merchantId, Long shopId, Long userId, Long taskId);

    /***
     * 任务提交
     * @param taskV7Req
     * @return
     */
    SimpleResponse commitUserTaskAudit(TaskV7Req taskV7Req);

    /***
     * 获取userTaskId
     * @param merchantId
     * @param shopId
     * @param userId
     * @param activityId
     * @param activityDefId
     * @return
     */
    SimpleResponse getUserTaskId(Long merchantId, Long shopId, Long userId, Long activityId, Long activityDefId, Long goodsId, String orderId, Integer source);

    /***
     * 任务的重新提交
     * @param merchantId
     * @param shopId
     * @param userId
     * @param taskId
     * @param retryCommitType 重新提交类型 1：失败重新提交 2：超时重新提交(此场景需要重置，startTime)
     * @return
     */
    SimpleResponse retryCommitUserTask(Long merchantId, Long shopId, Long userId, Long taskId, int retryCommitType);
}
