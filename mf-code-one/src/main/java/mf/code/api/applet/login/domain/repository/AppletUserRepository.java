package mf.code.api.applet.login.domain.repository;

import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.valueobject.InviteSpecification;
import mf.code.common.dto.WxUserSession;

/**
 * mf.code.api.applet.login.domain.repository
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-18 上午10:32
 */
public interface AppletUserRepository {

	/**
	 * 通过openid查找 或 根据 wxUserSession创建  一个对象
	 * @param wxUserSession
	 * @return
	 */
	AppletUserAggregateRoot updateOrSaveByWxUserSession(WxUserSession wxUserSession);

	/**
	 * 通过userid查找 一个 不带活动邀请记录 的对象
	 * @param userId
	 * @return
	 */
	AppletUserAggregateRoot findById(Long userId);

	/**
	 * 通过pubUserId 查找 一个 带此活动邀请记录 的对象
	 * @param pubUserId 邀请者id
	 * @param inviteSpecification 邀请规格
	 * @return
	 */
	AppletUserAggregateRoot findInviteHistoryByUserId(String pubUserId, InviteSpecification inviteSpecification);

	/**
	 * 通过subUserId 查找一个 此type邀请类型的 带有被邀请的记录 的对象
	 * @param subUserId 被邀请者id
	 * @param inviteSpecification 邀请规格
	 * @return
	 */
	AppletUserAggregateRoot findInvitedHistoryByUserId(String subUserId, InviteSpecification inviteSpecification);

	/**
	 * 获取平台用户
	 * @return
	 */
	AppletUserAggregateRoot findPlatform();

	/**
	 * 增装 活动用户的邀请事件
	 * @param appletUser
	 * @param appletActivity
	 */
	void addActivityInviteEvent(AppletUserAggregateRoot appletUser, AppletActivity appletActivity);
}
