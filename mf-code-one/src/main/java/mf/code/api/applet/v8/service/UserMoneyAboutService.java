package mf.code.api.applet.v8.service;

import mf.code.activity.repo.po.Activity;
import mf.code.common.repo.po.CommonDict;
import mf.code.goods.repo.po.Goods;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.repo.po.User;

import java.util.Map;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月18日 14:38
 */
public interface UserMoneyAboutService {

    /***
     * 查询提现记录
     * @param shopID
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    Map<String, Object> queryPresentOrder(Long merchantID, Long shopID, Long userID, int offset, int size);


    /***
     * 映射用户记录推送converter
     * @param upayWxOrder
     * @param payType
     * @param activityMap
     * @param goodsMap
     */
    Map<String, Object> covertUserMoneyTradeDetail(UpayWxOrder upayWxOrder,
                                                   Integer payType,
                                                   Map<Long, Activity> activityMap,
                                                   Map<Long, Goods> goodsMap,
                                                   CommonDict commonDict);

    /***
     * 映射用户购买商品记录converter
     * @param upayWxOrder
     * @param productGoodsShoppingMap
     */
    Map<String, Object> covertUserMoneyTradeDetailForProductGoodsShopping(UpayWxOrder upayWxOrder, int payType, Map<Long, Map> productGoodsShoppingMap);

    /***
     * 映射用户购买商品团员贡献记录converter
     * @param upayWxOrder
     * @param userMap
     */
    Map<String, Object> covertUserMoneyTradeDetailForContribution(UpayWxOrder upayWxOrder, int payType, Map<Long, User> userMap);

    /***
     * 店长、新手任务 映射
     * @param upayWxOrder
     * @param payType
     * @param userMap
     * @return
     */
    Map<String, Object> covertTaskTradeDetail(UpayWxOrder upayWxOrder, Integer payType, Map<Long, User> userMap);
}
