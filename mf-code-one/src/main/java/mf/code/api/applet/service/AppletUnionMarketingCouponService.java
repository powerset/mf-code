package mf.code.api.applet.service;

import mf.code.api.applet.dto.JKMFUserCouponDTO;
import mf.code.api.applet.dto.UserCouponQueryDTO;
import mf.code.common.simpleresp.SimpleResponse;

import java.math.BigDecimal;

/**
 * @author gel
 */
public interface AppletUnionMarketingCouponService {

    /**
     * 我的卡券
     * 根据优惠券的使用状态查询优惠券信息，status 1，未使用，2已使用，3以过期
     * <p>
     * 根据类型查询已拥有的优惠券信息
     *
     * @param userCouponQueryDTO 1，未使用，2已使用，3以过期
     * @return
     */
    SimpleResponse queryMyCoupon(UserCouponQueryDTO userCouponQueryDTO);

    /**
     * 查询可以使用的优惠券的金额
     *
     * @param shopId
     * @param userId
     * @param goodId
     * @param skuId
     * @param orderPrice
     * @return
     */
    JKMFUserCouponDTO queryCanApplyCoupon(Long shopId, Long userId, Long goodId, Long skuId, BigDecimal orderPrice);

    /**
     * 查看优惠券
     * <p>
     * 可用优惠群和不可用优惠券都只展示优惠券未使用的状态，已使用和已过期则不使用
     * <p>
     * 不可用优惠券是指优惠券关联的产品id和入参的产品id不一致，且优惠券的状态是未使用
     * <p>
     * 根据优惠券使用状况查询优惠券信息列表
     *
     * @param userCouponQueryDTO
     * @return
     */
    SimpleResponse queryJKMFCouponByStatusForOrder(UserCouponQueryDTO userCouponQueryDTO);
}
