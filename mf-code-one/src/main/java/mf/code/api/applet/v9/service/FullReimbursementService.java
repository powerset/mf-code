package mf.code.api.applet.v9.service;

import mf.code.activity.domain.applet.application.AppletAssistActivityServiceAbstract;
import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v9.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-02 15:33
 */
public interface FullReimbursementService extends AppletAssistActivityServiceAbstract {
    void deleteActivityAssistRedis(String redisKey);

    /***
     * 全额报销-- 创建活动页的前置过滤和数据支持
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    SimpleResponse activityCreateFilter(String merchantId, String shopId, String userId);

    /***
     * 弹幕
     * @param userId
     * @param shopId
     * @param merchantId
     * @return
     */
    SimpleResponse barrage(String merchantId, String shopId, String userId);

    /***
     * 报销活动弹窗-首页
     * @param shopId
     * @param userId
     * @param scenes
     * @return
     */
    SimpleResponse fullReimbursementPopup(Long shopId, Long userId, Integer scenes);
}
