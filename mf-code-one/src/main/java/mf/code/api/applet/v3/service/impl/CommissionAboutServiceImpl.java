package mf.code.api.applet.v3.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.enums.CheckpointTaskSpaceEnum;
import mf.code.api.applet.v3.service.CommissionAboutService;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.common.constant.RocketMqTopicTagEnum;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.IpUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.distribution.constant.PlatformOrderBizTypeEnum;
import mf.code.distribution.constant.PlatformOrderStatusEnum;
import mf.code.distribution.constant.PlatformOrderTypeEnum;
import mf.code.merchant.constants.WxTradeTypeEnum;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.PlatformOrder;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.PlatformOrderService;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.*;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v3.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月20日 14:12
 */
@Service
@Slf4j
public class CommissionAboutServiceImpl implements CommissionAboutService {
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired(required = false)
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private UserService userService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private PlatformOrderService platformOrderService;


    /****
     * 完成佣金任务-完成任务的存储
     * @param userPubJoin
     * @param commissionBO
     * @param commissionDisDto
     * @param merchantShop
     */
    @Override
    public void finishCheckPointNew(UserCoupon userCoupon, ActivityDef activityDef, UserPubJoin userPubJoin, CommissionBO commissionBO, CommissionDisDto commissionDisDto, MerchantShop merchantShop) {
        // 获取自己平台所有下级矿工的累计贡献总和
        //本人信息,未成为店长，缴税收益不计算在内
        User fromUser = userService.selectByPrimaryKey(commissionBO.getFromUserId());
        //计算下级缴税收益
        BigDecimal totalScottareProfit = calculateUserScottare(fromUser);
        // -_-||这里之前如果是刚创建的则会缺少字段，需要重新查询
        UserPubJoin pubJoin = userPubJoinService.getById(userPubJoin.getId());
        BigDecimal afterAmount = pubJoin.getTotalCommission().add(totalScottareProfit);
        BigDecimal beforeAmount = afterAmount.subtract(commissionBO.getCommission());
        //是否通关
        checkPointPass(true, commissionBO, merchantShop, beforeAmount, afterAmount);
        //自己完成任务的奖金存储
        saveUpayWxOrderAndUpdateBalance(activityDef, true, null, fromUser, commissionBO.getCommission(), merchantShop);

        // 本人缴税，上级是否能过关
        User toUser = userService.selectByPrimaryKey(pubJoin.getUserId());
        if (toUser.getGrantStatus().equals(UserConstant.GRANT_STATUS_PLAT)
                || commissionBO.getTax().equals(BigDecimal.ZERO)) {
            // 如果上级是平台则跳过
            savePlatformOrder(userCoupon);
            return;
        }

        // 获取上级平台财富值和贡献值
        Map<String, Object> params = new HashMap<>(4);
        params.put("subUid", commissionBO.getToUserId());
        params.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        // 查询该店铺下财富大闯关类型的数据记录，虽然只有绑定店铺才能抓取矿工，获得税金，但是其他店铺依旧可以自己做任务，自己收获佣金
        List<UserPubJoin> userPubJoins = userPubJoinService.listPageByParams(params);
        if (CollectionUtils.isEmpty(userPubJoins)) {
            // 获取上级的上级
            Map<String, Object> vipShopParams = new HashMap<>(4);
            vipShopParams.put("subUid", toUser.getId());
            vipShopParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            List<UserPubJoin> vipShopUserPubJoins = userPubJoinService.listPageByParams(vipShopParams);
            if (CollectionUtils.isEmpty(vipShopUserPubJoins)) {
                Map<String, Object> userParams = new HashMap<>(4);
                userParams.put("merchantId", 0);
                userParams.put("shopId", 0);
                userParams.put("grantStatus", UserConstant.GRANT_STATUS_PLAT);
                List<User> users = userService.listPageByParams(userParams);
                User platUser = users.get(0);
                userPubJoin = userPubJoinService.insertUserPubJoin(-1L, platUser.getId(), toUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            } else {
                userPubJoin = vipShopUserPubJoins.get(0);
                Long puid = userPubJoin.getUserId();
                userPubJoin = userPubJoinService.insertUserPubJoin(-1L, puid, toUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
            }
            pubJoin = userPubJoin;
        } else {
            pubJoin = userPubJoins.get(0);
        }

        //上级成为店长时,分配闯关佣金并记录
        if (toUser.getRole() == 1) {
            //上级的佣金存储
            saveUpayWxOrderAndUpdateBalance(activityDef, false, fromUser, toUser, commissionBO.getTax(), merchantShop);
            //下级缴税的收益
            BigDecimal toProfit = calculateUserScottare(toUser);
            //因为前面已经保存了数据，所以在此查出的数据应该是最后的金额
            BigDecimal toAfterAmount = pubJoin.getTotalCommission().add(toProfit);
            BigDecimal toBeforeAmount = toAfterAmount.subtract(commissionBO.getTax());
            //上级是否通关
            checkPointPass(false, commissionBO, merchantShop, toBeforeAmount, toAfterAmount);
        } else {
            //上级获得的佣金全部放入upay_wx_order_pend表
            saveUpayWxOrderPend(activityDef, fromUser, toUser, commissionBO.getTax(), merchantShop);
        }
    }

    /***
     * 完成任务的reids-佣金任务
     * @param commissionDisDto
     * @param commissionBO
     */
    @Override
    public void updateCheckPointRedisDataNew(ActivityDef activityDef, CommissionDisDto commissionDisDto, CommissionBO commissionBO) {
        if (commissionBO.getCommission().compareTo(BigDecimal.ZERO) == 0) {
            return;
        }
        // 5.1、本人今日收益（任务所得，税收所得），今日缴税 闯关今日信息 {"todayTaskProfit":"0.1","todayScottareProfit":"0.1","todayScottare":"0.1"}
        log.info("用户任务完成：" + JSON.toJSONString(commissionBO));
        saveTodayProfitRedis(true, commissionBO.getFromUserId(), commissionBO.getCommission(), commissionBO.getTax());

        //上级的今日收益(任务所得，税收所得)
        saveTodayProfitRedis(false, commissionBO.getToUserId(), commissionBO.getCommission(), commissionBO.getTax());

        // 5.2、一段时间总税收金额，暂定30天
        //矿工为上级产生的收益
        String minerprovideProfitKey = RedisKeyConstant.CHECKPOINT_MINERPROVIDE_PROFIT_TOTAL + commissionBO.getToUserId();
        String minerprovideProfitValue = stringRedisTemplate.opsForValue().get(minerprovideProfitKey);
        if (minerprovideProfitValue == null) {
            minerprovideProfitValue = "0.00";
        }
        minerprovideProfitValue = new BigDecimal(minerprovideProfitValue).add(commissionBO.getTax()).setScale(2, BigDecimal.ROUND_DOWN).toString();
        stringRedisTemplate.opsForValue().set(minerprovideProfitKey, minerprovideProfitValue, 30L, TimeUnit.DAYS);

        // 5.3、一段时间任务所得详情，暂定30天 [{"type":1,"amount":"0.1"}] 其中 type 从CheckpointTaskSpaceEnum枚举里取
        boolean newbieTask = activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode()
                || activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode();

        if (!newbieTask) {
            String usertaskFinishKey = RedisKeyConstant.CHECKPOINT_USERTASK_FINISH + commissionDisDto.getShopId() + ":" + commissionBO.getFromUserId();
            String usertaskFinishValue = stringRedisTemplate.opsForValue().get(usertaskFinishKey);
            JSONArray taskArr = JSON.parseArray(usertaskFinishValue);
            if (CollectionUtils.isEmpty(taskArr)) {
                taskArr = new JSONArray();
            }
            Map<String, Object> map = new HashMap<>(2);
            map.put("type", activityDef.getType());
            map.put("amount", commissionBO.getCommission());
            taskArr.add(map);
            stringRedisTemplate.opsForValue().set(usertaskFinishKey, JSON.toJSONString(taskArr), 30L, TimeUnit.DAYS);
        }

        // 5.4、关卡完成
        if (commissionBO.isFromFinish()) {
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.CHECKPOINT_FINISH_PLATFORM + commissionBO.getFromUserId(),
                    commissionBO.getFromLevel(), 30L, TimeUnit.DAYS);
        }
        if (commissionBO.isToFinish()) {
            stringRedisTemplate.opsForValue().set(RedisKeyConstant.CHECKPOINT_FINISH_PLATFORM + commissionBO.getFromUserId(),
                    commissionBO.getToLevel(), 30L, TimeUnit.DAYS);
        }
        //5.5提现弹窗准备
        canCashDialogRedisData(activityDef.getId(), commissionBO.getFromUserId());
    }

    /***
     * 今日完成任务的redis存储
     *
     * @param self 是否是自己
     * @param userId 用户id
     * @param commission 获得的佣金
     * @param tax 缴税金额
     */
    @Override
    public void saveTodayProfitRedis(boolean self, Long userId, BigDecimal commission, BigDecimal tax) {
        String todayinfoKey = RedisKeyConstant.CHECKPOINT_TODAYINFO_TOTAL + userId;
        String toUserTodayInfo = stringRedisTemplate.opsForValue().get(todayinfoKey);
        JSONObject toUserToday = JSON.parseObject(toUserTodayInfo);
        if (CollectionUtils.isEmpty(toUserToday)) {
            toUserToday = new JSONObject();
            toUserToday.put("todayTaskProfit", "0.00");
            toUserToday.put("todayScottareProfit", "0.00");
            toUserToday.put("todayScottare", "0.00");
        }
        if (self) {
            toUserToday.put("todayTaskProfit", new BigDecimal(toUserToday.getString("todayTaskProfit")).add(commission).setScale(2, BigDecimal.ROUND_DOWN).toString());
            toUserToday.put("todayScottare", new BigDecimal(toUserToday.getString("todayScottare")).add(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
        } else {
            toUserToday.put("todayScottareProfit", new BigDecimal(toUserToday.getString("todayScottareProfit")).add(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        stringRedisTemplate.opsForValue().set(todayinfoKey, JSON.toJSONString(toUserToday), DateUtil.getTimeoutSecond(), TimeUnit.SECONDS);
    }

    /***
     * 非任务 活动的佣金分配
     * @param activityDef
     * @param commissionBO
     * @param commissionDisDto
     * @param fromUser
     */
    @Override
    public void finishNotTaskDefType(ActivityDef activityDef, CommissionDisDto commissionDisDto, CommissionBO commissionBO, User fromUser) {
        //获取平台用户
        Map<String, Object> params = new HashMap<>(4);
        params.put("merchantId", 0);
        params.put("shopId", 0);
        params.put("grantStatus", UserConstant.GRANT_STATUS_PLAT);
        List<User> users = userService.listPageByParams(params);
        User platUser = users.get(0);

        params.clear();
        // 查找闯关记录
        UserPubJoin userPubJoin;
        //查询平台user_pub_join是否存在
        Map<String, Object> vipShopParams = new HashMap<>(4);
        vipShopParams.put("userId", platUser.getId());
        vipShopParams.put("subUid", fromUser.getId());
        vipShopParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        vipShopParams.put("shopId", fromUser.getVipShopId());
        List<UserPubJoin> vipShopUserPubJoins = userPubJoinService.listPageByParams(vipShopParams);
        if (CollectionUtils.isEmpty(vipShopUserPubJoins)) {
            userPubJoin = userPubJoinService.insertUserPubJoin(-1L, platUser.getId(), fromUser, UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        } else {
            userPubJoin = vipShopUserPubJoins.get(0);
        }
        //给钱给平台用户
        userPubJoinService.updatePubJoinCommission(userPubJoin.getId(), BigDecimal.ZERO, activityDef.getCommission());
    }

    /***
     * 提现的弹窗相关
     * @param activityDefId
     * @param userId
     */
    @Override
    public void canCashDialogRedisData(Long activityDefId, Long userId) {
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        BizTypeEnum bizTypeByDefType = BizTypeEnum.findBizTypeByDefType(activityDef.getType());
        // 用户自己完成金额组成
        if (activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            bizTypeByDefType = BizTypeEnum.NEWBIE_TASK_OPENREDPACK;
        }
        //提现成功后的弹窗存储
        String canCashRedisKey = mf.code.one.redis.RedisKeyConstant.FINISH_DONE_TASK_ZSET + userId;
        Long aLong = stringRedisTemplate.opsForZSet().zCard(canCashRedisKey);
        String value = String.valueOf(bizTypeByDefType.getCode());
        if (aLong != null && aLong > 0) {
            //更新
            stringRedisTemplate.opsForZSet().incrementScore(canCashRedisKey, value, 1);
        } else {
            //存储
            stringRedisTemplate.opsForZSet().add(canCashRedisKey, value, 1);
        }
    }

    /***
     * 完成任务时 的金额去向 存储
     * @param self
     * @param user
     * @param amount
     * @param merchantShop
     */
    @Override
    public void saveUpayWxOrderAndUpdateBalance(ActivityDef activityDef, boolean self, User fromUser, User user, BigDecimal amount, MerchantShop merchantShop) {
        BizTypeEnum bizTypeByDefType = BizTypeEnum.findBizTypeByDefType(activityDef.getType());
        // 用户自己完成金额组成
        if (activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            bizTypeByDefType = BizTypeEnum.NEWBIE_TASK_OPENREDPACK;
        }
        Long bizValue = activityDef.getId();
        if (fromUser != null && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
            bizValue = fromUser.getId();
        }
        UpayWxOrder upayWxOrderUpper = upayWxOrderService.addPo(user.getId(), OrderPayTypeEnum.CASH.getCode(),
                self ? OrderTypeEnum.PALTFORMRECHARGE.getCode() : OrderTypeEnum.REBATE.getCode(), null, bizTypeByDefType.getMessage(),
                merchantShop.getMerchantId(), merchantShop.getId(), OrderStatusEnum.ORDERED.getCode(), amount,
                IpUtil.getServerIp(), null, self ? "任务奖金" : "任务佣金", new Date(), null, null,
                bizTypeByDefType.getCode(), bizValue);
        upayWxOrderService.create(upayWxOrderUpper);
        // 更新用户余额
        upayBalanceService.updateOrCreate(merchantShop.getMerchantId(), merchantShop.getId(), user.getId(), amount);

        //上级获得佣金之后，消息推送
        if (!self) {
            SimpleResponse simpleResponse = distributionAppService.sendTemplateMessage(user.getNickName(), user.getId(), amount.toString(), bizTypeByDefType.getMessage());
            if (simpleResponse != null) {
                log.info("<<<<<<<<上级获得佣金的推送 simpleResponse：{}", JSONObject.toJSONString(simpleResponse));
            }
        }
    }


    /***
     * 验证是否通关
     * @param self
     * @param commissionBO
     * @param merchantShop
     * @param beforeAmount
     * @param afterAmount
     */
    private void checkPointPass(boolean self, CommissionBO commissionBO, MerchantShop merchantShop, BigDecimal beforeAmount, BigDecimal afterAmount) {
        // 获取公共配置
        CommonDict byTypeKey = commonDictService.selectByTypeKey("checkpoint", "level");
        List<Map> array = JSONObject.parseArray(byTypeKey.getValue(), Map.class);
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (Map map : array) {
            // 当前关卡金额即为待发放金额
            BigDecimal levelAmount = new BigDecimal(map.get("money").toString());
            totalAmount = totalAmount.add(levelAmount);
            if (totalAmount.compareTo(beforeAmount) > 0 && totalAmount.compareTo(afterAmount) <= 0) {
                if (self) {
                    commissionBO.setFromFinish(true);
                    commissionBO.setFromLevel(JSON.toJSONString(map));
                } else {
                    commissionBO.setToFinish(true);
                    commissionBO.setToLevel(JSON.toJSONString(map));
                }
                log.info("<<<<<<<<用户关卡升级：" + JSON.toJSONString(commissionBO));
                //消息推送
                Map<String, Object> tempMap = new HashMap<>();
                tempMap.put("merchantId", merchantShop.getMerchantId());
                tempMap.put("shopId", merchantShop.getId());
                tempMap.put("userId", commissionBO.getFromUserId());
                tempMap.put("finishNum", map.get("name"));
                tempMap.put("commonDict", byTypeKey);
                try {
                    this.rocketMQTemplate.syncSend(RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTopic() + ":"
                            + RocketMqTopicTagEnum.TEMPLATE_CHECKPOINT.getTag(), JSON.toJSONString(tempMap));
                    log.info("rocketmq消息生产：" + JSON.toJSONString(tempMap));
                } catch (Exception e) {
                    log.error("rocketmq消息生产错误：" + JSON.toJSONString(tempMap), e);
                }
                break;
            }
        }
    }

    /***
     * 获取用户的缴税收益-
     * 需要做下是否成为店长的判断
     * @param user
     * @return
     */
    private BigDecimal calculateUserScottare(User user) {
        Map<String, Object> totalScottareParams = new HashMap<>();
        totalScottareParams.put("userId", user.getId());
        totalScottareParams.put("type", UserPubJoinTypeEnum.SHOPPINGMALL.getCode());
        BigDecimal totalScottareProfit = this.userPubJoinService.sumByTotalScottare(totalScottareParams);
        if (user.getRole() != 1) {
            return BigDecimal.ZERO;
        }
        return totalScottareProfit;
    }

    /***
     * 完成任务时 给上级 的金额去向 存储
     * @param user
     * @param amount
     * @param merchantShop
     */
    private void saveUpayWxOrderPend(ActivityDef activityDef, User fromUser, User user, BigDecimal amount, MerchantShop merchantShop) {
        BizTypeEnum bizTypeByDefType = BizTypeEnum.findBizTypeByDefType(activityDef.getType());
        // 用户自己完成金额组成
        if (activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
            bizTypeByDefType = BizTypeEnum.NEWBIE_TASK_OPENREDPACK;
        }
        Long bizValue = activityDef.getId();
        if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
            bizValue = fromUser.getId();
        }
        Date now = new Date();
        UpayWxOrderReq upayWxOrder = new UpayWxOrderReq();
        upayWxOrder.setMchId(merchantShop.getMerchantId());
        upayWxOrder.setShopId(merchantShop.getId());
        upayWxOrder.setUserId(user.getId());
        upayWxOrder.setType(UpayWxOrderTypeEnum.REBATE.getCode());
        upayWxOrder.setPayType(UpayWxOrderPayTypeEnum.CASH.getCode());
        upayWxOrder.setOrderNo("V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis());
        upayWxOrder.setOrderName(bizTypeByDefType.getMessage());
        upayWxOrder.setAppletAppId("");
        upayWxOrder.setStatus(UpayWxOrderStatusEnum.ORDERED.getCode());
        upayWxOrder.setTotalFee(amount);
        upayWxOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
        upayWxOrder.setIpAddress(IpUtil.getServerIp());
        upayWxOrder.setTradeId("");
        upayWxOrder.setRemark("任务佣金");
        upayWxOrder.setNotifyTime(now);
        upayWxOrder.setPaymentTime(now);
        upayWxOrder.setCtime(now);
        upayWxOrder.setUtime(now);
        upayWxOrder.setReqJson(null);
        upayWxOrder.setBizType(bizTypeByDefType.getCode());
        upayWxOrder.setBizValue(bizValue);
        upayWxOrder.setJsapiScene(JsapiSceneEnum.APPLETJSAPI.getCode());
        upayWxOrder.setRefundOrderId(0L);

        List<UpayWxOrderReq> reqs = new ArrayList<>();
        reqs.add(upayWxOrder);
        SimpleResponse simpleResponse = distributionAppService.saveUpayWxOrderPend(reqs);
        log.info("<<<<<<<< 存储pend 表 simpleResponse:{}", JSONObject.toJSONString(simpleResponse));
    }

    /***
     * 上级为平台时 存储此刻的佣金
     * @param userCoupon
     */
    private void savePlatformOrder(UserCoupon userCoupon) {
        Map<String, Object> jsonMap = JSONObject.parseObject(userCoupon.getCommissionDef(), Map.class);
        BigDecimal commissionTax = BigDecimal.ZERO;
        if (jsonMap != null && StringUtils.isNotBlank(jsonMap.get("tax").toString())) {
            commissionTax = new BigDecimal(jsonMap.get("tax").toString());
        }

        if (commissionTax.compareTo(BigDecimal.ZERO) <= 0) {
            return;
        }
        PlatformOrderBizTypeEnum platformOrderBizTypeEnum = PlatformOrderBizTypeEnum.findByUserCouponType(userCoupon.getType());
        //是平台,补充平台的订单
        PlatformOrder platformOrder = new PlatformOrder();
        platformOrder.setMerchantId(userCoupon.getMerchantId());
        platformOrder.setShopId(userCoupon.getShopId());
        platformOrder.setUserId(userCoupon.getUserId());
        platformOrder.setType(PlatformOrderTypeEnum.PALTFORM_PRESTORE.getCode());
        platformOrder.setPayType(1);
        platformOrder.setOrderNo("V5" + RandomStrUtil.randomStr(6) + System.currentTimeMillis());
        platformOrder.setOrderName(platformOrderBizTypeEnum.getDesc());
        platformOrder.setPubAppId("");
        platformOrder.setStatus(PlatformOrderStatusEnum.SUCCESS.getCode());
        platformOrder.setTotalFee(commissionTax);
        platformOrder.setTradeType(0);
        platformOrder.setTradeId("");
        platformOrder.setRemark(null);
        platformOrder.setNotifyTime(new Date());
        platformOrder.setPaymentTime(new Date());
        platformOrder.setCtime(new Date());
        platformOrder.setUtime(new Date());
        platformOrder.setReqJson(null);
        platformOrder.setBizType(platformOrderBizTypeEnum.getCode());
        platformOrder.setBizValue(userCoupon.getId());
        platformOrderService.save(platformOrder);
    }


    private CheckpointTaskSpaceEnum changeUserCouponToCheckpoint(UserCouponTypeEnum userCouponType) {
        switch (userCouponType) {
            case LUCKY_WHEEL:
                return CheckpointTaskSpaceEnum.LUCKYWHEEL;
            case ORDER_RED_PACKET:
                return CheckpointTaskSpaceEnum.OPENREDPACK;
            case COUPON:
                break;
            case GOODS_COMMENT:
                return CheckpointTaskSpaceEnum.GOODCOMMENT;
            case FAV_CART:
                return CheckpointTaskSpaceEnum.FAVCART;
            case MCH_PLAN:
                break;
            case ORDER_BACK_START:
            case ORDER_BACK_JOIN:
            case NEW_MAN_START:
            case NEW_MAN_JOIN:
                return CheckpointTaskSpaceEnum.ORDERBACK;
            case ASSIST:
                return CheckpointTaskSpaceEnum.ASSIST;
            case OPEN_RED_PACKAGE_START:
            case OPEN_RED_PACKAGE_ASSIST:
                return CheckpointTaskSpaceEnum.OPENREDPACK;
            default:
                break;
        }
        return CheckpointTaskSpaceEnum.ORDERBACK;
    }
}
