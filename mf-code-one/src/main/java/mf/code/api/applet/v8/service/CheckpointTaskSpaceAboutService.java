package mf.code.api.applet.v8.service;

import mf.code.api.applet.v8.dto.CheckpointTaskSpaceDTO;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;

import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 18:14
 */
public interface CheckpointTaskSpaceAboutService {
    /***
     * 新手任务详细
     * @param tasks
     * @param user
     */
    void queryNewbieTaskDetail(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, List<CheckpointTaskSpaceDTO.TaskDTO> tasks, User user);

    /***
     * 店长任务详细
     * @param checkpointTaskSpaceDTO
     * @param tasks
     * @param user
     */
    void queryShopManagerTaskDetail(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, List<CheckpointTaskSpaceDTO.TaskDTO> tasks, User user);

    /***
     * 日常任务详细
     * @param tasks
     * @param merchantId
     * @param shopId
     * @param userId
     */
    void queryDailyTaskDetail(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, List<CheckpointTaskSpaceDTO.TaskDTO> tasks, Long merchantId, Long shopId, Long userId);

    /***
     * 查询活动定义信息-中奖信息
     * @param merchantId
     * @param shopId
     * @param defIds
     * @return
     */
    Map<Long, List<UserCoupon>> queryActivityDefInfo(Long merchantId, Long shopId, Long userId, List<Long> defIds, List<Integer> types);

    /***
     * 获取已赚收益
     * @param checkpointTaskSpaceDTO
     * @param merchantId
     * @param shopId
     * @param user
     */
    void queryFinishProfit(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, Long merchantId, Long shopId, User user);

    /***
     * 弹窗信息
     * @param checkpointTaskSpaceDTO
     * @param merchantId
     * @param shopId
     * @param user
     */
    void queryDialog(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, Long merchantId, Long shopId, User user);
}
