package mf.code.api.applet.v3.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.activity.constant.*;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.*;
import mf.code.api.AppletMybatisPageDto;
import mf.code.api.applet.service.impl.AppletUserActivityServiceImpl;
import mf.code.api.applet.v3.dto.CheckpointBarrageResp;
import mf.code.api.applet.v3.dto.CheckpointTaskSpaceResp;
import mf.code.api.applet.v3.enums.CheckpointTaskSpaceEnum;
import mf.code.api.applet.v3.service.CheckpointTaskSpaceService;
import mf.code.api.applet.v6.service.OpenRedPackageService;
import mf.code.common.constant.*;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.RobotUserUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v3.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月12日 11:20
 */
@Service
@Slf4j
public class CheckpointTaskSpaceServiceImpl implements CheckpointTaskSpaceService {
    @Autowired
    private ActivityDefTypeService activityDefTypeService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private AppletUserActivityServiceImpl appletUserActivityServiceImpl;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityCommissionService activityCommissionService;
    @Autowired
    private OpenRedPackageService openRedPackageService;


    /***
     *
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse queryCheckpointTaskSpace(Long merchantId, Long shopId, Long userId) {

        List<ActivityDef> activityDefList = this.getActivityDef(merchantId, shopId, userId, null);
        if (CollectionUtils.isEmpty(activityDefList)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该商户没有创建任何活动");
        }
        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        List<ActivityTask> activityTasks = this.getActivityTasks(merchantId, shopId, userId, null);
//        //拆红包
//        ActivityDef activityDefOpenRedPack = this.activityDefTypeService.getOpenRedPack(activityDefList);
        //幸运大转盘
        ActivityDef activityDefLuckyWheel = this.activityDefTypeService.getLuckyWheel(activityDefList);
        //收藏加购
        ActivityDef activityDefFavCart = this.activityDefTypeService.getFavCart(activityDefList);
        //好评晒图
        ActivityDef activityDefGoodsComment = this.activityDefTypeService.getGoodsComment(activityDefList);
        //回填订单
        ActivityDef activityDefFillOrder = this.activityDefTypeService.getFillOrder(activityDefList);
        //赠品
        ActivityDef activityDefAssist = this.activityDefTypeService.getAssist(activityDefList);
        //免单抽奖
        ActivityDef activityDefOrderBack = this.activityDefTypeService.getOrderBack(activityDefList);

        //好评晒图新版
        ActivityDef activityDefGoodsCommentNew = this.activityDefTypeService.getGoodsCommentNew(activityDefList);
        //免单抽奖新版
        ActivityDef activityDefOrderBackNew = this.activityDefTypeService.getOrderBackNew(activityDefList);
        //赠品新版
        ActivityDef activityDefAssistNew = this.activityDefTypeService.getAssistNew(activityDefList);

        CheckpointTaskSpaceResp checkpointTaskSpaceResp = new CheckpointTaskSpaceResp();
        checkpointTaskSpaceResp.setList(new ArrayList<CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail>());
        //拆红包活动改版，不做任务空间展示  2019-05-17 13:48
//        if (activityDefOpenRedPack != null) {
//            int successOpenRedPack = countByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.OPEN_RED_PACKAGE_START.getCode());
//            BigDecimal amountEarn = activityDefOpenRedPack.getCommission().multiply(new BigDecimal(successOpenRedPack));
//            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
//            Long activityId = null;
//            Activity activity = this.getActivity(merchantId, shopId, activityDefOpenRedPack.getId(), ActivityTypeEnum.OPEN_RED_PACKET_START.getCode());
//            if (activity != null) {
//                activityId = activity.getId();
//            }
//            detail.from(activityDefOpenRedPack.getId(), activityId, 0, CheckpointTaskSpaceEnum.OPENREDPACK.getCode(),
//                    amountEarn, null, rate);
//            checkpointTaskSpaceResp.getList().add(detail);
//        }
        if (activityDefGoodsCommentNew != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            int taskNum = activityDefGoodsCommentNew.getStock();
            BigDecimal commission = BigDecimal.ZERO;
            if (activityDefGoodsCommentNew.getCommission() != null) {
                commission = activityDefGoodsCommentNew.getCommission();
            }
            detail.from(activityDefGoodsCommentNew.getId(), null, taskNum, CheckpointTaskSpaceEnum.GOOD_COMMENT_V2.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.GOODS_COMMENT.getCode()),
                    getTotalCommissionByLast(taskNum, commission), rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefOrderBackNew != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
            List<ActivityDef> activityDefs = getActivityDef(merchantId, shopId, userId, types);
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.ORDER_BACK_START_V2.getCode(), goodsIds);
            int totalStock = 0;
            BigDecimal totalCommission = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    UserTask userTask = userTaskMap.get(activityDef.getGoodsId());
                    if (userTask == null || (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode())) {
                        totalStock++;
                        totalCommission = totalCommission.add(activityDef.getCommission());
                    }
                }
            }
            detail.from(activityDefOrderBackNew.getId(), null, totalStock, CheckpointTaskSpaceEnum.ORDERBACK.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ORDER_BACK_START.getCode()),
                    totalCommission, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefAssistNew != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ASSIST_V2.getCode());
            List<ActivityDef> activityDefs = getActivityDef(merchantId, shopId, userId, types);
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.ASSIST_START_V2.getCode(), goodsIds);
            int totalStock = 0;
            BigDecimal totalCommission = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    UserTask userTask = userTaskMap.get(activityDef.getGoodsId());
                    if (userTask == null || (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode())) {
                        totalStock++;
                        totalCommission = totalCommission.add(activityDef.getCommission());
                    }
                }
            }
            detail.from(activityDefAssistNew.getId(), null, totalStock, CheckpointTaskSpaceEnum.ASSIST.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ASSIST.getCode()),
                    totalCommission, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefLuckyWheel != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            Long activityId = null;
            Activity activity = this.getActivity(merchantId, shopId, activityDefLuckyWheel.getId(), null,
                    ActivityTypeEnum.LUCK_WHEEL.getCode(), ActivityStatusEnum.PUBLISHED.getCode());
            if (activity != null) {
                activityId = activity.getId();
            }
            int taskNum = 0;
            Map<String, Object> map = this.appletUserActivityServiceImpl.getTimesAndChance(shopId, userId);
            if (map != null && map.get("times") != null) {
                taskNum = (int) map.get("times");
            }
            detail.from(activityDefLuckyWheel.getId(), activityId, taskNum, CheckpointTaskSpaceEnum.LUCKYWHEEL.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.LUCKY_WHEEL.getCode()),
                    getTotalCommissionByLast(1, activityDefLuckyWheel.getDeposit()), rate);
            if (activity != null) {
                checkpointTaskSpaceResp.getList().add(detail);
            }
        }
        if (activityDefFavCart != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<ActivityTask> activityTasks1 = getActivityTasks(merchantId, shopId, userId, ActivityTaskTypeEnum.FAVCART.getCode());
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityTasks1)) {
                for (ActivityTask activityTask : activityTasks1) {
                    if (goodsIds.indexOf(activityTask.getGoodsId()) == -1) {
                        goodsIds.add(activityTask.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode(), goodsIds);
            int taskNum = 0;
            BigDecimal amountLastSingle = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityTasks1)) {
                taskNum = activityTasks1.size() - userTaskMap.size();
                amountLastSingle = activityTasks1.get(0).getRpAmount();
            }
            detail.from(null, null, taskNum, CheckpointTaskSpaceEnum.FAVCART.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.FAV_CART.getCode()),
                    getTotalCommissionByLast(taskNum, amountLastSingle), rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefGoodsComment != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<ActivityTask> activityTasks1 = getActivityTasks(merchantId, shopId, userId, ActivityTaskTypeEnum.GOOD_COMMENT.getCode());
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityTasks1)) {
                for (ActivityTask activityTask : activityTasks1) {
                    if (goodsIds.indexOf(activityTask.getGoodsId()) == -1) {
                        goodsIds.add(activityTask.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.GOOD_COMMENT.getCode(), goodsIds);
            int taskNum = 0;
            BigDecimal amountLastSingle = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityTasks1)) {
                taskNum = activityTasks1.size() - userTaskMap.size();
                amountLastSingle = activityTasks1.get(0).getRpAmount();
            }

            CheckpointTaskSpaceEnum goodcomment = CheckpointTaskSpaceEnum.GOODCOMMENT;
            if (activityDefGoodsComment.getType() == ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()) {
                goodcomment = CheckpointTaskSpaceEnum.GOOD_COMMENT_V2;
            }
            detail.from(null, null, taskNum, goodcomment.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.GOODS_COMMENT.getCode()),
                    getTotalCommissionByLast(taskNum, amountLastSingle), rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefFillOrder != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
//            List<ActivityTask> activityTasks1 = getActivityTasks(merchantId, shopId, userId, ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode());
//            int taskNum = 0;
//            BigDecimal amountLastSingle = BigDecimal.ZERO;
//            if (!CollectionUtils.isEmpty(activityTasks1)) {
//                taskNum = activityTasks1.size();
//                amountLastSingle = activityTasks1.get(0).getRpAmount();
//            }
            detail.from(null, null, -1, CheckpointTaskSpaceEnum.FILLORDER.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ORDER_RED_PACKET.getCode()),
                    activityDefFillOrder.getDeposit(), rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefAssist != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ASSIST.getCode());
            List<ActivityDef> activityDefs = getActivityDef(merchantId, shopId, userId, types);
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.ASSIST_START.getCode(), goodsIds);
            int totalStock = 0;
            BigDecimal totalCommission = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    UserTask userTask = userTaskMap.get(activityDef.getGoodsId());
                    if (userTask == null || (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode())) {
                        totalStock = totalStock + 1;
                        totalCommission = totalCommission.add(activityDef.getCommission());
                    }
                }
            }
            detail.from(activityDefAssist.getId(), null, totalStock, CheckpointTaskSpaceEnum.ASSIST.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ASSIST.getCode()),
                    totalCommission, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefOrderBack != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ORDER_BACK.getCode());
            List<ActivityDef> activityDefs = getActivityDef(merchantId, shopId, userId, types);
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.ORDER_BACK_START.getCode(), goodsIds);
            int totalStock = 0;
            BigDecimal totalCommission = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    UserTask userTask = userTaskMap.get(activityDef.getGoodsId());
                    if (userTask == null || (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode())) {
                        totalStock = totalStock + 1;
                        totalCommission = totalCommission.add(activityDef.getCommission());
                    }
                }
            }
            detail.from(activityDefOrderBack.getId(), null, totalStock, CheckpointTaskSpaceEnum.ORDERBACK.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ORDER_BACK_START.getCode()),
                    totalCommission, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(checkpointTaskSpaceResp);
        return simpleResponse;
    }

    /***
     * 选择商品详细
     * @param merchantId
     * @param shopID
     * @param type 1.收藏加购 2.好评晒图 3.赠品 4.免单抽奖 5.回填订单
     * @return
     */
    @Override
    public SimpleResponse queryGoodsDetail(Long merchantId, Long shopID, Long userId, int type, int offset, int size) {
        List<Long> goodsIds = new ArrayList<>();
        Map<Long, ActivityTask> activityTaskMap = new HashMap<>();
        Map<Long, ActivityDef> activityDefMap = new HashMap<>();
        Map<Long, Activity> activityMap = new HashMap<>();
        Map<Long, UserTask> userTaskMap = new HashMap<>();
        if (type == 1 || type == 2 || type == 5) {
            List<ActivityTask> activityTasks = null;
            if (type == 1) {
                activityTasks = this.getActivityTasks(merchantId, shopID, userId, ActivityTaskTypeEnum.FAVCART.getCode());
            }
            if (type == 2) {
                activityTasks = this.getActivityTasks(merchantId, shopID, userId, ActivityTaskTypeEnum.GOOD_COMMENT.getCode());
                if (CollectionUtils.isEmpty(activityTasks)) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "好评晒图是全店商品，直接跳转任务页...");
                }
            }
            if (type == 5) {
                new SimpleResponse<>(ApiStatusEnum.ERROR_BUS_NO1, "回填订单不需调用此接口");
            }
            if (!CollectionUtils.isEmpty(activityTasks)) {
                for (ActivityTask activityTask : activityTasks) {
                    if (goodsIds.indexOf(activityTask.getGoodsId()) == -1) {
                        goodsIds.add(activityTask.getGoodsId());
                    }
                    activityTaskMap.put(activityTask.getGoodsId(), activityTask);
                }
            }
            if (type == 1) {
                userTaskMap = this.getUserTasks(merchantId, shopID, userId, UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode(), goodsIds);
            }
            if (type == 2) {
                userTaskMap = this.getUserTasks(merchantId, shopID, userId, UserTaskTypeEnum.GOOD_COMMENT.getCode(), goodsIds);
            }
        } else if (type == 3 || type == 4) {
            List<ActivityDef> activityDefList = null;
            if (type == 3) {
                List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ASSIST.getCode(), ActivityDefTypeEnum.ASSIST_V2.getCode());
                activityDefList = this.getActivityDef(merchantId, shopID, userId, types);
            }
            if (type == 4) {
                List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ORDER_BACK.getCode(), ActivityDefTypeEnum.ORDER_BACK_V2.getCode());
                activityDefList = this.getActivityDef(merchantId, shopID, userId, types);
            }
            if (!CollectionUtils.isEmpty(activityDefList)) {
                for (ActivityDef activityDef : activityDefList) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                    activityDefMap.put(activityDef.getGoodsId(), activityDef);
                }
            }
            if (type == 3) {
                List<Integer> assistTypes = Arrays.asList(ActivityTypeEnum.ASSIST.getCode(), ActivityTypeEnum.ASSIST_V2.getCode());
                activityMap = this.getActivitys(merchantId, shopID, userId, assistTypes, goodsIds);
            }
            if (type == 4) {
                activityMap = new HashMap<>();
            }
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        AppletMybatisPageDto<Object> goodsInfoRespMybatisPageDto = new AppletMybatisPageDto<Object>();
        goodsInfoRespMybatisPageDto.from(size, offset, 0);
        if (CollectionUtils.isEmpty(goodsIds)) {
            simpleResponse.setData(goodsInfoRespMybatisPageDto);
            return simpleResponse;
        }
        //获取页码
        int pageNum = offset / size + 1;
        Page<Goods> page = new Page<>(pageNum, size);
        QueryWrapper<Goods> goodsWrapper = new QueryWrapper<>();
        goodsWrapper.lambda()
                .eq(Goods::getMerchantId, merchantId)
                .eq(Goods::getShopId, shopID)
                .eq(Goods::getDel, DelEnum.NO.getCode())
                .in(Goods::getId, goodsIds)
                .orderByDesc(Goods::getId)
        ;
        IPage<Goods> goodsIPage = goodsService.page(page, goodsWrapper);
        List<Goods> goodsList = goodsIPage.getRecords();

        if (CollectionUtils.isEmpty(goodsList)) {
            simpleResponse.setData(goodsInfoRespMybatisPageDto);
            return simpleResponse;
        }

        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        goodsInfoRespMybatisPageDto.setContent(new ArrayList<>());
        for (Goods goods : goodsList) {
            Map<String, Object> map = BeanMapUtil.beanToMapIgnore(goods, "parentId", "propertiesName", "topTime", "onsale", "top", "ctime", "utime", "del", "xxbtopReq", "xxbtopRes", "xxbtopDesc", "xxbtopNumIid", "srcType", "xxbtopPicUrl", "descPath", "displayBanner", "xxbtopTitle", "categoryId", "xxbtopPrice");
            map.put("activityDefType", 0);
            ActivityDef activityDef = activityDefMap.get(goods.getId());
            ActivityTask activityTask = activityTaskMap.get(goods.getId());
            Activity activity = activityMap.get(goods.getId());
            UserTask userTask = userTaskMap.get(goods.getId());
            if (activityDef != null) {
                BigDecimal tax = activityDef.getCommission().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
                map.put("commission", activityDef.getCommission().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
                map.put("activityDefId", activityDef.getId());
                map.put("activityDefType", activityDef.getType());
            }
            if (activityTask != null) {
                BigDecimal tax = activityTask.getRpAmount().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);
                map.put("commission", activityTask.getRpAmount().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
            }
            if (activity != null) {
                map.put("activityId", activity.getId());
            }
            if (userTask != null) {
                map.put("taskId", userTask.getId());
            }
            map.put("type", type);
            if (!CollectionUtils.isEmpty(map)) {
                goodsInfoRespMybatisPageDto.getContent().add(map);
            }
        }
        int pullDown = 0;
        //是否还有下一页
        if (goodsIPage.getRecords().size() >= size && goodsIPage.getPages() > goodsIPage.getCurrent()) {
            pullDown = 1;
        }
        goodsInfoRespMybatisPageDto.from(goodsIPage.getSize(), offset, pullDown);
        simpleResponse.setData(goodsInfoRespMybatisPageDto);
        return simpleResponse;
    }

    /***
     * 获取闯关地图弹幕
     * @param merchantId
     * @param shopId
     * @return
     */
    @Override
    public SimpleResponse getBarrage(Long merchantId, Long shopId) {
        int goalNum = 40;
        Map<String, Object> userPubJoinParams = new HashMap<>();
        userPubJoinParams.put("shopId", shopId);
        userPubJoinParams.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
        int countUserPubJoin = this.userPubJoinService.countByParams(userPubJoinParams);
        if (countUserPubJoin > 1) {
            //减掉平台假数据
            countUserPubJoin = countUserPubJoin - 1;
        }

        Map<String, Object> upayWxOrderParams = new HashMap<>();
        upayWxOrderParams.put("mchId", merchantId);
        upayWxOrderParams.put("shopId", shopId);
        upayWxOrderParams.put("status", OrderStatusEnum.ORDERED.getCode());
        upayWxOrderParams.put("bizType", BizTypeEnum.CHECKPOINT.getCode());
        int countUpayWxOrder = this.upayWxOrderService.countUpayWxOrder(upayWxOrderParams);

        int sum = countUserPubJoin + countUpayWxOrder;
        int robotNum = 0;
        //若两个数量相加>=40,则只取真实数据，不掺假数据
        if (sum >= goalNum) {
            if (countUpayWxOrder >= goalNum && countUserPubJoin >= goalNum) {
                //若直接大于40，则取一半
                countUpayWxOrder = 20;
                countUserPubJoin = 20;
            } else {
                countUserPubJoin = goalNum - countUpayWxOrder;
            }
        } else {
            //此时假数据数量，40-sum
            robotNum = goalNum - sum;
        }

        CommonDict commonDict = this.commonDictService.selectByTypeKey("checkpoint", "level");

        Page<UserPubJoin> userPubJoinPage = new Page<>(1, countUserPubJoin);
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getShopId, shopId)
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
        ;
        IPage<UserPubJoin> userPubJoinIPage = userPubJoinService.page(userPubJoinPage, userPubJoinQueryWrapper);
        List<UserPubJoin> userPubJoins = userPubJoinIPage.getRecords();

        Page<UpayWxOrder> upayWxOrderPage = new Page<>(1, countUpayWxOrder);
        QueryWrapper<UpayWxOrder> upayWxOrderQueryWrapper = new QueryWrapper<>();
        upayWxOrderQueryWrapper.lambda()
                .eq(UpayWxOrder::getMchId, merchantId)
                .eq(UpayWxOrder::getShopId, shopId)
                .eq(UpayWxOrder::getStatus, OrderStatusEnum.ORDERED.getCode())
        ;
        IPage<UpayWxOrder> upayWxOrderIPage = upayWxOrderService.page(upayWxOrderPage, upayWxOrderQueryWrapper);
        List<UpayWxOrder> upayWxOrders = upayWxOrderIPage.getRecords();

        Map<Long, User> userMap = this.getUsers(upayWxOrders, userPubJoins);

        List<Map<String, Object>> robotUsers = null;
        if (robotNum > 0) {
            robotUsers = this.getRobotAssistUser(robotNum);
        }

        CheckpointBarrageResp resp = new CheckpointBarrageResp();
        resp.setCheckpointBarrages(new ArrayList<>());
        if (!CollectionUtils.isEmpty(upayWxOrders)) {
            for (UpayWxOrder upayWxOrder : upayWxOrders) {
                CheckpointBarrageResp.CheckpointBarrage checkpointBarrage = new CheckpointBarrageResp.CheckpointBarrage();
                checkpointBarrage.setType(2);
                checkpointBarrage.setCheckpoint(new CheckpointBarrageResp.Checkpoint());
                User user = userMap.get(upayWxOrder.getUserId());
                if (user != null) {
                    checkpointBarrage.from(user);
                }
                checkpointBarrage.getCheckpoint().from(commonDict, upayWxOrder);
                resp.getCheckpointBarrages().add(checkpointBarrage);
            }
        }
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            for (UserPubJoin userPubJoin : userPubJoins) {
                if (userPubJoin.getSubUid() == 0) {
                    continue;
                }
                CheckpointBarrageResp.CheckpointBarrage checkpointBarrage = new CheckpointBarrageResp.CheckpointBarrage();
                checkpointBarrage.setType(1);
                checkpointBarrage.setCheckpoint(new CheckpointBarrageResp.Checkpoint());
                User user = userMap.get(userPubJoin.getSubUid());
                if (user != null) {
                    checkpointBarrage.from(user);
                }
                resp.getCheckpointBarrages().add(checkpointBarrage);
            }
        }
        if (!CollectionUtils.isEmpty(robotUsers)) {
            for (Map map : robotUsers) {
                CheckpointBarrageResp.CheckpointBarrage checkpointBarrage = new CheckpointBarrageResp.CheckpointBarrage();
                checkpointBarrage.setType(NumberUtils.toInt(RobotUserUtil.getRandom(1, 2)));
                checkpointBarrage.setCheckpoint(new CheckpointBarrageResp.Checkpoint());
                checkpointBarrage.from(map.get("nickName").toString(), map.get("avatarUrl").toString());
                checkpointBarrage.getCheckpoint().from(commonDict);
                resp.getCheckpointBarrages().add(checkpointBarrage);
            }
        }

        //顺序打乱
        Collections.shuffle(resp.getCheckpointBarrages());

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    @Override
    public SimpleResponse getItemList(Long merchantId, Long shopId) {
        List<ActivityDef> activityDefList = this.getActivityDef(merchantId, shopId, null, null);
        if (CollectionUtils.isEmpty(activityDefList)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "该商户没有创建任何活动");
        }

        List<ActivityTask> activityTasks = this.getActivityTasks(merchantId, shopId, null, null);
        //拆红包
        ActivityDef activityDefOpenRedPack = this.activityDefTypeService.getOpenRedPack(activityDefList);
        //幸运大转盘
        ActivityDef activityDefLuckyWheel = this.activityDefTypeService.getLuckyWheel(activityDefList);
        //收藏加购
        ActivityDef activityDefFavCart = this.activityDefTypeService.getFavCart(activityDefList);
        //好评晒图
        ActivityDef activityDefGoodsComment = this.activityDefTypeService.getGoodsCommentWithoutGoodsId(activityDefList);
        //回填订单
        ActivityDef activityDefFillOrder = this.activityDefTypeService.getFillOrder(activityDefList);
        //赠品
        ActivityDef activityDefAssist = this.activityDefTypeService.getAssist(activityDefList);
        //免单抽奖
        ActivityDef activityDefOrderBack = this.activityDefTypeService.getOrderBack(activityDefList);
        // 全额报销
        ActivityDef activityDefReimbursement = this.activityDefTypeService.getReimbursement(activityDefList);

        List<Map> list = new ArrayList<>();
        Map m;
        if (activityDefFillOrder != null) {
            m = new HashMap(2);
            m.put("value", 1);
            m.put("label", "回填订单");
            list.add(m);
        }
        if (activityDefGoodsComment != null) {
            m = new HashMap(2);
            m.put("value", 2);
            m.put("label", "好评晒图");
            list.add(m);
        }
        if (activityDefOpenRedPack != null && activityDefOpenRedPack.getId() != null) {
            m = new HashMap(2);
            m.put("value", 3);
            m.put("label", "拆红包");
            list.add(m);
        }
        if (activityDefAssist != null && activityDefAssist.getId() != null) {
            m = new HashMap(2);
            m.put("value", 4);
            m.put("label", "助力赠品");
            list.add(m);
        }
        if (activityDefLuckyWheel != null && activityDefLuckyWheel.getId() != null) {
            m = new HashMap(2);
            m.put("value", 5);
            m.put("label", "轮盘抽奖");
            list.add(m);
        }
        if (activityDefFavCart != null) {
            m = new HashMap(2);
            m.put("value", 6);
            m.put("label", "收藏加购");
            list.add(m);
        }
        if (activityDefOrderBack != null && activityDefOrderBack.getId() != null) {
            m = new HashMap(2);
            m.put("value", 7);
            m.put("label", "免单抽奖");
            list.add(m);
        }
        if (activityDefReimbursement != null && activityDefReimbursement.getId() != null) {
            m = new HashMap(2);
            m.put("value", 32);
            m.put("label", "全额报销");
            list.add(m);
        }
        SimpleResponse response = new SimpleResponse(ApiStatusEnum.SUCCESS);
        response.setData(list);
        return response;
    }

    @Override
    public SimpleResponse queryActivity(Long merchantId, Long shopId, Long userId) {
        List<ActivityDef> activityDefList = this.getActivityDef(merchantId, shopId, userId, null);
        if (CollectionUtils.isEmpty(activityDefList)) {
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("list", activityDefList);
            return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "该商户没有创建任何活动", resultVO);
        }
        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        List<ActivityTask> activityTasks = this.getActivityTasks(merchantId, shopId, userId, null);
        //拆红包
        ActivityDef activityDefOpenRedPack = this.activityDefTypeService.getOpenRedPack(activityDefList);
        //幸运大转盘
        ActivityDef activityDefLuckyWheel = this.activityDefTypeService.getLuckyWheel(activityDefList);
        // //收藏加购
        // ActivityDef activityDefFavCart = this.activityDefTypeService.getFavCart(activityDefList);
        // //好评晒图
        // ActivityDef activityDefGoodsComment = this.activityDefTypeService.getGoodsComment(activityDefList);
        // //回填订单
        // ActivityDef activityDefFillOrder = this.activityDefTypeService.getFillOrder(activityDefList);
        //赠品
        ActivityDef activityDefAssist = this.activityDefTypeService.getAssist(activityDefList);
        //免单抽奖
        ActivityDef activityDefOrderBack = this.activityDefTypeService.getOrderBack(activityDefList);
        //报销活动
        ActivityDef activityDefFullReimburserment = this.activityDefTypeService.getReimbursement(activityDefList);

        CheckpointTaskSpaceResp checkpointTaskSpaceResp = new CheckpointTaskSpaceResp();
        checkpointTaskSpaceResp.setList(new ArrayList<CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail>());
        //拆红包活动改版，不做任务空间展示  2019-05-17 13:48
        if (activityDefOpenRedPack != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            Long activityId = null;
            Activity activity = this.getActivity(merchantId, shopId, activityDefOpenRedPack.getId(), null,
                    ActivityTypeEnum.OPEN_RED_PACKET_START.getCode(), ActivityStatusEnum.PUBLISHED.getCode());

            if (activity != null) {
                activityId = activity.getId();
            }
            detail.from(activityDefOpenRedPack.getId(), activityId, 0, CheckpointTaskSpaceEnum.OPENREDPACK.getCode(),
                    null, null, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefLuckyWheel != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            Long activityId = null;
            Activity activity = this.getActivity(merchantId, shopId, activityDefLuckyWheel.getId(), null,
                    ActivityTypeEnum.LUCK_WHEEL.getCode(), ActivityStatusEnum.PUBLISHED.getCode());
            if (activity != null) {
                activityId = activity.getId();
            }
            int taskNum = 0;
            Map<String, Object> map = this.appletUserActivityServiceImpl.getTimesAndChance(shopId, userId);
            if (map != null && map.get("times") != null) {
                taskNum = (int) map.get("times");
            }
            detail.from(activityDefLuckyWheel.getId(), activityId, taskNum, CheckpointTaskSpaceEnum.LUCKYWHEEL.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.LUCKY_WHEEL.getCode()),
                    getTotalCommissionByLast(1, activityDefLuckyWheel.getDeposit()), rate);
            if (activity != null) {
                checkpointTaskSpaceResp.getList().add(detail);
            }
        }
// 		if (activityDefFavCart != null) {
// 			CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
// 			List<ActivityTask> activityTasks1 = getActivityTasks(merchantId, shopId, userId, ActivityTaskTypeEnum.FAVCART.getCode());
// 			List<Long> goodsIds = new ArrayList<>();
// 			if (!CollectionUtils.isEmpty(activityTasks1)) {
// 				for (ActivityTask activityTask : activityTasks1) {
// 					if (goodsIds.indexOf(activityTask.getGoodsId()) == -1) {
// 						goodsIds.add(activityTask.getGoodsId());
// 					}
// 				}
// 			}
// 			Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode(), goodsIds);
// 			int taskNum = 0;
// 			BigDecimal amountLastSingle = BigDecimal.ZERO;
// 			if (!CollectionUtils.isEmpty(activityTasks1)) {
// 				taskNum = activityTasks1.size() - userTaskMap.size();
// 				amountLastSingle = activityTasks1.get(0).getRpAmount();
// 			}
// 			detail.from(null, null, taskNum, CheckpointTaskSpaceEnum.FAVCART.getCode(),
// 					sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.FAV_CART.getCode()),
// 					getTotalCommissionByLast(taskNum, amountLastSingle), rate);
// 			checkpointTaskSpaceResp.getList().add(detail);
// 		}
// 		if (activityDefGoodsComment != null) {
// 			CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
// 			List<ActivityTask> activityTasks1 = getActivityTasks(merchantId, shopId, userId, ActivityTaskTypeEnum.GOOD_COMMENT.getCode());
// 			List<Long> goodsIds = new ArrayList<>();
// 			if (!CollectionUtils.isEmpty(activityTasks1)) {
// 				for (ActivityTask activityTask : activityTasks1) {
// 					if (goodsIds.indexOf(activityTask.getGoodsId()) == -1) {
// 						goodsIds.add(activityTask.getGoodsId());
// 					}
// 				}
// 			}
// 			Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.GOOD_COMMENT.getCode(), goodsIds);
// 			int taskNum = 0;
// 			BigDecimal amountLastSingle = BigDecimal.ZERO;
// 			if (!CollectionUtils.isEmpty(activityTasks1)) {
// 				taskNum = activityTasks1.size() - userTaskMap.size();
// 				amountLastSingle = activityTasks1.get(0).getRpAmount();
// 			}
// 			detail.from(null, null, taskNum, CheckpointTaskSpaceEnum.GOODCOMMENT.getCode(),
// 					sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.GOODS_COMMENT.getCode()),
// 					getTotalCommissionByLast(taskNum, amountLastSingle), rate);
// 			checkpointTaskSpaceResp.getList().add(detail);
// 		}
// 		if (activityDefFillOrder != null) {
// 			CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
// //            List<ActivityTask> activityTasks1 = getActivityTasks(merchantId, shopId, userId, ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode());
// //            int taskNum = 0;
// //            BigDecimal amountLastSingle = BigDecimal.ZERO;
// //            if (!CollectionUtils.isEmpty(activityTasks1)) {
// //                taskNum = activityTasks1.size();
// //                amountLastSingle = activityTasks1.get(0).getRpAmount();
// //            }
// 			detail.from(null, null, -1, CheckpointTaskSpaceEnum.FILLORDER.getCode(),
// 					sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ORDER_RED_PACKET.getCode()),
// 					activityDefFillOrder.getDeposit(), rate);
// 			checkpointTaskSpaceResp.getList().add(detail);
// 		}
        if (activityDefAssist != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ASSIST.getCode());
            List<ActivityDef> activityDefs = getActivityDef(merchantId, shopId, userId, types);
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.ASSIST_START.getCode(), goodsIds);
            int totalStock = 0;
            BigDecimal totalCommission = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    UserTask userTask = userTaskMap.get(activityDef.getGoodsId());
                    if (userTask == null) {
                        totalStock = totalStock + 1;
                        totalCommission = totalCommission.add(activityDef.getCommission());
                    }
                }
            }
            detail.from(activityDefAssist.getId(), null, totalStock, CheckpointTaskSpaceEnum.ASSIST.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ASSIST.getCode()),
                    totalCommission, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }
        if (activityDefOrderBack != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            List<Integer> types = Arrays.asList(ActivityDefTypeEnum.ORDER_BACK.getCode());
            List<ActivityDef> activityDefs = getActivityDef(merchantId, shopId, userId, types);
            List<Long> goodsIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    if (goodsIds.indexOf(activityDef.getGoodsId()) == -1) {
                        goodsIds.add(activityDef.getGoodsId());
                    }
                }
            }
            Map<Long, UserTask> userTaskMap = this.getUserTasks(merchantId, shopId, userId, UserTaskTypeEnum.ORDER_BACK_START.getCode(), goodsIds);
            int totalStock = 0;
            BigDecimal totalCommission = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(activityDefs)) {
                for (ActivityDef activityDef : activityDefs) {
                    UserTask userTask = userTaskMap.get(activityDef.getGoodsId());
                    if (userTask == null) {
                        totalStock = totalStock + 1;
                        totalCommission = totalCommission.add(activityDef.getCommission());
                    }
                }
            }
            detail.from(activityDefOrderBack.getId(), null, totalStock, CheckpointTaskSpaceEnum.ORDERBACK.getCode(),
                    sumByTypeTotalCommission(merchantId, shopId, userId, UserCouponTypeEnum.ORDER_BACK_START.getCode()),
                    totalCommission, rate);
            checkpointTaskSpaceResp.getList().add(detail);
        }

        //全额报销活动
        if (activityDefFullReimburserment != null) {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            detail.setType(CheckpointTaskSpaceEnum.FULLREIMBURSEMENT.getCode());
            detail.setActivityDefId(activityDefFullReimburserment.getId());

            //有无正在进行的活动
            Activity activity = this.getActivity(merchantId, shopId, activityDefFullReimburserment.getId(), userId, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode(), null);
            if (activity != null) {
                detail.setActivityId(activity.getId());
            }
            checkpointTaskSpaceResp.getList().add(detail);
        } else {
            CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail detail = new CheckpointTaskSpaceResp.CheckpointTaskSpaceDetail();
            detail.setType(CheckpointTaskSpaceEnum.FULLREIMBURSEMENT.getCode());
            //查看是否存在进行中的活动
            Activity activity = this.getActivity(merchantId, shopId, null, userId, ActivityTypeEnum.FULL_REIMBURSEMENT.getCode(), ActivityStatusEnum.PUBLISHED.getCode());
            if (activity != null && activity.getAwardStartTime() == null) {
                detail.setActivityId(activity.getId());
                checkpointTaskSpaceResp.getList().add(detail);
            }
        }

        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setData(checkpointTaskSpaceResp);
        return simpleResponse;
    }

    /***
     * 获取用户信息
     * @param upayWxOrders
     * @param userPubJoins
     * @return
     */
    private Map<Long, User> getUsers(List<UpayWxOrder> upayWxOrders, List<UserPubJoin> userPubJoins) {
        Map<Long, User> userMap = new HashMap<>();
        List<Long> userIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(upayWxOrders)) {
            for (UpayWxOrder upayWxOrder : upayWxOrders) {
                if (userIds.indexOf(upayWxOrder.getUserId()) == -1) {
                    userIds.add(upayWxOrder.getUserId());
                }
            }
        }
        if (!CollectionUtils.isEmpty(userPubJoins)) {
            for (UserPubJoin userPubJoin : userPubJoins) {
                if (userIds.indexOf(userPubJoin.getSubUid()) == -1) {
                    userIds.add(userPubJoin.getSubUid());
                }
            }
        }
        if (CollectionUtils.isEmpty(userIds)) {
            return userMap;
        }
        List<User> users = this.userService.listPageByParams(userIds);
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                userMap.put(user.getId(), user);
            }
        }
        return userMap;
    }

    /***
     * 获取假数据
     * @param limit
     * @return
     */
    private List<Map<String, Object>> getRobotAssistUser(int limit) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            while (true) {
                Map<String, Object> map = RobotUserUtil.getAssistUserAvatarUrlAndNickFromOss();
                int frequency = Collections.frequency(list, map);
                //每次获取的时候对象重复次数不能超过2次
                if (frequency <= 2) {
                    list.add(map);
                    break;
                }
            }
        }
        return list;
    }

    /***
     * 获取活动
     * @param merchantId
     * @param shopId
     * @param activityDefId
     * @param type
     * @return
     */
    private Activity getActivity(Long merchantId, Long shopId, Long activityDefId, Long userId, int type, Integer status) {
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getMerchantId, merchantId)
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getDel, DelEnum.NO.getCode())
                .eq(Activity::getType, type)
                .orderByDesc(Activity::getId)
        ;

        if (status != null && status > 0) {
            wrapper.and(params -> params.eq("status", status));
        }
        if (userId != null && userId > 0) {
            wrapper.and(params -> params.eq("user_id", userId));
        }
        if (activityDefId != null && activityDefId > 0) {
            wrapper.and(params -> params.eq("activity_def_id", activityDefId));
        }
        wrapper.orderByDesc("id");
        Activity activity = null;
        List<Activity> activities = this.activityService.list(wrapper);
        if (!CollectionUtils.isEmpty(activities)) {
            activity = activities.get(0);
        }
        return activity;
    }

    /***
     * 查询活动任务-过滤掉已经结束的活动
     * @param merchantId
     * @param shopId
     * @param type
     * @return
     */
    private List<ActivityTask> getActivityTasks(Long merchantId, Long shopId, Long userId, Integer type) {
        QueryWrapper<ActivityTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityTask::getMerchantId, merchantId)
                .eq(ActivityTask::getShopId, shopId)
                .eq(ActivityTask::getDepositStatus, DepositStatusEnum.PAYMENT.getCode())
                .eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
                .eq(ActivityTask::getDel, DelEnum.NO.getCode())
                .orderByDesc(ActivityTask::getId)
        ;
        if (type != null) {
            wrapper.and(params -> params.eq("type", type));
        } else {
            wrapper.and(params -> params.in("type", Arrays.asList(ActivityTaskTypeEnum.FILL_BACK_ORDER.getCode(),
                    ActivityTaskTypeEnum.FAVCART.getCode(), ActivityTaskTypeEnum.GOOD_COMMENT.getCode())));
        }
        return activityTaskService.list(wrapper);
    }

    /***
     * 查询活动定义
     * @param merchantId
     * @param shopId
     * @param types
     * @return
     */
    private List<ActivityDef> getActivityDef(Long merchantId, Long shopId, Long userId, List<Integer> types) {
        QueryWrapper<ActivityDef> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ActivityDef::getMerchantId, merchantId)
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode())
                .orderByDesc(ActivityDef::getId)
        ;
        if (!CollectionUtils.isEmpty(types)) {
            wrapper.and(params -> params.in("type", types));
        }
        return this.activityDefService.list(wrapper);
    }

    /***
     * 获取已赚取的收益-计算佣金后所得
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type
     * @return
     */
    private BigDecimal sumByTypeTotalCommission(Long merchantId, Long shopId, Long userId, int type) {
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("userId", userId);
        params.put("type", type);
        params.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
        return this.userCouponService.sumByTypeTotalCommission(params);
    }

    /***
     * 获取已赚取的收益-计算获得佣金个数
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type
     * @return
     */
    private int countByTypeTotalCommission(Long merchantId, Long shopId, Long userId, int type) {
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("userId", userId);
        params.put("type", type);
        params.put("status", UserCouponStatusEnum.RECEIVIED.getCode());
        return this.userCouponService.countByTypeTotalCommission(params);
    }

    /***
     * 获取剩余佣金(原始佣金的计算方式，未扣除税率)
     * @param taskNum
     * @param commissionDef
     * @return
     */
    private BigDecimal getTotalCommissionByLast(int taskNum, BigDecimal commissionDef) {
        return commissionDef.multiply(new BigDecimal(taskNum));
    }


    /***
     * 根据类型获取用户完成得奖的记录
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type
     * @return
     */
    private List<UserCoupon> getUserCoupon(Long merchantId, Long shopId, Long userId, int type) {
        QueryWrapper<UserCoupon> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserCoupon::getMerchantId, merchantId)
                .eq(UserCoupon::getShopId, shopId)
                .eq(UserCoupon::getUserId, userId)
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                .eq(UserCoupon::getType, type)
                .gt(UserCoupon::getCommission, BigDecimal.ZERO)
        ;
        return this.userCouponService.list(wrapper);
    }

    /***
     * 获取userTask
     * @param merchantId
     * @param shopId
     * @param userId
     * @param type
     * @param goodsIds
     * @return
     */
    private Map<Long, UserTask> getUserTasks(Long merchantId, Long shopId, Long userId, int type, List<Long> goodsIds) {
        QueryWrapper<UserTask> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(UserTask::getMerchantId, merchantId)
                .eq(UserTask::getShopId, shopId)
                .eq(UserTask::getUserId, userId)
                .eq(UserTask::getStatus, UserTaskStatusEnum.AWARD_SUCCESS.getCode())
                .eq(UserTask::getType, type)
                .in(UserTask::getGoodsId, goodsIds)
                .orderByDesc(UserTask::getStatus, UserTask::getId)
        ;
        Map<Long, UserTask> map = new HashMap<>();
        List<UserTask> userTasks = this.userTaskService.list(wrapper);
        if (!CollectionUtils.isEmpty(userTasks)) {
            for (UserTask userTask : userTasks) {
                UserTask userTaskMap = map.get(userTask.getGoodsId());
                if (userTaskMap == null) {
                    map.put(userTask.getGoodsId(), userTask);
                }
            }
        }
        return map;
    }

    /***
     * 获取活动
     * @param merchantId
     * @param shopId
     * @param userId
     * @param types
     * @param goodsIds
     * @return
     */
    private Map<Long, Activity> getActivitys(Long merchantId, Long shopId, Long userId, List<Integer> types, List<Long> goodsIds) {
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(Activity::getMerchantId, merchantId)
                .eq(Activity::getShopId, shopId)
                .eq(Activity::getUserId, userId)
                .in(Activity::getType, types)
                .in(Activity::getGoodsId, goodsIds)
        ;
        wrapper.orderByDesc("id");
        if (!types.contains(ActivityTypeEnum.ASSIST.getCode()) && !types.contains(ActivityTypeEnum.ASSIST_V2.getCode())) {
            wrapper.and(params -> params.isNotNull("award_start_time"));
            wrapper.and(params -> params.eq("status", ActivityStatusEnum.END.getCode()));
        }
        Map<Long, Activity> map = new HashMap<>();
        List<Activity> activities = this.activityService.list(wrapper);
        if (CollectionUtils.isEmpty(activities)) {
            return map;
        }

        for (Activity activity : activities) {
            Activity activityMap = map.get(activity.getGoodsId());
            if (activityMap == null
            ) {
                map.put(activity.getGoodsId(), activity);
            }
        }
        return map;
    }
}
