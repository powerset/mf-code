package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.*;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityOrder;
import mf.code.activity.repo.po.ActivityTask;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityOrderService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.BackFillOrderReq;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.service.AppletUserActivityV2Service;
import mf.code.api.applet.service.AsyncFillOrderService;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.common.caller.luckcrm.LuckCrmCaller;
import mf.code.common.constant.*;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.exception.CommissionReduceException;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.verify.TaobaoVerifyService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年05月09日 15:07
 */
@Service
@Slf4j
public class AsyncFillOrderServiceImpl implements AsyncFillOrderService {
    @Autowired
    private TaobaoVerifyService taobaoVerifyService;
    @Autowired
    private LuckCrmCaller luckCrmCaller;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private AppletUserActivityService appletUserActivityService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private AppletUserActivityV2Service appletUserActivityV2Service;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityOrderService activityOrderService;

    @Async
    @Override
    public void dealWithFillOrderPopupAsync(String orderId, Long merhcantId, Long shopId, Long userId) {
        SimpleResponse response = new SimpleResponse();
        long start = System.currentTimeMillis();
        //异步防重key
        String redisRepeatKey = RedisKeyConstant.BACKFILLORDER_ASYNC_REPEAT + orderId;
        //异步结果key
        String redisResultKey = RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId;
        int timeoutMinute = 30;

        // 校验所填订单号
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merhcantId);
        if (luckCrmCaller.isError(orderInfo)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            response.setMessage("订单填写有误");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date date = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (date.getTime() < DateUtil.threeMouthBefore().getTime()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO5);
            response.setMessage("该订单已失效");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            response.setMessage("您的订单还未确认收货");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }
        // 检查任务
        JSONArray numIids = jsonObject.getJSONArray("num_iid");
        if (CollectionUtils.isEmpty(numIids)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
            response.setMessage("该订单数据错误");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        // 以上用户订单校验通过，进入业务主流程

        // 校验该订单号 是否在 商家在后台创建的商品表里
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("popStatus", 3);
        resultMap.put("amount", 0.00);
        Long activityDefId = null;
        // 获取免单商品抽奖活动定义
        QueryWrapper<ActivityDef> adWrapper = new QueryWrapper<>();
        adWrapper.lambda()
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.ORDER_BACK.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.list(adWrapper);
        if (CollectionUtils.isEmpty(activityDefs)) {
            log.info("没有免单抽奖活动");
        } else {
            activityDefId = activityDefs.get((int) (Math.random() * activityDefs.size())).getId();
            resultMap.put("activityDefId", activityDefId);
        }
        // 返回结果
        if (activityDefId != null) {
            stringRedisTemplate.opsForValue().set(mf.code.uactivity.repo.redis.RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + userId, activityDefId.toString(), 1, TimeUnit.DAYS);
        }

        Map<String, Object> params = new HashMap<>();
        params.put("shopId", shopId);
        params.put("numIids", numIids);
        params.put("del", DelEnum.NO.getCode());
        List<Goods> goodsList = goodsService.pageList(params);
        // 查询商品,如果商品为空则抽取红包
        // 首页扫码回填订单，不参与活动认领，不创建活动
        Map<String, Goods> goodsIdMap = new HashMap<>();
        Set<Long> goodsIdSet = new HashSet<>();
        if (!CollectionUtils.isEmpty(goodsList)) {
            for (Goods goods : goodsList) {
                goodsIdMap.put(goods.getXxbtopNumIid(), goods);
                goodsIdSet.add(goods.getId());
            }
        } else {
            // 如果存在不存在的商品，则向喜销宝查询
            for (Object numiid : numIids) {
                String xxbResult = taobaoVerifyService.getGoodsInfo(numiid.toString(), merhcantId);
                if (luckCrmCaller.isError(xxbResult)) {
                    continue;
                }
                Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
                if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() <= 1) {
                    continue;
                }
                // 组装参数 名称-主图-详情图-价格
                Goods goods = new Goods();
                goods.setXxbtopTitle(xxbMap.get("title").toString());
                goods.setDisplayGoodsName(xxbMap.get("title").toString());
                goods.setPicUrl(xxbMap.get("pic_url").toString());
                goods.setXxbtopPrice(xxbMap.get("price").toString());
                goods.setDisplayPrice(new BigDecimal(xxbMap.get("price").toString()));
                goods.setXxbtopNumIid(numiid.toString());
                goodsIdMap.put(numiid.toString(), goods);
            }
        }
        if (CollectionUtils.isEmpty(goodsIdMap)) {
            log.info("后台和淘宝没有此商品信息");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            resultMap.put("popStatus", 3);
            response.setData(resultMap);
            response.setMessage("回填订单红包已经被抢光了，下次早点来哦");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        // 查看该商品 有无 回填订单红包 任务
        QueryWrapper<ActivityDef> atWrapper = new QueryWrapper<>();
        atWrapper.lambda()
                .eq(ActivityDef::getShopId, shopId)
                .eq(ActivityDef::getType, ActivityDefTypeEnum.FILL_BACK_ORDER.getCode())
                .eq(ActivityDef::getStatus, ActivityDefStatusEnum.PUBLISHED.getCode())
                .eq(ActivityDef::getDel, DelEnum.NO.getCode());
        List<ActivityDef> activityDefList = activityDefService.list(atWrapper);
        if (CollectionUtils.isEmpty(activityDefList) || activityDefList.get(0).getDeposit().compareTo(BigDecimal.ZERO) <= 0) {
            log.info("该商品 没有回填订单红包任务");
            for (Goods goods : goodsIdMap.values()) {
                updateOrCreateActivityOrder(userId, orderId, shopId, null, goods, jsonObject);
            }
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            resultMap.put("popStatus", 3);
            response.setData(resultMap);
            response.setMessage("回填订单红包已经被抢光了，下次早点来哦");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }
        ActivityDef activityDef = activityDefList.get(0);

        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("shopId", shopId);
        userTaskParams.put("userId", userId);
        userTaskParams.put("type", UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTaskParams.put("orderId", orderId);
        int countUserTask = userTaskService.countUserTask(userTaskParams);
        if (countUserTask > 0) {
            log.info("该订单已完成回填确认，请填写其他订单");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            response.setMessage("该订单已完成回填确认，请填写其他订单");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }
        // 创建userActivty
        UserActivity userActivity = new UserActivity();
        if (!CollectionUtils.isEmpty(goodsList)) {
            userActivity.setGoodsId(goodsList.get(0).getId());
        }
        userActivity.setUserId(userId);
        userActivity.setMerchantId(merhcantId);
        userActivity.setShopId(shopId);
        userActivity.setActivityId(0L);
        userActivity.setType(UserActivityTypeEnum.FILL_BACK_ORDER.getCode());
        userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        userActivity.setActivityDefId(activityDef.getId());
        int rows = userActivityService.insertUserActivity(userActivity);
        if (rows == 0) {
            log.error("回填订单失败");
            response = goodCommentTask(shopId, userId, goodsIdSet, orderId, resultMap);
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        BigDecimal realAmount = appletUserActivityV2Service.calcCommission(jsonObject, activityDef);
        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, shopId, UserCouponTypeEnum.ORDER_RED_PACKET, realAmount);
        commissionDisDto.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userId, shopId, merhcantId);
        resultMap.putAll(BeanMapUtil.beanToMap(commissionResp));

        UserTask userTask = new UserTask();
        userTask.setUserId(userId);
        userTask.setActivityDefId(activityDef.getId());
        if (!CollectionUtils.isEmpty(goodsList)) {
            userTask.setGoodsId(goodsList.get(0).getId());
        }
        // 直接通过发放奖励
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        // 设置默认值
        userTask.setActivityId(0L);
        userTask.setUserActivityId(userActivity.getId());
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setMerchantId(merhcantId);
        userTask.setShopId(shopId);
        userTask.setType(UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTask.setOrderId(orderId);
        Date now = new Date();
        userTask.setStartTime(now);
        userTask.setReceiveTime(now);
        userTask.setAuditingTime(now);
        userTask.setCtime(now);
        userTask.setUtime(now);
        userTaskService.insertUserTask(userTask);
        for (Goods goods : goodsIdMap.values()) {
            updateOrCreateActivityOrder(userId, orderId, shopId, null, goods, jsonObject);
        }

        resultMap.put("amount", commissionBO.getCommission());
        resultMap.put("commission", commissionBO.getCommission());
        resultMap.put("popStatus", 2);
        //组装结果
        resultMap.put("status", 2);

        response = goodCommentTask(shopId, userId, goodsIdSet, orderId, resultMap);
        // 主动删除redis锁
        stringRedisTemplate.delete(redisRepeatKey);
        stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(response), timeoutMinute, TimeUnit.MINUTES);
        log.info("回填订单 异步任务 共消耗{} ms", System.currentTimeMillis() - start);
        return;
    }

    @Async
    @Override
    public void dealwith(String orderId, Long merchantId, Long shopId, BackFillOrderReq req) {
        long start = System.currentTimeMillis();
        //异步防重key
        String redisRepeatKey = RedisKeyConstant.BACKFILLORDER_ASYNC_REPEAT + orderId;
        //异步结果key
        String redisResultKey = RedisKeyConstant.BACKFILLORDER_ASYNC_RESULT + orderId;
        int timeoutMinute = 30;

        SimpleResponse simpleResponse = new SimpleResponse();
        //根据 订单号 和 商户id 查询 订单详情  -- 一商城 多淘宝店
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantId);
        // 检查订单信息
        simpleResponse = checkOrderInfoFromXxb(orderInfo);
        if (simpleResponse.error()) {
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        // 检查有没有回填订单活动的activity_def
        Map<String, Object> defParams = new HashMap<>();
        defParams.put("shopId", shopId);
        defParams.put("type", ActivityDefTypeEnum.FILL_BACK_ORDER.getCode());
        defParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        defParams.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.selectByParams(defParams);
        if (CollectionUtils.isEmpty(activityDefs)) {
            simpleResponse = new SimpleResponse();
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO12);
            simpleResponse.setMessage("暂无该订单对应的任务");
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        JSONArray numiids = jsonObject.getJSONArray("num_iid");
        List<Long> goodsIds = new ArrayList<>();
        // 筛选库中没有的商品
        Map<String, Goods> goodsMap = new HashMap<>();
        // 查询订单中的商品信息
        simpleResponse = selectGoodsFromOrderInfo(merchantId, numiids, goodsIds, goodsMap);
        if (simpleResponse.error()) {
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        // 妥协处理，回填订单没有商品ID，而好评返现是有的
        if (StringUtils.isNotBlank(req.getGoodsId())) {
            simpleResponse = executeGoodComment(req.getUserId(), req.getOrderId(), req.getGoodsId(), shopId, jsonObject, goodsMap);
            if (simpleResponse.error()) {
                // 主动删除redis锁
                stringRedisTemplate.delete(redisRepeatKey);
                stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
                return;
            }
        }

        // 执行回填订单逻辑
        simpleResponse = executeFillBackOrder(activityDefs, req, jsonObject, goodsIds);
        if (simpleResponse.error()) {
            // 主动删除redis锁
            stringRedisTemplate.delete(redisRepeatKey);
            stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
            return;
        }

        Map map = null;
        if (simpleResponse != null && simpleResponse.getData() != null) {
            map = (Map) simpleResponse.getData();
        }
        Long userId = Long.valueOf(req.getUserId());
        CommissionResp commissionResp = commissionService.commissionCheckpoint(userId, shopId, merchantId);

        Map<String, Object> resultMap = BeanMapUtil.beanToMap(commissionResp);
        //前端展现数值的字段
        resultMap.put("commission", BigDecimal.ZERO.toString());
        if (map != null) {
            resultMap.put("commission", map.get("commission"));
        }

        Goods goods = goodsMap.get(numiids.get(0).toString());
        // 继续组装结果
        resultMap.putAll(combineResult(req.getOrderId(), req.getUserId(), shopId, jsonObject, goods));
        //处理结果结束状态
        resultMap.put("status", 2);
        simpleResponse.setData(resultMap);
        appletUserActivityService.updateOrCreateActivityOrder(userId, orderId, shopId, null, goods, jsonObject);
        // 自动激活免单抽奖活动
        if (!CollectionUtils.isEmpty(goodsIds)) {
            createActviityFromDefByGoodsId(userId, orderId, merchantId, shopId, jsonObject, goodsIds);
        }
        // 主动删除redis锁
        stringRedisTemplate.delete(redisRepeatKey);
        stringRedisTemplate.opsForValue().set(redisResultKey, JSONObject.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
        log.info("回填订单 异步任务 共消耗{} ms", System.currentTimeMillis() - start);
        return;
    }

    /**
     * 活动订单回填（包括任务订单）
     *
     * @param userId     用户ID
     * @param orderId    订单ID该商品不在免单活动内
     * @param activity   创建活动实例
     * @param goods      商品实例
     * @param jsonObject 喜销宝返回订单详情
     */
    private Integer updateOrCreateActivityOrder(Long userId, String orderId, Long shopId, Activity activity, Goods goods, JSONObject jsonObject) {
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        // 如果同一订单已经被回填过，需要修改补充字段
        List<ActivityOrder> activityOrders = activityOrderService.selectByOrderId(merchantShop.getMerchantId(),
                merchantShop.getId(), userId, orderId);
        if (CollectionUtils.isEmpty(activityOrders)) {
            return createActivityOrder(userId, orderId, merchantShop, activity, goods, jsonObject);
        }
        ActivityOrder activityOrder = activityOrders.get(0);
        copyParamToActivityOrder(activity, goods, jsonObject, activityOrder);
        return activityOrderService.updateActivityOrder(activityOrder);
    }

    /**
     * 拷贝参数
     *
     * @param activity
     * @param goods
     * @param jsonObject
     * @param activityOrder
     */
    private void copyParamToActivityOrder(Activity activity, Goods goods, JSONObject jsonObject, ActivityOrder activityOrder) {
        if (activity != null) {
            activityOrder.setActivityId(activity.getId());
            activityOrder.setActivityDefId(activity.getActivityDefId());
        }
        if (goods != null) {
            activityOrder.setDisplayGoodsBanner(goods.getDisplayBanner());
            activityOrder.setDisplayGoodsName(goods.getDisplayGoodsName());
            activityOrder.setDisplayGoodsPrice(goods.getDisplayPrice());
            activityOrder.setGoodsId(goods.getId());
            activityOrder.setPicUrl(goods.getPicUrl());
            activityOrder.setTitle(goods.getXxbtopTitle());
        }
        if (jsonObject != null) {
            activityOrder.setSrcType(GoodsSrcTypeEnum.TAOBAO.getCode());
            activityOrder.setTradeStatus(jsonObject.getString("status"));
            activityOrder.setXxbtopRes(jsonObject.toJSONString());
            activityOrder.setStartTime(DateUtil.stringtoDate(jsonObject.getString("created"), DateUtil.FORMAT_ONE));
            activityOrder.setPayTime(DateUtil.stringtoDate(jsonObject.getString("pay_time"), DateUtil.FORMAT_ONE));
            activityOrder.setPayment(new BigDecimal(jsonObject.getString("payment")));
            if (!JsonParseUtil.booJsonArr(jsonObject.get("num_iid").toString())) {
                return;
            }
            JSONArray numIids = JSONArray.parseArray(jsonObject.get("num_iid").toString());
            activityOrder.setNumIid(jsonObject.get("num_iid").toString());
            // 这里比较恶心，目前只取了第一条
            MerchantShop merchantShop = merchantShopService.selectMerchantShopById(activityOrder.getShopId());
            String xxbResult = taobaoVerifyService.getGoodsInfo(numIids.get(0).toString(), merchantShop.getMerchantId());
            if (luckCrmCaller.isError(xxbResult)) {
                return;
            }
            Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
            if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() == 1) {
                return;
            }
            // 名称-主图
            activityOrder.setPicUrl(xxbMap.get("pic_url").toString());
            activityOrder.setTitle(xxbMap.get("title").toString());
        }
    }

    /**
     * 活动订单回填（包括任务订单）
     *
     * @param userId     用户ID
     * @param orderId    订单ID
     * @param activity   创建活动实例
     * @param goods      商品实例
     * @param jsonObject 喜销宝返回订单详情
     */
    private Integer createActivityOrder(Long userId, String orderId, MerchantShop merchantShop, Activity activity, Goods goods, JSONObject jsonObject) {

        ActivityOrder activityOrder = new ActivityOrder();
        activityOrder.setUserId(userId);
        activityOrder.setOrderId(orderId);
        activityOrder.setMerchantId(merchantShop.getMerchantId());
        activityOrder.setShopId(merchantShop.getId());
        copyParamToActivityOrder(activity, goods, jsonObject, activityOrder);
        activityOrder.setCtime(new Date());
        activityOrder.setUtime(new Date());
        return activityOrderService.createActivityOrder(activityOrder);
    }


    private SimpleResponse goodCommentTask(Long shopId, Long userId, Set goodsIds, String orderId, Map resultVO) {
        SimpleResponse<Object> response = new SimpleResponse<>();

        if (goodsIds.size() == 0) {
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        // 校验 商家是否有好评晒图 的红包任务
        QueryWrapper<ActivityTask> atWrapper = new QueryWrapper<>();
        atWrapper.lambda()
                .eq(ActivityTask::getShopId, shopId)
                .in(ActivityTask::getGoodsId, goodsIds)
                .eq(ActivityTask::getType, ActivityTaskTypeEnum.GOOD_COMMENT.getCode())
                .eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
                .eq(ActivityTask::getDel, DelEnum.NO.getCode());
        List<ActivityTask> activityTasks = activityTaskService.list(atWrapper);
        if (CollectionUtils.isEmpty(activityTasks)) {
            log.info("该商品 没有好评晒图红包任务");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        // 校验回填订单红包任务 是否还有库存
        ActivityTask activityTask = activityTasks.get(0);
        if (activityTasks.get(0).getRpStock() <= 0) {
            log.info("好评晒图已经被抢光了，下次早点来哦");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        Long goodsId = activityTask.getGoodsId();

        // 校验是否有 好评晒图的红包任务 资格

        QueryWrapper<UserTask> userTaskParams = new QueryWrapper<>();
        userTaskParams.lambda()
                .eq(UserTask::getShopId, shopId)
                .eq(UserTask::getGoodsId, goodsId)
                .eq(UserTask::getUserId, userId)
                .eq(UserTask::getType, UserTaskTypeEnum.GOOD_COMMENT.getCode())
                .ne(UserTask::getStatus, UserTaskStatusEnum.AWARD_EXPIRE.getCode());
        int countUserTask = userTaskService.count(userTaskParams);
        if (countUserTask > 0) {
            log.info("你已参加过好评返现任务");
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        resultVO.put("popStatus", 1);
        resultVO.put("orderId", orderId);
        resultVO.put("goodsId", goodsId);
        response.setData(resultVO);
        return response;
    }

    /**
     * 检查用户填写订单
     *
     * @param orderInfo
     * @return
     */
    private SimpleResponse checkOrderInfoFromXxb(String orderInfo) {
        if (luckCrmCaller.isError(orderInfo)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "订单填写有误");
        }
        // 判断支付时间、支付状态、商品id
        JSONObject jsonObject = JSONObject.parseObject(orderInfo);
        // 时间格式：yyyy-MM-dd HH:mm:ss
        String createTime = jsonObject.getString("created");
        Date createDate = DateUtil.stringtoDate(createTime, "yyyy-MM-dd HH:mm:ss");
        if (createDate.getTime() < DateUtil.threeMouthBefore().getTime()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "该订单已失效");
        }
        String status = jsonObject.getString("status");
        if (!StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_BUYER_SIGNED.getStatus())
                && !StringUtils.equalsIgnoreCase(status, TAOBAOPayStatusEnum.TRADE_FINISHED.getStatus())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该订单未完成");
        }
        // 检查任务
        // 2019-03-20修改为全店商品皆可以成功回填订单
        JSONArray numIids = jsonObject.getJSONArray("num_iid");
        if (CollectionUtils.isEmpty(numIids)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "该订单数据错误");
        }
        return new SimpleResponse();
    }

    /**
     * TODO:重写，存入goods_parent里
     * 根据订单中的商品numiids查询商品信息
     *
     * @param merchantId 商户id
     * @param numiids    淘宝商品id
     * @param goodsIds   商品id
     * @param goodsMap   商品map
     * @return
     */
    private SimpleResponse selectGoodsFromOrderInfo(Long merchantId, JSONArray numiids, List<Long> goodsIds, Map<String, Goods> goodsMap) {
        SimpleResponse simpleResponse = new SimpleResponse();
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "display_price", "xxbtop_price", "pic_url", "display_goods_name", "xxbtop_title", "xxbtop_num_iid");
        queryWrapper.in("xxbtop_num_iid", numiids);
        List<Goods> goodsList = goodsService.list(queryWrapper);
        log.info("根据订单中的商品numiids查询商品信息" + JSON.toJSONString(goodsList));
        JSONArray numiidsRemove = new JSONArray();
        if (!CollectionUtils.isEmpty(goodsList)) {
            for (Goods goods : goodsList) {
                goodsIds.add(goods.getId());
                goodsMap.put(goods.getXxbtopNumIid(), goods);
                numiids.remove(goods.getXxbtopNumIid());
                numiidsRemove.add(goods.getXxbtopNumIid());
            }
        }
        // 如果存在不存在的商品，则向喜销宝查询
        if (!CollectionUtils.isEmpty(numiids)) {
            for (Object numiid : numiids) {
                String xxbResult = taobaoVerifyService.getGoodsInfo(numiid.toString(), merchantId);
                if (luckCrmCaller.isError(xxbResult)) {
                    continue;
                }
                Map<String, Object> xxbMap = JSONObject.parseObject(xxbResult);
                if (CollectionUtils.isEmpty(xxbMap) || xxbMap.size() <= 1) {
                    continue;
                }
                // 组装参数 名称-主图-详情图-价格
                Goods goods = new Goods();
                goods.setXxbtopTitle(xxbMap.get("title").toString());
                goods.setDisplayGoodsName(xxbMap.get("title").toString());
                goods.setPicUrl(xxbMap.get("pic_url").toString());
                goods.setXxbtopPrice(xxbMap.get("price").toString());
                goods.setDisplayPrice(new BigDecimal(xxbMap.get("price").toString()));
                goods.setXxbtopNumIid(numiid.toString());
                goodsMap.put(numiid.toString(), goods);
            }
        }
        if (CollectionUtils.isEmpty(goodsMap)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "该订单未包含任务商品");
        }
        numiids.addAll(0, numiidsRemove);
        return simpleResponse;
    }


    /**
     * 执行好评刷图任务逻辑
     *
     * @param shopId
     * @param jsonObject
     * @param goodsMap
     * @return
     */
    private SimpleResponse executeGoodComment(String userId, String orderId, String goodsId, Long shopId, JSONObject jsonObject, Map<String, Goods> goodsMap) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Long goodsIdReq = Long.valueOf(goodsId);
        Goods goods = null;
        for (Goods good : goodsMap.values()) {
            // goodsMap肯定不为空，但是不保证goods包含id
            if (null == good.getId()) {
                continue;
            }
            if (good.getId().equals(goodsIdReq)) {
                goods = good;
            }
        }
        if (goods == null) {
            // 这里应该不会出现
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "该订单未包含任务商品");
        }
        // 查询好评返现的任务，然后赋值
        simpleResponse.setData(combineResult(orderId, userId, shopId, jsonObject, goods));
        return simpleResponse;
    }

    /**
     * 组装结果
     *
     * @param orderId    订单编号
     * @param userId     用户编号
     * @param shopId     店铺id
     * @param jsonObject 订单信息
     * @param goods      商品信息
     * @return resultMap返回结果
     */
    private Map<String, Object> combineResult(String orderId, String userId, Long shopId, JSONObject jsonObject, Goods goods) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("goodsId", goods.getId());
        resultMap.put("picPath", goods.getPicUrl());
        resultMap.put("displayPrice", goods.getDisplayPrice());
        resultMap.put("title", goods.getXxbtopTitle());
        // 支付时间（格式：yyyy.MM.dd HH:mm）
        Date payTime = DateUtil.stringtoDate(jsonObject.getString("pay_time"), DateUtil.FORMAT_ONE);
        resultMap.put("payTime", DateUtil.dateToString(payTime, "yyyy.MM.dd HH:mm"));
        resultMap.put("orderId", orderId);
        resultMap.put("amount", 0);
        // 检查有没有该商品的好评返现活动的activity_task， 返回amount字段，用于连接好评任务
        Map<String, Object> taskParams = new HashMap<>();
        taskParams.put("shopId", shopId);
        taskParams.put("goodsId", goods.getId());
        taskParams.put("type", ActivityDefTypeEnum.GOOD_COMMENT.getCode());
        taskParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        taskParams.put("del", DelEnum.NO.getCode());
        List<ActivityTask> activityTaskList = activityTaskService.selectByParams(taskParams);
        if (!CollectionUtils.isEmpty(activityTaskList)) {
            for (ActivityTask activityTask : activityTaskList) {
                Map<String, Object> utparams = new HashMap<>();
                utparams.put("shopId", shopId);
                utparams.put("userId", Long.valueOf(userId));
                utparams.put("type", UserTaskTypeEnum.GOOD_COMMENT.getCode());
                utparams.put("status", UserTaskStatusEnum.AUDIT_WAIT.getCode());
                utparams.put("activityTaskIds", Collections.singletonList(activityTask.getId()));
                List<UserTask> userTaskList = userTaskService.listPageByParams(utparams);
                if (!CollectionUtils.isEmpty(userTaskList)) {
                    CommissionBO commissionBO = commissionService.commissionCalculate(Long.valueOf(userId), activityTask.getRpAmount());
                    resultMap.put("amount", commissionBO.getCommission());
                    UserTask updateComment = userTaskList.get(0);
                    updateComment.setId(updateComment.getId());
                    updateComment.setOrderId(orderId);
                    updateComment.setUtime(new Date());
                    userTaskService.update(updateComment);
                }
            }
        }
        return resultMap;
    }


    private SimpleResponse executeFillBackOrder(List<ActivityDef> activityDefs, BackFillOrderReq req, JSONObject orderObj, List<Long> goodsIds) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Long userId = Long.valueOf(req.getUserId());
        Long shopId = Long.valueOf(req.getShopId());
        String orderId = req.getOrderId();
        // 如果同一订单已经被回填过就不能再次领取红包
        ActivityDef activityDef = activityDefs.get(0);
        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", activityDef.getMerchantId());
        userTaskParams.put("shopId", shopId);
        userTaskParams.put("userId", userId);
        userTaskParams.put("type", UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTaskParams.put("orderId", orderId);
        int countUserTask = userTaskService.countUserTask(userTaskParams);
        if (countUserTask > 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO13);
            simpleResponse.setMessage("该订单已完成回填确认，请填写其他订单");
            return simpleResponse;
        }
        /*
         * 判断是否是阶梯定价的类型，并计算出应扣除佣金金额
         * 区间红包金额,若是区间，则用json存储[{"from":"0.1", "to":"0.2","amount":"1"},{},{}]（非必填）
         */
        BigDecimal realCommission = appletUserActivityV2Service.calcCommission(orderObj, activityDef);


        // 创建userActivty
        UserActivity userActivity = new UserActivity();
        userActivity.setUserId(userId);
        userActivity.setMerchantId(activityDef.getMerchantId());
        userActivity.setShopId(shopId);
        userActivity.setActivityDefId(activityDef.getId());
        userActivity.setActivityId(0L);
        userActivity.setType(UserActivityTypeEnum.FILL_BACK_ORDER.getCode());
        userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE.getCode());
        userActivity.setCtime(new Date());
        userActivity.setUtime(new Date());
        userActivity.setActivityDefId(activityDef.getId());
        int rows = userActivityService.insertUserActivity(userActivity);
        if (rows == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            simpleResponse.setMessage("回填订单失败");
            return simpleResponse;
        }
        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配
        CommissionDisDto commissionDisDto = new CommissionDisDto(userId, shopId, UserCouponTypeEnum.ORDER_RED_PACKET, realCommission);
        commissionDisDto.setActivityDefId(activityDef.getId());
        CommissionBO commissionBO = null;
        try {
            commissionBO = commissionService.commissionDistribution(commissionDisDto);
        }catch (CommissionReduceException e) {
            log.error("Exception :{}", e);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO15.getCode(), "啊哦！奖金被领完了，老板正在充钱呢~");
        } catch (Exception e) {
            log.error("Exception :{}", e);
        }

        UserTask userTask = new UserTask();
        userTask.setUserId(userId);
        // 直接通过发放奖励
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        userTask.setActivityDefId(activityDef.getId());
        // 设置默认值
        userTask.setActivityId(0L);
        userTask.setUserActivityId(userActivity.getId());
        if (commissionBO != null) {
            userTask.setRpAmount(commissionBO.getCommission());
        }
        userTask.setMerchantId(activityDef.getMerchantId());
        userTask.setShopId(shopId);
        userTask.setType(UserTaskTypeEnum.ORDER_REDPACK.getCode());
        userTask.setOrderId(orderId);
        userTask.setActivityDefId(activityDef.getId());
        if (!CollectionUtils.isEmpty(goodsIds)) {
            userTask.setGoodsId(goodsIds.get(0));
        }
        Date now = new Date();
        userTask.setStartTime(now);
        userTask.setReceiveTime(now);
        userTask.setAuditingTime(now);
        userTask.setCtime(now);
        userTask.setUtime(now);
        userTaskService.insertUserTask(userTask);

        Map map = new HashMap();
        map.put("commission", BigDecimal.ZERO);
        if (commissionBO != null) {
            map.put("commission", commissionBO.getCommission());
        }
        simpleResponse.setData(map);
        return simpleResponse;
    }


    /**
     * 依据商品ID创建免单活动
     *
     * @param userId     用户id
     * @param orderId    订单号
     * @param merchantId 商户编号
     * @param shopId     店铺编号
     * @param jsonObject 订单信息
     * @param goodsIds   订单包含商品id
     */
    private void createActviityFromDefByGoodsId(Long userId, String orderId, Long merchantId, Long shopId, JSONObject jsonObject, List<Long> goodsIds) {
        Map<String, Object> defsParams = new HashMap<>();
        defsParams.clear();
        defsParams.put("merchantId", merchantId);
        defsParams.put("shopId", shopId);
        defsParams.put("goodsIdList", goodsIds);
        defsParams.put("type", ActivityDefTypeEnum.ORDER_BACK.getCode());
        defsParams.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        List<ActivityDef> activityDefList = activityDefService.selectByParams(defsParams);
        if (CollectionUtils.isEmpty(activityDefList)) {
            return;
        }
        ActivityDef orderBackDef = activityDefList.get(0);
        // 判断同一用户同一活动定义，总共只能发起三次
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("shopId", shopId);
        params.put("activityDefId", orderBackDef.getId());
        params.put("userId", userId);
        params.put("types", Arrays.asList(ActivityTypeEnum.ORDER_BACK_START.getCode(),ActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
        params.put("del", DelEnum.NO.getCode());
        List<Activity> activityList = activityService.findPage(params);
        if (activityList.size() >= 3) {
            return;
        }
        // 创建activity, 创建不成功，不再返回错误信息
        SimpleResponse simpleResponse1 = appletUserActivityService.createActivityFromDef(userId, orderId,
                orderBackDef.getId(), ActivityStatusEnum.UNPUBLISHED.getCode());
        if (simpleResponse1.error()) {
            return;
        }
        if (simpleResponse1.getData() == null) {
            return;
        }
        Activity activity = (Activity) simpleResponse1.getData();
        // 订单表
        Goods goods = goodsService.selectById(orderBackDef.getGoodsId());
        appletUserActivityService.updateOrCreateActivityOrder(userId, orderId, shopId, activity, goods, jsonObject);
    }
}
