package mf.code.api.applet.service.SendTemplateMessage.impl.award;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMsgService;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * mf.code.api.applet.service.SendTemplateMessage.impl.award
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 10:53
 */
@Service
@Slf4j
public class NomalSendTemplateMsgConverterImpl extends SendTemplateMsgService {
    @Autowired
    private RedisService redisService;
    @Autowired
    private UserService userService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @Override
    public String awardSendTemplateMessageConverter(MerchantShop merchantShop,
                                                    Activity activity,
                                                    Long nextAwardUserID) {
        List<Integer> activityTypes = Arrays.asList(
                ActivityTypeEnum.NEW_MAN_START.getCode(),
                ActivityTypeEnum.ORDER_BACK_START.getCode(),
                ActivityTypeEnum.ORDER_BACK_START_V2.getCode(),
                ActivityTypeEnum.MCH_PLAN.getCode()
        );
        if (!activityTypes.contains(activity.getType())) {
            return null;
        }
        Goods goods = this.goodsService.selectById(activity.getGoodsId());
        if (goods == null) {
            log.warn("活动编号为：{}，推送消息时，对应的商品不存在", activity.getId());
            return null;
        }

        //根据活动获取该活动参加的人
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(activity.getId());
        List<Long> userIDs = new ArrayList<Long>();
        if (activityPersonList != null && activityPersonList.size() > 0) {
            for (String str : activityPersonList) {
                JSONObject jsonObject = JSON.parseObject(str);
                Long userID = jsonObject.getLong("uid");
                if (userID != null && userIDs.indexOf(userID) == -1) {
                    userIDs.add(userID);
                }
            }
        }

        //新人有礼没有人参加失效的推送消息处理
        if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode() && CollectionUtils.isEmpty(userIDs)) {
            this.sendNewManStartMessage(activity);
            return null;
        }

        //查询该活动下的所有用户，获得openid
        Map<String, Object> userParams = new HashMap<String, Object>();
        List<User> users = new ArrayList<>();
        if (!CollectionUtils.isEmpty(userIDs)) {
            userParams.put("userIds", userIDs);
            users = this.userService.query(userParams);
        }
        if (CollectionUtils.isEmpty(users)) {
            log.warn("活动编号为：{}，推送消息时,查询该活动参与的redis，对应的用户不存在，redis的key为：{}", activity.getId(), "templateMessage:formId:userId:");
        }
        Map<Long, User> userMap = new HashMap<Long, User>();
        for (User user : users) {
            userMap.put(user.getId(), user);
        }

        //免单发起|新手有礼发起|计划类
        if (userIDs != null && userIDs.size() > 0) {
            //判断该活动是否是新人有礼
            if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
                this.sendNewManStartMessage(activity);
            }

            for (Long userID : userIDs) {
                User user = userMap.get(userID);
                if (user == null) {
                    continue;
                }
                String formIDStr = this.queryRedisUserFormIds(userID);
                if (StringUtils.isBlank(formIDStr)) {
                    log.warn("活动编号：{}，该用户编号：{} 的formId不存在", activity.getId(), userID);
                    continue;
                }
                this.addActivitySingleSendTemplateMsg(merchantShop, activity, goods, user, formIDStr);
            }
        }

        return ApiStatusEnum.SUCCESS.getMessage();
    }

    /***
     * 新人有礼发起者的消息推送
     * @param activity
     */
    private Map<String, Object> sendNewManStartMessage(Activity activity) {
        String formIDStr = this.queryRedisUserFormIds(activity.getUserId());
        if (StringUtils.isNotBlank(formIDStr)) {
            String templateID = wxmpProperty.getOrderProessMsgTmpId();
            Object[] objects = new Object[]{"已获得免单资格", "您发起的抽奖活动已达到开奖条件，开团成功", "请您尽快完成领奖"};
            if (activity.getStatus() == ActivityStatusEnum.FAILURE.getCode()) {
                templateID = wxmpProperty.getOrderProessMsgTmpId();
                objects = new Object[]{"未达到活动条件", "您发起的抽奖活动未达到开奖条件，开团失败", "再次发起"};
            }
            int newManKeywordNum = objects.length;
            String newManFormID = formIDStr;
            User user = this.userService.selectByPrimaryKey(activity.getUserId());

            WxSendMsgVo voNewMan = new WxSendMsgVo();
            voNewMan.from(templateID, user.getOpenId(), newManFormID, this.getPageUrl(activity, user.getId(), null),
                    this.getTempteDate(newManKeywordNum, objects), EMPHASISKEYWORD1);

            Map<String, Object> stringObjectMapNewMan = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), voNewMan);
            log.info("抽奖活动推送消息结果--新人发起者：{}, parms:{}", stringObjectMapNewMan, voNewMan);
            this.respMap(stringObjectMapNewMan, voNewMan, user.getId());
        } else {
            log.warn("超级警戒，新人有礼发起者未得到推送消息");
        }
        return null;
    }

    /***
     * 推送活动-消息的单个
     * @param activity
     * @param user
     * @param formId
     */
    private void addActivitySingleSendTemplateMsg(MerchantShop merchantShop,
                                                  Activity activity,
                                                  Goods goods,
                                                  User user,
                                                  String formId) {
        Object[] objects = {};
        String templateID = "";
        boolean newMan = false;
        //判断该活动是否未成团失效
        if (activity.getStatus() == ActivityStatusEnum.FAILURE.getCode()) {
            templateID = wxmpProperty.getOrdercancelMsgTmpId();
            objects = new Object[]{"抽奖活动已取消", "您参加的抽奖活动由于规定时间内未达到开奖条件已自动失效", "去参加其余抽奖"};

            int keywordNum = objects.length;

            WxSendMsgVo vo = new WxSendMsgVo();
            vo.from(templateID, user.getOpenId(), formId, this.getPageUrl(activity, null, null),
                    this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);

            Map<String, Object> stringObjectMap = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
            log.info("抽奖活动推送消息结果--未成团：{}, parms:{}", stringObjectMap, vo);
            this.respMap(stringObjectMap, vo, user.getId());
        } else {
            //正常的抽奖活动开奖 区分计划类|发起者类
            if (activity.getType() == ActivityTypeEnum.MCH_PLAN.getCode()) {
                templateID = wxmpProperty.getPlanAwardResultMsgTmpId();
                objects = new Object[]{"开奖结果公布", "《" + goods.getDisplayGoodsName() + "》抽奖活动", "请及时查看结果，以免错过领奖", merchantShop.getShopName()};
                int keywordNum = objects.length;

                WxSendMsgVo vo = new WxSendMsgVo();
                vo.from(templateID, user.getOpenId(), formId, this.getPageUrl(activity, null, null),
                        this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);

                Map<String, Object> stringObjectMap = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
                log.info("抽奖活动推送消息结果--开奖：{}, parms:{}", stringObjectMap, vo);
                this.respMap(stringObjectMap, vo, user.getId());
            } else if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activity.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
                templateID = wxmpProperty.getUserAwardResultMsgTmpId();
                objects = new Object[]{"开奖结果公布", "《" + goods.getDisplayGoodsName() + "》抽奖活动", "请及时查看结果，以免错过领奖"};
                int keywordNum = objects.length;

                WxSendMsgVo vo = new WxSendMsgVo();
                vo.from(templateID, user.getOpenId(), formId, this.getPageUrl(activity, null, null),
                        this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);

                Map<String, Object> stringObjectMap = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), vo);
                log.info("抽奖活动推送消息结果--开奖：{}, parms:{}", stringObjectMap, vo);
                this.respMap(stringObjectMap, vo, user.getId());
            }
        }
    }
}
