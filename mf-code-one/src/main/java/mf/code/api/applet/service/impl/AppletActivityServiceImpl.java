package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSONArray;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.activity.service.ActivityTaskService;
import mf.code.api.applet.dto.ActivityDefResp;
import mf.code.api.applet.service.AppletActivityService;
import mf.code.api.applet.service.FakeService;
import mf.code.common.constant.*;
import mf.code.common.redis.RedisCommonKeys;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.FakeDataUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.service.UserTaskService;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.service.UpayBalanceService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.service.impl
 * Description:
 *
 * @author: gel
 * @date: 2018-11-07 17:16
 */
@Service
public class AppletActivityServiceImpl implements AppletActivityService {
    @Autowired
    private FakeService fakeService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 查询小程序端免单商品页面 新人有礼，免单机会活动
     *
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse orderback(Long shopId, Long userId) {
        /**
         * 1、查询activity_def
         * 2、查询activity，新人有礼活动参与资格
         * 3、查询activity，免单机会参与资格
         */
        SimpleResponse simpleResponse = new SimpleResponse();
        if (shopId == null || userId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("参数为空");
            return simpleResponse;
        }
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("传入店铺信息有误");
            return simpleResponse;
        }
        // 查询activity_def
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantShop.getMerchantId());
        params.put("shopId", shopId);
        params.put("queryTime", new Date());
        List<Integer> typeList = new ArrayList<>(2);
        typeList.add(ActivityDefTypeEnum.ORDER_BACK.getCode());
        typeList.add(ActivityDefTypeEnum.NEW_MAN.getCode());
        params.put("typeList", typeList);
        params.put("status", ActivityDefStatusEnum.PUBLISHED.getCode());
        params.put("del", DelEnum.NO.getCode());
        List<ActivityDef> activityDefs = activityDefService.selectByParams(params);
        // 数据分类
        List<ActivityDefResp> newManList = new ArrayList<>();
        List<ActivityDefResp> orderBackList = new ArrayList<>();
        Map<String, Object> resultMap = new HashMap<>(2);
        if (CollectionUtils.isEmpty(activityDefs)) {
            resultMap.put("newManList", newManList);
            resultMap.put("orderBackList", orderBackList);
            // 添加假数据
            FakeDataUtil.orderBackData(resultMap);
            simpleResponse.setData(resultMap);
            return simpleResponse;
        }
        List<Long> defIdList = new ArrayList<>();
        List<Long> goodsIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            ActivityDefResp resp = copyDefToResp(activityDef);
            goodsIds.add(activityDef.getGoodsId());
            if (activityDef.getType() == ActivityDefTypeEnum.NEW_MAN.getCode()) {
                newManList.add(resp);
            }
            if (activityDef.getType() == ActivityDefTypeEnum.ORDER_BACK.getCode()) {
                orderBackList.add(resp);
                defIdList.add(activityDef.getId());
            }
        }
        // 查询商品数据
        List<Goods> goodsList = goodsService.selectByIdList(goodsIds);
        if (CollectionUtils.isEmpty(goodsList)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("数据有误");
            return simpleResponse;
        }
        Map<Long, Goods> goodsMap = new HashMap<>(goodsList.size());
        for (Goods goods : goodsList) {
            goodsMap.put(goods.getId(), goods);
        }
        // 查询新人有礼活动参与资格
        Map<String, Object> newManParams = new HashMap<>();
        newManParams.put("merchantId", merchantShop.getMerchantId());
        newManParams.put("shopId", shopId);
        newManParams.put("userId", userId);
        newManParams.put("type", ActivityTypeEnum.NEW_MAN_START.getCode());
        List<Integer> statusList = new ArrayList<>();
        // 未付款发起成功，不允许重新发起
//        statusList.add(ActivityStatusEnum.UNPUBLISHED.getCode());
        // 正在进行中，不允许重新发起
        statusList.add(ActivityStatusEnum.PUBLISHED.getCode());
        // 发起者，中奖者都完成任务，记做已结束，不允许重新发起
        statusList.add(ActivityStatusEnum.END.getCode());
        newManParams.put("activityStatuses", statusList);
        List<Activity> newManActList = activityService.findPage(newManParams);
        // 判断有无，修改新人有礼任务状态
        for (ActivityDefResp resp : newManList) {
            resp.setPicUrl(goodsMap.get(Long.valueOf(resp.getGoodsId())).getPicUrl());
            resp.setTitle(goodsMap.get(Long.valueOf(resp.getGoodsId())).getDisplayGoodsName());
            // 只要完成或正在进行一次该店铺下的新人有礼活动，就显示已发起
            if (!CollectionUtils.isEmpty(newManActList)) {
                for (Activity activity : newManActList) {
                    if (!activity.getActivityDefId().equals(Long.valueOf(resp.getActivityDefId()))) {
                        continue;
                    }
                    resp.setActivityId(activity.getId().toString());
                    if (activity.getStatus() == ActivityStatusEnum.PUBLISHED.getCode()
                            && activity.getAwardStartTime() == null) {
                        resp.setActStatus(OrderBackFrontStatusEnum.NEW_ING.getCode().toString());
                        resp.setErrorMsg("活动未结束不能发起");
                        break;
                    }
                    // 只有验证发起者有没有完成任务user_task
                    Map<String, Object> taskParams = new HashMap<>();
                    taskParams.put("type", UserTaskTypeEnum.NEW_MAN_START.getCode());
                    taskParams.put("userId", userId);
                    taskParams.put("merchantId", merchantShop.getMerchantId());
                    taskParams.put("shopId", merchantShop.getId());
                    taskParams.put("activityId", activity.getId());
                    List<Integer> userTaskStatusList = new ArrayList<>();
                    userTaskStatusList.add(UserTaskStatusEnum.INIT.getCode());
                    userTaskStatusList.add(UserTaskStatusEnum.AUDIT_WAIT.getCode());
                    userTaskStatusList.add(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
                    userTaskStatusList.add(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
                    taskParams.put("statusList", userTaskStatusList);
                    List<UserTask> countUserTask = userTaskService.listPageByParams(taskParams);
                    if (!CollectionUtils.isEmpty(countUserTask)) {
                        resp.setActStatus(OrderBackFrontStatusEnum.NEW_TASK.getCode().toString());
                        // 添加个人微信号
                        Map<String, Object> customServiceAbout = merchantShopService.getCustomServiceAbout(merchantShop);
                        if (!CollectionUtils.isEmpty(customServiceAbout) && StringUtils.isBlank(resp.getCustomServiceWxNum())) {
                            resp.setCustomServiceWxNum(customServiceAbout.get("customServiceWxNum").toString());
                        }
                        resp.setErrorMsg("任务进行中");
                        resp.setTaskStatus(countUserTask.get(0).getStatus().toString());
                        break;
                    }
                }
            }
        }

        // 查询免单活动参与资格
        Map<String, Object> orderBackParams = new HashMap<>();
        orderBackParams.put("merchantId", merchantShop.getMerchantId());
        orderBackParams.put("shopId", shopId);
        orderBackParams.put("userId", userId);
        orderBackParams.put("types", Arrays.asList(ActivityTypeEnum.ORDER_BACK_START.getCode(),ActivityTypeEnum.ORDER_BACK_START_V2.getCode()));
        List<Integer> activityStatuses = new ArrayList<>();
        activityStatuses.add(ActivityStatusEnum.UNPUBLISHED.getCode());
        activityStatuses.add(ActivityStatusEnum.PUBLISHED.getCode());
        activityStatuses.add(ActivityStatusEnum.FAILURE.getCode());
        orderBackParams.put("activityStatuses", activityStatuses);
        orderBackParams.put("activityDefIdList", defIdList);
        List<Activity> orderBackActList = activityService.findPage(orderBackParams);
        Map<String, Object> countMap = new HashMap<>();
        // 如果不为空则需要修改免单活动参与状态
        for (ActivityDefResp resp : orderBackList) {
            resp.setPicUrl(goodsMap.get(Long.valueOf(resp.getGoodsId())).getPicUrl());
            resp.setTitle(goodsMap.get(Long.valueOf(resp.getGoodsId())).getDisplayGoodsName());
            if (CollectionUtils.isEmpty(orderBackActList)) {
                continue;
            }
            for (Activity activity : orderBackActList) {
                if (!activity.getActivityDefId().toString().equals(resp.getActivityDefId())) {
                    continue;
                }
                if (ActivityStatusEnum.UNPUBLISHED.getCode() == activity.getStatus()) {
                    resp.setActivityId(activity.getId().toString());
                    resp.setActStatus(OrderBackFrontStatusEnum.ORDER_PAY.getCode().toString());
                    resp.setErrorMsg("可发起支付");
                    break;
                } else if (ActivityStatusEnum.PUBLISHED.getCode() == activity.getStatus()) {
                    resp.setActStatus(OrderBackFrontStatusEnum.ORDER_CANNOT.getCode().toString());
                    resp.setErrorMsg("活动未结束不能发起");
                    break;
                } else {
                    resp.setActStatus(OrderBackFrontStatusEnum.ORDER_PAY.getCode().toString());
                    resp.setErrorMsg("");
                }
            }
            // TODO 查询完成活动次数，待优化
            countMap.put("activityDefId", resp.getActivityDefId());
            countMap.put("merchantId", merchantShop.getMerchantId());
            countMap.put("shopId", merchantShop.getId());
            countMap.put("userId", userId);
            countMap.put("del", DelEnum.NO.getCode());
            int size = activityService.countActivity(countMap);
            if (size >= 3) {
                resp.setActStatus(OrderBackFrontStatusEnum.ORDER_CANNOT.getCode().toString());
                resp.setErrorMsg("本商品您已发起3次抽奖活动，不能再发起");
                continue;
            }
        }
        resultMap.put("newManList", newManList);
        resultMap.put("orderBackList", orderBackList);
        // 添加假数据
        FakeDataUtil.orderBackData(resultMap);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /***
     * 查询活动--跳转支付中间页--夜辰
     * @param userID
     * @return
     */
    @Override
    public SimpleResponse queryActivity(Long activityDefID, Long userID) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (activityDefID == null || activityDefID == 0 || userID == null || userID == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("入参传入异常");
            return simpleResponse;
        }

        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityDefID);
        if (activityDef == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("该活动定义不存在");
            return simpleResponse;
        }
        Date startTime = activityDef.getStartTime();
        Date endTime = activityDef.getEndTime();
        Long goodsID = activityDef.getGoodsId();
        Long merchantID = activityDef.getMerchantId();
        Long shopID = activityDef.getShopId();

        Goods goods = this.goodsService.selectById(goodsID);
        if (goods == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("该活动下对应的goods不存在");
            return simpleResponse;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("picUrl", goods.getPicUrl());
        map.put("title", goods.getDisplayGoodsName());
        map.put("price", goods.getDisplayPrice());
        map.put("goodsId", goods.getId());
        map.put("startTime", DateFormatUtils.format(startTime, "yyyy.MM.dd HH:mm"));
        map.put("endTime", DateFormatUtils.format(endTime, "yyyy.MM.dd HH:mm"));
        map.put("isValid", true);
        map.put("condPersionCount", activityDef.getCondPersionCount() == null ? 0 : activityDef.getCondPersionCount());
        //验证这个支付页是否可以有效进行付款
        simpleResponse = this.checkPay(activityDefID, userID);
        if (simpleResponse.error()) {
            map.put("isValid", false);
        }

        //查询余额
        UpayBalance upayBalance = this.upayBalanceService.query(merchantID, shopID, userID);
        map.put("balance", BigDecimal.ZERO);
        if (upayBalance != null) {
            map.put("balance", upayBalance.getBalance());
        }

        // 1.4期 调整页面返回的数据
        Integer stockType = activityDef.getStockType();
        map.put("stockStatus", ActivityDefStockTypeEnum.COMPLETE_REDUCE.getCode().equals(stockType));
        // 剩余库存
        Integer stock = activityDef.getStock();
        // 总库存
        Integer stockDef = activityDef.getStockDef();

        map.put("stock", stock);
        map.put("hitsPerDraw", activityDef.getHitsPerDraw());
        map.put("stockDef", stockDef);
        map.put("rate", (stock * 100) / stockDef);

        simpleResponse.setData(map);
        return simpleResponse;
    }

    /**
     * 检查是否能跳转支付页面
     *
     * @param activityDefId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse checkPay(Long activityDefId, Long userId) {
        return activityService.checkPayForActivity(activityDefId, userId);
    }


    @Override
    public SimpleResponse awardActivityList(Long merchantId, Long shopId, Long activityDefId, Long goodsId, Long userId) {
        SimpleResponse response = new SimpleResponse();
        Map<String, Object> resultMap = new HashMap<>();
        //产品详情
        Goods goods = goodsService.selectById(goodsId);

        List<Map> activityList = activityService.findByDefId(activityDefId);
        if (activityList == null || activityList.size() == 0) {
            // goods 详情
            Map<String, Object> resultGoods = new HashMap<>();
            resultGoods.put("picUrl", goods.getPicUrl());
            resultGoods.put("goodsName", goods.getDisplayGoodsName());
            resultGoods.put("goodsPrice", goods.getDisplayPrice());
            resultMap.put("goods", resultGoods);
            response.setData(resultMap);
            return response;
        }
        Long joinPersonTotal = 0L;
        for (Map map : activityList) {
            Long activityId = Long.parseLong(map.get("activityId").toString());
            //已参与人数
            String key = RedisCommonKeys.FAKE_DATA_LUCHY_DRAW_LIST_ITEM + merchantId + ":" + shopId;
            Object joinPersonStr = stringRedisTemplate.opsForHash().get(key, goodsId.toString());

            String redisKey = RedisKeyConstant.ACTIVITY_TRIGGER + activityId;
            String joinPersionThisActivity = stringRedisTemplate.opsForValue().get(redisKey);

            //中奖人数        真   查 数据库
            map.put("hits_per_draw", activityService.findById(activityId).getHitsPerDraw());
            //参与人数        假   查 redis  -> 给首页用
            map.put("joinPerson", joinPersonStr == null ? 0 : joinPersonStr);
            //此活动参与人数  真   查  redis
            Integer record = joinPersionThisActivity == null ? 0 : Integer.parseInt(joinPersionThisActivity);
            map.put("joinPersionThisActivity", record);
            joinPersonTotal = joinPersonTotal + record;
            //参与状态
            Activity a = activityService.findById(activityId);
            if (a == null) {
                map.put("hasJoin", false);
            } else {
                Integer status = a.getStatus();
                map.put("hasJoin", true);
                map.put("activityStatus", status);
            }
            //时间
            String startTimeCalc = map.get("startTimeCalc") == null ? "" : map.get("startTimeCalc").toString();
            Date startTimeDate = DateUtil.stringtoDate(startTimeCalc, DateUtil.FORMAT_ONE);
            Date now = new Date();
            Long dayDiff = DateUtil.dayDiff(startTimeDate, now);
            long minuteDiff = DateUtil.minuteDiff(startTimeDate, now);
            long secondDiff = DateUtil.secondDiff(startTimeDate, now);
            map.put("day", dayDiff);
            map.put("hour", (minuteDiff % (24 * 60)) / 60);
            map.put("minute", minuteDiff % 60);
            map.put("second", ((secondDiff % (24 * 60 * 60)) % (60 * 60)) % 60);
        }

        // 活动定义内容
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDefId);
        Map<String, Object> defMap = new HashMap<>();
        defMap.put("defId", activityDef.getId());
        defMap.put("picUrl", goods.getPicUrl());
        defMap.put("goodsName", goods.getDisplayGoodsName());
        defMap.put("hitsPerDraw", activityDef.getHitsPerDraw());
        defMap.put("sumGoodsStock", activityDef.getStock() == null ? 0 : activityDef.getStock());
        defMap.put("goodsPrice", goods.getDisplayPrice());
        defMap.put("condPersionCount", activityDef.getCondPersionCount());
        //红包数量   真
        Long stock = activityTaskService.getStockByGoodsId(goodsId);
        defMap.put("taskRpNum", stock == null ? 0 : stock);
        //参与人数
        defMap.put("joinPersonTotal", joinPersonTotal);
        // 2019-03-19 添加页面数据: condDrawTime活动时长； stockRate库存比例； canLaunch能否发起
        defMap.put("condDrawTime", activityDef.getCondDrawTime());
        defMap.put("stockRate", (activityDef.getStock() * 100) / activityDef.getStockDef());
        SimpleResponse simpleResponse = activityService.checkPayForActivity(activityDef.getId(), userId);
        defMap.put("canLaunch", !simpleResponse.error());

        resultMap.put("activityInfo", defMap);
        resultMap.put("activityList", activityList);
        response.setData(resultMap);
        return response;

    }

    /**
     * 计算假数据
     *
     * @param goodsId
     * @return
     */
    private String getCoupon(Long goodsId) {
        String redisKey = "home:fake:goodsId:coupon:" + goodsId;
        String val = stringRedisTemplate.opsForValue().get(redisKey);

        if (val == null) {
            Integer num = this.getRandom(new Random().nextInt(6) + 1);
            stringRedisTemplate.opsForValue().set(redisKey, num.toString());
            return num.toString();
        } else {
            Integer valInt = Integer.parseInt(val);
            String redisIncrKey = redisKey + ":incr";
            Long increment = stringRedisTemplate.opsForValue().increment(redisIncrKey, 1L);
            Long result = valInt - increment;
            if (result >= 0) {
                return result.toString();
            } else {
                stringRedisTemplate.opsForValue().set(redisIncrKey, "0");
                Integer num = this.getRandom(new Random().nextInt(6) + 1);
                stringRedisTemplate.opsForValue().set(redisKey, num.toString());
                return num.toString();
            }
        }
    }

    private Integer getRandom(Integer hundred) {
        Random r = new Random();
        hundred = hundred * 23;

        Integer ten = r.nextInt(99);
        return (Integer.parseInt(hundred.toString()) * 100) + ten;
    }

    @Override
    public SimpleResponse joinAndInitiate(Long merchantId, Long shopId, Long activityId, Long goodsId, Long userId) {
        return null;
    }

    private ActivityDefResp copyDefToResp(ActivityDef activityDef) {
        ActivityDefResp resp = new ActivityDefResp();
        resp.setActivityDefId(activityDef.getId().toString());
        resp.setPrice(activityDef.getGoodsPrice().toString());
        resp.setStock(activityDef.getStock().toString());
        resp.setGoodsId(activityDef.getGoodsId().toString());
        // 如果新人有礼库存只有小于等于1则禁止创建
        if (activityDef.getType() == ActivityDefTypeEnum.NEW_MAN.getCode()) {
            resp.setActStatus(OrderBackFrontStatusEnum.NEW_PAY.getCode().toString());
            if (activityDef.getStock() <= 1) {
                resp.setActStatus(OrderBackFrontStatusEnum.STOCK.getCode().toString());
                resp.setErrorMsg("该商品暂无库存");
            }
        }
        // 无库存不能参加
        if (activityDef.getStock() <= 0) {
            resp.setActStatus(OrderBackFrontStatusEnum.STOCK.getCode().toString());
            resp.setErrorMsg("该商品暂无库存");
        }
        // 20190119 更新客服微信号和微信图片
        if (StringUtils.isNotBlank(activityDef.getServiceWx())) {
            if (JsonParseUtil.booJsonArr(activityDef.getServiceWx())) {
                List<Map> csMapList = JSONArray.parseArray(activityDef.getServiceWx(), Map.class);
                if (!CollectionUtils.isEmpty(csMapList)) {
                    //微信号
                    resp.setCustomServiceWxNum((String) csMapList.get(0).get("wechat"));
                    //微信二维码
                    resp.setCustomServiceCode((String) csMapList.get(0).get("pic"));
                }
            }
        }
        return resp;
    }
}
