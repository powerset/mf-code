package mf.code.api.applet.login.domain.repository.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.login.domain.valueobject.ActivityInvitationSpecification;
import mf.code.api.applet.login.domain.valueobject.InvitationDimensionEnum;
import mf.code.api.applet.login.domain.valueobject.InvitationRoleEnum;
import mf.code.api.applet.login.domain.valueobject.InviteSpecification;
import mf.code.common.dto.WxUserSession;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.utils.RegexUtils;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.login.domain.repository.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-18 上午10:34
 */
@Slf4j
@Service
public class AppletUserRepositoryImpl implements AppletUserRepository {
	private final StringRedisTemplate stringRedisTemplate;
	private final UserService userService;
	private final UserPubJoinService userPubJoinService;

	@Autowired
	public AppletUserRepositoryImpl(StringRedisTemplate stringRedisTemplate,
	                                UserService userService,
	                                UserPubJoinService userPubJoinService) {
		this.stringRedisTemplate = stringRedisTemplate;
		this.userService = userService;
		this.userPubJoinService = userPubJoinService;
	}

	@Override
	public AppletUserAggregateRoot updateOrSaveByWxUserSession(WxUserSession wxUserSession) {
		// 获取AppletUser的跟踪标识 openid
		String openid = wxUserSession.getOpenid();

		// 加锁 (先查后插，原子性操作，redis防重)
		boolean locked = RedisForbidRepeat.NX(this.stringRedisTemplate, RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openid);
		if (!locked) {
			log.info("正在处理上一个登录请求");
			return null;
		}

		// 用户信息 先查
		User user = userService.findByOpenid(openid);
		if (user != null) {
			// 老用户 更新用户最近状态
			user = updateUser(user, wxUserSession);
		} else {
			// 后插 新用户信息
			user = saveUser(wxUserSession);
			if (user == null) {
				// 释放锁
				stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openid);

				// 用户未授权
				return null;
			}
		}

		assert user != null;
		if (user.getId() != null) {
			stringRedisTemplate.opsForValue().set(RedisKeyConstant.MF_CODE_USER_CACHE + user.getId(), JSON.toJSONString(user), 3, TimeUnit.SECONDS);
		}

		// 装配 appletUser
		AppletUserAggregateRoot appletUser = assemblyAppletUserWithoutInvitationHistory(user);

		if (appletUser.getBeVipShop()) {
			user.setVipShopId(user.getShopId());
		}
		boolean saveOrUpdate = userService.saveOrUpdate(user);
		if (!saveOrUpdate) {
			log.error("插入失败");

			// 释放锁
			stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openid);
			return null;
		}
		appletUser.setUserId(user.getId());

		// 释放锁
		stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openid);
		return appletUser;
	}

	@Override
	public AppletUserAggregateRoot findById(Long userId) {
		User user;
		String userJson = stringRedisTemplate.opsForValue().get(RedisKeyConstant.MF_CODE_USER_CACHE + userId);
		if (StringUtils.isNotBlank(userJson)) {
			user = JSON.parseObject(userJson, User.class);
		} else {
			// 查询邀请者 用户信息
			user = userService.getById(userId);
		}

		if (user == null) {
			return null;
		}
		stringRedisTemplate.opsForValue().set(RedisKeyConstant.MF_CODE_USER_CACHE + user.getId(), JSON.toJSONString(user), 3, TimeUnit.SECONDS);

		// 装配 appletUser
		return assemblyAppletUserWithoutInvitationHistory(user);
	}

	@Override
	public AppletUserAggregateRoot findInviteHistoryByUserId(String pubUserId, InviteSpecification invitationSpecification) {
		if (StringUtils.isBlank(pubUserId) || !RegexUtils.StringIsNumber(pubUserId)) {
			return null;
		}
		AppletUserAggregateRoot appletPubUser = this.findById(Long.valueOf(pubUserId));
		if (appletPubUser == null) {
			return null;
		}
		if (invitationSpecification == null) {
			return appletPubUser;
		}
		if (InvitationRoleEnum.PUB != invitationSpecification.getInvitationRoleEnum()) {
			return appletPubUser;
		}
		// 获取参数
		ActivityInvitationSpecification ais = (ActivityInvitationSpecification)invitationSpecification;
		Long userId = appletPubUser.getUserId();
		Long activityId = Long.valueOf(ais.getActivityId());

		if (!ais.isInvite()) {
			return appletPubUser;
		}

		List<UserPubJoin> userPubJoins = null;
		if (ais.getInvitationDimension() == InvitationDimensionEnum.ACTIVITY) {
			QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
			wrapper.lambda()
					.eq(UserPubJoin::getUserId, userId)
					.eq(UserPubJoin::getActivityId, activityId);
			userPubJoins = userPubJoinService.list(wrapper);
		}
		if (ais.getInvitationDimension() == InvitationDimensionEnum.SHOP) {
			QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
			wrapper.lambda()
					.eq(UserPubJoin::getUserId, userId)
					.eq(UserPubJoin::getType, ais.getUserPubJoinType())
					.eq(UserPubJoin::getShopId, ais.getShopId());
			userPubJoins = userPubJoinService.list(wrapper);
		}

		appletPubUser.setInvitationHistory(activityId, userPubJoins, null);
		return appletPubUser;
	}

	@Override
	public AppletUserAggregateRoot findInvitedHistoryByUserId(String subUserId, InviteSpecification invitationSpecification) {
		if (StringUtils.isBlank(subUserId) || !RegexUtils.StringIsNumber(subUserId)) {
			return null;
		}
		AppletUserAggregateRoot appletSubUser = this.findById(Long.valueOf(subUserId));
		if (appletSubUser == null) {
			return null;
		}

		// 获取参数
		if (invitationSpecification == null) {
			return appletSubUser;
		}
		if (InvitationRoleEnum.SUB != invitationSpecification.getInvitationRoleEnum()) {
			return appletSubUser;
		}

		Integer type = Integer.valueOf(invitationSpecification.getUserPubJoinType());

		QueryWrapper<UserPubJoin> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(UserPubJoin::getSubUid, subUserId)
				.eq(UserPubJoin::getType, type);
		List<UserPubJoin> userPubJoins = userPubJoinService.list(wrapper);
		// 可以没有记录，也可以是这条记录 上级是平台，或者是用户，没有记录，或上级是平台可以被邀请

		// 获取平台id
		QueryWrapper<UserPubJoin> platformWrapper = new QueryWrapper<>();
		platformWrapper.lambda()
				.eq(UserPubJoin::getSubUid, 0)
				.eq(UserPubJoin::getType, type);
		UserPubJoin userPubJoin = userPubJoinService.getOne(platformWrapper);

		appletSubUser.setInvitationHistory(type, userPubJoins, userPubJoin.getUserId());
		return appletSubUser;
	}

	/**
	 * 获取平台用户
	 * @return
	 */
	@Override
	public AppletUserAggregateRoot findPlatform() {
		QueryWrapper<User> wrapper = new QueryWrapper<>();
		wrapper.lambda()
				.eq(User::getMerchantId, 0)
				.eq(User::getShopId, 0)
				.eq(User::getGrantStatus, -2);
		User user = this.userService.getOne(wrapper);
		// 装配用户
		return assemblyAppletUserWithoutInvitationHistory(user);
	}

	/**
	 * 增装 活动用户的邀请事件
	 * @param appletUser
	 * @param appletActivity
	 */
	@Override
	public void addActivityInviteEvent(AppletUserAggregateRoot appletUser, AppletActivity appletActivity) {

	}

	private AppletUserAggregateRoot assemblyAppletUserWithoutInvitationHistory(User user) {
		AppletUserAggregateRoot appletUser = new AppletUserAggregateRoot();

		appletUser.setOpenid(user.getOpenId());
		appletUser.setUserId(user.getId());
		appletUser.setUserinfo(user);
		appletUser.setShopId(user.getShopId());

		if (user.getVipShopId() == null) {
			if (user.getShopId() != null) {
				// 成为该店铺的第几位粉丝
				QueryWrapper<User> userWrapper = new QueryWrapper<>();
				userWrapper.lambda()
						.eq(User::getVipShopId, user.getShopId())
						.eq(User::getGrantStatus, 1);
				List<User> users = userService.list(userWrapper);
				appletUser.setVipShopInfo(true, users.size());
			}
		}
		return appletUser;
	}

	private User updateUser(User user, WxUserSession wxUserSession) {
		Date now = new Date(System.currentTimeMillis() / 1000 * 1000);

		// 获取参数
		String merchantId = wxUserSession.getMerchantId();
		String shopId = wxUserSession.getShopId();
		String scene = wxUserSession.getScene();
		JSONObject wxUser = wxUserSession.getWxUser();

		// 新用户
		if (user == null) {
			if (wxUser == null) {
				// 未授权
				return null;
			} else {
				// 已授权
				User newUser = new User();
				newUser.setNickName(wxUser.getString("nickName"));
				newUser.setGender(wxUser.getIntValue("gender"));
				newUser.setCity(wxUser.getString("city"));
				newUser.setProvince(wxUser.getString("province"));
				newUser.setCountry(wxUser.getString("country"));
				newUser.setAvatarUrl(wxUser.getString("avatarUrl"));
				newUser.setGrantStatus(1);
				newUser.setGrantTime(now);
				newUser.setCtime(now);
                if (wxUser.containsKey("unionId")) {
	                newUser.setUnionId(wxUser.getString("unionId"));
                }
                user = newUser;
			}
		}

		// 老用户（更新） 或 授权后的新用户（插入）
		user.setOpenId(wxUserSession.getOpenid());
		user.setSessionKey(wxUserSession.getSessionKey());

		if (StringUtils.isNotBlank(merchantId) && RegexUtils.StringIsNumber(merchantId)) {
			user.setMerchantId(Long.valueOf(merchantId));
		}
		if (StringUtils.isNotBlank(shopId) && RegexUtils.StringIsNumber(shopId)) {
			user.setShopId(Long.valueOf(shopId));
		}
		if (StringUtils.isNotBlank(scene) && RegexUtils.StringIsNumber(scene)) {
			user.setScene(Integer.valueOf(scene));
		}
		user.setUtime(new Date());

		return user;
	}

	private User saveUser(WxUserSession wxUserSession) {
		return updateUser(null, wxUserSession);
	}
}
