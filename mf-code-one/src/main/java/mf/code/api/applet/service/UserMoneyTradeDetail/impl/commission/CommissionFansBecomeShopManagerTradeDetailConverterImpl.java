package mf.code.api.applet.service.UserMoneyTradeDetail.impl.commission;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyIndexNameEnum;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 19:21
 */
@Service
@Slf4j
public class CommissionFansBecomeShopManagerTradeDetailConverterImpl extends UserMoneyTradeDetailService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private UserService userService;

    @Override
    public Map<String, Object> getTaskTradeDetail(UpayWxOrder upayWxOrder,
                                                  Integer payType,
                                                  Map<Long, User> userMap) {
        //PALTFORMRECHARGE：平台充值 USERCASH：用户提现 USERPAY：用户支付 PALTFORMREFUND：平台退款
        boolean nullPayType = payType == null;
        boolean rebate = OrderTypeEnum.REBATE.getCode() == upayWxOrder.getType();
        boolean fansBecomeShopManager = BizTypeEnum.SHOP_MANAGER_PAY.getCode() == upayWxOrder.getBizType();
        if (nullPayType && rebate && fansBecomeShopManager) {
            //获取用户信息
            UpayWxOrder needUpayOrder = upayWxOrderService.selectByKeyId(upayWxOrder.getBizValue());
            if (needUpayOrder == null) {
                return null;
            }
            User user = userService.selectByPrimaryKey(needUpayOrder.getUserId());
            if (user == null) {
                return null;
            }
            Map<String, Object> map = new HashMap<>();
            String title = "升级成为店长";
            title = "粉丝" + "\"" + user.getNickName() + "\"" + title;
            map.put("title", title);
            map.put("type", UserMoneyIndexNameEnum.TASK_COMMISSION.getCode());
            map.put("indexName", UserMoneyIndexNameEnum.TASK_COMMISSION.getMessage());
            String plus_minus = "+";
            map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());//直接删除多余的小数位，如2.357会变成2.35
            map.put("time", "");
            if (upayWxOrder.getPaymentTime() != null) {
                map.put("time", DateFormatUtils.format(upayWxOrder.getPaymentTime(), "yyyy.MM.dd HH:mm"));
            }
            return map;
        }
        return null;
    }
}
