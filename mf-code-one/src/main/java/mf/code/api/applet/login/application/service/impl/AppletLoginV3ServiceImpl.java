package mf.code.api.applet.login.application.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.login.application.service.AppletLoginV3Service;
import mf.code.api.applet.login.domain.aggregateroot.ActivityAggregateRoot;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.api.applet.login.domain.aggregateroot.HandlingEventAggregateRoot;
import mf.code.api.applet.login.domain.aggregateroot.InvitationEvent;
import mf.code.api.applet.login.domain.repository.ActivityRepository;
import mf.code.api.applet.login.domain.repository.AppletUserRepository;
import mf.code.api.applet.login.domain.repository.HandlingEventRepository;
import mf.code.api.applet.login.domain.valueobject.InviteSpecification;
import mf.code.api.seller.activityDef.domain.aggregateroot.ShopAggregateRoot;
import mf.code.api.seller.activityDef.domain.repository.ShopRepository;
import mf.code.api.seller.service.FansV3Service;
import mf.code.api.seller.service.RecallV3Service;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.dto.WxUserSession;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.verify.AppletIdentityVerifyService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.login.application.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-21 下午6:08
 */
@Slf4j
@Service
public class AppletLoginV3ServiceImpl implements AppletLoginV3Service {
    private final AppletIdentityVerifyService identityVerifyService;
    private final AppletUserRepository appletUserRepository;
    private final ActivityRepository activityRepository;
    private final ShopRepository shopRepository;
    private final HandlingEventRepository handlingEventRepository;
    private final RecallV3Service recallV3Service;
    private final WxpayProperty wxpayProperty;
    private final UserService userService;
    private final FansV3Service fansV3Service;

    @Autowired
    public AppletLoginV3ServiceImpl(AppletIdentityVerifyService identityVerifyService,
                                    AppletUserRepository appletUserRepository,
                                    ActivityRepository activityRepository,
                                    ShopRepository shopRepository,
                                    HandlingEventRepository handlingEventRepository,
                                    RecallV3Service recallV3Service,
                                    WxpayProperty wxpayProperty,
                                    UserService userService,
                                    FansV3Service fansV3Service) {
        this.identityVerifyService = identityVerifyService;
        this.appletUserRepository = appletUserRepository;
        this.activityRepository = activityRepository;
        this.shopRepository = shopRepository;
        this.handlingEventRepository = handlingEventRepository;
        this.recallV3Service = recallV3Service;
        this.wxpayProperty = wxpayProperty;
        this.userService = userService;
        this.fansV3Service = fansV3Service;
    }

    @Override
    public SimpleResponse login(WxUserSession wxUserSession) {
        wxUserSession.setAppid(wxpayProperty.getMfAppId());
        wxUserSession.setAppSecret(wxpayProperty.getMfAppSecret());

        wxUserSession = identityVerifyService.verify(wxUserSession);
        if (wxUserSession.getErrcode() != ApiStatusEnum.SUCCESS.getCode()) {
            log.info("登录校验异常");
            return new SimpleResponse(wxUserSession.getErrcode(), wxUserSession.getErrmsg());
        }

        AppletUserAggregateRoot appletUser = appletUserRepository.updateOrSaveByWxUserSession(wxUserSession);
        if (appletUser == null) {
            log.info("用户未授权");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "用户未授权");
        }

        ShopAggregateRoot shop = shopRepository.findByShopIdWithoutActivityDefHistory(appletUser.getShopId());
        if (shop == null) {
            log.info("无店铺信息");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "店铺信息无效");
        }

        // 财富大闯关 创建 平台邀请关系
        // checkpointsPlatformInvition(appletUser.getUserId());


        invitation(wxUserSession.getPub_uid(), wxUserSession.getAid(), appletUser.getUserId().toString(), wxUserSession.getPubType());

        try {
            recallV3Service.incrScanTime(wxUserSession.getRecallScene());
            //recallScene: 8,52,11,9146,uid36,pid19
            log.info("粉丝召回推送：" + wxUserSession.getRecallScene());
            fansV3Service.asyncIncrClickPush(wxUserSession.getRecallScene());
        } catch (Exception e) {
            log.error("粉丝召回计数失败：" + JSON.toJSONString(wxUserSession));
        }

        // 加工用户登录数据
        Map<String, Object> loginVO = new HashMap<>();
        loginVO.putAll(appletUser.getLoginInfoWithScene(wxUserSession.getScene()));
        loginVO.putAll(shop.getAppletHomePage());
        loginVO.putAll(appletUser.becomeShopFans(shop));
        return new SimpleResponse(ApiStatusEnum.SUCCESS, loginVO);
    }

    private SimpleResponse checkpointsPlatformInvition(Long userId) {
        Integer pubType = UserPubJoinTypeEnum.CHECKPOINTS.getCode();
        InviteSpecification invitationSpecification = InviteSpecification.selectInvitationSpecification(null, pubType.toString());
        AppletUserAggregateRoot appletSubUser = appletUserRepository.findInvitedHistoryByUserId(userId.toString(), invitationSpecification);
        AppletUserAggregateRoot platform = appletUserRepository.findPlatform();
        InvitationEvent invitationEvent = HandlingEventAggregateRoot.newInvitationEvent(platform, appletSubUser, invitationSpecification);
        if (invitationEvent == null) {
            log.info("邀请失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "邀请失败");
        }
        Boolean saveOrUpdate = handlingEventRepository.saveOrUpdate(invitationEvent);
        if (!saveOrUpdate) {
            log.error("邀请记录存储或更新失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "邀请记录存储或更新失败");
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

    @Override
    public SimpleResponse invitation(String pubUserId, String activityId, String userId, String pubType) {
        ActivityAggregateRoot activity = activityRepository.findByActivityId(activityId);
        InviteSpecification invitationSpecification = InviteSpecification.selectInvitationSpecification(activity, pubType);
        AppletUserAggregateRoot appletSubUser = appletUserRepository.findInvitedHistoryByUserId(userId, invitationSpecification);
        AppletUserAggregateRoot appletPubUser = appletUserRepository.findInviteHistoryByUserId(pubUserId, invitationSpecification);

        InvitationEvent invitationEvent = HandlingEventAggregateRoot.newInvitationEvent(appletPubUser, appletSubUser, invitationSpecification);
        if (invitationEvent == null) {
            log.info("邀请失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "邀请失败");
        }
        Boolean saveOrUpdate = handlingEventRepository.saveOrUpdate(invitationEvent);
        if (!saveOrUpdate) {
            log.error("邀请记录存储或更新失败");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "邀请记录存储或更新失败");
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS);
    }

}
