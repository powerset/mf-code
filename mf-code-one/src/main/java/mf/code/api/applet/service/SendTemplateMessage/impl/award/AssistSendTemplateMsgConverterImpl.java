package mf.code.api.applet.service.SendTemplateMessage.impl.award;

import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.api.applet.service.SendTemplateMessage.SendTemplateMsgService;
import mf.code.common.caller.wxmp.WeixinMpService;
import mf.code.common.caller.wxmp.WxmpProperty;
import mf.code.common.caller.wxmp.vo.WxSendMsgVo;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * mf.code.api.applet.service.SendTemplateMessage.impl.award
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 10:53
 */
@Service
@Slf4j
public class AssistSendTemplateMsgConverterImpl extends SendTemplateMsgService {
    @Autowired
    private UserService userService;
    @Autowired
    private WxmpProperty wxmpProperty;
    @Autowired
    private WeixinMpService weixinMpService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private WxpayProperty wxpayProperty;

    @Override
    public String awardSendTemplateMessageConverter(MerchantShop merchantShop,
                                                    Activity activity,
                                                    Long nextAwardUserID) {
        //助力拼团推送消息
        if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
            Goods goods = this.goodsService.selectById(activity.getGoodsId());
            if (goods == null) {
                log.warn("活动编号为：{}，推送消息时，对应的商品不存在", activity.getId());
                return null;
            }
            this.sendAssistMessage(activity, goods);
            return ApiStatusEnum.SUCCESS.getMessage();
        }
        return null;
    }

    /***
     * 助力拼团推送
     * (活动时间内完成拼团要求，可领取商品)拼团成功通知：拼团成功，《》新人有礼活动，请及时完成奖品领取，未及时领取的奖品将失效！
     * (活动时间内未完成拼团要求，领取失败)拼团失败通知：拼团失败，《》新人有礼活动，未在规定时间内达到满团条件，活动失败，可点击重新发起活动！
     * (成团后进行第二次提醒。提醒时间：距离结束时间剩余2小时。
     * 活动时间根据商户后台设置，最小一天)拼团成功通知：请尽快完成领奖！，《》新人有礼活动，你已达到满团条件，请尽快完成领奖，2小时后奖品将自动失效，无法领取！
     * @param activity
     */
    private void sendAssistMessage(Activity activity, Goods goods) {
        String formIDStr = this.queryRedisUserFormIds(activity.getUserId());
        if (StringUtils.isNotBlank(formIDStr)) {
            String templateID = wxmpProperty.getAssembleSuccessMsgTmpId();
            Object[] objects = new Object[]{"拼团成功", "《" + goods.getDisplayGoodsName() + "》新人有礼活动", "请及时完成奖品领取,未及时领取的奖品将失效"};
            if (activity.getStatus() == ActivityStatusEnum.FAILURE.getCode()) {
                templateID = wxmpProperty.getAssembleFailMsgTmpId();
                objects = new Object[]{"拼团失败", "《" + goods.getDisplayGoodsName() + "》新人有礼活动", "未在规定时间内达到满团条件，活动失败,可点击重新发起活动"};
            }

            int keywordNum = objects.length;
            User user = this.userService.selectByPrimaryKey(activity.getUserId());
            WxSendMsgVo voNewMan = new WxSendMsgVo();
            voNewMan.from(templateID, user.getOpenId(), formIDStr, this.getPageUrl(activity, null, null),
                    this.getTempteDate(keywordNum, objects), EMPHASISKEYWORD1);
            Map<String, Object> stringObjectMapNewMan = this.weixinMpService.sendTemplateMessage(wxpayProperty.getMfAppId(), wxpayProperty.getMfAppSecret(), voNewMan);
            log.info("抽奖活动推送消息结果--助力拼团：{}, parms:{}", stringObjectMapNewMan, voNewMan);
            this.respMap(stringObjectMapNewMan, voNewMan, user.getId());
        } else {
            log.warn("超级警戒，助力拼团未得到推送消息");
        }
    }
}
