package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.activity.domain.applet.aggregateroot.AppletActivity;
import mf.code.activity.domain.applet.aggregateroot.AppletActivityDef;
import mf.code.activity.domain.applet.repository.AppletActivityDefRepository;
import mf.code.activity.domain.applet.repository.AppletActivityRepository;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityAboutService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.AssistService;
import mf.code.common.constant.ActivityDefStockTypeEnum;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.UserTaskStatusEnum;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.FakeDataUtil;
import mf.code.common.utils.RobotUserUtil;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.service.impl
 *
 * @description: 助力活动相关逻辑
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月24日 17:26
 */
@Slf4j
@Service
public class AssistServiceImpl implements AssistService {
    @Autowired
    private AppletActivityRepository appletActivityRepository;
    @Autowired
    private AppletActivityDefRepository appletActivityDefRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityAboutService activityAboutService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /***
     * 助力活动详情
     * @param activityId
     * @param userId
     * @param sceneId 0不是分享进入 1：分享进入 2：查看历史进入==传啥查啥
     * @return
     */
    @Override
    public SimpleResponse queryAssistActivity(Long activityId, Long userId, Integer sceneId) {
        Map<String, Object> respMap = new HashMap<>();
        SimpleResponse d = new SimpleResponse();
        if (activityId == null || userId == null || userId == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        if (activityId == 0) {
            return FakeDataUtil.activityData();
        }
        Activity activity = this.activityService.findById(activityId);
        //查询目的：获取 活动开始时间，活动结束时间
        if (activity == null || activity.getDel() == 1) {//等于1时 说明该活动已删除
            //活动不存在异常处理
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "活动已经不存在");
        }
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
        if (activityDef == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "活动定义已经不存在");
        }
        //进入的活动创建人信息==分享人
        User user = this.userService.selectByPrimaryKey(activity.getUserId());
        respMap.put("sharePersonInfo", BeanMapUtil.beanToMapIgnore(user, "ctime", "grantTime", "country", "grantStatus", "unionId", "gender", "sessionKey", "city", "utime", "openId", "mobile", "scene", "province", "wxopenid"));
        //该活动是否开团了--true开团了 false未开团
        boolean initActivityAward = this.activityAboutService.checkAwardOrAwarded(activity, false);
        //该用户是否助力过==有无助力资格(false:有助力资格 true:没有助力资格)
        boolean assisted = this.checkAssist(userId);

        if (sceneId == 1) {//该进入为分享进入
            activity = this.nextActivity(activity, userId, initActivityAward, assisted);
        } else if (sceneId == 0) {
            //去查询自己的活动-最新的
            activity = this.activityAboutService.getMySelfActivity(activity, userId);
        }

        Goods goods = this.goodsService.selectById(activity.getGoodsId());
        if (goods == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该商品不存在");
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(activity.getShopId());
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "根据活动表的shopid查询merchantshop,未查询到结果");
        }

        //获取微信号
        Map<String, Object> customServiceMap = this.activityAboutService.getServiceWx(activity);
        //查询活动参与人数
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(activity.getId());

        //客服
        respMap.put("customServiceInfo", customServiceMap);
        //商品
        respMap.put("goods", BeanMapUtil.beanToMapIgnore(goods, "ctime", "utime", "del", "xxbtopReq", "xxbtopRes", "xxbtopDesc", "xxbtopNumIid", "srcType", "xxbtopPicUrl", "descPath", "displayBanner", "xxbtopTitle", "categoryId", "xxbtopPrice"));
        //活动详细
        respMap.put("activityInfo", BeanMapUtil.beanToMapIgnore(activity, "ctime", "utime", "del", "merchantCouponJson", "deposit", "keyWordsJson", "orderId", "depositDef", "version", "activityItemId", "payPrice", "missionNeedTime", "hitsPerDraw", "batchNum"));
        //成功领取数目
        respMap.put("successNum", this.getActivityNumFromGoods(activity));
        //剩余库存数
        respMap.put("stock", activityDef.getStock());
        //助力活动进度状态
        respMap.put("processStatus", this.getProcessStatus(activity, userId, activityPersonList.size()));
        //助力活动状态
        respMap.put("status", this.getStatus(activity, activityId, userId, activityPersonList, assisted));
        //参与人
        respMap.put("activityJoinPerson", this.getActivityJoinList(activity, activityPersonList));

        // 是否是新库存逻辑
        respMap.put("showStock", ActivityDefStockTypeEnum.COMPLETE_REDUCE.getCode().equals(activityDef.getStockType()));
        respMap.put("dialogInfo", new HashMap<>());
        //弹窗对象信息
        if (activity.getUserId() == userId.longValue()) {
            respMap.put("dialogInfo", this.getDialogInfo(activity, activityPersonList, activity.getUserId()));
        }

        UserTask userTask = userTaskService.findByUserIdActivityId(activity.getId(), userId);
        respMap.put("taskId", userTask != null ? userTask.getId() : 0);

        d.setData(respMap);
        return d;
    }

    /***
     * 查询助力类活动参与人数
     * @param activityId
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse queryActivityJoin(Long activityId, Long userId) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        SimpleResponse d = new SimpleResponse();
        if (activityId == null || activityId == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        Activity activity = this.activityService.findById(activityId);
        if (activity == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该活动不存在");
            return d;
        }
        //查询活动参与人数
        List<String> activityPersonList = this.redisService.queryRedisJoinUser(activity.getId());

        respMap.put("activityJoinPerson", this.getActivityJoinList(activity, activityPersonList));
        d.setData(respMap);

        return d;
    }

    /***
     * 获取弹幕信息
     * @param lastId 上次的最大一个编号
     * @param limit 多少条
     * @return
     */
    @Override
    public SimpleResponse getBarrage(Long merchantId,
                                     Long shopId,
                                     Long lastId,
                                     int limit) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (merchantId == null || merchantId == 0 || shopId == null || shopId == 0) {
            simpleResponse.setData(new ArrayList<>());
            return simpleResponse;
        }
        Map<String, Object> activityParams = new HashMap<>();
        activityParams.put("merchantId", merchantId);
        activityParams.put("shopId", shopId);
        activityParams.put("types", Arrays.asList(ActivityTypeEnum.ASSIST.getCode(), ActivityTypeEnum.ASSIST_V2.getCode()));
        activityParams.put("order", "ctime");
        activityParams.put("direct", "desc");
        activityParams.put("size", limit);
        activityParams.put("offset", 0);
        List<Activity> activities = this.activityService.findPage(activityParams);
        if (CollectionUtils.isEmpty(activities)) {
            //假数据
            Map<String, Object> resp = new HashMap<>();
            resp.put("barrageInfo", this.getRobotAssistUser(limit));
            simpleResponse.setData(resp);
            return simpleResponse;
        }
        //存储上一次的编号数据
        List<Long> activityDefIds = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        List<Long> activityIds = new ArrayList<>();
        for (Activity activity : activities) {
            if (activityDefIds.indexOf(activity.getActivityDefId()) == -1) {
                activityDefIds.add(activity.getActivityDefId());
            }
            if (userIds.indexOf(activity.getUserId()) == -1) {
                userIds.add(activity.getUserId());
            }
            if (activityIds.indexOf(activity.getId()) == -1) {
                activityIds.add(activity.getId());
            }
        }
        Map<Long, User> userMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(userIds)) {
            userMap = this.getUserMap(userIds);
        }
        Map<Long, UserTask> userTaskMap = this.getUserTaskMap(merchantId, shopId, activityIds);

        List<Object> list = new ArrayList<>();
        for (Activity activity : activities) {
            Map<String, Object> map = new HashMap<>();
            User user = userMap.get(activity.getUserId());
            if (user == null) {
                continue;
            }
            UserTask userTask = userTaskMap.get(activity.getId());
            map.put("isSuccess", false);
            if (userTask != null && userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                map.put("isSuccess", true);
            }
            map.put("avatarUrl", user.getAvatarUrl());
            map.put("nickName", user.getNickName());
            list.add(map);
        }

        Map<String, Object> resp = new HashMap<>();
        resp.put("barrageInfo", list);
        simpleResponse.setData(resp);

        return simpleResponse;
    }

    /**
     * 查询活动剩余库存信息
     *
     * @param activityId
     * @return
     */
    @Override
    public SimpleResponse queryAssistStockInfo(Long activityId) {
        AppletActivity appletActivity = appletActivityRepository.findById(activityId);
        if (appletActivity == null) {
            log.error("查无活动， activityId = {}", activityId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "查无活动");
        }
        Long activityDefId = appletActivity.getActivityDefId();
        AppletActivityDef appletActivityDef = appletActivityDefRepository.findUserBoardById(activityDefId);
        if (appletActivityDef == null) {
            log.error("查无此活动定义，activityDefId = {}", activityDefId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "查无此活动定义");
        }

        // 剩余奖品数
        Integer stock = appletActivityDef.getActivityDefInfo().getStock();
        // 总奖品数
        Integer stockDef = appletActivityDef.getActivityDefInfo().getStockDef();

        Map<String, Object> resultVO = new HashMap<>();
        resultVO.put("stock", stock);
        resultVO.put("stockDef", stockDef);
        resultVO.put("rate", (stock * 100) / stockDef);
        resultVO.put("completeNum", appletActivityDef.queryAlreadyCompleteNum());
        resultVO.put("almostNum", appletActivityDef.queryAlmostCompleteNum());
        resultVO.put("almostInfo", appletActivityDef.queryAlmostCompleteUserInfo());
        resultVO.put("prusueNum", appletActivityDef.queryPrusueNum());
        return new SimpleResponse(ApiStatusEnum.SUCCESS, resultVO);
    }

    /***
     * 获取usertask信息
     * @param merchantID
     * @param shopID
     * @param activityIds
     * @return
     */
    private Map<Long, UserTask> getUserTaskMap(Long merchantID, Long shopID, List<Long> activityIds) {
        QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
        userTaskWrapper.lambda().eq(UserTask::getMerchantId, merchantID).eq(UserTask::getShopId, shopID)
                .in(UserTask::getActivityId, activityIds)
        ;
        List<UserTask> userTasks = this.userTaskService.list(userTaskWrapper);
        if (CollectionUtils.isEmpty(userTasks)) {
            return new HashMap<Long, UserTask>();
        }
        Map<Long, UserTask> userTaskMap = new HashMap<>();
        for (UserTask userTask : userTasks) {
            userTaskMap.put(userTask.getActivityId(), userTask);
        }
        return userTaskMap;
    }

    /***
     * 获取假数据
     * @param limit
     * @return
     */
    private List<Object> getRobotAssistUser(int limit) {
        List<Object> list = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            Map<String, Object> map = RobotUserUtil.getAssistUserAvatarUrlAndNickFromOss();
            map.put("isSuccess", false);
            if (i % 3 == 0) {
                map.put("isSuccess", true);
            }
            list.add(map);
        }
        return list;
    }

    /***
     * 获取用户信息
     * @param userIds
     * @return
     */
    private Map<Long, User> getUserMap(List<Long> userIds) {
        Map<String, Object> userParams = new HashMap<>();
        userParams.put("userIds", userIds);
        Map<Long, User> userMap = new HashMap<Long, User>();
        if (CollectionUtils.isEmpty(userIds)) {
            return userMap;
        }
        List<User> users = this.userService.query(userParams);
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                userMap.put(user.getId(), user);
            }
        }
        return userMap;
    }

    /***
     * 助力活动的展现形式数据
     * @param activity
     * @param map
     * @return
     */
    @Override
    public Map<String, Object> addAssistMap(Activity activity, Map<String, Object> map) {
        if (activity.getType() == ActivityTypeEnum.ASSIST.getCode() || activity.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
            if (map == null || map.size() == 0) {
                map = new HashMap<>();
            }
            if (activity.getUserId() == null || activity.getUserId() <= 0) {
                return null;
            }
            User user = this.userService.selectByPrimaryKey(activity.getUserId());
            if (user == null) {
                return null;
            }
            map.put("avatarUrl", user.getAvatarUrl());
            map.put("nickName", user.getNickName());
            return map;
        }
        return null;
    }

    /***
     * 助力活动弹窗信息
     * @param activity
     * @param activityPersonList
     * @param userID
     * @return
     */
    private Map<String, Object> getDialogInfo(Activity activity, List<String> activityPersonList, Long userID) {
        Map<String, Object> map = new HashMap<>();
        map.put("addInviteNum", 0);
        map.put("inviteSum", activityPersonList.size());
        int lastInviteNum = this.redisService.queryRedisJoinInviteLastNum(activity.getId(), userID);
        if (activityPersonList.size() > lastInviteNum) {
            map.put("addInviteNum", activityPersonList.size() - lastInviteNum);
            this.redisService.updateRedisJoinInviteLastNum(activity.getId(), userID, activityPersonList.size());
        }
        map.put("awardDialog", false);
        String redisValue = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.AWARDDIALOG + activity.getUserId() + activity.getType());
        if (StringUtils.isNotBlank(redisValue) && "0".equals(redisValue)) {
            map.put("awardDialog", true);
            this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.AWARDDIALOG + activity.getUserId() + activity.getType(), "1", 1, TimeUnit.DAYS);
            if (activityPersonList.size() > lastInviteNum) {
                this.redisService.updateRedisJoinInviteLastNum(activity.getId(), userID, activityPersonList.size());
            }
        }
        String couponValue = this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.ACTIVITY_COUPONER + activity.getId());
        if (StringUtils.isNotBlank(couponValue)) {
            JSONObject jsonObject = JSON.parseObject(couponValue);
            map.put("couponDialog", jsonObject);
        }
        return map;
    }

    /***
     * 获取助力活动参与人数
     * @param activity
     * @param activityPersonList
     * @return
     */
    private List<Object> getActivityJoinList(Activity activity, List<String> activityPersonList) {
        List<Object> list = new ArrayList<>();
        if (this.addAssistMap(activity, new HashMap<>()) != null) {
            list.add(this.addAssistMap(activity, new HashMap<>()));
        }
        //处理参与人信息
        if (activityPersonList != null && activityPersonList.size() > 0) {
            for (int i = 0; i < activityPersonList.size(); i++) {
                JSONObject jsonObject = JSON.parseObject(activityPersonList.get(i).toString());
                Map<String, Object> map = new HashMap<>();
                if (jsonObject.getLong("uid").longValue() != activity.getUserId().longValue()) {
                    map.put("avatarUrl", jsonObject.getString("avatarUrl"));
                    map.put("nickName", jsonObject.getString("nickName"));
                    list.add(map);
                }
            }
        }
        return list;
    }

    /***
     * 获取助力活动的进度状态
     * @param activity
     * @return
     */
    private Integer getProcessStatus(Activity activity, Long userId, int joinSize) {
        int status = 0;//发起活动状态
        if (joinSize > 0) {
            status = 1;//邀请好友状态
        }
        if (activity.getAwardStartTime() != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("merchantId", activity.getMerchantId());
            params.put("shopId", activity.getShopId());
            params.put("activityId", activity.getId());
            params.put("userId", userId);
            List<UserTask> userTasks = this.userTaskService.query(params);
            if (CollectionUtils.isEmpty(userTasks)) {
                status = 2;//开团失败==拼团失败
                return status;
            }
            status = 3;//开团成功，去领奖
            UserTask userTask = userTasks.get(0);
            if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                status = 4;//领奖成功
            }
            if (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                status = 5;//领奖任务过期==领奖失败
            }
        }
        return status;
    }

    /***
     * 助力活动的状态
     * @param activity
     * @param userId
     * @param activityPersonList
     * @param assisted 用户助力状态 true:助力过，没资格助力，false:没助力过，有资格助力
     * @return -1未成团(重新发起按钮) 4邀请好友助力 5已为ta助力 6助力成功(立即领取) 7马上领取(创建过程) 8奖品领取失效(重新发起) 9领取成功
     */
    private Integer getStatus(Activity activity,
                              Long oldActivityId,
                              Long userId,
                              List<String> activityPersonList,
                              boolean assisted) {
        //-1 已结束 1待开始 2待开奖 3已开奖 另外不做小程序展现的状态：0:待发布 -2：强制下线
        int activityStatus = this.activityService.getActivityStatus(activity.getStatus(), activity.getStartTime(), activity.getAwardStartTime(), activity.getEndTime());
        if (activityStatus == -1) {
            activityStatus = -1;
        } else {
            //邀请好友助力
            activityStatus = 4;
        }

        //该用户是否创建过
        boolean thisUserCreate = activity.getId().longValue() != oldActivityId;

        //是否助力
        if (!activity.getUserId().equals(userId)) {
            for (int i = 0; i < activityPersonList.size(); i++) {
                JSONObject jsonObject = JSON.parseObject(activityPersonList.get(i).toString());
                if (jsonObject.getLong("uid").longValue() == userId.longValue()) {
                    //已为ta助力
                    activityStatus = 5;
                }
            }
            //马上领取，是否有助力，是否创建过-只要有一个false==马上领取
            if (!thisUserCreate || !assisted) {
                //马上领取
                activityStatus = 7;
            }
        }

        //若是自己的活动，则判断下是否开团中奖了
        if (activity.getUserId().equals(userId)) {
            Map<String, Object> userTaskParams = new HashMap<>();
            userTaskParams.put("merchantId", activity.getMerchantId());
            userTaskParams.put("shopId", activity.getShopId());
            userTaskParams.put("activityId", activity.getId());
            userTaskParams.put("types", Arrays.asList(UserTaskTypeEnum.ASSIST_START.getCode()
                    , UserTaskTypeEnum.ASSIST_START_V2.getCode()));
            List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
            if (!CollectionUtils.isEmpty(userTasks)) {
                UserTask userTask = userTasks.get(0);
                if (userTask.getStatus() == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                    //奖品领取失效
                    activityStatus = 8;
                } else if (userTask.getStatus() == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                    //领取成功
                    activityStatus = 9;
                } else {
                    //助力成功
                    activityStatus = 6;
                    if (StringUtils.isBlank(this.stringRedisTemplate.opsForValue().get(RedisKeyConstant.AWARDDIALOG + activity.getUserId() + activity.getType()))) {
                        this.stringRedisTemplate.opsForValue().set(RedisKeyConstant.AWARDDIALOG + activity.getUserId() + activity.getType(), "0", 1, TimeUnit.DAYS);
                    }
                }
                if (activityStatus != 6) {
                    //删除中奖弹窗
                    this.stringRedisTemplate.delete(RedisKeyConstant.AWARDDIALOG + activity.getUserId() + activity.getType());
                }
            }
        }
        return activityStatus;
    }

    /***
     * 当天是否助力过 true:助力过 false:未助力
     * @param userId
     * @return
     */
    private Boolean checkAssist(Long userId) {
        if (StringUtils.isNotBlank(stringRedisTemplate.opsForValue().get(RedisKeyConstant.ASSIST_ABILITY + userId))) {
            return true;
        }
        return false;
    }

    /***
     * 获取库存数
     * @param activityDefId
     * @return
     */
    private Integer getDefStock(Long activityDefId) {
        if (activityDefId == null || activityDefId <= 0) {
            return 0;
        }
        ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityDefId);
        if (activityDef == null) {
            return 0;
        }
        return activityDef.getStock();
    }

    /***
     * 分享进入判断
     * @param activity
     * @param userId
     * @param award 进入的分享卡片所对应的活动的开奖状态 true开团了 false未开团
     * @param assisted 用户助力状态 true:助力过，没资格助力，false:没助力过，有资格助力
     * @return
     */
    private Activity nextActivity(Activity activity, Long userId, boolean award, boolean assisted) {
        //查找该用户该商品有无该活动
        QueryWrapper<Activity> activityWrapper = new QueryWrapper<>();
        activityWrapper.lambda()
                .eq(Activity::getMerchantId, activity.getMerchantId())
                .eq(Activity::getShopId, activity.getShopId())
                .eq(Activity::getGoodsId, activity.getGoodsId())
                .eq(Activity::getUserId, userId)
                .in(Activity::getType, Arrays.asList(ActivityTypeEnum.ASSIST.getCode(), ActivityTypeEnum.ASSIST_V2.getCode()))
                .orderByDesc(Activity::getId)
        ;
        List<Activity> activities = this.activityService.list(activityWrapper);
        if (!CollectionUtils.isEmpty(activities)) {
            for (Activity activity1 : activities) {
                //该用户是否助力过--是否跳转自己活动，还是展现"马上领取"
                if (assisted || award) {
                    if (activity1.getStatus() == ActivityStatusEnum.PUBLISHED.getCode() || this.activityAboutService.checkAwardOrAwarded(activity1, true)) {
                        return activity1;
                    }
                }
            }
        }
        return activity;
    }

    /***
     * 获取助力已领取数目
     * @param activity
     * @return
     */
    private Integer getActivityNumFromGoods(Activity activity) {
        int initNum = 1089;//产品定的初始数目
        Map<String, Object> params = new HashMap<>();
        params.put("merchantId", activity.getMerchantId());
        params.put("shopId", activity.getShopId());
        params.put("goodsId", activity.getGoodsId());
        params.put("merchantId", activity.getMerchantId());
        params.put("types", Arrays.asList(ActivityTypeEnum.ASSIST.getCode(), ActivityTypeEnum.ASSIST_V2.getCode()));
        int sum = this.activityService.countActivity(params);

        return initNum + sum;
    }
}
