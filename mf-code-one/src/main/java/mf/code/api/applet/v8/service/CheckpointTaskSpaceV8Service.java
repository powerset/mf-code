package mf.code.api.applet.v8.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.v8.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 16:47
 */
public interface CheckpointTaskSpaceV8Service {
    /***
     * 任务空间列表改版----赚收益页面
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    SimpleResponse queryCheckpointTaskSpace(Long merchantId, Long shopId, Long userId);
}
