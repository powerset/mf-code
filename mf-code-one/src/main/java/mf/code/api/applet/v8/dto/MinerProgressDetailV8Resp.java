package mf.code.api.applet.v8.dto;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.api.AppletMybatisPageDto;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * mf.code.api.applet.v3.dto
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月14日 10:49
 */
@Data
public class MinerProgressDetailV8Resp {
    //今日累计
    private String todayAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //今日任务收益
    private String todayTaskProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //今日缴税收益
    private String todayScottareProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //历史任务收益
    private String totalTaskProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //历史缴税收益
    private String totalScottare = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //历史累计
    private String totalAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //矿工缴税详情
    private AppletMybatisPageDto<MinerProgressDetail> detail;

    //用户角色 0:普通用户 1：店长
    private int role;

    @Data
    public static class MinerProgressDetail {
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm", timezone = "GMT+8")
        private Date time;
        private String projectMessage;
        private String money = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        public void from(UserCoupon userCoupon, User minerUser, boolean shopManager) {
            this.time = userCoupon.getCtime();
            this.projectMessage = UserCouponTypeEnum.findByDesc(userCoupon.getType());
            if (shopManager) {
                if (minerUser != null) {
                    if (userCoupon.getType() == UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode()) {
                        return;
                    }
                    this.projectMessage = "矿工" + minerUser.getNickName() + "完成" + this.getProjectMessage() + "缴税";
                }
            }
            if (StringUtils.isNotBlank(userCoupon.getCommissionDef())) {
                Object object = JSONObject.parse(userCoupon.getCommissionDef());
                JSONObject jsonObject = JSONObject.parseObject(object.toString());
                Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                if (jsonMap != null) {
                    String commissionDef = jsonMap.get("commissionDef").toString();
                    String rate = jsonMap.get("rate").toString();
                    BigDecimal bigDecimal = new BigDecimal(commissionDef).multiply(new BigDecimal(rate)).divide(new BigDecimal("100"));
                    if (minerUser != null) {
                        this.money = bigDecimal.setScale(2, BigDecimal.ROUND_DOWN).toString();
                    } else {
                        this.money = userCoupon.getCommission().setScale(2, BigDecimal.ROUND_DOWN).toString();
                    }
                }
            } else {
                if (userCoupon.getType() == UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode()) {
                    this.money = userCoupon.getAmount().setScale(2, BigDecimal.ROUND_DOWN).toString();
                }
            }
        }
    }

    /***
     * 汇总历史累计相关数据
     * @param shopManager
     * @param userPubJoin
     * @param allMinerTotalScottare
     */
    public void from(BigDecimal newbieTask_open_red_pack, boolean shopManager, UserPubJoin userPubJoin, BigDecimal allMinerTotalScottare) {
        //获取累计任务收益
        this.totalTaskProfit = userPubJoin.getTotalCommission().add(newbieTask_open_red_pack).setScale(2, BigDecimal.ROUND_DOWN).toString();
        //获取累计缴税
        this.totalScottare = allMinerTotalScottare.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //历史累计
        this.totalAmount = userPubJoin.getTotalCommission().add(newbieTask_open_red_pack).setScale(2, BigDecimal.ROUND_DOWN).toString();
        if (shopManager) {
            this.totalAmount = userPubJoin.getTotalCommission().add(newbieTask_open_red_pack).add(allMinerTotalScottare).setScale(2, BigDecimal.ROUND_DOWN).toString();
        }
    }

    /***
     * 获取今日累计
     * @param shopManager
     * @param todayCheckPoint
     */
    public void fromTodayProfit(boolean shopManager, String todayCheckPoint) {
        if (StringUtils.isNotBlank(todayCheckPoint)) {
            JSONObject jsonObject = JSONObject.parseObject(todayCheckPoint);
            Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
            if (jsonMap != null) {
                //今日任务收益
                if (jsonMap.get("todayTaskProfit") != null) {
                    this.todayTaskProfit = new BigDecimal(jsonMap.get("todayTaskProfit").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString();
                }
                //今日缴税收益
                if (jsonMap.get("todayScottareProfit") != null) {
                    this.todayScottareProfit = new BigDecimal(jsonMap.get("todayScottareProfit").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString();
                }
                //今日累计
                this.todayAmount = new BigDecimal(this.todayTaskProfit).toString();
                if (shopManager) {
                    this.todayAmount = new BigDecimal(this.todayTaskProfit).add(new BigDecimal(this.todayScottareProfit)).toString();
                }
            }
        }
    }
}
