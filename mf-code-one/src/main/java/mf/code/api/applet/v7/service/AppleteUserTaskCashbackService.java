package mf.code.api.applet.v7.service;

import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.upay.repo.enums.BizTypeEnum;

/**
 * mf.code.api.applet.v7.service
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年05月28日 17:52
 */
public interface AppleteUserTaskCashbackService {

    /***
     * 人工版-审核返现
     * @param userTaskOrg
     * @return
     */
    SimpleResponse userTaskCashback(UserTask userTaskOrg, BizTypeEnum bizTypeEnum, UserCouponTypeEnum userCouponTypeEnum, Integer type);
}
