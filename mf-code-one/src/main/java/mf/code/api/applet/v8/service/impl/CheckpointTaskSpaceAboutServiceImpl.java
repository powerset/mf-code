package mf.code.api.applet.v8.service.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v8.dto.CheckpointTaskSpaceDTO;
import mf.code.api.applet.v8.dto.NewbieTask;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceAboutService;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.common.RedisKeyConstant;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.CheckpointTaskSpaceTypeEnum;
import mf.code.one.vo.UserCouponAboutVO;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UpayWxOrderStatusEnum;
import mf.code.user.repo.po.User;
import mf.code.user.vo.UpayWxOrderAboutVO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 18:14
 */
@Service
@Slf4j
public class CheckpointTaskSpaceAboutServiceImpl implements CheckpointTaskSpaceAboutService {
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DistributionAppService distributionAppService;

    /***
     * 新手任务详细
     * @param tasks
     * @param user
     */
    @Override
    public void queryNewbieTaskDetail(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, List<CheckpointTaskSpaceDTO.TaskDTO> tasks, User user) {
        Long userId = user.getId();
        Long shopId = user.getVipShopId();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            log.error("<<<<<<<<获取店铺信息异常： shopId:{}", shopId);
        }
        Long merchantId = merchantShop.getMerchantId();
        List<ActivityDef> activityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId, ActivityDefTypeEnum.NEWBIE_TASK.getCode());
        if (CollectionUtils.isEmpty(activityDefs)) {
            return;
        }
        List<Long> activityDefIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            if (!activityDef.getId().equals(activityDef.getParentId()) && activityDefIds.indexOf(activityDef.getId()) == -1) {
                activityDefIds.add(activityDef.getId());
            }
        }
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = new CheckpointTaskSpaceDTO.TaskDTO();
        taskDTO.setType(CheckpointTaskSpaceTypeEnum.NEWBIE_TASK.getCode());
        taskDTO.setTaskDetails(new ArrayList<>());
        taskDTO.setMerchantId(merchantId);
        taskDTO.setShopId(shopId);
        Map<Long, List<UserCoupon>> userCouponsMap = queryActivityDefInfo(merchantId, shopId, userId, activityDefIds,
                UserCouponAboutVO.addNewbieTaskType());

        //该用户是否做过
        Map<Long, List<UserCoupon>> existUserCouponsMap = queryActivityDefInfo(merchantId, shopId, userId, null,
                UserCouponAboutVO.addNewbieTaskType());
        boolean isDone = false;
        if(!CollectionUtils.isEmpty(existUserCouponsMap)){
            for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                List<UserCoupon> coupons = entry.getValue();
                UserCoupon userCoupon = coupons.get(0);
                if(userCoupon.getType() != UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode()){
                    isDone = true;
                }
            }
        }

        for (ActivityDef activityDef : activityDefs) {
            //父级定义活动基本数据
            if (activityDef.getId().equals(activityDef.getParentId())) {
                //判断是否有库存
                if (activityDef.getStock() <= 0 && !isDone) {
                    return;
                }
            }
        }
        for (ActivityDef activityDef : activityDefs) {
            //父级定义活动基本数据
            if (activityDef.getId().equals(activityDef.getParentId())) {
                taskDTO.setRemainStock(activityDef.getStock());
                taskDTO.setStock(activityDef.getStockDef());
            }

            //子级所有各活动情况
            if (!activityDef.getId().equals(activityDef.getParentId())) {
                //扣除税率的佣金
                CommissionBO commissionBO = commissionService.commissionCalculate(userId, activityDef.getCommission());
                NewbieTask newbieTask = new NewbieTask();
                newbieTask.setType(activityDef.getType());
                for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                    List<UserCoupon> coupons = entry.getValue();
                    isDoneUserCoupon(newbieTask, coupons, 1, null, activityDef.getType());
                }
                List<UserCoupon> userCoupons = userCouponsMap.get(activityDef.getId());
                if (!CollectionUtils.isEmpty(userCoupons)) {
                    isDoneUserCoupon(newbieTask, userCoupons, 1, null, activityDef.getType());
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode()) {
                    addRemainTask(checkpointTaskSpaceDTO, 1);
                    addEstimateProfit(checkpointTaskSpaceDTO, commissionBO.getCommission());
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode()) {
                    addRemainTask(checkpointTaskSpaceDTO, 1);
                    addEstimateProfit(checkpointTaskSpaceDTO, commissionBO.getCommission());
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                    addRemainTask(checkpointTaskSpaceDTO, 1);
                    addEstimateProfit(checkpointTaskSpaceDTO, activityDef.getCommission());
                }
                newbieTask.setActivityDefId(activityDef.getId());
                newbieTask.setRebate(commissionBO.getCommission().toString());
                if (activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                    newbieTask.setRebate(activityDef.getCommission().toString());
                }
                taskDTO.getTaskDetails().add(newbieTask);
            }
        }
        tasks.add(taskDTO);
    }

    /***
     * 店长任务详细
     * @param checkpointTaskSpaceDTO
     * @param tasks
     * @param user
     */
    @Override
    public void queryShopManagerTaskDetail(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, List<CheckpointTaskSpaceDTO.TaskDTO> tasks, User user) {
        Long shopId = user.getVipShopId();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (merchantShop == null) {
            log.error("<<<<<<<<获取店铺信息异常： shopId:{}", shopId);
        }
        Long merchantId = merchantShop.getMerchantId();
        List<ActivityDef> activityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId, ActivityDefTypeEnum.SHOP_MANAGER_TASK.getCode());
        if (CollectionUtils.isEmpty(activityDefs)) {
            return;
        }

        List<Long> activityDefIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            if (!activityDef.getId().equals(activityDef.getParentId()) && activityDefIds.indexOf(activityDef.getId()) == -1) {
                activityDefIds.add(activityDef.getId());
            }
        }
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = new CheckpointTaskSpaceDTO.TaskDTO();
        taskDTO.setType(CheckpointTaskSpaceTypeEnum.SHOP_MANAGER_TASK.getCode());
        taskDTO.setTaskDetails(new ArrayList<NewbieTask>());
        taskDTO.setMerchantId(merchantId);
        taskDTO.setShopId(shopId);
        //是否开启了店长角色, 展示可做任务
        boolean openShopManager = user.getRole() == 1 ? true : false;

        //该用户是否做过
        Map<Long, List<UserCoupon>> existUserCouponsMap = queryActivityDefInfo(merchantId, shopId, user.getId(), null,
                UserCouponAboutVO.addShopManagerTaskType());

        Map<Long, List<UserCoupon>> userCouponsMap = queryActivityDefInfo(merchantId, shopId, user.getId(), activityDefIds,
                UserCouponAboutVO.addShopManagerTaskType());
        for (ActivityDef activityDef : activityDefs) {
            //父级定义活动基本数据
            if (activityDef.getId().equals(activityDef.getParentId())) {
                taskDTO.setRemainStock(activityDef.getStock());
                taskDTO.setStock(activityDef.getStockDef());
            }
            //子级所有各活动情况
            if (!activityDef.getId().equals(activityDef.getParentId())) {
                //扣除税率的佣金
                CommissionBO commissionBO = commissionService.commissionCalculate(user.getId(), activityDef.getCommission());
                NewbieTask newbieTask = new NewbieTask();
                newbieTask.setType(activityDef.getType());

                for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                    List<UserCoupon> coupons = entry.getValue();
                    if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                        isDoneUserCoupon(newbieTask, coupons, 2, activityDef.getStartNum(), activityDef.getType());
                    } else {
                        isDoneUserCoupon(newbieTask, coupons, 2, null, activityDef.getType());
                    }
                }
                List<UserCoupon> userCoupons = userCouponsMap.get(activityDef.getId());
                if (!CollectionUtils.isEmpty(userCoupons)) {
                    if (activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                        isDoneUserCoupon(newbieTask, userCoupons, 2, activityDef.getStartNum(), activityDef.getType());
                    } else {
                        isDoneUserCoupon(newbieTask, userCoupons, 2, null, activityDef.getType());
                    }
                }

                BigDecimal amount = commissionBO.getCommission();
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode()
                        && openShopManager) {
                    addRemainTask(checkpointTaskSpaceDTO, 1);
                    addEstimateProfit(checkpointTaskSpaceDTO, commissionBO.getCommission());
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode()
                        && openShopManager) {
                    addRemainTask(checkpointTaskSpaceDTO, 1);
                    addEstimateProfit(checkpointTaskSpaceDTO, commissionBO.getCommission());
                }
                if (!newbieTask.isFinish() && activityDef.getType() == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()
                        && openShopManager) {
                    addRemainTask(checkpointTaskSpaceDTO, 1);
                    String amountByShopManagerPay = distributionAppService.getAmountByShopManagerPay();
                    log.info("<<<<<<<< 获取店长付费收益： resp:{}", amountByShopManagerPay);
                    BigDecimal amountByShopManager = BigDecimal.ZERO;
                    if(StringUtils.isNotBlank(amountByShopManagerPay)){
                        amountByShopManager = amountByShopManager.add(new BigDecimal(amountByShopManagerPay));
                    }
                    amount = amount.add(amountByShopManager);
                    addEstimateProfit(checkpointTaskSpaceDTO, amount);
                }
                newbieTask.setInviteNum(activityDef.getStartNum() == null ? 0 : activityDef.getStartNum());
                newbieTask.setActivityDefId(activityDef.getId());
                newbieTask.setRebate(amount.toString());
                taskDTO.getTaskDetails().add(newbieTask);
            }
        }
        tasks.add(taskDTO);
    }

    /***
     * 日常任务详细
     * @param tasks
     * @param merchantId
     * @param shopId
     * @param userId
     */
    @Override
    public void queryDailyTaskDetail(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, List<CheckpointTaskSpaceDTO.TaskDTO> tasks, Long merchantId, Long shopId, Long userId) {
        CheckpointTaskSpaceDTO.TaskDTO taskDTO = new CheckpointTaskSpaceDTO.TaskDTO();
        //是否有活动
        boolean hasTask = false;
        //查询店铺下的回填订单任务
        List<ActivityDef> fillBackOrderActivityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId,
                Arrays.asList(ActivityDefTypeEnum.FILL_BACK_ORDER.getCode(), ActivityDefTypeEnum.FILL_BACK_ORDER_V2.getCode()));
        if (!CollectionUtils.isEmpty(fillBackOrderActivityDefs)) {
            ActivityDef activityDef = fillBackOrderActivityDefs.get(0);
            if (activityDef.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
                addTaskDetail(userId, checkpointTaskSpaceDTO, taskDTO, activityDef);
                hasTask = true;
            }
        }
        //查询店铺下的好评晒图任务
        List<ActivityDef> goodCommentActivityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId,
                Arrays.asList(ActivityDefTypeEnum.GOOD_COMMENT.getCode(), ActivityDefTypeEnum.GOOD_COMMENT_V2.getCode()));
        if (!CollectionUtils.isEmpty(goodCommentActivityDefs)) {
            ActivityDef activityDef = goodCommentActivityDefs.get(0);
            if (activityDef.getStock() > 0) {
                addTaskDetail(userId, checkpointTaskSpaceDTO, taskDTO, activityDef);
                hasTask = true;
            }
        }
        //查询店铺下的收藏加购任务
        List<ActivityDef> favcartActivityDefs = activityDefService.queryByQueryWrapper(merchantId, shopId,
                Arrays.asList(ActivityDefTypeEnum.FAV_CART.getCode(), ActivityDefTypeEnum.FAV_CART_V2.getCode()));
        if (!CollectionUtils.isEmpty(favcartActivityDefs)) {
            ActivityDef activityDef = favcartActivityDefs.get(0);
            if (activityDef.getStock() > 0) {
                addTaskDetail(userId, checkpointTaskSpaceDTO, taskDTO, activityDef);
                hasTask = true;
            }
        }
        if (hasTask) {
            taskDTO.setType(CheckpointTaskSpaceTypeEnum.DAILY_TASK.getCode());
            taskDTO.setMerchantId(merchantId);
            taskDTO.setShopId(shopId);
            tasks.add(taskDTO);
        }
    }

    /***
     * 拼接任务详细，剩余任务数，预估可赚
     * @param userId
     * @param checkpointTaskSpaceDTO
     * @param taskDTO
     * @param activityDef
     */
    private void addTaskDetail(Long userId, CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, CheckpointTaskSpaceDTO.TaskDTO taskDTO, ActivityDef activityDef) {
        NewbieTask newbieTask = new NewbieTask();
        newbieTask.setType(activityDef.getType());
        BigDecimal commission = activityDef.getCommission() != null ? activityDef.getCommission() : activityDef.getGoodsPrice();
        CommissionBO commissionBO = commissionService.commissionCalculate(userId, commission);
        newbieTask.setRebate(commissionBO.getCommission().toString());
        newbieTask.setActivityDefId(activityDef.getId());
        taskDTO.getTaskDetails().add(newbieTask);

        addRemainTask(checkpointTaskSpaceDTO, 1);
        addEstimateProfit(checkpointTaskSpaceDTO, commissionBO.getCommission());
    }

    /***
     * 查询活动定义的已中奖信息
     * @param merchantId
     * @param shopId
     * @param defIds
     * @return
     */
    @Override
    public Map<Long, List<UserCoupon>> queryActivityDefInfo(Long merchantId, Long shopId, Long userId, List<Long> defIds, List<Integer> types) {
        List<UserCoupon> userCoupons = userCouponService.queryByDefIds(merchantId, shopId, userId, defIds, types);
        if (CollectionUtils.isEmpty(userCoupons)) {
            return new HashMap<>();
        }
        Map<Long, List<UserCoupon>> resp = new HashMap<>();
        for (UserCoupon userCoupon : userCoupons) {
            List<UserCoupon> userCouponList = resp.get(userCoupon.getActivityDefId());
            if (CollectionUtils.isEmpty(userCouponList)) {
                userCouponList = new ArrayList<>();
            }
            userCouponList.add(userCoupon);
            resp.put(userCoupon.getActivityDefId(), userCouponList);
        }
        return resp;
    }

    /***
     * 获取已赚收益
     * @param checkpointTaskSpaceDTO
     * @param merchantId
     * @param shopId
     * @param user
     */
    @Override
    public void queryFinishProfit(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, Long merchantId, Long shopId, User user) {
        Map<String, Object> params = new HashMap<>();
        params.put("mchId", merchantId);
        params.put("shopId", shopId);
        params.put("userId", user.getId());
        params.put("types", UpayWxOrderAboutVO.finishProfitUpayWxOrderTypes());
        params.put("status", UpayWxOrderStatusEnum.ORDERED.getCode());
        params.put("bizTypes", UpayWxOrderAboutVO.finishProfitUpayWxOrderBizTypes());
        BigDecimal totalFee = upayWxOrderService.sumUpayWxOrderTotalFee(params);
        checkpointTaskSpaceDTO.setEarnProfit(totalFee.setScale(2, BigDecimal.ROUND_DOWN));
    }

    /***
     * 弹窗信息
     * @param checkpointTaskSpaceDTO
     * @param merchantId
     * @param shopId
     * @param user
     */
    @Override
    public void queryDialog(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, Long merchantId, Long shopId, User user) {
        checkpointTaskSpaceDTO.setDialogs(new ArrayList<>());
        Long userId = user.getId();
        String inviteAward = stringRedisTemplate.opsForValue().get(RedisKeyConstant.SHOP_MANAGER_INVITE_FANS_AWARD + userId);
        if (StringUtils.isNotBlank(inviteAward)) {
            Map map = JSONObject.parseObject(inviteAward, Map.class);
            if (map == null) {
                log.error("<<<<<<<<店长邀请粉丝奖励 异常： redisInfo:{}", inviteAward);
            }
            CheckpointTaskSpaceDTO.CheckpointTaskSpaceDialog dialog = new CheckpointTaskSpaceDTO.CheckpointTaskSpaceDialog();
            String inviteNum = map.get("inviteNum").toString();
            String amount = map.get("amount").toString();
            dialog.setNum(NumberUtils.toInt(inviteNum));
            dialog.setRebate(amount);
            dialog.setType(1);
            dialog.setUnit(new BigDecimal(amount).divide(new BigDecimal(inviteNum)).toString());
            checkpointTaskSpaceDTO.getDialogs().add(dialog);
            stringRedisTemplate.delete(RedisKeyConstant.SHOP_MANAGER_INVITE_FANS_AWARD + userId);
        }

        Long recommendSize = stringRedisTemplate.opsForList().size(RedisKeyConstant.RECOMMEND_SHOP_MANAGER_AWARD + userId);
        recommendSize = recommendSize == null ? 0 : recommendSize;
        if (recommendSize > 0) {
            BigDecimal totalAmount = BigDecimal.ZERO;
            for (int i = 0; i < recommendSize; i++) {
                String amount = stringRedisTemplate.opsForList().rightPop(RedisKeyConstant.RECOMMEND_SHOP_MANAGER_AWARD + userId);
                if (StringUtils.isNotBlank(amount)) {
                    totalAmount = totalAmount.add(new BigDecimal(amount));
                }
            }

            CheckpointTaskSpaceDTO.CheckpointTaskSpaceDialog dialog = new CheckpointTaskSpaceDTO.CheckpointTaskSpaceDialog();
            dialog.setNum(recommendSize.intValue());
            dialog.setRebate(totalAmount.toString());
            dialog.setType(2);
            dialog.setUnit(totalAmount.divide(new BigDecimal(String.valueOf(recommendSize))).toString());
            checkpointTaskSpaceDTO.getDialogs().add(dialog);
            stringRedisTemplate.delete(RedisKeyConstant.SHOP_MANAGER_INVITE_FANS_AWARD + userId);
        }
    }

    /***
     * 计算剩余任务数
     * @param checkpointTaskSpaceDTO
     * @param taskNum
     */
    private void addRemainTask(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, int taskNum) {
        checkpointTaskSpaceDTO.setRemainTask(checkpointTaskSpaceDTO.getRemainTask() + taskNum);
    }

    /***
     * 计算预估收益
     * @param checkpointTaskSpaceDTO
     * @param taskAmount
     */
    private void addEstimateProfit(CheckpointTaskSpaceDTO checkpointTaskSpaceDTO, BigDecimal taskAmount) {
        checkpointTaskSpaceDTO.setEstimateProfit(checkpointTaskSpaceDTO.getEstimateProfit().add(taskAmount));
    }

    /***
     * 判断是否做过
     * @param newbieTask
     * @param userCoupons
     * @param scene 1新手 2店长
     * @param type  活动定义类型
     */
    private void isDoneUserCoupon(NewbieTask newbieTask, List<UserCoupon> userCoupons, int scene, Integer startNum, Integer type) {
        if (scene == 1) {
            UserCoupon userCoupon = userCoupons.get(0);
            if (UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode() == userCoupon.getType()
                    && type == ActivityDefTypeEnum.NEWBIE_TASK_BONUS.getCode()) {
                newbieTask.setFinish(true);
            }
            if (UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode() == userCoupon.getType()
                    && type == ActivityDefTypeEnum.NEWBIE_TASK_REDPACK.getCode()) {
                newbieTask.setFinish(true);
            }
            if (UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode() == userCoupon.getType()
                    && type == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                newbieTask.setFinish(true);
            }
        }
        if (scene == 2) {
            if (startNum != null) {
                List<UserCoupon> recommend_shopmanagers = new ArrayList<>();
                for (UserCoupon userCoupon : userCoupons) {
                    if (UserCouponTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode() == userCoupon.getType()) {
                        recommend_shopmanagers.add(userCoupon);
                    }
                }
                if (recommend_shopmanagers.size() >= startNum
                        && type == ActivityDefTypeEnum.SHOP_MANAGER_TASK_RECOMMEND_SHOPMANAGER.getCode()) {
                    newbieTask.setFinish(true);
                }
            } else {
                UserCoupon userCoupon = userCoupons.get(0);
                if (UserCouponTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode() == userCoupon.getType()
                        && type == ActivityDefTypeEnum.SHOP_MANAGER_TASK_ADD_WX.getCode()) {
                    newbieTask.setFinish(true);
                }
                if (UserCouponTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode() == userCoupon.getType()
                        && type == ActivityDefTypeEnum.SHOP_MANAGER_TASK_INVITE_FANS.getCode()) {
                    newbieTask.setFinish(true);
                }
            }
        }
    }
}
