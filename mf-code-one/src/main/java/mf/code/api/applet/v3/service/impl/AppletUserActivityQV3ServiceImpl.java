package mf.code.api.applet.v3.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityCommissionService;
import mf.code.activity.service.ActivityDefCheckService;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.utils.AppletValidator;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.AppletUserActivityQV3Service;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.constant.UserActivityStatusEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.JsonParseUtil;
import mf.code.common.utils.PageUtil;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.service.UserActivityService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v3.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2019-01-17 下午3:08
 */
@Slf4j
@Service
public class AppletUserActivityQV3ServiceImpl implements AppletUserActivityQV3Service {
    private final ActivityService activityService;
    private final UserActivityService userActivityService;
    private final StringRedisTemplate stringRedisTemplate;
    private final UserCouponService userCouponService;
    private final UserService userService;
    private final ActivityDefService activityDefService;
    private final CommissionService commissionService;
    private final CommonDictService commonDictService;
    private final ActivityCommissionService activityCommissionService;
    private final ActivityDefCheckService activityDefCheckService;

    @Autowired
    public AppletUserActivityQV3ServiceImpl(ActivityCommissionService activityCommissionService, CommonDictService commonDictService, CommissionService commissionService, ActivityService activityService, UserActivityService userActivityService, StringRedisTemplate stringRedisTemplate, UserCouponService userCouponService, UserService userService, ActivityDefService activityDefService, ActivityDefCheckService activityDefCheckService) {
        this.activityService = activityService;
        this.userActivityService = userActivityService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.userCouponService = userCouponService;
        this.userService = userService;
        this.activityDefService = activityDefService;
        this.commissionService = commissionService;
        this.commonDictService = commonDictService;
        this.activityCommissionService = activityCommissionService;
        this.activityDefCheckService = activityDefCheckService;
    }

    @Override
    public Activity createableActivity(Long userId, Long activityDefId) {
        // 判断用户是否创建过活动：1.集合为空，表示有资格
        QueryWrapper<Activity> aWrapper = new QueryWrapper<>();
        aWrapper.lambda()
                .eq(Activity::getUserId, userId)
                .eq(Activity::getActivityDefId, activityDefId);
        List<Activity> activities = activityService.list(aWrapper);
        if (CollectionUtils.isEmpty(activities)) {
            return null;
        }
        // 判断用户是否有进行中的活动：1.有则返回此活动
        List<Long> activityIds = new ArrayList<>();
        for (Activity activity : activities) {
            activityIds.add(activity.getId());
            Integer status = activity.getStatus();
            if (status != null && status == ActivityStatusEnum.PUBLISHED.getCode()) {
                return activity;
            }
        }
        // 判断用户是否有 已中奖的活动
        QueryWrapper<UserActivity> uaWrapper = new QueryWrapper<>();
        uaWrapper.lambda()
                .eq(UserActivity::getUserId, userId)
                .in(UserActivity::getActivityId, activityIds)
                .eq(UserActivity::getStatus, UserActivityStatusEnum.WINNER.getCode());
        List<UserActivity> userActivities = userActivityService.list(uaWrapper);
        if (CollectionUtils.isEmpty(userActivities)) {
            return null;
        }
        for (UserActivity userActivity : userActivities) {
            Integer status = userActivity.getStatus();
            if (status != null && status == UserActivityStatusEnum.WINNER.getCode()) {
                for (Activity activity : activities) {
                    if (activity.getId().equals(userActivity.getActivityId())) {
                        return activity;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public SimpleResponse<Object> openRedPackageStatus(Activity activity, int in, Integer pubJoinScene) {
        Long activityDefId = activity.getActivityDefId();
        Long userId = activity.getUserId();
        Long activityId = activity.getId();

        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验活动是否已过期
        boolean expired = activityService.checkExpired(activity, null, null);
        if (expired) {
            log.info("任务失败, 弹出在挑战一次的弹框");
            // 清除redis历史数据
            Object startUser = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId, userId.toString());
            if (startUser != null) {
                Map startUserVO = JSON.parseObject(startUser.toString(), Map.class);
                Object raid = startUserVO.get("activityId");
                // 清除发起信息
                stringRedisTemplate.opsForHash().delete(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId, userId.toString());
                // 助力者信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + raid + ":" + userId);
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + raid);
                // 删除奖池信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + raid);
                // 删除活动助力金集合
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + raid);
            }

            // 加工返回信息
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("activityStatus", 4);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        // 获取拆红包活动详情数据
        Map<String, Object> resultVO = this.queryOpenRedPackageActivity(activity);
        // 校验活动是否开奖(数据库是否存在 award_time)
        boolean awarded = activityService.checkAwarded(activity);

        if (awarded && in == 1) {
            // 已开奖
            // 是否弹出中奖窗口
            boolean awardDialog = this.awardDialog(activity);
            if (awardDialog) {
                // 删除助力者弹窗信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
				/*// 连续2次调用时，避免第一次出现6后，前端立马调第二次出现5
				RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_FORBID +userId, 3, TimeUnit.SECONDS);*/
                resultVO.put("activityStatus", 6);
                resultVO.putAll(getCheckpointInfo(userId, activity.getShopId(), activity.getMerchantId(), activity.getPayPrice()));
                response.setStatusEnum(ApiStatusEnum.SUCCESS);
                response.setData(resultVO);
                return response;
            } else if (pubJoinScene != null) {
                resultVO.put("activityStatus", 5);
            }
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_FORBID + userId);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }
        // 是否弹助力窗口
        boolean assistDialog = this.assistDialog(activity);
        if (assistDialog && in == 1) {
            // 删除redis中的助力信息，只弹一次
            stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activityId + ":" + userId);
            resultVO.put("activityStatus", 7);
        }
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    private Map<String, Object> getCheckpointInfo(Long userId, Long shopID, Long merchantID, BigDecimal payPrice) {
        Map<String, Object> map = new HashMap<>();
        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userId, shopID, merchantID);
        CommissionBO commissionBO = this.activityCommissionService.calculateCommission(userId, payPrice);
        map.putAll(BeanMapUtil.beanToMap(commissionResp));
        map.put("commission", commissionBO.getCommission());
        return map;
    }

    @Override
    public Map<String, Object> queryOpenRedPackageActivity(Activity activity) {
        Long activityId = activity.getId();
        Long activityDefId = activity.getActivityDefId();
        Long userId = activity.getUserId();
        Date ctime = activity.getCtime();

        // 获取redis中 发起者信息
        Map startUserVO = new HashMap<>();
        Object startUser = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId.toString(), userId.toString());
        if (startUser != null) {
            startUserVO = JSON.parseObject(startUser.toString(), Map.class);
            stringRedisTemplate.expire(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId.toString(), 180, TimeUnit.DAYS);
        }

        // 获取redis中 助力者信息
        List<Map> assistVOList = new ArrayList<>();
        String assisters = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId);
        if (JsonParseUtil.booJsonArr(assisters)) {
            List<Map> assisterList = JSON.parseArray(assisters, Map.class);
            assistVOList.addAll(assisterList);
        }
        if (CollectionUtils.isEmpty(assistVOList) && DateUtil.addDay(ctime, 1).getTime() <= System.currentTimeMillis()) {
            // redis 助力信息为空 并且 已过了一天，则从数据库查询是否有助力者信息
            List<Long> couponUserIds = new ArrayList<>();
            QueryWrapper<UserCoupon> ucWrapper = new QueryWrapper<>();
            ucWrapper.lambda()
                    .eq(UserCoupon::getActivityId, activityId)
                    .eq(UserCoupon::getType, UserCouponTypeEnum.OPEN_RED_PACKAGE_ASSIST.getCode());
            ucWrapper.orderByDesc("id");
            List<UserCoupon> userCoupons = userCouponService.list(ucWrapper);
            if (!CollectionUtils.isEmpty(userCoupons)) {
                for (UserCoupon userCoupon : userCoupons) {
                    couponUserIds.add(userCoupon.getUserId());
                }
            }
            Map<Long, User> userMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(couponUserIds)) {
                QueryWrapper<User> uWrapper = new QueryWrapper<>();
                uWrapper.lambda()
                        .in(User::getId, couponUserIds);
                List<User> users = userService.list(uWrapper);
                for (User user : users) {
                    userMap.put(user.getId(), user);
                }
            }
            if (!CollectionUtils.isEmpty(userCoupons)) {
                for (UserCoupon userCoupon : userCoupons) {
                    User user = userMap.get(userCoupon.getUserId());
                    Map<String, Object> assistVO = this.processUserInfoVO4OpenRedPackageActivity(user, userCoupon.getAwardTime(), userCoupon.getAmount());
                    assistVOList.add(assistVO);
                }
                stringRedisTemplate.opsForValue().set(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + activityId, JSON.toJSONString(assistVOList));
            }
        }

        // 获取 邀请人信息
        Map pubUserVO = new HashMap<>();
        Long puid = null;
        Object pubUserId = startUserVO.get("pubUserId");
        if (null != pubUserId) {
            puid = Long.valueOf(pubUserId.toString());
        }
        if (null != puid) {
            Object pubUser = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId.toString(), puid.toString());
            if (pubUser != null) {
                Map pubUserInfo = JSON.parseObject(pubUser.toString(), Map.class);
                Date pubTime = DateUtil.stringtoDate(pubUserInfo.get("time").toString(), DateUtil.POINT_DATE_FORMAT_TWO);
                Date time = DateUtil.stringtoDate(startUserVO.get("time").toString(), DateUtil.POINT_DATE_FORMAT_TWO);
                ;
                if (pubTime != null && time != null && time.getTime() >= pubTime.getTime()) {
                    pubUserVO = pubUserInfo;
                }
            }
        }

        // 获取 本次助力的信息
        List<Map> assistDialogList = new ArrayList<>();
        boolean assistDialog = assistDialog(activity);
        if (assistDialog) {
            String assistDialogs = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activity.getId() + ":" + activity.getUserId());
            if (JsonParseUtil.booJsonArr(assistDialogs)) {
                assistDialogList.addAll(JSON.parseArray(assistDialogs, Map.class));
            }
        }

        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        BigDecimal tax = activity.getPayPrice().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);

        // 加工前端页面展示的数据结构
        Map<String, Object> activityVO = new HashMap<>();
        activityVO.put("activityId", activity.getId());
        activityVO.put("activityDefId", activity.getActivityDefId());
        activityVO.put("activityAmount", activity.getPayPrice().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
        activityVO.put("endTime", activity.getEndTime());
        activityVO.put("awardPool", startUserVO.get("awardPool"));
        activityVO.put("buttonStatus", activity.getCondPersionCount() > assistVOList.size() + 1 ? 0 : 1);
        activityVO.put("startUserVO", startUserVO);
        activityVO.put("assistVOList", assistVOList);
        activityVO.put("assistDialogList", assistDialogList);
        activityVO.put("pubUserVO", pubUserVO);
        return activityVO;
    }

    @Override
    public Map<String, Object> processUserInfoVO4OpenRedPackageActivity(User user, Date awardTime, BigDecimal amount) {
        Map<String, Object> assistMap = new HashMap<>();
        if (user != null) {
            assistMap.put("userId", user.getId());
            assistMap.put("avatarUrl", user.getAvatarUrl());
            assistMap.put("nickName", user.getNickName());
            assistMap.put("time", DateUtil.dateToString(awardTime, DateUtil.POINT_DATE_FORMAT_TWO));
            assistMap.put("amount", amount);
        }
        return assistMap;
    }

    @Override
    public SimpleResponse openRedPackageBarrage(String current, String size, String activityDefId) {
        // 参数校验
        SimpleResponse<Object> response = AppletValidator.checkParam4queryActivityDefUserJoin(current, size, activityDefId);
        if (response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 转化参数
        Integer crt = Integer.valueOf(current);
        Integer sz = Integer.valueOf(size);
        // 查询redis缓存
        String winners = stringRedisTemplate.opsForValue().get(RedisKeyConstant.OPEN_RED_PACKAGE_WINNER_LIST + activityDefId);
        if (StringUtils.isNotBlank(winners)) {
            List<Map> winnerList = JSON.parseArray(winners, Map.class);
            Map<Integer, List<Map>> winnerListPageMap = PageUtil.VO2PageVO(winnerList, sz);
            HashMap<Object, Object> pageVO = new HashMap<>();
            pageVO.put("pages", winnerListPageMap.size());
            pageVO.put("current", crt);
            pageVO.put("size", sz);
            pageVO.put("total", winnerList.size());
            pageVO.put("barrageInfoList", winnerListPageMap.get(crt));
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(pageVO);
            return response;
        }
        return response;
    }

    @Override
    public SimpleResponse checkOpenRedPackageValidity(String merchantId, String shopId, String userId, String activityDefId, String in) {
        // 校验参数
        SimpleResponse<Object> response = AppletValidator.checkParams4UserIdAndActivityDefId(userId, activityDefId, in);
        if (response != null && response.getCode() != ApiStatusEnum.SUCCESS.getCode()) {
            return response;
        }
        // 转化参数
        Long uid = Long.valueOf(userId);
        Long adid = Long.valueOf(activityDefId);
        int bin = Integer.parseInt(in);
        // 校验用户发起活动的资格 (返回在进行中，或已中奖的活动）
        Activity activity = this.createableActivity(uid, adid);
        if (activity != null) {
            return this.openRedPackageStatus(activity, bin, null);
        }
        // 校验活动定义是否能正常发起
        boolean validity = activityDefCheckService.checkValidity(adid, null, false);

        // 加工展现数据
        Map<String, Object> resultVO = new HashMap<>();

        // 没有进行中的活动或已中奖的活动后，校验是否有发起过，但已过期的活动 并且 redis 有key
        boolean expired = activityService.checkExpired(null, uid, adid);
        if (expired) {
            log.info("任务失败, 弹出在挑战一次的弹框");
            // 清除redis历史数据
            Object startUser = stringRedisTemplate.opsForHash().get(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId, userId);
            if (startUser != null) {
                Map startUserVO = JSON.parseObject(startUser.toString(), Map.class);
                Object raid = startUserVO.get("activityId");
                // 清除发起信息
                stringRedisTemplate.opsForHash().delete(RedisKeyConstant.OPEN_RED_PACKAGE_STARTER + activityDefId, userId);
                // 助力者信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + raid + ":" + userId);
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTER + raid);
                // 删除奖池信息
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_POOL + raid);
                // 删除活动助力金集合
                stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AMOUNT_LIST + raid);
            }

            // 加工返回信息
            resultVO = new HashMap<>();
            resultVO.put("activityStatus", 4);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        if (!validity) {
            log.info("红包已被领取光啦，下次早点来！");
            resultVO.put("activityStatus", 3);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(uid, Long.valueOf(shopId), Long.valueOf(merchantId));

        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        BigDecimal tax = activityDefService.getById(adid).getGoodsPrice().multiply(rate).divide(new BigDecimal("100"), 3, BigDecimal.ROUND_DOWN);

        resultVO.putAll(BeanMapUtil.beanToMap(commissionResp));
        resultVO.put("activityAmount", activityDefService.getById(adid).getGoodsPrice().subtract(tax).setScale(2, BigDecimal.ROUND_DOWN).toString());
        resultVO.put("activityStatus", 2);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    /**
     * 是否弹出中奖窗口
     *
     * @param activity
     * @return
     */
    private boolean awardDialog(Activity activity) {
        boolean awardDialog = stringRedisTemplate.hasKey(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_DIALOG + activity.getId() + ":" + activity.getUserId());
        stringRedisTemplate.delete(RedisKeyConstant.OPEN_RED_PACKAGE_AWARD_DIALOG + activity.getId() + ":" + activity.getUserId());
        return awardDialog;
    }

    /**
     * 是否弹出助力窗口, 不能这里直接把redis的数据删掉
     *
     * @param activity
     * @return
     */
    private boolean assistDialog(Activity activity) {
        return stringRedisTemplate.hasKey(RedisKeyConstant.OPEN_RED_PACKAGE_ASSIST_DIALOG + activity.getId() + ":" + activity.getUserId());
    }

    @Override
    public boolean assistableActivity(Long userId, Activity activity) {
        // 判断用户助力的活动是否是自己的活动
        if (userId.equals(activity.getUserId())) {
            return false;
        }
        // 校验同活动定义下是不是 助力过
        return RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.OPEN_RED_PACKAGE_ASSISTABLE + activity.getActivityDefId() + ":" + userId, 365, TimeUnit.DAYS);
    }
}
