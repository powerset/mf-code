package mf.code.api.applet.v11.upayorder;

import mf.code.common.exception.ArgumentException;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.UserOrderReqDTO;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * mf.code.api.applet.v11.upayorder
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月26日 16:14
 */
@Component
public class UpayOrderDelegate {

    private static Map<UpayWxOrderBizTypeEnum, AbstractUpayOrderService> registerService = new ConcurrentHashMap<>(128);

    @Autowired
    private List<AbstractUpayOrderService> upayOrderServices;


    @PostConstruct
    public void init() {
        for (AbstractUpayOrderService upayOrderService : upayOrderServices) {
            registerService.put(upayOrderService.getType(), upayOrderService);
        }
    }

    private AbstractUpayOrderService getUpayOrderService(int type) {
        AbstractUpayOrderService upayOrderService = registerService.get(UpayWxOrderBizTypeEnum.findByCode(type));
        if (upayOrderService == null) {
            throw new ArgumentException("该类型的业务，暂未提供！");
        }
        return upayOrderService;
    }

    /***
     * 支付创建
     *
     * @param req
     * @return
     */
    public SimpleResponse create(UserOrderReqDTO req) {
        AbstractUpayOrderService converterService = getUpayOrderService(NumberUtils.toInt(req.getBizType()));
        return converterService.doCreate(req);
    }

    /***
     * 支付回调
     *
     * @param order
     * @return
     */
    public SimpleResponse wxPayCallBackBiz(UpayWxOrder order) {
        AbstractUpayOrderService converterService = getUpayOrderService(order.getBizType());
        return converterService.wxPayCallBack(order);
    }

    public Map queryStatusBiz(UpayWxOrder upayWxOrder) {
        AbstractUpayOrderService converterService = getUpayOrderService(upayWxOrder.getBizType());
        return converterService.queryStatusBizData(upayWxOrder);
    }
}
