package mf.code.api.applet.login.domain.valueobject;

import lombok.Data;
import mf.code.api.applet.login.domain.aggregateroot.ActivityAggregateRoot;
import mf.code.api.applet.login.domain.aggregateroot.AppletUserAggregateRoot;
import mf.code.common.utils.RegexUtils;
import mf.code.user.constant.UserPubJoinTypeEnum;
import org.apache.commons.lang.StringUtils;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-23 下午3:03
 */
@Data
public abstract class InviteSpecification {
	protected String activityId;
	protected String userPubJoinType;
	protected String shopId;
	protected InvitationRoleEnum invitationRoleEnum;

	public static InviteSpecification selectInvitationSpecification(ActivityAggregateRoot activity, String pubType) {
		if (activity != null) {
			return activity.getActivityInvitationSpecification();
		}
		if (StringUtils.isBlank(pubType) || !RegexUtils.StringIsNumber(pubType)) {
			return null;
		}
		int type = Integer.parseInt(pubType);
		if (UserPubJoinTypeEnum.CHECKPOINTS.getCode() == type) {
			return new CheckpointsInvitationSpecification();
		}
		return null;
	}

	public Boolean canInvitation(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser) {
		if (this.getInvitationRoleEnum() == InvitationRoleEnum.PUB) {
			return canInviteAppletSubUser(appletPubUser, appletSubUser);
		}
		if (this.getInvitationRoleEnum() == InvitationRoleEnum.SUB) {
			return canBeInvitedByAppletPubUser(appletPubUser, appletSubUser);
		}
		return false;
	}

	// 是否可以邀请别人
	protected abstract Boolean canInviteAppletSubUser(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser);

	// 是否可以被别人邀请
	protected abstract Boolean canBeInvitedByAppletPubUser(AppletUserAggregateRoot appletPubUser, AppletUserAggregateRoot appletSubUser);

}
