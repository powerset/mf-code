package mf.code.api.applet.v11.upayorder;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.one.dto.UserOrderReqDTO;
import mf.code.one.dto.UserPayOrderBizDTO;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import mf.code.user.constant.UpayWxOrderBizTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.seller.service.createDef.impl
 *
 * @author yechen
 * @date 2019年03月18日 20:18
 */
@Component
@Slf4j
public abstract class AbstractUpayOrderService {
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private WeixinPayService weixinPayService;
    @Autowired
    private WxpayProperty wxpayProperty;
    @Autowired
    private UserService userService;
    @Autowired
    private UpayBalanceService upayBalanceService;

    /***
     * 创建
     *
     * @param req
     * @return
     */
    final SimpleResponse doCreate(UserOrderReqDTO req) {
        SimpleResponse simpleResponse;
        simpleResponse = assertReq(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        simpleResponse = checkCanCreate(req);
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        Object data = simpleResponse.getData();
        UserPayOrderBizDTO userPayOrderBizDTO = null;
        if (data instanceof UserPayOrderBizDTO) {
            userPayOrderBizDTO = (UserPayOrderBizDTO) data;
        }
        if (new BigDecimal(req.getAmount()).compareTo(BigDecimal.ZERO) == 0) {
            //当金额为0元时，采用余额支付方式
            req.setPayType(2);
        }
        if (req.getPayType() == 2) {
            //余额支付
            simpleResponse = createUserOrderBalance(req, userPayOrderBizDTO);
        } else if (req.getPayType() == 1) {
            //wx支付
            simpleResponse = createUserOrderWx(req, userPayOrderBizDTO);
        } else {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请选择正确的支付方式");
        }
        if (simpleResponse.error()) {
            return simpleResponse;
        }
        return simpleResponse;
    }


    /***
     * wx 支付的回调业务
     *
     * @param order
     * @return
     */
    final SimpleResponse wxPayCallBack(UpayWxOrder order) {
        SimpleResponse simpleResponse;
        simpleResponse = wxPayCallBackBiz(order);
        return simpleResponse;
    }

    /***
     * 查询订单支付成功后的业务数据返回
     *
     * @param order
     * @return
     */
    final Map queryStatusBizData(UpayWxOrder order) {
        Map map = new HashMap();
        map = queryStatusBiz(order);
        return map;
    }

    /**
     * 支付订单-余额
     *
     * @param req
     * @return
     */
    protected SimpleResponse createUserOrderBalance(UserOrderReqDTO req, UserPayOrderBizDTO userPayOrderBizDTO) {
        boolean zeroPay = false;
        if (new BigDecimal(req.getAmount()).compareTo(BigDecimal.ZERO) == 0) {
            zeroPay = true;
        }
        Long bizValue = 0L;
        String remark = "";
        if (userPayOrderBizDTO != null) {
            if (StringUtils.isNotBlank(userPayOrderBizDTO.getRemark())) {
                remark = userPayOrderBizDTO.getRemark();
            }
            if (userPayOrderBizDTO.getBizValue() != null && userPayOrderBizDTO.getBizValue() > 0) {
                bizValue = userPayOrderBizDTO.getBizValue();
            }
        }
        if (StringUtils.isNotBlank(req.getBizvalue())) {
            bizValue = NumberUtils.toLong(req.getBizvalue());
        }

        if(!zeroPay){
            UpayBalance upayBalance = upayBalanceService.query(req.getMerchantId(), req.getShopId(), req.getUserId());
            //余额的准确性处理 BigDecimal 0表示相等，-1表示小于，1表示大于
            if (upayBalance == null || upayBalance.getBalance().subtract(new BigDecimal(req.getAmount())).compareTo(BigDecimal.ZERO) >= 0) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "钱包余额不足，不能发起");
            }
        }

        //订单号
        String outTraderNo = "V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();

        // begin
        //订单创建
        UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(req.getUserId(), OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.USERPAY.getCode(),
                outTraderNo, null, req.getMerchantId(), req.getShopId(), OrderStatusEnum.ORDERED.getCode(), new BigDecimal(req.getAmount()),
                IpUtil.getServerIp(), null, remark, new Date(), null, null, NumberUtils.toInt(req.getBizType()), bizValue);
        Integer andUpdateBalanceResult = upayWxOrderService.createAndUpdateBalance(upayWxOrder, new BigDecimal(req.getAmount()));
        if (andUpdateBalanceResult == 0) {
            return new SimpleResponse(10, "创建订单/更新余额失败");
        }
        Map<String, Object> respMap = this.addRespMap(null, null, null, null, upayWxOrder.getId(), upayWxOrder.getBizValue());
        respMap.put("dialog", false);
        SimpleResponse simpleResponse = wxPayCallBackBiz(upayWxOrder);
        if(simpleResponse.error()){
            return simpleResponse;
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS, respMap);
    }

    /**
     * 支付订单-wx
     *
     * @param req
     * @return
     */
    protected SimpleResponse createUserOrderWx(UserOrderReqDTO req, UserPayOrderBizDTO userPayOrderBizDTO) {
        Long bizValue = 0L;
        String remark = "";
        if (userPayOrderBizDTO != null) {
            if (StringUtils.isNotBlank(userPayOrderBizDTO.getRemark())) {
                remark = userPayOrderBizDTO.getRemark();
            }
            if (userPayOrderBizDTO.getBizValue() != null && userPayOrderBizDTO.getBizValue() > 0) {
                bizValue = userPayOrderBizDTO.getBizValue();
            }
        }
        if (StringUtils.isNotBlank(req.getBizvalue())) {
            bizValue = NumberUtils.toLong(req.getBizvalue());
        }
        SimpleResponse simpleResponse = new SimpleResponse();
        //创建
        User user = userService.selectByPrimaryKey(req.getUserId());
        //订单号
        String outTraderNo = "V1" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        String ipAddress = IpUtil.getServerIp();
        //附加数据
        String attach = String.valueOf(req.getUserId());
        BigDecimal amount = new BigDecimal(req.getAmount());
        String body = WeixinPayConstants.APPLETE_BODY;
        //拼装微信统一下单的参数
        Map<String, Object> unifiedorderReqParams = weixinPayService.getUnifiedorderReqParams(amount, outTraderNo, ipAddress,
                wxpayProperty.getMfAppId(), WeixinPayConstants.JSAPI, attach, user.getOpenId(), WeixinPayConstants.APPLET_NOTIFY_SOURCE_NEW, body);
        UpayWxOrder upayWxOrder = upayWxOrderService.addPo(req.getUserId(), OrderPayTypeEnum.WEIXIN.getCode(), OrderTypeEnum.USERPAY.getCode(), outTraderNo,
                null, req.getMerchantId(), req.getShopId(), OrderStatusEnum.ORDERING.getCode(), amount, ipAddress, null, remark, null, null,
                JSONObject.toJSONString(unifiedorderReqParams), NumberUtils.toInt(req.getBizType()), bizValue);
        this.upayWxOrderService.create(upayWxOrder);

        //统一下单的返回
        Map<String, Object> unifiedorderRespMap = this.weixinPayService.unifiedorder(unifiedorderReqParams);
        log.info("小程序支付后的结果 xmlMap:{}", unifiedorderRespMap);
        if (unifiedorderRespMap == null || (StringUtils.isNotBlank(unifiedorderRespMap.get("return_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("return_code").toString()))
                || (StringUtils.isNotBlank(unifiedorderRespMap.get("result_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("result_code").toString()))) {
            if (StringUtils.isNotBlank(unifiedorderRespMap.get("return_msg").toString())) {
                simpleResponse.setMessage(unifiedorderRespMap.get("return_msg").toString());
                upayWxOrder.setRemark(unifiedorderRespMap.get("return_msg").toString());
            } else if (StringUtils.isNotBlank(unifiedorderRespMap.get("err_code_des").toString())) {
                simpleResponse.setMessage(unifiedorderRespMap.get("err_code_des").toString());
                upayWxOrder.setRemark(unifiedorderRespMap.get("err_code_des").toString());
            } else {
                simpleResponse.setMessage("微信充值异常...");
                upayWxOrder.setRemark("微信充值异常...");
            }
            //更新db
            upayWxOrder.setStatus(OrderStatusEnum.FAILED.getCode());
            upayWxOrder.setUtime(new Date());
            this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);
            return simpleResponse;
        }

        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("timeStamp", String.valueOf(DateUtil.getCurrentSeconds()));
        respMap.put("nonceStr", RandomStrUtil.randomStr(28));
        String packageInfo = "prepay_id=" + unifiedorderRespMap.get("prepay_id");
        respMap.put("package", packageInfo);
        respMap.put("signType", "MD5");
        String sortSign = SortUtil.buildSignStr(respMap);
        String sign = "appId=" + wxpayProperty.getMfAppId() + "&" + sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        log.info("小程序返回前端的sign：{}", sign);
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        //签名sign
        respMap.put("paySign", sign);
        respMap.put("orderId", upayWxOrder.getId());
        respMap.put("bizValue", upayWxOrder.getBizValue());
        respMap.put("dialog", true);
        simpleResponse.setData(respMap);
        return simpleResponse;
    }

    private Map<String, Object> addRespMap(String timeStamp, String nonceStr, String packageInfo, String sign, Long orderkeyID, Long bizvalue) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("timeStamp", StringUtils.isNotBlank(timeStamp) ? timeStamp : "");
        respMap.put("nonceStr", StringUtils.isNotBlank(nonceStr) ? nonceStr : "");
        respMap.put("package", StringUtils.isNotBlank(packageInfo) ? packageInfo : "");
        respMap.put("signType", "MD5");
        //签名sign
        respMap.put("paySign", StringUtils.isNotBlank(sign) ? sign : "");
        respMap.put("orderId", orderkeyID);
        respMap.put("bizValue", bizvalue);
        return respMap;
    }

    /***
     * 获取bizType枚举类
     *
     * @return
     */
    public abstract UpayWxOrderBizTypeEnum getType();


    /***
     * 检查入参的有效性
     *
     * @param req
     * @return
     */
    protected abstract SimpleResponse assertReq(UserOrderReqDTO req);

    /***
     * 检查是否可创建
     *
     * @param req
     * @return
     */
    protected abstract SimpleResponse checkCanCreate(UserOrderReqDTO req);

    /***
     * wx支付的回调业务处理
     *
     * @param order
     * @return
     */
    protected abstract SimpleResponse wxPayCallBackBiz(UpayWxOrder order);

    /***
     * 查询订单支付成功后的业务数据返回
     *
     * @param upayWxOrder
     * @return
     */
    protected abstract Map queryStatusBiz(UpayWxOrder upayWxOrder);
}
