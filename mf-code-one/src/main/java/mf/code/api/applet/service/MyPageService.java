package mf.code.api.applet.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.api.applet.service
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月02日 17:25
 */
public interface MyPageService {

    SimpleResponse queryMypage(Long userID, Long shopID);

    SimpleResponse queryMyShopCoupon(Long userID, Long shopID, int offset, int size);

    SimpleResponse queryMyAwardLogV2(Long userID, Long shopID, int offset, int size);

    SimpleResponse queryMyAwardLog(Long userID, Long shopID, int offset, int size);

    SimpleResponse queryMyJoinTaskV2(Long userID, Long shopID, int offset, int size, String type);

    SimpleResponse queryMyJoinTask(Long userID, Long shopID, int offset, int size, String type);

    SimpleResponse queryMyOrder(Long userID, Long merchantID, Long shopID, int offset, int size);

    SimpleResponse queryMyGoods(Long userID, Long shopID, int offset, int size);
}
