package mf.code.api.applet.v3;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v3.service.MinerService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.applet.v3
 *
 * @description: 矿工详情
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月14日 09:42
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/v3/miner")
public class MinerV3Api {
    @Autowired
    private MinerService minerService;

    /***
     * 我的闯关页面信息
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Deprecated
    @RequestMapping(path = "/queryCheckpoint", method = RequestMethod.GET, headers = {"token"})
    public SimpleResponse queryCheckpoint(@RequestParam(name = "merchantId") Long merchantId,
                                          @RequestParam(name = "shopId") Long shopId,
                                          @RequestParam(name = "userId") Long userId) {
        return this.minerService.queryCheckpoint(merchantId, shopId, userId);

    }

    /***
     * 查询我的矿工
     * @param merchantId
     * @param shopId
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    @Deprecated
    @RequestMapping(path = "/queryMyMiner", method = RequestMethod.GET)
    public SimpleResponse queryMyMiner(@RequestParam(name = "merchantId") Long merchantId,
                                       @RequestParam(name = "shopId") Long shopId,
                                       @RequestParam(name = "userId") Long userId,
                                       @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                       @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        return this.minerService.queryMyMiner(merchantId, shopId, userId, offset, size);

    }

    /***
     * 矿工详情
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Deprecated
    @RequestMapping(path = "/queryMinerDetail", method = RequestMethod.GET)
    public SimpleResponse queryMinerDetail(@RequestParam(name = "merchantId") Long merchantId,
                                           @RequestParam(name = "shopId") Long shopId,
                                           @RequestParam(name = "userId") Long userId,
                                           @RequestParam(name = "minerId") Long minerId,
                                           @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                           @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        return this.minerService.queryMinerDetail(merchantId, shopId, minerId, offset, size);
    }

    /***
     * 矿工进度详情--弃用-归到v8版本api内
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @Deprecated
    @RequestMapping(path = "/queryMinerProgressDetail", method = RequestMethod.GET)
    public SimpleResponse queryMinerProgressDetail(@RequestParam(name = "merchantId") Long merchantId,
                                                   @RequestParam(name = "shopId") Long shopId,
                                                   @RequestParam(name = "userId") Long userId,
                                                   @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
                                                   @RequestParam(name = "limit", required = false, defaultValue = "6") int size) {
        return this.minerService.queryMinerProgressDetail(merchantId, shopId, userId, offset, size);
    }
}
