package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.dto.BindMobileReq;
import mf.code.api.applet.service.AppletLoginService;
import mf.code.api.seller.dto.SmsCodeRedis;
import mf.code.common.caller.aliyundayu.DayuCaller;
import mf.code.common.caller.wxlogin.WeixinLoginCaller;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.common.utils.RandomStrUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.common.utils.TokenUtil;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.user.constant.UserConstant;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.codehaus.xfire.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class AppletLoginServiceImpl implements AppletLoginService {
    @Autowired
    private UserService userService;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private WeixinLoginCaller weixinLoginCaller;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DayuCaller dayuCaller;
    @Autowired
    private MerchantShopService merchantShopService;
    @Value("${shopId}")
    private Long shopId;
    @Value("${merchantId}")
    private Long merchantId;

    private final static int SMS_CODE_LIMIT = 5;

    @Override
    public SimpleResponse login(String code, String encryptedData, String iv, String userinfo, String pubUserId, String activityId, String mid, String sid, String sceneStr, String pubType) {
        Long merchantId = null;
        Long shopId = null;
        Integer scene = null;
        if (StringUtils.isNotBlank(mid) && RegexUtils.StringIsNumber(mid)) {
            merchantId = Long.valueOf(mid);
        }
        if (StringUtils.isNotBlank(sid) && RegexUtils.StringIsNumber(sid)) {
            shopId = Long.valueOf(sid);
        }
        if (StringUtils.isNotBlank(sceneStr) && RegexUtils.StringIsNumber(sceneStr)) {
            scene = Integer.valueOf(sceneStr);
        }
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 验证code
        if (StringUtils.isBlank(code)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }

        // 通过code获取用户唯一标识 OpenID 和 会话密钥 session_key
        String session = weixinLoginCaller.code2Session(code);
        if (StringUtils.isBlank(session) || "{}".equals(session)) {
            log.error("code2Session调用微信接口出错, 返回值为空");
            response.setStatusEnum(ApiStatusEnum.ERROR_INTERNET_CONNECTION);
            return response;
        }
        // 是否有errorCode
        JSONObject sessionJO = JSON.parseObject(session);
        if (sessionJO.containsKey("errcode")) {
            log.error("code2Session调用微信接口出错");
            response.setCode(sessionJO.getIntValue("errcode"));
            response.setMessage(sessionJO.getString("errmsg"));
            return response;
        }

        String openId = sessionJO.getString("openid");
        String sessionKey = sessionJO.getString("session_key");

        // redis防重
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }


        Long pubUid = null;
        Activity activity = null;

        QueryWrapper<User> platformUserWrapper = new QueryWrapper<>();
        platformUserWrapper.lambda()
                .eq(User::getShopId, 0)
                .eq(User::getMerchantId, 0)
                .eq(User::getGrantStatus, -2);
        User platformUser = userService.getOne(platformUserWrapper);
        Long platformUserId = platformUser.getId();

        if (StringUtils.isNotBlank(pubUserId) && RegexUtils.StringIsNumber(pubUserId)) {
            pubUid = Long.valueOf(pubUserId);
        }

        // 查询数据库 获取活动对象
        if (StringUtils.isNotBlank(activityId) && RegexUtils.StringIsNumber(activityId)) {
            activity = activityService.findById(Long.valueOf(activityId));
        }


        // 查询数据库 获取用户信息
        boolean isGrant = false;
        boolean beShopVip = false;
        User user = userService.findByOpenid(openId);
        if (user != null) {
            if (user.getGrantStatus().equals(UserConstant.GRANT_STATUS_YES)) {
                isGrant = true;
            }
            if (pubUid != null && pubUid.equals(user.getId())) {
                pubUid = null;
            }
        }


        if (isGrant) {

            if (user.getVipShopId() == null) {
                beShopVip = true;
            }

            // 更新用户店铺信息
            if (shopId != null) {
                user.setShopId(shopId);
                if (beShopVip) {
                    user.setVipShopId(shopId);
                }
            } else {
                user.setVipShopId(user.getShopId());
            }
            if (StringUtils.isNotBlank(mid) && RegexUtils.StringIsNumber(mid)) {
                user.setMerchantId(Long.valueOf(mid));
            }
            if (StringUtils.isNotBlank(sceneStr) && RegexUtils.StringIsNumber(sceneStr)) {
                user.setScene(Integer.valueOf(sceneStr));
            }
            user.setSessionKey(sessionKey);
            user.setUtime(new Date());
            int rows = userService.updateByPrimaryKeySelective(user);
            if (rows == 0) {
                stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("user表更新店铺信息失败");
                return response;
            }

            // 用户邀请记录
            if (pubUid != null && StringUtils.equals(pubType, UserPubJoinTypeEnum.CHECKPOINTS.getCode() + "")) {
                // 非活动类型的邀请关系（财富大闯关 type = 14）
                User pubUser = userService.getById(pubUid);

                if (pubUser != null) {
                    userPubJoinService.userPubJoin4Checkpoints(user, pubUser, platformUserId);
                }
            } else {
                // 活动类型的邀请关系
                userPubJoinService.userPubJoin(pubUid, activity, user);
            }

            Map<String, Object> resultVO = getResultVO(sid, sceneStr, user);
            if (user.getShopId() != null) {
                Map<String, Object> shopInfo = merchantShopService.getShopInfo(user.getShopId());
                resultVO.putAll(shopInfo);

                if (beShopVip) {
                    Map<String, Object> shopVipDialog = new HashMap<>();
                    shopVipDialog.put("userName", user.getNickName());
                    shopVipDialog.put("userAvatar", user.getAvatarUrl());
                    Map shopInfoMap = (Map) shopInfo.get("shopInfo");
                    String shopName = shopInfoMap.get("shopName").toString();
                    shopVipDialog.put("shopName", StringUtils.substring(shopName, 0, 6));
                    QueryWrapper<User> userWrapper = new QueryWrapper<>();
                    userWrapper.lambda()
                            .eq(User::getVipShopId, user.getShopId())
                            .eq(User::getGrantStatus, 1);
                    List<User> users = userService.list(userWrapper);
                    shopVipDialog.put("userCount", 102789 + users.size());
                    resultVO.put("shopVipDialog", shopVipDialog);
                }
            }

            stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
            response.setStatusEnum(ApiStatusEnum.SUCCESS);
            response.setData(resultVO);
            return response;
        }

        Date now = new Date(System.currentTimeMillis() / 1000 * 1000);
        if (StringUtils.isBlank(encryptedData) || StringUtils.isBlank(iv)) {
            if (StringUtils.isNotBlank(userinfo)) {
                if (user == null) {
                    JSONObject uinfo = JSON.parseObject(userinfo);
                    User newUser = new User();
                    newUser.setOpenId(openId);
                    newUser.setMerchantId(merchantId);
                    newUser.setShopId(shopId);
                    newUser.setScene(scene);
                    newUser.setSessionKey(sessionKey);
                    newUser.setNickName(uinfo.getString("nickName"));
                    newUser.setGender(uinfo.getIntValue("gender"));
                    newUser.setCity(uinfo.getString("city"));
                    newUser.setProvince(uinfo.getString("province"));
                    newUser.setCountry(uinfo.getString("country"));
                    newUser.setAvatarUrl(uinfo.getString("avatarUrl"));
                    newUser.setGrantStatus(UserConstant.GRANT_STATUS_NO);
                    newUser.setGrantTime(now);
                    newUser.setCtime(now);
                    newUser.setUtime(now);
                    user = userService.insertUser(user);

                    if (pubUid != null && StringUtils.equals(pubType, UserPubJoinTypeEnum.CHECKPOINTS.getCode() + "")) {
                        // 非活动类型的邀请关系（财富大闯关 type = 14）
                        User pubUser = userService.getById(pubUid);

                        if (pubUser != null) {
                            userPubJoinService.userPubJoin4Checkpoints(user, pubUser, platformUserId);
                        }
                    } else {
                        // 活动类型的邀请关系
                        userPubJoinService.userPubJoin(pubUid, activity, user);
                    }
                }
            }
            response.setCode(2);
            response.setMessage("用户未授权");
            if (user != null) {
                log.error("未授权时 数据库中的user = " + user.toString());
                Map<String, Object> resultVO = getResultVO(sid, sceneStr, user);
                if (user.getShopId() != null) {
                    Map<String, Object> shopInfo = merchantShopService.getShopInfo(user.getShopId());
                    resultVO.putAll(shopInfo);

                    if (beShopVip) {
                        Map<String, Object> shopVipDialog = new HashMap<>();
                        shopVipDialog.put("userName", user.getNickName());
                        shopVipDialog.put("userAvatar", user.getAvatarUrl());
                        Map shopInfoMap = (Map) shopInfo.get("shopInfo");
                        String shopName = shopInfoMap.get("shopName").toString();
                        shopVipDialog.put("shopName", StringUtils.substring(shopName, 0, 6));
                        QueryWrapper<User> userWrapper = new QueryWrapper<>();
                        userWrapper.lambda()
                                .eq(User::getVipShopId, user.getShopId())
                                .eq(User::getGrantStatus, 1);
                        List<User> users = userService.list(userWrapper);
                        shopVipDialog.put("userCount", 102789 + users.size());
                        resultVO.put("shopVipDialog", shopVipDialog);
                    }
                }
                response.setData(resultVO);
            }
            return response;
        }

        String result = decodeEncryptedData(encryptedData, iv, sessionKey);
        if (StringUtils.isBlank(result)) {
            stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
            response.setStatusEnum(ApiStatusEnum.ERROR);
            return response;
        }

        // 入库
        JSONObject jsonRst = JSON.parseObject(result);
        // 当数据库有该用户信息，只是未授权时，只做用户更新操作
        if (user != null) {
            user.setScene(scene);
            user.setShopId(shopId);
            user.setMerchantId(merchantId);
            user.setGrantStatus(1);
            user.setGrantTime(now);
            if (jsonRst.containsKey("unionId")) {
                user.setUnionId(jsonRst.getString("unionId"));
            }
            user.setUtime(now);
            if (user.getVipShopId() == null) {
                beShopVip = true;
                user.setVipShopId(shopId);
            }
            int rows = userService.updateByPrimaryKeySelective(user);
            if (rows == 0) {
                stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                return response;
            }
            if (pubUid != null && StringUtils.equals(pubType, UserPubJoinTypeEnum.CHECKPOINTS.getCode() + "")) {
                // 非活动类型的邀请关系（财富大闯关 type = 14）
                User pubUser = userService.getById(pubUid);

                if (pubUser != null) {
                    userPubJoinService.userPubJoin4Checkpoints(user, pubUser, platformUserId);
                }
            } else {
                // 活动类型的邀请关系
                userPubJoinService.userPubJoin(pubUid, activity, user);
            }

        } else {
            // 当用户第一次登录，数据库里没有用户信息时，做用户插入操作
            beShopVip = true;
            User newUser = new User();
            newUser.setOpenId(openId);
            newUser.setMerchantId(merchantId);
            newUser.setShopId(shopId);
            newUser.setVipShopId(shopId);
            newUser.setScene(scene);
            newUser.setSessionKey(sessionKey);
            newUser.setNickName(jsonRst.getString("nickName"));
            newUser.setGender(jsonRst.getIntValue("gender"));
            newUser.setCity(jsonRst.getString("city"));
            newUser.setProvince(jsonRst.getString("province"));
            newUser.setCountry(jsonRst.getString("country"));
            newUser.setAvatarUrl(jsonRst.getString("avatarUrl"));
            newUser.setGrantStatus(1);
            newUser.setGrantTime(now);
            newUser.setCtime(now);
            newUser.setUtime(now);
            if (jsonRst.containsKey("unionId")) {
                newUser.setUnionId(jsonRst.getString("unionId"));
            }
            user = userService.insertUser(newUser);

            if (user == null) {
                stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                return response;
            }
            if (pubUid != null && StringUtils.equals(pubType, UserPubJoinTypeEnum.CHECKPOINTS.getCode() + "")) {
                // 非活动类型的邀请关系（财富大闯关 type = 14）
                User pubUser = userService.getById(pubUid);

                if (pubUser != null) {
                    userPubJoinService.userPubJoin4Checkpoints(user, pubUser, platformUserId);
                }
            } else {
                // 活动类型的邀请关系
                userPubJoinService.userPubJoin(pubUid, activity, user);
            }
        }

        Map<String, Object> resultVO = getResultVO(sid, sceneStr, user);
        if (user.getShopId() != null) {
            Map<String, Object> shopInfo = merchantShopService.getShopInfo(user.getShopId());
            resultVO.putAll(shopInfo);

            if (beShopVip) {
                Map<String, Object> shopVipDialog = new HashMap<>();
                shopVipDialog.put("userName", user.getNickName());
                shopVipDialog.put("userAvatar", user.getAvatarUrl());
                Map shopInfoMap = (Map) shopInfo.get("shopInfo");
                String shopName = shopInfoMap.get("shopName").toString();
                shopVipDialog.put("shopName", StringUtils.substring(shopName, 0, 6));
                QueryWrapper<User> userWrapper = new QueryWrapper<>();
                userWrapper.lambda()
                        .eq(User::getVipShopId, user.getShopId())
                        .eq(User::getGrantStatus, 1);
                List<User> users = userService.list(userWrapper);
                shopVipDialog.put("userCount", 102789 + users.size());
                resultVO.put("shopVipDialog", shopVipDialog);
            }
        }
        stringRedisTemplate.delete(RedisKeyConstant.USER_LOGIN_FORBID_REPEAT + openId);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setData(resultVO);
        return response;
    }

    private Map<String, Object> getResultVO(String sid, String sceneStr, User user) {
        String token = TokenUtil.encryptToken(user.getId().toString(), user.getOpenId(), user.getCtime(), TokenUtil.APPLET);
        HashMap<String, Object> resultVO = new HashMap<>();
        resultVO.put("token", token);
        resultVO.put("userId", user.getId());
        resultVO.put("nickName", user.getNickName());
        resultVO.put("mobile", user.getMobile());
        resultVO.put("avatarUrl", user.getAvatarUrl());
        resultVO.put("shopId", user.getShopId());
        if (user.getShopId() == null) {
            resultVO.put("shopId", shopId);
        }
        resultVO.put("merchantId", user.getMerchantId());
        if (user.getMerchantId() == null) {
            resultVO.put("merchantId", merchantId);
        }
        resultVO.put("scene", user.getScene());
        if (StringUtils.isNotBlank(sid) && RegexUtils.StringIsNumber(sid)
                && StringUtils.isNotBlank(sceneStr) && RegexUtils.StringIsNumber(sceneStr)) {
            resultVO.put("scene", Integer.valueOf(sceneStr));
        }
        if (StringUtils.isNotBlank(sid) && RegexUtils.StringIsNumber(sid)
                && StringUtils.isNotBlank(sceneStr) && !RegexUtils.StringIsNumber(sceneStr)) {
            resultVO.put("scene", "");
        }
        if (StringUtils.isNotBlank(sid) && RegexUtils.StringIsNumber(sid)
                && StringUtils.isBlank(sceneStr)) {
            resultVO.put("scene", "");
        }
        return resultVO;
    }

    @Override
    public SimpleResponse registerOrUpdateMobile(String userId, String mobile) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        // 校验参数
        if (StringUtils.isBlank(mobile) || !RegexUtils.isMobileExact(mobile)
                || StringUtils.isBlank(userId) || !RegexUtils.StringIsNumber(userId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            response.setMessage("userId未设置 或 mobile格式不正确");
            return response;
        }
        Long uid = Long.valueOf(userId);
        return userService.registerOrUpdateMobile(uid, mobile);
    }

    /**
     * 获取绑定手机号验证码
     *
     * @param userId
     * @return
     */
    @Override
    public SimpleResponse mobileCode(String userId, String mobile) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (userId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("");
            return simpleResponse;
        }
        // 验证请求次数
        String mobileCodeNumKey = RedisKeyConstant.MOBILE_CODE_NUM + userId + ":" + mobile;
        String mobileCodeNumValue = stringRedisTemplate.opsForValue().get(mobileCodeNumKey);
        if (mobileCodeNumValue != null && Integer.valueOf(mobileCodeNumValue) >= SMS_CODE_LIMIT) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            simpleResponse.setMessage("您请求验证码次数过多");
            StringBuilder warnLog = new StringBuilder();
            warnLog.append("用户绑定手机号请求短信验证码过多，userId:").append(userId).append(",mobile:").append(mobile);
            log.warn(warnLog.toString());
            return simpleResponse;
        }
        // 目前没有替换手机号需求
        User user = userService.selectByPrimaryKey(Long.valueOf(userId));
        if (user.getMobile() != null && RegexUtils.isMobileExact(user.getMobile())) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("已经绑定过手机号");
            return simpleResponse;
        }
        Integer count = userService.countByMoblie(mobile);
        if (count >= 1) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO11);
            simpleResponse.setMessage("手机号已被注册");
            return simpleResponse;
        }
        /**
         * 手机码存储redis
         */
        String redisKey = RedisKeyConstant.MOBILE_CODE + userId + ":" + mobile;
        // 判断 手机号码 是否60秒之内重复发送
        String redisString = stringRedisTemplate.opsForValue().get(redisKey);
        SmsCodeRedis smsRedis = JSONObject.parseObject(redisString, SmsCodeRedis.class);
        if (smsRedis != null && DateUtil.getCurrentMillis() < Long.valueOf(smsRedis.getNow()) + DateUtil.ONE_MINUTE_MILLIS) {
            // 当前时间在预定时间之前，返回错误提示
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("稍后再次请求短信验证码");
            return simpleResponse;
        }
        // 生成短信验证码，6位
        String smsCode = RandomStrUtil.getRandomNum();
        // 短信验证码存入redis，过期时间5分钟key-->user:mobilecode:<userId>:<phone> value-->smsCode, now
        SmsCodeRedis login = new SmsCodeRedis();
        login.setNow(DateUtil.getCurrentMillis().toString());
        login.setSmsCode(smsCode);
        stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(login), 5, TimeUnit.MINUTES);
        // 调用大于短信接口(手机号，短信验证码)
        SendSmsResponse sendSmsResponse = dayuCaller.smsForIdValidate(mobile, smsCode);
        // 处理大于短信返回状态
        if (!StringUtils.equalsIgnoreCase("ok", sendSmsResponse.getCode())) {
            log.error("请求阿里大于失败, phone" + mobile);
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
            simpleResponse.setMessage("短信服务请求失败");
            return simpleResponse;
        }
        // 计数增加,一天有效期，即一天五次
        stringRedisTemplate.opsForValue().increment(mobileCodeNumKey, 1);
        stringRedisTemplate.expire(mobileCodeNumKey, 1, TimeUnit.DAYS);
        return simpleResponse;
    }

    /**
     * 绑定账号手机号
     *
     * @param userId
     * @param mobile
     * @param code
     * @return
     */
    @Override
    public SimpleResponse bindMobile(String userId, String mobile, String code) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (userId == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("");
            return simpleResponse;
        }
        if (StringUtils.isBlank(mobile) || !RegexUtils.isMobileExact(mobile)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("手机号输入有误");
            return simpleResponse;
        }
        if (StringUtils.isBlank(code) || !RegexUtils.StringIsNumber(code) || code.length() != 6) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("验证码输入有误");
            return simpleResponse;
        }
        /**
         * 手机码存储redis
         */
        String redisKey = RedisKeyConstant.MOBILE_CODE + userId + ":" + mobile;
        // 判断 手机号码 验证码已过期
        String redisString = stringRedisTemplate.opsForValue().get(redisKey);
        SmsCodeRedis smsCodeRedis = JSON.parseObject(redisString, SmsCodeRedis.class);
        if (smsCodeRedis == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("验证码过期，请重新获取");
            return simpleResponse;
        }
        if (!StringUtils.equalsIgnoreCase(smsCodeRedis.getSmsCode(), code)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("验证码不匹配");
            return simpleResponse;
        }
        return registerOrUpdateMobile(userId, mobile);
    }

    /**
     * 绑定微信授权手机号
     *
     * @param bindMobileReq 绑定用户手机号参数
     * @return 手机号
     */
    @Override
    public SimpleResponse bindPhoneNumber(BindMobileReq bindMobileReq) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (bindMobileReq == null) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("");
            return simpleResponse;
        }
        String userId = bindMobileReq.getUserId();
        String encryptedData = bindMobileReq.getEncryptedData();
        String code = bindMobileReq.getCode();
        String iv = bindMobileReq.getIv();
        if (StringUtils.isBlank(userId)
                || StringUtils.isBlank(encryptedData)
                || StringUtils.isBlank(code)
                || StringUtils.isBlank(iv)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("获取手机号数据有误，请重试");
            return simpleResponse;
        }
        /**
         * 解密
         */
        // 通过code获取用户唯一标识 OpenID 和 会话密钥 session_key
        String session = weixinLoginCaller.code2Session(code);
        if (StringUtils.isBlank(session) || "{}".equals(session)) {
            log.error("code2Session调用微信接口出错, 返回值为空");
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("微信接口出错");
            return simpleResponse;
        }
        // 是否有errorCode
        JSONObject sessionJO = JSON.parseObject(session);
        if (sessionJO.containsKey("errcode")) {
            log.error("code2Session调用微信接口出错");
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            simpleResponse.setMessage("微信接口报错");
            return simpleResponse;
        }
        String sessionKey = sessionJO.getString("session_key");
        String result = decodeEncryptedData(encryptedData, iv, sessionKey);
        if (StringUtils.isBlank(result)) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            simpleResponse.setMessage("获取手机号数据有误，请重试");
            return simpleResponse;
        }
        // 入库
        JSONObject jsonRst = JSON.parseObject(result);
        SimpleResponse simpleResponse1 = registerOrUpdateMobile(bindMobileReq.getUserId(), jsonRst.getString("purePhoneNumber"));
        if(simpleResponse1.error()){
            return simpleResponse1;
        }
        Map<String, Object> data = new HashMap<>();
        data.put("phoneNumber", jsonRst.getString("purePhoneNumber"));
        simpleResponse.setData(data);
        return simpleResponse;
    }

    // 解密
    private String decodeEncryptedData(String encryptedData, String iv, String sessionKey) {
        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decode(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv);
        try {
            byte[] decrypt = this.decrypt(keyByte, ivByte, dataByte);
            if (null != decrypt && decrypt.length > 0) {
                return new String(decrypt, "UTF-8");
            }

            // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
            byte[] resultByte = cipher.doFinal(dataByte);

            if (null != resultByte && resultByte.length > 0) {
                return new String(resultByte, "UTF-8");
            }

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidParameterSpecException
                | IllegalBlockSizeException | UnsupportedEncodingException | BadPaddingException
                | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchProviderException e) {
            log.info("解析异常：" + e);
        }
        return null;
    }

    public byte[] decrypt(byte[] sessionkey, byte[] iv, byte[] encryptedData) {
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
        byte[] data = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keySpec = new SecretKeySpec(sessionkey, "AES");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            data =  cipher.doFinal(encryptedData);
        }catch (Exception e){
            log.info("解析异常：" + e);

        }
        return data;
    }

}
