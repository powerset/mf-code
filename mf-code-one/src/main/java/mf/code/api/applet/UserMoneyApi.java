package mf.code.api.applet;

import mf.code.api.applet.service.UserMoneyService;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * mf.code.api.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月01日 14:27
 */

@RestController
@RequestMapping("/api/applet/userMoney")
public class UserMoneyApi {
    @Autowired
    private UserMoneyService userMoneyService;

    /***
     * 用户提现-企业付款(公众号支付)-弃用-新接口在user服务内
     * @param map
     * @return
     */
    @RequestMapping(path = "/createMktTransfersTradeOrder",method = RequestMethod.POST)
    public SimpleResponse createMktTransfersTradeOrder(@RequestBody Map<String,Object> map){
        return this.userMoneyService.createMktTransfers(map);
    }

    /***
     * 查询我的余额-弃用-新接口在user服务内
     * @param userID
     * @return
     */
    @RequestMapping(path = "/queryUserBalance",method = RequestMethod.GET)
    public SimpleResponse queryUserBalance(@RequestParam(name = "merchantId") Long merchantID,
                                           @RequestParam(name = "shopId") Long shopID,
                                           @RequestParam(name = "userId") Long userID){
        return this.userMoneyService.queryUserBalance(merchantID,shopID,userID);
    }

    /***
     * 查询我的提现记录
     * @param shopID
     * @param offset
     * @param size
     * @param type
     * @return
     */
    @RequestMapping(path = "/queryPresentOrderLog",method = RequestMethod.GET)
    public SimpleResponse queryPresentOrderLog(@RequestParam(name = "shopId") final Long shopID,
                                               @RequestParam(name = "userId") final Long userID,
                                               @RequestParam(name = "limit", required = false,defaultValue="10") int size,
                                               @RequestParam(name = "offset", required = false,defaultValue="0") int offset,
                                               @RequestParam(name = "type", required = false,defaultValue = "cash") String type){
        return this.userMoneyService.queryPresentOrderLog(shopID,userID,offset,size,type);
    }

    /***
     * 查询收入记录
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @return
     */
    @RequestMapping(path = "/queryIncomeLog",method = RequestMethod.GET)
    public SimpleResponse queryIncomeLog(@RequestParam(name = "shopId") final Long shopID,
                                         @RequestParam(name = "userId") final Long userID,
                                         @RequestParam(name = "limit", required = false,defaultValue="10") int size,
                                         @RequestParam(name = "offset", required = false,defaultValue="0") int offset){
        return this.userMoneyService.queryIncomeLog(shopID,userID,offset,size);
    }

    /***
     * 查询交易明细
     * @param merchantID
     * @param shopID
     * @param userID
     * @param size
     * @param offset
     * @return
     */
    @RequestMapping(path = "/queryTradeDetail",method = RequestMethod.GET)
    public SimpleResponse queryTradeDetail(@RequestParam(name = "merchantId") final Long merchantID,
                                           @RequestParam(name = "shopId") final Long shopID,
                                           @RequestParam(name = "userId") final Long userID,
                                           @RequestParam(name = "pageLastId",required = false) final Long pageLastID,
                                           @RequestParam(name = "limit", required = false,defaultValue="10") int size,
                                           @RequestParam(name = "offset", required = false,defaultValue="0") int offset){
        return this.userMoneyService.queryTradeDetail(merchantID,shopID,userID,offset,size,pageLastID);
    }
}
