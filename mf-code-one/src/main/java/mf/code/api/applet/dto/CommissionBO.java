package mf.code.api.applet.dto;

import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;

/**
 * mf.code.activity.repo.bo
 * Description: 佣金业务数据
 *
 * @author: gel
 * @date: 2019-02-14 14:12
 */
@Data
public class CommissionBO {
    /**
     * 佣金来源
     */
    @NonNull
    private Long fromUserId;
    /**
     * 佣金去处
     */
    private Long toUserId;
    /**
     * 总佣金
     */
    @NonNull
    private BigDecimal commissionDef;
    /**
     * 自己获得佣金数额
     */
    private BigDecimal commission;
    /**
     * 待上缴佣金税额
     */
    private BigDecimal tax;
    /**
     * 佣金税率
     */
    private BigDecimal rate;
    /**
     * 本人关卡描述
     */
    private String fromLevel;
    /**
     * 上级关卡描述
     */
    private String toLevel;

    /**
     * 本人完成关卡
     */
    private boolean fromFinish;
    /**
     * 上级完成关卡
     */
    private boolean toFinish;
}
