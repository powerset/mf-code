package mf.code.api.applet.v9.service;

import mf.code.activity.domain.applet.aggregateroot.AppletAssistActivity;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.one.constant.ActivityDetailStatusEnum;
import mf.code.one.dto.activitydetail.AssertUserDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v9.service
 * Description:
 *
 * @author: 百川
 * @date: 2019-07-02 15:33
 */
public interface FullReimbursementAboutService {

    /***
     * 获取活动的状态
     *
     * @param userId 用户编号
     * @param appletAssistActivity 小程序助力对象信息
     * @return
     */
    ActivityDetailStatusEnum findByActivityDetailStatusEnum(Long userId, AppletAssistActivity appletAssistActivity);

    /***
     * 用户第一次助力时，若该活动的前几人直接到账数目还未达到,存储到账redis
     *
     * @param appletAssistActivity
     */
    void saveCanCashRedisByFullReimbursement(Long userId, AppletAssistActivity appletAssistActivity, int fullReimbursementMoneyNum);

    /***
     * 查询报销活动的弹窗信息
     * @param userId
     * @param activityDef
     * @param activity
     * @return
     */
    List queryFullReimbursementDialog(Long userId, ActivityDef activityDef, Activity activity, int type);

    BigDecimal getFullReimbursementMoney(Long mid, Long sid, Long userId, Long activityId, Integer hitPerDraw);

    /***
     * 查询报销活动已经助力到账的人数
     * @param userId
     * @param mid
     * @param sid
     * @return
     */
    int getFullReimbursementMoneyNum(Long mid, Long sid, Long userId, Long activityId);

    /***
     * 查找报销活动进行中的
     *
     * @param shopId
     * @param userId
     * @return
     */
    Map<String, Object> fullReimbursementPopup(Long shopId, Long userId);

    /***
     * 获取活动助力信息--后期这方法 被百川取缔掉
     * @param activityId
     * @return
     */
    AppletMybatisPageDto<AssertUserDto> getAssistListPage(Long activityId, int offset, int size);
}
