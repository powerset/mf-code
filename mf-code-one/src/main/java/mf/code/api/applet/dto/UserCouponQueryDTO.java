package mf.code.api.applet.dto;

import lombok.Data;

import java.util.List;

/**
 * mf.code.api.applet.dto
 * Description:
 *
 * @author gel
 * @date 2019-07-18 14:32
 */
@Data
public class UserCouponQueryDTO {

    private Integer type;
    private Integer status;
    private Long userId;
    private Long shopId;
    private Long goodId;
    private Long skuId;
    private Integer offset;
    private Integer limit;
}
