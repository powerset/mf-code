package mf.code.api.applet.v8.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mf.code.activity.constant.ActivityDefTypeEnum;
import mf.code.activity.constant.UserCouponTypeEnum;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.service.ActivityDefService;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.applet.v8.dto.MyMinerV8Dto;
import mf.code.api.applet.v8.dto.NewbieTask;
import mf.code.api.applet.v8.service.CheckpointTaskSpaceAboutService;
import mf.code.api.applet.v8.service.MinerAboutService;
import mf.code.api.applet.v8.service.UserCouponAboutDTO;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.utils.DateUtil;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.one.constant.UserCouponStatusEnum;
import mf.code.one.vo.UserCouponAboutVO;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.ucoupon.repo.po.UserCoupon;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.user.constant.UserPubJoinTypeEnum;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.v8.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 10:40
 */
@Service
public class MinerAboutServiceImpl implements MinerAboutService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private CheckpointTaskSpaceAboutService checkpointTaskSpaceAboutService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;

    @Value("${checkpoint.time}")
    private String userCouponTime;

    @Override
    public List<MyMinerV8Dto.DiaLogInfo> getCheckpointDialog(Long shopId, User user, UserPubJoin userPubJoin, BigDecimal allMinerTotalScottare, List<CommonDict> commonDicts) {
        Long userId = user.getId();
        List<MyMinerV8Dto.DiaLogInfo> diaLogInfos = new ArrayList<>();
        //矿工们为上级产生的收益
        String checkpoint_minerprovide_profit_key = RedisKeyConstant.CHECKPOINT_MINERPROVIDE_PROFIT_TOTAL + userId;
        //用户是否通关(value=json字符串{"value":"不屈黑铁","money":0,"name":0})
        String checkpoint_finish_key = RedisKeyConstant.CHECKPOINT_FINISH_PLATFORM + userId;
        //一段时间任务所得详情
        String usertaskFinishKey = RedisKeyConstant.CHECKPOINT_USERTASK_FINISH + shopId + ":" + userId;

        //矿工们给上级又增加的缴税总收益
        String addScottareProfit = this.stringRedisTemplate.opsForValue().get(checkpoint_minerprovide_profit_key);
        //用户是否通关(value=json字符串{"value":"不屈黑铁","money":0,"name":0})
        String userCheckPointFinish = this.stringRedisTemplate.opsForValue().get(checkpoint_finish_key);
        //一段时间内的任务奖金
        String userTaskFinish = stringRedisTemplate.opsForValue().get(usertaskFinishKey);

        if (StringUtils.isNotBlank(userCheckPointFinish)) {
            //是否通关
            MyMinerV8Dto.DiaLogInfo diaLogInfo = new MyMinerV8Dto.DiaLogInfo();
            diaLogInfo.setType(1);
            diaLogInfo.fromUserCheckPointFinish(userCheckPointFinish);
            diaLogInfos.add(diaLogInfo);
            this.stringRedisTemplate.delete(checkpoint_finish_key);
        }
        if (user.getRole() == 1 && StringUtils.isNotBlank(addScottareProfit)) {
            MyMinerV8Dto.DiaLogInfo diaLogInfo = new MyMinerV8Dto.DiaLogInfo();
            diaLogInfo.setType(2);
            diaLogInfo.fromAddScottareProfit(addScottareProfit);
            diaLogInfos.add(diaLogInfo);
            this.stringRedisTemplate.delete(checkpoint_minerprovide_profit_key);
        }
        if (StringUtils.isNotBlank(userTaskFinish)) {
            MyMinerV8Dto.DiaLogInfo diaLogInfo = new MyMinerV8Dto.DiaLogInfo();
            diaLogInfo.setType(3);
            diaLogInfo.fromUserCheckPointTask(userTaskFinish);
            diaLogInfos.add(diaLogInfo);
            this.stringRedisTemplate.delete(usertaskFinishKey);
        }
        return diaLogInfos;
    }

    /***
     * 获取累计缴税
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public List<UserPubJoin> getUserPubJoin(Long shopId, Long userId, Long bossUserId) {
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
        ;
        if (shopId != null && shopId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("shop_id", shopId));
        }
        if (userId != null && userId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("sub_uid", userId));
        }
        if (bossUserId != null && bossUserId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("user_id", bossUserId));
        }
        return this.userPubJoinService.list(userPubJoinQueryWrapper);
    }

    /***
     * 获取下级人数
     * @param shopId
     * @param userId
     * @return
     */
    @Override
    public List<UserPubJoin> getUserPubJoinMinerNum(Long shopId, Long userId, Long bossUserId) {
        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
        userPubJoinQueryWrapper.lambda()
                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.SHOPPINGMALL.getCode())
        ;
        if (shopId != null && shopId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("shop_id", shopId));
        }
        if (userId != null && userId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("sub_uid", userId));
        }
        if (bossUserId != null && bossUserId > 0) {
            userPubJoinQueryWrapper.and(params -> params.eq("user_id", bossUserId));
        }
        return this.userPubJoinService.list(userPubJoinQueryWrapper);
    }

    /***
     * 完成新手任务的进度情况
     * @param user
     * @return
     */
    @Override
    public List<NewbieTask> queryFinishProgressInfo(User user) {
        Long vipShopId = user.getVipShopId();
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(vipShopId);
        if (merchantShop == null) {
            return new ArrayList<>();
        }
        Long vipMerchantId = merchantShop.getMerchantId();

        //是否存在新手任务
        List<ActivityDef> activityDefs = activityDefService.queryByQueryWrapper(vipMerchantId, vipShopId, ActivityDefTypeEnum.NEWBIE_TASK.getCode());
        if (CollectionUtils.isEmpty(activityDefs)) {
            return new ArrayList<>();
        }

        List<Long> activityDefIds = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            if (!activityDef.getId().equals(activityDef.getParentId()) && activityDefIds.indexOf(activityDef.getId()) == -1) {
                activityDefIds.add(activityDef.getId());
            }
        }
        Map<Long, List<UserCoupon>> userCouponsMap = checkpointTaskSpaceAboutService.queryActivityDefInfo(vipMerchantId, vipShopId, user.getId(), activityDefIds,
                UserCouponAboutVO.addNewbieTaskType());

        //该用户是否做过
        Map<Long, List<UserCoupon>> existUserCouponsMap = checkpointTaskSpaceAboutService.queryActivityDefInfo(vipMerchantId, vipShopId, user.getId(), null,
                UserCouponAboutVO.addNewbieTaskType());
        boolean isDone = false;
        if (!CollectionUtils.isEmpty(existUserCouponsMap)) {
            for (Map.Entry<Long, List<UserCoupon>> entry : existUserCouponsMap.entrySet()) {
                List<UserCoupon> coupons = entry.getValue();
                UserCoupon userCoupon = coupons.get(0);
                if (userCoupon.getType() != UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode()) {
                    isDone = true;
                }
            }
        }

        List<NewbieTask> newbieTaskList = new ArrayList<>();
        for (ActivityDef activityDef : activityDefs) {
            if (activityDef.getId().equals(activityDef.getParentId())) {
                if (activityDef.getStock() <= 0 && !isDone) {
                    return new ArrayList<>();
                }
                continue;
            }
            //扣除税率的佣金
            CommissionBO commissionBO = commissionService.commissionCalculate(user.getId(), activityDef.getCommission());
            NewbieTask newbieTask = new NewbieTask();
            newbieTask.setType(activityDef.getType());
            List<UserCoupon> userCoupons = userCouponsMap.get(activityDef.getId());
            if (!CollectionUtils.isEmpty(userCoupons)) {
                for (UserCoupon userCoupon : userCoupons) {
                    if (UserCouponTypeEnum.NEWBIE_TASK_BONUS.getCode() == userCoupon.getType()) {
                        newbieTask.setFinish(true);
                    }
                    if (UserCouponTypeEnum.NEWBIE_TASK_REDPACK.getCode() == userCoupon.getType()) {
                        newbieTask.setFinish(true);
                    }
                    if (UserCouponTypeEnum.NEWBIE_TASK_OPENREDPACK_AMOUNT.getCode() == userCoupon.getType()) {
                        newbieTask.setFinish(true);
                    }
                }
            }
            newbieTask.setActivityDefId(activityDef.getId());
            newbieTask.setRebate(commissionBO.getCommission().toString());
            if (activityDef.getType() == ActivityDefTypeEnum.OPEN_RED_PACKET.getCode()) {
                newbieTask.setRebate(activityDef.getCommission().toString());
            }
            newbieTask.setMerchantId(vipMerchantId);
            newbieTask.setShopId(vipShopId);
            newbieTaskList.add(newbieTask);
        }
        return newbieTaskList;
    }

    /***
     * 获取矿工信息
     * @param userIds
     * @return
     */
    @Override
    public List<User> getUser(List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return new ArrayList<>();
        }
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .in(User::getId, userIds)
        ;
        return userService.list(wrapper);
    }

    /***
     * 获取矿工页面详细任务
     * @param merchantId
     * @param shopId
     * @param userId
     * @param subUserIds
     * @param offset
     * @param size
     * @return
     */
    @Override
    public IPage<UserCoupon> getUserCoupon(Long merchantId, Long shopId, Long userId, List<Long> subUserIds, int offset, int size) {
        //获取页码
        int pageNum = offset / size + 1;
        Page<UserCoupon> page = new Page<>(pageNum, size);
        Date date = DateUtil.parseDate(userCouponTime, null, "yyyy-MM-dd HH:mm:ss");
        QueryWrapper<UserCoupon> userCouponQueryWrapper = new QueryWrapper<>();
        userCouponQueryWrapper.lambda()
                .eq(UserCoupon::getMerchantId, merchantId)
                .eq(UserCoupon::getShopId, shopId)
                .in(UserCoupon::getType, UserCouponAboutDTO.checkPointMinerDetailUserCouponTyeps())
                .eq(UserCoupon::getStatus, UserCouponStatusEnum.RECEIVIED.getCode())
                .ge(UserCoupon::getCommission, BigDecimal.ZERO)
                .orderByDesc(UserCoupon::getId)
        ;
        if (date != null) {
            userCouponQueryWrapper.and(params -> params.ge("ctime", date));
        }
        if (!CollectionUtils.isEmpty(subUserIds)) {
            userCouponQueryWrapper.and(params -> params.in("user_id", subUserIds));
        }
        if (userId != null && userId > 0) {
            userCouponQueryWrapper.and(params -> params.eq("user_id", userId));
        }
        IPage<UserCoupon> userCouponIPage = userCouponService.page(page, userCouponQueryWrapper);
        return userCouponIPage;
    }
}
