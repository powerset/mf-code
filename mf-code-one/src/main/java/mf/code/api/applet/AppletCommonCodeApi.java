package mf.code.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.repo.dto.RedirectDTO;
import mf.code.common.service.CommonCodeService;
import mf.code.common.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * mf.code.api.applet
 * Description: 二维码接口
 *
 * @author gel
 * @date 2019-06-21 14:01
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/code")
public class AppletCommonCodeApi {

    @Autowired
    private CommonCodeService commonCodeService;

    @GetMapping(value = "")
    public String getLiveCode(HttpServletRequest httpServletRequest,
                            HttpServletResponse httpServletResponse,
                            @RequestParam(value = "code") String code) {
        boolean needCalc = true;
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null && cookies.length != 0) {
            for (Cookie cookie : cookies) {
                if (code.equals(cookie.getName())) {
                    needCalc = false;
                }
            }
        }
        RedirectDTO redirectDTO = commonCodeService.queryLiveCodeByCodeNo(code, needCalc);
        if (redirectDTO.getCanRedirect()) {
            try {
                Cookie cookie = new Cookie(code, DateUtil.getCurrentMillis().toString());
                cookie.setMaxAge(7 * 24 * 60 * 60);
                cookie.setPath("/");
                httpServletResponse.addCookie(cookie);
                httpServletResponse.sendRedirect(redirectDTO.getTargetUrl());
            } catch (IOException e) {
                log.error("转发失败", e);
            }
        }
        return redirectDTO.getTargetUrl();
    }

}
