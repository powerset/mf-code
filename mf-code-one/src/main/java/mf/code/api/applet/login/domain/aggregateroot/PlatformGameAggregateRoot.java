package mf.code.api.applet.login.domain.aggregateroot;

import lombok.Data;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-25 下午1:51
 */
@Data
public class PlatformGameAggregateRoot {
	protected Long userId;
	protected Integer userPubJoinType;
	protected Long shopId;
	// 以上三种类型组成 唯一标识
}
