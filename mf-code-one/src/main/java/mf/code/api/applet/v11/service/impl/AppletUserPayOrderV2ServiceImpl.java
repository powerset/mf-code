package mf.code.api.applet.v11.service.impl;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.v11.service.AppletUserPayOrderV2Service;
import mf.code.api.applet.v11.upayorder.UpayOrderDelegate;
import mf.code.common.RedisKeyConstant;
import mf.code.common.caller.wxpay.WXPayUtil;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.one.dto.UserOrderReqDTO;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayWxOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.applet.v11.service.impl
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月25日 17:31
 */
@Service
@Slf4j
public class AppletUserPayOrderV2ServiceImpl implements AppletUserPayOrderV2Service {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private WeixinPayService weixinPayService;

    /***
     * 支付  TODO:防重处理
     *
     * @param reqDTO
     * @return
     */
    @Override
    public SimpleResponse createUpayWxOrder(UserOrderReqDTO reqDTO) {
        UpayOrderDelegate upayOrderDelegate = new UpayOrderDelegate();
        SimpleResponse simpleResponse = upayOrderDelegate.create(reqDTO);
        return simpleResponse;
    }

    @Override
    public SimpleResponse applyRefundUpayWxOrder() {
        return null;
    }

    /***
     * 支付回调
     *
     * @param orderNo 订单编号
     * @param transactionId 交易流水
     * @param totalFee 支付金额
     * @param paymentTime 支付时间
     * @param payOpenId 支付人标识
     * @return
     */
    @Override
    public String wxPayCallback(String orderNo, String transactionId, BigDecimal totalFee, Date paymentTime, String payOpenId) {
        //redis去重 try finally
        String redisKey = RedisKeyConstant.FORBID_KEY + orderNo;
        try {
            UpayWxOrder order = upayWxOrderService.selectByOrderNoPlus(orderNo);
            if (order == null || order.getTotalFee() == null) {
                log.error("该订单不存在or订单金额不存在");
                return WXPayUtil.resp();
            }
            if (totalFee.subtract(order.getTotalFee()).compareTo(BigDecimal.ZERO) != 0) {
                return "支付失败-下单金额跟回调的金额不匹配";
            }

            //更新状态
            //插入成功，设置过期时间
            stringRedisTemplate.expire(redisKey, 3, TimeUnit.SECONDS);

            order.setStatus(OrderStatusEnum.ORDERED.getCode());
            order.setTotalFee(totalFee.setScale(2, BigDecimal.ROUND_DOWN));
            order.setTradeId(transactionId);
            order.setPaymentTime(paymentTime);
            order.setNotifyTime(new Date());
            order.setUtime(new Date());
            boolean i = upayWxOrderService.updateById(order);
            log.info("订单更新成功与否：{} orderId:{}", i, order.getId());

            UpayOrderDelegate upayOrderDelegate = new UpayOrderDelegate();
            SimpleResponse simpleResponse = upayOrderDelegate.wxPayCallBackBiz(order);
            if (!simpleResponse.error()) {
                return WXPayUtil.resp();
            }
        } finally {
            stringRedisTemplate.delete(redisKey);
        }
        return null;
    }

    @Override
    public String wxRefundCallback(String orderNo, String refundOrderNo, BigDecimal totalFee, BigDecimal settlementRefundFee, String transactionId) {
        return null;
    }

    /***
     * 查询订单的状态
     *
     * @param orderKeyID
     * @return
     */
    @Override
    public SimpleResponse queryOrderStatus(Long orderKeyID) {
        SimpleResponse d = new SimpleResponse();
        //入参安全性验证
        if (orderKeyID == null || orderKeyID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        UpayWxOrder upayWxOrder = this.upayWxOrderService.selectByKeyId(orderKeyID);
        if (upayWxOrder == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该订单不存在");
            return d;
        }
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("status", upayWxOrder.getStatus());
        //下单中时,调用微信平台接口查验
        if (upayWxOrder.getStatus() == OrderStatusEnum.FAILED.getCode() || upayWxOrder.getStatus() == OrderStatusEnum.TIMEOUT.getCode()) {
            respMap.put("status", OrderStatusEnum.FAILED.getCode());
        }

        if (upayWxOrder.getStatus() == OrderStatusEnum.ORDERING.getCode() && System.currentTimeMillis() - upayWxOrder.getCtime().getTime() >= 2 * 60 * 1000) {
            respMap.put("status", OrderStatusEnum.FAILED.getCode());

            Map<String, Object> queryParams = weixinPayService.getQueryOrderParams(upayWxOrder.getOrderNo(), WeixinPayConstants.PAYSCENE_APPLET_APPID);
            Map<String, Object> respQueryOrderMap = weixinPayService.queryOrder(queryParams);
            log.info("查询微信该订单信息:{}", respQueryOrderMap);
            boolean returnCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("return_code") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("return_code").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("return_code"));
            boolean resultCodeSuccess = respQueryOrderMap != null && respQueryOrderMap.get("result_code") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("result_code").toString()) &&
                    "SUCCESS".equals(respQueryOrderMap.get("result_code"));
            boolean tradeStateSuccess = respQueryOrderMap != null && respQueryOrderMap.get("trade_state") != null &&
                    StringUtils.isNotBlank(respQueryOrderMap.get("trade_state").toString());
            if (returnCodeSuccess && resultCodeSuccess && tradeStateSuccess) {
                /***
                 * SUCCESS—支付成功 REFUND—转入退款 NOTPAY—未支付 CLOSED—已关闭 REVOKED—已撤销（刷卡支付）USERPAYING--用户支付中 PAYERROR--支付失败(其他原因，如银行返回失败)
                 */
                upayWxOrder.setStatus(OrderStatusEnum.ORDERED.getCode());
                upayWxOrder.setTradeId(respQueryOrderMap.get("transaction_id").toString());
                upayWxOrder.setPaymentTime(DateUtil.parseDate(respQueryOrderMap.get("time_end").toString(), null, "yyyyMMddHHmmss"));
                upayWxOrder.setUtime(new Date());
                this.upayWxOrderService.updateByPrimaryKeySelective(upayWxOrder);

                respMap.put("status", OrderStatusEnum.ORDERED.getCode());
            }
        }

        UpayOrderDelegate upayOrderDelegate = new UpayOrderDelegate();
        Map map = upayOrderDelegate.queryStatusBiz(upayWxOrder);
        respMap.putAll(map);
        d.setData(respMap);
        return d;
    }
}
