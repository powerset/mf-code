package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.ActivityTypeEnum;
import mf.code.activity.repo.po.Activity;
import mf.code.activity.repo.po.ActivityDef;
import mf.code.activity.repo.po.ActivityOrder;
import mf.code.activity.service.ActivityDefService;
import mf.code.activity.service.ActivityOrderService;
import mf.code.activity.service.ActivityService;
import mf.code.api.applet.service.MyPageService;
import mf.code.api.feignclient.CommentAppService;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.activity.constant.UserActivityTypeEnum;
import mf.code.activity.constant.UserTaskTypeEnum;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.repo.po.MerchantShopCoupon;
import mf.code.merchant.service.MerchantShopCouponService;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserPubJoinService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.upay.repo.enums.GoodsOrderStatusEnum;
import mf.code.upay.repo.enums.GoodsOrderTypeEnum;
import mf.code.upay.repo.po.UpayBalance;
import mf.code.upay.service.OrderService;
import mf.code.upay.service.UpayBalanceService;
import mf.code.user.repo.po.User;
import mf.code.user.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年11月02日 17:26
 */
@Service
@Slf4j
public class MyPageServiceImpl implements MyPageService {
    @Autowired
    private UserService userService;
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private MerchantShopCouponService merchantShopCouponService;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private ActivityOrderService activityOrderService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private UserPubJoinService userPubJoinService;
    @Autowired
    private CommonDictService commonDictService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CommentAppService commentAppService;


    /***
     * 小程序端-我的页面信息展现
     * @param userID
     * @return
     */
    @Override
    public SimpleResponse queryMypage(Long userID, Long shopID) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> respMap = new HashMap<String, Object>();
        if (userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        //用户信息
        User user = this.userService.selectByPrimaryKey(userID);
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (user == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该用户不存在");
            return d;
        }

        d.setData(this.queryMyPageInfo(user, shopID, merchantShop.getMerchantId()));
        return d;
    }

    /***
     * 获取我的卡券
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMyShopCoupon(Long userID, Long shopID, int offset, int size) {
        SimpleResponse d = new SimpleResponse();
        if (userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        //分页查询结果
        List<UserActivity> userActivities = this.userActivityService.pageListUserActivity(
                this.queryUserActivityParams(userID, shopID, 3, null, new Date(), null, null, offset, size, null));
        //查询总数
        int countUserActivities = this.userActivityService.countUserActivity(
                this.queryUserActivityParams(userID, shopID, 3, null, new Date(), null, null, null, null, null));
        //查询MerchantShopCoupon
        List<Long> shopCouponIDs = new ArrayList<Long>();
        for (UserActivity userActivity : userActivities) {
            if (shopCouponIDs.indexOf(userActivity.getMerchantShopCouponId()) == -1) {
                shopCouponIDs.add(userActivity.getMerchantShopCouponId());
            }
        }
        Map<Long, MerchantShopCoupon> merchantShopCouponMap = this.queryMapShopCounponId(userID, userActivities, this.queryMerchantShopCouponParams(shopCouponIDs, null, null, null));
        Map<Long, Activity> activityMap = this.queryActivityInfo(userID, userActivities, null);
        List<Object> list = new ArrayList<>();
        for (UserActivity userActivity : userActivities) {
            Activity activity = activityMap.get(userActivity.getActivityId());
            MerchantShopCoupon merchantShopCoupon = merchantShopCouponMap.get(userActivity.getMerchantShopCouponId());
            if (activity == null || merchantShopCoupon == null) {
                continue;
            }
            //有效的优惠券
            list.add(this.addShopCouponRespMap(merchantShopCoupon.getAmount(), merchantShopCoupon.getName(), merchantShopCoupon.getTpwd(),
                    DateFormatUtils.format(merchantShopCoupon.getDisplayStartTime(), "yyyy.MM.dd"),
                    DateFormatUtils.format(merchantShopCoupon.getDisplayEndTime(), "yyyy.MM.dd"), null));
        }

        d.setData(this.returnInfoObject(offset, size, countUserActivities, list));
        return d;
    }

    /***
     * 中奖纪录
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMyAwardLogV2(Long userID, Long shopID, int offset, int size) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> resp = new HashMap<>();
        if (userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }

        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该店铺不存在");
            return d;
        }

        // 分店铺中奖记录类型
        List<Integer> shopTypes = new ArrayList<>();
        shopTypes.add(UserTaskTypeEnum.MCH_PLAN.getCode());
        shopTypes.add(UserTaskTypeEnum.ASSIST_START.getCode());
        shopTypes.add(UserTaskTypeEnum.NEW_MAN_JOIN.getCode());
        shopTypes.add(UserTaskTypeEnum.NEW_MAN_START.getCode());
        shopTypes.add(UserTaskTypeEnum.ORDER_BACK_JOIN.getCode());
        shopTypes.add(UserTaskTypeEnum.ORDER_BACK_START.getCode());
        shopTypes.add(UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode());
        shopTypes.add(UserTaskTypeEnum.ORDER_BACK_START_V2.getCode());
        shopTypes.add(UserTaskTypeEnum.ASSIST_START_V2.getCode());
        // 全平台中奖记录类型
        List<Integer> platformTypes = new ArrayList<>();
        platformTypes.add(UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode());
        // 加工查询条件
        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", merchantShop.getMerchantId());
        userTaskParams.put("shopId", shopID);
        userTaskParams.put("userId", userID);
        userTaskParams.put("shopTypes", shopTypes);
        userTaskParams.put("platformTypes", platformTypes);
        //-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功
        userTaskParams.put("statusList", Arrays.asList(0, 1, -2, 2, 3));
        int countAwardNum = this.userTaskService.countUserTaskShopAndPlatform(userTaskParams);
        // ${order} ${direct}
        userTaskParams.put("order", "ctime");
        userTaskParams.put("direct", "desc");
        userTaskParams.put("offset", offset);
        userTaskParams.put("limit", size);
        List<UserTask> userTasks = this.userTaskService.queryShopAndPlatform(userTaskParams);
        if (userTasks == null || userTasks.size() == 0) {
            d.setData(this.returnInfoObject(offset, size, countAwardNum, new ArrayList<>()));
            return d;
        }
        List<Long> goodsIDs = new ArrayList<>();
        List<Long> activityIDs = new ArrayList<>();
        Set<Long> productSkuIdSet = new HashSet<>();
        for (UserTask userTask : userTasks) {

            if (activityIDs.indexOf(userTask.getActivityId()) == -1) {
                activityIDs.add(userTask.getActivityId());
            }

            if (userTask.getType() == UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
                productSkuIdSet.add(userTask.getGoodsId());
                continue;
            }

            if (goodsIDs.indexOf(userTask.getGoodsId()) == -1) {
                goodsIDs.add(userTask.getGoodsId());
            }
        }

        List<Long> productSkuIdList = new ArrayList<>(productSkuIdSet);
        Map<Long, ProductEntity> productEntityMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(productSkuIdList)) {
            List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
            if (!CollectionUtils.isEmpty(productEntities)) {
                for (ProductEntity productEntity : productEntities) {
                    productEntityMap.put(productEntity.getSkuId(), productEntity);
                }
            }
        }


        Map<Long, Activity> activityMap = this.queryActivityInfo(userID, null, activityIDs);
        Map<Long, Goods> goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        List<Object> list = new ArrayList<>();
        for (UserTask userTask : userTasks) {
            Activity activity = activityMap.get(userTask.getActivityId());
            Goods goods = goodsMap.get(userTask.getGoodsId());

            String displayGoodsName = "";
            Long goodsId = 0L;
            String picUrl = "";

            if (goods != null) {
                displayGoodsName = goods.getXxbtopTitle();
                goodsId = goods.getId();
                picUrl = goods.getPicUrl();
            }
            ProductEntity productEntity = productEntityMap.get(userTask.getGoodsId());
            if (productEntity != null) {
                displayGoodsName = productEntity.getProductTitle();
                goodsId = productEntity.getId();
                picUrl = JSON.parseArray(productEntity.getMainPics()).get(0).toString();
            }

            if (StringUtils.isBlank(displayGoodsName)) {
                log.error("<<<<<<<< 紧急警告：任务编号{}的活动不存在！", userTask.getId());
                continue;
            }

            Map map = new HashMap();
            map.put("picUrl", this.ossCaller.cdnReplace(picUrl));
            map.put("taskId", userTask.getId());
            map.put("activityType", activity.getType());
            map.put("title", displayGoodsName);
            map.put("goodsId", goodsId);
            map.put("startTime", activity != null && activity.getStartTime() != null ? DateFormatUtils.format(activity.getStartTime(), "yyyy.MM.dd HH:mm") : "");
            map.put("activityId", userTask.getActivityId() == null ? 0L : userTask.getActivityId());
            map.put("userId", userTask.getUserId());
            //-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功
            map.put("status", userTask.getStatus());//返回一个状态，当==3时，已领取，其余未领取
            map.put("type", this.getType(userTask));
            //常量值：用于前端获得红包弹窗的组件做区分
            map.put("isDialog", true);
            list.add(map);
        }
        d.setData(this.returnInfoObject(offset, size, countAwardNum, list));
        return d;
    }

    /***
     * 中奖纪录
     * @param userID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMyAwardLog(Long userID, Long shopID, int offset, int size) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> resp = new HashMap<String, Object>();
        if (userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }

        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该店铺不存在");
            return d;
        }

        Map<String, Object> userTaskParams = new HashMap<String, Object>();
        userTaskParams.put("merchantId", merchantShop.getMerchantId());
        userTaskParams.put("shopId", shopID);
        userTaskParams.put("userId", userID);
        userTaskParams.put("types", Arrays.asList(
                UserTaskTypeEnum.MCH_PLAN.getCode(),
                UserTaskTypeEnum.ASSIST_START.getCode(),
                UserTaskTypeEnum.NEW_MAN_JOIN.getCode(),
                UserTaskTypeEnum.NEW_MAN_START.getCode(),
                UserTaskTypeEnum.ORDER_BACK_JOIN.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START.getCode(),
                UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode(),
                UserTaskTypeEnum.ORDER_BACK_JOIN_V2.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START_V2.getCode(),
                UserTaskTypeEnum.ASSIST_START_V2.getCode()
        ));
        //-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功
        userTaskParams.put("statusList", Arrays.asList(0, 1, -2, 2, 3));
        int countAwardNum = this.userTaskService.countUserTask(userTaskParams);
        userTaskParams.put("offset", offset);
        userTaskParams.put("limit", size);
        List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
        if (userTasks == null || userTasks.size() == 0) {
            d.setData(this.returnInfoObject(offset, size, countAwardNum, new ArrayList<>()));
            return d;
        }
        List<Long> goodsIDs = new ArrayList<Long>();
        List<Long> activityIDs = new ArrayList<Long>();
        Set<Long> productSkuIdSet = new HashSet<>();
        for (UserTask userTask : userTasks) {

            if (activityIDs.indexOf(userTask.getActivityId()) == -1) {
                activityIDs.add(userTask.getActivityId());
            }

            if (userTask.getType() == UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
                productSkuIdSet.add(userTask.getGoodsId());
                continue;
            }

            if (goodsIDs.indexOf(userTask.getGoodsId()) == -1) {
                goodsIDs.add(userTask.getGoodsId());
            }
        }

        List<Long> productSkuIdList = new ArrayList<>(productSkuIdSet);
        Map<Long, ProductEntity> productEntityMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(productSkuIdList)) {
            List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
            if (!CollectionUtils.isEmpty(productEntities)) {
                for (ProductEntity productEntity : productEntities) {
                    productEntityMap.put(productEntity.getSkuId(), productEntity);
                }
            }
        }


        Map<Long, Activity> activityMap = this.queryActivityInfo(userID, null, activityIDs);
        Map<Long, Goods> goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        List<Object> list = new ArrayList<>();
        for (UserTask userTask : userTasks) {
            Activity activity = activityMap.get(userTask.getActivityId());
            Goods goods = goodsMap.get(userTask.getGoodsId());

            String displayGoodsName = "";
            Long goodsId = 0L;
            String picUrl = "";

            if (goods != null) {
                displayGoodsName = goods.getXxbtopTitle();
                goodsId = goods.getId();
                picUrl = goods.getPicUrl();
            }
            ProductEntity productEntity = productEntityMap.get(userTask.getGoodsId());
            if (productEntity != null) {
                displayGoodsName = productEntity.getProductTitle();
                goodsId = productEntity.getId();
                picUrl = JSON.parseArray(productEntity.getMainPics()).get(0).toString();
            }

            if (StringUtils.isBlank(displayGoodsName)) {
                log.error("<<<<<<<< 紧急警告：任务编号{}的活动不存在！", userTask.getId());
                continue;
            }

            Map map = new HashMap();
            map.put("picUrl", this.ossCaller.cdnReplace(picUrl));
            map.put("taskId", userTask.getId());
            map.put("activityType", activity.getType());
            map.put("title", displayGoodsName);
            map.put("goodsId", goodsId);
            map.put("startTime", activity != null && activity.getStartTime() != null ? DateFormatUtils.format(activity.getStartTime(), "yyyy.MM.dd HH:mm") : "");
            map.put("activityId", userTask.getActivityId() == null ? 0L : userTask.getActivityId());
            map.put("userId", userTask.getUserId());
            //-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功
            map.put("status", userTask.getStatus());//返回一个状态，当==3时，已领取，其余未领取
            map.put("type", this.getType(userTask));
            //常量值：用于前端获得红包弹窗的组件做区分
            map.put("isDialog", true);
            list.add(map);
        }
        d.setData(this.returnInfoObject(offset, size, countAwardNum, list));
        return d;
    }

    /***
     *
     * @param userTask
     * @return type 1:奖品类 2:红包任务(收藏加购)
     */
    private Integer getType(UserTask userTask) {
        int type = 0;
        //红包
        if (Arrays.asList(UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()).contains(userTask.getType())) {
            type = 2;
        } else if (Arrays.asList(
                UserTaskTypeEnum.MCH_PLAN.getCode(),
                UserTaskTypeEnum.ASSIST_START.getCode(),
                UserTaskTypeEnum.NEW_MAN_JOIN.getCode(),
                UserTaskTypeEnum.NEW_MAN_START.getCode(),
                UserTaskTypeEnum.PLATFORM_ORDER_BACK.getCode(),
                UserTaskTypeEnum.ORDER_BACK_JOIN.getCode(),
                UserTaskTypeEnum.ORDER_BACK_START.getCode()).contains(userTask.getType())) {
            type = 1;//奖品(早期的，只是审核截图)
        } else {
            type = 3;//人工审核奖品(包含tb订单号)
        }
        return type;
    }

    /***
     * 返回信息结果赋值
     * @param offset
     * @param size
     * @param total
     * @param list
     * @return
     */
    private Map<String, Object> returnInfoObject(int offset, int size, int total, List<Object> list) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("offset", offset);
        respMap.put("limit", size);
        respMap.put("total", total);
        respMap.put("content", list);
        return respMap;
    }

    /***
     * 我的参与活动任务V2
     * @param userID
     * @param offset
     * @param size
     * @param type
     * @return
     */
    @Override
    public SimpleResponse queryMyJoinTaskV2(Long userID, Long shopID, int offset, int size, String type) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> resp = new HashMap<>();
        if (userID == null || userID == 0 || shopID == null || shopID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该店铺不存在");
            return d;
        }
        // 分店铺展示的类型
        List<Integer> shopTypes = new ArrayList<>();
        shopTypes.add(UserActivityTypeEnum.ASSIST.getCode());
        shopTypes.add(UserActivityTypeEnum.ASSIST_V2.getCode());
        shopTypes.add(UserActivityTypeEnum.MCH_PLAN.getCode());
        shopTypes.add(UserActivityTypeEnum.NEW_MAN_JOIN.getCode());
        shopTypes.add(UserActivityTypeEnum.NEW_MAN_START.getCode());
        shopTypes.add(UserActivityTypeEnum.ORDER_BACK_JOIN.getCode());
        shopTypes.add(UserActivityTypeEnum.ORDER_BACK_START.getCode());
        shopTypes.add(UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode());
        shopTypes.add(UserActivityTypeEnum.ORDER_BACK_START_V2.getCode());
        // 全平台展示的类型
        List<Integer> platformTypes = new ArrayList<>();
        platformTypes.add(UserActivityTypeEnum.PLATFORM_ORDER_BACK_START.getCode());
        platformTypes.add(UserActivityTypeEnum.PLATFORM_ORDER_BACK_JOIN.getCode());
        // 加工查询条件
        Map<String, Object> userActivityParams = new HashMap<>();
        userActivityParams.put("userId", userID);
        userActivityParams.put("shopId", shopID);
        userActivityParams.put("shopTypes", shopTypes);
        userActivityParams.put("platformTypes", platformTypes);
        userActivityParams.put("size", size);
        userActivityParams.put("offset", offset);
        userActivityParams.put("order", "ctime");
        userActivityParams.put("direct", "desc");
        //分页查询结果
        List<UserActivity> userActivities = this.userActivityService.pageListUserActivityShopAndPlatform(userActivityParams);
        //查询总数
        int userActivityCount = this.userActivityService.countUserActivityShopAndPlatform(userActivityParams);
        if (userActivities == null || userActivities.size() == 0) {
            d.setData(this.returnInfoObject(offset, size, userActivityCount, new ArrayList<>()));
            return d;
        }
        //查询对应活动，获取goodsid
        List<Long> activityIDs = new ArrayList<>();
        for (UserActivity userActivity : userActivities) {
            if (activityIDs.indexOf(userActivity.getActivityId()) == -1) {
                activityIDs.add(userActivity.getActivityId());
            }
        }

        Map<String, Object> activityParams = new HashMap<>();
        // activityParams.put("merchantId", merchantShop.getMerchantId());
        // activityParams.put("shopId", merchantShop.getId());
        activityParams.put("activityIds", activityIDs);
        List<Activity> activities = this.activityService.findPage(activityParams);

        Map<String, Object> userTaskParams = new HashMap<>();
        // userTaskParams.put("merchantId", merchantShop.getMerchantId());
        // userTaskParams.put("shopId", merchantShop.getId());
        userTaskParams.put("activityIds", activityIDs);
        userTaskParams.put("userId", userID);
        List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
        Map<Long, List<UserTask>> userTaskMap = new HashMap<>();
        if (userTasks != null && userTasks.size() > 0) {
            for (UserTask userTask : userTasks) {
                List<UserTask> userTasks1 = userTaskMap.get(userTask.getActivityId());
                if (userTasks1 == null) {
                    userTasks1 = new ArrayList<>();
                    userTaskMap.put(userTask.getActivityId(), userTasks1);
                }
                userTasks1.add(userTask);
            }
        }

        List<Long> goodsIDs = new ArrayList<>();
        Set<Long> productSkuIdSet = new HashSet<>();
        Map<Long, Activity> activityMap = new HashMap<>();
        if (activities != null && activities.size() > 0) {
            for (Activity activity : activities) {
                activityMap.put(activity.getId(), activity);

                if (activity.getType() == ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
                    productSkuIdSet.add(activity.getGoodsId());
                    continue;
                }

                if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                    goodsIDs.add(activity.getGoodsId());
                }
            }
        }

        List<Long> productSkuIdList = new ArrayList<>(productSkuIdSet);
        Map<Long, ProductEntity> productEntityMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(productSkuIdList)) {
            List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
            if (!CollectionUtils.isEmpty(productEntities)) {
                for (ProductEntity productEntity : productEntities) {
                    productEntityMap.put(productEntity.getSkuId(), productEntity);
                }
            }
        }


        //根据goodsid获取goodsMap
        Map<Long, Goods> goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        //赋值阶段
        List<Object> contentList = new ArrayList<>();
        for (UserActivity userActivity : userActivities) {
            Map<String, Object> map = new HashMap<>();
            List<String> joinUsers = this.redisService.queryRedisJoinUser(userActivity.getActivityId());
            Activity activity = activityMap.get(userActivity.getActivityId());
            List<UserTask> userTasks1 = userTaskMap.get(userActivity.getActivityId());
            if (activity == null) {
                continue;
            }

            String displayGoodsName = "";
            Long goodsId = 0L;
            BigDecimal displayPrice = BigDecimal.ZERO;
            String picUrl = "";

            Goods goods = goodsMap.get(activity.getGoodsId());
            if (goods != null) {
                displayGoodsName = goods.getDisplayGoodsName();
                goodsId = goods.getId();
                displayPrice = goods.getDisplayPrice();
                picUrl = goods.getPicUrl();
            }
            ProductEntity productEntity = productEntityMap.get(activity.getGoodsId());
            if (productEntity != null) {
                displayGoodsName = productEntity.getProductTitle();
                goodsId = productEntity.getId();
                displayPrice = productEntity.getThirdPriceMax();
                picUrl = JSON.parseArray(productEntity.getMainPics()).get(0).toString();
            }

            if (StringUtils.isBlank(displayGoodsName)) {
                log.error("没有商品信息");
                continue;
            }

            int status = 1;
            if ((activity.getStatus() == 2 && activity.getAwardStartTime() != null) || activity.getStatus() == -1) {
                status = 2;
            }
            //判断该活动是否是新人有礼发起者
            boolean isYourself = false;
            if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                    && activity.getUserId() != null
                    && activity.getUserId().longValue() == userID.longValue()) {
                isYourself = true;
            }
            // -1;//未中奖已结束 1;//中奖已结束 3;//中奖已返现 2;//中奖前往领取 4;//未中奖无奖品 5;//优惠券 6;//红包 7;//优惠券 红包 8:进行中，待开奖
            map.put("status", this.getMyJoinActivityStatus(activity, userActivity, userTasks1));
            map.put("activityId", activity.getId());//活动编号

            // redis更新活动列表
            Long joinSize = stringRedisTemplate.opsForList().size(RedisKeyConstant.ACTIVITY_JOINLIST_SHOW + activity.getId());

            map.put("joinCount", joinSize == null ? joinUsers.size() : joinSize);//参与人数

            map.put("title", displayGoodsName);//商品名
            map.put("goodsId", goodsId);
            map.put("picUrl", this.ossCaller.cdnReplace(picUrl));//商品图片地址
            map.put("price", displayPrice);//商品价格
            map.put("activityType", activity.getType());
            map.put("isYourself", isYourself);
            map.put("ctime", DateFormatUtils.format(userActivity.getCtime(), "yyyy.MM.dd HH:mm"));
            map.put("taskId", !CollectionUtils.isEmpty(userTasks1) ? userTasks1.get(0).getId() : 0);
            contentList.add(map);
        }

        d.setData(this.returnInfoObject(offset, size, userActivityCount, contentList));
        return d;
    }

    /***
     * 我的参与活动任务
     * @param userID
     * @param offset
     * @param size
     * @param type
     * @return
     */
    @Override
    public SimpleResponse queryMyJoinTask(Long userID, Long shopID, int offset, int size, String type) {
        SimpleResponse d = new SimpleResponse();
        Map<String, Object> resp = new HashMap<String, Object>();
        if (userID == null || userID == 0 || shopID == null || shopID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该店铺不存在");
            return d;
        }

        List<Integer> types = Arrays.asList(
                UserActivityTypeEnum.ASSIST.getCode(),
                UserActivityTypeEnum.ASSIST_V2.getCode(),
                UserActivityTypeEnum.MCH_PLAN.getCode(),
                UserActivityTypeEnum.NEW_MAN_JOIN.getCode(),
                UserActivityTypeEnum.NEW_MAN_START.getCode(),
                UserActivityTypeEnum.ORDER_BACK_JOIN.getCode(),
                UserActivityTypeEnum.ORDER_BACK_START.getCode(),
                UserActivityTypeEnum.ORDER_BACK_JOIN_V2.getCode(),
                UserActivityTypeEnum.ORDER_BACK_START_V2.getCode(),
                UserActivityTypeEnum.PLATFORM_ORDER_BACK_START.getCode(),
                UserActivityTypeEnum.PLATFORM_ORDER_BACK_JOIN.getCode());
        //分页查询结果
        Map<String, Object> userActivityParams = this.queryUserActivityParams(userID, shopID, null, null, null, "ctime", "desc", offset, size, types);

        List<UserActivity> userActivities = this.userActivityService.pageListUserActivity(userActivityParams);
        //查询总数
        int userActivityCount = this.userActivityService.countUserActivity(
                this.queryUserActivityParams(userID, shopID, null, null, null, null, null, null, null, types));
        if (userActivities == null || userActivities.size() == 0) {
            d.setData(this.returnInfoObject(offset, size, userActivityCount, new ArrayList<>()));
            return d;
        }
        //查询对应活动，获取goodsid
        List<Long> activityIDs = new ArrayList<Long>();
        for (UserActivity userActivity : userActivities) {
            if (activityIDs.indexOf(userActivity.getActivityId()) == -1) {
                activityIDs.add(userActivity.getActivityId());
            }
        }

        Map<String, Object> activityParams = new HashMap<String, Object>();
        activityParams.put("merchantId", merchantShop.getMerchantId());
        activityParams.put("shopId", merchantShop.getId());
        activityParams.put("activityIds", activityIDs);
        List<Activity> activities = this.activityService.findPage(activityParams);

        Map<String, Object> userTaskParams = new HashMap<>();
        userTaskParams.put("merchantId", merchantShop.getMerchantId());
        userTaskParams.put("shopId", merchantShop.getId());
        userTaskParams.put("activityIds", activityIDs);
        userTaskParams.put("userId", userID);
        List<UserTask> userTasks = this.userTaskService.query(userTaskParams);
        Map<Long, List<UserTask>> userTaskMap = new HashMap<>();
        if (userTasks != null && userTasks.size() > 0) {
            for (UserTask userTask : userTasks) {
                List<UserTask> userTasks1 = userTaskMap.get(userTask.getActivityId());
                if (userTasks1 == null) {
                    userTasks1 = new ArrayList<UserTask>();
                    userTaskMap.put(userTask.getActivityId(), userTasks1);
                }
                userTasks1.add(userTask);
            }
        }

        List<Long> goodsIDs = new ArrayList<Long>();
        Set<Long> productSkuIdSet = new HashSet<>();
        Map<Long, Activity> activityMap = new HashMap<>();
        if (activities != null && activities.size() > 0) {
            for (Activity activity : activities) {
                activityMap.put(activity.getId(), activity);

                if (activity.getType() == ActivityTypeEnum.PLATFORM_ORDER_BACK.getCode()) {
                    productSkuIdSet.add(activity.getGoodsId());
                    continue;
                }

                if (goodsIDs.indexOf(activity.getGoodsId()) == -1) {
                    goodsIDs.add(activity.getGoodsId());
                }
            }
        }

        List<Long> productSkuIdList = new ArrayList<>(productSkuIdSet);
        Map<Long, ProductEntity> productEntityMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(productSkuIdList)) {
            List<ProductEntity> productEntities = goodsAppService.queryProductDetailBySkuIds(productSkuIdList);
            if (!CollectionUtils.isEmpty(productEntities)) {
                for (ProductEntity productEntity : productEntities) {
                    productEntityMap.put(productEntity.getSkuId(), productEntity);
                }
            }
        }


        //根据goodsid获取goodsMap
        Map<Long, Goods> goodsMap = this.goodsService.queryGoodsInfo(goodsIDs);
        //赋值阶段
        List<Object> contentList = new ArrayList<Object>();
        for (UserActivity userActivity : userActivities) {
            Map<String, Object> map = new HashMap<String, Object>();
            List<String> joinUsers = this.redisService.queryRedisJoinUser(userActivity.getActivityId());
            Activity activity = activityMap.get(userActivity.getActivityId());
            List<UserTask> userTasks1 = userTaskMap.get(userActivity.getActivityId());
            if (activity == null) {
                continue;
            }

            String displayGoodsName = "";
            Long goodsId = 0L;
            BigDecimal displayPrice = BigDecimal.ZERO;
            String picUrl = "";

            Goods goods = goodsMap.get(activity.getGoodsId());
            if (goods != null) {
                displayGoodsName = goods.getDisplayGoodsName();
                goodsId = goods.getId();
                displayPrice = goods.getDisplayPrice();
                picUrl = goods.getPicUrl();
            }
            ProductEntity productEntity = productEntityMap.get(activity.getGoodsId());
            if (productEntity != null) {
                displayGoodsName = productEntity.getProductTitle();
                goodsId = productEntity.getId();
                displayPrice = productEntity.getThirdPriceMax();
                picUrl = JSON.parseArray(productEntity.getMainPics()).get(0).toString();
            }

            if (StringUtils.isBlank(displayGoodsName)) {
                log.error("没有商品信息");
                continue;
            }

            int status = 1;
            if ((activity.getStatus() == 2 && activity.getAwardStartTime() != null) || activity.getStatus() == -1) {
                status = 2;
            }
            //判断该活动是否是新人有礼发起者
            boolean isYourself = false;
            if (activity.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()
                    && activity.getUserId() != null
                    && activity.getUserId().longValue() == userID.longValue()) {
                isYourself = true;
            }
            // -1;//未中奖已结束 1;//中奖已结束 3;//中奖已返现 2;//中奖前往领取 4;//未中奖无奖品 5;//优惠券 6;//红包 7;//优惠券 红包 8:进行中，待开奖
            map.put("status", this.getMyJoinActivityStatus(activity, userActivity, userTasks1));
            map.put("activityId", activity.getId());//活动编号
            map.put("joinCount", joinUsers.size());//参与人数


            map.put("title", displayGoodsName);//商品名
            map.put("goodsId", goodsId);
            map.put("picUrl", this.ossCaller.cdnReplace(picUrl));//商品图片地址
            map.put("price", displayPrice);//商品价格
            map.put("activityType", activity.getType());
            map.put("isYourself", isYourself);
            map.put("ctime", DateFormatUtils.format(userActivity.getCtime(), "yyyy.MM.dd HH:mm"));
            map.put("taskId", !CollectionUtils.isEmpty(userTasks1) ? userTasks1.get(0).getId() : 0);
            contentList.add(map);
        }

        d.setData(this.returnInfoObject(offset, size, userActivityCount, contentList));
        return d;
    }

    /***
     * 获取参与者的中奖状态
     * @param activity
     * @param userActivity
     * @param userTasks
     * @return
     */
    private int getMyJoinActivityStatus(Activity activity, UserActivity userActivity, List<UserTask> userTasks) {
        int returnStatus = -1;//未中奖已结束
        //-1 已结束 1待开始 2待开奖 3已开奖 另外不做小程序展现的状态：0:待发布 -2：强制下线
        int activtityStatus = this.activityService.getActivityStatus(activity.getStatus(), activity.getStartTime(), activity.getAwardStartTime(), activity.getEndTime());
        if (activtityStatus == -1 && userTasks != null && userTasks.size() > 0) {
            for (UserTask userTask : userTasks) {
                if (userTask.getType() != UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                    returnStatus = 1;//中奖已结束
                    break;
                }
            }
            return returnStatus;
        }
        if (activtityStatus == 2) {
            //进行中，待开奖
            returnStatus = 8;
        }
        //已开奖
        if (activtityStatus == 3) {
            //中奖已返现
            if (userTasks != null && userTasks.size() > 0) {
                for (UserTask userTask : userTasks) {
                    if (userTask.getType() != UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() && userTask.getStatus() == 3) {
                        returnStatus = 3;//中奖已返现
                        break;
                    }
                }
            }
            //中奖
            if (userTasks != null && userTasks.size() > 0) {
                for (UserTask userTask : userTasks) {
                    if (userTask.getType() != UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() && userActivity.getStatus() == 2) {
                        returnStatus = 2;//中奖前往领取
                        break;
                    }
                }
            }
            //没中奖
            if (userActivity.getStatus() == 1) {
                returnStatus = 4;//未中奖无奖品
            }
            //优惠券
            if (userActivity.getStatus() == 3) {
                returnStatus = 5;//优惠券
            }
            if (userTasks != null && userTasks.size() > 0) {
                for (UserTask userTask : userTasks) {
                    if (userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
                        if (returnStatus == 5) {
                            returnStatus = 7;//优惠券，红包
                        } else {
                            returnStatus = 6;//红包
                        }
                        break;
                    }
                }
            }
        }
        return returnStatus;
    }

    /***
     * 我的回填订单
     * @param userID
     * @param merchantID
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMyOrder(Long userID, Long merchantID, Long shopID, int offset, int size) {
        SimpleResponse d = new SimpleResponse();
        if (userID == null || userID == 0 || shopID == null || shopID == 0 || merchantID == null || merchantID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参不满足条件");
            return d;
        }
        Map<String, Object> activityOrderParams = new HashMap<String, Object>();
        activityOrderParams.put("merchantId", merchantID);
        activityOrderParams.put("shopId", shopID);
        activityOrderParams.put("userId", userID);
        activityOrderParams.put("size", size);
        activityOrderParams.put("offset", offset);
        activityOrderParams.put("order", "ctime");
        activityOrderParams.put("direct", "desc");
        List<ActivityOrder> activityOrders = this.activityOrderService.selectByParams(activityOrderParams);
        int countActivityOrder = this.activityOrderService.countActivityOrder(activityOrderParams);
        if (activityOrders == null || activityOrders.size() == 0) {
            d.setData(this.returnInfoObject(offset, size, countActivityOrder, new ArrayList<>()));
            return d;
        }

        List<Long> activityDefIds = new ArrayList<Long>();
        for (ActivityOrder activityOrder : activityOrders) {
            if (activityOrder.getActivityDefId() != null && activityDefIds.indexOf(activityOrder.getActivityDefId()) == -1) {
                activityDefIds.add(activityOrder.getActivityDefId());
            }
        }
        Map<Long, ActivityDef> activityDefMap = new HashMap<>();
        if (activityDefIds != null && activityDefIds.size() > 0) {
            Map<String, Object> activityDefParams = new HashMap<String, Object>();
            activityDefParams.put("activityDefIds", activityDefIds);
            List<ActivityDef> activityDefs = this.activityDefService.selectByParams(activityDefParams);
            if (activityDefs != null && activityDefs.size() > 0) {
                for (ActivityDef activityDef : activityDefs) {
                    activityDefMap.put(activityDef.getId(), activityDef);
                }
            }
        }

        List<Object> listContents = new ArrayList<Object>();
        for (ActivityOrder activityOrder : activityOrders) {
            ActivityDef activityDef = null;
            if (activityOrder.getActivityDefId() != null) {
                activityDef = activityDefMap.get(activityOrder.getActivityDefId());
            }
            Map<String, Object> map = this.getActivityOrderGoodsInfo(activityOrder, activityDef);
            map.put("pay_time", DateFormatUtils.format(activityOrder.getCtime(), "yyyy.MM.dd HH:mm"));
            map.put("activityDefId", activityOrder.getActivityDefId() == null ? 0 : activityOrder.getActivityDefId());
            map.put("activityId", activityOrder.getActivityId() == null ? 0 : activityOrder.getActivityId());
            listContents.add(map);
        }
        d.setData(this.returnInfoObject(offset, size, countActivityOrder, listContents));
        return d;
    }

    /**
     * 查询我的已购商品
     *
     * @param userID
     * @param shopID
     * @param offset
     * @param size
     * @return
     */
    @Override
    public SimpleResponse queryMyGoods(Long userID, Long shopID, int offset, int size) {
        // 添加查询我的已购商品
        SimpleResponse simpleResponse = new SimpleResponse();
        if (userID == 0 || shopID == 0) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            simpleResponse.setMessage("入参不满足条件");
            return simpleResponse;
        }
        Map<String, Object> activityOrderParams = new HashMap<>();
        activityOrderParams.put("shopId", shopID);
        activityOrderParams.put("userId", userID);
        activityOrderParams.put("goodsIdNotNull", "null");
        int countActivityOrder = activityOrderService.countActivityOrder(activityOrderParams);
        // 分页
        activityOrderParams.put("size", size);
        activityOrderParams.put("offset", offset);
        activityOrderParams.put("order", "id");
        activityOrderParams.put("direct", "desc");
        List<ActivityOrder> activityOrders = activityOrderService.selectByParams(activityOrderParams);
        if (activityOrders == null || activityOrders.size() == 0) {
            simpleResponse.setData(this.returnInfoObject(offset, size, countActivityOrder, new ArrayList<>()));
            return simpleResponse;
        }

        List<Long> activityDefIds = new ArrayList<>();
        for (ActivityOrder activityOrder : activityOrders) {
            if (activityOrder.getActivityDefId() != null && activityDefIds.indexOf(activityOrder.getActivityDefId()) == -1) {
                activityDefIds.add(activityOrder.getActivityDefId());
            }
        }
        Map<Long, ActivityDef> activityDefMap = new HashMap<>();
        if (activityDefIds.size() > 0) {
            Map<String, Object> activityDefParams = new HashMap<>();
            activityDefParams.put("activityDefIds", activityDefIds);
            List<ActivityDef> activityDefs = this.activityDefService.selectByParams(activityDefParams);
            if (activityDefs != null && activityDefs.size() > 0) {
                for (ActivityDef activityDef : activityDefs) {
                    activityDefMap.put(activityDef.getId(), activityDef);
                }
            }
        }

        List<Object> listContents = new ArrayList<>();
        for (ActivityOrder activityOrder : activityOrders) {
            ActivityDef activityDef = null;
            if (activityOrder.getActivityDefId() != null) {
                activityDef = activityDefMap.get(activityOrder.getActivityDefId());
            }
            Map<String, Object> map = this.getActivityOrderGoodsInfo(activityOrder, activityDef);
            map.put("payTime", DateFormatUtils.format(activityOrder.getPayTime(), "yyyy.MM.dd HH:mm"));
            map.put("activityDefId", activityOrder.getActivityDefId() == null ? 0 : activityOrder.getActivityDefId());
            map.put("activityId", activityOrder.getActivityId() == null ? 0 : activityOrder.getActivityId());
            listContents.add(map);
        }
        simpleResponse.setData(this.returnInfoObject(offset, size, countActivityOrder, listContents));

        return simpleResponse;
    }

    private Map<String, Object> getActivityOrderGoodsInfo(ActivityOrder activityOrder, ActivityDef activityDef) {
        Map<String, Object> map = new HashMap<>();
        if (activityOrder != null && activityOrder.getGoodsId() != null && activityOrder.getGoodsId() > 0) {
            map.put("title", activityOrder.getDisplayGoodsName());
            map.put("picPath", this.ossCaller.cdnReplace(activityOrder.getPicUrl()));
            map.put("displayPrice", activityOrder.getDisplayGoodsPrice());
        } else {
            map.put("title", activityOrder.getTitle());
            map.put("picPath", activityOrder.getPicUrl());
            map.put("displayPrice", activityOrder.getPayment());
        }
        map.put("stockNum", 0);
        if (activityOrder.getActivityId() == null || activityOrder.getActivityId() == 0) {
            map.put("is_launch", false);
        } else {
            Activity activity = this.activityService.findById(activityOrder.getActivityId());
            if (activity != null && activity.getStatus() != 0) {
                map.put("is_launch", false);
            } else {
                map.put("is_launch", true);
                if (activityDef != null) {
                    map.put("stockNum", activityDef.getStock());
                }
            }
        }
        map.put("actStatus", 0);

        return map;
    }

    /***
     * 小程序-我的页面的所有展现对象
     * @param user
     * @return
     */
    private Map<String, Object> queryMyPageInfo(User user, Long shopID, Long merchantID) {
        Map<String, Object> map = new HashMap<String, Object>();
        //用户信息
        map.put("userId", user.getId());
        map.put("nick", user.getNickName());
        map.put("avatarUrl", user.getAvatarUrl());
        map.put("balance", BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString());
        //用户余额信息
        UpayBalance upayBalance = this.upayBalanceService.query(merchantID, shopID, user.getId());
        if (upayBalance != null) {
            BigDecimal taskIncome = upayBalance.getBalance().setScale(2, BigDecimal.ROUND_DOWN);
            BigDecimal promotionIncome = upayBalance.getShoppingBalance().setScale(2, BigDecimal.ROUND_DOWN);
            BigDecimal openRedPackBalance = upayBalance.getOpenredpackBalance().setScale(2, BigDecimal.ROUND_DOWN);
            BigDecimal balance = taskIncome.add(promotionIncome).add(openRedPackBalance);
            map.put("balance", balance.setScale(2, BigDecimal.ROUND_DOWN).toString());
        }
        //中奖信息
        map.put("awardNum", this.queryAwardInfo(user.getId()));
        //优惠券信息
        List<UserActivity> userActivities = this.userActivityService.pageListUserActivity(
                this.queryUserActivityParams(user.getId(), shopID, 3, null, new Date(), null, null, null, null, null));
        map.put("shopCoupon", this.queryMyPageShopCoupon(user.getId(), userActivities));
        map.put("willpayNum", this.orderService.countByStatusByShopUser(shopID, user.getId(), Arrays.asList(GoodsOrderTypeEnum.PAY.getCode()), Arrays.asList(GoodsOrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode())));
        map.put("willsendNum", this.orderService.countByStatusByShopUser(shopID, user.getId(), Arrays.asList(GoodsOrderTypeEnum.PAY.getCode()), Arrays.asList(GoodsOrderStatusEnum.WILLSEND_OR_REFUND.getCode())));
        map.put("willreceiveNum", this.orderService.countByStatusByShopUser(shopID, user.getId(), Arrays.asList(GoodsOrderTypeEnum.PAY.getCode()), Arrays.asList(GoodsOrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode())));
        map.put("willcommentNum", this.commentAppService.getUnCommentNum(user.getId(), merchantID, shopID));
        map.put("willrefundNum", this.orderService.countByStatusByShopUser(shopID, user.getId(),
                Arrays.asList(GoodsOrderTypeEnum.REFUNDMONEY.getCode(), GoodsOrderTypeEnum.REFUNDMONEYANDREFUNDGOODS.getCode()),
                Arrays.asList(GoodsOrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode(), GoodsOrderStatusEnum.WILLSEND_OR_REFUND.getCode(), GoodsOrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode())));

        /**商城上线 隐藏掉 2019-04-17**/
//        //查询矿工数目
//        Map<String, Object> params = new HashMap<>();
//        params.put("shopId", shopID);
//        params.put("userId", user.getId());
//        params.put("type", UserPubJoinTypeEnum.CHECKPOINTS.getCode());
//        int minerNum = this.userPubJoinService.countByParams(params);
//        //矿工数
//        map.put("minerNum", minerNum);
//
//        //缴税总金额
//        BigDecimal allMinerTotalScottare = this.userPubJoinService.sumByTotalScottare(params);
//        //任务收益
//        QueryWrapper<UserPubJoin> userPubJoinQueryWrapper = new QueryWrapper<>();
//        userPubJoinQueryWrapper.lambda()
//                .eq(UserPubJoin::getShopId, shopID)
//                .eq(UserPubJoin::getType, UserPubJoinTypeEnum.CHECKPOINTS.getCode())
//                .eq(UserPubJoin::getSubUid, user.getId())
//        ;
//        List<UserPubJoin> userPubJoins = this.userPubJoinService.list(userPubJoinQueryWrapper);
//        BigDecimal taskProfit = BigDecimal.ZERO;
//        if (!CollectionUtils.isEmpty(userPubJoins)) {
//            taskProfit = userPubJoins.get(0).getTotalCommission();
//        }
//        BigDecimal totalProfit = taskProfit.add(allMinerTotalScottare);
//        //总收益 任务收益+缴税收益
//        map.put("totalProfit", totalProfit.setScale(2, BigDecimal.ROUND_DOWN).toString());
//        //获取该用户的财富等级
//        CommonDict commonDict = this.commonDictService.selectByTypeKey("checkpoint", "level");
//        List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
//        if (!CollectionUtils.isEmpty(objects)) {
//            BigDecimal overAmount = BigDecimal.ZERO;
//            for (int i = 0; i < objects.size(); i++) {
//                //[{"value":"不屈黑铁","money":0,"name":1},{"value":"倔强青铜","money":8.8,"name":2}]
//                Map<String, Object> jsonMap = JSONObject.parseObject(objects.get(i).toString(), Map.class);
//                BigDecimal checkpointMoney = new BigDecimal(jsonMap.get("money").toString());
//                overAmount = overAmount.add(checkpointMoney);
//                if (totalProfit.compareTo(overAmount) < 0) {
//                    //等级
//                    map.put("level", JSONObject.parseObject(objects.get(i - 1).toString(), Map.class));
//                    break;
//                }
//                if (i == objects.size() - 1) {
//                    //等级
//                    map.put("level", JSONObject.parseObject(objects.get(i).toString(), Map.class));
//                }
//            }
//        }

        return map;
    }

    /***
     * 小程序-我的页面-优惠券-详情
     * @param userID
     * @param userActivities
     * @return
     */
    private Map<String, Object> queryMyPageShopCoupon(Long userID, List<UserActivity> userActivities) {
        Map<String, Object> resp = new HashMap<String, Object>();
        int willTimeout = 0;
        if (userActivities == null || userActivities.size() == 0) {
            resp.put("shopCouponWillTimeoutNum", willTimeout);
            resp.put("shopCoupons", new ArrayList<>());
            return resp;
        }
        //活动开奖时间
        Map<Long, Activity> activityMap = this.queryActivityInfo(userID, userActivities, null);
        //优惠券信息
        List<Long> shopCouponIDs = new ArrayList<Long>();
        for (UserActivity userActivity : userActivities) {
            if (shopCouponIDs.indexOf(userActivity.getMerchantShopCouponId()) == -1) {
                shopCouponIDs.add(userActivity.getMerchantShopCouponId());
            }
        }
        Map<Long, MerchantShopCoupon> merchantShopCouponMap = this.queryMapShopCounponId(userID, userActivities, this.queryMerchantShopCouponParams(shopCouponIDs, null, null, null));

        List<Object> list = new ArrayList<>();
        for (UserActivity userActivity : userActivities) {
            Activity activity = activityMap.get(userActivity.getActivityId());
            MerchantShopCoupon merchantShopCoupon = merchantShopCouponMap.get(userActivity.getMerchantShopCouponId());
            if (activity == null || merchantShopCoupon == null) {
                continue;
            }
            //判断几张优惠券即将过期(暂定三天)
            if (DateUtil.dayDiff(new Date(), userActivity.getCouponEndTime()) <= 3) {
                willTimeout = willTimeout + 1;
            }
            //有效的优惠券
            if (DateUtil.dayDiff(new Date(), userActivity.getCouponEndTime()) > 0) {
                list.add(this.addShopCouponRespMap(merchantShopCoupon.getAmount(), merchantShopCoupon.getName(), merchantShopCoupon.getTpwd(),
                        DateFormatUtils.format(activity.getAwardStartTime(), "yyyy.MM.dd"), DateFormatUtils.format(userActivity.getCouponEndTime(), "yyyy.MM.dd"), null));
            }
        }
        resp.put("shopCouponWillTimeoutNum", willTimeout);
        resp.put("shopCoupons", list);
        return resp;
    }

    /***
     * 填充返回对象---卡券
     * @param amount
     * @param name
     * @param tpwd
     * @param couponStartTime
     * @param couponEndTime
     * @param shopCouponWillTimeoutNum
     * @return
     */
    private Map<String, Object> addShopCouponRespMap(BigDecimal amount, String name, String tpwd, String couponStartTime, String couponEndTime, Integer shopCouponWillTimeoutNum) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put("amount", amount == null ? BigDecimal.ZERO : amount);
        resp.put("name", StringUtils.isBlank(name) ? "" : name);
        resp.put("tpwd", StringUtils.isBlank(tpwd) ? "" : tpwd);
        resp.put("couponStartTime", StringUtils.isBlank(couponStartTime) ? "" : couponStartTime);
        resp.put("couponEndTime", StringUtils.isBlank(couponEndTime) ? "" : couponEndTime);
        if (shopCouponWillTimeoutNum != null) {
            resp.put("shopCouponWillTimeoutNum", shopCouponWillTimeoutNum);
        }

        //判断是否是小程序提审阶段，若是，淘口令不返回
        boolean isCommitAudit = this.redisService.selectCommitAuditWx("");
        if (isCommitAudit) {
            resp.put("tpwd", "");
        }
        return resp;
    }

    /***
     * 查询优惠券的入参
     * @return
     */
    private Map<String, Object> queryMerchantShopCouponParams(List<Long> shopCouponIDs, Integer offset, Integer size, Integer inValidDay) {
        Map<String, Object> merchantShopCouponParams = new HashMap<String, Object>();
        merchantShopCouponParams.put("shopCouponIds", shopCouponIDs);
        merchantShopCouponParams.put("inValidDay", inValidDay);
        merchantShopCouponParams.put("offset", offset);
        merchantShopCouponParams.put("limit", size);
        return merchantShopCouponParams;
    }

    /***
     * 优惠券map信息 Map<activityID,MerchantShopCoupon>
     * @return
     */
    private Map<Long, MerchantShopCoupon> queryMapShopCounponId(Long userID, List<UserActivity> userActivities, Map<String, Object> merchantShopCouponParams) {
        if (userActivities == null || userActivities.size() == 0) {
            return new HashMap<Long, MerchantShopCoupon>();
        }

        //优惠券具体详情信息过期
        List<MerchantShopCoupon> merchantShopCoupons = this.merchantShopCouponService.queryParams(merchantShopCouponParams);
        if (merchantShopCoupons == null || merchantShopCoupons.size() == 0) {
            return new HashMap<Long, MerchantShopCoupon>();
        }

        Map<Long, MerchantShopCoupon> merchantShopCouponMap = new HashMap<Long, MerchantShopCoupon>();
        for (MerchantShopCoupon merchantShopCoupon : merchantShopCoupons) {
            merchantShopCouponMap.put(merchantShopCoupon.getId(), merchantShopCoupon);
        }
        return merchantShopCouponMap;
    }

    /***
     * 获取活动开奖时间 //TODO：可以优化到po的本体service
     * @param userID
     * @return
     */
    private Map<Long, Activity> queryActivityInfo(Long userID, List<UserActivity> userActivities, List<Long> activityIDs) {
        if ((userActivities == null || userActivities.size() == 0) && (activityIDs == null || activityIDs.size() == 0)) {
            return new HashMap<Long, Activity>();
        }

        if (userActivities != null && userActivities.size() > 0) {
            activityIDs = new ArrayList<Long>();
            for (UserActivity userActivity : userActivities) {
                if (activityIDs.indexOf(userActivity.getActivityId()) == -1) {
                    activityIDs.add(userActivity.getActivityId());
                }
            }
        }
        if (CollectionUtils.isEmpty(activityIDs)) {
            return new HashMap<Long, Activity>();
        }

        //开奖时间
        Map<String, Object> activityParams = new HashMap<String, Object>();
        activityParams.put("activityIds", activityIDs);
        List<Activity> activities = this.activityService.findPage(activityParams);
        if (activities == null || activities.size() == 0) {
            return new HashMap<Long, Activity>();
        }
        Map<Long, Activity> activityMap = new HashMap<Long, Activity>();
        for (Activity activity : activities) {
            activityMap.put(activity.getId(), activity);
        }
        return activityMap;
    }

    /***
     * 用户活动 查询优惠券信息，活动开奖时间
     * @param userID
     * @return
     */
    private Map<String, Object> queryUserActivityParams(Long userID, Long shopID, Integer status, List<Integer> statusList,
                                                        Date inCouponEndTime, String order, String direct, Integer offset, Integer size, List<Integer> types) {
        Map<String, Object> userActivityParams = new HashMap<String, Object>();
        userActivityParams.put("userId", userID);
        userActivityParams.put("shopId", shopID);
        userActivityParams.put("status", status);
        userActivityParams.put("statusList", statusList);
        userActivityParams.put("types", types);
        userActivityParams.put("size", size);
        userActivityParams.put("offset", offset);
        userActivityParams.put("inCouponEndTime", inCouponEndTime);
        userActivityParams.put("order", order);
        userActivityParams.put("direct", direct);
        return userActivityParams;
    }

    /***
     * 获取中奖信息-数目
     * @param userID
     * @return
     */
    private int queryAwardInfo(Long userID) {
        Map<String, Object> userTaskParams = new HashMap<String, Object>();
        userTaskParams.put("userId", userID);
        userTaskParams.put("status", 3);
        userTaskParams.put("read", 0);
        int userAwardNum = this.userTaskService.countUserTask(userTaskParams);
        return userAwardNum;
    }
}
