package mf.code.api.applet.service.UserMoneyTradeDetail.impl.shopping;

import com.alibaba.excel.util.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyIndexNameEnum;
import mf.code.api.applet.service.UserMoneyTradeDetail.UserMoneyTradeDetailService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.api.applet.service.UserMoneyTradeDetail.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年01月21日 19:21
 */
@Service
@Slf4j
public class UserRebateTradeDetailConverterImpl extends UserMoneyTradeDetailService {
    @Override
    public Map<String, Object> covertUserMoneyTradeDetailForProductGoodsShopping(UpayWxOrder upayWxOrder,
                                                                                 Integer payType,
                                                                                 Map<Long, Map> shopMallMap) {
        //PALTFORMRECHARGE：平台充值 USERCASH：用户提现 USERPAY：用户支付 PALTFORMREFUND：平台退款
        boolean notNullPayType = payType != null;
        boolean userPayType = OrderTypeEnum.PALTFORMRECHARGE.getCode() == upayWxOrder.getType();
        boolean rebate = BizTypeEnum.REBATE.getCode() == upayWxOrder.getBizType();
        if (notNullPayType && userPayType && rebate && !CollectionUtils.isEmpty(shopMallMap)) {
            Map shopGoodsMap = shopMallMap.get(upayWxOrder.getBizValue());
            if (shopGoodsMap == null) {
                return new HashMap<>();
            }
            Map<String, Object> map = new HashMap<>();
            map.put("title", shopGoodsMap.get("title"));
            map.put("type", UserMoneyIndexNameEnum.SHOPPING_REBATE.getCode());
            map.put("indexName", UserMoneyIndexNameEnum.SHOPPING_REBATE.getMessage());
            String plus_minus = "+";
            //直接删除多余的小数位，如2.357会变成2.35
            map.put("price", plus_minus + upayWxOrder.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN).toString());
            map.put("time", DateFormatUtils.format(upayWxOrder.getCtime(), "yyyy.MM.dd HH:mm"));
            return map;
        }
        return null;
    }
}
