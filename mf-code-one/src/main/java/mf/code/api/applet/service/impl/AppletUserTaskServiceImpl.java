package mf.code.api.applet.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.constant.*;
import mf.code.activity.repo.po.*;
import mf.code.activity.service.*;
import mf.code.api.applet.dto.CommissionBO;
import mf.code.api.applet.dto.TaskReq;
import mf.code.api.applet.dto.TaskResp;
import mf.code.api.applet.service.AppletUserActivityService;
import mf.code.api.applet.service.AppletUserTaskService;
import mf.code.api.applet.v3.dto.CommissionDisDto;
import mf.code.api.applet.v3.dto.CommissionResp;
import mf.code.api.applet.v3.service.CommissionService;
import mf.code.api.feignservice.OneAppServiceImpl;
import mf.code.common.caller.aliyunoss.OssCaller;
import mf.code.common.caller.luckcrm.LuckCrmCaller;
import mf.code.common.constant.*;
import mf.code.common.constant.ActivityStatusEnum;
import mf.code.common.redis.RedisForbidRepeat;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.common.verify.TaobaoVerifyService;
import mf.code.goods.repo.po.Goods;
import mf.code.goods.repo.po.GoodsPlus;
import mf.code.goods.service.GoodsPlusService;
import mf.code.goods.service.GoodsService;
import mf.code.merchant.repo.po.MerchantShop;
import mf.code.merchant.service.MerchantShopService;
import mf.code.uactivity.repo.po.UserActivity;
import mf.code.uactivity.repo.po.UserTask;
import mf.code.uactivity.repo.redis.RedisKeyConstant;
import mf.code.uactivity.repo.redis.RedisService;
import mf.code.uactivity.service.UserActivityService;
import mf.code.uactivity.service.UserTaskService;
import mf.code.ucoupon.service.UserCouponService;
import mf.code.upay.repo.enums.BizTypeEnum;
import mf.code.upay.repo.enums.OrderPayTypeEnum;
import mf.code.upay.repo.enums.OrderStatusEnum;
import mf.code.upay.repo.enums.OrderTypeEnum;
import mf.code.upay.repo.po.UpayWxOrder;
import mf.code.upay.service.UpayBalanceService;
import mf.code.upay.service.UpayWxOrderService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * mf.code.api.applet.service.impl
 * Description:
 *
 * @author: 百川
 * @date: 2018-11-01 17:51
 */
@Slf4j
@Service
public class AppletUserTaskServiceImpl implements AppletUserTaskService {
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private MerchantShopService merchantShopService;
    @Autowired
    private LuckCrmCaller luckCrmCaller;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UpayBalanceService upayBalanceService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private OssCaller ossCaller;
    @Autowired
    private UpayWxOrderService upayWxOrderService;
    @Autowired
    private AppletUserActivityService appletUserActivityService;
    @Autowired
    private ActivityDefService activityDefService;
    @Autowired
    private ActivityPlanService activityPlanService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private ActivityTaskService activityTaskService;
    @Autowired
    private GoodsPlusService goodsPlusService;
    @Autowired
    private ActivityTaskUService activityTaskUService;
    @Autowired
    private UserActivityService userActivityService;
    @Autowired
    private ActivityOrderService activityOrderService;
    @Autowired
    private UserCouponService userCouponService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private ActivityCommissionService activityCommissionService;
    @Autowired
    private TaobaoVerifyService taobaoVerifyService;
    @Autowired
    private OneAppServiceImpl oneAppService;

    private static final String AWARDTASKPICPATH = "awardtaskpic/shop/";//店铺

    /**
     * 用户中奖任务 截图提交
     *
     * @param uid  用户id
     * @param aid  活动id
     * @param pics 提交的截图信息
     * @return 提交状态
     */
    @Override
    public SimpleResponse commitAwardTaskPics(Long uid, Long aid, List<String> pics, Long taskId) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (uid == null || aid == null || pics == null || pics.size() < 3) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
            return response;
        }
        for (String pic : pics) {
            if (StringUtils.isBlank(pic)) {
                response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
                return response;
            }
        }
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + aid + ":" + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 判断活动是否过期
        Activity activityDB = activityService.findById(aid);
        Integer activityStatus = activityDB.getStatus();
        if (activityStatus == ActivityStatusEnum.FAILURE.getCode()
                || activityStatus == ActivityStatusEnum.DEL.getCode()
                || activityStatus == ActivityStatusEnum.END.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            return response;
        }
        // 判断中奖任务是否有资格提交审核
        List<UserTask> userTasks = userTaskService.selectByUidAndAid(uid, aid);
        if (userTasks != null && userTasks.size() != 0) {
            for (UserTask userTask : userTasks) {
                Integer status = userTask.getStatus();
                if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                    response.setMessage("审核中，请勿再次提交");
                    return response;
                }
                if (status == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                    response.setMessage("中奖任务超时，请勿再次提交");
                    return response;
                }
                if (status == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                    response.setMessage("截图已审核通过，请勿再次提交");
                    return response;
                }
                if (status == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                    response.setMessage("任务已完成，请勿再次提交");
                    return response;
                }
            }
        }

        // 信息加工入库
        UserTask userTask = this.userTaskService.selectByPrimaryKey(taskId);
        userTask.setPics(JSONObject.toJSONString(pics));
        userTask.setStatus(UserTaskStatusEnum.AUDIT_WAIT.getCode());
        userTask.setReceiveTime(new Date());
        userTask.setUtime(new Date());
        boolean rows = this.userTaskService.updateById(userTask);
        if (!rows) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库更新失败：没有此用户任务记录");
            return response;
        }
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + aid + ":" + uid);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setMessage("提交成功，请等待审核");

        return response;
    }

    /***
     * 非活动用户的任务提交
     * @param taskReq
     * @return
     */
    @Override
    public SimpleResponse commitNotActivityTask(TaskReq taskReq) {
        if (StringUtils.isBlank(taskReq.getPics())) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "请先上传图片");
        }
        List<String> pics = JSONArray.parseArray(taskReq.getPics(), String.class);

        if (taskReq == null || taskReq.getUserTaskId() == null || taskReq.getUserTaskId() == 0 || CollectionUtils.isEmpty(pics) || pics.size() < 3) {
            return new SimpleResponse(ApiStatusEnum.ERROR_PARAM_NOT_FIND.getCode(), "参数缺失");
        }
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + taskReq.getUserTaskId());
        if (!success) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO10.getCode(), "");
        }
        // 判断中奖任务是否有资格提交审核
        UserTask userTask = this.userTaskService.selectByPrimaryKey(taskReq.getUserTaskId());
        Integer status = userTask.getStatus();
        if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "审核中，请勿再次提交");
        }
        if (status == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "中奖任务超时，请勿再次提交");
        }
        if (status == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "截图已审核通过，请勿再次提交");
        }
        if (status == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "任务已完成，请勿再次提交");
        }

        //在规定时间范围内，非第一次提交,不扣库存
        String costStr = redisService.getUserTaskSupplyAuditCost(userTask.getId());
        if (StringUtils.isBlank(costStr)) {
            //扣库存
            if (userTask.getActivityTaskId() != null && userTask.getActivityTaskId() > 0) {
                ActivityTask activityTask = this.activityTaskService.getById(userTask.getActivityTaskId());

                if (activityTask == null || activityTask.getStatus() == ActivityTaskStatusEnum.DEL.getCode()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该活动已经下线了！");
                }
                if (activityDefService.reduceDeposit(userTask.getActivityDefId(), BigDecimal.ZERO, 1) <= 0) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "库存不足啦！");
                }
            }
        }

        // 信息加工入库
        userTask.setReceiveTime(new Date());
        userTask.setUtime(new Date());
        userTask.setStatus(UserTaskStatusEnum.AUDIT_WAIT.getCode());
        userTask.setPics(JSONObject.toJSONString(pics));
        userTask.setAuditingReason("");
        boolean rows = this.userTaskService.updateById(userTask);
        if (!rows) {
            return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "数据库更新失败：没有此用户任务记录");
        }
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_TASK_COMMIT_PICS_FORBID_REPEAT + taskReq.getUserTaskId());

        //更新userActivity
        if (userTask.getUserActivityId() != null && userTask.getUserActivityId() > 0) {
            UserActivity userActivity = new UserActivity();
            userActivity.setId(userTask.getUserActivityId());
            userActivity.setUtime(new Date());
            this.userActivityService.updateByPrimaryKey(userActivity);
        }

        if (stringRedisTemplate.hasKey(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskReq.getUid())) {
            String activityDefId = stringRedisTemplate.opsForValue().get(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskReq.getUid());
            Map<String, Object> resultVO = new HashMap<>();
            resultVO.put("popStatus", 3);
            resultVO.put("activityDefId", Long.valueOf(activityDefId));
            stringRedisTemplate.delete(RedisKeyConstant.HOME_PAGE_GOODCOMMENT_POP + taskReq.getUid());
            return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核", resultVO);
        }
        return new SimpleResponse(ApiStatusEnum.SUCCESS.getCode(), "提交成功，请等待审核");
    }

    /**
     * 用户中奖任务 订单提交
     *
     * @param uid     用户id
     * @param aid     活动id
     * @param shopId  店铺id
     * @param orderId 提交的订单号
     * @return 提交状态
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @Override
    public SimpleResponse commitAwardTaskOrder(Long uid, Long aid, Long shopId, String orderId) {
        // 校验提交的有效性
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (uid == null || aid == null || StringUtils.isBlank(orderId)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_VALID);
            return response;
        }
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT + aid + ":" + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            return response;
        }
        // 判断活动是否过期
        Activity activityDB = activityService.findById(aid);
        Integer activityStatus = activityDB.getStatus();
        // 活动未开奖已结束
        Date activityDBEndTime = activityDB.getEndTime();
        Date activityDBAwardStartTime = activityDB.getAwardStartTime();
        boolean isFailureEnd = null == activityDBAwardStartTime && activityDBEndTime.getTime() < System.currentTimeMillis();
        if (isFailureEnd || activityStatus == ActivityStatusEnum.DEL.getCode() || activityStatus == ActivityStatusEnum.END.getCode()) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
            return response;
        }
        // 该订单已参与活动，请勿重复提交
        List<UserTask> userTaskList = userTaskService.selectByOrderId(orderId);
        if (userTaskList != null && userTaskList.size() > 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO17);
            return response;
        }
        // 判断中奖任务是否有资格提交审核
        List<UserTask> userTasks = userTaskService.selectByUidAndAid(uid, aid);
        if (userTasks != null && userTasks.size() != 0) {
            for (UserTask userTask : userTasks) {
                Integer status = userTask.getStatus();
                if (status != UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                    response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO18);
                    return response;
                }
            }
        }
        // 通过shopid 查找 店铺nick
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(shopId);
        if (null == merchantShop) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：未查到商户店铺");
            return response;
        }
        String nick = merchantShop.getNick();
        // 调用喜销宝接口 获取 订单实际支付金额
        String orderInfo = taobaoVerifyService.getOrderInfo(orderId, merchantShop.getMerchantId());
        if (luckCrmCaller.isError(orderInfo)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO16);
            return response;
        }
        JSONObject orderInfoJO = JSONObject.parseObject(orderInfo);
        List<Object> numIidList = orderInfoJO.getJSONArray("num_iid");
        Goods goods = goodsService.selectById(activityDB.getGoodsId());
        if (null == goods) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("数据库异常：未找到该活动商品");
            return response;
        }
        log.info("numIidList = " + numIidList.toString());
        if (!numIidList.contains(goods.getXxbtopNumIid())) {
            // 该订单编号非商品订单
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO20);
            return response;
        }
        String orderCreatedTime = orderInfoJO.getString("created");
        // orderCreatedTime = 2018-11-12 23:54:19
        long orderCtime = DateUtil.stringtoDate(orderCreatedTime, DateUtil.FORMAT_ONE).getTime();
        long now = System.currentTimeMillis();
        long threeMonthTime = 3 * 30 * 24 * 3600 * 1000L;
        boolean isThreeMonthAgo = now - orderCtime > threeMonthTime;
        if (isThreeMonthAgo) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO23);
            return response;
        }
        // 判断订单是否已支付
        String status = orderInfoJO.getString("status");
        if (StringUtils.isBlank(status)) {
            log.error("用户提交订单号，喜销宝返回的结果字段缺失支付状态信息status");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            response.setMessage("网络异常，请稍候重试");
            return response;
        }
        if (StringUtils.equals(status, "TRADE_NO_CREATE_PAY")
                || StringUtils.equals(status, "WAIT_BUYER_PAY")
                || StringUtils.equals(status, "TRADE_CLOSED")
                || StringUtils.equals(status, "TRADE_CLOSED_BY_TAOBAO")
                || StringUtils.equals(status, "PAY_PENDING")
                || StringUtils.equals(status, "WAIT_PRE_AUTH_CONFIRM")) {
            log.info("此订单未支付或已退款，请更换其他订单号");
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            response.setMessage("此订单未支付或已退款，请更换其他订单号");
            return response;
        }
        // 2019-03-19 修改商品返现金额为实际支付金额，更新activity
        BigDecimal realPayAmount = new BigDecimal(orderInfoJO.getString("payment"));
        // 实际支付金额，判断最多返回集客免单价格
        if (realPayAmount.compareTo(goods.getDisplayPrice()) < 0) {
            activityDB.setPayPrice(realPayAmount);
            activityService.update(activityDB);
        }
        // 如果订单支付金额大于商品定价，则返现定义商品金额
        if (realPayAmount.compareTo(goods.getDisplayPrice()) > 0) {
            realPayAmount = goods.getDisplayPrice();
        }
        BizTypeEnum bizTypeEnum = null;
        if (activityDB.getType() == ActivityTypeEnum.MCH_PLAN.getCode()) {
            bizTypeEnum = BizTypeEnum.ACTIVITY;
        } else if (activityDB.getType() == ActivityTypeEnum.NEW_MAN_START.getCode()) {
            bizTypeEnum = BizTypeEnum.NEWMAN_START;
        } else if (activityDB.getType() == ActivityTypeEnum.ASSIST.getCode() || activityDB.getType() == ActivityTypeEnum.ASSIST_V2.getCode()) {
            bizTypeEnum = BizTypeEnum.ASSIST_ACTIVITY;
        } else if (activityDB.getType() == ActivityTypeEnum.ORDER_BACK_START.getCode() || activityDB.getType() == ActivityTypeEnum.ORDER_BACK_START_V2.getCode()) {
            bizTypeEnum = BizTypeEnum.ORDER_BACK;
        }
        // 中奖保证金扣除
        Integer integer = activityDefService.reduceDeposit(activityDB.getActivityDefId(), activityDB.getPayPrice(), 0);
        // 2019-04-04 添加方法事务，添加金额更新判断
        if (integer <= 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("任务库存不足");
            return response;
        }
        //创建订单
        UpayWxOrder upayWxOrder = this.upayWxOrderService.addPo(uid, OrderPayTypeEnum.CASH.getCode(), OrderTypeEnum.PALTFORMRECHARGE.getCode(),
                null, "中奖任务红包", activityDB.getMerchantId(), shopId, OrderStatusEnum.ORDERED.getCode(), activityDB.getPayPrice()
                , IpUtil.getServerIp(), null, "中奖任务红包", new Date(), null, null, bizTypeEnum.getCode(), aid);
        this.upayWxOrderService.create(upayWxOrder);
        // 对用户余额进行操作
        this.upayBalanceService.updateOrCreate(activityDB.getMerchantId(), shopId, uid, activityDB.getPayPrice());
        // 中奖信息入库user_coupon表
        List<Long> amountUserList = new ArrayList<>();
        amountUserList.add(uid);
        success = userCouponService.saveAmountUsersFromActivity(activityDB, amountUserList);
        if (!success) {
            log.error("插入user_coupon表失败");
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            response.setMessage("插入user_coupon表失败");
            return response;
        }

        // 2月15号改为分配佣金模式,即原流程 1创建订单，2更新余额，3记录用户奖品 --> 1佣金分配，佣金扣除
        ActivityDef activityDef = activityDefService.selectByPrimaryKey(activityDB.getActivityDefId());
        CommissionDisDto commissionDisDto = new CommissionDisDto(uid, shopId,
                UserCouponTypeEnum.findByCode(userCouponService.getUserCouponTypeFromActivityType(activityDB.getType())),
                activityDef.getCommission());
        commissionDisDto.setActivityId(activityDB.getId());
        commissionDisDto.setActivityDefId(activityDB.getActivityDefId());
        commissionDisDto.setGoodsPrice(activityDB.getPayPrice());
        CommissionBO commissionBO = commissionService.commissionDistribution(commissionDisDto);

        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(uid, shopId, merchantShop.getMerchantId());

        // 信息加工入库
        QueryWrapper<UserTask> utWrapper = new QueryWrapper<>();
        utWrapper.lambda()
                .eq(UserTask::getUserId, uid)
                .eq(UserTask::getActivityId, aid);
        UserTask userTask = userTaskService.getOne(utWrapper);
        userTask.setUserId(uid);
        userTask.setActivityId(aid);
        userTask.setOrderId(orderId);
        userTask.setRpAmount(commissionBO.getCommission());
        userTask.setStatus(UserTaskStatusEnum.AWARD_SUCCESS.getCode());
        userTask.setReceiveTime(new Date());
        userTask.setUtime(new Date());
        int rows = userTaskService.update(userTask);
        if (rows == 0) {
            response.setStatusEnum(ApiStatusEnum.ERROR_DB);
            return response;
        }
        // 小程序用户活动订单
        appletUserActivityService.updateOrCreateActivityOrder(uid, orderId, shopId, null, goods, orderInfoJO);


        redisService.deleteUserTaskCommitAndAudit(userTask.getId());
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_TASK_COMMIT_ORDER_FORBID_REPEAT + aid + ":" + uid);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        Map<String, Object> userTaskVO = new HashMap<>();
        userTaskVO.putAll(BeanMapUtil.beanToMap(commissionResp));
        userTaskVO.put("rpAmount", userTask.getRpAmount());
        userTaskVO.put("commission", userTask.getRpAmount());
        userTaskVO.put("type", userTask.getType());
        userTaskVO.put("redPackMoney", activityDef.getGoodsPrice() != null ? activityDef.getGoodsPrice() : userTask.getRpAmount());
        response.setData(userTaskVO);
        return response;
    }

    @Override
    public SimpleResponse uploadAwardTaskPics(MultipartFile imageFile, Long shopID, Long activityID) {
        SimpleResponse d = new SimpleResponse();
        if (activityID == null || activityID == 0 || shopID == null || shopID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        if (imageFile.getSize() > 1024 * 5 * 1024) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("上传图片大小过大");
            return d;
        }
        String imgUrl = this.uploadObject2OSS(imageFile, shopID, activityID);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("imgUrl", imgUrl);
        d.setData(map);
        return d;
    }

    @Override
    public SimpleResponse uploadPics(MultipartFile imageFile, Long shopID, Long goodsId) {
        SimpleResponse d = new SimpleResponse();
        if (shopID == null || shopID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }
        if (imageFile.getSize() > 1024 * 5 * 1024) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("上传图片大小过大");
            return d;
        }
        if (goodsId == null) {
            goodsId = 0L;
        }
        String imgUrl = this.uploadObject2OSS(imageFile, shopID, goodsId);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("imgUrl", imgUrl);
        d.setData(map);
        return d;
    }

    private String uploadObject2OSS(MultipartFile image, Long shopID, Long value) {
        String ext = FilenameUtils.getExtension(image.getOriginalFilename());
        String[] imageType = {"gif", "png", "jpg", "jpeg", "bmp"};
        if (!checkFileType(imageType, ext)) {
            log.error("文件格式格式错误");
        }
        String fileName = value + "_" + System.currentTimeMillis() + RandomStrUtil.randomStr(3) + "." + ext;
        String bucket = AWARDTASKPICPATH + shopID + "/" + fileName;
        String imgUrl = "";
        try {
            imgUrl = this.ossCaller.uploadObject2OSSInputstream(image.getInputStream(), bucket);
        } catch (IOException e) {

        }
        return imgUrl;
    }

    /***
     * 查询领任务奖品的状态过程
     * @param userID
     * @param activityID
     * @return
     */
    @Override
    public SimpleResponse queryAwardTask(Long userID, Long activityID) {
        SimpleResponse d = new SimpleResponse();
        if (activityID == null || activityID == 0 || userID == null || userID == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
            d.setMessage("入参传入异常");
            return d;
        }

        Activity activity = this.activityService.findById(activityID);
        if (activity == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该活动已不存在");
            return d;
        }
        Goods goods = this.goodsService.selectById(activity.getGoodsId());
        if (goods == null) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            d.setMessage("该活动对应的商品库不存在");
            return d;
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("merchantId", activity.getMerchantId());
        map.put("shopId", activity.getShopId());
        map.put("activityId", activityID);
        map.put("userId", userID);
        //-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功
        map.put("statusList", Arrays.asList(0, 1, -2, 2, 3));
        List<UserTask> userTasks = this.userTaskService.query(map);
        if (userTasks == null || userTasks.size() == 0) {
            d.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
            d.setMessage("该中奖任务不存在，你没有中奖任务资格");
            return d;
        }
        UserTask userTask = userTasks.get(0);
        // 百川修改 用户执行任务的开始时间 begin
        Date startTime = userTask.getStartTime();
        Date now = new Date();
        if (null == startTime) {
            startTime = now;
            userTask.setStartTime(now);
            userTask.setUtime(now);
            int updateRows = userTaskService.update(userTask);
            if (updateRows == 0) {
                log.error("user_task表，更新开始执行任务时间失败");
                d.setStatusEnum(ApiStatusEnum.ERROR_DB);
                d.setMessage("网络异常，请稍候重试");
                return d;
            }
        }
        // 百川修改 用户执行任务的开始时间 end

        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("orderNo", StringUtils.isBlank(userTask.getOrderId()) ? "" : userTask.getOrderId());
        //截止时间的时间戳
        respMap.put("cutoffTime", DateUtils.addMinutes(startTime, activity.getMissionNeedTime()).getTime());
        respMap.put("keyword", StringUtils.isNotBlank(userTask.getKeyWords()) ? userTask.getKeyWords() : "");
        respMap.put("redPackMoneyTime", userTask.getReceiveTime() != null ? DateFormatUtils.format(userTask.getReceiveTime(), "yyyy.MM.dd HH:mm") : "");
        respMap.put("status", userTask.getStatus());
        respMap.put("competitorPic", "");
        respMap.put("startTime", DateFormatUtils.format(activity.getStartTime(), "yyyy.MM.dd HH:mm"));
        respMap.put("endTime", DateFormatUtils.format(activity.getEndTime(), "yyyy.MM.dd HH:mm"));

        respMap.put("redPackMoney", userTask.getRpAmount() == null ?
                BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString() : userTask.getRpAmount().setScale(2, BigDecimal.ROUND_DOWN).toString());
        respMap.put("goodsPic1", "");
        respMap.put("goodsPic2", "");
        respMap.put("auditingReason", StringUtils.isNotBlank(userTask.getAuditingReason()) ? userTask.getAuditingReason() : "");
        //是否弹窗--主要用于红包收藏加购任务
        respMap.put("isDialog", false);
        if (userTask.getType() == UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()) {
            respMap.put("isDialog", this.redisService.getRedisUserTaskRedPack(userTask.getId()));
        }
        //非抽奖活动任务展现
        respMap.put("title", goods.getDisplayGoodsName());

        List<String> pics = new ArrayList<String>();
        if (StringUtils.isNotBlank(userTask.getPics())) {
            pics = JSONObject.parseArray(userTask.getPics(), String.class);
        }

        boolean isUserTaskFailed = userTask.getStatus() == UserTaskStatusEnum.AUDIT_ERROR.getCode();
        if (isUserTaskFailed) {
            //更新usertask
            userTask.setPics("");
            userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
            userTask.setUtime(now);
            userTask.setAuditingReason("");//TODO:后期每次的失败可以做mongo存储处理
            this.userTaskService.update(userTask);
            d.setData(respMap);
            return d;
        }
        if (ActivityTypeEnum.REDPACK_TASK_FAV_CART.getCode() == userTask.getType()) {
            if (pics != null && pics.size() >= 2) {
                int i = 1;
                for (String pic : pics) {
                    respMap.put("goodsPic" + i, this.ossCaller.cdnReplace(pic));
                    i++;
                }
            }
            if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
                ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
                respMap.put("redPackMoney", activityDef.getTaskRpAmount() == null ? BigDecimal.ZERO : activityDef.getTaskRpAmount());
            } else if (activity.getActivityPlanId() != null && activity.getActivityPlanId() > 0) {
                ActivityPlan activityPlan = this.activityPlanService.findById(activity.getActivityPlanId());
                respMap.put("redPackMoney", activityPlan.getRedpackUnitPrice() == null ? BigDecimal.ZERO : activityPlan.getRedpackUnitPrice());
            }
        } else {
            if (pics != null && pics.size() >= 3) {
                respMap.put("competitorPic", pics.get(0));
                pics.remove(0);
                int i = 1;
                for (String pic : pics) {
                    respMap.put("goodsPic" + i, this.ossCaller.cdnReplace(pic));
                    i++;
                }
            }
        }
        // 20181217 添加店铺商品url信息
        MerchantShop merchantShop = merchantShopService.selectMerchantShopById(goods.getShopId());
        respMap.put("shopName", ConvertUtil.dataMasking(merchantShop.getShopName()));
        respMap.put("goodsPrice", goods.getDisplayPrice().setScale(2, BigDecimal.ROUND_DOWN).toString());
        respMap.put("goodsUrl", userTask.getTaskGoodsUrl() == null ? "" : userTask.getTaskGoodsUrl());
        respMap.put("goodsPicUrl", goods.getPicUrl());

        d.setData(respMap);
        return d;
    }

    /**
     * 用户中奖任务 商品URL提交
     *
     * @param uid
     * @param aid
     * @param userTaskId
     * @param goodsUrl
     * @return
     */
    @Override
    public SimpleResponse commitAwardGoodsUrl(Long uid, Long aid, Long goodsId, Long userTaskId, String goodsUrl) {
        SimpleResponse<Object> response = new SimpleResponse<>();
        if (uid == null || StringUtils.isBlank(goodsUrl)) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
            return response;
        }
        if (goodsUrl.length() > 300) {
            response.setStatusEnum(ApiStatusEnum.ERROR_PARAM_NOT_FIND);
            response.setMessage("输入字符过长，请检查输入是否正确");
            return response;
        }
        boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ACTIVITY_TASK_COMMIT_GOODSURL_FORBID_REPEAT + uid);
        if (!success) {
            response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO10);
            response.setMessage("点击太快，请稍后重试");
            return response;
        }
        /**
         * 依据字段有无来判断中奖者任务和红包任务
         */
        if (goodsId == null && userTaskId == null) {
            // 判断活动是否过期
            Activity activityDB = activityService.findById(aid);
            Integer activityStatus = activityDB.getStatus();
            if (activityStatus == ActivityStatusEnum.FAILURE.getCode()
                    || activityStatus == ActivityStatusEnum.DEL.getCode()
                    || activityStatus == ActivityStatusEnum.END.getCode()) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO14);
                return response;
            }
            // 判断中奖任务是否有资格提交审核
            List<UserTask> userTasks = userTaskService.selectByUidAndAid(uid, aid);
            if (userTasks != null && userTasks.size() != 0) {
                for (UserTask userTask : userTasks) {
                    Integer status = userTask.getStatus();
                    if (status == UserTaskStatusEnum.AUDIT_WAIT.getCode()) {
                        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO1);
                        response.setMessage("审核中，请勿再次提交");
                        return response;
                    }
                    if (status == UserTaskStatusEnum.AWARD_EXPIRE.getCode()) {
                        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO2);
                        response.setMessage("中奖任务超时，请勿再次提交");
                        return response;
                    }
                    if (status == UserTaskStatusEnum.AUDIT_SUCCESS.getCode()) {
                        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
                        response.setMessage("截图已审核通过，请勿再次提交");
                        return response;
                    }
                    if (status == UserTaskStatusEnum.AWARD_SUCCESS.getCode()) {
                        response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO4);
                        response.setMessage("任务已完成，请勿再次提交");
                        return response;
                    }
                }
            }
            goodsId = activityDB.getGoodsId();
        }
        // 匹配数据库的淘宝商品ID
        Goods goods = goodsService.selectById(goodsId);
        // 判断商品url是否正确
        String numIidStr = TaobaoUtil.getIdFromGoodsUrl(goodsUrl);
        if (StringUtils.isNotBlank(numIidStr)) {
            if (!StringUtils.equals(numIidStr, goods.getXxbtopNumIid())) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO6);
                response.setMessage("请上传正确的商品地址");
                return response;
            }
        } else {
            if (!TaobaoUtil.containGoodsFromTpwd(goodsUrl, goods.getXxbtopNumIid())) {
                response.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO7);
                response.setMessage("请上传正确的商品链接");
                return response;
            }
        }
        // 信息加工入库
        UserTask userTask = new UserTask();
        userTask.setTaskGoodsUrl(goodsUrl);
        userTask.setReceiveTime(new Date());
        userTask.setUtime(new Date());
        if (userTaskId == null) {
            userTask.setUserId(uid);
            userTask.setActivityId(aid);
            Activity activity = activityService.findById(aid);
            if (activity != null && activity.getActivityDefId() != null) {
                ActivityDef activityDef = activityDefService.selectByPrimaryKey(activity.getActivityDefId());
                boolean v2Def = ActivityDefTypeEnum.ASSIST_V2.getCode() == activityDef.getType()
                        || ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == activityDef.getType();
                if (!v2Def && activity.getWeighting() != null && activity.getWeighting() == WeightingEnum.NO.getCode()) {
                    userTask.setStatus(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
                }
            }
            int rows = userTaskService.updateByUidAndAid(userTask);
            if (rows == 0) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("数据库更新失败：没有此用户任务记录");
                return response;
            }
        } else {
            UserTask userTask1 = userTaskService.selectByPrimaryKey(userTaskId);
            Activity activity = activityService.findById(userTask1.getActivityId());
            if (activity != null && activity.getActivityDefId() != null) {
                ActivityDef activityDef = activityDefService.selectByPrimaryKey(activity.getActivityDefId());
                boolean v2Def = ActivityDefTypeEnum.ASSIST_V2.getCode() == activityDef.getType()
                        || ActivityDefTypeEnum.ORDER_BACK_V2.getCode() == activityDef.getType();
                if (!v2Def && activity.getWeighting() != null && activity.getWeighting() == WeightingEnum.NO.getCode()) {
                    userTask.setStatus(UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
                }
            }
            userTask.setId(userTaskId);
            boolean update = userTaskService.updateById(userTask);
            if (!update) {
                response.setStatusEnum(ApiStatusEnum.ERROR_DB);
                response.setMessage("数据库更新失败：没有此用户任务记录");
                return response;
            }
        }
        stringRedisTemplate.delete(RedisKeyConstant.ACTIVITY_TASK_COMMIT_GOODSURL_FORBID_REPEAT + uid);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("goodsUrl", goodsUrl);
        resultMap.put("status", UserTaskStatusEnum.AUDIT_SUCCESS.getCode());
        response.setData(resultMap);
        response.setStatusEnum(ApiStatusEnum.SUCCESS);
        response.setMessage("商品审核成功");
        return response;
    }

    /***
     * 任务列表展现
     * 红包活动计时规则:
     * 1.从领取任务或者从任务空间里点击进到相关红包任务页面时开始计时
     * 2.提交审核后终止计时
     * 3.审核未通过重新开始计时
     * 4.审核未通过且过时  库存释放
     * @param merchantID
     * @param shopID
     * @param userID
     * @param goodsId
     * @param type
     * @return
     */
    @Override
    public SimpleResponse queryNotActivityTask(Long merchantID,
                                               Long shopID,
                                               Long taskID,
                                               String orderId,
                                               Integer recommitType,
                                               Long userID,
                                               Long goodsId,
                                               int type) {
        SimpleResponse simpleResponse = new SimpleResponse();
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该店铺不存在");
        }
        //查询商品信息
        Goods goods = this.goodsService.selectById(goodsId);
        if (goods == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该商品不存在");
        }

        UserTask userTask = null;
        ActivityTask activityTask = null;
        UserActivity userActivity = null;
        ActivityOrder activityOrder = null;
        boolean existActivityOrder = false;

        int userActivityType = 0;
        if (UserTaskTypeEnum.GOOD_COMMENT.getCode() == type) {
            userActivityType = UserActivityTypeEnum.GOODS_COMMENT.getCode();
        } else if (UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
            userActivityType = UserActivityTypeEnum.FAV_CART.getCode();
        } else if (UserTaskTypeEnum.GOOD_COMMENT_V2.getCode() == type) {
            userActivityType = UserActivityTypeEnum.GOODS_COMMENT_V2.getCode();
        } else if (UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
            userActivityType = UserActivityTypeEnum.FAV_CART_V2.getCode();
        }
        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userID, shopID, merchantID);
        boolean existTaskId = false;
        if (taskID != null && taskID > 0) {
            existTaskId = true;
        }

        if (!existTaskId) {
            List<ActivityTask> activityTasks = this.getActivityTasks(merchantID, shopID, goodsId, type);
            if (CollectionUtils.isEmpty(activityTasks)) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO7.getCode(), "该活动未创建！");
            }
            if (!CollectionUtils.isEmpty(activityTasks)) {
                activityTask = activityTasks.get(0);
                if (activityTask == null || activityTask.getStatus() == ActivityTaskStatusEnum.DEL.getCode()) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该活动已下线！");
                }
            }
        }


        TaskResp resp = new TaskResp();
        BeanUtils.copyProperties(commissionResp, resp);
        if (taskID != null && taskID > 0) {
            userTask = this.userTaskService.selectByPrimaryKey(taskID);
            if (userTask == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO5.getCode(), "该活动任务不存在");
            }
            activityTask = this.activityTaskService.getById(userTask.getActivityTaskId());
            if (StringUtils.isNotBlank(userTask.getOrderId())) {
                List<ActivityOrder> activityOrders = this.activityOrderService.selectByOrderIdOne(userTask.getMerchantId(), userTask.getShopId(), userTask.getUserId(), userTask.getGoodsId(), userTask.getOrderId());
                if (!CollectionUtils.isEmpty(activityOrders)) {
                    activityOrder = activityOrders.get(0);
                }
            }
            if (userTask.getUserActivityId() == null || userTask.getUserActivityId() <= 0) {
                userActivity = this.userActivityService.insertUserActivityByRedPack(userID, merchantID, shopID, 0L, userActivityType, UserActivityStatusEnum.RED_PACKAGE_ING.getCode(), activityTask.getId());
            } else {
                userActivity = this.userActivityService.selectById(userTask.getUserActivityId());
            }
        } else {
            //先查找，若没，则自我创建userTask
            List<ActivityOrder> activityOrders = this.getActivityOrders(merchantID, shopID, goodsId, userID, orderId);
            List<UserTask> userTasks = this.getUserTask(merchantID, shopID, goodsId, userID, type, orderId, activityTask.getId());
            if (CollectionUtils.isEmpty(userTasks)) {
                if (!CollectionUtils.isEmpty(activityOrders) && StringUtils.isBlank(orderId)) {
                    orderId = activityOrders.get(0).getOrderId();
                    existActivityOrder = true;
                    activityOrder = activityOrders.get(0);
                }
                //查询关键词
                String keywords = "";
                if (UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
                    keywords = this.getRedPackFavCart(merchantID, goodsId);
                    if (keywords == null) {
                        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该红包任务商品没有关键词");
                    }
                }
                //创建userTask流程
                userActivity = this.userActivityService.insertUserActivityByRedPack(userID, merchantID, shopID, 0L, userActivityType, UserActivityStatusEnum.RED_PACKAGE_ING.getCode(), activityTask.getId());
                CommissionBO commissionBO = this.commissionService.commissionCalculate(userID, activityTask.getRpAmount());
                userTask = this.userTaskService.insertUserTaskByRedPackTask(merchantID, shopID, 0L, userID, UserTaskStatusEnum.INIT.getCode(), type, keywords, goodsId, orderId, activityTask.getId(), userActivity.getId(), commissionBO.getCommission());
            } else {
                //有userTask 1:有订单 2：没订单
                userTask = userTasks.get(0);
                if (StringUtils.isBlank(userTask.getOrderId())) {
                    if (!CollectionUtils.isEmpty(activityOrders)) {
                        orderId = activityOrders.get(0).getOrderId();
                        existActivityOrder = true;
                        activityOrder = activityOrders.get(0);

                        userTask.setOrderId(orderId);
                        userTask.setUtime(new Date());
                        this.userTaskService.updateById(userTask);
                    }
                }
                activityTask = this.activityTaskService.getById(userTask.getActivityTaskId());
                //userActivity的逻辑
                if (userTask.getUserActivityId() == null || userTask.getUserActivityId() <= 0) {
                    userActivity = this.userActivityService.insertUserActivityByRedPack(userID, merchantID, shopID, 0L, userActivityType, UserActivityStatusEnum.RED_PACKAGE_ING.getCode(), userTask.getActivityTaskId());
                    userTask.setUserActivityId(userActivity.getId());
                    this.userTaskService.updateById(userTask);
                } else {
                    userActivity = this.userActivityService.selectById(userTask.getUserActivityId());
                }
            }

        }
        if (recommitType != null && recommitType > 0) {
            if (activityTask != null) {
                ActivityDef activityDef = this.activityDefService.selectByPrimaryKey(activityTask.getActivityDefId());
                if (activityDef == null) {
                    return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该活动已下线！");
                } else {
                    if (Arrays.asList(ActivityDefStatusEnum.REFUNDED.getCode(), ActivityDefStatusEnum.CLOSE.getCode(),
                            ActivityDefStatusEnum.END.getCode()).contains(activityDef.getStatus())) {
                        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO6.getCode(), "该活动已下线！");
                    }
                    if (activityDef.getStock() != null && activityDef.getStock() <= 0) {
                        return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "库存不足了");
                    }
                }
            }
            userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
            userTask.setUtime(new Date());
            if (TaskResp.recommitType_timeout == recommitType) {
                //若是超时，则重置开始时间
                userTask.setStartTime(new Date());
            }
            this.userTaskService.update(userTask);
            userActivity.setStatus(UserActivityStatusEnum.RED_PACKAGE_ING.getCode());
            userActivity.setUtime(new Date());
            this.userActivityService.updateById(userActivity);
        }
        /**用过就更新*/
        if (existActivityOrder) {
            activityOrder.setStatus(ActivityOrderStatusEnum.AUDITED_USEED.getCode());
            this.activityOrderService.updateById(activityOrder);
        }
        //税率
        BigDecimal rate = this.activityCommissionService.getCommissionRate();
        List<ActivityOrder> userTaskActivityOrders = this.getActivityOrders(merchantID, shopID, goodsId, userID, orderId);
        if (!CollectionUtils.isEmpty(userTaskActivityOrders) && userTaskActivityOrders.get(0).getPayTime() != null) {
            resp.setBuyTime(DateFormatUtils.format(userTaskActivityOrders.get(0).getPayTime(), "yyyy.MM.dd HH:mm"));
        }

        boolean redisUserTaskRedPack = this.redisService.getRedisUserTaskRedPack(userTask.getId());
        resp.fromUserTask(userTask, null, redisUserTaskRedPack);
        resp.fromPics(userTask, null, null, activityTask, rate);
        resp.fromShop(merchantShop);

        resp.setCommission(userTask.getRpAmount().setScale(2, BigDecimal.ROUND_DOWN).toString());

        //今日预计可赚收益
        Map<String, Object> map = oneAppService.queryNewcomerTaskInfo(shopID, userID);
        BigDecimal estimateProfit = BigDecimal.ZERO;
        if (map != null) {
            if (map.get("newbieAmount") != null) {
                estimateProfit = estimateProfit.add(new BigDecimal(map.get("newbieAmount").toString()));
            }
            if (map.get("shopManagerAmount") != null) {
                estimateProfit = estimateProfit.add(new BigDecimal(map.get("shopManagerAmount").toString()));
            }
            if (map.get("dailyAmount") != null) {
                estimateProfit = estimateProfit.add(new BigDecimal(map.get("dailyAmount").toString()));
            }
            resp.setEstimateProfit(estimateProfit.setScale(2, BigDecimal.ROUND_DOWN).toString());
        }

        //任务倒计时
        String costStr = redisService.getUserTaskSupplyAuditCost(userTask.getId());
        resp.fromUserTaskCountdown(userTask, null, null, activityTask, NumberUtils.toLong(costStr));
        resp.fromGoods(goods);
        simpleResponse.setData(resp);
        return simpleResponse;
    }

    /***
     * 2期查询活动任务
     * @param merchantID
     * @param shopID
     * @param userID
     * @param activityID
     * @return
     */
    @Override
    public SimpleResponse queryActivityTask(Long merchantID, Long shopID, Long userID, Integer recommitType, Long activityID) {
        SimpleResponse d = new SimpleResponse();
        if (activityID == null || activityID == 0 || userID == null || userID == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参传入异常");
        }
        MerchantShop merchantShop = this.merchantShopService.selectMerchantShopById(shopID);
        if (merchantShop == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "该店铺不存在");
        }
        Activity activity = this.activityService.findById(activityID);
        if (activity == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "该活动已不存在");
        }
        Goods goods = this.goodsService.selectById(activity.getGoodsId());
        if (goods == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该活动对应的商品库不存在");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("merchantId", merchantID);
        map.put("shopId", shopID);
        map.put("activityId", activityID);
        map.put("userId", userID);
        //-1: 中奖任务过期 0：未提交  1审核中，-2：审核失败，2：审核通过/未发奖， 3: 发奖成功
        map.put("statusList", Arrays.asList(0, 1, -2, 2, 3));
        List<UserTask> userTasks = this.userTaskService.query(map);
        if (userTasks == null || userTasks.size() == 0) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO3.getCode(), "该中奖任务不存在，你没有中奖任务资格");
        }
        UserTask userTask = userTasks.get(0);
        // 百川修改 用户执行任务的开始时间 begin
        Date now = new Date();
        if (null == userTask.getStartTime()) {
            userTask.setStartTime(now);
            userTask.setUtime(now);
            int updateRows = userTaskService.update(userTask);
            if (updateRows == 0) {
                log.error("user_task表，更新开始执行任务时间失败");
                return new SimpleResponse(ApiStatusEnum.ERROR_DB.getCode(), "网络异常，请稍候重试");
            }
        }
        // 百川修改 用户执行任务的开始时间 end

        /**失败重新提交功能*/
        if (recommitType != null && recommitType > 0) {
            //更新usertask
            userTask.setStatus(UserTaskStatusEnum.INIT.getCode());
            userTask.setUtime(now);
            this.userTaskService.update(userTask);
        }

        ActivityDef activityDef = null;
        ActivityPlan activityPlan = null;
        if (activity.getActivityDefId() != null && activity.getActivityDefId() > 0) {
            activityDef = this.activityDefService.selectByPrimaryKey(activity.getActivityDefId());
        } else if (activity.getActivityPlanId() != null && activity.getActivityPlanId() > 0) {
            activityPlan = this.activityPlanService.findById(activity.getActivityPlanId());
        }

        /*
         * 当前佣金关卡数
         */
        CommissionResp commissionResp = this.commissionService.commissionCheckpoint(userID, shopID, merchantID);

        TaskResp resp = new TaskResp();
        BeanUtils.copyProperties(commissionResp, resp);
        resp.setCommission(userTask.getRpAmount().setScale(2, BigDecimal.ROUND_DOWN).toString());
        resp.fromShop(merchantShop);
        resp.fromUserTask(userTask, activity, false);
        resp.fromPics(userTask, activityDef, activityPlan, null, null);
        String costStr = redisService.getUserTaskSupplyAuditCost(userTask.getId());
        resp.fromUserTaskCountdown(userTask, null, activity, null, NumberUtils.toLong(costStr));
        resp.fromGoods(goods);
        d.setData(resp);
        return d;
    }

    /***********************私有方法********************************************/
    //验证图片格式
    private static boolean checkFileType(String[] allow, String fileName) {
        Iterator<String> type = Arrays.asList(allow).iterator();
        while (type.hasNext()) {
            String ext = type.next();
            if (fileName.toLowerCase().endsWith(ext)) {
                return true;
            }
        }
        return false;
    }

    /***
     * 获取关键词信息
     * @param merchantID
     * @param goodsId
     * @return
     */
    private String getRedPackFavCart(Long merchantID, Long goodsId) {
        String keywords = "";
        QueryWrapper<GoodsPlus> goodsPlusWrapper = new QueryWrapper<>();
        goodsPlusWrapper.lambda().eq(GoodsPlus::getMerchantId, merchantID).eq(GoodsPlus::getGoodsId, goodsId)
                .eq(GoodsPlus::getType, GoodsPlusTypeEnum.KEY_WORDS.getCode())
        ;
        GoodsPlus goodsPlus = this.goodsPlusService.getOne(goodsPlusWrapper);
        if (goodsPlus == null) {
            return null;
        }
        List<String> kwds = JSONObject.parseArray(goodsPlus.getValue(), String.class);
        //随机取一个
        if (kwds.size() == 1) {
            keywords = kwds.get(0);
        } else {
            keywords = kwds.get(NumberUtils.toInt(RobotUserUtil.getRandom(0, kwds.size() - 1)));
        }
        return keywords;
    }

    /***
     * 获取activityTask
     * @param merchantID
     * @param shopID
     * @param goodsId
     * @param type
     * @return
     */
    private List<ActivityTask> getActivityTasks(Long merchantID, Long shopID, Long goodsId, int type) {
        QueryWrapper<ActivityTask> activityTaskWrapper = new QueryWrapper<>();
        activityTaskWrapper.lambda().eq(ActivityTask::getMerchantId, merchantID).eq(ActivityTask::getShopId, shopID)
                .eq(ActivityTask::getGoodsId, goodsId).eq(ActivityTask::getStatus, ActivityTaskStatusEnum.PUBLISHED.getCode())
                .eq(ActivityTask::getDel, DelEnum.NO.getCode())
        ;
        if (UserTaskTypeEnum.GOOD_COMMENT.getCode() == type) {
            activityTaskWrapper.and(params -> params.eq("type", ActivityTaskTypeEnum.GOOD_COMMENT.getCode()));
        } else if (UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
            activityTaskWrapper.and(params -> params.eq("type", ActivityTaskTypeEnum.FAVCART.getCode()));
        }
        activityTaskWrapper.orderByDesc("id");
        return this.activityTaskService.list(activityTaskWrapper);
    }

    /***
     * 获取活动订单信息
     * @param merchantID
     * @param shopID
     * @param goodsId
     * @param userID
     * @return
     */
    private List<ActivityOrder> getActivityOrders(Long merchantID, Long shopID, Long goodsId, Long userID, String orderId) {
        QueryWrapper<ActivityOrder> activityOrderWrapper = new QueryWrapper<>();
        activityOrderWrapper.lambda()
                .eq(ActivityOrder::getMerchantId, merchantID)
                .eq(ActivityOrder::getShopId, shopID)
                .eq(ActivityOrder::getGoodsId, goodsId)
                .eq(ActivityOrder::getUserId, userID)
                .eq(ActivityOrder::getStatus, ActivityOrderStatusEnum.AUDITED.getCode())
                .orderByDesc(ActivityOrder::getCtime)
        ;
        if (StringUtils.isNotBlank(orderId)) {
            activityOrderWrapper.and(params -> params.eq("order_id", orderId));
        }
        return this.activityOrderService.list(activityOrderWrapper);
    }

    /***
     * 获取userTask
     * @param merchantID
     * @param shopID
     * @param goodsId
     * @param userID
     * @param type
     * @return
     */
    private List<UserTask> getUserTask(Long merchantID, Long shopID, Long goodsId, Long userID, int type, String activityOrderId, Long activityTaskId) {
        QueryWrapper<UserTask> userTaskWrapper = new QueryWrapper<>();
        userTaskWrapper.lambda()
                .eq(UserTask::getMerchantId, merchantID)
                .eq(UserTask::getShopId, shopID)
                .eq(UserTask::getGoodsId, goodsId)
                .eq(UserTask::getUserId, userID)
                .eq(UserTask::getActivityTaskId, activityTaskId)
                .orderByDesc(UserTask::getCtime)
        ;
        if (UserTaskTypeEnum.GOOD_COMMENT.getCode() == type) {
            userTaskWrapper.and(params -> params.eq("type", UserTaskTypeEnum.GOOD_COMMENT.getCode()));
        } else if (UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode() == type) {
            userTaskWrapper.and(params -> params.eq("type", UserTaskTypeEnum.REDPACK_TASK_FAV_CART.getCode()));
        }
        if (StringUtils.isNotBlank(activityOrderId)) {
            userTaskWrapper.and(params -> params.eq("order_id", activityOrderId));
        }
        return this.userTaskService.list(userTaskWrapper);
    }
}
