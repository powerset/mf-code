package mf.code.api.applet.login.domain.valueobject;

import lombok.Data;
import mf.code.uactivity.repo.po.UserPubJoin;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.api.applet.login.domain.valueobject
 * Description:
 *
 * @author: 百川
 * @date: 2019-02-22 下午5:40
 */
@Data
public class InvitationHistory {

	private Long platformId;

	/**
	 * 邀请者
	 * Map<UserId, List<UserPubJoin>>
	 */
	private Map<String, List<UserPubJoin>> records = new HashMap<>();

	public UserPubJoin queryUserPubJoinByUserId(Long userId) {
		List<UserPubJoin> userPubJoins = this.records.get(userId.toString());
		return CollectionUtils.isEmpty(userPubJoins)? null: userPubJoins.get(0);
	}
}
