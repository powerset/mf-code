package mf.code.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.api.applet.service.AssistService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * mf.code.api.applet
 *
 * @description: 助力活动相关
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2018年12月25日 09:01
 */
@Slf4j
@RestController
@RequestMapping("/api/applet/assist")
public class AssistApi {
    @Autowired
    private AssistService assistService;

    /**
     * 功能描述: 助力详情页
     *
     * @param: 活动id，用户id
     * @param: scene，场景id 1：分享进入(若该活动已失效，自动跳转下个活动)
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @date: 2018/10/29 0029 21:22
     * @return:
     */
    @RequestMapping(path = "/queryAssistActivity", method = RequestMethod.GET)
    public SimpleResponse queryAssistActivity(@RequestParam(name = "activityId") Long activityId,
                                              @RequestParam(name = "userId") Long userId,
                                              @RequestParam(name = "scene", required = false, defaultValue = "0") Integer sceneId) {
        return this.assistService.queryAssistActivity(activityId, userId, sceneId);
    }

    /***
     * 查询助力类活动参与人数
     * @param activityId
     * @param userId
     * @auther: yechen
     * @Email: wangqingfeng@wxyundian.com
     * @return
     */
    @RequestMapping(path = "/queryActivityJoin", method = RequestMethod.GET)
    public SimpleResponse queryActivityJoin(@RequestParam(name = "activityId") Long activityId,
                                            @RequestParam(name = "userId") Long userId) {
        return this.assistService.queryActivityJoin(activityId, userId);
    }

    /***
     * 弹幕相关
     * @param lastId
     * @param limit
     * @param merchantId
     * @param shopId
     * @param userId
     * @return
     */
    @RequestMapping(path = "/getBarrage", method = RequestMethod.GET)
    public SimpleResponse getBarrage(@RequestParam(name = "lastId",defaultValue = "0") Long lastId,
                                     @RequestParam(name = "limit",defaultValue = "5") int limit,
                                     @RequestParam(name = "merchantId") Long merchantId,
                                     @RequestParam(name = "shopId") Long shopId,
                                     @RequestParam(name = "userId", required = false) Long userId) {
        return this.assistService.getBarrage(merchantId, shopId, lastId, limit);
    }

    /**
     * 查询活动进度及剩余库存信息
     * @param activityId
     * @return
     */
    @GetMapping("/queryAssistStockInfo")
    public SimpleResponse queryAssistStockInfo(@RequestParam("activityId") String activityId) {
        // 获取活动定义信息
        if (StringUtils.isBlank(activityId) || !RegexUtils.StringIsNumber(activityId)) {
            log.error("活动id 为空");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "活动id 为空");
        }
        return this.assistService.queryAssistStockInfo(Long.valueOf(activityId));
    }
}
