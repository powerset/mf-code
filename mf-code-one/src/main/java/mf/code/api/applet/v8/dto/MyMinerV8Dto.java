package mf.code.api.applet.v8.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import mf.code.api.applet.v3.enums.CheckpointTaskSpaceEnum;
import mf.code.common.repo.po.CommonDict;
import mf.code.uactivity.repo.po.UserPubJoin;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.one.dto
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年06月15日 14:16
 */
@Data
public class MyMinerV8Dto {
    //该关卡目标对象信息
    private Checkpoint checkpoint;
    //任务收益
    private String taskProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //缴税收益
    private String scottareProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
    //昵称
    private String nickName;
    //头像
    private String avatarUrl;
    //矿工缴税税率
    private String scottareRate;
    //弹窗对象信息
    private List<DiaLogInfo> dialogInfo;
    //所有关卡信息
    private List<Object> allCheckpoint;
    //是否是店长
    private int role;
    //该用户的矿工数
    private int minerNum;
    //可提现金额
    private String cashMoney;
    /***
     * 新人宝箱任务对象展现
     */
    private List<NewbieTask> newbieTasks;


    //所有关卡信息
    public void fromAllCheckpoint(List<CommonDict> commonDicts) {
        for (CommonDict commonDict : commonDicts) {
            if ("level".equals(commonDict.getKey())) {
                this.allCheckpoint = JSONObject.parseArray(commonDict.getValue(), Object.class);
            }
        }
    }

    //获取税率
    public void fromRate(List<CommonDict> commonDicts) {
        for (CommonDict commonDict : commonDicts) {
            if ("rate".equals(commonDict.getKey())) {
                this.scottareRate = commonDict.getValue() + "%";
            }
        }
    }

    //获取该用户信息
    public void fromUser(User user) {
        this.nickName = user.getNickName();
        this.avatarUrl = user.getAvatarUrl();
        this.role = user.getRole();
    }

    /***
     * 获取收益 -当前关卡的
     * @param userPubJoin 本人收益
     * @param scottareProfit 下级上缴税收信息
     * @param commonDicts 字典表闯关信息
     */
    public void fromProfit(UserPubJoin userPubJoin, BigDecimal scottareProfit, List<CommonDict> commonDicts) {
        CommonDict commonDict = null;
        for (CommonDict commonDict1 : commonDicts) {
            if ("level".equals(commonDict1.getKey())) {
                commonDict = commonDict1;
            }
        }
        //用户此时的闯关的总累计收益
        BigDecimal userProfit = BigDecimal.ZERO;
        BigDecimal totalCommission = BigDecimal.ZERO;
        if (userPubJoin != null && userPubJoin.getTotalCommission().compareTo(BigDecimal.ZERO) > 0) {
            totalCommission = userPubJoin.getTotalCommission();
        }
        userProfit = userProfit.add(totalCommission);

        BigDecimal userTotalProfit = userProfit.add(scottareProfit);

        if (commonDict != null) {
            List<Object> commonDictObjects = JSONObject.parseArray(commonDict.getValue(), Object.class);
            BigDecimal overAmount = BigDecimal.ZERO;
            for (Object obj : commonDictObjects) {
                //[{"value":"不屈黑铁","money":0,"name":1},{"value":"倔强青铜","money":8.8,"name":2}]
                JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                BigDecimal checkpointMoney = new BigDecimal(jsonMap.get("money").toString());
                overAmount = overAmount.add(checkpointMoney);
                if (userTotalProfit.compareTo(overAmount) > 0) {
                    userTotalProfit = userTotalProfit.subtract(overAmount).setScale(2, BigDecimal.ROUND_DOWN);
                    if (userProfit.subtract(overAmount).compareTo(BigDecimal.ZERO) > 0) {
                        userProfit = userProfit.subtract(overAmount).setScale(2, BigDecimal.ROUND_DOWN);
                    } else {
                        if (userProfit.compareTo(BigDecimal.ZERO) < 0) {
                            scottareProfit = scottareProfit.add(userProfit);
                        }else {
                            scottareProfit = scottareProfit.subtract(overAmount).setScale(2, BigDecimal.ROUND_DOWN);
                        }
                    }
                } else {
                    this.taskProfit = userProfit.setScale(2, BigDecimal.ROUND_DOWN).toString();
                    this.scottareProfit = scottareProfit.setScale(2, BigDecimal.ROUND_DOWN).toString();
                    break;
                }
            }
        }
    }

    //获取弹窗相关信息
    @Data
    public static class DiaLogInfo {
        /***
         * 又有矿工为您产生10.5的收益(存redis，不删除的那种)
         * 恭喜您，完成了收藏加购任务，获得0.6元 今日累计任务收益xx元(存redis，完成时候就存储)
         * 恭喜你完成了x个任务，共获得x元，并有矿工为你产生10.5元收益;(多个任务都完成了，同时又有矿工为其增加收益)(当有多个任务的时候)
         *
         * 恭喜您，完成第一关，获得“倔强青铜”称号，8.8元闯关奖金已发至钱包
         * 再接再厉！还差0.5元！获得“倔强青铜”称号即可获得提现奖金8.8元(完成百分之50和百分之90的时候 各弹窗一次)
         */
        //增加的收益(大于0才弹窗)
        private String addProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        //完成任务的个数
        private int taskNum;
        //任务名称
        private String taskName;
        //完成任务的总金额
        private String taskAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //今日累计任务收益
        private String todayTaskProfit = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        //称号
        private String checkpointName;
        //称号的对应目标金额
        private String checkpointGoalAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();
        //称号对应的关卡数
        private int checkpointNum;

        //差额
        private String differenceAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        private int rate;

        //类型
        private int type;

        public void fromAddScottareProfit(String addScottareProfit) {
            if (StringUtils.isNotBlank(addScottareProfit)) {
                this.addProfit = addScottareProfit;
            }
        }

        public void fromUserCheckPointTask(String userCheckPointTask) {
            if (StringUtils.isNotBlank(userCheckPointTask)) {
                List<Object> objects = JSONObject.parseArray(userCheckPointTask, Object.class);
                if (!CollectionUtils.isEmpty(objects)) {
                    this.taskNum = objects.size();
                    BigDecimal taskMoney = BigDecimal.ZERO;
                    for (Object obj : objects) {
                        JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                        Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                        if (objects.size() == 1 && jsonMap != null) {
                            this.taskName = CheckpointTaskSpaceEnum.findByDesc((Integer) jsonMap.get("type"));
                        }
                        if (jsonMap != null && jsonMap.get("amount") != null) {
                            taskMoney = taskMoney.add(new BigDecimal(jsonMap.get("amount").toString()));
                        }
                    }
                    this.addProfit = taskMoney.setScale(2, BigDecimal.ROUND_DOWN).toString();
                }
            }
        }

        public void fromUserCheckPointFinish(String userCheckPointFinish) {
            if (StringUtils.isNotBlank(userCheckPointFinish)) {
                JSONObject jsonObject = JSONObject.parseObject(userCheckPointFinish);
                Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                if (jsonMap != null) {
                    this.checkpointName = jsonMap.get("value").toString();
                    this.checkpointGoalAmount = new BigDecimal(jsonMap.get("money").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString();
                    this.checkpointNum = (int) jsonMap.get("name");
                }
            }
        }
    }

    @Data
    public static class Checkpoint {
        //关卡数
        private int checkpoint;
        //关卡目标金额
        private String checkpointGoalMoney = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN).toString();

        //获取目标金额
        public void fromCheckpointGoalMoney(List<CommonDict> commonDicts, UserPubJoin userPubJoin, BigDecimal scottareProfit) {
            //任务收益+缴税总收益
            BigDecimal totalAmount = BigDecimal.ZERO;
            if (userPubJoin != null) {
                totalAmount = userPubJoin.getTotalCommission().add(scottareProfit);
            }
            CommonDict commonDict = null;
            for (CommonDict commonDict1 : commonDicts) {
                if ("level".equals(commonDict1.getKey())) {
                    commonDict = commonDict1;
                }
            }
            if (commonDict != null) {
                List<Object> objects = JSONObject.parseArray(commonDict.getValue(), Object.class);
                if (!CollectionUtils.isEmpty(objects)) {
                    BigDecimal overAmount = BigDecimal.ZERO;
                    for (Object obj : objects) {
                        //[{"value":"不屈黑铁","money":0,"name":1},{"value":"倔强青铜","money":8.8,"name":2}]
                        JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                        Map<String, Object> jsonMap = JSONObject.toJavaObject(jsonObject, Map.class);
                        BigDecimal checkpointMoney = new BigDecimal(jsonMap.get("money").toString());
                        overAmount = overAmount.add(checkpointMoney);
                        if (totalAmount.compareTo(overAmount) < 0) {
                            this.setCheckpoint((Integer) jsonMap.get("name"));
                            this.setCheckpointGoalMoney(new BigDecimal(jsonMap.get("money").toString()).setScale(2, BigDecimal.ROUND_DOWN).toString());
                            break;
                        }
                    }
                }
            }
        }
    }
}
