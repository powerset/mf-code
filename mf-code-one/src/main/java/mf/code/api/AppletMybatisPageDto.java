package mf.code.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * mf.code.api
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年02月13日 10:27
 */
@Data
public class AppletMybatisPageDto<T> {

    private List<T> content = new ArrayList<>();
    private long limit;// 每页条数
    private long offset;// 偏移量
    @JsonProperty(value = "isPullDown")
    private boolean pullDown;// 是否可以下拉

    public AppletMybatisPageDto() {
    }

    public AppletMybatisPageDto(long size, long offset) {
        this.limit = size;
        this.offset = offset;
        this.pullDown = false;
        this.content = new ArrayList<>();
    }

    public void from(long size, int offset, int pullDown) {
        this.limit = size;
        this.offset = offset;
        if (pullDown > 0) {
            this.pullDown = true;
        }
    }
}
