package mf.code.api.common.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.common.dto.PupOrderReq;
import mf.code.api.common.service.PubUserOrderService;
import mf.code.common.caller.wxpay.WeixinPayConstants;
import mf.code.common.caller.wxpay.WeixinPayService;
import mf.code.common.caller.wxpay.WxpayProperty;
import mf.code.common.caller.wxpublic.WeixinPublicHelper;
import mf.code.common.constant.MerchantFissionEnum;
import mf.code.common.repo.po.CommonDict;
import mf.code.common.service.CommonDictService;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.*;
import mf.code.merchant.constants.BizTypeEnum;
import mf.code.merchant.repo.enums.*;
import mf.code.merchant.repo.po.MerchantInviteTemp;
import mf.code.merchant.repo.po.MerchantOrder;
import mf.code.merchant.service.MerchantInviteTempService;
import mf.code.merchant.service.MerchantOrderService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * mf.code.api.common.service.impl
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年03月07日 18:49
 */
@Service
@Slf4j
public class PubUserOrderServiceImpl implements PubUserOrderService {
    private final CommonDictService commonDictService;
    private final MerchantOrderService merchantOrderService;
    private final StringRedisTemplate stringRedisTemplate;
    private final MerchantInviteTempService merchantInviteTempService;
    private final WeixinPayService weixinPayService;
    private final WxpayProperty wxpayProperty;
    private final WeixinPublicHelper weixinPublicHelper;

    @Value("${merchantPay.debug.mode}")
    private Integer debugMode;

    /**
     * 公众号用户邀请人购买课程返现key
     */
    public static final String PUB_PRESENT_MONEY = "teacher:presentMoney:";//<id>

    //商户订单的redis临时存储
    public static final String MERCHANTORDERREDISKEY = "order:merchantorder:";

    @Autowired
    public PubUserOrderServiceImpl(WxpayProperty wxpayProperty, WeixinPayService weixinPayService, MerchantInviteTempService merchantInviteTempService, StringRedisTemplate stringRedisTemplate, CommonDictService commonDictService, MerchantOrderService merchantOrderService, WeixinPublicHelper weixinPublicHelper) {
        this.commonDictService = commonDictService;
        this.merchantOrderService = merchantOrderService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.merchantInviteTempService = merchantInviteTempService;
        this.weixinPayService = weixinPayService;
        this.wxpayProperty = wxpayProperty;
        this.weixinPublicHelper = weixinPublicHelper;
    }

    /***
     * 创建商户订单
     * @param req
     * @param request
     * @return
     */
    @Override
    public SimpleResponse creatPupOrder(PupOrderReq req, HttpServletRequest request) {
        //验证入参的有效性
        if (req == null || !req.checkValid()) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "入参不满足条件");
        }
        //该用户在公众号内
        MerchantInviteTemp merchantInviteTemp = this.merchantInviteTempService.selectByOpenidAndType(req.getPubOpenId(),
                MerchantFissionEnum.TWO.getCode());
        //是否已经购买过
        List<MerchantOrder> merchantOrders = this.merchantOrderService.queryForPublicFission(merchantInviteTemp.getId());
        if (!CollectionUtils.isEmpty(merchantOrders)) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO4.getCode(), "您已经购买了哟，请勿重复");
        }
        CommonDict commonDict = this.commonDictService.selectByTypeKey("weixinPublic", "pay_menu");
        if (commonDict == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO2.getCode(), "购买订单列表不存在");
        }
        Map<String, Object> jsonMap = JSONObject.parseObject(commonDict.getValue(), Map.class);
        BigDecimal amount = new BigDecimal(jsonMap.get("money").toString());
        Integer bizType = (Integer) jsonMap.get("bizType");
        //是否已经发起过
        String str = this.stringRedisTemplate.opsForValue().get(this.getMerchantOrderRedisKey(req));
        if (StringUtils.isNotBlank(str)) {
            return JSONObject.parseObject(str, SimpleResponse.class);
        }
        //step2:拼装微信统一下单的参数
        //订单号
        String outTraderNo = "pub" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
        //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
        String ipAddress = IpUtil.getIpAddress(request);
        String attach = "支付订单";
        String orderName = "购买千万级讲师讲解社交电商课程-支付订单";

        //step3:储存db
        MerchantOrder merchantOrder = this.merchantOrderService.addMerchantOrderPo(0L, 0L, "",
                null, outTraderNo, orderName, OrderStatusEnum.ORDERING.getCode(), amount, null, attach,
                null, null, null, null, null, ipAddress, null, null);
        merchantOrder.setTradeType(WxTradeTypeEnum.JSAPI.getCode());
        merchantOrder.setBizType(BizTypeEnum.PUBLIC_BUY_COURSE.getCode());
        merchantOrder.setBizValue(merchantInviteTemp.getId());
        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
        merchantOrder.setPayType(OrderPayTypeEnum.WEIXIN.getCode());

        String body = BizTypeEnum.findByDesc(bizType);
        String openID = req.getPubOpenId();//JSAPI支付必须传openid
        Map<String, Object> unifiedorderReqParams = this.weixinPayService.getUnifiedorderReqParams(amount, outTraderNo, ipAddress,
                wxpayProperty.getPubAppId(), WeixinPayConstants.JSAPI, attach, openID, WeixinPayConstants.SELLER_NOTIFY_SOURCE, body);
        merchantOrder.setReqJson(JSON.toJSONString(unifiedorderReqParams));
        merchantOrder.setType(OrderTypeEnum.PALTFORMRECHARGE_PAY.getCode());
        merchantOrder.setPayType(OrderPayTypeEnum.WEIXIN.getCode());
        this.merchantOrderService.save(merchantOrder);

        //step5: 拼接调用微信平台统一下单接口的参数
        //统一下单的返回
        return this.wxPubPay(req, merchantOrder, unifiedorderReqParams);
    }


    /***
     * 提现异步
     * @param openId
     * @param amount
     */
    @Async
    @Override
    public void createMktTransfersOrder(Long bizvalue, String openId, String fromNickName, BigDecimal amount) {
        if (StringUtils.isBlank(openId) || amount.compareTo(BigDecimal.ZERO) <= 0) {
            log.info("公众号购买返现入参不满足条件, id:{} openId:{} amount:{}", bizvalue, openId, amount);
        }

        if (amount.compareTo(BigDecimal.ZERO) == 0 || amount.compareTo(new BigDecimal("2")) < 0) {
            log.info("公众号购买返现-提现金额最低金额2元, id:{} openId:{} amount:{}", bizvalue, openId, amount);
        }

        //redis去重
        String redisKey = PUB_PRESENT_MONEY + bizvalue;
        if (!this.stringRedisTemplate.opsForValue().setIfAbsent(redisKey, "")) {
            log.info("公众号购买返现-提现正在触发，请勿重复, id:{} openId:{} amount:{}", bizvalue, openId, amount);
        }

        try {
            MerchantInviteTemp merchantInviteTemp = this.merchantInviteTempService.selectById(bizvalue);
            if (merchantInviteTemp == null) {
                log.info("公众号购买返现-用户不存在, id:{} openId:{} amount:{}", bizvalue, openId, amount);
            }
            /**
             * step2:调用微信企业支付接口
             */
            //提现描述
            String remark = "您的好友" + fromNickName + "已订购社交电商课程 - 返现20元" + bizvalue;
            Map<String, Object> reqMapParams = this.weixinPayService.getTransferReqParams(openId, null, amount, remark, WeixinPayConstants.PAYSCENE_PUB_APPID);
            Map<String, Object> xmlMap = this.weixinPayService.transfer(reqMapParams);
            log.info("公众号购买返现-微信支付返回：xmlMap {}", xmlMap);
            if (xmlMap == null) {
                log.info("公众号购买返现-提现异常，请咨询客服, id:{} openId:{} amount:{}", bizvalue, openId, amount);
            }
            //通信标识+交易标识  状态都成功时 做数据处理
            boolean returnCode = xmlMap.get("return_code") != null && StringUtils.isNotBlank(xmlMap.get("return_code").toString()) && "FAIL".equals(xmlMap.get("return_code").toString().toUpperCase());
            boolean resultCode = xmlMap.get("result_code") != null && StringUtils.isNotBlank(xmlMap.get("result_code").toString()) && "FAIL".equals(xmlMap.get("result_code").toString().toUpperCase());
            boolean systemErrorCode = xmlMap.get("err_code") != null && StringUtils.isNotBlank(xmlMap.get("err_code").toString().toUpperCase()) && "SYSTEMERROR".equals(xmlMap.get("err_code").toString().toUpperCase());//为这种错误时，需确认是否真的失败
            if (returnCode || resultCode) {
                if (resultCode && systemErrorCode) {
                    //为这种错误时，需确认是否真的失败-需亲自查询订单获取状态
                    boolean checkStatus = this.weixinPayService.checkTransfers(xmlMap.get("nonce_str").toString(), xmlMap.get("sign").toString(), xmlMap.get("partner_trade_no").toString());
                    if (!checkStatus) {
                        //TODO:提现失败，是重试，还是让用户重新走流程
                        log.info("公众号购买返现-微信零钱出现异常，请重新发起提现请求, id:{} openId:{} amount:{}", bizvalue, openId, amount);
                    }
                }
                boolean returnMsg = StringUtils.isNotBlank(xmlMap.get("return_msg").toString());
                boolean errCodeDesc = StringUtils.isNotBlank(xmlMap.get("err_code_des").toString());
                String errMsg = ApiStatusEnum.ERROR_WXPAY.getMessage();
                if (returnMsg) {
                    errMsg = xmlMap.get("return_msg").toString();
                }
                if (errCodeDesc) {
                    errMsg = xmlMap.get("err_code_des").toString();
                }
                log.info("公众号购买返现-微信零钱异常:{}, id:{} openId:{} amount:{}", errMsg, bizvalue, openId, amount);
            }
            //插入成功，设置过期时间
            stringRedisTemplate.expire(redisKey, 4, TimeUnit.SECONDS);

            /**
             * step3:db处理  订单表的处理 订单表-的创建-用户余额的更新
             */
            //订单表的创建
            String orderNo = "pub" + RandomStrUtil.randomStr(6) + System.currentTimeMillis();
            String orderName = fromNickName + "订购社交电商课程，" + merchantInviteTemp.getNickname() + "发起返现";
            //获取ip:阿里云的负载均衡的客户端真是ip放在X-Forwarded-For的头字段里
            String ipAddress = IpUtil.getServerIp();
            MerchantOrder merchantOrder = this.merchantOrderService.addMerchantOrderPo(0L, 0L, merchantInviteTemp.getNickname(),
                    null, orderNo, orderName, OrderStatusEnum.ORDERED.getCode(), amount,
                    xmlMap.get("partner_trade_no").toString(), remark, null, BizTypeEnum.PUBLIC_BUY_COURSE.getCode(), bizvalue, OrderTypeEnum.USERCASH.getCode(),
                    OrderPayTypeEnum.CASH.getCode(), ipAddress, null, JSON.toJSONString(reqMapParams));
            merchantOrder.setPaymentTime(DateUtil.parseDate(xmlMap.get("payment_time").toString(), null, "yyyy-MM-dd HH:mm:ss"));
            this.merchantOrderService.save(merchantOrder);
        } finally {
            //结束时，对key进行删除处理
            this.stringRedisTemplate.delete(redisKey);
        }
        // TODO 推送返现成功消息
        weixinPublicHelper.sendPayBackSuccessMsg(openId);
        log.info("公众号购买返现-成功, id:{} openId:{} amount:{}", bizvalue, openId, amount);
    }

    /***
     * 微信统一下单模块
     * @param merchantOrder
     * @param unifiedorderReqParams
     * @return
     */
    private SimpleResponse wxPubPay(PupOrderReq req, MerchantOrder merchantOrder, Map<String, Object> unifiedorderReqParams) {
        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> unifiedorderRespMap = this.weixinPayService.unifiedorder(unifiedorderReqParams);
        log.info("小程序支付后的结果 xmlMap:{}", unifiedorderRespMap);
        boolean returnCode = unifiedorderRespMap.get("return_code") != null && StringUtils.isNotBlank(unifiedorderRespMap.get("return_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("return_code").toString().toUpperCase());
        boolean resultCode = unifiedorderRespMap.get("result_code") !=
                null && StringUtils.isNotBlank(unifiedorderRespMap.get("result_code").toString()) && "FAIL".equals(unifiedorderRespMap.get("result_code").toString().toUpperCase());
        if (returnCode || resultCode) {
            simpleResponse.setStatusEnum(ApiStatusEnum.ERROR_BUS_NO3);
            if (StringUtils.isNotBlank(unifiedorderRespMap.get("return_msg").toString())) {
                simpleResponse.setMessage(unifiedorderRespMap.get("return_msg").toString());
                merchantOrder.setRemark(unifiedorderRespMap.get("return_msg").toString());
            } else if (StringUtils.isNotBlank(unifiedorderRespMap.get("err_code_des").toString())) {
                simpleResponse.setMessage(unifiedorderRespMap.get("err_code_des").toString());
                merchantOrder.setRemark(unifiedorderRespMap.get("err_code_des").toString());
            } else {
                simpleResponse.setMessage("微信充值异常...");
                merchantOrder.setRemark("微信充值异常...");
            }
            //更新db
            merchantOrder.setUtime(new Date());
            this.merchantOrderService.updateById(merchantOrder);

            return simpleResponse;
        }

        Map<String, Object> respMap = new HashMap<String, Object>();
        respMap.put("timeStamp", String.valueOf(DateUtil.getCurrentSeconds()));
        respMap.put("nonceStr", RandomStrUtil.randomStr(28));
        String packageInfo = "prepay_id=" + unifiedorderRespMap.get("prepay_id");
        respMap.put("package", packageInfo);
        respMap.put("signType", "MD5");
        String sortSign = SortUtil.buildSignStr(respMap);
        String sign = "appId=" + wxpayProperty.getPubAppId() + "&" + sortSign + "&key=" + wxpayProperty.getMchSecretKey();
        log.info("公众号返回前端的sign：{}", sign);
        //注：MD5签名方式
        sign = MD5Util.md5(sign).toUpperCase();
        //签名sign
        respMap.put("paySign", sign);
        respMap.put("orderId", merchantOrder.getId());

        int timeoutMinute = 30;//30分钟过期
        respMap.put("cutoffTime", DateUtils.addMinutes(new Date(), timeoutMinute));

        simpleResponse.setData(respMap);
        //存储redis 30分钟过期
        this.stringRedisTemplate.opsForValue().set(this.getMerchantOrderRedisKey(req), JSON.toJSONString(simpleResponse), timeoutMinute, TimeUnit.MINUTES);
        return simpleResponse;
    }

    /***
     * 订单存储rediskey
     * @param dto
     * @return
     */
    @Override
    public String getMerchantOrderRedisKey(PupOrderReq dto) {
        return MERCHANTORDERREDISKEY + dto.getPubOpenId();
    }
}
