package mf.code.api.common;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.constant.RocketMqTopicTagEnum;
import mf.code.common.simpleresp.SimpleResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.api.common
 * Description:
 *
 * @author: gel
 * @date: 2019-02-26 9:08
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/common/rocketmq")
public class RocketMqApi {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping(value = "/send")
    public SimpleResponse refreshCommonDict(@RequestParam(value = "value", required = false) String value) {
        SimpleResponse simpleResponse = new SimpleResponse();
        if (StringUtils.isBlank(value)) {
            value = "{}";
        }
        rocketMQTemplate.syncSend(RocketMqTopicTagEnum.DEMOTOPIC_ALL.getTopic() + ":" + RocketMqTopicTagEnum.DEMOTOPIC_ALL.getTag(), value);
        return simpleResponse;
    }
}
