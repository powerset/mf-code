package mf.code.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.dto.ProductSkuEntity;
import mf.code.one.dto.MsInfoDTO;
import mf.code.upay.repo.po.Product;
import mf.code.upay.repo.po.ProductSku;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * mf.code.goods.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 11:10
 */
@Component
@Slf4j
public class GoodsAppFeignFallbackFactory implements FallbackFactory<GoodsAppService> {
    @Override
    public GoodsAppService create(Throwable cause) {
        return new GoodsAppService() {
            @Override
            public ProductSkuEntity createSkuForPlatformOrderBack(Long productSkuId, Integer stockDef) {
                return null;
            }

            /**
             * 根据商品id列表查询商品详情，全部字段对应db
             *
             * @param productIdList
             * @return
             */
            @Override
            public List<ProductEntity> queryProductDetailList(List<Long> productIdList) {
                return null;
            }

            @Override
            public List<ProductEntity> queryProductDetailBySkuIds(List<Long> skuIdList) {
                return null;
            }

            @Override
            public List<ProductEntity> listByIds(List<Long> goodsIds) {
                return null;
            }

            @Override
            public List<Product> querySelfProductByShopId(Long shopId, Integer page, Integer size) {
                return null;
            }

            @Override
            public int queryProductCountByShopId(Long shopId, Long merchantId) {
                return 0;
            }

            @Override
            public List<ProductSku> querySkuListByProductIds(List<Long> productIdList) {
                return null;
            }

            @Override
            public List<ProductDistributionDTO> queryOrCreateForFlashSale(MsInfoDTO msInfoDTO) {
                return null;
            }

            @Override
            public SimpleResponse queryProductSkuMapBySkuIdList(List<Long> skuIdList) {
                return null;
            }
        };
    }
}
