package mf.code.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.api.feignclient.DistributionAppService;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.dto.UpayWxOrderReq;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class DistributionAppFeignFallbackFactory implements FallbackFactory<DistributionAppService> {
    @Override
    public DistributionAppService create(Throwable throwable) {
        return new DistributionAppService() {
            @Override
            public SimpleResponse becomeShopManagerV2(UpayWxOrderReq upayWxOrderReq) {
                return null;
            }

            @Override
            public SimpleResponse saveUpayWxOrderPend(List<UpayWxOrderReq> reqs) {
                return null;
            }

            @Override
            public SimpleResponse sendTemplateMessage(String nickName, Long upperId, String commission, String taskName) {
                return null;
            }

            @Override
            public String sumTotalFeeByUpayWxOrderPend(Map<String, Object> params) {
                return null;
            }

            @Override
            public SimpleResponse addIncomeRankListOfShopByUserIds(Long shopId, List<Long> userIds) {
                return null;
            }

            @Override
            public List<UpayWxOrderReq> queryShopManagerPend(Map<String, Object> shopManagerParams) {
                return null;
            }

            @Override
            public String getAmountByShopManagerPay() {
                return "0";
            }
        };
    }
}
