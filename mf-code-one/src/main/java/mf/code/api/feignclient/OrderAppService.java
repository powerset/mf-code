package mf.code.api.feignclient;

import mf.code.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import mf.code.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@FeignClient(name = "mf-code-order", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OrderAppService {

    @GetMapping("/feignapi/order/applet/v5/countGoodsNumByOrder")
    int countGoodsNumByOrder(@RequestParam(name = "goodsId") Long goodsId);

    /**
     * 店铺订单总额-销售金额
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/order/applet/orderTotalFee")
    String orderTotalFeeBySale(@RequestParam("shopId") Long shopId,
                               @RequestParam(name = "begin", required = false) Date begin,
                               @RequestParam(name = "end", required = false) Date end);

    /**
     * 查询订单数
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/feignapi/order/applet/orderNumber")
    Integer orderNumber(@RequestParam("shopId") Long shopId,
                        @RequestParam(name = "begin", required = false) Date begin,
                        @RequestParam(name = "end", required = false) Date end);

    /**
     * 店铺总佣金
     *
     * @param shopId
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/feignapi/order/applet/commissionTotal")
    String commissionTotal(@RequestParam("shopId") Long shopId,
                           @RequestParam(name = "begin", required = false) Date begin,
                           @RequestParam(name = "end", required = false) Date end);
}
