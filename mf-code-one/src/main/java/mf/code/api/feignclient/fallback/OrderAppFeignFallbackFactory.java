package mf.code.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import mf.code.api.feignclient.GoodsAppService;
import mf.code.api.feignclient.OrderAppService;
import mf.code.goods.dto.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {
    @Override
    public OrderAppService create(Throwable throwable) {
        return new OrderAppService() {
            @Override
            public int countGoodsNumByOrder(Long goodsId) {
                return 0;
            }

            @Override
            public String orderTotalFeeBySale(Long shopId, Date begin, Date end) {
                return null;
            }

            @Override
            public Integer orderNumber(Long shopId, Date begin, Date end) {
                return null;
            }

            @Override
            public String commissionTotal(Long shopId, Date begin, Date end) {
                return null;
            }
        };
    }
}
