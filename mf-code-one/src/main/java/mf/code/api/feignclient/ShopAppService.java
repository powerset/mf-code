package mf.code.api.feignclient;

import mf.code.api.feignclient.fallback.ShopAppFeignFallbackFactory;
import mf.code.common.config.feign.FeignLogConfiguration;
import mf.code.merchant.dto.MerchantDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mf.code.web.api.feignclient
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-27 14:28
 */
@FeignClient(name = "mf-code-shop", fallbackFactory = ShopAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface ShopAppService {

    /**
     * 通过 merchantId 获取 抖带带主播信息
     * @param merchantId
     * @return
     */
    @GetMapping("feignapi/shop/applet/queryMerchantById")
    MerchantDTO queryMerchantById(@RequestParam("merchantId") Long merchantId);
}
