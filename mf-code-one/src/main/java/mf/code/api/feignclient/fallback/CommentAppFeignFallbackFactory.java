package mf.code.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.api.feignclient.CommentAppService;
import mf.code.comment.dto.OrderCommentDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommentAppFeignFallbackFactory implements FallbackFactory<CommentAppService> {
    @Override
    public CommentAppService create(Throwable throwable) {
        return new CommentAppService(){
            @Override
            public int getUnCommentNum(Long userId, Long merchantId, Long shopId) {
                return 0;
            }
        };
    }
}
