package mf.code.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.api.feignclient.CommAppService;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.stereotype.Component;

/**
 * mf.code.api.feignclient.fallback
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:42
 */
@Component
public class CommAppFeignFallbackFactory implements FallbackFactory<CommAppService> {

    @Override
    public CommAppService create(Throwable cause) {
        return new CommAppService() {
            @Override
            public SimpleResponse sendJkmfTemplateMsg(TemplateMsgDTO templateMsgDTO) {
                return null;
            }

            @Override
            public CommonDictDTO selectByTypeKey(String type, String key) {
                return null;
            }
        };
    }
}
