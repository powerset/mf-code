package mf.code.api.feignclient;

import mf.code.api.feignclient.fallback.CommentAppFeignFallbackFactory;
import mf.code.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mf.code.api.feignclient
 * Description:
 *
 * @author: 百川
 * @date: 2019-08-02 14:58
 */
@FeignClient(value = "mf-code-comment", fallbackFactory = CommentAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface CommentAppService {

    /**
     * 我的订单中待评价数量
     *
     * @return
     */
    @GetMapping("/feignapi/comment/applet/getUnCommentNum")
    int getUnCommentNum(@RequestParam("userId") Long userId,
                        @RequestParam("merchantId") Long merchantId,
                        @RequestParam("shopId") Long shopId);
}
