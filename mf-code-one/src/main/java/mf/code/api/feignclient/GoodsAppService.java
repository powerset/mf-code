package mf.code.api.feignclient;

import mf.code.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import mf.code.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.dto.ProductEntity;
import mf.code.goods.dto.ProductSkuEntity;
import mf.code.one.dto.MsInfoDTO;
import mf.code.upay.repo.po.Product;
import mf.code.upay.repo.po.ProductSku;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * mf.code.goods.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-03 14:10
 */
@FeignClient(name = "mf-code-goods", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface GoodsAppService {

    /**
     * 根据传入skuId，查询或者创建新的sku，平台免单抽奖专用
     */
    @GetMapping("/feignapi/goods/applet/v8/createSkuForPlatformOrderBack")
    ProductSkuEntity createSkuForPlatformOrderBack(@RequestParam("productSkuId") Long productSkuId,
                                                   @RequestParam("stockDef") Integer stockDef);

    /**
     * 根据商品id列表查询商品详情，全部字段对应db
     *
     * @param productIdList
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v5/queryProductDetailList")
    List<ProductEntity> queryProductDetailList(@RequestParam("productIdList") List<Long> productIdList);

    /**
     * 通过商品skuId 查询商品详情，全部字段对应db
     */
    @GetMapping("/feignapi/goods/applet/v5/queryProductDetailBySkuIds")
    List<ProductEntity> queryProductDetailBySkuIds(@RequestParam("skuIdList") List<Long> skuIdList);

    /**
     * 通过主键 批量获取商品信息
     *
     * @param goodsIds
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v8/listByIds")
    List<ProductEntity> listByIds(@RequestParam("goodsIds") List<Long> goodsIds);

    /**
     * 查询该店铺下的所有商品
     *
     * @param shopId
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/feignapi/goods/one/v5/querySelfProductByShopId")
    List<Product> querySelfProductByShopId(@RequestParam("shopId") Long shopId,
                                           @RequestParam(value = "page", defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", defaultValue = "10") Integer size);

    /**
     * 获取该店铺下的商品数量
     *
     * @param shopId
     * @param merchantId
     * @return
     */
    @GetMapping("/feignapi/goods/one/v5/queryProductCountByShopId")
    int queryProductCountByShopId(@RequestParam("shopId") Long shopId,
                                  @RequestParam("merchantId") Long merchantId);


    /**
     * 通过商品id查询sku
     *
     * @param productIdList
     * @return
     */
    @GetMapping("/feignapi/goods/applet/sku/querySkuListByProductIds")
    List<ProductSku> querySkuListByProductIds(@RequestParam("productIdList") List<Long> productIdList);

    /**
     * 根据传入sku信息，查询或者创建新的sku，秒杀专用
     *
     * @return Map
     */
    @PostMapping("/feignapi/goods/applet/sku/queryOrCreateForFlashSale")
    List<ProductDistributionDTO> queryOrCreateForFlashSale(@RequestBody MsInfoDTO msInfoDTO);


    @GetMapping("/feignapi/goods/seller/v5/queryProductSkuMapBySkuIdList")
    SimpleResponse queryProductSkuMapBySkuIdList(@RequestParam("skuIdList") List<Long> skuIdList);
}
