package mf.code.api.feignclient;

import mf.code.api.feignclient.fallback.CommAppFeignFallbackFactory;
import mf.code.comm.dto.CommonDictDTO;
import mf.code.comm.dto.TemplateMsgDTO;
import mf.code.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mf.code.api.feignclient
 *
 * @description:
 * @author: yechen
 * @email: wangqingfeng@wxyundian.com
 * @date: 2019年07月08日 16:42
 */
@FeignClient(name = "mf-code-comm", fallbackFactory = CommAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface CommAppService {

    @PostMapping("/feignapi/one/applet/v9/sendJkmfTemplateMsg")
    SimpleResponse sendJkmfTemplateMsg(@RequestBody TemplateMsgDTO templateMsgDTO);

    /**
     * 按照类型键值查询
     *
     * @param type
     * @param key
     * @return
     */
    @GetMapping("/feignapi/comm/selectByTypeKey")
    CommonDictDTO selectByTypeKey(@RequestParam("type") String type,
                                  @RequestParam("key") String key);
}
