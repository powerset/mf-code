package mf.code.api.feignclient;

import mf.code.api.feignclient.fallback.DistributionAppFeignFallbackFactory;
import mf.code.common.config.feign.FeignLogConfiguration;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.user.dto.UpayWxOrderReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * mf.code.distribution.feignclient.applet
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-08 19:11
 */
@FeignClient(value = "mf-code-distribution", fallbackFactory = DistributionAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface DistributionAppService {

    @PostMapping("/feignapi/distribution/applet/becomeShopManagerV2")
    SimpleResponse becomeShopManagerV2(@RequestBody UpayWxOrderReq upayWxOrderReq);

    /**
     * 存储用户获取pend存储
     *
     * @param reqs
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/v8/saveUpayWxOrderPend")
    SimpleResponse saveUpayWxOrderPend(@RequestBody List<UpayWxOrderReq> reqs);

    /**
     * 粉丝完成任务 发送消息模板
     *
     * @param nickName   完成人
     * @param upperId    上级
     * @param commission 上级获得的佣金
     * @param taskName   任务名称
     */
    @GetMapping("/feignapi/distribution/applet/v8/sendTemplateMessage")
    SimpleResponse sendTemplateMessage(@RequestParam("nickName") String nickName,
                                       @RequestParam("upperId") Long upperId,
                                       @RequestParam("commission") String commission,
                                       @RequestParam("taskName") String taskName);


    /***
     * 汇总pend金额
     * @param params
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/v8/sumTotalFeeByUpayWxOrderPend")
    String sumTotalFeeByUpayWxOrderPend(@RequestBody Map<String, Object> params);


    @GetMapping("/feignapi/distribution/applet/v8/addIncomeRankListOfShopByUserIds")
    SimpleResponse addIncomeRankListOfShopByUserIds(@RequestParam("shopId") Long shopId,
                                                    @RequestParam("userIds") List<Long> userIds);

    /**
     * 获取店长待结算记录
     * @param shopManagerParams
     * @return
     */
    @PostMapping("/feignapi/distribution/applet/queryShopManagerPend")
    List<UpayWxOrderReq> queryShopManagerPend(@RequestBody Map<String, Object> shopManagerParams);

    /**
     * 付费升店长，发展一个粉丝成为店长可获得的奖金
     *
     * @return
     */
    @GetMapping("/feignapi/distribution/applet/getAmountByShopManagerPay")
    String getAmountByShopManagerPay();
}
