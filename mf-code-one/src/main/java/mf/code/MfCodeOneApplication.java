package mf.code;

import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.aop.SQLInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import java.util.Properties;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "mf.code.*.repo.dao")
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 28800, redisNamespace = "mf:session")
@EnableAsync
@EnableScheduling
@EnableEurekaClient
@EnableFeignClients(basePackages = "mf.code.api.feignclient")
@EnableCircuitBreaker//启用熔断
@Slf4j
public class MfCodeOneApplication {

    public static boolean BOO_LAZY_LOADING = false;
    public static final String KEY_LAZY_LOADING = "lazyloading";
    public static final String VAL_LAZY_LOADING = "true";

    public static void main(String[] args) {
        for (String str : args) {
            String[] param = str.trim().toLowerCase().split("=");
            //系统参数中有此k-v对,并且key=lazyloading
            if (param.length > 1 && KEY_LAZY_LOADING.equals(param[0])) {
                if (VAL_LAZY_LOADING.equals(param[1])) {
                    BOO_LAZY_LOADING = true;
                }
            }
        }
        SpringApplication.run(MfCodeOneApplication.class, args);
    }

    /**
     * 远程调用第三方api专用
     *
     * @return
     */
    @Bean("remoteApiRestTemplate")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public SQLInterceptor sqlStatsInterceptor() {
        SQLInterceptor sqlStatsInterceptor = new SQLInterceptor();
        Properties properties = new Properties();
        properties.setProperty("dialect", "mysql");
        sqlStatsInterceptor.setProperties(properties);
        return sqlStatsInterceptor;
    }

    @Bean
    public PageHelper pageHelper() {
        PageHelper pageHelper = new PageHelper();
        Properties properties = new Properties();
        properties.setProperty("offsetAsPageNum", "true");
        properties.setProperty("rowBoundsWithCount", "true");
        properties.setProperty("reasonable", "true");
        // 配置mysql数据库的方言
        properties.setProperty("dialect", "mysql");
        pageHelper.setProperties(properties);
        return pageHelper;
    }

    @Bean(name = "processExecutor")
    public TaskExecutor workExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setThreadNamePrefix("Async-Attila-");
        threadPoolTaskExecutor.setCorePoolSize(10);
        threadPoolTaskExecutor.setMaxPoolSize(20);
        threadPoolTaskExecutor.setQueueCapacity(100);
        threadPoolTaskExecutor.afterPropertiesSet();
        return threadPoolTaskExecutor;
    }
}
