package mf.code.web.api.feignclient;

import mf.code.merchant.dto.MerchantDTO;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.web.api.feignclient.fallback.ShopAppFeignFallbackFactory;
import mf.code.web.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mf.code.web.api.feignclient
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-27 14:28
 */
@FeignClient(name = "mf-code-shop", fallbackFactory = ShopAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface ShopAppService {

    /**
     * 通过id查店铺
     *
     * @param shopId
     * @return
     */
    @GetMapping("/feignapi/shop/applet/v8/findShopById")
    MerchantShopDTO findShopById(@RequestParam("shopId") Long shopId);

    /**
     * 通过 merchantId 获取 抖带带主播信息
     * @param merchantId
     * @return
     */
    @GetMapping("feignapi/shop/applet/queryMerchantById")
    MerchantDTO queryMerchantById(@RequestParam("merchantId") Long merchantId);
}
