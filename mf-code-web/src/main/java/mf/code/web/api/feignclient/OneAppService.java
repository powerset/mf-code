package mf.code.web.api.feignclient;


import mf.code.web.common.config.feign.FeignLogConfiguration;
import mf.code.web.api.feignclient.fallback.OneAppFeignFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.web.api.feignclient
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-28 14:05
 */
@FeignClient(name = "mf-code-one", fallbackFactory = OneAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface OneAppService {

    /***
     * 首页获取新手模块数据展现
     * @return
     */
    @GetMapping("/feignapi/one/applet/v8/queryHomePageNewbieTask")
    Map<String, Object> queryHomePageNewbieTask(@RequestParam("shopId") Long shopId,
                                                @RequestParam("userId") Long userId);

    @GetMapping("/feignapi/one/applet/v9/queryFullReimbursementInfo")
    Map<String, Object> queryFullReimbursementInfo(@RequestParam("shopId") Long shopId,
                                                          @RequestParam("userId") Long userId);
}
