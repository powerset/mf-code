package mf.code.web.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.web.api.feignclient.GoodsAppService;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * mf.code.web.api.feignclient.fallback
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-28 09:59
 */
@Component
public class GoodsAppFeignFallbackFactory implements FallbackFactory<GoodsAppService> {
    @Override
    public GoodsAppService create(Throwable throwable) {
        return new GoodsAppService() {

            @Override
            public Map<String, Object> homePageGoodsInfo(Long shopId, String userId, Integer type, Long offset, Long size) {
                return null;
            }
        };
    }
}
