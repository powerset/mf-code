package mf.code.web.api.applet;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.RegexUtils;
import mf.code.web.service.HomePageService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * mf.code.shop.api
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-01 08:55
 */
@Slf4j
@RestController
@RequestMapping("/api/web/applet/homepage")
public class HomaPageApi {

    @Autowired
    private HomePageService homePageService;

    /**
     * 查询小程序首页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    @GetMapping("/queryHomePage")
    public SimpleResponse queryHomePage(@RequestParam(value = "userId", required = false) String userId,
                                        @RequestParam("shopId") String shopId,
                                        @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                        @RequestParam(value = "size", defaultValue = "10") Long size) {

        // 校验入参
        if (StringUtils.isBlank(shopId) || !RegexUtils.StringIsNumber(shopId)) {
            log.error("参数异常");
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "参数异常");
        }

        return homePageService.queryHomePage(userId, Long.valueOf(shopId), offset, size);
    }
}
