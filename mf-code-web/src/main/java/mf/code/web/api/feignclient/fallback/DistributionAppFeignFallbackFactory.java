package mf.code.web.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.web.api.feignclient.DistributionAppService;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * mf.code.web.api.feignclient.fallback
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-28 11:04
 */
@Component
public class DistributionAppFeignFallbackFactory implements FallbackFactory<DistributionAppService> {
    @Override
    public DistributionAppService create(Throwable throwable) {
        return new DistributionAppService() {
            @Override
            public Map<String, Object> homePageDistributionInfo(AppletUserProductRebateDTO appletUserProductRebateDTO) {
                return null;
            }
        };
    }
}
