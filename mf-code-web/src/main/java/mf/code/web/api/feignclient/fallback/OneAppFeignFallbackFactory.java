package mf.code.web.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.web.api.feignclient.OneAppService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.web.api.feignclient.fallback
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-28 14:06
 */
@Component
public class OneAppFeignFallbackFactory implements FallbackFactory<OneAppService> {
    @Override
    public OneAppService create(Throwable throwable) {
        return new OneAppService() {
            @Override
            public Map<String, Object> queryHomePageNewbieTask(Long shopId, Long userId) {
                return null;
            }

            @Override
            public Map<String, Object> queryFullReimbursementInfo(Long shopId, Long userId) {
                return new HashMap<>();
            }
        };
    }
}
