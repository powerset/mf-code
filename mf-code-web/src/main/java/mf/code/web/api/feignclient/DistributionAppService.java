package mf.code.web.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.web.api.feignclient.fallback.DistributionAppFeignFallbackFactory;
import mf.code.web.common.config.feign.FeignLogConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.web.api.feignclient
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-28 11:03
 */
@FeignClient(value = "mf-code-distribution", fallbackFactory = DistributionAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface DistributionAppService {

    /**
     * 获取首页佣金收益情况，包括获取当前用户的商品返利金额、在此店铺内用户推广收益情况
     *
     * @param appletUserProductRebateDTO
     * @return
     */
    @PostMapping(value = "/feignapi/distribution/applet/v8/homePageDistributionInfo")
    Map<String, Object> homePageDistributionInfo(@RequestBody AppletUserProductRebateDTO appletUserProductRebateDTO);
}
