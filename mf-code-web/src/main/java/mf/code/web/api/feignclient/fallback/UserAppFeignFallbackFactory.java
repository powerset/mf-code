package mf.code.web.api.feignclient.fallback;

import feign.hystrix.FallbackFactory;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.web.api.feignclient.UserAppService;
import mf.code.user.dto.UpayWxOrderReq;
import mf.code.user.dto.UpayWxOrderReqDTO;
import mf.code.user.dto.UserResp;
import mf.code.user.feignapi.applet.dto.UserAddressReq;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * mf.code.user.feignclient.applet
 *
 * @description:
 * @auther: yechen
 * @email: wangqingfeng@wxyundian.com
 * @Date: 2019年04月13日 17:23
 */
@Component
public class UserAppFeignFallbackFactory implements FallbackFactory<UserAppService> {
    @Override
    public UserAppService create(Throwable throwable) {
        return new UserAppService() {
            @Override
            public UserResp queryUser(Long userId) {
                return null;
            }
        };
    }
}
