package mf.code.web.api.feignclient;

import mf.code.common.simpleresp.SimpleResponse;
import mf.code.web.common.config.feign.FeignLogConfiguration;
import mf.code.web.api.feignclient.fallback.GoodsAppFeignFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * mf.code.web.api.feignclient
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-28 09:57
 */
@FeignClient(name = "mf-code-goods", fallbackFactory = GoodsAppFeignFallbackFactory.class, configuration = FeignLogConfiguration.class)
public interface GoodsAppService {

    /**
     * 获取首页商品信息，包括：是否购买过新人专享、获取新人专享销售商品、获取销售商品
     *
     * @return
     */
    @GetMapping("/feignapi/goods/applet/v8/homePageGoodsInfo")
    Map<String, Object> homePageGoodsInfo(@RequestParam("shopId") Long shopId,
                                                         @RequestParam(value = "userId", required = false) String userId,
                                                         @RequestParam(value = "type", defaultValue = "0") Integer type,
                                                         @RequestParam(value = "offset", defaultValue = "0") Long offset,
                                                         @RequestParam(value = "size", defaultValue = "16") Long size);
}
