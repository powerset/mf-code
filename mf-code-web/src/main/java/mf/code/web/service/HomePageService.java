package mf.code.web.service;

import mf.code.common.simpleresp.SimpleResponse;

/**
 * mf.code.web.service
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-27 14:16
 */
public interface HomePageService {

    /**
     * 查询店铺首页
     *
     * @param userId
     * @param shopId
     * @param offset
     * @param size
     * @return
     */
    SimpleResponse queryHomePage(String userId, Long shopId, Long offset, Long size);
}
