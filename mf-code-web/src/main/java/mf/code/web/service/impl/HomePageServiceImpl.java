package mf.code.web.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.dto.AppletMybatisPageDto;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.BeanMapUtil;
import mf.code.common.utils.RegexUtils;
import mf.code.distribution.feignapi.dto.AppletUserProductRebateDTO;
import mf.code.distribution.feignapi.dto.ProductDistributionDTO;
import mf.code.goods.constant.ProductShowTypeEnum;
import mf.code.shop.dto.MerchantShopDTO;
import mf.code.user.feignapi.applet.dto.UserDistributionInfoDTO;
import mf.code.web.api.feignclient.DistributionAppService;
import mf.code.web.api.feignclient.GoodsAppService;
import mf.code.web.api.feignclient.OneAppService;
import mf.code.web.api.feignclient.ShopAppService;
import mf.code.web.service.HomePageService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.web.service.impl
 * Description:
 *
 * @author: 大妖怪
 * @date: 2019-06-27 14:17
 */
@Service
@Slf4j
public class HomePageServiceImpl implements HomePageService {

    @Autowired
    private ShopAppService shopAppService;
    @Autowired
    private GoodsAppService goodsAppService;
    @Autowired
    private DistributionAppService distributionAppService;
    @Autowired
    private OneAppService oneAppService;
    @Autowired
    private ObjectMapper mapper;

    @Override
    public SimpleResponse queryHomePage(String userId, Long shopId, Long offset, Long size) {
        MerchantShopDTO merchantShopDTO = shopAppService.findShopById(shopId);
            if (merchantShopDTO == null) {
            log.error("查无此店铺, sid = {}", shopId);
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1.getCode(), "查无此店铺");
        }

        // 获取商品相关
        Map<String, Object> goodsResult = goodsAppService.homePageGoodsInfo(shopId, userId, ProductShowTypeEnum.NEWBIE.getCode(),  offset, size);
        if (goodsResult == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "获取商品失败");
        }

        AppletUserProductRebateDTO appletUserProductRebateDTO = null;
        if (goodsResult.get("appletUserProductRebateDTO") != null) {
            appletUserProductRebateDTO = mapper.convertValue(goodsResult.get("appletUserProductRebateDTO"), AppletUserProductRebateDTO.class);
        }
        if (appletUserProductRebateDTO == null) {
            return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "获取商品失败");
        }

        boolean newManRecommandImpression = false;
        if (goodsResult.get("newManRecommandImpression") != null) {
            newManRecommandImpression = mapper.convertValue(goodsResult.get("newManRecommandImpression"), boolean.class);
        }

        AppletUserProductRebateDTO newMansGoodsRecommend =  null;
        if (goodsResult.get("newMansGoodsRecommend") != null) {
            newMansGoodsRecommend = mapper.convertValue(goodsResult.get("newMansGoodsRecommend"), AppletUserProductRebateDTO.class);
        }

        appletUserProductRebateDTO.setShopId(shopId);
        appletUserProductRebateDTO.setSize(size);
        appletUserProductRebateDTO.setOffset(offset);
        AppletMybatisPageDto<ProductDistributionDTO> distributionProductPage = new AppletMybatisPageDto<>(appletUserProductRebateDTO.getSize(),
                appletUserProductRebateDTO.getOffset());
        distributionProductPage.setContent(appletUserProductRebateDTO.getProductDistributionDTOList());
        if (appletUserProductRebateDTO.getOffset() + appletUserProductRebateDTO.getSize() < appletUserProductRebateDTO.getTotal()) {
            distributionProductPage.setPullDown(true);
        }

        UserDistributionInfoDTO userDistributionInfo = null;
        if (StringUtils.isNotBlank(userId) && RegexUtils.StringIsNumber(userId)) {
            appletUserProductRebateDTO.setUserId(Long.valueOf(userId));
            Map<String, Object> distributionResult = distributionAppService.homePageDistributionInfo(appletUserProductRebateDTO);
            if (distributionResult == null) {
                return new SimpleResponse(ApiStatusEnum.ERROR_BUS_NO1, "获取佣金信息失败");
            }
            distributionProductPage = mapper.convertValue(distributionResult.get("distributionProductPage"), AppletMybatisPageDto.class);
            userDistributionInfo = mapper.convertValue(distributionResult.get("userDistributionInfo"), UserDistributionInfoDTO.class);
        }

        //获取首页报销活动信息
        Map<String, Object> fullReimbursementInfo = null;
        if (StringUtils.isNotBlank(userId) && RegexUtils.StringIsNumber(userId)) {
            fullReimbursementInfo = oneAppService.queryFullReimbursementInfo(shopId, Long.valueOf(userId));
        }

        // 加工首页数据
        Map<String, Object> homePage = new HashMap<>();
        homePage.put("picPath", merchantShopDTO.getPicPath());
        homePage.put("shopName", merchantShopDTO.getShopName());
        if (StringUtils.isBlank(merchantShopDTO.getBannerJson())) {
            homePage.put("bannerJson", new ArrayList<>());
        } else {
            homePage.put("bannerJson", JSON.parseArray(merchantShopDTO.getBannerJson()));
        }
        if (StringUtils.isBlank(merchantShopDTO.getCsJson())) {
            homePage.put("csJson", new ArrayList<>());
        }  else {
            homePage.put("csJson", JSON.parseArray(merchantShopDTO.getCsJson()));
        }
        homePage.put("goodsRecommend", distributionProductPage);
        homePage.put("newMansGoodsRecommend", newManRecommandImpression && newMansGoodsRecommend != null ? newMansGoodsRecommend.getProductDistributionDTOList() : new ArrayList<>());
        homePage.put("commissionStandard", 0);
        homePage.put("spendingAmountTomonth", 0);
        homePage.put("leftAmount", 0);
        homePage.put("fullReimbursement", fullReimbursementInfo);

        if (userDistributionInfo != null) {
            homePage.putAll(BeanMapUtil.beanToMap(userDistributionInfo));
        }

        // 2.3迭代 新手任务
        if (StringUtils.isNotBlank(userId) && RegexUtils.StringIsNumber(userId)) {
            Map<String, Object> newBie = oneAppService.queryHomePageNewbieTask(shopId, Long.valueOf(userId));
            homePage.put("newBie", newBie);
        }

        return new SimpleResponse(ApiStatusEnum.SUCCESS, homePage);
    }
}
