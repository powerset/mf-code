package mf.code.web.common.config;

import mf.code.web.common.aop.AppletTokenInterceptor;
import mf.code.web.common.aop.PlatformTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 17:28
 */
@Configuration
public class TokenConfig extends WebMvcConfigurationSupport {

    @Bean
    AppletTokenInterceptor appletTokenInterceptor() {
        return new AppletTokenInterceptor();
    }

    @Bean
    PlatformTokenInterceptor platformTokenInterceptor() {
        return new PlatformTokenInterceptor();
    }
}
