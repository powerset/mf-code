package mf.code.web.common.redis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Auther: yechen
 * @Email: wangqingfeng@wxyundian.com
 * @Date: 2018/10/7 0007 14:06
 * @Description:
 */
@ConfigurationProperties(prefix="redis")
@Data
@Component
public class RedisConfigProperties {
    private String host;
    private int port;
    private String password;
    private long timeout;
    private int database;
    private int maxIdle;
    private int minIdle;
    private int maxTotal;
    private int  maxWaitMillis;
    private long minEvictableIdleTimeMillis;
    private int numTestsPerEvictionRun;
    private long timeBetweenEvictionRunsMillis;
    private boolean testOnBorrow;
    private boolean testWhileIdle;

}
