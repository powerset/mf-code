package mf.code.web.common.caller.aliyundayu;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mf.code.common.caller.aliyundayu
 * Description: 阿里大于短信服务
 *
 * @author: gel
 * @date: 2018-10-24 15:39
 */
@Component
@Slf4j
public class DayuCaller {

    @Autowired
    private DayuConfig dayuConfig;

    /**
     * 产品名称:云通信短信API产品,开发者无需替换
     */
    private static final String PRODUCT = "Dysmsapi";
    /**
     * 产品域名,开发者无需替换
     */
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";

    static {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
    }

    /**
     * 发送短信
     * 返回错误码信息
     * OK	请求成功
     * isp.RAM_PERMISSION_DENY	RAM权限DENY
     * isv.OUT_OF_SERVICE	业务停机
     * isv.PRODUCT_UN_SUBSCRIPT	未开通云通信产品的阿里云客户
     * isv.PRODUCT_UNSUBSCRIBE	产品未开通
     * isv.ACCOUNT_NOT_EXISTS	账户不存在
     * isv.ACCOUNT_ABNORMAL	账户异常
     * isv.SMS_TEMPLATE_ILLEGAL	短信模板不合法
     * isv.SMS_SIGNATURE_ILLEGAL	短信签名不合法
     * isv.INVALID_PARAMETERS	参数异常
     * isp.SYSTEM_ERROR	系统错误
     * isv.MOBILE_NUMBER_ILLEGAL	非法手机号
     * isv.MOBILE_COUNT_OVER_LIMIT	手机号码数量超过限制
     * isv.TEMPLATE_MISSING_PARAMETERS	模板缺少变量
     * isv.BUSINESS_LIMIT_CONTROL	业务限流
     * isv.INVALID_JSON_PARAM	JSON参数不合法，只接受字符串值
     * isv.BLACK_KEY_CONTROL_LIMIT	黑名单管控
     * isv.PARAM_LENGTH_LIMIT	参数超出长度限制
     * isv.PARAM_NOT_SUPPORT_URL	不支持URL
     * isv.AMOUNT_NOT_ENOUGH	账户余额不足
     *
     * @param phone
     * @param code
     * @return
     */
    public SendSmsResponse sendSms(String phone, String code, String templateCode) {


        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile(dayuConfig.getRegion(), dayuConfig.getAccessKeyId(), dayuConfig.getAccessKeySecret());
        try {
            DefaultProfile.addEndpoint(dayuConfig.getEndpoint(), dayuConfig.getRegion(), PRODUCT, DOMAIN);
        } catch (ClientException e) {
            e.printStackTrace();
            log.error("DayuCaller 未知异常1,phone:" + phone + " code:" + code, e);
        }
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        // 必填:待发送手机号
        request.setPhoneNumbers(phone);
        // 必填:短信签名-可在短信控制台中找到
        request.setSignName(dayuConfig.getSignName());
        // 必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(templateCode);
        // 需要:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        request.setTemplateParam(JSON.toJSONString(map));

        /*//选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        request.setSmsUpExtendCode("90997");*/

        // 可选 outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId(phone);

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = new SendSmsResponse();
        try {
            log.info("DayuCaller 短信请求入参" + JSON.toJSONString(request));
            sendSmsResponse = acsClient.getAcsResponse(request);
            log.info("DayuCaller 短信请求返回" + JSON.toJSONString(sendSmsResponse));
        } catch (ClientException e) {
            e.printStackTrace();
            log.error("DayuCaller 未知异常2,phone:" + phone + " code:" + code, e);
        }
        return sendSmsResponse;
    }

    /**
     * 登录验证码
     *
     * @param phone 手机号
     * @param code  验证码
     * @return 返回数据
     */
    public SendSmsResponse smsForLogin(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsLogin());
    }

    public SendSmsResponse smsForRegister(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsRegister());
    }

    public SendSmsResponse smsForPasswordUpdate(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsPasswordUpdate());
    }

    public SendSmsResponse smsForIdChange(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsIdChange());
    }

    public SendSmsResponse smsForIdValidate(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsIdValidate());
    }

    public SendSmsResponse smsForLoginException(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsLoginException());
    }

    public SendSmsResponse smsForInitPassword(String phone, String code) {
        return sendSms(phone, code, dayuConfig.getSmsPasswordInit());
    }

    /**
     * 粉多多激活通知短信
     * @param phone 手机号
     * @return 阿里大于返回
     */
    public SendSmsResponse smsFanMuchActivate(String phone) {
        return sendSms(phone, "t.cn/Ef4XDbU", dayuConfig.getSmsFanMuchActivate());
    }

    public QuerySendDetailsResponse querySendDetails(String bizId, String phone) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", dayuConfig.getAccessKeyId(), dayuConfig.getAccessKeySecret());
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", PRODUCT, DOMAIN);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber(phone);
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);

        //hint 此处可能会抛出异常，注意catch
        return acsClient.getAcsResponse(request);
    }

    public static void main(String[] args) throws ClientException, InterruptedException {

        //发短信
        DayuCaller dayuCaller = new DayuCaller();
        SendSmsResponse response = dayuCaller.sendSms("phone", "code", "SMS_153535053");
        System.out.println("短信接口返回的数据----------------");
        System.out.println("Code=" + response.getCode());
        System.out.println("Message=" + response.getMessage());
        System.out.println("RequestId=" + response.getRequestId());
        System.out.println("BizId=" + response.getBizId());

        Thread.sleep(3000L);

        //查明细
        if (response.getCode() != null && response.getCode().equals("OK")) {
            QuerySendDetailsResponse querySendDetailsResponse = dayuCaller.querySendDetails(response.getBizId(), "phone");
            System.out.println("短信明细查询接口返回数据----------------");
            System.out.println("Code=" + querySendDetailsResponse.getCode());
            System.out.println("Message=" + querySendDetailsResponse.getMessage());
            int i = 0;
            for (QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse.getSmsSendDetailDTOs()) {
                System.out.println("SmsSendDetailDTO[" + i + "]:");
                System.out.println("Content=" + smsSendDetailDTO.getContent());
                System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
                System.out.println("OutId=" + smsSendDetailDTO.getOutId());
                System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
                System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
                System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
                System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
                System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
            }
            System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
            System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
        }

    }

}
