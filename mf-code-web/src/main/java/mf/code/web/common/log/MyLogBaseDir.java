package mf.code.web.common.log;

import ch.qos.logback.core.PropertyDefinerBase;
import org.apache.commons.lang.StringUtils;

/**
 * <pre>
 * MyLogBaseDir
 * 定制 日志文件路径 与 开发环境 有关联关系
 *   例如
 *     macOS操作系统
 *     日志文件路径为
 *       /Users/{用户名}/logs⁩/home⁩/code⁩/logs⁩/mf-code-one-8001/all⁩/all-%d{yyyy-MM-dd}.log
 *       /Users/{用户名}/logs⁩/home⁩/code⁩/logs⁩/mf-code-one-8001/error/error-%d{yyyy-MM-dd}.log
 * </pre>
 */
public class MyLogBaseDir extends PropertyDefinerBase {

    @Override
    public String getPropertyValue() {
        String os = System.getProperty("os.name");
        if (StringUtils.isNotBlank(os) && os.toLowerCase().contains("mac os x")) {
            String userHome = System.getProperty("user.home");
            return userHome + "/logs";
        }
        return "";
    }
}
