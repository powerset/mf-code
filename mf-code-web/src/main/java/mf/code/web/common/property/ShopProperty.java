package mf.code.web.common.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "shop")
@Data
@Component
public class ShopProperty {
    private String shopId; //店铺id
    private String merchantId; //商户id
}
