package mf.code.web.common.caller.wxmp;

/**
 * @Author yechen
 * @Date
 **/
public class RedisKeyConstant {

    /**小程序accessToken*/
    public static final String APPLET_ACCESS_TOKEN="applet:accesstoken";//

    /***
     * 集客师accessToken
     */
    public static final String TEACHER_ACCESS_TOKEN="teacher:accesstoken";//

}
