package mf.code.web.common.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.annotation.Upload;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Component
@Aspect
@Slf4j
public class AspectJ4ServiceLog {

    @Around("execution(public * mf.code.*.service..*.*(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Signature signature = proceedingJoinPoint.getSignature();
        MethodSignature msig = (MethodSignature) signature;
        Method currentMethod = proceedingJoinPoint.getTarget().getClass().getMethod(msig.getName(), msig.getParameterTypes());
        String name = msig.getDeclaringTypeName() + "#" + msig.getName();
        String argsStr = null;
        Upload upload = currentMethod.getClass().getAnnotation(Upload.class);
        if (upload == null) {
            Object[] args = proceedingJoinPoint.getArgs();
            argsStr = handleWX(args);
        }

        log.info("serviceLog-before,serviceName={}, args={}", name, argsStr);
        //
        Object result = proceedingJoinPoint.proceed();
        //
        log.info("serviceLog-end,serviceName={},  args={}, return={}", name, argsStr, JSON.toJSONString(result));
        return result;
    }

    private String handleWX(Object[] args) {
        List<Object> argsList = new ArrayList<>();
        for (Object o : args) {
            if (!(o instanceof InputStreamSource)) {
                argsList.add(o);
            } else {
                argsList.add("IO流");
            }
        }
        try {
            return JSON.toJSONString(argsList);
        } catch (IllegalStateException e) {
            return "wx callback:" + args;
        }
    }
}
