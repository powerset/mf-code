package mf.code.web.common.redis;

/**
 * mf.code.shop.common.redis
 * Description:
 *
 * @author: 百川
 * @date: 2019-04-11 18:01
 */
public class RedisKeyConstant {

	/**
	 * 数据库 teacherInvite缓存
	 */
	public static final String TEACHER_INVITE_CACHE = "teacher:invite:cache:id:";//<tid>
	/**
	 * 尾货清仓版 店铺首页banner图
	 */
	public static final String CLEAN_VERSION_SHOP_BANNER = "mf:clean:shop:banner";
}
