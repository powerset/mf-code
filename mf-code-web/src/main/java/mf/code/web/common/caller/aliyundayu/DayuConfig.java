package mf.code.web.common.caller.aliyundayu;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里大于配置参数
 * @author gel
 */
@Data
@Configuration
public class DayuConfig {

    /**
     * 阿里大于 地域
     */
    @Value("${aliyundayu.region}")
    private String region;
    /**
     * 阿里大于 接口地址Endpoint
     */
    @Value("${aliyundayu.endpoint}")
    private String endpoint;
    /**
     * 阿里大于 API的密钥Access Key ID
     */
    @Value("${aliyundayu.accessKeyId}")
    private String accessKeyId;
    /**
     * 阿里大于 API的密钥Access Key Secret
     */
    @Value("${aliyundayu.accessKeySecret}")
    private String accessKeySecret;
    /**
     * 阿里大于 短信签名
     */
    @Value("${aliyundayu.signName}")
    private String signName;



    /**
     * 身份验证验证码 验证码${code}，您正在进行身份验证，打死不要告诉别人哦！
     */
    @Value("${aliyundayu.smsIdValidate}")
    private String smsIdValidate;
    /**
     * 登录确认验证码 验证码${code}，您正在登录，若非本人操作，请勿泄露。
     */
    @Value("${aliyundayu.smsLogin}")
    private String smsLogin;
    /**
     * 登录异常验证码 验证码${code}，您正尝试异地登录，若非本人操作，请勿泄露。
     */
    @Value("${aliyundayu.smsLoginException}")
    private String smsLoginException;
    /**
     * 用户注册验证码 验证码${code}，您正在注册成为新用户，感谢您的支持！
     */
    @Value("${aliyundayu.smsRegister}")
    private String smsRegister;
    /**
     * 修改密码验证码 验证码${code}，您正在尝试修改登录密码，请妥善保管账户信息。
     */
    @Value("${aliyundayu.smsPasswordUpdate}")
    private String smsPasswordUpdate;
    /**
     * 信息变更验证码 验证码${code}，您正在尝试变更重要信息，请妥善保管账户信息。
     */
    @Value("${aliyundayu.smsIdChange}")
    private String smsIdChange;
    /**
     * 初始密码通知 您好，您的密码已经初始化为${code}，请及时登录并修改密码。
     */
    @Value("${aliyundayu.smsPasswordInit}")
    private String smsPasswordInit;
    /**
     * 粉多多激活短信模板 您的粉多多账号已激活完成，戳 ${code} 立即查看！感谢您的使用 回T退订
     */
    @Value("${aliyundayu.smsFanMuchActivate}")
    private String smsFanMuchActivate;
}
