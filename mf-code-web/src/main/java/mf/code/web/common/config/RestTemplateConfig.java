package mf.code.web.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * mf.code.shop.common.config
 * Description:
 *
 * @author gel
 * @date 2019-04-09 11:30
 */
@Configuration
public class RestTemplateConfig {
    /**
     * 远程调用第三方api专用
     *
     * @return
     */
    @Bean("remoteApiRestTemplate")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
