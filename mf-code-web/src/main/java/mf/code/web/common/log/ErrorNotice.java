package mf.code.web.common.log;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.RedisKeyConstant;
import mf.code.common.exception.ArgumentException;
import mf.code.common.exception.SqlUpdateFaildException;
import mf.code.web.common.email.EmailService;
import mf.code.web.common.redis.RedisForbidRepeat;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 全局异常处理类
 */
@RestControllerAdvice
@Slf4j
public class ErrorNotice {

    @Autowired
    private EmailService emailService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Value("${spring.profiles.active}")
    private String active;
    private String packageName = "mf.code";

    @ExceptionHandler(Exception.class)
    public Map<String, Object> doHandleException(HttpServletRequest request, HttpServletResponse response, Exception e) {
        String uri = request.getRequestURI();
        Map<String, String[]> parameterMap = request.getParameterMap();
        String fullStackTrace = ExceptionUtils.getFullStackTrace(e);

        // 发送邮件
        // 处理邮件标题
        if ("prod".equals(active)) {
            String title = getEmailTitle(e);
            boolean success = RedisForbidRepeat.NX(stringRedisTemplate, RedisKeyConstant.ERROR_EMAILSENDER + title, 1, TimeUnit.HOURS);
            if (success) {
                log.info("开始发送邮件");
                String context = "uri = " + uri + " , parameter = " + JSON.toJSONString(parameterMap) + "\r\n" + fullStackTrace;
                emailService.sendSimpleMail(title, context);
            }
        }

        // 返回异常信息

        log.error("uri= {} , parameter= {} Exception= {} ",uri, JSON.toJSONString(parameterMap), fullStackTrace);
        Map<String, Object> rtn = new HashMap<>();

        Integer code = -1;
        Object data = "";
        String errorMessage = "操作失败,请稍后重试";
        // 事务异常
        if (e instanceof SqlUpdateFaildException) {
            SqlUpdateFaildException ee = (SqlUpdateFaildException) e;
            errorMessage = "操作失败,请稍后重试";
            code = ee.getCode();
            data = ee.getData();
        }
        // 前端参数异常
        if (e instanceof ArgumentException) {
            ArgumentException ee = (ArgumentException) e;
            code = ee.getCode();
            errorMessage = ee.getErrorMessage();
            data = ee.getData();
        }

        rtn.put("code", code);
        rtn.put("message", errorMessage);
        rtn.put("data", data);
        return rtn;
    }

    /**
     * 参数异常 -- 不发邮件
     * @param request
     * @param response
     * @param e
     * @return
     */
    @ExceptionHandler(ArgumentException.class)
    public Map<String, Object> argumentException(HttpServletRequest request, HttpServletResponse response, Exception e) {

        String uri = request.getRequestURI();
        Map<String, String[]> parameterMap = request.getParameterMap();
        String fullStackTrace = ExceptionUtils.getFullStackTrace(e);

        // 返回异常信息
        log.error("uri= {} , parameter= {} Exception= {} ",uri, JSON.toJSONString(parameterMap), fullStackTrace);
        Map<String, Object> rtn = new HashMap<>();

        Integer code;
        Object data;
        String errorMessage;
        // 前端参数异常
        ArgumentException ee = (ArgumentException) e;
        code = ee.getCode();
        errorMessage = ee.getErrorMessage();
        data = ee.getData();

        rtn.put("code", code);
        rtn.put("message", errorMessage);
        rtn.put("data", data);
        return rtn;
    }

    private String getEmailTitle(Exception e) {
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException excption) {
            ip = "IP未知";
        }
        String message = "错误类型未知";
        String[] messageSplit = ExceptionUtils.getMessage(e).split(": ");
        String[] eSplit;
        if (messageSplit.length > 1) {
            eSplit = messageSplit[0].split("\\.");

        } else {
            eSplit = ExceptionUtils.getMessage(e).split("\\.");
        }
        message = eSplit[eSplit.length -1];
        log.info("message = " + message);

        String className = "类名未知";
        String line = "行数未知";
        String fullStackTrace = ExceptionUtils.getFullStackTrace(e);
        int packageIndex = fullStackTrace.indexOf(packageName);
        int beginIndex = fullStackTrace.indexOf("(", packageIndex);
        int endIndex = fullStackTrace.indexOf(")", packageIndex);
        String classErrorInfo = fullStackTrace.substring(beginIndex + 1, endIndex);
        String[] classErrorInfoSplit = classErrorInfo.split("\\.");
        String lineInfo = "";
        if (classErrorInfoSplit.length > 1) {
            className = classErrorInfoSplit[0];
            lineInfo = classErrorInfoSplit[1];
        }
        if (StringUtils.isNotBlank(lineInfo)) {
            line = lineInfo.split(":")[1];
        }

        return ip + "_" + message + "_" +className + "_" +line;
    }

}
