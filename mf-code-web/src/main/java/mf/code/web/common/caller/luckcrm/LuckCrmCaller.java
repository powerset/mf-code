package mf.code.web.common.caller.luckcrm;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Slf4j
@Service
public class LuckCrmCaller {

    @Resource(name = "remoteApiRestTemplate")
    private RestTemplate remoteApiRestTemplate;

    /**
     * 根据 订单号 和 商户名称(旺旺名称) 查询 订单详情  - v1
     *
     * @param tid  订单号  必填
     * @param nick 商户名称(旺旺名称) 必填
     * @return String
     * {
     * tid: "230632143621616822",
     * seller_nick: "美智母婴专营店",
     * status: "TRADE_FINISHED",
     * modified: "2018-10-11 23:09:20",
     * end_time: "2018-10-11 15:43:45",
     * created: "2018-10-06 10:20:20",
     * pay_time: "2018-10-06 10:25:04",
     * payment: "56.00",
     * num_iid: ["557739092081"]
     * }
     * 字段解释查看地址: http://bigdata.taobao.com/api.htm?docId=54&docType=2
     * status 交易状态。可选值:
     * TRADE_NO_CREATE_PAY(没有创建支付宝交易)
     * WAIT_BUYER_PAY(等待买家付款)
     * SELLER_CONSIGNED_PART(卖家部分发货)
     * WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款)
     * WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货)
     * TRADE_BUYER_SIGNED(买家已签收,货到付款专用)
     * TRADE_FINISHED(交易成功)
     * TRADE_CLOSED(付款以后用户退款成功，交易自动关闭)
     * TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易)
     * PAY_PENDING(国际信用卡支付付款确认中)
     * WAIT_PRE_AUTH_CONFIRM(0元购合约中)
     * PAID_FORBID_CONSIGN(拼团中订单或者发货强管控的订单，已付款但禁止发货)
     */
    public String getOrderInfo(String tid, String nick) {
        String url = "http://api.luckcrm.com/querytid.php?tid=" + tid + "&nick=" + nick;
        return returnContent(url);
    }

    /**
     * 根据 订单号和 商户名称(旺旺名称) 获取评论 - v1
     *
     * @param tid  订单号  必填
     * @param nick 商户名称(旺旺名称) 必填
     * @return String
     * {
     * trade_rate: {
     * content: "有点小贵",
     * created: "2018-10-11 23:09:21",
     * item_price: "150.0",
     * item_title: "小猪佩奇书包幼儿园佩琪乔治儿童宝宝可爱双肩背包男童女孩1-3岁5",
     * num_iid: "557739092081",
     * oid: "230632143621616822",
     * oid_str: "230632143621616822",
     * result: "good",
     * role: "buyer",
     * tid: "230632143621616822",
     * tid_str: "230632143621616822"
     * }
     * }
     * 字段解释查看地址: http://bigdata.taobao.com/api.htm?docId=55&docType=2
     */
    public String getOrderComment(String tid, String nick) {
        String url = "http://api.luckcrm.com/querycomment.php?tid=" + tid + "&nick=" + nick + "&rate_type=get&role=buyer";
        return returnContent(url);
    }

    /**
     * 获取商品详情信息 - v1
     *
     * @param num_iid 淘宝商品id  必填
     * @param nick    商户名称(旺旺名称)  必填
     * @return {
     * "approve_status":"onsale",
     * "delist_time":"2018-10-18 12:03:17",
     * "desc":"xxx",
     * "list_time":"2018-10-11 12:03:17",
     * "newprepay":"default",
     * "nick":"圣淘电子技术服务有限公司",
     * "num_iid":"576149782147",
     * "pic_url":"http://dddddd",
     * "price":"10.01",
     * "title":"公众号关注活动福利",
     * "skus_price":[ ]
     * }
     * 字段解释查看地址: http://bigdata.taobao.com/api.htm?docId=24625&docType=2
     */
    public String getGoodsInfo(String num_iid, String nick) {
        String url = "http://api.luckcrm.com/queryseller.php?nick=" + nick + "&num_iid=" + num_iid;
        return returnContent(url);
    }

    /**
     * 查询 淘宝店铺 最近三个月订单信息 - v1
     *
     * @param nick 商户名称(旺旺名称) 必填
     *             [
     *             {
     *             tid: "227252676240557283",
     *             seller_nick: "圣淘电子技术服务有限公司",
     *             status: "WAIT_SELLER_SEND_GOODS",
     *             modified: "2018-10-15 20:33:30",
     *             end_time: null,
     *             created: "2018-10-15 20:33:10",
     *             pay_time: "2018-10-15 20:33:20",
     *             num_iid: []
     *             }
     *             ]
     *             字段解释查看地址:http://bigdata.taobao.com/api.htm?docId=46&docType=2
     */
    public String getShopOrders(String nick) {
        String url = "http://api.luckcrm.com/querylist.php?nick=" + nick + "&page=1";
        return returnContent(url);
    }

    /**
     * 获取 店铺 详情信息 -v1
     * nick 商户名称(旺旺名称)  必填
     * {
     * "bulletin":[],
     * "cid":"1042",
     * "created":"2014-04-01 16:09:31",
     * "desc":"店铺详情",
     * "modified":"2018-10-11 15:18:29",
     * "nick":"圣淘电子技术服务有限公司",
     * "pic_path":"/75/b4/TB1Hvi4unqWBKNjSZFASuunSpXa.jpg",
     * "shop_score":{
     * "delivery_score":"5.0",
     * "item_score":"5.0",
     * "service_score":"5.0"
     * },
     * "sid":"109889707",
     * "title":"云店生活馆",
     * "deadline": "2019-08-30 00:00:00"
     * }
     * 字段解释查看地址: http://bigdata.taobao.com/api.htm?docId=68&docType=2
     * 注意：deadline为喜销宝过期时间，deadline
     */
    public String getShopInfo(String nick) {
        String url = "http://api.luckcrm.com/queryshop.php?nick=" + nick;
        return returnContent(url);
    }


    /*******************************************************************************************************************************************
     * 工具
     *******************************************************************************************************************************************/
    private String returnContent(String url) {
        long now = System.currentTimeMillis();
        log.info(now + "喜销宝请求url：" + url);
        ResponseEntity<String> forEntity = remoteApiRestTemplate.getForEntity(url, String.class);
        String responseBody = forEntity == null ? "{}" : forEntity.getBody();
        log.info(now + "喜销宝请求结果：" + responseBody);
        return responseBody;
    }

    public boolean isSuccess(String resultStr) {
        if (StringUtils.isBlank(resultStr)) {
            return false;
        }
        String trimStr = resultStr.trim();
        return !StringUtils.equals(trimStr, "[]") && !StringUtils.equals(trimStr, "{}");
    }


    public boolean isError(String resultStr) {
        return !isSuccess(resultStr);
    }
}
