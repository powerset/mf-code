#!/bin/bash

echo "******************** build.sh start ********************"

git pull

mvn clean package -DskipTests

echo "******************** build.sh end ********************"

