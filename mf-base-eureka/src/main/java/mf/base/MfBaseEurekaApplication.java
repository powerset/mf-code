package mf.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
@Slf4j
public class MfBaseEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MfBaseEurekaApplication.class, args);
        log.info("ELK启动日志标识:MF-CODE-EUREKA");
    }

}
