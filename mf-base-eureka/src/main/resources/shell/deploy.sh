#!/usr/bin/env bash
echo "------------------- kill java progress start --------------"
port="8099"
aliasname="mf-base-eureka"

echo "当前服务端口"${aliasname} ${port}
echo "试验性杀进程,如有报错请勿惊慌"

ps aux|grep ${port}|grep -v grep|awk '{print $2}'|xargs kill -9
echo "------------------- kill java progress end --------------"

echo "******************** deploy start ********************"

nohup java -Dserver.port=${port} -jar -Xmx256m -Xms32m /home/code/git-data/mf-code-parent/${aliasname}/target/${aliasname}-1.0-SNAPSHOT.jar --spring.profiles.active=test3> /home/code/git-data/mf-code-parent/${aliasname}/nohup.out 2>&1 &

echo "******************** deploy end ********************"
