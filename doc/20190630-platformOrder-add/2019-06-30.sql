CREATE TABLE `platform_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户号-主账号id',
  `shop_id` bigint(20) unsigned DEFAULT NULL COMMENT '店铺id',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户id',
  `type` int(4) NOT NULL DEFAULT '0' COMMENT '订单类型 0：平台提现 1：平台-付款 2：平台-入账 3:平台-退款',
  `pay_type` int(4) NOT NULL DEFAULT '1' COMMENT '支付方式：1:余额(预存金余额) 2:微信 3：支付宝',
  `order_no` varchar(255) NOT NULL DEFAULT '' COMMENT '订单编号 本系统的平台的订单编号 -- 特定方式生成 同种条件生成唯一值',
  `order_name` varchar(255) NOT NULL DEFAULT '' COMMENT '订单名称 -- 提示平台订单业务类型',
  `pub_app_id` varchar(255) NOT NULL DEFAULT '' COMMENT '公众号appid 冗余字段',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '订单状态 0：进行中 1：成功  2：失效 3：超时（只存在逻辑）',
  `total_fee` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '总金额,单位元',
  `trade_type` int(4) NOT NULL DEFAULT '0' COMMENT '交易方式：1：JSAPI--公众号支付2： NATIVE--原生扫码支付、3：APP--app支付 4:企业付款',
  `trade_id` varchar(255) NOT NULL DEFAULT '' COMMENT '流水号 微信流水号唯一标识',
  `remark` varchar(2048) DEFAULT NULL COMMENT '备注',
  `notify_time` datetime DEFAULT NULL COMMENT '回调处理系统时间',
  `payment_time` datetime DEFAULT NULL COMMENT '响应数据中的支付时间',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '更新时间',
  `req_json` text COMMENT '订单入参，json对象',
  `biz_type` int(11) NOT NULL DEFAULT '0' COMMENT '业务类型，1：订单成交-商户交易税，2：订单成交-平台商品类目抽佣，3:订单成交-上级缺失 用户分佣回收，4:订单成交-用户返利分佣税务部分回收，5:订单成交-用户返利分佣团队奖励部分回收，6,"转盘",7, "回填",8, "好评",9,"收藏",10,"商户计划",11, "免单",12, "新人",13, "助力",14, "拆红包",15, "加群享福利，扫一扫",16, "查看攻略，拿红包",17, "设置店长微信群",18, "邀请粉丝",19, "推荐粉丝成为店长",20, "新人红包，奖励到"',
  `biz_value` bigint(20) NOT NULL DEFAULT '0' COMMENT '业务类型针对的值：订单成交相关(1~5)此值为orderId， 任务完成相关(6)此值为 待定',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_no` (`order_no`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='平台订单表,用于平台充值,提现,查账';

UPDATE `common_dict` SET `value` = '[{\"amountCondition\":0,\"rebateRate\":10,\"commissionRate\":60,\"scale\":5,\"teamRewardRate\":4,\"taxRate\":26},{\"amountCondition\":2,\"rebateRate\":50,\"commissionRate\":20,\"scale\":5,\"teamRewardRate\":4,\"taxRate\":26}]' WHERE `type` = "shoppingMall" and `key` = "rebatePolicy";

