INSERT INTO `jkmf0`.`common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (42, 'shoppingMall', 'scale', '5', '', '', NULL, 0, '商城团队层级数', 999, '2019-05-15 10:15:37', '2019-05-15 10:15:37');


CREATE TABLE `order_biz` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `order_id` bigint(20) NOT NULL COMMENT '订单主键',
  `biz_type` int(11) NOT NULL COMMENT '业务类型，0："分销场景"',
  `biz_value` text COMMENT '业务信息',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '更新时间',
  `del` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记 . 0:正常 1:删除',
  PRIMARY KEY (`id`),
  KEY `index_order_id` (`order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单业务';


CREATE TABLE `product_distribution_audit_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `merchant_id` bigint(20) NOT NULL COMMENT '商户id',
  `shop_id` bigint(20) NOT NULL COMMENT '店铺id',
  `product_id` bigint(20) NOT NULL COMMENT '产品id',
  `plat_support_ratio` decimal(10,2) NOT NULL COMMENT '平台分销佣金比例,审核通过后,将此值写入product表',
  `status` int(11) NOT NULL COMMENT '审核状态 0:未审核 1:审核通过 -1:审核不通过 -2作废',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `audit_reason` varchar(255) DEFAULT NULL COMMENT '审核不通过原因',
  `audit_pics` varchar(1024) DEFAULT NULL COMMENT '审核图片["","",""]',
  `operator_id` bigint(20) DEFAULT NULL COMMENT '审核人',
  `ctime` datetime DEFAULT NULL COMMENT '创建时间',
  `utime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商品分销属性审核记录表';


