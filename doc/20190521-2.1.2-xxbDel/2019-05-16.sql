CREATE TABLE `product_group` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品分组id',
  `merchant_id` bigint(20) unsigned NOT NULL COMMENT '商户id',
  `shop_id` bigint(20) unsigned NOT NULL COMMENT '店铺id',
  `name` varchar(32) NOT NULL COMMENT '分组名',
  `sort` tinyint(4) NOT NULL DEFAULT '0' COMMENT '分组排序商家编辑，默认为0。正序排列',
  `goods_num` int(11) NOT NULL DEFAULT '0' COMMENT '冗余商品数量，方便查询',
  `del` int(11) NOT NULL DEFAULT '0' COMMENT '删除标识',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `INDEX_SHOPID` (`shop_id`) USING BTREE COMMENT 'shopId索引'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COMMENT='商品分组表，记录商户端创建修改分组信息。';

CREATE TABLE `product_group_map` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品分组映射id',
  `product_id` bigint(20) unsigned NOT NULL COMMENT '商品id',
  `parent_product_id` bigint(20) unsigned NOT NULL COMMENT '来源商品id',
  `group_id` bigint(20) unsigned NOT NULL COMMENT '分组id',
  `del` int(11) NOT NULL DEFAULT '0' COMMENT '删除标识',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `INDEX_GROUP_PRODUCT` (`product_id`,`group_id`) USING BTREE COMMENT '分组id商品id索引'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COMMENT='商品分组映射表，记录商品与分组的映射关系';


alter table `upay_balance` add  `openRedPack_balance` decimal(11,2) DEFAULT '0.00' COMMENT '拆红包收益余额' AFTER `shopping_balance`;
alter table `upay_wx_order` add  `mf_trade_type` int(4) NOT NULL  DEFAULT '2' COMMENT '魔方交易方式:1待结算 2已结算' AFTER `refund_order_id`;
alter table `product` add  `show_type` int(11) DEFAULT '0' COMMENT '展示类型（0：无，1：新人）' AFTER `third_price_max`;
alter table `product` add  `init_sales` int(11) DEFAULT '0' COMMENT '商户填入初始销量' AFTER `putaway_time`;
