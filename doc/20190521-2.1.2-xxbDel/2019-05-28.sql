alter table `product` add `num_iid` varchar(32) DEFAULT NULL COMMENT '淘宝商品id' AFTER `init_sales`;
alter table `product` add `commission_num` decimal(10,2) DEFAULT NULL COMMENT '佣金（抽取固定抽佣比后的佣金）';
alter table `product` add from_flag int(11) DEFAULT '0' COMMENT '来源标识（0：无，1：自营，2:分销。判断条件是id是否等于parent_id,等于是自营，不等于是分销）';

alter table `goods` add `product_id` bigint(20) DEFAULT NULL COMMENT 'product主键，2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余' AFTER `xxbtop_title`;
alter table `goods` add `sku_id` bigint(20) DEFAULT NULL COMMENT 'product_sku表主键 2.1版本人工审核版本修改，需要从product表查询活动关联商品数据，为保持与原逻辑基本一致，存储goods做冗余' AFTER `product_id`;

alter table `activity_def` add `finish_type` int(11) DEFAULT '1' COMMENT '任务完成方式（1:淘宝下单，2:微信下单，即小程序内直接下单）';
alter table `activity_def` add `audit_type` int(11) DEFAULT '1' COMMENT '审核方式（1:人工审核 2:自动审核）';

INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (43, 'shoppingMall', 'transactionRate', '2', '', '', NULL, 0, '商城交易税-- 百分比', 999, '2019-05-21 16:18:45', '2019-05-21 16:18:49');
INSERT INTO `common_dict`(`id`, `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime`) VALUES (44, 'shoppingMall', 'vipShopRate', '50', '', '', NULL, 0, '粉丝店铺分佣 -- 百分比', 999, '2019-05-21 16:18:45', '2019-05-21 16:18:49');
