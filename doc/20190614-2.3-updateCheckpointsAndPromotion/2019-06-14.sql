alter table `activity_def` add `parent_id` bigint(20) DEFAULT '0' COMMENT '定义活动的父级编号' AFTER `merchant_id`;
-- 填写数据
UPDATE `activity_def` SET parent_id = id;

alter table `user` add `role` int(20) DEFAULT '0' COMMENT '角色，0:普通用户；1:店长' AFTER `grant_status`;
alter table `user` add `role_time` datetime DEFAULT NULL COMMENT '成为角色时间' AFTER `grant_status`;
alter table `user` add `wx_code` varchar(255) DEFAULT NULL COMMENT '微信个人二维码' AFTER `cash_protocal`;
