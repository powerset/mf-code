CREATE TABLE `product_promotion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
  `merchant_id` bigint(20) NOT NULL COMMENT '供应商商户id（所属商户）',
  `shop_id` bigint(20) NOT NULL COMMENT '店铺id（所属店铺）',
  `type` int(11) DEFAULT NULL COMMENT '促销活动类型：1.本周特推',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '活动状态 0:待发布(默认值) 1:进行中 2:已停止',
  `product_detail` text COMMENT '推广产品描述',
  `promotion_pics` text COMMENT '推广图（n+图片） 存json数组 {"productPics":["http://2.jpg","http://1.jpg"],"buyerShowPics":["http://2.jpg","http://1.jpg"]}',
  `start_time` datetime NOT NULL COMMENT '活动开始时间',
  `end_time` datetime NOT NULL COMMENT '活动结束时间',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime DEFAULT NULL COMMENT '更新时间',
  `del` int(11) NOT NULL DEFAULT '0' COMMENT '标记删除 0 :正常 1:删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品促销活动表';