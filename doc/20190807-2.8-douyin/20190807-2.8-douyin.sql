alter table `merchant` add `role` int(11) DEFAULT '0' COMMENT '用户角色：默认0，集客魔方商户；1 抖带带主播' AFTER `name`;
alter table `merchant` add `grant_status` int(11) DEFAULT '0' COMMENT '用户授权状态，0:未授权，1:授权' AFTER `name`;
alter table `merchant` add `grant_time` datetime DEFAULT NULL COMMENT '用户授权时间' AFTER `name`;
alter table `merchant` add `avatar_url` varchar(255) DEFAULT NULL COMMENT '头像图片地址' AFTER `name`;

alter table `order` add `source` int(11) DEFAULT '0' COMMENT '订单来源 0:集客名品 1:抖带带 2:抖小铺';
UPDATE merchant_order set mf_trade_type = 8 WHERE type = 0 AND `status` = 1;
alter table `merchant` drop index UNIQUE_PHONE;
alter table `merchant_shop` MODIFY  COLUMN `title` varchar(2000) DEFAULT NULL COMMENT '集客魔方店铺标题 | 抖带带主播店铺描述'

