alter table `merchant_shop_coupon` add `limit_amount` decimal(10,2)  COMMENT '使用门槛金额，-1表示无门槛' ;
alter table `merchant_shop_coupon` add `type` int(11)  COMMENT '优惠券类型：1淘宝优惠券，2商品优惠券' ;
alter table `merchant_shop_coupon` add `stock_def` int(11)  COMMENT '发行总量' ;
alter table `merchant_shop_coupon` add `stock` int(11)  COMMENT '已发发行量' ;
alter table `merchant_shop_coupon` add `limit_per_num` int(11)  COMMENT '每人限领取张数' ;
alter table `merchant_shop_coupon` add `coupon_status` int(11)  COMMENT '优惠券状态: 1:发行中，2已结束' ;
alter table `merchant_shop_coupon` add `product_id` bigint(20)  COMMENT '商品ID' ;



alter table `user_coupon` add `coupon_id` bigint(20)  COMMENT '优惠券ID ,merchant_shop_coupon 的主键ID' ;
alter table `user_coupon` modify `type` int(11)  COMMENT '奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6计划类抽奖活动 7抽奖免单任务 8免单商品抽奖参与者 9新手有礼发起者 10新手有礼抽奖参与者 11助力任务 12拆红包活动 13拆红包活动助力者 25:新手任务-拆红包 29:集客优惠券' ;
alter table `user_coupon` modify `status` int(11)  COMMENT '0：未领奖，1：已领奖，2已使用 3已过期' ;

alter table `order` add `discount_type` int(11)  COMMENT '优惠类型（0：无优惠，1：优惠券，2：助力免单，3：抽奖免单）' ;
alter table `order` add `discount_id` bigint(20)  COMMENT '优惠id（优惠券类型为优惠券id，其他为相应的活动id）' ;

alter table `product_sku` add `effect_time` varchar(32)  COMMENT '生效时间，可以作为秒杀sku的区分' ;
alter table `product_sku` add `extra` varchar(256)  COMMENT 'sku额外字段（json对象：初始销量init_sales）' ;
