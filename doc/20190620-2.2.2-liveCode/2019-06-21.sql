-- 创建二维码表，活码管理与扫码计数
CREATE TABLE `common_code`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
	`parent_id` bigint(20) UNSIGNED NOT NULL COMMENT '父级记录id',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户id（user或者merchant）',
  `user_type` int(11) DEFAULT '0' COMMENT '用户类型（1：用户，2：商户）',
  `code_no` varchar(32) DEFAULT NULL COMMENT '二维码编号',
  `code_type` int(11) DEFAULT '0' COMMENT '二维码类型（1：活码，2：群二维码，3：个人微信号，4：公众号）',
  `code_status` int(11) DEFAULT '0' COMMENT '二维码使用状态（0：初始状态，1：使用中，2，主动停用，3：自动过期）',
  `code_url` varchar(512) DEFAULT NULL COMMENT '二维码码文本地址',
  `code_path` varchar(125) DEFAULT NULL COMMENT '二维码码图片地址',
  `scan_limit` int(11) DEFAULT '0' COMMENT '可展现次数',
  `scan_num` int(11) DEFAULT '0' COMMENT '剩余展现次数',
  `start_time` datetime NOT NULL COMMENT '二维码生效时间',
  `end_time` datetime NOT NULL COMMENT '二维码失效时间',
  `extra` varchar(1024) DEFAULT NULL COMMENT '个性化信息（logo图，替换图等 logo_path中间图片，reserve_path替代图片）',
  `del` int(11) NOT NULL DEFAULT '0' COMMENT '删除标识 0:未删除 1:已删除',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT '二维码表，活码管理与扫码计数' ROW_FORMAT = Dynamic;
