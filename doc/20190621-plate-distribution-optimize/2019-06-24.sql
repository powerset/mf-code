alter table `product_group` add `type` int(11) default 1 COMMENT '分组类型（1：商品分组）' AFTER `shop_id`;

alter table `category_taobao` add `sort` int(11) NOT NULL DEFAULT '0' COMMENT '分组排序运营编辑，默认为0。正序排列' AFTER `level`;
alter table `category_taobao` add `distribution_ratio` DECIMAL(10,2) DEFAULT NULL COMMENT '类目佣金比例' AFTER `level`;

-- TODO 需要添加类目对应记录

-- 类目表修改字段
UPDATE  `category_taobao` SET `spell` = 'dnyjother_typexsqother_typednzb', `name` = '电脑硬件/显示器/电脑周边', `sid` = '11', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621318;
UPDATE  `category_taobao` SET `spell` = 'bjbdn', `name` = '笔记本电脑', `sid` = '1101', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621315;
UPDATE  `category_taobao` SET `spell` = 'mpother_typeother_typempother_typeother_typeipodother_typelyb', `name` = 'MP3/MP4/iPod/录音笔', `sid` = '1201', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621314;
UPDATE  `category_taobao` SET `spell` = 'tnl', `name` = '淘女郎', `sid` = '120894001', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697845;
UPDATE  `category_taobao` SET `spell` = 'zc', `name` = '众筹', `sid` = '121266001', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697843;
UPDATE  `category_taobao` SET `spell` = 'txother_typeyexother_typeqzx', `name` = '童鞋/婴儿鞋/亲子鞋', `sid` = '122650005', `parent_id` = '1000000005', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2935124;
UPDATE  `category_taobao` SET `spell` = 'zxcother_typeqxzbother_typelpj', `name` = '自行车/骑行装备/零配件', `sid` = '122684003', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076042;
UPDATE  `category_taobao` SET `spell` = 'jtbj', `name` = '家庭保健', `sid` = '122718004', `parent_id` = '1000000007', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1759804;
UPDATE  `category_taobao` SET `spell` = 'jjby', `name` = '居家布艺', `sid` = '122852001', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447368;
UPDATE  `category_taobao` SET `spell` = 'snzl', `name` = '收纳整理', `sid` = '122928002', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336273;
UPDATE  `category_taobao` SET `spell` = 'jqypother_typelp', `name` = '节庆用品/礼品', `sid` = '122950001', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336271;
UPDATE  `category_taobao` SET `spell` = 'cyj', `name` = '餐饮具', `sid` = '122952001', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336272;
UPDATE  `category_taobao` SET `spell` = 'altxzslm', `name` = '阿里通信专属类目', `sid` = '123536002', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697847;
UPDATE  `category_taobao` SET `spell` = 'pptjother_typeppytjother_typefwq', `name` = '品牌台机/品牌一体机/服务器', `sid` = '124044001', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621325;
UPDATE  `category_taobao` SET `spell` = 'qwdz', `name` = '全屋定制', `sid` = '124050001', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447369;
UPDATE  `category_taobao` SET `spell` = 'znsb', `name` = '智能设备', `sid` = '124242008', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697848;
UPDATE  `category_taobao` SET `spell` = 'ddcother_typepjother_typejtgj', `name` = '电动车/配件/交通工具', `sid` = '124354002', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076043;
UPDATE  `category_taobao` SET `spell` = 'c', `name` = '茶', `sid` = '124458005', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336274;
UPDATE  `category_taobao` SET `spell` = 'nywz', `name` = '农用物资', `sid` = '124466001', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697849;
UPDATE  `category_taobao` SET `spell` = 'njother_typenjother_typenm', `name` = '农机/农具/农膜', `sid` = '124468001', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697850;
UPDATE  `category_taobao` SET `spell` = 'cmother_typeyzwz', `name` = '畜牧/养殖物资', `sid` = '124470001', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697851;
UPDATE  `category_taobao` SET `spell` = 'mwother_typedmother_typezbother_typecosother_typezy', `name` = '模玩/动漫/周边/cos/桌游', `sid` = '124484008', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450815;
UPDATE  `category_taobao` SET `spell` = 'snsjs', `name` = '室内设计师', `sid` = '124568010', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697852;
UPDATE  `category_taobao` SET `spell` = 'zxfw', `name` = '装修服务', `sid` = '124698018', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697853;
UPDATE  `category_taobao` SET `spell` = 'pmhzy', `name` = '拍卖会专用', `sid` = '124844002', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697854;
UPDATE  `category_taobao` SET `spell` = 'essm', `name` = '二手数码', `sid` = '124852003', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621326;
UPDATE  `category_taobao` SET `spell` = 'djyw', `name` = '到家业务', `sid` = '125102006', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697855;
UPDATE  `category_taobao` SET `spell` = 'shylcz', `name` = '生活娱乐充值', `sid` = '126602002', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697856;
UPDATE  `category_taobao` SET `spell` = 'jzdsgy', `name` = '家装灯饰光源', `sid` = '126700003', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697857;
UPDATE  `category_taobao` SET `spell` = 'mrmtyq', `name` = '美容美体仪器', `sid` = '126762001', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697858;
UPDATE  `category_taobao` SET `spell` = 'fzmlother_typeflother_typept', `name` = '纺织面料/辅料/配套', `sid` = '127442006', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697863;
UPDATE  `category_taobao` SET `spell` = 'jscljzp', `name` = '金属材料及制品', `sid` = '127450004', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697864;
UPDATE  `category_taobao` SET `spell` = 'xscljzp', `name` = '橡塑材料及制品', `sid` = '127452002', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697865;
UPDATE  `category_taobao` SET `spell` = 'byother_typeccother_typewlsb', `name` = '搬运/仓储/物流设备', `sid` = '127458007', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697862;
UPDATE  `category_taobao` SET `spell` = 'rhother_typejzother_typesjother_typesyshc', `name` = '润滑/胶粘/试剂/实验室耗材', `sid` = '127484003', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697860;
UPDATE  `category_taobao` SET `spell` = 'bzjother_typelbjother_typegyhc', `name` = '标准件/零部件/工业耗材', `sid` = '127492006', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697859;
UPDATE  `category_taobao` SET `spell` = 'jxsb', `name` = '机械设备', `sid` = '127508003', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697861;
UPDATE  `category_taobao` SET `spell` = 'smxjother_typedfxjother_typesxj', `name` = '数码相机/单反相机/摄像机', `sid` = '14', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621313;
UPDATE  `category_taobao` SET `spell` = 'sj', `name` = '手机', `sid` = '1512', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621312;
UPDATE  `category_taobao` SET `spell` = 'nzother_typensjp', `name` = '女装/女士精品', `sid` = '16', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3393;
UPDATE  `category_taobao` SET `spell` = 'nsnyother_typensnyother_typejjf', `name` = '女士内衣/男士内衣/家居服', `sid` = '1625', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3390;
UPDATE  `category_taobao` SET `spell` = 'mrhfother_typemtother_typejy', `name` = '美容护肤/美体/精油', `sid` = '1801', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959886;
UPDATE  `category_taobao` SET `spell` = 'dwother_typepjother_typeyxother_typegl', `name` = '电玩/配件/游戏/攻略', `sid` = '20', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621324;
UPDATE  `category_taobao` SET `spell` = 'jjry', `name` = '居家日用', `sid` = '21', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336259;
UPDATE  `category_taobao` SET `spell` = 'gdother_typeybother_typezhother_typesz', `name` = '古董/邮币/字画/收藏', `sid` = '23', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450809;
UPDATE  `category_taobao` SET `spell` = 'wjother_typetcother_typeyzother_typejmother_typemx', `name` = '玩具/童车/益智/积木/模型', `sid` = '25', `parent_id` = '1000000005', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2935123;
UPDATE  `category_taobao` SET `spell` = 'qcother_typeypother_typepjother_typegz', `name` = '汽车/用品/配件/改装', `sid` = '26', `parent_id` = '1000000012', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5498027;
UPDATE  `category_taobao` SET `spell` = 'jzzc', `name` = '家装主材', `sid` = '27', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447362;
UPDATE  `category_taobao` SET `spell` = 'zippoother_typersjdother_typeyj', `name` = 'ZIPPO/瑞士军刀/眼镜', `sid` = '28', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959889;
UPDATE  `category_taobao` SET `spell` = 'crypother_typeqqyp', `name` = '成人用品/情趣用品', `sid` = '2813', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336265;
UPDATE  `category_taobao` SET `spell` = 'cwother_typecwspjyp', `name` = '宠物/宠物食品及用品', `sid` = '29', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450813;
UPDATE  `category_taobao` SET `spell` = 'nz', `name` = '男装', `sid` = '30', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3391;
UPDATE  `category_taobao` SET `spell` = 'sjother_typezzother_typebz', `name` = '书籍/杂志/报纸', `sid` = '33', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450810;
UPDATE  `category_taobao` SET `spell` = 'ylother_typeysother_typemxother_typeyx', `name` = '音乐/影视/明星/音像', `sid` = '34', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450811;
UPDATE  `category_taobao` SET `spell` = 'nfother_typefsother_typeyypother_typels', `name` = '奶粉/辅食/营养品/零食', `sid` = '35', `parent_id` = '1000000005', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2935119;
UPDATE  `category_taobao` SET `spell` = 'txqqzq', `name` = '腾讯QQ专区', `sid` = '40', `parent_id` = '1000000013', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1;
UPDATE  `category_taobao` SET `spell` = 'lsother_typejgother_typetc', `name` = '零食/坚果/特产', `sid` = '50002766', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336263;
UPDATE  `category_taobao` SET `spell` = 'grhlother_typebjother_typeamqc', `name` = '个人护理/保健/按摩器材', `sid` = '50002768', `parent_id` = '1000000007', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1759803;
UPDATE  `category_taobao` SET `spell` = 'xbpjother_typerxnbother_typenb', `name` = '箱包皮具/热销女包/男包', `sid` = '50006842', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3395;
UPDATE  `category_taobao` SET `spell` = 'nx', `name` = '女鞋', `sid` = '50006843', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3394;
UPDATE  `category_taobao` SET `spell` = 'xhsdother_typehhfzother_typelzyy', `name` = '鲜花速递/花卉仿真/绿植园艺', `sid` = '50007216', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481612;
UPDATE  `category_taobao` SET `spell` = 'bgsbother_typehcother_typexgfw', `name` = '办公设备/耗材/相关服务', `sid` = '50007218', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621322;
UPDATE  `category_taobao` SET `spell` = 'cymskq', `name` = '餐饮美食卡券', `sid` = '50008075', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481608;
UPDATE  `category_taobao` SET `spell` = 'other_typecsmpj', `name` = '3C数码配件', `sid` = '50008090', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621320;
UPDATE  `category_taobao` SET `spell` = 'csyp', `name` = '床上用品', `sid` = '50008163', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447366;
UPDATE  `category_taobao` SET `spell` = 'zzjj', `name` = '住宅家具', `sid` = '50008164', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447360;
UPDATE  `category_taobao` SET `spell` = 'tzother_typeyezother_typeqzz', `name` = '童装/婴儿装/亲子装', `sid` = '50008165', `parent_id` = '1000000005', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2935122;
UPDATE  `category_taobao` SET `spell` = 'fspjother_typepdother_typemzother_typewj', `name` = '服饰配件/皮带/帽子/围巾', `sid` = '50010404', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3396;
UPDATE  `category_taobao` SET `spell` = 'ydother_typeyjother_typejsother_typeqmyp', `name` = '运动/瑜伽/健身/球迷用品', `sid` = '50010728', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076037;
UPDATE  `category_taobao` SET `spell` = 'czother_typexsother_typemzgj', `name` = '彩妆/香水/美妆工具', `sid` = '50010788', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959885;
UPDATE  `category_taobao` SET `spell` = 'zbother_typezsother_typefcother_typehj', `name` = '珠宝/钻石/翡翠/黄金', `sid` = '50011397', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959888;
UPDATE  `category_taobao` SET `spell` = 'wyzbother_typeyxbother_typezhother_typedl', `name` = '网游装备/游戏币/帐号/代练', `sid` = '50011665', `parent_id` = '1000000013', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2;
UPDATE  `category_taobao` SET `spell` = 'ydfother_typexxfz', `name` = '运动服/休闲服装', `sid` = '50011699', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076039;
UPDATE  `category_taobao` SET `spell` = 'lxnx', `name` = '流行男鞋', `sid` = '50011740', `parent_id` = '1000000002', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3392;
UPDATE  `category_taobao` SET `spell` = 'tjjdother_typetskzother_typegylg', `name` = '特价酒店/特色客栈/公寓旅馆', `sid` = '50011949', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450808;
UPDATE  `category_taobao` SET `spell` = 'yydq', `name` = '影音电器', `sid` = '50011972', `parent_id` = '1000000007', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1759800;
UPDATE  `category_taobao` SET `spell` = 'ydxnew', `name` = '运动鞋new', `sid` = '50012029', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076040;
UPDATE  `category_taobao` SET `spell` = 'cfdq', `name` = '厨房电器', `sid` = '50012082', `parent_id` = '1000000007', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1759802;
UPDATE  `category_taobao` SET `spell` = 'shdq', `name` = '生活电器', `sid` = '50012100', `parent_id` = '1000000007', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1759801;
UPDATE  `category_taobao` SET `spell` = 'sckother_typeupother_typeccother_typeydyp', `name` = '闪存卡/U盘/存储/移动硬盘', `sid` = '50012164', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621321;
UPDATE  `category_taobao` SET `spell` = 'spother_typelxssother_typessspx', `name` = '饰品/流行首饰/时尚饰品新', `sid` = '50013864', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959890;
UPDATE  `category_taobao` SET `spell` = 'hwother_typedsother_typeyyother_typelxyp', `name` = '户外/登山/野营/旅行用品', `sid` = '50013886', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076038;
UPDATE  `category_taobao` SET `spell` = 'wdother_typewlfwother_typerj', `name` = '网店/网络服务/软件', `sid` = '50014811', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481613;
UPDATE  `category_taobao` SET `spell` = 'npother_typexhother_typewbother_typetcc', `name` = '尿片/洗护/喂哺/推车床', `sid` = '50014812', `parent_id` = '1000000005', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2935120;
UPDATE  `category_taobao` SET `spell` = 'jypx', `name` = '教育培训', `sid` = '50014927', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481610;
UPDATE  `category_taobao` SET `spell` = 'jtother_typegrqjgj', `name` = '家庭/个人清洁工具', `sid` = '50016348', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336261;
UPDATE  `category_taobao` SET `spell` = 'cfother_typepryj', `name` = '厨房/烹饪用具', `sid` = '50016349', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336260;
UPDATE  `category_taobao` SET `spell` = 'lymmother_typenbghother_typedwp', `name` = '粮油米面/南北干货/调味品', `sid` = '50016422', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336264;
UPDATE  `category_taobao` SET `spell` = 'lqother_typejtother_typegqother_typepj', `name` = '乐器/吉他/钢琴/配件', `sid` = '50017300', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450812;
UPDATE  `category_taobao` SET `spell` = 'dzcdother_typedzsother_typewhyp', `name` = '电子词典/电纸书/文化用品', `sid` = '50018004', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621323;
UPDATE  `category_taobao` SET `spell` = 'diydn', `name` = 'DIY电脑', `sid` = '50018222', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621317;
UPDATE  `category_taobao` SET `spell` = 'wlsbother_typewlxg', `name` = '网络设备/网络相关', `sid` = '50018264', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621319;
UPDATE  `category_taobao` SET `spell` = 'xfk', `name` = '消费卡', `sid` = '50019095', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481609;
UPDATE  `category_taobao` SET `spell` = 'pbdnother_typemid', `name` = '平板电脑/MID', `sid` = '50019780', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621316;
UPDATE  `category_taobao` SET `spell` = 'ctzbyyp', `name` = '传统滋补营养品', `sid` = '50020275', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336262;
UPDATE  `category_taobao` SET `spell` = 'jcjc', `name` = '基础建材', `sid` = '50020332', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447363;
UPDATE  `category_taobao` SET `spell` = 'wjother_typegj', `name` = '五金/工具', `sid` = '50020485', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447364;
UPDATE  `category_taobao` SET `spell` = 'dzother_typedg', `name` = '电子/电工', `sid` = '50020579', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447365;
UPDATE  `category_taobao` SET `spell` = 'syother_typebgjj', `name` = '商业/办公家具', `sid` = '50020611', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447361;
UPDATE  `category_taobao` SET `spell` = 'jjsp', `name` = '家居饰品', `sid` = '50020808', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447358;
UPDATE  `category_taobao` SET `spell` = 'tssgy', `name` = '特色手工艺', `sid` = '50020857', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447359;
UPDATE  `category_taobao` SET `spell` = 'yfzother_typeycfypother_typeyy', `name` = '孕妇装/孕产妇用品/营养', `sid` = '50022517', `parent_id` = '1000000005', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 2935121;
UPDATE  `category_taobao` SET `spell` = 'djd', `name` = '大家电', `sid` = '50022703', `parent_id` = '1000000007', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1759799;
UPDATE  `category_taobao` SET `spell` = 'mfhfother_typejf', `name` = '美发护发/假发', `sid` = '50023282', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959887;
UPDATE  `category_taobao` SET `spell` = 'fcother_typezfother_typexfother_typeesfother_typewtfw', `name` = '房产/租房/新房/二手房/委托服务', `sid` = '50023575', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481614;
UPDATE  `category_taobao` SET `spell` = 'otcypother_typeylqxother_typejsyp', `name` = 'OTC药品/医疗器械/计生用品', `sid` = '50023717', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336266;
UPDATE  `category_taobao` SET `spell` = 'qt', `name` = '其他', `sid` = '50023724', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697846;
UPDATE  `category_taobao` SET `spell` = 'zxsjother_typesgother_typejl', `name` = '装修设计/施工/监理', `sid` = '50023804', `parent_id` = '1000000008', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3447367;
UPDATE  `category_taobao` SET `spell` = 'dzyqjsc', `name` = '电子元器件市场', `sid` = '50024099', `parent_id` = '1000000004', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1621311;
UPDATE  `category_taobao` SET `spell` = 'xcother_typeesc', `name` = '新车/二手车', `sid` = '50024971', `parent_id` = '1000000012', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5498028;
UPDATE  `category_taobao` SET `spell` = 'gxdzother_typesjfwother_typediy', `name` = '个性定制/设计服务/DIY', `sid` = '50025004', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481615;
UPDATE  `category_taobao` SET `spell` = 'dyother_typeycother_typetyss', `name` = '电影/演出/体育赛事', `sid` = '50025110', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481616;
UPDATE  `category_taobao` SET `spell` = 'bdhshfw', `name` = '本地化生活服务', `sid` = '50025111', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481618;
UPDATE  `category_taobao` SET `spell` = 'xhqjjother_typewsjother_typezother_typexx', `name` = '洗护清洁剂/卫生巾/纸/香薰', `sid` = '50025705', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336267;
UPDATE  `category_taobao` SET `spell` = 'djxlother_typeqzsgother_typelyfw', `name` = '度假线路/签证送关/旅游服务', `sid` = '50025707', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450807;
UPDATE  `category_taobao` SET `spell` = 'kfother_typempother_typecy', `name` = '咖啡/麦片/冲饮', `sid` = '50026316', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336268;
UPDATE  `category_taobao` SET `spell` = 'xxyl', `name` = '休闲娱乐', `sid` = '50026523', `parent_id` = '1000000013', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481607;
UPDATE  `category_taobao` SET `spell` = 'gwthq', `name` = '购物提货券', `sid` = '50026555', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481606;
UPDATE  `category_taobao` SET `spell` = 'bjspother_typessyybcsp', `name` = '保健食品/膳食营养补充食品', `sid` = '50026800', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336269;
UPDATE  `category_taobao` SET `spell` = 'scrlother_typexxsgother_typess', `name` = '水产肉类/新鲜蔬果/熟食', `sid` = '50050359', `parent_id` = '1000000001', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 4336270;
UPDATE  `category_taobao` SET `spell` = 'hqother_typesyother_typesxfw', `name` = '婚庆/摄影/摄像服务', `sid` = '50050471', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481611;
UPDATE  `category_taobao` SET `spell` = 'mtcother_typezbother_typepj', `name` = '摩托车/装备/配件', `sid` = '50074001', `parent_id` = '1000000012', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5498029;
UPDATE  `category_taobao` SET `spell` = 'wldpdjother_typeyhq', `name` = '网络店铺代金/优惠券', `sid` = '50158001', `parent_id` = '1000000006', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5481617;
UPDATE  `category_taobao` SET `spell` = 'jdmpother_typeyyycother_typezby', `name` = '景点门票/演艺演出/周边游', `sid` = '50454031', `parent_id` = '1000000010', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5450814;
UPDATE  `category_taobao` SET `spell` = 'sb', `name` = '手表', `sid` = '50468001', `parent_id` = '1000000003', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 1959891;
UPDATE  `category_taobao` SET `spell` = 'ydbother_typehwbother_typepj', `name` = '运动包/户外包/配件', `sid` = '50510002', `parent_id` = '1000000009', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5076041;
UPDATE  `category_taobao` SET `spell` = 'bz', `name` = '包装', `sid` = '98', `parent_id` = '1000000011', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 5697844;
UPDATE  `category_taobao` SET `spell` = 'wlyxdk', `name` = '网络游戏点卡', `sid` = '99', `parent_id` = '1000000013', `type` = 1, `level` = 1, `distribution_ratio` = NULL, `sort` = 0, `ctime` = '2019-06-27 10:15:37', `utime` = '2019-06-27 10:15:37' WHERE `id` = 3;

-- 类目表新增数据
INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000001', '百货食品', '1000000001', '0', 1, 0, 10.00, 1, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000002', '服装鞋包', '1000000002', '0', 1, 0, 10.00, 2, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000003', '美妆饰品', '1000000003', '0', 1, 0, 10.00, 3, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000004', '手机数码', '1000000004', '0', 1, 0, 10.00, 4, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000005', '母婴用品', '1000000005', '0', 1, 0, 10.00, 5, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000006', '生活服务', '1000000006', '0', 1, 0, 10.00, 6, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000007', '家用电器', '1000000007', '0', 1, 0, 10.00, 7, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000008', '家居建材', '1000000008', '0', 1, 0, 10.00, 8, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000009', '运动户外', '1000000009', '0', 1, 0, 10.00, 9, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000010', '文化玩乐', '1000000010', '0', 1, 0, 10.00, 10, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000011', '其他商品', '1000000011', '0', 1, 0, 10.00, 11, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000012', '汽配摩托', '1000000012', '0', 1, 0, 10.00, 12, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );

INSERT INTO `category_taobao` ( `spell`, `name`, `sid`, `parent_id`, `type`, `level`, `distribution_ratio`, `sort`, `ctime`, `utime` )
VALUES
	( 'new_1000000013', '游戏话费', '1000000013', '0', 1, 0, 10.00, 13, '2019-06-27 00:00:00', '2019-06-27 00:00:00' );


-- common_dict 新增
INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000001', '["https://asset.wxyundian.com/data/platFromDistribution/1000000001.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000002', '["https://asset.wxyundian.com/data/platFromDistribution/1000000002.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000003', '["https://asset.wxyundian.com/data/platFromDistribution/1000000003.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000004', '["https://asset.wxyundian.com/data/platFromDistribution/1000000004.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000005', '["https://asset.wxyundian.com/data/platFromDistribution/1000000005.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000006', '["https://asset.wxyundian.com/data/platFromDistribution/1000000006.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000007', '["https://asset.wxyundian.com/data/platFromDistribution/1000000007.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000008', '["https://asset.wxyundian.com/data/platFromDistribution/1000000008.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000009', '["https://asset.wxyundian.com/data/platFromDistribution/1000000009.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000010', '["https://asset.wxyundian.com/data/platFromDistribution/1000000010.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000011', '["https://asset.wxyundian.com/data/platFromDistribution/1000000011.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000012', '["https://asset.wxyundian.com/data/platFromDistribution/1000000012.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000013', '["https://asset.wxyundian.com/data/platFromDistribution/1000000013.jpg"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );

INSERT INTO `common_dict` (  `type`, `key`, `value`, `value1`, `value2`, `value3`, `parent_id`, `remark`, `sort`, `ctime`, `utime` )
VALUES
	( 'platformDistribution', '1000000000', '["https://asset.wxyundian.com/public/applet/sixthPashe/mall-default.png"]', '', '', '', 0, '', 999, '2019-06-27 10:15:37', '2019-05-15 10:15:37' );
