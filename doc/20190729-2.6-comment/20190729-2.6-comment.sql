CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` bigint(20) DEFAULT NULL,
  `merchant_id` bigint(20) NOT NULL COMMENT '商户id',
  `shop_id` bigint(20) NOT NULL COMMENT '店铺id',
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `order_no` varchar(255) NOT NULL COMMENT '订单编号 本系统的订单编号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '谁评论就填睡的id，若role=1则userId为用户id,若role=2则userId=merchantId',
  `goods_parent_id` bigint(20) NOT NULL COMMENT '商品的parentid',
  `goods_id` bigint(20) NOT NULL COMMENT '商品id',
  `sku_id` bigint(20) NOT NULL COMMENT '商品skuid',
  `status` int(10) DEFAULT NULL COMMENT '-1初始值,0买家已评论，1审核通过，2不通过',
  `reason` varchar(256) DEFAULT NULL COMMENT '审核不通过的理由',
  `contents` varchar(256) DEFAULT NULL COMMENT '评论信息',
  `pics` varchar(1024) DEFAULT NULL COMMENT '评论上传的图片信息，json字符串 ["url1","url2"]',
  `total_stars` decimal(10,0) DEFAULT NULL COMMENT '总评分',
  `comment_stars_detail` varchar(255) DEFAULT NULL COMMENT '评论等级详情，json格式 {"desStar":2,"logisticsStar":3,"serviceStar":4}',
  `submit_time` datetime DEFAULT NULL COMMENT '提交评价时间',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `utime` datetime DEFAULT NULL COMMENT '修改时间',
  `del` int(10) NOT NULL DEFAULT '0' COMMENT '是否删除，0未删除，1已删除，默认是0',
  `role` int(10) DEFAULT NULL COMMENT '1用户，2商户，3平台',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- 老数据处理
INSERT INTO `comment` (parent_id,merchant_id,shop_id,`order_id`,order_no,user_id,goods_id,sku_id,`status`,utime,ctime,del,role,goods_parent_id)
SELECT
o.*,
p.parent_id as goods_parent_id
FROM
(SELECT
 0 parent_id,
 merchant_id,
 shop_id,
 id as order_id,
 order_no,
 user_id,
 goods_id,
	sku_id,
	-1 `status`,
 deal_time as utime,
 deal_time as ctime,
 0 del,
 1 role
FROM
 `order`
WHERE
 type = 1
 AND `status` = 3
 AND id NOT IN (
 SELECT
  pay.id
 FROM
  `order` AS pay
  JOIN `order` AS refund ON refund.refund_order_id = pay.id
 WHERE
  pay.`status` = 3
  AND pay.type = 1
  AND refund.type IN ( 2, 3 )
 AND refund.`status` IN ( 0, 1, 2 )
 )) AS o
 join
 product as p
 on
 o.goods_id = p.id;
