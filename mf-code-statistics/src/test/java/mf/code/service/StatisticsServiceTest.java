package mf.code.service;/**
 * create by qc on 2019/3/14 0014
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.MfCodeStatisticsApplication;
import mf.code.statistics.api.v1.ActivityStatisticsV1Api;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.utils.DateUtil;
import mf.code.statistics.mongo.domain.UserAction;
import mf.code.statistics.mongo.repository.MonitorDataRepository;
import mf.code.statistics.service.MonitorDataService;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.autoconfigure.RocketMQProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @author gbf
 * 2019/3/14 0014、14:53
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MfCodeStatisticsApplication.class)
@Slf4j
public class StatisticsServiceTest {
    @Autowired
    ActivityStatisticsV1Api activityStatisticsV1Api;
    @Autowired
    MonitorDataService monitorDataService;

    @Autowired
    private MonitorDataRepository monitorDataRepository;

    @Test
    public void rankingList(){
        Date beginDate = DateUtil.addDay(new Date(), -19);
        Date endDate = new Date();
        Date dateAfterZero = DateUtil.getDateAfterZero(beginDate);
        Date dateBeforeZero = DateUtil.getDateBeforeZero(endDate);

        List<UserAction> pvListByDate = monitorDataRepository.findPVListByDate(dateAfterZero, dateBeforeZero,null);
        System.out.println(pvListByDate);
    }

    @Test
    public void mq(){
        DefaultMQProducer producer = new DefaultMQProducer("no");
        producer.setNamesrvAddr("49.234.154.166:8060");
        try {
            producer.start();
            Message message = new Message("top1","tag1","message is qc".getBytes());
            SendResult send = producer.send(message);
            System.out.println(send);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 1 头部
     */
    @Test
    public void totalData() {
        SimpleResponse response = activityStatisticsV1Api.totalData("8");
        System.out.println(response.getData());
    }

    /**
     * 2 近期活动
     */
    @Test
    public void recentActivityData() {
        String shopId = "8";
        Integer activeType = 1;
        Integer dayTime = 1;
        String startDate = "2019-01-01";
        String endDate = "2019-03-15";
        String indexArray = "[1,3]";
        SimpleResponse response = activityStatisticsV1Api.recentActivityData(shopId, activeType, dayTime, startDate, endDate, indexArray);
        System.out.println(response.getMessage());
        System.out.println(response.getData());
    }

    /**
     * 3 参与情况
     */
    @Test
    public void participate() {
        String shopId = "8";
        Integer dayTime = 3;
        String startDate = "2019-01-01";
        String endDate = "2019-03-15";
        SimpleResponse response = activityStatisticsV1Api.participate(shopId, dayTime, startDate, endDate);
        System.out.println(response.getMessage());
        System.out.println(response.getData());

    }
    /**
     * 4 完成
     */
    @Test
    public void finish(){
        String shopId = "8";
        Integer dayTime = 1;
        String startDate = "2019-01-01";
        String endDate = "2019-03-15";
        SimpleResponse response = activityStatisticsV1Api.finish(shopId, dayTime, startDate, endDate);
        System.out.println(response.getMessage());
        System.out.println(response.getData());
    }

    /**
     * 5 分享
     */
    @Test
    public void share() {
        String shopId = "8";
        Integer dayTime = 1;
        String startDate = "2019-01-01";
        String endDate = "2019-03-15";
        SimpleResponse response = activityStatisticsV1Api.share(shopId, dayTime, startDate, endDate);
        System.out.println(response.getMessage());
        System.out.println(response.getData());
    }


    @Test
    public void create(){
        UserAction monitorData = new UserAction();
        monitorData.setUid("777");
        monitorData.setOpenid("777openid");
        monitorData.setMid("1");
        monitorData.setSid("2");
        monitorData.setEvent("click");
        monitorData.setPath("/page/envelope");
        monitorData.setScene("1");
        monitorData.setVersion("1.0");
        monitorData.setCurrent("20190527");
        monitorData.setIp("10.1.1.1");
        monitorData.setUserAgent("User-Agent");
        UserAction userAction = monitorDataService.insertMonitorData(monitorData);
        System.out.println(userAction);
    }
}
