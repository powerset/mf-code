package mf.code.loginLog;/**
 * create by qc on 2019/7/31 0031
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.MfCodeStatisticsApplication;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.merchant.dto.MerchantLoginResultDTO;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 登陆
 * @author gbf
 * 2019/7/31 0031、10:15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MfCodeStatisticsApplication.class)
@Slf4j
public class LoginLogTest {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;


    @Test
    public void loginLog() {

        MerchantLoginResultDTO resultDTO = new MerchantLoginResultDTO();
        for (Long i = 0L; i < 100; i++) {
            resultDTO.setMerchantId(i);
            rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.STATISTICS_REPORT_MERCHANT_LOGIN), JSON.toJSONString(resultDTO));
        }

    }
}
