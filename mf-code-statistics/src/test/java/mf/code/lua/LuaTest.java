package mf.code.lua;/**
 * create by qc on 2019/8/1 0001
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.MfCodeStatisticsApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gbf
 * 2019/8/1 0001、17:29
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MfCodeStatisticsApplication.class)
@Slf4j
public class LuaTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    @Qualifier("realTimeRankScript")
    private DefaultRedisScript<List> script;

    String goodsId = "goodsId-v2-4";

    @Test
    public void test() {
        String zsetKey = "lua:zsetKey:date-v2-1";
        String setKeyIsGoodsId = "lua:" + goodsId;
        String setValIsUserId = "lua:userId-v2-2";

        System.out.println("\n\n\n ------------------------------lua-------------------------------");

        List<String> keyList = new ArrayList<>();
        keyList.add(setKeyIsGoodsId);
        keyList.add(zsetKey);
        List execute = stringRedisTemplate.execute(script, keyList, setValIsUserId, goodsId);
        System.out.println(execute);

        System.out.println("------------------------------lua-------------------------------\n\n\n");
    }


}
