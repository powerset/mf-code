package mf.code.common.tool;/**
 * create by qc on 2019/3/21 0021
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.MfCodeStatisticsApplication;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.utils.DateUtil;
import mf.code.statistics.po.FissionUser;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * @author gbf
 * 2019/3/21 0021、10:37
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MfCodeStatisticsApplication.class)
@Slf4j
public class DateAssertTest {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Test
    public void testFission(){
        FissionUser fissionUser  =new FissionUser();
        fissionUser.setMid("88");
        fissionUser.setSid("888");
        fissionUser.setActivityId("a8");
        fissionUser.setCtime(new Date());
        fissionUser.setParentId("parent1");
        fissionUser.setSubId("sub1");
        fissionUser.setGoodsId("goods1");

        rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.STATISTICS_REPORT_FISSION), JSON.toJSONString(fissionUser));
    }


    @Test
    public void testAssert() {
        Date end = DateTool.end(new Date());
        System.out.println(end.getTime());
        String dateStr = "2019-2-31";
        String format = DateUtil.LONG_DATE_FORMAT;
        //Assert.canConvertDate(dateStr, format, "日期错误");
    }

    @Test
    public void testBeginEndDate(){
        String beginStr = "2019-3-1";
        String endStr = "2019-01-01";
        String format = DateUtil.LONG_DATE_FORMAT;
        //Assert.canConvertDate(beginStr,endStr, format, "日期错误");
    }

    @Test
    public void testNumberAssert(){
        String string1 = "1,2,3,4";
        //Assert.isIntegerArray(string1,",","含有非数字");
        System.out.println("string1 is right");
        String string2 = "1,2,3,4,a";
        //Assert.isIntegerArray(string2,",","含有非数字");
    }

}
