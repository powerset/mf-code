-- 脚本功能: 通用的对人数(非人次)的即时排行榜
-- 2019/8/2 v1  by qc

-- 以商品维度的人气榜为例:

-- 数据结构 -----  key值 ------ value ---- score
--     SET -----  商品ID ----- 用户ID ---- 无
--    ZSET ----- mf:plat ----- 商品ID ---- SET集合的长度

-- 脚本的入参示例:
-- key值:
-- >> 1. set集合的key => statis:rt:goods:set:<goodsId>
-- >> 2. zset的key    => statis:rt:goods:rank:plat:<date>
-- value
-- >> 1. set集合的value => 用户id
-- >> 2. zset的value值  => goodsId , zset value值对应分数为 set集合的长度

-- 入参key值
local setKey = KEYS[1]
local zsetKey = KEYS[2]

-- 入参value值
local setVal = ARGV[1]
local zsetValue = ARGV[2]

-- set集合的长度
local setSize = 0

-- 设置set值
redis.call("sadd",setKey,setVal)

-- 获取此set集合的长度 , 赋值给 setSize
setSize = redis.call("scard",setKey)

-- 设置 zset
redis.call("zadd", zsetKey, setSize, zsetValue)

-- 查zset成员的分数
local zsetMemberScore = redis.call("zscore", zsetKey, zsetValue)

-- lua {} 为数组
return {
        " set集合key=" .. setKey ,
        " set集合长度=" .. setSize ,
        " zset key=" .. zsetKey ,
        " zset成员 value=" .. zsetValue ,
        " zset成员 score=" .. zsetMemberScore
       }

