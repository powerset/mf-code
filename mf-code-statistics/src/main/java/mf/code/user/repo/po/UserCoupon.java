package mf.code.user.repo.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * user_coupon
 * 用户奖品表 用户各种奖品扩充 比如：积分，优惠券，小礼品
 */
public class UserCoupon implements Serializable {
    /**
     * 用户奖品记录表ID主键
     */
    private Long id;

    /**
     * 店铺id
     */
    private Long shopId;

    /**
     * 商户id
     */
    private Long merchantId;

    /**
     * 用户id，user表的主键
     */
    private Long userId;

    /**
     * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6拆红包 26集客优惠券
     */
    private Integer type;

    /**
     * 活动ID, activity表的主键
     */
    private Long activityId;

    /**
     * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     */
    private Long activityDefId;

    /**
     * activity_task表id
     */
    private Long activityTaskId;

    /**
     * 奖金（元）
     */
    private BigDecimal amount;

    /**
     * 活动定义佣金和当时税率，{"commission_def":10, "rate": 10}
     */
    private String commissionDef;

    /**
     * 所得的额外任务佣金
     */
    private BigDecimal commission;

    /**
     * 获奖时间
     */
    private Date awardTime;

    /**
     * 领奖状态(0：未领奖，1：已领奖，2已使用 3已过期)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 优惠券id
     */
    private Long couponId;

    /**
     * user_coupon
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户奖品记录表ID主键
     * @return id 用户奖品记录表ID主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 用户奖品记录表ID主键
     * @param id 用户奖品记录表ID主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 店铺id
     * @return shop_id 店铺id
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * 店铺id
     * @param shopId 店铺id
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * 商户id
     * @return merchant_id 商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * 商户id
     * @param merchantId 商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 用户id，user表的主键
     * @return user_id 用户id，user表的主键
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id，user表的主键
     * @param userId 用户id，user表的主键
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6拆红包
     * @return type 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6拆红包
     */
    public Integer getType() {
        return type;
    }

    /**
     * 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6拆红包
     * @param type 奖品类型：1轮盘抽奖，2订单红包，3优惠券 4好评晒图 5收藏加购 6拆红包
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 活动ID, activity表的主键
     * @return activity_id 活动ID, activity表的主键
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * 活动ID, activity表的主键
     * @param activityId 活动ID, activity表的主键
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     * @return activity_def_id 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     */
    public Long getActivityDefId() {
        return activityDefId;
    }

    /**
     * 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     * @param activityDefId 活动定义ID, activity_def表的主键，用来记录随机红包活动记录
     */
    public void setActivityDefId(Long activityDefId) {
        this.activityDefId = activityDefId;
    }

    /**
     * activity_task表id
     * @return activity_task_id activity_task表id
     */
    public Long getActivityTaskId() {
        return activityTaskId;
    }

    /**
     * activity_task表id
     * @param activityTaskId activity_task表id
     */
    public void setActivityTaskId(Long activityTaskId) {
        this.activityTaskId = activityTaskId;
    }

    /**
     * 奖金（元）
     * @return amount 奖金（元）
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 奖金（元）
     * @param amount 奖金（元）
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 活动定义佣金和当时税率，{"commission_def":10, "rate": 10}
     * @return commission_def 活动定义佣金和当时税率，{"commission_def":10, "rate": 10}
     */
    public String getCommissionDef() {
        return commissionDef;
    }

    /**
     * 活动定义佣金和当时税率，{"commission_def":10, "rate": 10}
     * @param commissionDef 活动定义佣金和当时税率，{"commission_def":10, "rate": 10}
     */
    public void setCommissionDef(String commissionDef) {
        this.commissionDef = commissionDef == null ? null : commissionDef.trim();
    }

    /**
     * 所得的额外任务佣金
     * @return commission 所得的额外任务佣金
     */
    public BigDecimal getCommission() {
        return commission;
    }

    /**
     * 所得的额外任务佣金
     * @param commission 所得的额外任务佣金
     */
    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    /**
     * 获奖时间
     * @return award_time 获奖时间
     */
    public Date getAwardTime() {
        return awardTime;
    }

    /**
     * 获奖时间
     * @param awardTime 获奖时间
     */
    public void setAwardTime(Date awardTime) {
        this.awardTime = awardTime;
    }

    /**
     * 领奖状态(0：未领奖，1：已领奖)
     * @return status 领奖状态(0：未领奖，1：已领奖)
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 领奖状态(0：未领奖，1：已领奖)
     * @param status 领奖状态(0：未领奖，1：已领奖)
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return ctime 创建时间
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 创建时间
     * @param ctime 创建时间
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * 更新时间
     * @return utime 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 更新时间
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }


    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    @Override
    public String toString() {
        return "UserCoupon{" +
                "id=" + id +
                ", shopId=" + shopId +
                ", merchantId=" + merchantId +
                ", userId=" + userId +
                ", type=" + type +
                ", activityId=" + activityId +
                ", activityDefId=" + activityDefId +
                ", activityTaskId=" + activityTaskId +
                ", amount=" + amount +
                ", commissionDef='" + commissionDef + '\'' +
                ", commission=" + commission +
                ", awardTime=" + awardTime +
                ", status=" + status +
                ", ctime=" + ctime +
                ", utime=" + utime +
                ", couponId=" + couponId +
                '}';
    }
}