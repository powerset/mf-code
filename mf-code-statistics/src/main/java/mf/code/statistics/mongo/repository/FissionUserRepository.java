package mf.code.statistics.mongo.repository;

import mf.code.activity.repo.po.FissionUser;
import mf.code.statistics.mongo.repository.operator.FissionUserOperator;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 裂变记录的操作
 * create by qc on 2019/6/26 0026
 */
public interface FissionUserRepository extends MongoRepository<FissionUser, String>, FissionUserOperator {
}
