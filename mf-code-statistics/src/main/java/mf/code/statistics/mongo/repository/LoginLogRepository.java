package mf.code.statistics.mongo.repository;/**
 * create by qc on 2019/7/31 0031
 */

import mf.code.statistics.mongo.domain.LoginLog;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author gbf
 * 2019/7/31 0031、09:38
 */
public interface LoginLogRepository extends MongoRepository<LoginLog,String>{
}
