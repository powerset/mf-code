package mf.code.statistics.mongo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * mf.code.statistics.mongo.domain
 * Description:
 *
 * @author gel
 * @date 2019-03-13 11:37
 */
@Data
@ToString
@NoArgsConstructor
@Document(value = "user_action")
public class UserAction {

    @Id
    @Field("_id")
    private String id;
    /**
     * 用户ID
     */
    @Field(value = "uid")
    private String uid;
    /**
     * 页面路径
     */
    @Field(value = "path")
    private String path;
    /**
     * 页面按钮 -  区分同一个页面上多个按钮
     */
    @Field(value = "btnName")
    private String btnName;
    /**
     * 页面按钮状态  - 区分同一个按钮的多种状态
     */
    @Field(value = "btnStat")
    private String btnStat;
    /**
     * 埋点事件(init初始化页面,share分享,click点击,out离开)
     */
    @Field(value = "event")
    private String event;
    /**
     * 埋点场景(0:通用埋点 1:参与埋点)
     */
    @Field(value = "scene")
    private String scene;
    /**
     * 版本号
     */
    @Field(value = "version")
    private String version;
    /**
     * 时间戳
     */
    @Field(value = "current")
    private String current;
    /**
     * 用户openid
     */
    @Field(value = "openid")
    private String openid;
    /**
     * 用户unionid
     */
    @Field(value = "unionid")
    private String unionid;
    /**
     * 商户id
     */
    @Field(value = "mid")
    private String mid;
    /**
     * 店铺id
     */
    @Field(value = "sid")
    private String sid;
    /**
     * 商品id
     */
    @Field(value = "goodsId")
    private String goodsId;
    /**
     * ip
     */
    @Field(value = "ip")
    private String ip;
    /**
     * user-agent
     */
    @Field(value = "user_agent")
    private String userAgent;
    /**
     * 额外字段
     */
    @Field(value = "extra")
    private String extra;
    /**
     * 创建时间
     */
    @Field(value = "ctime")
    private Date ctime;
    /**
     * 更新时间
     */
    @Field(value = "utime")
    private Date utime;


}
