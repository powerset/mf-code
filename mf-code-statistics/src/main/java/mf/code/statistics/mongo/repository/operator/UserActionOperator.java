package mf.code.statistics.mongo.repository.operator;


import mf.code.statistics.mongo.domain.UserAction;

import java.util.Date;
import java.util.List;

/**
 * mf.code.statistics.mongo.repository.operator
 * Description:
 *
 * @author gel
 * @date 2019-03-13 14:07
 */
public interface UserActionOperator {

    /**
     * 次数  分场景
     *
     * @param sid       店铺id
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @param path      页面路径
     * @param scene     场景
     * @return 次数 Long
     */
    Long countTimeByScene(String sid, Date startDate, Date endDate, String path, String scene);

    /**
     * 人数  分场景
     *
     * @param sid       店铺id
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @param path      页面路径
     * @param scene     场景
     * @return 人数 int
     */
    Integer countPersionByScene(String sid, Date startDate, Date endDate, String path, String scene);


    /**
     * 查询参与的信息
     * @param startDate
     * @param endDate
     * @return
     */
    List<UserAction> findPVListByDate(Date startDate, Date endDate,String shopId);
}
