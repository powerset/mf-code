package mf.code.statistics.mongo.repository.operator;

import mf.code.constent.Event;
import mf.code.constent.PagePath;
import mf.code.constent.PathEnum;
import mf.code.statistics.mongo.domain.UserAction;
import mf.code.user.repo.po.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * mf.code.statistics.mongo.repository.operator
 * Description:
 *
 * @author gel
 * @date 2019-03-13 14:08
 */
public class UserActionOperatorImpl implements UserActionOperator {

    @Autowired
    private MongoTemplate mongoTemplate;

    public static final String collectionName = "user_action";

    /**
     * 次数  分场景
     *
     * @param sid       店铺id
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @param path      页面路径
     * @param scene     场景
     * @return 次数 Long
     */
    @Override
    public Long countTimeByScene(String sid, Date startDate, Date endDate, String path, String scene) {
        Long begin = startDate.getTime();
        Long end = endDate.getTime();
        Criteria criteria = Criteria
                .where("scene").is(scene)
                .and("sid").is(sid)
                .and("current").gte(begin.toString()).lte(end.toString())
                .and("event").is(Event.CLICK);
        // 一个指标对应多个场景path的情况
        criteria = this.calcPathCondition(criteria, path);
        return mongoTemplate.count(new Query(criteria), collectionName);
    }

    /**
     * 处理path的情况
     *
     * @param criteria 查询条件
     * @param path     页面路径值:代表场景值
     * @return criteria 查询条件
     */
    private Criteria calcPathCondition(Criteria criteria, String path) {
        if (path != null) {
            if (PathEnum.FREE_DRAW.getPath().equals(path)) {
                // 抽奖情况
                criteria.and("path").in(PagePath.FREE_DRAW);
            } else if (PathEnum.GIFT.getPath().equals(path)) {
                // 助力情况
                criteria.and("path").in(PagePath.GIFT);
            } else {
                // 其余5种情况
                criteria.and("path").is(path);
            }
        } else {
            //如果path==null,需要全部的场景
            criteria.and("path").in(PagePath.ALL);
        }
        return criteria;
    }

    /**
     * 人数  分场景
     *
     * @param sid       店铺id
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @param path      页面路径
     * @param scene     场景
     * @return 人数 int
     */
    @Override
    public Integer countPersionByScene(String sid, Date startDate, Date endDate, String path, String scene) {
        Long begin = startDate.getTime();
        Long end = endDate.getTime();
        Criteria criteria = Criteria
                .where("scene").is(scene)
                .and("sid").is(sid)
                .and("current").gte(begin.toString()).lte(end.toString())
                .and("event").is(Event.CLICK);
        // 一个指标对应多个场景path的情况
        criteria = this.calcPathCondition(criteria, path);

        MatchOperation matchOperation = Aggregation.match(criteria);
        GroupOperation group = Aggregation.group("uid");
        Aggregation aggregation = Aggregation.newAggregation(matchOperation, group);
        AggregationResults<Map> rs = mongoTemplate.aggregate(aggregation, collectionName, Map.class);
        List<Map> mappedResults = rs.getMappedResults();
        return mappedResults.size();
    }

    /**
     * 查询全部参与的信息
     * @param startDate
     * @param endDate
     * @return
     */
    @Override
    public List<UserAction> findPVListByDate(Date startDate, Date endDate,String shopId) {
        Criteria criteria=Criteria.where
                ("ctime").gte(startDate).lte(endDate)
                .and("scene").is("1")
                ;
        if(StringUtils.isNotBlank(shopId)){
            criteria.and("shopId").is(shopId);
        }
        return mongoTemplate.find(new Query(criteria), UserAction.class);
    }
}
