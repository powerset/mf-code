package mf.code.statistics.mongo.domain;/**
 * create by qc on 2019/7/30 0030
 */

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * 登陆统计
 *
 * @author gbf
 * 2019/7/30 0030、18:00
 */
@Data
@ToString
@NoArgsConstructor
@Document(value = "login_log")
public class LoginLog {

    /**
     * 记录主键
     */
    @Id
    @Field("_id")
    private String id;

    /**
     * 类型
     */
    @Field(value = "type")
    private String type;

    /**
     * 用户id
     */
    @Field(value = "uid")
    private String uid;

    /**
     * 时间戳
     */
    @Field(value = "current")
    private Long current;

    /**
     * 消息插入时间
     */
    @Field(value = "ctime")
    private Date ctime;

    /**
     * 版本
     */
    @Field(value = "version")
    private String version;

}
