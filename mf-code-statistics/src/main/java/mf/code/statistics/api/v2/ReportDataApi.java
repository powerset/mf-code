package mf.code.statistics.api.v2;/**
 * create by qc on 2019/6/25 0025
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.statistics.api.dto.ReportDTOV2;
import mf.code.common.RocketMqTopicTagEnum;
import mf.code.common.utils.IpUtil;
import mf.code.statistics.mongo.domain.UserAction;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author gbf
 * 2019/6/25 0025、15:13
 */
@RestController
@RequestMapping(value = "/api/statistics/v2")
@Slf4j
public class ReportDataApi {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * @param request 请求实体
     * @param dto     参数实体
     * @return 返回
     */
    @PostMapping(value = "report")
    public String receiveMonitorV2(HttpServletRequest request, @RequestBody ReportDTOV2 dto) {
        UserAction userAction = new UserAction();
        userAction.setUid(dto.getUid());
        userAction.setOpenid(dto.getOpenid());
        userAction.setMid(dto.getMid());
        userAction.setSid(dto.getSid());
        userAction.setGoodsId(dto.getGoodsId());
        userAction.setEvent(dto.getEvent());
        userAction.setPath(dto.getPath());
        userAction.setBtnName(dto.getBtnName());
        userAction.setBtnStat(dto.getBtnStat());
        userAction.setScene(dto.getScene());
        userAction.setVersion(dto.getVersion());
        userAction.setCurrent(dto.getCurrent());
        userAction.setIp(IpUtil.getIpAddress(request));
        userAction.setUserAgent(request.getHeader("User-Agent"));
        //monitorDataService.insertMonitorData(monitorData);

        rocketMQTemplate.syncSend(RocketMqTopicTagEnum.findTopicAndTagsByEnum(RocketMqTopicTagEnum.STATISTICS_REPORT), JSON.toJSONString(userAction));
        log.info("统计服务-埋点上报-生产消息：消息体={}", JSON.toJSONString(userAction));
        return "Reported!";
    }
}
