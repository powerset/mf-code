package mf.code.statistics.api.common.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户邀请记录表  用户记录用户邀请好友参与的情况(user_pub_join)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum UserPubJoinTypeMapping {


    LUCK_WHEEL(1, "大转盘", 4),
    LUCK_DRAW(4, "抽奖", 5),
    ASSIST(6, "助力赠品", 6),
    OPEN_RED_PACKET_1(12, "拆红包", 7),
    OPEN_RED_PACKET_2(13, "拆红包", 7),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (UserPubJoinTypeMapping e : UserPubJoinTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }


    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    UserPubJoinTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
