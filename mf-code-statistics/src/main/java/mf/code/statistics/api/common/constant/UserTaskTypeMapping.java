package mf.code.statistics.api.common.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户中奖任务记录表  用于记录用户完成中奖任务的情况(user_task)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum UserTaskTypeMapping {

    FILL_ORDER_1(6, "回填订单", 1),
    FILL_ORDER_2(17, "回填订单", 1),

    GOOD_COMMENT_1(9, "好评晒图", 2),
    GOOD_COMMENT_2(19, "好评晒图", 2),

    CART_1(7, "收藏加购", 3),
    CART_2(18, "收藏加购", 3),

    LUCK_WHEEL(0, "大转盘", 4),

    LUCK_DRAW_1(2, "抽奖", 5),
    LUCK_DRAW_2(3, "抽奖", 5),
    LUCK_DRAW_3(14, "抽奖", 5),
    LUCK_DRAW_4(15, "抽奖", 5),

    ASSIST_1(8, "助力赠品", 6),
    ASSIST_2(16, "助力赠品", 6),

    OPEN_RED_PACKET_1(12, "拆红包", 7),
    OPEN_RED_PACKET_2(13, "拆红包", 7),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (UserTaskTypeMapping e : UserTaskTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }


    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    UserTaskTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
