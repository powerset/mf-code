package mf.code.statistics.api.v1;

import mf.code.statistics.api.common.constant.BizTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.tool.DateTool;
import mf.code.common.utils.DateUtil;
import mf.code.constent.DayTypeValue;
import mf.code.statistics.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 活动统计
 */
@RestController
@RequestMapping(value = "/api/seller/report")
public class ActivityStatisticsV1Api {

    @Autowired
    private StatisticsService statisticsV1Service;

    /**
     * 页面头部 统计数据
     *
     * @param shopId 店铺
     * @return SimpleResponse
     */
    @GetMapping(value = "tatalData")
    public SimpleResponse totalData(@RequestParam(value = "shopId") String shopId) {
        return statisticsV1Service.totalData(shopId);
    }

    /**
     * 活动 近期数据
     *
     * @param shopId     店铺
     * @param activeType 活动类型- * 0  '全部' (默认)- * 1 '回填订单'- * 2  '好评晒图'- * 3: '拆红包'- * 4, 助力赠品'- * 5  '新手轮盘'- * 6 '收藏加购'- * 7 '免单抽奖'
     * @param dayTime    指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @param startDate  指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate    指标时间为 3 自定义 的情况的 结束时间   格式2019-03-05
     * @param indexArray 指标选项，可以多选，例如 [ "2", "3", "4", "6" ]，；默认[参与人数, 完成人数]
     *                   1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @return SimpleResponse
     *
     * <pre>
     *  1:开始结束时间为空
     *  2:开始时间在结束时间之后
     *  3:指标时间类型不合法
     *  4:活动类型不合法
     *  5:指标选项错误
     * </pre>
     */
    @GetMapping(value = "recentActivityData")
    public SimpleResponse recentActivityData(@RequestParam(value = "shopId") String shopId,
                                             @RequestParam(value = "activeType") Integer activeType,
                                             @RequestParam(value = "dayTime") Integer dayTime,
                                             @RequestParam(value = "startDate", required = false) String startDate,
                                             @RequestParam(value = "endDate", required = false) String endDate,
                                             @RequestParam(value = "indexArray") String indexArray) {
        // 计算开始结束时间
        Date begin;
        Date end;

        if (DayTypeValue.CUSTOM.equals(dayTime)) {
            SimpleResponse simpleResponse = this.verifyBeginAndEndDateError(startDate, endDate);
            if (simpleResponse != null) {
                return simpleResponse;
            }
            Date beginTempDate = DateUtil.stringtoDate(startDate, DateUtil.LONG_DATE_FORMAT);
            begin = DateTool.begin(beginTempDate);
            Date endTempDate = DateUtil.stringtoDate(endDate, DateUtil.LONG_DATE_FORMAT);
            end = DateTool.end(endTempDate);
        } else if (DayTypeValue.LAST_7_DAYS.equals(dayTime)) {
            // 7天前 0:0:0 开始
            begin = DateTool.offsetBegin(-7);
            end = DateTool.offsetEnd(-1);
        } else if (DayTypeValue.LAST_30_DAYS.equals(dayTime)) {
            // 30 天前 0:0:0 开始
            begin = DateTool.offsetBegin(-30);
            end = DateTool.offsetEnd(-1);
        } else {
            return new SimpleResponse(3, "指标时间类型不合法");
        }
        if (activeType < 0 || activeType > 7) {
            return new SimpleResponse(4, "活动类型不合法");
        }

        List<Integer> indexList = new ArrayList<>();
        try {
            List<String> list = Arrays.asList(indexArray.split(","));
            for (String str : list) {
                indexList.add(Integer.parseInt(str));
            }
        } catch (Exception e) {
            return new SimpleResponse(5, "指标选项错误");
        }
        BizTypeEnum bizTypeEnum = this.getBizTypeEnum(activeType);
        return statisticsV1Service.recentActivityData(shopId, bizTypeEnum, dayTime, begin, end, indexList);
    }

    /**
     * 活动 参与情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     * <pre>
     *  1:开始结束时间为空
     *  2:开始时间在结束时间之后
     *  3:指标时间类型不合法
     * </pre>
     */
    @GetMapping(value = "participate")
    public SimpleResponse participate(@RequestParam(value = "shopId") String shopId,
                                      @RequestParam(value = "dayTime") Integer dayTime,
                                      @RequestParam(value = "startDate", required = false) String startDate,
                                      @RequestParam(value = "endDate", required = false) String endDate) {
        // 计算开始结束时间
        Date begin;
        Date end;

        if (DayTypeValue.CUSTOM.equals(dayTime)) {
            SimpleResponse simpleResponse = this.verifyBeginAndEndDateError(startDate, endDate);
            if (simpleResponse != null) {
                return simpleResponse;
            }
            Date beginTempDate = DateUtil.stringtoDate(startDate, DateUtil.LONG_DATE_FORMAT);
            begin = DateTool.begin(beginTempDate);
            Date endTempDate = DateUtil.stringtoDate(endDate, DateUtil.LONG_DATE_FORMAT);
            end = DateTool.end(endTempDate);
        } else if (DayTypeValue.LAST_7_DAYS.equals(dayTime)) {
            // 7天前 0:0:0 开始
            begin = DateTool.offsetBegin(-7);
            end = DateTool.offsetEnd(-1);
        } else if (DayTypeValue.LAST_30_DAYS.equals(dayTime)) {
            // 30 天前 0:0:0 开始
            begin = DateTool.offsetBegin(-30);
            end = DateTool.offsetEnd(-1);
        } else {
            return new SimpleResponse(3, "指标时间类型不合法");
        }
        return statisticsV1Service.participate(shopId, begin, end, dayTime);
    }


    /**
     * 活动 完成情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     * <pre>
     *     1:开始结束时间为空
     *     2:开始时间在结束时间之后
     *     3:指标时间类型不合法
     * </pre>
     */
    @GetMapping(value = "finish")
    public SimpleResponse finish(@RequestParam(value = "shopId") String shopId,
                                 @RequestParam(value = "dayTime") Integer dayTime,
                                 @RequestParam(value = "startDate", required = false) String startDate,
                                 @RequestParam(value = "endDate", required = false) String endDate) {
        // 计算开始结束时间
        Date begin;
        Date end;

        if (DayTypeValue.CUSTOM.equals(dayTime)) {
            SimpleResponse simpleResponse = this.verifyBeginAndEndDateError(startDate, endDate);
            if (simpleResponse != null) {
                return simpleResponse;
            }
            Date beginTempDate = DateUtil.stringtoDate(startDate, DateUtil.LONG_DATE_FORMAT);
            begin = DateTool.begin(beginTempDate);
            Date endTempDate = DateUtil.stringtoDate(endDate, DateUtil.LONG_DATE_FORMAT);
            end = DateTool.end(endTempDate);
        } else if (DayTypeValue.LAST_7_DAYS.equals(dayTime)) {
            // 7天前 0:0:0 开始
            begin = DateTool.offsetBegin(-7);
            end = DateTool.offsetEnd(-1);
        } else if (DayTypeValue.LAST_30_DAYS.equals(dayTime)) {
            // 30 天前 0:0:0 开始
            begin = DateTool.offsetBegin(-30);
            end = DateTool.offsetEnd(-1);
        } else {
            return new SimpleResponse(3, "指标时间类型不合法");
        }
        return statisticsV1Service.finish(shopId, begin, end, dayTime);
    }

    /**
     * 活动 分享情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    @GetMapping(value = "share")
    public SimpleResponse share(@RequestParam(value = "shopId") String shopId,
                                @RequestParam(value = "dayTime") Integer dayTime,
                                @RequestParam(value = "startDate", required = false) String startDate,
                                @RequestParam(value = "endDate", required = false) String endDate) {
        // 计算开始结束时间
        Date begin;
        Date end;

        if (DayTypeValue.CUSTOM.equals(dayTime)) {
            SimpleResponse simpleResponse = this.verifyBeginAndEndDateError(startDate, endDate);
            if (simpleResponse != null) {
                return simpleResponse;
            }
            Date beginTempDate = DateUtil.stringtoDate(startDate, DateUtil.LONG_DATE_FORMAT);
            begin = DateTool.begin(beginTempDate);
            Date endTempDate = DateUtil.stringtoDate(endDate, DateUtil.LONG_DATE_FORMAT);
            end = DateTool.end(endTempDate);
        } else if (DayTypeValue.LAST_7_DAYS.equals(dayTime)) {
            // 7天前 0:0:0 开始
            begin = DateTool.offsetBegin(-7);
            end = DateTool.offsetEnd(-1);
        } else if (DayTypeValue.LAST_30_DAYS.equals(dayTime)) {
            // 30 天前 0:0:0 开始
            begin = DateTool.offsetBegin(-30);
            end = DateTool.offsetEnd(-1);
        } else {
            return new SimpleResponse(3, "指标时间类型不合法");
        }
        return statisticsV1Service.share(shopId, begin, end, dayTime);
    }

    /**
     * 校验日期
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private SimpleResponse verifyBeginAndEndDateError(String startDate, String endDate) {
        Date begin;
        Date end;
        if (startDate == null || endDate == null || "".equals(startDate) || "".equals(endDate)) {
            return new SimpleResponse(1, "开始结束时间为空");
        }
        begin = DateUtil.stringtoDate(startDate, DateUtil.LONG_DATE_FORMAT);
        end = DateUtil.stringtoDate(endDate, DateUtil.LONG_DATE_FORMAT);
        if (begin.after(end)) {
            return new SimpleResponse(2, "开始时间在结束时间之后");
        }
        return null;
    }

    private BizTypeEnum getBizTypeEnum(int type) {
        switch (type) {
            case 0:
                return BizTypeEnum.ALL;
            case 1:
                return BizTypeEnum.FILL_ORDER;
            case 2:
                return BizTypeEnum.GOOD_COMMENT;
            case 3:
                return BizTypeEnum.OPEN_RED_PACKET;
            case 4:
                return BizTypeEnum.ASSIST;
            case 5:
                return BizTypeEnum.LUCK_WHEEL;
            case 6:
                return BizTypeEnum.CART;
            case 7:
                return BizTypeEnum.LUCK_DRAW;
        }
        return null;
    }
}
