package mf.code.statistics.api.common.constant;

/**
 * 活动定义表(activity_def)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum BizTypeEnum {

    FILL_ORDER(1, "回填订单", "/page/addOrder"),
    GOOD_COMMENT(2, "好评晒图", "/page/slideShow"),
    CART(3, "收藏加购", "/page/redpacket"),
    LUCK_WHEEL(4, "大转盘", "/page/wheel"),
    LUCK_DRAW(5, "抽奖", "/page/taskDetail"),
    ASSIST(6, "助力赠品", "/page/gift"),
    OPEN_RED_PACKET(7, "拆红包", "/page/envelope"),
    ALL(0, "全部", "ALL"),
    ;

    /**
     * code
     */
    private int code;
    /**
     * 描述
     */
    private String desc;
    /**
     * 埋点的path值
     */
    private String path;

    /**
     */
    BizTypeEnum(int code, String desc, String path) {
        this.code = code;
        this.desc = desc;
        this.path = path;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getPath() {
        return path;
    }
}