package mf.code.statistics.api.feignclient;

import mf.code.common.config.feign.FeignLogConfiguration;
import mf.code.statistics.api.feignclient.fallback.UnionMarketingApolloDataServiceFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 联合营销活动的apollo配置获取
 * create by qc on 2019/7/18 0018
 */
@FeignClient(name = "mf-code-one", fallbackFactory = UnionMarketingApolloDataServiceFactory.class, configuration = FeignLogConfiguration.class)
public interface UnionMarketingApolloDataService {
    /**
     * 获取apollo配置skuid
     *
     * @return
     */
    @GetMapping("/feignapi/one/unionmarketing/apollodata/skuids")
    String getSkuIds();
}
