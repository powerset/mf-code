package mf.code.statistics.api.v1;

import mf.code.common.utils.IpUtil;
import mf.code.statistics.mongo.domain.UserAction;
import mf.code.statistics.service.MonitorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * mf.code.api.statistics
 * Description:
 *
 * @author gel
 * @date 2019-03-13 11:35
 */
@RestController
@RequestMapping(value = "/api/statistics")
public class MonitorDataApi {

    @Autowired
    private MonitorDataService monitorDataService;

    /**
     * 通用埋点接口
     *
     * @param request    request
     * @param openid     openid
     * @param uid        用户id
     * @param path       页面路径
     * @param event      event-埋点事件(init：进入页面；click：点击事件；out：跳出页面)
     * @param scene      scene-场景值，用于相同事件下的不同触发点
     * @param version    version-版本号
     * @param current    current-时间戳
     * @param merchantId merchantId-商户id
     * @param shopId     shopId-店铺id
     * @return 空字符串
     */
    @GetMapping(value = "")
    public String receiveMonitor(HttpServletRequest request,
                                 @RequestParam(value = "openid") String openid,
                                 @RequestParam(value = "uid") String uid,
                                 @RequestParam(value = "ph") String path,
                                 @RequestParam(value = "et") String event,
                                 @RequestParam(value = "se", required = false) String scene,
                                 @RequestParam(value = "v") String version,
                                 @RequestParam(value = "ct") String current,
                                 @RequestParam(value = "md") String merchantId,
                                 @RequestParam(value = "sd") String shopId) {
        UserAction monitorData = new UserAction();
        monitorData.setUid(uid);
        monitorData.setOpenid(openid);
        monitorData.setMid(merchantId);
        monitorData.setSid(shopId);
        monitorData.setEvent(event);
        monitorData.setPath(path);
        monitorData.setScene(scene);
        monitorData.setVersion(version);
        monitorData.setCurrent(current);
        monitorData.setIp(IpUtil.getIpAddress(request));
        monitorData.setUserAgent(request.getHeader("User-Agent"));
        monitorDataService.insertMonitorData(monitorData);
        return "~(`-`)~";
    }
}
