package mf.code.statistics.api.v3;/**
 * create by qc on 2019/7/17 0017
 */

import com.csvreader.CsvWriter;
import mf.code.activity.model.MsActivityExcelPropertyIndexModel;
import mf.code.common.utils.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author gbf
 * 2019/7/17 0017、14:45
 */
@RestController
@RequestMapping("/api/statistics/union/csv/v1")
public class UnionMarketingStatisticsApi {

    /**
     * 字符集
     */
    private final String charset = "GBK";
    /**
     * 表头
     */
    private String[] tableHeaderArr = new String[]{"订单号", "下单时间", "店铺名称", "商品ID", "店铺名称", "下单商品数", "下单金额", "支付商品数", "支付金额"};

    /**
     * 导出csv
     *
     * @param request  请求
     * @param response 响应
     * @throws Exception
     */
    @GetMapping("/exportCsv")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String dateStr = DateUtil.dateToString(new Date(), DateUtil.FORMAT_THREE);
        String fileName = new String("秒杀活动统计" + dateStr);
        String userAgent = request.getHeader("USER-AGENT");
        try {
            if (StringUtils.contains(userAgent, "MSIE")) {//IE浏览器
                fileName = URLEncoder.encode(fileName, charset);
            } else if (StringUtils.contains(userAgent, "Mozilla")) {//google,火狐浏览器
                fileName = new String(fileName.getBytes(), "ISO8859-1");
            }
            response.setContentType("application/csv; charset=" + charset);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        List<MsActivityExcelPropertyIndexModel> resultList = new ArrayList<>();

        File file = File.createTempFile(fileName, ".csv");
        CsvWriter csvWriter = new CsvWriter(file.getCanonicalPath(), ',', Charset.forName(charset));
        csvWriter.writeRecord(tableHeaderArr);
        for (MsActivityExcelPropertyIndexModel model : resultList) {
            Date paymentTime = model.getPaymentTime();
            csvWriter.writeRecord(new String[]{model.getOrderNo(),
                    model.getCtime(),
                    model.getShopName(),
                    model.getGoodsId().toString(),
                    model.getGoodsName(),
                    model.getCreateFee().toString(),
                    model.getCreateFee().toString(),
                    paymentTime == null ? "0" : model.getPayNumber().toString(),
                    paymentTime == null ? "0" : model.getPayFee().toString()});
        }
        csvWriter.close();
        InputStream in = new FileInputStream(file);
        OutputStream out = null;
        try {
            out = response.getOutputStream();
            int len;
            byte[] buffer = new byte[1024];
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
        }
    }
}
