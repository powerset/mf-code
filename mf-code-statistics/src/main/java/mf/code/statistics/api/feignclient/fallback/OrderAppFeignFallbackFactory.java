package mf.code.statistics.api.feignclient.fallback;/**
 * create by qc on 2019/7/18 0018
 */

import feign.hystrix.FallbackFactory;
import mf.code.statistics.api.feignclient.OrderAppService;

/**
 * @author gbf
 * 2019/7/18 0018、09:11
 */
public class OrderAppFeignFallbackFactory implements FallbackFactory<OrderAppService> {

    @Override
    public OrderAppService create(Throwable cause) {
        return new OrderAppService() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };
    }
}
