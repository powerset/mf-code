package mf.code.statistics.api.common.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户活动记录表  用于用户领取活动,作为用户自己查看提交的活动记录,并记录奖励(user_activity)
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum UserActivityTypeMapping {

    LUCK_WHEEL(0, "大转盘", 4),
    LUCK_DRAW_1(2, "抽奖", 5),
    LUCK_DRAW_2(3, "抽奖", 5),
    ASSIST_1(6, "助力赠品", 6),
    ASSIST_2(7, "助力赠品", 6),
    FILL_ORDER_1(8, "回填订单", 1),
    GOOD_COMMENT_1(9, "好评晒图", 2),
    CART_1(10, "收藏加购", 3),
    OPEN_RED_PACKET_1(11, "拆红包", 7),
    OPEN_RED_PACKET_2(12, "拆红包", 7),
    OPEN_RED_PACKET_3(13, "拆红包", 7),
    LUCK_DRAW_3(14, "抽奖", 5),
    LUCK_DRAW_4(15, "抽奖", 5),
    ASSIST_3(16, "助力赠品", 6),
    ASSIST_4(17, "助力赠品", 6),
    FILL_ORDER_2(18, "回填订单", 1),
    GOOD_COMMENT_2(19, "好评晒图", 2),
    CART_2(20, "收藏加购", 3),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (UserActivityTypeMapping e : UserActivityTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }


    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    UserActivityTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
