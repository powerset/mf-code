package mf.code.statistics.api.common.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动任务表(activity_task) .------ 尽量避免使用
 * 注意:当前在业务中只用了收藏加购.虽然当前逻辑中还依赖此表,未来一定会废弃,用活动定义表替代.
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum ActivityTaskTypeMapping {


    CART(3, "收藏加购", 3),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (ActivityTaskTypeMapping e : ActivityTaskTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }

    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    ActivityTaskTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }

}
