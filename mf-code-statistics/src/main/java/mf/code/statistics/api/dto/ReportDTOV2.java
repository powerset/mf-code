package mf.code.statistics.api.dto;/**
 * create by qc on 2019/6/25 0025
 */

import lombok.Data;

/**
 * @author gbf
 * 2019/6/25 0025、15:20
 */
@Data
public class ReportDTOV2 {

    /**
     * 小程序openid
     */
    private String openid;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 前端-页面路径
     */
    private String path;
    /**
     * 时间
     * 1.click点击
     * 2.share分享
     * 3.init页面初始化
     */
    private String event;
    /**
     * 按钮名称 - 区分同一个页面多个按钮
     */
    private String btnName;
    /**
     * 按钮状态 - 区分同一个按钮多种状态
     */
    private String btnStat;
    /**
     * 场景
     * 0:通用埋点
     * 1:新埋点
     */
    private String scene;
    /**
     * 版本
     */
    private String version;
    /**
     * 前端报送时间
     */
    private String current;
    /**
     * 商户id
     */
    private String mid;
    /**
     * 店铺id
     */
    private String sid;
    /**
     * 商品id
     */
    private String goodsId;
}
