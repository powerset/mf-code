package mf.code.statistics.api.expose;/**
 * create by qc on 2019/7/18 0018
 */

import com.alibaba.fastjson.JSON;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.statistics.api.feignclient.UnionMarketingApolloDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.apache.poi.poifs.crypt.EncryptionMode.xor;

/**
 * @author gbf
 * 2019/7/18 0018、09:36
 */
@RestController
@RequestMapping(value = "/api/statistics/expose/v1")
public class UnionMarketingApolloDataApi {

    @Autowired
    private UnionMarketingApolloDataService unionMarketingApolloDataService;

    /**
     * 为python提供apollo数据
     */
    @GetMapping("apollo/data")
    public SimpleResponse apollodata() {
        String skuIds = unionMarketingApolloDataService.getSkuIds();
        if (skuIds != null) {
            return new SimpleResponse(ApiStatusEnum.SUCCESS, JSON.parseObject(skuIds));
        }
        return new SimpleResponse(ApiStatusEnum.ERROR);
    }
}
