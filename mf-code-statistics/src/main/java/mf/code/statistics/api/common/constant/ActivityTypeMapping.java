package mf.code.statistics.api.common.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动表(activity)
 * 注意:当前activity表type只有3种活动+1中红包任务 . 红包任务未来将全部移出.
 * <p>
 * create by qc on 2019/6/3 0003
 */
public enum ActivityTypeMapping {

    LUCK_WHEEL(0, "大转盘", 4),
    LUCK_DRAW_1(2, "抽奖", 5),
    LUCK_DRAW_2(3, "抽奖", 5),
    ASSIST_1(8, "助力赠品", 6),
    OPEN_RED_PACKET_1(12, "拆红包", 7),
    OPEN_RED_PACKET_2(13, "拆红包", 7),
    LUCK_DRAW_3(16, "抽奖", 5),
    LUCK_DRAW_4(17, "抽奖", 5),
    ASSIST_2(18, "助力赠品", 6),
    ;

    /**
     * 获取该业务对应的type类型List
     */
    public static List<Integer> getTypeList(Integer bizCode) {
        List<Integer> typeList = new ArrayList<>();
        for (ActivityTypeMapping e : ActivityTypeMapping.values()) {
            if (e.bizCode.equals(bizCode)) {
                typeList.add(e.typeCode);
            }
        }
        return typeList;
    }


    /**
     * 数据库中的type类型
     */
    private Integer typeCode;
    /**
     * 描述
     */
    private String desc;
    /**
     * 业务对应值
     */
    private Integer bizCode;

    /**
     */
    ActivityTypeMapping(Integer typeCode, String desc, Integer bizCode) {
        this.typeCode = typeCode;
        this.desc = desc;
        this.bizCode = bizCode;
    }

    public int getCode() {
        return typeCode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getBizCode() {
        return bizCode;
    }
}
