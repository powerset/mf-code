package mf.code.statistics.api.feignclient.fallback;/**
 * create by qc on 2019/7/18 0018
 */


import feign.hystrix.FallbackFactory;
import mf.code.statistics.api.feignclient.UnionMarketingApolloDataService;
import org.springframework.stereotype.Component;

/**
 * @author gbf
 * 2019/7/18 0018、10:00
 */
@Component
public class UnionMarketingApolloDataServiceFactory implements FallbackFactory<UnionMarketingApolloDataService> {
    @Override
    public UnionMarketingApolloDataService create(Throwable cause) {
        return new UnionMarketingApolloDataService() {
            @Override
            public String getSkuIds() {
                return null;
            }
        };
    }
}
