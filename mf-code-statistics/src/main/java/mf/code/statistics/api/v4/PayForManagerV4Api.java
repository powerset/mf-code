package mf.code.statistics.api.v4;/**
 * create by qc on 2019/7/29 0029
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.one.dto.UserOrderReqDTO;
import org.springframework.web.bind.annotation.*;

/**
 * @author gbf
 * 2019/7/29 0029、17:21
 */
@RestController
@RequestMapping("/api/statistics/plat/shopManager/v4")
@Slf4j
public class PayForManagerV4Api {

    /**
     * 店长缴纳金额统计数据
     * @return
     */
    @GetMapping(path = "/money")
    public SimpleResponse createOrder() {
        return null;
    }
}
