package mf.code.statistics.service;/**
 * create by qc on 2019/3/14 0014
 */

import mf.code.statistics.api.common.constant.BizTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;

import java.util.Date;
import java.util.List;

/**
 * @author gbf
 * 2019/3/14 0014、10:02
 */

public interface StatisticsService {
    /**
     * 页面头部统计数据
     *
     * @param shopId 店铺
     * @return SimpleResponse
     */
    SimpleResponse totalData(String shopId);

    /**
     * 活动近期数据
     *
     * @param shopId     店铺
     * @param bizTypeEnum 活动类型- * 0  '全部' (默认)- * 1 '回填订单'- * 2  '好评晒图'- * 3: '拆红包'- * 4, 助力赠品'- * 5  '新手轮盘'- * 6 '收藏加购'- * 7 '免单抽奖'
     * @param dayTime    指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @param startDate  指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate    指标时间为 3 自定义 的情况的 结束时间   格式2019-03-05
     * @param indexArr 指标选项，可以多选，例如 [ "2", "3", "4", "6" ]，；默认[参与人数, 完成人数]
     *                   1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @return SimpleResponse
     */
    SimpleResponse recentActivityData(String shopId,
                                      BizTypeEnum bizTypeEnum,
                                      Integer dayTime,
                                      Date startDate, Date endDate,
                                      List<Integer> indexArr);

    /**
     * 活动 参与情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    SimpleResponse participate(String shopId,
                               Date startDate, Date endDate,
                               Integer dayTime);

    /**
     * 活动 完成情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    SimpleResponse finish(String shopId,
                          Date startDate, Date endDate,
                          Integer dayTime);

    /**
     * 活动 分享情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    SimpleResponse share(String shopId,
                         Date startDate, Date endDate,
                         Integer dayTime);

}
