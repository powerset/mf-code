package mf.code.statistics.service.impl;/**
 * create by qc on 2019/7/30 0030
 */


import lombok.extern.slf4j.Slf4j;
import mf.code.statistics.mongo.domain.LoginLog;
import mf.code.statistics.mongo.repository.LoginLogRepository;
import mf.code.statistics.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 写登陆日志
 *
 * @author gbf
 * 2019/7/30 0030、17:58
 */
@Service
@Slf4j
public class LoginLogServiceImpl implements LoginLogService {

    @Autowired
    private LoginLogRepository loginLogRepository;

    @Override
    public LoginLog insertLoginLog(LoginLog loginLog) {
        loginLog.setCurrent(new Date().getTime());
        loginLog.setCtime(new Date());
        LoginLog insertResultObj = loginLogRepository.insert(loginLog);
        if (insertResultObj != null) {
            return insertResultObj;
        } else {
            log.error("^^^^^^^^^^^^^^^^^^^ 写登陆日志错误,实体对象={}", loginLog);
            return null;
        }
    }
}
