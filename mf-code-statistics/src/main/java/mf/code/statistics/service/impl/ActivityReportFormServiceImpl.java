package mf.code.statistics.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import mf.code.activity.repo.dao.ActivityMapper;
import mf.code.statistics.api.common.constant.BizTypeEnum;
import mf.code.statistics.api.common.constant.UserCouponTypeMapping;
import mf.code.statistics.api.common.constant.UserPubJoinTypeMapping;
import mf.code.common.utils.DateUtil;
import mf.code.statistics.dto.ReportFormDTO;
import mf.code.statistics.service.ActivityReportFormService;
import mf.code.user.repo.dao.UserCouponMapper;
import mf.code.user.repo.dao.UserMapper;
import mf.code.user.repo.dao.UserPubJoinMapper;
import mf.code.user.repo.dao.UserTaskMapper;
import mf.code.user.repo.po.UserCoupon;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mf.code.statistics.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-03-14 14:00
 */
@Service
public class ActivityReportFormServiceImpl implements ActivityReportFormService {

    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserTaskMapper userTaskMapper;
    @Autowired
    private UserPubJoinMapper userPubJoinMapper;
    @Autowired
    private UserCouponMapper userCouponMapper;

    /**
     * 查询完成人数
     *
     * @param reportFormDTO 请求参数
     *                      shopId     店铺
     *                      activeType 活动类型- * 0  '全部' (默认)- * 1 '回填订单'- * 2  '好评晒图'- * 3: '拆红包'- * 4, 助力赠品'- * 5  '新手轮盘'- * 6 '收藏加购'- * 7 '免单抽奖'
     *                      dayTime    指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     *                      startDate  指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     *                      endDate    指标时间为 3 自定义 的情况的 结束时间   格式2019-03-05
     *                      indexArray 指标选项，可以多选，例如 [ "2", "3", "4", "6" ]，；默认[参与人数, 完成人数]
     *                      1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @return 返回完成人数
     */
    @Override
    public Integer countPersonNumByActivityFinish(ReportFormDTO reportFormDTO) {

        Map<String, Object> paramsMap = new HashMap<>(7);
        paramsMap.put("merchantId", reportFormDTO.getMerchantId());
        paramsMap.put("shopId", reportFormDTO.getShopId());
        paramsMap.put("userCouponTypeList", UserCouponTypeMapping.getTypeList(reportFormDTO.getActivityType()));
        paramsMap.put("status", 1);
        paramsMap.put("startTime", DateUtil.dateToString(reportFormDTO.getStartTime(), DateUtil.FORMAT_ONE));
        paramsMap.put("endTime", DateUtil.dateToString(reportFormDTO.getEndTime(), DateUtil.FORMAT_ONE));
        return userCouponMapper.countPersonNumByActivityFinish(paramsMap);
    }

    /**
     * 查询裂变粉丝数
     *
     * @param reportFormDTO 请求参数
     *                      shopId     店铺
     *                      activeType 活动类型- * 0  '全部' (默认)- * 1 '回填订单'- * 2  '好评晒图'- * 3: '拆红包'- * 4, 助力赠品'- * 5  '新手轮盘'- * 6 '收藏加购'- * 7 '免单抽奖'
     *                      dayTime    指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     *                      startDate  指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     *                      endDate    指标时间为 3 自定义 的情况的 结束时间   格式2019-03-05
     *                      indexArray 指标选项，可以多选，例如 [ "2", "3", "4", "6" ]，；默认[参与人数, 完成人数]
     *                      1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @return 返回新粉丝人数
     */
    @Override
    public Integer countFansByShare(ReportFormDTO reportFormDTO) {
        /*
         * 先查出时间范围内创建的用户user数据中，id的最大值最小值
         * 然后根据id边界来查询分享进入的新粉丝人数
         */
        String startTime = DateUtil.dateToString(reportFormDTO.getStartTime(), DateUtil.FORMAT_ONE);
        String endTime = DateUtil.dateToString(reportFormDTO.getEndTime(), DateUtil.FORMAT_ONE);
        Long minUserId = userMapper.findMinIdBetweenTime(reportFormDTO.getShopId(), startTime, endTime);
        // 如果查询不到时间段内最小的userId直接返回0，也不用执行下面无用的操作。
        if (minUserId == null) {
            return 0;
        }
        Long maxUserId = userMapper.findMaxIdBetweenTime(reportFormDTO.getShopId(), startTime, endTime);
        Map<String, Object> paramsMap = new HashMap<>(9);
        paramsMap.put("shopId", reportFormDTO.getShopId());
        paramsMap.put("userPubJoinTypeList", UserPubJoinTypeMapping.getTypeList(reportFormDTO.getActivityType()));
        paramsMap.put("minUserId", minUserId);
        paramsMap.put("maxUserId", maxUserId);
        paramsMap.put("startTime", startTime);
        paramsMap.put("endTime", endTime);
        return userPubJoinMapper.countFansByShare(paramsMap);
    }

    /**
     * 查询保证金数
     *
     * @param reportFormDTO 请求参数
     *                      shopId     店铺
     *                      activeType 活动类型- * 0  '全部' (默认)- * 1 '回填订单'- * 2  '好评晒图'- * 3: '拆红包'- * 4, 助力赠品'- * 5  '新手轮盘'- * 6 '收藏加购'- * 7 '免单抽奖'
     *                      dayTime    指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     *                      startDate  指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     *                      endDate    指标时间为 3 自定义 的情况的 结束时间   格式2019-03-05
     *                      indexArray 指标选项，可以多选，例如 [ "2", "3", "4", "6" ]，；默认[参与人数, 完成人数]
     *                      1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @return 返回新粉丝人数
     */
    @Override
    public BigDecimal sumAmountByFinishActivity(ReportFormDTO reportFormDTO) {
        Map<String, Object> paramsMap = new HashMap<>(7);
        paramsMap.put("merchantId", reportFormDTO.getMerchantId());
        paramsMap.put("shopId", reportFormDTO.getShopId());
        paramsMap.put("userCouponTypeList", UserCouponTypeMapping.getTypeList(reportFormDTO.getActivityType()));
        if (reportFormDTO.getActivityType() == null || reportFormDTO.getActivityType() != BizTypeEnum.OPEN_RED_PACKET.getCode()) {
            paramsMap.put("status", 1);
        }
        paramsMap.put("startTime", DateUtil.dateToString(reportFormDTO.getStartTime(), DateUtil.FORMAT_ONE));
        paramsMap.put("endTime", DateUtil.dateToString(reportFormDTO.getEndTime(), DateUtil.FORMAT_ONE));
        BigDecimal totalAmount = BigDecimal.ZERO;
        // 暂时先以此来确定是否需要sum商品返现 4助力赠品 7免单抽奖
        if (reportFormDTO.getActivityType() == null
                || reportFormDTO.getActivityType() == 4
                || reportFormDTO.getActivityType() == 7
                || reportFormDTO.getActivityType() == 0) {
            totalAmount = totalAmount.add(userCouponMapper.sumAmountByFinishActivityForGoodsPrice(paramsMap));
        }

        List<UserCoupon> userCoupons = userCouponMapper.listAmountByFinishActivityForCommission(paramsMap);
        for (UserCoupon userCoupon : userCoupons) {
            if (StringUtils.isBlank(userCoupon.getCommissionDef())) {
                continue;
            }
            JSONObject jsonObject = JSON.parseObject(userCoupon.getCommissionDef());
            totalAmount = totalAmount.add(jsonObject.getBigDecimal("commissionDef"));
        }

        //  当查询全部保证金消耗时,sql 条件中status=1, 但是拆红包status=0时也要计算在内
        if (reportFormDTO.getActivityType() == null || reportFormDTO.getActivityType() == 0) {
            paramsMap.put("userCouponTypeList", UserPubJoinTypeMapping.getTypeList(BizTypeEnum.OPEN_RED_PACKET.getCode()));
            paramsMap.put("status", 0);
            totalAmount = totalAmount.add(userCouponMapper.sumAmountByFinishActivityForGoodsPrice(paramsMap));
        }

        return totalAmount;
    }

}
