package mf.code.statistics.service.impl;/**
 * create by qc on 2019/6/26 0026
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.FissionUser;
import mf.code.statistics.mongo.repository.FissionUserRepository;
import mf.code.statistics.service.FissionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 裂变记录的操作
 * create by qc on 2019/6/26 0026
 */
@Service
@Slf4j
public class FisionUserServiceImpl implements FissionUserService {

    @Autowired
    private FissionUserRepository fissionUserRepository;

    @Override
    public FissionUser
    insertFissionUser(FissionUser fissionUser) {
        String jsonStr = JSON.toJSONString(fissionUser);
        log.info("mongo before insert fission user log , data={}", jsonStr);

        try {
            return fissionUserRepository.insert(fissionUser);
        } catch (Exception e) {
            log.info("mongo insert fission user error log , data={}", jsonStr);
            e.printStackTrace();
            return null;
        }
    }
}
