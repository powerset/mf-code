package mf.code.statistics.service;

import mf.code.statistics.mongo.domain.LoginLog;

/**
 * create by qc on 2019/7/30 0030
 */
public interface LoginLogService {
    LoginLog insertLoginLog(LoginLog loginLog);
}
