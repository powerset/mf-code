package mf.code.statistics.service;


import mf.code.activity.repo.po.FissionUser;

/**
 * 裂变统计
 * create by qc on 2019/6/26 0026
 */
public interface FissionUserService {

    /**
     * 插入记录
     * @param fissionUser 裂变记录
     * @return
     */
    FissionUser insertFissionUser(FissionUser fissionUser);
}
