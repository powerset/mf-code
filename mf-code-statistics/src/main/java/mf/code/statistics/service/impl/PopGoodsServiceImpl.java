package mf.code.statistics.service.impl;/**
 * create by qc on 2019/6/29 0029
 */

import mf.code.statistics.mongo.domain.UserAction;
import mf.code.statistics.mongo.repository.MonitorDataRepository;
import mf.code.statistics.service.PopGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author gbf
 * 2019/6/29 0029、14:21
 */
@Service
public class PopGoodsServiceImpl implements PopGoodsService {

    @Autowired
    private MonitorDataRepository monitorDataRepository;

    @Override
    public List<UserAction> findPVListByDate(Date begin, Date end, String shopId) {
        return monitorDataRepository.findPVListByDate(begin,end,shopId);
    }
}
