package mf.code.statistics.service;/**
 * create by qc on 2019/6/29 0029
 */

import mf.code.statistics.mongo.domain.UserAction;

import java.util.Date;
import java.util.List;

/**
 * @author gbf
 * 2019/6/29 0029、14:21
 */
public interface PopGoodsService {

    /**
     * 查询商品人气
     * @param begin
     * @param end
     * @param shopId
     * @return
     */
    List<UserAction> findPVListByDate(Date begin, Date end,String shopId);
}
