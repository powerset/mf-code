package mf.code.statistics.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.statistics.mongo.domain.UserAction;
import mf.code.statistics.mongo.repository.MonitorDataRepository;
import mf.code.statistics.service.MonitorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * mf.code.statistics.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-03-13 18:06
 */
@Service
@Slf4j
public class MonitorDataServiceImpl implements MonitorDataService {
    @Autowired
    private MonitorDataRepository monitorDataRepository;

    @Override
    public UserAction insertMonitorData(UserAction monitorData) {
        Date now = new Date();
        monitorData.setCtime(now);
        monitorData.setUtime(now);

        String jsonString = JSON.toJSONString(monitorData);
        log.info("mongo before insert log , data={}", jsonString);

        UserAction insertResult = new UserAction();
        try {
            insertResult = monitorDataRepository.insert(monitorData);
        } catch (Exception e) {
            log.info("mongo insert error log , data={}", jsonString);
            e.printStackTrace();
        }
        return insertResult;
    }
}
