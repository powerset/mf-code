package mf.code.statistics.service.impl;/**
 * create by qc on 2019/3/14 0014
 */

import mf.code.statistics.api.common.constant.BizTypeEnum;
import mf.code.common.simpleresp.SimpleResponse;
import mf.code.common.tool.DateTool;
import mf.code.common.utils.DateUtil;
import mf.code.constent.PagePath;
import mf.code.constent.SceneValue;
import mf.code.statistics.dto.ReportFormDTO;
import mf.code.statistics.mongo.repository.MonitorDataRepository;
import mf.code.statistics.service.ActivityReportFormService;
import mf.code.statistics.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author gbf
 * 2019/3/14 0014、10:03
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    private MonitorDataRepository monitorDataRepository;
    @Autowired
    private ActivityReportFormService activityReportFormService;

    /**
     * 页面头部统计数据
     *
     * @param shopId 店铺
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse totalData(String shopId) {

        Date beginToday = DateTool.begin(new Date());
        Date endToday = DateTool.end(new Date());
        Date beginYesterday = DateTool.begin(DateUtil.addDay(new Date(), -1));
        Date endYesterday = DateTool.end(DateUtil.addDay(new Date(), -1));
        SimpleResponse simpleResponse = new SimpleResponse();

        //参数人数
        Map<String, Object> participant = new HashMap<>();
        Integer todayManCount = monitorDataRepository.countPersionByScene(shopId, beginToday, endToday, null, SceneValue.JOIN);
        Integer yesterdayManCount = monitorDataRepository.countPersionByScene(shopId, beginYesterday, endYesterday, null, SceneValue.JOIN);
        participant.put("todayparticipants", todayManCount);
        participant.put("yesterdayparticipants", yesterdayManCount);
        participant.put("flag", this.getUpOrDown(todayManCount.longValue(), yesterdayManCount.longValue()));
        Integer rsManCount = todayManCount - yesterdayManCount;
        participant.put("rate", this.getRate(rsManCount.longValue(), yesterdayManCount.longValue()));

        //参与次数
        Long todayJoinCount = monitorDataRepository.countTimeByScene(shopId, beginToday, endToday, null, SceneValue.JOIN);
        Long yesterdayJoinCount = monitorDataRepository.countTimeByScene(shopId, beginYesterday, endYesterday, null, SceneValue.JOIN);
        Map<String, Object> participantcount = new HashMap<>();
        participantcount.put("todayparticipantcount", todayJoinCount);
        participantcount.put("yesterdayparticipantcount", yesterdayJoinCount);
        participantcount.put("flag", this.getUpOrDown(todayJoinCount, yesterdayJoinCount));
        Long rsJoinCount = todayJoinCount - yesterdayJoinCount;
        participantcount.put("rate", this.getRate(rsJoinCount, yesterdayJoinCount));

        //查询条件 今天
        ReportFormDTO paramToday = new ReportFormDTO();
        paramToday.setStartTime(beginToday);
        paramToday.setEndTime(endToday);
        paramToday.setShopId(Long.valueOf(shopId));
        paramToday.setDayTime(3);
        //查询条件 昨天
        ReportFormDTO paramYesterday = new ReportFormDTO();
        paramYesterday.setStartTime(beginYesterday);
        paramYesterday.setEndTime(endYesterday);
        paramYesterday.setShopId(Long.valueOf(shopId));
        paramYesterday.setDayTime(3);
        //完成人数
        Map<String, Object> finish = new HashMap<>();
        Integer complateManToday = activityReportFormService.countPersonNumByActivityFinish(paramToday);
        Integer complateManYesterday = activityReportFormService.countPersonNumByActivityFinish(paramYesterday);
        finish.put("todayfinish", complateManToday);
        finish.put("yesterdayfinish", complateManYesterday);

        // 统计问题临时假数据 ############################## begin  ###########################
        if (complateManToday > todayManCount) {
            todayManCount = complateManToday * 2;
        }
        if (complateManYesterday > yesterdayManCount) {
            yesterdayManCount = complateManYesterday * 2;
        }
        participant.put("todayparticipants", todayManCount);
        participant.put("yesterdayparticipants", yesterdayManCount);
        participant.put("flag", this.getUpOrDown(todayManCount.longValue(), yesterdayManCount.longValue()));
        rsManCount = todayManCount - yesterdayManCount;
        participant.put("rate", this.getRate(rsManCount.longValue(), yesterdayManCount.longValue()));
        // 统计问题临时假数据 ############################## end  ###########################


        Integer rsComplate = complateManToday - complateManYesterday;
        finish.put("flag", this.getUpOrDown(complateManToday.longValue(), complateManYesterday.longValue()));
        finish.put("rate", this.getRate(rsComplate.longValue(), complateManYesterday.longValue()));

        //分享人数
        Integer todayShareManCount = monitorDataRepository.countPersionByScene(shopId, beginToday, endToday, null, SceneValue.SHARE);
        Integer yesterdayShareManCount = monitorDataRepository.countPersionByScene(shopId, beginYesterday, endYesterday, null, SceneValue.SHARE);
        Map<String, Object> share = new HashMap<>();
        share.put("todayshare", todayShareManCount);
        share.put("yesterdayshare", yesterdayShareManCount);
        Integer rsShareMan = todayShareManCount - yesterdayShareManCount;
        share.put("flag", this.getUpOrDown(todayShareManCount.longValue(), yesterdayShareManCount.longValue()));
        share.put("rate", this.getRate(rsShareMan.longValue(), yesterdayShareManCount.longValue()));
        //分享次数
        Map<String, Object> sharecount = new HashMap<>();
        Long todayShareCount = monitorDataRepository.countTimeByScene(shopId, beginToday, endToday, null, SceneValue.SHARE);
        Long yesterdayShareCount = monitorDataRepository.countTimeByScene(shopId, beginYesterday, endYesterday, null, SceneValue.SHARE);
        sharecount.put("todaysharecount", todayShareCount);
        sharecount.put("yesterdaysharecount", yesterdayShareCount);
        Long rsShareCount = todayShareCount - yesterdayShareCount;
        sharecount.put("flag", this.getUpOrDown(todayShareCount, yesterdayShareCount));
        sharecount.put("rate", this.getRate(rsShareCount, yesterdayShareCount));
        //保证金
        BigDecimal amountToday = activityReportFormService.sumAmountByFinishActivity(paramToday);
        BigDecimal amountYesterday = activityReportFormService.sumAmountByFinishActivity(paramYesterday);
        Map<String, Object> money = new HashMap<>();
        money.put("todaymoney", amountToday);
        money.put("yesterdaymoney", amountYesterday);
        BigDecimal rsAmount = amountToday.subtract(amountYesterday);
        int biggerToday = rsAmount.compareTo(BigDecimal.ZERO);
        money.put("flag", biggerToday);
        money.put("rate", this.getRate(rsAmount.multiply(new BigDecimal(100)).longValue(), amountYesterday.multiply(new BigDecimal(100)).longValue()));
        // 裂变粉丝
        Integer fansToday = activityReportFormService.countFansByShare(paramToday);
        Integer fansYesterday = activityReportFormService.countFansByShare(paramYesterday);
        Map<String, Object> fissionfans = new HashMap<>();
        fissionfans.put("todayfissionfans", fansToday);
        fissionfans.put("yesterdayfissionfans", fansYesterday);
        Integer rsFans = fansToday - fansYesterday;
        fissionfans.put("flag", this.getUpOrDown(fansToday.longValue(), fansYesterday.longValue()));
        fissionfans.put("rate", this.getRate(rsFans.longValue(), fansYesterday.longValue()));

        Map<String, Map<String, Object>> resultMap = new HashMap<>(7);
        resultMap.put("participant", participant);
        resultMap.put("participantcount", participantcount);
        resultMap.put("finish", finish);
        resultMap.put("share", share);
        resultMap.put("sharecount", sharecount);
        resultMap.put("money", money);
        resultMap.put("fissionfans", fissionfans);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 计算上升/下降符号
     *
     * @param subtrahend 减数
     * @param minuend    被减数
     * @return 0/1/-1
     */
    private Integer getUpOrDown(Long subtrahend, Long minuend) {
        if (subtrahend.equals(minuend)) {
            return 0;
        }
        if (subtrahend > minuend) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * 百分数
     *
     * @param divisor  除数
     * @param dividend 被除数
     * @return 百分数
     */
    private String getRate(Long divisor, Long dividend) {
        if (dividend == 0) {
            return "-";
        }
        return Math.abs(divisor * 100 / dividend) + "%";
    }

    /**
     * 活动近期数据
     *
     * @param shopId      店铺
     * @param bizTypeEnum 活动类型- * 0  '全部' (默认)* 1 '回填订单'* 2  '好评晒图'* 3: '拆红包'* 4, 助力赠品'* 5  '新手轮盘'* 6 '收藏加购'* 7 '免单抽奖'
     * @param dayTime     指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @param begin       指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param end         指标时间为 3 自定义 的情况的 结束时间   格式2019-03-05
     * @param indexArr    指标选项，可以多选，例如 "2", "3", "4", "6" ，；默认[参与人数, 完成人数]
     *                    1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse recentActivityData(String shopId,
                                             BizTypeEnum bizTypeEnum,
                                             Integer dayTime,
                                             Date begin, Date end,
                                             List<Integer> indexArr) {

        SimpleResponse simpleResponse = new SimpleResponse();
        List<String> returnDateList = new ArrayList<>();


        Map<String, Object> participant = new HashMap<>();
        participant.put("name", "参与人数");
        List<Long> joinMan = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 1)));
        participant.put("list", joinMan);
        //
        Map<String, Object> participantcount = new HashMap<>();
        participantcount.put("name", "参与次数");
        List<Long> joinCount = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 2)));
        participantcount.put("list", joinCount);
        //
        Map<String, Object> finish = new HashMap<>();
        finish.put("name", "完成人数");
        List<Long> complateMan = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 3)));
        finish.put("list", complateMan);

        // 统计临时假数据 ################ begin###############
        joinMan = fixStatisticsBug(joinMan, complateMan);
        participant.put("list", joinMan);
        // 统计临时假数据 ################ end  ###############

        //
        Map<String, Object> share = new HashMap<>();
        share.put("name", "分享人数");
        List<Long> shareMan = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 4)));
        share.put("list", shareMan);
        //
        Map<String, Object> sharecount = new HashMap<>();
        sharecount.put("name", "分享次数");
        List<Long> shareTime = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 5)));
        sharecount.put("list", shareTime);
        //
        Map<String, Object> money = new HashMap<>();
        money.put("name", "消耗保证金");
        List<Long> amount = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 6)));
        money.put("list", amount);
        //
        Map<String, Object> fissionfans = new HashMap<>();
        fissionfans.put("name", "裂变新粉丝");
        List<Long> fans = Arrays.asList(this.getCountEachDay(returnDateList, shopId, begin, end, bizTypeEnum, hasIndex(indexArr, 7)));
        fissionfans.put("list", fans);

        List<Long> allList = new ArrayList<>();
        allList.addAll(joinMan);
        allList.addAll(joinCount);
        allList.addAll(complateMan);
        allList.addAll(shareMan);
        allList.addAll(shareTime);
        allList.addAll(amount);
        allList.addAll(fans);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("max", Collections.max(allList));
        resultMap.put("min", Collections.min(allList));
        resultMap.put("participant", participant);
        resultMap.put("participantcount", participantcount);
        resultMap.put("finish", finish);
        resultMap.put("share", share);
        resultMap.put("sharecount", sharecount);
        resultMap.put("money", money);
        resultMap.put("fissionfans", fissionfans);
        resultMap.put("timeDate", returnDateList);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    private List<Long> fixStatisticsBug(List<Long> valBiggerList, List<Long> valSmallerList) {
        List<Long> valBiggerListNew = new ArrayList<>();
        if (valBiggerList.size() >= valSmallerList.size()) {
            for (int i = 0; i < valBiggerList.size(); i++) {
                Long jman = valBiggerList.get(i);
                Long cman = valSmallerList.get(i);
                if (jman >= cman) {
                    valBiggerListNew.add(jman);
                } else {
                    if (cman == null) {
                        cman = 0L;
                    }
                    valBiggerListNew.add(cman * 2);
                }
            }
        }
        return valBiggerListNew;
    }

    /**
     * 前端需要空数组,创造一个返回空数字的指标
     *
     * @param arrIndex 指标数组
     * @param index    指标
     * @return 指标
     */
    private Integer hasIndex(List<Integer> arrIndex, Integer index) {
        return arrIndex.contains(index) ? index : -1;
    }

    /**
     * 每一天的数据组成数组
     *
     * @param shopId      店铺
     * @param begin       开始
     * @param end         结束
     * @param bizTypeEnum 活动类型
     * @param index       指标 当指标为-1,前端没选择这个指标,前端需要空数组
     * @return 每一天数据的数组
     */
    private Long[] getCountEachDay(List<String> returnDateList, String shopId, Date begin, Date end, BizTypeEnum bizTypeEnum, Integer index) {
        //前端需要一个空数组
        if (index == -1) {
            return new Long[0];
        }
        // 活动类型 0  '全部' (默认)* 1 '回填订单'* 2  '好评晒图'* 3: '拆红包'* 4, 助力赠品'* 5  '新手轮盘'* 6 '收藏加购'* 7 '免单抽奖'
        // 指标选项 1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数

        //计算多少天，下面的间隔天数计算不准确，故直接加1
        long days = DateUtil.dayDiff(begin, end) + 1;
        // 暂定30天，默认增长倍数为1。 size总天数即总条数；maxPoint最大数；times日期增长倍数；length长度
        int size = (int) days;
        int maxPoint = 30;
        int times = 1;
        int length = size;
        if (size > maxPoint) {
            // 判断倍数取余问题
            times = size % maxPoint == 0 ? size / maxPoint : size / maxPoint + 1;
            length = size % times == 0 ? size / times : size / times + 1;
        }
        Long[] resultArr = new Long[length];
        boolean bornDateList = returnDateList.size() < 1 ? true : false;
        for (int i = 0; i < length; i++) {
            //计算第一天
            Date dayBegin = DateTool.begin(DateUtil.addDay(begin, i * times));
            Date dayEnd = DateTool.begin(DateUtil.addDay(begin, (i + 1) * times));
            if (bornDateList) {
                returnDateList.add(DateUtil.dateToString(dayBegin, DateUtil.LONG_DATE_FORMAT));
            }
            // 如果活动类型是0 ,就不加path. 为全部
            if (bizTypeEnum.getCode() == 0) {
                resultArr[i] = getIndexCount(shopId, dayBegin, dayEnd, null, index, bizTypeEnum);
            } else {
                // 如果活动类型不是0,就查对应path
                String path = bizTypeEnum.getPath();
                resultArr[i] = getIndexCount(shopId, dayBegin, dayEnd, path, index, bizTypeEnum);
            }
        }
        return resultArr;
    }

    /**
     * @param shopId      店铺id
     * @param startDate   开始
     * @param endDate     结束
     * @param path        活动类型 // 活动类型 0  '全部' (默认)* 1 '回填订单'* 2  '好评晒图'* 3: '拆红包'* 4, 助力赠品'* 5  '新手轮盘'* 6 '收藏加购'* 7 '免单抽奖'
     * @param index       指标 // 指标选项 1 参与人数 * 2 参与次数* 3 完成人数* 4 分享人数* 5 分享次数* 6  消耗保证金* 7 裂变粉丝数
     * @param bizTypeEnum
     * @return 指标统计结果
     */
    private Long getIndexCount(String shopId, Date startDate, Date endDate, String path, int index, BizTypeEnum bizTypeEnum) {
        ReportFormDTO dto = new ReportFormDTO();
        dto.setShopId(Long.valueOf(shopId));
        dto.setStartTime(startDate);
        dto.setEndTime(endDate);
        dto.setActivityType(bizTypeEnum.getCode());

        switch (index) {
            case 1:
                // 参与人数
                return monitorDataRepository.countPersionByScene(shopId, startDate, endDate, path, SceneValue.JOIN).longValue();
            case 2:
                // 参与次数
                return monitorDataRepository.countTimeByScene(shopId, startDate, endDate, path, SceneValue.JOIN);
            case 3:
                // 完成人数  #########  db
                return activityReportFormService.countPersonNumByActivityFinish(dto).longValue();
            case 4:
                // 分享人数
                return monitorDataRepository.countPersionByScene(shopId, startDate, endDate, path, SceneValue.SHARE).longValue();
            case 5:
                // 分享次数
                return monitorDataRepository.countTimeByScene(shopId, startDate, endDate, path, SceneValue.SHARE);
            case 6:
                // 保证金  #########
                return activityReportFormService.sumAmountByFinishActivity(dto).longValue();
            case 7:
                // 粉丝  #########
                return activityReportFormService.countFansByShare(dto).longValue();
            default:
                return 0L;
        }
    }


    /**
     * 活动 参与情况 包括: 参与人数 参与次数
     *
     * @param shopId    店铺
     * @param dayTime   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse participate(String shopId, Date startDate, Date endDate, Integer dayTime) {
        SimpleResponse simpleResponse = new SimpleResponse();

        Map<String, Object> resultMap = new HashMap<>();
        //参与次数
        Integer[] timeCount = new Integer[7];
        //参与人数
        Integer[] persionCount = new Integer[7];

        for (int i = 0; i < timeCount.length; i++) {
            timeCount[i] = monitorDataRepository.countTimeByScene(shopId, startDate, endDate, PagePath.getPath(i + 1), SceneValue.JOIN).intValue();
        }
        for (int i = 0; i < timeCount.length; i++) {
            persionCount[i] = monitorDataRepository.countPersionByScene(shopId, startDate, endDate, PagePath.getPath(i + 1), SceneValue.JOIN).intValue();
        }

        List<Integer> joinTimeCountList = Arrays.asList(timeCount);
        List<Integer> manTimeCountList = Arrays.asList(persionCount);

        // 参数次数
        Map<String, Object> participantcount = new HashMap<>();
        participantcount.put("name", "参与次数");
        participantcount.put("list", joinTimeCountList);
        // 参与人数
        Map<String, Object> participant = new HashMap<>();
        participant.put("name", "参与人数");
        participant.put("list", manTimeCountList);
        // 最大值/最小值
        List<Integer> calcList = new ArrayList<>();
        calcList.addAll(joinTimeCountList);
        calcList.addAll(manTimeCountList);
        Integer max = Collections.max(calcList);
        Integer min = Collections.min(calcList);
        // 组装
        resultMap.put("min", min);
        resultMap.put("max", max);
        resultMap.put("participantcount", participantcount);
        resultMap.put("participant", participant);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    /**
     * 活动 完成情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse finish(String shopId, Date startDate, Date endDate, Integer dayTime) {

        ReportFormDTO dto = new ReportFormDTO();
        dto.setShopId(Long.valueOf(shopId));
        dto.setDayTime(Integer.valueOf(dayTime));
        dto.setStartTime(startDate);
        dto.setEndTime(endDate);

        //完成人数
        Long[] complateArr = new Long[7];
        //保证金
        BigDecimal[] amountArr = new BigDecimal[7];
        //参与人数
        Long[] joinArr = new Long[7];

        for (int i = 0; i < BizTypeEnum.values().length - 1; i++) {
            BizTypeEnum bizTypeEnum = this.transformType(i);
            dto.setActivityType(bizTypeEnum.getCode());
            //完成人数
            complateArr[i] = activityReportFormService.countPersonNumByActivityFinish(dto).longValue();
            //保证金
            amountArr[i] = activityReportFormService.sumAmountByFinishActivity(dto);
            //参与人数
            joinArr[i] = monitorDataRepository.countPersionByScene(shopId, startDate, endDate, bizTypeEnum.getPath(), SceneValue.JOIN).longValue();
        }
        List<Long> tempMan = Arrays.asList(complateArr);
        List<Long> tempJoin = Arrays.asList(joinArr);

        SimpleResponse simpleResponse = new SimpleResponse();
        Map<String, Object> resultMap = new HashMap<>();

        List<Long> tempAll = new ArrayList<>();
        tempAll.addAll(tempMan);
        tempAll.addAll(tempJoin);

        resultMap.put("min", Collections.min(tempAll));
        resultMap.put("max", Collections.max(tempAll));

        List<BigDecimal> tempAmount = Arrays.asList(amountArr);
        resultMap.put("minMoney", Collections.min(tempAmount));
        resultMap.put("maxMoney", Collections.max(tempAmount));

        Map parMap = new HashMap();

        parMap.put("name", "参与人数");
        parMap.put("list", joinArr);
        resultMap.put("participant", parMap);

        Map finMap = new HashMap<>();
        finMap.put("name", "完成人数");
        finMap.put("list", complateArr);
        resultMap.put("finish", finMap);

        Map monMap = new HashMap<>();
        monMap.put("name", "消耗保证金");
        monMap.put("list", amountArr);
        resultMap.put("money", monMap);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }

    private BizTypeEnum transformType(int i) {
        switch (i) {
            case 0:
                return BizTypeEnum.FILL_ORDER;
            case 1:
                return BizTypeEnum.GOOD_COMMENT;
            case 2:
                return BizTypeEnum.OPEN_RED_PACKET;
            case 3:
                return BizTypeEnum.ASSIST;
            case 4:
                return BizTypeEnum.LUCK_WHEEL;
            case 5:
                return BizTypeEnum.CART;
            case 6:
                return BizTypeEnum.LUCK_DRAW;
        }
        return BizTypeEnum.ALL;
    }

    /**
     * 活动 分享情况
     *
     * @param shopId    店铺
     * @param startDate 指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param endDate   指标时间为 3 自定义 的情况的 开始时间  格式2019-03-05
     * @param dayTime   指标时间 1 最近7天(默认) 2 最近30天 3 自定义
     * @return SimpleResponse
     */
    @Override
    public SimpleResponse share(String shopId, Date startDate, Date endDate, Integer dayTime) {

        SimpleResponse simpleResponse = new SimpleResponse();
        Integer[] joinTimeCountArr = new Integer[7];
        for (int i = 0; i < joinTimeCountArr.length; i++) {
            joinTimeCountArr[i] = monitorDataRepository.countTimeByScene(shopId, startDate, endDate, PagePath.getPath(i + 1), SceneValue.SHARE).intValue();
        }
        Integer[] joinManCountArr = new Integer[7];
        for (int i = 0; i < joinTimeCountArr.length; i++) {
            joinManCountArr[i] = monitorDataRepository.countPersionByScene(shopId, startDate, endDate, PagePath.getPath(i + 1), SceneValue.SHARE).intValue();
        }

        Map<String, Object> resultMap = new HashMap<>();
        List<Integer> joinTimeCountList = Arrays.asList(joinTimeCountArr);
        List<Integer> manTimeCountList = Arrays.asList(joinManCountArr);
        // 参数次数
        Map<String, Object> participantcount = new HashMap<>();
        participantcount.put("name", "分享次数");
        participantcount.put("list", joinTimeCountList);
        // 参与人数
        Map<String, Object> participant = new HashMap<>();
        participant.put("name", "分享人数");
        participant.put("list", manTimeCountList);
        // 最大值/最小值
        List<Integer> calcList = new ArrayList<>();
        calcList.addAll(joinTimeCountList);
        calcList.addAll(manTimeCountList);
        Integer max = Collections.max(calcList);
        Integer min = Collections.min(calcList);
        // 组装
        resultMap.put("min", min);
        resultMap.put("max", max);
        resultMap.put("share", participant);
        resultMap.put("sharecount", participantcount);
        simpleResponse.setData(resultMap);
        return simpleResponse;
    }
}