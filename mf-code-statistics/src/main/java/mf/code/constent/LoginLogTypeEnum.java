package mf.code.constent;

/**
 * create by qc on 2019/7/31 0031
 * <p>
 * 登陆记录-登陆统计角色类型
 */
public enum LoginLogTypeEnum {


    MERCHANT(1, "商户"),
    SHOP(2, "店铺"),
    USER_NOT_MANAGER(3, "非店长"),
    SHOP_MANAGER(4, "店长"),
    ;

    private Integer code;
    private String message;

    LoginLogTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
