package mf.code;

import lombok.extern.slf4j.Slf4j;
import mf.code.common.aop.SQLInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Properties;


/**
 * 启动类
 * @author gel
 */
@SpringBootApplication
@MapperScan(basePackages = "mf.code.*.repo.dao")
@EnableEurekaClient
@EnableScheduling
@EnableFeignClients(basePackages = "mf.code.*.api.feignclient")
@EnableCircuitBreaker//启用熔断
@EnableAsync
@Slf4j
public class MfCodeStatisticsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MfCodeStatisticsApplication.class, args);
        log.info("ELK启动日志标识:MF-CODE-STATISTICS");
    }

    @Bean
    public SQLInterceptor sqlStatsInterceptor() {
        SQLInterceptor sqlStatsInterceptor = new SQLInterceptor();
        Properties properties = new Properties();
        properties.setProperty("dialect", "mysql");
        sqlStatsInterceptor.setProperties(properties);
        return sqlStatsInterceptor;
    }
}
