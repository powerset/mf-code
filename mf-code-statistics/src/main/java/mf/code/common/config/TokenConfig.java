package mf.code.common.config;


import mf.code.common.aop.SellerTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * mf.code.common.config
 * Description:
 *
 * @author: gel
 * @date: 2018-10-31 17:28
 */
@Configuration
public class TokenConfig extends WebMvcConfigurationSupport {

    /**
     * 添加拦截器之前先自己创建一下这个Spring Bean
     * 将拦截器注册为bean也交给spring管理,这样拦截器内部注入其他的bean对象也就可以被spring识别了
     */
    @Bean
    SellerTokenInterceptor sellerTokenInterceptor() {
        return new SellerTokenInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(sellerTokenInterceptor()).addPathPatterns("/api/seller/**");

        super.addInterceptors(registry);
    }

    /**
     * 静态资源
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }
}
