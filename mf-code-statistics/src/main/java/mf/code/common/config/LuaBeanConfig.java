package mf.code.common.config;/**
 * create by qc on 2019/8/2 0002
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

import java.util.List;

/**
 * 实时计算访问/支付等等 人数统计相关的场景使用
 *
 * @author gbf
 * 2019/8/2 0002、14:47
 */
@Configuration
public class LuaBeanConfig {

    /**
     * 使用lua脚本 / redis set /redis zset 完成即时排行榜功能
     *
     * @return
     */
    @Bean("realTimeRankScript")
    public DefaultRedisScript<List> realTimeRank() {
        DefaultRedisScript<List> defaultRedisScript = new DefaultRedisScript<>();
        defaultRedisScript.setResultType(List.class);
        defaultRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("lua/realTimeRankScript.lua")));
        return defaultRedisScript;
    }
}
