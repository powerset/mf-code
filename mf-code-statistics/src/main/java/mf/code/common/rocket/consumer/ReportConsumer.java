package mf.code.common.rocket.consumer;/**
 * create by qc on 2019/6/25 0025
 */

import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.utils.DateUtil;
import mf.code.statistics.mongo.domain.UserAction;
import mf.code.statistics.service.MonitorDataService;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 对前端埋点上报的消费-访问人次
 *
 * @author gbf
 * 2019/6/25 0025、16:57
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "report", consumerGroup = "report-consumer")
@RequiredArgsConstructor
public class ReportConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    private final MonitorDataService monitorDataService;
    private final StringRedisTemplate stringRedisTemplate;
    @Qualifier("realTimeRankScript")
    private final DefaultRedisScript<List> realTimeRankScript;

    @Override
    public void onMessage(MessageExt message) {
        String msgValue = new String(message.getBody(), Charset.forName("UTF-8"));
        this.doConsumer(msgValue);
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

    /**
     * 消费队列
     *
     * @param jsonMsg
     */
    private void doConsumer(String jsonMsg) {

        log.info(">>>>>>>>>>>>>>>>>>> 前端埋点上报-消费消息-开始,消息体={}", jsonMsg);
        UserAction userAction = null;
        try {
            userAction = JSON.parseObject(jsonMsg, UserAction.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userAction == null) {
            log.info(">>>>>>>>>>>>>>>>>>> 前端埋点上报-消费消息-错误,消息体={}", jsonMsg);
            return;
        }
        // 前端埋点上报 写mongodb
        monitorDataService.insertMonitorData(userAction);
        log.info(">>>>>>>>>>>>>>>>>>> 前端埋点上报-消费消息-结束");

        String uid = userAction.getUid();
        if (StringUtils.isBlank(uid)) {
            log.info(">>>>>>>>>>>>>>>>>>> 前端埋点上报-消费消息-错误,消息体中用户id不合法");
            return;
        }

        String shopId = userAction.getSid();
        String goodsIdStr = userAction.getGoodsId();

        List executeList;
        String today = DateUtil.dateToString(new Date(), DateUtil.getYYYYMMDD());

        if (!StringUtils.isBlank(shopId)) {
            log.info("^^^^^^^^^^^^^^^^^^^ 店铺人气榜-消费-开始");
            List<String> shopUVLuaKey = new ArrayList<>();
            shopUVLuaKey.add(RedisKeyConstant.Statistics.Shop.SHOP_UV_RANKING_SET_RT + today + ":" + shopId);
            shopUVLuaKey.add(RedisKeyConstant.Statistics.Shop.SHOP_UV_RANKING_ZSET_RT + today);
            executeList = stringRedisTemplate.execute(realTimeRankScript, shopUVLuaKey, uid, shopId);
            log.info("^^^^^^^^^^^^^^^^^^^ LUA-店铺人气脚本执行结果={}", executeList);
        }


        if (StringUtils.isBlank(goodsIdStr) || "-1".equals(goodsIdStr)) {
            log.info(">>>>>>>>>>>>>>>>>>> 前端埋点上报-消费消息-错误,消息体中商品id不合法");
            return;
        }
        List<String> goodsUVLuaKeyList = new ArrayList<>();
        goodsUVLuaKeyList.add(RedisKeyConstant.Statistics.Goods.GOODS_RANKING_SET_RT_PLAT + today + ":" + goodsIdStr);
        goodsUVLuaKeyList.add(RedisKeyConstant.Statistics.Goods.GOODS_RANKING_ZSET_RT_PLAT + today);
        executeList = stringRedisTemplate.execute(realTimeRankScript, goodsUVLuaKeyList, uid, goodsIdStr);
        log.info("^^^^^^^^^^^^^^^^^^^ LUA-商品人气脚本执行结果={}", executeList);
    }
}
