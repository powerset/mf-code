package mf.code.common.rocket.consumer;/**
 * create by qc on 2019/6/25 0025
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.constent.LoginLogTypeEnum;
import mf.code.merchant.dto.MerchantLoginResultDTO;
import mf.code.statistics.mongo.domain.LoginLog;
import mf.code.statistics.service.LoginLogService;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.Date;

/**
 * 后端埋点-商户登陆埋点
 *
 * @author gbf
 * 2019/6/25 0025、16:57
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "merchant_login", consumerGroup = "statistics-merchant-login")
public class MerchantLoginReportConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    @Autowired
    private LoginLogService loginLogService;

    @Override
    public void onMessage(MessageExt message) {
        String msgValue = new String(message.getBody(), Charset.forName("UTF-8"));
        this.doConsumer(msgValue);
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

    /**
     * 消费队列
     *
     * @param jsonMsg
     */
    private void doConsumer(String jsonMsg) {
        try {
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^ 商户登陆埋点-开始");
            MerchantLoginResultDTO merchantLoginResultDTO = JSON.parseObject(jsonMsg, MerchantLoginResultDTO.class);
            LoginLog loginLog = new LoginLog();
            loginLog.setType(LoginLogTypeEnum.MERCHANT.getCode().toString());
            loginLog.setUid(merchantLoginResultDTO.getMerchantId().toString());
            loginLog.setVersion("1.0.0");
            loginLogService.insertLoginLog(loginLog);
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^ 商户登陆埋点-结束,消息体={}", jsonMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
