package mf.code.common.rocket.consumer;/**
 * create by qc on 2019/6/25 0025
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mf.code.activity.repo.po.FissionUser;
import mf.code.statistics.service.FissionUserService;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

/**
 * 对登陆埋点上报的消费-裂变人数
 *
 * @author gbf
 * 2019/6/25 0025、16:57
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "fissionReport", consumerGroup = "report-fission-consumer")
public class FissionReportConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    @Autowired
    private FissionUserService fissionUserService;

    @Override
    public void onMessage(MessageExt message) {
        String msgValue = new String(message.getBody(), Charset.forName("UTF-8"));
        this.doConsumer(msgValue);
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

    /**
     * 消费队列
     * @param jsonMsg
     */
    private void doConsumer(String jsonMsg) {
        try{
            FissionUser fissionUser = JSON.parseObject(jsonMsg, FissionUser.class);
            log.info(">>>>>>>>>>>>>>>>>>> 登陆上报-裂变人数-消息消费-开始-fissionUser={} >>>>>>>>>>>>>>>>>>>>>>>>>",fissionUser);
            fissionUserService.insertFissionUser(fissionUser);
            log.info(">>>>>>>>>>>>>>>>>>> 登陆上报-裂变人数-消息消费-成功-结束 >>>>>>>>>>>>>>>>>>>>>>>>>");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
