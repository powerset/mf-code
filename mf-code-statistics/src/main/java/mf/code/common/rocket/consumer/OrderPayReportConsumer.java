package mf.code.common.rocket.consumer;/**
 * create by qc on 2019/6/25 0025
 */

import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.utils.Assert;
import mf.code.common.utils.DateUtil;
import mf.code.order.dto.GoodsOrderResp;
import mf.code.statistics.service.FissionUserService;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

/**
 * 对登陆埋点上报的消费-裂变人数
 *
 * @author gbf
 * 2019/6/25 0025、16:57
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "order_pay", consumerGroup = "report-orderpay-consumer")
@RequiredArgsConstructor
public class OrderPayReportConsumer implements RocketMQListener<MessageExt>, RocketMQPushConsumerLifecycleListener {
    private final FissionUserService fissionUserService;
    private final StringRedisTemplate stringRedisTemplate;

    @Override
    public void onMessage(MessageExt message) {
        String msgValue = new String(message.getBody(), Charset.forName("UTF-8"));
        this.doConsumer(msgValue);
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }

    /**
     * 消费队列
     *
     * @param jsonMsg
     */
    private void doConsumer(String jsonMsg) {
        try {
            GoodsOrderResp goodsOrder = JSON.parseObject(jsonMsg, GoodsOrderResp.class);
            Long goodsId = goodsOrder.getGoodsId();
            if (goodsId == null) {
                log.error(">>>>>>>>>>>>>>>>>>>> 订单支付-消息消费-错误-商品id不合法");
                return;
            }
            Long shopId = goodsOrder.getShopId();
            if (shopId == null) {
                log.error(">>>>>>>>>>>>>>>>>>>> 订单支付-消息消费-错误-店铺id不合法");
                return;
            }
            String saleNumberStr = goodsOrder.getNumber();
            Assert.isNumber(saleNumberStr, 1, ">>>>>>>>>>>>>>>>>>>> 订单支付-消息消费-错误-销量不合法");


            String goodsIdStr = goodsId.toString();
            Double saleNumber = Double.parseDouble(saleNumberStr);
            log.info(">>>>>>>>>>>>>>>>>>> 订单支付-消息消费-开始-fissionUser={} >>>>>>>>>>>>>>>>>>>>>>>>>", goodsOrder);
            String today = DateUtil.getYYYYMMDD();
            // 平台商品销售订单排行榜
            stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.Statistics.Goods.GOODS_ORDER_PAY_RT_RANKING_PLAT + today,
                    goodsIdStr, saleNumber);
            // 店铺商品销售订单排行榜
            stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.Statistics.Goods.GOODS_ORDER_PAY_RT_RANKING_SHOP + shopId.toString() + ":" + today,
                    goodsIdStr, saleNumber);
            stringRedisTemplate.opsForZSet().incrementScore(RedisKeyConstant.Statistics.Shop.SHOP_SV_RANKING_ZSET_RT + today,
                    shopId.toString(), saleNumber);
            log.info(">>>>>>>>>>>>>>>>>>> 订单支付-消息消费-结束 >>>>>>>>>>>>>>>>>>>>>>>>>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
