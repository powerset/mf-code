package mf.code.common.aop;/**
 * create by qc on 2019/3/20 0020
 */


import lombok.extern.slf4j.Slf4j;
import mf.code.common.email.EmailService;
import mf.code.common.utils.DateUtil;
import org.aopalliance.intercept.Interceptor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Mongo查询时间监控
 *
 * @author gbf
 * 2019/3/20 0020、14:34
 */
@Component
@Aspect
@Slf4j
public class MongoInterceptor implements Interceptor {

    /**
     * 超时时间 单位:毫秒
     */
    @Value("${mongo.overtime}")
    private Long LIMIT_TIME_MS = 1000L;

    @Autowired
    private EmailService emailService;

    @Around("execution(public * mf.code.statistics.mongo.repository..*.*(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        // 开始时间 ms
        long startMs = System.currentTimeMillis();
        // 执行查询
        Object proceed = proceedingJoinPoint.proceed();

        // 查询参数
        Object[] args = proceedingJoinPoint.getArgs();
        // 类全限定名.方法名(..)
        Signature signature = proceedingJoinPoint.getSignature();

        // 消耗时间
        Long spendTime = System.currentTimeMillis() - startMs;

        if (spendTime > LIMIT_TIME_MS) {
            // 拼接打印参数数组
            StringBuilder builder = new StringBuilder();
            builder.append(" [  ");
            for (int i = 0; i < args.length; i++) {
                Object argObj = args[i];
                if (argObj instanceof Date) {
                    argObj = DateUtil.dateToString((Date) argObj, DateUtil.FORMAT_ONE);
                }
                if (i == 0) {
                    builder.append(argObj);
                } else {
                    builder.append(" , " + argObj);
                }
            }
            builder.append("  ]  ");

            String content = "mf monitor: MONGO QUERY OVERTIME , spend ?ms.\n\n fully-qualified name : ?  ,\n\n args : ? . 配置超时时间 : ?ms";
            // 参数分别对照上面为: 花费时间 /  全限定名 /  参数 /  超时时间配置
            String[] argsArr = {spendTime.toString(), signature.toString(), builder.toString(),LIMIT_TIME_MS.toString()};
            int length = content.split("\\?").length;
            for (int i = 0; i < length - 1; i++) {
                content = content.replaceFirst("\\?", argsArr[i]);
            }
//            emailService.sendSimpleMail("Mongo查询超时.用时:" + spendTime + "ms", content);
            log.error(content.replaceAll("\n",""));
        }
        return proceed;
    }
}
