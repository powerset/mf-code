package mf.code.common.aop;/**
 * create by qc on 2019/3/20 0020
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.Interceptor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;


/**
 * 整个服务执行时间监控
 *
 * @author gbf
 * 2019/3/20 0020、13:46
 */
@Component
@Aspect
@Slf4j
public class ServiceIntercepter {

    /**
     * 服务超时时间 单位:毫秒
     */
    private static final int LIMIT_TIME_MS = 3000;

    @Around("execution(public * mf.code.*.service..*.*(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startMs = System.currentTimeMillis();

        Signature signature = proceedingJoinPoint.getSignature();
        String name = signature.getDeclaringTypeName() + "#" + signature.getName();
        Object[] args = proceedingJoinPoint.getArgs();

        String argsStr = JSON.toJSONString(args.length == 1 ? args[0] : Arrays.asList(args));
        log.info("serviceLog-before,serviceName={}, args={}", name, argsStr);
        //
        Object result = proceedingJoinPoint.proceed();
        //
        log.info("serviceLog-end,serviceName={},  args={}, return={}", name, argsStr, JSON.toJSONString(result));
        long ms = System.currentTimeMillis() - startMs;
        if (ms > LIMIT_TIME_MS) {
            // TODO 邮件报警
            log.error("mf monitor: SERVICE OVERTIME , spend {}ms. fully-qualified name : {}  , args : {} , ", ms, signature, args);
        }
        return result;
    }
}
