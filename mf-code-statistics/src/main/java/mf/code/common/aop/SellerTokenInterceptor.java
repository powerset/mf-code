package mf.code.common.aop;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mf.code.common.simpleresp.ApiStatusEnum;
import mf.code.common.utils.TokenUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * mf.code.common.aop
 * Description: 添加最基本的商户端token校验
 *
 * @author: gel
 * @date: 2018-10-31 15:28
 */
@Slf4j
public class SellerTokenInterceptor extends HandlerInterceptorAdapter {

    @Value("${seller.token.debug.mode}")
    private Integer debugMode;

    @Value("${seller.token.name}")
    private String tokenName;
    @Value("${seller.uid.name}")
    private String uidName;

    /**
     * 不能区分小程序端和商户端平台端TokenInterceptor
     *
     * @param request
     * @param response
     * @param handler
     * @return
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        HttpSession session = request.getSession();
        // 如果关掉校验直接通过
        if (debugMode != null && debugMode == 1) {
            Object debugToken = request.getHeader("debugToken");
            if (debugToken == null) {
                return tokenErrorReturn(response, "debugToken不存在");
            }
            // 这里不用放入session,因为参数在前面的filter已经填充了
            session.setAttribute(uidName, debugToken);
            return true;
        }

        Object merchant = session.getAttribute(uidName);
        if (null == merchant) {
            log.error("会话不存在merchant数据");
            return tokenErrorReturn(response, "会话不存在merchant数据");
        }
        Cookie[] cookies = request.getCookies();
        if (cookies == null || cookies.length == 0) {
            log.error("cookie数据不存在");
            return tokenErrorReturn(response, "cookie数据不存在");
        }
        String token = "";
        for (Cookie cookie : cookies) {
            if (tokenName.equals(cookie.getName())) {
                token = cookie.getValue();
                break;
            }
        }
        if (StringUtils.isBlank(token)) {
            log.error("指定cookie数据不存在");
            return tokenErrorReturn(response, "指定cookie数据不存在");
        }
        /**
         *   uid 用户id，商户id，平台运营用户id
         *   user 小程序openid，微信公众号openid，商户手机号，平台运营用户手机号
         *   ctime 用户，商户，平台运营用户的数据记录的创建时间，yyyyMMddHHmmss格式
         *   now 系统时间，yyyyMMddHHmmss格式
         *   digest
         *
         *   XXX 固定值，场景不同，结尾语不同
         *   YYY 固定值，场景不同，结尾语不同
         *   ZZZ 固定值，场景不同，结尾语不同
         */
        Map<String, String> stringObjectMap = TokenUtil.decryptToken(token, TokenUtil.SAAS);
        if (stringObjectMap == null) {
            log.error("token解密数据为空");
            return tokenErrorReturn(response, "token解密数据为空");
        }
        if (!StringUtils.equals(merchant.toString(), stringObjectMap.get("uid"))) {
            log.error("token解密uid数据 与 会话中merchant数据不匹配");
            return tokenErrorReturn(response, "token解密uid数据 与 会话中merchant数据不匹配");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    }

    private boolean tokenErrorReturn(HttpServletResponse response, String message) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            JSONObject res = new JSONObject();
            res.put("code", ApiStatusEnum.ERROR_TOKEN.getCode());
            res.put("message", ApiStatusEnum.ERROR_TOKEN.getMessage());
            if (message != null) {
                res.put("message", message);
            }
            res.put("data", "");
            out.append(res.toString());
            out.flush();
        } catch (IOException e) {
            try {
                response.sendError(500);
            } catch (IOException e1) {
            }
        }
        return false;
    }


}
