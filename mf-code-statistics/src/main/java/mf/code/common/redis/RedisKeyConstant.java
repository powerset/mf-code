package mf.code.common.redis;

/**
 * Redis key
 */
public class RedisKeyConstant {

    public static final String ERROR_EMAILSENDER = "log:email:send:";//<title>


    /**
     * 统计服务
     */
    public static class Statistics {
        /**
         * 店铺业务
         */
        public static final class Shop {
            /**
             * 店铺人气榜单 zset key
             */
            public static final String SHOP_UV_RANKING_ZSET_RT = "statis:rt:shop:uv:rank:plat:"; //<date>
            /**
             * 店铺人气榜单 set key
             */
            public static final String SHOP_UV_RANKING_SET_RT = "statis:rt:shop:uv:set:plat:";

            public static final String SHOP_SV_RANKING_ZSET_RT =  "statis:rt:shop:sv:rank:plat:";
        }

        /**
         * 商品业务
         */
        public static class Goods {
            /**
             * 商品实时人气榜单
             */
            public static final String GOODS_RANKING_ZSET_RT_PLAT = "statis:rt:goods:uv:rank:plat:";// [<goods>] : [<date>]

            /**
             * 访问人数临时存储 set
             */
            public static final String GOODS_RANKING_SET_RT_PLAT = "statis:rt:goods:temp:set:"; // <date>:<goodsId>

            /**
             * 商品订单支付实时排行榜
             */
            public static final String GOODS_ORDER_PAY_RT_RANKING_PLAT = "statis:rt:goods:pay:rank:plat:";//<date>
            public static final String GOODS_ORDER_PAY_RT_RANKING_SHOP = "statis:rt:goods:pay:rank:shop:";//<shopId>:<date>
        }

        /**
         * 平台人气排行
         */
        public static final String POP_RANKING_LIST_PLAT = "mf:statis:uv:rank:plate:";//<date>
        /**
         * 店铺人气排行
         */
        public static final String POP_RANKING_LIST_SHOP = "mf:statis:uv:rank:shop:";//<shopID>:<date>
        /**
         * 概览-实时概况-支付金额-缓存昨日数据
         */
        public static final String REAL_TIME_PAY_HOUR = "mf:code:statis:realtime:";//<shopId>:<date>
        /**
         * 概览-实时概况-店铺整体看板-用户资产
         */
        public static final String STATISTICS_OVERVIEW_BOARD_USER = "mf:statis:ov:board:user:";//<shopId>:<date>
        /**
         * 概览-实时概况-店铺整体看板-订单资产
         */
        public static final String STATISTICS_OVERVIEW_BOARD_ORDER = "mf:statis:ov:bboard:order:";//<shopId>:<date>
        /**
         * 概览-实时概况-店铺整体看板-订单资产
         */
        public static final String STATISTICS_OVERVIEW_BOARD_ACTIVITY = "mf:statis:ov:board:activity:";//<shopId>:<date>
        /**
         * 概览-交易趋势-近七天
         */
        public static final String STATISTICS_OVERVIEW_TREND_PAY_7 = "mf:statis:ov:trend:pay:last7";//<shopId>
        /**
         * 概览-交易趋势-近30天
         */
        public static final String STATISTICS_OVERVIEW_TREND_PAY_30 = "mf:statis:ov:trend:pay:last30";//<shopId>
        /**
         * 概览-用户趋势-近7天
         */
        public static final String STATISTICS_OVERVIEW_TREND_USER_7 = "mf:statis:ov:trend:user:last7";//<shopId>
        /**
         * 概览-用户趋势-近30天
         */
        public static final String STATISTICS_OVERVIEW_TREND_USER_30 = "mf:statis:ov:trend:user:last30";//<shopId>
        /**
         * 概览-活动趋势-近7天
         */
        public static final String STATISTICS_OVERVIEW_TREND_ACTIVITY_7 = "mf:statis:ov:trend:activity:last7";//<shopId>
        /**
         * 概览-活动趋势-近30天
         */
        public static final String STATISTICS_OVERVIEW_TREND_ACTIVITY_30 = "mf:statis:ov:trend:activity:last30";//<shopId>

    }
}
