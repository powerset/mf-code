package mf.code.common.job;/**
 * create by qc on 2019/6/28 0028
 */

import lombok.extern.slf4j.Slf4j;
import mf.code.common.redis.RedisKeyConstant;
import mf.code.common.utils.DateUtil;
import mf.code.order.feignapi.constant.OrderStatusEnum;
import mf.code.statistics.mongo.domain.UserAction;
import mf.code.statistics.service.PopGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 热销排行榜
 *
 * @author gbf
 * 2019/6/28 0028、20:27
 */

@Slf4j
@Component
@Profile(value = {"test2", "prod", "dev"})
public class PopRankingListScheduledService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private PopGoodsService popGoodsService;

    /**
     * zset步长
     */
    public static final int STEP_SIZE = 1;

    /**
     * 商品人气榜定时任务
     */
    @Scheduled(cron = "1 0 0 * * *")
    public void findPayOrderJob() {
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 商品人气榜 1.准备查询 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        List statList = new ArrayList();
        statList.add(OrderStatusEnum.WILLPAY_OR_REFUNDAPPLY.getCode());
        statList.add(OrderStatusEnum.WILLSEND_OR_REFUND.getCode());
        statList.add(OrderStatusEnum.WILLRECEIVE_OR_REFUND.getCode());
        statList.add(OrderStatusEnum.TRADE_SUCCESS_OR_REFUND_SUCCESS.getCode());
        statList.add(OrderStatusEnum.TRADE_CLOSE_WILLPAY_OR_REFUNDCANCEL.getCode());
        statList.add(OrderStatusEnum.TRADE_CLOSE_SELLERCANCEL_OR_REFUND_FAIL.getCode());
        statList.add(OrderStatusEnum.TRADE_CLOSE_TIMEOUT.getCode());
        statList.add(OrderStatusEnum.TRADE_CLOSE_REFUND_SUCCESS.getCode());

        Date yesterday = DateUtil.addDay(new Date(), -1);
        Date yesterdayBegin = DateUtil.getDateAfterZero(yesterday);
        Date yesterdayEnd = DateUtil.getDateBeforeZero(yesterday);

        String yesterdayStr = DateUtil.dateToString(yesterday, DateUtil.LONG_DATE_FORMAT);

        // 平台排行榜key
        String platZsetYesterdayKey = RedisKeyConstant.Statistics.POP_RANKING_LIST_PLAT + yesterdayStr;

        List<UserAction> userActionList = popGoodsService.findPVListByDate(yesterdayBegin, yesterdayEnd, null);
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 商品人气榜 2.mongo 查询结果 :{}", userActionList);
        for (UserAction userAction : userActionList) {
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 商品人气榜 3.mongo 查询结果 :{}", userAction);
            String sid = userAction.getSid();
            if (sid == null) {
                continue;
            }
            String goodsId = userAction.getGoodsId();
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 商品人气榜 4.获取goodsId={}", goodsId);
            if (goodsId == null || goodsId.equals("") || goodsId.equals("-1")) {
                log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 商品人气榜 5.商品id不合法", goodsId);
                continue;
            }
            log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 商品人气榜 5.商品id合法", goodsId);
            // 店铺排行榜key
            String shopZsetYesterdayKey = RedisKeyConstant.Statistics.POP_RANKING_LIST_SHOP + sid + ":" + yesterdayStr;
            // 平台排行
            stringRedisTemplate.opsForZSet().incrementScore(platZsetYesterdayKey, goodsId, STEP_SIZE);
            // 店铺排行
            stringRedisTemplate.opsForZSet().incrementScore(shopZsetYesterdayKey, goodsId, STEP_SIZE);
        }
    }
}

