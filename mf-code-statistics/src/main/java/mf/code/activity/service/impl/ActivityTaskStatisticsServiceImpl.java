package mf.code.activity.service.impl;

import mf.code.activity.service.ActivityStatisticsService;
import mf.code.activity.service.ActivityTaskStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * mf.code.activity.service.impl
 * Description:
 *
 * @author gel
 * @date 2019-03-14 13:07
 */
@Service
public class ActivityTaskStatisticsServiceImpl implements ActivityTaskStatisticsService {
    @Autowired
    private ActivityStatisticsService activityStatisticsService;
    @PostConstruct
    public void init(){
    }
}
