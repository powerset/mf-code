package mf.code.activity.model;/**
 * create by qc on 2019/7/17 0017
 */

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * excel模型对象
 * @author gbf
 * 2019/7/17 0017、16:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MsActivityExcelPropertyIndexModel extends BaseRowModel {
    @ExcelProperty(value = "订单号",index = 0)
    private String orderNo;
    @ExcelProperty(value = "下单时间",index = 1)
    private String ctime;
    @ExcelProperty(value = "店铺名称",index = 2)
    private String shopName;
    @ExcelProperty(value = "商品ID",index = 3)
    private Long goodsId;
    @ExcelProperty(value = "商品名称",index = 4)
    private String goodsName;
    @ExcelProperty(value = "下单商品数",index = 5)
    private Integer createNumber;
    @ExcelProperty(value = "下单金额",index = 6)
    private BigDecimal createFee;
    @ExcelProperty(value = "支付商品数",index = 7)
    private Integer payNumber;
    @ExcelProperty(value = "支付金额",index = 8)
    private BigDecimal payFee;
    private Date paymentTime;
}
